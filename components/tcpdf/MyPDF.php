<?php
define ('myPDF_MARGIN_TOP', 15);
define ('myPDF_MARGIN_LEFT', 20);
define ('myPDF_MARGIN_RIGHT', 10);

require_once( __DIR__ . '/../../vendor/tcpdf/tcpdf.php');




class MyPDF extends TCPDF
{
    public $model;

    public $report_type;
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image

        if($this->CurOrientation=='L'){
            $img_file = K_PATH_IMAGES.'pdf_background_l.jpg';
            $this->Image($img_file, 0, 0, 297, 210, '', '', '', false, 300, '', false, false, 0);
        }else{
            $img_file = K_PATH_IMAGES.'pdf_background_p.jpg';
            $this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        }

        $this->SetFont('arialn', '',8, '', true);

        // Image example with resizing

        $this->setFillColor();
        $this->Rect(0, 277, 2000, 25,'F',array(),array(176,190,197));
        // $this->Image('images/windmilld-logo.PNG', 6, 3, 70, 17, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        if ($this->model != '') {
            if($this->model->client_reference <> "Not Applicable" && $this->model->client_reference <> ""){
                $clientReference = $this->model->client_reference.', ';
            }else {
                $clientReference = "";
            }

            $header_text=$clientReference.$this->model->reference_number.'<br>'
                .$this->model->building->title.', '.$this->model->building->subCommunities->title.'<br>'.$this->model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$this->model->building->city];
            $this->writeHTMLCell($w = 0, $h = 0, $x = '140', $y = '281', $header_text, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'right', $autopadding = true);


        }


        if ($this->report_type==2) {
            $this->Image('images/windmils-footer.png', 135,1, 65, '', '', '', '', false, 300);
        }



        // $this->Cell(200, 25, $header_text, 0, false, 'C', 0, '', 0, false, 'M', 'M');



        // Image example with resizing

        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }

    // Page footer
    public function Footer() {

        // $this->SetAlpha(0.2);
        // Image example with resizing
        // $background_text = str_repeat('', 50);
        // $this->MultiCell(0, 5, $background_text, 0, 'J', 0, 2, '', '', true, 0, false);
        // $this->Cell(30, 0, 'Bottom-Top', 1, $ln=0, 'C', 0, '', 0, false, 'T', 'T');
        // // $this->Cell(0, 10, $this->getAliasNumPage(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        // $this->Cell(30, 0, 'Top-Center', 1, $ln=0, 'C', 0, '', 0, false, 'T', 'C');
        $this->SetFont('helvetica', '', 13);
        $this->Cell(0, 0, $this->getAliasNumPage(), 0, $ln=0, 'C', 0, '', 0, false, 'C', 'C');


        if ($this->model != '') {
            $this->Image('images/windmils-footer.png', 11, 277.5, 50, '', '', '', '', false, 300);
        }
        $this->writeHTMLCell($w = 0, $h = 0, $x = '150', $y = '200', '', $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'right', $autopadding = true);

        //  $this->Image('images/windmils-footer.png', 10, 280, 55, 13, 'PNG', '', '', true, 300, '', false, false, false, false, false, false);

    }





}

?>
