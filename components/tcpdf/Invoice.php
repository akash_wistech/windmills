<?php

define ('myPDF_MARGIN_TOP', 20);
define ('myPDF_MARGIN_LEFT', 14);
define ('myPDF_MARGIN_RIGHT', 14);
require_once( __DIR__ . '/../../vendor/tcpdf/tcpdf.php');

class Invoice extends TCPDF
{
    public $branch;

    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);

        // $logo = 'images/quotationheaderlogo.png';
        // $this->Image($logo, 65, 5, 70, 0, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);


        // set bacground image
        // $img_file = 'images/proposalbackgroundimage.png';
        $img_file = 'images/windmills-invoice-front1.png';
        $this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

        $this->writeHTMLCell($w, $h, $x, 34, '<hr>', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true);
        
        $text ='<b>Tax Invoice</b>';
        $this->writeHTMLCell($w, $h, $x=100, 29, $text, $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true);


        // if($this->CurOrientation=='L'){
        //   $img_file = K_PATH_IMAGES.'pdf_background_l.jpg';
        //   $this->Image($img_file, 0, 0, 297, 210, '', '', '', false, 300, '', false, false, 0);
        // }else{
        //   $img_file = K_PATH_IMAGES.'pdf_background_p.jpg';
        //   $this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        // }


        // $this->setFillColor(255,255,255);
        // $logo = 'images/quotationlogo.png';
        // $this->Image($logo, 140, 10, 50, 0, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);


        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }

    // Page footer
    public function Footer() {

      $office_phone=''; $mobile=''; $address='';
      if($this->branch->address<>null){
        $address = $this->branch->address;
      }
      if($this->branch->mobile<>null){
        $mobile = ' | M: '.$this->branch->mobile;
      }
      if($this->branch->office_phone<>null){
        $office_phone = ' | T: '.$this->branch->office_phone;
      }

      $addres_line1 = $address.' '.$office_phone.' '.$mobile;
      
        $branch_detail = $addres_line1;
        $branch_detail .= '<br>www.windmillsgroup.com';

        $branch_detail = '<p style="">'.$branch_detail.'</p>';
        
        $this->SetTextColor(128,128,128);
        $this->SetFont('helvetica', 'B', 10);
        $this->writeHTMLCell($w, $h, $x, 274, $branch_detail, $border=0, $ln=0, $fill=0, $reseth=true, $align='C', $autopadding=false);
        

        // $this->setFillColor(255,255,255);
        // $this->SetTextColor(128,128,128);
        // $this->SetFont('helvetica', 'B', 10);

        // $this->writeHTMLCell(120, '', 45, $y=278, $branch_detail, 0, 0, 1, true, 'C', true);


    // $html='';
    //   $property = Yii::$app->appHelperFunctions->getFirstProperty($this->model->id);
    //   if ($property<>null) {
    //     $branchDetail = \app\models\Branch::find()->where(['zone_list' => $property->building->city])->one();
    //   // echo "<pre>"; print_r($branchDetail); echo "</pre>"; die();
    //     if ($branchDetail<>null) {
    //         $html.= $branchDetail->company;
    //         $html.='<br>'.$branchDetail->address.'';
    //         $html.='<br>Office Phone: '.$branchDetail->office_phone.', Mobile: '.$branchDetail->mobile.'';
    //         $html.='<br>'.$branchDetail->email.'';
    //         $html.='<br>'.$branchDetail->website.'';
    //     }
    //   }

        // $footerData = 'Office 604, Barsha Business Centre, Al Barsha 1 Street, Al Barsha 1, Dubai, UAE
        //           <br>Office Phone: +9714-3460290, Mobile +97152-5891366
        //             <br>Email: service@windmillsgroup.com
        //             <br>Website: www.windmillsgroup.com';

        // set color for text
        // $this->setFillColor(255,255,0);
        // $this->SetTextColor(0,0,0);
        // $this->SetFont('', '', 8);
        // $this->writeHTMLCell(120, '', 45, $y=280, $footerData, 0, 0, 1, true, 'C', true);

        // $this->Cell(0, 10, $this->getAliasNumPage(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }


}

?>