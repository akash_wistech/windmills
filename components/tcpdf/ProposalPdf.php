<?php

define ('myPDF_MARGIN_TOP', 20);
define ('myPDF_MARGIN_LEFT', 14);
define ('myPDF_MARGIN_RIGHT', 14);
require_once( __DIR__ . '/../../vendor/tcpdf/tcpdf.php');

class ProposalPdf extends TCPDF
{
    public $model;

    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);

        $logo = 'images/quotationheaderlogo.png';
        $this->Image($logo, 5, 5, 60, 0, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);



        // if($this->CurOrientation=='L'){
        //   $img_file = K_PATH_IMAGES.'pdf_background_l.jpg';
        //   $this->Image($img_file, 0, 0, 297, 210, '', '', '', false, 300, '', false, false, 0);
        // }else{
        //   $img_file = K_PATH_IMAGES.'pdf_background_p.jpg';
        //   $this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        // }


        // $this->setFillColor(255,255,255);
        // $logo = 'images/quotationlogo.png';
        // $this->Image($logo, 140, 10, 50, 0, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);


        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);

        // set the starting point for the page content
        $this->setPageMark();

    }

    // Page footer
    public function Footer() {

        $html='';
        $property = Yii::$app->appHelperFunctions->getFirstProperty($this->model->id);
        $quotation = \app\models\CrmQuotations::findOne($this->model->id);
        if($quotation->type_of_service == 2){
            $city = $quotation->location;
        }else{
            $city = $property->building->city;
        }

        if ($property<>null) {
            // $branchDetail = \app\models\Branch::find()->where(['zone_list' => $property->building->city])->one();
            $branchDetail = \app\models\Branch::find()->where(['zone_list' => $city])->one();
            // echo "<pre>"; print_r($branchDetail); echo "</pre>"; die();
            if ($branchDetail<>null) {
                $html.= $branchDetail->company;
                $html.='<br>'.$branchDetail->address.'';
                $html.='<br>Office Phone: '.$branchDetail->office_phone.', Mobile: '.$branchDetail->mobile.'';
                $html.='<br>'.$branchDetail->email.'';
                $html.='<br>'.$branchDetail->website.'';
            }
        }

        $footerData = 'Windmills Real Estate Valuation Services LLC
                  <br>Office 604, Barsha Business Centre, Al Barsha 1 Street, Al Barsha 1, Dubai, UAE
                  <br>Office Phone: +9714-3460290, Mobile +97152-5891366
                    <br>Email: service@windmillsgroup.com
                    <br>Website: www.windmillsgroup.com';

        // set color for text
        $this->setFillColor(250, 250, 250);
        $this->SetTextColor(0,0,0);
        $this->SetFont('', '', 8);
        $this->writeHTMLCell(120, '', 45, $y=278, $html, 0, 0, 1, true, 'C', true);

        $this->Cell(0, 10, $this->getAliasNumPage(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }


}

?>
