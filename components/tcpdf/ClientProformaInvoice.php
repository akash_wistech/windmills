<?php

define('myPDF_MARGIN_TOP', 20);
define('myPDF_MARGIN_LEFT', 14);
define('myPDF_MARGIN_RIGHT', 14);
require_once(__DIR__ . '/../../vendor/tcpdf/tcpdf.php');

class ProformaInvoice extends TCPDF
{
        public function Header()
        {

                // get the current page break margin
                $bMargin = $this->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $this->AutoPageBreak;
                // disable auto-page-break
                $this->SetAutoPageBreak(false, 0);


                // $this->setFillColor(255,255,255);
                $logo = 'images/proforma-invoice-logo.png';
                $this->Image($logo, 10, 10, 45, 0, 'PNG', '', 'L', true, 150, '', false, false, 0, false, false, false);

                $centre = '
                Windmills Real Estate Valuation Services LLC
                ';

                // Set font
                $this->SetFont('helvetica', 'B', 10);
                $this->SetTextColor(0, 0, 0); // Set Text Color
                // $this->Cell(0, 50, 'Windmills Real Estate Valuation Services LLC ', 0, false, 'C', 0, '', 0, false, 'M', 'M');
                $this->writeHTMLCell(100, 5, 63, 10, $centre, 0, 0, 0, true, 'L', true);

                $centre2 = '604 Barsha Business Center, Al Barsha 1
                <br>Dubai, Dubai 0000 AE
                <br>+971 43460290
                <br>www.windmillsgroup.com
                <br>TRN 100498542800003
                ';
                // Set font
                $this->SetFont('helvetica', '', 10);
                $this->SetTextColor(0, 0, 0); // Set Text Color
                // $this->Cell(0, 50, 'Windmills Real Estate Valuation Services LLC ', 0, false, 'C', 0, '', 0, false, 'M', 'M');
                $this->writeHTMLCell(100, 5, 63, 15, $centre2, 0, 0, 0, true, 'L', true);


                $this->SetFont('helvetica', 'B', 16); // Set font
                $this->SetTextColor(144, 202, 249); // Set Text Color
                // Title
                //      $this->Cell(0, 50, 'Proforma Invoice', 0, false, 'R', 0, '', 0, false, 'M', 'M');
                $this->writeHTMLCell(60, 10, 140, 10, 'Proforma Invoice', 0, 0, 0, true, 'R', true);


                // restore auto-page-break status
                $this->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $this->setPageMark();
        }

        // Page footer
        public function Footer()
        {

                $data = '
                Bank Details
                <br>Windmills Real Estate Valuation Services L.L.C. 
                <br>Emirates NBD
                <br>Account No: 1015286631901
                <br>IBAN: AE66 0260 0010 1528 6631 901
                ';

                //      set color for text
                //      $this->setFillColor(250, 250, 250);
                $this->SetTextColor(0, 0, 0);
                $this->SetFont('', '', 10);
                $this->writeHTMLCell(120, '', 10, $y = 267, $data, 0, 0, 0, true, 'L', true);
        }
}
