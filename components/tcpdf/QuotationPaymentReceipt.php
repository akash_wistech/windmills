<?php

define ('myPDF_MARGIN_TOP', 20);
define ('myPDF_MARGIN_LEFT', 14);
define ('myPDF_MARGIN_RIGHT', 14);
require_once( __DIR__ . '/../../vendor/tcpdf/tcpdf.php');

class QuotationPaymentReceipt extends TCPDF
{
    public function Header() {

        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);


        // $this->setFillColor(255,255,255);
        $logo = 'images/windmils-footer.png';
        $this->Image($logo, 30, 15, 45, 0, 'PNG', '', 'L', true, 150, '', false, false, 0, false, false, false);

        $centre = 'Windmills Group';

        // Set font
        $this->SetFont('helvetica', 'B', 14);
        $this->SetTextColor(0, 0, 0);// Set Text Color
        // $this->Cell(0, 50, 'Windmills Real Estate Valuation Services LLC ', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        $this->writeHTMLCell(100, 5, 100, 10, $centre, 0, 0, 0, true, 'L', true);

        $centre2 = '
        <br>Dubai Dubai 38091
        <br>United Arab Emirates
        <br>TRN 100498542800003
        <br>0528197504
        <br>finance@windmillsgroup.com
        ';
        // Set font
        $this->SetFont('helvetica', '', 10);
        $this->SetTextColor(0, 0, 0);// Set Text Color
        // $this->Cell(0, 50, 'Windmills Real Estate Valuation Services LLC ', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        $this->writeHTMLCell(100, 5, 100, 17, $centre2, 0, 0, 0, true, 'L', true);


        $this->SetFont('helvetica', 'B', 16);// Set font
        $this->SetTextColor(144,202,249);// Set Text Color
        // Title
        //      $this->Cell(0, 50, 'Proforma Invoice', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        // $this->writeHTMLCell(60, 10, 140, 10, 'Proforma Invoice', 0, 0, 0, true, 'R', true);


        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();



}

// Page footer
public function Footer() {

//     $data = '
//     Bank Details
//     <br>Windmills Real Estate Valuation Services L.L.C. 
//     <br>Emirates NBD
//     <br>Account No: 1015286631901
//     <br>IBAN: AE66 0260 0010 1528 6631 901
//     ';

        $data = '<hr>';

//      set color for text
//      $this->setFillColor(250, 250, 250);
    $this->SetTextColor(0,0,0);
    $this->SetFont('', '', 10);
//     $this->writeHTMLCell(190, '', 10, $y=267, $data, 0, 0, 0, true, 'L', true);
    $this->writeHTMLCell(190, '', 10, $y=285, $data, 0, 0, 0, true, 'L', true);

}
}

?>
