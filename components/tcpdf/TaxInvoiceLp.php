<?php

define ('myPDF_MARGIN_TOP', 20);
define ('myPDF_MARGIN_LEFT', 14);
define ('myPDF_MARGIN_RIGHT', 14);
require_once( __DIR__ . '/../../vendor/tcpdf/tcpdf.php');

class TaxInvoice extends TCPDF
{
    public $ref_number;
    public $invoice_text;
    public $inv_type;

    public function Header() {

// get the current page break margin
        $bMargin = $this->getBreakMargin();
// get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
// disable auto-page-break
        $this->SetAutoPageBreak(false, 0);


// $this->setFillColor(255,255,255);
        $logo = 'images/proforma-invoice-logo.png';
        $this->Image($logo, 10, 12, 55, 0, 'PNG', '', 'L', true, 150, '', false, false, 0, false, false, false);

        $centre = '';

// Set font
        $this->SetFont('helvetica', 'B', 10);
$this->SetTextColor(0, 0, 0);// Set Text Color
// $this->Cell(0, 50, 'Windmills Real Estate Valuation Services LLC ', 0, false, 'C', 0, '', 0, false, 'M', 'M');
$this->writeHTMLCell(100, 5, 63, 10, $centre, 0, 0, 0, true, 'L', true);

$centre2 = '';
// Set font
$this->SetFont('helvetica', '', 10);
$this->SetTextColor(0, 0, 0);// Set Text Color
// $this->Cell(0, 50, 'Windmills Real Estate Valuation Services LLC ', 0, false, 'C', 0, '', 0, false, 'M', 'M');
$this->writeHTMLCell(100, 5, 63, 15, $centre2, 0, 0, 0, true, 'L', true);









$this->SetFont('helvetica', 'B', 14);// Set font
        $this->inv_type= 'Original';
        if($this->inv_type  == 'Original'){
            $this->SetFont('helvetica', 'B', 14);// Set font
            $this->SetTextColor(55,99,174);// Set Text Color
            $this->writeHTMLCell(60, 10, 140, 10, '<br/>'.$this->inv_type, 0, 0, 0, true, 'R', true);
        }
$this->SetTextColor(55,99,174);// Set Text Color
// Title
//      $this->Cell(0, 50, 'Proforma Invoice', 0, false, 'R', 0, '', 0, false, 'M', 'M');
$this->writeHTMLCell(60, 10, 140, 15, '<br/>'.$this->invoice_text, 0, 0, 0, true, 'R', true);
$this->writeHTMLCell(60, 10, 140, 20, '<br/>'."Final Payment", 0, 0, 0, true, 'R', true);




// restore auto-page-break status
$this->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$this->setPageMark();

/*
        $this->SetTextColor(68,114,196);
        $this->SetFont('', 'B', 10);
        $this->writeHTMLCell(60, 10, 140, 18, 'Invoice No: '.$this->ref_number, 0, 0, 0, true, 'R', true);*/

}

// Page footer
public function Footer() {

    $data = 'This is a computer application generated document and hence carries no signature. Please return and delete communication to the sender if this does not belong to you.';

//      set color for text
//      $this->setFillColor(250, 250, 250);
    $this->SetTextColor(166,166,166);
    $this->SetFont('', '', 10);
    $this->writeHTMLCell('', '', 10, $y=267, $data, 0, 0, 0, true, 'L', true);

}
}

?>
