<?php
define ('myPDF_MARGIN_TOP', 20);
define ('myPDF_MARGIN_LEFT', 10);
define ('myPDF_MARGIN_RIGHT', 10);

require_once( __DIR__ . '/../../vendor/tcpdf/tcpdf.php');




class MyPDFDraft extends TCPDF
{
    public $model;

    public $report_type;

    public function Header() {
        // Get the current page break margin
        $bMargin = $this->getBreakMargin();

        // Get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;

        // Disable auto-page-break
        $this->SetAutoPageBreak(false, 0);

        // Define the path to the image that you want to use as watermark.
        $img_file = 'images/draft.png';

        // Render the image
        $this->Image($img_file, 0, 0, 223, 280, '', '', '', false, 300, '', false, false, 0);

        // Restore the auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);

        // Set the starting point for the page content
        $this->setPageMark();
    }

}

?>
