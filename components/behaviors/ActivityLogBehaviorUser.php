<?php

namespace app\components\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use app\models\ActivityLog;

class ActivityLogBehaviorUser extends Behavior
{
  /**
  * @var string The message of current action
  */
  public $message = null;

  /**
  * @inheritdoc
  */
  public function events()
  {
    return [
      ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
      ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
      ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
    ];
  }

  public function beforeInsert($event)
  {
    ActivityLog::add(ActivityLog::LOG_STATUS_INFO, $this->message !== null ? $this->message : __METHOD__);
  }

  public function beforeUpdate($event)
  {
    ActivityLog::add(ActivityLog::LOG_STATUS_INFO, $this->message !== null ? $this->message : __METHOD__);
  }

  public function beforeDelete($event)
  {
    ActivityLog::add(ActivityLog::LOG_STATUS_INFO, $this->message !== null ? $this->message : __METHOD__);
  }
}
