<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MemberActivity;

class ActivityHelper extends Component
{
  public function log($type,$model,$parent_id=0)
  {
    $icon='';
    $descp='';
    $link='javascript:;';
    if($type=='sent-inquiry'){
      $icon='fa-envelope';
      $descp='Sent Inquiry to '.$model->mediaOutlet->company->title;
      $link=Url::to(['inquiries/view','id'=>$model->rssid]);
    }
    if($type=='replied-inquiry'){
      $icon='fa-envelope';
      $descp='Replied to Inquiry';
      $link=Url::to(['inquiries/view','id'=>$model->inquiry->rssid]);
    }
    $activity=new MemberActivity;
    $activity->user_id=Yii::$app->user->identity->id;
    $activity->type=$type;
    $activity->icon=$icon;
    $activity->descp=$descp;
    $activity->parent_id=$parent_id;
    $activity->ref_id=$model->id;
    $activity->link=$link;
    $activity->save();
  }

	/**
	 * return action log types array
   * @return array
	 */
  public function getActionLogTypesArr()
  {
    return [
      'comment'=>Yii::t('app','Comment'),
      'action'=>Yii::t('app','Action'),
      'call'=>Yii::t('app','Log Call'),
      'timer'=>Yii::t('app','Log Time'),
      'calendar'=>Yii::t('app','Calendar'),
    ];
  }

	/**
	 * return sm input class for action log form
   * @return string
	 */
  public function getActionLogSmInputClass()
  {
    return 'form-control form-control-sm';
  }

	/**
	 * return md input class for action log form
   * @return string
	 */
  public function getActionLogMdInputClass()
  {
    return 'form-control form-control-md';
  }

	/**
	 * return date sm input class for action log form
   * @return string
	 */
  public function getActionLogDtSmInputClass()
  {
    return 'form-control form-control-sm datetimepicker-input';
  }

	/**
	 * return date md input class for action log form
   * @return string
	 */
  public function getActionLogDtMdInputClass()
  {
    return 'form-control form-control-md datetimepicker-input';
  }

	/**
	 * return sm input group template for action log form
   * @return string
	 */
  public function getActionLogSmInputGroupTemplate()
  {
    return '
    <div class="input-group input-group-sm">
      <div class="input-group-prepend">
        <span class="input-group-text">{label}</span>
      </div>
      {input}
    </div>
    {error}{hint}
    ';
  }

	/**
	 * return md input group template for action log form
   * @return string
	 */
  public function getActionLogMdInputGroupTemplate()
  {
    return '
    <div class="input-group input-group-md">
      <div class="input-group-prepend">
        <span class="input-group-text">{label}</span>
      </div>
      {input}
    </div>
    {error}{hint}
    ';
  }
}
?>
