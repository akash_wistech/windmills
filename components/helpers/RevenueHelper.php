<?php
namespace app\components\helpers;
use Yii;
use yii\base\Component;
use app\models\Valuation;
use app\models\Company;
use app\models\Buildings;

class RevenueHelper extends Component {


	public function getParentQuery($time_period=null, $goal=null)
	{

		$query = Valuation::find();

		if($goal == "client_revenue"){
			if($time_period==1){
				$query->select([
					'client_revenue'=>'SUM('.Valuation::tableName().'.fee)',
					'month_number' => 'MONTH(' . Valuation::tableName() . '.submission_approver_date)'
				]);
			}
			if($time_period==2 || $time_period==3 || $time_period==4){
				$query->select([
					'client_revenue'=>'SUM('.Valuation::tableName().'.fee)',
				]);
			}
		}
		else{
			if($time_period==1){
				$query->select([
					'total_valuations' => 'COUNT(' . Valuation::tableName() . '.id)',
					'month_number' => 'MONTH(' . Valuation::tableName() . '.submission_approver_date)'
				]);
			}
			if($time_period==2 || $time_period==3 || $time_period==4){
				$query->select([
					'total_valuations'=>'COUNT('.Valuation::tableName().'.id)',
				]);
			}
		}
		
		$query->from(Valuation::tableName());
		$query->leftJoin(Company::tableName(), Valuation::tableName() . '.client_id=' . Company::tableName() . '.id');
		$query->where([Valuation::tableName() .'.valuation_status' => 5]);
		$query->andWhere([Company::tableName() . '.client_type' => 'bank']);
		return $query;
	}
	
	public function getClientsValByMonth($time_period=null, $year=null, $goal=null)
	{
		$revenueByMonth = array();
		$query = $this->getParentQuery($time_period, $goal);
		$query->andWhere('YEAR(valuation.submission_approver_date) = '.$year.'');
		$query->groupBy('MONTH(valuation.submission_approver_date)');
		$query = $query->all();

		foreach ($query as $row) {
			if($goal == 'client_revenue'){
				$revenueByMonth[$row->month_number] = $row->client_revenue;
			}else{
				$revenueByMonth[$row->month_number] = $row->total_valuations;
			}
		}
		
		return $revenueByMonth;
	}

	public function getClientsValByQuarter($time_period=null, $year=null, $goal=null)
	{
		$valByQuarter = array();

        for ($quarter=1; $quarter <=4 ; $quarter++) { 
            $qtr = $quarter."-quarter";
            $date_array = Yii::$app->appHelperFunctions->getThisQuarterNew($qtr, $year);
            $date['date_from'] = date('Y-m-d', $date_array['start_date']);
            $date['date_to']   = date('Y-m-d', $date_array['end_date']);

			// dd($date);

			$query = $this->getParentQuery($time_period, $goal);
			$query->andWhere('YEAR(valuation.submission_approver_date) = '.$year.'');
            $query->andFilterWhere([
                'between', 'submission_approver_date', $date['date_from'], $date['date_to']
            ]);
			$query = $query->one();

			if($goal == 'client_revenue'){
				if($query->client_revenue>0){
					$valByQuarter[$quarter] = $query->client_revenue;
				}else{
					$valByQuarter[$quarter] = 0;
				}
			}else{
				if($query->total_valuations>0){
					$valByQuarter[$quarter] = $query->total_valuations;
				}else{
					$valByQuarter[$quarter] = 0;
				}
			}


        }
		return $valByQuarter;
	}

	public function getClientsHalfYearVal($time_period=null, $year=null ,$goal=null)
	{
		$valByHalfYear = array();		
		for ($yealry=1; $yealry <=2 ; $yealry++) { 
            $qtr = $yealry."-Half";
            $date_array = Yii::$app->appHelperFunctions->getThisQuarterNew($qtr, $year);
            $date['date_from'] = date('Y-m-d', $date_array['start_date']);
            $date['date_to']   = date('Y-m-d', $date_array['end_date']);
			

			$query = $this->getParentQuery($time_period, $goal);
			$query->andWhere('YEAR(valuation.submission_approver_date) = '.$year.'');	
            $query->andFilterWhere([
                'between', 'submission_approver_date', $date['date_from'], $date['date_to']
            ]);
			$query = $query->one();

			if($goal == 'client_revenue'){
				if($query->client_revenue>0){
					$valByHalfYear[$yealry] = $query->client_revenue;
				}else{
					$valByHalfYear[$yealry] = 0;
				}
			}
			else{
				if($query->total_valuations>0){
					$valByHalfYear[$yealry] = $query->total_valuations;
				}else{
					$valByHalfYear[$yealry] = 0;
				}
			}

        }
		return $valByHalfYear;
	}

	public function getClientsYearlyVal($time_period=null, $year=null, $goal=null)
	{
		$valByYearly = array();
		foreach(Yii:: $app->appHelperFunctions->getYearArr() as $key=>$year_num){
			$query = $this->getParentQuery($time_period, $goal);
			$query->andWhere('YEAR(valuation.submission_approver_date) = '.$year_num.'');	
			$query = $query->one();

			if($goal == 'client_revenue'){
				if($query->client_revenue>0){
					$valByYearly[$year_num] = $query->client_revenue;
				}else{
					$valByYearly[$year_num] = 0;
				}
			}
			else{
				if($query->total_valuations>0){
					$valByYearly[$year_num] = $query->total_valuations;
				}else{
					$valByYearly[$year_num] = 0;
				}
			}
			

        }
		return $valByYearly;
	}











	public function getNoOfValuations($time_period=null, $year=null)
	{
		$query = Valuation::find()
			->select([
				'total_valuations' => 'COUNT(' . Valuation::tableName() . '.id)',
			])
			->from(Valuation::tableName())			
			->leftJoin(Company::tableName(), Valuation::tableName() . '.client_id=' . Company::tableName() . '.id')
			->where([Valuation::tableName() .'.valuation_status' => 5])
			->andWhere([Company::tableName() . '.client_type' => 'bank']);

			if($time_period == 1 || $time_period == 2 ||  $time_period == 3){
				$query->andWhere('YEAR(valuation.submission_approver_date) = '.$year.'');
			}
			if($time_period==4){
				$query->andWhere(['in', 'YEAR(valuation.submission_approver_date)', Yii:: $app->appHelperFunctions->getYearArr() ]);
			}
			$query = $query->one();
			return $query->total_valuations;
	}		





	public function getTotalRevenue($time_period=null, $year=null)
    {
        $query = Valuation::find()
        ->select([
            'client_revenue'=>'SUM('.Valuation::tableName().'.fee)',
        ])
        ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id');
        $query->where(['company.client_type' => 'bank']);
        $query->andWhere(['valuation_status' => 5]);

		if ($time_period == 1 || $time_period == 2 || $time_period == 3){
            $query->andWhere('YEAR(valuation.submission_approver_date) = '.$year.'');
        }

		if($time_period == 4){
            $query->andWhere(['in', 'YEAR(valuation.submission_approver_date)', Yii:: $app->appHelperFunctions->getYearArr() ]);
        }


        $query = $query->one();
        return $query->client_revenue;
    }

	
    public function getClientValuations($client_id=null, $month=null, $year=null, $time_period=null, $date=null, $goal=null)
    {
        $query = Valuation::find();

		if($goal == 'client_revenue'){
			$query->select([
				Valuation::tableName().'.id',
				'client_revenue'=>'SUM('.Valuation::tableName().'.fee)',
			]);
		}
		else{
			$query->select([
				Valuation::tableName().'.id',
				'total_valuations'=>'Count('.Valuation::tableName().'.id)',
			]);
		}

        $query->where(['valuation_status' => 5]);
        $query->andWhere(['client_id' => $client_id]);

        if($time_period==1){
            $query->andWhere('MONTH(valuation.submission_approver_date) = '.$month.'');
        }

        if($time_period==1 || $time_period==4 || $time_period==2){
            $query->andWhere('YEAR(valuation.submission_approver_date) = '.$year.'');
        }

        if($time_period == 2 || $time_period == 3){
            $query->andFilterWhere([
                'between', 'valuation.submission_approver_date', $date['date_from'], $date['date_to']
            ]);
        }

        $query = $query->one();
        return $query;
    }




    public function getFooterValues($val=null, $total=null, $totalClients=null)
    {
        // dd($total);
        // $html = '<br>'.number_format($val);
        $html = '<br>'.Yii::$app->appHelperFunctions->wmFormate($val);

        if($totalClients>0){
            $html.= '<br>'. number_format( ($val / $totalClients), 2);
        }else{
            $html .= '<br>'. 0.00;
        }

        if($total>0){
            $html.= '<br>'. number_format( ($val / $total) * 100, 2);
        }else{
            $html .= '<br>'. 0.00;
        }
        return $html;
    }

    public function getLastFooterValue($total=null, $totalClients=null)
    {
        $html = '<br>'.number_format($total);
        $html.= '<br>'. number_format( ($total / $totalClients), 2);
        $html .= '<br>'. 100;

        return $html;
    }

	public function getClientRevenueBank($client_id=null, $month=null, $year=null, $time_period=null)
    {
        $query = Valuation::find()
        ->select([
            Valuation::tableName().'.id',
            'client_revenue'=>'SUM('.Valuation::tableName().'.fee)',
        ]);
        if($time_period==4){
            $query->where(['in', 'YEAR(valuation.submission_approver_date)', Yii:: $app->appHelperFunctions->getYearArr() ]);
        }
        if($time_period == 1 || $time_period == 2 || $time_period == 3){
            $query->where('YEAR(valuation.submission_approver_date) = '.$year.'');
        }
        $query->andWhere(['valuation_status' => 5]);
        $query->andWhere(['client_id' => $client_id]);
		
        $query = $query->one();
        return $query;
    }


	
    public function getReportPeriodRev()
    {
        return [
            '1' => Yii::t('app','Monthly'),
            '2' => Yii::t('app','Quarterly'),
            '3' => Yii::t('app','Half Yearly'),
            '4' => Yii::t('app','Yearly'),
        ];
    }


	
    public function getClientValuationsCount($client_id=null, $month=null, $year=null, $time_period=null)
    {
        $query = Valuation::find()
        ->select([
            Valuation::tableName().'.id',
            'total_valuations'=>'Count('.Valuation::tableName().'.id)',
            // 'client_revenue'=>'SUM('.Valuation::tableName().'.fee)',
        ]);

        if($time_period == 1 || $time_period == 2 || $time_period == 3){
            $query->where('YEAR(valuation.submission_approver_date) = '.$year.'');
        }

        if($time_period == 4){
            $query->where(['in', 'YEAR(valuation.submission_approver_date)', Yii:: $app->appHelperFunctions->getYearArr() ]);
        }

        $query->andWhere(['valuation_status' => 5]);
        $query->andWhere(['client_id' => $client_id]);     
        $query = $query->one();
        return $query;
    }



	public function getValuations($goal=null, $city=null, $model=null, $month=null, $year=null)
	{
		$query = Valuation::find()
		->leftJoin(Buildings::tableName(), Buildings::tableName() . '.id =' . Valuation::tableName() . '.building_info')
        ->where(['client_id' => $model->id])
        ->andWhere('MONTH(instruction_date) = '.$month.'')
        ->andWhere('YEAR(instruction_date) = '.$year.'');

		if(is_array($city)){
			$query->andWhere(['in', Buildings::tableName().'.city', $city ]);
		}else{
			$query->andWhere([Buildings::tableName().'.city' => $city]);
		}   

		if($goal == 'valuations'){
			$query = $query->all();
		}else if($goal == 'fee'){
			$query = $query->sum(Valuation::tableName().'.fee');
		}

		return $query;
	}



	public function getTblBody($valuation=null)
	{
		$tblBody = '
		<tr>
			<td class="tbl-body" style="font-size:9px;text-align:left;">'.$valuation->instruction_date .'</td>
			<td class="tbl-body" style="font-size:9px;width: 100px">'.$valuation->reference_number .'</td>
			<td class="tbl-body" style=" font-size:9px; text-align: center;">'.$valuation->client_reference .'</td>
			<td class="tbl-body" style=" font-size:9px; text-align: center;width: 95px">'.$valuation->client_name_passport .'</td>
			<td class="tbl-body" style="font-size:9px; text-align: center;">'.$valuation->building->title .'</td>
			<td class="tbl-body" style="font-size:9px; text-align:right;">'.$valuation->building->subCommunities->title .'</td>
			<td class="tbl-body" style="font-size:9px; text-align:right;">'.Yii::$app->appHelperFunctions->emiratedListArr[$valuation->building->city] .'</td>
			<td class="tbl-body" style="font-size:9px; text-align:right;">'.number_format($valuation->fee,2) .'</td>
	  	</tr>
		';
		return $tblBody;
	}

	public function getFeeTable($grossFee=null, $discount=null, $vat=null, $fianlFeePayable=null, $finalFeePayableToWords)
	{
		$tbl = '
		<table class="border airal-family" cellspacing="1" cellpadding="5" width="547">
			<tbody>
				<tr style="background-color: ;">
					<td  style="font-size:9px;text-align:left;font-weight:bold">Gross Fee</td>
					<td  style="font-size:9px;font-weight:bold">AED</td>
					<td  style=" font-size:9px; text-align: center;font-weight:bold">'.number_format($grossFee ,2). '</td>
					<td colspan="5"  style=" font-size:9px; text-align: center;"></td>
				</tr>
				<tr style="background-color: ;">
					<td  style="font-size:9px;text-align:left;font-weight:bold">Discount</td>
					<td  style="font-size:9px;font-weight:bold">AED</td>
					<td  style=" font-size:9px; text-align: center;font-weight:bold">' .number_format($discount,2).'</td>
					<td colspan="5"  style=" font-size:9px; text-align: center;font-weight:bold"></td>
				</tr>
				<tr style="background-color: ;">
					<td  style="font-size:9px;text-align:left;font-weight:bold">Fee</td>
					<td  style="font-size:9px;font-weight:bold">AED</td>
					<td  style=" font-size:9px; text-align: center;font-weight:bold">'.number_format($grossFee ,2). '</td>
					<td colspan="5"  style=" font-size:9px; text-align: center;"></td>
				</tr>
				<tr style="background-color: ;">
					<td  style="font-size:9px;text-align:left;font-weight:bold">VAT (5%)</td>
					<td  style="font-size:9px;font-weight:bold">AED</td>
					<td  style=" font-size:9px; text-align: center;font-weight:bold">' .number_format($vat,2). '</td>
					<td colspan="5"  style=" font-size:9px; text-align: center;"></td>
				</tr>
				<tr style="background-color: ;">
					<td  style="font-size:9px;text-align:left;font-weight:bold">Net Fee</td>
					<td  style="font-size:9px;font-weight:bold">AED</td>
					<td  style=" font-size:9px; text-align: center;font-weight:bold">' .number_format($fianlFeePayable ,2). '</td>
					<td colspan="5"  style=" font-size:9px; text-align: left;font-weight:bold">' .$finalFeePayableToWords.' only.</td>
				</tr>
			</tbody>
		</table>	
		';
        return $tbl;
    }

}
?>