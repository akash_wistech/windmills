<?php
namespace app\components\helpers;

use Yii;
use yii\helpers\Url;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use DateTime;
use app\models\Company;
use DateTimeZone;

class HelperFunctions extends Component
{
	/**
	 * encrypts string with a key
   * @return string
	 */
	public function encryptData($data,$key)
	{
		return utf8_encode(Yii::$app->getSecurity()->encryptByKey($data,$key));
	}

	/**
	 * dencrypts string with a key
   * @return string
	 */
	public function deryptData($data,$key)
	{
		return Yii::$app->getSecurity()->decryptByKey(utf8_decode($data),$key);
	}

	/**
	 * returns number of days between two dates
   * @return integer
	 */
	public function getNumberOfDaysBetween($s,$e){
		$start = strtotime($s);
		$end = strtotime($e);

		$days_between = ceil(abs($end - $start) / 86400);
		return $days_between;
	}

	/**
	 * converts a given date time to ago info string respect to current date time
   * @return string
	 */
	public function convertTime($time_ago2)
	{
		$time_ago = strtotime($time_ago2);
		$cur_time   = time();
		$time_elapsed   = $cur_time - $time_ago;
		$seconds    = $time_elapsed ;
		$minutes    = round($time_elapsed / 60 );
		$hours      = round($time_elapsed / 3600);
		$days       = round($time_elapsed / 86400 );
		$weeks      = round($time_elapsed / 604800);
		$months     = round($time_elapsed / 2600640 );
		$years      = round($time_elapsed / 31207680 );

		if($seconds <= 60){// Seconds
			return Yii::t('app', 'just now');
		}else if($minutes <=60){//Minutes
			if($minutes==1){
				return Yii::t('app', 'one minute ago');
			}else{
				return Yii::t('app', '{mn} minutes',['mn'=>$minutes]).' '.Yii::t('app', 'ago');
			}
		}else if($hours <=24){//Hours
			if($hours==1){
				return Yii::t('app', 'an hour ago');
			}else{
				return Yii::t('app', '{hr} hrs ago',['hr'=>$hours]);
			}
		}else{
			return Yii::$app->formatter->asdatetime($time_ago2);
		}
	}

	public function getDateTimeWithDiff($date)
	{
    $serverDif=Yii::$app->appHelperFunctions->getSetting('cron_server_diff');
		return date("Y-m-d H:i:s",strtotime($serverDif." Minute",strtotime($date)));
	}

	/**
	 * returns a date before given date
   * @return datetime
	 */
	public function getDayBefore($date)
	{
		return date ("Y-m-d", strtotime("-1 day", strtotime($date)));
	}

	/**
	 * returns a date after given date
   * @return datetime
	 */
	public function getNextDay($date)
	{
		return date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	}

	/**
	 * returns an array of start and end date for the difference
   * @return array
	 */
	public function getStartEndDateTime($date,$diff)
	{
		return ['start'=>date("Y-m-d H:i:s", strtotime($diff, strtotime($date))),'end'=>$date];
	}

	/**
	 * returns an array of start and end date for the difference from now
   * @return array
	 */
	public function getStartEndDateTimeFromNow($diff)
	{
		$date=date("Y-m-d H:i:s");
		return ['start'=>date("Y-m-d H:i:s", strtotime($diff, strtotime($date))),'end'=>$date];
	}

	// Parses a string into a DateTime object, optionally forced into the given timeZone.
	function parseDateTime($string, $timeZone=null) {
	  $date = new DateTime(
	    $string,
	    $timeZone ? $timeZone : new DateTimeZone('UTC')
	      // Used only when the string is ambiguous.
	      // Ignored if string has a timeZone offset in it.
	  );
	  if ($timeZone) {
	    // If our timeZone was ignored above, force it.
	    $date->setTimezone($timeZone);
	  }
	  return $date;
	}


	// Takes the year/month/date values of the given DateTime and converts them to a new DateTime,
	// but in UTC.
	function stripTime($datetime) {
	  return $datetime->format('Y-m-d');
	}

	/**
	 * returns array of user types
   * @return array
	 */
	public function getUserTypes()
	{
		return [
			'0' => Yii::t('app','Contact / User'),
			'10' => Yii::t('app','Admin'),
			'20' => Yii::t('app','Super Admin'),
		];
	}

	/**
	 * returns array of Yes/No
   * @return array
	 */
	public function getArrYesNo()
	{
		return [
			'1' => Yii::t('app','Yes'),
			'0' => Yii::t('app','No'),
		];
	}

	/**
	 * returns array of Yes/No for badge icon html
   * @return array
	 */
	public function getArrYesNoIcon()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i></span>',
			'0' => '<span class="badge grid-badge badge-danger"><i class="fa fa-times"></i></span>',
		];
	}

	/**
	 * returns array of Yes/No for badge html
   * @return array
	 */
	public function getArrYesNoSpan()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Yes').'</span>',
			'0' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','No').'</span>',
		];
	}

	/**
	 * returns array of Status types
   * @return array
	 */
	public function getArrPublishing()
	{
		return [
			'1' => Yii::t('app','Publish'),
			'2' => Yii::t('app','Disable'),
			'0' => Yii::t('app','Draft'),
		];
	}

	/**
	 * returns array of status types html
   * @return array
	 */
	public function getArrPublishingIcon()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Published').'</span>',
			'2' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Disabled').'</span>',
			'0' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
		];
	}

	/**
	 * returns array of status types for form Dropdowns
   * @return array
	 */
	public function getArrFormStatus()
	{
		return [
			'1' => Yii::t('app','Active'),
			'2' => Yii::t('app','Disable'),
			//'0' => Yii::t('app','Draft'),
		];
	}

	/**
	 * returns array of status types for filter Dropdowns
   * @return array
	 */
	public function getArrFilterStatus()
	{
		return [
			'1' => Yii::t('app','Active'),
			'2' => Yii::t('app','Disabled'),
			//'0' => Yii::t('app','Draft'),
		];
	}

	/**
	 * returns array of status types with badge html
   * @return array
	 */
	public function getArrStatusIcon()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Active').'</span>',
			'2' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Disabled').'</span>',
			//'0' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
		];
	}

	/**
	 * returns array of email status types
   * @return array
	 */
	public function getEmailPublishing()
	{
		return [
			'1' => Yii::t('app','Sent'),
			'0' => Yii::t('app','In Queue'),
			'2' => Yii::t('app','Draft'),
			'-1' => Yii::t('app','Canceled'),
		];
	}

	/**
	 * returns array of email status types with badge html
   * @return array
	 */
	public function getEmailPublishingIcon()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Sent').'</span>',
			'0' => '<span class="badge grid-badge badge-primary"><i class="fa fa-hourglass"></i> '.Yii::t('app','In Queue').'</span>',
			'2' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
			'-1' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Cancelled').'</span>',
		];
	}

  /**
  * returns listing types for permission group
  */
	public function getPermissionListingType()
	{
		return [
		  '1'=>Yii::t('app','All Listing'),
		  '2'=>Yii::t('app','Own Listing'),
		];
	}

	/**
	 * returns array of week days
   * @return array
	 */
	public function getWeekDayNames()
	{
		return [
			'0' => 'Sunday',
			'1' => 'Monday',
			'2' => 'Tuesday',
			'3' => 'Wednesday',
			'4' => 'Thursday',
			'5' => 'Friday',
			'6' => 'Saturday',
		];
	}

	/**
	 * returns if given date is a weekend
   * @return boolean
	 */
	public function isWeekend($date)
	{
		$weekDay = date('w', strtotime($date));
		return ($weekDay == 5 || $weekDay == 6);
	}

	/**
	 * returns day of week
   * @return integer
	 */
	public function WeekendDay($date)
	{
		$weekDay = date('w', strtotime($date));
		return $weekDay;
	}

	/**
	 * returns string with currency sign prefixed
   * @return string
	 */
	public function withCurrencySign($amount)
	{
		return Yii::$app->appHelperFunctions->getSetting('currency_sign').' '.Yii::$app->formatter->asDecimal($amount);
	}

	/**
	 * returns array of visibility list
   * @return array
	 */
	public function getVisibilityListArr()
	{
		return [
			'1'=>Yii::t('app','Public'),
			'2'=>Yii::t('app','Private'),
		];
	}

	/**
	 * returns array of priority list
   * @return array
	 */
	public function getPriorityListArr()
	{
		return [
			'1'=>Yii::t('app','Low'),
			'2'=>Yii::t('app','Medium'),
			'3'=>Yii::t('app','High'),
		];
	}

	/**
	 * returns array of priority list
   * @return array
	 */
	public function getActionLogActionRemindToListArr()
	{
		return [
			'me'=>Yii::t('app','Me'),
			'assigned'=>Yii::t('app','Assigned Users'),
			'both'=>Yii::t('app','Me and Assigned Users'),
		];
	}

	/**
	 * returns array of priority list
   * @return array
	 */
	public function getActionLogActionRemindTimeListArr()
	{
		return [
			'1'=>Yii::t('app','1 minute'),
			'5'=>Yii::t('app','5 minutes'),
			'10'=>Yii::t('app','10 minutes'),
			'15'=>Yii::t('app','15 minutes'),
			'30'=>Yii::t('app','30 minutes'),
			'60'=>Yii::t('app','1 hour'),
			'1440'=>Yii::t('app','1 day'),
			'10080'=>Yii::t('app','1 week'),
		];
	}

	/**
	 * returns array of Log Call Quick Note action list
   * @return array
	 */
	public function getActionLogCallQuickNoteAction()
	{
		return [
			'1'=>Yii::t('app','Contacted'),
			'2'=>Yii::t('app','Not Contacted'),
		];
	}

	/**
	 * returns array of Log Call Quick Note action response list
   * @return array
	 */
	public function getActionLogCallQuickNoteResponse()
	{
		return [
			'1'=>[
				'1'=>Yii::t('app','Not interested.'),
				'2'=>Yii::t('app','Requested follow up call.'),
				'3'=>Yii::t('app','Contact made.'),
			],
			'2'=>[
				'1'=>Yii::t('app','No answer.'),
				'2'=>Yii::t('app','Wrong number.'),
				'3'=>Yii::t('app','Left voicemail.'),
			],
		];
	}

	/**
	 * returns array of Calendar Colors
   * @return array
	 */
	public function getCalendarColorListArr()
	{
		return [
			'#6389de'=>Yii::t('app','Blue'),
			'#a9c1fd'=>Yii::t('app','Light Blue'),
			'#5de1e5'=>Yii::t('app','Turquoise'),
			'#82e7c2'=>Yii::t('app','Light Green'),
			'#6bc664'=>Yii::t('app','Green'),
			'#fddb68'=>Yii::t('app','Yellow'),
			'#ffbc80'=>Yii::t('app','Orange'),
			'#ff978c'=>Yii::t('app','Pink'),
			'#e74046'=>Yii::t('app','Red'),
			'#d9adfb'=>Yii::t('app','Purple'),
			'#dedddd'=>Yii::t('app','Gray'),
		];
	}

	/**
	 * returns array of Event Sub Types
   * @return array
	 */
	public function getEventSubTypesListArr()
	{
		return [
			'1'=>Yii::t('app','Meeting'),
			'2'=>Yii::t('app','Appointment'),
			'3'=>Yii::t('app','Call'),
		];
	}

	/**
	 * returns array of Event Status
   * @return array
	 */
	public function getEventStatusListArr()
	{
		return [
			'1'=>Yii::t('app','Confirmed'),
			'2'=>Yii::t('app','Cancelled'),
		];
	}

	/**
	 * returns array of property type for fee structure
   * @return array
	 */
	public function getFeeStructureTypeListArr()
	{
		return [
			'1'=>Yii::t('app','Free Hold'),
			'2'=>Yii::t('app','Non Free Hold'),
		];
	}
    public function getConfigrationSectionListArr(){

        return [
            'config_bedrooms'=>'no_of_bedrooms',
            'config_bathrooms'=>'no_of_bathrooms',
            'config_kitchen'=>'no_of_kitchen',
            'config_living_area'=>'no_of_living_area',
            'config_dining_area'=>'no_of_dining_area',
            'config_maid_rooms'=>'no_of_maid_rooms',
            'config_laundry_area'=>'no_of_laundry_area',
            'config_store'=>'no_of_store',
            'config_service_block'=>'no_of_service_block',
            'config_garage'=>'no_of_garage',
            'config_balcony'=>'no_of_balcony',
            'config_family_room'=>'no_of_family_room',
            'config_powder_room'=>'no_of_powder_room',
            'config_study_room'=>'no_of_study_room',
            'config_flooring'=>'',
            'config_ceiling'=>'',
            'config_view'=>'',
            'config_general_elevation'=>'',
            'config_unit_tag'=>'',
            'config_electricity_board'=>'',


        ];
    }
    public function getValuationStatusListArrLabel()
    {
        return [
            '1' => '<span class="badge grid-badge badge-info"> '.Yii::t('app','Valuation Received').'</span>',
            '2' => '<span class="badge grid-badge badge-primary"> '.Yii::t('app','Inspection Requested').'</span>',
            '3' => '<span class="badge grid-badge badge-warning"></i> '.Yii::t('app','Property Inspected').'</span>',
            '4' => '<span class="badge grid-badge badge-dark">'.Yii::t('app','Valuation Reviewed').'</span>',
            '5' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Approved').'</span>',
            '6' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Rejected').'</span>',
            '7' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Documents Requested').'</span>',
            '8' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Valuation Prepared').'</span>',
            '9' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Cancelled').'</span>',
            '10' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Rejected by Viewer').'</span>',
            '11' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Rejected by Reviewer').'</span>',
            '12' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Rejected by Approver').'</span>',


        ];
    }
    public function getValuationStatusListArr()
    {
        return [
            '1'=>Yii::t('app','Valuation Received'),
            '2'=>Yii::t('app','Inspection Requested'),
            '3'=>Yii::t('app','Property Inspected'),
            '4'=>Yii::t('app','Valuation Reviewed'),
            '5'=>Yii::t('app','Approved'),
            '6' => Yii::t('app','Rejected'),
            '7' => Yii::t('app','Documents Requested'),
            '8' => Yii::t('app','Valuation Prepared'),
        ];
    }
    public function getValuationStatusListArr2()
    {
        return [
            '1' => Yii::t('app','Valuation Received'),
            '3' => Yii::t('app','Inspection Requested'),
            '4' => Yii::t('app','Valuation Reviewed'),
            '5' => Yii::t('app','Approved'),
            '6' => Yii::t('app','Rejected'),

        ];
    }
    public function getValuationStatusListArr3()
    {
        return [
            '1' => Yii::t('app','Valuation Received'),
            '4' => Yii::t('app','Valuation Reviewed'),
            '5' => Yii::t('app','Approved'),
            '6' => Yii::t('app','Rejected'),

        ];
    }
    public function getDateArray()
    {
        return [
            2019 => [
                "2019-01-01", "2019-12-30"
            ],
            2020 => [
                "2020-01-01", "2020-12-30"
            ],
            2021 => [
                "2021-01-01", "2021-12-30"
            ],


        ];
    }
    public function getTransactionData()
    {
        $transact_data= \app\models\AttributesData::find()->select(['value'])->
        where(['attribute_name'=>'transaction_limit'])->one();
        return	intval($transact_data['value']);
    }


	/**
	 * returns general delete confirmation msg
   * @return string
	 */
	public function getDelConfirmationMsg()
	{
		return Yii::t('app','Are you sure you want to delete this?');
	}

	public function getOpportunitySourceListArr()
	{
		return [
			'1'=>Yii::t('app','Web'),
			'2'=>Yii::t('app','Google'),
			'3'=>Yii::t('app','Facebook'),
			'4'=>Yii::t('app','Friend'),
		];
	}

    public function getdonwloadpdf($id)
    {


        $model=Company::findOne($id);

        // Include the main TCPDF library (search for installation path).
        require_once( __DIR__ .'/../tcpdf/MyPDF.php');
        // create new PDF document
        $pdf = new \MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //  $pdf->model = $model;
        $pdf->report_type=2;
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle('Client Report');
        $pdf->SetSubject('Valuation Report');
        $pdf->SetKeywords('');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(8, 20, 8);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);
        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);
        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('arialn', '', 14, '', true);
        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');
        $bodydata= \app\models\SpecialAssumptionreport::findOne(1);
        $valuation_data= \app\models\Valuation::find()->where(['client_id'=>$id])->andWhere(['!=','valuation_status', 5])->andWhere(['!=','valuation_status', 6])->andWhere(['!=','valuation_status', 9])->asArray()->all();
        $html='
	    <style>
	    .bold{ font-weight:bold;  }
	    table.main-class{
	      border-top: 1px solid gray;
	      border-bottom: 1px solid gray;
	      border-left: 1px solid gray;
	      border-right: 1px solid gray;
	      font-size:10px;
	      text-align:left;
	      font-weight:bold;
	    }
	  tr:nth-child(even) {
	    background-color: #dddddd;
	  }
	  td.table_of_content{
	     color:#0D47A1;
	     font-size:11px;
	     font-weight:bold;
	     border-bottom: 1px solid #BBDEFB;
	     text-align:center;
	  }
	    </style>
	    <p>To,</p>
	    <p>'.$model->title.'</p>
	    <h3><u>Subject: Periodical Valuation Status Report as of '.date('l, jS \of F, Y').'</u></h3><br>
	    <p>'.$bodydata['status_report'].'</p><br>
	    <table class="main-class" cellspacing="2" cellpadding="5" style="border: 1px dashed #64B5F6;">
	    <tr style="background-color:#ECEFF1; text-align:center">
	    <td class="table_of_content"><br><b>NO</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Instruction Date</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Inspection Date</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Client Reference</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Property Category</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Property Type</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Building Info</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Sub Community</b></td>
			<td class="table_of_content" colspan="2"><br><b>Status</b></td>
	    </tr>';

        $i=1; $j=1;
        foreach($valuation_data as $value) {


            $inspectionDate = \app\models\ScheduleInspection::find()->where(['valuation_id' => $value['id']])->one();
            $propetyType = \app\models\Properties::find()->where(['id' => $value['property_id']])->one();
            $building_info = \app\models\Buildings::find()->where(['id' => $value['building_info']])->one();
            $subCommunity = \app\models\SubCommunities::find()->where(['id' => $building_info['sub_community']])->one();
            $instruction_date = ($value['instruction_date'] <> null)?  date('d-m-Y',strtotime($value['instruction_date'])):'Not yet scheduled';
            $inspection_date = ($inspectionDate['inspection_date'] <> null)?  date('d-m-Y',strtotime($inspectionDate['inspection_date'])):'Not yet scheduled';
            $target_date = ($value['target_date'] <> null)?  date('d-m-Y',strtotime($value['target_date'])):'Not yet scheduled';
            if ($j==1) { $j++;  $val='';  }
			elseif($j==2) {  $j=1;  $val='style="background-color:#ECEFF1; "';  }
            $html .= '
	                <tr '.$val.'>
	                <td>' . $i++ . '</td>
	                <td colspan="2">' . $instruction_date. '</td>
	                <td colspan="2">' . $inspection_date . '</td>
	                <td colspan="2">' . $value['client_reference'] . '</td>
	                <td colspan="2">' . Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$value['property_category']] . '</td>
	                <td colspan="2">' . $propetyType['title'] . '</td>
	                <td colspan="2">' . $building_info['title'] . '</td>
	                <td colspan="2">' . $subCommunity['title'] . '</td>
									<td colspan="2">' . \Yii::$app->helperFunctions->valuationStatusListArr[$value['valuation_status']] . '</td>
	                </tr>';
        }
        $html.='</table>';
        if($i == 1){
            $html='';
        }

        if ( $value['client_id']!=null) {


            $pdfFile = $value['client_id'].'.pdf';
            $fullPath = realpath(dirname(__FILE__).'/../../uploads/client_pdf').'/'.$pdfFile;
            $pdf->writeHTML($html, true, false, false, false, '');
            //$pdf->Output($fullPath, 'F');
            $pdf->Output($fullPath, 'F');
            return [$fullPath];
        }
        else {
            return '';
        }



    }
    public function getModelStepSubmit_20_09_2021($model,$valuation)
    {

        // print_r("working");
        // die();


        if ($model->status=='Approve') {
            $Status='Recommended For Approval';
        }else {
            $Status='Reject';
            $reason=$model->reason;
            $reasonlabel='Reason for Rejection: ';
        }
        $notifyData = [
            'client' => $valuation->client,
            'uid' => $valuation->id,
            'attachments' => [],
           // 'subject' => $valuation->email_subject,
            'replacements'=>[
                '{clientName}'=>   $valuation->client->title,
                '{estimatedMarkerValue}'=> $model->estimated_market_value,
                '{estimatedMarkerValueSqf}'=>   $model->estimated_market_value_sqf,
                '{estimatedMarkertRent}'=>   $model->estimated_market_rent,
                '{estimatedMarkertRentSqf}'=>   $model->estimated_market_rent_sqf,
                '{parkingMarketValue}'=>   $model->parking_market_value,
                '{parkingMarketValueSqf}'=>   $model->parking_market_value_sqf,
                '{status}'=>   $Status,
                '{reason}'=>   $reason,
                '{reasonlabel}'=> $reasonlabel,
                '{value}'=>   (isset($model->created_by) && ($model->created_by <> null))? ($model->user->firstname): Yii::$app->user->identity->firstname,
                '{date}'=>   (date('Y-m-d',strtotime($model->created_at))),
            ],
        ];

        $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval'];
        // echo $valuation_steps[$step];
        // die();
     //   \app\modules\wisnotify\listners\NotifyEvent::fire('Valuation.'.$valuation_steps[$model->step].'.send', $notifyData);
        // $model->email_status = 1;

        if ($model->step==21) {
            if ($model->status=='Approve') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 8], 'id='.$valuation->id.'')->execute();
            }
			elseif ($model->status=='Reject') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 10], 'id='.$valuation->id.'')->execute();
            }
            // UPDATE (table name, column values, condition)
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .'
                      	AND approver_type="valuer"')->execute();
        }

        if ($model->step==22) {
            if ($model->status=='Approve') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 4], 'id='.$valuation->id.'')
                    ->execute();

            }elseif ($model->status=='Reject') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 11], 'id='.$valuation->id.'')
                    ->execute();
            }
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="reviewer"')->execute();
        }
		elseif ($model->step==23) {
            if ($model->status=='Approve') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 5], 'id='.$valuation->id.'')->execute();
            }elseif ($model->status=='Reject') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 12], 'id='.$valuation->id.'')->execute();
            }
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="approver"')->execute();
        }
    }
    public function getModelStepSubmit($model,$valuation)
    {

        if ($model->status=='Approve') {
            $Status='Recommended For Approval';
        }else {
            $Status='Reject';
            $reason=$model->reason;
            $reasonlabel='Reason for Rejection: ';
        }
        //  print_r("working1");
        //  print_r("working1");
        // die();
        // $val =file_get_contents();

        if ($model->approver_type=='approver') {


            $curl_handle=curl_init();
            curl_setopt($curl_handle, CURLOPT_URL,Url::toRoute(['valuation/sendpdfreport', 'id' => $valuation->id]));
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            $ip = curl_getinfo($curl_handle,CURLINFO_PRIMARY_IP);
            curl_close($curl_handle);
            $attachments[]=$val;
        }
        else {
            $attachments=[];
        }
       // $attachments=[];
        // print_r( $query);
        // die();
        if ($valuation->signature_img<>null) {
            // $signature_report_link = Yii::$app->get('s3bucket')->getUrl('signature/images/'.$valuation->signature_img);
        }


        $notifyData = [
            'client' => $valuation->client,
            'uid' => $valuation->id,
            'attachments' => $attachments,
            'subject' => $valuation->email_subject,
            //'valuer' => $valuation->approver->email,
            'valuer' => '',
            'replacements'=>[
                '{clientName}'=>   $valuation->client->title,
            ],
        ];


        $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval'];

        // echo $valuation_steps[$step];
        // die();
        if ($model->approver_type=='approver') {
            $allow_properties = [1, 2, 5, 6, 12, 37, 20];
            if (in_array($valuation->property_id, $allow_properties)) {
                if ($valuation->client->id != 1) {
                    \app\modules\wisnotify\listners\NotifyEvent::fire1('Valuation.approval.send', $notifyData);
                }
            }
        }
        // $model->email_status = 1;
        if ($model->step==21) {
            // UPDATE (table name, column values, condition)
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .'
                      	AND approver_type="valuer"')->execute();
        }
        if ($model->step==22) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="reviewer"')->execute();
        }
		elseif ($model->step==23) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="approver"')->execute();
        }
    }
		/*
		* Get Reverse color to background
		*/
		public function getReverseColor($colorCode)
    {
      $d = 0;
      list($red, $green, $blue) = sscanf($colorCode, "#%02x%02x%02x");
      $l = ( 0.299 * $red + 0.587 * $green + 0.114 * $blue)/255;
      if($l > 0.5)$d=0;
      else $d=255;
      return 'rgb('.$d.','.$d.','.$d.')';
    }

    public function getArrStatusIconList()
    {
        return [
            '1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Verified').'</span>',
            '2' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Unverified').'</span>',
            //'0' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
        ];
    }

    public function getArrFilterStatusList()
    {
        return [
            '1' => Yii::t('app','Verified'),
            '2' => Yii::t('app','Unverified'),
            //'0' => Yii::t('app','Draft'),
        ];
    }

    public function resize($filename, $width, $height)
    {
        $name_array = explode('/',$filename);
        if(!empty($name_array)){
            $new_image ='cache/' .end($name_array);
        }else{
            $new_image ='cache/' .rand(10,100000);
        }
        //$filename = 'http://valplibrary.files.wordpress.com/2009/01/5b585d_merry-christmas-blue-style.jpg';
        $percent = 0.3; // percentage of resize

// Content type
        // header('Content-type: image/jpeg');

// Get new dimensions
        list($width, $height) = getimagesize($filename);
        $new_width = $width * $percent;
        $new_height = $height * $percent;

// Resample
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefromjpeg($filename);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

// Output
        $image =imagejpeg($image_p, $new_image, 50);


        return $new_image;
    }
}
?>
