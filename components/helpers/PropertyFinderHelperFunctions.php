<?php
namespace app\components\helpers;
use Yii;
use yii\base\Component;
use app\models\Communities;
use app\models\SubCommunities;
use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Exception;


use app\models\PropertyFinderUrls;
use app\models\PropertyFinderFetchUrl;
use app\models\PropertyFinderDetailUrls;
use app\models\Buildings;
use app\models\ListingsTransactions;


class PropertyFinderHelperFunctions extends Component
{

    public function getWebPageImageUrl($url=null)
    {
        if($url != null)
        {

            $api_key = "AIzaSyCgOsZq9GEQtt6RwDZ6ay7MERxJCnKi-RI";
            $url = trim($url);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url={$url}&key=" . $api_key,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $response = json_decode($response);
            $image = $response->lighthouseResult->audits->{'full-page-screenshot'}->details->screenshot->data;
            $randomString = $this->generateRandomString();
            $img_map = __DIR__ .'/../../images/property-finder/image_'.$randomString.'.png';

            if($image <> null){

                $data = $image;

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);
                file_put_contents($img_map, $data);
                $uploadObject = Yii::$app->get('s3bucket')->upload('/property-finder-screenshots/'.$img_map, $img_map);
                $url = Yii::$app->get('s3bucket')->getUrl('/property-finder-screenshots/'.$img_map);
                if($url <> null)
                {
                    // INSERT (table name, column values)
                    Yii::$app->db->createCommand()->insert('propertyfinder_imageurls', [
                        'bucket_url' => $url,
                    ])->execute();
                    return $url;
                }
            }


        }
    }


    public function getWebPageImageUrl_old($url=null)
    {
        if ($url<>null) {
            $url = $url;
            $api_key = "AIzaSyCgOsZq9GEQtt6RwDZ6ay7MERxJCnKi-RI";
            $request = "https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url={$url}&key=" . $api_key;


            // Let's go cURLing...
            $ch = curl_init($request);

            curl_setopt($ch, CURLOPT_HEADER, 0);

            $usama = curl_exec($ch);
          /*  echo "<pre>"; print_r($usama);
            curl_close($ch);


            die();*/




            $hit = file_get_contents($request);

            $response = json_decode($hit);

            $image = $response->lighthouseResult->audits->{'full-page-screenshot'}->details->screenshot->data;
            $randomString = $this->generateRandomString();

            $content = file_get_contents($image);
            file_put_contents('images/property-finder/image-'.$randomString.'.png', $content);

            $img_map = 'images/property-finder/image-'.$randomString.'.png';
            file_put_contents($img_map, file_get_contents($image));

            $uploadObject = Yii::$app->get('s3bucket')->upload('/property-finder-screenshots/'.$img_map, $img_map);
            $url = Yii::$app->get('s3bucket')->getUrl('/property-finder-screenshots/'.$img_map);
            if($url <> null){
                // INSERT (table name, column values)
                Yii::$app->db->createCommand()->insert('propertyfinder_imageurls', [
                    'bucket_url' => $url,
                ])->execute();
                return $url;
            }
        }
    }

    public function getFullPageContent($imgUrl=null)
    {
        // $imgUrl = 'https://wis-windmills.s3.ap-southeast-1.amazonaws.com//property-finder-screenshots/images/property-finder/image-jzgID1XqH6.png';

        $url = 'https://api.ocr.space/parse/imageurl?apikey=ce5c9417c088957&url='.$imgUrl;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        $json = json_decode($response, true);
        $html = '';
        if (isset($json['ParsedResults'][0]['ParsedText']) && $json['ParsedResults'][0]['ParsedText']<>null) {
            $html = $json['ParsedResults'][0]['ParsedText'];
            $html = strtolower($html);
            // echo "<pre>"; print_r($html); echo "</pre>"; die();
            return $html;
        }else{
            return $html;
        }
    }

    public function getListingPropertyType($html=null)
    {
        // echo "<pre>"; print_r($html); die();
        $listing_property_type = 'Not known';

        $position = strpos($html, 'type:');
        if ($position<>null) {
            $nextChar = substr($html, $position, 20);
            $seprateLine = explode(PHP_EOL, $nextChar);
            $explode = explode('type:', trim($seprateLine[0]));
            $keyword = $explode[1];
            $findInDatabase = \app\models\ListingSubTypes::find()->where(['title'=>trim($keyword)])->one();
            if ($findInDatabase<>null) {
                $listing_property_type = $findInDatabase->id;
            }else{
                $listing_property_type = 'Not known';
            }

        }
        else{
            $listing_property_type = 'Not known';
        }
        // echo "<br><br>Property Type:- ". $listing_property_type; die();

        return $listing_property_type;
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getCommAndSubCommIds($community=null, $sub_community=null)
    {
        $community_id = '';
        $sub_community_id = '';
        if ($community<>null) {
            $findCommunity = Communities::find()->where(['title'=> trim($community)])->one();
            if ($findCommunity<>null) {
                $community_id =  $findCommunity->id;
            }
        }
        if ($sub_community<>null) {
            $findSub_community = SubCommunities::find()->where(['title'=> trim($sub_community)])->one();
            if ($findSub_community<>null) {
                // $community_id    = $findSub_community->community;
                $sub_community_id = $findSub_community->id;
            }
        }else{
            $findSub_community = SubCommunities::find()->where(['title'=> trim($community)])->one();
            if ($findSub_community<>null) {
                // $community_id    = $findSub_community->community;
                $sub_community_id = $findSub_community->id;
            }
        }

        return [
            'community_id' => $community_id,
            'sub_community_id' => $sub_community_id,
        ];
    }

    public function getBuaAndLandArea($desc=null,$property_size=null,$bedrooms=null)
    {
        if ($property_size<>null) {
            $percentage_value = $property_size*10/100;
            $addition_value = $property_size+$percentage_value;
            $subtraction_value = $property_size-$percentage_value;
        }

        $buaWords = [
            'bua','built up','builtup','built-up','b u a','building area','building size','buildup','build up','b.u.a'
        ];
        $PlotWords = [
            'plot','land','total area'
        ];
        $bua = $land_size = null;

        if (
            strpos($desc, 'bua') !== false
            || strpos($desc, 'built up') !== false
            || strpos($desc, 'builtup') !== false
            || strpos($desc, 'built-up') !== false
            || strpos($desc, 'building area') !== false
            || strpos($desc, 'building size') !== false
            || strpos($desc, 'buildup') !== false
            || strpos($desc, 'build up') !== false
            || strpos($desc, 'b.u.a') !== false
        ) {
            foreach ($buaWords as $word) {
                $findWord = strpos($desc, $word);
                if ($findWord<>null) {
                    $nextChar = substr($desc, $findWord, 20);
                    // echo "<br><br><br>".$nextChar."<br><br>"; die();
                    $val = preg_replace('/[^0-9-,-.]/', '', $nextChar);
                    // echo "<br><br><br>".$val."<br><br>"; die();
                    $finalBUA = str_replace(',', '', $val);
                    // echo "<br><br><br>".$finalBUA."<br><br>"; die();
                    if ($finalBUA<>null && $finalBUA>0 && $finalBUA!=$property_size) {
                        $bua = $finalBUA;
                        $land_size = $property_size;
                    }else{
                        $bua = $property_size;
                    }
                }
            }
        }
        // else{
        // 	$bua = $property_size;
        // }


        //if size mentiond in description is not different then size given in property info then find land size in description using keyeords ||
        if ($land_size==null) {
            if (
                strpos($desc, 'plot') !== false
                || strpos($desc, 'land') !== false
                || strpos($desc, 'total area') !== false
            ) {
                foreach ($PlotWords as $word) {
                    $findWord = strpos($desc, $word);
                    if ($findWord<>null) {
                        $nextChar = substr($desc, $findWord, 20);
                        $val = preg_replace('/[^0-9-,-.]/', '', $nextChar);

                        $finalLandSize = str_replace(',', '', $val);
                        if ($finalLandSize<>null && $finalLandSize>0 && $finalLandSize!=$property_size) {
                            $land_size = $finalLandSize;
                            $bua = $property_size;
                        }else if ($finalLandSize<>null && $finalLandSize>0 && $finalLandSize==$property_size){
                            $land_size = $property_size;
                        }
                    }
                }
            }
        }


        // if BUA is not found in description then find bua in databased in previous listings ||
        if ($bua==null) {
            $findInPreviousListing = \app\models\ListingsTransactions::find()
                ->where(['no_of_bedrooms'=>$bedrooms])
                ->andWhere(['and',['>=', 'built_up_area', $subtraction_value],['<=', 'built_up_area', $addition_value]])
                ->asArray()->all();
            if ($findInPreviousListing<>null) {
                $totalBuildUpArea = 0;
                foreach ($findInPreviousListing as $key => $value) {
                    $totalBuildUpArea += $value['built_up_area'];
                }
                $built_up_areaa = $totalBuildUpArea/count($findInPreviousListing);
                $bua = number_format((float)$built_up_areaa, 2, '.', '');
            }else{
                $bua = $property_size;
            }
        }

        if ($land_size==null) {
            $findInPreviousListing = \app\models\ListingsTransactions::find()
                ->where(['no_of_bedrooms'=>$bedrooms])
                ->andWhere(['and',['>=', 'land_size', $subtraction_value],['<=', 'land_size', $addition_value]])
                ->asArray()->all();

            if ($findInPreviousListing<>null) {
                $totalLandArea = 0;
                foreach ($findInPreviousListing as $key => $value) {
                    $totalLandArea += $value['built_up_area'];
                }
                $landArea = $totalLandArea/count($findInPreviousListing);
                $land_size = number_format((float)$landArea, 2, '.', '');
            }
            else{
                $land_size = $property_size;
            }
        }

        return[
            'bua' => $bua,
            'land_size' => $land_size,
        ];
    }

    public function getListingRent($property_size=null, $no_of_bedrooms=null)
    {
        if ($property_size<>null) {
            $percentage_value = $property_size*10/100;
            $addition_value = $property_size+$percentage_value;
            $subtraction_value = $property_size-$percentage_value;

            $listings_rent = '';

            $findInPreviousListing = \app\models\ListingsTransactions::find()
                ->where(['no_of_bedrooms'=>$no_of_bedrooms])
                ->andWhere(['and',['>=', 'listings_rent', $subtraction_value],['<=', 'listings_rent', $addition_value]])
                ->asArray()->all();
            if ($findInPreviousListing<>null) {
                $totalListRent = 0;
                foreach ($findInPreviousListing as $key => $value) {
                    $totalListRent += $value['listings_rent'];
                }
                $listings_rent = $totalListRent/count($findInPreviousListing);
                $listings_rent = number_format((float)$listings_rent, 2, '.', '');
                return $listings_rent;
                // echo "average listing rent: ".$built_up_area."<br>"; die();
            }
            else{
                return 0;
                // echo "no listings found in previous records"; die();
            }
        }
    }

    public function PropertyDetail($contents='', $keyword='')
    {
        $len = strlen($contents);
        $pos = 0;

        $pos = strpos($contents, $keyword, $pos);
        $startOfLine = strrpos($contents, "\n", ($pos - $len));
        $endOfLine = strpos($contents, "\n", $pos);
        $line = substr($contents, $startOfLine, ($endOfLine - $startOfLine));

        list($key, $value) = explode('=', trim($line));
        $result = preg_replace("/[^a-zA-Z0-9]+/", " ", html_entity_decode($value, ENT_QUOTES));
        return trim($result);
    }
}
