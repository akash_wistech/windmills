<?php
namespace app\components\helpers;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use app\models\SalesAndMarketing;

class SalesAndMarketingHelper extends Component 
{
    
    public function sendEmail($model=null)
    {
        // $attendeesHtml = '';
        // if($model->smAttendees){
        //     foreach ($model->smAttendees as $key => $smAttendee)
        //     {
        //         $attendeesHtml .= '<li>'.$smAttendee->attendee->firstname.' '.$smAttendee->attendee->lastname.'</li>';
                
        //     }
        //     $attendeesHtml = '<ul>'.$attendeesHtml.'</ul>';
        // }

        $countWmAttendee = count($model->wmAttendees);
        $wm_attendeesHtml = '';
        if($model->wmAttendees<>null){
            foreach($model->wmAttendees as $index => $attendee){
                $wm_attendeesHtml .= $attendee->client_attendee_title. ' '.$attendee->attendee->firstname.' '.$attendee->attendee->lastname;
                if($index < $countWmAttendee - 1) {
                    $wm_attendeesHtml .= ', ';
                }
            }
        }

        $countClientOtherAttendee = count($model->clientAllAttendees);
        $client_attendeesHtml = '';
        if($model->clientAllAttendees<>null){
            foreach($model->clientAllAttendees as $index => $attendee){
                $client_attendeesHtml .= $attendee->client_attendee_title. ' '.$attendee->attendee->firstname.' '.$attendee->attendee->lastname;
                if($index < $countClientOtherAttendee - 1) {
                    $client_attendeesHtml .= ', ';
                }
            }
        }

        if($model->clientMainAttendee<>null){
            $client_main_attendee = $model->clientMainAttendee->client_attendee_title .' '.$model->clientMainAttendee->attendee->firstname.' '.$model->clientMainAttendee->attendee->lastname;
        }else{
            $client_main_attendee = $model->client->title;
        }

        
        $attendeeIds = [];
        if($model->smAttendees<>null){
            $attendeeIds = \yii\helpers\ArrayHelper::getColumn($model->smAttendees, 'sales_and_marketing_attendees_id');
            // dd($attendeeIds);
        }


        // $calendar_invite = '';
        $calendar_invite = Yii::$app->smHelper->getCalendarInviteLink($model);

        $attachments[] = 'https://maxima.windmillsgroup.com/Windmills_Corporate_Profile_2023.pdf';
        $notifyData = [
            'client' => $model->client,
            'attendeeIds' => $attendeeIds,
            'attachments' => $attachments,
            'replacements'=>[
                '{client_main_attendee}' => $client_main_attendee,
                '{meeting_start_date}'   => date('d F Y', strtotime($model->date)),
                '{meeting_start_time}'   => date('H:m:s', strtotime($model->time)),
                '{meeting_place}'        => $model->meeting_place,
                '{client_attendees}'     => $client_attendeesHtml,
                '{windmills_attendees}'  => $wm_attendeesHtml,
                '{meeting_interface}'    => Yii::$app->smHelper->getMeetingInterfaceArr()[$model->meeting_interface],
                '{purpose}'              => Yii::$app->smHelper->getPurposeArr()[$model->purpose],
                '{google_location_pin}'  => str_replace(' ', '+', $model->location_url),
                '{calendar_invitation_link}' => $calendar_invite,
                '{name_of_scheduler}'    => $model->scheduler->firstname.' '.$model->scheduler->lastname,

            ],
        ];
        // dd($notifyData);
        //sending email
        \app\modules\wisnotify\listners\NotifyEvent::fireMeeting2('sm.meeting', $notifyData);

        
        Yii::$app->db->createCommand()
        ->update(SalesAndMarketing::tableName(), [ 'email_status' => 1 ], [ 'id' => $model->id ])
        ->execute();
        
        Yii::$app->db->createCommand()
        ->update(SalesAndMarketing::tableName(), [ 'calendar_invitation_link' => $calendar_invite ], [ 'id' => $model->id ])
        ->execute();
    }


    public function sendFeedbackEmail($model)
    {
        $attachments = [];
        if($model->send_corporate_profile==1){
            $corporate_profile =\app\models\WindmillsDocs::find()->where(['file' => 'corporate_profile'])->orderBy(['created_at' => SORT_DESC])->one();
            if($corporate_profile<>null){ $attachments[] = $corporate_profile->url; }
        }
        if($model->send_fee_tariff==1){
            // $attachments[] = '';
        }
        if($model->send_market_research_reports_newsletter==1){
            // $attachments[] = '';
        }
        // dd($attachments);
        $notifyData = [
            'client' => $model->client,
            'attachments' => $attachments,
            'replacements'=>[
                '{clientName}' => $model->client->title,
            ],
        ];
        // dd($notifyData);
        //sending email
        \app\modules\wisnotify\listners\NotifyEvent::fireMeeting2('sm.feedbackemail', $notifyData);

        
        Yii::$app->db->createCommand()
        ->update(SalesAndMarketing::tableName(), [ 'is_feedback_email_send' => 1 ], [ 'id' => $model->id ])
        ->execute();

    }
    
    
    public function getCalendarInviteLink($model)
    {
        //generate access token
        $this->generateAccessRefreshTokens();
        
        $calendar_id = Yii::$app->appHelperFunctions->getSetting('google_calendar_id');
        $access_token = Yii::$app->appHelperFunctions->getSetting('google_calendar_access_token');

        $attendeeIds = [];
        if($model->smAttendees<>null){
            $attendeeIds = \yii\helpers\ArrayHelper::getColumn($model->smAttendees, 'sales_and_marketing_attendees_id');
            $attendeeEmails = yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['in', 'id', $attendeeIds])
            // ->andWhere(['status'=>1])
            ->asArray()->all(), 'email','email');

            $c_attendees_emails = [];
            $i=0;
            
            foreach($attendeeEmails as $email){
                $c_attendees_emails[$i]['email'] = $email;
                $i++;
            }
        }   

        $phpData = array(
            "summary" => Yii::$app->smHelper->getPurposeArr()[$model->purpose],
            "location" => $model->meeting_place,
            "description" => Yii::$app->smHelper->getPurposeArr()[$model->purpose],
            "start" => array(
                "dateTime" => $model->date.'T'.$model->time,
                "timeZone" => "Asia/Dubai"
            ),
            "end" => array(
                "dateTime" => $model->meeting_end_date.'T'.$model->meeting_end_time,
                "timeZone" => "Asia/Dubai"
            ),
            "reminders" => array(
                "useDefault" => true
            ),
            "attendees" => $c_attendees_emails,
        );
        // dd($phpData);

        
        //create a calendar event
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://www.googleapis.com/calendar/v3/calendars/'.$calendar_id.'/events',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($phpData),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$access_token.'',
                'Content-Type: application/json'
            ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $response = json_decode($response,true);

        // echo"<pre>"; print_r($response); echo"</pre>"; die();

        // if(isset($response['status']) && $response['status']=='confirmed'){
            return $response['htmlLink'];
        // }

        // if(isset($response['error']['errors'][0]['message']) && $response['error']['errors'][0]['message'] == 'Invalid Credentials'){
        //     $this->generateAccessRefreshTokens();
        //     $this->getCalendarInviteLink($model);
        // }

        
    }
    
    
    public function generateAccessRefreshTokens()
    {
        $client_id = Yii::$app->appHelperFunctions->getSetting('google_calendar_client_id');
        $client_secret = Yii::$app->appHelperFunctions->getSetting('google_calendar_client_secret');
        $refresh_token = Yii::$app->appHelperFunctions->getSetting('google_calendar_refresh_token');
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://oauth2.googleapis.com/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'client_id='.$client_id.'&client_secret='.$client_secret.'&grant_type=refresh_token&refresh_token='.$refresh_token.'',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $response = json_decode($response,true);

        
        if(isset($response['access_token'])){
            Yii::$app->db->createCommand()->update('setting', ['config_value' => $response['access_token']], [ 'config_name'=>'google_calendar_access_token' ])->execute();
        }
        
        
        
        
    }
    
    
    
    public function getMeetingInterfaceArr()
    {
        return 
        [
            '1' =>'Face to face',
            '2' =>'Zoom',
            '3' =>'Call',
        ];
    }
    
    public function getPurposeArr()
    {
        return ArrayHelper::map(\app\models\SalesAndMarketingPurpose::find()->select(['id','title'])->asArray()->all(), 'id','title');
    }
    
    public function getCalendarInviteArr()
    {
        return 
        [
            '1' => 'Yes',
            '0' => 'No',
        ];
    }
    
    public function getAttendeesArr()
    {
        return ArrayHelper::map(\app\models\User::find()->select(['id', 'CONCAT(firstname, " ", lastname) AS full_name'])->where(['user_type'=>10])->asArray()->all(), 'id','full_name');
    }

    public function getTitleArr()
    {
        return [
            'Mr.' => 'Mr.',
            'Mrs.' => 'Mrs.',
            'Miss.' => 'Miss.',
            'Ms.' => 'Ms.',
            'Dr.' => 'Dr.',
            'Prof.' => 'Prof.',
        ];
    }

    public function getMeetingStatusArr()
    {
        return [
            1 => 'Pending',
            2 => 'Postpone',
            3 => 'Rejected',
            4 => 'Done',
        ];
    }

    public function getAttendeesLable($attendees, $color)
    {
        $countWmAttendee = count($attendees);
        $wm_attendeesHtml = '';
        if($attendees<>null){
            foreach($attendees as $index => $attendee){
                $wm_attendeesHtml .= '<span class="badge grid-badge badge-'.$color.'">'.$attendee->client_attendee_title. ' '.$attendee->attendee->firstname.' '.$attendee->attendee->lastname.'</span>';
                if($index < $countWmAttendee - 1) {
                    $wm_attendeesHtml .= ' ';
                }
            }
        }
        return $wm_attendeesHtml;
    }

    public function getMeetingStatusLabelArr()
    {
        return[
            1 => '<span class="badge grid-badge badge-warning">'.Yii::t('app','Pending').'</span>',
            2 => '<span class="badge grid-badge badge-info">'.Yii::t('app','Postpone').'</span>',
            3 => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Rejected').'</span>',
            4 => '<span class="badge grid-badge badge-success">'.Yii::t('app','Done').'</span>',
        ];
    }


    public function getClientContactsArr($client_id)
    {
      return ArrayHelper::map(\app\models\User::find()->select(['id', 'CONCAT(firstname, " ", lastname) as fullname'])->where(['company_id'=>$client_id])->asArray()->all(), "id", "fullname");
    }


    public function getDocAllowArr()
    {
        return [
            1 => 'Yes',
            2 => 'No',
        ];
    }
    
    public function getClientPriorityArr()
    {
        return [
            1 => '1st',
            2 => '2nd',
            3 => '3rd',
            4 => '4th',
            5 => '5th',
        ];
    }
    public function getPrefixArr()
    {
        return [
            'Mr.' => 'Mr.',
            'Mrs.' => 'Mrs.',
            'Miss.' => 'Miss.',
        ];
    }






}