<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use app\models\WaterSportEquipment;
use yii\helpers\Url;

class JavaScriptFunctions extends Component
{
  public function getLogCallActionResponseList()
  {
    $responses=Yii::$app->helperFunctions->actionLogCallQuickNoteResponse;
    $txtJScript='var alLogCallActionResponses = '.json_encode($responses).';';
    return $txtJScript;
  }
  public function getIasSpinnerHtml()
  {
    $txtJScript='';
    $txtJScript.='iasSpinnerHtml = "<div class=\"col-xs-12 col-sm-12 text-center\">";';
    $txtJScript.='iasSpinnerHtml+= "  <div class=\"loading-message loading-message-boxed\">";';
    $txtJScript.='iasSpinnerHtml+= "    <img src=\"images/loading.gif\" height=\"25\">";';
    $txtJScript.='iasSpinnerHtml+= "    <span>&nbsp;&nbsp;'.Yii::t('app','Loading').'</span>";';
    $txtJScript.='iasSpinnerHtml+= "  </div>";';
    $txtJScript.='iasSpinnerHtml+= "</div>";';
    return $txtJScript;
  }

  /**
   * Generates javascript array for the workflow states
   *
   * @return array
   */
  // public function getWorkFlowStagesList($workflowLists)
  // {
  //   // echo '<pre>';print_r($workflowLists);echo '</pre>';
  //   $stages=Yii::$app->appHelperFunctions->getWorkflowStagesList($workflowLists);
  //   //echo '<pre>';print_r($stages);echo '</pre>';
  //   $txtJScript="var workflowStages = new Array();\n";
  //   $fullArr=[];
  //   if($stages!=null){
  //     foreach($stages as $stage){
  //       $fullArr[$stage['workflow_id']][]=['display'=>$stage['title'],'value'=>$stage['id'],'sel'=>''];
  //     }
  //   }
  //   //echo '<pre>';print_r($fullArr);echo '</pre>';
  //   return "var workflowStages = ".json_encode($fullArr).";\n";
  // }

  /**
   * Generates javascript array for the workflow states
   *
   * @return array
   */
  public function getWorkFlowStagesList($workflowLists)
  {
    // echo '<pre>';print_r($workflowLists);echo '</pre>';
    // $stages=Yii::$app->appHelperFunctions->getWorkflowStagesList($workflowLists);
    //

    $servicesListId = Yii::$app->appHelperFunctions->getSetting('service_list_id');
    $servicesList = Yii::$app->appHelperFunctions->getPredefinedListOptions($servicesListId);

    $fullArr=[];
    if($workflowLists!=null){
      foreach($workflowLists as $workFlowList){
        $servicesArr=[];
        if($servicesList!=null){
          foreach($servicesList as $serviceItem){
            $stagesList = Yii::$app->appHelperFunctions->getWorkflowServiceStagesList($workFlowList['id'],$serviceItem['id']);
            $stagesArr=[];
            if($stagesList!=null){
              foreach($stagesList as $stageItem){
                $stagesArr[$stageItem['id']]=['id'=>$stageItem['id'],'title'=>$stageItem['title']];
              }
            }
            $servicesArr[$serviceItem['id']]=['id'=>$serviceItem['id'],'title'=>$serviceItem['title'],'stages'=>$stagesArr];
          }
        }
        $fullArr[$workFlowList['id']]=['id'=>$workFlowList['id'],'title'=>$workFlowList['title'],'services'=>$servicesArr];
      }
    }
    // echo '<pre>';print_r($fullArr);echo '</pre>';
    // $stages=[];
    // $txtJScript="var workflowStages = new Array();\n";
    // $fullArr=[];
    // if($stages!=null){
    //   foreach($stages as $stage){
    //     // $fullArr[$stage['workflow_id']][]=;
    //   }
    // }
    //echo '<pre>';print_r($fullArr);echo '</pre>';
    return "var workflowStages = ".json_encode($fullArr).";\n";
  }
}
