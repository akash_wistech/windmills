<?php
namespace app\components\helpers;

use app\models\CustomAttachements;
use Yii;
use yii\base\Component;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MemberActivity;
use app\models\User;
use app\models\UserProfileInfo;
use app\models\InspectProperty;
use app\models\ValuationConfiguration;

class PdfHelper extends Component
{


    public function getTabeleCss(){

        $TableCss='<style>

    table {

    }

    table.main-class{
     /* border-top: 1px solid #BBDEFB;
      border-bottom: 1px solid #BBDEFB;
      border-left: 1px solid #BBDEFB;
      border-right: 1px solid #BBDEFB;*/
      font-size:12px;
    }
    span.heading-main{
      color:#0D47A1; font-size:14px;
      background-color:#E65100;
      width:100%;
    }
    span.tdbold{
      font-weight: bold;
    }

    td.tdbold{
      font-weight: bold;
    }
    td.tdbold&red{
      font-weight: bold;
      color:#D50000;
    }
    td.tdbold&green{
      font-weight: bold;
      color:#1B5E20;
    }
    tr.bggray {
    background-color: #ECEFF1;
    }
    div.instruction{
    font-size:12px;
    text-align:justify;

    }
    td.table_of_content{
       color:#0D47A1;
       font-size:11px;
       font-weight:bold;
       border-bottom: 0px solid #BBDEFB;
       text-align:center;
    }
    td.contentfontsize{
      font-size:12px;
    }
    td.tableFirstHeading{
      font-weight: bold;
      font-size:14px;
      color:#0D47A1;
    }
    td.tableSecondHeading{
      font-weight: bold;
      border-bottom: 1px solid #64B5F6;
      font-size:14px;
      color:#0D47A1;
      padding-top:0px;
    }
    td.tableSecondHeading h3 { padding: 0px}

    td.evrsheading { line-height: 25px; }
    
    .tcNumber {
        font-size:36px; color:#E65100;
    }

    .tcHeading{
      font-weight: bold;
      font-size:16px;
      color:#0D47A1;
    }
    .tcTextList {
        font-size:10px;
        padding:0px;
        margin:0px;
    }
    
    .contentHeading{
      font-size:12px;
      line-height:12px;
      font-style:italic;
      font-weight:bold;
      padding:0px;
    }
    td.detailheading{
      color:#0D47A1;
      font-size:14px;
      font-weight:bold;
      border-bottom: 1px solid #64B5F6;
      background-color:#ECEFF1;
      padding-left:10px;
    }
    td.detailtext{
      font-size:11px;
      padding-left:10px;
    }
    tr.color{
      background-color:#ECEFF1;
    }
    td.tdBorder{
        border-top: 1px solid #666666;
        border-bottom: 1px solid #666666;
    }
    

    </style>';


        return $TableCss;
    }

    public function StrReplace($value){

        //$value1=str_replace("_"," ",$value);
        $value1=explode("_",$value);
        return  $value1[1]." ".$value1[0];
    }

    public function getUserInformation($model){


        $contact= User::find()
            ->where(['company_id'=>$model->client_id,'user_type'=>0,'primary_contact'=>1])
            ->innerJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().".user_id=".User::tableName().".id")
            ->one();

        $instructorname = "";

        if($contact <> null && !empty($contact)){
            $instructorname = $contact->firstname. " ".$contact->lastname;
        }
        return $instructorname;

    }


    public function getViewType($model){



        if ($model->inspectProperty->view_community!='none') {

            $viewtype['viewCommunity']=Yii::$app->appHelperFunctions->viewCommunityAttributesValue[$model->inspectProperty->view_community];

        }
        else {
            $viewtype['viewCommunity']='';
        }

        if ($model->inspectProperty->view_pool!='none') {
            $viewtype['viewPool']='<br>'.Yii::$app->appHelperFunctions->viewAttributesValue[$model->inspectProperty->view_pool].' Pool/ Fountain View ';
        }
        else {
            $viewtype['viewPool']='';
        }


        if ($model->inspectProperty->view_burj!='none') {
            $viewtype['viewBurj']='<br>'.Yii::$app->appHelperFunctions->viewAttributesValue[$model->inspectProperty->view_burj].' Burj View';
        }
        else {
            $viewtype['viewBurj']='';
        }


        if ($model->inspectProperty->view_sea!='none') {
            $viewtype['viewSea']='<br>'.Yii::$app->appHelperFunctions->viewAttributesValue[$model->inspectProperty->view_sea].' Sea View';
        }
        else {
            $viewtype['viewSea'] ='';
        }


        if ($model->inspectProperty->view_marina!='none') {
            $viewtype['viewMarina']='<br>'.Yii::$app->appHelperFunctions->viewAttributesValue[$model->inspectProperty->view_marina].' Marina / Creek View';
        }
        else {
            $viewtype['viewMarina']='';
        }
        if ($model->inspectProperty->view_mountains!='none') {
            $viewtype['h']='<br>'.Yii::$app->appHelperFunctions->viewAttributesValue[$model->inspectProperty->view_mountains].' Mountains View';
            // echo $viewtype['h'];
            // die();
            //  echo $viewtype['viewMountains'];

        }
        // else {
        //   $viewtype['viewMountains']='';
        // }


        if ($model->inspectProperty->view_lake!='none') {
            $viewtype['viewLake']='<br>'.Yii::$app->appHelperFunctions->viewAttributesValue[$model->inspectProperty->view_lake].' Lake View';
        }
        else {
            $viewtype['viewLake']='';
        }

        if ($model->inspectProperty->view_golf_course!='none') {
            $viewtype['viewGolfCourse']='<br>'.Yii::$app->appHelperFunctions->viewAttributesValue[$model->inspectProperty->view_golf_course].' Golf Course View';
        }
        else {
            $viewtype['viewGolfCourse']='';
        }



        if ($model->inspectProperty->view_park!='none') {
            $viewtype['viewPark'] ='<br>'.Yii::$app->appHelperFunctions->viewAttributesValue[$model->inspectProperty->view_park].' Park View';
        }
        else {
            $viewtype['viewPark']='';
        }

        if ($model->inspectProperty->view_special!='none') {
            $viewtype['viewSpecial']='<br>'.Yii::$app->appHelperFunctions->viewAttributesValue[$model->inspectProperty->view_special].' Special View';
        }
        else {
            $viewtype['viewSpecial']='';
        }
        return $viewtype;



    }


    public function getFloorConfig_old($model){



        $model2 = ValuationConfiguration::find()->where(['valuation_id' => $model->id])->one();

        //Array of configuration such as bedroom, bathrooms etc
        $configuration = InspectProperty::find()->where(['valuation_id' => $model->id])->one();
        $config_type= Yii::$app->helperFunctions->configrationSectionListArr;


        if ($model->valuationConfiguration->over_all_upgrade >=3 && $model->property_id != 1) {

            foreach ($config_type as $key => $value) {

                if($value!=null)
                {
                    $title = str_replace("_", " ", str_replace("config"," ",$key));
                    $title = rtrim($title,"s");
                    $title7=ucfirst(trim($title));



                    $groundFloornumber=0;
                    $firstFloornumber=0;
                    $secondFloornumber=0;

                    for ($i=0; $i <$configuration->{$value} ; $i++) {
                        $bed_Details = \app\models\ConfigurationFiles::find()->where(["valuation_id" => $model->id, "index_id" => $i, 'type' => $key])->one();

                        if($bed_Details->floor==1){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title = rtrim($title,"s");
                            $title2=ucfirst(trim($title)).'<br>';
                            $title=ucfirst(trim($title)).' # '.($i+1).'  ';
                            $floorConfig['groundFloor'].=$title;
                            if ($bed_Details->flooring==1) $floorConfig['groundFloor'].='Flooring, ';
                            if ($bed_Details->ceilings==1) $floorConfig['groundFloor'].='Ceiling, ';
                            if ($bed_Details->speciality==1)   $floorConfig['groundFloor'].='Speciality';
                            $floorConfig['groundFloor'].='<br>';
                            $groundFloornumber++;
                            $j++;

                        }
                        if($bed_Details->floor==2){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title = rtrim($title,"s");
                            $title=ucfirst(trim($title)).' # '.($i+1).'  ';
                            $floorConfig['firstFloor'].=$title;
                            if ($bed_Details->flooring==1) $floorConfig['firstFloor'].='Flooring, ';
                            if ($bed_Details->ceilings==1) $floorConfig['firstFloor'].='Ceiling, ';
                            if ($bed_Details->speciality==1)   $floorConfig['firstFloor'].='Speciality';
                            $floorConfig['firstFloor'].='<br>';
                            $firstFloornumber++;

                        }
                        if($bed_Details->floor==3){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title = rtrim($title,"s");
                            $title=ucfirst(trim($title)).' # '.($i+1).'  ';
                            $floorConfig['secondFloor'].=$title;
                            if ($bed_Details->flooring==1) $floorConfig['secondFloor'].='Flooring, ';
                            if ($bed_Details->ceilings==1) $floorConfig['secondFloor'].='Ceiling, ';
                            if ($bed_Details->speciality==1)   $floorConfig['secondFloor'].='Speciality';
                            $floorConfig['secondFloor'].='<br>';
                            $secondFloornumber++;
                        }
                    }


                    if ($groundFloornumber !=null) {
                        $floorConfig['groundFloortitle'].='Total '. $title7.' : '.$groundFloornumber.'<br>';
                    }
                    if ($firstFloornumber !=null) {
                        $floorConfig['firstFloortitle'].='Total '. $title7.' : '.$firstFloornumber.'<br>';
                    }
                    if ($secondFloornumber !=null) {
                        $floorConfig['secondFloortitle'].='Total '. $title7.' : '.$secondFloornumber.'<br>';
                    }
                }
            }
            $floorConfig['upgrade_text'] ='Upgrades <br>'.($floorConfig['groundFloor']!='' ? $floorConfig['groundFloor'] : 'None').'<br>First Floor<br>'.($floorConfig['firstFloor']!='' ? $floorConfig['firstFloor'] : 'None').'<br>Second Floor<br>'.($floorConfig['secondFloortitle']!='' ? $floorConfig['secondFloortitle'] : 'None');


        }

        // echo "<pre>";
        // print_r($floorConfig);
        // echo "<pre>";
        // die();
        //






        if ($model->valuationConfiguration->over_all_upgrade>=3 && $model->property_id == 1 ) {
            foreach ($config_type as $key => $value) {

                if($value!=null)
                {
                    $title = str_replace("_", " ", str_replace("config"," ",$key));
                    $title7=ucfirst(trim($title));
                    $groundFloornumber=0;
                    $firstFloornumber=0;
                    $secondFloornumber=0;

                    for ($i=0; $i <$configuration->{$value} ; $i++) {
                        $bed_Details = \app\models\ConfigurationFiles::find()->where(["valuation_id" => $model->id, "index_id" => $i, 'type' => $key])->one();

                        if($bed_Details->floor==1){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title2=ucfirst(trim($title)).'<br>';
                            $title=ucfirst(trim($title)).' #'.($i+1).' : ';
                            $floorConfig['groundFloor'].=$title;
                            if ($bed_Details->flooring==1) $floorConfig['groundFloor'].='Flooring, ';
                            if ($bed_Details->ceilings==1) $floorConfig['groundFloor'].='Ceiling, ';
                            if ($bed_Details->speciality==1)   $floorConfig['groundFloor'].='Speciality';
                            $floorConfig['groundFloor'].='<br>';
                            $groundFloornumber++;
                            $j++;

                        }
                        if($bed_Details->floor==2){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title=ucfirst(trim($title)).' #'.($i+1).' : ';
                            $floorConfig['firstFloor'].=$title;
                            if ($bed_Details->flooring==1) $floorConfig['firstFloor'].='Flooring, ';
                            if ($bed_Details->ceilings==1) $floorConfig['firstFloor'].='Ceiling, ';
                            if ($bed_Details->speciality==1)   $floorConfig['firstFloor'].='Speciality';
                            $floorConfig['firstFloor'].='<br>';
                            $firstFloornumber++;

                        }
                        if($bed_Details->floor==3){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title=ucfirst(trim($title)).' #'.($i+1).' : ';
                            $floorConfig['secondFloor'].=$title;
                            if ($bed_Details->flooring==1) $floorConfig['secondFloor'].='Flooring, ';
                            if ($bed_Details->ceilings==1) $floorConfig['secondFloor'].='Ceiling, ';
                            if ($bed_Details->speciality==1)   $floorConfig['secondFloor'].='Speciality';
                            $floorConfig['secondFloor'].='<br>';
                            $secondFloornumber++;
                        }
                    }


                    if ($groundFloornumber !=null) {
                        $floorConfig['groundFloortitle'].='Total '. $title7.' : '.$groundFloornumber.'<br>';
                    }
                    if ($firstFloornumber !=null) {
                        $floorConfig['firstFloortitle'].='Total '. $title7.' : '.$firstFloornumber.'<br>';
                    }
                    if ($secondFloornumber !=null) {
                        $floorConfig['secondFloortitle'].='Total '. $title7.' : '.$secondFloornumber.'<br>';
                    }
                }
            }
            // $floorConfig['upgrade_text'] ='Upgrades <br>'.($groundFloor!='' ? $groundFloor : 'None').'<br>First Floor<br>'.($firstFloor!='' ? $firstFloor : 'None').'<br>Second Floor<br>'.($secondFloor!='' ? $secondFloor : 'None');
            $floorConfig['upgrade_text'] ='Upgrades <br>'.($floorConfig['groundFloor']!='' ? $floorConfig['groundFloor'] : 'None').'<br>First Floor<br>'.($floorConfig['firstFloor']!='' ? $floorConfig['firstFloor'] : 'None').'<br>Second Floor<br>'.($floorConfig['secondFloortitle']!='' ? $floorConfig['secondFloortitle'] : 'None');

        }


        if ($floorConfig['groundFloortitle']==null) {
            $floorConfig['groundFloortitle']="None";
        }
        if ($floorConfig['firstFloortitle']==null) {
            $floorConfig['firstFloortitle']="None";

        }
        if ($floorConfig['secondFloortitle']==null) {
            $floorConfig['secondFloortitle']="None";
        }

        // print_r($floorConfig);
        // die();

        return $floorConfig;

    }

    public function getFloorConfig($model){



        $model2 = ValuationConfiguration::find()->where(['valuation_id' => $model->id])->one();

        //Array of configuration such as bedroom, bathrooms etc
        $configuration = InspectProperty::find()->where(['valuation_id' => $model->id])->one();
        $config_type= Yii::$app->helperFunctions->configrationSectionListArr;


        if ($model->valuationConfiguration->over_all_upgrade>=3 && $model->property_id != 1 && $model->property_id != 17 && $model->property_id != 12 && $model->property_id != 14 && $model->property_id != 19 && $model->property_id != 28 && $model->property_id != 37) {

            foreach ($config_type as $key => $value) {

                if($value!=null)
                {
                    $title = str_replace("_", " ", str_replace("config"," ",$key));
                    $title = rtrim($title,"s");
                    $title7=ucfirst(trim($title));



                    $groundFloornumber=0;
                    $firstFloornumber=0;
                    $secondFloornumber=0;
                    $basementnumber=0;

                    for ($i=0; $i <$configuration->{$value} ; $i++) {
                        $bed_Details = \app\models\ConfigurationFiles::find()->where(["valuation_id" => $model->id, "index_id" => $i, 'type' => $key])->one();

                        if($bed_Details->floor==1){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title = rtrim($title,"s");
                            $title2=ucfirst(trim($title)).'<br>';
                            $title=ucfirst(trim($title)).' # '.($i+1).'  ';
                            if($bed_Details->flooring==1 ||$bed_Details->ceilings==1 ||  $bed_Details->speciality==1) {
                                $floorConfig['groundFloor'] .= $title;
                                if ($bed_Details->flooring == 1) $floorConfig['groundFloor'] .= 'Flooring, ';
                                if ($bed_Details->ceilings == 1) $floorConfig['groundFloor'] .= 'Ceiling, ';
                                if ($bed_Details->speciality == 1) $floorConfig['groundFloor'] .= 'Speciality';
                                $floorConfig['groundFloor'] .= '<br>';
                            }
                            $groundFloornumber++;
                            $j++;

                        }
                        if($bed_Details->floor==2){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title = rtrim($title,"s");
                            $title=ucfirst(trim($title)).' # '.($i+1).'  ';
                            if($bed_Details->flooring==1 ||$bed_Details->ceilings==1 ||  $bed_Details->speciality==1) {
                                $floorConfig['firstFloor'] .= $title;
                                if ($bed_Details->flooring == 1) $floorConfig['firstFloor'] .= 'Flooring, ';
                                if ($bed_Details->ceilings == 1) $floorConfig['firstFloor'] .= 'Ceiling, ';
                                if ($bed_Details->speciality == 1) $floorConfig['firstFloor'] .= 'Speciality';
                                $floorConfig['firstFloor'] .= '<br>';
                            }
                            $firstFloornumber++;

                        }
                        if($bed_Details->floor==3){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title = rtrim($title,"s");
                            $title=ucfirst(trim($title)).' # '.($i+1).'  ';
                            if($bed_Details->flooring==1 ||$bed_Details->ceilings==1 ||  $bed_Details->speciality==1) {
                                $floorConfig['secondFloor'] .= $title;
                                if ($bed_Details->flooring == 1) $floorConfig['secondFloor'] .= 'Flooring, ';
                                if ($bed_Details->ceilings == 1) $floorConfig['secondFloor'] .= 'Ceiling, ';
                                if ($bed_Details->speciality == 1) $floorConfig['secondFloor'] .= 'Speciality';
                                $floorConfig['secondFloor'] .= '<br>';
                            }
                            $secondFloornumber++;
                        }
                        if($bed_Details->floor==4){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title = rtrim($title,"s");
                            $title=ucfirst(trim($title)).' # '.($i+1).'  ';
                            if($bed_Details->flooring==1 ||$bed_Details->ceilings==1 ||  $bed_Details->speciality==1) {
                                $floorConfig['basement'] .= $title;
                                if ($bed_Details->flooring == 1) $floorConfig['basement'] .= 'Flooring, ';
                                if ($bed_Details->ceilings == 1) $floorConfig['basement'] .= 'Ceiling, ';
                                if ($bed_Details->speciality == 1) $floorConfig['basement'] .= 'Speciality';
                                $floorConfig['basement'] .= '<br>';
                            }
                            $basementnumber++;
                        }
                    }


                    /*  if ($groundFloornumber !=null) {
                          $floorConfig['groundFloortitle'].='Total '. $title7.' : '.$groundFloornumber.'<br>';
                      }
                      if ($firstFloornumber !=null) {
                          $floorConfig['firstFloortitle'].='Total '. $title7.' : '.$firstFloornumber.'<br>';
                      }
                      if ($secondFloornumber !=null) {
                          $floorConfig['secondFloortitle'].='Total '. $title7.' : '.$secondFloornumber.'<br>';
                      }*/

                    if ($groundFloornumber !=null) {
                        $floorConfig['groundFloortitle'].=$groundFloornumber.' '. $title7.'<br>';
                    }
                    if ($firstFloornumber !=null) {
                        $floorConfig['firstFloortitle'].=$firstFloornumber.' '. $title7.'<br>';
                    }
                    if ($secondFloornumber !=null) {
                        $floorConfig['secondFloortitle'].=$secondFloornumber.' '. $title7.'<br>';
                    }
                    if ($basementnumber !=null) {
                        $floorConfig['basementTitle'].=$basementnumber.' '. $title7.'<br>';
                    }
                }
            }
            $floorConfig['upgrade_text'] ='Basement '.($floorConfig['basement']!='' ? $floorConfig['basement'] : 'None').'<br>Ground Floor '.($floorConfig['groundFloor']!='' ? $floorConfig['groundFloor'] : 'None').'<br>First Floor '.($floorConfig['firstFloor']!='' ? $floorConfig['firstFloor'] : 'None').'<br>Second Floor '.($floorConfig['secondFloor']!='' ? $floorConfig['secondFloor'] : 'None');


        }
        /*  echo "<pre>";
          print_r($floorConfig);
          die;*/

        // echo "<pre>";
        // print_r($floorConfig);
        // echo "<pre>";
        // die();
        //






        if ($model->valuationConfiguration->over_all_upgrade>=3 && ($model->property_id == 1 || $model->property_id == 17 || $model->property_id == 12 || $model->property_id == 14 || $model->property_id == 19 || $model->property_id == 28 || $model->property_id == 37)) {
            foreach ($config_type as $key => $value) {

                if($value!=null)
                {
                    $title = str_replace("_", " ", str_replace("config"," ",$key));
                    $title7=ucfirst(trim($title));
                    $groundFloornumber=0;
                    $firstFloornumber=0;
                    $secondFloornumber=0;

                    for ($i=0; $i <$configuration->{$value} ; $i++) {
                        $bed_Details = \app\models\ConfigurationFiles::find()->where(["valuation_id" => $model->id, "index_id" => $i, 'type' => $key])->one();

                        if($bed_Details->floor==1){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title2=ucfirst(trim($title)).'<br>';
                            $title=ucfirst(trim($title)).' #'.($i+1).' : ';
                            if($bed_Details->flooring==1 ||$bed_Details->ceilings==1 ||  $bed_Details->speciality==1) {
                                $floorConfig['groundFloor'] .= $title;
                                if ($bed_Details->flooring == 1) $floorConfig['groundFloor'] .= 'Flooring, ';
                                if ($bed_Details->ceilings == 1) $floorConfig['groundFloor'] .= 'Ceiling, ';
                                if ($bed_Details->speciality == 1) $floorConfig['groundFloor'] .= 'Speciality';
                                $floorConfig['groundFloor'] .= '<br>';
                            }
                            $groundFloornumber++;
                            $j++;

                        }
                        if($bed_Details->floor==2){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title=ucfirst(trim($title)).' #'.($i+1).' : ';
                            if($bed_Details->flooring==1 ||$bed_Details->ceilings==1 ||  $bed_Details->speciality==1) {
                                $floorConfig['firstFloor'] .= $title;
                                if ($bed_Details->flooring == 1) $floorConfig['firstFloor'] .= 'Flooring, ';
                                if ($bed_Details->ceilings == 1) $floorConfig['firstFloor'] .= 'Ceiling, ';
                                if ($bed_Details->speciality == 1) $floorConfig['firstFloor'] .= 'Speciality';
                                $floorConfig['firstFloor'] .= '<br>';
                            }
                            $firstFloornumber++;

                        }
                        if($bed_Details->floor==3){
                            $title = str_replace("_", " ", str_replace("config"," ",$key));
                            $title=ucfirst(trim($title)).' #'.($i+1).' : ';
                            if($bed_Details->flooring==1 ||$bed_Details->ceilings==1 ||  $bed_Details->speciality==1) {
                                $floorConfig['secondFloor'] .= $title;
                                if ($bed_Details->flooring == 1) $floorConfig['secondFloor'] .= 'Flooring, ';
                                if ($bed_Details->ceilings == 1) $floorConfig['secondFloor'] .= 'Ceiling, ';
                                if ($bed_Details->speciality == 1) $floorConfig['secondFloor'] .= 'Speciality';
                                $floorConfig['secondFloor'] .= '<br>';
                            }
                            $secondFloornumber++;
                        }
                    }

                    /*
                                        if ($groundFloornumber !=null) {
                                            $floorConfig['groundFloortitle'].='Total '. $title7.' : '.$groundFloornumber.'<br>';
                                        }
                                        if ($firstFloornumber !=null) {
                                            $floorConfig['firstFloortitle'].='Total '. $title7.' : '.$firstFloornumber.'<br>';
                                        }
                                        if ($secondFloornumber !=null) {
                                            $floorConfig['secondFloortitle'].='Total '. $title7.' : '.$secondFloornumber.'<br>';
                                        }*/
                    if ($groundFloornumber !=null) {
                        $floorConfig['groundFloortitle'].=$groundFloornumber.' '. $title7.'<br>';
                    }
                    if ($firstFloornumber !=null) {
                        $floorConfig['firstFloortitle'].=$firstFloornumber.' '. $title7.'<br>';
                    }
                    if ($secondFloornumber !=null) {
                        $floorConfig['secondFloortitle'].=$secondFloornumber.' '. $title7.'<br>';
                    }
                }
            }
            $floorConfig['upgrade_text'] ='Unit Floor '.($floorConfig['groundFloor']!='' ? $floorConfig['groundFloor'] : 'None').'<br>First Floor '.($floorConfig['firstFloor']!='' ? $floorConfig['firstFloor'] : 'None').'<br>Second Floor '.($floorConfig['secondFloor']!='' ? $floorConfig['secondFloor'] : 'None');


        }


        if ($floorConfig['groundFloortitle']==null) {
            $floorConfig['groundFloortitle']="None";
        }
        if ($floorConfig['firstFloortitle']==null) {
            $floorConfig['firstFloortitle']="None";

        }
        if ($floorConfig['secondFloortitle']==null) {
            $floorConfig['secondFloortitle']="None";
        }

        // print_r($floorConfig);
        // die();

        return $floorConfig;

    }


    public function getFloorConfigCustom($model){



        $model2 = ValuationConfiguration::find()->where(['valuation_id' => $model->id])->one();

        //Array of configuration such as bedroom, bathrooms etc
        $configuration = CustomAttachements::find()->where(['valuation_id' => $model->id])->all();

        $config_type = Yii::$app->helperFunctions->configrationSectionListArr;


        foreach ($configuration as $key => $value) {

            if($value!=null)
            {

                $title = $value->name;
                $title7=ucfirst(trim($value->name));

                $groundFloornumber=0;
                $firstFloornumber=0;
                $secondFloornumber=0;
                $basementnumber=0;

                for ($i=0; $i <$value->quantity ; $i++) {

                    $bed_Details = \app\models\ConfigurationFiles::find()->where(["valuation_id" => $model->id, "index_id" => $i,"custom_field_id" => $value->id, 'type' => 'custom_fields'])->one();

                    if($bed_Details->floor==1){
                        $title = $value->name;
                        $title=ucfirst(trim($title)).' # '.($i+1).'  ';
                        $groundFloornumber++;
                        $j++;

                    }
                    if($bed_Details->floor==2){
                        $title =$value->name;
                        $title = $value->name;
                        $title=ucfirst(trim($value->name)).' # '.($i+1).'  ';
                        $firstFloornumber++;

                    }
                    if($bed_Details->floor==3){
                        $title = $value->name;
                        $title = $value->name;
                        $title=ucfirst(trim($value->name)).' # '.($i+1).'  ';
                        $secondFloornumber++;
                    }
                    if($bed_Details->floor==4){
                        $title = $value->name;
                        $title = rtrim($value->name,"s");
                        $title=ucfirst(trim($value->name)).' # '.($i+1).'  ';
                        $basementnumber++;
                    }
                }


                /*  if ($groundFloornumber !=null) {
                      $floorConfig['groundFloortitle'].='Total '. $title7.' : '.$groundFloornumber.'<br>';
                  }
                  if ($firstFloornumber !=null) {
                      $floorConfig['firstFloortitle'].='Total '. $title7.' : '.$firstFloornumber.'<br>';
                  }
                  if ($secondFloornumber !=null) {
                      $floorConfig['secondFloortitle'].='Total '. $title7.' : '.$secondFloornumber.'<br>';
                  }*/

                if ($groundFloornumber !=null) {
                    $floorConfig['groundFloortitle'].=$groundFloornumber.' '. $title7.'<br>';
                }
                if ($firstFloornumber !=null) {
                    $floorConfig['firstFloortitle'].=$firstFloornumber.' '. $title7.'<br>';
                }
                if ($secondFloornumber !=null) {
                    $floorConfig['secondFloortitle'].=$secondFloornumber.' '. $title7.'<br>';
                }
                if ($basementnumber !=null) {
                    $floorConfig['basementTitle'].=$basementnumber.' '. $title7.'<br>';
                }
            }
        }
        //$floorConfig['upgrade_text'] ='<br>Basement<br>'.($floorConfig['basement']!='' ? $floorConfig['basement'] : 'None').'<br>Ground Floor <br>'.($floorConfig['groundFloor']!='' ? $floorConfig['groundFloor'] : 'None').'<br>First Floor<br>'.($floorConfig['firstFloor']!='' ? $floorConfig['firstFloor'] : 'None').'<br>Second Floor<br>'.($floorConfig['secondFloor']!='' ? $floorConfig['secondFloor'] : 'None');






        if ($floorConfig['groundFloortitle']==null) {
            // $floorConfig['groundFloortitle']="None";
        }
        if ($floorConfig['firstFloortitle']==null) {
            //  $floorConfig['firstFloortitle']="None";

        }
        if ($floorConfig['secondFloortitle']==null) {
            // $floorConfig['secondFloortitle']="None";
        }

        // print_r($floorConfig);
        // die();

        return $floorConfig;

    }


    //1.66 Documents Provided by Client
    public function getDocumentByClient($model){

        $document='';

        if (isset($model->property->required_documents) && ($model->property->required_documents <> null)) {
            $required_documents = explode(',', $model->property->required_documents);

            foreach ($required_documents as $required_document) {
                $attachment_DetailsAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document],['!=',"attachment",'']])->one();
                $attachment_DetailsNotAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document,"attachment"=>'']])->one();

                $attachment= null;
                if($attachment_DetailsAvail != null){
                    $attachment = $attachment_DetailsAvail->attachment;

                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentAvail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].'<br>';

                    }


                }else{
                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentNotavail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].'<br>';

                    }
                }




                /* if($attachment_DetailsNotAvail != null){
                     $attachment = $attachment_DetailsNotAvail->attachment;

                     if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                     {
                         $DocumentByClient['documentNotavail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].'<br>';

                     }

                 }*/
            }

        }

        if (isset($model->property->optional_documents) && ($model->property->optional_documents <> null)) {
            $required_documents = explode(',', $model->property->optional_documents);

            foreach ($required_documents as $required_document) {
                $attachment_DetailsAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document],['!=',"attachment",'']])->one();
                $attachment_DetailsNotAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document,"attachment"=>'']])->one();

                $attachment= null;
                if($attachment_DetailsAvail != null){
                    $attachment = $attachment_DetailsAvail->attachment;

                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentAvail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].'<br>';

                    }


                }else{
                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentNotavail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].'<br>';

                    }
                }

                /*if($attachment_DetailsNotAvail != null){
                    $attachment = $attachment_DetailsNotAvail->attachment;

                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentNotavail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].'<br>';

                    }

                }*/
            }
        }

        return $DocumentByClient;
    }

    public function getDocumentByClient_31($model){

        $document='';
        if (isset($model->property->required_documents) && ($model->property->required_documents <> null)) {
            $required_documents = explode(',', $model->property->required_documents);

            foreach ($required_documents as $required_document) {
                $attachment_DetailsAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document],['!=',"attachment",'']])->one();
                $attachment_DetailsNotAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document,"attachment"=>'']])->one();

                $attachment= null;
                if($attachment_DetailsAvail != null){
                    $attachment = $attachment_DetailsAvail->attachment;

                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentAvail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].'<br>';

                    }


                }

                if($attachment_DetailsNotAvail != null){
                    $attachment = $attachment_DetailsNotAvail->attachment;

                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentNotavail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].'<br>';

                    }

                }
            }
        }

        if (isset($model->property->optional_documents) && ($model->property->optional_documents <> null)) {
            $required_documents = explode(',', $model->property->optional_documents);

            foreach ($required_documents as $required_document) {
                $attachment_DetailsAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document],['!=',"attachment",'']])->one();
                $attachment_DetailsNotAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document,"attachment"=>'']])->one();

                $attachment= null;
                if($attachment_DetailsAvail != null){
                    $attachment = $attachment_DetailsAvail->attachment;

                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentAvail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].'<br>';

                    }


                }

                if($attachment_DetailsNotAvail != null){
                    $attachment = $attachment_DetailsNotAvail->attachment;

                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentNotavail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].'<br>';

                    }

                }
            }
        }

        return $DocumentByClient;
    }

    public function getDocumentByClientpdf($model){

        $document='';
        if (isset($model->property->required_documents) && ($model->property->required_documents <> null)) {
            $required_documents = explode(',', $model->property->required_documents);

            foreach ($required_documents as $required_document) {
                $attachment_DetailsAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document],['!=',"attachment",'']])->one();
                $attachment_DetailsNotAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document,"attachment"=>'']])->one();

                $attachment= null;
                if($attachment_DetailsAvail != null){
                    $attachment = $attachment_DetailsAvail->attachment;

                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentAvail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].', ';

                    }


                }

                if($attachment_DetailsNotAvail != null){
                    $attachment = $attachment_DetailsNotAvail->attachment;

                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentNotavail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].', ';

                    }

                }
            }
        }

        if (isset($model->property->optional_documents) && ($model->property->optional_documents <> null)) {
            $required_documents = explode(',', $model->property->optional_documents);

            foreach ($required_documents as $required_document) {
                $attachment_DetailsAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document],['!=',"attachment",'']])->one();
                $attachment_DetailsNotAvail = \app\models\ReceivedDocsFiles::find()->where(['and',["valuation_id" => $model->id, "document_id" => $required_document,"attachment"=>'']])->one();

                $attachment= null;
                if($attachment_DetailsAvail != null){
                    $attachment = $attachment_DetailsAvail->attachment;

                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentAvail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].', ';

                    }


                }

                if($attachment_DetailsNotAvail != null){
                    $attachment = $attachment_DetailsNotAvail->attachment;

                    if(Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] !=null)
                    {
                        $DocumentByClient['documentNotavail'] .=Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document].', ';

                    }

                }
            }
        }

        return $DocumentByClient;
    }


    public function getMarketRent($incomeapproch_data,$model,$costapproch_data){


        $building_info = \app\models\Buildings::find()->where(['id' => $model->building_info])->one();

        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $model->id])->one();

        $original_purchase_price['mv'] = $cost_info->original_purchase_price;
        $summary['original_purchase_price'] = $original_purchase_price;

        $summary['valuation_approach'] = Yii::$app->appHelperFunctions->valuationApproachListArr[$building_info->property->valuation_approach];
        $summary['basis_of_value'] = $building_info->property->basis_of_value;
        $summary['approach_reasoning'] = $building_info->property->approach_reason;
        $summary['market_value_of_land'] = $cost_info->lands_price;

        $summary['estimated_market_rent'] = $incomeapproch_data['estimated_market_rent'];


        $drive_margin_sold = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $model->id, 'type' => 'sold'])->one();
        $drive_margin_list = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $model->id, 'type' => 'list'])->one();
        $drive_margin_previous = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $model->id, 'type' => 'previous'])->one();



        $mv_sold['mv'] = $drive_margin_sold->mv_total_price;
        $mv_list['mv'] = $drive_margin_list->mv_total_price;
        $mv_prvious['mv'] = $drive_margin_previous->mv_total_price;
        $mv_cost['mv'] = $costapproch_data['estimated_total_cost_value'];
        $mv_income['mv'] = $incomeapproch_data['estimated_market_value'];
        $summary['approval_market_value'] = round((($mv_sold['mv'] + $mv_list['mv'] + $mv_prvious['mv'] + $mv_cost['mv'] + $mv_income['mv']) / 5), 3);

        return $summary;
    }


    Public function getReceiveDocs($model){

        $receiveDoc = \app\models\ReceivedDocs::find()->where(['valuation_id' => $model->id])->one();
        $receiveDocumentlabel = new \app\models\ReceivedDocs;
        $ReceivedDocs['dld_data_enquiry']='';
        $ReceivedDocs['kharetati_application_enquiry']='';
        $ReceivedDocs['dld_sold_transaction_enquiry']='';
        $ReceivedDocs['dld_valuations_enquir']='';
        $ReceivedDocs['discussions_real_estate']='';

        if($receiveDoc->dld_data_enquiry=='Yes'){
            $ReceivedDocs['dld_data_enquiry']='<li>'.$receiveDocumentlabel->getAttributeLabel('dld_data_enquiry').'</li>';
        }
        if($receiveDoc->kharetati_application_enquiry=='Yes'){
            $ReceivedDocs['kharetati_application_enquiry']='<li>'.$receiveDocumentlabel->getAttributeLabel('kharetati_application_enquiry').'</li>';
        }
        if($receiveDoc->dld_sold_transaction_enquiry=='Yes'){
            $ReceivedDocs['dld_sold_transaction_enquiry']='<li>'.$receiveDocumentlabel->getAttributeLabel('dld_sold_transaction_enquiry').'</li>';
        }
        if($receiveDoc->dld_valuations_enquir=='Yes'){
            $ReceivedDocs['dld_valuations_enquir']='<li>'.$receiveDocumentlabel->getAttributeLabel('dld_valuations_enquir').'</li>';
        }
        if($receiveDoc->discussions_real_estate=='Yes'){
            $ReceivedDocs['discussions_real_estate']='<li>'.$receiveDocumentlabel->getAttributeLabel('discussions_real_estate').'</li>';
        }

        return $ReceivedDocs;

    }


    public function getTransactionlist($model){


        $list_selected_records = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $model->id, 'type' => 'list','search_type'=>0])->one();

        $listing_filter = array();
        if (!empty($list_selected_records) && $list_selected_records) {

           // $listing_filter['ListingsTransactionsSearch']['date_from'] = $list_selected_records->date_from;
            $listing_filter['ListingsTransactionsSearch']['date_from'] = $list_selected_records->new_from_date;
            $listing_filter['ListingsTransactionsSearch']['date_to'] = $list_selected_records->date_to;
            $listing_filter['ListingsTransactionsSearch']['building_info_data'] = $list_selected_records->building_info;
            $listing_filter['ListingsTransactionsSearch']['property_id'] = $list_selected_records->property_id;
            $listing_filter['ListingsTransactionsSearch']['property_category'] = $list_selected_records->property_category;
            $listing_filter['ListingsTransactionsSearch']['bedroom_from'] = $list_selected_records->bedroom_from;
            $listing_filter['ListingsTransactionsSearch']['bedroom_to'] = $list_selected_records->bedroom_to;
            $listing_filter['ListingsTransactionsSearch']['landsize_from'] = $list_selected_records->landsize_from;
            $listing_filter['ListingsTransactionsSearch']['landsize_to'] = $list_selected_records->landsize_to;
            $listing_filter['ListingsTransactionsSearch']['bua_from'] = $list_selected_records->bua_from;
            $listing_filter['ListingsTransactionsSearch']['bua_to'] = $list_selected_records->bua_to;
            $listing_filter['ListingsTransactionsSearch']['price_from'] = $list_selected_records->price_from;
            $listing_filter['ListingsTransactionsSearch']['price_to'] = $list_selected_records->price_to;
            $listing_filter['ListingsTransactionsSearch']['price_psf_from'] = $list_selected_records->price_psf_from;
            $listing_filter['ListingsTransactionsSearch']['price_psf_to'] = $list_selected_records->price_psf_to;
        }




        $searchModel = new \app\models\ListingsTransactionsSearch();
        $searchModel->selectedOnly= true;
        $searchModel->valuation_id= $model->id;
        $dataProvider['Listselected'] = $searchModel->searchvaluation($listing_filter);
        $dataProvider['Listselected']->pagination=false;


        $searchModel = new \app\models\ListingsTransactionsSearch();
        $searchModel->unselectedOnly= true;
        $searchModel->valuation_id= $model->id;
        $dataProvider['Listunselected']  = $searchModel->searchvaluation($listing_filter);
        $dataProvider['Listunselected']->pagination=false;



        return $dataProvider;



    }


    public function getTransactionlistSold($model){



        $sold_selected_records = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $model->id, 'type' => 'sold','search_type'=>0])->one();

        $listing_filter = array();
        if (!empty($sold_selected_records) && $sold_selected_records) {

           // $listing_filter['SoldTransactionSearch']['date_from'] = $sold_selected_records->date_from;
            $listing_filter['SoldTransactionSearch']['date_from'] = $sold_selected_records->new_from_date;
            $listing_filter['SoldTransactionSearch']['date_to'] = $sold_selected_records->date_to;
            $listing_filter['SoldTransactionSearch']['building_info_data'] = $sold_selected_records->building_info;
            $listing_filter['SoldTransactionSearch']['property_id'] = $sold_selected_records->property_id;
            $listing_filter['SoldTransactionSearch']['property_category'] = $sold_selected_records->property_category;
            $listing_filter['SoldTransactionSearch']['bedroom_from'] = $sold_selected_records->bedroom_from;
            $listing_filter['SoldTransactionSearch']['bedroom_to'] = $sold_selected_records->bedroom_to;
            $listing_filter['SoldTransactionSearch']['landsize_from'] = $sold_selected_records->landsize_from;
            $listing_filter['SoldTransactionSearch']['landsize_to'] = $sold_selected_records->landsize_to;
            $listing_filter['SoldTransactionSearch']['bua_from'] = $sold_selected_records->bua_from;
            $listing_filter['SoldTransactionSearch']['bua_to'] = $sold_selected_records->bua_to;
            $listing_filter['SoldTransactionSearch']['price_from'] = $sold_selected_records->price_from;
            $listing_filter['SoldTransactionSearch']['price_to'] = $sold_selected_records->price_to;
            $listing_filter['SoldTransactionSearch']['price_psf_from'] = $sold_selected_records->price_psf_from;
            $listing_filter['SoldTransactionSearch']['price_psf_to'] = $sold_selected_records->price_psf_to;

        }


        $searchModel = new \app\models\SoldTransactionSearch();
        $searchModel->selectedOnly= true;
        $searchModel->valuation_id= $model->id;
        $dataProvider['Soldselected'] = $searchModel->searchvaluation($listing_filter);
        $dataProvider['Soldselected']->pagination=false;


        $searchModel = new \app\models\SoldTransactionSearch();
        $searchModel->unselectedOnly= true;
        $searchModel->valuation_id= $model->id;
        $dataProvider['Soldunselected'] = $searchModel->searchvaluation($listing_filter);
        $dataProvider['Soldunselected']->pagination=false;


        return $dataProvider;
    }


    public function getTransactionlistType($model,$stype = 0){


        $list_selected_records = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $model->id, 'type' => 'list','search_type'=>0,'income_type' => $stype])->one();

        $listing_filter = array();
        if (!empty($list_selected_records) && $list_selected_records) {

            // $listing_filter['ListingsTransactionsSearch']['date_from'] = $list_selected_records->date_from;
            $listing_filter['ListingsRentSearch']['date_from'] = $list_selected_records->new_from_date;
            $listing_filter['ListingsRentSearch']['date_to'] = $list_selected_records->date_to;
            $listing_filter['ListingsRentSearch']['building_info_data'] = $list_selected_records->building_info;
            $listing_filter['ListingsRentSearch']['property_id'] = $list_selected_records->property_id;
            $listing_filter['ListingsRentSearch']['property_category'] = $list_selected_records->property_category;
            $listing_filter['ListingsRentSearch']['bedroom_from'] = $list_selected_records->bedroom_from;
            $listing_filter['ListingsRentSearch']['bedroom_to'] = $list_selected_records->bedroom_to;
            $listing_filter['ListingsRentSearch']['landsize_from'] = $list_selected_records->landsize_from;
            $listing_filter['ListingsRentSearch']['landsize_to'] = $list_selected_records->landsize_to;
            $listing_filter['ListingsRentSearch']['bua_from'] = $list_selected_records->bua_from;
            $listing_filter['ListingsRentSearch']['bua_to'] = $list_selected_records->bua_to;
            $listing_filter['ListingsRentSearch']['price_from'] = $list_selected_records->price_from;
            $listing_filter['ListingsRentSearch']['price_to'] = $list_selected_records->price_to;
            $listing_filter['ListingsRentSearch']['price_psf_from'] = $list_selected_records->price_psf_from;
            $listing_filter['ListingsRentSearch']['price_psf_to'] = $list_selected_records->price_psf_to;
        }

        if($stype == 1){
            $listing_filter['ListingsRentSearch']['auto_list_type'] = 'apartment';
            $listing_filter['ListingsRentSearch']['bedroom_from'] = 0;
            $listing_filter['ListingsRentSearch']['bedroom_to'] = 0;
        }else if($stype == 2){
            $listing_filter['ListingsRentSearch']['auto_list_type'] = 'apartment';
            $listing_filter['ListingsRentSearch']['bedroom_from'] = 1;
            $listing_filter['ListingsRentSearch']['bedroom_to'] = 1;
        }else if($stype == 3){
            $listing_filter['ListingsRentSearch']['auto_list_type'] = 'apartment';
            $listing_filter['ListingsRentSearch']['bedroom_from'] = 2;
            $listing_filter['ListingsRentSearch']['bedroom_to'] = 2;
        }else if($stype == 4){

            $listing_filter['ListingsRentSearch']['auto_list_type'] = 'apartment';
            $listing_filter['ListingsRentSearch']['bedroom_from'] = 3;
            $listing_filter['ListingsRentSearch']['bedroom_to'] = 3;
        }else if($stype == 5){

            $listing_filter['ListingsRentSearch']['auto_list_type'] = 'apartment';
            $listing_filter['ListingsRentSearch']['bedroom_from'] = 4;
            $listing_filter['ListingsRentSearch']['bedroom_to'] = 4;
        }else if($stype == 6){

            $listing_filter['ListingsRentSearch']['auto_list_type'] =  array('apartment','penthouse');
            $listing_filter['ListingsRentSearch']['bedroom_from'] = 2;
            $listing_filter['ListingsRentSearch']['bedroom_to'] = 10;
        }else if($stype == 7){

            $listing_filter['ListingsRentSearch']['auto_list_type'] =  'shop';
            $listing_filter['ListingsRentSearch']['bedroom_from'] = '';
            $listing_filter['ListingsRentSearch']['bedroom_to'] = '';
        }else if($stype == 8){

            $listing_filter['ListingsRentSearch']['auto_list_type'] =  'office';
            $listing_filter['ListingsRentSearch']['bedroom_from'] = 0;
            $listing_filter['ListingsRentSearch']['bedroom_to'] = 0;
        }else if($stype == 9){

            $listing_filter['ListingsRentSearch']['auto_list_type'] =  'warehouse';
            $listing_filter['ListingsRentSearch']['bedroom_from'] = 0;
            $listing_filter['ListingsRentSearch']['bedroom_to'] = 0;
        }




        $searchModel = new \app\models\ListingsRentSearch();
        $searchModel->selectedOnly= true;
        $searchModel->valuation_id= $model->id;
        $dataProvider['Listselected'] = $searchModel->searchvaluation($listing_filter);
        $dataProvider['Listselected']->pagination=false;


        $searchModel = new \app\models\ListingsRentSearch();
        $searchModel->unselectedOnly= true;
        $searchModel->valuation_id= $model->id;
        $dataProvider['Listunselected']  = $searchModel->searchvaluation($listing_filter);
        $dataProvider['Listunselected']->pagination=false;



        return $dataProvider;



    }

    public function getTransactionlistSoldType($model, $stype = 0){



        $sold_selected_records = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $model->id, 'type' => 'sold','search_type'=>0,'income_type' => $stype])->one();

        $listing_filter = array();
        if (!empty($sold_selected_records) && $sold_selected_records) {

            // $listing_filter['SoldTransactionSearch']['date_from'] = $sold_selected_records->date_from;
            $listing_filter['SoldTransactionRentsSearch']['date_from'] = $sold_selected_records->new_from_date;
            $listing_filter['SoldTransactionRentsSearch']['date_to'] = $sold_selected_records->date_to;
            $listing_filter['SoldTransactionRentsSearch']['building_info_data'] = $sold_selected_records->building_info;
            $listing_filter['SoldTransactionRentsSearch']['property_id'] = $sold_selected_records->property_id;
            $listing_filter['SoldTransactionRentsSearch']['property_category'] = $sold_selected_records->property_category;
            $listing_filter['SoldTransactionRentsSearch']['bedroom_from'] = $sold_selected_records->bedroom_from;
            $listing_filter['SoldTransactionRentsSearch']['bedroom_to'] = $sold_selected_records->bedroom_to;
            $listing_filter['SoldTransactionRentsSearch']['landsize_from'] = $sold_selected_records->landsize_from;
            $listing_filter['SoldTransactionRentsSearch']['landsize_to'] = $sold_selected_records->landsize_to;
            $listing_filter['SoldTransactionRentsSearch']['bua_from'] = $sold_selected_records->bua_from;
            $listing_filter['SoldTransactionRentsSearch']['bua_to'] = $sold_selected_records->bua_to;
            $listing_filter['SoldTransactionRentsSearch']['price_from'] = $sold_selected_records->price_from;
            $listing_filter['SoldTransactionRentsSearch']['price_to'] = $sold_selected_records->price_to;
            $listing_filter['SoldTransactionRentsSearch']['price_psf_from'] = $sold_selected_records->price_psf_from;
            $listing_filter['SoldTransactionRentsSearch']['price_psf_to'] = $sold_selected_records->price_psf_to;

        }

        if($stype == 1){
            $listing_filter['SoldTransactionRentsSearch']['auto_list_type'] = 'Apartment';
            $listing_filter['SoldTransactionRentsSearch']['bedroom_from'] = 0;
            $listing_filter['SoldTransactionRentsSearch']['bedroom_to'] = 0;
        }else if($stype == 2){

            $listing_filter['SoldTransactionRentsSearch']['auto_list_type'] = 'Apartment';
            $listing_filter['SoldTransactionRentsSearch']['bedroom_from'] = 1;
            $listing_filter['SoldTransactionRentsSearch']['bedroom_to'] = 1;
        }else if($stype == 3){
            $listing_filter['SoldTransactionRentsSearch']['auto_list_type'] = 'Apartment';
            $listing_filter['SoldTransactionRentsSearch']['bedroom_from'] = 2;
            $listing_filter['SoldTransactionRentsSearch']['bedroom_to'] = 2;
        }else if($stype == 4){

            $listing_filter['SoldTransactionRentsSearch']['auto_list_type'] = 'Apartment';
            $listing_filter['SoldTransactionRentsSearch']['bedroom_from'] = 3;
            $listing_filter['SoldTransactionRentsSearch']['bedroom_to'] = 3;
        }else if($stype == 5){

            $listing_filter['SoldTransactionRentsSearch']['auto_list_type'] = 'Apartment';
            $listing_filter['SoldTransactionRentsSearch']['bedroom_from'] = 4;
            $listing_filter['SoldTransactionRentsSearch']['bedroom_to'] = 4;
        }else if($stype == 6){
            $listing_filter['SoldTransactionRentsSearch']['auto_list_type'] = array('Apartment','Penthouse');
            $listing_filter['SoldTransactionRentsSearch']['bedroom_from'] = 2;
            $listing_filter['SoldTransactionRentsSearch']['bedroom_to'] = 10;
        }else if($stype == 7){
            $listing_filter['SoldTransactionRentsSearch']['auto_list_type'] ='Shop';
            $listing_filter['SoldTransactionRentsSearch']['bedroom_from'] = '';
            $listing_filter['SoldTransactionRentsSearch']['bedroom_to'] = '';
        }else if($stype == 8){
            $listing_filter['SoldTransactionRentsSearch']['auto_list_type'] ='Office';
            $listing_filter['SoldTransactionRentsSearch']['bedroom_from'] = '';
            $listing_filter['SoldTransactionRentsSearch']['bedroom_to'] = '';
        }else if($stype == 9){
            $listing_filter['SoldTransactionRentsSearch']['auto_list_type'] ='Industrial-Warehouse';
            $listing_filter['SoldTransactionRentsSearch']['bedroom_from'] = '';
            $listing_filter['SoldTransactionRentsSearch']['bedroom_to'] = '';
        }


        $searchModel = new \app\models\SoldTransactionRentsSearch();
        $searchModel->selectedOnly= true;
        $searchModel->valuation_id= $model->id;

        $dataProvider['Soldselected'] = $searchModel->searchvaluation($listing_filter);
        $dataProvider['Soldselected']->pagination=false;


        $searchModel = new \app\models\SoldTransactionRentsSearch();
        $searchModel->unselectedOnly= true;
        $searchModel->valuation_id= $model->id;
        $dataProvider['Soldunselected'] = $searchModel->searchvaluation($listing_filter);
        $dataProvider['Soldunselected']->pagination=false;


        return $dataProvider;
    }













}
?>
