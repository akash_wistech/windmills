<?php
namespace app\components\helpers;
use Yii;
use yii\base\Component;
use app\models\Valuation;
use app\models\Company;
use app\models\Buildings;

class ClientHelperFunctions extends Component 
{
    public function getParentQuery($time_period=null)
    {
        $query = Valuation::find();
        if($time_period == 1)
        {
            $query->select([
                'client_revenue' => 'SUM('.Valuation::tableName().'.fee)',
                'month_number'   => 'MONTH('.Valuation::tableName().'.submission_approver_date)'
            ]);  
        }
        if($time_period == 2)
        {
            $query->select([
                'quarter'=>'QUARTER('.Valuation::tableName().'.submission_approver_date)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.fee)',
            ]);
        }
        if($time_period == 3)
        {
            $query->select([
                'half_year' => 'IF(MONTH('.Valuation::tableName().'.submission_approver_date) >= 1 AND MONTH('.Valuation::tableName().'.submission_approver_date) <= 6, 1, 2)', 
                'client_revenue'=>'SUM('.Valuation::tableName().'.fee)',
            ]);
        }
        if($time_period == 4)
        {
            $query->select([
                'year' => 'YEAR('.Valuation::tableName().'.submission_approver_date)', 
                'client_revenue'=>'SUM('.Valuation::tableName().'.fee)',
            ]);
        }
        $query->from(Valuation::tableName());
        $query->leftJoin(Company::tableName(), Valuation::tableName().'.client_id='.Company::tableName().'.id');
        $query->where([Valuation::tableName().'.valuation_status' => 5]);
        $query->andWhere([Company::tableName().'.client_type' => 'bank']);
        return $query;
    }
    
    
    
    
    public function monthlyRevenue($time_period=null, $year=null, $client_id=null)
    {
        $revenueArr = array();
        
        $query = $this->getParentQuery($time_period);
        $query->andWhere('YEAR('.Valuation::tableName().'.submission_approver_date) = '.$year.'');
        if(isset($client_id) && $client_id<>null){
            $query->andWhere([Valuation::tableName().'.client_id' => $client_id]);
        }
        $query->groupBy('month_number');
        $query = $query->all();
        
        foreach ($query as $key => $row) {
            $revenueArr[$row->month_number] = $row->client_revenue;
        }
        
        return $revenueArr;
    }
    
    
    
    
    
    
    public function getQuarterlyRevenue($time_period=null, $year=null, $client_id=null)
    {
        $revenueArr = array();
        
        $query = $this->getParentQuery($time_period);
        $query->andWhere('YEAR(valuation.submission_approver_date) = '.$year.'');
        if(isset($client_id) && $client_id<>null){
            $query->andWhere([Valuation::tableName().'.client_id' => $client_id]);
        }
        $query->groupBy(['QUARTER('.Valuation::tableName().'.submission_approver_date)']);
        $query = $query->all();
        
        foreach($query as $key=>$value)
        {
            $revenueArr[$value['quarter']] = $value['client_revenue'];
        }
        
        return $revenueArr;
    }
    
    
    public function getHalfYearlyRevenue($time_period=null, $year=null, $client_id=null)
    {
        $revenueArr = array();
        $half_years = [1, 2];
        
        $query = $this->getParentQuery($time_period);
        $query->andWhere('YEAR('.Valuation::tableName().'.submission_approver_date) = '.$year.'');
        $query->andWhere(['IF(MONTH('.Valuation::tableName().'.submission_approver_date) >= 1 AND MONTH('.Valuation::tableName().'.submission_approver_date) <= 6, 1, 2)' => $half_years]);
        if(isset($client_id) && $client_id<>null){
            $query->andWhere([Valuation::tableName().'.client_id' => $client_id]);
        }
        
        $query->groupBy('half_year');
        $query = $query->all();
        
        foreach($query as $key=>$value)
        {
            $revenueArr[$value['half_year']] = $value['client_revenue'];
        }
        
        return $revenueArr;
    }
    
    
    public function getYearlyRevenue($time_period=null, $year=null, $client_id=null)
    {
        $revenueArr = array();
        
        $query = $this->getParentQuery($time_period);
        if(isset($client_id) && $client_id<>null){
            $query->andWhere([Valuation::tableName().'.client_id' => $client_id]);
        }
        $query->groupBy('year');
        $query->orderBy(['year' => SORT_DESC]);
        $query = $query->all();

        foreach($query as $key=>$value)
        {
            $revenueArr[$value['year']] = $value['client_revenue'];
        }
        
        return $revenueArr;
    }
    
    
    
    
    
    
    public function totalRevenue($time_period=null, $year=null)
    {
        $query = Valuation::find();
        $query->select([
            'client_revenue'=>'SUM('.Valuation::tableName().'.fee)',
        ]);
        $query->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id');
        $query->where([Company::tableName().'.client_type' => 'bank']);
        $query->andWhere([Valuation::tableName().'.valuation_status' => 5]);
        if ($time_period == 1 || $time_period == 2 || $time_period == 3){
            $query->andWhere('YEAR('.Valuation::tableName().'.submission_approver_date) = '.$year.'');
        }
        $query = $query->one();
        
        if($query->client_revenue<>null){
            return $query->client_revenue;
        }
        return null;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    public function totalClients()
    {     
        $query = Valuation::find();
        $query->select([
            Valuation::tableName().'.id',
            Valuation::tableName().'.client_id',
        ]);
        $query->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id');
        $query->where([Company::tableName().'.client_type' => 'bank']);
        $query->groupBy(Valuation::tableName().'.client_id');
        $query = $query->count();
        return $query;
    }
}

