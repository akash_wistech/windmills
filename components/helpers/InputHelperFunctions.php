<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use yii\helpers\Url;
use app\models\CustomField;
use app\models\CustomFieldSubOption;
use app\models\PredefinedList;
use app\models\Country;
use app\models\Zone;
use app\models\CustomFieldData;
use app\models\CustomFieldDataMultiple;
use app\models\CustomFieldDatetimeInfo;
use app\models\CustomFieldGoogleMapInfo;
use app\models\CustomFieldModule;
use app\models\Tags;
use app\models\ModuleTag;
use app\models\ModuleManager;
use app\models\ModuleEmail;
use app\models\ModuleNumber;
use app\models\ModuleCompany;
use app\models\ModuleAddress;
use yii\helpers\ArrayHelper;


class InputHelperFunctions extends Component
{
	/**
	 * array of input types
   * @return array
	 */
	public function getInputOptions()
	{
		return [
			'text' => ['icon'=>'icon-text','title'=>Yii::t('app','Text Field')],
			'autocomplete' => ['icon'=>'icon-autocomplete','title'=>Yii::t('app','Autocomplete')],
			'textarea' => ['icon'=>'icon-textarea','title'=>Yii::t('app','Text Area')],
			'tinymce' => ['icon'=>'icon-rich-text','title'=>Yii::t('app','Text Editor')],

			//'qtyField' => ['icon'=>'icon-number','title'=>Yii::t('app','Qty')],

			'select' => ['icon'=>'icon-select','title'=>Yii::t('app','Drop Down')],
			'checkbox' => ['icon'=>'icon-checkbox-group','title'=>Yii::t('app','Checkbox')],
			'radio' => ['icon'=>'icon-radio-group','title'=>Yii::t('app','Radio Buttons')],

			'numberinput' => ['icon'=>'icon-number','title'=>Yii::t('app','Number Input')],
			'websiteinput' => ['icon'=>'icon2-link','title'=>Yii::t('app','Url Input')],
			'emailinput' => ['icon'=>'icon2-envelop','title'=>Yii::t('app','Email Input')],
			'date' => ['icon'=>'icon-date','title'=>Yii::t('app','Date Input')],
			'daterange' => ['icon'=>'icon-date','title'=>Yii::t('app','Date Range Input')],
			'time' => ['icon'=>'icon2-clock','title'=>Yii::t('app','Time Input')],
			'timerange' => ['icon'=>'icon2-clock','title'=>Yii::t('app','Time Range Input')],
			//'imagedropzone' => ['icon'=>'icon-file','title'=>Yii::t('app','Multiple Image Upload')],
			//'filedropzone' => ['icon'=>'icon-file','title'=>Yii::t('app','Multiple Files Upload')],

			'googlemap' => ['icon'=>'icon2-location','title'=>Yii::t('app','Google Map')],
		];
	}

	/**
	 * array of input types with key value pair
   * @return array
	 */
	public function getDropDownList()
	{
		$arr=[];
		foreach($this->inputOptions as $key=>$val){
			$arr[$key]=$val['title'];
		}
		return $arr;
	}

	/**
	 * array of selection types
   * @return array
	 */
	public function getSelectTypeArr()
	{
		return [
			'1'=>Yii::t('app','Single'),
			'2'=>Yii::t('app','Multiple'),
		];
	}

	/**
	 * generate selection list for dropdown, checkbox and radio
   * @return array
	 */
	public function getInputListArr($inputType)
	{
		if($inputType['predefined_link']!=null){
			return Yii::$app->appHelperFunctions->getPredefinedListOptionsArr($inputType['predefined_link']);
		}else{
			return $this->getInputSubOptionListArr($inputType['id']);
		}
	}

	/**
	 * generate selection list for dropdown, checkbox and radio
   * @return array
	 */
	public function getInputSubOptionList($id)
	{
		return CustomFieldSubOption::find()
		->select(['id','title'])
		->where(['custom_field_id' => $id, 'status' => 1, 'trashed' => 0])
		->orderBy(['sort_order' => SORT_ASC])->asArray()->all();
	}

	/**
	 * generate selection list for dropdown, checkbox and radio
   * @return array
	 */
	public function getInputSubOptionListArr($id)
	{
		return ArrayHelper::map($this->getInputSubOptionList($id), "id", "title");
	}

	/**
	 * get a sub option by its id
   * @return array
	 */
	public function getInputSubOption($id)
	{
		return CustomFieldSubOption::find()
		->select(['id','title'])
		->where(['id' => $id, 'status' => 1, 'trashed' => 0])
		->asArray()->one();
	}

	public function getInputTypesByModule($type)
	{
		return CustomField::find()
    ->select([
      CustomField::tableName().'.id',
      CustomField::tableName().'.input_type',
      CustomField::tableName().'.title',
      CustomField::tableName().'.colum_width',
      CustomField::tableName().'.hint_text',
      CustomField::tableName().'.autocomplete_text',
      CustomField::tableName().'.autocomplete_link',
      CustomField::tableName().'.predefined_link',
      CustomField::tableName().'.selection_type',
      CustomField::tableName().'.is_required',
    ])
    ->innerJoin(CustomFieldModule::tableName(),CustomFieldModule::tableName().".custom_field_id=".CustomField::tableName().".id")
    ->where([
      CustomFieldModule::tableName().'.module_type'=>$type,
      CustomField::tableName().'.status'=>1,
      CustomField::tableName().'.trashed'=>0
    ])
    ->orderBY(['sort_order'=>SORT_ASC])->asArray()->all();
	}

	/**
	 * Save custom fields data
   * @return boolean
	 */
	public function saveCustomField($model)
	{
		$customInputFields=$customInputFields=Yii::$app->inputHelperFunctions->getInputTypesByModule($model->moduleTypeId);
    if($customInputFields!=null){
      foreach($customInputFields as $customField){
        //Save Simple Data
        $fieldValue=CustomFieldData::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->one();
        if($fieldValue==null){
          $fieldValue=new CustomFieldData;
          $fieldValue->module_type=$model->moduleTypeId;
          $fieldValue->module_id=$model->id;
          $fieldValue->input_field_id=$customField['id'];
        }
        if($customField['input_type']=='autocomplete'){
          $fieldValue->input_value=isset($model->input_field[$customField['id']]['title']) ? $model->input_field[$customField['id']]['title'] : '';
          $fieldValue->input_org_value=isset($model->input_field[$customField['id']]['id']) ? $model->input_field[$customField['id']]['id'] : '';
        }else{
					if(isset($model->input_field[$customField['id']]) && !is_array($model->input_field[$customField['id']])){
						$fieldValue->input_value=trim($model->input_field[$customField['id']]);
					}
        }
        if($customField['input_type']=='time'){
          $fieldValue->input_org_value=$this->getOriginalTime($fieldValue->input_value);
        }
        if($customField['input_type']=='select' || $customField['input_type']=='radio' || $customField['input_type']=='checkbox'){
          $fieldValue->input_org_value=$this->getOptionTitleById($fieldValue->input_value,$customField,$model);
        }
        $fieldValue->save();

        //Save Select Multiple values
        if(($customField['input_type']=='select' && $customField['selection_type']==2) || $customField['input_type']=='checkbox'){
					if(isset($model->input_field[$customField['id']]) && $model->input_field[$customField['id']]!=null){
						CustomFieldDataMultiple::deleteAll(['and', ['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']], ['not in', 'option_id', $model->input_field[$customField['id']]]]);
						foreach($model->input_field[$customField['id']] as $key=>$val){
							$childRow=CustomFieldDataMultiple::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id'],'option_id'=>$val])->one();
							if($childRow!=null){
								$childRow=new CustomFieldDataMultiple;
								$childRow->module_type=$model->moduleTypeId;
								$childRow->module_id=$model->id;
								$childRow->input_field_id=$customField['id'];
							}
							$childRow->option_id=$val;
							$childRow->option_id_value=$this->getOptionTitleById($val,$customField,$model);
							$childRow->save();
						}
					}
        }
        //Save Date Time Info
        if($customField['input_type']=='timerange'){
          $dimensionValue=CustomFieldDatetimeInfo::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->one();
          if($dimensionValue==null){
            $dimensionValue=new CustomFieldDatetimeInfo;
            $dimensionValue->module_type=$model->moduleTypeId;
            $dimensionValue->module_id=$model->id;
            $dimensionValue->input_field_id=$customField['id'];
          }
          $dimensionValue->start_time=isset($model->input_field[$customField['id']]['start']) ? $model->input_field[$customField['id']]['start'] : '';
          $dimensionValue->start_org_time=$this->getOriginalTime($dimensionValue->start_time);
          $dimensionValue->end_time=isset($model->input_field[$customField['id']]['end']) ? $model->input_field[$customField['id']]['end'] : '';
          $dimensionValue->end_org_time=$this->getOriginalTime($dimensionValue->end_time);
          $dimensionValue->save();
        }

        //Save Google Map
        if($customField['input_type']=='googlemap'){
          $googleMap=CustomFieldGoogleMapInfo::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->one();
          if($googleMap==null){
            $googleMap=new CustomFieldGoogleMapInfo;
            $googleMap->module_type=$model->moduleTypeId;
            $googleMap->module_id=$model->id;
            $googleMap->input_field_id=$customField['id'];
          }
          $googleMap->map_location=isset($model->map_location) ? $model->map_location : '';
          $googleMap->map_lat=isset($model->map_lat) ? $model->map_lat : '';
          $googleMap->map_lng=isset($model->map_lng) ? $model->map_lng : '';
          $googleMap->save();
        }
      }
    }
	}

	/**
	 * Validates custom fields
   * @return boolean
	 */
	public function validateRequired($model)
	{
		$requiredValidator = new yii\validators\RequiredValidator();
		$integerValidator = new yii\validators\NumberValidator();
		$integerValidator->integerOnly=true;

		$customInputFields=Yii::$app->inputHelperFunctions->getInputTypesByModule($model->moduleTypeId);
		foreach($customInputFields as $customField){
			//Required Validation
			if($customField['is_required']==1){
				if(isset($this->input_field[$customField['id']])){
					if(!$requiredValidator->validate($this->input_field[$customField['id']], $error)){
						$this->addError($this->input_field[$customField['id']], $customField['title'].' is required');
						return false;
					}
				}
			}
			//Integer Validation
			if($customField['input_type']=='numberinput'){
				if(isset($this->input_field[$customField['id']])){
					if(!$integerValidator->validate($this->input_field[$customField['id']], $error)){
						$this->addError($this->input_field[$customField['id']], $customField['title'].' should only be digits');
						return false;
					}
				}
			}
		}
	}

	/**
	 * formats time for db
   * @return string
	 */
	public function getOriginalTime($input_time)
	{
		return date("H:i",strtotime($input_time));
	}

	/**
	 * get title of record by its id
   * @return string
	 */
	public function getOptionTitleById($id,$customField)
	{
		if($customField['autocomplete_link']!=''){
			$row=Yii::$app->appHelperFunctions->getPredefinedListOption($id);
			return $row!=null ? $row['title'] : '';
		}elseif($customField['predefined_link']!=''){
			$row=Yii::$app->appHelperFunctions->getPredefinedListOption($id);
			return $row!=null ? $row['title'] : '';
		}else{
			$row=$this->getInputSubOption($id);
			return $row!=null ? $row['title'] : '';
		}
	}

	/**
	 * get saved value of input
   * @return string
	 */
	public function getInputFielSavedValue($model,$customField)
	{
		$value='';
		if(
			$customField['input_type']=='text' ||
			$customField['input_type']=='autocomplete' ||
			$customField['input_type']=='numberinput' ||
			$customField['input_type']=='websiteinput' ||
			$customField['input_type']=='emailinput' ||
			$customField['input_type']=='date' ||
			$customField['input_type']=='daterange' ||
			$customField['input_type']=='time' ||
			$customField['input_type']=='textarea' ||
			$customField['input_type']=='tinymce' ||
			$customField['input_type']=='radio'
		){
			$valueRow=CustomFieldData::find()->asArray()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->one();
			if($valueRow!=null){
				if($customField['input_type']=='autocomplete'){
					$model->input_field[$customField['id']]['id']=$valueRow['input_org_value'];
					$model->input_field[$customField['id']]['title']=$valueRow['input_value'];
				}else{
					$model->input_field[$customField['id']]=$valueRow['input_value'];
				}
			}
		}
		//Select & Checkbox
		if($customField['input_type']=='select' || $customField['input_type']=='checkbox'){
			if(($customField['input_type']=='select' && $customField['selection_type']==2) || $customField['input_type']=='checkbox'){
				$model->input_field[$customField['id']]=ArrayHelper::map(CustomFieldDataMultiple::find()->select(['option_id'])->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->asArray()->all(),"option_id","option_id");
			}else{
				$valueRow=CustomFieldData::find()->asArray()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->one();
				if($valueRow!=null){
					$model->input_field[$customField['id']]=$valueRow['input_value'];
				}
			}
		}
		if($customField['input_type']=='timerange'){
			$valueRow=CustomFieldDatetimeInfo::find()->asArray()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->one();
			if($valueRow!=null){
				$model->input_field[$customField['id']]['start']=$valueRow['start_time'];
				$model->input_field[$customField['id']]['end']=$valueRow['end_time'];
			}
		}
		return $value;
	}

	/**
	 * get saved value of input for detail
   * @return string
	 */
	public function getInputFielSavedValueForDetail($model,$customField)
	{
		$value='';
		if(
			$customField['input_type']=='text' ||
			$customField['input_type']=='autocomplete' ||
			$customField['input_type']=='numberinput' ||
			$customField['input_type']=='websiteinput' ||
			$customField['input_type']=='emailinput' ||
			$customField['input_type']=='date' ||
			$customField['input_type']=='daterange' ||
			$customField['input_type']=='time' ||
			$customField['input_type']=='textarea' ||
			$customField['input_type']=='tinymce' ||
			$customField['input_type']=='radio'
		){
			$valueRow=CustomFieldData::find()->asArray()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->one();
			if($valueRow!=null){
				if($customField['input_type']=='autocomplete'){
					$value=$valueRow['input_value'];
				}else{
					if($valueRow['input_value']!=''){
						if($customField['input_type']=='date'){
							$value=Yii::$app->formatter->asDate($valueRow['input_value']);
						}elseif($customField['input_type']=='daterange'){
							list($from,$to)=explode(" - ",$valueRow['input_value']);
							$value=Yii::t('app','{from} - {to}',['from'=>Yii::$app->formatter->asDate($from),'to'=>Yii::$app->formatter->asDate($to)]);
						}elseif($customField['input_type']=='time'){
							$value=date("h:i a",strtotime($valueRow['input_value']));
						}elseif($customField['input_type']=='textarea'){
							$value=nl2br($valueRow['input_value']);
						}else{
							$value=$valueRow['input_value'];
						}
					}
				}
			}
		}
		//Select & Checkbox
		if($customField['input_type']=='select' || $customField['input_type']=='checkbox'){
			if(($customField['input_type']=='select' && $customField['selection_type']==2) || $customField['input_type']=='checkbox'){
				$multipleDataRows=CustomFieldDataMultiple::find()->select(['option_id'])->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->asArray()->all();
				if($multipleDataRows!=null){
					foreach($multipleDataRows as $multipleDataRow){
						$value.=($value!='' ? ', ' : '').$this->getOptionTitleById($multipleDataRow['option_id'],$customField);
					}
				}
			}else{
				$valueRow=CustomFieldData::find()->asArray()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->one();
				if($valueRow!=null){
					$value=$this->getOptionTitleById($valueRow['input_value'],$customField);
				}
			}
		}
		if($customField['input_type']=='radio'){
			$valueRow=CustomFieldData::find()->asArray()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->one();
			if($valueRow!=null){
				$value=$this->getOptionTitleById($valueRow['input_value'],$customField);
			}
		}
		if($customField['input_type']=='timerange'){
			$valueRow=CustomFieldDatetimeInfo::find()->asArray()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->one();
			if($valueRow!=null){
				if($valueRow['start_time']!='')$value=date("h:i a",strtotime($valueRow['start_time']));
				if($valueRow['start_time']!='' && $valueRow['end_time']!='')$value.=' - ';
				if($valueRow['end_time']!='')$value.=date("h:i a",strtotime($valueRow['end_time']));
			}
		}
		return $value;
	}

	/**
	 * save emails & phone numbers
	 */
	public function saveMultiEmailNumbers($model)
	{
		if($model->emails!=null){
			ModuleEmail::deleteAll([
				'and',
				['module_type'=>$model->moduleTypeId,'module_id'=>$model->id],
				['not in','email',$model->emails]
			]);
			foreach($model->emails as $key=>$val){
				if($val!=''){
					$childRow=ModuleEmail::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'email'=>$val]);
					if(!$childRow->exists()){
						$childRow=new ModuleEmail;
						$childRow->module_type=$model->moduleTypeId;
						$childRow->module_id=$model->id;
						$childRow->email=$val;
						$childRow->save();
					}
				}
			}
		}
		if($model->phone_numbers!=null){
			ModuleNumber::deleteAll([
				'and',
				['module_type'=>$model->moduleTypeId,'module_id'=>$model->id],
				['not in','phone',$model->phone_numbers]
			]);
			foreach($model->phone_numbers as $key=>$val){
				if($val!=''){
					$childRow=ModuleNumber::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'phone'=>$val]);
					if(!$childRow->exists()){
						$childRow=new ModuleNumber;
						$childRow->module_type=$model->moduleTypeId;
						$childRow->module_id=$model->id;
						$childRow->phone=$val;
	  				$childRow->save();
					}
				}
			}
		}
	}

	/**
	 * save multiple address
	 */
	public function saveMultiCompanies($model)
	{
		if($model->comp_id!=null && count($model->comp_id)>0){
			$jrListId=Yii::$app->appHelperFunctions->getSetting('role_list_id');
			$jtListId=Yii::$app->appHelperFunctions->getSetting('job_title_list_id');
			foreach($model->comp_id as $key=>$val){
				if($val>0){
					$companyId=$model->comp_id[$key];
					$roleId=$model->comp_role_id[$key];
					$jobRole=$model->comp_role[$key];
					$jobTitleId=$model->comp_job_title_id[$key];
					$jobTitle=$model->comp_job_title[$key];
					$isPrimary=isset($model->is_primary[$key]) ? $model->is_primary[$key] : 0;

					//Save New Job Role
					$jobRoleRow=PredefinedList::find()->where(['parent'=>$jrListId,'title'=>trim($jobRole)])->one();
		      if($jobRoleRow==null){
		        $jobRoleRow=new PredefinedList;
		        $jobRoleRow->parent=$jrListId;
		        $jobRoleRow->title=$jobRole;
		        $jobRoleRow->status=1;
		        if($jobRoleRow->save()){
		          $roleId=$jobRoleRow->id;
		        }
		      }else{
		        $roleId=$jobRoleRow->id;
		      }

					//Save New Job Title
					$jobTitleRow=PredefinedList::find()->where(['parent'=>$jtListId,'title'=>trim($jobTitle)])->one();
		      if($jobTitleRow==null){
		        $jobTitleRow=new PredefinedList;
		        $jobTitleRow->parent=$jtListId;
		        $jobTitleRow->title=$jobTitle;
		        $jobTitleRow->status=1;
		        if($jobTitleRow->save()){
		          $jobTitleId=$jobTitleRow->id;
		        }
		      }else{
		        $jobTitleId=$jobTitleRow->id;
		      }

					if(isset($model->comp_row_id[$key])){
						$rowId=$model->comp_row_id[$key];
						$childRow=ModuleCompany::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'id'=>$rowId])->one();
					}else{
						$childRow=new ModuleCompany;
						$childRow->module_type=$model->moduleTypeId;
						$childRow->module_id=$model->id;
					}

					$childRow->company_id=$companyId;
					$childRow->role_id=$roleId;
					$childRow->job_title_id=$jobTitleId;
					$childRow->is_primary=$isPrimary;
					if(!$childRow->save()){
						echo 'Error';
						print_r($childRow->getErrors());
						die();
					}
				}
			}
		}
	}

	/**
	 * save multiple address
	 */
	public function saveMultiAddress($model)
	{
		// echo '<pre>';
		// print_r($model->add_is_primary);
		// echo '</pre>';
		// die();
		if($model->add_country_id!=null){
			$primaryId=0;
			$primaryId=$model->add_is_primary;
			foreach($model->add_country_id as $key=>$val){
				$isPrimary=0;
				// if(isset($model->add_is_primary)){
				// 	$primaryId=$model->add_is_primary[$key];
				// }
				$isPrimary=($primaryId==$key ? 1 : 0);
				$countryId=$model->add_country_id[$key];
				$zoneId=$model->add_zone_id[$key];
				$addCity=$model->add_city[$key];
				$addPhone=$model->add_phone[$key];
				$addAddress=$model->add_address[$key];
				if(isset($model->add_id[$key])){
					$addressId=$model->add_id[$key];
					$childRow=ModuleAddress::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'id'=>$addressId])->one();
				}else{
					$childRow=new ModuleAddress;
					$childRow->module_type=$model->moduleTypeId;
					$childRow->module_id=$model->id;
				}
				$childRow->is_primary=$isPrimary;
				$childRow->country_id=$countryId;
				$childRow->zone_id=$zoneId;
				$childRow->city=$addCity;
				$childRow->phone=$addPhone;
				$childRow->address=$addAddress;
				$childRow->save();
			}
		}
	}

	/**
	 * save multiple tags
	 */
	public function saveTags($model)
	{
		if($model->tags!=null){
      $tags=explode(",",$model->tags);
			$tagIds=[];
			foreach($tags as $key=>$val){
				$tagRow=Tags::find()->where(['title'=>$val])->one();
				if($tagRow==null){
					$tagRow=new Tags;
					$tagRow->title=$val;
  				$tagRow->save();
				}
				$tagIds[]=$tagRow->id;
			}
			ModuleTag::deleteAll([
				'and',
				['module_type'=>$model->moduleTypeId,'module_id'=>$model->id],
				['not in','tag_id',$tagIds]
			]);
			foreach($tagIds as $key=>$val){
				$childRow=ModuleTag::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'tag_id'=>$val]);
				if(!$childRow->exists()){
					$childRow=new ModuleTag;
					$childRow->module_type=$model->moduleTypeId;
					$childRow->module_id=$model->id;
					$childRow->tag_id=$val;
  				$childRow->save();
				}
			}
		}
	}

	/**
	 * save multiple managers
	 */
	public function saveManagers($model)
	{
		if($model->manager_id!=null){
			ModuleManager::deleteAll(['and',['module_type'=>$model->moduleTypeId,'module_id'=>$model->id],['not in','staff_id',$model->manager_id]]);
			foreach($model->manager_id as $key=>$val){
				$managerRow=ModuleManager::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'staff_id'=>$val])->one();
				if($managerRow==null){
					$managerRow=new ModuleManager;
					$managerRow->module_type=$model->moduleTypeId;
					$managerRow->module_id=$model->id;
					$managerRow->staff_id=$val;
					$managerRow->save();
				}
			}
		}
    $managerRow=ModuleManager::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'staff_id'=>$model->created_by]);
    if(!$managerRow->exists()){
			$managerRow=new ModuleManager;
			$managerRow->module_type=$model->moduleTypeId;
			$managerRow->module_id=$model->id;
			$managerRow->staff_id=$model->created_by;
			$managerRow->save();
    }
	}

	/**
	* columns for grid view for a module
	* @return \yii\db\ActiveQuery
	*/
	public function getGridViewColumns($type)
	{
		return CustomField::find()
		->innerJoin(CustomFieldModule::tableName(),CustomFieldModule::tableName().".custom_field_id=".CustomField::tableName().".id")
		->where([
			CustomFieldModule::tableName().'.module_type'=>$type,
			CustomField::tableName().'.show_in_grid'=>1,
			CustomField::tableName().'.status'=>1,
			CustomField::tableName().'.trashed'=>0,
		])
		->orderBy([CustomField::tableName().'.sort_order'=>SORT_ASC])
		->all();
	}

	/**
	* grid view column value
	* @return mix
	*/
	public function getGridValue($type,$model,$customField)
	{
		$value='';
		if(
			$customField['input_type']=='text' ||
			$customField['input_type']=='autocomplete' ||
			$customField['input_type']=='numberinput' ||
			$customField['input_type']=='websiteinput' ||
			$customField['input_type']=='emailinput' ||
			$customField['input_type']=='date' ||
			$customField['input_type']=='daterange' ||
			$customField['input_type']=='time' ||
			$customField['input_type']=='textarea' ||
			$customField['input_type']=='tinymce' ||
			$customField['input_type']=='radio'
		){
			$valueRow=CustomFieldData::find()->asArray()->where(['module_type'=>$type,'module_id'=>$model['id'],'input_field_id'=>$customField['id']])->one();
			if($valueRow!=null){
				if($customField['input_type']=='autocomplete'){
					$value=$valueRow['input_value'];
				}else{
					if($valueRow['input_value']!=''){
						if($customField['input_type']=='date'){
							$value=Yii::$app->formatter->asDate($valueRow['input_value']);
						}elseif($customField['input_type']=='daterange'){
							list($from,$to)=explode(" - ",$valueRow['input_value']);
							$value=Yii::t('app','{from} - {to}',['from'=>Yii::$app->formatter->asDate($from),'to'=>Yii::$app->formatter->asDate($to)]);
						}elseif($customField['input_type']=='time'){
							$value=date("h:i a",strtotime($valueRow['input_value']));
						}elseif($customField['input_type']=='textarea'){
							$value=nl2br($valueRow['input_value']);
						}else{
							$value=$valueRow['input_value'];
						}
					}
				}
			}
		}
		//Select & Checkbox
		if($customField['input_type']=='select' || $customField['input_type']=='checkbox'){
			if(($customField['input_type']=='select' && $customField['selection_type']==2) || $customField['input_type']=='checkbox'){
				$multipleDataRows=CustomFieldDataMultiple::find()->select(['option_id'])->where(['module_type'=>$type,'module_id'=>$model['id'],'input_field_id'=>$customField['id']])->asArray()->all();
				if($multipleDataRows!=null){
					foreach($multipleDataRows as $multipleDataRow){
						$value.=($value!='' ? ', ' : '').$this->getOptionTitleById($multipleDataRow['option_id'],$customField);
					}
				}
			}else{
				$valueRow=CustomFieldData::find()->asArray()->where(['module_type'=>$type,'module_id'=>$model['id'],'input_field_id'=>$customField['id']])->one();
				if($valueRow!=null){
					$value=$this->getOptionTitleById($valueRow['input_value'],$customField);
				}
			}
		}
		if($customField['input_type']=='radio'){
			$valueRow=CustomFieldData::find()->asArray()->where(['module_type'=>$type,'module_id'=>$model['id'],'input_field_id'=>$customField['id']])->one();
			if($valueRow!=null){
				$value=$this->getOptionTitleById($valueRow['input_value'],$customField);
			}
		}
		if($customField['input_type']=='timerange'){
			$valueRow=CustomFieldDatetimeInfo::find()->asArray()->where(['module_type'=>$type,'module_id'=>$model['id'],'input_field_id'=>$customField['id']])->one();
			if($valueRow!=null){
				if($valueRow['start_time']!='')$value=date("h:i a",strtotime($valueRow['start_time']));
				if($valueRow['start_time']!='' && $valueRow['end_time']!='')$value.=' - ';
				if($valueRow['end_time']!='')$value.=date("h:i a",strtotime($valueRow['end_time']));
			}
		}
		return $value;
	}

	public function getGridViewFilters($customField)
	{

	}
	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////

	public function getGoogleMapValue($model,$mediaTypeField)
	{
		$valueRow=MediaOutletGoogleMapInfo::find()->asArray()->where(['media_outlet_id'=>$model->id,'input_field_id'=>$mediaTypeField['id']])->one();
		if($valueRow!=null){
			return [
				'map_location'=>$valueRow['map_location'],
				'map_lat'=>$valueRow['map_lat'],
				'map_lat_org'=>$valueRow['map_lat_org'],
				'map_lng'=>$valueRow['map_lng'],
				'map_lng_org'=>$valueRow['map_lng_org'],
				'google_static_image'=>$valueRow['google_static_image'],
			];
		}
	}

	public function getInputTimeRangeValue($model,$mediaTypeField)
	{
		$valueRow=MediaOutletTimeRange::find()->asArray()->where(['media_outlet_id'=>$model->id,'input_field_id'=>$mediaTypeField->id])->one();
		if($valueRow!=null){
			return [
				'start'=>$valueRow['start_time'],
				'end'=>$valueRow['end_time'],
			];
		}
	}
}
?>
