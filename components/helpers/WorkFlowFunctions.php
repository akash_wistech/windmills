<?php
namespace app\components\helpers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Component;
use app\models\WorkflowDataItem;

class WorkFlowFunctions extends Component
{
  /**
   * save workflow and stage
   */
  public function saveWorkFlow($model)
  {
    //Saving Workflow
    if($model->workflow_id!=null || $model->workflow_stage_id!=null){
      $workflowRow=WorkflowDataItem::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])->one();
      if($workflowRow==null){
        $workflowRow=new WorkflowDataItem;
        $workflowRow->module_type=$model->moduleTypeId;
        $workflowRow->module_id=$model->id;
      }
      $workflowRow->workflow_id=$model->workflow_id;
      $workflowRow->workflow_stage_id=$model->workflow_stage_id;
      $workflowRow->save();
    }
  }

  public function getBoardItemHtml($model)
  {
    $controller='opportunity';
    if($model['rec_type']=='l')$controller='lead';
    $html = '';
    $html.='<div class="card card-custom card-stretch gutter-b mb-0">';
    $html.='  <div class="card-header border-0 p-1">';
    $html.='    <h3 class="card-title font-weight-bolder text-dark">'.$model['title'].'</h3>';
    $html.='    <div class="card-tools mr-1">';
    $html.='      <a href="javascript:;" onclick="javascript:window.location.href=\''.Url::to([$controller.'/update','id'=>$model['id']]).'\';" target="_blank" class="btn btn-tool" data-toggle="tooltip" title="'.Yii::t('app','Update').'">';
    $html.='        <i class="fas fa-edit"></i>';
    $html.='      </a>';
    $html.='      <a href="javascript:;" onclick="javascript:window.location.href=\''.Url::to([$controller.'/view','id'=>$model['id']]).'\';" target="_blank" class="btn btn-tool" data-toggle="tooltip" title="'.Yii::t('app','View').'">';
    $html.='        <i class="fas fa-table"></i>';
    $html.='      </a>';
    $html.='    </div>';
    $html.='  </div>';
    $html.='  <div class="card-body p-2">';
    $html.='    '.$model['descp'];
    $html.='  </div>';
    return $html;
  }
}
