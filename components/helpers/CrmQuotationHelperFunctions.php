<?php
namespace app\components\helpers;
use app\models\CrmQuotations;
use app\models\ValuationQuotationPropertiesDetails;
use app\models\ValuationQuotation;
use app\models\Company;
use app\models\Valuation;
use app\models\Buildings;
use app\models\ProposalStandardReport;
use app\models\ValuationConflict;
use app\models\Properties;
use app\models\ProposalMasterFile;
use app\models\ProposalDocs;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class CrmQuotationHelperFunctions extends Component
{

	public function getUniqueReference()
	{
		$previous_record = CrmQuotations::find()
		->select([
			'id',
			'reference_number',
		])->where(['parent_id' => null])
		->andWhere(['not in', 'type_of_service', [2, 3]])
		->orderBy(['id' => SORT_DESC])->asArray()->one();

		if ($previous_record <> null) {
			$prev_ref = explode("-", $previous_record['reference_number']);
				// 	dd($prev_ref);
			if(count($prev_ref) == 3){
                return 'REQ-' . date('Y') . '-' . ($prev_ref[2] + 1);
			}else {
			    if($prev_ref[4] > 0){
                    return 'REQ-' . date('Y') . '-' . ($prev_ref[4] + 1);
			    }else{
			        return 'REQ-' . date('Y') . '-' . ($prev_ref[3] + 1);
			    }

            }

		} else {
			return 'REQ-' . date('Y') . '-1';
		}

	}

	public function getUniqueReferenceBcs()
	{
		$previous_record = CrmQuotations::find()
		->select([
			'id',
			'reference_number',
		])->where(['parent_id' => null])
		->andWhere(['type_of_service' => 3])
		->orderBy(['id' => SORT_DESC])->asArray()->one();
		// dd($previous_record);

		if ($previous_record <> null) {
			$prev_ref = explode("-", $previous_record['reference_number']);
				// 	dd($prev_ref);
			if(count($prev_ref) == 3){
                return 'BCQ-' . date('Y') . '-' . ($prev_ref[2] + 1);
			}else {
			    if($prev_ref[4] > 0){
                    return 'BCQ-' . date('Y') . '-' . ($prev_ref[4] + 1);
			    }else{
			        return 'BCQ-' . date('Y') . '-' . ($prev_ref[3] + 1);
			    }

            }

		} else {
			return 'BCQ-' . date('Y') . '-1';
		}

	}

	public function getUniqueReferencePme()
	{
		$previous_record = CrmQuotations::find()
		->select([
			'id',
			'reference_number',
		])->where(['parent_id' => null])
		->andWhere(['type_of_service' => 2])
		->orderBy(['id' => SORT_DESC])->asArray()->one();

		if ($previous_record <> null) {
			$prev_ref = explode("-", $previous_record['reference_number']);
				// 	dd($prev_ref);
			if(count($prev_ref) == 3){
                return 'MEQ-' . date('Y') . '-' . ($prev_ref[2] + 1);
			}else {
			    if($prev_ref[4] > 0){
                    return 'MEQ-' . date('Y') . '-' . ($prev_ref[4] + 1);
			    }else{
			        return 'MEQ-' . date('Y') . '-' . ($prev_ref[3] + 1);
			    }

            }

		} else {
			return 'MEQ-' . date('Y') . '-1';
		}

	}
	

	public function getVatTotal($final_fee)
	{
		if ($final_fee!=null) {
			$tax = $final_fee*5/100;
			return $tax;
				// $finalAfterTax = $final_fee+$tax;
				// return $finalAfterTax;
		}else {
			return false;
		}
	}

    /**
     * List of List Purpose of valuation
     *
     * @return array
     */
    public function getPurposeOfValuationForQuotationArr()
    {
        return ArrayHelper::map(\app\models\ValuationPurposes::find()->where(['trashed' => 0])->all(), 'id', 'title');

    	/*return [
            '1' => Yii::t('app', 'Secured Lending.'),
            '2' => Yii::t('app', 'Financial Reporting.'),
            '3' => Yii::t('app', 'Strategic Management.'),
            '4' => Yii::t('app', 'Internal Management.'),
            '5' => Yii::t('app', 'Purchase of Property.'),
            '6' => Yii::t('app', 'Intention to Sell.'),
            '7' => Yii::t('app', 'Intention to Buy.'),
            '8' => Yii::t('app', 'Internal Purpose.'),
            '9' => Yii::t('app', 'Court Proceedings.'),
            '10' => Yii::t('app', 'Golden Visa Application.'),
            '11' => Yii::t('app', 'Insurance Purpose.'),
            '12' => Yii::t('app', 'Gift'),
            '13' => Yii::t('app', 'Visa Application'),
    	];*/
    }



    public function getHowManyProperties()
    {
    	return [
    		'1' => Yii::t('app', '1'),
    		'2' => Yii::t('app', '2'),
    		'3' => Yii::t('app', '3'),
    		'4' => Yii::t('app', '4'),
    		'5' => Yii::t('app', '5'),
    		'6' => Yii::t('app', '6'),
    		'7' => Yii::t('app', '7'),
    		'8' => Yii::t('app', '8'),
    		'9' => Yii::t('app', '9'),
    		'10' => Yii::t('app', '10'),
    		'other' => Yii::t('app', 'Other'),
    	];
    }

    public function getStatusArr()
    {
    	return [
    		'1' => Yii::t('app', 'Active'),
    		'0' => Yii::t('app', 'InActive'),
    	];
    }

    public function getQuotationStatusArr()
    {
    	return [
    		'0' => Yii::t('app', 'in Progress'),
    		'1' => Yii::t('app', 'Sent Quotation'),
    		'2' => Yii::t('app', 'Converted to Valuation'),
    	];

    }

    public function getValuationApproach()
    {
    	return [
    		'market-approach' => Yii::t('app', 'Market Approach'),
    		'income-approach' => Yii::t('app', 'Income Approach'),
    		'residual-value-approach' => Yii::t('app', 'Residual Value Approach'),
    		'cost-approach' => Yii::t('app', 'Cost Approach'),
    	];
    }

    public function getNumberofComparables()
    {
    	return [
    		'lessThan-3' => Yii::t('app', 'Less Than 3'),
    		'3-5' => Yii::t('app', '3 - 5'),
    		'6-10' => Yii::t('app', '6 - 10'),
    		'11-15' => Yii::t('app', '11 - 15'),
    		'above-15' => Yii::t('app', 'Above 15'),
    	];
    }

    public function getNumberofUnitsInBuildingArr()
    {
    	return [
    		'1' => Yii::t('app', '1-20 units'),
    		'2' => Yii::t('app', '21-50 units'),
    		'3' => Yii::t('app', '50-100 units'),
    		'4' => Yii::t('app', 'GreaterThan 100 units'),
    	];
    }

    public function getNumberofUnitsInLandArr()
    {
    	return [
    		'1' => Yii::t('app', '1'),
    		'2' => Yii::t('app', '2'),
    		'3' => Yii::t('app', '3'),
    		'4' => Yii::t('app', '4'),
    		'5' => Yii::t('app', '5'),
    	];
    }

    public function getLandSizrArr()
    {
    	return [
    		'1' => Yii::t('app', '1-7500'),
    		'2' => Yii::t('app', '7501-12500'),
    		'3' => Yii::t('app', '12,501-17500'),
    		'4' => Yii::t('app', '17501-25000'),
    		'5' => Yii::t('app', 'Above-25000'),
    	];
    }

    public function getTypeofvaluation()
    {
    	return [
    		'physical-inspection' => Yii::t('app', 'Physical Inspection'),
    		'desktop' => Yii::t('app', 'Desktop'),
    		'drive-by' => Yii::t('app', 'Drive by'),

    	];
    }

    public function getComplexity()
    {
    	return [
    		'standard' => Yii::t('app', 'Standard '),
    		'non-standard' => Yii::t('app', 'Non-Standard'),
    	];
    }

    public function getNewRepeatValuation()
    {
    	return [
    		'new-valuation' => Yii::t('app', 'New Valuation'),
    		'repeat-valuation' => Yii::t('app', 'Repeat Valuation'),
    	];
    }


	    /**
     * List of Valuation Approach
     *
     * @return array
     */
	    public function getValuationApproachArr()
	    {

	    	return [
	    		'1' => Yii::t('app', 'Market Approach'),
	    		'2' => Yii::t('app', 'Income Approach'),
	    		'3' => Yii::t('app', 'Profit Approach'),
	    		'4' => Yii::t('app', 'Residual Approach'),
	    		'5' => Yii::t('app', 'DCF Approach'),
	    		'6' => Yii::t('app', 'Reinstatement Cost Approach'),
	    	];
	    }



	    public function getExistingProperties($model_id)
	    {
	    	if ($model_id !=null) {
	    		$result =  ValuationQuotationPropertiesDetails::find()
	    		->where(['valuation_quotation_id'=>$model_id])
	// ->asArray()
	    		->all();
	// echo "<pre>";
	// print_r($result);
	// die();

	    		if($result !=null){
	    			return $result;
	    		}else{
	    			return false;
	    		}
	    	}else{
	    		return false;
	    	}
	    }



	    public function getMultipleProperties($id)
	    {
	    	if ($id !=null) {
	    		$result =  ValuationQuotationPropertiesDetails::find()
	    		->where(['valuation_quotation_id'=>$id])
		// ->asArray()
	    		->all();
		// echo "<pre>";
		// print_r($result);
		// die();

	    		if($result !=null){
	    			return $result;
	    		}else{
	    			return false;
	    		}
	    	}else{
	    		return false;
	    	}
	    }

	    public function getPaymentTerms()
	    {
	    	return [
	    		'0%' => Yii::t('app', '0%'),
	    		'25%' => Yii::t('app', '25%'),
	    		'50%' => Yii::t('app', '50%'),
	    		'75%' => Yii::t('app', '75%'),
	    		'100%' => Yii::t('app', '100%'),
	    	];
	    }


	    public function getBuiltUpAreaArr()
	    {
	    	return [
	    		'1' => Yii::t('app', '1 - 4000'),
	    		'2' => Yii::t('app', '4001 - 6000'),
	    		'3' => Yii::t('app', '6001 - 8000'),
	    		'4' => Yii::t('app', '8001 - 10000'),
	    		'5' => Yii::t('app', 'Above 10000'),
	    	];
	    }



	    public function getMainTableData($id)
	    {
	    	if ($id !=null) {
	    		$result =  ValuationQuotation::find()
	    		->where(['id'=>$id])
	    		->asArray()
	    		->one();
	// echo "<pre>";
	// print_r($result);
	// die();

	    		if($result !=null){
	    			return $result;
	    		}else{
	    			return false;
	    		}
	    	}else{
	    		return false;
	    	}
	    }

	    public function getClientName($id)
	    {
	    	if ($id !=null) {
	    		$result =  Company::find()

	    		->where(['id'=>$id])
	    		->asArray()
	    		->one();
	// echo "<pre>";
	// print_r($result);
	// die();

	    		if($result !=null){
	    			return $result;
	    		}else{
	    			return false;
	    		}
	    	}else{
	    		return false;
	    	}
	    }


	    public function getBuildingName($id)
	    {
	    	if ($id !=null) {
	    		$result =  Buildings::find()
	    		->select(['title'])
	    		->where(['id'=>$id])
	    		->asArray()
	    		->one();
	// echo "<pre>";
	// print_r($result);
	// die();

	    		if($result !=null){
	    			return $result;
	    		}else{
	    			return false;
	    		}
	    	}else{
	    		return false;
	    	}
	    }

	    public function StandardReport()
	    {
	    	$result = ProposalStandardReport::find()->asArray()->one();
	    	return $result;
	    }

	    public function getDcouments($id)
	    {
	    	if ($id !=null) {
	    		$result =  Properties::find()
	    		->select(['required_documents'])
	    		->where(['id'=>$id])
	    		->asArray()
	    		->one();
			// echo "<pre>";
			// print_r($result);
			// die();

	    		if($result !=null){
	    			return $result;
	    		}else{
	    			return false;
	    		}
	    	}else{
	    		return false;
	    	}
	    }

	    public function getClientNameForConflict($client_id)
	    {
	    	if ($client_id !=null) {
	    		$result =  Company::find()
	    		->select(['title'])
	    		->where(['id'=>$client_id])
	    		->asArray()
	    		->one();
			// echo "<pre>";
			// print_r($result);
			// die();

	    		if($result !=null){
	    			return $result;
	    		}else{
	    			return false;
	    		}
	    	}else{
	    		return false;
	    	}
	    }

	    public function getValuationOfSelectedClient($id)
	    {
	    	if ($id !=null) {
	    		$result =  Valuation::find()
	    		->select(['id'])
	    		->where(['client_id'=>$id]);
			// echo "<pre>";
			// print_r($result);
			// die();

	    		if($result !=null){
	    			return $result;
	    		}else{
	    			return false;
	    		}
	    	}else{
	    		return false;
	    	}
	    }


	    public function getChildTableData($id)
	    {
	    	if ($id !=null) {
	    		$result =  ValuationQuotationPropertiesDetails::find()
	    		->where(['valuation_quotation_id'=>$id])
	    		->asArray()
	    		->all();
			// echo "<pre>";
			// print_r($result);
			// die();

	    		if($result !=null){
	    			return $result;
	    		}else{
	    			return false;
	    		}
	    	}else{
	    		return false;
	    	}
	    }

	    public function getClientType()
	    {
	    	return [
	    		'bank' => Yii::t('app', 'Bank'),
	    		'corporate' => Yii::t('app', 'Corporate'),
	    		'individual' => Yii::t('app', 'Individual'),
	    		'partner' => Yii::t('app', 'Partner'),
	    		'foreign-individual' => Yii::t('app', 'Foreign Individual'),
	    	];
	    }

	    public function getRelativeDiscount()
	    {
	    	
	    	$discount['base-fee'] = Yii::t('app', 'Base Fee');
	    	for($i=1; $i <=100 ; $i++) { 
	    		$discount[$i] =  Yii::t('app', $i.'%');
	    	}
	    	return $discount;
	    	
	    }

	    public function getPropertiesCategoriesListArr()
	    {

	    	return [
	    		'residential-land' => Yii::t('app', 'Residential Land'),
	    		'residential-apartment' => Yii::t('app', 'Residential Apartment'),
	    		'commercial-office' => Yii::t('app', 'Commercial Office'),
	    		'residential-villa' => Yii::t('app', 'Residential Villa'),
	    		'residential-building' => Yii::t('app', 'Residential Building'),
	    		'commercial-building' => Yii::t('app', 'Commercial Building'),
	    		'mixed-use-building' => Yii::t('app', 'Mixed Use Building '),
	    	];
	    }






	    public function getTotalFee($Qrecommended_fee, $relative_discount)
	    {

	    	$baseFee = $this->getBaseFeeFromMasterFile();

			// in quotation Rec fee
	    	if ($Qrecommended_fee!=null) {
	    		if ($relative_discount=='base-fee') {
					// 	echo "if";
					// print_r($relative_discount.','.$Qrecommended_fee);
					// die();
	    			$afterDiscount = $Qrecommended_fee-$baseFee;
	    			return $afterDiscount;
	    		}
	    		else {
						// echo "else";
						// print_r($relative_discount.','.$Qrecommended_fee);
						// die();
	    			$discount = $Qrecommended_fee/100*$relative_discount;
	    			$totalAmount = $Qrecommended_fee-$discount;
	    			$afterDiscount = $Qrecommended_fee-$totalAmount;
	    			return $afterDiscount;
	    		}
	    	}
	    	else {
	    		return false;
	    	}



	    }





	    function numberTowords($num)
	    {

	    	$ones = array(
	    		0 =>"Zero",
	    		1 => "One",
	    		2 => "Two",
	    		3 => "Three",
	    		4 => "Four",
	    		5 => "Five",
	    		6 => "Six",
	    		7 => "Seven",
	    		8 => "Eight",
	    		9 => "Nine",
	    		10 => "Ten",
	    		11 => "Eleven",
	    		12 => "Twelve",
	    		13 => "Thirteen",
	    		14 => "Fourteen",
	    		15 => "Fifteen",
	    		16 => "Sixteen",
	    		17 => "Seventeen",
	    		18 => "Eighteen",
	    		19 => "Nineteen",
	    		"014" => "Fourteen"
	    	);
	    	$tens = array(
	    		0 => "Zero",
	    		1 => "Ten",
	    		2 => "Twenty",
	    		3 => "Thirty",
	    		4 => "Forty",
	    		5 => "Fifty",
	    		6 => "Sixty",
	    		7 => "Seventy",
	    		8 => "Eighty",
	    		9 => "Ninety"
	    	);
	    	$hundreds = array(
	    		"Hundred",
	    		"Thousand",
	    		"Million",
	    		"Billion",
	    		"Trillion",
	    		"Quardrillion"
	    	); /*limit t quadrillion */
	    	$num = number_format($num,2,".",",");
	    	$num_arr = explode(".",$num);
	    	$wholenum = $num_arr[0];
	    	$decnum = $num_arr[1];
	    	$whole_arr = array_reverse(explode(",",$wholenum));
	    	krsort($whole_arr,1);
	    	$rettxt = "";
	    	foreach($whole_arr as $key => $i){

	    		while(substr($i,0,1)=="0")
	    			$i=substr($i,1,5);
	    		if($i < 20){
	    			/* echo "getting:".$i; */
	    			$rettxt .= $ones[$i];
	    		}elseif($i < 100){
	    			if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)];
	    			if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)];
	    		}else{
	    			if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0];
	    			if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)];
	    			if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)];
	    		}
	    		if($key > 0){
	    			$rettxt .= " ".$hundreds[$key]." ";
	    		}
	    	}
	    	if($decnum > 0){
	    		$rettxt .= " and ";
	    		if($decnum < 20){
	    			$rettxt .= $ones[$decnum];
	    		}elseif($decnum < 100){
	    			$rettxt .= $tens[substr($decnum,0,1)];
	    			$rettxt .= " ".$ones[substr($decnum,1,1)];
	    		}
	    	}
	    	return ucwords($rettxt);
	    }


	    public function numberTowords1($amount)
	    {
	    	$amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
	        // Check if there is any number after decimal
	    	$amt_hundred = null;
	    	$count_length = strlen($num);
	    	$x = 0;
	    	$string = array();
	    	$change_words = array(0 => '', 1 => 'One', 2 => 'Two',
	    		3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
	    		7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
	    		10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
	    		13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
	    		16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
	    		19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
	    		40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
	    		70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
	    	$here_digits = array('', 'Hundred','Thousand','Thousand Hundred', 'Crore');
	    	while( $x < $count_length ) {
	    		$get_divider = ($x == 2) ? 10 : 100;
	    		$amount = floor($num % $get_divider);
	    		$num = floor($num / $get_divider);
	    		$x += $get_divider == 10 ? 1 : 2;
	    		if ($amount) {
	    			$add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
	    			$amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
	    			$string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.'
	    			'.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. '
	    			'.$here_digits[$counter].$add_plural.' '.$amt_hundred;
	    		}else $string[] = null;
	    	}
	    	$implode_to_Rupees = implode('', array_reverse($string));
	    	$get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . "
	    		" . $change_words[$amount_after_decimal % 10]) . ' Fils' : '';
	    	return ($implode_to_Rupees ? $implode_to_Rupees . 'AED ' : '') . $get_paise;
	    }


	    public function getClientAddress($client_id)
	    {
	    	if ($client_id !=null) {
	    		$result =  Company::find()
	    		->select(['address','title'])
	    		->where(['id'=>$client_id])
					// ->asArray()
	    		->one();

	    		if($result !=null){
	    			return $result;
	    		}else{
	    			return false;
	    		}
	    	}else{
	    		return false;
	    	}
	    }





	    public function getDiscountRupee($totalValuationFee,$discount){
	    	if ($totalValuationFee!=null && $discount!=null) {
	    		if ($discount=='base-fee') {
			 // echo "string base fee";
	    			return $this->getBaseFeeFromMasterFile();
	    		}
	    		else {
	    			// echo "string others";die();
	    			if ($totalValuationFee!=null && $discount!=null) {
	    			// echo $discount;die();
	    				$discountval = $totalValuationFee/100*$discount;


	    				return $discountval;
	    			}
	    		}
	    	}else{
	    		return false;
	    	}
	    }



    public function getAmount($post)
    {
        // echo $post['paymentTerms']; die();
        $ponits=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null) {

//client Tye
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Client Type', 'sub_heading'=>$post['clientType']])->one();
            if($result !=null){
                $points += $result['values'];
// yii::info('clientType if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "1"; die();
                return false;
            }

//Payment Terms
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Payment Terms', 'sub_heading'=>$post['paymentTerms']])->one();
            if($result !=null){
                // echo $result['values']; die();
                $points += $result['values'];
// yii::info('payment term if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "2"; die();
                return false;
            }



            // subject property value
            $name = Properties::find()->select(['title'])->where(['id'=>$post['property']])->one();
            $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>$name['title']])->one();
            if($result !=null){
                $points += $result['values'];
// yii::info('subj property if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    $points += $result['values'];
// yii::info('subj property else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "3"; die();
                    return false;
                }
            }

            //city value
            $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>$post['city']])->one();
            if($result !=null){
                $points += $result['values'];
// yii::info('city if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "else";
                $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    // echo $result['values']; die();
                    $points += $result['values'];
// yii::info('city else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "4"; die();
                    return false;
                }
            }

            //tenure value
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            $result = ProposalMasterFile::find()->where(['heading'=>'Tenure', 'sub_heading'=>$tenureName])->one();
            if($result !=null){
                // echo "working"; echo $result['values']; die();
                $points += $result['values'];
// yii::info('tenure if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "5"; die();
                $points +=0;
            }

            // complexity value
            $result = ProposalMasterFile::find()->where(['heading'=>'Complexity', 'sub_heading'=>$post['complexity']])->one();
            if($result !=null){
                $points += $result['values'];
// yii::info('complexity if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "6"; die();
                return false;
            }

            //Type of Valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Type of Valuation', 'sub_heading'=>$post['type_of_valuation']])->one();
            if ($result!=null) {
                $points += $result['values'];
// yii::info('type of val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "7"; die();
                return false;
            }

            //number_of_comparables
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Number of Comparables Data', 'sub_heading'=>$post['number_of_comparables']])->one();
            if ($result!=null) {
                // echo $result['values']; die();
                $points += $result['values'];
// yii::info('No of comparables if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "8"; die();
                return false;
            }

            //new repeat_valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'New Repeat Valuation Data', 'sub_heading'=>$post['repeat_valuation']])->one();
            if ($result!=null) {
                $points += $result['values'];
// yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }




//built up area
            if ($post['property']==3 || $post['property']==7 || $post['property']==22 || $post['property']==4 || $post['property']==5 ||
                $post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29) {
                // echo "if bla bla bla bla"; die();
                $points +=0;
// yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }
            else{
                // echo "else bla bla bla bla"; die();
                $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArr()[$post['built_up_area']];
                $result = ProposalMasterFile::find()
                    ->where(['heading'=>'Build up Area of Subject Property', 'sub_heading'=>$sub_heading])->one();
                if ($result!=null) {
                    // echo $result['values']; die();
                    $points += $result['values'];
// yii::info('BUA else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "10"; die();
                    return false;
                }
            }


//land size
            if ($post['property']==4 || $post['property']==5 ||
                $post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29) {
                $landSize = yii::$app->quotationHelperFunctions->getLandSizrArr()[$post['land']];
                $result = ProposalMasterFile::find()->where(['heading'=>'Land', 'sub_heading'=>$landSize])->one();
                if ($result!=null) {
                    $points += $result['values'];
// yii::info('landsize if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "11"; die();
                    return false;
                }
            }else{
                $points +=0;
// yii::info('landsize else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }


            //number of units 3,7,22 buildings wali han......
            if ($post['property']==3 || $post['property']==7 || $post['property']==22) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                // echo "hello"; die();
                $result = ProposalMasterFile::find()->where(['heading'=>'No Of Units Building', 'sub_heading'=>$num_units])->one();
                if ($result!=null) {
                    $points += $result['values'];
// yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "12"; die();
                    return false;
                }
            }else{
                // echo "land"; die();
                $result = ProposalMasterFile::find()->where(['heading'=>'Number of Units Land', 'sub_heading'=>$post['no_of_units']])->one();
                if ($result!=null) {
                    $points += $result['values'];
// yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "13"; die();
                    return false;
                }
            }
// elseif($post['property']==4 || $post['property']==5 ||
// 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me"; die();
// }



            $tat = $points;
// yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
// yii::info('basefee > = '.$baseFee.' ','my_custom_log');
            $fee = $points+$baseFee;
// yii::info('Fee > = '.$fee.' ','my_custom_log');

            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }






    public function getBaseFee($property_id)
    {
        if ($property_id!=null) {
            if ($property_id==3 || $property_id==7 || $property_id==22) {
                $baseFee = ProposalMasterFile::find()
                    ->where(['heading'=>'Base Fee Building', 'sub_heading'=>'base-fee-building'])->one();
                if ($baseFee!=null) {
                    return $baseFee['values'];
                }else{
                    return false;
                }

            }else{
                $baseFee = ProposalMasterFile::find()
                    ->where(['heading'=>'Base Fee Others', 'sub_heading'=>'base-fee-others'])->one();
                if ($baseFee!=null) {
                    return $baseFee['values'];
                }else{
                    return false;
                }

            }
        }else{
            return false;
        }

    }

    public function getQuotationStatusListArrLabel()
    {
        return [
            '0' => '<span class="badge grid-badge badge-info"> '.Yii::t('app','Inquiry Received').'</span>',
            '1' => '<span class="badge grid-badge badge-primary"> '.Yii::t('app','Quotation Sent').'</span>',
            '2' => '<span class="badge grid-badge badge-warning"></i> '.Yii::t('app','Quotation Verified').'</span>',
            '3' => '<span class="badge grid-badge badge-dark">'.Yii::t('app','Toe Sent').'</span>',
            '4' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Toe Accepted').'</span>',
            '5' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Toe Signed and Received').'</span>',
            '6' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Payment Received').'</span>',
            '7' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Quotation Rejected').'</span>',
            '8' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Toe Rejected').'</span>',
            '9' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Pending').'</span>',
            '10' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','On Hold').'</span>',
            '11' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Cancelled').'</span>',
            '12' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Regretted').'</span>',
           // '13' => '<span class="badge grid-badge badge-info">'.Yii::t('app','Closed').'</span>',
            '14' => '<span class="badge grid-badge badge-warning">'.Yii::t('app','Documents Requested').'</span>',
            // '15' => '<span class="badge grid-badge badge-info">'.Yii::t('app','Recommended For Approval').'</span>',
            '16' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Quotation Approved').'</span>',
            '17' => '<span class="badge grid-badge badge-warning">'.Yii::t('app','Quotation Recommended').'</span>',
			'18' => '<span class="badge grid-badge badge-warning">'.Yii::t('app','TOE Verified').'</span>',
        ];
    }

	public function getQuotationStatusListArrLabelTextWithColor()
    {
        return [
            '0' => '<span class="badge grid-badge text-info"> '.Yii::t('app','Inquiry Received').'</span>',
            '1' => '<span class="badge grid-badge text-primary"> '.Yii::t('app','Quotation Sent').'</span>',
            '2' => '<span class="badge grid-badge text-warning"></i> '.Yii::t('app','Quotation Verified').'</span>',
            '3' => '<span class="badge grid-badge text-success">'.Yii::t('app','Toe Sent').'</span>',
            '4' => '<span class="badge grid-badge text-success">'.Yii::t('app','Toe Accepted').'</span>',
            '5' => '<span class="badge grid-badge text-success">'.Yii::t('app','Toe Signed and Received').'</span>',
            '6' => '<span class="badge grid-badge text-success">'.Yii::t('app','Payment Received').'</span>',
            '7' => '<span class="badge grid-badge text-danger">'.Yii::t('app','Quotation Rejected').'</span>',
            '8' => '<span class="badge grid-badge text-danger">'.Yii::t('app','Toe Rejected').'</span>',
            '9' => '<span class="badge grid-badge text-danger">'.Yii::t('app','Pending').'</span>',
            '10' => '<span class="badge grid-badge text-danger">'.Yii::t('app','On Hold').'</span>',
            '11' => '<span class="badge grid-badge text-danger">'.Yii::t('app','Cancelled').'</span>',
            '12' => '<span class="badge grid-badge text-danger">'.Yii::t('app','Regretted').'</span>',
           // '13' => '<span class="badge grid-badge text-info">'.Yii::t('app','Closed').'</span>',
            '14' => '<span class="badge grid-badge text-warning">'.Yii::t('app','Documents Requested').'</span>',
            // '15' => '<span class="badge grid-badge text-info">'.Yii::t('app','Recommended For Approval').'</span>',
            '16' => '<span class="badge grid-badge text-success">'.Yii::t('app','Quotation Approved').'</span>',
            '17' => '<span class="badge grid-badge text-warning">'.Yii::t('app','Quotation Recommended').'</span>',
			'18' => '<span class="badge grid-badge text-warning">'.Yii::t('app','TOE Verified').'</span>',
        ];
    }
    public function getQuotationStatusListArr()
    {
        return [
            '0' => Yii::t('app', 'Inquiry Received'),
            '1' => Yii::t('app', 'Quotation Sent'),
            '2' => Yii::t('app', 'Quotation Verified'),
            '3' => Yii::t('app', 'Toe Sent'),
            '4' => Yii::t('app', 'Toe Accepted'),
            '5' => Yii::t('app', 'Toe Signed and Received'),
            '6' => Yii::t('app', 'Payment Received '),
            '7' => Yii::t('app', 'Quotation Rejected '),
            '8' => Yii::t('app', 'Toe Rejected '),
            '9' => Yii::t('app', 'Pending '),
            '10' => Yii::t('app', 'On Hold '),
            '11' => Yii::t('app', 'Cancelled'), 				// actionStep_8 		//done
            '12' => Yii::t('app', 'Regretted'), 				// actionStep_8 		//done
           // '13' => Yii::t('app', 'Closed'),
           '14' => Yii::t('app', 'Documents Requested'),
           // '15' => Yii::t('app', 'Recommended For Approval'),
           '16' => Yii::t('app', 'Quotation Approved'),
           '17' => Yii::t('app', 'Quotation Recommended'),
           '18' => Yii::t('app', 'TOE Verified'),
        ];
    }

	public function getQuotationStatusListTextArr()
    {
        return [
            '' => Yii::t('app', 'Select'),
            'Inquiry Received' => Yii::t('app', 'Inquiry Received'),
            'Documents Requested' => Yii::t('app', 'Documents Requested'),
            'Quotation Recommended' => Yii::t('app', 'Quotation Recommended'),
            'Quotation Sent' => Yii::t('app', 'Quotation Sent'),
            'Quotation Verified' => Yii::t('app', 'Quotation Verified'),
            'Quotation Approved' => Yii::t('app', 'Quotation Approved'),
            'Quotation Rejected' => Yii::t('app', 'Quotation Rejected'),
            'TOE Verified' => Yii::t('app', 'TOE Verified'),
            'Toe Sent' => Yii::t('app', 'Toe Sent'),
            'Toe Accepted' => Yii::t('app', 'Toe Accepted'),
            'Toe Signed and Received' => Yii::t('app', 'Toe Signed and Received'),
            'Toe Rejected' => Yii::t('app', 'Toe Rejected'),
            'Payment Received' => Yii::t('app', 'Payment Received'),
            'Pending' => Yii::t('app', 'Pending '),
            'On Hold' => Yii::t('app', 'On Hold '),
            'Cancelled' => Yii::t('app', 'Cancelled'), 				
            'Regretted' => Yii::t('app', 'Regretted'), 				
        ];
    }

    public function formatOutput($diff){
        /* function to return the highrst defference fount */
        if(!is_object($diff)){
            return;
        }

        if($diff->y > 0){
            return $diff->y .(" year".($diff->y > 1?"s":"")." ago");
        }

        if($diff->m > 0){
            return $diff->m .(" month".($diff->m > 1?"s":"")." ago");
        }

        if($diff->d > 0){
            return $diff->d .(" day".($diff->d > 1?"s":"")." ago");
        }

        if($diff->h > 0){
            return $diff->h .(" hour".($diff->h > 1?"s":"")." ago");
        }

        if($diff->i > 0){
            return $diff->i .(" minute".($diff->i > 1?"s":"")." ago");
        }

        if($diff->s > 0){
            return $diff->s .(" second".($diff->s > 1?"s":"")." ago");
        }
    }

    public function AddStatusHistory($model=null)
    {
        // echo "<pre>"; print_r($model->id); echo "</pre>"; //die();
        // echo "<pre>"; print_r($model->quotation_status); echo "</pre>"; //die();
        // echo "<pre>"; print_r($model->status_change_date); echo "</pre>"; die();

        $statusHistory 				 = new \app\models\QuotationStatusHistory;
        $statusHistory->quotation_id = $model->id;
        $statusHistory->status_id 	 = $model->quotation_status;
        $statusHistory->date 		 = $model->status_change_date;
        $statusHistory->save();
    }

    public function getReportPeriod()
    {
        return [
            '0' => Yii::t('app','All The Time'),
            '1' => Yii::t('app','Last 30 Days'),
            '2' => Yii::t('app','This Month'),
            '3' => Yii::t('app','This Quarter'),
            '4' => Yii::t('app','This Year'),
            '5' => Yii::t('app','Last Month'),
            '6' => Yii::t('app','Last Quarter'),
            '7' => Yii::t('app','Last Year'),
            '9' => Yii::t('app','Custom Dates'),
            '10' => Yii::t('app','Last 3 Months'),

        ];
    }
    public function  getFilterDates($period) {



        if ($period == 1) {

            $date_from = date('Y-m-d', strtotime('today - 30 days'));
            $date_to = date('Y-m-d');

        }else if($period == 2) {

            $date_from = date('Y-m-01');
            // $date_to = date('Y-m-t');
            $date_to = date('Y-m-d',strtotime("-1 days"));


        }else if($period== 3) {

            $date_array = $this->getThisQuarter();
            $date_from = date('Y-m-d', $date_array['start_date']);
            $date_to = date('Y-m-d',strtotime("-1 days"));
            // $date_to = date('Y-m-d', $date_array['end_date']);


        }else if($period== 4) {
            $date_from = date('Y-01-01');
            $date_to = date('Y-m-d',strtotime("-1 days"));
            // $date_to = date('Y-12-31');

        }else if($period == 5) {
            $date_from = date('Y-m-d', strtotime('first day of last month'));
            $date_to = date('Y-m-d', strtotime('last day of last month'));

        }else if($period == 6) {
            $date_array = $this->getLastQuarter();
            $date_from = date('Y-m-d', $date_array['start_date']);
            $date_to = date('Y-m-d', $date_array['end_date']);

        }else if($period == 7) {
            $last_year = date('Y') - 1;
            $date_from = date($last_year . '-01-01');
            $date_to = date($last_year . '-12-31');

        }else if($period == 8) {
            $date_from = date('Y-m-d');
            $date_to = date('Y-m-d');

        }else if($period == 10) {
            $date_from = date('Y-m-d', strtotime('today - 90 days'));
            $date_to = date('Y-m-d');

        }

        return array('start_date'=>$date_from, 'end_date'=> $date_to);

    }
    public function getToeSignedAndRecievedtArr()
    {
        return [
            '1' => Yii::t('app', 'Yes'), 	// actionStep_10 //done
        ];
    }

    public function getToeSignedAndRecievedtArrLabel()
    {
        return [
            '1' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Yes').'</span>',
        ];
    }
    public function  getThisQuarter() {


        $current_month = date('m');
        $current_year = date('Y');
        if($current_month>=1 && $current_month<=3)
        {
            $start_date = strtotime('1-January-'.$current_year);  // timestamp or 1-Januray 12:00:00 AM
            $end_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM means end of 31 March
        }
        else  if($current_month>=4 && $current_month<=6)
        {
            $start_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM
            $end_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM means end of 30 June
        }
        else  if($current_month>=7 && $current_month<=9)
        {
            $start_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM
            $end_date = strtotime('1-October-'.$current_year);  // timestamp or 1-October 12:00:00 AM means end of 30 September
        }
        else  if($current_month>=10 && $current_month<=12)
        {
            $start_date = strtotime('1-October-'.$current_year);  // timestamp or 1-October 12:00:00 AM
            $end_date = strtotime('1-January-'.($current_year+1));  // timestamp or 1-January Next year 12:00:00 AM means end of 31 December this year
        }

        return array('start_date'=>$start_date, 'end_date'=> $end_date);


    }

    public function  getLastQuarter() {


        $current_month = date('m');
        $current_year = date('Y');

        if($current_month>=1 && $current_month<=3)
        {
            $start_date = strtotime('1-October-'.($current_year-1));  // timestamp or 1-October Last Year 12:00:00 AM
            $end_date = strtotime('1-January-'.$current_year);  // // timestamp or 1-January  12:00:00 AM means end of 31 December Last year
        }
        else if($current_month>=4 && $current_month<=6)
        {
            $start_date = strtotime('1-January-'.$current_year);  // timestamp or 1-Januray 12:00:00 AM
            $end_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM means end of 31 March
        }
        else  if($current_month>=7 && $current_month<=9)
        {
            $start_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM
            $end_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM means end of 30 June
        }
        else  if($current_month>=10 && $current_month<=12)
        {
            $start_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM
            $end_date = strtotime('1-October-'.$current_year);  // timestamp or 1-October 12:00:00 AM means end of 30 September
        }
        return array('start_date'=>$start_date, 'end_date'=> $end_date);


    }
    
    
    
    
    
	public function getLast3MonthsApprovedFee($model)
	{
        $endDate = date('Y-m-d');  // Today's date
        $last_3m_date = date('Y-m-d', strtotime('-3 months', strtotime($endDate)));

		$data['last_3_months'] = $this->makeHighLowFee($model, $last_3m_date, $endDate);
		return $data['last_3_months']->highest_fee;
	}
    
    
	public function GetHighLowFee($model)
	{
        $endDate = date('Y-m-d');  // Today's date
        $last_3m_date = date('Y-m-d', strtotime('-3 months', strtotime($endDate)));
        $last_6m_date = date('Y-m-d', strtotime('-6 months', strtotime($endDate)));
        $last_1y_date = date('Y-m-d', strtotime('-1 year', strtotime($endDate)));

		$data['last_3_months'] = $this->makeHighLowFee($model, $last_3m_date, $endDate);
		$data['last_6_months'] = $this->makeHighLowFee($model, $last_6m_date, $endDate);
		$data['last_1_year']   = $this->makeHighLowFee($model, $last_1y_date, $endDate);
		return $data;
	}


	public function makeHighLowFee($model, $start_date, $end_date)
	{
        $property_id = $model->zeroIndexProperty->property_id;
        $building_id = $model->zeroIndexProperty->building_info;
        $city_id = $model->zeroIndexProperty->building->city;

        $query = \app\models\CrmReceivedProperties::find()
		->select([
			'MAX(toe_fee) AS highest_fee', 
			'MIN(toe_fee) AS lowest_fee'
		])
		->innerJoin('crm_quotations', 'crm_quotations.id = crm_received_properties.quotation_id')
		->innerJoin('buildings', 'buildings.id = crm_received_properties.building_info')
		->where(['crm_quotations.client_name' => $model->client_name])
		// ->andWhere(['crm_received_properties.building_info' => $building_id])
		// ->andWhere(['crm_quotations.converted' => 1])	
		->andWhere(['crm_received_properties.property_id' => $property_id])
		->andWhere(['buildings.city' => $city_id])
        ->andWhere(['not', ['crm_quotations.approved_date' => null]])
		->andFilterWhere(['between', 'crm_quotations.created_at', $start_date, $end_date])
		->one();
		return $query;
	}


	public function getHoursDiff($model)
	{

// 		$datetime1 = new \DateTime($model->instruction_date.' '.$model->inquiry_received_time);
// 		$datetime2 = new \DateTime($model->created_at);
// 		$interval = $datetime1->diff($datetime2);
// 		$hours = $interval->days * 24 + $interval->h;

		$datetime1 = new \DateTime($model->instruction_date . ' ' . $model->inquiry_received_time);
		$datetime2 = new \DateTime($model->created_at);
		$interval = $datetime1->diff($datetime2);
		$hours = $interval->days * 24 + $interval->h + $interval->i / 60; // Calculate hours including minutes
		if ($hours < 1) {
			$hours = round($hours, 2); // Round to 2 decimal places if less than 1 hour
		}
		
		return number_format($hours,2);
		

	}
	
	
	public function getMaximaRecommendedFee($model)
	{
		return (new \yii\db\Query())->select('SUM(recommended_fee)')->from('crm_received_properties')->where(['quotation_id'=>$model->id])->scalar();
	}

	public function getBoRecommendedFee($model)
	{
		return (new \yii\db\Query())->select('SUM(bo_recommended_fee)')->from('crm_received_properties')->where(['quotation_id'=>$model->id])->scalar();
	}
	public function getReviewedFee($model)
	{
		return (new \yii\db\Query())->select('SUM(reviewed_fee)')->from('crm_received_properties')->where(['quotation_id'=>$model->id])->scalar();
	}

	public function getApprovedFee($model)
	{
		return (new \yii\db\Query())->select('SUM(quotation_fee)')->from('crm_received_properties')->where(['quotation_id'=>$model->id])->scalar();
	}
	

	public function getSubWords($string, $word_number){
		$words = explode(" ", $string);
		$word_count = count($words);
		// Take only the first three words
		$limitedWords = array_slice($words, 0, $word_number);
		
		// Join the limited words back into a string
		$limitedString = implode(" ", $limitedWords);
		if($word_count > $word_number){
			return $limitedString.'...';
		}else {
			return $limitedString;
		}
	}


	public function getTypeOfServiceArr()
    {
    	return [
    		'1' => Yii::t('app', 'Valuation - Real Estate'),
    		'2' => Yii::t('app', 'Valuation - Plant Machinery Equipment'),
    		'3' => Yii::t('app', 'BCS'),
    	];
    }

	public function getIndustryList()
    {
        $industryList = \app\models\Industry::find()
            ->where(['status_verified' => 1])
            ->orderBy(['title' => SORT_ASC])
            ->all();

        return ArrayHelper::map($industryList, "id", "title");
    }

	public function getAssetComplexity()
    {
    	return [
    		'low' => Yii::t('app', 'Low'),
    		'medium' => Yii::t('app', 'Medium'),
    		'high' => Yii::t('app', 'High'),
    	];
    }


	public function getNormalizeUaePhoneNumber($number) {

		$cleanedNumber = preg_replace('/[^\d+]/', '', $number);

		// Check if the number starts with '0', '971', or '+971'
		if (preg_match('/^(2|3|4|5|6|0|971|\+971)/', $cleanedNumber)) {
			// Remove country code and leading zero if present
			if (substr($cleanedNumber, 0, 2) == '00') {
				$cleanedNumber = substr($cleanedNumber, 2);
			} elseif (substr($cleanedNumber, 0, 3) == '971') {
				$cleanedNumber = substr($cleanedNumber, 3);
			} elseif (substr($cleanedNumber, 0, 4) == '+971') {
				$cleanedNumber = substr($cleanedNumber, 4);
			}

			// If the number starts with '0', remove it
			if (substr($cleanedNumber, 0, 1) == '0') {
				$cleanedNumber = substr($cleanedNumber, 1);
			}else{
				$cleanedNumber = substr($cleanedNumber, 0);
			}

			// The final formatted number
			$formattedNumber = '+971-' . $cleanedNumber;
			return $formattedNumber;
		}
	
		// If the number doesn't match the specific patterns, return it as is
		return $number;
	}


	
	
	
}
	?>
