<?php
namespace app\components\helpers;

use app\models\AttributesData;
use app\models\AutoInvoiceNum;
use app\models\AutoMonthlyInvoiceNum;
use app\models\BrokerageInquires;
use app\models\Buaweightage;
use app\models\CrmQuotations;
use app\models\CrmReceivedProperties;
use app\models\Gfaweightage;
use app\models\OnlineValuation;
use app\models\OnlineValuationDocs;
use app\models\ScheduleInspection;
use app\models\SettingVal;
use app\models\Valuation;
use Yii;
use yii\base\Component;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use app\models\Setting;
use app\models\User;
use app\models\AdminGroup;
use app\models\Company;
use app\models\Country;
use app\models\Zone;
use app\models\CompanyFeeStructure;
use app\models\CompanyManager;
use app\models\ContactManager;
use app\models\LeadManager;
use app\models\Calendar;
use app\models\CustomFieldModule;
use app\models\Workflow;
use app\models\WorkflowModule;
use app\models\WorkflowStage;
use app\models\WorkflowServiceType;
use app\models\PredefinedList;
use app\models\ModuleEmail;
use app\models\ModuleNumber;
use app\models\ModuleCompany;
use app\models\ModuleAddress;
use app\models\ModuleTag;
use app\models\ModuleManager;
use QuickBooksOnline\API\DataService\DataService;
use app\models\Communities;
use app\models\SubCommunities;
use app\models\AciveZone;
use app\models\CrmScopeOfService;
use app\models\ValuationApproversData;
use DateTime;
use DateInterval;
use DatePeriod;
use app\models\ClientValuation;

class AppHelperFunction extends Component
{
    /**
     * Get settings value for a keyword
     */
    public static function getSetting($key)
    {
        $value = "";
        $model = Setting::find()->where("config_name='$key'")->asArray()->one();
        if ($model != null) {
            $value = $model['config_value'];
        }
        return $value;
    }

    public static function getValSetting($key)
    {
        $value = "";
        $model = SettingVal::find()->where("config_name='$key'")->asArray()->one();
        if ($model != null) {
            $value = $model['config_value'];
        }
        return $value;
    }

    /**
     * page sizes for grid pagination
     *
     * @return array
     */
    public function getPageSizeArray()
    {
        return [
            25, 50, 100
        ];
    }

    /**
     * All active Permission groups
     *
     * @return array
     */
    public function getPermissionGroupList()
    {
        return AdminGroup::find()
            ->select([
                'id',
                'title',
            ])
            ->where(['status' => 1, 'trashed' => 0])
            ->andWhere(['not in', 'id', 1])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }

    /**
     * All active Permission groups for dropdown
     *
     * @return array
     */
    public function getPermissionGroupListArr()
    {
        return ArrayHelper::map($this->permissionGroupList, "id", "title");
    }

    /**
     * All active Countries
     *
     * @return array
     */
    public function getCountryList()
    {
        return Country::find()
            ->select([
                'id',
                'title',
            ])
            ->where(['status' => 1, 'trashed' => 0])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }

    /**
     * All active Countries for dropdown
     *
     * @return array
     */
    public function getCountryListArr()
    {
        return ArrayHelper::map($this->countryList, "id", "title");
    }

    /**
     * All active Zones
     *
     * @return array
     */
    public function getZoneList()
    {
        return Zone::find()
            ->select([
                'id',
                'title',
            ])
            ->where(['status' => 1, 'trashed' => 0])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }

    /**
     * All active Zones for dropdown
     *
     * @return array
     */
    public function getZoneListArr()
    {
        return ArrayHelper::map($this->zoneList, "id", "title");
    }

    /**
     * All active Zones
     *
     * @return array
     */
    public function getCountryZoneList($country_id)
    {
        return Zone::find()
            ->select([
                'id',
                'title',
            ])
            ->where(['country_id' => $country_id, 'status' => 1, 'trashed' => 0])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }

    /**
     * All active Zones for dropdown
     *
     * @return array
     */
    public function getCountryZoneListArr($country_id)
    {
        return ArrayHelper::map($this->getCountryZoneList($country_id), "id", "title");
    }

    /**
     * All active Emirates
     *
     * @return array
     */
    public function getEmiratedList_old()
    {
        $uae_country_id = $this->getSetting('emirates_country_id');
        return Zone::find()
            ->select([
                'id',
                'title',
            ])
            ->where(['country_id' => $uae_country_id, 'status' => 1, 'trashed' => 0])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }
    public function getEmiratedList()
    {

        return AciveZone::find()
            ->select([
                AciveZone::tableName().'.zone_id as id',
                Zone::tableName().'.title as title',
            ])
            ->leftJoin(Zone::tableName(),Zone::tableName().'.id='.AciveZone::tableName().'.zone_id')
            ->where([
                Zone::tableName().'.status' => 1,
                Zone::tableName().'.trashed' => 0,
            ])
            ->asArray()
            ->all();
    }
    /**
     * All active Emirates for dropdown
     *
     * @return array
     */
    public function getEmiratedListArr()
    {
        return ArrayHelper::map($this->emiratedList, "id", "title");
    }

    /**
     * All predefined lists
     *
     * @return array
     */
    public function getPredefinedList()
    {
        return PredefinedList::find()
            ->select([
                'id',
                'title',
            ])
            ->where(['parent' => 0, 'status' => 1, 'trashed' => 0])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }

    /**
     * All predefined for dropdown
     *
     * @return array
     */
    public function getPredefinedListArr()
    {
        return ArrayHelper::map($this->predefinedList, "id", "title");
    }

    /**
     * All predefined lists
     *
     * @return array
     */
    public function getPredefinedListOptions($id)
    {
        return PredefinedList::find()
            ->select([
                'id',
                'title',
            ])
            ->where(['parent' => $id, 'status' => 1, 'trashed' => 0])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }

    /**
     * All predefined for dropdown
     *
     * @return array
     */
    public function getPredefinedListOptionsArr($id)
    {
        return ArrayHelper::map($this->getPredefinedListOptions($id), "id", "title");
    }

    /**
     * predefined list option by id
     *
     * @return array
     */
    public function getPredefinedListOption($id)
    {
        return PredefinedList::find()
            ->select([
                'id',
                'title',
            ])
            ->where(['id' => $id, 'status' => 1, 'trashed' => 0])
            ->asArray()->one();
    }

    /**
     * All active Emirates
     *
     * @return array
     */
    public function getStaffMemberList()
    {
        return User::find()
            ->select([
                'id',
                'full_name' => 'CONCAT(firstname," ",lastname)',
            ])
            ->where(['user_type' => 10, 'status' => 1, 'trashed' => 0])
         //   ->where(['permission_group_id' => 3, 'status' => 1, 'trashed' => 0])
            ->orderBy(['full_name' => SORT_ASC])->asArray()->all();
    }
    public function getStaffMemberListArrLastName()
    {
        return ArrayHelper::map($this->staffMemberListLastName, "id", "lastname");
    }
    public function getStaffMemberListLastName()
    {
        return User::find()
            ->select([
                'id',
                'lastname'
            ])
            ->where([ 'status' => 1, 'trashed' => 0])
            ->where(['in', 'user_type', [9,10]])
            ->where(['in', 'permission_group_id', [3,9]])
               ->where(['status' => 1, 'trashed' => 0])
            ->orderBy(['lastname' => SORT_ASC])->asArray()->all();
    }


    public function getStaffMemberListArrLastNameservice()
    {
        return ArrayHelper::map($this->staffMemberListLastNameservice, "id", "lastname");
    }
    public function getStaffMemberListLastNameservice()
    {
        return User::find()
            ->select([
                'id',
                'lastname'
            ])
            ->where(['user_type' => 10, 'status' => 1, 'trashed' => 0])
               ->where(['permission_group_id' => 3, 'status' => 1, 'trashed' => 0])
            ->orderBy(['lastname' => SORT_ASC])->asArray()->all();
    }

    /**
     * All active Emirates for dropdown
     *
     * @return array
     */
    public function getStaffMemberListArr()
    {
        return ArrayHelper::map($this->staffMemberList, "id", "full_name");
    }


    /**
     * All active Emirates
     *
     * @return array
     */
    public function getStaffMemberListBusiness()
    {
        return User::find()
            ->select([
                'id',
                'full_name' => 'CONCAT(firstname," ",lastname)',
            ])
            ->where(['permission_group_id' => [4,13], 'status' => 1, 'trashed' => 0])
            //   ->where(['permission_group_id' => 3, 'status' => 1, 'trashed' => 0])
            ->orderBy(['full_name' => SORT_ASC])->asArray()->all();
    }

    /**
     * All active Emirates for dropdown
     *
     * @return array
     */
    public function getStaffMemberListArrBusiness()
    {
        return ArrayHelper::map($this->staffMemberListBusiness, "id", "full_name");
    }
    public function getStaffMemberListArrBusinessLastname()
    {
        return ArrayHelper::map($this->staffMemberListBusiness, "id", "lastname");
    }

    /**
     * All active Emirates
     *
     * @return array
     */
    public function getMyCalendarList()
    {
        return Calendar::find()
            ->select([
                'id',
                'title',
            ])
            ->where(['created_by' => Yii::$app->user->identity->id, 'trashed' => 0])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }

    /**
     * All active Emirates for dropdown
     *
     * @return array
     */
    public function getMyCalendarListArr()
    {
        return ArrayHelper::map($this->myCalendarList, "id", "title");
    }

    /**
     * All email for a module
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModuleEmails($model)
    {
      return ModuleEmail::find()
      ->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])
      ->asArray()->all();
    }

    /**
     * All numbers for a module
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModuleNumbers($model)
    {
      return ModuleNumber::find()
      ->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])
      ->asArray()->all();
    }

    /**
     * All companies for a module
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModuleCompany($model)
    {
      return ModuleCompany::find()
      ->select([
        ModuleCompany::tableName().'.id',
        ModuleCompany::tableName().'.company_id',
        'company_name'=>'comp.title',
        ModuleCompany::tableName().'.role_id',
        'role_name'=>'jr.title',
        ModuleCompany::tableName().'.job_title_id',
        'job_title'=>'jt.title',
      ])
      ->leftJoin(Company::tableName().' as comp','comp.id='.ModuleCompany::tableName().'.company_id')
      ->leftJoin(PredefinedList::tableName().' as jr','jr.id='.ModuleCompany::tableName().'.role_id')
      ->leftJoin(PredefinedList::tableName().' as jt','jt.id='.ModuleCompany::tableName().'.job_title_id')
      ->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])
      ->asArray()->all();
    }

    /**
     * All companies for a module
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModuleCompanyById($type,$id)
    {
      return ModuleCompany::find()
      ->select([
        ModuleCompany::tableName().'.id',
        ModuleCompany::tableName().'.company_id',
        'company_name'=>'comp.title',
        ModuleCompany::tableName().'.role_id',
        'role_name'=>'jr.title',
        ModuleCompany::tableName().'.job_title_id',
        'job_title'=>'jt.title',
      ])
      ->leftJoin(Company::tableName().' as comp','comp.id='.ModuleCompany::tableName().'.company_id')
      ->leftJoin(PredefinedList::tableName().' as jr','jr.id='.ModuleCompany::tableName().'.role_id')
      ->leftJoin(PredefinedList::tableName().' as jt','jt.id='.ModuleCompany::tableName().'.job_title_id')
      ->where(['module_type'=>$type,'module_id'=>$id])
      ->asArray()->all();
    }

    /**
     * All Addresses for a module
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModuleAddress($model)
    {
      return ModuleAddress::find()
      ->select([
        ModuleAddress::tableName().'.id',
        ModuleAddress::tableName().'.is_primary',
        ModuleAddress::tableName().'.country_id',
        'country_name'=>Country::tableName().'.title',
        ModuleAddress::tableName().'.zone_id',
        'zone_name'=>Zone::tableName().'.title',
        ModuleAddress::tableName().'.city',
        ModuleAddress::tableName().'.phone',
        ModuleAddress::tableName().'.address',
      ])
      ->innerJoin(Country::tableName(),Country::tableName().".id=".ModuleAddress::tableName().".country_id")
      ->innerJoin(Zone::tableName(),Zone::tableName().".id=".ModuleAddress::tableName().".zone_id")
      ->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,ModuleAddress::tableName().'.trashed'=>0])
      ->asArray()->all();
    }

    /**
     * All fee structure options
     *
     * @return array
     */
    public function getFeeStructureOptionListArr()
    {
        return [
            '1' => Yii::t('app', 'Physical Inspection'),
            '2' => Yii::t('app', 'Desktop'),
            '3' => Yii::t('app', 'DriveBy'),
        ];
    }

    /**
     * Return fee structure row for a company, emirate and type
     *
     * @return array
     */
    public function getCompanyFeeStructureValues($company_id, $feeStructureId, $emirate_id, $type_id)
    {
        $fee = '';
        $tat = '';
        $row = CompanyFeeStructure::find()
            ->select(['fee', 'tat'])
            ->where(['company_id' => $company_id, 'fee_structure_type_id' => $feeStructureId, 'emirate_id' => $emirate_id, 'type_id' => $type_id])
            ->asArray()->one();
        if ($row != null) {
            $fee = $row['fee'];
            $tat = $row['tat'];
        }
        return ['fee' => $fee, 'tat' => $tat];
    }

    /**
     * List of Lead Types
     *
     * @return array
     */
    public function getLeadTypeListArr()
    {
        return [
            '1' => Yii::t('app', 'None'),
            '2' => Yii::t('app', 'Web'),
            '3' => Yii::t('app', 'In Person'),
            '4' => Yii::t('app', 'Phone'),
            '5' => Yii::t('app', 'Email'),
        ];
    }

    /**
     * List of Lead Sources
     *
     * @return array
     */
    public function getLeadSourceListArr()
    {
        return [
            '1' => Yii::t('app', 'None'),
            '2' => Yii::t('app', 'Google'),
            '3' => Yii::t('app', 'Facebook'),
            '4' => Yii::t('app', 'Walk In'),
        ];
    }

    /**
     * List of Lead Status
     *
     * @return array
     */
    public function getLeadStatusListArr()
    {
        return [
            '1' => Yii::t('app', 'Unassigned'),
            '2' => Yii::t('app', 'Assigned'),
            '3' => Yii::t('app', 'Accepted'),
            '4' => Yii::t('app', 'Working'),
            '5' => Yii::t('app', 'Dead'),
            '6' => Yii::t('app', 'Rejected'),
        ];
    }

    /**
     * List of Deal Status
     *
     * @return array
     */
    public function getDealStatusListArr()
    {
        return [
            '1' => Yii::t('app', 'Working'),
            '2' => Yii::t('app', 'Won'),
            '3' => Yii::t('app', 'Lost'),
        ];
    }

    /**
     * List of Deal Status
     *
     * @return array
     */
    public function getLeadStagesListArr()
    {
        return [
            '1' => Yii::t('app', 'Working'),
            '2' => Yii::t('app', 'Won'),
            '3' => Yii::t('app', 'Lost'),
        ];
    }

    /**
     * List of Property Documents
     *
     * @return array
     */
    public function getPropertiesDocumentsListArrold()
    {

        return [
            '1' => Yii::t('app', 'Signed Terms of Engagement'),
            '2' => Yii::t('app', 'Title/Lease Deed'),
       //     '3' => Yii::t('app', 'Property Lease Deed'),
            '4' => Yii::t('app', 'Land Lease Agreement'),
            '5' => Yii::t('app', 'Land Purchase Document'),
            '6' => Yii::t('app', 'MoU for sale'),
            '7' => Yii::t('app', 'Initial Contract for Sale'),
            '8' => Yii::t('app', 'Affection or Site Plan'),
            '9' => Yii::t('app', 'Floor Plans / Drawings approved by Municipality'),
            '10' => Yii::t('app', 'Building Permission Certificate'),
            '11' => Yii::t('app', 'Building Completion Certificate'),
            '12' => Yii::t('app', 'Contracting Agreement (including BOQ)'),
        //    '13' => Yii::t('app', 'Renovation Agreement (including BOQ)'),
            '14' => Yii::t('app', 'Civil Defence Certificate'),
            '15' => Yii::t('app', 'Lease/Tenancy Contracts'),
            '16' => Yii::t('app', 'Rent List Spreadsheet'),
            '17' => Yii::t('app', 'Utilities Connection Certificate/Bills'),
            '18' => Yii::t('app', 'Facilities management agreement'),
            '19' => Yii::t('app', 'Financing Agreement'),
            '20' => Yii::t('app', 'HVAC Maintenance Contract'),
            '21' => Yii::t('app', 'MEP Maintenance Contract'),
            '22' => Yii::t('app', 'Civil Maintenance Contract'),
            '23' => Yii::t('app', 'Security Contract'),
            '24' => Yii::t('app', 'Cleaning Contract'),
            '25' => Yii::t('app', 'Pest Control Agreement'),
            '26' => Yii::t('app', 'Insurance Policy'),
            '27' => Yii::t('app', 'Historical Financial Performance Statements'),
            '28' => Yii::t('app', '10 Years Financial Projections'),
            '29' => Yii::t('app', 'Average Daily Rate Performance Statement'),
            '30' => Yii::t('app', 'Average Daily Rate Projections Statement'),
            '31' => Yii::t('app', 'Average Occupancy Rate Performance Statement'),
            '32' => Yii::t('app', 'Average Occupancy Rate Projections Statement'),
       //     '33' => Yii::t('app', 'None'),
            '34' => Yii::t('app', 'Sale and Purchase Agreement'),
            '35' => Yii::t('app', 'GFA / BUA and NLA Measurements'),
           // '36' => Yii::t('app', 'REIDIN Database'),
       //     '37' => Yii::t('app', 'Contact Person Confirmation'),
       //     '38' => Yii::t('app', 'Propsearch'),
        //    '39' => Yii::t('app', 'Bayut Building Guide'),
         //   '40' => Yii::t('app', 'Inspection Confirmation'),
         //   '41' => Yii::t('app', 'Client Confirmation'),
          //  '42' => Yii::t('app', 'Client Email'),
          //  '43' => Yii::t('app', 'Land Department'),
          //  '44' => Yii::t('app', 'Google Earth'),
           // '45' => Yii::t('app', 'Bayut Area Guide'),
          //  '46' => Yii::t('app', 'Property Finder'),
       //     '47' => Yii::t('app', 'Floor Plans / Drawings Approved by Municipality'),
     //       '48' => Yii::t('app', 'Valuation Certificate from Land Department'),
            '49' => Yii::t('app', 'Operations and maintenance expenses spreadsheet'),
            '50' => Yii::t('app', 'Ejari contracts'),
            '51' => Yii::t('app', 'Building Insurance Policy'),
            '52' => Yii::t('app', 'Renovation/Upgrade Details And Expenditures List'),
     //      '53' => Yii::t('app', 'Lease Contract'),
      //      '54' => Yii::t('app', '10 year financial projection'),
            '55' => Yii::t('app', 'Vacant Units Spreadsheet'),
      //      '56' => Yii::t('app', 'Building Configuration Plans'),
            '57' => Yii::t('app', 'Capital expenditure spreadsheet for utilities'),
        ];
    }
    public function getPropertiesDocumentsListArr()
    {

        return [
            '1' => Yii::t('app', 'Signed Terms of Engagement'),
            '2' => Yii::t('app', 'Title Deed/Pre Title Deed/Lease Deed/Oqood/Initial Contract of Sale'),
            //  '3' => Yii::t('app', 'Property Lease Deed'),
            '4' => Yii::t('app', 'Land Lease Agreement'),
            '5' => Yii::t('app', 'Land Purchase Document'),
            '6' => Yii::t('app', 'MoU for sale'),
            '7' => Yii::t('app', 'Initial Contract for Sale'),
            '8' => Yii::t('app', 'Affection / Site Plan'),
            '9' => Yii::t('app', 'Floor Plans / Drawings approved by Municipality'),
            '10' => Yii::t('app', 'Building Permission Certificate'),
            '11' => Yii::t('app', 'Building Completion Certificate (BCC) and final as built Drawings / <br/>Building Permission Certificate and Approved Drawings, if BCC not available'),
            '12' => Yii::t('app', 'Contracting Agreement (including BOQ)'),
            //  '13' => Yii::t('app', 'Renovation Agreement (including BOQ)'),
            '14' => Yii::t('app', 'Civil Defence Certificate'),
            '15' => Yii::t('app', 'Lease/Tenancy Contracts'),
            '16' => Yii::t('app', 'Rent List Spreadsheet'),
            '17' => Yii::t('app', 'Utilities Connection Certificate/Bills'),
            '18' => Yii::t('app', 'Facilities management agreement'),
            '19' => Yii::t('app', 'Financing Agreement'),
            '20' => Yii::t('app', 'HVAC Maintenance Contract'),
            '21' => Yii::t('app', 'MEP Maintenance Contract'),
            '22' => Yii::t('app', 'Civil Maintenance Contract'),
            '23' => Yii::t('app', 'Security Contract'),
            '24' => Yii::t('app', 'Cleaning Contract'),
            '25' => Yii::t('app', 'Pest Control Agreement'),
            '26' => Yii::t('app', 'Insurance Policy'),
            '27' => Yii::t('app', 'Historical Financial Performance Statements'),
            '28' => Yii::t('app', 'Financial Statements (Historic 3Y, Current, Projections 10Y)'),
            '29' => Yii::t('app', 'Average Daily Rate Performance Statement'),
            '30' => Yii::t('app', 'Average Daily Rate Sheet (Historic, Current, Projected)'),
            '31' => Yii::t('app', 'Average Occupancy Rate Performance Statement'),
            '32' => Yii::t('app', 'Occupancy Rate Sheet (Historic, Current, Projected)'),
            //  '33' => Yii::t('app', 'None'),
            '34' => Yii::t('app', 'Sale and Purchase Agreement'),
            '35' => Yii::t('app', 'GFA / BUA and NLA Measurements'),
            //  '36' => Yii::t('app', 'REIDIN Database'),
            //  '37' => Yii::t('app', 'Contact Person Confirmation'),
            //  '38' => Yii::t('app', 'Propsearch'),
            //  '39' => Yii::t('app', 'Bayut Building Guide'),
            //  '40' => Yii::t('app', 'Inspection Confirmation'),
            //  '41' => Yii::t('app', 'Client Confirmation'),
            //  '42' => Yii::t('app', 'Client Email'),
            //  '43' => Yii::t('app', 'Land Department'),
            //  '44' => Yii::t('app', 'Google Earth'),
            //  '45' => Yii::t('app', 'Bayut Area Guide'),
            //  '46' => Yii::t('app', 'Property Finder'),
            //  '47' => Yii::t('app', 'Floor Plans / Drawings Approved by Municipality'),
            //  '48' => Yii::t('app', 'Valuation Certificate from Land Department'),
            '49' => Yii::t('app', 'Operations and Maintenance Expenses Spreadsheet'),
            '50' => Yii::t('app', 'Ejari Contracts'),
            '51' => Yii::t('app', 'Building Insurance Policy'),
            '52' => Yii::t('app', 'Renovation/Upgrade Details And Expenditures List'),
            //  '53' => Yii::t('app', 'Lease Contract'),
            //  '54' => Yii::t('app', '10 year financial projection'),
            '55' => Yii::t('app', 'Vacant Units Spreadsheet'),
            //  '56' => Yii::t('app', 'Building Configuration Plans'),
            '57' => Yii::t('app', 'Capital expenditure spreadsheet for utilities'),
            '58' => Yii::t('app', 'NOC from Developer and final as built drawings,<br/> If extended/improved'),
            '59' => Yii::t('app', 'NOC from DDA  and final as built drawings,<br/> If extended/improved'),
            '60' => Yii::t('app', 'Owners NOC, If he is not Client'),
            '61' => Yii::t('app', 'Completion Status Certificate from  Engineering Co,<br/> If under construction'),
            '62' => Yii::t('app', 'Operating Agreements'),
            '63' => Yii::t('app', 'Alcohol License'),
            '64' => Yii::t('app', 'Star Rating Certificated from DTCM'),
            '65' => Yii::t('app', 'Purchase Document of Land with Price'),
            '66' => Yii::t('app', 'Upgrade Development Contracting Contract'),
            '67' => Yii::t('app', 'Upgrade Development Consultancy Contract'),
            '68' => Yii::t('app', 'Rental Contracts  if Units are  > 1'),
            '69' => Yii::t('app', 'Service Charges Invoice'),
            '70' => Yii::t('app', 'Operations and Maintenance Expenses Contracts'),
            '71' => Yii::t('app', 'Purchase Inovices of Power, Gas, Water, Fire etc'),
            '72' => Yii::t('app', 'Development Costs if Owned Constructed'),
            '73' => Yii::t('app', 'Valuation Instruction Email Screenshot'),
            '74' => Yii::t('app', 'Developer/Consultant certificate'),
        ];
    }

    /**
     * List of Property Categories
     *
     * @return array
     */
    public function getPropertiesCategoriesListArr()
    {

        return [
            '1' => Yii::t('app', 'Residential'),
            '2' => Yii::t('app', 'Industrial'),
            '3' => Yii::t('app', 'Mixed Use'),
            '4' => Yii::t('app', 'Commercial'),
            '5' => Yii::t('app', 'Business'),
            '6' => Yii::t('app', 'Other')
        ];
    }

    public function getPropertiesCategoriesList()
    {

        return [
            '1' => Yii::t('app', 'Residential Unit'),
            '2' => Yii::t('app', 'Residential Project'),
            '3' => Yii::t('app', 'Residential Unit Land'),
            '4' => Yii::t('app', 'Residential Project Land'),
            '5' => Yii::t('app', 'Commercial Unit'),
            '6' => Yii::t('app', 'Commercial Project'),
            '7' => Yii::t('app', 'Commercial Unit Land'),
            '8' => Yii::t('app', 'Commercial Project Land'),
            '9' => Yii::t('app', 'Industrial Unit'),
            '10' => Yii::t('app', 'Industrial Project'),
            '11' => Yii::t('app', 'Industrial Unit Land'),
            '12' => Yii::t('app', 'Industrial Project Land'),
            '13' => Yii::t('app', 'Business Project'),
            '14' => Yii::t('app', 'Business Project Land'),
            '15' => Yii::t('app', 'Other')
        ];
    }



    /**
     * List of Valuation Approach
     *
     * @return array
     */
    public function getValuationApproachListArr()
    {

        return [
            '1' => Yii::t('app', 'Market Approach'),
            '2' => Yii::t('app', 'Income Approach'),
            '3' => Yii::t('app', 'Profit Approach'),
            '4' => Yii::t('app', 'Cost Approach')
        ];
    }


    /**
     * List of Property Source
     *
     * @return array
     */
    public function getPropertySourceListArr()
    {

        return [
            '1' => Yii::t('app', 'Property Finder'),
            '2' => Yii::t('app', 'Dubizzle'),
            '3' => Yii::t('app', 'Bayut'),
            '4' => Yii::t('app', 'Waseet'),
            '5' => Yii::t('app', 'Dubai-Rest'),
            '6' => Yii::t('app', 'Simsari'),
            '7' => Yii::t('app', 'Real Estate Agency'),
            '8' => Yii::t('app', 'Zoom Property'),
            '9' => Yii::t('app', 'Property ePortal'),
            '10' => Yii::t('app', 'Driven Properties'),
            '11' => Yii::t('app', 'Just Property'),
            '12' => Yii::t('app', 'Property Price'),
            '13' => Yii::t('app', 'Index'),
            '14' => Yii::t('app', 'Houza'),
            '15' => Yii::t('app', 'Bayut-Auto'),
            '16' => Yii::t('app', 'PF-Auto'),
            '17' => Yii::t('app', 'Wasalt'),
            '18' => Yii::t('app', 'Sanadak'),
        ];
    }

    /**
     * List of Location Conditions
     *
     * @return array
     */
    public function getLocationConditionsListArr()
    {

        return [
            '1' => Yii::t('app', 'Poor'),
            '2' => Yii::t('app', 'Below Average'),
            '3' => Yii::t('app', 'Average'),
            '4' => Yii::t('app', 'Good'),
            '5' => Yii::t('app', 'Very Good'),
        ];
    }

    /**
     * List of Property Placement
     *
     * @return array
     */
    public function getPropertyPlacementListArr()
    {

        return [
            '1.00' => Yii::t('app', 'Middle'),
            '2.00' => Yii::t('app', 'Corner'),
            '1.50' => Yii::t('app', 'Semi Corner'),
            '3.00' => Yii::t('app', 'Not Applicable'),
        ];
    }

    /**
     * List of Property Visibility
     *
     * @return array
     */
    public function getPropertyVisibilityListArr()
    {

        return [
            '1' => Yii::t('app', 'Back'),
            '2' => Yii::t('app', 'Front'),
            '3' => Yii::t('app', 'Not Applicable'),
        ];
    }

    /**
     * List of Property Exposure
     *
     * @return array
     */
    public function getPropertyExposureListArr()
    {

        return [
            '1.00' => Yii::t('app', 'Back to Back'),
            '1.50' => Yii::t('app', 'Semi Back to Back'),
            '2.00' => Yii::t('app', 'Single Row'),
            '3.00' => Yii::t('app', 'Not Applicable'),

        ];
    }

    /**
     * List of Listings Property Type
     *
     * @return array
     */
    public function getListingsPropertyTypeListArr()
    {

        return [
            '2M' => Yii::t('app', '2M'),
            '3M' => Yii::t('app', '3M'),
            '1E' => Yii::t('app', '1E'),
            '2E' => Yii::t('app', '2E'),
            '3E' => Yii::t('app', '3E'),
            'A' => Yii::t('app', 'A'),
            'B' => Yii::t('app', 'B'),
            'C' => Yii::t('app', 'C'),
            'D' => Yii::t('app', 'D'),
            '1' => Yii::t('app', '1'),
            '2' => Yii::t('app', '2'),
            '3' => Yii::t('app', '3'),
            '4' => Yii::t('app', '4'),
            '5' => Yii::t('app', '5'),
            '6' => Yii::t('app', '6'),
            '7' => Yii::t('app', '7'),
            '8' => Yii::t('app', '8'),
            '9' => Yii::t('app', '9'),
            '10' => Yii::t('app', '10'),
            'Not known' => Yii::t('app', 'Not known'),
        ];
    }

    /**
     * List of Property Condition
     *
     * @return array
     */
    public function getPropertyConditionListArr()
    {

        return [
            '1' => Yii::t('app', 'Very Good'),
            '2' => Yii::t('app', 'Good'),
            '3' => Yii::t('app', 'Average'),
            '4' => Yii::t('app', 'Not Good'),
            '5' => Yii::t('app', 'Poor'),
        ];
    }

    public function getPropertyConditionRatingtArr()
    {

        return [
            '1' => 5,
            '2' => 4,
            '3' => 3,
            '4' => 2,
            '5' => 1,
        ];
    }
    public function getPropertyConditionListArrMaster()
    {

        return [
            '5' => Yii::t('app', 'Very Good'),
            '4' => Yii::t('app', 'Good'),
            '3' => Yii::t('app', 'Average'),
            '2' => Yii::t('app', 'Not Good'),
            '1' => Yii::t('app', 'Poor'),
        ];
    }

    /**
     * List of Development Type
     *
     * @return array
     */
    public function getDevelopmentTypeListArr()
    {

        return [
            '1' => Yii::t('app', 'Standard'),
            '2' => Yii::t('app', 'Non-Standard'),
        ];
    }

    /**
     * List of Finished Status
     *
     * @return array
     */
    public function getFinishedStatusListArr()
    {

        return [
            '1' => Yii::t('app', 'Shell & Core'),
            '2' => Yii::t('app', 'Fitted'),
        ];
    }

    /**
     * List of Other Community Facilities
     *
     * @return array
     */
    public function getOtherCommunityFacilitiesListArr()
    {

        return [
            '1' => Yii::t('app', 'Park'),
            '2' => Yii::t('app', 'Party Hall'),
            '3' => Yii::t('app', 'Hospital'),
            '4' => Yii::t('app', 'School'),
        ];
    }

    /**
     * List of Other Property Views
     *
     * @return array
     */
    public function getOtherPropertyViewListArr()
    {

        return [
            '1' => Yii::t('app', 'Park View'),
            '2' => Yii::t('app', 'Sea View'),
            '3' => Yii::t('app', 'Marina View'),
            '4' => Yii::t('app', 'Community View'),
            '5' => Yii::t('app', 'Pool View'),
            '6' => Yii::t('app', 'Mountain View'),
            '7' => Yii::t('app', 'Lake View'),
            '8' => Yii::t('app', 'Partial Park View'),
            '9' => Yii::t('app', 'Partial Sea View'),
            '10' => Yii::t('app', 'Partial Marina View'),
            '11' => Yii::t('app', 'Partial Mountain View'),
            '12' => Yii::t('app', 'Partial Lake View'),
            '13' => Yii::t('app', 'Substandard View'),
            '14' => Yii::t('app', 'Poor View'),
            '15' => Yii::t('app', 'Park and Pool View'),
            '16' => Yii::t('app', 'Good View')
        ];
    }

    /**
     * List of List Levels
     *
     * @return array
     */
    public function getListingLevelsListArr()
    {

        return [
            '1' => Yii::t('app', 'Ground'),
            '2' => Yii::t('app', 'Ground + 1 Floor'),
            '3' => Yii::t('app', 'Ground + 2 Floor'),
            '4' => Yii::t('app', 'Ground + 3 Floor'),
            '5' => Yii::t('app', 'Ground + 4 Floor'),
            '6' => Yii::t('app', 'Ground + 5 Floor'),
            '7' => Yii::t('app', 'Ground + 6 Floor'),
            '8' => Yii::t('app', 'Ground + 7 Floor'),
            '9' => Yii::t('app', 'Ground + 8 Floor'),
            '10' => Yii::t('app', 'Ground + 9 Floor'),
            '11' => Yii::t('app', 'Ground + 10 Floor'),
        ];
    }

    /**
     * List of List Levels
     *
     * @return array
     */
    public function getSoldTypesListArr()
    {

        return [
            '1' => Yii::t('app', 'Ready Properties'),
            '2' => Yii::t('app', 'Off Plan Properties'),
        ];
    }

    /**
     * List of List Levels
     *
     * @return array
     */
    public function getBuildingTenureArr()
    {

        return [
            '1' => Yii::t('app', 'Non-Freehold or Freehold for GCC Nationalities Only'),
            '2' => Yii::t('app', 'Freehold'),
            '3' => Yii::t('app', 'Leasehold/Usufruct'),
        ];
    }

    /**
     * List of List Purpose of valuation
     *
     * @return array
     */
    public function getPurposeOfValuationArr()
    {

        return ArrayHelper::map(\app\models\ValuationPurposes::find()->where(['status' => 1])->all(), 'id', 'title');
       /* return [
            '1' => Yii::t('app', 'Secured Lending'),
            '2' => Yii::t('app', 'Non-Secured Lending'),
            '3' => Yii::t('app', 'Financial Reporting'),
            '4' => Yii::t('app', 'Strategic Management'),
            '5' => Yii::t('app', 'Internal Management'),
            '6' => Yii::t('app', 'Gift'),
            '7' => Yii::t('app', 'Visa Application'),
        ];*/
    }

    /**
     * Types of valuation
     *
     * @return array
     */
    public function getTypesOfValuationArr()
    {

        return [
            '1' => Yii::t('app', 'Inspection Based'),
            '2' => Yii::t('app', 'Drive By'),
            '3' => Yii::t('app', 'Desktop'),
            '4' => Yii::t('app', 'Partial Inspection'),
        ];
    }

    /**
     * Methods of valuation
     *
     * @return array
     */
    public function getMethodsOfValuationArr()
    {

        return [
            '1' => Yii::t('app', 'Market Approach'),
            '2' => Yii::t('app', 'Cost Approach'),
            '3' => Yii::t('app', 'Income Approach'),
            '4' => Yii::t('app', 'DCF'),
        ];
    }

    /**
     * Types of valuation
     *
     * @return array
     */
    public function geTypesOfValuationArr()
    {

        return [
            '1' => Yii::t('app', 'Inspection'),
            '2' => Yii::t('app', 'Desktop'),
            '3' => Yii::t('app', 'Comparative'),
            '4' => Yii::t('app', 'Drag'),
        ];
    }


    /**
     * List of List Inspection Types
     *
     * @return array
     */
    public function getInspectionTypeArr()
    {

        return [
            '2' => Yii::t('app', 'Physical Inspection'),
            '1' => Yii::t('app', 'Drive By'),
            '3' => Yii::t('app', 'Desktop'),
        ];
    }

    /**
     * List of Modes Of Transport
     *
     * @return array
     */
    public function getModesOfTransportArr()
    {

        return [
            '1' => Yii::t('app', 'Company Car'),
            '2' => Yii::t('app', 'Personal Car'),
            '3' => Yii::t('app', 'External Transport'),
        ];
    }

    /**
     * List of Modes Of Acquisition Methods
     *
     * @return array
     */
    public function getAcquisitionMethodsArr()
    {

        return [
            '1' => Yii::t('app', 'Granted'),
            '2' => Yii::t('app', 'Gifted'),
            '3' => Yii::t('app', 'Purchased'),
            '4' => Yii::t('app', 'Inherited'),
        ];
    }

    /**
     * List of Modes Of Property Defects
     *
     * @return array
     */
    public function getPropertyDefectsArr()
    {

        return [
            '4' => Yii::t('app', 'None'),
            '1' => Yii::t('app', 'Water Leakage'),
            '2' => Yii::t('app', 'Broken Tiles'),
            '3' => Yii::t('app', 'Broken Windows'),
            '5' => Yii::t('app', 'Cracked wall'),
            '6' => Yii::t('app', 'Rotting wood'),
            '7' => Yii::t('app', 'Broken appliance/s'),
            '8' => Yii::t('app', 'Paint defect'),
            '9' => Yii::t('app', 'Leaking ceiling'),
            '10' => Yii::t('app', 'Faulty door/s'),
            '11' => Yii::t('app', 'Faulty window/s'),
        ];
    }

    /**
     * List of Modes Of AC Types
     *
     * @return array
     */
    public function getAcTypesArr()
    {

        return [
            '1' => Yii::t('app', 'Split Units AC'),
            '2' => Yii::t('app', 'Window AC'),
            '3' => Yii::t('app', 'Central Chiller'),
            '4' => Yii::t('app', 'Split Ducted AC'),
        ];
    }

    /**
     * List of Media directories
     *
     * @return array
     */
    public function getMediaDirectoriesArr()
    {

        return [
            '1' => Yii::t('app', 'received-info'),
            '2' => Yii::t('app', 'configration'),
            '3' => Yii::t('app', 'other'),
        ];
    }

    /**
     * List of Configration Floor List
     *
     * @return array
     */
    public function getConfigrationFloorListArr()
    {

        return [
            '1' => Yii::t('app', 'Ground'),
            '2' => Yii::t('app', 'First'),
            '3' => Yii::t('app', 'Second'),
            '4' => Yii::t('app', 'Basement'),
        ];
    }

    /**
     * List of Configration Upgrades List
     *
     * @return array
     */
    public function getConfigrationUpgradesListArr()
    {

        return [
            '1' => Yii::t('app', '1'),
            '2' => Yii::t('app', '2'),
            '3' => Yii::t('app', '3'),
            '4' => Yii::t('app', '4'),
            '5' => Yii::t('app', '5'),
        ];
    }

    /**
     * List of Configration Upgrades List
     *
     * @return array
     */
    public function getConfigrationOverAllUpgradesListArr()
    {

        return [
            '1' => Yii::t('app', '1'),
            '2' => Yii::t('app', '2'),
            '3' => Yii::t('app', '3'),
            '4' => Yii::t('app', '4'),
            '5' => Yii::t('app', '5'),
        ];
    }

    /**
     * List of Enquiry Bedrooms
     *
     * @return array
     */
    public function getEnquiryBedroomsList()
    {

        return [
            '1' => Yii::t('app', '1-Bedroom'),
            '2' => Yii::t('app', '2-Bedroom'),
            '3' => Yii::t('app', '3-Bedroom'),
            '4' => Yii::t('app', '4-Bedroom'),
            '5' => Yii::t('app', '5-Bedroom'),
            '6' => Yii::t('app', '6-Bedroom'),
            '7' => Yii::t('app', '7-Bedroom'),
            '8' => Yii::t('app', '8-Bedroom'),
            '9' => Yii::t('app', '9-Bedroom'),
            '10' => Yii::t('app', '10-Bedroom'),
        ];
    }

    /**
     * List of Enquiry Landsize
     *
     * @return array
     */
    public function getEnquiryLandsizeList()
    {

        return [
            '0' => Yii::t('app', '0'),
            '500' => Yii::t('app', '500'),
            '1000' => Yii::t('app', '1000'),
            '1500' => Yii::t('app', '1500'),
            '2000' => Yii::t('app', '2000'),
            '2500' => Yii::t('app', '2500'),
            '3000' => Yii::t('app', '3000'),
            '3500' => Yii::t('app', '3500'),
            '4000' => Yii::t('app', '4000'),
            '4500' => Yii::t('app', '4500'),
            '5000' => Yii::t('app', '5000'),
            '5500' => Yii::t('app', '5500'),
            '6000' => Yii::t('app', '6000'),
            '6500' => Yii::t('app', '6500'),
            '7000' => Yii::t('app', '7000'),
            '7500' => Yii::t('app', '7500'),
            '8000' => Yii::t('app', '8000'),
            '8500' => Yii::t('app', '8500'),
            '9000' => Yii::t('app', '9000'),
            '9500' => Yii::t('app', '9500'),
            '10000' => Yii::t('app', '10000'),
        ];
    }

    /**
     * List of Enquiry BUA
     *
     * @return array
     */
    public function getEnquiryBUAList()
    {

        return [
            '0' => Yii::t('app', '0'),
            '500' => Yii::t('app', '500'),
            '1000' => Yii::t('app', '1000'),
            '1500' => Yii::t('app', '1500'),
            '2000' => Yii::t('app', '2000'),
            '2500' => Yii::t('app', '2500'),
            '3000' => Yii::t('app', '3000'),
            '3500' => Yii::t('app', '3500'),
            '4000' => Yii::t('app', '4000'),
            '4500' => Yii::t('app', '4500'),
            '5000' => Yii::t('app', '5000'),
            '5500' => Yii::t('app', '5500'),
            '6000' => Yii::t('app', '6000'),
            '6500' => Yii::t('app', '6500'),
            '7000' => Yii::t('app', '7000'),
            '7500' => Yii::t('app', '7500'),
            '8000' => Yii::t('app', '8000'),
            '8500' => Yii::t('app', '8500'),
            '9000' => Yii::t('app', '9000'),
            '9500' => Yii::t('app', '9500'),
            '10000' => Yii::t('app', '10000'),
        ];
    }

    /**
     * List of Enquiry Price
     *
     * @return array
     */
    public function getEnquiryPriceList()
    {

        return [
            '0' => Yii::t('app', '0'),
            '500000' => Yii::t('app', '500000'),
            '1000000' => Yii::t('app', '1000000'),
            '1500000' => Yii::t('app', '1500000'),
            '2000000' => Yii::t('app', '2000000'),
            '2500000' => Yii::t('app', '2500000'),
            '3000000' => Yii::t('app', '3000000'),
            '3500000' => Yii::t('app', '3500000'),
            '4000000' => Yii::t('app', '4000000'),
            '4500000' => Yii::t('app', '4500000'),
            '5000000' => Yii::t('app', '5000000'),
            '5500000' => Yii::t('app', '5500000'),
            '6000000' => Yii::t('app', '6000000'),
            '6500000' => Yii::t('app', '6500000'),
            '7000000' => Yii::t('app', '7000000'),
            '7500000' => Yii::t('app', '7500000'),
            '8000000' => Yii::t('app', '8000000'),
            '8500000' => Yii::t('app', '8500000'),
            '9000000' => Yii::t('app', '9000000'),
            '9500000' => Yii::t('app', '9500000'),
            '10000000' => Yii::t('app', '10000000'),
        ];
    }

    /**
     * List of Enquiry Price/Sqt
     *
     * @return array
     */
    public function getEnquiryPricePerSQTList()
    {

        return [
            '0' => Yii::t('app', '0'),
            '100' => Yii::t('app', '100'),
            '200' => Yii::t('app', '200'),
            '300' => Yii::t('app', '300'),
            '400' => Yii::t('app', '400'),
            '500' => Yii::t('app', '500'),
            '600' => Yii::t('app', '600'),
            '700' => Yii::t('app', '700'),
            '800' => Yii::t('app', '800'),
            '900' => Yii::t('app', '900'),
            '1000' => Yii::t('app', '1000'),
            '1100' => Yii::t('app', '1100'),
            '1200' => Yii::t('app', '1200'),
            '1300' => Yii::t('app', '1300'),
            '1400' => Yii::t('app', '1400'),
            '1500' => Yii::t('app', '1500'),
            '1600' => Yii::t('app', '1600'),
            '1700' => Yii::t('app', '1700'),
            '1800' => Yii::t('app', '1800'),
            '1900' => Yii::t('app', '1900'),
            '2000' => Yii::t('app', '2000'),

        ];
    }

    /**
     * List of Deal Status
     *
     * @return array
     */
    public function getDeriveMvValue()
    {
        return [
            'Shell & Core' => 1,
            'Fitted' => 2,
            'No' => 1,
            'Yes' => 2,
            'Corner' => 2,
            'Semi Corner' => 1.5,
            'Middle' => 1,
            'Not Applicable' => 0,
            'Single Row' => 2,
            'Back to Back' => 1,
            'Semi Back to Back'=>1.5,
            'Semi-Landscape' => 1.5,
            'Non-Freehold' => 1,
            'Leasehold' => 2,
            'Freehold' => 2,
            'Back' => 1,
            'Front' => 2,
            'Park View' => 4,
            'Sea View' => 5,
            'Marina View' => 4,
            'Community View' => 3,
            'Pool View' => 4,
            'Mountain View' => 4,
            'Partial Park View' => 4,
            'Partial Marina View' => 4,
            'Partial Mountain View' => 4,
            'Partial Lake View' => 4,
            'Pool and Park View' => 5,
            'Garden View' => 4,
        ];
    }


    /**
     * Get settings value for a keyword
     */
    public static function getBuaweightages($difference)
    {


        $weightage = -1;
        $all_buaweightages = Buaweightage::find()
            ->select([
                'id',
                'difference',
                'bigger_sp',
            ])
            ->orderBy(['id' => SORT_DESC])->asArray()->all();
        $buaweightages_filter = array();

        foreach ($all_buaweightages as $key => $record) {

            if ($key == 0 && $difference <= $record['difference']) {

                $weightage = $record['bigger_sp'];
                break;
            } else if ((($key + 1) == count($all_buaweightages))) {

                $weightage = $record['bigger_sp'];
                break;

            }else if(($difference > $all_buaweightages[$key]['difference']) && ($difference <= $all_buaweightages[$key + 1]['difference']) ){
                $weightage = $all_buaweightages[$key + 1]['bigger_sp'];
                break;
            }
        }


        return $weightage;
    }
    public static function getGfaweightages($difference)
    {


        $weightage = -1;
        $weightage = -1;
        $all_buaweightages = Gfaweightage::find()
            ->select([
                'id',
                'difference',
                'bigger_sp',
            ])
            ->orderBy(['id' => SORT_DESC])->asArray()->all();
        $buaweightages_filter = array();

        foreach ($all_buaweightages as $key => $record) {

         if(($difference > $all_buaweightages[$key]['difference']) && ($difference <= $all_buaweightages[$key]['difference_2']) ){
                $weightage = $all_buaweightages[$key]['bigger_sp'];
                break;
            }
        }


        return $weightage;
    }

    public function getUniqueReference_old()
    {
        $previous_record = Valuation::find()
            ->select([
                'id',
                'reference_number',
            ])
            ->orderBy(['id' => SORT_DESC])->asArray()->one();
        if ($previous_record <> null) {
            $prev_ref = explode("-", $previous_record['reference_number']);
            if(isset($prev_ref[3]) && $prev_ref[3] == 'V'){
                $previous_record_2 = Valuation::find()
                    ->select([
                        'id',
                        'reference_number',
                    ])->where(['id' => ($previous_record['id']-1)])
                    ->orderBy(['id' => SORT_DESC])->asArray()->one();

                if ($previous_record_2 <> null) {
                    $prev_ref_2 = explode("-", $previous_record_2['reference_number']);
                    return 'REV-' . date('Y') . '-' . ($prev_ref_2[2] + 1);
                }


            }
            return 'REV-' . date('Y') . '-' . ($prev_ref[2] + 1);

        } else {
            return 'REV-' . date('Y') . '-1';
        }

    }
    public function getUniqueReference()
    {
        $previous_record = Valuation::find()
            ->select([
                'id',
                'reference_number',
            ])
            ->orderBy(['id' => SORT_DESC])->asArray()->one();
        $prev_ref = explode("-", $previous_record['reference_number']);
        $new_refrence = $prev_ref[2];

        do {
            $new_refrence++;
            $previous_record = Valuation::find()
                ->select([
                    'id',
                    'reference_number',
                ])->where(['reference_number'=>('REV-' . date('Y') . '-' . ($new_refrence))])
                ->orderBy(['id' => SORT_DESC])->asArray()->one();

        } while ($previous_record != null);

        return 'REV-' . date('Y') . '-' . ($new_refrence);

    }

    public function getUniqueReferencePortfolio()
    {
        $previous_record = Valuation::find()
            ->select([
                'id',
                'ref_portfolio',
            ])
            ->orderBy(['id' => SORT_DESC])->asArray()->one();
        $prev_ref = explode("-", $previous_record['reference_number']);
        if($previous_record <> null) {
            $new_refrence = $prev_ref[2];
        }else{
            $new_refrence = 0;
        }
        do {
            $new_refrence++;
            $previous_record = Valuation::find()
                ->select([
                    'id',
                    'ref_portfolio',
                ])->where(['ref_portfolio'=>('REP-' . date('Y') . '-' . ($new_refrence))])
                ->orderBy(['id' => SORT_DESC])->asArray()->one();

        } while ($previous_record != null);

        return 'REP-' . date('Y') . '-' . ($new_refrence);

    }



    public function getUniqueReferencebr()
    {
        $previous_record = BrokerageInquires::find()
            ->select([
                'id',
                'reference_number',
            ])
            ->orderBy(['id' => SORT_DESC])->asArray()->one();
        $prev_ref = explode("-", $previous_record['reference_number']);
        $new_refrence = $prev_ref[2];

        do {
            $new_refrence++;
            $previous_record = Valuation::find()
                ->select([
                    'id',
                    'reference_number',
                ])->where(['reference_number'=>('REB-' . date('Y') . '-' . ($new_refrence))])
                ->orderBy(['id' => SORT_DESC])->asArray()->one();

        } while ($previous_record != null);

        return 'REB-' . date('Y') . '-' . ($new_refrence);

    }

    /**
     * List of Location Attributes
     *
     * @return array
     */
    public function getLocationAttributesValue()
    {
        return [
            'minutes_2' => '2 minutes',
            'minutes_5' => '5 minutes',
            'minutes_10' => '10 minutes',
            'minutes_15' => '15 minutes',
            'minutes_20' => '20 minutes',
        ];
    }
    /**
     * List of View Attributes
     *
     * @return array
     */
    public function getViewAttributesValue()
    {
        return [
            'none' => 'None',
            'partial' => 'Partial',
            'full' => 'Full',
        ];
    }
    /**
     * List of Deal Status
     *
     * @return array
     */
    public function getViewCommunityAttributesValue()
    {
        return [

            'open_area' => 'Open Area or Road View',
            'normal' => 'Community View',
            'close_buildings' => 'Close Buildings View',
        ];
    }


    /**
     * Get settings value for a keyword
     */
    public static function getLocationAttributes($inspect_data)
    {

        $location_attributes_data = 0;

        $row_location_highway_drive = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'drive_to_highway', 'key_name' => $inspect_data->location_highway_drive])
            ->one();

        $row_location_school_drive = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'drive_to_school', 'key_name' => $inspect_data->location_school_drive])
            ->one();

        $row_location_mall_drive = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'drive_to_commercial_area', 'key_name' => $inspect_data->location_mall_drive])
            ->one();

        $row_location_sea_drive = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'drive_to_special_landmark', 'key_name' => $inspect_data->location_sea_drive])
            ->one();

        $row_location_park_drive = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'drive_to_pool', 'key_name' => $inspect_data->location_park_drive])
            ->one();

        $location_attributes_data =  $row_location_highway_drive->value + $row_location_school_drive->value + $row_location_mall_drive->value + $row_location_sea_drive->value + $row_location_park_drive->value;

        return $location_attributes_data;
    }

    /**
     * Get settings value for a keyword
     */
    public static function getViewAttributes($inspect_data)
    {

        $view_attributes_data = 0;

        $row_view_community = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'community', 'key_name' => $inspect_data->view_community])
            ->one();

        $row_view_pool = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'pool_fountain', 'key_name' => $inspect_data->view_pool])
            ->one();

        $row_view_burj = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'burj', 'key_name' => $inspect_data->view_burj])
            ->one();

        $row_view_sea = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'sea', 'key_name' => $inspect_data->view_sea])
            ->one();

        $row_view_marina = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'marina', 'key_name' => $inspect_data->view_marina])
            ->one();

        $row_view_mountains = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'mountains', 'key_name' => $inspect_data->view_mountains])
            ->one();

        $row_view_lake = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'lake', 'key_name' => $inspect_data->view_lake])
            ->one();

        $row_view_golf_course = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'golf_course', 'key_name' => $inspect_data->view_golf_course])
            ->one();

        $row_view_park = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'park', 'key_name' => $inspect_data->view_park])
            ->one();

        $row_view_special = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'special_view', 'key_name' => $inspect_data->view_special])
            ->one();

        $view_attributes_data =  $row_view_community->value +
            $row_view_pool->value + $row_view_burj->value + $row_view_sea->value + $row_view_marina->value +
            $row_view_mountains->value + $row_view_lake->value + $row_view_golf_course->value
            + $row_view_park->value  +$row_view_special->value;

        if($view_attributes_data > 5){
            return 5;
        }else{
            return $view_attributes_data;
        }
    }

    /**
     * Get settings value for a keyword
     */
    public static function getUpgradeAttributes($inspect_data)
    {

        $upgrade_attributes_data = 0;
        $flooring = 0;
        $ceiling = 0;
        $speciality = 0;

        $standard_value = AttributesData::find()
            ->select(['value'])
            ->where(['type' => 'standard', 'key_name' => 'standard'])
            ->one();


        $bedrooms_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'bedrooms'])->all(), 'key_name', 'value');
        if($inspect_data->config_bedrooms <> null && !empty($inspect_data->config_bedrooms)) {
            foreach ($inspect_data->config_bedrooms as $bedkey => $bedroom) {

                if ($bedroom['flooring'] == 1) {
                    $flooring = $flooring + $bedrooms_attribute_data['flooring'];
                }

                if ($bedroom['ceilings'] == 1) {
                    $ceiling = $ceiling + $bedrooms_attribute_data['ceilings'];
                }

                if ($bedroom['speciality'] == 1) {
                    $speciality = $speciality + $bedrooms_attribute_data['speciality'];
                }

            }
        }

        $bathrooms_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'bathrooms'])->all(), 'key_name', 'value');
        if($inspect_data->config_bathrooms <> null && !empty($inspect_data->config_bathrooms)) {
            foreach ($inspect_data->config_bathrooms as $bathroom) {

                if ($bathroom['flooring'] == 1) {
                    $flooring = $flooring + $bathrooms_attribute_data['flooring'];
                }

                if ($bathroom['ceilings'] == 1) {
                    $ceiling = $ceiling + $bathrooms_attribute_data['ceilings'];
                }

                if ($bathroom['speciality'] == 1) {
                    $speciality = $speciality + $bathrooms_attribute_data['speciality'];
                }

            }
        }


        $kitchen_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'kitchen'])->all(), 'key_name', 'value');
        if($inspect_data->config_kitchen <> null && !empty($inspect_data->config_kitchen)) {
            foreach ($inspect_data->config_kitchen as $kitchen) {

                if ($kitchen['flooring'] == 1) {
                    $flooring = $flooring + $kitchen_attribute_data['flooring'];
                }

                if ($kitchen['ceilings'] == 1) {
                    $ceiling = $ceiling + $kitchen_attribute_data['ceilings'];
                }

                if ($kitchen['speciality'] == 1) {
                    $speciality = $speciality + $kitchen_attribute_data['speciality'];
                }

            }
        }

        $living_area_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'living_area'])->all(), 'key_name', 'value');
        if($inspect_data->config_living_area <> null && !empty($inspect_data->config_living_area)) {
            foreach ($inspect_data->config_living_area as $living_area) {

                if ($living_area['flooring'] == 1) {
                    $flooring = $flooring + $living_area_attribute_data['flooring'];
                }

                if ($living_area['ceilings'] == 1) {
                    $ceiling = $ceiling + $living_area_attribute_data['ceilings'];
                }

                if ($living_area['speciality'] == 1) {
                    $speciality = $speciality + $living_area_attribute_data['speciality'];
                }

            }
        }

        $dining_area_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'dining_area'])->all(), 'key_name', 'value');
        if($inspect_data->config_dining_area <> null && !empty($inspect_data->config_dining_area)) {
            foreach ($inspect_data->config_dining_area as $dining_area) {

                if ($dining_area['flooring'] == 1) {
                    $flooring = $flooring + $dining_area_attribute_data['flooring'];
                }

                if ($dining_area['ceilings'] == 1) {
                    $ceiling = $ceiling + $dining_area_attribute_data['ceilings'];
                }

                if ($dining_area['speciality'] == 1) {
                    $speciality = $speciality + $dining_area_attribute_data['speciality'];
                }

            }
        }

        $maid_rooms_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'maid_rooms'])->all(), 'key_name', 'value');
        if($inspect_data->config_maid_rooms <> null && !empty($inspect_data->config_maid_rooms)) {
            foreach ($inspect_data->config_maid_rooms as $maid_rooms) {

                if ($maid_rooms['flooring'] == 1) {
                    $flooring = $flooring + $maid_rooms_attribute_data['flooring'];
                }

                if ($maid_rooms['ceilings'] == 1) {
                    $ceiling = $ceiling + $maid_rooms_attribute_data['ceilings'];
                }

                if ($maid_rooms['speciality'] == 1) {
                    $speciality = $speciality + $maid_rooms_attribute_data['speciality'];
                }

            }
        }


        $laundry_area_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'laundry_area'])->all(), 'key_name', 'value');
        if($inspect_data->config_laundry_area <> null && !empty($inspect_data->config_laundry_area)) {
            foreach ($inspect_data->config_laundry_area as $laundry_area) {

                if ($laundry_area['flooring'] == 1) {
                    $flooring = $flooring + $laundry_area_attribute_data['flooring'];
                }

                if ($laundry_area['ceilings'] == 1) {
                    $ceiling = $ceiling + $laundry_area_attribute_data['ceilings'];
                }

                if ($laundry_area['speciality'] == 1) {
                    $speciality = $speciality + $laundry_area_attribute_data['speciality'];
                }

            }
        }


        $store_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'store'])->all(), 'key_name', 'value');
        if($inspect_data->config_store <> null && !empty($inspect_data->config_store)) {
            foreach ($inspect_data->config_store as $store) {

                if ($store['flooring'] == 1) {
                    $flooring = $flooring + $store_attribute_data['flooring'];
                }

                if ($store['ceilings'] == 1) {
                    $ceiling = $ceiling + $store_attribute_data['ceilings'];
                }

                if ($store['speciality'] == 1) {
                    $speciality = $speciality + $store_attribute_data['speciality'];
                }

            }
        }

        $service_block_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'service_block'])->all(), 'key_name', 'value');
        if($inspect_data->config_service_block <> null && !empty($inspect_data->config_service_block)) {
            foreach ($inspect_data->config_service_block as $service_block) {

                if ($service_block['flooring'] == 1) {
                    $flooring = $flooring + $service_block_attribute_data['flooring'];
                }

                if ($service_block['ceilings'] == 1) {
                    $ceiling = $ceiling + $service_block_attribute_data['ceilings'];
                }

                if ($service_block['speciality'] == 1) {
                    $speciality = $speciality + $service_block_attribute_data['speciality'];
                }

            }
        }

        $garage_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'garage'])->all(), 'key_name', 'value');
        if($inspect_data->config_garage <> null && !empty($inspect_data->config_garage)) {
            foreach ($inspect_data->config_garage as $garage) {

                if ($garage['flooring'] == 1) {
                    $flooring = $flooring + $garage_attribute_data['flooring'];
                }

                if ($garage['ceilings'] == 1) {
                    $ceiling = $ceiling + $garage_attribute_data['ceilings'];
                }

                if ($garage['speciality'] == 1) {
                    $speciality = $speciality + $garage_attribute_data['speciality'];
                }

            }
        }

        $balcony_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'balcony'])->all(), 'key_name', 'value');
        if($inspect_data->config_balcony <> null && !empty($inspect_data->config_balcony)) {
            foreach ($inspect_data->config_balcony as $balcony) {

                if ($balcony['flooring'] == 1) {
                    $flooring = $flooring + $balcony_attribute_data['flooring'];
                }

                if ($balcony['ceilings'] == 1) {
                    $ceiling = $ceiling + $balcony_attribute_data['ceilings'];
                }

                if ($balcony['speciality'] == 1) {
                    $speciality = $speciality + $balcony_attribute_data['speciality'];
                }

            }
        }

        $powder_room_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'powder_room'])->all(), 'key_name', 'value');
        if($inspect_data->config_powder_room <> null && !empty($inspect_data->config_powder_room)) {
            foreach ($inspect_data->config_powder_room as $powder_room) {

                if ($powder_room['flooring'] == 1) {
                    $flooring = $flooring + $powder_room_attribute_data['flooring'];
                }

                if ($powder_room['ceilings'] == 1) {
                    $ceiling = $ceiling + $powder_room_attribute_data['ceilings'];
                }

                if ($powder_room['speciality'] == 1) {
                    $speciality = $speciality + $powder_room_attribute_data['speciality'];
                }

            }
        }

        $study_room_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'study_room'])->all(), 'key_name', 'value');
        if($inspect_data->config_study_room <> null && !empty($inspect_data->config_study_room)) {
            foreach ($inspect_data->config_study_room as $study_room) {

                if ($study_room['flooring'] == 1) {
                    $flooring = $flooring + $study_room_attribute_data['flooring'];
                }

                if ($study_room['ceilings'] == 1) {
                    $ceiling = $ceiling + $study_room_attribute_data['ceilings'];
                }

                if ($study_room['speciality'] == 1) {
                    $speciality = $speciality + $study_room_attribute_data['speciality'];
                }

            }
        }

        $family_room_attribute_data=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'family_room'])->all(), 'key_name', 'value');
        if($inspect_data->config_family_room <> null && !empty($inspect_data->config_family_room)) {
            foreach ($inspect_data->config_family_room as $family_room) {

                if ($family_room['flooring'] == 1) {
                    $flooring = $flooring + $family_room_attribute_data['flooring'];
                }

                if ($family_room['ceilings'] == 1) {
                    $ceiling = $ceiling + $family_room_attribute_data['ceilings'];
                }

                if ($family_room['speciality'] == 1) {
                    $speciality = $speciality + $family_room_attribute_data['speciality'];
                }

            }
        }

        $upgrade_attributes_data = $standard_value->value + $flooring + $ceiling + $speciality;
        if($upgrade_attributes_data > 5){
            return 5;
        }else{
            return $upgrade_attributes_data;
        }
    }

    /**
     * List of Location Attributes
     *
     * @return array
     */
    public function getReportAttributesValueOther()
    {
        return [
            'Yes' => 'Yes - we are valuing the unit as unequipped as comparable are unequipped',
            'No' => 'No',
            'standard' => 'Yes - we are valuing the unit as standard equipped',
        ];
    }

    public function getReportAttributesValuefurnished()
    {
        return [
            'Yes' => 'Yes - we are valuing the unit as unfurnished',
            'No' => 'No',
            'standard' => 'Yes - we are valuing the unit as standard furnished',
        ];
    }

    public function getIncomeReportAttributesValueOther()
    {
        return [
            'Yes' => 'Yes',
            'No' => 'No',
            'standard' => 'Yes',
        ];
    }

    public function getIncomeReportAttributesValuefurnished()
    {
        return [
            'Yes' => 'Yes',
            'No' => 'No',
            'standard' => 'Yes',
        ];
    }

    public function getValuationScopeArr()
    {

        return [
            '1' => Yii::t('app', 'Estimated Market Value of the Subject Property'),
            '3' => Yii::t('app', 'Estimated Fair Value of the Subject Property'),
            '2' => Yii::t('app', 'Reinstatement Cost of the Subject Property'),
        ];
    }
    public function getValuationScopeAjmanArr()
    {

        return [
            '1' => Yii::t('app', 'Market Value of the Subject Property'),
            '3' => Yii::t('app', 'Fair Value of the Subject Property'),
            '2' => Yii::t('app', 'Reinstatement Cost of the Subject Property'),
        ];
    }
    public function getPurpose()
    {
        return ArrayHelper::map(\app\models\AttributesData::find()->where(['trashed' => 0])->all(), 'id', 'title');
        // print_r($id);
        // die();
       /* return [
            '1' => Yii::t('app', 'Secured Lending.'),
            '2' => Yii::t('app', 'Financial Reporting.'),
            '3' => Yii::t('app', 'Strategic Management.'),
            '4' => Yii::t('app', 'Internal Management.'),
            '5' => Yii::t('app', 'Purchase of Property.'),
            '6' => Yii::t('app', 'Intention to Sell.'),
            '7' => Yii::t('app', 'Intention to Buy.'),
            '8' => Yii::t('app', 'Internal Purpose.'),
            '9' => Yii::t('app', 'Court Proceedings.'),
            '10' => Yii::t('app', 'Golden Visa Application.'),
            '11' => Yii::t('app', 'Insurance Purpose.'),
            '12' => Yii::t('app', 'Gift'),
            '13' => Yii::t('app', 'Visa Application'),
        ];*/
    }

    /**
     * List of Calendar Event Types
     *
     * @return array
     */
    public function getCalenderEventTypeListArr()
    {

        return [
            'client' => Yii::t('app', 'Clients'),
            'contact' => Yii::t('app', 'Contacts'),
            'lead' => Yii::t('app', 'Leads'),
            'opportunity' => Yii::t('app', 'Opportunities'),
        ];
    }

    /**
     * List of Module Types
     *
     * @return array
     */
    public function getModuleTypeListArr()
    {

        return [
            'prospect' => Yii::t('app', 'Prospects'),
            // 'client' => Yii::t('app', 'Clients'),
            // 'contact' => Yii::t('app', 'Contacts'),
            // 'lead' => Yii::t('app', 'Leads'),
            'opportunity' => Yii::t('app', 'Opportunities'),
        ];
    }

    /**
     * List of Module Types
     *
     * @return array
     */
    public function getOlModulesListArr()
    {

        return [
            'prospect' => Yii::t('app', 'Prospects'),
            'client' => Yii::t('app', 'Clients'),
            'contact' => Yii::t('app', 'Contacts'),
        ];
    }

    /**
     * List of Module Types
     *
     * @return array
     */
    public function getWorkFlowModulesListArr()
    {

        return [
            'opportunity' => Yii::t('app', 'Opportunity'),
        ];
    }

    /**
     * List of Module Types
     *
     * @return array
     */
    public function getOlModulesSearchLabelsListArr()
    {

        return [
            'prospect' => Yii::t('app', 'Search Prospect'),
            'client' => Yii::t('app', 'Search Clients'),
            'contact' => Yii::t('app', 'Search Contacts'),
        ];
    }

    /**
     * List of Module Types
     *
     * @return array
     */
    public function modulesClassNames()
    {

        return [
            'prospect' => 'app\models\Prospect',
            'client' => 'app\models\Company',
            'contact' => 'app\models\User',
        ];
    }

    /**
     * List of Module Type Names for a workflow
     *
     * @return array
     */
    public function getCustomFieldModuleNames($id)
    {
      $listArr=$this->moduleTypeListArr;
      $namesArr=[];
      $results=CustomFieldModule::find()
      ->select(['module_type'])
      ->where(['custom_field_id'=>$id])
      ->asArray()->all();
      if($results!=null){
        foreach($results as $result){
          $namesArr[]=$listArr[$result['module_type']];
        }
      }
      return $namesArr;
    }

    /**
     * List of Module Type Names for a workflow
     *
     * @return array
     */
    public function getWorkflowModuleNames($id)
    {
      $listArr=$this->moduleTypeListArr;
      $namesArr=[];
      $results=WorkflowModule::find()
      ->select(['module_type'])
      ->where(['workflow_id'=>$id])
      ->asArray()->all();
      if($results!=null){
        foreach($results as $result){
          $namesArr[]=$listArr[$result['module_type']];
        }
      }
      return $namesArr;
    }

    /**
     * List of Service Type Names for a workflow
     *
     * @return array
     */
    public function getWorkflowServiceTypeNames($id)
    {
      $namesArr=[];
      $results=WorkflowServiceType::find()
      ->innerJoin(PredefinedList::tableName(),PredefinedList::tableName().".id=".WorkflowServiceType::tableName().".service_type")
      ->select(['title'])
      ->where(['workflow_id'=>$id])
      ->asArray()->all();
      if($results!=null){
        foreach($results as $result){
          $namesArr[]=$result['title'];
        }
      }
      return $namesArr;
    }

    /**
     * List of Stage Names for a workflow
     *
     * @return array
     */
    public function getWorkflowStageNames($id)
    {
      $arrList=[];
      $results=WorkflowStage::find()
      ->select(['title'])
      ->where(['workflow_id'=>$id,'trashed'=>0])
      ->asArray()->orderBy(['sort_order'=>SORT_ASC])->all();
      if($results!=null){
        foreach($results as $result){
          $arrList[]=$result['title'];
        }
      }
      return $arrList;
    }

    /**
     * List of Workflows for a keyword
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflowList($keyword)
    {
      $subQueryWithKeywords=WorkflowModule::find()->select(['workflow_id'])->where(['module_type'=>$keyword]);
      return Workflow::find()
      ->select(['id','title'])
      ->where(['id'=>$subQueryWithKeywords,'trashed'=>0])
      ->asArray()->all();
    }

    /**
     * List of Workflows for a keyword
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflowServiceStagesList($workflows,$service)
    {
      $subQueryWithKeywords=WorkflowServiceType::find()->select(['workflow_id'])->where(['service_type'=>$service]);
      return WorkflowStage::find()
      ->select(['id','title'])
      ->where(['workflow_id'=>$subQueryWithKeywords,'trashed'=>0])
      ->asArray()->all();
    }

    /**
     * List of Workflows Array for a keyword
     *
     * @return array
     */
    public function getWorkflowListArr($keyword)
    {
        return ArrayHelper::map($this->getWorkflowList($keyword), "id", "title");
    }

    /**
     * List of Workflow Stages for a list if ids
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflowStagesList($workfFlowIds)
    {
      return WorkflowStage::find()
      ->select(['id','workflow_id','title'])
      ->where(['workflow_id'=>$workfFlowIds,'trashed'=>0])
      ->asArray()->all();
    }

    /**
     * List of Workflow Stages for a list if ids
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKanbanWorkflowStagesList($workfFlowIds)
    {
      return WorkflowStage::find()
      ->select(['id','workflow_id','title','color_code','descp'])
      ->where(['workflow_id'=>$workfFlowIds,'trashed'=>0])
      ->orderBy(['sort_order'=>SORT_ASC])
      ->asArray()->all();
    }

	public function getPredefinedSubOptionsList($parent)
	{
		return PredefinedList::find()
		->select([
			'id',
			'title',
		])
		->where(['parent'=>$parent,'status'=>1,'trashed'=>0])
		->orderBy(['title'=>SORT_ASC])
		->asArray()
		->all();
	}

	public function getPredefinedOptionsItem($id)
	{
		return PredefinedList::find()
		->select([
			'id',
			'title',
		])
		->where(['id'=>$id,'status'=>1,'trashed'=>0])
		->orderBy(['title'=>SORT_ASC])
		->asArray()
		->one();
	}

    public function getClientList()
    {
        return \app\models\Company::find()
            ->select(['id','title'])
           // ->where(['data_type',0])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }

    //getting all banks for month & quarter
    public function getBankClients($year)
    {
        return \app\models\Company::find()
            ->select(['company.id','company.title','company.nick_name','COUNT(valuation.id) as val_count'])
            ->innerJoin('valuation', "valuation.client_id=company.id")
            ->where(['valuation.valuation_status' => 5])
            ->andWhere(['company.client_type' => 'bank'])
            ->andWhere(['IN' , 'YEAR(valuation.submission_approver_date)' , $year])
            ->orderBy(['val_count' => SORT_DESC])
            ->groupBy('company.id')
            ->asArray()->all();
    }

    //get all banks for only year
    public function getAllBankClients()
    {
        return \app\models\Company::find()
            ->select(['company.id','company.title','company.nick_name','COUNT(valuation.id) as val_count'])
            ->innerJoin('valuation', "valuation.client_id=company.id")
            ->where(['valuation.valuation_status' => 5])
            ->andWhere(['company.client_type' => 'bank'])
            ->orderBy(['val_count' => SORT_DESC])
            ->groupBy('company.id')
            ->asArray()->all();
    }

    //getting all corporate clients for months & quarters
    public function getCorporateClients($year)
    {

        return \app\models\Company::find()
            ->select(['company.id','company.title','company.nick_name','COUNT(valuation.id) as val_count'])
            ->innerJoin('valuation', "valuation.client_id=company.id")
            ->where(['valuation.valuation_status' => 5])
            ->andWhere(['company.client_type' => 'corporate'])
            ->andWhere(['IN' , 'YEAR(valuation.submission_approver_date)' , $year])
            ->orderBy(['val_count' => SORT_DESC])
            ->groupBy('company.id')
            ->asArray()->all();
    }

    //getting all corporate clients for year
    public function getAllCorporateClients()
    {

        return \app\models\Company::find()
            ->select(['company.id','company.title','company.nick_name','COUNT(valuation.id) as val_count'])
            ->innerJoin('valuation', "valuation.client_id=company.id")
            ->where(['valuation.valuation_status' => 5])
            ->andWhere(['company.client_type' => 'corporate'])
            ->orderBy(['val_count' => SORT_DESC])
            ->groupBy('company.id')
            ->asArray()->all();
    }

    //getting all banks for fee
    public function getFeeBankClients($year)
    {
        return \app\models\Company::find()
            ->select(['company.id','company.title','company.nick_name','SUM(valuation.total_fee) as val_fee'])
            ->innerJoin('valuation', "valuation.client_id=company.id")
            ->where(['valuation.valuation_status' => 5])
            ->andWhere(['company.client_type' => 'bank'])
            ->andWhere(['IN' , 'YEAR(valuation.submission_approver_date)' , $year])
            ->orderBy(['val_fee' => SORT_DESC])
            ->groupBy('company.id')
            ->asArray()->all();
    }

    public function getAllFeeBankClients()
    {
        return \app\models\Company::find()
            ->select(['company.id','company.title','company.nick_name','SUM(valuation.total_fee) as val_fee'])
            ->innerJoin('valuation', "valuation.client_id=company.id")
            ->where(['valuation.valuation_status' => 5])
            ->andWhere(['company.client_type' => 'bank'])
            ->orderBy(['val_fee' => SORT_DESC])
            ->groupBy('company.id')
            ->asArray()->all();
    }

    //getting all corporate clients for month-quarter fee
    public function getFeeCorporateClients($year)
    {

        return \app\models\Company::find()
            ->select(['company.id','company.title','company.nick_name','SUM(valuation.total_fee) as val_fee'])
            ->innerJoin('valuation', "valuation.client_id=company.id")
            ->where(['valuation.valuation_status' => 5])
            ->andWhere(['company.client_type' => 'corporate'])
            ->andWhere(['IN' , 'YEAR(valuation.submission_approver_date)' , $year])
            ->orderBy(['val_fee' => SORT_DESC])
            ->groupBy('company.id')
            ->asArray()->all();
    }

    public function getAllFeeCorporateClients()
    {

        return \app\models\Company::find()
            ->select(['company.id','company.title','company.nick_name','SUM(valuation.total_fee) as val_fee'])
            ->innerJoin('valuation', "valuation.client_id=company.id")
            ->where(['valuation.valuation_status' => 5])
            ->andWhere(['company.client_type' => 'corporate'])
            ->orderBy(['val_fee' => SORT_DESC])
            ->groupBy('company.id')
            ->asArray()->all();
    }

    //getting all revenues bank-wise
    public function getBankTotal($id , $month, $year)
    {
        // dd($id , $month);

        if($month == "01")
        {
            $start_date = $year.'-01-01'; 
            $end_date = $year.'-01-31';  
        }
        if($month == "02")
        {
            $start_date = $year.'-02-01'; 
            $end_date = $year.'-02-31';  
        }
        if($month == "03")
        {
            $start_date = $year.'-03-01'; 
            $end_date = $year.'-03-31';  
        }
        if($month == "04")
        {
            $start_date = $year.'-04-01'; 
            $end_date = $year.'-04-31';  
        }
        if($month == "05")
        {
            $start_date = $year.'-05-01'; 
            $end_date = $year.'-05-31';  
        }
        if($month == "06")
        {
            $start_date = $year.'-06-01'; 
            $end_date = $year.'-06-31';  
        }
        if($month == "07")
        {
            $start_date = $year.'-07-01'; 
            $end_date = $year.'-07-31';  
        }
        if($month == "08")
        {
            $start_date = $year.'-08-01'; 
            $end_date = $year.'-08-31';  
        }
        if($month == "09")
        {
            $start_date = $year.'-09-01'; 
            $end_date = $year.'-09-31';  
        }
        if($month == "10")
        {
            $start_date = $year.'-10-01'; 
            $end_date = $year.'-10-31';  
        }
        if($month == "11")
        {
            $start_date = $year.'-11-01'; 
            $end_date = $year.'-11-31';  
        }
        if($month == "12")
        {
            $start_date = $year.'-12-01'; 
            $end_date = $year.'-12-31';  
        }

        if($month == "13")
        {
            $start_date = $year.'-01-01'; 
            $end_date = $year.'-12-31';  
        }

        // dd($start_date , $end_date);

        $query =  (new \yii\db\Query())
        ->select('SUM(valuation.total_fee) as total_fee , COUNT(valuation.id) as total_valuations, SUM(estimated_market_value) as market_price,')
        ->from('valuation')
        ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
        ->where(['valuation_status' => 5])
        ->andWhere(['valuation_approvers_data.approver_type' => 'approver'])
        ->andWhere(['between', 'submission_approver_date', $start_date, $end_date])
        ->andWhere(['client_id' => $id])
        ->one();

        return $query;
    
        
    }

    //getting all revenues quarterly bank-wise
    public function getBankTotalQuarterly($id , $quarter, $year)
    {
        // dd($id , $month);

        if($quarter == "01")
        {
            $start_date = $year.'-01-01'; 
            $end_date = $year.'-03-31';  
        }
        if($quarter == "02")
        {
            $start_date = $year.'-04-01'; 
            $end_date = $year.'-06-31';  
        }
        if($quarter == "03")
        {
            $start_date = $year.'-07-01'; 
            $end_date = $year.'-09-31';  
        }
        if($quarter == "04")
        {
            $start_date = $year.'-010-01'; 
            $end_date = $year.'-12-31';  
        }

        if($quarter == "05")
        {
            $start_date = $year.'-01-01'; 
            $end_date = $year.'-12-31';  
        }

        // dd($start_date , $end_date);

        $query =  (new \yii\db\Query())
        ->select('SUM(total_fee) as total_fee , COUNT(*) as total_valuations, SUM(estimated_market_value) as market_price,')
        ->from('valuation')
        ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
        ->where(['valuation_status' => 5])
        ->andWhere(['valuation_approvers_data.approver_type' => 'approver'])
        ->andWhere(['between', 'submission_approver_date', $start_date, $end_date])
        ->andWhere(['client_id' => $id])
        ->one();

        return $query;
    
        
    }

    //getting all revenues yearly bank-wise
    public function getBankTotalYearly($id , $year)
    {

        $query =  (new \yii\db\Query())
        ->select('SUM(total_fee) as total_fee , COUNT(*) as total_valuations, SUM(estimated_market_value) as market_price,')
        ->from('valuation')
        ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
        ->where(['valuation_status' => 5])
        ->andWhere(['valuation_approvers_data.approver_type' => 'approver'])
        ->andWhere(['IN' , 'YEAR(submission_approver_date)' , $year])
        ->andWhere(['client_id' => $id])
        ->one();

        return $query;
        
    }

        //getting all revenues yearly bank-wise
    public function getBankTotalYearlyFee($id)
    {

        $query =  (new \yii\db\Query())
        ->select('SUM(total_fee) as total_fee , COUNT(*) as total_valuations, SUM(estimated_market_value) as market_price,')
        ->from('valuation')
        ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
        ->where(['valuation_status' => 5])
        ->andWhere(['valuation_approvers_data.approver_type' => 'approver'])
        ->andWhere(['client_id' => $id])
        ->one();

        return $query;
        
    }

    public function getClientListArr(){
        return ArrayHelper::map($this->clientList, "id", "title");
    }

    public function getReferenceList()
    {
        return \app\models\Valuation::find()
            ->select(['id','reference_number'])
            ->orderBy(['reference_number' => SORT_ASC])->asArray()->all();
    }

    public function getReferenceListArr(){
        return ArrayHelper::map($this->referenceList, "reference_number", "reference_number");
    }

    public function getBuildingList()
    {
        return \app\models\Buildings::find()
            ->select(['id','title'])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }
    public function getBuildingListArr(){
        return ArrayHelper::map($this->buildingList, "id", "title");
    }

    public function getCommunitiesList()
    {
        return \app\models\Communities::find()
            ->select(['id','title'])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }
    public function getCommunitiesListArr(){
        return ArrayHelper::map($this->communitiesList, "id", "title");
    }

    public function getSubCommunitiesList()
    {
        return \app\models\SubCommunities::find()
            ->select(['id','title'])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }
    public function getSubCommunitiesListArr(){
        return ArrayHelper::map($this->SubCommunitiesList, "id", "title");
    }

    public function getApproverList()
    {
        $subQuery = \app\models\Valuation::find()->where(['not', ['approval_id' => null]])->select('approval_id')->groupBy(['approval_id']);
        return \app\models\User::find()->where(['id'=>$subQuery])->select(['id','full_name' => 'CONCAT(firstname," ",lastname)'])->asArray()->all();
    }
    public function getApproverListArr(){
        return ArrayHelper::map($this->approverList, "id", "full_name");
    }
    public function getValuerList()
    {
        $subQuery = \app\models\Valuation::find()->where(['not', ['created_by' => null]])->select('created_by')->groupBy(['created_by']);
        return \app\models\User::find()->where(['id'=>$subQuery])->select(['id','full_name' => 'CONCAT(firstname," ",lastname)'])->asArray()->all();
    }
    public function getValuerListArr(){
        return ArrayHelper::map($this->valuerList, "id", "full_name");
    }

    public function getStepsSequence()
    {

        return [
            '1' => 1,
            '2' => 7,
            '3' => 2,
            '4' => 3,
            '5' => 8,
            '6' => 4,
            '7'=> 5
        ];
    }
    public function getClientRevenue($id)
    {
        $valuation=\app\models\Valuation::find()->where(['id'=>$id])->one();
        $fee = '';
        $tat = '';
        $inspection_type= 1;
        if($valuation->inspection_type == 1){
            $inspection_type = 3;
        }else if($valuation->inspection_type == 3){
            $inspection_type=2;
        }

        if ($valuation->tenure==2) {
            $row = \app\models\CompanyFeeStructure::find()
                ->select(['fee', 'tat'])
                ->where(['company_id' => $valuation->client_id, 'fee_structure_type_id' =>1, 'emirate_id' =>$valuation->building->city, 'type_id' => $inspection_type])
                ->asArray()->one();
        }
        if ($valuation->tenure<>null && $valuation->tenure!=2) {
            $row = \app\models\CompanyFeeStructure::find()
                ->select(['fee', 'tat'])
                ->where(['company_id' => $valuation->client_id, 'fee_structure_type_id' =>2, 'emirate_id' =>$valuation->building->city, 'type_id' => $inspection_type])
                ->asArray()->one();
        }

        if ($row != null) {
            $fee = $row['fee'];
        }
        return $fee;
    }
    public function getClientRevenueQoutaion($client_id,$inspection,$tenure, $city)
    {




        $fee = '';
        $tat = '';
        $inspection_type= 1;
        if($inspection == 1){
            $inspection_type = 3;
        }else if($inspection == 3){
            $inspection_type=2;
        }

        if ($tenure==2) {
            $row = \app\models\CompanyFeeStructure::find()
                ->select(['fee', 'tat'])
                ->where(['company_id' =>$client_id, 'fee_structure_type_id' =>1, 'emirate_id' =>$city, 'type_id' => $inspection_type])
                ->asArray()->one();
        }
        if ($tenure<>null && $tenure!=2) {
            $row = \app\models\CompanyFeeStructure::find()
                ->select(['fee', 'tat'])
                ->where(['company_id' => $client_id, 'fee_structure_type_id' =>2, 'emirate_id' =>$city, 'type_id' => $inspection_type])
                ->asArray()->one();
        }
    
        if ($row != null) {
            $fee = $row['fee'];
            $tat = $row['tat'];
        }
        return array('fee'=>$fee,'tat'=> $tat);
    }

    public function getDateArr()
    {

        return [
            '1' => Yii::t('app', '1 Month'),
            '2' => Yii::t('app', '2 Month'),
            '3' => Yii::t('app', '3 Month'),
            '4' => Yii::t('app', '4 Month'),
            '5' => Yii::t('app', '5 Month'),
            '6' => Yii::t('app', '6 Month'),
            '7' => Yii::t('app', '7 Month'),
            '8' => Yii::t('app', '8 Month'),
            '9' => Yii::t('app', '9 Month'),
            '10' => Yii::t('app', '10 Month'),
            '11' => Yii::t('app', '11 Month'),
            '12' => Yii::t('app', '1 Year'),
        ];
    }
    public function getRevisedReasons()
    {

        return [
            '1' => Yii::t('app', 'Low valuation challenge by client'),
            '6' => Yii::t('app', 'Value remains same'),
            '2' => Yii::t('app', 'High valuation challenge by client'),
            '3' => Yii::t('app', 'Error made'),
            '5' => Yii::t('app', 'Modification requested by client'),
            '4' => Yii::t('app', 'Copy previous valuation details'),
        ];
    }

    /**
     * returns array of time frame period
     * @return array
     */
    public function getReportPeriod()
    {
        return [
            '0' => Yii::t('app','All The Time'),
            '1' => Yii::t('app','Last 30 Days'),
            '2' => Yii::t('app','This Month'),
            '3' => Yii::t('app','This Quarter'),
            '4' => Yii::t('app','This Year'),
            '5' => Yii::t('app','Last Month'),
            '6' => Yii::t('app','Last Quarter'),
            '7' => Yii::t('app','Last Year'),
            '9' => Yii::t('app','Custom Dates'),

        ];
    }

    public function getReportPeriodCompare()
    {
        return [
            '0' => Yii::t('app','All The Time'),
            '1' => Yii::t('app','This Month - Last Month'),
            '2' => Yii::t('app','This Quarter - Last Quarter'),
            '3' => Yii::t('app','This Year - Last Year'),
            '4' => Yii::t('app','Custom Dates'),

        ];
    }

    public function getTatReportPeriod()
    {
        return [
            '0' => Yii::t('app','All The Time'),
            '1' => Yii::t('app','Last 30 Days'),
            '2' => Yii::t('app','This Month'),
            '3' => Yii::t('app','This Quarter'),
            '4' => Yii::t('app','This Year'),
            '5' => Yii::t('app','Last Month'),
            '6' => Yii::t('app','Last Quarter'),
            '7' => Yii::t('app','Last Year'),
            '8' => Yii::t('app','Today'),
            '9' => Yii::t('app','Custom Dates'),


        ];
    }

    public function getClientRevenueReport($client_id)
    {

        $valuations = \app\models\Valuation::find()
            ->select([
                'tenure',
                'client_id',
                'building_info',
                'inspection_type'
            ])
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.Valuation::tableName().'.client_id')
            ->where(['client_id' => $client_id])
            ->andWhere(['valuation_status' => 5])
            ->all();
        $total_fee = 0;

        foreach ($valuations as $valuation) {

            $fee = '';
            $inspection_type = 1;
            if ($valuation->inspection_type == 1) {
                $inspection_type = 3;
            } else if ($valuation->inspection_type == 3) {
                $inspection_type = 2;
            }

            if ($valuation->tenure == 2) {
                $row = \app\models\CompanyFeeStructure::find()
                    ->select(['fee'])
                    ->where(['company_id' => $valuation->client_id, 'fee_structure_type_id' => 1, 'emirate_id' => $valuation->building->city, 'type_id' => $inspection_type])
                    ->asArray()->one();
            }
            if ($valuation->tenure <> null && $valuation->tenure != 2) {
                $row = \app\models\CompanyFeeStructure::find()
                    ->select(['fee'])
                    ->where(['company_id' => $valuation->client_id, 'fee_structure_type_id' => 2, 'emirate_id' => $valuation->building->city, 'type_id' => $inspection_type])
                    ->asArray()->one();
            }

            if ($row != null) {
                $total_fee = $total_fee + $row['fee'];
            }
        }
        return $total_fee;
    }

    public function getClientRevenueReportAll()
    {
            $valuations = \app\models\Valuation::find()
                ->select([
                    'tenure',
                    'client_id',
                    'building_info',
                    'inspection_type'
                ])
                ->andWhere(['valuation_status' => 5])
                ->all();

            $total_fee = 0;

            foreach ($valuations as $valuation) {

                $fee = '';
                $inspection_type = 1;
                if ($valuation->inspection_type == 1) {
                    $inspection_type = 3;
                } else if ($valuation->inspection_type == 3) {
                    $inspection_type = 2;
                }

                if ($valuation->tenure == 2) {
                    $row = \app\models\CompanyFeeStructure::find()
                        ->select(['fee'])
                        ->where(['company_id' => $valuation->client_id, 'fee_structure_type_id' => 1, 'emirate_id' => $valuation->building->city, 'type_id' => $inspection_type])
                        ->asArray()->one();
                }
                if ($valuation->tenure <> null && $valuation->tenure != 2) {
                    $row = \app\models\CompanyFeeStructure::find()
                        ->select(['fee'])
                        ->where(['company_id' => $valuation->client_id, 'fee_structure_type_id' => 2, 'emirate_id' => $valuation->building->city, 'type_id' => $inspection_type])
                        ->asArray()->one();
                }

                if ($row != null) {
                    $total_fee = $total_fee + $row['fee'];
                }
            }

        return $total_fee;
    }

    public function getClientRevenueReportAllSynch()
    {



        $valuations = \app\models\Valuation::find()
            ->select([
                'id'=>Valuation::tableName() . '.id',
                'tenure',
                'client_id',
                'building_info',
                'inspection_type'
            ])
            ->all();

        $total_fee = 0;

        foreach ($valuations as $valuation) {

            $fee = '';
            $inspection_type = 1;
            if ($valuation->inspection_type == 1) {
                $inspection_type = 3;
            } else if ($valuation->inspection_type == 3) {
                $inspection_type = 2;
            }

            if ($valuation->tenure == 2) {
                $row = \app\models\CompanyFeeStructure::find()
                    ->select(['fee'])
                    ->where(['company_id' => $valuation->client_id, 'fee_structure_type_id' => 1, 'emirate_id' => $valuation->building->city, 'type_id' => $inspection_type])
                    ->asArray()->one();
            }
            if ($valuation->tenure <> null && $valuation->tenure != 2) {
                $row = \app\models\CompanyFeeStructure::find()
                    ->select(['fee'])
                    ->where(['company_id' => $valuation->client_id, 'fee_structure_type_id' => 2, 'emirate_id' => $valuation->building->city, 'type_id' => $inspection_type])
                    ->asArray()->one();
            }

            if ($row != null) {

                Yii::$app->db->createCommand()->update('valuation', ['fee' =>$row['fee']], 'id='.$valuation->id .'')->execute();


                $total_fee = $total_fee + $row['fee'];
            }
        }

        return $total_fee;
    }





    public function  getThisQuarter() {


        $current_month = date('m');
        $current_year = date('Y');
        if($current_month>=1 && $current_month<=3)
        {
            $start_date = strtotime('1-January-'.$current_year);  // timestamp or 1-Januray 12:00:00 AM
            $end_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM means end of 31 March
        }
        else  if($current_month>=4 && $current_month<=6)
        {
            $start_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM
            $end_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM means end of 30 June
        }
        else  if($current_month>=7 && $current_month<=9)
        {
            $start_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM
            $end_date = strtotime('1-October-'.$current_year);  // timestamp or 1-October 12:00:00 AM means end of 30 September
        }
        else  if($current_month>=10 && $current_month<=12)
        {
            $start_date = strtotime('1-October-'.$current_year);  // timestamp or 1-October 12:00:00 AM
            $end_date = strtotime('1-January-'.($current_year+1));  // timestamp or 1-January Next year 12:00:00 AM means end of 31 December this year
        }

        return array('start_date'=>$start_date, 'end_date'=> $end_date);


    }

    public function  getLastQuarter() {


        $current_month = date('m');
        $current_year = date('Y');

        if($current_month>=1 && $current_month<=3)
        {
            $start_date = strtotime('1-October-'.($current_year-1));  // timestamp or 1-October Last Year 12:00:00 AM
            $end_date = strtotime('1-January-'.$current_year);  // // timestamp or 1-January  12:00:00 AM means end of 31 December Last year
        }
        else if($current_month>=4 && $current_month<=6)
        {
            $start_date = strtotime('1-January-'.$current_year);  // timestamp or 1-Januray 12:00:00 AM
            $end_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM means end of 31 March
        }
        else  if($current_month>=7 && $current_month<=9)
        {
            $start_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM
            $end_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM means end of 30 June
        }
        else  if($current_month>=10 && $current_month<=12)
        {
            $start_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM
            $end_date = strtotime('1-October-'.$current_year);  // timestamp or 1-October 12:00:00 AM means end of 30 September
        }
        return array('start_date'=>$start_date, 'end_date'=> $end_date);


    }
public function  getFilterDates($period) {



    if ($period == 1) {

        $date_from = date('Y-m-d', strtotime('today - 30 days'));
        $date_to = date('Y-m-d');

    }else if($period == 2) {

        $date_from = date('Y-m-01');
       // $date_to = date('Y-m-t');
        $date_to = date('Y-m-d',strtotime("-1 days"));


    }else if($period== 3) {

        $date_array = $this->getThisQuarter();
        $date_from = date('Y-m-d', $date_array['start_date']);
        $date_to = date('Y-m-d',strtotime("-1 days"));
       // $date_to = date('Y-m-d', $date_array['end_date']);


    }else if($period== 4) {
        $date_from = date('Y-01-01');
        $date_to = date('Y-m-d',strtotime("-1 days"));
       // $date_to = date('Y-12-31');

    }else if($period == 5) {
        $date_from = date('Y-m-d', strtotime('first day of last month'));
        $date_to = date('Y-m-d', strtotime('last day of last month'));

    }else if($period == 6) {
        $date_array = $this->getLastQuarter();
        $date_from = date('Y-m-d', $date_array['start_date']);
        $date_to = date('Y-m-d', $date_array['end_date']);

    }else if($period == 7) {
        $last_year = date('Y') - 1;
        $date_from = date($last_year . '-01-01');
        $date_to = date($last_year . '-12-31');

    }else if($period == 8) {
        $date_from = date('Y-m-d');
        $date_to = date('Y-m-d');

    }

    return array('start_date'=>$date_from, 'end_date'=> $date_to);

}
    public function  getFilterDatesCompare($period) {



        if ($period == 1) {

            $date_from_this = date('Y-m-01');
            $date_to_this = date('Y-m-d',strtotime("-1 days"));
            $date_from_las = date('Y-m-d', strtotime('first day of last month'));
            $date_to_last = date('Y-m-d', strtotime('last day of last month'));

        }else if($period == 2) {

            $date_array = $this->getThisQuarter();
            $date_from_this = date('Y-m-d', $date_array['start_date']);
            $date_to_this = date('Y-m-d',strtotime("-1 days"));


            $date_array = $this->getLastQuarter();
            $date_from_last = date('Y-m-d', $date_array['start_date']);
            $date_to_last = date('Y-m-d', $date_array['end_date']);

        }else if($period== 3) {

            $date_from_this = date('Y-01-01');
            $date_to_this = date('Y-m-d',strtotime("-1 days"));
            $last_year = date('Y') - 1;
            $date_from_last= date($last_year . '-01-01');
            $date_to_last = date($last_year . '-12-31');


        }

        return array('start_date'=>$date_from_this, 'end_date'=> $date_to_this,'start_date_last'=>$date_from_last, 'end_date_last'=> $date_to_last);

    }

    public function  getRevenueTotal($period,$custom_period = null) {

        if($period > 0) {
            if($period == 9){
                $Date=(explode(" - ",$custom_period));
                $from_date = $Date[0];
                $to_date = $Date[1];
            }else{
                $date_array = $this->getFilterDates($period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $allvalues = (new \yii\db\Query())
                ->select('SUM(total_fee) as total_values,COUNT(id) as total_valuations_count')
                ->from('valuation')
                ->where(['valuation_status' => 5])
                ->andFilterWhere([
                    'between', 'submission_approver_date', $from_date. ' 00:00:00', $to_date. ' 23:59:59'
                    ])
                ->one();
        }else{
            $allvalues = (new \yii\db\Query())
                ->select('SUM(total_fee) as total_values,COUNT(id) as total_valuations_count')
                ->from('valuation')
                ->where(['valuation_status' => 5])
                ->one();
        }
        return $allvalues;


    }

    public function  getRevenueTotalByType($period,$custom_period = null) {
        if($period > 0) {
            if($period == 9){
                $Date=(explode(" - ",$custom_period));
                $from_date = $Date[0];
                $to_date = $Date[1];
            }else{
                $date_array = $this->getFilterDates($period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }
            $all_type_revenues = (new \yii\db\Query())
                ->select([
                    'client_revenue' => 'SUM(' . Valuation::tableName() . '.total_fee)',
                    'total_valuations_count' => 'COUNT(' . Valuation::tableName() . '.id)',
                    Company::tableName() . '.client_type',
                ])
                ->from('valuation')
                ->leftJoin(Company::tableName(), Valuation::tableName() . '.client_id=' . Company::tableName() . '.id')
                ->where(['valuation_status' => 5])
                ->andFilterWhere(['between', 'instruction_date', $from_date, $to_date])
                ->groupBy('company.client_type')->all();

            $all_Value_array = array();
            foreach ($all_type_revenues as $all_type_revenue) {
                $all_Value_array[$all_type_revenue['client_type']][0] = $all_type_revenue['client_revenue'];
                $all_Value_array[$all_type_revenue['client_type']][1] = $all_type_revenue['total_valuations_count'];
            }
            return $all_Value_array;
        }else {
            $all_type_revenues = (new \yii\db\Query())
                ->select([
                    'client_revenue' => 'SUM(' . Valuation::tableName() . '.total_fee)',
                    'total_valuations_count' => 'COUNT(' . Valuation::tableName() . '.id)',
                    Company::tableName() . '.client_type',
                ])
                ->from('valuation')
                ->leftJoin(Company::tableName(), Valuation::tableName() . '.client_id=' . Company::tableName() . '.id')
                // ->where(['company.client_type' => 'bank'])
                ->andWhere(['valuation_status' => 5])
                ->groupBy('company.client_type')->all();

            $all_Value_array = array();
            foreach ($all_type_revenues as $all_type_revenue) {
                $all_Value_array[$all_type_revenue['client_type']][0] = $all_type_revenue['client_revenue'];
                $all_Value_array[$all_type_revenue['client_type']][1] = $all_type_revenue['total_valuations_count'];
            }
            return $all_Value_array;
        }

    }

    public function  getNumberOfWorkingDays($startDate,$endDate)
    {
        $workingDays = 0;
        $startTimestamp = strtotime($startDate);
        $endTimestamp = strtotime($endDate);
        for ($i = $startTimestamp; $i <= $endTimestamp; $i = $i + (60 * 60 * 24)) {
            if (date("N", $i) <= 5) $workingDays = $workingDays + 1;
        }
        return $workingDays;
    }

    public function  getNumberOfWeekendDays($startDate,$endDate)
    {
        $weekendDays = 0;
        $startTimestamp = strtotime($startDate);
        $endTimestamp = strtotime($endDate);
        for ($i = $startTimestamp; $i <= $endTimestamp; $i = $i + (60 * 60 * 24)) {
            if (date("N", $i) > 5) $weekendDays = $weekendDays + 1;
        }
        return $weekendDays;
    }

    public function  getWeekends($period) {
        if($period > 0) {
            $date_array = $this->getFilterDates($period);
            $startDate = $date_array['start_date'];
            if(in_array( $period, [2,3,4])) {
                $endDate = date('Y-m-d');
            }else {
                $endDate = $date_array['end_date'];
            }

            // $workingDays = $this->getNumberOfWorkingDays($startDate, $endDate);
            $weekendDays = $this->getNumberOfWeekendDays($startDate, $endDate);


             if($weekendDays <> null){
               return $weekendDays;
            }else{
              return 0;
            }

        }

    }
    public function getReportPeriodNew()
    {
        return [
            '1' => Yii::t('app','All the Time '),
            '2' => Yii::t('app','Last 30 Days'),
            '3' => Yii::t('app','This Month'),
            '4' => Yii::t('app','This Quarter'),
            '5' => Yii::t('app','This Year'),
            '6' => Yii::t('app','Last Month'),
            '7' => Yii::t('app','Last Quarter'),
            '8' => Yii::t('app','Last Year'),
            '9' => Yii::t('app','Custom Dates'),

        ];
    }
    public function getReportPeriodNewtat()
    {
        return [
            '10' => Yii::t('app','All the Time'),
            '8' => Yii::t('app','Today'),
            '1' => Yii::t('app','Last 30 Days'),
            '2' => Yii::t('app','This Month'),
            '3' => Yii::t('app','This Quarter'),
            '4' => Yii::t('app','This Year'),
            '5' => Yii::t('app','Last Month'),
            '6' => Yii::t('app','Last Quarter'),
            '7' => Yii::t('app','Last Year'),
            '9' => Yii::t('app','Custom Dates'),

        ];
    }
    public function getPropertiesDocumentsListArr_old()
    {

        return [
            '1' => Yii::t('app', 'Signed Terms of Engagement'),
            '2' => Yii::t('app', 'Title Deed'),
            '3' => Yii::t('app', 'Property Lease Deed'),
            '4' => Yii::t('app', 'Land Lease Agreement'),
            '5' => Yii::t('app', 'Land Purchase Document'),
            '6' => Yii::t('app', 'MoU for sale'),
            '7' => Yii::t('app', 'Initial Contract for Sale'),
            '8' => Yii::t('app', 'Affection or Site Plan'),
            '9' => Yii::t('app', 'Floor Plans'),
            '10' => Yii::t('app', 'Building Permission Certificate'),
            '11' => Yii::t('app', 'Building Completion Certificate'),
            '12' => Yii::t('app', 'Contracting Agreement (including BOQ)'),
            '13' => Yii::t('app', 'Renovation Agreement (including BOQ)'),
            '14' => Yii::t('app', 'Civil Defence Certificate'),
            '15' => Yii::t('app', 'Tenancy Contracts'),
            '16' => Yii::t('app', 'Rent List Spreadsheet'),
            '17' => Yii::t('app', 'Utilities Connection Certificate'),
            '18' => Yii::t('app', 'Property Management/Service Contract'),
            '19' => Yii::t('app', 'Financing Agreement'),
            '20' => Yii::t('app', 'HVAC Maintenance Contract'),
            '21' => Yii::t('app', 'MEP Maintenance Contract'),
            '22' => Yii::t('app', 'Civil Maintenance Contract'),
            '23' => Yii::t('app', 'Security Contract'),
            '24' => Yii::t('app', 'Cleaning Contract'),
            '25' => Yii::t('app', 'Pest Control Agreement'),
            '26' => Yii::t('app', 'Insurance Policy'),
            '27' => Yii::t('app', 'Financial Performance Statements'),
            '28' => Yii::t('app', 'Financial Projections Statements'),
            '29' => Yii::t('app', 'Average Daily Rate Performance Statement'),
            '30' => Yii::t('app', 'Average Daily Rate Projections Statement'),
            '31' => Yii::t('app', 'Average Occupancy Rate Performance Statement'),
            '32' => Yii::t('app', 'Average Occupancy Rate Projections Statement'),
            '33' => Yii::t('app', 'None'),
            '34' => Yii::t('app', 'Sale and Purchase Agreement'),
            '35' => Yii::t('app', 'Measurements'),
            '36' => Yii::t('app', 'REIDIN Database'),
            '37' => Yii::t('app', 'Contact Person Confirmation'),
            '38' => Yii::t('app', 'Propsearch'),
            '39' => Yii::t('app', 'Bayut Building Guide'),
            '40' => Yii::t('app', 'Inspection Confirmation'),
            '41' => Yii::t('app', 'Client Confirmation'),
            '42' => Yii::t('app', 'Client Email'),
            '43' => Yii::t('app', 'Land Department'),
            '44' => Yii::t('app', 'Google Earth'),
            '45' => Yii::t('app', 'Bayut Area Guide'),
            '46' => Yii::t('app', 'Property Finder'),
            '47' => Yii::t('app', 'Approved Municipality Drawings'),
            '48' => Yii::t('app', 'Valuation Certificate from Land Department'),
        ];
    }

    public function getPropertiesDocumentsListArrPlotSource()
    {

        return [
            '2' => Yii::t('app', 'Title Deed'),
            '7' => Yii::t('app', 'Initial Contract for Sale'),
            '34' => Yii::t('app', 'Sale and Purchase Agreement'),
            '6' => Yii::t('app', 'MoU for sale'),
            '41' => Yii::t('app', 'Client Confirmation'),
            '40' => Yii::t('app', 'Inspection Confirmation'),
            '37' => Yii::t('app', 'Contact Person Confirmation'),
            '8' => Yii::t('app', 'Affection or Site Plan'),
            '4' => Yii::t('app', 'Land Lease Agreement'),
            '43' => Yii::t('app', 'Land Department'),
            '48' => Yii::t('app', 'Valuation Certificate from Land Department'),
            '33' => Yii::t('app', 'None'),
        ];
    }

    public function getPropertiesDocumentsListArrParkingSource()
    {

        return [
            '2' => Yii::t('app', 'Title Deed'),
            '7' => Yii::t('app', 'Initial Contract for Sale'),
            '34' => Yii::t('app', 'Sale and Purchase Agreement'),
            '6' => Yii::t('app', 'MoU for sale'),
            '41' => Yii::t('app', 'Client Confirmation'),
            '40' => Yii::t('app', 'Inspection Confirmation'),
            '37' => Yii::t('app', 'Contact Person Confirmation'),
            '8' => Yii::t('app', 'Affection or Site Plan'),
            '48' => Yii::t('app', 'Valuation Certificate from Land Department'),
            '33' => Yii::t('app', 'None'),
        ];
    }
    public function getPropertiesDocumentsListArrAgeSource()
    {

        return [
            '36' => Yii::t('app', 'REIDIN Database'),
            '2' => Yii::t('app', 'Title Deed'),
            '7' => Yii::t('app', 'Initial Contract for Sale'),
            '34' => Yii::t('app', 'Sale and Purchase Agreement'),
            '6' => Yii::t('app', 'MoU for sale'),
            '41' => Yii::t('app', 'Client Confirmation'),
            '37' => Yii::t('app', 'Contact Person Confirmation'),
            '8' => Yii::t('app', 'Affection or Site Plan'),
            '48' => Yii::t('app', 'Valuation Certificate from Land Department'),
            '44' => Yii::t('app', 'Google Earth'),
            '39' => Yii::t('app', 'Bayut Building Guide'),
            '45' => Yii::t('app', 'Bayut Area Guide'),
            '38' => Yii::t('app', 'Propsearch'),
            '46' => Yii::t('app', 'Property Finder'),
            '33' => Yii::t('app', 'None'),
            '50' => Yii::t('app', 'Building Completion Certificate'),
        ];
    }

    public function getPropertiesDocumentsListArrBuaSource()
    {

        return [
            '2' => Yii::t('app', 'Title Deed'),
            '7' => Yii::t('app', 'Initial Contract for Sale'),
            '34' => Yii::t('app', 'Sale and Purchase Agreement'),
            '6' => Yii::t('app', 'MoU for sale'),
            '41' => Yii::t('app', 'Client Confirmation'),
            '9' => Yii::t('app', 'Floor Plans'),
            '4' => Yii::t('app', 'Land Lease Agreement'),
            '40' => Yii::t('app', 'Inspection Confirmation'),
            '37' => Yii::t('app', 'Contact Person Confirmation'),
            '8' => Yii::t('app', 'Affection or Site Plan'),
            '48' => Yii::t('app', 'Valuation Certificate from Land Department'),
            '36' => Yii::t('app', 'REIDIN Database'),
            '39' => Yii::t('app', 'Bayut Building Guide'),
            '45' => Yii::t('app', 'Bayut Area Guide'),
            '38' => Yii::t('app', 'Propsearch'),
            '46' => Yii::t('app', 'Property Finder'),
            '49' => Yii::t('app', 'Building Completion Certificate'),
            '50' => Yii::t('app', 'Building Permit'),
            '33' => Yii::t('app', 'None')
        ];

    }

    public function getPropertiesDocumentsListArrExtensionSource()
    {

        return [
            '10' => Yii::t('app', 'Building Permission Certificate'),
            '37' => Yii::t('app', 'Contact Person Confirmation'),
            '47' => Yii::t('app', 'Approved Municipality Drawings'),
            '41' => Yii::t('app', 'Client Confirmation'),
            '33' => Yii::t('app', 'None'),
        ];
    }

    public function getRateAndItemAmount_old($company_id='',$city_id='',$type_id='',$tenure='')
    {
        $inspection_type = 1;
        if ($type_id == 1) {
            $inspection_type = 3;
        } else if ($type_id == 3) {
            $inspection_type = 2;
        }
        $type_id = $inspection_type;

        if ($tenure==2) {
            $rateAndItemAmount = \app\models\CompanyFeeStructure::find()->where(['company_id'=>$company_id,'emirate_id'=>$city_id,'type_id'=>$type_id,'fee_structure_type_id'=>1])->one();
            if ($rateAndItemAmount) {
                if ($rateAndItemAmount->fee>0) {
                    return $rateAndItemAmount->fee;
                }else{
                    return 0;
                }
            }else{
                return 0;
            }
        }else{
            $rateAndItemAmount = \app\models\CompanyFeeStructure::find()->where(['company_id'=>$company_id,'emirate_id'=>$city_id,'type_id'=>$type_id,'fee_structure_type_id'=>2])->one();
            if ($rateAndItemAmount<>null) {
                if ($rateAndItemAmount->fee>0) {
                    return $rateAndItemAmount->fee;
                }else{
                    return 0;
                }
            }else{
                return 0;
            }
        }
    }
    public function getRateAndItemAmount($company_id='',$city_id='',$type_id='',$tenure='',$p_type='',$urgency,$bua='',$plot_size='',$no_of_units='')
    {

        // dd($company_id,$city_id,$type_id,$tenure,$p_type,$urgency,$bua,$plot_size,$no_of_units);
        $client = \app\models\Company::find()->where(['id' => $company_id])->one();



        $fee = 0;
        $fee_recored_checl_all = \app\models\SlaMasterFile::find()
            ->where([
                    'client'=>$company_id,
                    // 'city'=>$city_id,
                    'inspection_type'=>$type_id,
                    //  'tenure'=>$tenure,
                    'property_type'=>$p_type,
                    'priority_type'=>$urgency
                ]
            )->one();




        if(($fee_recored_checl_all <> null) && $fee_recored_checl_all->city == 0){
            $city = 0;
        }else{
            $city = $city_id;
        }
        if($fee_recored_checl_all->tenure == 3){
            $tenure_check= 3;
        }else{
            $tenure_check = $tenure;
        }


        if($p_type == 1 || $p_type == 2 || $p_type == 6 || $p_type == 93 || $p_type == 17 || $p_type == 28 || $p_type == 4 || $p_type == 50 || $p_type == 53 || $p_type == 48 || $p_type == 39 || $p_type == 5 ){
            $check_value = $bua;
            $check_field = 'bua_to';
        }else if($p_type == 20 || $p_type == 24  || $p_type == 49  ){
            $check_value = $plot_size;
            $check_field = 'plot_size_to';
        }else if($p_type == 81 || $p_type == 59 || $p_type == 3 || $p_type == 42 || $p_type == 25 || $p_type == 9  ){
            $check_value = $no_of_units;
            $check_field = 'no_of_units_to';
        }else {
            $check_value = 0;
            $check_field = 'property_type';
        }





        if ($client->fee_with_bua_check) {
            // Start with all conditions
            $fee_recored = \app\models\SlaMasterFile::find()
                ->where([
                    'client' => $company_id,
                    'property_type' => $p_type,
                    'city' => $city,
                    'inspection_type' => $type_id,
                    'tenure' => $tenure_check,
                    'priority_type' => $urgency,
                ])
                ->andWhere(['>', $check_field, $check_value])
                ->one();

            if ($fee_recored == null) {
                // Remove city
                $fee_recored = \app\models\SlaMasterFile::find()
                    ->where([
                        'client' => $company_id,
                        'property_type' => $p_type,
                        'inspection_type' => $type_id,
                        'tenure' => $tenure_check,
                        'priority_type' => $urgency,
                    ])
                    ->andWhere(['>', $check_field, $check_value])
                    ->one();

                if ($fee_recored == null) {
                    // Remove tenure
                    $fee_recored = \app\models\SlaMasterFile::find()
                        ->where([
                            'client' => $company_id,
                            'property_type' => $p_type,
                            'city' => $city,
                            'inspection_type' => $type_id,
                            'priority_type' => $urgency,
                        ])
                        ->andWhere(['>', $check_field, $check_value])
                        ->one();

                    if ($fee_recored == null) {
                        // Remove priority_type
                        $fee_recored = \app\models\SlaMasterFile::find()
                            ->where([
                                'client' => $company_id,
                                'property_type' => $p_type,
                                'city' => $city,
                                'inspection_type' => $type_id,
                                'tenure' => $tenure_check,
                            ])
                            ->andWhere(['>', $check_field, $check_value])
                            ->one();

                        if ($fee_recored == null) {
                            // Remove city and tenure
                            $fee_recored = \app\models\SlaMasterFile::find()
                                ->where([
                                    'client' => $company_id,
                                    'property_type' => $p_type,
                                    'inspection_type' => $type_id,
                                    'priority_type' => $urgency,
                                ])
                                ->andWhere(['>', $check_field, $check_value])
                                ->one();

                            if ($fee_recored == null) {
                                // Remove city and priority_type
                                $fee_recored = \app\models\SlaMasterFile::find()
                                    ->where([
                                        'client' => $company_id,
                                        'property_type' => $p_type,
                                        'inspection_type' => $type_id,
                                        'tenure' => $tenure_check,
                                    ])
                                    ->andWhere(['>', $check_field, $check_value])
                                    ->one();

                                if ($fee_recored == null) {
                                    // Remove tenure and priority_type
                                    $fee_recored = \app\models\SlaMasterFile::find()
                                        ->where([
                                            'client' => $company_id,
                                            'property_type' => $p_type,
                                            'city' => $city,
                                            'inspection_type' => $type_id,
                                        ])
                                        ->andWhere(['>', $check_field, $check_value])
                                        ->one();

                                    if ($fee_recored == null) {
                                        // Remove city, tenure, and priority_type
                                        $fee_recored = \app\models\SlaMasterFile::find()
                                            ->where([
                                                'client' => $company_id,
                                                'property_type' => $p_type,
                                                'inspection_type' => $type_id,
                                            ])
                                            ->andWhere(['>', $check_field, $check_value])
                                            ->one();

                                        if ($fee_recored == null) {
                                            // Remove city, tenure, priority_type, and inspection_type
                                            $fee_recored = \app\models\SlaMasterFile::find()
                                                ->where([
                                                    'client' => $company_id,
                                                    'property_type' => $p_type,
                                                ])
                                                ->andWhere(['>', $check_field, $check_value])
                                                ->one();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else{


            $fee_recored = \app\models\SlaMasterFile::find()
                ->where([
                        'client'=>$company_id,
                        'city'=>$city,
                        'inspection_type'=>$type_id,
                        'tenure'=>$tenure_check,
                        'property_type'=>$p_type,
                        'priority_type'=>$urgency
                    ]
                )->one();

            // dd($fee_recored);


            if($fee_recored == null){

                $fee_recored = \app\models\SlaMasterFile::find()
                    ->where([
                            'client'=>$company_id,
                            'inspection_type'=>$type_id,
                            'tenure'=>$tenure_check,
                            'property_type'=>$p_type,
                            'priority_type'=>$urgency
                        ]
                    )->one();

                if($fee_recored == null){

                    $fee_recored = \app\models\SlaMasterFile::find()
                        ->where([
                                'client'=>$company_id,
                                'city'=>$city,
                                'inspection_type'=>$type_id,
                                'property_type'=>$p_type,
                                'priority_type'=>$urgency
                            ]
                        )->one();

                    if($fee_recored == null){

                        $fee_recored = \app\models\SlaMasterFile::find()
                            ->where([
                                    'client'=>$company_id,
                                    'city'=>$city,
                                    'inspection_type'=>$type_id,
                                    'tenure'=>$tenure_check,
                                    'priority_type'=>$urgency
                                ]
                            )->one();

                        if($fee_recored == null){

                            $fee_recored = \app\models\SlaMasterFile::find()
                                ->where([
                                        'client'=>$company_id,
                                        'city'=>$city,
                                        'inspection_type'=>$type_id,
                                        'priority_type'=>$urgency

                                    ]
                                )->one();

                            if($fee_recored == null){
                                $fee_recored = \app\models\SlaMasterFile::find()
                                    ->where([
                                            'client'=>$company_id,
                                            'inspection_type'=>$type_id,
                                            'priority_type'=>$urgency
                                        ]
                                    )->one();

                            }
                        }
                    }
                }
            }


        }

        if(isset($fee_recored) && ($fee_recored->fee > 0)){
            $fee  = $fee_recored->fee;
        }
        return $fee;
        // return $fee_recored->fee;

    }



    public function getDataServiceObject()
    {
        $ClientID = Yii::$app->appHelperFunctions->getSetting('quickbook_client_id');
        $ClientSecret = Yii::$app->appHelperFunctions->getSetting('quickbook_client_secret');
        $accessTokenKey = Yii::$app->appHelperFunctions->getSetting('quickbook_access_token');
        $refreshTokenKey = Yii::$app->appHelperFunctions->getSetting('quickbook_refresh_token');
        $QBORealmID = Yii::$app->appHelperFunctions->getSetting('quickbook_realm_id');

        return DataService::Configure(array(
            'auth_mode' => 'oauth2',
            'ClientID' => $ClientID,
            'ClientSecret' => $ClientSecret,
            'accessTokenKey' => $accessTokenKey,
            'refreshTokenKey' => $refreshTokenKey,
            'QBORealmID' => $QBORealmID,
            'baseUrl' => "Production"
        ));
    }

    public function refreshTokens($dataService)
    {
        $refreshTokenKey = Yii::$app->appHelperFunctions->getSetting('quickbook_refresh_token');

        $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
        $obj               = $OAuth2LoginHelper->refreshAccessTokenWithRefreshToken($refreshTokenKey);
        $newAccessToken    = $obj->getAccessToken();
        $newRefreshToken   = $obj->getRefreshToken();

        // update in database
        $queryAccessToken =  Yii::$app->db->createCommand()
            ->update('setting', ['config_value' => $newAccessToken], 'config_name = "quickbook_access_token"')
            ->execute();
        $queryRefreshToken =  Yii::$app->db->createCommand()
            ->update('setting', ['config_value' => $newRefreshToken], 'config_name = "quickbook_refresh_token"')
            ->execute();

        // return $dataService function
        return $this->getDataServiceObject();
    }
    public function getQbLocations()
    {

        return [
            '3506' => 'AbuDhabi',
            '3507' => 'Ajman',
            '3508' => 'Fujairah',
            '3509' => 'Sharjah',
            '3510' => 'Dubai',
            '3511' => 'Ras Al Khaimah',
            '3512' => 'Umm Al Quwain',
        ];
    }

    public function getQbLocationsTax()
    {

        return [
            '3506' => 6,
            '3507' => 7,
            '3508' => 3,
            '3509' => 13,
            '3510' => 12,
            '3511' => 8,
            '3512' => 11,
        ];
    }

    public function getBuaAndLandArea($propertyDescription=null, $MainLandOrBua=null, $no_of_bedrooms=null)
    {
//         echo "<pre>"; print_r($propertyDescription); echo "</pre><br>";  die();
//         echo "num of bedrooms: ".$no_of_bedrooms."<br>"; die();
        $totalArea = str_replace(',', '', $MainLandOrBua);
        $percentageValue = $totalArea*10/100;
        $addInTotalArea = $totalArea+$percentageValue;
        $removeInTotalArea = $totalArea-$percentageValue;

        $buaWords = [
            'BUA','built up','builtup','built-up','B U A','Building area','Building size','buildup','build up','B.U.A'
        ];
        $PlotWords = [
            'plot','land','total area','Plot'
        ];

        $built_up_area ='';
        $land_size ='';

        if (strpos($propertyDescription, 'BUA') !== false || strpos($propertyDescription, 'built up') !== false || strpos($propertyDescription, 'builtup') !== false || strpos($propertyDescription, 'built-up') !== false || strpos($propertyDescription, 'Building area') !== false || strpos($propertyDescription, 'Building size') !== false || strpos($propertyDescription, 'buildup') !== false || strpos($propertyDescription, 'build up') !== false || strpos($propertyDescription, 'B.U.A')) {
//             echo "Bua found in description<br>"; die();
            foreach ($buaWords as $word) {
                $findWord = strpos($propertyDescription, $word);
                if ($findWord<>null) {
                    $nextChar = substr($propertyDescription, $findWord, 20);
                    $val = preg_replace('/[^0-9-,-.]/', '', $nextChar);
                    $finalBUA = str_replace(',', '', $val);
                    if ($val<>null && $val>0) {
                        $built_up_area = $finalBUA;
                        $land_size     = $totalArea;
                    }else{
                        $built_up_area = $totalArea;
                    }
                }
            }
//            print_r($built_up_area); die();
        }
        else{
//             echo "bua not found in description<br>"; die();
            $findInPreviousListing = \app\models\ListingsTransactions::find()
                ->where(['no_of_bedrooms'=>$no_of_bedrooms])
                ->andWhere(['and',['>=', 'built_up_area', $removeInTotalArea],['<=', 'built_up_area', $addInTotalArea]])
                ->asArray()->all();
            // echo "<pre>"; print_r($findInPreviousListing); echo "</pre>"; die();
            if ($findInPreviousListing<>null) {
                $totalBuildUpArea = 0;
                foreach ($findInPreviousListing as $key => $value) {
                    $totalBuildUpArea += $value['built_up_area'];
                }
                $built_up_areaa = $totalBuildUpArea/count($findInPreviousListing);
                $built_up_area = number_format((float)$built_up_areaa, 2, '.', '');
                // echo "average built up area: ".$built_up_area."<br>";
            }else{
                $built_up_area = $totalArea;
                // echo "<br>BUA not found in range now Bua Is: ".$built_up_area."<br>";
            }
        }
        // echo "<br><br>"; // die();

        // plot area array loop

        if (strpos($propertyDescription, 'plot') !== false || strpos($propertyDescription, 'land') !== false || strpos($propertyDescription, 'total area') !== false || strpos($propertyDescription, 'Plot') !== false) {
//                echo "plot area fount in description"; die();
            $i=1;
            foreach ($PlotWords as $word) {
                // echo "i is equal to:- ".$i."<br>";
                $findWord = strpos($propertyDescription, $word);
                if ($findWord<>null) {
//                     echo "word find:- ".$word."<br>"; //die();
                    $nextChar = substr($propertyDescription, $findWord, 20);
                    $extraWords = strpos($nextChar, 'parks');
//                    echo $nextChar; echo "<br>";
//                    echo "Hello ".$extraWords; die();
                    if ($extraWords==null){
//                        echo "usaamamama"; //die();
                        $val = preg_replace('/[^0-9-,-.]/', '', $nextChar);
//                     echo "next char: ".$nextChar; echo "<br>";
//                     echo "val: ".$val; echo "<br>"; die();
                        $finalLandSize = str_replace(',', '', $val);
                        if ($val<>null && $val>0) {
                            // echo "val is >0:- ".$val."<br>";
                            $land_size = $finalLandSize;
                        }else{
                            $land_size = $totalArea;
                            // echo "else land size is total area: ".$land_size."<br>";
                        }
                    }

//


                }
                $i++;
            }
//            echo $land_size; die();
        }

        if($land_size==null){
//            echo "plot area not fount in description"; die();
            //if bua not found in description and plot area also not found in description then the area mentioned
            //in bayut is built up area.
            // $built_up_area = $totalArea;
            // echo "Word found but integer not found:- ".$word."<br>";
            $findInPreviousListing = \app\models\ListingsTransactions::find()
                ->where(['no_of_bedrooms'=>$no_of_bedrooms])
                ->andWhere(['and',['>=', 'land_size', $removeInTotalArea],['<=', 'land_size', $addInTotalArea]])
                ->asArray()->all();

            if ($findInPreviousListing<>null) {
                // echo "land size lie in bayut range<br>";
                $totalLandArea = 0;
                foreach ($findInPreviousListing as $key => $value) {
                    $totalLandArea += $value['built_up_area'];
                }
                $landArea = $totalLandArea/count($findInPreviousListing);
                $land_size = number_format((float)$landArea, 2, '.', '');
//                 echo "land size after range is:- ".$land_size."<br>";
            }
            else{
                $land_size = $totalArea;
                // echo "so land size is also equal to bayut area given in website: ".$land_size."<br>";
            }
        }



        //end plot area  loop
        return [
            'built_up_area'=>$built_up_area,
            'land_size'=>$land_size,
        ];

        // echo "<br><br>built up area: ".$built_up_area; echo "<br>";
        // echo "land size: ".$land_size; echo "<br>";
        // die();
    }



    public function getFullPageContent_old($listing_website_url_image=null)
    {
        $url = 'https://api.ocr.space/parse/imageurl?apikey=ce5c9417c088957&url='.$listing_website_url_image;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $json = json_decode($response, true);
        $html = $json['ParsedResults'][0]['ParsedText'];
        $needle = "AED";
        $lastPos = 0;
        $positions = array();

        while (($lastPos = strpos($html, $needle, $lastPos))!== false) {
            $positions[] = $lastPos;
            $lastPos = $lastPos + strlen($needle);
        }
        $listing_rent = '';
        foreach ($positions as $position) {
            // echo $position ."<br />";
            $nextChar = substr($html, $position, 18);
            if (strpos($nextChar, 'Yearly')!==false) {
                $AverageRent = preg_replace('/[^0-9-,-.]/', '', $nextChar);
                $listing_rent = str_replace(',', '', $AverageRent);
            }
            // echo "<pre>"; print_r($nextChar); echo "</pre><br><br>";
        }

        if ($listing_rent<>null && $listing_rent>0) {
            return $listing_rent;
        }else{
            return $listing_rent = 0;
        }


    }
    public function getFullPageContent_v1($listing_website_url_image=null)
    {

        $url = 'https://api.ocr.space/parse/imageurl?apikey=ce5c9417c088957&url='.$listing_website_url_image;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $json = json_decode($response, true);
        $agent_name = '';
        $Final_listing_rent = '';
        $listing_rent='';

        // echo "<pre>"; print_r($json); echo "</pre><br><br><br><br><br>"; die();
        if (isset($json['ParsedResults'][0]['ParsedText']) && $json['ParsedResults'][0]['ParsedText']<>null) {
            $html = $json['ParsedResults'][0]['ParsedText'];
            // echo "<pre>"; print_r($html); echo "</pre><br><br>";
            // die();

            $agent_name = $this->getAgentName($html);

            $needle = "AED";
            $lastPos = 0;
            $positions = array();

            while (($lastPos = strpos($html, $needle, $lastPos))!== false) {
                $positions[] = $lastPos;
                $lastPos = $lastPos + strlen($needle);
            }

            foreach ($positions as $position) {
                // echo $position ."<br />";
                $nextChar = substr($html, $position, 18);
                if (strpos($nextChar, 'Yearly')!==false) {
                    $AverageRent = preg_replace('/[^0-9-,-.]/', '', $nextChar);
                    $listing_rent = str_replace(',', '', $AverageRent);
                }
                // echo "<pre>"; print_r($nextChar); echo "</pre><br><br>";
            }
            // echo "listing rent:- ".$listing_rent; die();

            if ($listing_rent<>null && $listing_rent>0) {
                $Final_listing_rent =  $listing_rent;
            }else{
                $Final_listing_rent =  $listing_rent = 0;
            }
        }
        else{
           // echo "in else<br>";
            $Final_listing_rent  = 0;
        }

       //  echo "Listings Rent: ".$Final_listing_rent."<br>Agent name: ".$agent_name; die();
        return [
            'listings_rent' => $Final_listing_rent,
            'agent_name'    => $agent_name,
        ];


    }
    public function getFullPageContent_old_v2($listing_website_url_image=null)
    {
        $url = 'https://api.ocr.space/parse/imageurl?apikey=ce5c9417c088957&url='.$listing_website_url_image;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $json = json_decode($response, true);
        $agent_name = '';
        $Final_listing_rent = '';
        $listing_rent='';
        $listing_property_type='';

        // echo "<pre>"; print_r($json); echo "</pre><br><br><br><br><br>"; die();
        if (isset($json['ParsedResults'][0]['ParsedText']) && $json['ParsedResults'][0]['ParsedText']<>null) {
            $html = $json['ParsedResults'][0]['ParsedText'];
            // echo "<pre>"; print_r($html); echo "</pre><br><br>";


            $listing_property_type = $this->getListingPropertyType($html);


            $agent_name = $this->getAgentName($html);

            $needle = "AED";
            $lastPos = 0;
            $positions = array();

            while (($lastPos = strpos($html, $needle, $lastPos))!== false) {
                $positions[] = $lastPos;
                $lastPos = $lastPos + strlen($needle);
            }

            foreach ($positions as $position) {
                // echo $position ."<br />";
                $nextChar = substr($html, $position, 18);
                if (strpos($nextChar, 'Yearly')!==false) {
                    $AverageRent = preg_replace('/[^0-9-,-.]/', '', $nextChar);
                    $listing_rent = str_replace(',', '', $AverageRent);
                }
                // echo "<pre>"; print_r($nextChar); echo "</pre><br><br>";
            }
            // echo "listing rent:- ".$listing_rent; die();

            if ($listing_rent<>null && $listing_rent>0) {
                $Final_listing_rent =  $listing_rent;
            }else{
                $Final_listing_rent =  $listing_rent = 0;
            }
        }
        else{
            echo "in else<br>";
            $Final_listing_rent  = 0;
        }

        // echo "Listings Rent: ".$Final_listing_rent."<br>Agent name: ".$agent_name; die();
        return [
            'listings_rent' => $Final_listing_rent,
            'agent_name'    => $agent_name,
            'listing_property_type'    => $listing_property_type,
        ];


    }
    public function getFullPageContent($listing_website_url_image=null, $MainLandOrBua=null, $no_of_bedrooms=null)
    {
        $totalArea = str_replace(',', '', $MainLandOrBua);
        $percentageValue = $totalArea*10/100;
        $addInTotalArea = $totalArea+$percentageValue;
        $removeInTotalArea = $totalArea-$percentageValue;

        $url = 'https://api.ocr.space/parse/imageurl?apikey=ce5c9417c088957&url='.$listing_website_url_image;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $json = json_decode($response, true);
        $agent_name = '';
        $Final_listing_rent = '';
        $listing_rent='';
        $listing_property_type='';

        // echo "<pre>"; print_r($json); echo "</pre><br><br><br><br><br>"; die();
        if (isset($json['ParsedResults'][0]['ParsedText']) && $json['ParsedResults'][0]['ParsedText']<>null) {
            $html = $json['ParsedResults'][0]['ParsedText'];
            // echo "<pre>"; print_r($html); echo "</pre><br><br>";


            $listing_property_type = $this->getListingPropertyType($html);


            $agent_name = $this->getAgentName($html);

            $needle = "AED";
            $lastPos = 0;
            $positions = array();

            while (($lastPos = strpos($html, $needle, $lastPos))!== false) {
                $positions[] = $lastPos;
                $lastPos = $lastPos + strlen($needle);
            }

            foreach ($positions as $position) {
                // echo $position ."<br />";
                $nextChar = substr($html, $position, 18);
                if (strpos($nextChar, 'Yearly')!==false) {
                    $AverageRent = preg_replace('/[^0-9-,-.]/', '', $nextChar);
                    $listing_rent = str_replace(',', '', $AverageRent);
                }
                // echo "<pre>"; print_r($nextChar); echo "</pre><br><br>";
            }
            // echo "listing rent:- ".$listing_rent; die();

            if ($listing_rent<>null && $listing_rent>0) {
                $Final_listing_rent =  $listing_rent;
            }else{
                // echo $no_of_bedrooms." ".$removeInTotalArea." ".$addInTotalArea; die();
                $findInPreviousListing = \app\models\ListingsTransactions::find()
                    ->where(['no_of_bedrooms'=>$no_of_bedrooms])
                    ->andWhere(['and',['>=', 'listings_rent', $removeInTotalArea],['<=', 'listings_rent', $addInTotalArea]])
                    ->asArray()->all();
                if ($findInPreviousListing<>null) {
                    // echo "land size lie in bayut range<br>";
                    $listing_rent_db = 0;
                    foreach ($findInPreviousListing as $key => $value) {
                        $listing_rent_db += $value['listings_rent'];
                    }
                    $listing_rentSum = $listing_rent_db/count($findInPreviousListing);
                    $Final_listing_rent = number_format((float)$listing_rentSum, 2, '.', '');
                }
                else{
                    // die('rent also not found in listing');
                    $Final_listing_rent = 0;
                }
            }
        }
        else{
           // echo "in else<br>";
            $Final_listing_rent  = 0;
        }

        // echo "Listings Rent: ".$Final_listing_rent."<br>Agent name: ".$agent_name; die();
        return [
            'listings_rent' => $Final_listing_rent,
            'agent_name'    => $agent_name,
            'listing_property_type'    => $listing_property_type,
        ];


    }
    public function getListingPropertyType_v2($html=null)
    {
        // echo "In getListingPropertyType Function<pre>"; print_r($html); echo "<pre><br><br>"; //die();
        $listing_property_type = '';

        $position = strpos($html, 'Type:');
        if ($position<>null) {
            $nextChar = substr($html, $position, 20);

            $seprateLine = explode(PHP_EOL, $nextChar);
            $explode = explode('Type:', trim($seprateLine[0]));
            $keyword = $explode[1];


            $findInDatabase = \app\models\ListingSubTypes::find()->where(['title'=>trim($keyword)])->one();
            if ($findInDatabase<>null) {
                $listing_property_type = $findInDatabase->id;
            }else{
                return 'Not known';
            }

        }
        else{
            return 'Not known';
        }

        return $listing_property_type;
    }
    public function getListingPropertyType($html=null)
    {
        // echo "In getListingPropertyType Function<pre>"; print_r($html); echo "<pre><br><br>"; //die();
        $listing_property_type = 'Not known';

        $position = strpos($html, 'Type:');
        if ($position<>null) {
            $nextChar = substr($html, $position, 20);

            $seprateLine = explode(PHP_EOL, $nextChar);
            $explode = explode('Type:', trim($seprateLine[0]));
            $keyword = $explode[1];


            $findInDatabase = \app\models\ListingSubTypes::find()->where(['title'=>trim($keyword)])->one();
            if ($findInDatabase<>null) {
                $listing_property_type = $findInDatabase->id;
            }else{
                $listing_property_type =  'Not known';
            }

        }
        else{
            $listing_property_type =  'Not known';
        }

        return $listing_property_type;
    }



    public function SaveBayutImage($detailUrl='', $delFromBucket=null)
    {
        if($detailUrl != null)
        {
            $api_key = "AIzaSyCgOsZq9GEQtt6RwDZ6ay7MERxJCnKi-RI";
            $url = trim($detailUrl);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url={$url}&key=" . $api_key,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $response = json_decode($response);
            $image = $response->lighthouseResult->audits->{'full-page-screenshot'}->details->screenshot->data;
            $randomString = $this->generateRandomString();
            $img_map = __DIR__ .'/../../images/bayut/image_'.$randomString.'.png';

            if($image <> null){

                $data = $image;

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);
                file_put_contents($img_map, $data);
                $uploadObject = Yii::$app->get('s3bucket')->upload('/bayut-screenshots/'.$img_map, $img_map);
                $url = Yii::$app->get('s3bucket')->getUrl('/bayut-screenshots/'.$img_map);
                if($url <> null)
                {
                    if ($delFromBucket!='Yes') {
                        // delete from bucket also code
                        Yii::$app->get('s3bucket')->delete('/bayut-screenshots/'.$img_map);
                    }
                    unlink($img_map);
                    return $url;
                }
            }


        }
    }


    public function SaveBayutImage_old($detailUrl='')
    {
        if($detailUrl != null)
        {


            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=https://www.bayut.com/property/details-5773378.html&key=AIzaSyCgOsZq9GEQtt6RwDZ6ay7MERxJCnKi-RI',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);



            //$request = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=https://www.bayut.com/property/details-5773378.html&key=AIzaSyCgOsZq9GEQtt6RwDZ6ay7MERxJCnKi-RI';
            $response = json_decode($response);
            $image = $response->lighthouseResult->audits->{'full-page-screenshot'}->details->screenshot->data;

            error_log($response->captchaResult);

           /* $data = 'data:image/png;base64,AAAFBfj42Pj4';

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);*/



            $img_map = __DIR__ .'/listings/imagewer.png';

            if($image <> null){

                $data = $image;

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                //$content = @file_get_contents($image);
                //if($content){
                    file_put_contents($img_map, $data);
              //  }
            }

            error_log($response->captchaResult);

        }else{
            //return 'https://wis-windmills.s3.ap-southeast-1.amazonaws.com//bayut-screenshots/images/bayut/image-gkRypj5Cre.png';
        }
    }



    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getCommunitySubCommunity($community=null, $sub_community=null, $city=null)
    {
        // echo "Community: ".$community."<br>Sub Community: ".$sub_community."<br>City: ".$city."<br><br><br>";

        if ($city<>null) {
            $uae_country_id = $this->getSetting('emirates_country_id');
            // echo "Country id: ".$uae_country_id; die();
            $findCity =  Zone::find()->where(['country_id' => $uae_country_id, 'title' => trim($city), 'status' => 1, 'trashed' => 0])->one();
            // echo "array:--- <pre>"; print_r($findCity); echo "</pre>"; die();
            if ($findCity<>null) {
                $finalCity = $findCity->id;
            }
        }

        if ($community<>null) {
            // echo "community- ".$community; die();
            $findCommunity = Communities::find()->where([
                'or',
                ['title'=> trim($community)],
                ['like', 'title', trim($community) . '%', false],
            ])->one();
            if ($findCommunity<>null) {
                $finalCommunity = $findCommunity->id;
            }else{
                $findCommunity = new Communities;
                $findCommunity->title = trim($community);
                $findCommunity->city  = $finalCity;
                $findCommunity->status = 1;
                $findCommunity->source = 'Scrape';

                if ($findCommunity->save()) {
                    $finalCommunity = $findCommunity->id;
                }
            }
        }

        if ($sub_community<>null) {
            $findSub_community = SubCommunities::find()->where([
                'or',
                ['title'=> trim($sub_community)],
                ['like', 'title', trim($sub_community) . '%', false],
            ])->one();
            if ($findSub_community<>null) {
                $finalSub_community = $findSub_community->id;
            }else{
                $findSub_community = new SubCommunities;
                $findSub_community->title = trim($sub_community);
                $findSub_community->community  = $finalCommunity;
                $findSub_community->status = 1;
                $findSub_community->source = 'Scrape';

                if ($findSub_community->save()) {
                    $finalSub_community = $findSub_community->id;
                }
            }
        }



        return [
            'city' => $finalCity,
            'community' => $finalCommunity,
            'sub_community' => $finalSub_community,
        ];
    }

    public function getPropertyType($type=null)
    {
        if ($type<>null) {
            $findType = \app\models\Properties::find()->where([
                'or',
                ['title'=> trim($type)],
                ['like', 'title', trim($type) . '%', false],
            ])->one();
            // echo "<pre>"; print_r($findType); echo "</pre>"; die();
            if ($findType<>null) {
                return $findType->id;
            }
            else{
                $findType = new \app\models\Properties;
                $findType->title =  trim($type);
                $findType->category =  6;
                $findType->valuation_approach = 1;
                if ($findType->save()) {
                    return $findType->id;
                }
            }
        }
    }
    public function getCityId($city=null)
    {
        // echo "in helper: ".trim($city)."<br>"; //die();
        if ($city<>null) {
            $uae_country_id = $this->getSetting('emirates_country_id');
            // echo "Country id: ".$uae_country_id; die();
            $findCity =  Zone::find()->where(['country_id' => $uae_country_id, 'title' => trim($city), 'status' => 1, 'trashed' => 0])->one();
            // echo "array:--- <pre>"; print_r($findCity); echo "</pre>"; die();
            if ($findCity<>null) {
                return $findCity->id;
            }
        }
    }

    public function getCommunityAndSubCommunityId_old_version_v1($community=null, $city_id=null)
    {
        $community_id = '';
        $subCommunity_id = '';
        if ($community<>null) {
            // echo "community- ".$community; die();
            $findCommunity = Communities::find()->where([
                'or',
                ['title'=> trim($community)],
                ['like', 'title', trim($community) . '%', false],
            ])->one();
            if ($findCommunity<>null) {
                $community_id =  $findCommunity->id;

            }

            $findSub_community = SubCommunities::find()->where([
                'or',
                ['title'=> trim($community)],
                ['like', 'title', trim($community) . '%', false],
            ])->one();
            if ($findSub_community<>null) {
                $community_id    = $findSub_community->community;
                $subCommunity_id = $findSub_community->id;
            }

            if ($community_id==null) {
                // save community
                $findCommunity = new Communities;
                $findCommunity->title = trim($community);
                $findCommunity->city  = ($city_id<>null) ? $city_id : '';
                $findCommunity->status = 1;
                $findCommunity->source = 'Scrape';
              /*  if ($findCommunity->save()) {
                    $community_id = $findCommunity->id;
                }*/

            }

            if ($subCommunity_id==null) {
                // save sub community
                $findSub_community = new SubCommunities;
                $findSub_community->title = trim($community);
                $findSub_community->community  = $community_id;
                $findSub_community->status = 1;
                $findSub_community->source = 'Scrape';
              /*  if ($findSub_community->save()) {
                    $subCommunity_id = $findSub_community->id;
                }*/
            }

            return [
                'community_id' => $community_id,
                'subCommunity_id' => $subCommunity_id,
            ];
        }
    }
    public function getCommunityAndSubCommunityId($community=null, $sub_community, $city_id=null)
    {
        $community_id = '';
        $subCommunity_id = '';

        if ($community<>null) {
            // echo "community- ".$community; die();
            $findCommunity = Communities::find()->where([
                'or',
                ['title'=> trim($community)],
                ['like', 'title', trim($community) . '%', false],
            ])->one();

            if ($findCommunity<>null) {
                $community_id =  $findCommunity->id;
            }
        }

        if ($sub_community<>null) {
            $findSub_community = SubCommunities::find()->where([
                'or',
                ['title'=> trim($sub_community)],
                ['like', 'title', trim($sub_community) . '%', false],
            ])->one();

            if ($findSub_community<>null) {
                $subCommunity_id = $findSub_community->id;
            }
        }

        return [
            'community_id' => $community_id,
            'subCommunity_id' => $subCommunity_id,
        ];
    }
    public function getAgentName($html='')
    {
        $agent_name='';
        if ($html<>null) {
            if (
                strpos($html, 'Agent:')!==false
                || strpos($html, 'Agent;')!==false
                || strpos($html, 'Agen e')!==false
                || strpos($html, 'Agene')!==false
                || strpos($html, 'Agen t:')!==false
                || strpos($html, "Agent'")!==false
                || strpos($html, "t:")!==false
            ) {
                $agentPosition = strpos($html, 'Agent:');
                $agentPosition2 = strpos($html, 'Agent;');
                $agentPosition3 = strpos($html, 'Agen e');
                $agentPosition4 = strpos($html, 'Agene');
                $agentPosition5 = strpos($html, 'Agen t:');
                $agentPosition6 = strpos($html, "Agent'");
                $agentPosition7 = strpos($html, "t:");
                if ($agentPosition<>null && $agentPosition>0) {
                  //  echo "222<br>";
                    $nextChar = substr($html, $agentPosition, 35);
                    $chunks = explode("View",$nextChar);
                    $againChunks = explode(":",$chunks[0]);
                    $agent_name = $againChunks[1];
                }else if ($agentPosition2<>null && $agentPosition2>0) {
                  //  echo "333<br>";
                    $nextChar2 = substr($html, $agentPosition2, 35);
                    $chunks2 = explode("View",$nextChar2);
                    $againChunks2 = explode(";",$chunks2[0]);
                    $agent_name = $againChunks2[1];
                }else if ($agentPosition3<>null && $agentPosition3>0) {
                 //   echo "444<br>";
                    $nextChar3 = substr($html, $agentPosition3, 35);
                    $chunks3 = explode("View",$nextChar3);
                    $againChunks3 = explode(" e",$chunks3[0]);
                    $agent_name = $againChunks3[1];
                }else if ($agentPosition4<>null && $agentPosition4>0) {
                //    echo "555<br>";
                    $nextChar4 = substr($html, $agentPosition4, 35);
                    $chunks4 = explode("View",$nextChar4);
                    $againChunks4 = explode("Agene ",$chunks4[0]);
                    $agent_name = $againChunks4[1];
                }else if ($agentPosition5<>null && $agentPosition5>0) {
                 //   echo "666<br>";
                    $nextChar5 = substr($html, $agentPosition5, 35);
                    $chunks5 = explode("View",$nextChar5);
                    $againChunks5 = explode(":",$chunks5[0]);
                    $agent_name = $againChunks5[1];
                }else if ($agentPosition6<>null && $agentPosition6>0) {
                 //   echo "777<br>";
                    $nextChar6 = substr($html, $agentPosition6, 35);
                    $chunks6 = explode("View",$nextChar6);
                    $againChunks6 = explode("Agent'",$chunks6[0]);
                    $agent_name = $againChunks6[1];
                }else if ($agentPosition7<>null && $agentPosition7>0) {
                //    echo "888<br>";
                    $nextChar7 = substr($html, $agentPosition7, 35);
                    $chunks7 = explode("View",$nextChar7);
                    $againChunks7 = explode(":",$chunks7[0]);
                    $agent_name = $againChunks7[1];
                }
                return $agent_name;
            }
            else{}
        }
    }

    public function GetImgLink($listing_id='')
    {
        if ($listing_id<>null) {
            $result = \app\Models\ListingsTransactions::findOne($listing_id);
            if ($result<>null) {
                return $result->listing_website_link;
            }else{
                return '';
            }
        }else{
            return '';
        }
    }

    public function getWorkingDays($startDate,$endDate,$holidays){
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);


        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        }
        else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)

            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            }
            else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0 )
        {
            $workingDays += $no_remaining_days;
        }

        //We subtract the holidays
        foreach($holidays as $holiday){
            $time_stamp=strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
        }

        return $workingDays;
    }

    public function getFirstProperty($quotation_id='')
    {
        if ($quotation_id<>null) {
            $result = \app\models\CrmReceivedProperties::find()->where(['quotation_id'=>$quotation_id])->one();
            if ($result<>null) {
                return $result;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function getVatArr()
    {
        return [
            '1' => 'Yes',
            '2' => 'No',
        ];
    }
    public function getIncomeTypesArr_old()
    {
        return[
            '1' => 'Apartment',
            '2' => 'Apartment - Duplex',
            '3' => 'Manual',
            '4' => 'Studio',
            '5' => '1 Bedroom',
            '6' => '2 Bedroom',
            '7' => '3 Bedroom',
            '8' => '4 Bedroom',
            '9' => '5 Bedroom',
            '10' => '6 Bedroom',
        ];
    }

    public function getCprArr()
    {
        return[
            '1' => 'Vacant',
            '2' => 'Occupied',
            '3' => 'legal Issue',
        ];
    }
    public function getApproverDate($model='')
    {
        if ($model<>null) {
            $query = \app\models\ValuationApproversData::find()->select(['created_at'])
                ->where(['valuation_id'=>$model['id'],'approver_type'=>'approver','status'=>'Approve'])
                ->asArray()->one();
            if ($query<>null) {
                return $query['created_at'];
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    public function getPropertyTypeArr_old()
    {
        return[
            'apartment'             => 'Apartment',
            'townhouse'             => 'Townhouse',
            'villa compound'        => 'Villa Compound',
            'residential plot'      => 'Residential Plot',
            'residential building'  => 'Residential Building',
            'villa'                 => 'Villa',
            'penthouse'             => 'Penthouse',
            'hotel apartment'       => 'Hotel Apartment',
            'residential floor'     => 'Residential Floor',
            'office'                => 'Office',
            'warehouse'             => 'Warehouse',
            'commercial villa'      => 'Commercial Villa',
            'commercial plot'       => 'Commercial Plot',
            'commercial building'   => 'Commercial Building',
            'industrial land'       => 'industrial Land',
            'showroom'              => 'Showroom',
            'shop'                  => 'Shop',
            'labour camp'           => 'labour Camp',
            'bulk unit'             => 'Bulk Unit',
            'commercial floor'      => 'Commercial Floor',
            'factory'               => 'Factory',
            'mixed use land'        => 'Mixed Use Land',
            'other commercial'      => 'Other Commercial',
        ];
    }

    public function getMoveToListingArr()
    {
        return [
            '1' => Yii::t('app','Moved'),
            '0' => Yii::t('app','Pending'),
            '2' => Yii::t('app','Building Not Found'),
            '3' => Yii::t('app','Duplicate'),
        ];
    }


    public function getMoveToListingArrLable()
    {
        return [
            '1' => '<span class="badge grid-badge badge-success">Moved</span>',
            '0' => '<span class="badge grid-badge badge-warning">Pending</span>',
            '2' => '<span class="badge grid-badge badge-secondary">Building not found</span>',
            '3' => '<span class="badge grid-badge badge-primary">Duplicate</span>',
        ];
    }
    public function getPropertyTypeArr()
    {
        return[
            'apartment'             => 'Apartment',
            'townhouse'             => 'Townhouse',
            'villa compound'        => 'Villa Compound',
            'residential plot'      => 'Residential Plot',
            'residential building'  => 'Residential Building',
            'villa'                 => 'Villa',
            'penthouse'             => 'Penthouse',
            'hotel apartment'       => 'Hotel Apartment',
            'residential floor'     => 'Residential Floor',
            'office'                => 'Office',
            'warehouse'             => 'Warehouse',
            'commercial villa'      => 'Commercial Villa',
            'commercial plot'       => 'Commercial Plot',
            'commercial building'   => 'Commercial Building',
            'industrial land'       => 'industrial Land',
            'showroom'              => 'Showroom',
            'shop'                  => 'Shop',
            'labour camp'           => 'labour Camp',
            'bulk unit'             => 'Bulk Unit',
            'commercial floor'      => 'Commercial Floor',
            'factory'               => 'Factory',
            'mixed use land'        => 'Mixed Use Land',
            'other commercial'      => 'Other Commercial',

            //propertyfinder types
            'compound'              => 'Compound',
            'duplex'                => 'Duplex',
            'full floor'            => 'Full Floor',
            'half floor'            => 'Half Floor',
            'whole building'        => 'Whole Building',
            'land'                  => 'Land',
            'bulk sale unit'        => 'Bulk Sale Unit',
            'bungalow'              => 'Bungalow',
            'hotel hotel apartment' => 'Hotel apartments',
        ];
    }

    public function getGreenEfficientCertificationArr()
    {
        return [
            1 =>   'Not Available',
            2 =>   'Yes',
        ];
    }

    public function getCertifierNameArr()
    {
        return [
            1 => 'Not Available',
            2 => 'Al Sa’fat',
            3 => 'Estidamah',
            4 => 'LEED',
            5 => 'Breem',
            6 => 'Pearls',
        ];
    }


    public function getCertificationLevelArr()
    {
        return [
            1 => 'Not Available',
            2 => 'Silver',
            3 => 'Gold',
            4 => 'Platinum',
            5 => 'Pearls',
        ];
    }

    public function getSGCInformationArr()
    {
        return [
            1 =>   'Not Available',
            2 =>   'Government Authority',
        ];
    }

    public function  getThisQuarterNew_old($quarter, $year)
    {
        if($quarter == '1-quarter')
        {
            $start_date = strtotime('1-January-'.$year);  // timestamp or 1-Januray 12:00:00 AM
            $end_date = strtotime('1-April-'.$year);  // timestamp or 1-April 12:00:00 AM means end of 31 March
        }
        else  if($quarter == '2-quarter')
        {
            $start_date = strtotime('1-April-'.$year);  // timestamp or 1-April 12:00:00 AM
            $end_date = strtotime('1-July-'.$year);  // timestamp or 1-July 12:00:00 AM means end of 30 June
        }
        else  if($quarter == '3-quarter')
        {
            $start_date = strtotime('1-July-'.$year);  // timestamp or 1-July 12:00:00 AM
            $end_date = strtotime('1-October-'.$year);  // timestamp or 1-October 12:00:00 AM means end of 30 September
        }
        else  if($quarter == '4-quarter')
        {
            $start_date = strtotime('1-October-'.$year);  // timestamp or 1-October 12:00:00 AM
            $end_date = strtotime('1-January-'.($year+1));  // timestamp or 1-January Next year 12:00:00 AM means end of 31 December this year
        }

        return array('start_date'=>$start_date, 'end_date'=> $end_date);
    }
    public function  getThisQuarterNew($quarter, $year)
    {
        if($quarter == '1-quarter')
        {
            $start_date = strtotime('1-January-'.$year);  // timestamp or 1-Januray 12:00:00 AM
            $end_date = strtotime('31-March-'.$year);  // timestamp or 1-April 12:00:00 AM means end of 31 March
        }
        else  if($quarter == '2-quarter')
        {
            $start_date = strtotime('1-April-'.$year);  // timestamp or 1-April 12:00:00 AM
            $end_date = strtotime('30-June-'.$year);  // timestamp or 1-July 12:00:00 AM means end of 30 June
        }
        else  if($quarter == '3-quarter')
        {
            $start_date = strtotime('1-July-'.$year);  // timestamp or 1-July 12:00:00 AM
            $end_date = strtotime('30-September-'.$year);  // timestamp or 1-October 12:00:00 AM means end of 30 September
        }
        else  if($quarter == '4-quarter')
        {
            $start_date = strtotime('1-October-'.$year);  // timestamp or 1-October 12:00:00 AM
            $end_date = strtotime('31-December-'.$year);  // timestamp or 1-January Next year 12:00:00 AM means end of 31 December this year
        }
        else  if($quarter == '1-Half')
        {
            $start_date = strtotime('1-January-'.$year);
            $end_date = strtotime('30-June-'.$year);
        }
        else  if($quarter == '2-Half')
        {
            $start_date = strtotime('1-July-'.$year);
            $end_date = strtotime('31-December-'.$year);
        }
        return array('start_date'=>$start_date, 'end_date'=> $end_date);
    }



    function GetMonthlyResult_old($month_key, $year){
        // print_r($month_key); die;

        $total_val_recieved=$total_doc_requested=$total_inspect_requested=$total_approved=$total_rejected='';
        $per_total_val_recieved=$per_total_doc_requested=$per_total_inspect_requested=$per_total_approved=$per_total_rejected=0;

        if($year!='all-times' && $month_key<>null){
            $connection = Yii::$app->getDb();
            $total_valuations = Valuation::find()->count("id");

            //total_val_recieved
            $total_val_recieved = Valuation::find()
                ->where(new Expression(" YEAR(instruction_date) = ".$year." AND MONTH(instruction_date) = ".$month_key." "))
                ->count("id");
            $per_total_val_recieved = $this->getStatusBasedPercentage($total_val_recieved, $total_valuations);

            //total_doc_requested
            $command = $connection->createCommand(" select count(valuation.id) as total_doc_requested from valuation where valuation.valuation_status = 7 AND YEAR(valuation.updated_at) = ".$year." AND MONTH(valuation.updated_at) = ".$month_key." ");
            $command = $command->queryAll();
            $total_doc_requested = $command[0]['total_doc_requested'];
            $per_total_doc_requested = $this->getStatusBasedPercentage($total_doc_requested, $total_valuations);

            //total_inspect_requested
            // $command = $connection->createCommand(" select count(valuation.id) as total_inspect_requested from valuation where valuation.id in (select valuation_id from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$year." AND MONTH(schedule_inspection.inspection_date) =  ".$month_key.") ");
            $command = $connection->createCommand(" select count(valuation_id) as total_inspect_requested from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$year." AND MONTH(schedule_inspection.inspection_date) = ".$month_key." ");
            $command = $command->queryAll();
            $total_inspect_requested = $command[0]['total_inspect_requested'];
            $per_total_inspect_requested = $this->getStatusBasedPercentage($total_inspect_requested, $total_valuations);

            //total_approved
            $command = $connection->createCommand("  select count(valuation.id) as total_approved from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Approve' AND YEAR(valuation_approvers_data.created_at) = ".$year." AND MONTH(valuation_approvers_data.created_at) = ".$month_key.") ");
            $command = $command->queryAll();
            $total_approved = $command[0]['total_approved'];
            $per_total_approved = $this->getStatusBasedPercentage($total_approved, $total_valuations);

            //total_rejected
            $command = $connection->createCommand(" 
        select count(valuation.id) as total_rejected from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Reject' AND YEAR(valuation_approvers_data.created_at) = ".$year." AND MONTH(valuation_approvers_data.created_at) = ".$month_key.")
        ");
            $command = $command->queryAll();
            $total_rejected = $command[0]['total_rejected'];
            $per_total_rejected = $this->getStatusBasedPercentage($total_rejected, $total_valuations);
        }
        return [
            'total_val_recieved'          => $total_val_recieved,
            'total_doc_requested'         => $total_doc_requested,
            'total_inspect_requested'     => $total_inspect_requested,
            'total_approved'              => $total_approved,
            'total_rejected'              => $total_rejected,
            'per_total_val_recieved'      => $per_total_val_recieved,
            'per_total_doc_requested'     => $per_total_doc_requested,
            'per_total_inspect_requested' => $per_total_inspect_requested,
            'per_total_approved'          => $per_total_approved,
            'per_total_rejected'          => $per_total_rejected,
        ];
    }
    function GetMonthlyResult($month_key, $year){

        $total_val_recieved=$total_doc_requested=$total_inspect_requested=$total_approved=$total_rejected='';
        $per_total_val_recieved=$per_total_doc_requested=$per_total_inspect_requested=$per_total_approved=$per_total_rejected=0;

        if($year!='all-times' && $month_key<>null){
            $connection = Yii::$app->getDb();
            // $total_valuations = Valuation::find()->count("id");
            $command = $connection->createCommand("SELECT count(id) as total_valuations FROM `valuation` WHERE YEAR(instruction_date) = ".$year."");
            $command = $command->queryAll();
            $total_valuations = $command[0]['total_valuations'];

            //total_val_recieved
            $total_val_recieved = Valuation::find()
                ->where(new Expression(" YEAR(instruction_date) = ".$year." AND MONTH(instruction_date) = ".$month_key." "))
                ->count("id");
            $per_total_val_recieved = $this->getStatusBasedPercentage($total_val_recieved, $total_valuations);

            //total_doc_requested
            $command = $connection->createCommand("  select count(valuation.id) as total_doc_val_count from valuation where valuation.valuation_status = 7 AND YEAR(valuation.updated_at) = ".$year."");
            $command = $command->queryAll();
            $total_doc_val_count = $command[0]['total_doc_val_count'];

            $command = $connection->createCommand(" select count(valuation.id) as total_doc_requested from valuation where valuation.valuation_status = 7 AND YEAR(valuation.updated_at) = ".$year." AND MONTH(valuation.updated_at) = ".$month_key." ");
            $command = $command->queryAll();
            $total_doc_requested = $command[0]['total_doc_requested'];
            $per_total_doc_requested = $this->getStatusBasedPercentage($total_doc_requested, $total_doc_val_count);

            //total_inspect_requested
            // $command = $connection->createCommand(" select count(valuation.id) as total_inspect_requested from valuation where valuation.id in (select valuation_id from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$year." AND MONTH(schedule_inspection.inspection_date) =  ".$month_key.") ");
            $command = $connection->createCommand("  select count(valuation_id) as total_inspect_val_count from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$year."");
            $command = $command->queryAll();
            $total_inspect_val_count = $command[0]['total_inspect_val_count'];

            $command = $connection->createCommand(" select count(valuation_id) as total_inspect_requested from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$year." AND MONTH(schedule_inspection.inspection_date) = ".$month_key." ");
            $command = $command->queryAll();
            $total_inspect_requested = $command[0]['total_inspect_requested'];
            $per_total_inspect_requested = $this->getStatusBasedPercentage($total_inspect_requested, $total_inspect_val_count);

            //total_approved
            $command = $connection->createCommand("  select count(valuation.id) as total_approved_val_count from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Approve' AND YEAR(valuation_approvers_data.created_at) = ".$year.") ");
            $command = $command->queryAll();
            $total_approved_val_count = $command[0]['total_approved_val_count'];

            $command = $connection->createCommand("  select count(valuation.id) as total_approved from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Approve' AND YEAR(valuation_approvers_data.created_at) = ".$year." AND MONTH(valuation_approvers_data.created_at) = ".$month_key.") ");
            $command = $command->queryAll();
            $total_approved = $command[0]['total_approved'];
            $per_total_approved = $this->getStatusBasedPercentage($total_approved, $total_approved_val_count);

            //total_rejected
            // $command = $connection->createCommand("
            // select count(valuation.id) as total_rejected from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Reject' AND YEAR(valuation_approvers_data.created_at) = ".$year." AND MONTH(valuation_approvers_data.created_at) = ".$month_key.")
            // ");
            // $command = $command->queryAll();
            // $total_rejected = $command[0]['total_rejected'];
            // $per_total_rejected = $this->getStatusBasedPercentage($total_rejected, $total_valuations);
        }
        return [
            'total_val_recieved'          => $total_val_recieved,
            'total_doc_requested'         => $total_doc_requested,
            'total_inspect_requested'     => $total_inspect_requested,
            'total_approved'              => $total_approved,
            // 'total_rejected'              => $total_rejected,
            'per_total_val_recieved'      => $per_total_val_recieved,
            'per_total_doc_requested'     => $per_total_doc_requested,
            'per_total_inspect_requested' => $per_total_inspect_requested,
            'per_total_approved'          => $per_total_approved,
            // 'per_total_rejected'          => $per_total_rejected,
            'total_valuations'            => $total_valuations,
        ];
    }

    public function getMQYArr(){
        return [
            'monthly'   => 'Monthly',
            'quarterly' => 'Quarterly',
            'yearly'    => 'Yearly',
        ];
    }

    public function getCBArr(){
        return [
            'banks'   => 'Banks',
            'corporate'   => 'Corporate',
        ];
    }

    public function getYearArr_old(){
        $yearArr = \app\models\valuation::find()->select(['yearcol'=>'YEAR(created_at)'])->distinct('created_at')->asArray()->all();
        $yearArr = ArrayHelper::map($yearArr, "yearcol", "yearcol");
        // $yearArr['all-time'] = 'All Time';
        return $yearArr;
    }

    public function getYearArr(){
        $yearArr = \app\models\valuation::find()->select(['yearcol'=>'YEAR(created_at)'])->orderBy(['YEAR(created_at)'=>SORT_DESC])->limit(5)->distinct('created_at')->asArray()->all();
        $yearArr = ArrayHelper::map($yearArr, "yearcol", "yearcol");
        // $yearArr['all-time'] = 'All Time';
        return $yearArr;
    }

    public function getMonthsArrForLoopArr(){
        return [
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July ',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];
    }

    public function getMonthsForLoopArr(){
        return [
            '01' => 'JAN',
            '02' => 'FEB',
            '03' => 'MAR',
            '04' => 'APR',
            '05' => 'MAY',
            '06' => 'JUN',
            '07' => 'JULY ',
            '08' => 'AUG',
            '09' => 'SEP',
            '10' => 'OCT',
            '11' => 'NOV',
            '12' => 'DEC',
            '13' => 'Total Valuations',
        ];
    }

    public function getQuartersForLoop(){
        return [
            '01' => '1st Quarter',
            '02' => '2nd Quarter',
            '03' => '3rd Quarter',
            '04' => '4th Quarter',

            '05' => 'Total Valuations',
        ];
    }

    public function getYearsForLoop(){
        return [
            '01' => '2014',
            '02' => '2015',
            '03' => '2016',
            '04' => '2017',
            '05' => '2018',
            '06' => '2019',
            '07' => '2020',
            '08' => '2021',
            '09' => '2022',
            '10' => '2023',

            '11' => 'Total Valuations',
        ];
    }

    public function GetYearlyResultArr_old($year)
    {
        $total_val_recieved=$total_doc_requested=$total_inspect_requested=$total_approved=$total_rejected='';

        if($year <> null){

            $connection = Yii::$app->getDb();
            $total_valuations = Valuation::find()->count("id");

            //total_val_recieved
            $total_val_recieved = Valuation::find()
                ->where(new Expression(" YEAR(instruction_date) = ".$year." "))
                ->count("id");
            $per_total_val_recieved = $this->getStatusBasedPercentage($total_val_recieved, $total_valuations);

            //total_doc_requested
            $command = $connection->createCommand("select count(valuation.id) as total_doc_requested from valuation where valuation.valuation_status = 7 AND YEAR(valuation.updated_at) = ".$year." ");
            $command = $command->queryAll();
            $total_doc_requested = $command[0]['total_doc_requested'];
            $per_total_doc_requested = $this->getStatusBasedPercentage($total_doc_requested, $total_valuations);

            //total_inspect_requested
            // $command = $connection->createCommand(" select count(valuation.id) as total_inspect_requested from valuation where valuation.id in (select valuation_id from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$year.") ");
            $command = $connection->createCommand(" select count(valuation_id) as total_inspect_requested from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$year." ");
            $command = $command->queryAll();
            $total_inspect_requested = $command[0]['total_inspect_requested'];
            $per_total_inspect_requested = $this->getStatusBasedPercentage($total_inspect_requested, $total_valuations);

            //total_approved
            $command = $connection->createCommand(" 
            select count(valuation.id) as total_approved from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Approve' AND YEAR(valuation_approvers_data.created_at) = ".$year.")
            ");
            $command = $command->queryAll();
            $total_approved = $command[0]['total_approved'];
            $per_total_approved = $this->getStatusBasedPercentage($total_approved, $total_valuations);



            //total_rejected
            $command = $connection->createCommand(" 
            select count(valuation.id) as total_rejected from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Reject' AND YEAR(valuation_approvers_data.created_at) = ".$year.")
            ");
            $command = $command->queryAll();
            $total_rejected = $command[0]['total_rejected'];
            $per_total_rejected = $this->getStatusBasedPercentage($total_rejected, $total_valuations);

        }

        return [
            'total_val_recieved'          => $total_val_recieved,
            'total_doc_requested'         => $total_doc_requested,
            'total_inspect_requested'     => $total_inspect_requested,
            'total_approved'              => $total_approved,
            'total_rejected'              => $total_rejected,
            'per_total_val_recieved'      => $per_total_val_recieved,
            'per_total_doc_requested'     => $per_total_doc_requested,
            'per_total_inspect_requested' => $per_total_inspect_requested,
            'per_total_approved'          => $per_total_approved,
            'per_total_rejected'          => $per_total_rejected,
        ];
    }

    public function GetYearlyResultArr($year)
    {
        $total_val_recieved=$total_doc_requested=$total_inspect_requested=$total_approved=$total_rejected='';

        if($year <> null){
            $connection = Yii::$app->getDb();

            $yearsArr = $this->getYearArr();
            $yearsArr = implode(",", $yearsArr);

            //total_val_recieved
            $total_val_recieved_count = Valuation::find()->where(new Expression(" YEAR(instruction_date) IN (".$yearsArr.") "))->count("id");
            $total_val_recieved = Valuation::find()
                ->where(new Expression(" YEAR(instruction_date) = ".$year." "))
                ->count("id");
            $per_total_val_recieved = $this->getStatusBasedPercentage($total_val_recieved, $total_val_recieved_count);

            //total_doc_requested
            $total_doc_requested_count = Valuation::find()->where(new Expression(" valuation_status = 7 and YEAR(instruction_date) IN (".$yearsArr.") "))->count("id");

            $command = $connection->createCommand("select count(valuation.id) as total_doc_requested from valuation where valuation.valuation_status = 7 AND YEAR(valuation.updated_at) = ".$year." ");
            $command = $command->queryAll();
            $total_doc_requested = $command[0]['total_doc_requested'];
            $per_total_doc_requested = $this->getStatusBasedPercentage($total_doc_requested, $total_doc_requested_count);

            //total_inspect_requested
            // $command = $connection->createCommand(" select count(valuation.id) as total_inspect_requested from valuation where valuation.id in (select valuation_id from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$year.") ");
            $command = $connection->createCommand(" select count(valuation_id) as total_inspect_count from schedule_inspection where YEAR(schedule_inspection.inspection_date) in (".$yearsArr.") ");
            $command = $command->queryAll();
            $total_inspect_count = $command[0]['total_inspect_count'];

            $command = $connection->createCommand(" select count(valuation_id) as total_inspect_requested from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$year." ");
            $command = $command->queryAll();
            $total_inspect_requested = $command[0]['total_inspect_requested'];
            $per_total_inspect_requested = $this->getStatusBasedPercentage($total_inspect_requested, $total_inspect_count);

            //total_approved
            $command = $connection->createCommand(" 
            select count(valuation.id) as total_approved_count from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Approve' AND YEAR(valuation_approvers_data.created_at) in (".$yearsArr.") )
            ");
            $command = $command->queryAll();
            $total_approved_count = $command[0]['total_approved_count'];

            $command = $connection->createCommand(" 
            select count(valuation.id) as total_approved from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Approve' AND YEAR(valuation_approvers_data.created_at) = ".$year.")
            ");
            $command = $command->queryAll();
            $total_approved = $command[0]['total_approved'];
            $per_total_approved = $this->getStatusBasedPercentage($total_approved, $total_approved_count);



            //total_rejected
            // $command = $connection->createCommand("
            // select count(valuation.id) as total_rejected from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Reject' AND YEAR(valuation_approvers_data.created_at) = ".$year.")
            // ");
            // $command = $command->queryAll();
            // $total_rejected = $command[0]['total_rejected'];
            // $per_total_rejected = $this->getStatusBasedPercentage($total_rejected, $total_valuations);

        }

        return [
            'total_val_recieved'          => $total_val_recieved,
            'total_doc_requested'         => $total_doc_requested,
            'total_inspect_requested'     => $total_inspect_requested,
            'total_approved'              => $total_approved,
            // 'total_rejected'              => $total_rejected,
            'per_total_val_recieved'      => $per_total_val_recieved,
            'per_total_doc_requested'     => $per_total_doc_requested,
            'per_total_inspect_requested' => $per_total_inspect_requested,
            'per_total_approved'          => $per_total_approved,
            // 'per_total_rejected'          => $per_total_rejected,
        ];
    }

    public function getStatusBasedPercentage($selectedCount, $total_valuations)
    {
        if($total_valuations>0){
            return $selectedCount/$total_valuations*100;
        }else{
            return 0;
        }
    }
    public function getQuarterlyResultArr_old($data)
    {
        //declaring some used variables
        $total_val_recieved=$total_doc_requested=$total_inspect_requested=$total_approved=$total_rejected=$total_valuations=0;
        $per_total_val_recieved=$per_total_doc_requested=$per_total_inspect_requested=$per_total_approved=$per_total_rejected=0;

        //get date_from AND date_to
        $date_array = Yii::$app->appHelperFunctions->getThisQuarterNew($data['valuation_month'], $data['valuation_year'] );
        $date_from = date('Y-m-d', $date_array['start_date']);
        $date_to = date('Y-m-d', $date_array['end_date']);

        //get total valuation
        $total_valuations = Valuation::find()->count("id");

        //make a db connection
        $connection = Yii::$app->getDb();

        //total_val_recieved
        $total_val_recieved = Valuation::find()
            ->where(new Expression(" YEAR(instruction_date) = ".$data['valuation_year']." "))
            ->andWhere([ 'between', 'instruction_date', $date_from, $date_to ])
            ->count("id");
        $per_total_val_recieved = $this->getStatusBasedPercentage($total_val_recieved, $total_valuations);

        //total_doc_requested
        $command = $connection->createCommand(" select count(valuation.id) as total_doc_requested from valuation where valuation.valuation_status = 7 AND YEAR(valuation.updated_at) = ".$data['valuation_year']." AND valuation.updated_at BETWEEN '".$date_from."' AND '".$date_to."' ");
        $command = $command->queryAll();
        $total_doc_requested = $command[0]['total_doc_requested'];
        $per_total_doc_requested = $this->getStatusBasedPercentage($total_doc_requested, $total_valuations);

        //total_inspect_requested
        $command = $connection->createCommand(" select count(valuation_id) as total_inspect_requested from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$data['valuation_year']." AND schedule_inspection.inspection_date BETWEEN '".$date_from."' AND '".$date_to."' ");
        $command = $command->queryAll();
        $total_inspect_requested = $command[0]['total_inspect_requested'];
        $per_total_inspect_requested = $this->getStatusBasedPercentage($total_inspect_requested, $total_valuations);

        //total_approved
        $command = $connection->createCommand(" 
        select count(valuation.id) as total_approved from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Approve' AND YEAR(valuation_approvers_data.created_at) = ".$data['valuation_year']." AND valuation_approvers_data.created_at BETWEEN '".$date_from."' AND '".$date_to."') 
        ");
        $command = $command->queryAll();
        $total_approved = $command[0]['total_approved'];
        $per_total_approved = $this->getStatusBasedPercentage($total_approved, $total_valuations);

        //total_rejected
        $command = $connection->createCommand(" 
        select count(valuation.id) as total_rejected from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Reject' AND YEAR(valuation_approvers_data.created_at) = ".$data['valuation_year']." AND valuation_approvers_data.created_at BETWEEN '".$date_from."' AND '".$date_to."') 
        ");
        $command = $command->queryAll();
        $total_rejected = $command[0]['total_rejected'];
        $per_total_rejected = $this->getStatusBasedPercentage($total_rejected, $total_valuations);

        //return data
        return[
            'total_val_recieved'          => $total_val_recieved,
            'total_doc_requested'         => $total_doc_requested,
            'total_inspect_requested'     => $total_inspect_requested,
            'total_approved'              => $total_approved,
            'total_rejected'              => $total_rejected,
            'per_total_val_recieved'      => $per_total_val_recieved,
            'per_total_doc_requested'     => $per_total_doc_requested,
            'per_total_inspect_requested' => $per_total_inspect_requested,
            'per_total_approved'          => $per_total_approved,
            'per_total_rejected'          => $per_total_rejected,
        ];
    }
    public function getQuarterlyResultArr($data)
    {
        //declaring some used variables
        $total_val_recieved=$total_doc_requested=$total_inspect_requested=$total_approved=$total_rejected=$total_valuations=0;
        $per_total_val_recieved=$per_total_doc_requested=$per_total_inspect_requested=$per_total_approved=$per_total_rejected=0;

        //get date_from AND date_to
        $date_array = Yii::$app->appHelperFunctions->getThisQuarterNew($data['valuation_month'], $data['valuation_year']);
        $date_from = date('Y-m-d', $date_array['start_date']);
        $date_to = date('Y-m-d', $date_array['end_date']);

       // echo $date_from.' , '.$date_to.'<br>';

        //make a db connection
        $connection = Yii::$app->getDb();

        //total_val_recieved

        $total_val_recieved_count = Valuation::find()->where(new Expression(" YEAR(instruction_date) = ".$data['valuation_year']." "))->count("id");
        $total_val_recieved = Valuation::find()
            ->where(new Expression(" YEAR(instruction_date) = ".$data['valuation_year']." "))
            ->andWhere([ 'between', 'instruction_date', $date_from, $date_to ])
            ->count("id");
        $per_total_val_recieved = $this->getStatusBasedPercentage($total_val_recieved, $total_val_recieved_count);

        //total_doc_requested
        $total_doc_requested_count = Valuation::find()->where(new Expression(" valuation_status = 7 and YEAR(instruction_date) = ".$data['valuation_year']." "))->count("id");

        $command = $connection->createCommand(" select count(valuation.id) as total_doc_requested from valuation where valuation.valuation_status = 7 AND YEAR(valuation.updated_at) = ".$data['valuation_year']." AND valuation.updated_at BETWEEN '".$date_from."' AND '".$date_to."' ");
        $command = $command->queryAll();
        $total_doc_requested = $command[0]['total_doc_requested'];
        $per_total_doc_requested = $this->getStatusBasedPercentage($total_doc_requested, $total_doc_requested_count);

        //total_inspect_requested
        $command = $connection->createCommand(" select count(valuation_id) as total_inspect_count from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$data['valuation_year']." ");
        $command = $command->queryAll();
        $total_inspect_count = $command[0]['total_inspect_count'];

        $command = $connection->createCommand(" select count(valuation_id) as total_inspect_requested from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$data['valuation_year']." AND schedule_inspection.inspection_date BETWEEN '".$date_from."' AND '".$date_to."' ");
        $command = $command->queryAll();
        $total_inspect_requested = $command[0]['total_inspect_requested'];
        $per_total_inspect_requested = $this->getStatusBasedPercentage($total_inspect_requested, $total_inspect_count);

        //total_approved
        $command = $connection->createCommand(" 
        select count(valuation.id) as total_approved_count from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Approve' AND YEAR(valuation_approvers_data.created_at) = ".$data['valuation_year']." )
        ");
        $command = $command->queryAll();
        $total_approved_count = $command[0]['total_approved_count'];
        // dd($total_approved_count);

        $command = $connection->createCommand(" 
        select count(valuation.id) as total_approved from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Approve' AND YEAR(valuation_approvers_data.created_at) = ".$data['valuation_year']." AND valuation_approvers_data.created_at BETWEEN '".$date_from."' AND '".$date_to."') 
        ");
        $command = $command->queryAll();
        $total_approved = $command[0]['total_approved'];
        // dd($total_approved);
        $per_total_approved = $this->getStatusBasedPercentage($total_approved, $total_approved_count);

        //total_rejected
        // $command = $connection->createCommand("
        // select count(valuation.id) as total_rejected from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Reject' AND YEAR(valuation_approvers_data.created_at) = ".$data['valuation_year']." AND valuation_approvers_data.created_at BETWEEN '".$date_from."' AND '".$date_to."')
        // ");
        // $command = $command->queryAll();
        // $total_rejected = $command[0]['total_rejected'];
        // $per_total_rejected = $this->getStatusBasedPercentage($total_rejected, $total_valuations);

        //return data
        return[
            'total_val_recieved'          => $total_val_recieved,
            'total_doc_requested'         => $total_doc_requested,
            'total_inspect_requested'     => $total_inspect_requested,
            'total_approved'              => $total_approved,
            // 'total_rejected'              => $total_rejected,
            'per_total_val_recieved'      => $per_total_val_recieved,
            'per_total_doc_requested'     => $per_total_doc_requested,
            'per_total_inspect_requested' => $per_total_inspect_requested,
            'per_total_approved'          => $per_total_approved,
            // 'per_total_rejected'          => $per_total_rejected,
        ];
    }


    public function getCommunitiesListCity($city_id)
    {
        return \app\models\Communities::find()
            ->select(['id','title'])
            ->where(['city' => $city_id])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }
    public function getCommunitiesListCityArr($city_id){
        return ArrayHelper::map($this->getCommunitiesListCity($city_id), "id", "title");
    }

    public function getSubCommunitiesListCity($community_id)
    {
        return \app\models\SubCommunities::find()
            ->select(['id','title'])
            ->where(['community' => $community_id])
            ->orderBy(['title' => SORT_ASC])->asArray()->all();
    }
    public function getSubCommunitiesListCityArr($community_id){
        return ArrayHelper::map($this->getSubCommunitiesListCity($community_id), "id", "title");
    }

    public function getAssumptionTitle($assumption_id){
        if($assumption_id<>null){
            $result = \app\models\CrmAssumptions::find()->select(['title'])->where(['id'=>$assumption_id,'status' => 1])->asArray()->one();
            if($result<>null){
                return $result['title'];
            }
        }else{
            return null;
        }
    }

    public function getScopeOfServiceTitle($scope_id){
        if($scope_id<>null){
            $result = \app\models\CrmScopeOfService::find()->select(['title'])->where(['id'=>$scope_id, 'status' => 1])->asArray()->one();
            if($result<>null){
                return $result['title'];
            }
        }else{
            return null;
        }
    }

    public function  getvaluationTotal($period) {

        if($period > 0) {
            $date_array = $this->getFilterDates($period);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];
            $allvalues = (new \yii\db\Query())
                ->select('count(id) as total_values')
                ->from('valuation')
                ->where(['valuation_status' => 5])
                ->andFilterWhere(['between', 'instruction_date', $from_date, $to_date])
                ->one();
        }else{
            $allvalues = (new \yii\db\Query())
                ->select('count(id) as total_values')
                ->from('valuation')
                ->where(['valuation_status' => 5])
                ->one();
        }
        return $allvalues;


    }


    public function getMonthlyRevenue($month=null, $time_period=null, $client_type=null)
    {

        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                'client_type'=>Company::tableName().'.client_type',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->where('MONTH(valuation.instruction_date) = '.$month.'')
            ->andWhere(['valuation_status' => 5])
            ->andWhere(['company.client_type' => $client_type]);

        if($time_period <> null && $time_period != 0 ) {
            $date_array = $this->getFilterDates($time_period);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        $query = $query->all();
        return $query;
    }





    public function getMonthlyValuations($month=null, $time_period=null, $client_type=null)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->where('MONTH(valuation.instruction_date) = '.$month.'')
            ->andWhere(['valuation_status' => 5])
            ->andWhere(['company.client_type' => $client_type]);

        if($time_period <> null && $time_period != 0 ) {
            $date_array = $this->getFilterDates($time_period);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }

        $query = $query->all();
        return $query;
    }


    public function getCrmFloorNumber()
    {
        $floors= array();
        for($i=1;$i<=200;$i++){
            $floors[$i]=$i;
        }
        return $floors;
    }

    public function getBedrooms()
    {
        $floors= array();
        $floors[1]='Studio';
        for($i=1;$i<=200;$i++){
            $floors[$i+1]=$i;
        }
        return $floors;
    }

    public function getAssumptionTypesArr()
    {
        return [
            1=>'General',
            2=>'Special',
        ];
    }

    public function getScopeOfWorkArr()
    {
        return [
            1 =>'Estimate Market Value',
            2 =>'Estimate Reinstatement Costs',
            3 =>'Estimate Fair Value',
            4 =>'Cost Allocation Study',
            5 =>'Market Rental Assessment',
        ];
    }


    public function getReadyArr()
    {
        return [
            1 =>'< 75%',
            2 =>'> 75%',
            3 =>'N/A',
        ];
    }

    public function getZeroComparablesAvailableArr()
    {
        return [
            1 =>'Yes',
            2 =>'No',
        ];
    }

   /* public function getScopeOfWorkArr()
    {
        return [
            1 =>'Estimate Market Value',
            2 =>'Estimate Reinstatement Costs',
            3 =>'Estimate Fair Value',
        ];
    }*/

    public function getBasisOfValueArr()
    {
        return [
            1 =>'Market Value',
            2 =>'Replacement Cost',
            3 =>'Fair Value',
        ];
    }

    public function getValuaitonApproachArr()
    {
        return [
            1 =>'Market',
            2 =>'Income',
            3 =>'Reinstatement Cost',
            4 =>'Cost',
            5 =>'Residual Value Of Land',
            6 =>'Profit',
        ];
    }

    public function getUpgrades()
    {
        return [
            3 =>'3 star',
            1 =>'1 star',
            2 =>'2 star',
            4 =>'4 star',
            5 =>'5 star',
        ];
    }

    public function getCityShortName(){
        return [
            3510 => 'DXB',
            3506 => 'AUH',
            3507 => 'AJM',
            4260 => 'AUH',
            3508 => 'DXB',
            3511 => 'DXB',
            3509 => 'DXB',
            3512 => 'DXB',
        ];
    }


    public function getInvoiceNumber($model)
    {
        if(!Yii::$app->user->isGuest){
            $created_byy = \Yii::$app->user->identity->id;
            $updated_byy = \Yii::$app->user->identity->id;
        }else{
            $created_byy = null;
            $updated_byy = null;
        }

        if($model->quotation_id<>null){
            $previous = AutoInvoiceNum::find()->where(['quotation_id'=>$model->quotation_id])->asArray()->one();
            if($previous <> null){

                /*echo "<pre>";
                print_r($previous);
                die;*/
                Yii::$app->db->createCommand()->update(AutoInvoiceNum::tableName(),
                    ['valuation_id'=>$model->id],  [
                    'quotation_id' => $model->quotation_id,
                    'client_id'=>$model->client_id,
            ])->execute();

            }

        }

        if($model->quotation_id<>null){
            $valuation_idz = ArrayHelper::map(Valuation::find()->where(['quotation_id'=>$model->quotation_id])->asArray()->all(), "id", "id");
        }else{
            $valuation_idz = $model->id;
        }

        $previous = AutoInvoiceNum::find()->where(['valuation_id'=>$valuation_idz])->asArray()->one();

        if($previous<>null){
            $valuation_id = $previous['valuation_id'];
            $client_id = $previous['client_id'];
            $client_inv_no = $previous['client_inv_no'];
            $year_inv_num = $previous['year_inv_num'];
            $maxima_inv_num = $previous['maxima_inv_num'];
            $year = $previous['year'];
            $city = $previous['city'];
            $created_at = $previous['created_at'];
            $created_by = $previous['created_by'];

            $final_inv_num = $city.'-'.$maxima_inv_num.'-'.$year_inv_num;
        }
        else{
            $previous_maxima = AutoInvoiceNum::find()->select(['maxima_inv_num'])->where(['type'=>'maxima_inv_num'])->asArray()->one();
            if($previous_maxima<>null){
                $maxima_inv_num = str_pad($previous_maxima['maxima_inv_num']+1, 4, "0", STR_PAD_LEFT);
                //run all times
                Yii::$app->db->createCommand()->update(AutoInvoiceNum::tableName(),
                    ['maxima_inv_num'=>$maxima_inv_num],
                    ['type'=>'maxima_inv_num'])
                    ->execute();
            }else{
                $maxima_inv_num = str_pad(1, 4, "0", STR_PAD_LEFT);
                Yii::$app->db->createCommand()->insert(AutoInvoiceNum::tableName(), ['type'=>'maxima_inv_num','maxima_inv_num'=>$maxima_inv_num])->execute();
            }

            $clientCurrentInvNum = AutoInvoiceNum::find()->select(['client_inv_no'])->where(['client_id'=>$model->client_id])->orderBy(['id' => SORT_DESC])->asArray()->one();
            if($clientCurrentInvNum['client_inv_no'] <> null){
                $client_inv_no = str_pad($clientCurrentInvNum['client_inv_no']+1, 4, "0", STR_PAD_LEFT);
            }else{
                $client_inv_no = str_pad(1, 4, "0", STR_PAD_LEFT);
            }

            $yearInvNum = AutoInvoiceNum::find()->select(['year_inv_num'])->where(['year'=>date('Y')])->orderBy(['id' => SORT_DESC])->asArray()->one();
            if($yearInvNum['year_inv_num']<>null){
                $year_inv_num = str_pad($yearInvNum['year_inv_num']+1, 4, "0", STR_PAD_LEFT);
            }else{
                $year_inv_num = str_pad(1, 4, "0", STR_PAD_LEFT);
            }

            $valuation_id = $model->id;
            $client_id = $model->client_id;
            $year = date('Y');
            $city = $this->getCityShortName()[$model->building->city];
            $created_at = date('Y-m-d H:m:s');

            $final_inv_num = $city.'-'.$maxima_inv_num.'-'.$year_inv_num;


            Yii::$app->db->createCommand()->insert(AutoInvoiceNum::tableName(), [
                'valuation_id'=>$valuation_id,
                'client_id'=>$client_id,
                'client_inv_no'=>$client_inv_no,
                'maxima_inv_num'=>$maxima_inv_num,
                'year_inv_num'=>$year_inv_num,
                'year'=>$year,
                'city'=>$city,
                'created_at'=>$created_at,
                'created_by'=>$created_byy,
            ])->execute();
        }




        Yii::$app->db->createCommand()->update(AutoInvoiceNum::tableName(),
            [
                'valuation_id'=>$valuation_id,
                'client_id'=>$client_id,
                'client_inv_no'=>$client_inv_no,
                'maxima_inv_num'=>$maxima_inv_num,
                'year_inv_num'=>$year_inv_num,
                'year'=>$year,
                'city'=>$city,
                'updated_at'=>date('Y-m-d H:m:s'),
                'updated_by'=>$updated_byy,
            ],
            [
                'valuation_id' => $valuation_id,
                'client_id'=>$client_id,
            ])->execute();

        // dd($final_inv_num);
        return $final_inv_num;


    }
    public function getInvoiceNumber_new($model, $invmode=0)
    {
        if(!Yii::$app->user->isGuest){
            $created_byy = \Yii::$app->user->identity->id;
            $updated_byy = \Yii::$app->user->identity->id;
        }else{
            $created_byy = null;
            $updated_byy = null;
        }
       /* echo $invmode;
        die;*/

        if($model->quotation_id<>null){
            $previous = AutoInvoiceNum::find()->where(['quotation_id'=>$model->quotation_id])->andWhere(['payment_status'=>$invmode])->asArray()->one();
            if($previous <> null){

                /*echo "<pre>";
                print_r($previous);
                die;*/
                Yii::$app->db->createCommand()->update(AutoInvoiceNum::tableName(),
                    ['valuation_id'=>$model->id],  [
                        'quotation_id' => $model->quotation_id,
                        'client_id'=>$model->client_id,
                    ])->execute();

            }

        }

        if($model->quotation_id<>null){
            $valuation_idz = ArrayHelper::map(Valuation::find()->where(['quotation_id'=>$model->quotation_id])->asArray()->all(), "id", "id");
        }else{
            $valuation_idz = $model->id;
        }

        $previous = AutoInvoiceNum::find()->where(['valuation_id'=>$valuation_idz])->andWhere(['payment_status'=>$invmode])->asArray()->one();

        if($previous<>null){
            $valuation_id = $previous['valuation_id'];
            $client_id = $previous['client_id'];
            $client_inv_no = $previous['client_inv_no'];
            $year_inv_num = $previous['year_inv_num'];
            $maxima_inv_num = $previous['maxima_inv_num'];
            $year = $previous['year'];
            $city = $previous['city'];
            $created_at = $previous['created_at'];
            $created_by = $previous['created_by'];

            $final_inv_num = $city.'-'.$maxima_inv_num.'-'.$year_inv_num;
        }
        else{
            $payment_terms =0;
            if($model->quotation_id<>null){
                if($model->quotaionData<> null){
                    if($model->quotaionData->payment_status == 1){
                        $payment_terms =1;
                    }
                }
            }

            if($payment_terms == 0) {


                $previous_maxima = AutoInvoiceNum::find()->select(['maxima_inv_num'])->where(['type' => 'maxima_inv_num'])->asArray()->one();
                if ($previous_maxima <> null) {
                    $maxima_inv_num = str_pad($previous_maxima['maxima_inv_num'] + 1, 4, "0", STR_PAD_LEFT);
                    //run all times
                    Yii::$app->db->createCommand()->update(AutoInvoiceNum::tableName(),
                        ['maxima_inv_num' => $maxima_inv_num],
                        ['type' => 'maxima_inv_num'])
                        ->execute();

                } else {
                    $maxima_inv_num = str_pad(1, 4, "0", STR_PAD_LEFT);
                    Yii::$app->db->createCommand()->insert(AutoInvoiceNum::tableName(), ['type' => 'maxima_inv_num', 'maxima_inv_num' => $maxima_inv_num])->execute();
                }

                $clientCurrentInvNum = AutoInvoiceNum::find()->select(['client_inv_no'])->where(['client_id' => $model->client_id])->orderBy(['id' => SORT_DESC])->asArray()->one();
                if ($clientCurrentInvNum['client_inv_no'] <> null) {
                    $client_inv_no = str_pad($clientCurrentInvNum['client_inv_no'] + 1, 4, "0", STR_PAD_LEFT);
                } else {
                    $client_inv_no = str_pad(1, 4, "0", STR_PAD_LEFT);
                }

                $yearInvNum = AutoInvoiceNum::find()->select(['year_inv_num'])->where(['year' => date('Y')])->orderBy(['id' => SORT_DESC])->asArray()->one();
                if ($yearInvNum['year_inv_num'] <> null) {
                    $year_inv_num = str_pad($yearInvNum['year_inv_num'] + 1, 4, "0", STR_PAD_LEFT);
                } else {
                    $year_inv_num = str_pad(1, 4, "0", STR_PAD_LEFT);
                }

                $valuation_id = $model->id;
                $client_id = $model->client_id;
                $year = date('Y');
                $city = $this->getCityShortName()[$model->building->city];
                $created_at = date('Y-m-d H:m:s');

                $final_inv_num = $city . '-' . $maxima_inv_num . '-' . $year_inv_num;
                if($model->quotation_id<>null){
                    Yii::$app->db->createCommand()->insert(AutoInvoiceNum::tableName(), [
                        'valuation_id' => $valuation_id,
                        'client_id' => $client_id,
                        'client_inv_no' => $client_inv_no,
                        'maxima_inv_num' => $maxima_inv_num,
                        'year_inv_num' => $year_inv_num,
                        'year' => $year,
                        'city' => $city,
                        'created_at' => $created_at,
                        'created_by' => $created_byy,
                        'quotation_id' => $model->quotation_id,
                        'payment_status' => 0,
                    ])->execute();
                }else{

                    Yii::$app->db->createCommand()->insert(AutoInvoiceNum::tableName(), [
                        'valuation_id' => $valuation_id,
                        'client_id' => $client_id,
                        'client_inv_no' => $client_inv_no,
                        'maxima_inv_num' => $maxima_inv_num,
                        'year_inv_num' => $year_inv_num,
                        'year' => $year,
                        'city' => $city,
                        'created_at' => $created_at,
                        'created_by' => $created_byy,
                        'payment_status' => 0,
                    ])->execute();
                }
            }else{

                //first entry
                $previous_maxima = AutoInvoiceNum::find()->select(['maxima_inv_num'])->where(['type' => 'maxima_inv_num'])->asArray()->one();
                if ($previous_maxima <> null) {
                    $maxima_inv_num = str_pad($previous_maxima['maxima_inv_num'] + 1, 4, "0", STR_PAD_LEFT);
                    //run all times
                    Yii::$app->db->createCommand()->update(AutoInvoiceNum::tableName(),
                        ['maxima_inv_num' => $maxima_inv_num],
                        ['type' => 'maxima_inv_num'])
                        ->execute();

                } else {
                    $maxima_inv_num = str_pad(1, 4, "0", STR_PAD_LEFT);
                    Yii::$app->db->createCommand()->insert(AutoInvoiceNum::tableName(), ['type' => 'maxima_inv_num', 'maxima_inv_num' => $maxima_inv_num])->execute();
                }

                $clientCurrentInvNum = AutoInvoiceNum::find()->select(['client_inv_no'])->where(['client_id' => $model->client_id])->orderBy(['id' => SORT_DESC])->asArray()->one();
                if ($clientCurrentInvNum['client_inv_no'] <> null) {
                    $client_inv_no = str_pad($clientCurrentInvNum['client_inv_no'] + 1, 4, "0", STR_PAD_LEFT);
                } else {
                    $client_inv_no = str_pad(1, 4, "0", STR_PAD_LEFT);
                }

                $yearInvNum = AutoInvoiceNum::find()->select(['year_inv_num'])->where(['year' => date('Y')])->orderBy(['id' => SORT_DESC])->asArray()->one();
                if ($yearInvNum['year_inv_num'] <> null) {
                    $year_inv_num = str_pad($yearInvNum['year_inv_num'] + 1, 4, "0", STR_PAD_LEFT);
                } else {
                    $year_inv_num = str_pad(1, 4, "0", STR_PAD_LEFT);
                }

                $valuation_id = $model->id;
                $client_id = $model->client_id;
                $year = date('Y');
                $city = $this->getCityShortName()[$model->building->city];
                $created_at = date('Y-m-d H:m:s');

                $final_inv_num = $city . '-' . $maxima_inv_num . '-' . $year_inv_num;
                Yii::$app->db->createCommand()->insert(AutoInvoiceNum::tableName(), [
                    'valuation_id' => $valuation_id,
                    'client_id' => $client_id,
                    'client_inv_no' => $client_inv_no,
                    'maxima_inv_num' => $maxima_inv_num,
                    'year_inv_num' => $year_inv_num,
                    'year' => $year,
                    'city' => $city,
                    'created_at' => $created_at,
                    'created_by' => $created_byy,
                    'quotation_id' => $model->quotation_id,
                    'payment_status' => 0,
                ])->execute();

                //second entry

                $previous_maxima_2 = AutoInvoiceNum::find()->select(['maxima_inv_num'])->where(['type' => 'maxima_inv_num'])->asArray()->one();
                if ($previous_maxima_2 <> null) {
                    $maxima_inv_num_2 = str_pad($previous_maxima_2['maxima_inv_num'] + 1, 4, "0", STR_PAD_LEFT);
                    //run all times
                    Yii::$app->db->createCommand()->update(AutoInvoiceNum::tableName(),
                        ['maxima_inv_num' => $maxima_inv_num_2],
                        ['type' => 'maxima_inv_num'])
                        ->execute();

                } else {
                    $maxima_inv_num_2 = str_pad(1, 4, "0", STR_PAD_LEFT);
                    Yii::$app->db->createCommand()->insert(AutoInvoiceNum::tableName(), ['type' => 'maxima_inv_num', 'maxima_inv_num' => $maxima_inv_num_2])->execute();
                }

                $clientCurrentInvNum_2 = AutoInvoiceNum::find()->select(['client_inv_no'])->where(['client_id' => $model->client_id])->orderBy(['id' => SORT_DESC])->asArray()->one();
                if ($clientCurrentInvNum_2['client_inv_no'] <> null) {
                    $client_inv_no_2 = str_pad($clientCurrentInvNum_2['client_inv_no'] + 1, 4, "0", STR_PAD_LEFT);
                } else {
                    $client_inv_no_2 = str_pad(1, 4, "0", STR_PAD_LEFT);
                }

                $yearInvNum_2 = AutoInvoiceNum::find()->select(['year_inv_num'])->where(['year' => date('Y')])->orderBy(['id' => SORT_DESC])->asArray()->one();
                if ($yearInvNum_2['year_inv_num'] <> null) {
                    $year_inv_num_2 = str_pad($yearInvNum_2['year_inv_num'] + 1, 4, "0", STR_PAD_LEFT);
                } else {
                    $year_inv_num_2 = str_pad(1, 4, "0", STR_PAD_LEFT);
                }

                $valuation_id = $model->id;
                $client_id = $model->client_id;
                $year = date('Y');
                $city = $this->getCityShortName()[$model->building->city];
                $created_at = date('Y-m-d H:m:s');

                $final_inv_num = $city . '-' . $maxima_inv_num_2 . '-' . $year_inv_num_2;
                Yii::$app->db->createCommand()->insert(AutoInvoiceNum::tableName(), [
                    'valuation_id' => $valuation_id,
                    'client_id' => $client_id,
                    'client_inv_no' => $client_inv_no_2,
                    'maxima_inv_num' => $maxima_inv_num_2,
                    'year_inv_num' => $year_inv_num_2,
                    'year' => $year,
                    'city' => $city,
                    'created_at' => $created_at,
                    'created_by' => $created_byy,
                    'quotation_id' => $model->quotation_id,
                    'payment_status' => 1,
                ])->execute();



            }
        }




        Yii::$app->db->createCommand()->update(AutoInvoiceNum::tableName(),
            [
                'valuation_id'=>$valuation_id,
                'client_id'=>$client_id,
                'client_inv_no'=>$client_inv_no,
                'maxima_inv_num'=>$maxima_inv_num,
                'year_inv_num'=>$year_inv_num,
                'year'=>$year,
                'city'=>$city,
                'updated_at'=>date('Y-m-d H:m:s'),
                'updated_by'=>$updated_byy,
            ],
            [
                'valuation_id' => $valuation_id,
                'client_id'=>$client_id,
                'payment_status'=>$invmode,
            ])->execute();




        // dd($final_inv_num);
        return $final_inv_num;


    }

    public function getInvoiceNumberQt($model)
    {
        if(!Yii::$app->user->isGuest){
            $created_byy = \Yii::$app->user->identity->id;
            $updated_byy = \Yii::$app->user->identity->id;
        }else{
            $created_byy = null;
            $updated_byy = null;
        }


            $valuation_idz = $model->id;


        $previous = AutoInvoiceNum::find()->where(['quotation_id'=>$valuation_idz])->asArray()->one();



        if($previous<>null){
            $valuation_id = $previous['valuation_id'];
            $client_id = $previous['client_id'];
            $client_inv_no = $previous['client_inv_no'];
            $year_inv_num = $previous['year_inv_num'];
            $maxima_inv_num = $previous['maxima_inv_num'];
            $year = $previous['year'];
            $city = $previous['city'];
            $created_at = $previous['created_at'];
            $created_by = $previous['created_by'];

            $final_inv_num = $city.'-'.$maxima_inv_num.'-'.$year_inv_num;
        }
        else{

            $previous_maxima = AutoInvoiceNum::find()->select(['maxima_inv_num'])->where(['type'=>'maxima_inv_num'])->asArray()->one();
            if($previous_maxima<>null){
                $maxima_inv_num = str_pad($previous_maxima['maxima_inv_num']+1, 4, "0", STR_PAD_LEFT);
                //run all times
                Yii::$app->db->createCommand()->update(AutoInvoiceNum::tableName(),
                    ['maxima_inv_num'=>$maxima_inv_num],
                    ['type'=>'maxima_inv_num'])
                    ->execute();
            }else{
                $maxima_inv_num = str_pad(1, 4, "0", STR_PAD_LEFT);
                Yii::$app->db->createCommand()->insert(AutoInvoiceNum::tableName(), ['type'=>'maxima_inv_num','maxima_inv_num'=>$maxima_inv_num])->execute();
            }

            $clientCurrentInvNum = AutoInvoiceNum::find()->select(['client_inv_no'])->where(['client_id'=>$model->client_name])->orderBy(['id' => SORT_DESC])->asArray()->one();


            if($clientCurrentInvNum['client_inv_no'] <> null){
                $client_inv_no = str_pad($clientCurrentInvNum['client_inv_no']+1, 4, "0", STR_PAD_LEFT);
            }else{
                $client_inv_no = str_pad(1, 4, "0", STR_PAD_LEFT);
            }



            $yearInvNum = AutoInvoiceNum::find()->select(['year_inv_num'])->where(['year'=>date('Y')])->orderBy(['id' => SORT_DESC])->asArray()->one();
            if($yearInvNum['year_inv_num']<>null){
                $year_inv_num = str_pad($yearInvNum['year_inv_num']+1, 4, "0", STR_PAD_LEFT);
            }else{
                $year_inv_num = str_pad(1, 4, "0", STR_PAD_LEFT);
            }

            $valuation_id = $model->id;

            $client_id = $model->client_name;

            $year = date('Y');
            $properties = CrmReceivedProperties::find()->where(['quotation_id' => $valuation_id])->one();
            $city = $this->getCityShortName()[$properties->building->city];

            /*echo "<pre>";
            print_r($properties->building->city);
            die;*/
            $created_at = date('Y-m-d H:m:s');

            $final_inv_num = $city.'-'.$maxima_inv_num.'-'.$year_inv_num;


            Yii::$app->db->createCommand()->insert(AutoInvoiceNum::tableName(), [
                'quotation_id'=>$valuation_id,
                'client_id'=>$client_id,
                'client_inv_no'=>$client_inv_no,
                'maxima_inv_num'=>$maxima_inv_num,
                'year_inv_num'=>$year_inv_num,
                'year'=>$year,
                'city'=>$city,
                'created_at'=>$created_at,
                'created_by'=>$created_byy,
            ])->execute();
        }




        Yii::$app->db->createCommand()->update(AutoInvoiceNum::tableName(),
            [
              //  'valuation_id'=>$valuation_id,
                'quotation_id'=>$valuation_id,
                'client_id'=>$client_id,
                'client_inv_no'=>$client_inv_no,
                'maxima_inv_num'=>$maxima_inv_num,
                'year_inv_num'=>$year_inv_num,
                'year'=>$year,
                'city'=>$city,
                'updated_at'=>date('Y-m-d H:m:s'),
                'updated_by'=>$updated_byy,
            ],
            [
              //  'valuation_id' => $valuation_id,
                'quotation_id'=>$valuation_id,
                'client_id'=>$client_id,
            ])->execute();

        // dd($final_inv_num);
        return $final_inv_num;


    }

    public function getMonInvNum($model, $module_type=null)
    {
        if(!Yii::$app->user->isGuest){
            $created_byy = \Yii::$app->user->identity->id;
            $updated_byy = \Yii::$app->user->identity->id;
        }else{
            $created_byy = null;
            $updated_byy = null;
        }

        // if($model->quotation_id<>null){
        //     $valuation_idz = ArrayHelper::map(Valuation::find()->where(['quotation_id'=>$model->quotation_id])->asArray()->all(), "id", "id");
        // }else{
        //     $valuation_idz = $model->id;
        // }
        if($module_type<>null) {
            $previous = AutoMonthlyInvoiceNum::find()
                ->where(['client_id' => $model->id, 'month' => (date('m') -1), 'module_type' => $module_type])
                ->orderBy(['id' => SORT_DESC])->asArray()->one();
        }else{
            $previous = AutoMonthlyInvoiceNum::find()
                ->where(['client_id' => $model->id, 'month' => (date('m') - 1),'year'=>date('Y')])
                ->orderBy(['id' => SORT_DESC])->asArray()->one();
        }

        if($previous<>null){
            $client_id      = $previous['client_id'];
            $client_id      = $previous['module_type'];
            $client_inv_no  = $previous['client_inv_no'];
            $year_inv_num   = $previous['year_inv_num'];
            $maxima_inv_num = $previous['maxima_inv_num'];
            $year           = $previous['year'];
            $month          = $previous['month'];
            $city           = $previous['city'];
            $created_at     = $previous['created_at'];
            $created_by     = $previous['created_by'];
            $rec_id         = $previous['id'];

           // $final_inv_num = 'C:'.$city.' - YIN:'.$year.' - MIN:'.$maxima_inv_num.' - YIN:'.$year_inv_num.' - CIN:'.$client_inv_no.' - Month:'.$month;
            $final_inv_num = $month.'-'.$year.'-'.$maxima_inv_num;
        }else{

            $previous_maxima = AutoMonthlyInvoiceNum::find()
                ->select(['maxima_inv_num'])
                ->where(['type'=>'maxima_inv_num'])
                ->asArray()->one();

            if($previous_maxima<>null){
                $maxima_inv_num = str_pad($previous_maxima['maxima_inv_num']+1, 4, "0", STR_PAD_LEFT);
                Yii::$app->db->createCommand()->update(AutoMonthlyInvoiceNum::tableName(), ['maxima_inv_num' => $maxima_inv_num],['type' => 'maxima_inv_num'])->execute();
            }else{
                $maxima_inv_num = str_pad(193, 4, "0", STR_PAD_LEFT);
                Yii::$app->db->createCommand()->insert(AutoMonthlyInvoiceNum::tableName(), ['type'=>'maxima_inv_num','maxima_inv_num'=>$maxima_inv_num])->execute();
            }

            $clientCurrentInvNum = AutoMonthlyInvoiceNum::find()
                ->select(['client_inv_no'])
                ->where(['client_id'=>$model->id])
                ->orderBy(['id' => SORT_DESC])->asArray()->one();

            if($clientCurrentInvNum['client_inv_no'] <> null){
                $client_inv_no = str_pad($clientCurrentInvNum['client_inv_no']+1, 4, "0", STR_PAD_LEFT);
            }else{
                $client_inv_no = str_pad(1, 4, "0", STR_PAD_LEFT);
            }

            // $monthSearch = date('m');
            // if($monthSearch==0){
            //     $monthSearch = 12;
            // }

            $yearInvNum = AutoMonthlyInvoiceNum::find()
                ->select(['year_inv_num'])
                ->where([ 'year'=>date('Y') ])
                ->orderBy(['id' => SORT_DESC])->asArray()->one();

            if($yearInvNum['year_inv_num']<>null){
                $year_inv_num = str_pad($yearInvNum['year_inv_num']+1, 4, "0", STR_PAD_LEFT);
            }else{
                $year_inv_num = str_pad(1, 4, "0", STR_PAD_LEFT);
            }

            $client_id = $model->id;
            $year = date('Y');
            $month = date('m') - 1;
            if($month == 0){
                $month = 12;
            }
            $month_name = date("F", mktime(0, 0, 0, $month, 10));
            $city = $this->getCityShortName()[$model->zone_id];
            $created_at = date('Y-m-d H:m:s');

          //  $final_inv_num = 'C:'.$city.' - YIN:'.$year.' - MIN:'.$maxima_inv_num.' - YIN:'.$year_inv_num.' - CIN:'.$client_inv_no.' - Month:'.$month;
            $final_inv_num = $month.'-'.$year.'-'.$maxima_inv_num;

            $insertData                 = new AutoMonthlyInvoiceNum;
            $insertData->client_id      = $client_id;
            $insertData->module_type    = $module_type;
            $insertData->client_inv_no  = $client_inv_no;
            $insertData->maxima_inv_num = $maxima_inv_num;
            $insertData->year_inv_num   = $year_inv_num;
            $insertData->year           = $year;
            $insertData->month          = $month;
            $insertData->city           = $city;
            $insertData->created_at     = $created_at;
            $insertData->created_by     = $created_byy;
            if($insertData->save()){
                $rec_id = $insertData->id;
            }

        }


        //run all times
        Yii::$app->db->createCommand()->update(AutoMonthlyInvoiceNum::tableName(),
            [
                'client_id'      => $client_id,
                'module_type'    => $module_type,
                'client_inv_no'  => $client_inv_no,
                'maxima_inv_num' => $maxima_inv_num,
                'year_inv_num'   => $year_inv_num,
                'year'           => $year,
                'month'          => $month,
                'city'           => $city,
                'updated_at'     => date('Y-m-d H:m:s'),
                'updated_by'     => $updated_byy,
            ],
            [
                'id'        => $rec_id,
                'client_id' => $client_id,
            ])
            ->execute();

       // dd($final_inv_num);
        return $final_inv_num;

    }

    public function getStatusVerifyArr()
    {
        return[
            '2' => 'Unverified',
            '1' => 'Verified',
        ];
    }

    public function getCrmInvoiceArr()
    {
        return[
            '1' => 'REQ',
            '2' => 'MEQ',
            '3' => 'BCQ',
        ];
    }

    public function getStatusVerifyLabel()
    {
        return[
            '2' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Unverified').'</span>',
            '1' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Verified').'</span>',
        ];
    }

    public function checkAndExcludeWeekends_old($startDate=null, $endDate=null)
    {
        $start = new DateTime($startDate);
        $end = new DateTime($endDate);

        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($start, $interval ,$end);

        $count = 0;
        foreach($daterange as $date){
            if($date->format("N") < 6) {
                $count++;
            }
        }
        return $count;
    }
    
        
    //this function calculate number of hours betweern start and end dates and 
    // then divide by 24 to get num of days but exclude weekend (saturday and sunday)
    public function checkAndExcludeWeekends_oldd($startDate=null, $endDate=null)
    {
        $start = new DateTime($startDate);
        $end = new DateTime($endDate);

        $interval = new DateInterval('PT1H');
        $daterange = new DatePeriod($start, $interval ,$end);

        $count = 0;
        foreach($daterange as $date){
            if($date->format("N") < 6) {
                $count++;
            }
        }
        return $count/24;
    }
    

    public function checkAndExcludeWeekends($startDate=null, $endDate=null)
    {
        $holidays = ArrayHelper::map(\app\models\Holidays::find()->select(['date'])->asArray()->all(), "date", "date");
        
        $start = new DateTime($startDate);
        $end = new DateTime($endDate);
        // $start = new DateTime("2023-01-20 12:00:00");
        // $end = new DateTime("2023-01-31 12:00:00");
        
        $interval = new DateInterval("PT1H");
        $period = new DatePeriod($start, $interval, $end);
        $weekend = array('Saturday', 'Sunday');
        $hours = 0;
        
        foreach ($period as $dt) {

            if (!in_array($dt->format('l'), $weekend) && !in_array($dt->format('Y-m-d'), $holidays)) {
                    $hours++;
            }
        }
        $days = $hours/24;
        return $days;
    }
    public function checkAndExcludeWeekends_hours($startDate=null, $endDate=null)
    {
        $holidays = ArrayHelper::map(\app\models\Holidays::find()->select(['date'])->asArray()->all(), "date", "date");

        $start = new DateTime($startDate);
        $end = new DateTime($endDate);
        // $start = new DateTime("2023-01-20 12:00:00");
        // $end = new DateTime("2023-01-31 12:00:00");

        $interval = new DateInterval("PT1H");
        $period = new DatePeriod($start, $interval, $end);
        $weekend = array('Saturday', 'Sunday');
        $hours = 0;

        foreach ($period as $dt) {

            if (!in_array($dt->format('l'), $weekend) && !in_array($dt->format('Y-m-d'), $holidays)) {
                $hours++;
            }
        }
        return $hours;
        $days = $hours/24;
        return $days;
    }

    public function wmFormate($amount){
        if($amount >= 100){
            return number_format($amount);
        }else{
            return number_format($amount,2);
        }
    }
    public function getEmiratedListArrSla()
    {
        $city_array= array();
        $city_array[0] = 'All Cities';
        $all_cities = ArrayHelper::map($this->emiratedList, "id", "title");
        foreach ($all_cities as $key => $city){
            $city_array[$key] = $city;
        }
        return $city_array;
    }
    public function getBuildingTenureArrSla()
    {

        return [
            '1' => Yii::t('app', 'Non-Freehold'),
            '2' => Yii::t('app', 'Freehold'),
            '3' => Yii::t('app', 'Both'),
        ];
    }

    public function getTatNumberSla()
    {
        $floors= array();
        for($i=1;$i<=5;$i++){
            $floors[$i]=$i;
        }
        return $floors;
    }
    
        public function getModelName()
    {
        $controller_name = Yii::$app->controller->id;
        return 'app\models\\'.str_replace(' ', '', ucwords(str_replace('-', ' ', $controller_name)));
    }
    
    
    public function getLastActionHitory($data)
    {

        $html = '';
        $action = 'Last Data Updated';

        
        if(isset($data['file_type']) && $data['file_type'] <> null){
            $rec_type = 'masterfile';
            if(isset($data['rec_type']) && $data['rec_type'] <> null){
                $rec_type = $data['rec_type'];
            }
            $query = \app\models\History::find()->where(['rec_type'=>$rec_type, 'file_type'=>$data['file_type'], 'action'=>'data_updated'])->orderBy(['id' => SORT_DESC])->one();
            if($query==null){
                $action = 'Data Created';
                $query = \app\models\History::find()->where(['rec_type'=>$rec_type, 'file_type'=>$data['file_type'], 'action'=>'data_created'])->orderBy(['id' => SORT_DESC])->one();
            }
            $query_verify = \app\models\History::find()->where(['rec_type'=>$rec_type, 'file_type'=>$data['file_type'], 'action'=>'status_verified'])->orderBy(['id' => SORT_DESC])->one();
        }
        else{
            $query = \app\models\History::find()->where(['model_name'=>$data['model_name'], 'model_id'=>$data['model_id'], 'action'=>'data_updated'])->orderBy(['id' => SORT_DESC])->one();
            if($query==null){
                $action = 'Data Created';
                $query = \app\models\History::find()->where(['model_name'=>$data['model_name'], 'model_id'=>$data['model_id'], 'action'=>'data_created'])->orderBy(['id' => SORT_DESC])->one();
            }
            $query_verify = \app\models\History::find()->where(['model_name'=>$data['model_name'], 'model_id'=>$data['model_id'], 'action'=>'status_verified'])->orderBy(['id' => SORT_DESC])->one();
        }

        if($query<>null){
            $html .= '<label class="custom-label mx-1">'.$action.' at <span style="padding: 4px;color: red; text-decoration: underline;">'.date('d-F-Y h:i:s', strtotime($query->created_at)) .'</span> By <span style="padding: 4px;color: red; text-decoration: underline;">'.$query->user->firstname.' '.$query->user->lastname.' </span> </label>';
        }

        if($query_verify<>null){
            $html .= '|';
            $html .= '<label class="custom-label mx-1">Last Verified at <span style="padding: 4px;color: red; text-decoration: underline;">'.date('d-F-Y h:i:s', strtotime($query_verify->created_at)) .'</span> By <span style="padding: 4px;color: red; text-decoration: underline;">'.$query_verify->user->firstname.' '.$query_verify->user->lastname.' </span> </label>';
        }

        // dd($html);

        return $html;

    }

    public function getTypesOfUnitsArr()
    {
        return [
            '1' => Yii::t('app', '1 Type'),
            '2' => Yii::t('app', '2 Types'),
            '3' => Yii::t('app', '3 Types'),
            '4' => Yii::t('app', '4 Types'),
            '5' => Yii::t('app', '5 Types'),
            '6' => Yii::t('app', '6 Types'),
            '7' => Yii::t('app', '7 Types'),
            '8' => Yii::t('app', '8 Types'),
            '9' => Yii::t('app', '9 Types'),
            '10' => Yii::t('app', '10 Types'),

        ];
    }
    
        public function getDaysArr()
    {
        return [
            '1' => Yii::t('app', '1'),
            '2' => Yii::t('app', '2'),
            '3' => Yii::t('app', '3'),
            '4' => Yii::t('app', '4'),
            '5' => Yii::t('app', '5'),
            '6' => Yii::t('app', '6'),
            '7' => Yii::t('app', '7'),
            '8' => Yii::t('app', '8'),
            '9' => Yii::t('app', '9'),
            '10' => Yii::t('app', '10'),

        ];
    }

    public function getClientIndustry()
    {
        return [
            '1' => Yii::t('app', 'Developer'),
            '2' => Yii::t('app', 'Family'),
            '3' => Yii::t('app', 'Publicly Listed'),
            '4' => Yii::t('app', 'Private Company'),
            '5' => Yii::t('app', 'Audit Firm'),
            '6' => Yii::t('app', 'Law Firm'),
        ];
    }

    public function getClientSenisitivity ()
    {
        return [
            '1' => Yii::t('app', 'Price Sensitiive'),
            '2' => Yii::t('app', 'Time Sensitive'),
            '3' => Yii::t('app', 'Value Sensitive'),
            '4' => Yii::t('app', 'Payment Terms Sensitive'),


        ];
    }


    public function getClientPriority()
    {
        return [
            '1' => Yii::t('app', '1'),
            '2' => Yii::t('app', '2'),
            '3' => Yii::t('app', '3'),


        ];
    }

    public function getNumberOfTotalPropertiesClientOwns()
    {
        return [
            '1' => Yii::t('app', '1'),
            '2' => Yii::t('app', '2'),
            '3' => Yii::t('app', '3'),
            '4' => Yii::t('app', '4'),
            '5' => Yii::t('app', '5'),
            '6' => Yii::t('app', '6'),
            '7' => Yii::t('app', '7'),
            '8' => Yii::t('app', '8'),
            '9' => Yii::t('app', '9'),
            '10' => Yii::t('app', '10'),
            '11' => Yii::t('app', 'Greater than 10'),
        ];
    }

    public function wmFormateno($amount){

            return number_format($amount,2);
    }

    public function wmFormatenocomma($amount){

        return number_format($amount,2, '.', '');
    }

    public function getEmiratedListSearchArr()
    {
        return ArrayHelper::map($this->emiratedList, "title", "title");
    }
  

    public function getVoneValuations(){
        $vone_vals = \app\models\Valuation::find()
            ->select(['id','reference_number'])
            // ->where(['OR',
            //     ['LIKE', 'reference_number', 'V1'],
            //     ['LIKE', 'reference_number',  'V-1']
            // ])
            ->all();
        return ArrayHelper::map($vone_vals, "reference_number", "reference_number");
    }    
    
    public function getRevisedReasonsVone()
    {

        return [
            '1' => Yii::t('app', 'Low valuation challenge by client'),
            '6' => Yii::t('app', 'Value remains same'),
            '2' => Yii::t('app', 'High valuation challenge by client'),
            '3' => Yii::t('app', 'Error made'),
            '5' => Yii::t('app', 'Modification requested by client'),
            '4' => Yii::t('app', 'Copy previous valuation details'),
            '7' => Yii::t('app', 'High valuation by Rera'),
            '8' => Yii::t('app', 'Reminder By Client'),
        ];
    }
    
    



    public function getEmailStatusLable()
    {
        return [
            0 => '<span class="badge grid-badge badge-warning">'.Yii::t('app','Pending').'</span>',
            1 => '<span class="badge grid-badge badge-info">'.Yii::t('app','Sent').'</span>',
        ];
    }
    
    public function formatTimeAmPm($time) {
        $parts = explode(':', $time);
        $hour = (int)$parts[0];
        $minute = (int)$parts[1];
        
        $period = ($hour >= 12) ? 'PM' : 'AM';
        
        if ($hour > 12) {
            $hour -= 12;
        } elseif ($hour === 0) {
            $hour = 12;
        }
        
        return sprintf('%02d:%02d %s', $hour, $minute, $period);
    }
    
    
    public function getInspectionCharges($mode_of_transport, $start_kilometres, $end_kilometres)
    {
        $$amount = 0;
        if($mode_of_transport<>null){
            $charges = \app\models\FinancialManagement::findOne(1);
            if($mode_of_transport==1){
                $amount = $end_kilometres-$start_kilometres;
                $amount = $amount*$charges->company_car_rate;
            }
            else if($mode_of_transport==2){
                $amount = $end_kilometres-$start_kilometres;
                $amount = $amount*$charges->personal_car_rate;
            }
            else if($mode_of_transport==3){
                $amount = $end_kilometres-$start_kilometres;
                $amount = $amount*$charges->external_transport_rate;
            }
            
        }
        return $amount;
    }


    public function getTotalReceivedCountFee()
    {
        $query = Valuation::find()
        ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
        ->asArray()->one();
        return $query;
    }

    public function getTotalInspectedCountFee()
    {
        $query = Valuation::find()
        ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
        ->joinWith(['inspectProperty'])
        ->where(['IS NOT', 'inspect_property.inspection_done_date', null])
        ->asArray()->one();
        return $query;
    }
    
    
    public function getErrorStatusVone()
    {

        return [
            'pending' => Yii::t('app', 'Pending'),
            'solved' => Yii::t('app', 'Solved'),
        ];
    }



    public function getErrorStatusVoneLable()
    {
        return [
            'pending' => '<span class="badge grid-badge badge-warning">'.Yii::t('app','Pending').'</span>',
            'solved' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Solved').'</span>',
        ];
    }
    public function getReinstatementSources()
    {

        return [
            '1' => Yii::t('app', 'Colliers'),
            '2' => Yii::t('app', 'Aecom'),
            '3' => Yii::t('app', 'Linesight'),
            '4' => Yii::t('app', 'Turner & Townsend'),
            '5' => Yii::t('app', 'Jones Lang Lasalle'),
            '6' => Yii::t('app', 'Rider Levett Bucknall'),
            '7' => Yii::t('app', 'Arcallis'),
        ];
    }

    public function getReinstatementProperties()
    {

        return [
            '1' => Yii::t('app', 'Apartment'),
            '2' => Yii::t('app', 'Villa/Townhouse'),
            '3' => Yii::t('app', 'Building'),
            '4' => Yii::t('app', 'Colliers'),
            '5' => Yii::t('app', 'Colliers'),
            '6' => Yii::t('app', 'Colliers'),
            '7' => Yii::t('app', 'Colliers'),
            '8' => Yii::t('app', 'Colliers'),
            '9' => Yii::t('app', 'Colliers'),
            '10' => Yii::t('app', 'Colliers'),
            '11' => Yii::t('app', 'Colliers'),
            '12' => Yii::t('app', 'Colliers'),
        ];
    }

    public function getBrokerageIquiryTypes()
    {

        return [
            '1' => Yii::t('app', 'Buy'),
            '2' => Yii::t('app', 'Sell'),
            '3' => Yii::t('app', 'Rent Out'),
            '4' => Yii::t('app', 'Rent In'),
        ];
    }

    public function getBrokerageIquiryImportance()
    {

        return [
            '1' => Yii::t('app', 'Exclusive'),
            '2' => Yii::t('app', 'Non Exclusive'),
        ];
    }

    public function getBrokeragepurposes()
    {

        return [
            '1' => Yii::t('app', 'Investment'),
            '2' => Yii::t('app', 'End User'),
            '3' => Yii::t('app', 'Buy Other Property'),
        ];
    }
    public function getBrokerageTargetClients()
    {

        return [
            '1' => Yii::t('app', 'Corporate'),
            '2' => Yii::t('app', 'End User'),
            '3' => Yii::t('app', 'Buy Other Property'),
        ];
    }

    public function getBrokeragePropertyStatus()
    {

        return [
            '1' => Yii::t('app', 'Sold'),
            '2' => Yii::t('app', 'Rented'),
            '3' => Yii::t('app', 'Available'),
            '4' => Yii::t('app', 'Closed'),
        ];
    }

    public function getRecostQuality()
    {

        return [
            '1' => Yii::t('app', 'Low'),
            '2' => Yii::t('app', 'Middle'),
            '3' => Yii::t('app', 'High'),
        ];
    }

    public function getRecostTyplogy()
    {

        return [
            '1' => Yii::t('app', 'Low Rise'),
            '2' => Yii::t('app', 'Middle Rise'),
            '3' => Yii::t('app', 'High Rise'),
        ];
    }

    public function getRecostProjectPeriod()
    {

        return [
            '1' => Yii::t('app', '1 year'),
            '2' => Yii::t('app', '2 years'),
            '3' => Yii::t('app', '3 years'),
            '4' => Yii::t('app', '4 years'),
            '5' => Yii::t('app', '5 years'),
            '6' => Yii::t('app', '6 years'),
            '7' => Yii::t('app', '7 years'),
            '8' => Yii::t('app', '8 years'),
            '9' => Yii::t('app', '9 years'),
            '10' => Yii::t('app','10 years'),

        ];
    }

    public function getRecostAssets()
    {

        return [
            '1' => Yii::t('app', 'Electrical'),
            '2' => Yii::t('app', 'Fire System'),
            '3' => Yii::t('app', 'Heat Ventilation & Air Conditioning'),
            '4' => Yii::t('app', 'Mechanical'),
            '5' => Yii::t('app', 'Security System'),
            '6' => Yii::t('app', 'Water System'),
        ];
    }
    public function getBrokerageDealPossibility()
    {

        return [
            '1' => Yii::t('app', 'Low'),
            '2' => Yii::t('app', 'Medium'),
            '3' => Yii::t('app', 'High'),
        ];
    }

    public function getClientContactTypes()
    {

        return [
            '1' => Yii::t('app', 'Bilal Contacts'),
            '2' => Yii::t('app', 'Property Owners Contacts'),
            '3' => Yii::t('app', 'ICAI Contacts'),
            '4' => Yii::t('app', 'Broker Contacts'),
            '5' => Yii::t('app', 'Developers Contacts'),
            '6' => Yii::t('app', 'Banks Contacts'),
            '7' => Yii::t('app', 'ICAP Contacts'),
            '8' => Yii::t('app', 'Family Groups'),
            '9' => Yii::t('app', 'Insurance Companies Contacts'),
            '10' => Yii::t('app', 'Law Firms Contacts'),
            '11' => Yii::t('app', 'Publicly/Private Companies'),
            '12' => Yii::t('app', 'Audit Firms Contacts'),
            '13' => Yii::t('app', 'Government Authorties'),
            '14' => Yii::t('app', 'Industrial Client Contacts'),
            '15' => Yii::t('app', 'Asset Management Companies Contacts'),
            '16' => Yii::t('app', 'Property Managers Contacts'),
            '17' => Yii::t('app', 'Owners Association Companies Contacts'),
        ];
    }

    public function getSourceOfContact()
    {

        return [
            '1' => Yii::t('app', 'Meeting'),
            '2' => Yii::t('app', 'Whatsapp'),
            '3' => Yii::t('app', 'Phone'),
            '4' => Yii::t('app', 'Email'),
            '5' => Yii::t('app', 'Website'),
            '6' => Yii::t('app', 'Personal Reference'),
            '7' => Yii::t('app', 'Website'),
            '8' => Yii::t('app', 'Facebook'),
            '9' => Yii::t('app', 'Linkedin'),
            '10' => Yii::t('app', 'Instagram'),
            '11' => Yii::t('app', 'Twitter'),
            '12' => Yii::t('app', 'Walk In'),
        ];
    }

    public function getCostSource()
    {

        return [
            '1' => Yii::t('app', 'MOU'),
            '2' => Yii::t('app', 'Sale and Purchase Agreement'),
            '3' => Yii::t('app', 'Title Deed'),
            '4' => Yii::t('app', 'Affection Plan'),
            '5' => Yii::t('app', 'Initial Contract for Sale'),
        ];
    }
    public function getCalculateWorkingHours($startDateTime, $endDateTime)
    {
        if($startDateTime <> null && $endDateTime <> null) {

// Define working hours
            $workingHoursStart = 8.5; // 9:00 AM
            $workingHoursEnd = 18; // 5:00 PM

            // Initialize total working hours
            $totalWorkingHours = 0;

            // Clone the start date to iterate through the time range
            //  $currentDateTime = clone $startDateTime;
           /* echo $startDateTime;
            die;*/
            $currentDateTime = clone $startDateTime;

            // Loop through each hour between start and end dates
            while ($currentDateTime < $endDateTime) {
                // Check if it's a weekday (Monday to Friday)
                if ($currentDateTime->format('N') >= 1 && $currentDateTime->format('N') <= 5) {
                    // Check if it's within working hours
                    $currentHour = $currentDateTime->format('G');
                    if ($currentHour >= $workingHoursStart && $currentHour < $workingHoursEnd) {
                        $totalWorkingHours++;
                    }
                }

                // Move to the next hour
                $currentDateTime->add(new DateInterval('PT1H'));
            }

            return $totalWorkingHours;
        }else{
            return 0;
        }
    }

    public function getCalculateWorkingMinutes($startDateTime, $endDateTime)
    {
        if($startDateTime <> null && $endDateTime <> null) {


            //$startDateTime = '2023-10-02 08:00:00'; // Start DateTime
            //$endDateTime = '2023-10-06 17:00:00';  // End DateTime
   /*         $workingHoursStart = 9; // 9:00 AM
            $workingHoursEnd = 17;
            echo $startDateTime;
            die;*/

// Define working hours
            $workingHoursStart = 8.5; // 9:00 AM
            $workingHoursEnd = 18; // 5:00 PM

            $start = new DateTime($startDateTime);
            $end = new DateTime($endDateTime);

            // Initialize the total minutes counter
            $totalMinutes = 0;

            // Define the working days (Monday = 1, Sunday = 7)
            $workingDays = [1, 2, 3, 4, 5]; // Monday to Friday

            // Loop through each day within the date range
            while ($start <= $end) {
                $currentDay = $start->format('N'); // Get the day of the week (1 = Monday, 7 = Sunday)

                // Check if the current day is a working day
                if (in_array($currentDay, $workingDays)) {
                    // Calculate the minutes in the working hours
                    $startOfWorkingDay = clone $start;
                    $startOfWorkingDay->setTime($workingHoursStart, 0);
                    $endOfWorkingDay = clone $start;
                    $endOfWorkingDay->setTime($workingHoursEnd, 0);

                    // Calculate the minutes for this day
                    $minutesForDay = max(0, min($end, $endOfWorkingDay)->getTimestamp() - max($start, $startOfWorkingDay)->getTimestamp()) / 60;

                    // Add the minutes to the total
                    $totalMinutes += $minutesForDay;
                }

                // Move to the next day
                $start->modify('+1 day');
            }


            return $totalMinutes;

        }else{
            return 0;
        }
    }
// to get bank clients
    public function getClientBanksList() {
        $clientBanks = \app\models\Company::find()
            ->where(['status' => 1])
            ->andWhere(['client_type' => 'bank'])
            ->andWhere(['allow_for_valuation' => 1])
            ->orderBy(['title' => SORT_ASC])
            ->all();

        return $clientBanks;
    }

    public function getPropertyTypeImport()
    {

        return [
            'Building' => 81,
            'Industrial-Warehouse' => 24,
            'Medical Office' => 43,
            'Office' => 28,
            'Other' => 92,
            'Shop' => 17,
            'Showroom' => 92,
            'Sized Partition' => 92,
            'Sport & Entertainment' => 92,
            'Workshop' => 17,
            'Commercial Land' => 4,
            'Land - General' => 39,
            'Land for Agriculture' => 39,
            'Land for Airport' => 39,
            'Land for Education' => 39,
            'Land for Government Housing' => 39,
            'Land for Government Organizations' => 39,
            'Land for Industrial' => 29,
            'Land for Labor Camp' => 39,
            'Land for Shopping Mall' => 39,
            'Land for Sports' => 39,
            'Land for Warehouse' => 29,
            'Residential Land' => 20,
            'Other Land' => 29,
            'Apartment' => 1,
            'Serviced/Hotel Apartment' => 12,
            'Villa' => 2,
            'Villa Plot' => 20,

        ];
    }

    // for number options in dropdown fileds
    public function getCrmOptionZeroToThousandNumber()
    {
        $number = array();
        for ($i = 0; $i <= 1000; $i++) {
            $number[$i] = $i;
        }
        return $number;
    }
    public function getCrmOptionZeroToTwoHundredNumber()
    {
        $number = array();
        for ($i = 0; $i <= 1000; $i++) {
            $number[$i] = $i;
        }
        return $number;
    }
    public function getCrmOptionZeroToFiftyNumber()
    {
        $number = array();
        for ($i = 0; $i <= 50; $i++) {
            $number[$i] = $i;
        }
        return $number;
    }

    public function getCrmOptionZeroToTenNumber()
    {
        $numbers = array();
        for ($i = 0; $i <= 10; $i++) {
            $numbers[$i] = $i;
        }
        return $numbers;
    }

    public function getCrmOptionOneToTenNumber()
    {
        $numbers = array();
        for ($i = 1; $i <= 10; $i++) {
            $numbers[$i] = $i;
        }
        return $numbers;
    }

    public function getLandIdArr()
    {
        $landIds = \app\models\Properties::find()
            ->where(['like', 'title', 'land%', false])
            ->andWhere(['status' => 1])
            ->all();

        return $landIds;
    }

    public function getVillaIdArr()
    {
        $villaIds = \app\models\Properties::find()
            ->where(['like', 'title', 'villa%', false])
            ->andWhere(['status' => 1])
            ->all();

        return $villaIds;
    }

    public function getTownHouseArr()
    {
        $townhouseIds = \app\models\Properties::find()
            ->where(['like', 'title', 'townhouse%', false])
            ->andWhere(['status' => 1])
            ->all();

        return $townhouseIds;
    }

    // for number of floors in building
    public function getCrmNoOfFloors()
    {
        $floors = array();
        for ($i = 1; $i <= 200; $i++) {
            $floors[$i] = $i;
        }
        return $floors;
    }

    // for number of apartment floors
    public function getCrmNoOfApartmentFloors()
    {
        return [
            1 => '1',
            2 => 'Duplex',
        ];
    }
    public function getScopeOfWorkList()
    {
        $scopeOfServiceList = \app\models\ScopeOfService::find()
            ->where(['status_verified' => 1])
            ->orderBy(['title' => SORT_ASC])
            ->all();

        return ArrayHelper::map($scopeOfServiceList, "id", "title");
    }
    public function getDrawingsAvailable()
    {

        return [
            'civil' => Yii::t('app', 'Civil Drawing'),
            'mechanical' => Yii::t('app', 'Mechanical Drawing'),
            'electrical' => Yii::t('app', 'Electrical Drawing'),
            'plumbing' => Yii::t('app', 'Plumbing Drawing'),
            'hvac' => Yii::t('app', 'HVAC Drawing'),
        ];
    }
    public function ZohoRefereshToken()
    {
        //get access token
        $RefereshToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_referesh_token');
        $ClientID = Yii::$app->appHelperFunctions->getSetting('zohobooks_client_id');
        $ClientSecret = Yii::$app->appHelperFunctions->getSetting('zohobooks_client_secret');
        $RedirectURI = Yii::$app->appHelperFunctions->getSetting('zohobooks_redirect_uri');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://accounts.zoho.com/oauth/v2/token?refresh_token='.$RefereshToken.'&client_id='.$ClientID.'&client_secret='.$ClientSecret.'&redirect_uri='.$RedirectURI.'&grant_type=refresh_token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'Cookie: stk=cafdb6af1737ab5ffe2671e4aead7298; JSESSIONID=49F1656D557ACC11B35B7AF579128B04; _zcsr_tmp=a0ca8f32-2d34-4d48-a42d-e5667dd4ca25; b266a5bf57=a711b6da0e6cbadb5e254290f114a026; e188bc05fe=8db261d30d9c85a68e92e4f91ec8079a; iamcsr=a0ca8f32-2d34-4d48-a42d-e5667dd4ca25'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
        $access_token = $response->access_token;
        $query =  Yii::$app->db->createCommand()->update('setting', ['config_value' => $access_token], 'config_name="zohobooks_access_token"')->execute();

        return $access_token;
    }

    public function postClientInZohoBooks($id, $contact, $company)
    {

        Yii::$app->appHelperFunctions->ZohoRefereshToken();
        $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');
        $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');
        $user = \app\models\Company::find()->where(['id' => $id])->one();

        if(empty($user->zohobooks_id))
        {
            // Create Contact in Zoho
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://www.zohoapis.com/books/v3/contacts?organization_id='.$OrganizationID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('JSONString' => '{
                "contact_name": "'.$contact.'",
                "company_name": "'.$company.'"
            }'),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response);

            //if contact already exists
            if($response->code == 3062)
            {
                if (preg_match('/"([^"]+)"/', $response->message, $m)) {
                    $existing_zoho_name = $m[1];
                    $existing_zoho_name = str_replace(' ', '%20', $existing_zoho_name);

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'https://www.zohoapis.com/books/v3/contacts/?organization_id='.$OrganizationID.'&contact_name='.$existing_zoho_name,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                        CURLOPT_HTTPHEADER => array(
                            'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
                        ),
                    ));

                    $response = curl_exec($curl);
                    curl_close($curl);
                    $response = json_decode($response);
                    $query =  Yii::$app->db->createCommand()->update('company', ['zohobooks_id' => $response->contacts[0]->contact_id], 'id = '.$id.'')->execute();
                    if($response->contacts[0]->contact_id > 0){
                        Yii::$app->getSession()->addFlash('success', Yii::t('app','Customer Updated in ZohoBooks'));
                    }
                    return $response->contacts[0]->contact_id;
                }
            }


            $contact_id = $response->contact->contact_id;
            $query =  Yii::$app->db->createCommand()->update('company', ['zohobooks_id' => $contact_id], 'id = '.$id.'')->execute();
            if($contact_id > 0){
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Customer Created in ZohoBooks'));
            }else{
                Yii::$app->getSession()->addFlash('error', Yii::t('app','Customer NOT Created in ZohoBooks'));
            }

            return $contact_id;
        }
    }
    public function changeStatus($invoice_id ,$OrganizationID, $AccessToken)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://www.zohoapis.com/books/v3/invoices/'.$invoice_id.'/status/sent?organization_id='.$OrganizationID,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response);
        Yii::$app->getSession()->addFlash('success', Yii::t('app', $response->message));
    }

    public function getGfaRangesArr()
    {
        return [
            1=>'1 - 10',
            2=>'11 - 20',
            3=>'21 - 30',
            4=>'31 - 40',
            5=>'41 - 50',
            6=>'51 - 60',
            7=>'61 - 70',
            8=>'71 - 80',
            9=>'81 - 90',
            10=>'91 - 100',
        ];
    }
    public function getCalculateAvgVals1($id,$startDate,$endDate)
    {
        global $data;
        $valuations = Valuation::find()
            ->select([
                'valuation.id',
                'schedule_inspection.inspection_date',
                'schedule_inspection.inspection_time',
                'inspect_property.inspection_done_date',
            ])
            ->leftJoin('schedule_inspection', 'schedule_inspection.valuation_id = valuation.id')
            ->leftJoin('inspect_property', 'inspect_property.valuation_id = valuation.id')
            ->where(['service_officer_name'=>$id])
            ->andWhere(['valuation_status'=>5])
            ->andWhere(['parent_id'=>null])
            ->andWhere(['not', ['valuation.inspection_type' => 3]])
            ->andWhere(['IS NOT', 'schedule_inspection.inspection_date', null])
            ->andWhere(['BETWEEN', 'valuation.submission_approver_date', "$startDate", "$endDate"])
            ->asArray()
            ->all();

        // echo "<pre>";
        // print_r($valuations);
        // die;

        $total_count = COUNT($valuations);
        $total_valuation_time = 0;

        foreach($valuations as $key => $valuation)
        {
            $startDate = $valuation['inspection_date'].' '.$valuation['inspection_time'] . ':00';
            $endDate = $valuation['inspection_done_date'];

            $startDateTime = new DateTime($startDate); // Start date and time
            $endDateTime = new DateTime($endDate);

            $total_valuation_time = $total_valuation_time + $this->getCalculateWorkingHours($startDateTime, $endDateTime);
        }
        $data += $total_valuation_time/$total_count;
        $_SESSION['time'] = $data;
        return $total_valuation_time/$total_count;
    }

    public function getCalculateAvgVals2($id,$startDate,$endDate)
    {
        $valuations = Valuation::find()
            ->select([
                'valuation.id',
                'valuation_approvers_data.created_at',
                'inspect_property.inspection_done_date',
            ])
            ->leftJoin('valuation_approvers_data', 'valuation_approvers_data.valuation_id = valuation.id')
            ->leftJoin('inspect_property', 'inspect_property.valuation_id = valuation.id')
            ->leftJoin('schedule_inspection', 'schedule_inspection.valuation_id = valuation.id')
            ->where(['service_officer_name'=>$id])
            ->andWhere(['valuation_status'=>5])
            ->andWhere(['valuation_approvers_data.approver_type' => 'valuer'])
            ->andWhere(['parent_id'=>null])
            ->andWhere(['not', ['valuation.inspection_type' => 3]])
            ->andWhere(['IS NOT', 'schedule_inspection.inspection_date', null])
            ->andWhere(['BETWEEN', 'valuation.submission_approver_date', "$startDate", "$endDate"])
            ->asArray()
            ->all();

        // echo "<pre>";
        // print_r($valuations);
        // die;

        $total_count = COUNT($valuations);
        $total_valuation_time = 0;

        foreach($valuations as $key => $valuation)
        {
            $startDate = $valuation['created_at'];
            $endDate = $valuation['inspection_done_date'];

            $startDateTime = new DateTime($startDate); // Start date and time
            $endDateTime = new DateTime($endDate);

            $total_valuation_time = $total_valuation_time + $this->getCalculateWorkingHours($startDateTime, $endDateTime);
        }
        $data2 += $total_valuation_time/$total_count;
        $_SESSION['recom_time'] = $data2;
        return $total_valuation_time/$total_count;
    }

    public function getCalculateAvgVals3($id,$startDate,$endDate)
    {

        $valuations = Valuation::find()
            ->select([
                'valuation.id',
                'valuation_approvers_data.created_at',
                'inspect_property.inspection_done_date',
                'valuation.instruction_date',
                'valuation.instruction_time',
                'valuation.submission_approver_date',
            ])
            ->leftJoin('valuation_approvers_data', 'valuation_approvers_data.valuation_id = valuation.id')
            ->leftJoin('inspect_property', 'inspect_property.valuation_id = valuation.id')
            ->leftJoin('schedule_inspection', 'schedule_inspection.valuation_id = valuation.id')
            ->where(['service_officer_name'=>$id])
            ->andWhere(['valuation_status'=>5])
            ->andWhere(['valuation_approvers_data.approver_type' => 'valuer'])
            ->andWhere(['parent_id'=>null])
            ->andWhere(['not', ['valuation.inspection_type' => 3]])
            ->andWhere(['IS NOT', 'schedule_inspection.inspection_date', null])
            ->andWhere(['BETWEEN', 'valuation.submission_approver_date', "$startDate", "$endDate"])
            ->asArray()
            ->all();

        // echo "<pre>";
        // print_r($valuations);
        // die;

        $total_count = COUNT($valuations);
        $total_valuation_time = 0;

        foreach($valuations as $key => $valuation)
        {
            $startDate = $valuation['instruction_date'].' '.$valuation['instruction_time'].':00';
            $endDate = $valuation['submission_approver_date'];

            $startDateTime = new DateTime($startDate); // Start date and time
            $endDateTime = new DateTime($endDate);

            $total_valuation_time = $total_valuation_time + $this->getCalculateWorkingHours($startDateTime, $endDateTime);
        }
        $data3 += $total_valuation_time/$total_count;
        $_SESSION['tat_time'] = $data3/8.5;

        $days = $total_valuation_time/8.5;
        return $days/$total_count;
    }

    public function getCalculateAvgVals4($id,$startDate,$endDate)
    {

        $valuations = Valuation::find()
            ->select([
                'valuation.id',
                'valuation_approvers_data.created_at',
                'inspect_property.inspection_done_date',
                'valuation.instruction_date',
                'valuation.instruction_time',
                'valuation.submission_approver_date',
            ])
            ->leftJoin('valuation_approvers_data', 'valuation_approvers_data.valuation_id = valuation.id')
            ->leftJoin('inspect_property', 'inspect_property.valuation_id = valuation.id')
            ->leftJoin('schedule_inspection', 'schedule_inspection.valuation_id = valuation.id')
            ->where(['service_officer_name'=>$id])
            ->andWhere(['valuation_status'=>5])
            ->andWhere(['parent_id'=>null])
            ->andWhere(['not', ['valuation.inspection_type' => 3]])
            ->andWhere(['valuation_approvers_data.approver_type' => 'valuer'])
            ->andWhere(['IS NOT', 'schedule_inspection.inspection_date', null])
            ->andWhere(['BETWEEN', 'valuation.submission_approver_date', "$startDate", "$endDate"])
            ->asArray()
            ->all();

        // echo "<pre>";
        // print_r($valuations);
        // die;

        $total_count = COUNT($valuations);
        $total_valuation_time = 0;

        foreach($valuations as $key => $valuation)
        {
            $startDate = $valuation['instruction_date'].' '.$valuation['instruction_time'].':00';
            $endDate = $valuation['submission_approver_date'];

            $startDateTime = new DateTime($startDate); // Start date and time
            $endDateTime = new DateTime($endDate);

            $total_valuation_time = $total_valuation_time + $this->getCalculateWorkingHours($startDateTime, $endDateTime);
        }

        $data4 += $total_valuation_time/$total_count;
        $_SESSION['working_time'] = $data4/8.5;


        $days = $total_valuation_time/8.5;
        return $total_count/$days;
    }
    public function getCalculateWorkingHours2($startDateTime, $endDateTime)
    {
        if($startDateTime <> null && $endDateTime <> null) {

            // Define working hours
            $workingHoursStart = 8; // 9:00 AM
            $workingHoursEnd = 18; // 5:00 PM

            // Initialize total working hours
            $totalWorkingHours = 0;

            // Clone the start date to iterate through the time range
            //  $currentDateTime = clone $startDateTime;
            /* echo $startDateTime;
             die;*/
            $currentDateTime = clone $startDateTime;

            // Loop through each hour between start and end dates
            while ($currentDateTime < $endDateTime) {
                // Check if it's a weekday (Monday to Friday)
                if ($currentDateTime->format('N') >= 1 && $currentDateTime->format('N') <= 5) {
                    // Check if it's within working hours
                    $currentHour = $currentDateTime->format('G');
                    if ($currentHour >= $workingHoursStart && $currentHour <= $workingHoursEnd) {
                        $totalWorkingHours++;
                    }
                }

                // Move to the next hour
                $currentDateTime->add(new DateInterval('PT1H'));
            }

            return $totalWorkingHours;
        }else{
            return 0;
        }
    }

    public function getCrmOptionZeroToTwoThousandNumber()
    {
        $number = array();
        for ($i = 0; $i <= 2000; $i++) {
            $number[$i] = $i;
        }
        return $number;
    }

    public function getClientSegments()
    {

        return [
            '6' => Yii::t('app', 'Banks'),
            '13' => Yii::t('app', 'Government'),
            '14' => Yii::t('app', 'Industrial Client'),
            '15' => Yii::t('app', 'Asset Management'),
            '5' => Yii::t('app', 'Developers'),
            '16' => Yii::t('app', 'Property Managers'),
            '17' => Yii::t('app', 'Owners Association Companies'),
            '2' => Yii::t('app', 'Property Owners'),
            '3' => Yii::t('app', 'ICAI'),
            '7' => Yii::t('app', 'ICAP'),
            '4' => Yii::t('app', 'Broker'),
            '8' => Yii::t('app', 'Family'),
            '9' => Yii::t('app', 'Insurance Companies'),
            '10' => Yii::t('app', 'Law Firms'),
            '11' => Yii::t('app', 'Publicly/Private'),
            '12' => Yii::t('app', 'Audit Firms'),
            '1' => Yii::t('app', 'Bilal Contacts'),
            '18' => Yii::t('app', 'Real Estate Trustee'),
        ];
    }



    public function getValuationApproachByProperty($property)
    {
        if(in_array($property, [1,2,4,5,6,11,12,20,23,26,29,39,41,46,47,48,49,50,53,91,94])){ // market
            $valuation_approach = 0;
        } else if(in_array($property, [3,10,15,17,24,25,28,38,42,43,59,81,93])){ // income
            $valuation_approach = 1;
        } else if(in_array($property, [9,16,18])){ // profit
            $valuation_approach = 2;
            // } else if(in_array($property, [20,46])){ // cost
            //     $valuation_approach = 3;
            // } else {

        }

        return $valuation_approach;
    }

    public function getPropertyRequiredDocuments($id)
    {
        $propertyReqDocs = \app\models\Properties::find()
            ->select('required_documents')
            ->where(['id' => $id])
            ->andWhere(['status' => 1])
            ->one();

        if ($propertyReqDocs !== null) {
            return $propertyReqDocs->required_documents;
        } else {
            return null;
        }
    }

    public function getPropertyOptionalDocuments($id)
    {
        $propertyReqDocs = \app\models\Properties::find()
            ->select('optional_documents')
            ->where(['id' => $id])
            ->andWhere(['status' => 1])
            ->one();

        if ($propertyReqDocs !== null) {
            return $propertyReqDocs->optional_documents;
        } else {
            return null;
        }
    }

    public function getUniqueClientValuationReference_old($company)
    {

        $previous_record = ClientValuation::find()
            ->select([
                'id',
                'client_reference',
            ])
            ->orderBy(['id' => SORT_DESC])->asArray()->one();

        if($previous_record == null){
            $new_reference = sprintf('%04d', 0001);
            $prev_nickname = '';
        }else {
            $prev_ref = explode("-", $previous_record['client_reference']);
            $prev_nickname = $prev_ref[0];
            $new_reference = $prev_ref[3];
        }

        $nickname = Company::find()
            ->select(['nick_name'])
            ->where(['id' => $company])
            ->scalar();


        $client_nickname = ($nickname !== null) ? $nickname : '';

        do {
            if($prev_nickname == $nickname){
                $new_reference++;
            }
            $reference = $client_nickname . '-Windmills-' . date('Y') . '-' . sprintf('%04d', $new_reference);

            $previous_record = ClientValuation::find()
                ->select(['id', 'client_reference'])
                ->where(['client_reference' => $reference])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()
                ->one();

        } while ($previous_record != null);

        return $client_nickname.'-Windmills-' . date('Y') . '-' . (sprintf('%04d', $new_reference));

    }
    public function getUniqueClientValuationReference($company)
    {

        $previous_record = ClientValuation::find()
            ->select([
                'id',
                'client_reference',
            ])
            ->where(['created_by' => Yii::$app->user->identity->id])
            ->orderBy(['id' => SORT_DESC])->asArray()->one();



        if($previous_record == null){
            $new_reference = sprintf('%04d', 1);
            $prev_nickname = '';
        }else {
            $prev_ref = explode("-", $previous_record['client_reference']);
            $prev_nickname = $prev_ref[0];
            $new_reference = $prev_ref[3];
        }

        $nickname = Company::find()
            ->select(['nick_name'])
            ->where(['id' => $company])
            ->scalar();


        $client_nickname = ($nickname !== null) ? $nickname : '';

        do {
            if($prev_nickname == $nickname){
                $new_reference++;
            }
            $reference = $client_nickname . '-Windmills-' . date('Y') . '-' . sprintf('%04d', $new_reference);

            $previous_record = ClientValuation::find()
                ->select(['id', 'client_reference'])
                ->where(['client_reference' => $reference])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()
                ->one();

        } while ($previous_record != null);

        return $client_nickname.'-Windmills-' . date('Y') . '-' . (sprintf('%04d', $new_reference));

    }
    public function getAverageStarRating($average)
    {
        switch (true) {
            case ($average == 0):
                $stars = '<span class="gnfstar">☆☆☆☆☆</span> (' . $average . '%)';
                break;
            case ($average > 0 && $average <= 20):
                $stars = '<span class="yfstar">★</span><span class="gnfstar">☆☆☆☆</span> (' . $average . '%)';
                break;
            case ($average > 20 && $average <= 40):
                $stars = '<span class="yfstar">★★</span><span class="gnfstar">☆☆☆</span> (' . $average . '%)';
                break;
            case ($average > 40 && $average <= 60):
                $stars = '<span class="yfstar">★★★</span><span class="gnfstar">☆☆</span> (' . $average . '%)';
                break;
            case ($average > 60 && $average <= 80):
                $stars = '<span class="yfstar">★★★★</span><span class="gnfstar">☆</span> (' . $average . '%)';
                break;
            case ($average > 80 && $average <= 100):
                $stars = '<span class="yfstar">★★★★★</span> (' . $average . '%)';
                break;
            default:
                $stars = '<span class="gnfstar">☆☆☆☆☆</span>';
                break;
        }

        return $stars;
    }
    public function getSeaLevels()
    {
        $floors= array();
        for($i=1;$i<=1000;$i++){
            $floors[$i]=$i;
        }
        $floors[1001]='Not Applicable';
        $floors[1002]='Not Known';
        return $floors;
    }

    public function getFirstFiveWorkingDays() {
        $workingDays = [];
        $year = date('Y');
        $month = date('m');
        $date = new DateTime("$year-$month-01");
        $end = new DateTime($date->format('Y-m-t'));

        while (count($workingDays) < 5 && $date <= $end) {
            $dayOfWeek = $date->format('N'); // ISO-8601 numeric representation of the day of the week (1 for Monday, 7 for Sunday)
            if ($dayOfWeek < 6) { // Only consider Monday to Friday
                $workingDays[] = $date->format('Y-m-d');
            }
            $date->modify('+1 day');
        }

        return $workingDays;
    }

        public function getNthWorkingDay($n) {
        $currentYear = date('Y');
        $currentMonth = date('m');
        $dayCount = 0;
        $date = new DateTime("$currentYear-$currentMonth-01");

        while ($dayCount < $n) {
            $weekday = $date->format('N'); // 1 for Monday, 7 for Sunday
            if ($weekday < 6) { // Monday to Friday are working days
                $dayCount++;
            }
            if ($dayCount < $n) {
                $date->modify('+1 day');
            }
        }

        return $date->format('Y-m-d');
    }

    public function getCrmOptionOneToNumber($limit)
    {
        $number = array();
        for ($i = 1; $i <= $limit; $i++) {
            $number[$i] = $i;
        }
        return $number;
    }
    public function getScopeOfWorkListShort()
    {
        $scopeOfServiceList = \app\models\ScopeOfService::find()
            ->where(['status_verified' => 1])
            ->orderBy(['title' => SORT_ASC])
            ->all();

        $scopeList = [];
        foreach ($scopeOfServiceList as $scope) {
            switch ($scope->id) {
                case 1:
                    $title = 'MV';
                    break;
                case 2:
                    $title = 'RIC';
                    break;
                case 3:
                    $title = 'FV';
                    break;
                case 4:
                    $title = 'DRC';
                    break;
                case 5:
                    $title = 'MR';
                    break;
                case 6:
                    $title = 'RFS';
                    break;
                case 7:
                    $title = 'SCS';
                    break;
                case 8:
                    $title = 'BCA';
                    break;
                case 9:
                    $title = 'RICA';
                    break;
                case 10:
                    $title = 'TS';
                    break;
                case 11:
                    $title = 'ERR';
                    break;
                case 12:
                    $title = 'ARCT';
                    break;
                case 13:
                    $title = 'RFV';
                    break;
                default:
                    $title = $scope->title;
            }
            $scopeList[$scope->id] = $title;
        }

        return $scopeList;
    }

    public function  getOpertaionsValuationTotal($period,$custom_period = null) {

        if($period > 0) {
            if($period == 9){
                $Date=(explode(" - ",$custom_period));
                $from_date = $Date[0];
                $to_date = $Date[1];
            }else{
                $date_array = $this->getFilterDates($period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $allvalues = (new \yii\db\Query())
                ->select('COUNT(id) as total_valuations_count')
                ->from('valuation')
                ->where(['valuation_status' => 5])
                ->andFilterWhere([
                    'between', 'submission_approver_date', $from_date. ' 00:00:00', $to_date. ' 23:59:59'
                ])
                ->one();
        }else{
            $allvalues = (new \yii\db\Query())
                ->select('COUNT(id) as total_valuations_count')
                ->from('valuation')
                ->where(['valuation_status' => 5])
                ->one();
        }
        return $allvalues;


    }
    public function  getOpertaionsInspectionTotal($period,$custom_period = null,$created_by) {

        if($period > 0) {
            if($period == 9){
                $Date=(explode(" - ",$custom_period));
                $from_date = $Date[0];
                $to_date = $Date[1];
            }else{
                $date_array = $this->getFilterDates($period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $allvalues = (new \yii\db\Query())
                ->select('COUNT(schedule_inspection.id) as total_inspection_count')
                ->from('schedule_inspection')
                ->innerJoin('valuation', "valuation.id=schedule_inspection.valuation_id")
                ->where(['valuation.valuation_status' => 5])
                ->andWhere(['schedule_inspection.created_by' => $created_by])
                ->andwhere(['not in','valuation.id', ['11920']])
                ->andwhere(['not in','valuation.created_by', [28,14,27,25,26,9,40,43,42,11,31,21,41,37,39,19,206]])
                ->andWhere(['valuation.parent_id' => null])
                ->andWhere(['valuation.quotation_id' => null])
                ->andWhere(['valuation.client_module_id' => null])
                ->andFilterWhere([
                    'between', 'valuation.submission_approver_date', $from_date. ' 00:00:00', $to_date. ' 23:59:59'
                ])
                ->one();
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = (new \yii\db\Query())
                ->select('COUNT(schedule_inspection.id) as total_inspection_count')
                ->from('schedule_inspection')
                ->innerJoin('valuation', "valuation.id=schedule_inspection.valuation_id")
                ->where(['valuation.valuation_status' => 5])
                ->andWhere(['schedule_inspection.created_by' => $created_by])
                ->andwhere(['not in','valuation.id', ['11920']])
                ->andwhere(['not in','valuation.created_by', [28,14,27,25,26,9,40,43,42,11,31,21,41,37,39,19,206]])
                ->andWhere(['valuation.parent_id' => null])
                ->andWhere(['valuation.quotation_id' => null])
                ->andWhere(['valuation.client_module_id' => null])
                ->andFilterWhere([
                    'between', 'valuation.submission_approver_date', $start_date. ' 00:00:00', $end_date. ' 23:59:59'
                ])
                ->one();
        }

        return $allvalues['total_inspection_count'];


    }
    public function  getOpertaionsUnstoredTotal($period,$custom_period = null,$created_by) {

        if($period > 0) {
            if($period == 9){
                $Date=(explode(" - ",$custom_period));
                $from_date = $Date[0];
                $to_date = $Date[1];
               /* echo "<pre>";
                print_r($Date);
                die;*/
            }else{
                $date_array = $this->getFilterDates($period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
              /*  echo "<pre>";
                print_r($date_array);
                die;*/
            }

            $allvalues = (new \yii\db\Query())
                ->select('COUNT(valuation.id) as total_stored_count')
                ->from('valuation')
                ->where(['valuation_status' => 5])
                ->andWhere(['created_by' => $created_by])
                ->andWhere(['not', ['signature_img' => null]])
                ->andwhere(['not in','valuation.id', ['11920']])
                ->andwhere(['not in','created_by', [28,14,27,25,26,9,40,43,42,11,31,21,41,37,39,19,206]])
                ->andWhere(['valuation.parent_id' => null])
                ->andWhere(['valuation.quotation_id' => null])
                ->andWhere(['valuation.client_module_id' => null])
                ->andFilterWhere([
                    'between', 'submission_approver_date', $from_date. ' 00:00:00', $to_date. ' 23:59:59'
                ])
                ->one();
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = (new \yii\db\Query())
                ->select('COUNT(valuation.id) as total_stored_count')
                ->from('valuation')
                ->where(['valuation_status' => 5])
                ->andWhere(['created_by' => $created_by])
                ->andWhere(['not', ['signature_img' => null]])
                ->andwhere(['not in','valuation.id', ['11920']])
                ->andwhere(['not in','created_by', [28,14,27,25,26,9,40,43,42,11,31,21,41,37,39,19,206]])
                ->andWhere(['valuation.parent_id' => null])
                ->andWhere(['valuation.quotation_id' => null])
                ->andWhere(['valuation.client_module_id' => null])
                ->andFilterWhere([
                    'between', 'submission_approver_date', $start_date. ' 00:00:00', $end_date. ' 23:59:59'
                ])
                ->one();
        }

        return $allvalues['total_stored_count'];


    }

    public function  getDataTeamVerifiedListings($period,$custom_period = null,$created_by) {

        if($period > 0) {
            if($period == 9){
                $Date=(explode(" - ",$custom_period));
                $from_date = $Date[0];
                $to_date = $Date[1];
            }else{
                $date_array = $this->getFilterDates($period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }
            $allvalues = (new \yii\db\Query())
                ->select('COUNT(model_id) as total_verified_count')
                ->from('history')
                ->where(['action' => 'status_verified'])
                ->andWhere(['created_by' => $created_by])
                ->andWhere(['model_name' => 'app\models\ListingsTransactions'])
                ->distinct()
                ->andFilterWhere([
                    'between', 'created_at', $from_date. ' 00:00:00', $to_date. ' 23:59:59'
                ])
                ->one();
        }else{

            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = (new \yii\db\Query())
                ->select('COUNT(model_id) as total_verified_count')
                ->from('history')
                ->where(['action' => 'status_verified'])
                ->andWhere(['created_by' => $created_by])
                ->andWhere(['model_name' => 'app\models\ListingsTransactions'])
                ->distinct()
                ->andFilterWhere([
                    'between', 'created_at', $start_date. ' 00:00:00', $end_date. ' 23:59:59'
                ])
                ->one();
        }

        return $allvalues['total_verified_count'];


    }


    public function getCrmOptionZeroToNumber($limit)
    {
        $number = array();
        for ($i = 0; $i <= $limit; $i++) {
            $number[$i] = $i;
        }
        return $number;
    }


    function getCalculateWorkingHoursAndDays($startDate, $endDate) {

        $workingHoursStart = 8.5;
        $workingHoursEnd = 18;
        $timezone = 'Asia/Dubai';

        // $start = new DateTime($startDate, new DateTimeZone($timezone));
        // $end = new DateTime($endDate, new DateTimeZone($timezone));
        $start = $startDate;
        $end = $endDate;

        $totalWorkingHours = 0;
        $totalWorkingDays = 0;

        $current = clone $start;

        while ($current <= $end) {
            // Check if current day is a working day (Monday to Friday)
            if ($current->format('N') <= 5) {
                $workDayStart = clone $current;
                $workDayStart->setTime(floor($workingHoursStart), ($workingHoursStart - floor($workingHoursStart)) * 60);

                $workDayEnd = clone $current;
                $workDayEnd->setTime(floor($workingHoursEnd), ($workingHoursEnd - floor($workingHoursEnd)) * 60);

                if ($current == $start) {
                    $effectiveStart = max($workDayStart, $start);
                } else {
                    $effectiveStart = $workDayStart;
                }

                if ($current->format('Y-m-d') == $end->format('Y-m-d')) {
                    $effectiveEnd = min($workDayEnd, $end);
                } else {
                    $effectiveEnd = $workDayEnd;
                }

                if ($effectiveStart < $effectiveEnd) {
                    $totalWorkingHours += ($effectiveEnd->getTimestamp() - $effectiveStart->getTimestamp()) / 3600;
                    $totalWorkingDays += 1;
                }
            }

            // Move to the next day
            $current->modify('+1 day');
        }

        return [
            'totalWorkingHours' => $totalWorkingHours,
            'totalWorkingDays' => $totalWorkingDays
        ];
    }

    public function getIncomeTypesArr()
    {
        return [
            1=>'Studio',
            2=>'1 Bedroom',
            3=>'2 Bedroom',
            4=>'3 Bedroom',
            5=>'4 Bedroom',
            6=>'Penthouse',
            7=>'Shop',
            8=>'Office',
        ];
    }

    public function getIncomeRentsPropertyTypesArr()
    {
        return [
            'apartment'=>'Apartment',
            'villa'=>'Villa',
            'townhouse'=>'Townhouse',
            'residential floor'=>'Residential Floor',
            'residential plot'=>'residential plot',
            'villa compound'=>'villa compound',
            'shop'=>'Shop',
            'office'=>'Office',
            'penthouse'=>'Penthouse',
            'residential building'=>'Residential Building',
            'hotel apartment'=>'Hotel Apartment',
            'warehouse'=>'Warehouse',
            'labor camp'=>'Labor Camp',
        ];
    }

    public function getIncomeEjariPropertyTypesArr()
    {
        return [
            'Villa'=>'Villa',
            'Other'=>'Other',
            'Apartment'=>'Apartment',
            'Shop'=>'Shop',
            'Industrial-Warehouse'=>'Industrial-Warehouse',
            'Office'=>'Office',
            'Showroom'=>'Showroom',
            'Land - General'=>'Land - General',
            'Restaurant'=>'Restaurant',
            'Labor Camp'=>'Labor Camp',
            'Medical Office'=>'Medical Office',
            'Workshop'=>'Workshop',
            'Building'=>'Building',
            'Penthouse'=>'Penthouse',
            'Serviced/Hotel Apartment'=>'Serviced/Hotel Apartment',
            'Other Land'=>'Other Land',
            'Industrial-Warehouse'=>'Industrial-Warehouse',
        ];
    }

    public function getPropertiesDocumentsListArrNlaSource()
    {



        $select_query= \app\models\NlaSources::find()
            ->select(['id','title'])
            ->orderBy(['title' => SORT_ASC])->where(['status'=>1])->asArray()->all();

        return ArrayHelper::map($select_query, "id", "title");

      /*  return [

            '34' => Yii::t('app', 'Sale and Purchase Agreement'),
            '6' => Yii::t('app', 'MoU for sale'),
            '41' => Yii::t('app', 'Client Confirmation'),
            '9' => Yii::t('app', 'Floor Plans'),
            '4' => Yii::t('app', 'Land Lease Agreement'),
            '40' => Yii::t('app', 'Inspection Confirmation'),
            '37' => Yii::t('app', 'Contact Person Confirmation'),
            '8' => Yii::t('app', 'Affection or Site Plan'),
            '36' => Yii::t('app', 'REIDIN Database'),
            '39' => Yii::t('app', 'Bayut Building Guide'),
            '45' => Yii::t('app', 'Bayut Area Guide'),
            '38' => Yii::t('app', 'Propsearch'),
            '46' => Yii::t('app', 'Property Finder'),
            '49' => Yii::t('app', 'Building Completion Certificate'),
            '50' => Yii::t('app', 'Building Permit'),
            '51' => Yii::t('app', 'Ejari Contracts'),
            '52' => Yii::t('app', 'Rent List Spreadsheet'),
            '33' => Yii::t('app', 'None'),

        ];*/

    }

    public function getPoliciesTitles()
    {
        $select_query= \app\models\PoliciesTitles::find()
            ->select(['id','title'])
            ->orderBy(['title' => SORT_ASC])->where(['status'=>1])->asArray()->all();

        return ArrayHelper::map($select_query, "id", "title");
    }

    public function getPolisciesCategory()
    {
        return [
            1 =>'Secretary',
            2 =>'Management',
            3 =>'Sale',
            4 =>'Marketing',
            5 =>'Proposal',
            6 =>'Finance',
            7 =>'Human Resources',
            8 =>'Legal',
            9 =>'Admin',
            10 =>'Real Estate Valuation',
            11 =>'Machinery Equipment Valuation',
            12 =>'Reserve Fund Study',
            13 =>'Business Consultancy Services',
            14 =>'Information Technology',
            15 =>'Empanelment',

        ];
    }

    public function getPolisciesCategoryslug()
    {
        return [
            1 =>'msecretary',
            2 =>'mmanagment',
            3 =>'bsale',
            4 =>'bmarketing',
            5 =>'bproposal',
            6 =>'ofinance',
            7 =>'ohr',
            8 =>'olegal',
            9 =>'oadmin',
            10 =>'srev',
            11 =>'smev',
            12 =>'srfs',
            13 =>'sbcs',
            14 =>'oit',
            15 =>'bempanelment',

        ];
    }
    public function getFatherProfessions()
    {
        return [
            1 => "Doctor",
            2 => "Engineer",
            3 => "Teacher",
            4 => "Lawyer",
            5 => "Businessman",
            6 => "Farmer",
            7 => "Police Officer",
            8 => "Army Officer",
            9 => "Architect",
            10 => "Scientist",
            11 => "Accountant",
            12 => "Civil Servant",
            13 => "Artist",
            14 => "Driver",
            15 => "Mechanic",
            16 => "Electrician",
            17 => "Pilot",
            18 => "Software Developer",
            19 => "Banker",
            20 => "Chef",
            21 => "Plumber",
            22 => "Dentist",
            23 => "Pharmacist",
            24 => "Journalist",
            25 => "Professor",

        ];
    }

    public function getMotherProfessions()
    {
        return [
            1 => "Housewife",
            2 => "Teacher",
            3 => "Engineer",
            4 => "Nurse",
            5 => "Lawyer",
            6 => "Scientist",
            7 => "Accountant",
            8 => "Artist",
            9 => "Businesswoman",
            10 => "Doctor",
            11 => "Manager",
            12 => "Police Officer",
            13 => "Architect",
            14 => "Social Worker",
            15 => "Pharmacist",
            16 => "Consultant",
            17 => "Banker",
            18 => "Lecturer",
            19 => "Pilot",
            20 => "Dentist",

        ];
    }


    public function getJobtitles()
    {
        return [
            1 =>'MRICS',
            2 =>'IT Manager',
            3 =>'Finance Manager',
            4 =>'Sales Manager',
            5 =>'Cheif Business Officer',
            6 =>'Finance Manager',
            7 =>'Human Resources Manager',
            8 =>'Admin',
            9 =>'Other',
            10 =>'Real Estate Valuation Officer',
            11=>'Chief Technology Officer (CTO)',
            12=>'Lead Developer (PHP, Laravel, YII2)',
            13=>'Junior Developer (PHP, Laravel, YII2)',
            14=>'Lawyer',
            15=>'Marketing Manager',
            16=>'Data Entry Specialist',
            17=>'Valuation Operations Support',
            18=>'Valuer',
            19=>'General Accountant',
            20=>'Mid Level Developer (PHP, Laravel, YII2)',
            21=>'Executive Secretary',
            22=>'Recruitment Officer',
            23=>'Customer Service Representative & Proposals Manager',
            24=>'HR Director',
            25 =>'Head Of Residential Valuation(MRICS)',

        ];
    }

    public function getJobDepartment()
    {
        return [
            1 =>'Valuation',
            2 =>'IT',
            3 =>'Finance',
            4 =>'Sales',
            5 =>'Business',
            6 =>'Human Resources',
            7 =>'Admin',
            8 =>'Other',
            9 => 'Real Estate Valuation Officer',
            10 => 'Data Management',
            11 => 'Legal Management',
        ];
    }

    public function getUniqueOnlineValuationReference()
    {

        $previous_record = OnlineValuation::find()
            ->select([
                'id',
                'client_reference',
            ])
            //->where(['created_by' => Yii::$app->user->identity->id])
            ->orderBy(['id' => SORT_DESC])->asArray()->one();



        if($previous_record == null){
            $new_reference = sprintf('%04d', 1);
            $prev_nickname = '';
        }else {
            $prev_ref = explode("-", $previous_record['client_reference']);
            $prev_nickname = $prev_ref[0];
            $new_reference = $prev_ref[3];
        }

        $nickname = 'OW';


        $client_nickname = ($nickname !== null) ? $nickname : '';

        do {
            if($prev_nickname == $nickname){
                $new_reference++;
            }
            $reference = $client_nickname . '-Windmills-' . date('Y') . '-' . sprintf('%04d', $new_reference);

            $previous_record = OnlineValuation::find()
                ->select(['id', 'client_reference'])
                ->where(['client_reference' => $reference])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()
                ->one();

        } while ($previous_record != null);

        return $client_nickname.'-Windmills-' . date('Y') . '-' . (sprintf('%04d', $new_reference));

    }

}


?>
