<?php

namespace app\components\helpers;

use app\models\CrmQuotations;
use app\models\CrmReceivedProperties;
use app\models\QuotationFeeMasterFile;
use app\models\ValuationQuotationPropertiesDetails;
use app\models\ValuationQuotation;
use app\models\Company;
use app\models\Valuation;
use app\models\Buildings;
use app\models\AssetCategory;
use app\models\ProposalStandardReport;
use app\models\ProposalBcsReport;
use app\models\ProposalPmeReport;
use app\models\ValuationConflict;
use app\models\Properties;
use app\models\Building;
use app\models\ProposalMasterFile;
use app\models\ProposalDocs;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class QuotationHelperFunctions extends Component
{

	public function getUniqueReference()
	{
		$previous_record = ValuationQuotation::find()
			->select([
				'id',
				'reference_number',
			])
			->orderBy(['id' => SORT_DESC])->asArray()->one();
		// echo "<pre>";
		// print_r($previous_record['reference_number']);
		// echo "</pre>";
		// die();
		if ($previous_record <> null) {
			$prev_ref = explode("-", $previous_record['reference_number']);
			return 'REP-' . date('Y') . '-' . ($prev_ref[2] + 1);
		} else {
			return 'REP-' . date('Y') . '-1';
		}
	}

	public function getVatTotal_add($final_fee)
	{
		if ($final_fee != null) {
			$tax = $final_fee * 5 / 105;
			return $tax;
			// $finalAfterTax = $final_fee+$tax;
			// return $finalAfterTax;
		} else {
			return false;
		}
	}


	public function getVatTotal($final_fee)
	{
		if ($final_fee != null) {
			$tax = $final_fee * 5 / 100;
			return $tax;
			// $finalAfterTax = $final_fee+$tax;
			// return $finalAfterTax;
		} else {
			return false;
		}
	}

	/**
	 * List of List Purpose of valuation
	 *
	 * @return array
	 */
	public function getPurposeOfValuationForQuotationArr()
	{
		return ArrayHelper::map(\app\models\ValuationPurposes::find()->where(['trashed' => 0])->all(), 'id', 'title');

		/*return [
            '1' => Yii::t('app', 'Secured Lending.'),
            '2' => Yii::t('app', 'Financial Reporting.'),
            '3' => Yii::t('app', 'Strategic Management.'),
            '4' => Yii::t('app', 'Internal Management.'),
            '5' => Yii::t('app', 'Purchase of Property.'),
            '6' => Yii::t('app', 'Intention to Sell.'),
            '7' => Yii::t('app', 'Intention to Buy.'),
            '8' => Yii::t('app', 'Internal Purpose.'),
            '9' => Yii::t('app', 'Court Proceedings.'),
            '10' => Yii::t('app', 'Golden Visa Application.'),
            '11' => Yii::t('app', 'Insurance Purpose.'),
            '12' => Yii::t('app', 'Gift'),
            '13' => Yii::t('app', 'Visa Application'),
    	];*/
	}



	public function getHowManyProperties()
	{
		return [
			'1' => Yii::t('app', '1'),
			'2' => Yii::t('app', '2'),
			'3' => Yii::t('app', '3'),
			'4' => Yii::t('app', '4'),
			'5' => Yii::t('app', '5'),
			'6' => Yii::t('app', '6'),
			'7' => Yii::t('app', '7'),
			'8' => Yii::t('app', '8'),
			'9' => Yii::t('app', '9'),
			'10' => Yii::t('app', '10'),
			'other' => Yii::t('app', 'Other'),
		];
	}

	public function getStatusArr()
	{
		return [
			'1' => Yii::t('app', 'Active'),
			'0' => Yii::t('app', 'InActive'),
		];
	}

	public function getQuotationStatusArr()
	{
		return [
			'0' => Yii::t('app', 'in Progress'),
			'1' => Yii::t('app', 'Sent Quotation'),
			'2' => Yii::t('app', 'Converted to Valuation'),
		];
	}

	public function getQuotationStatus()
	{
		return [
			'0' => Yii::t('app', 'Inquiry Received'),
			'1' => Yii::t('app', 'Quotation Sent'),
			'2' => Yii::t('app', 'Quotation Approved'),
			'3' => Yii::t('app', 'Toe Sent'),
			'4' => Yii::t('app', 'Toe Signed and Received'),
			'5' => Yii::t('app', 'Cancelled '),
		];
	}

	public function getValuationApproach()
	{
		return [
			'market-approach' => Yii::t('app', 'Market Approach'),
			'income-approach' => Yii::t('app', 'Income Approach'),
			'residual-value-approach' => Yii::t('app', 'Residual Value Approach'),
			'cost-approach' => Yii::t('app', 'Cost Approach'),
		];
	}

	public function getNumberofComparables()
	{
		return [
			'above-15' => Yii::t('app', 'Above 15'),
			'lessThan-3' => Yii::t('app', 'Less Than 3'),
			'3-5' => Yii::t('app', '3 - 5'),
			'6-10' => Yii::t('app', '6 - 10'),
			'11-15' => Yii::t('app', '11 - 15'),

		];
	}

	public function getNumberofUnitsInBuildingArrOld()
	{
		return [
			'1' => Yii::t('app', '1-20 units'),
			'2' => Yii::t('app', '21-50 units'),
			'3' => Yii::t('app', '50-100 units'),
			'4' => Yii::t('app', 'GreaterThan 100 units'),
		];
	}
	public function getNumberofUnitsInBuildingArr()
	{
		return [
			'0' => Yii::t('app', '0 units'),
			'1' => Yii::t('app', '1-10 units'),
			'2' => Yii::t('app', '11-25 units'),
			'3' => Yii::t('app', '26-50 units'),
			'4' => Yii::t('app', '51-100 units'),
			'5' => Yii::t('app', '100-250 units'),
			'6' => Yii::t('app', '251-500 units'),
			'7' => Yii::t('app', '501-1000 units'),
			'8' => Yii::t('app', 'GreaterThan 1000 units'),
		];
	}
	public function getTypesOfUnitsArr()
	{
		return [
			'1' => Yii::t('app', '1 Type'),
			'2' => Yii::t('app', '2 Types'),
			'3' => Yii::t('app', '3 Types'),
			'4' => Yii::t('app', '4 Types'),
			'5' => Yii::t('app', '5 Types'),
			'6' => Yii::t('app', '6 Types'),
			'7' => Yii::t('app', '7 Types'),
			'8' => Yii::t('app', '8 Types'),
			'9' => Yii::t('app', '9 Types'),
			'10' => Yii::t('app', '10 Types'),

		];
	}

	public function getNumberofUnitsInLandArr()
	{
		return [
			'1' => Yii::t('app', '1'),
			'2' => Yii::t('app', '2'),
			'3' => Yii::t('app', '3'),
			'4' => Yii::t('app', '4'),
			'5' => Yii::t('app', '5'),
		];
	}

	public function getLandSizrArr_old()
	{
		return [
			'1' => Yii::t('app', '1-7500'),
			'2' => Yii::t('app', '7501-12500'),
			'3' => Yii::t('app', '12,501-17500'),
			'4' => Yii::t('app', '17501-25000'),
			'5' => Yii::t('app', 'Above-25000'),
		];
	}

	public function getTypeofvaluation()
	{
		return [
			'physical-inspection' => Yii::t('app', 'Physical Inspection'),
			'desktop' => Yii::t('app', 'Desktop'),
			'drive-by' => Yii::t('app', 'Drive by'),

		];
	}

	public function getComplexity()
	{
		return [
			'standard' => Yii::t('app', 'Standard '),
			'non-standard' => Yii::t('app', 'Non-Standard'),
			// 'penthouse' => Yii::t('app', 'Penthouse'),
		];
	}

	public function getNewRepeatValuation()
	{
		return [
			'new-valuation' => Yii::t('app', 'New Valuation'),
			'repeat-valuation' => Yii::t('app', 'Repeat Valuation'),
		];
	}


	/**
	 * List of Valuation Approach
	 *
	 * @return array
	 */
	public function getValuationApproachArr()
	{

		return [
			'1' => Yii::t('app', 'Market Approach'),
			'2' => Yii::t('app', 'Income Approach'),
			'3' => Yii::t('app', 'Profit Approach'),
			'4' => Yii::t('app', 'Residual Approach'),
			'5' => Yii::t('app', 'DCF Approach'),
			'6' => Yii::t('app', 'Reinstatement Cost Approach'),
		];
	}



	public function getExistingProperties($model_id)
	{
		if ($model_id != null) {
			$result =  ValuationQuotationPropertiesDetails::find()
				->where(['valuation_quotation_id' => $model_id])
				// ->asArray()
				->all();
			// echo "<pre>";
			// print_r($result);
			// die();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}



	public function getMultipleProperties($id)
	{
		if ($id != null) {
			$result =  ValuationQuotationPropertiesDetails::find()
				->where(['valuation_quotation_id' => $id])
				// ->asArray()
				->all();
			// echo "<pre>";
			// print_r($result);
			// die();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function getMultiplePropertiesNew($id)
	{
		if ($id != null) {
			$result =  CrmReceivedProperties::find()
				->where(['quotation_id' => $id])
				->all();
			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function getMultiplePropertiesMuliscope($id)
	{
		if ($id != null) {
			$result =  CrmReceivedProperties::find()
				->where(['quotation_id' => $id])
				->groupBy('multiscope_id')
				->all();
			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	

	public function getPaymentTerms()
	{
		return [
			'0%' => Yii::t('app', '0%'),
			'25%' => Yii::t('app', '25%'),
			'50%' => Yii::t('app', '50%'),
			'75%' => Yii::t('app', '75%'),
			'100%' => Yii::t('app', '100%'),
		];
	}


	public function getBuiltUpAreaArr_old()
	{
		return [
			'1' => Yii::t('app', '1 - 4000'),
			'2' => Yii::t('app', '4001 - 6000'),
			'3' => Yii::t('app', '6001 - 8000'),
			'4' => Yii::t('app', '8001 - 10000'),
			'5' => Yii::t('app', 'Above 10000'),
		];
	}



	public function getMainTableData($id)
	{
		if ($id != null) {
			$result =  ValuationQuotation::find()
				->where(['id' => $id])
				->asArray()
				->one();
			// echo "<pre>";
			// print_r($result);
			// die();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function getMainTableDataNew($id)
	{
		if ($id != null) {
			$result =  CrmQuotations::find()
				->where(['id' => $id])
				->asArray()
				->one();
			// echo "<pre>";
			// print_r($result);
			// die();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function getClientName($id)
	{
		if ($id != null) {
			$result =  Company::find()

				->where(['id' => $id])
				->asArray()
				->one();
			// echo "<pre>";
			// print_r($result);
			// die();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


	public function getBuildingName($id)
	{
		if ($id != null) {
			$result =  Buildings::find()
				->select(['title'])
				->where(['id' => $id])
				->asArray()
				->one();
			// echo "<pre>";
			// print_r($result);
			// die();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function getBuildingData($id)
	{
		if ($id != null) {
			$result =  Buildings::find()
				->where(['id' => $id])
				->one();
			// echo "<pre>";
			// print_r($result);
			// die();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function StandardReport()
	{
		$result = ProposalStandardReport::find()->asArray()->one();
		return $result;
	}

	public function getDcouments($id)
	{
		if ($id != null) {
			$result =  Properties::find()
				->select(['required_documents'])
				->where(['id' => $id])
				->asArray()
				->one();
			// echo "<pre>";
			// print_r($result);
			// die();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function getClientNameForConflict($client_id)
	{
		if ($client_id != null) {
			$result =  Company::find()
				->select(['title'])
				->where(['id' => $client_id])
				->asArray()
				->one();
			// echo "<pre>";
			// print_r($result);
			// die();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function getValuationOfSelectedClient($id)
	{
		if ($id != null) {
			$result =  Valuation::find()
				->select(['id'])
				->where(['client_id' => $id]);
			// echo "<pre>";
			// print_r($result);
			// die();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


	public function getChildTableData($id)
	{
		if ($id != null) {
			$result =  ValuationQuotationPropertiesDetails::find()
				->where(['valuation_quotation_id' => $id])
				->asArray()
				->all();
			// echo "<pre>";
			// print_r($result);
			// die();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function getClientType()
	{
		return [
			'bank' => Yii::t('app', 'Bank'),
			'corporate' => Yii::t('app', 'Corporate'),
			'individual' => Yii::t('app', 'Individual'),
			'government' => Yii::t('app', 'Government'),
			/*'partner' => Yii::t('app', 'Partner'),
	    		'foreign-individual' => Yii::t('app', 'Foreign Individual'),*/
		];
	}

	public function getRelativeDiscount()
	{

		//$discount['base-fee'] = Yii::t('app', 'Base Fee');
		for ($i = 1; $i <= 100; $i++) {
			$discount[$i] =  Yii::t('app', $i . '%');
		}
		return $discount;
	}

	public function getPropertiesCategoriesListArr()
	{

		return [
			'residential-land' => Yii::t('app', 'Residential Land'),
			'residential-apartment' => Yii::t('app', 'Residential Apartment'),
			'commercial-office' => Yii::t('app', 'Commercial Office'),
			'residential-villa' => Yii::t('app', 'Residential Villa'),
			'residential-building' => Yii::t('app', 'Residential Building'),
			'commercial-building' => Yii::t('app', 'Commercial Building'),
			'mixed-use-building' => Yii::t('app', 'Mixed Use Building '),
		];
	}






	public function getTotalFee($Qrecommended_fee, $relative_discount)
	{

		$baseFee = $this->getBaseFeeFromMasterFile();

		// in quotation Rec fee
		if ($Qrecommended_fee != null) {
			if ($relative_discount == 'base-fee') {
				// 	echo "if";
				// print_r($relative_discount.','.$Qrecommended_fee);
				// die();
				$afterDiscount = $Qrecommended_fee - $baseFee;
				return $afterDiscount;
			} else {
				// echo "else";
				// print_r($relative_discount.','.$Qrecommended_fee);
				// die();
				$discount = $Qrecommended_fee / 100 * $relative_discount;
				$totalAmount = $Qrecommended_fee - $discount;
				$afterDiscount = $Qrecommended_fee - $totalAmount;
				return $afterDiscount;
			}
		} else {
			return false;
		}
	}





	function numberTowords($num)
	{

		$ones = array(
			0 => "Zero",
			1 => "One",
			2 => "Two",
			3 => "Three",
			4 => "Four",
			5 => "Five",
			6 => "Six",
			7 => "Seven",
			8 => "Eight",
			9 => "Nine",
			10 => "Ten",
			11 => "Eleven",
			12 => "Twelve",
			13 => "Thirteen",
			14 => "Fourteen",
			15 => "Fifteen",
			16 => "Sixteen",
			17 => "Seventeen",
			18 => "Eighteen",
			19 => "Nineteen",
			"014" => "Fourteen"
		);
		$tens = array(
			0 => "Zero",
			1 => "Ten",
			2 => "Twenty",
			3 => "Thirty",
			4 => "Forty",
			5 => "Fifty",
			6 => "Sixty",
			7 => "Seventy",
			8 => "Eighty",
			9 => "Ninety"
		);
		$hundreds = array(
			"Hundred",
			"Thousand",
			"Million",
			"Billion",
			"Trillion",
			"Quardrillion"
		); /*limit t quadrillion */
		$num = number_format($num, 2, ".", ",");
		$num_arr = explode(".", $num);
		$wholenum = $num_arr[0];
		$decnum = $num_arr[1];
		$whole_arr = array_reverse(explode(",", $wholenum));
		krsort($whole_arr, 1);
		$rettxt = "";
		foreach ($whole_arr as $key => $i) {

			while (substr($i, 0, 1) == "0")
				$i = substr($i, 1, 5);
			if ($i < 20) {
				/* echo "getting:".$i; */
				$rettxt .= $ones[$i];
			} elseif ($i < 100) {
				if (substr($i, 0, 1) != "0")  $rettxt .= $tens[substr($i, 0, 1)];
				if (substr($i, 1, 1) != "0") $rettxt .= " " . $ones[substr($i, 1, 1)];
			} else {
				if (substr($i, 0, 1) != "0") $rettxt .= $ones[substr($i, 0, 1)] . " " . $hundreds[0];
				if (substr($i, 1, 1) != "0") $rettxt .= " " . $tens[substr($i, 1, 1)];
				if (substr($i, 2, 1) != "0") $rettxt .= " " . $ones[substr($i, 2, 1)];
			}
			if ($key > 0) {
				$rettxt .= " " . $hundreds[$key] . " ";
			}
		}
		if ($decnum > 0) {
			$rettxt .= " and ";
			if ($decnum < 20) {
				$rettxt .= $ones[$decnum];
			} elseif ($decnum < 100) {
				$rettxt .= $tens[substr($decnum, 0, 1)];
				$rettxt .= " " . $ones[substr($decnum, 1, 1)];
			}
		}
		return ucwords($rettxt);
	}


	public function numberTowords1($amount)
	{
		$amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
		// Check if there is any number after decimal
		$amt_hundred = null;
		$count_length = strlen($num);
		$x = 0;
		$string = array();
		$change_words = array(
			0 => '', 1 => 'One', 2 => 'Two',
			3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
			7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
			10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
			13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
			16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
			19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
			40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
			70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety'
		);
		$here_digits = array('', 'Hundred', 'Thousand', 'Thousand Hundred', 'Crore');
		while ($x < $count_length) {
			$get_divider = ($x == 2) ? 10 : 100;
			$amount = floor($num % $get_divider);
			$num = floor($num / $get_divider);
			$x += $get_divider == 10 ? 1 : 2;
			if ($amount) {
				$add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
				$amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
				$string[] = ($amount < 21) ? $change_words[$amount] . ' ' . $here_digits[$counter] . $add_plural . '
	    			' . $amt_hundred : $change_words[floor($amount / 10) * 10] . ' ' . $change_words[$amount % 10] . '
	    			' . $here_digits[$counter] . $add_plural . ' ' . $amt_hundred;
			} else $string[] = null;
		}
		$implode_to_Rupees = implode('', array_reverse($string));
		$get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . "
	    		" . $change_words[$amount_after_decimal % 10]) . ' Fils' : '';
		return ($implode_to_Rupees ? $implode_to_Rupees . 'AED ' : '') . $get_paise;
	}


	public function getClientAddress($client_id)
	{
		if ($client_id != null) {
			$result =  Company::find()
				->select(['address', 'title', 'vat'])
				->where(['id' => $client_id])
				// ->asArray()
				->one();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}





	public function getDiscountRupee($totalValuationFee, $discount)
	{
		if ($totalValuationFee != null && $discount != null) {
			if ($discount == 'base-fee') {
				// echo "string base fee";
				return $this->getBaseFeeFromMasterFile();
			} else {
				// echo "string others";die();
				if ($totalValuationFee != null && $discount != null) {
					// echo $discount;die();
					$discountval = $totalValuationFee / 100 * $discount;


					return $discountval;
				}
			}
		} else {
			return false;
		}
	}

	public function getDiscountRupeeNoOfProperties($totalValuationFee, $discount)
	{
		if ($totalValuationFee != null && $discount != null) {
			if ($discount == 'base-fee') {
				// echo "string base fee";
				return $this->getBaseFeeFromMasterFile();
			} else {
				// echo "string others";die();
				if ($totalValuationFee != null && $discount != null) {
					// echo $discount;die();
					$discountval = $totalValuationFee / 100 * $discount;


					return $discountval;
				}
			}
		} else {
			return false;
		}
	}

	public function getLandSizrArr()
	{
		return [
			'1' => Yii::t('app', '1-5000'),
			'2' => Yii::t('app', '5001-7500'),
			'3' => Yii::t('app', '7501-10000'),
			'4' => Yii::t('app', '10001-15000'),
			'5' => Yii::t('app', '15001-25000'),
			'6' => Yii::t('app', 'above-25000'),
		];
	}


	public function getBuiltUpAreaArr()
	{
		return [
			'1' => Yii::t('app', '1 - 2000'),
			'2' => Yii::t('app', '2001 - 4000'),
			'3' => Yii::t('app', '4001 - 6000'),
			'4' => Yii::t('app', '6001 - 8000'),
			'5' => Yii::t('app', '8001 - 10000'),
			'6' => Yii::t('app', 'Above 10000'),
		];
	}

	public function getBuiltUpAreaArrAuto()
	{
		return [
			'1' => Yii::t('app', '1_2000'),
			'2' => Yii::t('app', '2001_4000'),
			'3' => Yii::t('app', '4001_6000'),
			'4' => Yii::t('app', '6001_8000'),
			'5' => Yii::t('app', 'above_8000'),
		];
	}
	public function getBuiltUpAreaBLArrAuto()
	{
		return [
			'1' => Yii::t('app', '1_25000'),
			'2' => Yii::t('app', '25001_75000'),
			'3' => Yii::t('app', '75001_200000'),
			'4' => Yii::t('app', '200001_400000'),
			'5' => Yii::t('app', '400001_750000'),
			'6' => Yii::t('app', 'above_750000'),
		];
	}
	public function getLandSizrArrAuto()
	{
		return [
			'1' => Yii::t('app', '1_5000'),
			'2' => Yii::t('app', '5001_7500'),
			'3' => Yii::t('app', '7501_10000'),
			'4' => Yii::t('app', '10001_15000'),
			'5' => Yii::t('app', '15001_25000'),
			'6' => Yii::t('app', 'above_25000'),
		];
	}
	public function getLandSizrArrAutoIncome()
	{
		return [
			'1' => Yii::t('app', '1_5000'),
			'2' => Yii::t('app', '5001_10000'),
			'3' => Yii::t('app', '10001_20000'),
			'4' => Yii::t('app', '20001_40000'),
			'5' => Yii::t('app', '40001_75000'),
			'6' => Yii::t('app', 'above_75000'),
		];
	}
	public function getLandSizrArrAutoProfit()
	{
		return [
			'1' => Yii::t('app', '1_10000'),
			'2' => Yii::t('app', '10001_25000'),
			'3' => Yii::t('app', '25001_50000'),
			'4' => Yii::t('app', '50001_150000'),
			'5' => Yii::t('app', '150001_300000'),
			'6' => Yii::t('app', '300001_500000'),
			'7' => Yii::t('app', '500001_750000'),
			'8' => Yii::t('app', 'above_750000'),
		];
	}
	public function getTypesOfUnitsArrAuto()
	{
		return [
			'1' => Yii::t('app', '1_type'),
			'2' => Yii::t('app', '2_types'),
			'3' => Yii::t('app', '3_types'),
			'4' => Yii::t('app', '4_types'),
			'5' => Yii::t('app', '5_types'),
			'6' => Yii::t('app', '6_types'),
			'7' => Yii::t('app', '7_types'),
			'8' => Yii::t('app', '8_types'),
			'9' => Yii::t('app', '9_types'),
			'10' => Yii::t('app', '10_types'),

		];
	}
	public function getBuiltUpAreaArrAutoIncome()
	{
		return [
			'1' => Yii::t('app', '1_10000'),
			'2' => Yii::t('app', '10001_25000'),
			'3' => Yii::t('app', '25001_50000'),
			'4' => Yii::t('app', '50001_100000'),
			'5' => Yii::t('app', '100001_250000'),
			'6' => Yii::t('app', '250001_500000'),
			'7' => Yii::t('app', '500001_1000000'),
			'8' => Yii::t('app', 'above_1000000'),
		];
	}

	public function getBuiltUpAreaArrAutoProfit()
	{
		return [
			'1' => Yii::t('app', '1_50000'),
			'2' => Yii::t('app', '50001_125000'),
			'3' => Yii::t('app', '125001_250000'),
			'4' => Yii::t('app', '250001_750000'),
			'5' => Yii::t('app', '750001_1500000'),
			'6' => Yii::t('app', '1500001_2500000'),
			'7' => Yii::t('app', '2500001_3750000'),
			'8' => Yii::t('app', 'above_3750000'),
		];
	}

	public function getNumberofUnitsInBuildingArrAutoIncome()
	{
		return [
			'1' => Yii::t('app', '1_10'),
			'2' => Yii::t('app', '11_25'),
			'3' => Yii::t('app', '26_50'),
			'4' => Yii::t('app', '51_100'),
			'5' => Yii::t('app', '101_250'),
			'6' => Yii::t('app', '251_500'),
			'7' => Yii::t('app', '501_1000'),
			'8' => Yii::t('app', 'above_1000'),
		];
	}

	public function getNumberofUnitsInBuildingArrAuto()
	{
		return [
			'1' => Yii::t('app', '1_20_units'),
			'2' => Yii::t('app', '21_50_units'),
			'3' => Yii::t('app', '50_100_units'),
			'4' => Yii::t('app', 'greater_than_100_units'),
		];
	}

	public function getPaymentTermsArrAuto()
	{
		return [
			'lessThan-3' => Yii::t('app', 'less_than_3'),
			'3-5' => Yii::t('app', '3_5'),
			'6-10' => Yii::t('app', '6_10'),
			'11-15' => Yii::t('app', '11_15'),
			'above-15' => Yii::t('app', 'above_15'),
		];
	}

	public function getNumberOfPropertiesDiscount($properties, $base_fee, $model = null)
	{
		$param = 1;
		$points = 0;
		$value = 0;
		if ($properties > 1 && $properties <= 5) {
			$param = $properties;
		} else if ($properties > 5 && $properties <= 10) {
			$param = 6;
		} else if ($properties > 10 && $properties <= 20) {
			$param = 7;
		} else if ($properties > 20 && $properties <= 50) {
			$param = 8;
		} else if ($properties > 50 && $properties <= 100) {
			$param = 9;
		} else if ($properties > 100) {
			$param = 10;
		}
		if ($model->id <= 2465) {
			$result = QuotationFeeMasterFile::find()->where(['heading' => 'no_of_property_discount', 'sub_heading' => $param, 'approach_type' => 'market'])->one();
		} else {
			$result = QuotationFeeMasterFile::find()->where(['heading' => 'no_of_property_discount', 'sub_heading' => $param, 'approach_type' => 'discount'])->one();
		}
		if ($result != null) {
			$points = yii::$app->propertyCalcHelperFunction->getPercentageAmount($result['values'], $base_fee);
		} else {
		}

		return [
			'value' => $result['values'],
			'amount' => $points,
		];
	}

	public function getFirstTimeDiscount($base_fee, $model = null)
	{
		$points = 0;
		if ($model->id <= 2465) {
			$result = QuotationFeeMasterFile::find()->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => 'new_client', 'approach_type' => 'market'])->one();
		} else {
			$result = QuotationFeeMasterFile::find()->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => 'new_client', 'approach_type' => 'discount'])->one();
		}

		if ($result != null) {
			$points = yii::$app->propertyCalcHelperFunction->getPercentageAmount($result['values'], $base_fee);
		} else {
		}

		return [
			'value' => $result['values'],
			'amount' => $points,
		];
	}


	public function getSameBuildingDiscount($properties, $base_fee, $model = null)
	{
		$param = 1;
		$points = 0;
		$value = 0;
		if ($properties > 1 && $properties <= 5) {
			$param = $properties;
		} else if ($properties > 5) {
			$param = 6;
		}
		if ($model->id <= 2465) {
			$result = QuotationFeeMasterFile::find()->where(['heading' => 'no_of_units_same_building_discount', 'sub_heading' => $param, 'approach_type' => 'market'])->one();
		} else {
			$result = QuotationFeeMasterFile::find()->where(['heading' => 'no_of_units_same_building_discount', 'sub_heading' => $param, 'approach_type' => 'discount'])->one();
		}
		if ($result != null) {
			$points = yii::$app->propertyCalcHelperFunction->getPercentageAmount($result['values'], $base_fee);
		} else {
		}

		return [
			'value' => $result['values'],
			'amount' => $points,
		];
	}

	public function getUrgencyfee($tat, $base_fee, $property_type = null, $model = null)
	{
		$param = 1;
		$points = 0;
		if ($tat == 1) {
			$param = 'urgent_same_day';
		} else if ($tat == 2) {
			$param = 'urgent_1_day';
		} else if ($tat == 3) {
			$param = 'urgent_2_days';
		} else {
			$param = 'standard';
		}

		if ($model->id <= 2465) {
			$result = QuotationFeeMasterFile::find()->where(['heading' => 'urgency_fee', 'sub_heading' => $param, 'approach_type' => 'market'])->one();
		} else {
			$result = QuotationFeeMasterFile::find()->where(['heading' => 'urgency_fee', 'sub_heading' => $param, 'property_type' => $property_type])->one();
		}

		if ($result != null) {
			$points = yii::$app->propertyCalcHelperFunction->getPercentageAmount($result['values'], $base_fee);
		} else {
		}

		return [
			'value' => $result['values'],
			'amount' => $points,
		];
	}

	public function getAdvancePaymentfee($advance_payment, $base_fee, $model = null)
	{
		$param = 1;
		$points = 0;
		if ($model->id <= 2465) {
			$result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $advance_payment, 'approach_type' => 'market'])->one();
		} else {
			$result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $advance_payment, 'approach_type' => 'discount'])->one();
		}

		if ($result != null) {
			$points = yii::$app->propertyCalcHelperFunction->getPercentageAmount($result['values'], $base_fee);
		} else {
		}

		return [
			'value' => $result['values'],
			'amount' => $points,
		];
	}

	public function getPropertyPercentageAmount($property_fee_total, $discount_amount, $property_fee)
	{
		$percentage = ($discount_amount / $property_fee_total) * 100;



		$points = yii::$app->propertyCalcHelperFunction->getPercentageAmount($percentage, $property_fee);

		return $points;
	}

	public function getNewRepeatValuationAssignment()
	{
		return [
			'new-valuation' => Yii::t('app', 'New Valuation/Assignment'),
			'repeat-valuation' => Yii::t('app', 'Repeat Valuation/Assignment'),
		];
	}


	public function getNumberofUnitsInBuildingValueArr($unit_value)
	{
		if ($unit_value == 0) {
			return 0;
		} else if ($unit_value >= 1 && $unit_value <= 10) {
			return 1;
		} else if ($unit_value >= 11 && $unit_value <= 25) {
			return 2;
		} else if ($unit_value >= 26 && $unit_value <= 50) {
			return 3;
		} else if ($unit_value >= 51 && $unit_value <= 100) {
			return 4;
		} else if ($unit_value >= 101 && $unit_value <= 250) {
			return 5;
		} else if ($unit_value >= 251 && $unit_value <= 500) {
			return 6;
		} else if ($unit_value >= 501 && $unit_value <= 1000) {
			return 7;
		} else if ($unit_value >= 1001) {
			return 8;
		}
	}

	public function getNumberofComparablesValue($comparables_value)
	{
		if ($comparables_value < 3) {
			return 'lessThan-3';
		} else if ($comparables_value >= 3 && $comparables_value <= 5) {
			return '3-5';
		} else if ($comparables_value >= 6 && $comparables_value <= 10) {
			return '6-10';
		} else if ($comparables_value >= 11 && $comparables_value <= 15) {
			return '11-15';
		} else if ($comparables_value >= 15) {
			return 'above-15';
		}
	}


	public function getRangeArrAuto()
	{
		return [
			'1' => Yii::t('app', 'range1_fee'),
			'2' => Yii::t('app', 'range2_fee'),
			'3' => Yii::t('app', 'range3_fee'),
			'4' => Yii::t('app', 'range4_fee'),
			'5' => Yii::t('app', 'range5_fee'),
			'6' => Yii::t('app', 'range6_fee'),
			'7' => Yii::t('app', 'range7_fee'),
			'8' => Yii::t('app', 'range8_fee'),
			'9' => Yii::t('app', 'range9_fee'),
			'10' => Yii::t('app', 'range10_fee'),
			'11' => Yii::t('app', 'range11_fee'),
		];
	}


	public function getPropertyTypeApproach($id, $valuation_approach)
	{

		// if(in_array($id, [20, 15, 10])){
		if (in_array($id, [20, 46])) {
			$property_type = 'residential_land_villa';
		} else if ($id == 1) {
			$property_type = 'apartment';
		} else if ($id == 12) {
			$property_type = 'hotel_apartment';
		} else if ($id == 93) {
			$property_type = 'penthouse';
		} else if (in_array($id, [6, 11])) {
			$property_type = 'townhouse';
		} else if (in_array($id, [2, 41, 91])) {
			$property_type = 'villa';
		} else if ($id == 17) {
			$property_type = 'shop';
		} else if ($id == 28) {
			$property_type = 'office';
		} else if ($id == 24) {
			$property_type = 'warehouse';
		} else if (in_array($id, [4, 5, 39, 48, 49, 50, 53, 23, 26, 29, 47])) {
			$property_type = 'building_land';
		} else if ($id == 42) {
			$property_type = 'building_residential';
		} else if ($id == 81) {
			$property_type = 'building_commercial';
		} else if ($id == 3) {
			$property_type = 'building_mixeduse';
		} else if ($id == 59) {
			$property_type = 'building_hotel_apartment';
		} else if ($id == 10) {
			$property_type = 'villa_compound';
		} else if ($id == 92) {
			$property_type = 'warehouse_compound';
		} else if ($id == 25) {
			$property_type = 'labour_accommodation';
		} else if ($id == 9) {
			$property_type = 'hotel';
		} else if ($id == 43) {
			$property_type = 'hospital';
		} else if ($id == 16 && $valuation_approach == 2) {
			$property_type = 'school_leased';
		} else if ($id == 16 && $valuation_approach == 3) {
			$property_type = 'school_operated';
		} else if ($id == 18 && $valuation_approach == 2) {
			$property_type = 'mall_leased';
		} else if ($id == 18 && $valuation_approach == 3) {
			$property_type = 'mall_operated';
		}

		return $property_type;
	}

	public function getMultipleScopeProperties($id)
	{
		if ($id != null) {
			$result =  CrmReceivedProperties::find()
				->where(['quotation_id' => $id])
				->andWhere(['not', ['multiscope_id' => null]])
				->groupBy('multiscope_id')
				->orderBy(['id' => SORT_ASC])
				->all();
			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function getBcsReport()
	{
		$result = ProposalBcsReport::find()->asArray()->one();
		return $result;
	}

	public function getPmeReport()
	{
		$result = ProposalPmeReport::find()->asArray()->one();
		return $result;
	}

	public function getAssetCategory($id)
	{
		if ($id != null) {
			$result =  AssetCategory::find()
				->select(['title'])
				->where(['id' => $id])
				->asArray()
				->one();

			if ($result != null) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function getNumberRange1($no)
	{
		if($no == 1) {$number = 1;}
		else if($no == 2) {$number = 2;}
		else if($no == 3) {$number = 3;}
		else if($no == 4) {$number = 4;}
		else if($no == 5) {$number = 5;}
		else if($no >= 6 && $no <= 10) {$number = 6;}
		else if($no >= 11 && $no <= 20) {$number = 7;}
		else if($no >=21 && $no <= 50) {$number = 8;}
		else if($no >=51 && $no <= 100) {$number = 9;}
		else if($no >=101) {$number = 10;}
		else  {$number = 0;}
		
		return $number;
	}

	public function getNumberRange2($no)
	{
		if($no >= 0 && $no <= 2) {$number = 'less_than_3';}
		else if($no >= 3 && $no <= 5) {$number = '3_5';}
		else if($no >= 6 && $no <= 10) {$number = '6_10';}
		else if($no >=11 && $no <= 15) {$number = '11_15';}
		else  {$number = 'above_15';}

		return $number;
	}

	public function getNewRepeatAssignment()
	{
		return [
			'new-valuation' => Yii::t('app', 'New Assignment'),
			'repeat-valuation' => Yii::t('app', 'Repeat Assignment'),
		];
	}
}
