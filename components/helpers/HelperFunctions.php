<?php
namespace app\components\helpers;

use app\models\ScheduleInspection;
use app\models\User;
use app\models\ValuationDetail;
use Yii;
use yii\helpers\Url;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use DateTime;
use app\models\Company;
use DateTimeZone;

class HelperFunctions extends Component
{
    /**
     * encrypts string with a key
     * @return string
     */
    public function encryptData($data,$key)
    {
        return utf8_encode(Yii::$app->getSecurity()->encryptByKey($data,$key));
    }

    /**
     * dencrypts string with a key
     * @return string
     */
    public function deryptData($data,$key)
    {
        return Yii::$app->getSecurity()->decryptByKey(utf8_decode($data),$key);
    }

    /**
     * returns number of days between two dates
     * @return integer
     */
    public function getNumberOfDaysBetween($s,$e){
        $start = strtotime($s);
        $end = strtotime($e);

        $days_between = ceil(abs($end - $start) / 86400);
        return $days_between;
    }

    /**
     * converts a given date time to ago info string respect to current date time
     * @return string
     */
    public function convertTime($time_ago2)
    {
        $time_ago = strtotime($time_ago2);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );

        if($seconds <= 60){// Seconds
            return Yii::t('app', 'just now');
        }else if($minutes <=60){//Minutes
            if($minutes==1){
                return Yii::t('app', 'one minute ago');
            }else{
                return Yii::t('app', '{mn} minutes',['mn'=>$minutes]).' '.Yii::t('app', 'ago');
            }
        }else if($hours <=24){//Hours
            if($hours==1){
                return Yii::t('app', 'an hour ago');
            }else{
                return Yii::t('app', '{hr} hrs ago',['hr'=>$hours]);
            }
        }else{
            return Yii::$app->formatter->asdatetime($time_ago2);
        }
    }

    public function getDateTimeWithDiff($date)
    {
        $serverDif=Yii::$app->appHelperFunctions->getSetting('cron_server_diff');
        return date("Y-m-d H:i:s",strtotime($serverDif." Minute",strtotime($date)));
    }

    /**
     * returns a date before given date
     * @return datetime
     */
    public function getDayBefore($date)
    {
        return date ("Y-m-d", strtotime("-1 day", strtotime($date)));
    }

    /**
     * returns a date after given date
     * @return datetime
     */
    public function getNextDay($date)
    {
        return date ("Y-m-d", strtotime("+1 day", strtotime($date)));
    }

    /**
     * returns an array of start and end date for the difference
     * @return array
     */
    public function getStartEndDateTime($date,$diff)
    {
        return ['start'=>date("Y-m-d H:i:s", strtotime($diff, strtotime($date))),'end'=>$date];
    }

    /**
     * returns an array of start and end date for the difference from now
     * @return array
     */
    public function getStartEndDateTimeFromNow($diff)
    {
        $date=date("Y-m-d H:i:s");
        return ['start'=>date("Y-m-d H:i:s", strtotime($diff, strtotime($date))),'end'=>$date];
    }

    // Parses a string into a DateTime object, optionally forced into the given timeZone.
    function parseDateTime($string, $timeZone=null) {
        $date = new DateTime(
            $string,
            $timeZone ? $timeZone : new DateTimeZone('UTC')
        // Used only when the string is ambiguous.
        // Ignored if string has a timeZone offset in it.
        );
        if ($timeZone) {
            // If our timeZone was ignored above, force it.
            $date->setTimezone($timeZone);
        }
        return $date;
    }


    // Takes the year/month/date values of the given DateTime and converts them to a new DateTime,
    // but in UTC.
    function stripTime($datetime) {
        return $datetime->format('Y-m-d');
    }

    /**
     * returns array of user types
     * @return array
     */
    public function getUserTypes()
    {
        return [
            '0' => Yii::t('app','Contact / User'),
            '10' => Yii::t('app','Admin'),
            '20' => Yii::t('app','Super Admin'),
        ];
    }

    /**
     * returns array of Yes/No
     * @return array
     */
    public function getArrYesNo()
    {
        return [
            '1' => Yii::t('app','Yes'),
            '0' => Yii::t('app','No'),
        ];
    }

    /**
     * returns array of Yes/No for badge icon html
     * @return array
     */
    public function getArrYesNoIcon()
    {
        return [
            '1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i></span>',
            '0' => '<span class="badge grid-badge badge-danger"><i class="fa fa-times"></i></span>',
        ];
    }

    /**
     * returns array of Yes/No for badge html
     * @return array
     */
    public function getArrYesNoSpan()
    {
        return [
            '1' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Yes').'</span>',
            '0' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','No').'</span>',
        ];
    }

    /**
     * returns array of Status types
     * @return array
     */
    public function getArrPublishing()
    {
        return [
            '1' => Yii::t('app','Publish'),
            '2' => Yii::t('app','Disable'),
            '0' => Yii::t('app','Draft'),
        ];
    }

    /**
     * returns array of status types html
     * @return array
     */
    public function getArrPublishingIcon()
    {
        return [
            '1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Published').'</span>',
            '2' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Disabled').'</span>',
            '0' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
        ];
    }

    /**
     * returns array of status types for form Dropdowns
     * @return array
     */
    public function getArrFormStatus()
    {
        return [
            '1' => Yii::t('app','Active'),
            '2' => Yii::t('app','Disable'),
            //'0' => Yii::t('app','Draft'),
        ];
    }

    /**
     * returns array of status types for filter Dropdowns
     * @return array
     */
    public function getArrFilterStatus()
    {
        return [
            '1' => Yii::t('app','Active'),
            '2' => Yii::t('app','Disabled'),
            //'0' => Yii::t('app','Draft'),
        ];
    }

    /**
     * returns array of status types with badge html
     * @return array
     */
    public function getArrStatusIcon()
    {
        return [
            '1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Active').'</span>',
            '2' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Disabled').'</span>',
            //'0' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
        ];
    }

    /**
     * returns array of email status types
     * @return array
     */
    public function getEmailPublishing()
    {
        return [
            '1' => Yii::t('app','Sent'),
            '0' => Yii::t('app','In Queue'),
            '2' => Yii::t('app','Draft'),
            '-1' => Yii::t('app','Canceled'),
        ];
    }

    /**
     * returns array of email status types with badge html
     * @return array
     */
    public function getEmailPublishingIcon()
    {
        return [
            '1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Sent').'</span>',
            '0' => '<span class="badge grid-badge badge-primary"><i class="fa fa-hourglass"></i> '.Yii::t('app','In Queue').'</span>',
            '2' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
            '-1' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Cancelled').'</span>',
        ];
    }

    /**
     * returns listing types for permission group
     */
    public function getPermissionListingType()
    {
        return [
            '1'=>Yii::t('app','All Listing'),
            '2'=>Yii::t('app','Own Listing'),
        ];
    }

    /**
     * returns array of week days
     * @return array
     */
    public function getWeekDayNames()
    {
        return [
            '0' => 'Sunday',
            '1' => 'Monday',
            '2' => 'Tuesday',
            '3' => 'Wednesday',
            '4' => 'Thursday',
            '5' => 'Friday',
            '6' => 'Saturday',
        ];
    }

    /**
     * returns if given date is a weekend
     * @return boolean
     */
    public function isWeekend($date)
    {
        $weekDay = date('w', strtotime($date));
        return ($weekDay == 5 || $weekDay == 6);
    }

    /**
     * returns day of week
     * @return integer
     */
    public function WeekendDay($date)
    {
        $weekDay = date('w', strtotime($date));
        return $weekDay;
    }

    /**
     * returns string with currency sign prefixed
     * @return string
     */
    public function withCurrencySign($amount)
    {
        return Yii::$app->appHelperFunctions->getSetting('currency_sign').' '.Yii::$app->formatter->asDecimal($amount);
    }

    /**
     * returns array of visibility list
     * @return array
     */
    public function getVisibilityListArr()
    {
        return [
            '1'=>Yii::t('app','Public'),
            '2'=>Yii::t('app','Private'),
        ];
    }

    /**
     * returns array of priority list
     * @return array
     */
    public function getPriorityListArr()
    {
        return [
            '1'=>Yii::t('app','Low'),
            '2'=>Yii::t('app','Medium'),
            '3'=>Yii::t('app','High'),
        ];
    }

    /**
     * returns array of priority list
     * @return array
     */
    public function getActionLogActionRemindToListArr()
    {
        return [
            'me'=>Yii::t('app','Me'),
            'assigned'=>Yii::t('app','Assigned Users'),
            'both'=>Yii::t('app','Me and Assigned Users'),
        ];
    }

    /**
     * returns array of priority list
     * @return array
     */
    public function getActionLogActionRemindTimeListArr()
    {
        return [
            '1'=>Yii::t('app','1 minute'),
            '5'=>Yii::t('app','5 minutes'),
            '10'=>Yii::t('app','10 minutes'),
            '15'=>Yii::t('app','15 minutes'),
            '30'=>Yii::t('app','30 minutes'),
            '60'=>Yii::t('app','1 hour'),
            '1440'=>Yii::t('app','1 day'),
            '10080'=>Yii::t('app','1 week'),
        ];
    }

    /**
     * returns array of Log Call Quick Note action list
     * @return array
     */
    public function getActionLogCallQuickNoteAction()
    {
        return [
            '1'=>Yii::t('app','Contacted'),
            '2'=>Yii::t('app','Not Contacted'),
        ];
    }

    /**
     * returns array of Log Call Quick Note action response list
     * @return array
     */
    public function getActionLogCallQuickNoteResponse()
    {
        return [
            '1'=>[
                '1'=>Yii::t('app','Not interested.'),
                '2'=>Yii::t('app','Requested follow up call.'),
                '3'=>Yii::t('app','Contact made.'),
            ],
            '2'=>[
                '1'=>Yii::t('app','No answer.'),
                '2'=>Yii::t('app','Wrong number.'),
                '3'=>Yii::t('app','Left voicemail.'),
            ],
        ];
    }

    /**
     * returns array of Calendar Colors
     * @return array
     */
    public function getCalendarColorListArr()
    {
        return [
            '#6389de'=>Yii::t('app','Blue'),
            '#a9c1fd'=>Yii::t('app','Light Blue'),
            '#5de1e5'=>Yii::t('app','Turquoise'),
            '#82e7c2'=>Yii::t('app','Light Green'),
            '#6bc664'=>Yii::t('app','Green'),
            '#fddb68'=>Yii::t('app','Yellow'),
            '#ffbc80'=>Yii::t('app','Orange'),
            '#ff978c'=>Yii::t('app','Pink'),
            '#e74046'=>Yii::t('app','Red'),
            '#d9adfb'=>Yii::t('app','Purple'),
            '#dedddd'=>Yii::t('app','Gray'),
        ];
    }

    /**
     * returns array of Event Sub Types
     * @return array
     */
    public function getEventSubTypesListArr()
    {
        return [
            '1'=>Yii::t('app','Meeting'),
            '2'=>Yii::t('app','Appointment'),
            '3'=>Yii::t('app','Call'),
        ];
    }

    /**
     * returns array of Event Status
     * @return array
     */
    public function getEventStatusListArr()
    {
        return [
            '1'=>Yii::t('app','Confirmed'),
            '2'=>Yii::t('app','Cancelled'),
        ];
    }

    /**
     * returns array of property type for fee structure
     * @return array
     */
    public function getFeeStructureTypeListArr()
    {
        return [
            '1'=>Yii::t('app','Free Hold'),
            '2'=>Yii::t('app','Non Free Hold'),
        ];
    }
    public function getConfigrationSectionListArr(){

        return [
            'config_bedrooms'=>'no_of_bedrooms',
            'config_bathrooms'=>'no_of_bathrooms',
            'config_kitchen'=>'no_of_kitchen',
            'config_living_area'=>'no_of_living_area',
            'config_dining_area'=>'no_of_dining_area',
            'config_maid_rooms'=>'no_of_maid_rooms',
            'config_laundry_area'=>'no_of_laundry_area',
            'config_store'=>'no_of_store',
            'config_service_block'=>'no_of_service_block',
            'config_garage'=>'no_of_garage',
            'config_balcony'=>'no_of_balcony',
            'config_family_room'=>'no_of_family_room',
            'config_powder_room'=>'no_of_powder_room',
            'config_study_room'=>'no_of_study_room',
            'config_flooring'=>'',
            'config_ceiling'=>'',
            'config_view'=>'',
            'config_general_elevation'=>'',
            'config_unit_tag'=>'',
            'config_electricity_board'=>'',
            'config_studio'=>'no_studios',
            'config_one_bedroom'=>'no_one_bedrooms',
            'config_two_bedrooms'=>'no_two_bedrooms',
            'config_three_bedrooms'=>'no_three_bedrooms',
            'config_four_bedrooms'=>'no_four_bedrooms',
            'config_penthouse'=>'no_penthouse',
            'config_commercial_unit'=>'commercial_units',
            'config_retail_unit'=>'retail_units',


        ];
    }
    public function getValuationStatusListArrLabel()
    {
        return [
            '1' => '<span class="badge grid-badge badge-info"> '.Yii::t('app','Valuation Received').'</span>',
            '2' => '<span class="badge grid-badge badge-primary"> '.Yii::t('app','Inspection Requested').'</span>',
            '3' => '<span class="badge grid-badge badge-warning"></i> '.Yii::t('app','Property Inspected').'</span>',
            '4' => '<span class="badge grid-badge badge-dark">'.Yii::t('app','Valuation Reviewed').'</span>',
            '5' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Approved').'</span>',
            '6' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Rejected').'</span>',
            '7' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Documents Requested').'</span>',
            '8' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Valuation Prepared').'</span>',
            '9' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Cancelled').'</span>',
            '10' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Rejected by Viewer').'</span>',
            '11' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Rejected by Reviewer').'</span>',
            '12' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Rejected by Approver').'</span>',
            '13' => '<span class="badge grid-badge badge-warning">'.Yii::t('app','Hold(Partial Inspection )').'</span>',


        ];
    }
    public function getValuationStatusListArrWords()
    {
        return [
            '1' => '<strong class="text-info" style="font-size: 14px"> '.Yii::t('app','Valuation Received').'</strong>',
            '2' => '<strong class="text-primary" style="font-size: 14px"> '.Yii::t('app','Inspection Requested').'</strong>',
            '3' => '<strong class="text-warning" style="font-size: 14px"></i> '.Yii::t('app','Property Inspected').'</strong>',
            '4' => '<strong class="text-muted" style="font-size: 14px">'.Yii::t('app','Valuation Reviewed').'</strong>',
            '5' => '<strong class="text-success" style="font-size: 14px">'.Yii::t('app','Approved').'</strong>',
            '6' => '<strong class="text-danger" style="font-size: 14px">'.Yii::t('app','Rejected').'</strong>',
            '7' => '<strong class="text-danger" style="font-size: 14px">'.Yii::t('app','Documents Requested').'</strong>',
            '8' => '<strong class="text-success" style="font-size: 14px">'.Yii::t('app','Valuation Prepared').'</strong>',
            '9' => '<strong class="text-danger" style="font-size: 14px" >'.Yii::t('app','Cancelled').'</strong>',
            '10' => '<strong class="text-danger" style="font-size: 14px">'.Yii::t('app','Rejected by Viewer').'</strong>',
            '11' => '<strong class="text-danger" style="font-size: 14px">'.Yii::t('app','Rejected by Reviewer').'</strong>',
            '12' => '<strong class="text-danger" style="font-size: 14px">'.Yii::t('app','Rejected by Approver').'</strong>',
            '13' => '<strong class="text-warning" style="font-size: 14px">'.Yii::t('app','Hold(Partial Inspection )').'</strong>',


        ];
    }
    public function getValuationStatusListArr()
    {
        return [
            '1'=>Yii::t('app','Valuation Received'),
            '2'=>Yii::t('app','Inspection Scheduled'),
            '3'=>Yii::t('app','Property Inspected'),
            '4'=>Yii::t('app','Valuation Reviewed'),
            '5'=>Yii::t('app','Approved'),
            '6' => Yii::t('app','Rejected'),
            '7' => Yii::t('app','Documents Requested'),
            '8' => Yii::t('app','Valuation Prepared'),
        ];
    }
    public function getValuationStatusListArr2()
    {
        return [
            '1' => Yii::t('app','Valuation Received'),
            '3' => Yii::t('app','Inspection Requested'),
            '4' => Yii::t('app','Valuation Reviewed'),
            '5' => Yii::t('app','Approved'),
            '6' => Yii::t('app','Rejected'),

        ];
    }
    public function getValuationStatusListArr3()
    {
        return [
            '1' => Yii::t('app','Valuation Received'),
            '4' => Yii::t('app','Valuation Reviewed'),
            '5' => Yii::t('app','Approved'),
            '6' => Yii::t('app','Rejected'),

        ];
    }
    public function getDateArray()
    {
        return [
            2019 => [
                "2019-01-01", "2019-12-30"
            ],
            2020 => [
                "2020-01-01", "2020-12-30"
            ],
            2021 => [
                "2021-01-01", "2021-12-30"
            ],


        ];
    }
    public function getTransactionData()
    {
        $transact_data= \app\models\AttributesData::find()->select(['value'])->
        where(['attribute_name'=>'transaction_limit'])->one();
        return	intval($transact_data['value']);
    }
    public function getAutoFollowUpPeriodData()
    {
        $transact_data= \app\models\AttributesData::find()->select(['value'])->
        where(['attribute_name'=>'auto_follow_up_inspection'])->one();
        return	intval($transact_data['value']);
    }

    public function getProposalData()
    {
        $transact_data= \app\models\AttributesData::find()->select(['value'])->
        where(['attribute_name'=>'proposal_limit'])->one();
        return	intval($transact_data['value']);
    }

    /**
     * returns general delete confirmation msg
     * @return string
     */
    public function getDelConfirmationMsg()
    {
        return Yii::t('app','Are you sure you want to delete this?');
    }

    public function getOpportunitySourceListArr()
    {
        return [
            '1'=>Yii::t('app','Web'),
            '2'=>Yii::t('app','Google'),
            '3'=>Yii::t('app','Facebook'),
            '4'=>Yii::t('app','Friend'),
        ];
    }

    public function getdonwloadpdf($id)
    {


        $model=Company::findOne($id);

        // Include the main TCPDF library (search for installation path).
        require_once( __DIR__ .'/../tcpdf/MyPDF.php');
        // create new PDF document
        $pdf = new \MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //  $pdf->model = $model;
        $pdf->report_type=2;
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle('Client Report');
        $pdf->SetSubject('Valuation Report');
        $pdf->SetKeywords('');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(8, 20, 8);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);
        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);
        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('arialn', '', 14, '', true);
        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');
        $bodydata= \app\models\SpecialAssumptionreport::findOne(1);
        $valuation_data= \app\models\Valuation::find()->where(['client_id'=>$id])->andWhere(['!=','valuation_status', 5])->andWhere(['!=','valuation_status', 6])->andWhere(['!=','valuation_status', 9])->asArray()->all();
        $html='
	    <style>
	    .bold{ font-weight:bold;  }
	    table.main-class{
	      border-top: 1px solid gray;
	      border-bottom: 1px solid gray;
	      border-left: 1px solid gray;
	      border-right: 1px solid gray;
	      font-size:10px;
	      text-align:left;
	      font-weight:bold;
	    }
	  tr:nth-child(even) {
	    background-color: #dddddd;
	  }
	  td.table_of_content{
	     color:#0D47A1;
	     font-size:11px;
	     font-weight:bold;
	     border-bottom: 1px solid #BBDEFB;
	     text-align:center;
	  }
	    </style>
	    <p>To,</p>
	    <p>'.$model->title.'</p>
	    <h3><u>Subject: Periodical Valuation Status Report as of '.date('l, jS \of F, Y').'</u></h3><br>
	    <p>'.$bodydata['status_report'].'</p><br>
	    <table class="main-class" cellspacing="2" cellpadding="5" style="border: 1px dashed #64B5F6;">
	    <tr style="background-color:#ECEFF1; text-align:center">
	    <td class="table_of_content"><br><b>NO</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Instruction Date</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Inspection Date</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Client Reference</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Property Category</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Property Type</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Building Info</b></td>
	    <td class="table_of_content" colspan="2"><br><b>Sub Community</b></td>
			<td class="table_of_content" colspan="2"><br><b>Status</b></td>
	    </tr>';

        $i=1; $j=1;
        foreach($valuation_data as $value) {


            $inspectionDate = \app\models\ScheduleInspection::find()->where(['valuation_id' => $value['id']])->one();
            $propetyType = \app\models\Properties::find()->where(['id' => $value['property_id']])->one();
            $building_info = \app\models\Buildings::find()->where(['id' => $value['building_info']])->one();
            $subCommunity = \app\models\SubCommunities::find()->where(['id' => $building_info['sub_community']])->one();
            $instruction_date = ($value['instruction_date'] <> null)?  date('d-m-Y',strtotime($value['instruction_date'])):'Not yet scheduled';
            $inspection_date = ($inspectionDate['inspection_date'] <> null)?  date('d-m-Y',strtotime($inspectionDate['inspection_date'])):'Not yet scheduled';
            $target_date = ($value['target_date'] <> null)?  date('d-m-Y',strtotime($value['target_date'])):'Not yet scheduled';
            if ($j==1) { $j++;  $val='';  }
            elseif($j==2) {  $j=1;  $val='style="background-color:#ECEFF1; "';  }
            $html .= '
	                <tr '.$val.'>
	                <td>' . $i++ . '</td>
	                <td colspan="2">' . $instruction_date. '</td>
	                <td colspan="2">' . $inspection_date . '</td>
	                <td colspan="2">' . $value['client_reference'] . '</td>
	                <td colspan="2">' . Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$value['property_category']] . '</td>
	                <td colspan="2">' . $propetyType['title'] . '</td>
	                <td colspan="2">' . $building_info['title'] . '</td>
	                <td colspan="2">' . $subCommunity['title'] . '</td>
									<td colspan="2">' . \Yii::$app->helperFunctions->valuationStatusListArr[$value['valuation_status']] . '</td>
	                </tr>';
        }
        $html.='</table>';
        if($i == 1){
            $html='';
        }

        if ( $value['client_id']!=null) {


            $pdfFile = $value['client_id'].'.pdf';
            $fullPath = realpath(dirname(__FILE__).'/../../uploads/client_pdf').'/'.$pdfFile;
            $pdf->writeHTML($html, true, false, false, false, '');
            //$pdf->Output($fullPath, 'F');
            $pdf->Output($fullPath, 'F');
            return [$fullPath];
        }
        else {
            return '';
        }



    }
    public function getdonwloadpdfnewold($id)
    {


        $model=Company::findOne($id);

        // Include the main TCPDF library (search for installation path).
        require_once( __DIR__ .'/../tcpdf/MyPDF.php');
        // create new PDF document
        $pdf = new \MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //  $pdf->model = $model;
        $pdf->report_type=2;
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle('Client Report');
        $pdf->SetSubject('Valuation Report');
        $pdf->SetKeywords('');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(8, 20, 8);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);
        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);
        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('arialn', '', 14, '', true);
        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');
        $bodydata= \app\models\SpecialAssumptionreport::findOne(1);
        $valuation_data= \app\models\Valuation::find()->where(['client_id'=>$id])->andWhere(['!=','valuation_status', 5])->andWhere(['!=','valuation_status', 6])->andWhere(['!=','valuation_status', 9])->asArray()->all();
        $html='
	    <style>
	    .bold{ font-weight:bold;  }
	    table.main-class{
	      border-top: 1px solid gray;
	      border-bottom: 1px solid gray;
	      border-left: 1px solid gray;
	      border-right: 1px solid gray;
	      font-size:10px;
	      text-align:left;
	      font-weight:bold;
	    }
	  tr:nth-child(even) {
	    background-color: #dddddd;
	  }
	  td.table_of_content{
	     color:#0D47A1;
	     font-size:11px;
	     font-weight:bold;
	     border-bottom: 1px solid #BBDEFB;
	     text-align:center;
	  }
	    </style>
	    <p>To,</p>
	    <p>'.$model->title.'</p>
	    <h3><u>Subject: Periodical Valuation Status Report as of '.date('l, jS \of F, Y').'</u></h3><br>
	    <p>'.$bodydata['status_report'].'</p><br>
	    <table class="main-class" cellspacing="2" cellpadding="5" style="border: 1px dashed #64B5F6;">
	    <tr style="background-color:#ECEFF1; text-align:center">
	    
	
	    </tr>';

        $i=1; $j=1;
        foreach($valuation_data as $value) {


          //  $inspectionDate = \app\models\ScheduleInspection::find()->where(['valuation_id' => $value['id']])->one();
            $inspectionDate = \app\models\ScheduleInspection::find()->where(['valuation_id' => $value['id']])->one();
          //  $propetyType = \app\models\Properties::find()->where(['id' => $value['property_id']])->one();
         //   $building_info = \app\models\Buildings::find()->where(['id' => $value['building_info']])->one();
         //   $subCommunity = \app\models\SubCommunities::find()->where(['id' => $building_info['sub_community']])->one();
            $instruction_date = ($value['instruction_date'] <> null)?  date('d-m-Y',strtotime($value['instruction_date'])):'Not yet scheduled';
            $inspection_date = ($inspectionDate['inspection_date'] <> null)?  date('d-m-Y',strtotime($inspectionDate['inspection_date'])):'Not yet scheduled';
            $target_date = ($value['target_date'] <> null)?  date('d-m-Y',strtotime($value['target_date'])):'Not yet scheduled';
            if ($j==1) { $j++;  $val='';  }
            elseif($j==2) {  $j=1;  $val='style="background-color:#ECEFF1; "';  }
            $html .= '
	                <tr '.$val.'>
	                
	        
									
	                </tr>';
        }
        $html.='</table>';
       /* if($i == 1){
            $html='';
        }*/

        if ( $value['client_id']!=null) {


            $pdfFile = $value['client_id'].'.pdf';
            $fullPath = realpath(dirname(__FILE__).'/../../uploads/client_pdf').'/'.$pdfFile;
            $pdf->writeHTML($html, true, false, false, false, '');
            //$pdf->Output($fullPath, 'F');
            $pdf->Output($fullPath, 'F');
            return [$fullPath];
        }
        else {
            return '';
        }



    }
    public function getdonwloadpdfnew($id)
    {


        $model=Company::findOne($id);

        // Include the main TCPDF library (search for installation path).
        require_once( __DIR__ .'/../tcpdf/MyPDF.php');
        // create new PDF document
        $pdf = new \MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //  $pdf->model = $model;
        $pdf->report_type=2;
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle('Client Report');
        $pdf->SetSubject('Valuation Report');
        $pdf->SetKeywords('');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(8, 20, 8);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);
        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);
        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('arialn', '', 14, '', true);
        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');
        $bodydata= \app\models\SpecialAssumptionreport::findOne(1);
        $valuation_data= \app\models\Valuation::find()->where(['client_id'=>$id])->andWhere(['!=','valuation_status', 5])->andWhere(['!=','valuation_status', 6])->andWhere(['!=','valuation_status', 9])->asArray()->all();
        $html='
	    <style>
	    .bold{ font-weight:bold;  }
	    table.main-class{
	      border-top: 1px solid gray;
	      border-bottom: 1px solid gray;
	      border-left: 1px solid gray;
	      border-right: 1px solid gray;
	      font-size:10px;
	      text-align:left;
	      font-weight:bold;
	    }
	  tr:nth-child(even) {
	    background-color: #dddddd;
	  }
	  td.table_of_content{
	     color:#0D47A1;
	     font-size:11px;
	     font-weight:bold;
	     border-bottom: 1px solid #BBDEFB;
	     text-align:left;
	  }
	    </style>
	    <p>To,</p>
	    <p>'.$model->title.'</p>
	    <h3><u>Subject: Periodical Valuation Status Report as of '.date('l, jS \of F, Y').'</u></h3><br>
	    <p>'.$bodydata['status_report'].'</p><br>
	    <table class="main-class" cellspacing="1" cellpadding="5" style="border: 1px dashed #64B5F6;">
	    <tr style="background-color:#ECEFF1; text-align:center">
	    <td class="table_of_content" style="width: 10%"><br><b>Client Reference</b></td>
	    <td class="table_of_content" style="width: 14%"><br><b>Windmills Reference</b></td>
	    <td class="table_of_content" style="width: 11%"><br><b>Instruction Date</b></td>
	    <td class="table_of_content" style="width: 25%"><br><b>Client Customer Name</b></td>
	    <td class="table_of_content" style="width: 10%"><br><b>Property Type</b></td>
	    <td class="table_of_content" style="width: 18%"><br><b>Community Name</b></td>
<!--	    <td class="table_of_content"  style="width: 15%"><br><b>Building/Project Name</b></td>
	    <td class="table_of_content" style="width: 10%"><br><b>Inspection Type</b></td>-->
	    <td class="table_of_content" style="width: 11%"><br><b>Status</b></td>
           <!-- <td class="table_of_content" style="width: 10%"><br><b>Reasoning</b></td>-->
	  
	    <!--
	    <td class="table_of_content" colspan="2"><br><b>Assignment Type</b></td>
	
	    <td class="table_of_content" colspan="2"><br><b>Reprt Delivery Date</b></td>-->
	    </tr>';

        $i=1; $j=1;
        foreach($valuation_data as $value) {


            //  $inspectionDate = \app\models\ScheduleInspection::find()->where(['valuation_id' => $value['id']])->one();
            $inspectionDate = \app\models\ScheduleInspection::find()->where(['valuation_id' => $value['id']])->one();
            $cancelReasonsone = \app\models\InspectionCancelReasonsLog::find()->where(['valuation_id' => $value['id']])->one();

            if(isset($cancelReasonsone) && ($cancelReasonsone <> null)){

                $inspection_reason_title = $cancelReasonsone->reason->title;
                $inspection_reason_exp = explode(" ",$inspection_reason_title);
                $inspection_reason  = $inspection_reason_exp[0];

            }else{
                $inspection_reason='';
            }

            /*
                            echo "<pre>";
                            print_r($cancelReasonsone);
                            die;*/
            $inspection_reason = '';
            if($value['valuation_status'] >=3 && $value['valuation_status']!=7 && $value['valuation_status']!=8){
                $inspection_reason = 'Property Inspected';
            }else if($value['valuation_status'] ==2){

                if(isset($cancelReasonsone) && ($cancelReasonsone <> null)){

                    $inspection_reason = $cancelReasonsone->reason->title;

                }else{


                    $inspection_reason = 'Scheduled for '.date('d-M-Y', strtotime($inspectionDate->inspection_date));;
                }
            }else{
                $inspection_reason = 'To be confirm';
            }

            $propetyType = \app\models\Properties::find()->where(['id' => $value['property_id']])->one();



            $building_info = \app\models\Buildings::find()->where(['id' => $value['building_info']])->one();
            // $subCommunity = \app\models\SubCommunities::find()->where(['id' => $building_info['sub_community']])->one();
            $community = \app\models\Communities::find()->where(['id' => $building_info['community']])->one();
            $instruction_date = ($value['instruction_date'] <> null)?  date('d-M-Y',strtotime($value['instruction_date'])):'Not yet scheduled';
            // $inspection_date = ($inspectionDate['inspection_date'] <> null)?  date('d-m-Y',strtotime($inspectionDate['inspection_date'])):'Not yet scheduled';
            $target_date = ($value['target_date'] <> null)?  date('d-m-Y',strtotime($value['target_date'])):'Not yet scheduled';
            if ($j==1) { $j++;  $val='';  }
            elseif($j==2) {  $j=1;  $val='style="background-color:#ECEFF1; "';  }

            if(isset($cancelReasonsone) && ($cancelReasonsone <> null) && ($value['valuation_status']==7)){
                $status = "Inspection Requested";
            }
            else {
                $status = \Yii::$app->helperFunctions->valuationStatusListArr[$value['valuation_status']];
            }

            $html .= '
	                <tr '.$val.'>
	                <td>' . $value['client_reference'] . '</td>
	                <td>' . $value['reference_number'] . '</td>
	                <td>' . $instruction_date. '</td>
	                <td>' . $value['client_name_passport'] . '</td>
	                 <td>' . $propetyType->title . '</td>
	                <td>' . $community->title . '</td>
	               
	                <td >' . $status . '</td>
	             
	                </tr>';
        }
        $html.='</table>';
        if($i == 1){
            //$html='';
        }

        if ( $value['client_id']!=null) {


            $pdfFile = $value['client_id'].'.pdf';
            $fullPath = realpath(dirname(__FILE__).'/../../uploads/client_pdf').'/'.$pdfFile;
            $pdf->writeHTML($html, true, false, false, false, '');
            //$pdf->Output($fullPath, 'F');
            //    $pdf->Output($fullPath, 'I');
            $pdf->Output($fullPath, 'F');
            return [$fullPath];
        }
        else {
            return '';
        }



    }
    public function getModelStepSubmit_20_09_2021($model,$valuation)
    {
        if($valuation->id == 7185) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id=' . $model->id . ' AND approver_type="approver"')->execute();
            return true;
        }

        // print_r("working");
        // die();


        if ($model->status=='Approve') {
            $Status='Recommended For Approval';
        }else {
            $Status='Reject';
            $reason=$model->reason;
            $reasonlabel='Reason for Rejection: ';
        }
        $notifyData = [
            'client' => $valuation->client,
            'uid' => $valuation->id,
            'attachments' => [],
            // 'subject' => $valuation->email_subject,
            'replacements'=>[
                '{clientName}'=>   $valuation->client->title,
                '{estimatedMarkerValue}'=> $model->estimated_market_value,
                '{estimatedMarkerValueSqf}'=>   $model->estimated_market_value_sqf,
                '{estimatedMarkertRent}'=>   $model->estimated_market_rent,
                '{estimatedMarkertRentSqf}'=>   $model->estimated_market_rent_sqf,
                '{parkingMarketValue}'=>   $model->parking_market_value,
                '{parkingMarketValueSqf}'=>   $model->parking_market_value_sqf,
                '{status}'=>   $Status,
                '{reason}'=>   $reason,
                '{reasonlabel}'=> $reasonlabel,
                '{value}'=>   (isset($model->created_by) && ($model->created_by <> null))? ($model->user->firstname): Yii::$app->user->identity->firstname,
                '{date}'=>   (date('Y-m-d',strtotime($model->created_at))),
            ],
        ];

        $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval'];
        // echo $valuation_steps[$step];
        // die();
        //   \app\modules\wisnotify\listners\NotifyEvent::fire('Valuation.'.$valuation_steps[$model->step].'.send', $notifyData);
        // $model->email_status = 1;

        if ($model->step==21) {
            if ($model->status=='Approve') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 8], 'id='.$valuation->id.'')->execute();
            }
            elseif ($model->status=='Reject') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 10], 'id='.$valuation->id.'')->execute();
            }
            // UPDATE (table name, column values, condition)
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .'
                      	AND approver_type="valuer"')->execute();
        }

        if ($model->step==22) {
            if ($model->status=='Approve') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 4], 'id='.$valuation->id.'')
                    ->execute();

            }elseif ($model->status=='Reject') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 11], 'id='.$valuation->id.'')
                    ->execute();
            }
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="reviewer"')->execute();
        }
        elseif ($model->step==23) {
            if ($model->status=='Approve') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 5], 'id='.$valuation->id.'')->execute();
            }elseif ($model->status=='Reject') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 12], 'id='.$valuation->id.'')->execute();
            }
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="approver"')->execute();
        }
    }
    public function getModelStepSubmit($model,$valuation)
    {
        if($valuation->id == 7185) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id=' . $model->id . ' AND approver_type="approver"')->execute();
            return true;
        }
        if ($model->status=='Approve') {
            $Status='Recommended For Approval';
        }else {
            $Status='Reject';
            $reason=$model->reason;
            $reasonlabel='Reason for Rejection: ';
        }
        //  print_r("working1");
        //  print_r("working1");
        // die();
        // $val =file_get_contents();
        $attachment_govt = array();
        if ($model->approver_type=='approver') {


            if($valuation->client_id == 1){
                $curl_handle_1=curl_init();
                curl_setopt($curl_handle_1, CURLOPT_URL,Url::toRoute(['valuation/send_invoice_val', 'id' => $valuation->id]));
                curl_setopt($curl_handle_1, CURLOPT_CONNECTTIMEOUT, 2);
                curl_setopt($curl_handle_1, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl_handle_1, CURLOPT_USERAGENT, 'Maxima');

                $val2 = curl_exec($curl_handle_1);
                $ip = curl_getinfo($curl_handle_1,CURLINFO_PRIMARY_IP);
                curl_close($curl_handle_1);
                $attachments[]=$val2;

            }



            if(isset($valuation->ref_portfolio)) {
                $ptsmplCount = (new yii\db\Query())
                    ->from('valuation')
                    ->where(['portfolio' => 1])
                    ->andWhere(['ref_portfolio' => $valuation->ref_portfolio])
                    ->count();
                $ptaprvCount = (new yii\db\Query())
                    ->from('valuation')
                    ->where(['portfolio' => 1])
                    ->andWhere(['ref_portfolio' => $valuation->ref_portfolio])
                    ->andWhere(['valuation_status' => 5])
                    ->count();
                $ptcheck = $ptsmplCount - $ptaprvCount;


                /*  $lastRecordId = \app\models\Valuation::find()
                    ->where(['portfolio' => 1])
                    ->andWhere(['ref_portfolio' => $valuation->ref_portfolio])
                    //->andWhere(['valuation_status' => 5])
                    ->orderBy(['id' => SORT_DESC])
                    ->limit(1)
                    ->one()->id;*/

                // if(isset($lastRecordId) && $lastRecordId ==  $valuation->id) {
               /* if ($ptcheck == 1) {
                        $curl_handle_1=curl_init();
                        curl_setopt($curl_handle_1, CURLOPT_URL,Url::toRoute(['valuation/invoice_pt_path', 'ref_portfolio' => $valuation->ref_portfolio]));
                        curl_setopt($curl_handle_1, CURLOPT_CONNECTTIMEOUT, 5);
                        curl_setopt($curl_handle_1, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($curl_handle_1, CURLOPT_USERAGENT, 'Maxima');

                        $val3 = curl_exec($curl_handle_1);
                        $ip = curl_getinfo($curl_handle_1,CURLINFO_PRIMARY_IP);
                        curl_close($curl_handle_1);
                        $attachments[]=$val3;
                    }*/
            }

           /* $lastRecordId = \app\models\Valuation::find()
                ->where(['portfolio' => 1])
                ->andWhere(['ref_portfolio' => $valuation->ref_portfolio])
                //->andWhere(['valuation_status' => 5])
                ->orderBy(['id' => SORT_DESC])
                ->limit(1)
                ->one()->id;*/







            $curl_handle=curl_init();
            curl_setopt($curl_handle, CURLOPT_URL,Url::toRoute(['valuation/sendpdfreport', 'id' => $valuation->id]));
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            $ip = curl_getinfo($curl_handle,CURLINFO_PRIMARY_IP);
            curl_close($curl_handle);
            $attachments[]=$val;
            if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
                if($valuation->ajman_approved_image <> null) {
                    $str = "Hello world. It's a beautiful day.";
                    $file_approve_ajman =explode("received-info",$valuation->ajman_approved_image);
                    if(isset($file_approve_ajman[1]) && $file_approve_ajman[1] <> null){
                        $fullPath_file_approve = 'https://maxima.windmillsgroup.com/ajmanfiles/' . $file_approve_ajman[1];
                        $attachments[] = $fullPath_file_approve;
                    }

                }

            }

            $attachment_govt[]=$val;
            /*echo "<pre>";
            print_r($attachments);
            die;*/



            if($valuation->client_id == 5){

               /* $curl_handle=curl_init();
                curl_setopt($curl_handle, CURLOPT_URL,Url::toRoute(['client/doc-ajman', 'id' => $valuation->id]));
                curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

                $val4 = curl_exec($curl_handle);
                $ip = curl_getinfo($curl_handle,CURLINFO_PRIMARY_IP);
                curl_close($curl_handle);
                $attachments[]=$val4;*/
            }

            $table= '';
            if($valuation->client_id == 5) {

               // $table.='<p>Summary</p><br>';


                $approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $valuation->id,'approver_type' => 'approver'])->one();


                $ValuerName= \app\models\User::find()->select(['firstname', 'lastname'])->where(['id'=>$approver_data->created_by])->one();
                if($ValuerName <>  null){
                    $ValuerName_text =  $ValuerName['firstname'].' '.$ValuerName['lastname'];
                }else{
                    $ValuerName_text='';
                }
                $basis_of_value = $valuation->property->basis_of_value;
                if($valuation->purpose_of_valuation == 3){
                    $basis_of_value = 'Fair Value';
                }
                if($valuation->submission_approver_date <> null) {
                    $submission_approver_date = date('l, jS \of F, Y', strtotime($valuation->submission_approver_date ));
                }else{
                    $date = date('Y-m-d');
                    $submission_approver_date = date('l, jS \of F, Y', strtotime($date));
                }

                $special_assumptions='';
//special Assumptions Occupancy, Tananted, Vacant, Acquisition
//  $special_assumptions.= '<br><b>Occupancy Status</b><br>';
                $special_assumptionreport = \app\models\SpecialAssumptionreport::find()->one();
                $model_detail = ValuationDetail::find()->where(['valuation_id' => $valuation->id])->one();
                $detail= \app\models\ValuationDetailData::find()->where(['valuation_id' => $valuation->id])->one();


                if($valuation->inspectProperty->occupancy_status == "Vacant"){
                    $special_assumptions.=$special_assumptionreport->vacant;
                }

                if($valuation->inspectProperty->occupancy_status == "Owner Occupied"){
                    $special_assumptions.=$special_assumptionreport->owner_occupied;
                }

                if($valuation->inspectProperty->occupancy_status == "Tenanted"){
                    $special_assumptions.=$special_assumptionreport->tenanted;
                }


                if($valuation->inspectProperty->acquisition_method == 1 || $valuation->inspectProperty->acquisition_method == 2 ){
                    $special_assumptions.= '<br>'.$special_assumptionreport->gifted_granted;
                }else if($valuation->inspectProperty->acquisition_method == 4){
                    $special_assumptions.= '<br>'.$special_assumptionreport->inherited;

                }else{
                    if(trim($special_assumptionreport->purchased) != 'None' && trim($special_assumptionreport->purchased) != '') {
                        $special_assumptions .= '<br>' .$special_assumptionreport->purchased.'<br>';
                    }


                }

                if (($detail->valuation_date > $valuation->scheduleInspection->inspection_date) && ($valuation->inspection_type != 3)) {
                    $valuationdate_assumption= \app\models\GeneralAsumption::find()->where(['id'=>5])->one();
                    $special_assumptions .= '<br>' .$valuationdate_assumption->general_asumption.'<br>';

                }
                if (($detail->valuation_date < $valuation->scheduleInspection->inspection_date) && ($valuation->inspection_type != 3)) {
                    $special_assumptions .= "<br>For purposes of this valuation and based on your instruction to conduct a retrospective property valuation, our valuation report is based on a special assumption that any changes or developments occurring between the valuation date and the inspection date are considered to have had no material impact on the property's value as of the specified valuation date. In obtaining our opinion of value, we have relied on market data as of the specified valuation date. Additionally, we have assumed that physical attributes of the property, observed during the inspection date accurately reflect the conditions that existed on the valuation date.<br>";

                }
                if($valuation->special_assumption !== 'None' &&  ($valuation->special_assumption <> null)) {
                    $special_assumptions.= trim($valuation->special_assumption);
                }

//address
                if($model_detail->unit_number <> null && $model_detail->unit_number !=0 && is_numeric($model_detail->unit_number)){
                    $address = 'Unit Number '.$model_detail->unit_number.', '.$valuation->building->title.', '.$valuation->building->subCommunities->title.', '.$valuation->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$valuation->building->city].', UAE.';
                }else{
                    $address = 'Plot Number '.$valuation->plot_number.', '.$valuation->building->title.', '.$valuation->building->subCommunities->title.', '.$valuation->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$valuation->building->city].', UAE.';
                }

                $city ='';
                if($valuation->building->city == 3506){
                    $city= 'AUH';
                }else if($valuation->building->city == 3507){
                    $city= 'AJM';
                }else if($valuation->building->city == 3508){
                    $city= 'FJR';
                }else if($valuation->building->city == 3509){
                    $city= 'SHJ';
                }else if($valuation->building->city == 3510){
                    $city= 'DXB';
                }else if($valuation->building->city == 3511){
                    $city= 'RAK';
                }else if($valuation->building->city == 3512){
                    $city= 'UAQ';
                }else if($valuation->building->city == 4260){
                    $city= 'Al Ain';
                }
                $bua = '';
                if($valuation->inspectProperty->built_up_area > 0){
                    $bua= 'BUA:'.number_format($valuation->inspectProperty->built_up_area * 0.092903	).' Sq mt.';
                }


                if($valuation->inspectProperty->completion_status==100){
                    $ready = 'Ready';
                }else{
                    $ready = 'Under Construction';
                }
                if($approver_data <> null) {
                    $mv = 'AED '.number_format($approver_data->estimated_market_value);
                }else{
                    $mv = '';
                }


                $table = '<p>As requested please find below information</p><br>';
                $table .= 'Disclaimer : Please note that this information does not constitute an independent valuation report. It should be reviewed alongside the complete valuation report.<br>';
                $table .= '<br>';
                $table .= '<table style="border-collapse: collapse;width:80%;border: solid 1px black">';
                $table .= '<tr style=" border-bottom: solid 1px black">';
                $table .= '<th style="text-align: left; width: 5%;">#</th>';
                $table .= '<th style="text-align: left;width: 15%;">&nbsp;</th>';
                $table .= '<th style="text-align: left;width: 20%;;">Category</th>';
                $table .= '<th style="text-align: left;width: 60%">Data Column</th>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>1</td>';
                $table .= '<td>&nbsp;</td>';
                $table .= '<td>Windmills Ref No</td>';
                $table .= '<td>' . $valuation->reference_number . '</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>2</td>';
                $table .= '<td>&nbsp;</td>';
                $table .= '<td>Valuer Name</td>';
                $table .= '<td>' . $ValuerName_text . '</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>3</td>';
                $table .= '<td>&nbsp;</td>';
                $table .= '<td>Internal/ External Status of the Valuer</td>';
                $table .= '<td>The Valuer will be acting as an external Valuer in this valuation</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>4</td>';
                $table .= '<td></td>';
                $table .= '<td>Valuation Date</td>';
                $table .= '<td>'.$submission_approver_date.'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>5</td>';
                $table .= '<td><b>Valuation Details</b></td>';
                $table .= '<td>Basis of Value</td>';
                $table .= '<td>'.$basis_of_value.'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>6</td>';
                $table .= '<td></td>';
                $table .= '<td>Special Assumption, if any</td>';
                $table .= '<td>'.$special_assumptions.'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>7</td>';
                $table .= '<td></td>';
                $table .= '<td>Valuation Approach</td>';
                $table .= '<td>'.Yii::$app->appHelperFunctions->valuationApproachListArr[$valuation->property->valuation_approach].'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>8</td>';
                $table .= '<td></td>';
                $table .= '<td>Valuation Standards</td>';
                $table .= '<td>International Valuation Standards and RICS Valuation- Global Standards effective January 2022</td>';
                $table .= '</tr>';

                $table .= '<tr style=" border-bottom: solid 1px black">';
                $table .= '<td>9</td>';
                $table .= '<td></td>';
                $table .= '<td>Service Officer</td>';
                $table .= '<td>'.Yii::$app->appHelperFunctions->staffMemberListArr[$valuation->service_officer_name].'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>10</td>';
                $table .= '<td></td>';
                $table .= '<td>Property ID</td>';
                $table .= '<td>1</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>11</td>';
                $table .= '<td></td>';
                $table .= '<td>Latitude</td>';
                $table .= '<td>'.trim($valuation->inspectProperty->latitude).'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>12</td>';
                $table .= '<td><b>Property Location</b></td>';
                $table .= '<td>Longitude</td>';
                $table .= '<td>'.trim($valuation->inspectProperty->longitude).'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>13</td>';
                $table .= '<td></td>';
                $table .= '<td>Address</td>';
                $table .= '<td>'.$address.'</td>';
                $table .= '</tr>';

                $table .= '<tr style=" border-bottom: solid 1px black">';
                $table .= '<td>14</td>';
                $table .= '<td></td>';
                $table .= '<td>Emirate</td>';
                $table .= '<td>'.$city.'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>15</td>';
                $table .= '<td></td>';
                $table .= '<td>Property Type</td>';
                $table .= '<td>'.Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$valuation->property_category].'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>16</td>';
                $table .= '<td></td>';
                $table .= '<td>Number of stories</td>';
                $table .= '<td>'.$valuation->inspectProperty->full_building_floors.'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>17</td>';
                $table .= '<td></td>';
                $table .= '<td>Property storey</td>';
                $table .= '<td>'.$valuation->floor_number.'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>18</td>';
                $table .= '<td><b>Property Details</b></td>';
                $table .= '<td>Elevation from sea level</td>';
                $table .= '<td>Not Available</td>';
                $table .= '</tr>';


                $table .= '<tr>';
                $table .= '<td>19</td>';
                $table .= '<td></td>';
                $table .= '<td>Property Area</td>';
                $table .= '<td>'.$bua.'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>20</td>';
                $table .= '<td></td>';
                $table .= '<td>Age of property</td>';
                $table .= '<td>'.$valuation->inspectProperty->estimated_age.'</td>';
                $table .= '</tr>';


                $table .= '<tr>';
                $table .= '<td>21</td>';
                $table .= '<td></td>';
                $table .= '<td>Property Status</td>';
                $table .= '<td>'.$ready.'</td>';
                $table .= '</tr>';

                $table .= '<tr>';
                $table .= '<td>22</td>';
                $table .= '<td></td>';
                $table .= '<td>Property Value</td>';
                $table .= '<td>'.$mv.'</td>';
                $table .= '</tr>';




                $table .= '</table>';
                $table .= '<br>';

            }


           /* if($valuation->id == 11213){
                echo "<pre>";
                print_r($attachments);
                die;
            }*/
        }

        else {
            $attachments=[];
        }

      /*  if($valuation->id == 11309){

        }*/
       /* echo "<pre>";
        print_r($attachments);
        die;*/
        // $attachments=[];
        // print_r( $query);
        // die();
        if ($valuation->signature_img<>null) {
            // $signature_report_link = Yii::$app->get('s3bucket')->getUrl('signature/images/'.$valuation->signature_img);
        }
        $email = '';
       /* if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
            $email = 'valuation@ajmanre.gov.ae';
        }*/
        $uid= $valuation->id;
        if(isset($valuation->quotation_id) && $valuation->quotation_id <> null){
            $uid = 'crm'.$valuation->quotation_id;
        }
        $model_detail = ValuationDetail::find()->where(['valuation_id' => $valuation->id])->one();
        $address='';
        if($model_detail->unit_number <> null && $model_detail->unit_number !=0 && is_numeric($model_detail->unit_number)){
            $address .='Unit Number '.$model_detail->unit_number.', ';
        }else if($model_detail->plot_number <> null && $model_detail->plot_number !=0){
            $address .='Plot Number '.$model_detail->plot_number.', ';
        }
        $address .= $valuation->building->title;

        /*echo "<pre>";
        print_r($attachments);
        die('here');*/
        $notifyData = [
            'client' => $valuation->client,
            'uid' => $uid,
            'attachments' => $attachments,
            'subject' => $valuation->email_subject,
            //'valuer' => $valuation->approver->email,
            'valuer' => '',
            'scb' => 1,
            'replacements'=>[
                '{clientName}'=>   $valuation->client->title,
                '{address}'=>   $address,
                '{table_data}'=>   $table,
            ],
        ];


        $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval'];

        // echo $valuation_steps[$step];
        // die();
        if ($model->approver_type=='approver') {
            $allow_properties = [1,2,4,5,6,11,12,17,20,23,25,26,28,29,37,39,41,44,46,47,48,49,50,51,52,53,54,55,56,77,90];
          //  if (in_array($valuation->property_id, $allow_properties)) {
                if ($valuation->client->id != 1 && $valuation->client->id != 87 && $valuation->client->id != 9162) {
                    \app\modules\wisnotify\listners\NotifyEvent::fire1('Valuation.approval.send', $notifyData);
                    if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
                        $subject = "Windmills Valuation Report -".$valuation->reference_number;
                        $client = $valuation->client;
                       // $this->SendEmailToGovt($attachment_govt,$subject,$client);

                    }
                }else{
                    if($valuation->client->id == 1) {

                        \app\modules\wisnotify\listners\NotifyEvent::fireToSupport('Dib.invAndVal', $notifyData);
                          if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
                              $subject = "Windmills Valuation Report -".$valuation->reference_number;
                              $client = $valuation->client;
                           //   $this->SendEmailToGovt($attachment_govt,$subject,$client);

                          }
                    }
                }
           // }
        }
        $subject = "Windmills Valuation Report -".$valuation->reference_number;
        $client = $valuation->client;
        $getInspectionOfficer = ScheduleInspection::find()->where(['valuation_id' => $valuation->id])->one();
        if(isset($getInspectionOfficer->inspection_officer) && ($getInspectionOfficer->inspection_officer > 0) ) {
            $inspectionOfficer = User::find()->where(['id' => $getInspectionOfficer->inspection_officer])->one();
            $inspectionEmail = $inspectionOfficer->email;
        }else{
            $inspectionEmail = '';
        }
        $this->SendEmailToApprovers($attachments,$subject,$client,$valuation->approver->email,$inspectionEmail,$address );

        // $model->email_status = 1;
        if ($model->step==21) {
            // UPDATE (table name, column values, condition)
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .'
                      	AND approver_type="valuer"')->execute();
        }
        if ($model->step==22) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="reviewer"')->execute();
        }
        elseif ($model->step==23) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="approver"')->execute();
        }
    }

    public function getModelStepSubmitajman($model,$valuation)
    {
        if($valuation->id == 7185) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id=' . $model->id . ' AND approver_type="approver"')->execute();
            return true;
        }
        if ($model->status=='Approve') {
            $Status='Recommended For Approval';
        }else {
            $Status='Reject';
            $reason=$model->reason;
            $reasonlabel='Reason for Rejection: ';
        }
        //  print_r("working1");
        //  print_r("working1");
        // die();
        // $val =file_get_contents();
        $attachment_govt = array();
        if ($model->approver_type=='approver') {

            $curl_handle=curl_init();
            curl_setopt($curl_handle, CURLOPT_URL,Url::toRoute(['client/export-ajman', 'id' => $valuation->id]));
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            $ip = curl_getinfo($curl_handle,CURLINFO_PRIMARY_IP);
            curl_close($curl_handle);
            $attachments[]=$val;
            $attachment_govt[]=$val;
        }
        else {
            $attachments=[];
        }

        $uid= $valuation->id;


        if ($model->approver_type=='approver') {
            $allow_properties = [1,2,4,5,6,11,12,17,20,23,25,26,28,29,37,39,41,44,46,47,48,49,50,51,52,53,54,55,56,77,90];
            if (in_array($valuation->property_id, $allow_properties)) {
                if ($valuation->client->id != 1 && $valuation->client->id != 87 && $valuation->client->id != 9162) {
                    if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
                        $subject = "Windmills Valuation Report -".$valuation->reference_number;
                        $client = $valuation->client;
                        $this->SendEmailToGovt($attachment_govt,$subject,$client);

                    }
                }else{
                    if($valuation->client->id == 1) {
                        if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
                            $subject = "Windmills Valuation Report -".$valuation->reference_number;
                            $client = $valuation->client;
                            $this->SendEmailToGovt($attachment_govt,$subject,$client);

                        }
                    }
                }
            }
        }

        // $model->email_status = 1;
        if ($model->step==23) {
            // UPDATE (table name, column values, condition)
            Yii::$app->db->createCommand()->update('valuation', ['ajman_email_status' => 1], 'id='.$valuation->id .'')->execute();
            Yii::$app->db->createCommand()->update('valuation', ['ajman_follow_up_date' => date('Y-m-d')], 'id='.$valuation->id .'')->execute();

        }

    }


    public function getModelStepSubmit_draft($model,$valuation)
    {
        if($valuation->id == 7185) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id=' . $model->id . ' AND approver_type="approver"')->execute();
            return true;
        }
        if ($model->status=='Approve') {
            $Status='Recommended For Approval';
        }else {
            $Status='Reject';
            $reason=$model->reason;
            $reasonlabel='Reason for Rejection: ';
        }

        //  print_r("working1");
        // die();
        // $val =file_get_contents();

        if ($model->approver_type=='approver') {


           /* if($valuation->quotaionData->payment_status == 1){
                $curl_handle_1=curl_init();
                curl_setopt($curl_handle_1, CURLOPT_URL,Url::toRoute(['valuation/invoice_toe_pdf_half_final', 'id' => $valuation->id]));
                curl_setopt($curl_handle_1, CURLOPT_CONNECTTIMEOUT, 2);
                curl_setopt($curl_handle_1, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl_handle_1, CURLOPT_USERAGENT, 'Maxima');

                $val2 = curl_exec($curl_handle_1);
                $ip = curl_getinfo($curl_handle_1,CURLINFO_PRIMARY_IP);
                curl_close($curl_handle_1);
                //$attachments[]=$val2;
            }*/


            $curl_handle=curl_init();
            curl_setopt($curl_handle, CURLOPT_URL,Url::toRoute(['valuation/pdf_draft', 'id' => $valuation->id]));
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            $ip = curl_getinfo($curl_handle,CURLINFO_PRIMARY_IP);
            curl_close($curl_handle);
            $attachments[]=$val;

        }
        else {
            $attachments=[];
        }
        /* echo "<pre>";
         print_r($attachments);
         die;*/
        // $attachments=[];
        // print_r( $query);
        // die();
        if ($valuation->signature_img<>null) {
            // $signature_report_link = Yii::$app->get('s3bucket')->getUrl('signature/images/'.$valuation->signature_img);
        }
        $email = '';
        /* if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
             $email = 'valuation@ajmanre.gov.ae';
         }*/
        $uid= $valuation->id;
        if(isset($valuation->quotation_id) && $valuation->quotation_id <> null){
            $uid = 'crm'.$valuation->quotation_id;
        }
        $notifyData = [
            'client' => $valuation->client,
            'uid' => $uid,
            'attachments' => $attachments,
            'subject' => $valuation->email_subject,
            //'valuer' => $valuation->approver->email,
            'valuer' => '',
            'replacements'=>[
                '{clientName}'=>   $valuation->client->title,
            ],
        ];

        $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval'];

        // echo $valuation_steps[$step];
        // die();
        if ($model->approver_type=='approver') {

            // $allow_properties = [1,2,4,5,6,11,12,17,20,23,24,25,26,28,29,37,39,41,44,46,47,48,49,50,51,52,53,54,55,56,77];
            // if (in_array($valuation->property_id, $allow_properties)) {
            if ( $valuation->client->id != 87 && $valuation->client->id != 9162) {
                \app\modules\wisnotify\listners\NotifyEvent::fire1('Valuation.approval.send', $notifyData);
                if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
                    $subject = "Windmills Valuation Report -".$valuation->reference_number;
                    $client = $valuation->client;
                    // $this->SendEmailToGovt($attachments,$subject,$client);

                }
            }else{
                if($valuation->client->id == 1) {

                    \app\modules\wisnotify\listners\NotifyEvent::fireToSupport('Dib.invAndVal', $notifyData);
                    /*  if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
                          $subject = "Windmills Valuation Report -".$valuation->reference_number;
                          $client = $valuation->client;
                          $this->SendEmailToGovt($attachments,$subject,$client);

                      }*/
                }
            }
            // }

        }
        // $model->email_status = 1;
        if ($model->step==21) {
            // UPDATE (table name, column values, condition)
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="valuer"')->execute();
        }
        if ($model->step==22) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="reviewer"')->execute();
        }
        elseif ($model->step==23) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="approver"')->execute();
        }
    }

    public function getModelStepSubmit_draft_01($model,$valuation)
    {
        if($valuation->id == 7185) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id=' . $model->id . ' AND approver_type="approver"')->execute();
            return true;
        }
        if ($model->status=='Approve') {
            $Status='Recommended For Approval';
        }else {
            $Status='Reject';
            $reason=$model->reason;
            $reasonlabel='Reason for Rejection: ';
        }
        //  print_r("working1");
        //  print_r("working1");
        // die();
        // $val =file_get_contents();

        if ($model->approver_type=='approver') {


          /*  if($valuation->quotaionData->payment_status == 1){
                $curl_handle_1=curl_init();
                curl_setopt($curl_handle_1, CURLOPT_URL,Url::toRoute(['valuation/invoice_toe_pdf_half_final', 'id' => $valuation->id]));
                curl_setopt($curl_handle_1, CURLOPT_CONNECTTIMEOUT, 2);
                curl_setopt($curl_handle_1, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl_handle_1, CURLOPT_USERAGENT, 'Maxima');

                $val2 = curl_exec($curl_handle_1);
                $ip = curl_getinfo($curl_handle_1,CURLINFO_PRIMARY_IP);
                curl_close($curl_handle_1);
                //$attachments[]=$val2;
            }*/





            $curl_handle=curl_init();
            curl_setopt($curl_handle, CURLOPT_URL,Url::toRoute(['valuation/pdf_draft', 'id' => $valuation->id]));
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            $ip = curl_getinfo($curl_handle,CURLINFO_PRIMARY_IP);
            curl_close($curl_handle);
            $attachments[]=$val;
        }
        else {
            $attachments=[];
        }
        if( $model->id == 8842) {
            echo "<pre>";
            print_r($attachments);
            die;
        }
        // $attachments=[];
        // print_r( $query);
        // die();
        if ($valuation->signature_img<>null) {
            // $signature_report_link = Yii::$app->get('s3bucket')->getUrl('signature/images/'.$valuation->signature_img);
        }
        $email = '';
        /* if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
             $email = 'valuation@ajmanre.gov.ae';
         }*/
        $uid= $valuation->id;
        if(isset($valuation->quotation_id) && $valuation->quotation_id <> null){
            $uid = 'crm'.$valuation->quotation_id;
        }

        $getInspectionOfficer = ScheduleInspection::find()->where(['valuation_id' => $id])->one();
        if(isset($getInspectionOfficer->inspection_officer) && ($getInspectionOfficer->inspection_officer > 0) ) {
            $inspectionOfficer = User::find()->where(['id' => $getInspectionOfficer->inspection_officer])->one();
            $inspectionEmail = $inspectionOfficer->email;
        }else{
            $inspectionEmail = '';
        }
        $notifyData = [
            'client' => $valuation->client,
            'uid' => $uid,
            'attachments' => $attachments,
            'subject' => $valuation->email_subject,
            'valuer' => $valuation->approver->email,
            'inspector' => $inspectionEmail,
            //'valuer' => '',
            'replacements'=>[
                '{clientName}'=>   $valuation->client->title,
            ],
        ];


        $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval'];

        // echo $valuation_steps[$step];
        // die();
        if ($model->approver_type=='approver') {
            $allow_properties = [1,2,4,5,6,11,12,17,20,23,25,26,28,29,37,39,41,44,46,47,48,49,50,51,52,53,54,55,56,77,90];
            if (in_array($valuation->property_id, $allow_properties)) {
                if ( $valuation->client->id != 87 && $valuation->client->id != 9162) {
                   \app\modules\wisnotify\listners\NotifyEvent::fire1('Valuation.approval.send', $notifyData);
                     if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
                         $subject = "Windmills Valuation Report -".$valuation->reference_number;
                         $client = $valuation->client;
                     //    $this->SendEmailToGovt($attachments,$subject,$client);

                     }
                }else{
                    if($valuation->client->id == 1) {

                       // \app\modules\wisnotify\listners\NotifyEvent::fireToSupport('Dib.invAndVal', $notifyData);
                        /*  if(isset($valuation->building->city) && ($valuation->building->city == 3507)) {
                              $subject = "Windmills Valuation Report -".$valuation->reference_number;
                              $client = $valuation->client;
                              $this->SendEmailToGovt($attachments,$subject,$client);

                          }*/
                    }
                }
            }

        }
        // $model->email_status = 1;
        if ($model->step==21) {
            // UPDATE (table name, column values, condition)
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .'
                      	AND approver_type="valuer"')->execute();
        }
        if ($model->step==22) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="reviewer"')->execute();
        }
        elseif ($model->step==23) {
            Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="approver"')->execute();
        }
    }
    public function SendEmailToGovt($attachments,$subject,$client)
    {

        $notifyData = [
            'client' => $client,
            'attachments' => $attachments,
            'g_subject'=>$subject
        ];
        \app\modules\wisnotify\listners\NotifyEvent::fireToGovt('valuation.ajman', $notifyData);
    }

    public function SendEmailToApprovers($attachments,$subject,$client,$valuer,$inspector,$address)
    {

        $notifyData = [
            'client' => $client,
            'attachments' => $attachments,
            'g_subject'=>$subject,
            'valuer' => $valuer,
            'inspector' => $inspector,
            'replacements'=>[
                '{address}'=>   $address,
            ],
        ];
        \app\modules\wisnotify\listners\NotifyEvent::fireToApprovers('Valuation.approval.send', $notifyData);
    }
    /*
    * Get Reverse color to background
    */
    public function getReverseColor($colorCode)
    {
        $d = 0;
        list($red, $green, $blue) = sscanf($colorCode, "#%02x%02x%02x");
        $l = ( 0.299 * $red + 0.587 * $green + 0.114 * $blue)/255;
        if($l > 0.5)$d=0;
        else $d=255;
        return 'rgb('.$d.','.$d.','.$d.')';
    }

    public function getArrStatusIconList()
    {
        return [
            '1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Verified').'</span>',
            '2' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Unverified').'</span>',
            //'0' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
        ];
    }

    public function getArrFilterStatusList()
    {
        return [
            '1' => Yii::t('app','Verified'),
            '2' => Yii::t('app','Unverified'),
            //'0' => Yii::t('app','Draft'),
        ];
    }

    public function resize_old($filename, $width, $height)
    {
        $name_array = explode('/',$filename);
        if(!empty($name_array)){
            $new_image ='cache/' .end($name_array);
        }else{
            $new_image ='cache/' .rand(10,100000);
        }
        $new_image ='cache/' . uniqid();
        //$filename = 'http://valplibrary.files.wordpress.com/2009/01/5b585d_merry-christmas-blue-style.jpg';
        $percent = 0.3; // percentage of resize

// Content type
        // header('Content-type: image/jpeg');

// Get new dimensions
        list($width, $height) = getimagesize($filename);
        $new_width = $width * $percent;
        $new_height = $height * $percent;

// Resample
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefromjpeg($filename);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

// Output
        $image =imagejpeg($image_p, $new_image, 50);


        return $new_image;
    }

    public function resize_31_01_2024($filename, $width, $height)
    {
        $name_array = explode('/',$filename);
        if(!empty($name_array)){
            $new_image ='cache/' .end($name_array);
        }else{
            $new_image ='cache/' .rand(10,100000);
        }
        $new_image ='cache/' . uniqid();
        //$filename = 'http://valplibrary.files.wordpress.com/2009/01/5b585d_merry-christmas-blue-style.jpg';
        $percent = 0.3; // percentage of resize

// Content type
        // header('Content-type: image/jpeg');

// Get new dimensions
        $imageInfo = exif_read_data($filename);
        list($width, $height) = getimagesize($filename);
        $new_width = $width * $percent;
        $new_height = $height * $percent;

// Resample

        $image = imagecreatefromjpeg($filename);
        $image_check=0;

        if (!empty($imageInfo['Orientation'])) {
            switch ($imageInfo['Orientation']) {
                case 3:
                    $image_check=1;
                    $image = imagerotate($image, 180, 255);
                    break;
                case 6:
                    $image_check=2;
                    $image = imagerotate($image, -90, 255);
                    break;
                case 8:
                    $image_check=3;
                    $image = imagerotate($image, 90, 255);
                    break;
            }
        }
        /*  echo $image_check;
          die;*/



        $image_p = imagecreatetruecolor($new_width, $new_height);
        if($image_check > 0) {
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, imagesx($image), imagesy($image));
        }else{
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        }        /* if (is_resource($image) && get_resource_type($image) === 'gd') {
            die('dd');
            // $image is a valid GD image resource
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, imagesx($image), imagesy($image));
        } else {
            if ($image <> null) {
                // $image is not a valid GD image resource
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            }
        }*/




// Output
        $image =imagejpeg($image_p, $new_image, 50);


        return $new_image;
    }
    public function resize($filename, $width, $height)
    {
        $parts = explode('.', $filename);
        $extension = end($parts);

        $name_array = explode('/',$filename);

        if(file_exists('cache/' .end($name_array))){
            return 'cache/' .end($name_array);
        }else{


        if(!empty($name_array)){
            $new_image ='cache/' .end($name_array);
        }else{
            $new_image ='cache/' .rand(10,100000);
        }


        $new_image ='cache/' . end($name_array);
        //$filename = 'http://valplibrary.files.wordpress.com/2009/01/5b585d_merry-christmas-blue-style.jpg';
        $percent = 0.3; // percentage of resize

        // Content type
        // header('Content-type: image/jpeg');

        // Get new dimensions
        $imageInfo = exif_read_data($filename);
        list($width, $height) = getimagesize($filename);
        $new_width = $width * $percent;
        $new_height = $height * $percent;

        // Resample

        if($extension == "png"){

            return $filename;

        }else {
            $image = imagecreatefromjpeg($filename);
            $image_check = 0;

            if (!empty($imageInfo['Orientation'])) {
                switch ($imageInfo['Orientation']) {
                    case 3:
                        $image_check = 1;
                        $image = imagerotate($image, 180, 255);
                        break;
                    case 6:
                        $image_check = 2;
                        $image = imagerotate($image, -90, 255);
                        break;
                    case 8:
                        $image_check = 3;
                        $image = imagerotate($image, 90, 255);
                        break;
                }
            }

            /*  echo $image_check;
           die;*/


            $image_p = imagecreatetruecolor($new_width, $new_height);
            if ($image_check > 0) {
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, imagesx($image), imagesy($image));
            } else {
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            }        /* if (is_resource($image) && get_resource_type($image) === 'gd') {
                die('dd');
                // $image is a valid GD image resource
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, imagesx($image), imagesy($image));
            } else {
                if ($image <> null) {
                    // $image is not a valid GD image resource
                    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                }
            }*/

            // Output
            $image = imagejpeg($image_p, $new_image, 50);

            return $new_image;
        }


        }





    }

    public function resize_upload($file, $randomName, $extension)
    {
        $target_dir = "cloud/"; // Directory where you want to store the uploaded images
        $target_file = $target_dir . $file->name;
        if (move_uploaded_file($file->tempName, $target_file)) {
            $new_image = 'cache/' . $randomName;
            //$filename = 'http://valplibrary.files.wordpress.com/2009/01/5b585d_merry-christmas-blue-style.jpg';
            $percent = 0.3; // percentage of resize

            // Content type
            // header('Content-type: image/jpeg');

            // Get new dimensions
            $imageInfo = exif_read_data($target_file);
            list($width, $height) = getimagesize($target_file);
            $new_width = $width * $percent;
            $new_height = $height * $percent;

            // Resample

            if ($extension == "png") {

               // return $target_file;

            } else {
                $image = imagecreatefromjpeg($target_file);
                $image_check = 0;

                if (!empty($imageInfo['Orientation'])) {
                    switch ($imageInfo['Orientation']) {
                        case 3:
                            $image_check = 1;
                            $image = imagerotate($image, 180, 255);
                            break;
                        case 6:
                            $image_check = 2;
                            $image = imagerotate($image, -90, 255);
                            break;
                        case 8:
                            $image_check = 3;
                            $image = imagerotate($image, 90, 255);
                            break;
                    }
                }

                /*  echo $image_check;
               die;*/


                $image_p = imagecreatetruecolor($new_width, $new_height);
                if ($image_check > 0) {
                    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, imagesx($image), imagesy($image));
                } else {
                    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                }

                // Output
                $image = imagejpeg($image_p, $new_image, 50);
                unlink($target_file);

            }
        }







    }

    public function resize_uploadpng($file, $randomName, $extension)
    {
        $target_dir = "cache/"; // Directory where you want to store the uploaded images
        $target_file = $target_dir . $randomName;
        if (move_uploaded_file($file->tempName, $target_file)) {
           return $target_file;

            }
        }

    public function resize_update($filename)
    {
        $parts = explode('.', $filename);
        $extension = end($parts);

        $name_array = explode('/',$filename);

        if(file_exists('cache/' .end($name_array).'.'.$extension)){
            return 'cache/' .end($name_array);
        }else{

            if($extension == "png"){

                return $filename;

            }


        }





    }




    public function getHrRatingData($id)
    {
        $total_rating = 0;
        $rating_array = array();
        $user_data = \app\models\career::find()->where(['id' => $id])->one();
        $careerProfileInfo = \app\models\CareerProfileInfo::find()->where(['user_id' => $user_data->id])->one();
        $languages = json_decode($careerProfileInfo->language, true);
        $tech_skills = json_decode($careerProfileInfo->tech_skills, true);
        $driving_license = json_decode($careerProfileInfo->driving_license, true);
        if($languages <> null && !empty($languages)){
            $languages_count = count($languages);
        }else{
            $languages_count =0;
        }

        if($tech_skills <> null && !empty($tech_skills)){
            $tech_skills_count = count($tech_skills);
        }else{
            $tech_skills_count =0;
        }

        if($driving_license <> null && !empty($driving_license)){
            $driving_license_count = count($driving_license);
        }else{
            $driving_license_count =0;
        }

        $driving_license = json_decode($user_data->profileInfo->driving_license, true);
        $tech_skills = json_decode($user_data->profileInfo->driving_license, true);


        $academic_degrees_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'academic_degrees','type'=>'weightage'])->one();

        $professional_degrees_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'professional_degrees','type'=>'weightage'])->one();
        $experience_relevent_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'experience_relevent','type'=>'weightage'])->one();
        $experience_total_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'experience_total','type'=>'weightage'])->one();
        $experience_organization_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'experience_organization','type'=>'weightage'])->one();
        $performance_kpi_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'performance_kpi','type'=>'weightage'])->one();
        $performance_value_addition_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'performance_value_addition','type'=>'weightage'])->one();
        $communication_skills_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'technological_skills','type'=>'weightage'])->one();
        $technological_skills_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'communication_skills','type'=>'weightage'])->one();
        $driving_license_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'driving_license','type'=>'weightage'])->one();

        $count_professional_degress = \app\models\UserDegreeInfoCareer::find()->where(['user_id' => $user_data->id,'degree_name'=> 'professional_degree'])->count();
        $count_academic_degress = \app\models\UserDegreeInfoCareer::find()->where(['<>', 'degree_name', 'professional_degree'])->andWhere(['user_id'=>$user_data->id])->count();
        $experience_total = \app\models\UserExperienceCareer::find()->where(['user_id' => $user_data->id])->count();
        $performance_kpi_total = \app\models\TrtCareer::find()->where(['user_id' => $user_data->id])->count();
        $performance_value_total = \app\models\VaCareer::find()->where(['user_id' => $user_data->id])->count();

       if(intval($count_academic_degress) > 0) {


           $rating_academic_degress = number_format((floatval($count_academic_degress)) * (floatval($academic_degrees_weghtages->value) / 100), 2);
           $rating_array['rating_academic_degress']=$rating_academic_degress;
           $total_rating = $total_rating + $rating_academic_degress;
       }else{
           $rating_academic_degress = 0;
           $rating_array['rating_academic_degress']=$rating_academic_degress;
           $total_rating = $total_rating + $rating_academic_degress;

       }

        if(intval($count_professional_degress) > 0) {
            $rating_professional_degress = number_format((floatval($count_professional_degress) * floatval($professional_degrees_weghtages->value)) / 100, 2);
            $rating_array['rating_academic_degress']=$rating_professional_degress;
            $total_rating = $total_rating + $rating_professional_degress;
        }else{
            $rating_professional_degress = 0;
            $rating_array['rating_academic_degress']=$rating_professional_degress;
            $total_rating = $total_rating + $rating_professional_degress;
        }


        if(intval($experience_total) > 0) {
            $rating_experience_total = number_format((floatval($experience_total) * floatval($experience_total_weghtages->value)) / 100, 2);
            $rating_array['rating_experience_total']=$rating_experience_total;
            $total_rating = $total_rating + $rating_experience_total;
        }else{
            $rating_experience_total = 0;
            $rating_array['rating_experience_total']=$rating_experience_total;
            $total_rating = $total_rating + $rating_experience_total;
        }

        if(intval($languages_count) > 0) {
            $rating_languages = number_format((floatval($languages_count) * floatval($communication_skills_weghtages->value)) / 100, 2);
            $rating_array['rating_languages']=$rating_languages;
            $total_rating = $total_rating + $rating_languages;
        }else{
            $rating_languages = 0;
            $rating_array['rating_languages']=$rating_languages;
            $total_rating = $total_rating + $rating_languages;
        }

        if(intval($tech_skills_count) > 0) {
            $rating_tech_skills = number_format((floatval($tech_skills_count) * floatval($technological_skills_weghtages->value)) / 100, 2);
            $rating_array['rating_tech_skills']=$rating_tech_skills;
            $total_rating = $total_rating + $rating_tech_skills;
        }else{
            $rating_tech_skills = 0;
            $rating_array['rating_tech_skills']=$rating_tech_skills;
            $total_rating = $total_rating + $rating_tech_skills;
        }

        if(intval($driving_license_count) > 0) {
            $rating_driving_license = number_format((floatval($driving_license_count) * floatval($driving_license_weghtages->value)) / 100, 2);
            $rating_array['rating_driving_license']=$rating_driving_license;
            $total_rating = $total_rating + $rating_driving_license;
        }else{
            $rating_driving_license = 0;
            $rating_array['rating_driving_license']=$rating_experience_total;
            $total_rating = $total_rating + $rating_driving_license;
        }


        return	number_format($total_rating,2);
    }

    public function getHrRatingDataDetail($id)
    {
        $total_rating = 0;
        $rating_array = array();
        $user_data = \app\models\career::find()->where(['id' => $id])->one();
        $careerProfileInfo = \app\models\CareerProfileInfo::find()->where(['user_id' => $user_data->id])->one();
        $languages = json_decode($careerProfileInfo->language, true);
        $tech_skills = json_decode($careerProfileInfo->tech_skills, true);
        $driving_license = json_decode($careerProfileInfo->driving_license, true);
        if($languages <> null && !empty($languages)){

            $languages_count = count($languages);
        }else{
            $languages_count =0;
        }

        if($tech_skills <> null && !empty($tech_skills)){
            $tech_skills_count = count($tech_skills);
        }else{
            $tech_skills_count =0;
        }

        if($driving_license <> null && !empty($driving_license)){

            if (in_array('221', $driving_license))
            {
                $driving_license_count = 1;
            }else {
                $driving_license_count = 0;
            }
        }
        $department = Yii::$app->appHelperFunctions->jobDepartment[$user_data->profileInfo->department_id];
       // echo $department;


        $driving_license = json_decode($user_data->profileInfo->driving_license, true);
        $tech_skills = json_decode($user_data->profileInfo->driving_license, true);









/*
        echo "<pre>";
        print_r($count_academic_degress);
        print_r($academic_degrees_ratings_values);
        die;*/
        $professional_degrees_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'professional_degrees','type'=>'weightage'])->one();
        $experience_relevent_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'experience_relevent','type'=>'weightage'])->one();
        $experience_total_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'experience_total','type'=>'weightage'])->one();
        $experience_organization_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'experience_organization','type'=>'weightage'])->one();
        $performance_kpi_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'performance_kpi','type'=>'weightage'])->one();
        $performance_value_addition_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'performance_value_addition','type'=>'weightage'])->one();
        $communication_skills_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'technological_skills','type'=>'weightage'])->one();
        $technological_skills_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'communication_skills','type'=>'weightage'])->one();
        $driving_license_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'driving_license','type'=>'weightage'])->one();
        $cover_letter_weghtages= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'cover_letter','type'=>'weightage'])->one();

        $count_professional_degress = \app\models\UserDegreeInfoCareer::find()->where(['user_id' => $user_data->id,'degree_name'=> 'professional_degree'])->count();
       // $count_academic_degress = \app\models\UserDegreeInfoCareer::find()->where(['<>', 'degree_name', 'professional_degree'])->andWhere(['user_id'=>$user_data->id])->count();
        $experience_total = \app\models\UserExperienceCareer::find()->where(['user_id' => $user_data->id ])->count();
        $tenYearsAgo = date('Y-m-d', strtotime('-10 years'));
        $experience_total_org = \app\models\UserExperienceCareer::find()
            ->where(['user_id' => $user_data->id])
            ->andWhere(['>=', 'start_date', $tenYearsAgo])
            ->count();
       // $experience_total_org = \app\models\UserExperienceCareer::find()->where(['user_id' => $user_data->id ])->count();
        $experience_total_relavent = \app\models\UserExperienceCareer::find()->where(['user_id' => $user_data->id ,'department' => $department])->count();
        $performance_kpi_total = \app\models\TrtCareer::find()->where(['user_id' => $user_data->id])->count();
        $performance_value_total = \app\models\VaCareer::find()->where(['user_id' => $user_data->id])->count();



        $academic_degrees_ratings_values= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'academic_degrees'])->asArray()->all();
        $count_academic_degress = \app\models\UserDegreeInfoCareer::find()->select(['degree_name'])->where(['<>', 'degree_name', 'professional_degree'])->andWhere(['user_id'=>$user_data->id])->asArray()->all();

        if($count_academic_degress <> null && !empty($count_academic_degress)){
            $individual_academic_rating =0;
            foreach ($count_academic_degress as $academic_degress){

                if($academic_degress['degree_name']=='secondary_schooling'){
                    $individual_academic_rating = $individual_academic_rating + $academic_degrees_ratings_values[0]['value'];
                }
                if($academic_degress['degree_name']=='high_school_diploma'){
                    $individual_academic_rating = $individual_academic_rating + $academic_degrees_ratings_values[1]['value'];
                }
                if(($academic_degress['degree_name']=='bachelors_degree')){
                    $individual_academic_rating = $individual_academic_rating + $academic_degrees_ratings_values[2]['value'];
                }
                if(($academic_degress['degree_name']=='masters_degree')){
                    $individual_academic_rating = $individual_academic_rating + $academic_degrees_ratings_values[3]['value'];
                }
                if(($academic_degress['degree_name']=='doctorate_degree')){
                    $individual_academic_rating = $individual_academic_rating + $academic_degrees_ratings_values[4]['value'];
                }
            }
            if($individual_academic_rating > 1){
                $individual_academic_rating = 1;
            }

            $rating_academic_degress = number_format((floatval($individual_academic_rating)) * (floatval($academic_degrees_ratings_values[5]['value'])), 2);
            $rating_array['rating_academic_degress']=$rating_academic_degress;
            $total_rating = $total_rating + $rating_academic_degress;
        }else{
            $rating_academic_degress = 0;
            $rating_array['rating_academic_degress']=$rating_academic_degress;
            $total_rating = $total_rating + $rating_academic_degress;

        }

        $professional_degress_ratings_values= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'professional_degrees'])->asArray()->all();
        //$count_professional_degress = \app\models\UserDegreeInfoCareer::find()->where(['user_id' => $user_data->id,'degree_name'=> 'professional_degree'])->asArray()->all();


        if(intval($count_professional_degress) > 0) {
            $individual_academic_rating = 0;
            if(intval($count_professional_degress) >=5){
                $individual_academic_rating = $individual_academic_rating + $professional_degress_ratings_values[4]['value'];
            }
            if(intval($count_professional_degress) >= 1 ){
                $individual_academic_rating = $individual_academic_rating + $professional_degress_ratings_values[0]['value'];
            }
            if(intval($count_professional_degress) >= 2 ){
                $individual_academic_rating = $individual_academic_rating + $professional_degress_ratings_values[1]['value'];
            }
            if(intval($count_professional_degress) >= 3 ){
                $individual_academic_rating = $individual_academic_rating + $professional_degress_ratings_values[2]['value'];
            }
            if(intval($count_professional_degress) >= 4 ){
                $individual_academic_rating = $individual_academic_rating + $professional_degress_ratings_values[3]['value'];
            }
            if($individual_academic_rating > 1){
                $individual_academic_rating = 1;
            }
            //$count_professional_degress = intval($count_professional_degress)/10;
            $rating_professional_degress = number_format((floatval($individual_academic_rating) * floatval($professional_degrees_weghtages->value)), 2);
            $rating_array['rating_professional_degress']=$rating_professional_degress;
            $total_rating = $total_rating + $rating_professional_degress;
        }else{
            $rating_professional_degress = 0;
            $rating_array['rating_professional_degress']=$rating_professional_degress;
            $total_rating = $total_rating + $rating_professional_degress;
        }

        $experience_tota_ratings_values= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'experience_total'])->asArray()->all();

        if(intval($experience_total) > 0) {
            $individual_academic_rating = 0;
            if(intval($experience_total) >=5){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_ratings_values[4]['value'];
            }
            if(intval($experience_total) >= 1 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_ratings_values[0]['value'];
            }
            if(intval($experience_total) >= 2 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_ratings_values[1]['value'];
            }
            if(intval($experience_total) >= 3 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_ratings_values[2]['value'];
            }
            if(intval($experience_total) >= 4 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_ratings_values[3]['value'];
            }
            if($individual_academic_rating > 1){
                $individual_academic_rating = 1;
            }
            //$experience_total = intval($experience_total)/20;
            $rating_experience_total = number_format((floatval($individual_academic_rating) * floatval($experience_tota_ratings_values[5]['value'])) , 2);
            $rating_array['rating_experience_total']=$rating_experience_total;
            $total_rating = $total_rating + $rating_experience_total;
        }else{
            $rating_experience_total = 0;
            $rating_array['rating_experience_total']=$rating_experience_total;
            $total_rating = $total_rating + $rating_experience_total;
        }

        $experience_tota_relavent_ratings_values= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'experience_relevent'])->asArray()->all();
        if(intval($experience_total_relavent) > 0) {

            $individual_academic_rating = 0;
            if(intval($experience_total_relavent) >=5){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_relavent_ratings_values[4]['value'];
            }
            if(intval($experience_total_relavent) >= 1 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_relavent_ratings_values[0]['value'];
            }
            if(intval($experience_total_relavent) >= 2 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_relavent_ratings_values[1]['value'];
            }
            if(intval($experience_total_relavent) >= 3 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_relavent_ratings_values[2]['value'];
            }
            if(intval($experience_total_relavent) >= 4 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_relavent_ratings_values[3]['value'];
            }
            if($individual_academic_rating > 1){
                $individual_academic_rating = 1;
            }
            //$experience_total = intval($experience_total)/20;
            $rating_experience_total_relavent = number_format((floatval($individual_academic_rating) * floatval($experience_tota_relavent_ratings_values[5]['value'])) , 2);
            $rating_array['rating_experience_total_relavent']=$rating_experience_total_relavent;
            $total_rating = $total_rating + $rating_experience_total_relavent;
        }else{
            $rating_experience_total_relavent = 0;
            $rating_array['rating_experience_total_relavent']=$rating_experience_total_relavent;
            $total_rating = $total_rating + $rating_experience_total_relavent;
        }

        $experience_tota_org_ratings_values= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'experience_organization'])->asArray()->all();
        if(intval($experience_total_org) > 0) {
            $individual_academic_rating = 0;

            if(intval($experience_total_org) >= 5 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_org_ratings_values[0]['value'];
            }else if(intval($experience_total_org) == 4 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_org_ratings_values[1]['value'];
            }else if(intval($experience_total_org) == 3 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_org_ratings_values[2]['value'];
            }else if(intval($experience_total_org) == 2 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_org_ratings_values[3]['value'];
            }else if(intval($experience_total_org) == 1 ){
                $individual_academic_rating = $individual_academic_rating + $experience_tota_org_ratings_values[4]['value'];
            }
            if($individual_academic_rating > 1){
                $individual_academic_rating = 1;
            }
            //$experience_total = intval($experience_total)/20;
            $rating_experience_total_org = number_format((floatval($individual_academic_rating) * floatval($experience_tota_org_ratings_values[5]['value'])) , 2);
            $rating_array['rating_experience_total_org']=$rating_experience_total_org;
            $total_rating = $total_rating + $rating_experience_total_org;
        }else{
            $rating_experience_total_org = 0;
            $rating_array['rating_experience_total_org']=$rating_experience_total_org;
            $total_rating = $total_rating + $rating_experience_total_org;
        }

        $performance_kpi_org_ratings_values= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'performance_kpi'])->asArray()->all();
        if(intval($performance_kpi_total) > 0) {
            $individual_academic_rating = 0;
            if(intval($performance_kpi_total) >=5){
                $individual_academic_rating = $individual_academic_rating + $performance_kpi_org_ratings_values[4]['value'];
            }
            if(intval($performance_kpi_total) >= 1 ){
                $individual_academic_rating = $individual_academic_rating + $performance_kpi_org_ratings_values[0]['value'];
            }
            if(intval($performance_kpi_total) >= 2 ){
                $individual_academic_rating = $individual_academic_rating + $performance_kpi_org_ratings_values[1]['value'];
            }
            if(intval($performance_kpi_total) >= 3 ){
                $individual_academic_rating = $individual_academic_rating + $performance_kpi_org_ratings_values[2]['value'];
            }
            if(intval($performance_kpi_total) >= 4 ){
                $individual_academic_rating = $individual_academic_rating + $performance_kpi_org_ratings_values[3]['value'];
            }
            if($individual_academic_rating > 1){
                $individual_academic_rating = 1;
            }
           // $performance_kpi_total = intval($performance_kpi_total)/10;
            $rating_performance_kpi_total = number_format((floatval($individual_academic_rating) * floatval($performance_kpi_org_ratings_values[5]['value'])) , 2);
            $rating_array['performance_kpi_total']=$rating_performance_kpi_total;
            $total_rating = $total_rating + $rating_performance_kpi_total;
        }else{
            $rating_performance_kpi_total = 0;
            $rating_array['performance_kpi_total']=$rating_performance_kpi_total;
            $total_rating = $total_rating + $rating_performance_kpi_total;
        }


        $performance_value_ratings_values= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'performance_value_addition'])->asArray()->all();

        if(intval($performance_value_total) > 0) {
            $individual_academic_rating = 0;
            if(intval($performance_value_total) >=5){
                $individual_academic_rating = $individual_academic_rating + $performance_value_ratings_values[4]['value'];
            }
            if(intval($performance_value_total) >= 1 ){
                $individual_academic_rating = $individual_academic_rating + $performance_value_ratings_values[0]['value'];
            }
            if(intval($performance_value_total) >= 2 ){
                $individual_academic_rating = $individual_academic_rating + $performance_value_ratings_values[1]['value'];
            }
            if(intval($performance_value_total) >= 3 ){
                $individual_academic_rating = $individual_academic_rating + $performance_value_ratings_values[2]['value'];
            }
            if(intval($performance_value_total) >= 4 ){
                $individual_academic_rating = $individual_academic_rating + $performance_value_ratings_values[3]['value'];
            }
            if($individual_academic_rating > 1){
                $individual_academic_rating = 1;
            }
           // $performance_value_total = intval($performance_value_total)/10;
            $rating_performance_value_total = number_format((floatval($individual_academic_rating) * floatval($performance_value_ratings_values[5]['value'])) , 2);
            $rating_array['performance_value_total']=$rating_performance_value_total;
            $total_rating = $total_rating + $rating_performance_value_total;
        }else{
            $rating_performance_value_total = 0;
            $rating_array['performance_value_total']=$rating_performance_value_total;
            $total_rating = $total_rating + $rating_performance_value_total;
        }

        $languages_value_ratings_values= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'communication_skills'])->asArray()->all();
        if(intval($languages_count) > 0) {
            $individual_academic_rating = 0;
            if(intval($languages_count) >=5){
                $individual_academic_rating = $individual_academic_rating + $languages_value_ratings_values[4]['value'];
            }
            if(intval($languages_count) >= 1 ){
                $individual_academic_rating = $individual_academic_rating + $languages_value_ratings_values[0]['value'];;
            }
            if(intval($languages_count) >= 2 ){
                $individual_academic_rating = $individual_academic_rating + $languages_value_ratings_values[1]['value'];
            }
            if(intval($languages_count) >= 3 ){
                $individual_academic_rating = $individual_academic_rating + $languages_value_ratings_values[2]['value'];
            }
            if(intval($languages_count) >= 4 ){
                $individual_academic_rating = $individual_academic_rating + $languages_value_ratings_values[3]['value'];
            }
            if($individual_academic_rating > 1){
                $individual_academic_rating = 1;
            }
          //  $languages_count = intval($languages_count)/10;
            $rating_languages = number_format((floatval($individual_academic_rating) * floatval($languages_value_ratings_values[5]['value'])) , 2);
            $rating_array['rating_languages']=$rating_languages;
            $total_rating = $total_rating + $rating_languages;
        }else{
            $rating_languages = 0;
            $rating_array['rating_languages']=$rating_languages;
            $total_rating = $total_rating + $rating_languages;
        }

        $tech_skillsvalue_ratings_values= \app\models\RatingHrData::find()->select(['value'])->where(['attribute_name'=>'technological_skills'])->asArray()->all();

        if(intval($tech_skills_count) > 0) {
            $individual_academic_rating = 0;
            if(intval($tech_skills_count) >=5){
                $individual_academic_rating = $individual_academic_rating + $tech_skillsvalue_ratings_values[4]['value'];
            }
            if(intval($tech_skills_count) >= 1 ){
                $individual_academic_rating = $individual_academic_rating + $tech_skillsvalue_ratings_values[0]['value'];
            }
            if(intval($tech_skills_count) >= 2 ){
                $individual_academic_rating = $individual_academic_rating + $tech_skillsvalue_ratings_values[1]['value'];
            }
            if(intval($tech_skills_count) >= 3 ){
                $individual_academic_rating = $individual_academic_rating + $tech_skillsvalue_ratings_values[2]['value'];
            }
            if(intval($tech_skills_count) >= 4 ){
                $individual_academic_rating = $individual_academic_rating + $tech_skillsvalue_ratings_values[3]['value'];
            }
            if($individual_academic_rating > 1){
                $individual_academic_rating = 1;
            }
           // $tech_skills_count = intval($tech_skills_count)/10;
            $rating_tech_skills = number_format((floatval($individual_academic_rating) * floatval($tech_skillsvalue_ratings_values[5]['value'])) , 2);
            $rating_array['rating_tech_skills']=$rating_tech_skills;
            $total_rating = $total_rating + $rating_tech_skills;
        }else{
            $rating_tech_skills = 0;
            $rating_array['rating_tech_skills']=$rating_tech_skills;
            $total_rating = $total_rating + $rating_tech_skills;
        }

        if(intval($driving_license_count) > 0) {

            $driving_license_count = 1;
            $rating_driving_license = number_format((floatval($driving_license_count) * floatval($driving_license_weghtages->value)) , 2);
            $rating_array['rating_driving_license']=$rating_driving_license;
            $total_rating = $total_rating + $rating_driving_license;
        }else{
            $rating_driving_license = 0;
            $rating_array['rating_driving_license']=$rating_experience_total;
            $total_rating = $total_rating + $rating_driving_license;
        }
        if($user_data->cover_letter <> null) {
            $cover_letter_count = 1;
            $rating_cover_letter = number_format((floatval($cover_letter_count) * floatval($cover_letter_weghtages->value)) , 2);
            $rating_array['rating_cover_letter']=$rating_cover_letter;
            $total_rating = $total_rating + $rating_cover_letter;
        }else{
            $cover_letter_count = 0;
            $rating_array['rating_cover_letter']=$cover_letter_count;
            $total_rating = $total_rating + $cover_letter_count;
        }
        $rating_array['total_rating'] = $total_rating;


       /* echo "<pre>";
        print_r($rating_array);
        die;*/

        return	$rating_array;
    }




}
?>
