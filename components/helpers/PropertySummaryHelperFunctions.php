<?php
namespace app\components\helpers;

use app\models\QuotationFeeMasterFile;
use Yii;
use yii\base\Component;
use app\models\Properties;
use app\models\ProposalMasterFile;

class PropertySummaryHelperFunctions extends Component
{

    public function getPercentageAmount($percentage, $baseAmount)
    {
        $results = ($percentage / 100) * $baseAmount;
        return number_format($results, 2, '.', '');
    }

    public function getAmount_autoIncome($post, $model)
    {
        // dd($post);
        $old_quotation_26_02_2024 = 2465;

        $summary = [];
        $points = 0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'income';
        $post['approach_type'] = 'income';

        $property_type = $post['property_type'];

        $ponits = 0;
        $base_fee = 0;

        //$summary['building_title']['title'] = $post['building_title'];

        if ($post['property'] != null && $post['city'] != null && $post['tenure'] != null && $post['approach_type'] != null) {

            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();

            // property fee --------------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $propertyResult = QuotationFeeMasterFile::find()->where(['heading' => 'subject_property', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['property']])->one();
            }
            else{
                $propertyResult = QuotationFeeMasterFile::find()->where(['heading' => 'property_fee', 'approach_type' => $post['approach_type'],'sub_heading'=>$property_type, 'property_type'=>$property_type])->one();
            }

            if ($propertyResult != null) {
                $points += $propertyResult['values'];
                $base_fee = $propertyResult['values'];
            } else {
                $points += 0;
                $base_fee = 0;
            }
            
            

            //client Type (%) -------------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            }
            else{
                $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            }



            // subject property value -------------------------------------------------------------------

            if ($propertyResult != null) {
                $summary['property']['fee'] = $propertyResult['values'];
                $summary['property']['title'] = $property_detail->title;
            } else {
                $summary['property']['fee'] = 0;
                $summary['property']['title'] = $property_detail->title;
            }

            //height -----------------------------------------------------------------------------
            if ($post['typical_floors'] > 0) {
                $typicalFloor = ($post['typical_floors'] > 1) ? " + " . ($post['typical_floors']-1) . "F " : "";
                $basementFloor = ($post['basement_floors'] > 0) ? " + " . $post['basement_floors'] . "B " : "";
                $mezzanineFloor = ($post['mezzanine_floors'] > 0) ? " + " . $post['mezzanine_floors'] . "M " : "";
                $parkingFloor = ($post['parking_floors'] > 0) ? " + " . $post['parking_floors'] . "P " : "";
                $height = "G" . $typicalFloor . $basementFloor . $mezzanineFloor . $parkingFloor;
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = $height;
            } else {
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = 'NA';
            }


            //number of units 3,7,22 buildings wali han. -----------------------------------------------------------------
            if ($model->id < 2237) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $num_res_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_residential_units']];
                $num_com_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_commercial_units']];
                $num_ret_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_retail_units']];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = 'NA';
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = 'NA';
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_residential_units'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of commercial units no_of_commercial_units

                if ($post['no_of_commercial_units'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of retail units no_of_commercial_units

                if ($post['no_of_retail_units'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            } else {
                
                $num_units = $post['no_of_units_value'];
                $num_res_units = $post['no_of_res_units_value'];
                $num_com_units = $post['no_of_com_units_value'];
                $num_ret_units = $post['no_of_ret_units_value'];
                $num_warehouse_units = $post['no_of_warehouse_units_value'];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1 && $post['no_of_units_value'] <> null) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    if($model->id <  $old_quotation_26_02_2024){
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    }
                    else{
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    }
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                }

                

                //number of residential units no_of_residential_units
                if ($post['no_of_res_units_value'] > 0) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'residential', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_residential_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_residential_units']['title'] = $num_res_units;
                    } else {
                        $points += 0;
                        $summary['no_of_residential_units']['fee'] = 0;
                        $summary['no_of_residential_units']['title'] = $num_res_units;
                    }
                    // $points += 0;
                    // $summary['no_of_residential_units']['fee'] = 0;
                    // $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }


                //number of commercial units no_of_commercial_units

                if ($post['no_of_com_units_value'] > 0) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'commercial', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_commercial_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_commercial_units']['title'] = $num_res_units;
                    } else {
                        $points += 0;
                        $summary['no_of_commercial_units']['fee'] = 0;
                        $summary['no_of_commercial_units']['title'] = $num_res_units;
                    }
                    // $points += 0;
                    // $summary['no_of_commercial_units']['fee'] = 0;
                    // $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of retail units no_of_retail_units

                if ($post['no_of_ret_units_value'] > 0) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'retail', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_retail_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_retail_units']['title'] = $num_res_units;
                    } else {
                        $points += 0;
                        $summary['no_of_retail_units']['fee'] = 0;
                        $summary['no_of_retail_units']['title'] = $num_res_units;
                    }
                    // $points += 0;
                    // $summary['no_of_retail_units']['fee'] = 0;
                    // $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }

                //number of warehouse units no_of_warehouse_units

                if ($post['no_of_warehouse_units_value'] > 0) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'industrial', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_warehouse_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_warehouse_units']['title'] = $num_warehouse_units;
                    } else {
                        $points += 0;
                        $summary['no_of_warehouse_units']['fee'] = 0;
                        $summary['no_of_warehouse_units']['title'] = $num_warehouse_units;
                    }
                    // $points += 0;
                    // $summary['no_of_warehouse_units']['fee'] = 0;
                    // $summary['no_of_warehouse_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_warehouse_units']['fee'] = 0;
                    $summary['no_of_warehouse_units']['title'] = 'NA';
                }
            }

            // dd($base_fee,$result['values'],$this->getPercentageAmount($result['values'], $base_fee),$points);


            //number of type of units  -------------------------------------------------------------------
            // $num_units_type = yii::$app->quotationHelperFunctions->getTypesOfUnitsArrAuto()[$post['number_of_types']];
            // echo "hello"; die();
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            }
            else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['title'] = $post['number_of_types'];
            } else {
                $points += 0;
                $summary['number_of_types']['fee'] = 0;
                $summary['number_of_types']['title'] = 'NA';
            }
            // -----------------------------------------------------------------------------------


            //sub_community
            // $subCommunity = \app\models\SubCommunities::find()->where(['id'=>$post['sub_community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'sub_community', 'sub_heading'=>$post['sub_community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }else{
            //     $points += 0;
            //     $summary['sub_community']['fee'] = 0;
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }


            //city value (%) -------------------------------------------------------------------
            
            if ($post['city'] == 3506 || $post['city'] == 4260) {
                $citykey = 'abu_dhabi';
            } else if ($post['city'] == 3510) {
                $citykey = 'dubai';
            } else if ($post['city'] == 3507) {
                $citykey = 'ajman';
            } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                $citykey = 'sharjah';
            } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                $citykey = 'rak-fuj';
            } else {
                $citykey = 'others';
            }

            if ($post['city'] == 3506) {
                $summaryCity = 'Abu Dhabi';
            }
            if ($post['city'] == 4260) {
                $summaryCity = 'Al Ain';
            }
            if ($post['city'] == 3510) {
                $summaryCity = 'Dubai';
            }
            if ($post['city'] == 3507) {
                $summaryCity = 'Ajman';
            }
            if ($post['city'] == 3509) {
                $summaryCity = 'Sharjah';
            }
            if ($post['city'] == 3512) {
                $summaryCity = 'Umm Al Quwain';
            }
            if ($post['city'] == 3511) {
                $summaryCity = 'Ras Al Khaimah';
            }
            if ($post['city'] == 3508) {
                $summaryCity = 'Fujairah';
            }
            if ($post['type_of_valuation'] == 1 || $post['type_of_valuation'] == 2) {
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['title'] = $summaryCity;
                        }
                    }
                }
                else{
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['title'] = $summaryCity;
                        }
                    }
                }
                
            } else {
                $points += 0;
                $summary['city']['fee'] = 0;
                $summary['city']['title'] = $summaryCity;
            }




            //land size (%) ------------------------------------------------------------------------------
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {
                if($model->id <  $old_quotation_26_02_2024){
                    if ($post['land_size'] >= 1 && $post['land_size'] <= 5000) {
                        $post['land_size'] = 1;
                    } else if ($post['land_size'] >= 5001 && $post['land_size'] <= 10000) {
                        $post['land_size'] = 2;
                    } else if ($post['land_size'] >= 10001 && $post['land_size'] <= 20000) {
                        $post['land_size'] = 3;
                    } else if ($post['land_size'] > 20001 && $post['land_size'] <= 40000) {
                        $post['land_size'] = 4;
                    } else if ($post['land_size'] > 40001 && $post['land_size'] <= 75000) {
                        $post['land_size'] = 5;
                    } else if ($post['land_size'] >= 75001) {
                        $post['land_size'] = 6;
                    }
                    if ($post['land_size'] < 1) {
                        $points += 0;
                    } else {
                        $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type']])->one();
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['land_size']['title'] = $summaryLandSize;
                        }
                    }
                }
                else{
                    $land_range = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'land', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();

                    if($post['land_size'] >=$land_range[0]['values'] && $post['land_size'] <= $land_range[1]['values']){
                        $post['land_size']=1;
                    }
                    else if($post['land_size'] >= $land_range[3]['values'] && $post['land_size'] <= $land_range[4]['values']){
                        $post['land_size']=2;
                    }
                    else if($post['land_size'] >= $land_range[6]['values'] && $post['land_size'] <= $land_range[7]['values']){
                        $post['land_size']=3;
                    }
                    else if($post['land_size'] > $land_range[9]['values'] && $post['land_size'] <= $land_range[10]['values']){
                        $post['land_size']=4;
                    }
                    else if($post['land_size'] > $land_range[12]['values'] && $post['land_size'] <= $land_range[13]['values']){
                        $post['land_size']=5;
                    }
                    else if($post['land_size'] >= $land_range[15]['values']) {
                        $post['land_size']=6;
                    }
                    if ($post['land_size'] < 1) {
                        $points += 0;
                    } else {
                        $landSize = yii::$app->quotationHelperFunctions->getRangeArrAuto()[$post['land_size']];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['land_size']['title'] = $summaryLandSize;
                        }
                    }
                }
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = ($summaryLandSize <> null) ? $summaryLandSize : 'NA';
            }




            // bua  -------------------------------------------------------------------------------------
            // $summaryBUA = $post['built_up_area'];
            $summaryBUA = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area']; 
            $post['built_up_area'] = $summaryBUA;
            
            // $post['built_up_area'] = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            if ($no_of_units == 0) {
                //built up area (%)
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                } else {
                    if($model->id <  $old_quotation_26_02_2024){
                        if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 10000) {
                            $post['built_up_area'] = 1;
                        } else if ($post['built_up_area'] >= 10001 && $post['built_up_area'] <= 25000) {
                            $post['built_up_area'] = 2;
                        } else if ($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000) {
                            $post['built_up_area'] = 3;
                        } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000) {
                            $post['built_up_area'] = 4;
                        } else if ($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000) {
                            $post['built_up_area'] = 5;
                        } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000) {
                            $post['built_up_area'] = 6;
                        } else if ($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000) {
                            $post['built_up_area'] = 7;
                        } else if ($post['built_up_area'] >= 1000001) {
                            $post['built_up_area'] = 8;
                        }
                    }
                    else{

                        $bua_range = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();


                        if($post['built_up_area'] >=$bua_range[0]['values'] && $post['built_up_area'] <= $bua_range[1]['values']){
                            $post['built_up_area']=1;
                        }
                        else if($post['built_up_area'] >= $bua_range[3]['values'] && $post['built_up_area'] <= $bua_range[4]['values']){
                            $post['built_up_area']=2;
                        }
                        else if($post['built_up_area'] >= $bua_range[6]['values'] && $post['built_up_area'] <= $bua_range[7]['values']){
                            $post['built_up_area']=3;
                        }
                        else if($post['built_up_area'] >= $bua_range[9]['values'] && $post['built_up_area'] <= $bua_range[10]['values']){
                            $post['built_up_area']=4;
                        }
                        else if($post['built_up_area'] >= $bua_range[12]['values']) {
                            $post['built_up_area']=5;
                        }
                    }


                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = ($summaryBUA <> null) ? $summaryBUA : 'NA';
                    } else {

                        if($model->id <  $old_quotation_26_02_2024){
                            $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];
                            $result = QuotationFeeMasterFile::find()
                                ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        }
                        else{
                            $sub_heading = yii::$app->quotationHelperFunctions->getRangeArrAuto()[round($post['built_up_area'])];
                            $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                        }

                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                            $points += 0;
                            $summary['built_up_area']['fee'] = 0;
                            $summary['built_up_area']['title'] = ($summaryBUA <> null) ? $summaryBUA : 'NA';
                        }
                    }
                }
            } else {
                $points += 0;
                $summary['built_up_area']['fee'] = 0;
                $summary['built_up_area']['title'] = ($summaryBUA <> null) ? $summaryBUA : 'NA';
            }




            //tenure value (%) ---------------------------------------------------------------------------------
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "non_freehold";
            }

            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type']])->one();
            }
            else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['title'] = $tenureName;
            } else {
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = 'NA';
            }


            // complexity value (%) ---------------------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type']])->one();
            }
            else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type'],'property_type'=>$property_type])->one();
            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['title'] = $post['complexity'];
            } else {
                $points += 0;
                $summary['complexity']['fee'] = 0;
                $summary['complexity']['title'] = 'NA';
            }

            // upgrade value (%) -------------------------------------------------------------------
            if ($property_detail->upgrade_fee == 1) {
            
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type']])->one();
                }
                else{
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type'],'property_type'=>$property_type])->one();
                }
                if ($result != null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                } else {
                    $points += 0;
                    $summary['upgrade_value']['fee'] = 0;
                    $summary['upgrade_value']['title'] = 'NA';
                }
            }else {
                $points += 0;
                $summary['upgrade_value']['fee'] = 0;
                $summary['upgrade_value']['title'] = 'NA';
            }

            //number_of_comparables -------------------------------------------------------------------
            if ($model->id < 2237) {
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
            else{
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                } else {
                    $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type'],'property_type'=>$property_type])->one();
                }
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
           
            

            // other intedant users value (%) -------------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            }else {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type'],'property_type'=>$property_type])->one();
            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }

            // dd($summary);

            return $summary;

            //next process

            //Payment Terms
            $advance_payment = 0;
            $sub_heading_data = explode('%', $post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $sub_heading_data[0], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                if ($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            } else {
            }



            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => $name_repeat_valuation, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }

            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_3_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

    public function getAmount_autoProfit($post, $model)
    {
        // dd($post);
        $old_quotation_26_02_2024 = 2465;
        $points = 0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'profit';
        $post['approach_type'] = 'profit';

        $property_type = $post['property_type'];
        
        $ponits = 0;
        $base_fee = 0;

        $summary = [];
        //$summary['building_title']['title'] = $post['building_title'];

        if ($post['property'] != null && $post['city'] != null && $post['approach_type'] != null) {

            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();

            // subject property value -------------------------------------------------------------------
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type']])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();
            if($model->id <  $old_quotation_26_02_2024){
                $propertyResult = QuotationFeeMasterFile::find()->where(['heading' => 'subject_property', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['property']])->one();
            }
            else{
                $propertyResult = QuotationFeeMasterFile::find()->where(['heading' => 'property_fee', 'approach_type' => $post['approach_type'],'sub_heading'=>$property_type, 'property_type'=>$property_type])->one();
            }
            if ($propertyResult != null) {
                $points += $propertyResult['values'];
                $base_fee = $propertyResult['values'];
            } else {
                $points += 0;
                $base_fee = 0;
            } 

            //client Type (%) -------------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            }
            else {
                $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            } 


            //get property detail -------------------------------------------------------------------

            if ($propertyResult != null) {
                $summary['property']['fee'] = $propertyResult['values'];
                $summary['property']['title'] = $property_detail->title;
            } else {
                $summary['property']['fee'] = 0;
                $summary['property']['title'] = $property_detail->title;
            } 

            //height -------------------------------------------------------------------
            if ($post['typical_floors'] > 0) {
                // $typicalFloor = ($post['typical_floors'] > 0) ? " + " . $post['typical_floors'] . "F " : "";
                $typicalFloor = ($post['typical_floors'] > 1) ? " + " . ($post['typical_floors']-1) . "F " : "";
                $basementFloor = ($post['basement_floors'] > 0) ? " + " . $post['basement_floors'] . "B " : "";
                $mezzanineFloor = ($post['mezzanine_floors'] > 0) ? " + " . $post['mezzanine_floors'] . "M " : "";
                $parkingFloor = ($post['parking_floors'] > 0) ? " + " . $post['parking_floors'] . "P " : "";
                $height = "G" . $typicalFloor . $basementFloor . $mezzanineFloor . $parkingFloor;
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = $height;
            } else {
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = 'NA';
            } 


            //number of units 3,7,22 buildings wali han......-------------------------------------------------------------------
            if ($model->id < 2237) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $num_res_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_residential_units']];
                $num_com_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_commercial_units']];
                $num_ret_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['retails_units']];
                // dd($post['no_of_units']);
                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else if ($post['property'] == 18 || $post['property'] == 9) {
                        $no_of_units = 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = 'NA';
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = 'NA';
                }





                //number of residential units no_of_residential_units
                if ($post['no_of_residential_units'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_commercial_units'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_retail_units'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }

            } else {

                $num_units = $post['no_of_units_value'];
                $num_res_units = $post['no_of_res_units_value'];
                $num_com_units = $post['no_of_com_units_value'];
                $num_ret_units = $post['no_of_ret_units_value'];
                $num_warehouse_units = $post['no_of_warehouse_units_value'];


                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1 && $post['no_of_units_value'] <> null) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    if($model->id <  $old_quotation_26_02_2024){
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    }else{
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    }

                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else if ($post['property'] == 18 || $post['property'] == 9) {
                        $no_of_units = 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                }


                //number of residential units no_of_residential_units
                if ($post['no_of_res_units_value'] > 0) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'residential', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_residential_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_residential_units']['title'] = $num_res_units;
                    } else {
                        $points += 0;
                        $summary['no_of_residential_units']['fee'] = 0;
                        $summary['no_of_residential_units']['title'] = $num_res_units;
                    }
                    // $points += 0;
                    // $summary['no_of_residential_units']['fee'] = 0;
                    // $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of commercial units no_of_commercial_units

                if ($post['no_of_com_units_value'] > 0) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'commercial', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_commercial_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_commercial_units']['title'] = $num_res_units;
                    } else {
                        $points += 0;
                        $summary['no_of_commercial_units']['fee'] = 0;
                        $summary['no_of_commercial_units']['title'] = $num_res_units;
                    }
                    // $points += 0;
                    // $summary['no_of_commercial_units']['fee'] = 0;
                    // $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of retail units no_of_retail_units

                if ($post['no_of_ret_units_value'] > 0) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'retail', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_retail_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_retail_units']['title'] = $num_res_units;
                    } else {
                        $points += 0;
                        $summary['no_of_retail_units']['fee'] = 0;
                        $summary['no_of_retail_units']['title'] = $num_res_units;
                    }
                    // $points += 0;
                    // $summary['no_of_retail_units']['fee'] = 0;
                    // $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }

                //number of warehouse units no_of_warehouse_units

                if ($post['no_of_warehouse_units_value'] > 0) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'industrial', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_warehouse_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_warehouse_units']['title'] = $num_warehouse_units;
                    } else {
                        $points += 0;
                        $summary['no_of_warehouse_units']['fee'] = 0;
                        $summary['no_of_warehouse_units']['title'] = $num_warehouse_units;
                    }
                    // $points += 0;
                    // $summary['no_of_warehouse_units']['fee'] = 0;
                    // $summary['no_of_warehouse_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_warehouse_units']['fee'] = 0;
                    $summary['no_of_warehouse_units']['title'] = 'NA';
                }

            } 




            //number of units 3,7,22 buildings wali han...... -------------------------------------------------------------------


            $no_of_units = 0;

            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_rooms_building', 'sub_heading' => $post['no_of_rooms'], 'approach_type' => $post['approach_type']])->one();
            }else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_rooms_building', 'sub_heading' => $post['no_of_rooms'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }

            if ($result != null) {
                $no_of_units = 1;
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                // $summary['no_of_rooms']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                // $summary['no_of_rooms']['title'] = $post['no_of_rooms'];
            } else {
                $points += 0;
                // $summary['no_of_rooms']['fee'] = 0;
                // $summary['no_of_rooms']['title'] = "NA";
            } 


            // dd($post['number_of_types']);
            //number of type of units -------------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            }else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['title'] = $post['number_of_types'];
            } else {
                $points += 0;
                $summary['number_of_types']['fee'] = 0;
                $summary['number_of_types']['title'] = $post['number_of_types'];
            } 


            //city value (%)-------------------------------------------------------------------
            
            if ($post['city'] == 3506 || $post['city'] == 4260) {
                $citykey = 'abu_dhabi';
            } else if ($post['city'] == 3510) {
                $citykey = 'dubai';
            } else if ($post['city'] == 3507) {
                $citykey = 'ajman';
            } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                $citykey = 'sharjah';
            } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                $citykey = 'rak-fuj';
            } else {
                $citykey = 'others';
            }
            if ($post['city'] == 3506) {
                $summaryCity = 'Abu Dhabi';
            }
            if ($post['city'] == 4260) {
                $summaryCity = 'Al Ain';
            }
            if ($post['city'] == 3510) {
                $summaryCity = 'Dubai';
            }
            if ($post['city'] == 3507) {
                $summaryCity = 'Ajman';
            }
            if ($post['city'] == 3509) {
                $summaryCity = 'Sharjah';
            }
            if ($post['city'] == 3512) {
                $summaryCity = 'Umm Al Quwain';
            }
            if ($post['city'] == 3511) {
                $summaryCity = 'Ras Al Khaimah';
            }
            if ($post['city'] == 3508) {
                $summaryCity = 'Fujairah';
            }
            if ($post['type_of_valuation'] == 1 || $post['type_of_valuation'] == 2) {
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {

                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['title'] = $summaryCity;

                        }
                    }
                }
                else{
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    if ($result != null) {

                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['title'] = $summaryCity;

                        }
                    }
                }
            } else {
                $points += 0;
                $summary['city']['fee'] = 0;
                $summary['city']['title'] = $summaryCity;
            } 






            //land size (%)-------------------------------------------------------------------
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {
                if($model->id <  $old_quotation_26_02_2024){
                    if ($post['land_size'] >= 1 && $post['land_size'] <= 10000) {
                        $post['land_size'] = 1;
                    } else if ($post['land_size'] >= 10001 && $post['land_size'] <= 25000) {
                        $post['land_size'] = 2;
                    } else if ($post['land_size'] >= 25001 && $post['land_size'] <= 50000) {
                        $post['land_size'] = 3;
                    } else if ($post['land_size'] > 50001 && $post['land_size'] <= 150000) {
                        $post['land_size'] = 4;
                    } else if ($post['land_size'] > 150001 && $post['land_size'] <= 300000) {
                        $post['land_size'] = 5;
                    } else if ($post['land_size'] > 300001 && $post['land_size'] <= 500000) {
                        $post['land_size'] = 6;
                    } else if ($post['land_size'] > 500001 && $post['land_size'] <= 750000) {
                        $post['land_size'] = 7;
                    } else if ($post['land_size'] >= 7500001) {
                        $post['land_size'] = 8;
                    }
                }
                else {
                    $land_range = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'land', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();

                    if($post['land_size'] >=$land_range[0]['values'] && $post['land_size'] <= $land_range[1]['values']){
                        $post['land_size']=1;
                    }
                    else if($post['land_size'] >= $land_range[3]['values'] && $post['land_size'] <= $land_range[4]['values']){
                        $post['land_size']=2;
                    }
                    else if($post['land_size'] >= $land_range[6]['values'] && $post['land_size'] <= $land_range[7]['values']){
                        $post['land_size']=3;
                    }
                    else if($post['land_size'] > $land_range[9]['values'] && $post['land_size'] <= $land_range[10]['values']){
                        $post['land_size']=4;
                    }
                    else if($post['land_size'] > $land_range[12]['values'] && $post['land_size'] <= $land_range[13]['values']){
                        $post['land_size']=5;
                    }
                    else if($post['land_size'] >= $land_range[15]['values']) {
                        $post['land_size']=6;
                    }
                }
                if ($post['land_size'] < 1) {
                    $points += 0;
                    $summary['land_size']['fee'] = 0;
                    $summary['land_size']['title'] = $summaryLandSize;
                } else {
                    if($model->id <  $old_quotation_26_02_2024){
                        $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoProfit()[$post['land_size']];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type']])->one();
                    }else {
                        $landSize = yii::$app->quotationHelperFunctions->getRangeArrAuto()[$post['land_size']];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    }
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['title'] = $summaryLandSize;
                    }
                }
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = $summaryLandSize;
            }





            //built up area-------------------------------------------------------------------
            $summaryBUA = $post['built_up_area'];
            // $summaryBUA = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            // $post['built_up_area'] = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            if ($no_of_units == 0) {

                //built up area (%)
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                    $summary['built_up_area']['fee'] = 0;
                    $summary['built_up_area']['title'] = ($summaryBUA <> null) ? $summaryBUA : 'NA';
                } else {
                    if($model->id <  $old_quotation_26_02_2024){
                        if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 50000) {
                            $post['built_up_area'] = 1;
                        } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 125000) {
                            $post['built_up_area'] = 2;
                        } else if ($post['built_up_area'] >= 125001 && $post['built_up_area'] <= 250000) {
                            $post['built_up_area'] = 3;
                        } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 750000) {
                            $post['built_up_area'] = 4;
                        } else if ($post['built_up_area'] >= 750001 && $post['built_up_area'] <= 1500000) {
                            $post['built_up_area'] = 5;
                        } else if ($post['built_up_area'] >= 1500001 && $post['built_up_area'] <= 2500000) {
                            $post['built_up_area'] = 6;
                        } else if ($post['built_up_area'] >= 2500001 && $post['built_up_area'] <= 3750000) {
                            $post['built_up_area'] = 7;
                        } else if ($post['built_up_area'] >= 3750001) {
                            $post['built_up_area'] = 8;
                        }
                    } else {
                        $bua_range = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();


                        if($post['built_up_area'] >=$bua_range[0]['values'] && $post['built_up_area'] <= $bua_range[1]['values']){
                            $post['built_up_area']=1;
                        }
                        else if($post['built_up_area'] >= $bua_range[3]['values'] && $post['built_up_area'] <= $bua_range[4]['values']){
                            $post['built_up_area']=2;
                        }
                        else if($post['built_up_area'] >= $bua_range[6]['values'] && $post['built_up_area'] <= $bua_range[7]['values']){
                            $post['built_up_area']=3;
                        }
                        else if($post['built_up_area'] >= $bua_range[9]['values'] && $post['built_up_area'] <= $bua_range[10]['values']){
                            $post['built_up_area']=4;
                        }
                        else if($post['built_up_area'] >= $bua_range[12]['values']) {
                            $post['built_up_area']=5;
                        }
                    }
                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = ($summaryBUA <> null) ? $summaryBUA : 'NA';
                    } else {

                        if($model->id <  $old_quotation_26_02_2024){
                            $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoProfit()[round($post['built_up_area'])];
                            $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        }else{
                            $sub_heading = yii::$app->quotationHelperFunctions->getRangeArrAuto()[round($post['built_up_area'])];
                            $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();

                        }
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                            $points += 0;
                            $summary['built_up_area']['fee'] = 0;
                            $summary['built_up_area']['title'] = ($summaryBUA <> null) ? $summaryBUA : 'NA';
                        }
                    }
                }
            }else {
                $points += 0;
                $summary['built_up_area']['fee'] = 0;
                $summary['built_up_area']['title'] = ($summaryBUA <> null) ? $summaryBUA : 'NA';
            } 

            // dd($post);
            //number of type of resturants
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'restaurant', 'sub_heading' => $post['restaurant'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                // $summary['restaurant']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                // $summary['restaurant']['title'] = $post['restaurant'];
            } else {
                $points += 0;
                // $summary['restaurant']['fee'] = 0;
                // $summary['restaurant']['title'] = $post['restaurant'];
            }

            //number of type of ballrooms
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'ballrooms', 'sub_heading' => $post['ballrooms'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['ballrooms']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['ballrooms']['title'] = $post['ballrooms'];
            } else {
                $points += 0;
                //$summary['ballrooms']['fee'] = 0;
                //$summary['ballrooms']['title'] = $post['ballrooms'];
            }


            //number of type of atms
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'atms', 'sub_heading' => $post['atms'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['type_of_atms']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['type_of_atms']['title'] = $post['atms'];
            } else {
                $points += 0;
                //$summary['type_of_atms']['fee'] = 0;
                //$summary['type_of_atms']['title'] = $post['atms'];
            }

            //number of type of retails_units
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'retails_units', 'sub_heading' => $post['retails_units'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['retails_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['retails_units']['title'] = $post['retails_units'];
            } else {
                $points += 0;
                //$summary['retails_units']['fee'] = 0;
                //$summary['retails_units']['title'] = $post['retails_units'];
            }

            //number of type of night_clubs
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'night_clubs', 'sub_heading' => $post['night_clubs'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['night_clubs']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['night_clubs']['title'] = $post['night_clubs'];
            } else {
                $points += 0;
                //$summary['night_clubs']['fee'] = 0;
                //$summary['night_clubs']['title'] = $post['night_clubs'];
            }

            //number of type of bars
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'bars', 'sub_heading' => $post['bars'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['number_of_type_of_bars']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['number_of_type_of_bars']['title'] = $post['bars'];
            } else {
                $points += 0;
                //$summary['number_of_type_of_bars']['fee'] = 0;
                //$summary['number_of_type_of_bars']['title'] = $post['bars'];
            }

            //number of type of health_club
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'health_club', 'sub_heading' => $post['health_club'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['health_club']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['health_club']['title'] = $post['health_club'];
            } else {
                $points += 0;
                //$summary['health_club']['fee'] = 0;
                //$summary['health_club']['title'] = $post['health_club'];
            }

            //number of type of meeting_rooms
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'meeting_rooms', 'sub_heading' => $post['meeting_rooms'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['meeting_rooms']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['meeting_rooms']['title'] = $post['meeting_rooms'];
            } else {
                $points += 0;
                //$summary['meeting_rooms']['fee'] = 0;
                //$summary['meeting_rooms']['title'] = $post['meeting_rooms'];
            }

            //number of type of spa
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'spa', 'sub_heading' => $post['spa'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['type_of_spa']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['type_of_spa']['title'] = $post['spa'];
            } else {
                $points += 0;
                //$summary['type_of_spa']['fee'] = 0;
                //$summary['type_of_spa']['title'] = $post['spa'];
            }

            //number of type of beach_access
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'beach_access', 'sub_heading' => $post['beach_access'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['beach_access']['fee'] = $this->getPercentageAmount($result['values'], $base_fee); 
                //$summary['beach_access']['title'] = $post['beach_access'];
            } else {
                $points += 0;
                //$summary['beach_access']['fee'] = 0;
                //$summary['beach_access']['title'] = $post['beach_access'];
            }

            //number of type of parking_sale
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'parking_sale', 'sub_heading' => $post['parking_sale'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['parking_sale']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                //$summary['parking_sale']['title'] = $post['parking_sale'];
            } else {
                $points += 0;
                //$summary['parking_sale']['fee'] = 0;
                //$summary['parking_sale']['title'] = $post['parking_sale'];
            }


            //tenure value (%)-------------------------------------------------------------
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "non_freehold";
            }
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type']])->one();
            }
            else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();

            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['title'] = $tenureName;
            } else {
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = 'NA';
            }


            // complexity value (%)-------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type']])->one();
            }else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();

            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['title'] = $post['complexity'];
            } else {
                $points += 0;
                $summary['complexity']['fee'] = 0;
                $summary['complexity']['title'] = 'NA';
            }

            // upgrade value (%)-------------------------------------------------------------
            if ($property_detail->upgrade_fee == 1) {
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type']])->one();
                }else{
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                }
                if ($result != null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                } else {
                    $points += 0;
                    $summary['upgrade_value']['fee'] = 0;
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                }
            }else {
                $points += 0;
                $summary['upgrade_value']['fee'] = 0;
                $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
            }

            //number_of_comparables -------------------------------------------------------------
            if ($model->id < 2237) {
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
            else{
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                }else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                }
                
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }

            // other intedant users value (%) -------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            }else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }


            // last_3_years_finance value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_three_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
            if ($post['last_three_years_finance'] == 0) {
                $summary_last_three_years_finance = 'No';
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    //$summary['last_3_years_finance']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    //$summary['last_3_years_finance']['title'] = $summary_last_three_years_finance;
                } else {
                    $points += 0;
                    //$summary['last_3_years_finance']['fee'] = 0;
                    //$summary['last_3_years_finance']['title'] = $summary_last_three_years_finance;
                }
            } else {
                $summary_last_three_years_finance = 'Yes';
                //$summary['last_3_years_finance']['fee'] = 0;
                //$summary['last_3_years_finance']['title'] = $summary_last_three_years_finance;
            }


            // ten_years_projections value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
            if ($post['ten_years_projections'] == 0) {
                $summary_ten_years_projections = 'No';
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    //$summary['ten_years_projections']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    //$summary['ten_years_projections']['title'] = $summary_ten_years_projections;
                } else {
                    $points += 0;
                    //$summary['ten_years_projections']['fee'] = 0;
                    //$summary['ten_years_projections']['title'] = $summary_ten_years_projections;
                }
            } else {
                $summary_ten_years_projections = 'Yes';
                //$summary['ten_years_projections']['fee'] = 0;
                //$summary['ten_years_projections']['title'] = $summary_ten_years_projections;
            }
            

            // dd($summary);
            return $summary;














            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    // echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_3_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

    public function getAmount_auto($post, $model)
    {
        
        $old_quotation_26_02_2024 = 2465;
        $points = 0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'market';
        $post['approach_type'] = 'market';

        $property_type = $post['property_type'];

        $ponits = 0;
        $base_fee = 0;

        $summary = [];
        //$summary['building_title']['title'] = $post['building_title'];


        if ($post['property'] != null && $post['city'] != null && $post['tenure'] != null && $post['approach_type'] != null) {

            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();


            // property fee ---------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $propertyResult = QuotationFeeMasterFile::find()->where(['heading' => 'subject_property', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['property']])->one();
            }else{
                $propertyResult = QuotationFeeMasterFile::find()->where(['heading' => 'property_fee', 'approach_type' => $post['approach_type'],'sub_heading'=>$property_type, 'property_type'=>$property_type])->one();
            }

            if ($propertyResult != null) {
                $points += $propertyResult['values'];
                $base_fee = $propertyResult['values'];
            } else {
                $points += 0;
                $base_fee = 0;
            }


            //client Type (%) --------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            }
            else{
                $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }           
            
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            }

            //get property detail ----------------------------------------------------------
            if ($propertyResult != null) {
                $summary['property']['fee'] = $propertyResult['values'];
                $summary['property']['title'] = $property_detail->title;
            } else {
                $summary['property']['fee'] = 0;
                $summary['property']['title'] = $property_detail->title;
            }


            //height ------------------------------------------------------------------------
            if ($post['typical_floors'] > 0 && $post['property'] != 1) {
                // $typicalFloor = ($post['typical_floors'] > 0) ? " + " . $post['typical_floors'] . "F " : "";
                $typicalFloor = ($post['typical_floors'] > 1) ? " + " . ($post['typical_floors']-1) . "F " : "";
                $basementFloor = ($post['basement_floors'] > 0) ? " + " . $post['basement_floors'] . "B " : "";
                $mezzanineFloor = ($post['mezzanine_floors'] > 0) ? " + " . $post['mezzanine_floors'] . "M " : "";
                $parkingFloor = ($post['parking_floors'] > 0) ? " + " . $post['parking_floors'] . "P " : "";
                $height = "G" . $typicalFloor . $basementFloor . $mezzanineFloor . $parkingFloor;
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = $height;
            } else {
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = 'NA';
            }



            //  number of units --------------------------------------------------------------

            if ($model->id < 2237) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $num_res_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_residential_units']];
                $num_com_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_commercial_units']];
                $num_ret_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_retail_units']];

                //number of units 
                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = 'NA';
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else if ($post['property'] == 12) {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = 'NA';
                }


                //number of residential units no_of_residential_units
                if ($post['no_of_residential_units'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_commercial_units'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_retail_units'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            } else {

                $num_units = $post['no_of_units_value'];
                $num_res_units = $post['no_of_res_units_value'];
                $num_com_units = $post['no_of_com_units_value'];
                $num_ret_units = $post['no_of_ret_units_value'];
                $num_warehouse_units = $post['no_of_warehouse_units_value'];

                //number of units 
                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1 && $post['no_of_units_value'] <> null) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    if($model->id <  $old_quotation_26_02_2024){
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    }
                    else{
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    }
                    
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else if ($post['property'] == 12) {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                }


                //number of residential units no_of_residential_units
                if ($post['no_of_res_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of commercial units no_of_commercial_units
                if ($post['no_of_com_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of retail units no_of_ret_units
                if ($post['no_of_ret_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }

                //number of warehouse units no_of_warehouse_units        
                if ($post['no_of_warehouse_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_warehouse_units']['fee'] = 0;
                    $summary['no_of_warehouse_units']['title'] = $num_warehouse_units;
                } else {
                    $points += 0;
                    $summary['no_of_warehouse_units']['fee'] = 0;
                    $summary['no_of_warehouse_units']['title'] = 'NA';
                }
            }







            // number of types -----------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            }
            else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }
            
            if ($result != null) {

            } else {
                $points += 0;
                $summary['number_of_types']['fee'] = 0;
                $summary['number_of_types']['title'] = 'NA';
            }

            //sub_community
            // $community = \app\models\SubCommunities::find()->where(['id'=>$post['community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'community', 'sub_heading'=>$post['community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['community']['title'] = $community->title;
            // }else{
            //     $points += 0;
            //     $summary['community']['fee'] = 0;
            //     $summary['community']['title'] = $community->title;
            // }

            //sub_community
            // $subCommunity = \app\models\SubCommunities::find()->where(['id'=>$post['sub_community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'sub_community', 'sub_heading'=>$post['sub_community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }else{
            //     $points += 0;
            //     $summary['sub_community']['fee'] = 0;
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }


            //city value (%) -----------------------------------------------------------------------
            if ($post['city'] == 3506 || $post['city'] == 4260) {
                $citykey = 'abu_dhabi';
            } else if ($post['city'] == 3510) {
                $citykey = 'dubai';
            } else if ($post['city'] == 3507) {
                $citykey = 'ajman';
            } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                $citykey = 'sharjah';
            } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                $citykey = 'rak-fuj';
            } else {
                $citykey = 'others';
            }

            if ($post['city'] == 3506) {
                $summaryCity = 'Abu Dhabi';
            }
            if ($post['city'] == 4260) {
                $summaryCity = 'Al Ain';
            }
            if ($post['city'] == 3510) {
                $summaryCity = 'Dubai';
            }
            if ($post['city'] == 3507) {
                $summaryCity = 'Ajman';
            }
            if ($post['city'] == 3509) {
                $summaryCity = 'Sharjah';
            }
            if ($post['city'] == 3512) {
                $summaryCity = 'Umm Al Quwain';
            }
            if ($post['city'] == 3511) {
                $summaryCity = 'Ras Al Khaimah';
            }
            if ($post['city'] == 3508) {
                $summaryCity = 'Fujairah';
            }
            if ($post['type_of_valuation'] == 1 || $post['type_of_valuation'] == 2) {

                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {

                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['title'] = $summaryCity;
                        }
                    }
                }
                else{
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    if ($result != null) {

                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['city']['title'] = $summaryCity;
                        }
                    }
                }

                
            } else {
                $points += 0;
                $summary['city']['fee'] = 0;
                $summary['city']['title'] = $summaryCity;;
            }





            //land size (%) ------------------------------------------------------------------------
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {

                if($model->id <  $old_quotation_26_02_2024){
                    if ($post['land_size'] >= 1 && $post['land_size'] <= 5000) {
                        $post['land_size'] = 1;
                    } else if ($post['land_size'] >= 5001 && $post['land_size'] <= 7500) {
                        $post['land_size'] = 2;
                    } else if ($post['land_size'] >= 7501 && $post['land_size'] <= 10000) {
                        $post['land_size'] = 3;
                    } else if ($post['land_size'] > 10001 && $post['land_size'] <= 15000) {
                        $post['land_size'] = 4;
                    } else if ($post['land_size'] > 15001 && $post['land_size'] <= 25000) {
                        $post['land_size'] = 5;
                    } else if ($post['land_size'] >= 25001) {
                        $post['land_size'] = 6;
                    }

                    if ($post['land_size'] < 1) {
                        $points += 0;
                        $summary['land_size'] = 0;
                    } else {
                        $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAuto()[$post['land_size']];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type']])->one();
    
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['land_size']['title'] = $summaryLandSize;
                        }
                    }
                }
                else{
                    $land_range = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'land', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();

                    if($post['land_size'] >=$land_range[0]['values'] && $post['land_size'] <= $land_range[1]['values']){
                        $post['land_size']=1;
                    }
                    else if($post['land_size'] >= $land_range[3]['values'] && $post['land_size'] <= $land_range[4]['values']){
                        $post['land_size']=2;
                    }
                    else if($post['land_size'] >= $land_range[6]['values'] && $post['land_size'] <= $land_range[7]['values']){
                        $post['land_size']=3;
                    }
                    else if($post['land_size'] > $land_range[9]['values'] && $post['land_size'] <= $land_range[10]['values']){
                        $post['land_size']=4;
                    }
                    else if($post['land_size'] > $land_range[12]['values'] && $post['land_size'] <= $land_range[13]['values']){
                        $post['land_size']=5;
                    }
                    else if($post['land_size'] >= $land_range[15]['values']) {
                        $post['land_size']=6;
                    }

                    if ($post['land_size'] < 1) {
                        $points += 0;
                        $summary['land_size'] = 0;
                    } else {
    
                        $landSize = yii::$app->quotationHelperFunctions->getRangeArrAuto()[$post['land_size']];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['land_size']['title'] = $summaryLandSize;
                        }
                    }
                }
                
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = 'NA';
            }



            // Built up area ---------------------------------------------------------------------
            $summaryBUA = $post['built_up_area'];
            // $summaryBUA = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            // $post['built_up_area'] = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            //built up area (%)
            if ($property_detail->bua_fee != 1) {
                $points += 0;
                $summary['built_up_area']['fee'] = 0;
                $summary['built_up_area']['title'] = ($summaryBUA <> null) ? $summaryBUA : 'NA';
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else if ($post['property'] == 4 || $post['property'] == 5 || $post['property'] == 39 || $post['property'] == 49 || $post['property'] == 48 || $post['property'] == 50 || $post['property'] == 53) {
                //building land built up area
                if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 25000) {
                    $post['built_up_area'] = 1;
                } else if ($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 75000) {
                    $post['built_up_area'] = 2;
                } else if ($post['built_up_area'] >= 75001 && $post['built_up_area'] <= 200000) {
                    $post['built_up_area'] = 3;
                } else if ($post['built_up_area'] >= 200001 && $post['built_up_area'] <= 400000) {
                    $post['built_up_area'] = 4;
                } else if ($post['built_up_area'] >= 400001 && $post['built_up_area'] <= 750000) {
                    $post['built_up_area'] = 5;
                } else if ($post['built_up_area'] >= 750001) {
                    $post['built_up_area'] = 6;
                }
                if ($post['built_up_area'] < 1) {
                    $points += 0;
                    $summary['built_up_area']['fee'] = 0;
                    $summary['built_up_area']['title'] = ($summaryBUA <> null) ? $summaryBUA : 'NA';
                } else {

                    $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaBLArrAuto()[round($post['built_up_area'])];
                    // $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property_b_land', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property_b_land', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['built_up_area']['title'] = $summaryBUA;
                    } else {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = ($summaryBUA <> null) ? $summaryBUA : 'NA';
                    }
                }

            } else {

                if($model->id <  $old_quotation_26_02_2024){
                    if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 2000) {
                        $post['built_up_area'] = 1;
                    } else if ($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000) {
                        $post['built_up_area'] = 2;
                    } else if ($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000) {
                        $post['built_up_area'] = 3;
                    } else if ($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000) {
                        $post['built_up_area'] = 4;
                    } else if ($post['built_up_area'] >= 8001) {
                        $post['built_up_area'] = 5;
                    }
                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = $summaryBUA;
                    } else {
    
                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAuto()[round($post['built_up_area'])];
                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                        }
                    }
                }
                else{

                    $bua_range = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();


                    if($post['built_up_area'] >=$bua_range[0]['values'] && $post['built_up_area'] <= $bua_range[1]['values']){
                        $post['built_up_area']=1;
                    }
                    else if($post['built_up_area'] >= $bua_range[3]['values'] && $post['built_up_area'] <= $bua_range[4]['values']){
                        $post['built_up_area']=2;
                    }
                    else if($post['built_up_area'] >= $bua_range[6]['values'] && $post['built_up_area'] <= $bua_range[7]['values']){
                        $post['built_up_area']=3;
                    }
                    else if($post['built_up_area'] >= $bua_range[9]['values'] && $post['built_up_area'] <= $bua_range[10]['values']){
                        $post['built_up_area']=4;
                    }
                    else if($post['built_up_area'] >= $bua_range[12]['values']) {
                        $post['built_up_area']=5;
                    }

                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = $summaryBUA;
                    } else {

                        $sub_heading = yii::$app->quotationHelperFunctions->getRangeArrAuto()[round($post['built_up_area'])];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();

                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                        }
                    }
                }
            }



            //tenure value (%) ---------------------------------------------------------------------
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "non_freehold";
            }
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type']])->one();
            }
            else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }                
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['title'] = $tenureName;
            } else {
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = 'NA';
            }


            // complexity value (%)--------------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type']])->one();
            }
            else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }
            
            
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['title'] = $post['complexity'];
            } else {
                $points += 0;
                $summary['complexity']['fee'] = 0;
                $summary['complexity']['title'] = 'NA';
            }

            // upgrade value (%) --------------------------------------------------------------------
            if ($property_detail->upgrade_fee == 1) {
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type']])->one();
                }
                else{
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                }
                
                if ($result != null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                } else {
                    $points += 0;
                    $summary['upgrade_value']['fee'] = 0;
                    $summary['upgrade_value']['title'] = 'NA';
                }
            } else {
                $points += 0;
                $summary['upgrade_value']['fee'] = 0;
                $summary['upgrade_value']['title'] = 'NA';
            }

            //number_of_comparables -----------------------------------------------------------------
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            // $result = QuotationFeeMasterFile::find()
            //     ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
            // if ($result != null) {
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
            // } else {
            //     $points += 0;
            //     $summary['number_of_comparables']['fee'] = 0;
            //     $summary['number_of_comparables']['title'] = 'NA';
            // }

            if ($model->id < 2237) {
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
            else{
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                }
                else{
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                }
                
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }

            // other intedant users value (%)----------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            }
            else{
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }
            
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }

            return $summary;
            // return $points;

            //next process

            //Payment Terms
            $advance_payment = 0;
            $sub_heading_data = explode('%', $post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $sub_heading_data[0], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                if ($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            } else {
            }



            /*   echo $points;
               die;*/








            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => $name_repeat_valuation, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }








            //number of units 3,7,22 buildings wali han......

            if ($post['built_up_area'] > 0 && ($approach_type == 'income' || $approach_type == 'profit')) {

            } else {


                if ($property_detail->no_of_units_fee == 1) {
                    $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $num_units, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $result['values'];

                        // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "12"; die();
                        $points += 0;
                    }
                }
            }





            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    // echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_3_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

    public function getAmount_new($post, $model)
    {
        // dd($model);

        $ponits = 0;

        $summary = [];
        //$summary['building_title']['title'] = $post['building_title'];



        if ($post['property'] != null && $post['city'] != null && $post['tenure'] != null) {

            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();


            // subject property value
            $name = Properties::find()->select(['title'])->where(['id' => $post['property']])->one();
            $str = $name['title'];

            //  print_r ($name);
            $name_pro = explode(" ", $str);

            /* echo $name_pro[0];
             die;*/

            //client Type
            $result = $result = ProposalMasterFile::find()->where(['heading' => 'Client Type', 'sub_heading' => $post['clientType']])->one();
            if ($result != null) {
                $points += $result['values'];
                $summary['client_type']['fee'] = $result['values'];
                $summary['client_type']['title'] = $model->client->client_type;
                // yii::info('clientType if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                return false;
            }

            $result = ProposalMasterFile::find()->where(['heading' => 'Subject Property'])->andWhere(['like', 'sub_heading', '%' . trim($name_pro[0]) . '%', false])->one();

            if ($result != null) {
                if ($post['property'] != 7 && $post['property'] != 3 && $post['property'] != 22) {
                    $points += $result['values'];
                    $summary['property']['fee'] = $result['values'];
                    $summary['property']['title'] = $name->title;
                } else {
                    $points += 0;
                    $summary['property']['fee'] = 0;
                    $summary['property']['title'] = $name->title;
                }

                // yii::info('subj property if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                $result = ProposalMasterFile::find()->where(['heading' => 'Subject Property', 'sub_heading' => 'others'])->one();
                if ($result != null) {
                    if ($post['property'] != 7 && $post['property'] != 3 && $post['property'] != 22) {
                        $points += $result['values'];
                        $summary['property']['fee'] = $result['values'];
                        $summary['property']['title'] = $name->title;

                    }
                    // yii::info('subj property else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                } else {
                    // echo "3"; die();
                    return false;
                }
            }


            //number of units 3,7,22 buildings wali han......
            if ($property_detail->no_of_units_fee == 1) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                // echo "hello"; die();
                $result = ProposalMasterFile::find()->where(['heading' => 'No Of Units Building', 'sub_heading' => $num_units])->one();
                if ($result != null) {
                    $points += $result['values'];
                    $summary['num_units']['fee'] = $result['values'];
                    $summary['num_units']['title'] = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];

                    // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                } else {
                    // echo "12"; die();
                    $points += 0;
                    $summary['num_units']['fee'] = 0;
                    $summary['num_units']['title'] = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                }
            }



            //Payment Terms
            $result = $result = ProposalMasterFile::find()->where(['heading' => 'Payment Terms', 'sub_heading' => $post['paymentTerms']])->one();
            if ($result != null) {
                // echo $result['values']; die();
                if ($post['clientType'] != 'bank') {
                    $points += $result['values'];
                    $summary['payment_terms']['fee'] = $result['values'];
                    $summary['payment_terms']['title'] = $model->advance_payment_terms;
                }
                // yii::info('payment term if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                //  echo "2"; die();
                return false;
            }


            //sub_community
            // $subCommunity = \app\models\SubCommunities::find()->where(['id'=>$post['sub_community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'sub_community', 'sub_heading'=>$post['sub_community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }else{
            //     $points += 0;
            //     $summary['sub_community']['fee'] = 0;
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }           


            if ($post['city'] == 3506 || $post['city'] == 4260) {
                $citykey = 'Abu Dhabi';
            } else if ($post['city'] == 3510) {
                $citykey = 'dubai';
            } else if ($post['city'] == 3507) {
                $citykey = 'ajman';
            } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                $citykey = 'sharjah';
            } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                $citykey = 'rak-fuj';
            } else {
                $citykey = 'others';
            }
            if ($post['city'] == 3506) {
                $summaryCity = 'Abu Dhabi';
            }
            if ($post['city'] == 4260) {
                $summaryCity = 'Al Ain';
            }
            if ($post['city'] == 3510) {
                $summaryCity = 'Dubai';
            }
            if ($post['city'] == 3507) {
                $summaryCity = 'Ajman';
            }
            if ($post['city'] == 3509) {
                $summaryCity = 'Sharjah';
            }
            if ($post['city'] == 3512) {
                $summaryCity = 'Umm Al Quwain';
            }
            if ($post['city'] == 3511) {
                $summaryCity = 'Ras Al Khaimah';
            }
            if ($post['city'] == 3508) {
                $summaryCity = 'Fujairah';
            }
            //city value
            $result = ProposalMasterFile::find()->where(['heading' => 'City', 'sub_heading' => $citykey])->one();

            if ($result != null) {

                $points += $result['values'];
                $summary['city']['fee'] = $result['values'];
                $summary['city']['title'] = $summaryCity;

                // yii::info('city if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "else";
                $result = ProposalMasterFile::find()->where(['heading' => 'City', 'sub_heading' => 'others'])->one();
                if ($result != null) {
                    // echo $result['values']; die();
                    $points += $result['values'];
                    $summary['city']['fee'] = $result['values'];
                    $summary['city']['title'] = $summaryCity;
                    // yii::info('city else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                } else {
                    // echo "4"; die();
                    return false;
                }
            }




            $type_of_valuation = '';
            if ($post['type_of_valuation'] == 1) {
                $type_of_valuation = 'drive-by';
            } else if ($post['type_of_valuation'] == 2) {
                $type_of_valuation = 'physical-inspection';
            } else {
                $type_of_valuation = 'desktop';
            }

            //Type of Valuation
            $result = ProposalMasterFile::find()
                ->where(['heading' => 'Type of Valuation', 'sub_heading' => $type_of_valuation])->one();

            if ($result != null) {
                $points += $result['values'];
                $summary['type_of_valuation']['fee'] = $result['values'];
                $summary['type_of_valuation']['title'] = $type_of_valuation;
                // yii::info('type of val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "7"; die();
                return false;
            }


            //tenure value
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            // echo $tenureName;
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "Non-Freehold";
            }

            $result = ProposalMasterFile::find()->where(['heading' => 'Tenure', 'sub_heading' => $tenureName])->one();
            if ($result != null) {
                // echo "working"; echo $result['values']; die();
                $points += $result['values'];
                $summary['tenure']['fee'] = $result['values'];
                $summary['tenure']['title'] = $tenureName;
                // yii::info('tenure if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "5"; die();
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = $tenureName;
            }

            // dd($summary);
            // echo $post['complexity']; die;

            $summaryComplexity = $post['complexity'];
            // complexity value
            $result = ProposalMasterFile::find()->where(['heading' => 'Complexity', 'sub_heading' => $post['complexity']])->one();
            // dd($result);

            if ($result != null) {
                $points += $result['values'];
                $summary['complexity']['fee'] = $result['values'];
                $summary['complexity']['title'] = $summaryComplexity;
                /* echo $points;
                 die;*/
                // yii::info('complexity if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "6"; die();
                // return false;
            }
            // dd($summary);


            if ($property_detail->upgrade_fee == 1) {
                // echo "land"; die();
                $result = ProposalMasterFile::find()->where(['heading' => 'Number of Units Land', 'sub_heading' => $post['upgrades']])->one();

                if ($result != null) {
                    $points += $result['values'];
                    $summary['upgrades']['fee'] = $result['values'];
                    $summary['upgrades']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                    // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                } else {
                    // echo "13"; die();
                    $points += 0;
                    $summary['upgrades']['fee'] = 0;
                    $summary['upgrades']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                }

            }


            //number_of_comparables
            

            if ($model->id < 2237) {
                $result = ProposalMasterFile::find()
                    ->where(['heading' => 'Number of Comparables Data', 'sub_heading' => $post['number_of_comparables']])->one();
                if ($result != null) {
                    // echo $result['values']; die();
                    $points += $result['values'];
                    $summary['number_of_comparables']['fee'] = $result['values'];
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                    // yii::info('No of comparables if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                } else {
                    // echo "8"; die();
                    return false;
                }
            }
            else{
                $result = ProposalMasterFile::find()
                    ->where(['heading' => 'Number of Comparables Data', 'sub_heading' => $post['number_of_comparables']])->one();
                if ($result != null) {
                    // echo $result['values']; die();
                    $points += $result['values'];
                    $summary['number_of_comparables']['fee'] = $result['values'];
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value '];
                    // yii::info('No of comparables if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                } else {
                    // echo "8"; die();
                    return false;
                }
            }


            //new repeat_valuation
            $result = ProposalMasterFile::find()
                ->where(['heading' => 'New Repeat Valuation Data', 'sub_heading' => $post['repeat_valuation']])->one();
            if ($result != null) {
                $points += $result['values'];
                $summary['repeat_valuation']['fee'] = $result['values'];
                $summary['repeat_valuation']['title'] = $post['repeat_valuation'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }



            //built up area
            if ($property_detail->bua_fee != 1) {

                // echo "if bla bla bla bla"; die();
                $points += 0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                $summaryBUA = $post['built_up_area'];
                if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 2000) {
                    $post['built_up_area'] = 1;
                    $post['built_up_area'] = 1;
                } else if ($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000) {
                    $post['built_up_area'] = 2;
                } else if ($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000) {
                    $post['built_up_area'] = 3;
                } else if ($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000) {
                    $post['built_up_area'] = 4;
                } else if ($post['built_up_area'] > 8001 && $post['built_up_area'] <= 10000) {
                    $post['built_up_area'] = 5;
                } else if ($post['built_up_area'] >= 10001) {
                    $post['built_up_area'] = 6;
                }



                if ($post['built_up_area'] < 1) {
                    $points += 0;
                    $summary['built_up_area']['fee'] = 0;
                    $summary['built_up_area']['title'] = $summaryBUA;
                } else {

                    // echo "else bla bla bla bla"; die();
                    $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArr()[round($post['built_up_area'])];

                    $result = ProposalMasterFile::find()
                        ->where(['heading' => 'Build up Area of Subject Property', 'sub_heading' => $sub_heading])->one();
                    if ($result != null) {
                        // echo $result['values']; die();
                        $points += $result['values'];
                        $summary['built_up_area']['fee'] = $result['values'];
                        $summary['built_up_area']['title'] = $summaryBUA;
                        // yii::info('BUA else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "10"; die();
                        return false;
                    }
                }
            }



            //land size
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {


                if ($post['land_size'] >= 1 && $post['land_size'] <= 5000) {
                    $post['land_size'] = 1;
                } else if ($post['land_size'] >= 5001 && $post['land_size'] <= 7500) {
                    $post['land_size'] = 2;
                } else if ($post['land_size'] >= 7501 && $post['land_size'] <= 10000) {
                    $post['land_size'] = 3;
                } else if ($post['land_size'] > 10001 && $post['land_size'] <= 15000) {
                    $post['land_size'] = 4;
                } else if ($post['land_size'] > 15000 && $post['land_size'] <= 25000) {
                    $post['land_size'] = 5;
                } else if ($post['land_size'] >= 25001) {
                    $post['land_size'] = 6;
                }


                if ($post['land_size'] < 1) {
                    $points += 0;
                    $summary['land_size']['fee'] = 0;
                    $summary['land_size']['title'] = $summaryLandSize;
                } else {

                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArr()[$post['land_size']];

                    $result = ProposalMasterFile::find()->where(['heading' => 'Land', 'sub_heading' => $landSize])->one();

                    if ($result != null) {

                        $points += $result['values'];
                        $summary['land_size']['fee'] = $result['values'];
                        $summary['land_size']['title'] = $summaryLandSize;
                        // yii::info('landsize if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "11"; die();
                        return false;
                    }
                }
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = $summaryLandSize;
                // yii::info('landsize else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }







            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me"; die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            $summary['base_fee']['fee'] = $baseFee;
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;
            $summary['total_fee']['fee'] = $fee;


            return $summary;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }


    public function getBaseFee($property_id)
    {
        if ($property_id != null) {
            if ($property_id == 3 || $property_id == 7 || $property_id == 22 || $property_id == 42 || $property_id == 43 || $property_id == 9 || $property_id == 18) {
                $baseFee = ProposalMasterFile::find()
                    ->where(['heading' => 'Base Fee Building', 'sub_heading' => 'base-fee-building'])->one();
                if ($baseFee != null) {
                    return $baseFee['values'];
                } else {
                    return false;
                }

            } else {
                $baseFee = ProposalMasterFile::find()
                    ->where(['heading' => 'Base Fee Others', 'sub_heading' => 'base-fee-others'])->one();
                if ($baseFee != null) {
                    return $baseFee['values'];
                } else {
                    return false;
                }

            }
        } else {
            return false;
        }

    }




    public function getAmount_autoRFS($post, $model)
    {
        $summary = [];
        $points = 0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'bcs';
        $post['approach_type'] = 'bcs';
        $post['bcs_type'] = 'rfs';

        $ponits = 0;
        $base_fee = 0;

        //$summary['building_title']['title'] = $post['building_title'];


        if ($post['property'] != null && $post['city'] != null && $post['tenure'] != null && $post['approach_type'] != null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'bcs_type', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['bcs_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                $base_fee = $result['values'];
                $summary['bcs_type']['fee'] = $result['values'];
                $summary['bcs_type']['title'] = strtoupper($post['bcs_type']);
            }
            
            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            }




            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();


            // subject property value
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type']])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'subject_property', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['property']])->one();
            if ($result != null) {
                $points += $result['values'];
                // $base_fee += $result['values'];
                $summary['property']['fee'] = $result['values'];
                $summary['property']['title'] = $property_detail->title;
            } else {
                $points += 0;
                // $base_fee += 0;
                $summary['property']['fee'] = 0;
                $summary['property']['title'] = $property_detail->title;
            }


            //height
            if ($post['typical_floors'] > 0) {
                // $typicalFloor = ($post['typical_floors'] > 0) ? " + " . $post['typical_floors'] . "F " : "";
                $typicalFloor = ($post['typical_floors'] > 1) ? " + " . ($post['typical_floors']-1) . "F " : "";
                $basementFloor = ($post['basement_floors'] > 0) ? " + " . $post['basement_floors'] . "B " : "";
                $mezzanineFloor = ($post['mezzanine_floors'] > 0) ? " + " . $post['mezzanine_floors'] . "M " : "";
                $parkingFloor = ($post['parking_floors'] > 0) ? " + " . $post['parking_floors'] . "P " : "";
                $height = "G" . $typicalFloor . $basementFloor . $mezzanineFloor . $parkingFloor;
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = $height;
            } else {
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = 'NA';
            }



            //number of units 3,7,22 buildings wali han......



            //number of units 3,7,22 buildings wali han......
            if ($model->id < 2237) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $num_res_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_residential_units']];
                $num_com_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_commercial_units']];
                $num_ret_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_retail_units']];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = 'NA';
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = 'NA';
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_residential_units'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_commercial_units'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_retail_units'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            } else {

                $num_units = $post['no_of_units_value'];
                $num_res_units = $post['no_of_res_units_value'];
                $num_com_units = $post['no_of_com_units_value'];
                $num_ret_units = $post['no_of_ret_units_value'];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1 && $post['no_of_units_value'] <> null) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_res_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_com_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_ret_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            }



            //number of type of units
            // $num_units_type = yii::$app->quotationHelperFunctions->getTypesOfUnitsArrAuto()[$post['number_of_types']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['title'] = $post['number_of_types'];
            } else {
                $points += 0;
                $summary['number_of_types']['fee'] = 0;
                $summary['number_of_types']['title'] = 'NA';
            }

            //sub_community
            // $subCommunity = \app\models\SubCommunities::find()->where(['id'=>$post['sub_community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'sub_community', 'sub_heading'=>$post['sub_community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }else{
            //     $points += 0;
            //     $summary['sub_community']['fee'] = 0;
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }

            //city value (%)
            if ($post['city'] == 3506 || $post['city'] == 4260) {
                $citykey = 'abu_dhabi';
            } else if ($post['city'] == 3510) {
                $citykey = 'dubai';
            } else if ($post['city'] == 3507) {
                $citykey = 'ajman';
            } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                $citykey = 'sharjah';
            } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                $citykey = 'rak-fuj';
            } else {
                $citykey = 'others';
            }

            if ($post['city'] == 3506) {
                $summaryCity = 'Abu Dhabi';
            }
            if ($post['city'] == 4260) {
                $summaryCity = 'Al Ain';
            }
            if ($post['city'] == 3510) {
                $summaryCity = 'Dubai';
            }
            if ($post['city'] == 3507) {
                $summaryCity = 'Ajman';
            }
            if ($post['city'] == 3509) {
                $summaryCity = 'Sharjah';
            }
            if ($post['city'] == 3512) {
                $summaryCity = 'Umm Al Quwain';
            }
            if ($post['city'] == 3511) {
                $summaryCity = 'Ras Al Khaimah';
            }
            if ($post['city'] == 3508) {
                $summaryCity = 'Fujairah';
            }

            if ($post['approach_type'] <> null) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['title'] = $summaryCity;
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $points += 0;
                        $summary['city']['fee'] = 0;
                        $summary['city']['title'] = $summaryCity;
                    }
                }
            }else {
                $points += 0;
                $summary['city']['fee'] = 0;
                $summary['city']['title'] = $summaryCity;
            }




            //land size (%)
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {
                if ($post['land_size'] >= 1 && $post['land_size'] <= 5000) {
                    $post['land_size'] = 1;
                } else if ($post['land_size'] >= 5001 && $post['land_size'] <= 10000) {
                    $post['land_size'] = 2;
                } else if ($post['land_size'] >= 10001 && $post['land_size'] <= 20000) {
                    $post['land_size'] = 3;
                } else if ($post['land_size'] > 20001 && $post['land_size'] <= 40000) {
                    $post['land_size'] = 4;
                } else if ($post['land_size'] > 40001 && $post['land_size'] <= 75000) {
                    $post['land_size'] = 5;
                } else if ($post['land_size'] >= 75001) {
                    $post['land_size'] = 6;
                }
                if ($post['land_size'] < 1) {
                    $points += 0;
                } else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['title'] = $summaryLandSize;
                    }
                }
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = $summaryLandSize;
            }







            // $summaryBUA = $post['built_up_area'];
            $summaryBUA = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            $post['built_up_area'] = $summaryBUA;

            if ($no_of_units == 0) {
                //built up area (%)
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                } else {
                    if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 10000) {
                        $post['built_up_area'] = 1;
                    } else if ($post['built_up_area'] >= 10001 && $post['built_up_area'] <= 25000) {
                        $post['built_up_area'] = 2;
                    } else if ($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000) {
                        $post['built_up_area'] = 3;
                    } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000) {
                        $post['built_up_area'] = 4;
                    } else if ($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000) {
                        $post['built_up_area'] = 5;
                    } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000) {
                        $post['built_up_area'] = 6;
                    } else if ($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000) {
                        $post['built_up_area'] = 7;
                    } else if ($post['built_up_area'] >= 1000001) {
                        $post['built_up_area'] = 8;
                    }


                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = $summaryBUA;
                    } else {



                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];

                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        //   dd($result);

                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                        }
                    }
                }
            } else {
                $points += 0;
                $summary['built_up_area']['fee'] = 0;
                $summary['built_up_area']['title'] = $summaryBUA;
            }



            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "non_freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['title'] = $tenureName;
            } else {
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = 'NA';
            }


            // complexity value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['title'] = $post['complexity'];
            } else {
                $points += 0;
                $summary['complexity']['fee'] = 0;
                $summary['complexity']['title'] = 'NA';
            }

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                } else {
                    $points += 0;
                    $summary['upgrade_value']['fee'] = 0;
                    $summary['upgrade_value']['title'] = 'NA';
                }
            }else {
                $points += 0;
                $summary['upgrade_value']['fee'] = 0;
                $summary['upgrade_value']['title'] = 'NA';
            }


            //number_of_comparables

            if ($model->id < 2237) {
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
            else{
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }


            // drawing available  (%)
            $drawingSum = 0;
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'civil', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = "C, ";
                }
            }
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'mechanical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "M, ";
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'electrical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "E, ";
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'plumbing', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "P, ";
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'hvac', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "HVAC ";
                }
            }
            if ($drawingSum > 0) {

                //$summary['drawings_not_available']['fee'] = $drawingSum;
                //$summary['drawings_not_available']['title'] = $drawingsTitle;
            } else {
                //$summary['drawings_not_available']['fee'] = 0;
                //$summary['drawings_not_available']['title'] = 'NA';
            }



            // dd($summary);

            return $summary;

            //next process

            //Payment Terms
            $advance_payment = 0;
            $sub_heading_data = explode('%', $post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $sub_heading_data[0], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                if ($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            } else {
            }



            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => $name_repeat_valuation, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }

            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_three_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

    public function getAmount_autoSCS($post, $model)
    {
        $summary = [];
        $points = 0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'bcs';
        $post['approach_type'] = 'bcs';
        $post['bcs_type'] = 'scas';

        $ponits = 0;
        $base_fee = 0;

        //$summary['building_title']['title'] = $post['building_title'];

        if ($post['property'] != null && $post['city'] != null && $post['tenure'] != null && $post['approach_type'] != null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'bcs_type', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['bcs_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                $base_fee = $result['values'];
                $summary['bcs_type']['fee'] = $result['values'];
                $summary['bcs_type']['title'] = strtoupper($post['bcs_type']);
            }
            

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            }




            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();


            // subject property value
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type']])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'subject_property', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['property']])->one();
            if ($result != null) {
                $points += $result['values'];
                // $base_fee += $result['values'];
                $summary['property']['fee'] = $result['values'];
                $summary['property']['title'] = $property_detail->title;
            } else {
                $points += 0;
                // $base_fee += 0;
                $summary['property']['fee'] = 0;
                $summary['property']['title'] = $property_detail->title;
            }


            //height
            if ($post['typical_floors'] > 0) {
                // $typicalFloor = ($post['typical_floors'] > 0) ? " + " . $post['typical_floors'] . "F " : "";
                $typicalFloor = ($post['typical_floors'] > 1) ? " + " . ($post['typical_floors']-1) . "F " : "";
                $basementFloor = ($post['basement_floors'] > 0) ? " + " . $post['basement_floors'] . "B " : "";
                $mezzanineFloor = ($post['mezzanine_floors'] > 0) ? " + " . $post['mezzanine_floors'] . "M " : "";
                $parkingFloor = ($post['parking_floors'] > 0) ? " + " . $post['parking_floors'] . "P " : "";
                $height = "G" . $typicalFloor . $basementFloor . $mezzanineFloor . $parkingFloor;
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = $height;
            } else {
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = 'NA';
            }



            //number of units 3,7,22 buildings wali han......



            //number of units 3,7,22 buildings wali han......
            if ($model->id < 2237) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $num_res_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_residential_units']];
                $num_com_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_commercial_units']];
                $num_ret_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_retail_units']];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = 'NA';
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = 'NA';
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_residential_units'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_commercial_units'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_retail_units'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            } else {

                $num_units = $post['no_of_units_value'];
                $num_res_units = $post['no_of_res_units_value'];
                $num_com_units = $post['no_of_com_units_value'];
                $num_ret_units = $post['no_of_ret_units_value'];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1 && $post['no_of_units_value'] <> null) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_res_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_com_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_ret_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            }



            //number of type of units
            // $num_units_type = yii::$app->quotationHelperFunctions->getTypesOfUnitsArrAuto()[$post['number_of_types']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['title'] = $post['number_of_types'];
            } else {
                $points += 0;
                $summary['number_of_types']['fee'] = 0;
                $summary['number_of_types']['title'] = 'NA';
            }

            //sub_community
            // $subCommunity = \app\models\SubCommunities::find()->where(['id'=>$post['sub_community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'sub_community', 'sub_heading'=>$post['sub_community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }else{
            //     $points += 0;
            //     $summary['sub_community']['fee'] = 0;
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }

            //city value (%)
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }

                if ($post['city'] == 3506) {
                    $summaryCity = 'Abu Dhabi';
                }
                if ($post['city'] == 4260) {
                    $summaryCity = 'Al Ain';
                }
                if ($post['city'] == 3510) {
                    $summaryCity = 'Dubai';
                }
                if ($post['city'] == 3507) {
                    $summaryCity = 'Ajman';
                }
                if ($post['city'] == 3509) {
                    $summaryCity = 'Sharjah';
                }
                if ($post['city'] == 3512) {
                    $summaryCity = 'Umm Al Quwain';
                }
                if ($post['city'] == 3511) {
                    $summaryCity = 'Ras Al Khaimah';
                }
                if ($post['city'] == 3508) {
                    $summaryCity = 'Fujairah';
                }
            if ($post['approach_type'] <> null) {

                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['title'] = $summaryCity;
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $points += 0;
                        $summary['city']['fee'] = 0;
                        $summary['city']['title'] = $summaryCity;
                    }
                }
            }else {
                $points += 0;
                $summary['city']['fee'] = 0;
                $summary['city']['title'] = $summaryCity;
            }




            //land size (%)
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {
                if ($post['land_size'] >= 1 && $post['land_size'] <= 5000) {
                    $post['land_size'] = 1;
                } else if ($post['land_size'] >= 5001 && $post['land_size'] <= 10000) {
                    $post['land_size'] = 2;
                } else if ($post['land_size'] >= 10001 && $post['land_size'] <= 20000) {
                    $post['land_size'] = 3;
                } else if ($post['land_size'] > 20001 && $post['land_size'] <= 40000) {
                    $post['land_size'] = 4;
                } else if ($post['land_size'] > 40001 && $post['land_size'] <= 75000) {
                    $post['land_size'] = 5;
                } else if ($post['land_size'] >= 75001) {
                    $post['land_size'] = 6;
                }
                if ($post['land_size'] < 1) {
                    $points += 0;
                } else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['title'] = $summaryLandSize;
                    }
                }
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = $summaryLandSize;
            }







            // $summaryBUA = $post['built_up_area'];
            $summaryBUA = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            $post['built_up_area'] = $summaryBUA;

            if ($no_of_units == 0) {
                //built up area (%)
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                } else {
                    if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 10000) {
                        $post['built_up_area'] = 1;
                    } else if ($post['built_up_area'] >= 10001 && $post['built_up_area'] <= 25000) {
                        $post['built_up_area'] = 2;
                    } else if ($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000) {
                        $post['built_up_area'] = 3;
                    } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000) {
                        $post['built_up_area'] = 4;
                    } else if ($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000) {
                        $post['built_up_area'] = 5;
                    } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000) {
                        $post['built_up_area'] = 6;
                    } else if ($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000) {
                        $post['built_up_area'] = 7;
                    } else if ($post['built_up_area'] >= 1000001) {
                        $post['built_up_area'] = 8;
                    }


                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = $summaryBUA;
                    } else {



                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];

                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        //   dd($result);

                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                        }
                    }
                }
            } else {
                $points += 0;
                $summary['built_up_area']['fee'] = 0;
                $summary['built_up_area']['title'] = $summaryBUA;
            }



            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "non_freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['title'] = $tenureName;
            } else {
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = 'NA';
            }


            // complexity value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['title'] = $post['complexity'];
            } else {
                $points += 0;
                $summary['complexity']['fee'] = 0;
                $summary['complexity']['title'] = 'NA';
            }

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                } else {
                    $points += 0;
                    $summary['upgrade_value']['fee'] = 0;
                    $summary['upgrade_value']['title'] = 'NA';
                }
            } else {
                $points += 0;
                $summary['upgrade_value']['fee'] = 0;
                $summary['upgrade_value']['title'] = 'NA';
            }


            //number_of_comparables
            if ($model->id < 2237) {
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
            else{
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }


            // drawing available  (%)
            $drawingSum = 0;
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'civil', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = "C, ";
                }
            }
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'mechanical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "M, ";
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'electrical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "E, ";
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'plumbing', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "P, ";
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'hvac', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "HVAC ";
                }
            }
            if ($drawingSum > 0) {

                //$summary['drawings_not_available']['fee'] = $drawingSum;
                //$summary['drawings_not_available']['title'] = $drawingsTitle;
            } else {
                //$summary['drawings_not_available']['fee'] = 0;
                //$summary['drawings_not_available']['title'] = 'NA';
            }



            // dd($summary);

            return $summary;

            //next process

            //Payment Terms
            $advance_payment = 0;
            $sub_heading_data = explode('%', $post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $sub_heading_data[0], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                if ($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            } else {
            }



            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => $name_repeat_valuation, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }

            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_three_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

    public function getAmount_autoBCA($post, $model)
    {
        $summary = [];
        $points = 0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'bcs';
        $post['approach_type'] = 'bcs';
        $post['bcs_type'] = 'bca';

        $ponits = 0;
        $base_fee = 0;

        //$summary['building_title']['title'] = $post['building_title'];

        if ($post['property'] != null && $post['city'] != null && $post['tenure'] != null && $post['approach_type'] != null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'bcs_type', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['bcs_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                $base_fee = $result['values'];
                $summary['bcs_type']['fee'] = $result['values'];
                $summary['bcs_type']['title'] = strtoupper($post['bcs_type']);
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            }




            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();


            // subject property value
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type']])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'subject_property', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['property']])->one();
            if ($result != null) {
                $points += $result['values'];
                // $base_fee += $result['values'];
                $summary['property']['fee'] = $result['values'];
                $summary['property']['title'] = $property_detail->title;
            } else {
                $points += 0;
                // $base_fee += 0;
                $summary['property']['fee'] = 0;
                $summary['property']['title'] = $property_detail->title;
            }


            //height
            if ($post['typical_floors'] > 0) {
                // $typicalFloor = ($post['typical_floors'] > 0) ? " + " . $post['typical_floors'] . "F " : "";
                $typicalFloor = ($post['typical_floors'] > 1) ? " + " . ($post['typical_floors']-1) . "F " : "";
                $basementFloor = ($post['basement_floors'] > 0) ? " + " . $post['basement_floors'] . "B " : "";
                $mezzanineFloor = ($post['mezzanine_floors'] > 0) ? " + " . $post['mezzanine_floors'] . "M " : "";
                $parkingFloor = ($post['parking_floors'] > 0) ? " + " . $post['parking_floors'] . "P " : "";
                $height = "G" . $typicalFloor . $basementFloor . $mezzanineFloor . $parkingFloor;
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = $height;
            } else {
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = 'NA';
            }



            //number of units 3,7,22 buildings wali han......



            //number of units 3,7,22 buildings wali han......
            if ($model->id < 2237) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $num_res_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_residential_units']];
                $num_com_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_commercial_units']];
                $num_ret_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_retail_units']];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = 'NA';
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = 'NA';
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_residential_units'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_commercial_units'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_retail_units'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            } else {

                $num_units = $post['no_of_units_value'];
                $num_res_units = $post['no_of_res_units_value'];
                $num_com_units = $post['no_of_com_units_value'];
                $num_ret_units = $post['no_of_ret_units_value'];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1 && $post['no_of_units_value'] <> null) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_res_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_com_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_ret_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            }

            

            //number of type of units
            // $num_units_type = yii::$app->quotationHelperFunctions->getTypesOfUnitsArrAuto()[$post['number_of_types']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['title'] = $post['number_of_types'];
            } else {
                $points += 0;
                $summary['number_of_types']['fee'] = 0;
                $summary['number_of_types']['title'] = 'NA';
            }

            //sub_community
            // $subCommunity = \app\models\SubCommunities::find()->where(['id'=>$post['sub_community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'sub_community', 'sub_heading'=>$post['sub_community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }else{
            //     $points += 0;
            //     $summary['sub_community']['fee'] = 0;
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }

            //city value (%)
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }

                if ($post['city'] == 3506) {
                    $summaryCity = 'Abu Dhabi';
                }
                if ($post['city'] == 4260) {
                    $summaryCity = 'Al Ain';
                }
                if ($post['city'] == 3510) {
                    $summaryCity = 'Dubai';
                }
                if ($post['city'] == 3507) {
                    $summaryCity = 'Ajman';
                }
                if ($post['city'] == 3509) {
                    $summaryCity = 'Sharjah';
                }
                if ($post['city'] == 3512) {
                    $summaryCity = 'Umm Al Quwain';
                }
                if ($post['city'] == 3511) {
                    $summaryCity = 'Ras Al Khaimah';
                }
                if ($post['city'] == 3508) {
                    $summaryCity = 'Fujairah';
                }

            if ($post['approach_type'] <> null) {

                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['title'] = $summaryCity;
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $points += 0;
                        $summary['city']['fee'] = 0;
                        $summary['city']['title'] = $summaryCity;
                    }
                }
            }else {
                $points += 0;
                $summary['city']['fee'] = 0;
                $summary['city']['title'] = $summaryCity;
            }




            //land size (%)
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {
                if ($post['land_size'] >= 1 && $post['land_size'] <= 5000) {
                    $post['land_size'] = 1;
                } else if ($post['land_size'] >= 5001 && $post['land_size'] <= 10000) {
                    $post['land_size'] = 2;
                } else if ($post['land_size'] >= 10001 && $post['land_size'] <= 20000) {
                    $post['land_size'] = 3;
                } else if ($post['land_size'] > 20001 && $post['land_size'] <= 40000) {
                    $post['land_size'] = 4;
                } else if ($post['land_size'] > 40001 && $post['land_size'] <= 75000) {
                    $post['land_size'] = 5;
                } else if ($post['land_size'] >= 75001) {
                    $post['land_size'] = 6;
                }
                if ($post['land_size'] < 1) {
                    $points += 0;
                } else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['title'] = $summaryLandSize;
                    }
                }
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = $summaryLandSize;
            }







            // $summaryBUA = $post['built_up_area'];
            $summaryBUA = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            $post['built_up_area'] = $summaryBUA;

            if ($no_of_units == 0) {
                //built up area (%)
                // dd($property_detail->bua_fee);
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                    $summary['built_up_area']['fee'] = 0;
                    $summary['built_up_area']['title'] = $summaryBUA;
                } else {
                    if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 10000) {
                        $post['built_up_area'] = 1;
                    } else if ($post['built_up_area'] >= 10001 && $post['built_up_area'] <= 25000) {
                        $post['built_up_area'] = 2;
                    } else if ($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000) {
                        $post['built_up_area'] = 3;
                    } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000) {
                        $post['built_up_area'] = 4;
                    } else if ($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000) {
                        $post['built_up_area'] = 5;
                    } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000) {
                        $post['built_up_area'] = 6;
                    } else if ($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000) {
                        $post['built_up_area'] = 7;
                    } else if ($post['built_up_area'] >= 1000001) {
                        $post['built_up_area'] = 8;
                    }


                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = $summaryBUA;
                    } else {



                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];

                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        //   dd($result);

                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                        }
                    }
                }
            } else {
                $points += 0;
                $summary['built_up_area']['fee'] = 0;
                $summary['built_up_area']['title'] = $summaryBUA;
            }
            
            // dd($summary);

            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "non_freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['title'] = $tenureName;
            } else {
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = 'NA';
            }


            // complexity value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['title'] = $post['complexity'];
            } else {
                $points += 0;
                $summary['complexity']['fee'] = 0;
                $summary['complexity']['title'] = 'NA';
            }

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                } else {
                    $points += 0;
                    $summary['upgrade_value']['fee'] = 0;
                    $summary['upgrade_value']['title'] = 'NA';
                }
            } else {
                $points += 0;
                $summary['upgrade_value']['fee'] = 0;
                $summary['upgrade_value']['title'] = 'NA';
            }


            //number_of_comparables
            if ($model->id < 2237) {
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
            else{
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }


            // drawing available  (%)
            $drawingSum = 0;
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'civil', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = "C, ";
                }
            }
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'mechanical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "M, ";
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'electrical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "E, ";
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'plumbing', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "P, ";
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'hvac', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "HVAC ";
                }
            }
            if ($drawingSum > 0) {

                //$summary['drawings_not_available']['fee'] = $drawingSum;
                //$summary['drawings_not_available']['title'] = $drawingsTitle;
            } else {
                //$summary['drawings_not_available']['fee'] = 0;
                //$summary['drawings_not_available']['title'] = 'NA';
            }



            // dd($summary);

            return $summary;

            //next process

            //Payment Terms
            $advance_payment = 0;
            $sub_heading_data = explode('%', $post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $sub_heading_data[0], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                if ($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            } else {
            }



            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => $name_repeat_valuation, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }

            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_three_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

    public function getAmount_autoRICA($post, $model)
    {
        $summary = [];
        $points = 0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'bcs';
        $post['approach_type'] = 'bcs';
        $post['bcs_type'] = 'rica';

        $ponits = 0;
        $base_fee = 0;

        //$summary['building_title']['title'] = $post['building_title'];

        if ($post['property'] != null && $post['city'] != null && $post['tenure'] != null && $post['approach_type'] != null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'bcs_type', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['bcs_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                $base_fee = $result['values'];
                $summary['bcs_type']['fee'] = $result['values'];
                $summary['bcs_type']['title'] = strtoupper($post['bcs_type']);
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            }




            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();


            // subject property value
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type']])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'subject_property', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['property']])->one();
            if ($result != null) {
                $points += $result['values'];
                // $base_fee += $result['values'];
                $summary['property']['fee'] = $result['values'];
                $summary['property']['title'] = $property_detail->title;
            } else {
                $points += 0;
                // $base_fee += 0;
                $summary['property']['fee'] = 0;
                $summary['property']['title'] = $property_detail->title;
            }


            //height
            if ($post['typical_floors'] > 0) {
                // $typicalFloor = ($post['typical_floors'] > 0) ? " + " . $post['typical_floors'] . "F " : "";
                $typicalFloor = ($post['typical_floors'] > 1) ? " + " . ($post['typical_floors']-1) . "F " : "";
                $basementFloor = ($post['basement_floors'] > 0) ? " + " . $post['basement_floors'] . "B " : "";
                $mezzanineFloor = ($post['mezzanine_floors'] > 0) ? " + " . $post['mezzanine_floors'] . "M " : "";
                $parkingFloor = ($post['parking_floors'] > 0) ? " + " . $post['parking_floors'] . "P " : "";
                $height = "G" . $typicalFloor . $basementFloor . $mezzanineFloor . $parkingFloor;
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = $height;
            } else {
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = 'NA';
            }



            //number of units 3,7,22 buildings wali han......



            //number of units 3,7,22 buildings wali han......
            if ($model->id < 2237) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $num_res_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_residential_units']];
                $num_com_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_commercial_units']];
                $num_ret_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_retail_units']];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = 'NA';
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = 'NA';
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_residential_units'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_commercial_units'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_retail_units'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            } else {

                $num_units = $post['no_of_units_value'];
                $num_res_units = $post['no_of_res_units_value'];
                $num_com_units = $post['no_of_com_units_value'];
                $num_ret_units = $post['no_of_ret_units_value'];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1 && $post['no_of_units_value'] <> null) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_res_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of commercial units no_of_commercial_units

                if ($post['no_of_com_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of retail units no_of_retail_units

                if ($post['no_of_ret_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            }



            //number of type of units
            // $num_units_type = yii::$app->quotationHelperFunctions->getTypesOfUnitsArrAuto()[$post['number_of_types']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['title'] = $post['number_of_types'];
            } else {
                $points += 0;
                $summary['number_of_types']['fee'] = 0;
                $summary['number_of_types']['title'] = 'NA';
            }

            //sub_community
            // $subCommunity = \app\models\SubCommunities::find()->where(['id'=>$post['sub_community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'sub_community', 'sub_heading'=>$post['sub_community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }else{
            //     $points += 0;
            //     $summary['sub_community']['fee'] = 0;
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }

            //city value (%)
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }

                if ($post['city'] == 3506) {
                    $summaryCity = 'Abu Dhabi';
                }
                if ($post['city'] == 4260) {
                    $summaryCity = 'Al Ain';
                }
                if ($post['city'] == 3510) {
                    $summaryCity = 'Dubai';
                }
                if ($post['city'] == 3507) {
                    $summaryCity = 'Ajman';
                }
                if ($post['city'] == 3509) {
                    $summaryCity = 'Sharjah';
                }
                if ($post['city'] == 3512) {
                    $summaryCity = 'Umm Al Quwain';
                }
                if ($post['city'] == 3511) {
                    $summaryCity = 'Ras Al Khaimah';
                }
                if ($post['city'] == 3508) {
                    $summaryCity = 'Fujairah';
                }

            if ($post['approach_type'] <> null) {

                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['title'] = $summaryCity;
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $points += 0;
                        $summary['city']['fee'] = 0;
                        $summary['city']['title'] = $summaryCity;
                    }
                }
            }else {
                $points += 0;
                $summary['city']['fee'] = 0;
                $summary['city']['title'] = $summaryCity;
            }




            //land size (%)
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {
                if ($post['land_size'] >= 1 && $post['land_size'] <= 5000) {
                    $post['land_size'] = 1;
                } else if ($post['land_size'] >= 5001 && $post['land_size'] <= 10000) {
                    $post['land_size'] = 2;
                } else if ($post['land_size'] >= 10001 && $post['land_size'] <= 20000) {
                    $post['land_size'] = 3;
                } else if ($post['land_size'] > 20001 && $post['land_size'] <= 40000) {
                    $post['land_size'] = 4;
                } else if ($post['land_size'] > 40001 && $post['land_size'] <= 75000) {
                    $post['land_size'] = 5;
                } else if ($post['land_size'] >= 75001) {
                    $post['land_size'] = 6;
                }
                if ($post['land_size'] < 1) {
                    $points += 0;
                } else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['title'] = $summaryLandSize;
                    }
                }
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = $summaryLandSize;
            }







            // $summaryBUA = $post['built_up_area'];
            $summaryBUA = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            $post['built_up_area'] = $summaryBUA;

            if ($no_of_units == 0) {
                //built up area (%)
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                } else {
                    if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 10000) {
                        $post['built_up_area'] = 1;
                    } else if ($post['built_up_area'] >= 10001 && $post['built_up_area'] <= 25000) {
                        $post['built_up_area'] = 2;
                    } else if ($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000) {
                        $post['built_up_area'] = 3;
                    } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000) {
                        $post['built_up_area'] = 4;
                    } else if ($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000) {
                        $post['built_up_area'] = 5;
                    } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000) {
                        $post['built_up_area'] = 6;
                    } else if ($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000) {
                        $post['built_up_area'] = 7;
                    } else if ($post['built_up_area'] >= 1000001) {
                        $post['built_up_area'] = 8;
                    }


                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = $summaryBUA;
                    } else {



                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];

                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        //   dd($result);

                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                        }
                    }
                }
            } else {
                $points += 0;
                $summary['built_up_area']['fee'] = 0;
                $summary['built_up_area']['title'] = $summaryBUA;
            }



            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "non_freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['title'] = $tenureName;
            } else {
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = 'NA';
            }


            // complexity value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['title'] = $post['complexity'];
            } else {
                $points += 0;
                $summary['complexity']['fee'] = 0;
                $summary['complexity']['title'] = 'NA';
            }

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                } else {
                    $points += 0;
                    $summary['upgrade_value']['fee'] = 0;
                    $summary['upgrade_value']['title'] = 'NA';
                }
            } else {
                $points += 0;
                $summary['upgrade_value']['fee'] = 0;
                $summary['upgrade_value']['title'] = 'NA';
            }


            //number_of_comparables
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
            } else {
                $points += 0;
                $summary['number_of_comparables']['fee'] = 0;
                $summary['number_of_comparables']['title'] = 'NA';
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }


            // drawing available  (%)
            $drawingSum = 0;
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'civil', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = "C, ";
                }
            }
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'mechanical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "M, ";
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'electrical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "E, ";
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'plumbing', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "P, ";
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'hvac', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "HVAC ";
                }
            }
            if ($drawingSum > 0) {

                //$summary['drawings_not_available']['fee'] = $drawingSum;
                //$summary['drawings_not_available']['title'] = $drawingsTitle;
            } else {
                //$summary['drawings_not_available']['fee'] = 0;
                //$summary['drawings_not_available']['title'] = 'NA';
            }



            // dd($summary);

            return $summary;

            //next process

            //Payment Terms
            $advance_payment = 0;
            $sub_heading_data = explode('%', $post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $sub_heading_data[0], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                if ($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            } else {
            }



            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => $name_repeat_valuation, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }

            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_three_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

    public function getAmount_autoTS($post, $model)
    {
        $summary = [];
        $points = 0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'bcs';
        $post['approach_type'] = 'bcs';
        $post['bcs_type'] = 'ts';

        $ponits = 0;
        $base_fee = 0;

        //$summary['building_title']['title'] = $post['building_title'];


        if ($post['property'] != null && $post['city'] != null && $post['tenure'] != null && $post['approach_type'] != null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'bcs_type', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['bcs_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                $base_fee = $result['values'];
                $summary['bcs_type']['fee'] = $result['values'];
                $summary['bcs_type']['title'] = strtoupper($post['bcs_type']);
            }
            
            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            }




            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();


            // subject property value
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type']])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'subject_property', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['property']])->one();
            if ($result != null) {
                $points += $result['values'];
                // $base_fee += $result['values'];
                $summary['property']['fee'] = $result['values'];
                $summary['property']['title'] = $property_detail->title;
            } else {
                $points += 0;
                // $base_fee += 0;
                $summary['property']['fee'] = 0;
                $summary['property']['title'] = $property_detail->title;
            }


            //height
            if ($post['typical_floors'] > 0) {
                // $typicalFloor = ($post['typical_floors'] > 0) ? " + " . $post['typical_floors'] . "F " : "";
                $typicalFloor = ($post['typical_floors'] > 1) ? " + " . ($post['typical_floors']-1) . "F " : "";
                $basementFloor = ($post['basement_floors'] > 0) ? " + " . $post['basement_floors'] . "B " : "";
                $mezzanineFloor = ($post['mezzanine_floors'] > 0) ? " + " . $post['mezzanine_floors'] . "M " : "";
                $parkingFloor = ($post['parking_floors'] > 0) ? " + " . $post['parking_floors'] . "P " : "";
                $height = "G" . $typicalFloor . $basementFloor . $mezzanineFloor . $parkingFloor;
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = $height;
            } else {
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = 'NA';
            }



            //number of units 3,7,22 buildings wali han......



            //number of units 3,7,22 buildings wali han......
            if ($model->id < 2237) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $num_res_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_residential_units']];
                $num_com_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_commercial_units']];
                $num_ret_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_retail_units']];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = 'NA';
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = 'NA';
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_residential_units'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_commercial_units'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_retail_units'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            } else {

                $num_units = $post['no_of_units_value'];
                $num_res_units = $post['no_of_res_units_value'];
                $num_com_units = $post['no_of_com_units_value'];
                $num_ret_units = $post['no_of_ret_units_value'];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1 && $post['no_of_units_value'] <> null) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_res_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_com_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_ret_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            }



            //number of type of units
            // $num_units_type = yii::$app->quotationHelperFunctions->getTypesOfUnitsArrAuto()[$post['number_of_types']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['title'] = $post['number_of_types'];
            } else {
                $points += 0;
                $summary['number_of_types']['fee'] = 0;
                $summary['number_of_types']['title'] = 'NA';
            }

            //sub_community
            // $subCommunity = \app\models\SubCommunities::find()->where(['id'=>$post['sub_community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'sub_community', 'sub_heading'=>$post['sub_community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }else{
            //     $points += 0;
            //     $summary['sub_community']['fee'] = 0;
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }

            //city value (%)
            if ($post['city'] == 3506 || $post['city'] == 4260) {
                $citykey = 'abu_dhabi';
            } else if ($post['city'] == 3510) {
                $citykey = 'dubai';
            } else if ($post['city'] == 3507) {
                $citykey = 'ajman';
            } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                $citykey = 'sharjah';
            } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                $citykey = 'rak-fuj';
            } else {
                $citykey = 'others';
            }

            if ($post['city'] == 3506) {
                $summaryCity = 'Abu Dhabi';
            }
            if ($post['city'] == 4260) {
                $summaryCity = 'Al Ain';
            }
            if ($post['city'] == 3510) {
                $summaryCity = 'Dubai';
            }
            if ($post['city'] == 3507) {
                $summaryCity = 'Ajman';
            }
            if ($post['city'] == 3509) {
                $summaryCity = 'Sharjah';
            }
            if ($post['city'] == 3512) {
                $summaryCity = 'Umm Al Quwain';
            }
            if ($post['city'] == 3511) {
                $summaryCity = 'Ras Al Khaimah';
            }
            if ($post['city'] == 3508) {
                $summaryCity = 'Fujairah';
            }

            if ($post['approach_type'] <> null) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['title'] = $summaryCity;
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $points += 0;
                        $summary['city']['fee'] = 0;
                        $summary['city']['title'] = $summaryCity;
                    }
                }
            }else {
                $points += 0;
                $summary['city']['fee'] = 0;
                $summary['city']['title'] = $summaryCity;
            }




            //land size (%)
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {
                if ($post['land_size'] >= 1 && $post['land_size'] <= 5000) {
                    $post['land_size'] = 1;
                } else if ($post['land_size'] >= 5001 && $post['land_size'] <= 10000) {
                    $post['land_size'] = 2;
                } else if ($post['land_size'] >= 10001 && $post['land_size'] <= 20000) {
                    $post['land_size'] = 3;
                } else if ($post['land_size'] > 20001 && $post['land_size'] <= 40000) {
                    $post['land_size'] = 4;
                } else if ($post['land_size'] > 40001 && $post['land_size'] <= 75000) {
                    $post['land_size'] = 5;
                } else if ($post['land_size'] >= 75001) {
                    $post['land_size'] = 6;
                }
                if ($post['land_size'] < 1) {
                    $points += 0;
                } else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['title'] = $summaryLandSize;
                    }
                }
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = $summaryLandSize;
            }







            // $summaryBUA = $post['built_up_area'];
            $summaryBUA = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            $post['built_up_area'] = $summaryBUA;

            if ($no_of_units == 0) {
                //built up area (%)
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                } else {
                    if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 10000) {
                        $post['built_up_area'] = 1;
                    } else if ($post['built_up_area'] >= 10001 && $post['built_up_area'] <= 25000) {
                        $post['built_up_area'] = 2;
                    } else if ($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000) {
                        $post['built_up_area'] = 3;
                    } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000) {
                        $post['built_up_area'] = 4;
                    } else if ($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000) {
                        $post['built_up_area'] = 5;
                    } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000) {
                        $post['built_up_area'] = 6;
                    } else if ($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000) {
                        $post['built_up_area'] = 7;
                    } else if ($post['built_up_area'] >= 1000001) {
                        $post['built_up_area'] = 8;
                    }


                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = $summaryBUA;
                    } else {



                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];

                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        //   dd($result);

                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                        }
                    }
                }
            } else {
                $points += 0;
                $summary['built_up_area']['fee'] = 0;
                $summary['built_up_area']['title'] = $summaryBUA;
            }



            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "non_freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['title'] = $tenureName;
            } else {
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = 'NA';
            }


            // complexity value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['title'] = $post['complexity'];
            } else {
                $points += 0;
                $summary['complexity']['fee'] = 0;
                $summary['complexity']['title'] = 'NA';
            }

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                } else {
                    $points += 0;
                    $summary['upgrade_value']['fee'] = 0;
                    $summary['upgrade_value']['title'] = 'NA';
                }
            }else {
                $points += 0;
                $summary['upgrade_value']['fee'] = 0;
                $summary['upgrade_value']['title'] = 'NA';
            }


            //number_of_comparables

            if ($model->id < 2237) {
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
            else{
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }


            // drawing available  (%)
            $drawingSum = 0;
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'civil', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = "C, ";
                }
            }
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'mechanical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "M, ";
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'electrical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "E, ";
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'plumbing', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "P, ";
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'hvac', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "HVAC ";
                }
            }
            if ($drawingSum > 0) {

                //$summary['drawings_not_available']['fee'] = $drawingSum;
                //$summary['drawings_not_available']['title'] = $drawingsTitle;
            } else {
                //$summary['drawings_not_available']['fee'] = 0;
                //$summary['drawings_not_available']['title'] = 'NA';
            }



            // dd($summary);

            return $summary;

            //next process

            //Payment Terms
            $advance_payment = 0;
            $sub_heading_data = explode('%', $post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $sub_heading_data[0], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                if ($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            } else {
            }



            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => $name_repeat_valuation, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }

            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_three_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

    public function getAmount_autoERR($post, $model)
    {
        $summary = [];
        $points = 0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'bcs';
        $post['approach_type'] = 'bcs';
        $post['bcs_type'] = 'err';

        $ponits = 0;
        $base_fee = 0;

        //$summary['building_title']['title'] = $post['building_title'];


        if ($post['property'] != null && $post['city'] != null && $post['tenure'] != null && $post['approach_type'] != null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'bcs_type', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['bcs_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                $base_fee = $result['values'];
                $summary['bcs_type']['fee'] = $result['values'];
                $summary['bcs_type']['title'] = strtoupper($post['bcs_type']);
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            }




            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();


            // subject property value
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type']])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'subject_property', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['property']])->one();
            if ($result != null) {
                $points += $result['values'];
                // $base_fee += $result['values'];
                $summary['property']['fee'] = $result['values'];
                $summary['property']['title'] = $property_detail->title;
            } else {
                $points += 0;
                // $base_fee += 0;
                $summary['property']['fee'] = 0;
                $summary['property']['title'] = $property_detail->title;
            }


            //height
            if ($post['typical_floors'] > 0) {
                // $typicalFloor = ($post['typical_floors'] > 0) ? " + " . $post['typical_floors'] . "F " : "";
                $typicalFloor = ($post['typical_floors'] > 1) ? " + " . ($post['typical_floors']-1) . "F " : "";
                $basementFloor = ($post['basement_floors'] > 0) ? " + " . $post['basement_floors'] . "B " : "";
                $mezzanineFloor = ($post['mezzanine_floors'] > 0) ? " + " . $post['mezzanine_floors'] . "M " : "";
                $parkingFloor = ($post['parking_floors'] > 0) ? " + " . $post['parking_floors'] . "P " : "";
                $height = "G" . $typicalFloor . $basementFloor . $mezzanineFloor . $parkingFloor;
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = $height;
            } else {
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = 'NA';
            }



            //number of units 3,7,22 buildings wali han......



            //number of units 3,7,22 buildings wali han......
            if ($model->id < 2237) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $num_res_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_residential_units']];
                $num_com_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_commercial_units']];
                $num_ret_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_retail_units']];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = 'NA';
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = 'NA';
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_residential_units'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_commercial_units'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_retail_units'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            } else {

                $num_units = $post['no_of_units_value'];
                $num_res_units = $post['no_of_res_units_value'];
                $num_com_units = $post['no_of_com_units_value'];
                $num_ret_units = $post['no_of_ret_units_value'];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1 && $post['no_of_units_value'] <> null) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_res_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_com_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_ret_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            }



            //number of type of units
            // $num_units_type = yii::$app->quotationHelperFunctions->getTypesOfUnitsArrAuto()[$post['number_of_types']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['title'] = $post['number_of_types'];
            } else {
                $points += 0;
                $summary['number_of_types']['fee'] = 0;
                $summary['number_of_types']['title'] = 'NA';
            }

            //sub_community
            // $subCommunity = \app\models\SubCommunities::find()->where(['id'=>$post['sub_community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'sub_community', 'sub_heading'=>$post['sub_community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }else{
            //     $points += 0;
            //     $summary['sub_community']['fee'] = 0;
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }

            //city value (%)
            if ($post['city'] == 3506 || $post['city'] == 4260) {
                $citykey = 'abu_dhabi';
            } else if ($post['city'] == 3510) {
                $citykey = 'dubai';
            } else if ($post['city'] == 3507) {
                $citykey = 'ajman';
            } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                $citykey = 'sharjah';
            } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                $citykey = 'rak-fuj';
            } else {
                $citykey = 'others';
            }

            if ($post['city'] == 3506) {
                $summaryCity = 'Abu Dhabi';
            }
            if ($post['city'] == 4260) {
                $summaryCity = 'Al Ain';
            }
            if ($post['city'] == 3510) {
                $summaryCity = 'Dubai';
            }
            if ($post['city'] == 3507) {
                $summaryCity = 'Ajman';
            }
            if ($post['city'] == 3509) {
                $summaryCity = 'Sharjah';
            }
            if ($post['city'] == 3512) {
                $summaryCity = 'Umm Al Quwain';
            }
            if ($post['city'] == 3511) {
                $summaryCity = 'Ras Al Khaimah';
            }
            if ($post['city'] == 3508) {
                $summaryCity = 'Fujairah';
            }

            if ($post['approach_type'] <> null) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['title'] = $summaryCity;
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $points += 0;
                        $summary['city']['fee'] = 0;
                        $summary['city']['title'] = $summaryCity;
                    }
                }
            }else {
                $points += 0;
                $summary['city']['fee'] = 0;
                $summary['city']['title'] = $summaryCity;
            }




            //land size (%)
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {
                if ($post['land_size'] >= 1 && $post['land_size'] <= 5000) {
                    $post['land_size'] = 1;
                } else if ($post['land_size'] >= 5001 && $post['land_size'] <= 10000) {
                    $post['land_size'] = 2;
                } else if ($post['land_size'] >= 10001 && $post['land_size'] <= 20000) {
                    $post['land_size'] = 3;
                } else if ($post['land_size'] > 20001 && $post['land_size'] <= 40000) {
                    $post['land_size'] = 4;
                } else if ($post['land_size'] > 40001 && $post['land_size'] <= 75000) {
                    $post['land_size'] = 5;
                } else if ($post['land_size'] >= 75001) {
                    $post['land_size'] = 6;
                }
                if ($post['land_size'] < 1) {
                    $points += 0;
                } else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['title'] = $summaryLandSize;
                    }
                }
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = $summaryLandSize;
            }







            // $summaryBUA = $post['built_up_area'];
            $summaryBUA = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            $post['built_up_area'] = $summaryBUA;

            if ($no_of_units == 0) {
                //built up area (%)
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                } else {
                    if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 10000) {
                        $post['built_up_area'] = 1;
                    } else if ($post['built_up_area'] >= 10001 && $post['built_up_area'] <= 25000) {
                        $post['built_up_area'] = 2;
                    } else if ($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000) {
                        $post['built_up_area'] = 3;
                    } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000) {
                        $post['built_up_area'] = 4;
                    } else if ($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000) {
                        $post['built_up_area'] = 5;
                    } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000) {
                        $post['built_up_area'] = 6;
                    } else if ($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000) {
                        $post['built_up_area'] = 7;
                    } else if ($post['built_up_area'] >= 1000001) {
                        $post['built_up_area'] = 8;
                    }


                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = $summaryBUA;
                    } else {



                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];

                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        //   dd($result);

                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                        }
                    }
                }
            } else {
                $points += 0;
                $summary['built_up_area']['fee'] = 0;
                $summary['built_up_area']['title'] = $summaryBUA;
            }



            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "non_freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['title'] = $tenureName;
            } else {
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = 'NA';
            }


            // complexity value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['title'] = $post['complexity'];
            } else {
                $points += 0;
                $summary['complexity']['fee'] = 0;
                $summary['complexity']['title'] = 'NA';
            }

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                } else {
                    $points += 0;
                    $summary['upgrade_value']['fee'] = 0;
                    $summary['upgrade_value']['title'] = 'NA';
                }
            }else {
                $points += 0;
                $summary['upgrade_value']['fee'] = 0;
                $summary['upgrade_value']['title'] = 'NA';
            }


            //number_of_comparables

            if ($model->id < 2237) {
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
            else{
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }


            // drawing available  (%)
            $drawingSum = 0;
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'civil', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = "C, ";
                }
            }
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'mechanical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "M, ";
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'electrical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "E, ";
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'plumbing', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "P, ";
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'hvac', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "HVAC ";
                }
            }
            if ($drawingSum > 0) {

                //$summary['drawings_not_available']['fee'] = $drawingSum;
                //$summary['drawings_not_available']['title'] = $drawingsTitle;
            } else {
                //$summary['drawings_not_available']['fee'] = 0;
                //$summary['drawings_not_available']['title'] = 'NA';
            }



            // dd($summary);

            return $summary;

            //next process

            //Payment Terms
            $advance_payment = 0;
            $sub_heading_data = explode('%', $post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $sub_heading_data[0], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                if ($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            } else {
            }



            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => $name_repeat_valuation, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }

            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_three_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

    public function getAmount_autoARCT($post, $model)
    {
        $summary = [];
        $points = 0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'bcs';
        $post['approach_type'] = 'bcs';
        $post['bcs_type'] = 'arct';

        $ponits = 0;
        $base_fee = 0;

        //$summary['building_title']['title'] = $post['building_title'];


        if ($post['property'] != null && $post['city'] != null && $post['tenure'] != null && $post['approach_type'] != null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'bcs_type', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['bcs_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                $base_fee = $result['values'];
                $summary['bcs_type']['fee'] = $result['values'];
                $summary['bcs_type']['title'] = strtoupper($post['bcs_type']);
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            }




            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();


            // subject property value
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type']])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'subject_property', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['property']])->one();
            if ($result != null) {
                $points += $result['values'];
                // $base_fee += $result['values'];
                $summary['property']['fee'] = $result['values'];
                $summary['property']['title'] = $property_detail->title;
            } else {
                $points += 0;
                // $base_fee += 0;
                $summary['property']['fee'] = 0;
                $summary['property']['title'] = $property_detail->title;
            }


            //height
            if ($post['typical_floors'] > 0) {
                // $typicalFloor = ($post['typical_floors'] > 0) ? " + " . $post['typical_floors'] . "F " : "";
                $typicalFloor = ($post['typical_floors'] > 1) ? " + " . ($post['typical_floors']-1) . "F " : "";
                $basementFloor = ($post['basement_floors'] > 0) ? " + " . $post['basement_floors'] . "B " : "";
                $mezzanineFloor = ($post['mezzanine_floors'] > 0) ? " + " . $post['mezzanine_floors'] . "M " : "";
                $parkingFloor = ($post['parking_floors'] > 0) ? " + " . $post['parking_floors'] . "P " : "";
                $height = "G" . $typicalFloor . $basementFloor . $mezzanineFloor . $parkingFloor;
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = $height;
            } else {
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = 'NA';
            }



            //number of units 3,7,22 buildings wali han......



            //number of units 3,7,22 buildings wali han......
            if ($model->id < 2237) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $num_res_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_residential_units']];
                $num_com_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_commercial_units']];
                $num_ret_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_retail_units']];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = 'NA';
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = 'NA';
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_residential_units'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_commercial_units'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_retail_units'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            } else {

                $num_units = $post['no_of_units_value'];
                $num_res_units = $post['no_of_res_units_value'];
                $num_com_units = $post['no_of_com_units_value'];
                $num_ret_units = $post['no_of_ret_units_value'];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1 && $post['no_of_units_value'] <> null) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_res_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_com_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_ret_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            }



            //number of type of units
            // $num_units_type = yii::$app->quotationHelperFunctions->getTypesOfUnitsArrAuto()[$post['number_of_types']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['title'] = $post['number_of_types'];
            } else {
                $points += 0;
                $summary['number_of_types']['fee'] = 0;
                $summary['number_of_types']['title'] = 'NA';
            }

            //sub_community
            // $subCommunity = \app\models\SubCommunities::find()->where(['id'=>$post['sub_community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'sub_community', 'sub_heading'=>$post['sub_community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }else{
            //     $points += 0;
            //     $summary['sub_community']['fee'] = 0;
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }

            //city value (%)
            if ($post['city'] == 3506 || $post['city'] == 4260) {
                $citykey = 'abu_dhabi';
            } else if ($post['city'] == 3510) {
                $citykey = 'dubai';
            } else if ($post['city'] == 3507) {
                $citykey = 'ajman';
            } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                $citykey = 'sharjah';
            } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                $citykey = 'rak-fuj';
            } else {
                $citykey = 'others';
            }

            if ($post['city'] == 3506) {
                $summaryCity = 'Abu Dhabi';
            }
            if ($post['city'] == 4260) {
                $summaryCity = 'Al Ain';
            }
            if ($post['city'] == 3510) {
                $summaryCity = 'Dubai';
            }
            if ($post['city'] == 3507) {
                $summaryCity = 'Ajman';
            }
            if ($post['city'] == 3509) {
                $summaryCity = 'Sharjah';
            }
            if ($post['city'] == 3512) {
                $summaryCity = 'Umm Al Quwain';
            }
            if ($post['city'] == 3511) {
                $summaryCity = 'Ras Al Khaimah';
            }
            if ($post['city'] == 3508) {
                $summaryCity = 'Fujairah';
            }

            if ($post['approach_type'] <> null) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['title'] = $summaryCity;
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $points += 0;
                        $summary['city']['fee'] = 0;
                        $summary['city']['title'] = $summaryCity;
                    }
                }
            }else {
                $points += 0;
                $summary['city']['fee'] = 0;
                $summary['city']['title'] = $summaryCity;
            }




            //land size (%)
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {
                if ($post['land_size'] >= 1 && $post['land_size'] <= 5000) {
                    $post['land_size'] = 1;
                } else if ($post['land_size'] >= 5001 && $post['land_size'] <= 10000) {
                    $post['land_size'] = 2;
                } else if ($post['land_size'] >= 10001 && $post['land_size'] <= 20000) {
                    $post['land_size'] = 3;
                } else if ($post['land_size'] > 20001 && $post['land_size'] <= 40000) {
                    $post['land_size'] = 4;
                } else if ($post['land_size'] > 40001 && $post['land_size'] <= 75000) {
                    $post['land_size'] = 5;
                } else if ($post['land_size'] >= 75001) {
                    $post['land_size'] = 6;
                }
                if ($post['land_size'] < 1) {
                    $points += 0;
                } else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['title'] = $summaryLandSize;
                    }
                }
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = $summaryLandSize;
            }







            // $summaryBUA = $post['built_up_area'];
            $summaryBUA = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            $post['built_up_area'] = $summaryBUA;

            if ($no_of_units == 0) {
                //built up area (%)
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                } else {
                    if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 10000) {
                        $post['built_up_area'] = 1;
                    } else if ($post['built_up_area'] >= 10001 && $post['built_up_area'] <= 25000) {
                        $post['built_up_area'] = 2;
                    } else if ($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000) {
                        $post['built_up_area'] = 3;
                    } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000) {
                        $post['built_up_area'] = 4;
                    } else if ($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000) {
                        $post['built_up_area'] = 5;
                    } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000) {
                        $post['built_up_area'] = 6;
                    } else if ($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000) {
                        $post['built_up_area'] = 7;
                    } else if ($post['built_up_area'] >= 1000001) {
                        $post['built_up_area'] = 8;
                    }


                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = $summaryBUA;
                    } else {



                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];

                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        //   dd($result);

                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                        }
                    }
                }
            } else {
                $points += 0;
                $summary['built_up_area']['fee'] = 0;
                $summary['built_up_area']['title'] = $summaryBUA;
            }



            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "non_freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['title'] = $tenureName;
            } else {
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = 'NA';
            }


            // complexity value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['title'] = $post['complexity'];
            } else {
                $points += 0;
                $summary['complexity']['fee'] = 0;
                $summary['complexity']['title'] = 'NA';
            }

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                } else {
                    $points += 0;
                    $summary['upgrade_value']['fee'] = 0;
                    $summary['upgrade_value']['title'] = 'NA';
                }
            }else {
                $points += 0;
                $summary['upgrade_value']['fee'] = 0;
                $summary['upgrade_value']['title'] = 'NA';
            }


            //number_of_comparables

            if ($model->id < 2237) {
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
            else{
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }


            // drawing available  (%)
            $drawingSum = 0;
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'civil', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = "C, ";
                }
            }
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'mechanical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "M, ";
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'electrical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "E, ";
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'plumbing', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "P, ";
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'hvac', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "HVAC ";
                }
            }
            if ($drawingSum > 0) {

                //$summary['drawings_not_available']['fee'] = $drawingSum;
                //$summary['drawings_not_available']['title'] = $drawingsTitle;
            } else {
                //$summary['drawings_not_available']['fee'] = 0;
                //$summary['drawings_not_available']['title'] = 'NA';
            }



            // dd($summary);

            return $summary;

            //next process

            //Payment Terms
            $advance_payment = 0;
            $sub_heading_data = explode('%', $post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $sub_heading_data[0], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                if ($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            } else {
            }



            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => $name_repeat_valuation, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }

            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_three_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

    public function getAmount_autoRFV($post, $model)
    {
        $summary = [];
        $points = 0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'bcs';
        $post['approach_type'] = 'bcs';
        $post['bcs_type'] = 'rfv';

        $ponits = 0;
        $base_fee = 0;

        //$summary['building_title']['title'] = $post['building_title'];


        if ($post['property'] != null && $post['city'] != null && $post['tenure'] != null && $post['approach_type'] != null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'bcs_type', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['bcs_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                $base_fee = $result['values'];
                $summary['bcs_type']['fee'] = $result['values'];
                $summary['bcs_type']['title'] = strtoupper($post['bcs_type']);
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            }




            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id' => $post['property']])->one();


            // subject property value
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type']])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'subject_property', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['property']])->one();
            if ($result != null) {
                $points += $result['values'];
                // $base_fee += $result['values'];
                $summary['property']['fee'] = $result['values'];
                $summary['property']['title'] = $property_detail->title;
            } else {
                $points += 0;
                // $base_fee += 0;
                $summary['property']['fee'] = 0;
                $summary['property']['title'] = $property_detail->title;
            }


            //height
            if ($post['typical_floors'] > 0) {
                // $typicalFloor = ($post['typical_floors'] > 0) ? " + " . $post['typical_floors'] . "F " : "";
                $typicalFloor = ($post['typical_floors'] > 1) ? " + " . ($post['typical_floors']-1) . "F " : "";
                $basementFloor = ($post['basement_floors'] > 0) ? " + " . $post['basement_floors'] . "B " : "";
                $mezzanineFloor = ($post['mezzanine_floors'] > 0) ? " + " . $post['mezzanine_floors'] . "M " : "";
                $parkingFloor = ($post['parking_floors'] > 0) ? " + " . $post['parking_floors'] . "P " : "";
                $height = "G" . $typicalFloor . $basementFloor . $mezzanineFloor . $parkingFloor;
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = $height;
            } else {
                $points += 0;
                $summary['height']['fee'] = 0;
                $summary['height']['title'] = 'NA';
            }



            //number of units 3,7,22 buildings wali han......



            //number of units 3,7,22 buildings wali han......
            if ($model->id < 2237) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $num_res_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_residential_units']];
                $num_com_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_commercial_units']];
                $num_ret_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_retail_units']];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = 'NA';
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = 'NA';
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_residential_units'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_commercial_units'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_retail_units'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            } else {

                $num_units = $post['no_of_units_value'];
                $num_res_units = $post['no_of_res_units_value'];
                $num_com_units = $post['no_of_com_units_value'];
                $num_ret_units = $post['no_of_ret_units_value'];

                $no_of_units = 0;
                if ($property_detail->no_of_units_fee == 1 && $post['no_of_units_value'] <> null) {
                    // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $no_of_units = 1;
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['no_of_units']['title'] = $num_units;
                    } else {
                        $points += 0;
                        $summary['no_of_units']['fee'] = 0;
                        $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                    }
                } else if ($post['property'] == 'villa_compound') {
                    $no_of_units = 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = $num_units;
                } else {
                    $points += 0;
                    $summary['no_of_units']['fee'] = 0;
                    $summary['no_of_units']['title'] = ($num_units <> null) ?  $num_units : "NA";
                }

                //number of residential units no_of_residential_units
                if ($post['no_of_res_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = $num_res_units;
                } else {
                    $points += 0;
                    $summary['no_of_residential_units']['fee'] = 0;
                    $summary['no_of_residential_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_com_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = $num_com_units;
                } else {
                    $points += 0;
                    $summary['no_of_commercial_units']['fee'] = 0;
                    $summary['no_of_commercial_units']['title'] = 'NA';
                }

                //number of residential units no_of_commercial_units

                if ($post['no_of_ret_units_value'] <> null) {

                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = $num_ret_units;
                } else {
                    $points += 0;
                    $summary['no_of_retail_units']['fee'] = 0;
                    $summary['no_of_retail_units']['title'] = 'NA';
                }
            }



            //number of type of units
            // $num_units_type = yii::$app->quotationHelperFunctions->getTypesOfUnitsArrAuto()[$post['number_of_types']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['number_of_types']['title'] = $post['number_of_types'];
            } else {
                $points += 0;
                $summary['number_of_types']['fee'] = 0;
                $summary['number_of_types']['title'] = 'NA';
            }

            //sub_community
            // $subCommunity = \app\models\SubCommunities::find()->where(['id'=>$post['sub_community']])->one();
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'sub_community', 'sub_heading'=>$post['sub_community'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }else{
            //     $points += 0;
            //     $summary['sub_community']['fee'] = 0;
            //     $summary['sub_community']['title'] = $subCommunity->title;
            // }

            //city value (%)
            if ($post['city'] == 3506 || $post['city'] == 4260) {
                $citykey = 'abu_dhabi';
            } else if ($post['city'] == 3510) {
                $citykey = 'dubai';
            } else if ($post['city'] == 3507) {
                $citykey = 'ajman';
            } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                $citykey = 'sharjah';
            } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                $citykey = 'rak-fuj';
            } else {
                $citykey = 'others';
            }

            if ($post['city'] == 3506) {
                $summaryCity = 'Abu Dhabi';
            }
            if ($post['city'] == 4260) {
                $summaryCity = 'Al Ain';
            }
            if ($post['city'] == 3510) {
                $summaryCity = 'Dubai';
            }
            if ($post['city'] == 3507) {
                $summaryCity = 'Ajman';
            }
            if ($post['city'] == 3509) {
                $summaryCity = 'Sharjah';
            }
            if ($post['city'] == 3512) {
                $summaryCity = 'Umm Al Quwain';
            }
            if ($post['city'] == 3511) {
                $summaryCity = 'Ras Al Khaimah';
            }
            if ($post['city'] == 3508) {
                $summaryCity = 'Fujairah';
            }

            if ($post['approach_type'] <> null) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['city']['title'] = $summaryCity;
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['city']['title'] = $summaryCity;
                    } else {
                        $points += 0;
                        $summary['city']['fee'] = 0;
                        $summary['city']['title'] = $summaryCity;
                    }
                }
            }else {
                $points += 0;
                $summary['city']['fee'] = 0;
                $summary['city']['title'] = $summaryCity;
            }




            //land size (%)
            $summaryLandSize = $post['land_size'];
            if ($property_detail->land_size_fee == 1) {
                if ($post['land_size'] >= 1 && $post['land_size'] <= 5000) {
                    $post['land_size'] = 1;
                } else if ($post['land_size'] >= 5001 && $post['land_size'] <= 10000) {
                    $post['land_size'] = 2;
                } else if ($post['land_size'] >= 10001 && $post['land_size'] <= 20000) {
                    $post['land_size'] = 3;
                } else if ($post['land_size'] > 20001 && $post['land_size'] <= 40000) {
                    $post['land_size'] = 4;
                } else if ($post['land_size'] > 40001 && $post['land_size'] <= 75000) {
                    $post['land_size'] = 5;
                } else if ($post['land_size'] >= 75001) {
                    $post['land_size'] = 6;
                }
                if ($post['land_size'] < 1) {
                    $points += 0;
                } else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['land_size']['title'] = $summaryLandSize;
                    }
                }
            } else {
                $points += 0;
                $summary['land_size']['fee'] = 0;
                $summary['land_size']['title'] = $summaryLandSize;
            }







            // $summaryBUA = $post['built_up_area'];
            $summaryBUA = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'];
            $post['built_up_area'] = $summaryBUA;

            if ($no_of_units == 0) {
                //built up area (%)
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                } else {
                    if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 10000) {
                        $post['built_up_area'] = 1;
                    } else if ($post['built_up_area'] >= 10001 && $post['built_up_area'] <= 25000) {
                        $post['built_up_area'] = 2;
                    } else if ($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000) {
                        $post['built_up_area'] = 3;
                    } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000) {
                        $post['built_up_area'] = 4;
                    } else if ($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000) {
                        $post['built_up_area'] = 5;
                    } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000) {
                        $post['built_up_area'] = 6;
                    } else if ($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000) {
                        $post['built_up_area'] = 7;
                    } else if ($post['built_up_area'] >= 1000001) {
                        $post['built_up_area'] = 8;
                    }


                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                        $summary['built_up_area']['fee'] = 0;
                        $summary['built_up_area']['title'] = $summaryBUA;
                    } else {



                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];

                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        //   dd($result);

                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                            $summary['built_up_area']['title'] = $summaryBUA;
                        } else {
                        }
                    }
                }
            } else {
                $points += 0;
                $summary['built_up_area']['fee'] = 0;
                $summary['built_up_area']['title'] = $summaryBUA;
            }



            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if ($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only') {
                $tenureName = "non_freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading' => 'tenure', 'sub_heading' => $tenureName, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['tenure']['title'] = $tenureName;
            } else {
                $points += 0;
                $summary['tenure']['fee'] = 0;
                $summary['tenure']['title'] = 'NA';
            }


            // complexity value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'complexity', 'sub_heading' => $post['complexity'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['complexity']['title'] = $post['complexity'];
            } else {
                $points += 0;
                $summary['complexity']['fee'] = 0;
                $summary['complexity']['title'] = 'NA';
            }

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'upgrades_ratings', 'sub_heading' => $post['upgrades'], 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['upgrade_value']['title'] = Yii::$app->appHelperFunctions->upgrades[$post['upgrades']];
                } else {
                    $points += 0;
                    $summary['upgrade_value']['fee'] = 0;
                    $summary['upgrade_value']['title'] = 'NA';
                }
            }else {
                $points += 0;
                $summary['upgrade_value']['fee'] = 0;
                $summary['upgrade_value']['title'] = 'NA';
            }


            //number_of_comparables

            if ($model->id < 2237) {
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
            else{
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }


            // drawing available  (%)
            $drawingSum = 0;
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'civil', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = "C, ";
                }
            }
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'mechanical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "M, ";
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'electrical', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "E, ";
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'plumbing', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "P, ";
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'drawings_available', 'sub_heading' => 'hvac', 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $drawingSum += $this->getPercentageAmount($result['values'], $base_fee);
                    $drawingsTitle = $drawingsTitle . "HVAC ";
                }
            }
            if ($drawingSum > 0) {

                //$summary['drawings_not_available']['fee'] = $drawingSum;
                //$summary['drawings_not_available']['title'] = $drawingsTitle;
            } else {
                //$summary['drawings_not_available']['fee'] = 0;
                //$summary['drawings_not_available']['title'] = 'NA';
            }



            // dd($summary);

            return $summary;

            //next process

            //Payment Terms
            $advance_payment = 0;
            $sub_heading_data = explode('%', $post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $sub_heading_data[0], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                if ($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            } else {
            }



            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => $name_repeat_valuation, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }

            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_three_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

    public function getAmount_autoPME($post, $model)
    {
        // dd($post, $model);
        $summary = [];
        $points = 0;
        // $approach_type = $post['approach_type'];
        $post['approach_type'] = 'pme';

        $ponits = 0;
        $base_fee = 0;

        //$summary['building_title']['title'] = $post['building_title'];

        if ($post['asset_category'] != null && $post['no_of_asset'] != null && $post['approach_type'] != null) {

            
            //get asset category details
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'asset_category', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['asset_category']])->one();
            if ($result != null) {
                $points += $result['values'];
                $base_fee = $result['values'];
                $summary['asset_category']['fee'] = $result['values'];
                $summary['asset_category']['title'] = strtoupper($post['asset_category']);
            }
            // dd($summary);
            
            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'client_type', 'sub_heading' => $post['clientType'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['client_type']['title'] = $model->client->client_type;
            } else {
                $points += 0;
                $summary['client_type']['fee'] = 0;
                $summary['client_type']['title'] = $model->client->client_type;
            }

            //get asset quatity details
            $no_of_asset = yii::$app->quotationHelperFunctions->getNumberRange1($post['no_of_asset']);
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'no_of_asset', 'approach_type' => $post['approach_type'], 'sub_heading' => $no_of_asset])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['no_of_asset']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);;
                $summary['no_of_asset']['title'] = strtoupper($post['no_of_asset']);
            }

            //get asset quatity details
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'asset_complexity', 'approach_type' => $post['approach_type'], 'sub_heading' => $post['asset_complexity']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['asset_complexity']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);;
                $summary['asset_complexity']['title'] = strtoupper($post['asset_complexity']);
            }



            //city value (%)
                if ($post['location'] == 3506 || $post['location'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['location'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['location'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['location'] == 3509 || $post['location'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['location'] == 3511 || $post['location'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }


                if ($post['location'] == 3506) {
                    $summaryCity = 'Abu Dhabi';
                }
                if ($post['location'] == 4260) {
                    $summaryCity = 'Al Ain';
                }
                if ($post['location'] == 3510) {
                    $summaryCity = 'Dubai';
                }
                if ($post['location'] == 3507) {
                    $summaryCity = 'Ajman';
                }
                if ($post['location'] == 3509) {
                    $summaryCity = 'Sharjah';
                }
                if ($post['location'] == 3512) {
                    $summaryCity = 'Umm Al Quwain';
                }
                if ($post['location'] == 3511) {
                    $summaryCity = 'Ras Al Khaimah';
                }
                if ($post['location'] == 3508) {
                    $summaryCity = 'Fujairah';
                }


            if ($post['approach_type'] <> null) {

                $result = QuotationFeeMasterFile::find()->where(['heading' => 'location', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['location']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['location']['title'] = $summaryCity;
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'location', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['location']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                        $summary['location']['title'] = $summaryCity;
                    } else {
                        $points += 0;
                        $summary['location']['fee'] = 0;
                        $summary['location']['title'] = $summaryCity;
                    }
                }
            }else {
                $points += 0;
                $summary['location']['fee'] = 0;
                $summary['location']['title'] = $summaryCity;
            }

            //get number of location
            $no_of_location = yii::$app->quotationHelperFunctions->getNumberRange2($post['no_of_location']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'no_of_location', 'sub_heading' => $no_of_location, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['no_of_location']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['no_of_location']['title'] = $post['no_of_location'];
            } else {
                $points += 0;
                $summary['no_of_location']['fee'] = 0;
                $summary['no_of_location']['title'] = 'NA';
            }


            //get working days
            // $working_days = yii::$app->quotationHelperFunctions->getNumberRange1($post['working_days']);
            // $result = QuotationFeeMasterFile::find()
            //     ->where(['heading' => 'working_days', 'sub_heading' => $working_days, 'approach_type' => $post['approach_type']])->one();
            // if ($result != null) {
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['working_days']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
            //     $summary['working_days']['title'] = $post['working_days'];
            // } else {
            //     $points += 0;
            //     $summary['working_days']['fee'] = 0;
            //     $summary['working_days']['title'] = 'NA';
            // }


            //get asset age
            $asset_age = yii::$app->quotationHelperFunctions->getNumberRange1($post['asset_age']);
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'asset_age', 'approach_type' => $post['approach_type'], 'sub_heading' => $asset_age])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['asset_age']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);;
                $summary['asset_age']['title'] = strtoupper($post['asset_age']);
            }



            //number_of_comparables
            if ($model->id < 2237) {
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['number_of_comparables'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }
            else{
                $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
                $result = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'number_of_comparables_data', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                    $summary['number_of_comparables']['title'] = $post['no_of_comparables_value'];
                } else {
                    $points += 0;
                    $summary['number_of_comparables']['fee'] = 0;
                    $summary['number_of_comparables']['title'] = 'NA';
                }
            }

            // dd($summary);

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'other_intended_users', 'sub_heading' => $post['other_intended_users'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['fee'] = $this->getPercentageAmount($result['values'], $base_fee);
                $summary['other_intended_users']['title'] = $post['other_intended_users'];
            } else {
                $points += 0;
                $summary['other_intended_users']['fee'] = 0;
                $summary['other_intended_users']['title'] = 'NA';
            }

            



            // dd($summary);

            return $summary;

            //next process

            //Payment Terms
            $advance_payment = 0;
            $sub_heading_data = explode('%', $post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading' => 'payment_terms', 'sub_heading' => $sub_heading_data[0], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                if ($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            } else {
            }



            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading' => 'new_repeat_valuation_data', 'sub_heading' => $name_repeat_valuation, 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            } else {
                // echo "9"; die();
                return false;
            }

            if ($post['approach_type'] == "profit") {

                if ($post['last_3_years_finance'] == 1) {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'last_three_years_finance', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();

                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'ten_years_projections', 'sub_heading' => 'no', 'approach_type' => 'profit'])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points + $baseFee;


            return [
                'tat' => $tat,
                'fee' => $fee,
            ];



        } else {
            return false;
        }
    }

}


