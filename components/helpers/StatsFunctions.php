<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use app\models\PredefinedList;

class StatsFunctions extends Component
{
  public function getPredefinedListSubOptionCount($id)
  {
    return PredefinedList::find()->where(['parent'=>$id])->count('id');
  }
}
