<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;

class FileHelperFunctions extends Component
{
	public function getMemberImagePath()
	{
		return [
			'rel'=> '',
			'abs'=> dirname(dirname(__DIR__)) . '/Yii2crm/uploads/members/',
		];
	}
	public function getDefaultPhoto($type)
	{
		if($type=='user'){
			return 'images/default/default_avatar.png';
		}else{
			return 'images/default/default.png';
		}
	}

	public function getImagePath($source,$image,$size)
	{
		$photo='';
		if($image!='' && $image!=null){
			if(filter_var($image, FILTER_VALIDATE_URL)){
				return $image;
			}
			$ext = pathinfo($image, PATHINFO_EXTENSION);
			if($source=='user'){
				$absPath=Yii::$app->params['member_uploads_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='topbar'){
  					$photo=$this->resizeAbsolute($absPath, $image, 25, 25);
  				}elseif($size=='tiny'){
  					$photo=$this->resizeAbsolute($absPath, $image, 75, 75);
  				}elseif($size=='small'){
  					$photo=$this->resizeAbsolute($absPath, $image, 160, 160);
  				}elseif($size=='medium'){
  					$photo=$this->resizeAbsolute($absPath, $image, 160, 160);
  				}elseif($size=='full'){
  					$photo=$this->resizeAbsolute($absPath, $image, 500, 500);
  				}
				}
			}
			if($source=='attachment'){
				$absPath=Yii::$app->params['attachment_uploads_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='icon'){
  					$photo=$this->resizeAbsolute($absPath, $image, 75, 75);
  				}elseif($size=='small'){
  					$photo=$this->resizeAbsolute($absPath, $image, 160, 160);
  				}
				}
			}
		}
		if($photo==''){
			$photo=$this->getDefaultPhoto($source);
		}
		return $photo;
	}

	public function resizeAbsolute($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(Yii::$app->params['cache_abs_path'] . $new_image) || (filectime($folder . $old_image) > filectime(Yii::$app->params['cache_abs_path'] . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if($width>1000 || $height>1000){
				copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
			}else{

				if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
					$image = new ImageResize($folder . $old_image);
					$image->crop($width, $height);
					$image->save(Yii::$app->params['cache_abs_path'] . $new_image);

					//Crop
					//$image = new ImageResize(Yii::$app->params['cache_abs_path'] . $new_image);
					//$image->crop($width, $height);
					//$image->save(Yii::$app->params['cache_abs_path'] . $new_image);
				} else {
					copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
				}
			}
		}
		return Yii::$app->params['cache_rel_path'] . $new_image;
	}

	public function resizeByWidth($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(Yii::$app->params['cache_abs_path'] . $new_image) || (filectime($folder . $old_image) > filectime(Yii::$app->params['cache_abs_path'] . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if($width>1000 || $height>1000){
				copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
			}else{
				if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
					$image = new ImageResize($folder . $old_image);
					$image->resizeToWidth($width);
					$image->save(Yii::$app->params['cache_abs_path'] . $new_image);
				} else {
					copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
				}
			}
		}
		return Yii::$app->params['cache_rel_path'] . $new_image;
	}

	public function resizeByHeight($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(Yii::$app->params['cache_abs_path'] . $new_image) || (filectime($folder . $old_image) > filectime(Yii::$app->params['cache_abs_path'] . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if($width>1000 || $height>1000){
				copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
			}else{
				if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
					$image = new ImageResize($folder . $old_image);
					$image->resizeToHeight($width);
					$image->save(Yii::$app->params['cache_abs_path'] . $new_image);
				} else {
					copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
				}
			}
		}
		return Yii::$app->params['cache_rel_path'] . $new_image;
	}

	public function resizeBestFit($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(Yii::$app->params['cache_abs_path'] . $new_image) || (filectime($folder . $old_image) > filectime(Yii::$app->params['cache_abs_path'] . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
				$image = new ImageResize($folder . $old_image);
				$image->resizeToBestFit($width, $height);
				$image->save(Yii::$app->params['cache_abs_path'] . $new_image);
			} else {
				copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
			}
		}
		return Yii::$app->params['cache_rel_path'] . $new_image;
	}

	public function getGenerateName()
	{
		return str_replace(array(" ","."),"",microtime());
	}

  /**
   * Generates google map static image
   */
	public function copyGoogleImage($lat,$lng)
	{
		$imagePath = 'https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=800x400&maptype=roadmap&markers=color:red%7C'.$lat.','.$lng.'&key='.Yii::$app->params['googleApiKey'].'';
		$copiedImage = Yii::$app->helperFunction->generateName().".png";

		$image = file_get_contents($imagePath);
		$fp  = fopen(Yii::$app->params['gmap_uploads_abs_path'].$copiedImage, 'w+');
		fputs($fp, $image);
		fclose($fp);
		unset($image);
		return $copiedImage;
		//copy($imagePath, Yii::$app->params['media_outlet_gallery_uploads_abs_path'].$copiedImage);
	}
}
?>
