<?php
namespace app\components\helpers;

use app\models\QuotationFeeMasterFile;
use Yii;
use yii\base\Component;
use app\models\Properties;
use app\models\ProposalMasterFile;

class PropertyCalculationHelperFunction extends Component
{

	public function getAmount($post)
	{
		// echo $post['paymentTerms']; die();
		$ponits=0;
		if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null) {
			
//client Tye
$result = $result = ProposalMasterFile::find()->where(['heading'=>'Client Type', 'sub_heading'=>$post['clientType']])->one();
			if($result !=null){
				$points += $result['values'];
// yii::info('clientType if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
			}else{
				// echo "1"; die();
				return false;
			}

//Payment Terms
$result = $result = ProposalMasterFile::find()->where(['heading'=>'Payment Terms', 'sub_heading'=>$post['paymentTerms']])->one();
			if($result !=null){
				// echo $result['values']; die();
				$points += $result['values'];
// yii::info('payment term if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
			}else{
				// echo "2"; die();
				return false;
			}



			// subject property value
			$name = Properties::find()->select(['title'])->where(['id'=>$post['property']])->one();
			$result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>$name['title']])->one();
			if($result !=null){
				$points += $result['values'];
// yii::info('subj property if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
			}else{
				$result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>'others'])->one();
				if ($result!=null) {
					$points += $result['values'];
// yii::info('subj property else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
				}else{
					// echo "3"; die();
					return false;
				}
			}

			//city value
			$result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>$post['city']])->one();
			if($result !=null){
				$points += $result['values'];
// yii::info('city if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
			}else{
				// echo "else";
				$result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>'others'])->one();
				if ($result!=null) {
					// echo $result['values']; die();
					$points += $result['values'];
// yii::info('city else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
				}else{
					// echo "4"; die();
					return false;
				}
			}

			//tenure value
			$tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
			$result = ProposalMasterFile::find()->where(['heading'=>'Tenure', 'sub_heading'=>$tenureName])->one();
			if($result !=null){
			// echo "working"; echo $result['values']; die();
				$points += $result['values'];
// yii::info('tenure if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
			}else{
				// echo "5"; die();
				$points +=0;
			}

			// complexity value
			$result = ProposalMasterFile::find()->where(['heading'=>'Complexity', 'sub_heading'=>$post['complexity']])->one();
			if($result !=null){
				$points += $result['values'];
// yii::info('complexity if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
			}else{
				// echo "6"; die();
				return false;
			}

			//Type of Valuation
			$result = ProposalMasterFile::find()
			->where(['heading'=>'Type of Valuation', 'sub_heading'=>$post['type_of_valuation']])->one();
			if ($result!=null) {
				$points += $result['values'];
// yii::info('type of val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
			}else{
				// echo "7"; die();
				return false;
			}

			//number_of_comparables
			$result = ProposalMasterFile::find()
			->where(['heading'=>'Number of Comparables Data', 'sub_heading'=>$post['number_of_comparables']])->one();
			if ($result!=null) {
				// echo $result['values']; die();
				$points += $result['values'];
// yii::info('No of comparables if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
			}else{
				// echo "8"; die();
				return false;
			}

			//new repeat_valuation
			$result = ProposalMasterFile::find()
			->where(['heading'=>'New Repeat Valuation Data', 'sub_heading'=>$post['repeat_valuation']])->one();
			if ($result!=null) {
				$points += $result['values'];
// yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
			}else{
				// echo "9"; die();
				return false;
			}




//built up area 
			if ($post['property']==3 || $post['property']==7 || $post['property']==22 || $post['property']==4 || $post['property']==5 || 
				$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29) {
				// echo "if bla bla bla bla"; die();
				$points +=0;
// yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
		}
		else{
			// echo "else bla bla bla bla"; die();
			$sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArr()[$post['built_up_area']];
			$result = ProposalMasterFile::find()
			->where(['heading'=>'Build up Area of Subject Property', 'sub_heading'=>$sub_heading])->one();
			if ($result!=null) {
				// echo $result['values']; die();
				$points += $result['values'];
// yii::info('BUA else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
			}else{
				// echo "10"; die();
				return false;
			}
		}


//land size
		if ($post['property']==4 || $post['property']==5 || 
			$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29) {
			$landSize = yii::$app->quotationHelperFunctions->getLandSizrArr()[$post['land']];
		$result = ProposalMasterFile::find()->where(['heading'=>'Land', 'sub_heading'=>$landSize])->one();
		if ($result!=null) {
			$points += $result['values'];
// yii::info('landsize if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
		}else{
			// echo "11"; die();
			return false;
		}
	}else{
		$points +=0;
// yii::info('landsize else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
	}


	//number of units 3,7,22 buildings wali han...... 
	if ($post['property']==3 || $post['property']==7 || $post['property']==22) {
		$num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
		// echo "hello"; die();
		$result = ProposalMasterFile::find()->where(['heading'=>'No Of Units Building', 'sub_heading'=>$num_units])->one();
		if ($result!=null) {
			$points += $result['values'];
// yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
		}else{
			// echo "12"; die();
			return false;
		}
	}else{
		// echo "land"; die();
	$result = ProposalMasterFile::find()->where(['heading'=>'Number of Units Land', 'sub_heading'=>$post['no_of_units']])->one();
		if ($result!=null) {
			$points += $result['values'];
// yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
		}else{
			// echo "13"; die();
			return false;
		}
	}
// elseif($post['property']==4 || $post['property']==5 || 
// 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
	// echo "land wali else if me"; die();
// }



$tat = $points;
// yii::info('TAT > = '.$points.' ','my_custom_log');

$baseFee = $this->getBaseFee($post['property']);
// yii::info('basefee > = '.$baseFee.' ','my_custom_log');
$fee = $points+$baseFee;
// yii::info('Fee > = '.$fee.' ','my_custom_log');

return [
	'tat'=>$tat,
	'fee'=>$fee,
];



}else{
	return false;
}
    }

    public function getPercentageAmount($percentage,$baseAmount){
        $results = ($percentage / 100) * $baseAmount;
        return number_format($results,2, '.', '');
    }
    public function getAmount_new_before_usama($post)
    {

        // echo $post['paymentTerms']; die();
        $ponits=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null) {



//client Tye
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Client Type', 'sub_heading'=>$post['clientType']])->one();


            if($result !=null){
                $points += $result['values'];
// yii::info('clientType if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{

                // echo "1"; die();
                return false;
            }


//Payment Terms
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Payment Terms', 'sub_heading'=>$post['paymentTerms']])->one();
            if($result !=null){
                // echo $result['values']; die();
                $points += $result['values'];
// yii::info('payment term if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "2"; die();
                return false;
            }

            // subject property value
            $name = Properties::find()->select(['title'])->where(['id'=>$post['property']])->one();
            $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>$name['title']])->one();
            if($result !=null){
                $points += $result['values'];
// yii::info('subj property if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    $points += $result['values'];
// yii::info('subj property else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "3"; die();
                    return false;
                }
            }


            //city value
            $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>$post['city']])->one();
            if($result !=null){
                $points += $result['values'];
// yii::info('city if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "else";
                $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    // echo $result['values']; die();
                    $points += $result['values'];
// yii::info('city else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "4"; die();
                    return false;
                }
            }

            //tenure value
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            $result = ProposalMasterFile::find()->where(['heading'=>'Tenure', 'sub_heading'=>$tenureName])->one();
            if($result !=null){
                // echo "working"; echo $result['values']; die();
                $points += $result['values'];
// yii::info('tenure if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "5"; die();
                $points +=0;
            }

            // complexity value
            $result = ProposalMasterFile::find()->where(['heading'=>'Complexity', 'sub_heading'=>$post['complexity']])->one();
            if($result !=null){
                $points += $result['values'];
// yii::info('complexity if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "6"; die();
                return false;
            }

           $type_of_valuation = '';
            if($post['type_of_valuation'] == 1){
                $type_of_valuation = 'drive-by';
			}else if($post['type_of_valuation'] == 2){
                $type_of_valuation = 'physical-inspection';
            }else{
                $type_of_valuation = 'desktop';
            }

            //Type of Valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Type of Valuation', 'sub_heading'=>$type_of_valuation])->one();

            if ($result!=null) {
                $points += $result['values'];
// yii::info('type of val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "7"; die();
                return false;
            }

            //number_of_comparables
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Number of Comparables Data', 'sub_heading'=>$post['number_of_comparables']])->one();
            if ($result!=null) {
                // echo $result['values']; die();
                $points += $result['values'];
// yii::info('No of comparables if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "8"; die();
                return false;
            }

            //new repeat_valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'New Repeat Valuation Data', 'sub_heading'=>$post['repeat_valuation']])->one();
            if ($result!=null) {
                $points += $result['values'];
// yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }



//built up area
            if ($post['property']==3 || $post['property']==7 || $post['property']==22 || $post['property']==4 || $post['property']==5 ||
                $post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29) {

                // echo "if bla bla bla bla"; die();
                $points +=0;
// yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }
            else{
                if($post['built_up_area'] >=1 && $post['built_up_area'] <= 4000){
                    $post['built_up_area']=1;
                }else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
                    $post['built_up_area']=2;
                }
                else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
                    $post['built_up_area']=3;
                }
                else if($post['built_up_area'] > 8001 && $post['built_up_area'] <= 10000){
                    $post['built_up_area']=4;
                }
                else if($post['built_up_area'] >= 10001) {
                    $post['built_up_area']=5;
                }
                if($post['built_up_area'] < 1){
                    $points +=0;
				}else {

                    //  die;
                    // echo "else bla bla bla bla"; die();
                    $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArr()[round($post['built_up_area'])];

                    $result = ProposalMasterFile::find()
                        ->where(['heading' => 'Build up Area of Subject Property', 'sub_heading' => $sub_heading])->one();
                    if ($result != null) {
                        // echo $result['values']; die();
                        $points += $result['values'];
// yii::info('BUA else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "10"; die();
                        return false;
                    }
                }
            }


//land size
            if ($post['property']==4 || $post['property']==5 ||
                $post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29) {

                if($post['land'] >=1 && $post['land'] <= 4000){
                    $post['land']=1;
                }else if($post['land'] >= 4001 && $post['land'] <= 6000){
                    $post['land']=2;
                }
                else if($post['land'] >= 6001 && $post['land'] <= 8000){
                    $post['land']=3;
                }
                else if($post['land'] > 8001 && $post['land'] <= 10000){
                    $post['land']=4;
                }
                else if($post['land'] >= 10001) {
                    $post['land']=5;
                }
                if($post['land'] < 1){
                    $points +=0;
                }else {


                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArr()[$post['land']];
                    $result = ProposalMasterFile::find()->where(['heading' => 'Land', 'sub_heading' => $landSize])->one();
                    if ($result != null) {
                        $points += $result['values'];
// yii::info('landsize if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "11"; die();
                        return false;
                    }
                }
            }else{
                $points +=0;
// yii::info('landsize else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }


            //number of units 3,7,22 buildings wali han......
            if ($post['property']==3 || $post['property']==7 || $post['property']==22) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                // echo "hello"; die();
                $result = ProposalMasterFile::find()->where(['heading'=>'No Of Units Building', 'sub_heading'=>$num_units])->one();
                if ($result!=null) {
                    $points += $result['values'];
// yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "12"; die();
                    return false;
                }
            }else{
                // echo "land"; die();
                $result = ProposalMasterFile::find()->where(['heading'=>'Number of Units Land', 'sub_heading'=>$post['no_of_units']])->one();
                if ($result!=null) {
                    $points += $result['values'];
// yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "13"; die();
                    return false;
                }
            }
// elseif($post['property']==4 || $post['property']==5 ||
// 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me"; die();
// }



            $tat = $points;
// yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
// yii::info('basefee > = '.$baseFee.' ','my_custom_log');
            $fee = $points+$baseFee;
// yii::info('Fee > = '.$fee.' ','my_custom_log');

            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }

    public function getAmount_new($post)
    {
        // dd($post);

        $ponits=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null) {

            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();


            //client Tye
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Client Type', 'sub_heading'=>$post['clientType']])->one();
            if($result !=null){
                $points += $result['values'];
                // yii::info('clientType if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{

                // echo "1"; die();
                return false;
            }

/*
            echo "<pre>";
            echo $points;
            print_r($result);
            die;*/
            //Payment Terms
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Payment Terms', 'sub_heading'=>$post['paymentTerms']])->one();
            if($result !=null){
                // echo $result['values']; die();
                if($post['clientType'] != 'bank') {
                    $points += $result['values'];
                }
                // yii::info('payment term if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
               //  echo "2"; die();
                return false;
            }

            // subject property value
            $name = Properties::find()->select(['title'])->where(['id'=>$post['property']])->one();
            $str = $name['title'];

          //  print_r ($name);
           $name_pro = explode(" ",$str);

           /* echo $name_pro[0];
            die;*/
            $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property'])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();

            if($result !=null){
                if (  $post['property']!=7 && $post['property']!=3 && $post['property']!=22 ) {
                    $points += $result['values'];
                }

                // yii::info('subj property if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    if (  $post['property']!=7 && $post['property']!=3 && $post['property']!=22 ) {
                        $points += $result['values'];

                    }
                    // yii::info('subj property else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "3"; die();
                    return false;
                }
            }

         /*   echo $points;
            die;*/


if($post['city'] == 3506 || $post['city'] == 4260){
    $citykey = 'Abu Dhabi';
}else if($post['city'] == 3510){
    $citykey = 'dubai';
}else if($post['city'] == 3507){

    $citykey = 'ajman';
}else if($post['city'] == 3509 || $post['city'] == 3512){
    $citykey = 'sharjah';
}else if($post['city'] == 3511 || $post['city'] == 3508){
    $citykey = 'rak-fuj';
}else{
    $citykey = 'others';
}
            //city value
            $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>$citykey])->one();

            if($result !=null){

                $points += $result['values'];

                // yii::info('city if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "else";
                $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    // echo $result['values']; die();
                    $points += $result['values'];
                    // yii::info('city else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "4"; die();
                    return false;
                }
            }

            //tenure value
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
           // echo $tenureName;
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "Non-Freehold";
            }

            $result = ProposalMasterFile::find()->where(['heading'=>'Tenure', 'sub_heading'=>$tenureName])->one();
            if($result !=null){
                // echo "working"; echo $result['values']; die();
                $points += $result['values'];
                // yii::info('tenure if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "5"; die();
                $points +=0;
            }

            //echo $post['tenure'];

            // complexity value
            $result = ProposalMasterFile::find()->where(['heading'=>'Complexity', 'sub_heading'=>$post['complexity']])->one();

            if($result !=null){
                $points += $result['values'];
               /* echo $points;
                die;*/
                // yii::info('complexity if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "6"; die();
                return false;
            }

            $type_of_valuation = '';
            if($post['type_of_valuation'] == 1){
                $type_of_valuation = 'drive-by';
            }else if($post['type_of_valuation'] == 2){
                $type_of_valuation = 'physical-inspection';
            }else{
                $type_of_valuation = 'desktop';
            }

            //Type of Valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Type of Valuation', 'sub_heading'=>$type_of_valuation])->one();

            if ($result!=null) {
                $points += $result['values'];
                // yii::info('type of val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "7"; die();
                return false;
            }


            //number_of_comparables
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Number of Comparables Data', 'sub_heading'=>$post['number_of_comparables']])->one();
            if ($result!=null) {
                // echo $result['values']; die();
                $points += $result['values'];
                // yii::info('No of comparables if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "8"; die();
                return false;
            }


            //new repeat_valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'New Repeat Valuation Data', 'sub_heading'=>$post['repeat_valuation']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }


            //built up area
            if ($property_detail->bua_fee != 1) {

                // echo "if bla bla bla bla"; die();
                $points +=0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }
            else{
                if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
                    $post['built_up_area']=1;
                }
                else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
                    $post['built_up_area']=2;
                }

                else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
                    $post['built_up_area']=3;
                }
                else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
                    $post['built_up_area']=4;
                }
                else if($post['built_up_area'] > 8001 && $post['built_up_area'] <= 10000){
                    $post['built_up_area']=5;
                }
                else if($post['built_up_area'] >= 10001) {
                    $post['built_up_area']=6;
                }



                if($post['built_up_area'] < 1){
                    $points +=0;
                }else {

                    // echo "else bla bla bla bla"; die();
                    $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArr()[round($post['built_up_area'])];

                    $result = ProposalMasterFile::find()
                        ->where(['heading' => 'Build up Area of Subject Property', 'sub_heading' => $sub_heading])->one();
                    if ($result != null) {
                        // echo $result['values']; die();
                        $points += $result['values'];
                        // yii::info('BUA else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "10"; die();
                        return false;
                    }
                }
            }



            //land size
            if ($property_detail->land_size_fee == 1) {


                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 7500){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 7501 && $post['land_size'] <= 10000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 10001 && $post['land_size'] <= 15000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 15000 && $post['land_size'] <= 25000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 25001) {
                    $post['land_size']=6;
                }


                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {

                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArr()[$post['land_size']];

                    $result = ProposalMasterFile::find()->where(['heading' => 'Land', 'sub_heading' => $landSize])->one();

                    if ($result != null) {

                        $points += $result['values'];
                        // yii::info('landsize if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "11"; die();
                        return false;
                    }
                }
            }else{
                $points +=0;
                // yii::info('landsize else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }

            //number of units 3,7,22 buildings wali han......
            if ($property_detail->no_of_units_fee == 1) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                // echo "hello"; die();
                $result = ProposalMasterFile::find()->where(['heading'=>'No Of Units Building', 'sub_heading'=>$num_units])->one();
                if ($result!=null) {
                    $points += $result['values'];

                    // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "12"; die();
                    $points += 0;
                }
            }



            if ($property_detail->upgrade_fee == 1) {
                // echo "land"; die();
                $result = ProposalMasterFile::find()->where(['heading'=>'Number of Units Land', 'sub_heading'=>$post['upgrades']])->one();

                if ($result!=null) {
                    $points += $result['values'];
                    // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "13"; die();
                    $points += 0;
                }

            }

          /*  echo $points;
            die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me"; die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }

    public function getAmount_auto($post, $model)
    {
        // dd($post);
        $old_quotation_26_02_2024 = 2465;
        $points =0;
       // $approach_type = $post['approach_type'];
        $approach_type = 'market';
        $post['approach_type']= 'market';
        
        $property_type = $post['property_type'];


        $ponits=0;
        $base_fee=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null && $post['approach_type']!=null) {
            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type'],'sub_heading'=>$post['property'] ])->one();
            } else {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'property_fee','approach_type'=>$post['approach_type'],'sub_heading'=>$property_type, 'property_type'=>$property_type ])->one();
            }


            if($result !=null){
                    $points += $result['values'];
                    $base_fee = $result['values'];
            }
            
            //client Type (%) --------------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            }
            else {
                $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'],'property_type'=>$property_type ])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'],$base_fee);
            }else{
            }

            /*echo $post['type_of_valuation'];
            die;*/
            //city value (%) --------------------------------------------------------------------
            if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }
                /*  echo $citykey;
                die;*/
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                }
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    if($model->id <  $old_quotation_26_02_2024){
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    }else {
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    }
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }

            //tenure value (%) --------------------------------------------------------------------------
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "non_freehold";
            }
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type']])->one();
            }
            else{
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }


            // complexity value (%) --------------------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            } else {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // upgrade value (%) --------------------------------------------------------------------------
            if ($property_detail->upgrade_fee == 1) {
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
                }else{
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type'], 'property_type'=>$property_type])->one();
                }
                if ($result!=null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
            }


            
            

            //land size (%) --------------------------------------------------------------------------
            if ($property_detail->land_size_fee == 1) {

                if($model->id <  $old_quotation_26_02_2024){
                    if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                        $post['land_size']=1;
                    }
                    else if($post['land_size'] >= 5001 && $post['land_size'] <= 7500){
                        $post['land_size']=2;
                    }
                    else if($post['land_size'] >= 7501 && $post['land_size'] <= 10000){
                        $post['land_size']=3;
                    }
                    else if($post['land_size'] > 10001 && $post['land_size'] <= 15000){
                        $post['land_size']=4;
                    }
                    else if($post['land_size'] > 15001 && $post['land_size'] <= 25000){
                        $post['land_size']=5;
                    }
                    else if($post['land_size'] >= 25001) {
                        $post['land_size']=6;
                    }

                }else{

                    $land_range = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'land', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();

                    if($post['land_size'] >=$land_range[0]['values'] && $post['land_size'] <= $land_range[1]['values']){
                        $post['land_size']=1;
                    }
                    else if($post['land_size'] >= $land_range[3]['values'] && $post['land_size'] <= $land_range[4]['values']){
                        $post['land_size']=2;
                    }
                    else if($post['land_size'] >= $land_range[6]['values'] && $post['land_size'] <= $land_range[7]['values']){
                        $post['land_size']=3;
                    }
                    else if($post['land_size'] > $land_range[9]['values'] && $post['land_size'] <= $land_range[10]['values']){
                        $post['land_size']=4;
                    }
                    else if($post['land_size'] > $land_range[12]['values'] && $post['land_size'] <= $land_range[13]['values']){
                        $post['land_size']=5;
                    }
                    else if($post['land_size'] >= $land_range[15]['values']) {
                        $post['land_size']=6;
                    }
                }

                    
                if($post['land_size'] < 1){
                    $points +=0;
                }
                
                else {
                    if($model->id <  $old_quotation_26_02_2024){
                        $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAuto()[$post['land_size']];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type']])->one();
                    }
                    else{
                        $landSize = yii::$app->quotationHelperFunctions->getRangeArrAuto()[round($post['land_size'])];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
                    }
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }else{
                $points +=0;
            }

            //built up area (%)
            if ($property_detail->bua_fee != 1) {
                $points +=0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else if($post['property'] == 4 || $post['property'] == 5 || $post['property']==39 || $post['property']== 49 || $post['property'] == 48 || $post['property']== 50 || $post['property']==53){

                
                    if($model->id <  $old_quotation_26_02_2024){
                        //building land built up area
                        if($post['built_up_area'] >=1 && $post['built_up_area'] <= 25000){
                            $post['built_up_area']=1;
                        }
                        else if($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 75000){
                            $post['built_up_area']=2;
                        }
                        else if($post['built_up_area'] >= 75001 && $post['built_up_area'] <= 200000){
                            $post['built_up_area']=3;
                        }
                        else if($post['built_up_area'] >= 200001 && $post['built_up_area'] <= 400000){
                            $post['built_up_area']=4;
                        } else if($post['built_up_area'] >= 400001 && $post['built_up_area'] <= 750000){
                            $post['built_up_area']=5;
                        }
                        else if($post['built_up_area'] >= 750001) {
                            $post['built_up_area']=6;
                        }
                    }
                    else{
                        $bua_range = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();


                        if($post['built_up_area'] >=$bua_range[0]['values'] && $post['built_up_area'] <= $bua_range[1]['values']){
                            $post['built_up_area']=1;
                        }
                        else if($post['built_up_area'] >= $bua_range[3]['values'] && $post['built_up_area'] <= $bua_range[4]['values']){
                            $post['built_up_area']=2;
                        }
                        else if($post['built_up_area'] >= $bua_range[6]['values'] && $post['built_up_area'] <= $bua_range[7]['values']){
                            $post['built_up_area']=3;
                        }
                        else if($post['built_up_area'] >= $bua_range[9]['values'] && $post['built_up_area'] <= $bua_range[10]['values']){
                            $post['built_up_area']=4;
                        }
                        else if($post['built_up_area'] >= $bua_range[12]['values']) {
                            $post['built_up_area']=5;
                        }
                    }
                    
                    
                    if($post['built_up_area'] < 1){
                        $points +=0;
                    }else {

                        if($model->id <  $old_quotation_26_02_2024){
                            $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaBLArrAuto()[round($post['built_up_area'])];
                            $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property_b_land', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
                        }
                        else{
                            $sub_heading = yii::$app->quotationHelperFunctions->getRangeArrAuto()[round($post['built_up_area'])];
                            $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property_b_land', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
                        }

                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                        } else {
                        }
                    }

            }
            else{

                if($model->id <  $old_quotation_26_02_2024){
                    if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
                        $post['built_up_area']=1;
                    }
                    else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
                        $post['built_up_area']=2;
                    }
                    else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
                        $post['built_up_area']=3;
                    }
                    else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
                        $post['built_up_area']=4;
                    }
                    else if($post['built_up_area'] >= 8001) {
                        $post['built_up_area']=5;
                    }
                }
                else{
                    $bua_range = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();

                    if($post['built_up_area'] >=$bua_range[0]['values'] && $post['built_up_area'] <= $bua_range[1]['values']){
                        $post['built_up_area']=1;
                    }
                    else if($post['built_up_area'] >= $bua_range[3]['values'] && $post['built_up_area'] <= $bua_range[4]['values']){
                        $post['built_up_area']=2;
                    }
                    else if($post['built_up_area'] >= $bua_range[6]['values'] && $post['built_up_area'] <= $bua_range[7]['values']){
                        $post['built_up_area']=3;
                    }
                    else if($post['built_up_area'] >= $bua_range[9]['values'] && $post['built_up_area'] <= $bua_range[10]['values']){
                        $post['built_up_area']=4;
                    }
                    else if($post['built_up_area'] >= $bua_range[12]['values']) {
                        $post['built_up_area']=5;
                    }
                }
                
                if($post['built_up_area'] < 1){
                    $points +=0;
                }else {

                    if($model->id <  $old_quotation_26_02_2024){
                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAuto()[round($post['built_up_area'])];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();

                    } else {
                        $sub_heading = yii::$app->quotationHelperFunctions->getRangeArrAuto()[round($post['built_up_area'])];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
                    }
                    
                    if ($result != null) {
                       /* echo $this->getPercentageAmount($result['values'], $base_fee);
                        die;*/
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    } else {
                    }
                }
            }
           /* if( $post['key'] == '11') {
                echo $post['built_up_area'];
                die;
            }*/


            //number_of_comparables ----------------------------------------------------------
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            }else {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
            }
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // other intedant users value (%) ------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            }else{
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            return $points;

            //next process

            //Payment Terms
            $advance_payment =0;
            $sub_heading_data = explode('%',$post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'payment_terms', 'sub_heading'=>$sub_heading_data[0],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            }else{
            }



            /*   echo $points;
               die;*/








            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'new_repeat_valuation_data', 'sub_heading'=>$name_repeat_valuation,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }








            //number of units 3,7,22 buildings wali han......

            if($post['built_up_area'] > 0 && ($approach_type == 'income' ||  $approach_type == 'profit')){

            }else {


                if ($property_detail->no_of_units_fee == 1) {
                    $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $num_units, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $result['values'];

                        // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "12"; die();
                        $points += 0;
                    }
                }
            }





            if($post['approach_type'] == "profit"){

                if ($post['last_3_years_finance']== 1) {
                    // echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_3_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

                    if ($result!=null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    }else{
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                   //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
                    if ($result!=null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    }else{
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }

    public function getAmount_autoIncome($post, $model)
    {
        // dd($post);
        $old_quotation_26_02_2024 = 2465;
        $points =0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'income';
        $post['approach_type']= 'income';

        $property_type = $post['property_type'];

        $ponits=0;
        $base_fee=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null && $post['approach_type']!=null) {
            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            // subject property value
           // $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type']])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();

            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type'],'sub_heading'=>$post['property']])->one();
            } else {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'property_fee','approach_type'=>$post['approach_type'],'sub_heading'=>$property_type, 'property_type'=>$property_type])->one();
            }

            if($result !=null){
                $points += $result['values'];
                $base_fee = $result['values'];
            }
            
            
            //client Type (%) --------------------------------------------------------------------------

            if($model->id <  $old_quotation_26_02_2024){
                $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            }else {
                $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'], 'property_type'=>$property_type ])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'],$base_fee);
            }else{
            }

            //city value (%)--------------------------------------------------------------------------
            if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                }else{
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                }
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {

                    if($model->id <  $old_quotation_26_02_2024){
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    }else {
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    }
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }

            //tenure value (%) ---------------------------------------------------------------------
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "non_freehold";
            }
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type']])->one();
            } else {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }


            // complexity value (%) ---------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            }else {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // upgrade value (%) ----------------------------------------------------------
            if ($property_detail->upgrade_fee == 1) {
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
                }else{
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
                }
                if ($result!=null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
            }


            //land size (%)------------------------------------------------------------------------
            if ($property_detail->land_size_fee == 1) {
                if($model->id <  $old_quotation_26_02_2024){
                    if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                        $post['land_size']=1;
                    }
                    else if($post['land_size'] >= 5001 && $post['land_size'] <= 10000){
                        $post['land_size']=2;
                    }
                    else if($post['land_size'] >= 10001 && $post['land_size'] <= 20000){
                        $post['land_size']=3;
                    }
                    else if($post['land_size'] > 20001 && $post['land_size'] <= 40000){
                        $post['land_size']=4;
                    }
                    else if($post['land_size'] > 40001 && $post['land_size'] <= 75000){
                        $post['land_size']=5;
                    }
                    else if($post['land_size'] >= 75001) {
                        $post['land_size']=6;
                    }
                }else{
                    $land_range = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'land', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();

                    if($post['land_size'] >=$land_range[0]['values'] && $post['land_size'] <= $land_range[1]['values']){
                        $post['land_size']=1;
                    }
                    else if($post['land_size'] >= $land_range[3]['values'] && $post['land_size'] <= $land_range[4]['values']){
                        $post['land_size']=2;
                    }
                    else if($post['land_size'] >= $land_range[6]['values'] && $post['land_size'] <= $land_range[7]['values']){
                        $post['land_size']=3;
                    }
                    else if($post['land_size'] > $land_range[9]['values'] && $post['land_size'] <= $land_range[10]['values']){
                        $post['land_size']=4;
                    }
                    else if($post['land_size'] > $land_range[12]['values'] && $post['land_size'] <= $land_range[13]['values']){
                        $post['land_size']=5;
                    }
                    else if($post['land_size'] >= $land_range[15]['values']) {
                        $post['land_size']=6;
                    }
                }
                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {
                    if($model->id <  $old_quotation_26_02_2024){
                        $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type']])->one();
                    }else{
                        $landSize = yii::$app->quotationHelperFunctions->getRangeArrAuto()[$post['land_size']];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    }
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }else{
                $points +=0;
            }


            //number of units 3,7,22 buildings wali han......


            $no_of_units = 0;
            if ($property_detail->no_of_units_fee == 1) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAutoIncome()[$post['no_of_units']];
                // echo "hello"; die();
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
                }else{
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();  
                }
                if ($result != null) {
                    $no_of_units =1;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    $points += 0;
                }
            }
            if($post['property'] == 'villa_compound'){
                $no_of_units=0;
            }

            

            //built up area (%) ------------------------------------------------------------------------
            if($no_of_units == 0) {

                $post['built_up_area'] = ($post['built_up_area'] <> null) ? $post['built_up_area'] : $post['net_leasable_area'] ;
                
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                } else {
                    if($model->id <  $old_quotation_26_02_2024){
                        if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 10000) {
                            $post['built_up_area'] = 1;
                        } else if ($post['built_up_area'] >= 10001 && $post['built_up_area'] <= 25000) {
                            $post['built_up_area'] = 2;
                        } else if ($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000) {
                            $post['built_up_area'] = 3;
                        } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000) {
                            $post['built_up_area'] = 4;
                        } else if ($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000) {
                            $post['built_up_area'] = 5;
                        } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000) {
                            $post['built_up_area'] = 6;
                        } else if ($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000) {
                            $post['built_up_area'] = 7;
                        } else if ($post['built_up_area'] >= 1000001) {
                            $post['built_up_area'] = 8;
                        }
                    } else {
                        $bua_range = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();


                        if($post['built_up_area'] >=$bua_range[0]['values'] && $post['built_up_area'] <= $bua_range[1]['values']){
                            $post['built_up_area']=1;
                        }
                        else if($post['built_up_area'] >= $bua_range[3]['values'] && $post['built_up_area'] <= $bua_range[4]['values']){
                            $post['built_up_area']=2;
                        }
                        else if($post['built_up_area'] >= $bua_range[6]['values'] && $post['built_up_area'] <= $bua_range[7]['values']){
                            $post['built_up_area']=3;
                        }
                        else if($post['built_up_area'] >= $bua_range[9]['values'] && $post['built_up_area'] <= $bua_range[10]['values']){
                            $post['built_up_area']=4;
                        }
                        else if($post['built_up_area'] >= $bua_range[12]['values']) {
                            $post['built_up_area']=5;
                        }
                    }
                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                    } else {
                        if($model->id <  $old_quotation_26_02_2024){
                            $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];
                            $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        }else{
                            $sub_heading = yii::$app->quotationHelperFunctions->getRangeArrAuto()[round($post['built_up_area'])];
                            $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                        }
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                        } else {
                        }
                    }
                }
            }

            

            //number of type of units ---------------------------------------------------------------
           // $num_units_type = yii::$app->quotationHelperFunctions->getTypesOfUnitsArrAuto()[$post['number_of_types']];
            // echo "hello"; die();
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            } else {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            

            // Types of Propety Units/Rooms

            if($model->id >=  $old_quotation_26_02_2024){
                if($post['no_of_res_units_value'] > 0){ 
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'residential', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    } else {
                        $points += 0;
                    }
                }
                if($post['no_of_com_units_value'] > 0){ 
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'commercial', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    } else {
                        $points += 0;
                    }
                }
                if($post['no_of_ret_units_value'] > 0){ 
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'retail', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    } else {
                        $points += 0;
                    }
                }
                if($post['no_of_warehouse_units_value'] > 0){ 
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'industrial', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    } else {
                            $points += 0;
                    }
                }
                            
            }
            // dd($base_fee,$result['values'],$this->getPercentageAmount($result['values'], $base_fee),$points);
            
            

            
            


            //number_of_comparables ------------------------------------------------------------------------
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            }else{
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            

            // other intedant users value (%)------------------------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            } else {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            

            return $points;

            //next process

            //Payment Terms
            $advance_payment =0;
            $sub_heading_data = explode('%',$post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'payment_terms', 'sub_heading'=>$sub_heading_data[0],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            }else{
            }



            /*   echo $points;
               die;*/








            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'new_repeat_valuation_data', 'sub_heading'=>$name_repeat_valuation,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }













            if($post['approach_type'] == "profit"){

                if ($post['last_3_years_finance']== 1) {
                    // echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_3_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

                    if ($result!=null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    }else{
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
                    if ($result!=null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    }else{
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];


        }else{
            return false;
        }
    }

    public function getAmount_autoProfit($post, $model)
    {
        // dd($post);
        $old_quotation_26_02_2024 = 2465;
        $points =0;
        // $approach_type = $post['approach_type'];
        $approach_type = 'profit';
        $post['approach_type']= 'profit';
        $property_type = $post['property_type'];

        $ponits=0;
        $base_fee=0;
        
        if ($post['property']!=null && $post['city']!=null && $post['approach_type']!=null) {
            //get property detail

            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            // subject property value
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type']])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();

            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type'],'sub_heading'=>$post['property']])->one();
            } else {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'property_fee','approach_type'=>$post['approach_type'],'sub_heading'=>$property_type, 'property_type'=>$property_type])->one();
            }

            if($result !=null){
                $points += $result['values'];
                $base_fee = $result['values'];
            }


            //client Type (%)
            if($model->id <  $old_quotation_26_02_2024){
                $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            } else {
                $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'],'property_type'=>$property_type ])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'],$base_fee);
            }else{
            }




            //city value (%)
            if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type'],'property_type'=>$property_type])->one();
                }
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    if($model->id <  $old_quotation_26_02_2024){
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    } else {
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type'],'property_type'=>$property_type])->one();
                    }
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }
            
            
           /* echo $points;
            die;*/

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                if($model->id <  $old_quotation_26_02_2024){
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
                }
                else {
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
                }
                if ($result!=null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
            }



            //land size (%)
            if ($property_detail->land_size_fee == 1) {

                if($model->id <  $old_quotation_26_02_2024){
                    if($post['land_size'] >=1 && $post['land_size'] <= 10000){
                        $post['land_size']=1;
                    }
                    else if($post['land_size'] >= 10001  && $post['land_size'] <= 25000){
                        $post['land_size']=2;
                    }
                    else if($post['land_size'] >= 25001 && $post['land_size'] <= 50000){
                        $post['land_size']=3;
                    }
                    else if($post['land_size'] > 50001 && $post['land_size'] <= 150000){
                        $post['land_size']=4;
                    }
                    else if($post['land_size'] > 150001 && $post['land_size'] <=  300000){
                        $post['land_size']=5;
                    }
                    else if($post['land_size'] > 300001 && $post['land_size'] <=  500000){
                        $post['land_size']=6;
                    }
                    else if($post['land_size'] > 500001 && $post['land_size'] <=  750000){
                        $post['land_size']=7;
                    }
                    else if($post['land_size'] >= 7500001) {
                        $post['land_size']=8;
                    }
                } else {
                    $land_range = QuotationFeeMasterFile::find()
                    ->where(['heading' => 'land', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();

                    if($post['land_size'] >=$land_range[0]['values'] && $post['land_size'] <= $land_range[1]['values']){
                        $post['land_size']=1;
                    }
                    else if($post['land_size'] >= $land_range[3]['values'] && $post['land_size'] <= $land_range[4]['values']){
                        $post['land_size']=2;
                    }
                    else if($post['land_size'] >= $land_range[6]['values'] && $post['land_size'] <= $land_range[7]['values']){
                        $post['land_size']=3;
                    }
                    else if($post['land_size'] > $land_range[9]['values'] && $post['land_size'] <= $land_range[10]['values']){
                        $post['land_size']=4;
                    }
                    else if($post['land_size'] > $land_range[12]['values'] && $post['land_size'] <= $land_range[13]['values']){
                        $post['land_size']=5;
                    }
                    else if($post['land_size'] >= $land_range[15]['values']) {
                        $post['land_size']=6;
                    }
                }
                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {
                    if($model->id <  $old_quotation_26_02_2024){
                        $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoProfit()[$post['land_size']];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type']])->one();
                    } else {
                        $landSize = yii::$app->quotationHelperFunctions->getRangeArrAuto()[$post['land_size']];
                        $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    }
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }else{
                $points +=0;
            }

            

            //number of units 3,7,22 buildings wali han......


            $no_of_units = 0;

            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_rooms_building', 'sub_heading' => $post['no_of_rooms'], 'approach_type' => $post['approach_type']])->one();
            } else {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_rooms_building', 'sub_heading' => $post['no_of_rooms'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }

            if ($result != null) {
                $no_of_units =1;
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }
            

            
            if($no_of_units == 0) {

                //built up area (%)
                if ($property_detail->bua_fee != 1) {
                    $points += 0;
                } else {
                    if($model->id <  $old_quotation_26_02_2024){
                        if ($post['built_up_area'] >= 1 && $post['built_up_area'] <= 50000) {
                            $post['built_up_area'] = 1;
                        } else if ($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 125000) {
                            $post['built_up_area'] = 2;
                        } else if ($post['built_up_area'] >= 125001 && $post['built_up_area'] <= 250000) {
                            $post['built_up_area'] = 3;
                        } else if ($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 750000) {
                            $post['built_up_area'] = 4;
                        } else if ($post['built_up_area'] >= 750001 && $post['built_up_area'] <= 1500000) {
                            $post['built_up_area'] = 5;
                        } else if ($post['built_up_area'] >= 1500001 && $post['built_up_area'] <= 2500000) {
                            $post['built_up_area'] = 6;
                        } else if ($post['built_up_area'] >= 2500001 && $post['built_up_area'] <= 3750000) {
                            $post['built_up_area'] = 7;
                        } else if ($post['built_up_area'] >= 3750001) {
                            $post['built_up_area'] = 8;
                        }
                    } else {
                        $bua_range = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->all();


                        if($post['built_up_area'] >=$bua_range[0]['values'] && $post['built_up_area'] <= $bua_range[1]['values']){
                            $post['built_up_area']=1;
                        }
                        else if($post['built_up_area'] >= $bua_range[3]['values'] && $post['built_up_area'] <= $bua_range[4]['values']){
                            $post['built_up_area']=2;
                        }
                        else if($post['built_up_area'] >= $bua_range[6]['values'] && $post['built_up_area'] <= $bua_range[7]['values']){
                            $post['built_up_area']=3;
                        }
                        else if($post['built_up_area'] >= $bua_range[9]['values'] && $post['built_up_area'] <= $bua_range[10]['values']){
                            $post['built_up_area']=4;
                        }
                        else if($post['built_up_area'] >= $bua_range[12]['values']) {
                            $post['built_up_area']=5;
                        }
                    }
                    if ($post['built_up_area'] < 1) {
                        $points += 0;
                    } else {
                        if($model->id <  $old_quotation_26_02_2024){
                            $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoProfit()[round($post['built_up_area'])];
                            $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type']])->one();
                        } else {
                            $sub_heading = yii::$app->quotationHelperFunctions->getRangeArrAuto()[round($post['built_up_area'])];
                            $result = QuotationFeeMasterFile::find()->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading, 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                        }
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                        } else {
                        }
                    }
                }
            }

            

            //number of type of units

            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type']])->one();
            } else {
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_types', 'sub_heading' => $post['number_of_types'], 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }


            // Types of Propety Units/Rooms
            if($model->id >=  $old_quotation_26_02_2024){
                
                if($post['no_of_res_units_value'] > 0){ 
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'residential', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    } else {
                        $points += 0;
                    }
                }
                if($post['no_of_com_units_value'] > 0){ 
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'commercial', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    } else {
                        $points += 0;
                    }
                }
                if($post['no_of_ret_units_value'] > 0){ 
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'retail', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    } else {
                        $points += 0;
                    }
                }
                if($post['no_of_warehouse_units_value'] > 0){ 
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'industrial', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    } else {
                            $points += 0;
                    }
                }
                // if($post['no_of_indus_units_value'] > 0){ 
                //     $result = QuotationFeeMasterFile::find()->where(['heading' => 'types_of_property_unit', 'sub_heading' => 'industrial', 'approach_type' => $post['approach_type'], 'property_type'=>$property_type])->one();
                    
                //     if ($result != null) {
                //         $points += $this->getPercentageAmount($result['values'], $base_fee);
                //     } else {
                //         $points += 0;
                //     }
                // }
               
            }
            
            

            //number of type of resturants
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'restaurant', 'sub_heading' => $post['restaurant'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            

            //number of type of ballrooms
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'ballrooms', 'sub_heading' => $post['ballrooms'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }


            //number of type of atms
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'atms', 'sub_heading' => $post['atms'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            //number of type of retails_units
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'retails_units', 'sub_heading' => $post['retails_units'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            //number of type of night_clubs
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'night_clubs', 'sub_heading' => $post['night_clubs'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            //number of type of bars
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'bars', 'sub_heading' => $post['bars'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            //number of type of health_club
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'health_club', 'sub_heading' => $post['health_club'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            //number of type of meeting_rooms
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'meeting_rooms', 'sub_heading' => $post['meeting_rooms'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            //number of type of spa
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'spa', 'sub_heading' => $post['spa'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            //number of type of beach_access
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'beach_access', 'sub_heading' => $post['beach_access'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            //number of type of parking_sale
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'parking_sale', 'sub_heading' => $post['parking_sale'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }
            

            //tenure value (%) --------------------------------------------------------------------------
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "non_freehold";
            }
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type']])->one();
            }
            else{
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
                $points += 0;
            }


            // complexity value (%) ---------------------------------------------------------
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            }else {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type'],'property_type'=>$property_type])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            

            //number_of_comparables
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            } else {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
                $points += 0;
            }
           
            

            // other intedant users value (%)
            if($model->id <  $old_quotation_26_02_2024){
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            }
            else {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type'], 'property_type'=>$property_type])->one();
            }
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
                $points += 0;
            }   
            

            

            

            // last_3_years_finance value (%)
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_three_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
                

            // ten_years_projections value (%)
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }

                
               

            return $points;














            if($post['approach_type'] == "profit"){

                if ($post['last_3_years_finance']== 1) {
                    // echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_3_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

                    if ($result!=null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    }else{
                        // echo "13"; die();
                        $points += 0;
                    }

                }
                if ($post['projections_10_years'] == 1) {
                    //  echo "land"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
                    if ($result!=null) {
                        $points += $result['values'];
                        // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    }else{
                        // echo "13"; die();
                        $points += 0;
                    }

                }
            }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }





    public function getAmount_new_938($post)
    {
        // dd($post);

        $ponits=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null) {
            $property_detail = Properties::find($post['property'])->one();


            //client Tye
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Client Type', 'sub_heading'=>$post['clientType']])->one();
            if($result !=null){
                $points += $result['values'];
                // yii::info('clientType if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{

                // echo "1"; die();
                return false;
            }

            /*
                        echo "<pre>";
                        echo $points;
                        print_r($result);
                        die;*/
            //Payment Terms
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Payment Terms', 'sub_heading'=>$post['paymentTerms']])->one();
            if($result !=null){
                // echo $result['values']; die();
                if($post['clientType'] != 'bank') {
                    $points += $result['values'];
                }
                // yii::info('payment term if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                //  echo "2"; die();
                return false;
            }

            // subject property value
            $name = Properties::find()->select(['title'])->where(['id'=>$post['property']])->one();
            $str = $name['title'];

            //  print_r ($name);
            $name_pro = explode(" ",$str);


            $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property'])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();

            if($result !=null){
                if((trim($name_pro[0])) == 'Land' &&  $post['property']!=20 &&  $post['property']!=26){
                    $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>'others'])->one();
                    if ($result!=null) {
                        if (  $post['property']!=7 && $post['property']!=3 && $post['property']!=22 ) {
                            $points += $result['values'];
                        }

                        // yii::info('subj property else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    }
                }else {
                    $points += $result['values'];
                }

                // yii::info('subj property if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    if (  $post['property']!=7 && $post['property']!=3 && $post['property']!=22 ) {
                        $points += $result['values'];
                    }

                    // yii::info('subj property else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "3"; die();
                    return false;
                }
            }



            if($post['city'] == 3506 || $post['city'] == 4260){
                $citykey = 'Abu Dhabi';
            }else if($post['city'] == 3510){
                $citykey = 'dubai';
            }else if($post['city'] == 3507){

                $citykey = 'ajman';
            }else if($post['city'] == 3509 || $post['city'] == 3512){
                $citykey = 'sharjah';
            }else if($post['city'] == 3511 || $post['city'] == 3508){
                $citykey = 'rak-fuj';
            }else{
                $citykey = 'others';
            }
            //city value
            $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>$citykey])->one();

            if($result !=null){

                $points += $result['values'];

                // yii::info('city if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "else";
                $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    // echo $result['values']; die();
                    $points += $result['values'];
                    // yii::info('city else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "4"; die();
                    return false;
                }
            }

            //tenure value
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            // echo $tenureName;
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "Non-Freehold";
            }

            $result = ProposalMasterFile::find()->where(['heading'=>'Tenure', 'sub_heading'=>$tenureName])->one();
            if($result !=null){
                // echo "working"; echo $result['values']; die();
                $points += $result['values'];
                // yii::info('tenure if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "5"; die();
                $points +=0;
            }

            //echo $post['tenure'];

            // complexity value
            $result = ProposalMasterFile::find()->where(['heading'=>'Complexity', 'sub_heading'=>$post['complexity']])->one();

            if($result !=null){
                $points += $result['values'];
                /* echo $points;
                 die;*/
                // yii::info('complexity if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "6"; die();
                return false;
            }

            $type_of_valuation = '';
            if($post['type_of_valuation'] == 1){
                $type_of_valuation = 'drive-by';
            }else if($post['type_of_valuation'] == 2){
                $type_of_valuation = 'physical-inspection';
            }else{
                $type_of_valuation = 'desktop';
            }

            //Type of Valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Type of Valuation', 'sub_heading'=>$type_of_valuation])->one();

            if ($result!=null) {
                $points += $result['values'];
                // yii::info('type of val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "7"; die();
                return false;
            }


            //number_of_comparables
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Number of Comparables Data', 'sub_heading'=>$post['number_of_comparables']])->one();
            if ($result!=null) {
                // echo $result['values']; die();
                $points += $result['values'];
                // yii::info('No of comparables if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "8"; die();
                return false;
            }


            //new repeat_valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'New Repeat Valuation Data', 'sub_heading'=>$post['repeat_valuation']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }


            //built up area
            if (  $post['property']==22 || $post['property']==4 || $post['property']==5 ||
                $post['property']==23 || $post['property']==26 || $post['property']==29) {

                // echo "if bla bla bla bla"; die();
                $points +=0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }
            else{
                if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
                    $post['built_up_area']=1;
                }
                else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
                    $post['built_up_area']=2;
                }

                else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
                    $post['built_up_area']=3;
                }
                else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
                    $post['built_up_area']=4;
                }
                else if($post['built_up_area'] > 8001 && $post['built_up_area'] <= 10000){
                    $post['built_up_area']=5;
                }
                else if($post['built_up_area'] >= 10001) {
                    $post['built_up_area']=6;
                }



                if($post['built_up_area'] < 1){
                    $points +=0;
                }else {

                    // echo "else bla bla bla bla"; die();
                    $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArr()[round($post['built_up_area'])];

                    $result = ProposalMasterFile::find()
                        ->where(['heading' => 'Build up Area of Subject Property', 'sub_heading' => $sub_heading])->one();
                    if ($result != null) {
                        // echo $result['values']; die();
                        $points += $result['values'];
                        // yii::info('BUA else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "10"; die();
                        return false;
                    }
                }
            }



            //land size
            if ($post['property']==4 || $post['property']==5 ||
                $post['property']==11 || $post['property']==23 ||
                $post['property']==26 || $post['property']==29 || $post['property']==24) {

                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 7500){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 7501 && $post['land_size'] <= 10000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 10001 && $post['land_size'] <= 15000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 15001 && $post['land_size'] <= 25000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 25001) {
                    $post['land_size']=6;
                }


                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {

                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArr()[$post['land_size']];

                    $result = ProposalMasterFile::find()->where(['heading' => 'Land', 'sub_heading' => $landSize])->one();

                    if ($result != null) {

                        $points += $result['values'];
                        // yii::info('landsize if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "11"; die();
                        return false;
                    }
                }
            }else{
                $points +=0;
                // yii::info('landsize else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }

            //number of units 3,7,22 buildings wali han......
            if ($post['property']==3 || $post['property']==7 || $post['property']==22 || $post['property']==24) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                // echo "hello"; die();
                $result = ProposalMasterFile::find()->where(['heading'=>'No Of Units Building', 'sub_heading'=>$num_units])->one();
                if ($result!=null) {
                    $points += $result['values'];

                    // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "12"; die();
                    return false;
                }
            }else{
                // echo "land"; die();
                $result = ProposalMasterFile::find()->where(['heading'=>'Number of Units Land', 'sub_heading'=>$post['no_of_units']])->one();

                if ($result!=null) {
                    $points += $result['values'];
                    // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "13"; die();
                    return false;
                }

            }



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me"; die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }


    public function getAmount_new_latest($post)
    {

        // echo $post['paymentTerms']; die();
        $ponits=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null) {



//client Tye
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Client Type', 'sub_heading'=>$post['clientType']])->one();


            if($result !=null){
                $points += $result['values'];
// yii::info('clientType if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{

                // echo "1"; die();
                return false;
            }


//Payment Terms
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Payment Terms', 'sub_heading'=>$post['paymentTerms']])->one();
            if($result !=null){
                // echo $result['values']; die();
                $points += $result['values'];
// yii::info('payment term if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "2"; die();
                return false;
            }

            // subject property value
            $name = Properties::find()->select(['title'])->where(['id'=>$post['property']])->one();
            $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>$name['title']])->one();
            if($result !=null){
                $points += $result['values'];
// yii::info('subj property if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>'others'])->one();
                if ($result!=null) {

                    if (  $post['property']!=7 && $post['property']!=3 && $post['property']!=22 ) {

                        $points += $result['values'];
                    }
// yii::info('subj property else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "3"; die();
                    return false;
                }
            }


            //city value
            $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>$post['city']])->one();
            if($result !=null){
                $points += $result['values'];
// yii::info('city if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "else";
                $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    // echo $result['values']; die();
                    $points += $result['values'];
// yii::info('city else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "4"; die();
                    return false;
                }
            }

            //tenure value
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            $result = ProposalMasterFile::find()->where(['heading'=>'Tenure', 'sub_heading'=>$tenureName])->one();
            if($result !=null){
                // echo "working"; echo $result['values']; die();
                $points += $result['values'];
// yii::info('tenure if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "5"; die();
                $points +=0;
            }

            // complexity value
            $result = ProposalMasterFile::find()->where(['heading'=>'Complexity', 'sub_heading'=>$post['complexity']])->one();
            if($result !=null){
                $points += $result['values'];
// yii::info('complexity if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "6"; die();
                return false;
            }

            $type_of_valuation = '';
            if($post['type_of_valuation'] == 1){
                $type_of_valuation = 'drive-by';
            }else if($post['type_of_valuation'] == 2){
                $type_of_valuation = 'physical-inspection';
            }else{
                $type_of_valuation = 'desktop';
            }

            //Type of Valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Type of Valuation', 'sub_heading'=>$type_of_valuation])->one();

            if ($result!=null) {
                $points += $result['values'];
// yii::info('type of val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "7"; die();
                return false;
            }

            //number_of_comparables
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Number of Comparables Data', 'sub_heading'=>$post['number_of_comparables']])->one();
            if ($result!=null) {
                // echo $result['values']; die();
                $points += $result['values'];
// yii::info('No of comparables if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "8"; die();
                return false;
            }

            //new repeat_valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'New Repeat Valuation Data', 'sub_heading'=>$post['repeat_valuation']])->one();
            if ($result!=null) {
                $points += $result['values'];
// yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }



//built up area
            if ($post['property']==3 || $post['property']==7 || $post['property']==22 || $post['property']==4 || $post['property']==5 ||
                $post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29) {

                // echo "if bla bla bla bla"; die();
                $points +=0;
// yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }
            else{
                if($post['built_up_area'] >=1 && $post['built_up_area'] <= 4000){
                    $post['built_up_area']=1;
                }else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
                    $post['built_up_area']=2;
                }
                else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
                    $post['built_up_area']=3;
                }
                else if($post['built_up_area'] > 8001 && $post['built_up_area'] <= 10000){
                    $post['built_up_area']=4;
                }
                else if($post['built_up_area'] >= 10001) {
                    $post['built_up_area']=5;
                }
                if($post['built_up_area'] < 1){
                    $points +=0;
                }else {

                    //  die;
                    // echo "else bla bla bla bla"; die();
                    $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArr()[round($post['built_up_area'])];

                    $result = ProposalMasterFile::find()
                        ->where(['heading' => 'Build up Area of Subject Property', 'sub_heading' => $sub_heading])->one();
                    if ($result != null) {
                        // echo $result['values']; die();
                        $points += $result['values'];
// yii::info('BUA else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "10"; die();
                        return false;
                    }
                }
            }


//land size echo "<pre>";

            if ($post['property']==4 || $post['property']==5 ||
                $post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29 || $post['property']==24) {

                if($post['land'] >=1 && $post['land'] <= 4000){
                    $post['land']=1;
                }else if($post['land'] >= 4001 && $post['land'] <= 6000){
                    $post['land']=2;
                }
                else if($post['land'] >= 6001 && $post['land'] <= 8000){
                    $post['land']=3;
                }
                else if($post['land'] > 8001 && $post['land'] <= 10000){
                    $post['land']=4;
                }
                else if($post['land'] >= 10001) {
                    $post['land']=5;
                }
                if($post['land'] < 1){
                    $points +=0;
                }else {


                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArr()[$post['land']];
                    $result = ProposalMasterFile::find()->where(['heading' => 'Land', 'sub_heading' => $landSize])->one();

                    if ($result != null) {
                        $points += $result['values'];
// yii::info('landsize if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "11"; die();
                        return false;
                    }
                }
            }else{
                $points +=0;
// yii::info('landsize else condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }


            //number of units 3,7,22 buildings wali han......
            if ($post['property']==3 || $post['property']==7 || $post['property']==22) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                // echo "hello"; die();
                $result = ProposalMasterFile::find()->where(['heading'=>'No Of Units Building', 'sub_heading'=>$num_units])->one();
                if ($result!=null) {
                    $points += $result['values'];
// yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "12"; die();
                    return false;
                }
            }else{
                // echo "land"; die();
                $result = ProposalMasterFile::find()->where(['heading'=>'Number of Units Land', 'sub_heading'=>$post['no_of_units']])->one();
                if ($result!=null) {
                    $points += $result['values'];
// yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                }else{
                    // echo "13"; die();
                    return false;
                }
            }
// elseif($post['property']==4 || $post['property']==5 ||
// 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me"; die();
// }



            $tat = $points;
// yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
// yii::info('basefee > = '.$baseFee.' ','my_custom_log');
            $fee = $points+$baseFee;
// yii::info('Fee > = '.$fee.' ','my_custom_log');

            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }







    public function getBaseFee($property_id)
{
	if ($property_id!=null) {
        if ($property_id==3 || $property_id==7 || $property_id==22 || $property_id==42 || $property_id==43 || $property_id==9 || $property_id==18) {
			$baseFee = ProposalMasterFile::find()
			->where(['heading'=>'Base Fee Building', 'sub_heading'=>'base-fee-building'])->one();
			if ($baseFee!=null) {
				return $baseFee['values'];
			}else{
				return false;
			}

		}else{
			$baseFee = ProposalMasterFile::find()
			->where(['heading'=>'Base Fee Others', 'sub_heading'=>'base-fee-others'])->one();
			if ($baseFee!=null) {
				return $baseFee['values']; 
			}else{
				return false;
			}

		}
	}else{
		return false;
	}

}

    public function getQuotationSummary($post=null)
    {

        $summary = [];
        $points = 0;
        $summary['building_title'] = $post['building_title'];

        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null) {

            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            //client Type
            $result = ProposalMasterFile::find()->where(['heading'=>'Client Type', 'sub_heading'=>$post['clientType']])->one();
            if($result !=null){
                $points += $result['values'];
                $summary['client_type'] = $result['values'];
            }else{
                return false;
            }

            //Payment Terms
            $result = ProposalMasterFile::find()->where(['heading'=>'Payment Terms', 'sub_heading'=>$post['paymentTerms']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $points += $result['values'];
                    $summary['payment_terms']= $result['values'];
                }
            }else{
                return false;
            }


            // subject property value
            $name = Properties::find()->select(['title'])->where(['id'=>$post['property']])->one();
            $str = $name['title'];
            $name_pro = explode(" ",$str);
            $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property'])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();

            if($result !=null){
                if (  $post['property']!=7 && $post['property']!=3 && $post['property']!=22 ) {
                    $points += $result['values'];
                    $summary['property']= $result['values'];
                }
            }else{
                $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    if (  $post['property']!=7 && $post['property']!=3 && $post['property']!=22 ) {
                        $points += $result['values'];
                        $summary['property']= $result['values'];
                    }
                }else{
                    return false;
                }
            }




            if($post['city'] == 3506 || $post['city'] == 4260){
                $citykey = 'Abu Dhabi';
            }else if($post['city'] == 3510){
                $citykey = 'dubai';
            }else if($post['city'] == 3507){
                $citykey = 'ajman';
            }else if($post['city'] == 3509 || $post['city'] == 3512){
                $citykey = 'sharjah';
            }else if($post['city'] == 3511 || $post['city'] == 3508){
                $citykey = 'rak-fuj';
            }else{
                $citykey = 'others';
            }
            //city value
            $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>$citykey])->one();
            if($result !=null){
                $points += $result['values'];
                $summary['city'] = $result['values'];
            }else{
                $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    $points += $result['values'];
                    $summary['city'] = $result['values'];
                }else{
                    return false;
                }
            }

            //tenure value
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "Non-Freehold";
            }

            $result = ProposalMasterFile::find()->where(['heading'=>'Tenure', 'sub_heading'=>$tenureName])->one();
            if($result !=null){
                $points += $result['values'];
                $summary['tenure'] = $result['values'];
            }else{
                $points += 0;
                $summary['tenure'] = 0;
            }


            // complexity value
            $result = ProposalMasterFile::find()->where(['heading'=>'Complexity', 'sub_heading'=>$post['complexity']])->one();
            if($result !=null){
                $points += $result['values'];
                $summary['complexity'] = $result['values'];
            }else{
                return false;
            }


            $type_of_valuation = '';
            if($post['type_of_valuation'] == 1){
                $type_of_valuation = 'drive-by';
            }else if($post['type_of_valuation'] == 2){
                $type_of_valuation = 'physical-inspection';
            }else{
                $type_of_valuation = 'desktop';
            }

            //Type of Valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Type of Valuation', 'sub_heading'=>$type_of_valuation])->one();
            if ($result!=null) {
                $points += $result['values'];
                $summary['type_of_valuation'] = $result['values'];
            }else{
                return false;
            }


            //number_of_comparables
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Number of Comparables Data', 'sub_heading'=>$post['number_of_comparables']])->one();
            if ($result!=null) {
                $points += $result['values'];
                $summary['number_of_comparables'] = $result['values'];
            }else{
                return false;
            }

            //new repeat_valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'New Repeat Valuation Data', 'sub_heading'=>$post['repeat_valuation']])->one();
            if ($result!=null) {
                $points += $result['values'];
                $summary['repeat_valuation'] = $result['values'];
            }else{
                return false;
            }



            //built up area
            if ($property_detail->bua_fee != 1) {
                $points += 0;
                $summary['built_up_area'] = 0;
            }
            else{
                if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
                    $post['built_up_area']=1;
                }
                else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
                    $post['built_up_area']=2;
                }

                else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
                    $post['built_up_area']=3;
                }
                else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
                    $post['built_up_area']=4;
                }
                else if($post['built_up_area'] > 8001 && $post['built_up_area'] <= 10000){
                    $post['built_up_area']=5;
                }
                else if($post['built_up_area'] >= 10001) {
                    $post['built_up_area']=6;
                }

                if($post['built_up_area'] < 1){
                    $points += 0;
                    $summary['built_up_area'] = 0;
                }else {
                    $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArr()[round($post['built_up_area'])];
                    $result = ProposalMasterFile::find()
                        ->where(['heading' => 'Build up Area of Subject Property', 'sub_heading' => $sub_heading])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        $summary['built_up_area'] = $result['values'];
                    } else {
                        return false;
                    }
                }
            }




            //land size
            if ($property_detail->land_size_fee == 1) {
                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 7500){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 7501 && $post['land_size'] <= 10000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 10001 && $post['land_size'] <= 15000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 15001 && $post['land_size'] <= 25000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 25001) {
                    $post['land_size']=6;
                }
                if($post['land_size'] < 1){
                    $points += 0;
                    $summary['land_size'] = 0;
                }
                else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArr()[$post['land_size']];
                    $result = ProposalMasterFile::find()->where(['heading' => 'Land', 'sub_heading' => $landSize])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        $summary['land_size'] = $result['values'];
                    } else {
                        return false;
                    }
                }
            }else{
                $points += 0;
                $summary['land_size'] = '0';
            }



            //number of units 3,7,22 buildings wali han......
            if ($property_detail->no_of_units_fee == 1) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $result = ProposalMasterFile::find()->where(['heading'=>'No Of Units Building', 'sub_heading'=>$num_units])->one();
                if ($result!=null) {
                    $points += $result['values'];
                    $summary['no_of_units'] = $result['values'];
                }else{
                    $points += 0;
                    $summary['no_of_units'] = 0;
                }
            }


            if ($property_detail->upgrade_fee == 1) {
                $result = ProposalMasterFile::find()->where(['heading'=>'Number of Units Land', 'sub_heading'=>$post['upgrades']])->one();

                if ($result!=null) {
                    $points += $result['values'];
                    $summary['upgrades'] = $result['values'];
                }else{
                    $points += 0;
                    $summary['upgrades'] = 0;
                }
            }

            $summary['base_fee'] = $this->getBaseFee($post['property']);
            // $summary['points'] = $points;
            $summary['total_fee'] = $points + $this->getBaseFee($post['property']);


            return $summary;


        }
        else{
            return false;
        }
    }



    public function getPreviousQuotationSummary($post=null)
    {
        $summary = [];
        $points=0;

        $summary['building_title'] = $post['building_title'];

        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null) {

            $property_detail = Properties::find($post['property'])->one();

            //client Type
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Client Type', 'sub_heading'=>$post['clientType']])->one();
            if($result !=null){
                $points += $result['values'];
                $summary['client_type'] = $result['values'];
            }else{
                return false;
            }

            //Payment Terms
            $result = $result = ProposalMasterFile::find()->where(['heading'=>'Payment Terms', 'sub_heading'=>$post['paymentTerms']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $points += $result['values'];
                    $summary['payment_terms'] = $result['values'];
                }
            }else{
                return false;
            }



            // subject property value
            $name = Properties::find()->select(['title'])->where(['id'=>$post['property']])->one();
            $str = $name['title'];
            $name_pro = explode(" ",$str);

            $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property'])->andWhere(['like', 'sub_heading', '%'.trim($name_pro[0]). '%', false])->one();

            if($result !=null){
                if((trim($name_pro[0])) == 'Land' &&  $post['property']!=20 &&  $post['property']!=26){
                    $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>'others'])->one();
                    if ($result!=null) {
                        if (  $post['property']!=7 && $post['property']!=3 && $post['property']!=22 ) {
                            $points += $result['values'];
                            $summary['property'] = $result['values'];
                        }
                    }
                }else {
                    $points += $result['values'];
                    $summary['property'] = $result['values'];
                }
            }else{
                $result = ProposalMasterFile::find()->where(['heading'=>'Subject Property', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    if (  $post['property']!=7 && $post['property']!=3 && $post['property']!=22 ) {
                        $points += $result['values'];
                        $summary['property'] = $result['values'];
                    }
                }else{
                    return false;
                }
            }




            if($post['city'] == 3506 || $post['city'] == 4260){
                $citykey = 'Abu Dhabi';
            }else if($post['city'] == 3510){
                $citykey = 'dubai';
            }else if($post['city'] == 3507){
                $citykey = 'ajman';
            }else if($post['city'] == 3509 || $post['city'] == 3512){
                $citykey = 'sharjah';
            }else if($post['city'] == 3511 || $post['city'] == 3508){
                $citykey = 'rak-fuj';
            }else{
                $citykey = 'others';
            }
            //city value
            $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>$citykey])->one();

            if($result !=null){
                $points += $result['values'];
                $summary['city'] = $result['values'];
            }else{
                $result = ProposalMasterFile::find()->where(['heading'=>'City', 'sub_heading'=>'others'])->one();
                if ($result!=null) {
                    $points += $result['values'];
                    $summary['city'] = $result['values'];
                }else{
                    return false;
                }
            }




            //tenure value
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "Non-Freehold";
            }

            $result = ProposalMasterFile::find()->where(['heading'=>'Tenure', 'sub_heading'=>$tenureName])->one();
            if($result !=null){
                $points += $result['values'];
                $summary['tenure'] = $result['values'];
            }else{
                $points +=0;
                $summary['tenure'] = $result['values'];
            }


            // complexity value
            $result = ProposalMasterFile::find()->where(['heading'=>'Complexity', 'sub_heading'=>$post['complexity']])->one();
            if($result !=null){
                $points += $result['values'];
                $summary['complexity'] = $result['values'];
            }else{
                return false;
            }


            $type_of_valuation = '';
            if($post['type_of_valuation'] == 1){
                $type_of_valuation = 'drive-by';
            }else if($post['type_of_valuation'] == 2){
                $type_of_valuation = 'physical-inspection';
            }else{
                $type_of_valuation = 'desktop';
            }

            //Type of Valuation
            $result = ProposalMasterFile::find()->where(['heading'=>'Type of Valuation', 'sub_heading'=>$type_of_valuation])->one();
            if ($result!=null) {
                $points += $result['values'];
                $summary['type_of_valuation'] = $result['values'];
            }else{
                return false;
            }



            //number_of_comparables
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'Number of Comparables Data', 'sub_heading'=>$post['number_of_comparables']])->one();
            if ($result!=null) {
                $points += $result['values'];
                $summary['number_of_comparables'] = $result['values'];
            }else{
                return false;
            }



            //new repeat_valuation
            $result = ProposalMasterFile::find()
                ->where(['heading'=>'New Repeat Valuation Data', 'sub_heading'=>$post['repeat_valuation']])->one();
            if ($result!=null) {
                $points += $result['values'];
                $summary['repeat_valuation'] = $result['values'];
            }else{
                return false;
            }




            //built up area
            if (  $post['property']==22 || $post['property']==4 || $post['property']==5 ||
                $post['property']==23 || $post['property']==26 || $post['property']==29) {
                $points +=0;
                $summary['built_up_area'] = 0;
            }
            else{
                if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
                    $post['built_up_area']=1;
                }
                else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
                    $post['built_up_area']=2;
                }

                else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
                    $post['built_up_area']=3;
                }
                else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
                    $post['built_up_area']=4;
                }
                else if($post['built_up_area'] > 8001 && $post['built_up_area'] <= 10000){
                    $post['built_up_area']=5;
                }
                else if($post['built_up_area'] >= 10001) {
                    $post['built_up_area']=6;
                }

                if($post['built_up_area'] < 1){
                    $points +=0;
                    $summary['built_up_area'] = 0;
                }else {
                    $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArr()[round($post['built_up_area'])];

                    $result = ProposalMasterFile::find()->where(['heading' => 'Build up Area of Subject Property', 'sub_heading' => $sub_heading])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        $summary['built_up_area'] = $result['values'];
                    } else {
                        return false;
                    }
                }
            }




            //land size
            if ($post['property']==4 || $post['property']==5 ||
                $post['property']==11 || $post['property']==23 ||
                $post['property']==26 || $post['property']==29 || $post['property']==24) {

                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 7500){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 7501 && $post['land_size'] <= 10000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 10001 && $post['land_size'] <= 15000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 15001 && $post['land_size'] <= 25000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 25001) {
                    $post['land_size']=6;
                }

                if($post['land_size'] < 1){
                    $points +=0;
                    $summary['land_size'] = 0;
                }
                else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArr()[$post['land_size']];
                    $result = ProposalMasterFile::find()->where(['heading' => 'Land', 'sub_heading' => $landSize])->one();
                    if ($result != null) {
                        $points += $result['values'];
                        $summary['land_size'] = $result['values'];
                    } else {
                        return false;
                    }
                }
            }else{
                $points +=0;
                $summary['land_size'] = 0;
            }



            //number of units 3,7,22 buildings wali han......
            if ($post['property']==3 || $post['property']==7 || $post['property']==22 || $post['property']==24) {
                $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArr()[$post['no_of_units']];
                $result = ProposalMasterFile::find()->where(['heading'=>'No Of Units Building', 'sub_heading'=>$num_units])->one();
                if ($result!=null) {
                    $points += $result['values'];
                    $summary['no_of_units'] = $result['values'];
                }else{
                    return false;
                }
            }else{
                $result = ProposalMasterFile::find()->where(['heading'=>'Number of Units Land', 'sub_heading'=>$post['no_of_units']])->one();

                if ($result!=null) {
                    $points += $result['values'];
                    $summary['no_of_units'] = $result['values'];
                }else{
                    return false;
                }
            }

            $summary['base_fee'] = $this->getBaseFee($post['property']);
            // $summary['points'] = $points;
            $summary['total_fee'] = $points + $this->getBaseFee($post['property']);

            return $summary;



        }else{
            return false;
        }

    }



    public function getClientRevenueQoutaionSummary($client_id, $inspection, $tenure, $city, $building_title)
    {
        $summary = array();
        $summary['building_title'] = $building_title;

        $fee = '';
        $tat = '';
        $inspection_type= 1;
        if($inspection == 1){
            $inspection_type = 3;
        }else if($inspection == 3){
            $inspection_type=2;
        }

        if ($tenure==2) {
            $row = \app\models\CompanyFeeStructure::find()
                ->select(['fee', 'tat'])
                ->where(['company_id' =>$client_id, 'fee_structure_type_id' =>1, 'emirate_id' =>$city, 'type_id' => $inspection_type])
                ->asArray()->one();
        }
        if ($tenure<>null && $tenure!=2) {
            $row = \app\models\CompanyFeeStructure::find()
                ->select(['fee', 'tat'])
                ->where(['company_id' => $client_id, 'fee_structure_type_id' =>2, 'emirate_id' =>$city, 'type_id' => $inspection_type])
                ->asArray()->one();
        }

        if ($row != null) {
            $summary['fee'] = $row['fee'];
            $summary['tat'] = $row['tat'];
        }
        return $summary;
    }


    public function getAmount_autoRFS($post)
    {
        // dd($post);
        $points =0;
       
        $post['approach_type']= 'bcs';
        $post['bcs_type']='rfs';
        

        $ponits=0;
        $base_fee=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null && $post['approach_type']!=null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'bcs_type','approach_type'=>$post['approach_type'],'sub_heading'=>$post['bcs_type'] ])->one();
            if($result !=null){
                $points += $result['values'];
                $base_fee = $result['values'];
            }
            
            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type'],'sub_heading'=>$post['property'] ])->one();
            if($result !=null){
                    $points += $result['values'];
                    // $base_fee = $result['values'];
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'],$base_fee);
            }else{
            }

            /*echo $post['type_of_valuation'];
            die;*/
            //city value (%)
            // if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }
                /*  echo $citykey;
                    die;*/
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            // }
            
            

            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "non_freehold";
            }
            if($tenureName == 'Freehold'){
                $tenureName = "freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }
            


            // complexity value (%)
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            // }else{
            // }

            // drawing available value (%)               
            // $all_drawings_available = array('civil','mechanical','electrical','plumbing','hvac');
            // $saved_drawings_available = explode(',', $post['drawings_available']);
            // $drawings_not_available = array_diff($all_drawings_available, $saved_drawings_available);               

            // foreach ($drawings_not_available as $drawing) {    
            //     $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>$drawing,'approach_type'=>$post['approach_type']])->one();
            //     if($result !=null){
            //         $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     }else{
            //     }                    
            // }                       
            
            // drawing available  (%)
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'civil','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'mechanical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'electrical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'plumbing','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'hvac','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }


            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
            }

            //land size (%)
            if ($property_detail->land_size_fee == 1) {
                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 10000){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 10001 && $post['land_size'] <= 20000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 20001 && $post['land_size'] <= 40000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 40001 && $post['land_size'] <= 75000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 75001) {
                    $post['land_size']=6;
                }
                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }else{
                $points +=0;
            }
            


           

            //built up area (%)
            // dd($property_detail->bua_fee);
            if ($property_detail->bua_fee != 1) {
                $points +=0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else 
            // if($post['property'] == 4 || $post['property'] == 5 || $post['property']==39 || $post['property']== 49 || $post['property'] == 48 || $post['property']== 50 || $post['property']==53)
            {
                    //building land built up area
                    
                    if($post['built_up_area'] >=1 && $post['built_up_area'] <= 10000){
                        $post['built_up_area']=1; 
                    }
                    if($post['built_up_area'] >=10001 && $post['built_up_area'] <= 25000){
                        $post['built_up_area']=2;
                    }
                    else if($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000){
                        $post['built_up_area']=3; 
                    }
                    else if($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000){
                        $post['built_up_area']=4; 
                    }
                    else if($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000){
                        $post['built_up_area']=5;
                    }
                    else if($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000){
                        $post['built_up_area']=6;
                    } 
                    else if($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000){
                        $post['built_up_area']=7;
                    }
                    else if($post['built_up_area'] >= 1000000) {
                        $post['built_up_area']=8;
                    }

                    if($post['built_up_area'] < 1){
                        $points +=0;
                    }else {
                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];
                        
                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
                            
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                        } else {
                        }
                    }

            }
            
            // else{

            //     if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
            //         $post['built_up_area']=1;
            //     }
            //     else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
            //         $post['built_up_area']=2;
            //     }
            //     else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
            //         $post['built_up_area']=3;
            //     }
            //     else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
            //         $post['built_up_area']=4;
            //     }
            //     else if($post['built_up_area'] >= 8001) {
            //         $post['built_up_area']=5;
            //     }
            //     if($post['built_up_area'] < 1){
            //         $points +=0;
            //     }else {


            //         $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAuto()[round($post['built_up_area'])];

            //         $result = QuotationFeeMasterFile::find()
            //             ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
            //         if ($result != null) {
            //            /* echo $this->getPercentageAmount($result['values'], $base_fee);
            //             die;*/
            //             $points += $this->getPercentageAmount($result['values'], $base_fee);
            //         } else {
            //         }
            //     }
            // }



           /* if( $post['key'] == '11') {
                echo $post['built_up_area'];
                die;
            }*/

            
            //number_of_comparables
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            
            //number of units 
            $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }


            return $points;

            //next process

            //Payment Terms
            $advance_payment =0;
            $sub_heading_data = explode('%',$post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'payment_terms', 'sub_heading'=>$sub_heading_data[0],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            }else{
            }



            /*   echo $points;
               die;*/








            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'new_repeat_valuation_data', 'sub_heading'=>$name_repeat_valuation,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }








            //number of units 3,7,22 buildings wali han......

            if($post['built_up_area'] > 0 && ($approach_type == 'income' ||  $approach_type == 'profit')){

            }else {


                if ($property_detail->no_of_units_fee == 1) {
                    $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $num_units, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $result['values'];

                        // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "12"; die();
                        $points += 0;
                    }
                }
            }





            // if($post['approach_type'] == "profit"){

            //     if ($post['last_3_years_finance']== 1) {
            //         // echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_3_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            //     if ($post['projections_10_years'] == 1) {
            //        //  echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            // }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }

    public function getAmount_autoSCS($post)
    {
        // dd($post);
        $points =0;
       
        $post['approach_type']= 'bcs';
        $post['bcs_type']='scas';
        

        $ponits=0;
        $base_fee=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null && $post['approach_type']!=null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'bcs_type','approach_type'=>$post['approach_type'],'sub_heading'=>$post['bcs_type'] ])->one();
            if($result !=null){
                $points += $result['values'];
                $base_fee = $result['values'];
            }

            
            
            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type'],'sub_heading'=>$post['property'] ])->one();
            if($result !=null){
                    $points += $result['values'];
                    // $base_fee += $result['values'];
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'],$base_fee);
            }else{
            }

            /*echo $post['type_of_valuation'];
            die;*/
            //city value (%)
            // if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }
                /*  echo $citykey;
                    die;*/
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            // }

            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "non_freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }


            // complexity value (%)
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            // }else{
            // }

            // drawing available value (%)               
            // $all_drawings_available = array('civil','mechanical','electrical','plumbing','hvac');
            // $saved_drawings_available = explode(',', $post['drawings_available']);
            // $drawings_not_available = array_diff($all_drawings_available, $saved_drawings_available);               

            // foreach ($drawings_not_available as $drawing) {    
            //     $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>$drawing,'approach_type'=>$post['approach_type']])->one();
            //     if($result !=null){
            //         $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     }else{
            //     }                    
            // }  
            // drawing available  (%)
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'civil','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'mechanical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'electrical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'plumbing','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'hvac','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }  

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
            }


            //land size (%)
            if ($property_detail->land_size_fee == 1) {
                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 10000){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 10001 && $post['land_size'] <= 20000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 20001 && $post['land_size'] <= 40000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 40001 && $post['land_size'] <= 75000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 75001) {
                    $post['land_size']=6;
                }
                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }else{
                $points +=0;
            }

            //built up area (%)
            // dd($property_detail->bua_fee);
            if ($property_detail->bua_fee != 1) {
                $points +=0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else 
            // if($post['property'] == 4 || $post['property'] == 5 || $post['property']==39 || $post['property']== 49 || $post['property'] == 48 || $post['property']== 50 || $post['property']==53)
            {
                    //building land built up area
                    
                    if($post['built_up_area'] >=1 && $post['built_up_area'] <= 10000){
                        $post['built_up_area']=1; 
                    }
                    if($post['built_up_area'] >=10001 && $post['built_up_area'] <= 25000){
                        $post['built_up_area']=2;
                    }
                    else if($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000){
                        $post['built_up_area']=3; 
                    }
                    else if($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000){
                        $post['built_up_area']=4; 
                    }
                    else if($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000){
                        $post['built_up_area']=5;
                    }
                    else if($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000){
                        $post['built_up_area']=6;
                    } 
                    else if($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000){
                        $post['built_up_area']=7;
                    }
                    else if($post['built_up_area'] >= 1000000) {
                        $post['built_up_area']=8;
                    }

                    if($post['built_up_area'] < 1){
                        $points +=0;
                    }else {
                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];
                        
                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
                            
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                        } else {
                        }
                    }

            }
            // else{

            //     if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
            //         $post['built_up_area']=1;
            //     }
            //     else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
            //         $post['built_up_area']=2;
            //     }
            //     else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
            //         $post['built_up_area']=3;
            //     }
            //     else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
            //         $post['built_up_area']=4;
            //     }
            //     else if($post['built_up_area'] >= 8001) {
            //         $post['built_up_area']=5;
            //     }
            //     if($post['built_up_area'] < 1){
            //         $points +=0;
            //     }else {


            //         $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAuto()[round($post['built_up_area'])];

            //         $result = QuotationFeeMasterFile::find()
            //             ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
            //         if ($result != null) {
            //            /* echo $this->getPercentageAmount($result['values'], $base_fee);
            //             die;*/
            //             $points += $this->getPercentageAmount($result['values'], $base_fee);
            //         } else {
            //         }
            //     }
            // }
           /* if( $post['key'] == '11') {
                echo $post['built_up_area'];
                die;
            }*/
            //number_of_comparables
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            //number of units 
            $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            return $points;

//next process

            //Payment Terms
            $advance_payment =0;
            $sub_heading_data = explode('%',$post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'payment_terms', 'sub_heading'=>$sub_heading_data[0],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            }else{
            }



            /*   echo $points;
               die;*/








            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'new_repeat_valuation_data', 'sub_heading'=>$name_repeat_valuation,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }








            //number of units 3,7,22 buildings wali han......

            if($post['built_up_area'] > 0 && ($approach_type == 'income' ||  $approach_type == 'profit')){

            }else {


                if ($property_detail->no_of_units_fee == 1) {
                    $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $num_units, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $result['values'];

                        // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "12"; die();
                        $points += 0;
                    }
                }
            }





            // if($post['approach_type'] == "profit"){

            //     if ($post['last_3_years_finance']== 1) {
            //         // echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_3_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            //     if ($post['projections_10_years'] == 1) {
            //        //  echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            // }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }

    public function getAmount_autoBCA($post)
    {
        // dd($post);
        $points =0;
       
        $post['approach_type']= 'bcs';
        $post['bcs_type']='bca';
        

        $ponits=0;
        $base_fee=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null && $post['approach_type']!=null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'bcs_type','approach_type'=>$post['approach_type'],'sub_heading'=>$post['bcs_type'] ])->one();
            if($result !=null){
                $points += $result['values'];
                $base_fee = $result['values'];
            }
            
            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type'],'sub_heading'=>$post['property'] ])->one();
            if($result !=null){
                    $points += $result['values'];
                    //$base_fee += $result['values'];
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'],$base_fee);
            }else{
            }
            
            /*echo $post['type_of_valuation'];
            die;*/
            //city value (%)
            // if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }
                //   /*  echo $citykey;
                //     die;*/
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            // }
            

            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "non_freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }


            // complexity value (%)
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            // }else{
            // }

            // drawing available value (%)               
            // $all_drawings_available = array('civil','mechanical','electrical','plumbing','hvac');
            // $saved_drawings_available = explode(',', $post['drawings_available']);
            // $drawings_not_available = array_diff($all_drawings_available, $saved_drawings_available);               

            // foreach ($drawings_not_available as $drawing) {    
            //     $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>$drawing,'approach_type'=>$post['approach_type']])->one();
            //     if($result !=null){
            //         $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     }else{
            //     }                    
            // }    

            // dd($post);

            // drawing available  (%)
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'civil','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'mechanical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'electrical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'plumbing','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'hvac','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
            }


            //land size (%)
            if ($property_detail->land_size_fee == 1) {
                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 10000){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 10001 && $post['land_size'] <= 20000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 20001 && $post['land_size'] <= 40000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 40001 && $post['land_size'] <= 75000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 75001) {
                    $post['land_size']=6;
                }
                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }else{
                $points +=0;
            }

            //built up area (%)
            // dd($property_detail->bua_fee);
            if ($property_detail->bua_fee != 1) {
                $points +=0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else 
            // if($post['property'] == 4 || $post['property'] == 5 || $post['property']==39 || $post['property']== 49 || $post['property'] == 48 || $post['property']== 50 || $post['property']==53)
            {
                    //building land built up area
                    
                    if($post['built_up_area'] >=1 && $post['built_up_area'] <= 10000){
                        $post['built_up_area']=1; 
                    }
                    if($post['built_up_area'] >=10001 && $post['built_up_area'] <= 25000){
                        $post['built_up_area']=2;
                    }
                    else if($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000){
                        $post['built_up_area']=3; 
                    }
                    else if($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000){
                        $post['built_up_area']=4; 
                    }
                    else if($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000){
                        $post['built_up_area']=5;
                    }
                    else if($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000){
                        $post['built_up_area']=6;
                    } 
                    else if($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000){
                        $post['built_up_area']=7;
                    }
                    else if($post['built_up_area'] >= 1000000) {
                        $post['built_up_area']=8;
                    }

                    if($post['built_up_area'] < 1){
                        $points +=0;
                    }else {
                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];
                        
                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
                            
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                        } else {
                        }
                    }

            }
            // else{

            //     if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
            //         $post['built_up_area']=1;
            //     }
            //     else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
            //         $post['built_up_area']=2;
            //     }
            //     else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
            //         $post['built_up_area']=3;
            //     }
            //     else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
            //         $post['built_up_area']=4;
            //     }
            //     else if($post['built_up_area'] >= 8001) {
            //         $post['built_up_area']=5;
            //     }
            //     if($post['built_up_area'] < 1){
            //         $points +=0;
            //     }else {


            //         $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAuto()[round($post['built_up_area'])];

            //         $result = QuotationFeeMasterFile::find()
            //             ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
            //         if ($result != null) {
            //            /* echo $this->getPercentageAmount($result['values'], $base_fee);
            //             die;*/
            //             $points += $this->getPercentageAmount($result['values'], $base_fee);
            //         } else {
            //         }
            //     }
            // }
           /* if( $post['key'] == '11') {
                echo $post['built_up_area'];
                die;
            }*/
            //number_of_comparables
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            //number of units 
            $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }

            // dd($points);

            return $points;

//next process

            //Payment Terms
            $advance_payment =0;
            $sub_heading_data = explode('%',$post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'payment_terms', 'sub_heading'=>$sub_heading_data[0],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            }else{
            }



            /*   echo $points;
               die;*/








            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'new_repeat_valuation_data', 'sub_heading'=>$name_repeat_valuation,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }








            //number of units 3,7,22 buildings wali han......

            if($post['built_up_area'] > 0 && ($approach_type == 'income' ||  $approach_type == 'profit')){

            }else {


                if ($property_detail->no_of_units_fee == 1) {
                    $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $num_units, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $result['values'];

                        // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "12"; die();
                        $points += 0;
                    }
                }
            }





            // if($post['approach_type'] == "profit"){

            //     if ($post['last_3_years_finance']== 1) {
            //         // echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_3_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            //     if ($post['projections_10_years'] == 1) {
            //        //  echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            // }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }

    public function getAmount_autoRICA($post)
    {
        // dd($post);
        $points =0;
       
        $post['approach_type']= 'bcs';
        $post['bcs_type']='rica';
        

        $ponits=0;
        $base_fee=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null && $post['approach_type']!=null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'bcs_type','approach_type'=>$post['approach_type'],'sub_heading'=>$post['bcs_type'] ])->one();
            if($result !=null){
                $points += $result['values'];
                $base_fee = $result['values'];
            }
            
            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type'],'sub_heading'=>$post['property'] ])->one();
            if($result !=null){
                    $points += $result['values'];
                    // $base_fee = $result['values'];
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'],$base_fee);
            }else{
            }

            /*echo $post['type_of_valuation'];
            die;*/
            //city value (%)
            // if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }
                /*  echo $citykey;
                    die;*/
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            // }
            
            

            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "non_freehold";
            }
            if($tenureName == 'Freehold'){
                $tenureName = "freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }
            


            // complexity value (%)
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            // }else{
            // }

            // drawing available value (%)               
            // $all_drawings_available = array('civil','mechanical','electrical','plumbing','hvac');
            // $saved_drawings_available = explode(',', $post['drawings_available']);
            // $drawings_not_available = array_diff($all_drawings_available, $saved_drawings_available);               

            // foreach ($drawings_not_available as $drawing) {    
            //     $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>$drawing,'approach_type'=>$post['approach_type']])->one();
            //     if($result !=null){
            //         $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     }else{
            //     }                    
            // }                       
            
            // drawing available  (%)
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'civil','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'mechanical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'electrical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'plumbing','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'hvac','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }


            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
            }

            //land size (%)
            if ($property_detail->land_size_fee == 1) {
                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 10000){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 10001 && $post['land_size'] <= 20000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 20001 && $post['land_size'] <= 40000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 40001 && $post['land_size'] <= 75000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 75001) {
                    $post['land_size']=6;
                }
                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }else{
                $points +=0;
            }
            


           

            //built up area (%)
            // dd($property_detail->bua_fee);
            if ($property_detail->bua_fee != 1) {
                $points +=0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else 
            // if($post['property'] == 4 || $post['property'] == 5 || $post['property']==39 || $post['property']== 49 || $post['property'] == 48 || $post['property']== 50 || $post['property']==53)
            {
                    //building land built up area
                    
                    if($post['built_up_area'] >=1 && $post['built_up_area'] <= 10000){
                        $post['built_up_area']=1; 
                    }
                    if($post['built_up_area'] >=10001 && $post['built_up_area'] <= 25000){
                        $post['built_up_area']=2;
                    }
                    else if($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000){
                        $post['built_up_area']=3; 
                    }
                    else if($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000){
                        $post['built_up_area']=4; 
                    }
                    else if($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000){
                        $post['built_up_area']=5;
                    }
                    else if($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000){
                        $post['built_up_area']=6;
                    } 
                    else if($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000){
                        $post['built_up_area']=7;
                    }
                    else if($post['built_up_area'] >= 1000000) {
                        $post['built_up_area']=8;
                    }

                    if($post['built_up_area'] < 1){
                        $points +=0;
                    }else {
                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];
                        
                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
                            
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                        } else {
                        }
                    }

            }
            
            // else{

            //     if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
            //         $post['built_up_area']=1;
            //     }
            //     else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
            //         $post['built_up_area']=2;
            //     }
            //     else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
            //         $post['built_up_area']=3;
            //     }
            //     else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
            //         $post['built_up_area']=4;
            //     }
            //     else if($post['built_up_area'] >= 8001) {
            //         $post['built_up_area']=5;
            //     }
            //     if($post['built_up_area'] < 1){
            //         $points +=0;
            //     }else {


            //         $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAuto()[round($post['built_up_area'])];

            //         $result = QuotationFeeMasterFile::find()
            //             ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
            //         if ($result != null) {
            //            /* echo $this->getPercentageAmount($result['values'], $base_fee);
            //             die;*/
            //             $points += $this->getPercentageAmount($result['values'], $base_fee);
            //         } else {
            //         }
            //     }
            // }



           /* if( $post['key'] == '11') {
                echo $post['built_up_area'];
                die;
            }*/

            
            //number_of_comparables
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            
            //number of units 
            $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }


            return $points;

            //next process

            //Payment Terms
            $advance_payment =0;
            $sub_heading_data = explode('%',$post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'payment_terms', 'sub_heading'=>$sub_heading_data[0],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            }else{
            }



            /*   echo $points;
               die;*/








            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'new_repeat_valuation_data', 'sub_heading'=>$name_repeat_valuation,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }








            //number of units 3,7,22 buildings wali han......

            if($post['built_up_area'] > 0 && ($approach_type == 'income' ||  $approach_type == 'profit')){

            }else {


                if ($property_detail->no_of_units_fee == 1) {
                    $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $num_units, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $result['values'];

                        // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "12"; die();
                        $points += 0;
                    }
                }
            }





            // if($post['approach_type'] == "profit"){

            //     if ($post['last_3_years_finance']== 1) {
            //         // echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_3_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            //     if ($post['projections_10_years'] == 1) {
            //        //  echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            // }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }

    public function getAmount_autoTS($post)
    {
        // dd($post);
        $points =0;
       
        $post['approach_type']= 'bcs';
        $post['bcs_type']='ts';
        

        $ponits=0;
        $base_fee=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null && $post['approach_type']!=null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'bcs_type','approach_type'=>$post['approach_type'],'sub_heading'=>$post['bcs_type'] ])->one();
            if($result !=null){
                $points += $result['values'];
                $base_fee = $result['values'];
            }
            
            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type'],'sub_heading'=>$post['property'] ])->one();
            if($result !=null){
                    $points += $result['values'];
                    // $base_fee = $result['values'];
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'],$base_fee);
            }else{
            }

            /*echo $post['type_of_valuation'];
            die;*/
            //city value (%)
            // if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }
                /*  echo $citykey;
                    die;*/
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            // }
            
            

            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "non_freehold";
            }
            if($tenureName == 'Freehold'){
                $tenureName = "freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }
            


            // complexity value (%)
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            // }else{
            // }

            // drawing available value (%)               
            // $all_drawings_available = array('civil','mechanical','electrical','plumbing','hvac');
            // $saved_drawings_available = explode(',', $post['drawings_available']);
            // $drawings_not_available = array_diff($all_drawings_available, $saved_drawings_available);               

            // foreach ($drawings_not_available as $drawing) {    
            //     $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>$drawing,'approach_type'=>$post['approach_type']])->one();
            //     if($result !=null){
            //         $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     }else{
            //     }                    
            // }                       
            
            // drawing available  (%)
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'civil','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'mechanical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'electrical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'plumbing','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'hvac','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }

            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
            }

            //land size (%)
            if ($property_detail->land_size_fee == 1) {
                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 10000){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 10001 && $post['land_size'] <= 20000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 20001 && $post['land_size'] <= 40000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 40001 && $post['land_size'] <= 75000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 75001) {
                    $post['land_size']=6;
                }
                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }else{
                $points +=0;
            }
            


           

            //built up area (%)
            // dd($property_detail->bua_fee);
            if ($property_detail->bua_fee != 1) {
                $points +=0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else 
            // if($post['property'] == 4 || $post['property'] == 5 || $post['property']==39 || $post['property']== 49 || $post['property'] == 48 || $post['property']== 50 || $post['property']==53)
            {
                    //building land built up area
                    
                    if($post['built_up_area'] >=1 && $post['built_up_area'] <= 10000){
                        $post['built_up_area']=1; 
                    }
                    if($post['built_up_area'] >=10001 && $post['built_up_area'] <= 25000){
                        $post['built_up_area']=2;
                    }
                    else if($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000){
                        $post['built_up_area']=3; 
                    }
                    else if($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000){
                        $post['built_up_area']=4; 
                    }
                    else if($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000){
                        $post['built_up_area']=5;
                    }
                    else if($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000){
                        $post['built_up_area']=6;
                    } 
                    else if($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000){
                        $post['built_up_area']=7;
                    }
                    else if($post['built_up_area'] >= 1000000) {
                        $post['built_up_area']=8;
                    }

                    if($post['built_up_area'] < 1){
                        $points +=0;
                    }else {
                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];
                        
                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
                            
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                        } else {
                        }
                    }

            }
            
            // else{

            //     if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
            //         $post['built_up_area']=1;
            //     }
            //     else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
            //         $post['built_up_area']=2;
            //     }
            //     else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
            //         $post['built_up_area']=3;
            //     }
            //     else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
            //         $post['built_up_area']=4;
            //     }
            //     else if($post['built_up_area'] >= 8001) {
            //         $post['built_up_area']=5;
            //     }
            //     if($post['built_up_area'] < 1){
            //         $points +=0;
            //     }else {


            //         $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAuto()[round($post['built_up_area'])];

            //         $result = QuotationFeeMasterFile::find()
            //             ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
            //         if ($result != null) {
            //            /* echo $this->getPercentageAmount($result['values'], $base_fee);
            //             die;*/
            //             $points += $this->getPercentageAmount($result['values'], $base_fee);
            //         } else {
            //         }
            //     }
            // }



           /* if( $post['key'] == '11') {
                echo $post['built_up_area'];
                die;
            }*/

            
            //number_of_comparables
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            
            //number of units 
            $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }


            return $points;

            //next process

            //Payment Terms
            $advance_payment =0;
            $sub_heading_data = explode('%',$post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'payment_terms', 'sub_heading'=>$sub_heading_data[0],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            }else{
            }



            /*   echo $points;
               die;*/








            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'new_repeat_valuation_data', 'sub_heading'=>$name_repeat_valuation,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }








            //number of units 3,7,22 buildings wali han......

            if($post['built_up_area'] > 0 && ($approach_type == 'income' ||  $approach_type == 'profit')){

            }else {


                if ($property_detail->no_of_units_fee == 1) {
                    $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $num_units, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $result['values'];

                        // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "12"; die();
                        $points += 0;
                    }
                }
            }





            // if($post['approach_type'] == "profit"){

            //     if ($post['last_3_years_finance']== 1) {
            //         // echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_3_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            //     if ($post['projections_10_years'] == 1) {
            //        //  echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            // }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }

    public function getAmount_autoERR($post)
    {
        // dd($post);
        $points =0;
       
        $post['approach_type']= 'bcs';
        $post['bcs_type']='err';
        

        $ponits=0;
        $base_fee=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null && $post['approach_type']!=null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'bcs_type','approach_type'=>$post['approach_type'],'sub_heading'=>$post['bcs_type'] ])->one();
            if($result !=null){
                $points += $result['values'];
                $base_fee = $result['values'];
            }
            
            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type'],'sub_heading'=>$post['property'] ])->one();
            if($result !=null){
                    $points += $result['values'];
                    // $base_fee = $result['values'];
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'],$base_fee);
            }else{
            }

            /*echo $post['type_of_valuation'];
            die;*/
            //city value (%)
            // if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }
                /*  echo $citykey;
                    die;*/
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            // }
            
            

            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "non_freehold";
            }
            if($tenureName == 'Freehold'){
                $tenureName = "freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }
            


            // complexity value (%)
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            // }else{
            // }

            // drawing available value (%)               
            // $all_drawings_available = array('civil','mechanical','electrical','plumbing','hvac');
            // $saved_drawings_available = explode(',', $post['drawings_available']);
            // $drawings_not_available = array_diff($all_drawings_available, $saved_drawings_available);               

            // foreach ($drawings_not_available as $drawing) {    
            //     $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>$drawing,'approach_type'=>$post['approach_type']])->one();
            //     if($result !=null){
            //         $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     }else{
            //     }                    
            // }                       
            
            // drawing available  (%)
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'civil','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'mechanical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'electrical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'plumbing','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'hvac','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }


            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
            }

            //land size (%)
            if ($property_detail->land_size_fee == 1) {
                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 10000){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 10001 && $post['land_size'] <= 20000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 20001 && $post['land_size'] <= 40000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 40001 && $post['land_size'] <= 75000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 75001) {
                    $post['land_size']=6;
                }
                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }else{
                $points +=0;
            }
            


           

            //built up area (%)
            // dd($property_detail->bua_fee);
            if ($property_detail->bua_fee != 1) {
                $points +=0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else 
            // if($post['property'] == 4 || $post['property'] == 5 || $post['property']==39 || $post['property']== 49 || $post['property'] == 48 || $post['property']== 50 || $post['property']==53)
            {
                    //building land built up area
                    
                    if($post['built_up_area'] >=1 && $post['built_up_area'] <= 10000){
                        $post['built_up_area']=1; 
                    }
                    if($post['built_up_area'] >=10001 && $post['built_up_area'] <= 25000){
                        $post['built_up_area']=2;
                    }
                    else if($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000){
                        $post['built_up_area']=3; 
                    }
                    else if($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000){
                        $post['built_up_area']=4; 
                    }
                    else if($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000){
                        $post['built_up_area']=5;
                    }
                    else if($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000){
                        $post['built_up_area']=6;
                    } 
                    else if($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000){
                        $post['built_up_area']=7;
                    }
                    else if($post['built_up_area'] >= 1000000) {
                        $post['built_up_area']=8;
                    }

                    if($post['built_up_area'] < 1){
                        $points +=0;
                    }else {
                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];
                        
                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
                            
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                        } else {
                        }
                    }

            }
            
            // else{

            //     if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
            //         $post['built_up_area']=1;
            //     }
            //     else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
            //         $post['built_up_area']=2;
            //     }
            //     else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
            //         $post['built_up_area']=3;
            //     }
            //     else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
            //         $post['built_up_area']=4;
            //     }
            //     else if($post['built_up_area'] >= 8001) {
            //         $post['built_up_area']=5;
            //     }
            //     if($post['built_up_area'] < 1){
            //         $points +=0;
            //     }else {


            //         $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAuto()[round($post['built_up_area'])];

            //         $result = QuotationFeeMasterFile::find()
            //             ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
            //         if ($result != null) {
            //            /* echo $this->getPercentageAmount($result['values'], $base_fee);
            //             die;*/
            //             $points += $this->getPercentageAmount($result['values'], $base_fee);
            //         } else {
            //         }
            //     }
            // }



           /* if( $post['key'] == '11') {
                echo $post['built_up_area'];
                die;
            }*/

            
            //number_of_comparables
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            
            //number of units 
            $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }


            return $points;

            //next process

            //Payment Terms
            $advance_payment =0;
            $sub_heading_data = explode('%',$post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'payment_terms', 'sub_heading'=>$sub_heading_data[0],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            }else{
            }



            /*   echo $points;
               die;*/








            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'new_repeat_valuation_data', 'sub_heading'=>$name_repeat_valuation,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }








            //number of units 3,7,22 buildings wali han......

            if($post['built_up_area'] > 0 && ($approach_type == 'income' ||  $approach_type == 'profit')){

            }else {


                if ($property_detail->no_of_units_fee == 1) {
                    $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $num_units, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $result['values'];

                        // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "12"; die();
                        $points += 0;
                    }
                }
            }





            // if($post['approach_type'] == "profit"){

            //     if ($post['last_3_years_finance']== 1) {
            //         // echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_3_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            //     if ($post['projections_10_years'] == 1) {
            //        //  echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            // }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }

    public function getAmount_autoARCT($post)
    {
        // dd($post);
        $points =0;
       
        $post['approach_type']= 'bcs';
        $post['bcs_type']='arct';
        

        $ponits=0;
        $base_fee=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null && $post['approach_type']!=null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'bcs_type','approach_type'=>$post['approach_type'],'sub_heading'=>$post['bcs_type'] ])->one();
            if($result !=null){
                $points += $result['values'];
                $base_fee = $result['values'];
            }
            
            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type'],'sub_heading'=>$post['property'] ])->one();
            if($result !=null){
                    $points += $result['values'];
                    // $base_fee = $result['values'];
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'],$base_fee);
            }else{
            }

            /*echo $post['type_of_valuation'];
            die;*/
            //city value (%)
            // if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }
                /*  echo $citykey;
                    die;*/
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            // }
            
            

            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "non_freehold";
            }
            if($tenureName == 'Freehold'){
                $tenureName = "freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }
            


            // complexity value (%)
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            // }else{
            // }

            // drawing available value (%)               
            // $all_drawings_available = array('civil','mechanical','electrical','plumbing','hvac');
            // $saved_drawings_available = explode(',', $post['drawings_available']);
            // $drawings_not_available = array_diff($all_drawings_available, $saved_drawings_available);               

            // foreach ($drawings_not_available as $drawing) {    
            //     $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>$drawing,'approach_type'=>$post['approach_type']])->one();
            //     if($result !=null){
            //         $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     }else{
            //     }                    
            // }                       
            
            // drawing available  (%)
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'civil','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'mechanical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'electrical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'plumbing','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'hvac','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }


            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
            }

            //land size (%)
            if ($property_detail->land_size_fee == 1) {
                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 10000){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 10001 && $post['land_size'] <= 20000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 20001 && $post['land_size'] <= 40000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 40001 && $post['land_size'] <= 75000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 75001) {
                    $post['land_size']=6;
                }
                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }else{
                $points +=0;
            }
            


           

            //built up area (%)
            // dd($property_detail->bua_fee);
            if ($property_detail->bua_fee != 1) {
                $points +=0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else 
            // if($post['property'] == 4 || $post['property'] == 5 || $post['property']==39 || $post['property']== 49 || $post['property'] == 48 || $post['property']== 50 || $post['property']==53)
            {
                    //building land built up area
                    
                    if($post['built_up_area'] >=1 && $post['built_up_area'] <= 10000){
                        $post['built_up_area']=1; 
                    }
                    if($post['built_up_area'] >=10001 && $post['built_up_area'] <= 25000){
                        $post['built_up_area']=2;
                    }
                    else if($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000){
                        $post['built_up_area']=3; 
                    }
                    else if($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000){
                        $post['built_up_area']=4; 
                    }
                    else if($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000){
                        $post['built_up_area']=5;
                    }
                    else if($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000){
                        $post['built_up_area']=6;
                    } 
                    else if($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000){
                        $post['built_up_area']=7;
                    }
                    else if($post['built_up_area'] >= 1000000) {
                        $post['built_up_area']=8;
                    }

                    if($post['built_up_area'] < 1){
                        $points +=0;
                    }else {
                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];
                        
                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
                            
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                        } else {
                        }
                    }

            }
            
            // else{

            //     if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
            //         $post['built_up_area']=1;
            //     }
            //     else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
            //         $post['built_up_area']=2;
            //     }
            //     else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
            //         $post['built_up_area']=3;
            //     }
            //     else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
            //         $post['built_up_area']=4;
            //     }
            //     else if($post['built_up_area'] >= 8001) {
            //         $post['built_up_area']=5;
            //     }
            //     if($post['built_up_area'] < 1){
            //         $points +=0;
            //     }else {


            //         $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAuto()[round($post['built_up_area'])];

            //         $result = QuotationFeeMasterFile::find()
            //             ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
            //         if ($result != null) {
            //            /* echo $this->getPercentageAmount($result['values'], $base_fee);
            //             die;*/
            //             $points += $this->getPercentageAmount($result['values'], $base_fee);
            //         } else {
            //         }
            //     }
            // }



           /* if( $post['key'] == '11') {
                echo $post['built_up_area'];
                die;
            }*/

            
            //number_of_comparables
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            
            //number of units 
            $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }


            return $points;

            //next process

            //Payment Terms
            $advance_payment =0;
            $sub_heading_data = explode('%',$post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'payment_terms', 'sub_heading'=>$sub_heading_data[0],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            }else{
            }



            /*   echo $points;
               die;*/








            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'new_repeat_valuation_data', 'sub_heading'=>$name_repeat_valuation,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }








            //number of units 3,7,22 buildings wali han......

            if($post['built_up_area'] > 0 && ($approach_type == 'income' ||  $approach_type == 'profit')){

            }else {


                if ($property_detail->no_of_units_fee == 1) {
                    $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $num_units, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $result['values'];

                        // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "12"; die();
                        $points += 0;
                    }
                }
            }





            // if($post['approach_type'] == "profit"){

            //     if ($post['last_3_years_finance']== 1) {
            //         // echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_3_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            //     if ($post['projections_10_years'] == 1) {
            //        //  echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            // }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }
    
    public function getAmount_autoRFV($post)
    {
        // dd($post);
        $points =0;
       
        $post['approach_type']= 'bcs';
        $post['bcs_type']='rfv';
        

        $ponits=0;
        $base_fee=0;
        if ($post['property']!=null && $post['city']!=null && $post['tenure']!=null && $post['approach_type']!=null) {

            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'bcs_type','approach_type'=>$post['approach_type'],'sub_heading'=>$post['bcs_type'] ])->one();
            if($result !=null){
                $points += $result['values'];
                $base_fee = $result['values'];
            }
            
            //get property detail
            $property_detail = Properties::find($post['property'])->where(['id'=>$post['property']])->one();

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'subject_property','approach_type'=>$post['approach_type'],'sub_heading'=>$post['property'] ])->one();
            if($result !=null){
                    $points += $result['values'];
                    // $base_fee = $result['values'];
            }

            //client Type (%)
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'],$base_fee);
            }else{
            }

            /*echo $post['type_of_valuation'];
            die;*/
            //city value (%)
            // if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['city'] == 3506 || $post['city'] == 4260) {
                    $citykey = 'abu_dhabi';
                } else if ($post['city'] == 3510) {
                    $citykey = 'dubai';
                } else if ($post['city'] == 3507) {
                    $citykey = 'ajman';
                } else if ($post['city'] == 3509 || $post['city'] == 3512) {
                    $citykey = 'sharjah';
                } else if ($post['city'] == 3511 || $post['city'] == 3508) {
                    $citykey = 'rak-fuj';
                } else {
                    $citykey = 'others';
                }
                /*  echo $citykey;
                    die;*/
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => $citykey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'city', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            // }
            
            

            //tenure value (%)
            $tenureName = Yii::$app->appHelperFunctions->getBuildingTenureArr()[$post['tenure']];
            if($tenureName == 'Non-Freehold or Freehold for GCC Nationalities Only'){
                $tenureName = "non_freehold";
            }
            if($tenureName == 'Freehold'){
                $tenureName = "freehold";
            }

            $result = QuotationFeeMasterFile::find()->where(['heading'=>'tenure', 'sub_heading'=>$tenureName,'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }
            


            // complexity value (%)
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            // }else{
            // }

            // drawing available value (%)               
            // $all_drawings_available = array('civil','mechanical','electrical','plumbing','hvac');
            // $saved_drawings_available = explode(',', $post['drawings_available']);
            // $drawings_not_available = array_diff($all_drawings_available, $saved_drawings_available);               

            // foreach ($drawings_not_available as $drawing) {    
            //     $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>$drawing,'approach_type'=>$post['approach_type']])->one();
            //     if($result !=null){
            //         $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     }else{
            //     }                    
            // }                       
            
            // drawing available  (%)
            if ($post['civil_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'civil','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            
            if ($post['mechanical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'mechanical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['electrical_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'electrical','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['plumbing_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'plumbing','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }
            if ($post['hvac_drawing_available'] == "No") {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'drawings_available', 'sub_heading'=>'hvac','approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    
                }
            }


            // upgrade value (%)
            if ($property_detail->upgrade_fee == 1) {
                $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
                if ($result!=null) {
                    $results = ($result['values'] / 100) * $base_fee;
                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                }else{
                    $points += 0;
                }
            }

            //land size (%)
            if ($property_detail->land_size_fee == 1) {
                if($post['land_size'] >=1 && $post['land_size'] <= 5000){
                    $post['land_size']=1;
                }
                else if($post['land_size'] >= 5001 && $post['land_size'] <= 10000){
                    $post['land_size']=2;
                }
                else if($post['land_size'] >= 10001 && $post['land_size'] <= 20000){
                    $post['land_size']=3;
                }
                else if($post['land_size'] > 20001 && $post['land_size'] <= 40000){
                    $post['land_size']=4;
                }
                else if($post['land_size'] > 40001 && $post['land_size'] <= 75000){
                    $post['land_size']=5;
                }
                else if($post['land_size'] >= 75001) {
                    $post['land_size']=6;
                }
                if($post['land_size'] < 1){
                    $points +=0;
                }
                else {
                    $landSize = yii::$app->quotationHelperFunctions->getLandSizrArrAutoIncome()[$post['land_size']];
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'land', 'sub_heading' => $landSize,'approach_type'=>$post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }else{
                $points +=0;
            }
            


           

            //built up area (%)
            // dd($property_detail->bua_fee);
            if ($property_detail->bua_fee != 1) {
                $points +=0;
                // yii::info('BUA if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else 
            // if($post['property'] == 4 || $post['property'] == 5 || $post['property']==39 || $post['property']== 49 || $post['property'] == 48 || $post['property']== 50 || $post['property']==53)
            {
                    //building land built up area
                    
                    if($post['built_up_area'] >=1 && $post['built_up_area'] <= 10000){
                        $post['built_up_area']=1; 
                    }
                    if($post['built_up_area'] >=10001 && $post['built_up_area'] <= 25000){
                        $post['built_up_area']=2;
                    }
                    else if($post['built_up_area'] >= 25001 && $post['built_up_area'] <= 50000){
                        $post['built_up_area']=3; 
                    }
                    else if($post['built_up_area'] >= 50001 && $post['built_up_area'] <= 100000){
                        $post['built_up_area']=4; 
                    }
                    else if($post['built_up_area'] >= 100001 && $post['built_up_area'] <= 250000){
                        $post['built_up_area']=5;
                    }
                    else if($post['built_up_area'] >= 250001 && $post['built_up_area'] <= 500000){
                        $post['built_up_area']=6;
                    } 
                    else if($post['built_up_area'] >= 500001 && $post['built_up_area'] <= 1000000){
                        $post['built_up_area']=7;
                    }
                    else if($post['built_up_area'] >= 1000000) {
                        $post['built_up_area']=8;
                    }

                    if($post['built_up_area'] < 1){
                        $points +=0;
                    }else {
                        $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAutoIncome()[round($post['built_up_area'])];
                        
                        $result = QuotationFeeMasterFile::find()
                            ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
                            
                        if ($result != null) {
                            $points += $this->getPercentageAmount($result['values'], $base_fee);
                        } else {
                        }
                    }

            }
            
            // else{

            //     if($post['built_up_area'] >=1 && $post['built_up_area'] <= 2000){
            //         $post['built_up_area']=1;
            //     }
            //     else if($post['built_up_area'] >= 2001 && $post['built_up_area'] <= 4000){
            //         $post['built_up_area']=2;
            //     }
            //     else if($post['built_up_area'] >= 4001 && $post['built_up_area'] <= 6000){
            //         $post['built_up_area']=3;
            //     }
            //     else if($post['built_up_area'] >= 6001 && $post['built_up_area'] <= 8000){
            //         $post['built_up_area']=4;
            //     }
            //     else if($post['built_up_area'] >= 8001) {
            //         $post['built_up_area']=5;
            //     }
            //     if($post['built_up_area'] < 1){
            //         $points +=0;
            //     }else {


            //         $sub_heading = yii::$app->quotationHelperFunctions->getBuiltUpAreaArrAuto()[round($post['built_up_area'])];

            //         $result = QuotationFeeMasterFile::find()
            //             ->where(['heading' => 'built_up_area_of_subject_property', 'sub_heading' => $sub_heading,'approach_type'=>$post['approach_type']])->one();
            //         if ($result != null) {
            //            /* echo $this->getPercentageAmount($result['values'], $base_fee);
            //             die;*/
            //             $points += $this->getPercentageAmount($result['values'], $base_fee);
            //         } else {
            //         }
            //     }
            // }



           /* if( $post['key'] == '11') {
                echo $post['built_up_area'];
                die;
            }*/

            
            //number_of_comparables
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            
            //number of units 
            $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
            // echo "hello"; die();
            $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
            if ($result != null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            } else {
                $points += 0;
            }


            return $points;

            //next process

            //Payment Terms
            $advance_payment =0;
            $sub_heading_data = explode('%',$post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'payment_terms', 'sub_heading'=>$sub_heading_data[0],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            }else{
            }



            /*   echo $points;
               die;*/








            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'new_repeat_valuation_data', 'sub_heading'=>$name_repeat_valuation,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }








            //number of units 3,7,22 buildings wali han......

            if($post['built_up_area'] > 0 && ($approach_type == 'income' ||  $approach_type == 'profit')){

            }else {


                if ($property_detail->no_of_units_fee == 1) {
                    $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
                    // echo "hello"; die();
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $num_units, 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $result['values'];

                        // yii::info('units building if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
                    } else {
                        // echo "12"; die();
                        $points += 0;
                    }
                }
            }





            // if($post['approach_type'] == "profit"){

            //     if ($post['last_3_years_finance']== 1) {
            //         // echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'last_3_years_finance', 'sub_heading'=>'no','approach_type'=>'profit'])->one();

            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            //     if ($post['projections_10_years'] == 1) {
            //        //  echo "land"; die();
            //         $result = QuotationFeeMasterFile::find()->where(['heading'=>'ten_years_projections', 'sub_heading'=>'no','approach_type'=>'profit'])->one();
            //         if ($result!=null) {
            //             $points += $result['values'];
            //             // yii::info('units building else  condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            //         }else{
            //             // echo "13"; die();
            //             $points += 0;
            //         }

            //     }
            // }

            /*  echo $points;
              die;*/



            // elseif($post['property']==4 || $post['property']==5 ||
            // 	$post['property']==11 || $post['property']==23 || $post['property']==26 || $post['property']==29){
            // echo "land wali else if me";
            //die();
            // }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }

    public function getAmount_autoPME($post)
    {
        // dd($post);
        $points =0;
       
        $post['property_type']= 'pme';
        $post['approach_type']= 'pme';
        // $post['bcs_type']='bca';

        
        $ponits=0;
        $base_fee=0;
        if ($post['asset_category']!=null && $post['no_of_asset']!=null && $post['approach_type']!=null) {
            
            //get bcs details
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'asset_category','approach_type'=>$post['approach_type'],'sub_heading'=>$post['asset_category'] ])->one();
            if($result !=null){
                $points += $result['values'];
                $base_fee = $result['values'];
            }
            
            //client Type (%)
            // $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'client_type', 'sub_heading'=>$post['clientType'],'approach_type'=>$post['approach_type'] ])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'],$base_fee);
            // }else{
            // }  

            
            // asset quantity value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'no_of_asset', 'sub_heading'=>$post['no_of_asset'],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }
            
            
            // asset complexity value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'asset_complexity', 'sub_heading'=>$post['asset_complexity'],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }
            
            

            //city value (%)
            if($post['type_of_valuation']== 1 || $post['type_of_valuation'] == 2) {
                if ($post['location'] == 3506 || $post['location'] == 4260) {
                    $locationkey = 'abu_dhabi';
                } else if ($post['location'] == 3510) {
                    $locationkey = 'dubai';
                } else if ($post['location'] == 3507) {
                    $locationkey = 'ajman';
                } else if ($post['location'] == 3509 || $post['location'] == 3512) {
                    $locationkey = 'sharjah';
                } else if ($post['location'] == 3511 || $post['location'] == 3508) {
                    $locationkey = 'rak-fuj';
                } else {
                    $locationkey = 'others';
                }
          
                
                $result = QuotationFeeMasterFile::find()->where(['heading' => 'location', 'sub_heading' => $locationkey, 'approach_type' => $post['approach_type']])->one();
                if ($result != null) {

                    $points += $this->getPercentageAmount($result['values'], $base_fee);
                } else {
                    $result = QuotationFeeMasterFile::find()->where(['heading' => 'location', 'sub_heading' => 'others', 'approach_type' => $post['approach_type']])->one();
                    if ($result != null) {
                        $points += $this->getPercentageAmount($result['values'], $base_fee);
                    }
                }
            }
            

            // number of location value (%)
            $no_of_location = yii::$app->quotationHelperFunctions->getNumberRange2($post['no_of_location']);
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'no_of_location', 'sub_heading'=>$no_of_location,'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // working days value (%)
            // $working_days = yii::$app->quotationHelperFunctions->getNumberRange1($post['working_days']);
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'working_days', 'sub_heading'=>$working_days,'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            // }else{
            // }


            // asset age value (%)
            $asset_age = yii::$app->quotationHelperFunctions->getNumberRange1($post['asset_age']);
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'asset_age', 'sub_heading'=>$asset_age,'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }
            
            


           


            // complexity value (%)
            // $result = QuotationFeeMasterFile::find()->where(['heading'=>'complexity', 'sub_heading'=>$post['complexity'],'approach_type'=>$post['approach_type']])->one();
            // if($result !=null){
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            // }else{
            // }

            
            

            

            // upgrade value (%)
            // if ($property_detail->upgrade_fee == 1) {
            //     $result = QuotationFeeMasterFile::find()->where(['heading'=>'upgrades_ratings', 'sub_heading'=>$post['upgrades'],'approach_type'=>$post['approach_type']])->one();
            //     if ($result!=null) {
            //         $results = ($result['values'] / 100) * $base_fee;
            //         $points += $this->getPercentageAmount($result['values'], $base_fee);
            //     }else{
            //         $points += 0;
            //     }
            // }


            
            
            //number_of_comparables
            $sub_heading = yii::$app->quotationHelperFunctions->getPaymentTermsArrAuto()[$post['number_of_comparables']];
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'number_of_comparables_data', 'sub_heading'=>$sub_heading,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            // other intedant users value (%)
            $result = QuotationFeeMasterFile::find()->where(['heading'=>'other_intended_users', 'sub_heading'=>$post['other_intended_users'],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                $points += $this->getPercentageAmount($result['values'], $base_fee);
            }else{
            }

            //number of units 
            // $num_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingArrAuto()[$post['no_of_units']];
            // // echo "hello"; die();
            // $result = QuotationFeeMasterFile::find()->where(['heading' => 'number_of_units_building', 'sub_heading' => $post['no_of_units'], 'approach_type' => $post['approach_type']])->one();
            // if ($result != null) {
            //     $points += $this->getPercentageAmount($result['values'], $base_fee);
            // } else {
            //     $points += 0;
            // }

            return $points;

//next process

            //Payment Terms
            $advance_payment =0;
            $sub_heading_data = explode('%',$post['paymentTerms']);
            $result = $result = QuotationFeeMasterFile::find()->where(['heading'=>'payment_terms', 'sub_heading'=>$sub_heading_data[0],'approach_type'=>$post['approach_type']])->one();
            if($result !=null){
                if($post['clientType'] != 'bank') {
                    $advance_payment = $this->getPercentageAmount($result['values'], $total);
                    $total = $total - $advance_payment;
                }
            }else{
            }




            //new repeat_valuation
            $name_repeat_valuation = str_replace('-', '_', $post['repeat_valuation']);
            $result = QuotationFeeMasterFile::find()
                ->where(['heading'=>'new_repeat_valuation_data', 'sub_heading'=>$name_repeat_valuation,'approach_type'=>$post['approach_type']])->one();
            if ($result!=null) {
                $points += $result['values'];
                // yii::info('new rep val if condition > this value is '.$result['values'].' points = '.$points.' ','my_custom_log');
            }else{
                // echo "9"; die();
                return false;
            }



            $tat = $points;
            // yii::info('TAT > = '.$points.' ','my_custom_log');

            $baseFee = $this->getBaseFee($post['property']);
            // yii::info('basefee > = '.$baseFee.' ','my_custom_log');

            $fee = $points+$baseFee;


            // yii::info('Fee > = '.$fee.' ','my_custom_log');
            /*echo $points.'<br>';
            echo $fee.'<br>';
            die;*/
            return [
                'tat'=>$tat,
                'fee'=>$fee,
            ];



        }else{
            return false;
        }
    }


}


?>
