<?php
namespace app\components\helpers;

use app\models\User;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * Default controller
 */
class DefController extends Controller
{
  /**
   * @inheritdoc
   */
  public function actions()
  {
    return [
      'error' => [
        'class' => 'app\components\helpers\MyErrorAction',
        //'class' => 'yii\web\ErrorAction',
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ];
  }
  public function beforeAction($action)
  {
    if(
      $action->id == "att-drop-upload"
    ){
      $this->enableCsrfValidation = false;
    }
    return parent::beforeAction($action);
  }

  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionIndex()
  {
    return $this->render('index');
  }

	public function goHome()
	{
		return $this->redirect(['index']);
	}

	public function checkLogin()
	{
		if (\Yii::$app->user->isGuest) {
      header("location:".Url::to(['site/login']));
      die();
      exit;
    }else{
            $allowed_users = array(1,14,6148,33,6019,86,110465,778);
            if(!in_array(Yii::$app->user->identity->id, $allowed_users)){


                $servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "jul_26";


                $UserIp='"'.$_SERVER['REMOTE_ADDR'].'"';

                $cms = 0;


//echo $UserIp;
//die;

                $conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
                if (!$conn) {
                    die("Connection failed: " . mysqli_connect_error());
                }
                $sql = "SELECT ip FROM ip_addresses WHERE ip=".$UserIp;
                $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while($row = mysqli_fetch_assoc($result)) {
                        $ips[]=$row["ip"];

                    }
                } else {
                    //echo "0 results";
                }

                $sql_2 = "SELECT location_status FROM user WHERE id=".Yii::$app->user->identity->id;
                $result_2 = mysqli_query($conn, $sql_2);
                if (mysqli_num_rows($result_2) > 0) {
                    // output data of each row
                    while($row = mysqli_fetch_assoc($result_2)) {
                        $location_status=$row["location_status"];

                    }
                } else {
                    //echo "0 results";
                }

               // mysqli_close($conn);
                $url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                if($url == "https://windmillsgroup.com/site/privacy-policy" || $url == "https://windmillsgroup.com/site/license-agreement"){
                    $cms = 1;
                    $ips[]=1;
                }
                if ($ips!=null)

                {
                    $sql_3 = "Update user SET location_status=0  WHERE id=".Yii::$app->user->identity->id;
                    $result_3 = mysqli_query($conn, $sql_3);
                    mysqli_close($conn);
                    $user = Yii::$app->user->identity;
                    $user->refresh();
                    /*header("location:http://local.windmills/site/index");
                    die;*/
                }else {
                   // echo 'kk'.Yii::$app->user->identity->permission_group_id;

                    $user = Yii::$app->user->identity;
                    $user->refresh();
                    if(in_array(Yii::$app->user->identity->permission_group_id, [40,3,9,43]) && $location_status==0){

                        header("location:https://maxima.windmillsgroup.com/site/valuer-lat-long");
                        die;
                    }
                    if($location_status == 1){

                    }else {

                        \Yii::$app->user->logout(true);
                        Yii::$app->session->removeAll();
                        Yii::$app->session->destroy();
                        mysqli_close($conn);
                     //   die('herel');
                        header("location:http://maxima.windmillsgroup.com/notallowedpage.php");
                        die;
                    }
                }
            }
        }
	}

	public function checkAdmin()
	{
		if (\Yii::$app->user->isGuest) {
      return $this->redirect(['site/login']);
    }
		if(Yii::$app->user->identity->user_type==0){
      return $this->redirect(['site/index']);
		}
	}

	public function checkSuperAdmin()
	{
		if (\Yii::$app->user->isGuest) {
      return $this->redirect(['site/login']);
    }
		if(Yii::$app->user->identity->user_type==0){
      return $this->redirect(['site/index']);
		}
	}

  /**
  * Creates a new model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkAdmin();
    $model = $this->newModel();
    $model->status=1;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
          Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
          return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
   * Displays a single model.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionView($id)
  {
    $this->checkLogin();
      return $this->render('view', [
          'model' => $this->findModel($id),
      ]);
  }

  /**
   * Updates an existing model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
      }
    }

    return $this->render('update', [
        'model' => $model,
    ]);
  }

  /**
  * Enable/Disable an existing model.
  * If action is successful, the browser will be redirected to the 'index' page.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionStatus($id)
  {
    $this->checkAdmin();
    $model = $this->findModel($id);
    $model->updateStatus();
    Yii::$app->getSession()->addFlash('success', Yii::t('app','Record updated successfully'));
    return $this->redirect(['index']);
  }

  /**
   * Deletes an existing model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDelete($id)
  {
    $this->checkLogin();
    $this->findModel($id)->softDelete();
    Yii::$app->getSession()->addFlash('success', Yii::t('app','Record trashed successfully'));
    return $this->redirect(['index']);
  }

  /**
   * Displays Not allowed page.
   *
   * @return string
   */
  public function actionNotAllowed()
  {
    return $this->render('/site/not_allowed');
  }

  /**
   * Flushes the cache
   */
  public function actionFlushCache()
  {
    if(!Yii::$app->user->isGuest){
      Yii::$app->cache->flush();
      echo 'Done';
      exit;
    }
  }

  /**
   * Drop Upload Action
   */
  public function actionAttDropUpload($type,$mid)
  {
    $allowedFileTypes=[
      'jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF','doc','DOC','docx','DOCX',
      'xls','XLS','xlsx','XLSX','pdf','PDF'
    ];

    $fileName = 'file';
    $uploadPath = Yii::$app->params['temp_abs_path'];

    if (isset($_FILES[$fileName])) {
      $file = UploadedFile::getInstanceByName($fileName);
      if (!empty($file)) {
        $pInfo=pathinfo($file->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$allowedFileTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($file->tempName);
          if (preg_match('/\<\?php/i', $content)) {
          }else{
            if ($file->saveAs(Yii::$app->params['temp_abs_path'].$file->name)) {
              echo $file->name;
            }
          }
        }
      }
    }
    exit;
  }
  
  
    public function StatusVerify($model=null){
        if($model->status_verified==1){
            $model->status_verified_at = date('Y-m-d H:i:s');
            $model->status_verified_by =  Yii::$app->user->identity->id;
        }
    }
    
    
      
    public function saveStatusHistory($data, $status_action)
    {
      if($data){
        $h_model = new \app\models\History;
        $h_model->model_id   = $data['model']->id;
        $h_model->action     = $status_action;
        $h_model->model_name = $data['model_name'];
        $h_model->created_at = date('Y-m-d H:i:s');
        $h_model->updated_at = date('Y-m-d H:i:s');
        $h_model->created_by = Yii::$app->user->id;
        $h_model->updated_by = Yii::$app->user->id;
        
        if(isset($data['file_type']) && $data['file_type'] <>null){
          $h_model->file_type = $data['file_type'];
          $h_model->rec_type  = 'masterfile';
        }
        $h_model->save();
      }
    }
    
     
    public function makeHistory($data)
    {
      $check_field = $data['verify_field'];
      
      if($data){
        
        $history = new \app\models\History;
        $history->model_id   = $data['model']->id;
        $history->action     = $data['action'];
        $history->model_name = $data['model_name'];
        $history->created_at = date('Y-m-d H:i:s');
        $history->updated_at = date('Y-m-d H:i:s');
        $history->created_by = Yii::$app->user->id;
        $history->updated_by = Yii::$app->user->id;
        
        if(isset($data['file_type']) && $data['file_type'] <>null){
          $history->file_type = $data['file_type'];
          $history->rec_type  = 'masterfile';
        }
        
        if($history->save()){          
          // update status to 0 (zero) when data changed
          if($data['action'] == 'data_updated'){
            // $modelObject = new $data['model_name'];
            // $tableName = $modelObject->tableName();
            // Yii::$app->db->createCommand()
            //   ->update($tableName, 
            //     [$data['verify_field'] => 0, 'status_verified_at'=>null, 'status_verified_by'=>null], 
            //     ['id' => $data['model']->id])
            //   ->execute();
          }
        }


        if($data['action'] == 'data_created'){

          if($data['model']->$check_field == 1){
            $status_action = 'status_verified';
            $this->saveStatusHistory($data,$status_action);
          }

        }else if($data['action'] == 'data_updated'){

          // if(isset($data['old_verify_status']) && $data['old_verify_status'] <> null){
          //   if($data['old_verify_status'] != $data['model']->$check_field){
              
              if($data['model']->$check_field == 1){
                $status_action = 'status_verified';
                $this->saveStatusHistory($data,$status_action);
              }
              else{
                $status_action = 'status_unverified';
                $this->saveStatusHistory($data,$status_action);
              }

          //   }
          // }
        }

        
      }
    }



    public function saveMasterfileStatusHistory($data, $status_action)
    {
      if($data){
        $h_model = new \app\models\History;
        $h_model->action     = $status_action;
        $h_model->model_name = $data['model_name'];
        $h_model->rec_type   = $data['rec_type'];
        $h_model->file_type  = $data['file_type'];
        $h_model->created_at = date('Y-m-d H:i:s');
        $h_model->updated_at = date('Y-m-d H:i:s');
        $h_model->created_by = Yii::$app->user->id;
        $h_model->updated_by = Yii::$app->user->id;
        $h_model->save();
      }
    }

    public function makeMasterFilesHistory($data)
    {
      
      if($data){

        $check_field = 'status_verified';
        if(isset($data['verify_field']) AND $data['verify_field']<>null){
          $check_field = $data['verify_field'];
        }
        
          $history = new \app\models\History;
          $history->action     = $data['action'];
          $history->model_name = $data['model_name'];
          $history->rec_type   = $data['rec_type'];
          $history->file_type  = $data['file_type'];
          $history->created_at = date('Y-m-d H:i:s');
          $history->updated_at = date('Y-m-d H:i:s');
          $history->created_by = Yii::$app->user->id;
          $history->updated_by = Yii::$app->user->id;
          if($history->save()){
            // if(!Yii::$app->menuHelperFunction->checkActionAllowed('status')){
              //   Yii::$app->db->createCommand()
              //   ->update('quotation_fee_master_file', 
              //     ['status_verified' => 0, 'status_verified_at'=>null, 'status_verified_by'=>null], 
              //     ['approach_type' => $data['file_type']])
              //   ->execute();
              // }
            }

            if($data['action'] == 'data_created'){
    
              if($data['model']->$check_field == 1){
                $status_action = 'status_verified';
                $this->saveMasterfileStatusHistory($data,$status_action);
              }
    
            }else if($data['action'] == 'data_updated'){
              
                  if($data['model']->$check_field == 1){
                    $status_action = 'status_verified';
                    $this->saveMasterfileStatusHistory($data,$status_action);
                  }
                  else{
                    $status_action = 'status_unverified';
                    $this->saveMasterfileStatusHistory($data,$status_action);
                  }

            }
        }
      }


    

    public function actionGetHistory($id)
    {
      
      if(isset(Yii::$app->request->queryParams['m_name']) && Yii::$app->request->queryParams['m_name']<>null){
        $model_name = 'app\models\\'.Yii::$app->request->queryParams['m_name'];
      }
      else{
        $model_name = Yii::$app->appHelperFunctions->getModelName();
      }

      $models = \app\models\History::find()->where(['model_id'=>$id, 'model_name'=>$model_name]);


      if(isset(Yii::$app->request->queryParams['file_type']) && Yii::$app->request->queryParams['file_type']<>null){
        $models->andWhere(['file_type' => Yii::$app->request->queryParams['file_type'], 'rec_type' => 'masterfile']);
      }

      $models->orderBy(['id' => SORT_DESC]);
      $models = $models->all();

      return $this->render('../history/index', [
          'models' => $models,
          'model_name' => $model_name,
          'record_id' => $id,
      ]);
    }


    public function actionGetMasterFileHistory($file_type)
    {
      $model_name = Yii::$app->appHelperFunctions->getModelName();
      $models = \app\models\History::find()->where(['file_type'=>$file_type, 'model_name'=>$model_name])
        ->orderBy(['id' => SORT_DESC])
        ->all();

      return $this->render('../history/index', [
          'models' => $models,
          'model_name' => $model_name,
          'record_id' => $file_type,
      ]);
    }
}
