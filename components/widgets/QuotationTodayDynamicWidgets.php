<?php

namespace app\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use Yii;
use app\models\CrmQuotations;
use yii\db\Expression;

class QuotationTodayDynamicWidgets extends Widget
{
    
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        $data = [];
        $today_date = date("Y-m-d");
        $q_tbl = CrmQuotations::tableName();
        $from_date = date("Y-m-d")." 00:00:00";
        $to_date   = date("Y-m-d")." 23:59:59";
        
        $data['inquiry_overview'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();
        
        $data['inquiry_recieved'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere([$q_tbl.'.quotation_status' => 0])
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();
        
        $data['quotation_approved'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere([$q_tbl.'.quotation_status' => 2])
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();
        
        //wrong query ->where([$q_tbl.'.trashed' => "54678"])
        $data['document_requested'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere([$q_tbl.'.quotation_status' => 45789])
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();
        
        $data['quotation_sent'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere([$q_tbl.'.quotation_status' => 1])
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();

        //wrong query ->where([$q_tbl.'.trashed' => "54678"])
        $data['quotation_accepted'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere([$q_tbl.'.quotation_status' => 98756])
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();
        
        $data['toe_sent'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere([$q_tbl.'.quotation_status' => 3])
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();
        
        $data['toe_signed_and_received'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere([$q_tbl.'.toe_signed_and_received' => 5])
        ->andFilterWhere(['like', $q_tbl.'.toe_signed_and_received_date', $today_date])
        ->asArray()->one();
        
        $data['payment_received'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere([$q_tbl.'.quotation_status' => 6])
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();
        
        $data['quotation_rejected'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere([$q_tbl.'.quotation_status' => 7])
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();
        
        $data['toe_rejected'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere([$q_tbl.'.quotation_status' => 8])
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();
        
        $data['regretted'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(final_quoted_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere([$q_tbl.'.quotation_status' => 12])   
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();
       
        
        
        echo \Yii::$app->view->renderFile('@app/widgets/quotation-today-dynamic-widgets.php',[
            'data' => $data,
        ]);  
    }
}