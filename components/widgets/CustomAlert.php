<?php
namespace app\components\widgets;

use Yii;
use yii\bootstrap\Alert;
use yii\helpers\Html;

/**
* Extended Alert Widget to ignore yii bootstrap
*
* @author Naeem Awan <malick.naeem@gmail.com>
* @since 1.0
*/
class CustomAlert extends Alert
{
  /**
  * Renders the widget.
  */
  public function run()
  {
      echo "\n" . $this->renderBodyEnd();
      echo "\n" . Html::endTag('div');
  }

  /**
   * Initializes the widget options.
   * This method sets the default values for various options.
   */
  protected function initOptions()
  {
      Html::addCssClass($this->options, ['alert', 'in']);

      if ($this->closeButton !== false) {
          $this->closeButton = array_merge([
              'data-dismiss' => 'alert',
              'aria-hidden' => 'true',
              'class' => 'close',
          ], $this->closeButton);
      }
  }
}
