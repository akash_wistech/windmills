<?php

namespace app\components\widgets;

use Yii;
use yii\grid\GridView;
use yii\helpers\Html;

class CustomGridView extends GridView
{
	public $cardTitle = '';
	public $cardHeader = true;
	public $showPerPage = false;
	public $createBtn = false;
	public $createtamleekBtn = false;
	public $createinjazBtn = false;
	public $createontimeBtn = false;
	public $createtabureBtn = false;
	public $createfasttrackBtn = false;
	public $createapprovedprBtn = false;
	public $createIncomeBtn = false;
	public $createBtnp = false;
	public $mailBtn = false;
	public $showSummary = true;
	public $selected_data = [];
	public $createBtnLink = ['create'];
	public $createBtnLinkTamleek = ['create-tamleek'];
	public $createBtnLinkInjaz = ['create-injaz'];
	public $createBtnLinkOntime = ['create-ontime'];
	public $createBtnLinkTabure = ['create-tabure'];
	public $createBtnLinkFasttrack = ['create-fasttrack'];
	public $createBtnLinkApprovedpr = ['create-approvedpr'];
	public $createIncomeBtnLink = ['income-create'];
	public $createpBtnLink = ['create-prospect'];
	public $tableOptions = ['class' => 'table'];

	public $export = false;
	public $import = false;
	public $custom_value = '';

	public $importContacts = false;

	public $deleteAllbtn = false;
	public $deleteAllbtnLink = ['delete-all'];
	public $activeAllbtn = false;
	public $activeAllbtnLink = ['active-all'];


	public $advance_search = false;


	/**
	 * Initializes the grid view.
	 * This method will initialize required property values and instantiate [[columns]] objects.
	 */
	public function init()
	{
		$this->tableOptions = ['class' => 'table table-responsive-lg table-sm table-striped table-hover mb-0'];
		if (isset($this->filterModel) && $this->filterModel <> null) {
			$this->filterSelector = '#' . Html::getInputId($this->filterModel, 'pageSize');
			$this->filterSelector = '#filter-pageSize';
		}
		$Btns = '';


		if ($this->advance_search == true) {
			$Btns .= '<button type="button" class="btn btn-secondary btn-sm mx-2 " data-toggle="modal" data-target="#advance-search"> Advance Search</button>';
		}

		if ($this->createBtn == true) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-plus"></i>', $this->createBtnLink, ['class' => 'btn btn-xs btn-primary', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Add New')]);
		}
		if ($this->createtamleekBtn == true) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-plus"></i>', $this->createBtnLinkTamleek, ['class' => 'btn btn-xs btn-primary', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Add New')]);
		}
		if ($this->createinjazBtn == true) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-plus"></i>', $this->createBtnLinkInjaz, ['class' => 'btn btn-xs btn-primary', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Add New')]);
		}
		if ($this->createontimeBtn == true) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-plus"></i>', $this->createBtnLinkOntime, ['class' => 'btn btn-xs btn-primary', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Add New')]);
		}
		if ($this->createtabureBtn == true) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-plus"></i>', $this->createBtnLinkTabure, ['class' => 'btn btn-xs btn-primary', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Add New')]);
		}
		if ($this->createfasttrackBtn == true) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-plus"></i>', $this->createBtnLinkFasttrack, ['class' => 'btn btn-xs btn-primary', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Add New')]);
		}
		if ($this->createapprovedprBtn == true) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-plus"></i>', $this->createBtnLinkApprovedpr, ['class' => 'btn btn-xs btn-primary', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Add New')]);
		}

		if ($this->createIncomeBtn == true) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-plus"></i>', $this->createIncomeBtnLink, ['class' => 'btn btn-xs btn-primary', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Add New')]);
		}

		if ($this->createBtnp == true) {
			$Btns .= Html::a('<i class="fas fa-plus"></i>', $this->createpBtnLink, ['class' => 'btn btn-xs btn-primary', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Add New')]);
		}
		if ($this->mailBtn == true) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-envelope"></i>', 'javascript:;', ['id' => 'btn-mailing', 'class' => 'btn btn-xs btn-primary', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Send Email')]);
		}
		if ($this->import == true && Yii::$app->menuHelperFunction->checkActionAllowed('import')) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-file-import"></i>', ['import'], ['class' => 'btn btn-xs btn-success', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Import')]);
		}

		if ($this->importContacts == true) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-file-import"></i>', ['import'], ['class' => 'btn btn-xs btn-success', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Import')]);
		}

		if ($this->export == true && Yii::$app->menuHelperFunction->checkActionAllowed('export')) {
			$Btns .= '&nbsp;' . Html::a('<i class="fas fa-download"></i>', ['export'], ['class' => 'btn btn-xs btn-info', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'data-title' => Yii::t('app', 'Export')]);
		}

		if ($this->deleteAllbtn == true) {
			$Btns .= '&nbsp;' . Html::a('<button class="btn btn-sm btn-danger del-all-btn">Delete All</button>', $this->deleteAllbtnLink, ['class' => '', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'title' => 'Delete All Quotations', 'data-title' => Yii::t('app', 'Delete All Quotations')]);
		}
		if ($this->activeAllbtn == true) {
			$Btns .= '&nbsp;' . Html::a('<button class="btn btn-sm btn-secondary ative-all-btn">Active All</button> ', $this->activeAllbtnLink, ['class' => '', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'title' => 'Active All Quotations', 'data-title' => Yii::t('app', 'Active All Quotations')]);
		}


		$carHeaderHtml = '';
		if ($this->cardHeader == true) {
			$carHeaderHtml = '
			<header class="card-header">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<h2 class="card-title">' . ($this->cardTitle != '' ? $this->cardTitle : $this->view->title) . '</h2>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="clearfix">
							<div class="float-right">
								' . $Btns . '
							</div>
							<div class="float-right" style="margin-right:10px;">
								' . ($this->showPerPage == true ? Html::activeDropDownList($this->filterModel, 'pageSize', Yii::$app->params['pageSizeArray'], ['id' => 'filter-pageSize', 'class' => 'form-control input-xs']) : '') . '
							</div>
						</div>
					</div>
				</div>
			</header>';
		} else {
			$carHeaderHtml = '
			<header class="card-header">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="clearfix">
							<div class="pull-right">
								' . $Btns . '
							</div>
							<div class="pull-right" style="margin-right:10px;">
								' . ($this->showPerPage == true ? Html::activeDropDownList($this->filterModel, 'pageSize', Yii::$app->params['pageSizeArray'], ['id' => 'filter-pageSize', 'class' => 'form-control input-xs']) : '') . '
							</div>
						</div>
					</div>
				</div>
			</header>';
		}
		$this->layout = '
		<section class="card' . ($this->cardHeader == true ? ' card card-outline card-primary' : '') . '">
			' . $carHeaderHtml . '
			<div class="card-body tbl-container table-responsive">
				{items}
			</div>
			<div class="card-footer">
				<div class="row">
					' . ($this->showSummary == true ? '<div class="col-sm-3">{summary}</div>' : '') . '
					<div class="col-sm-' . ($this->showSummary == true ? '9' : '12') . ' pager-container">{pager}</div>
				</div>
			</div>
		</section>
		';
		$this->pager = [
			'maxButtonCount' => 5,
			'linkContainerOptions' => ['class' => 'paginate_button page-item'],
			'linkOptions' => ['class' => 'page-link'],
			'prevPageCssClass' => 'previous',
			'disabledPageCssClass' => 'disabled',
			'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link'],
		];
		parent::init();
	}
}
