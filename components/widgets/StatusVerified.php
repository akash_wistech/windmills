<?php

namespace app\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use Yii;

class StatusVerified extends Widget
{
    public $model;
    public $form;
    
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        echo \Yii::$app->view->renderFile('@app/widgets/status-verified.php',[
            'model' => $this->model,
            'form' => $this->form,
        ]);  
        }
    }