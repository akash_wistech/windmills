<?php

namespace app\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use Yii;
use app\models\CrmQuotations;

class QuotationAllTimeWidgets extends Widget
{
    
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        $data=array();
        $date_array = Yii::$app->crmQuotationHelperFunctions->getFilterDates(10);
        $from_date = $date_array['start_date']." 00:00:00";
        $to_date = $date_array['end_date']." 23:59:59";


        $data['inquiry_recieved_all'] = CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['crm_quotations.trashed' => null])
            ->andWhere(['in', 'quotation_status', [0,1,2,3,6,7,8,10,11,12,13]])
            ->asArray()->all();
        $data['inquiry_recieved_all'] = ArrayHelper::map($data['inquiry_recieved_all'] , "totalRecords", "totalFee");



		$data['inquiry_recieved'] = CrmQuotations::find()
		->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['in', 'quotation_status', [0] ])
            ->andWhere(['crm_quotations.trashed' => null])
		->asArray()->all();
		$data['inquiry_recieved'] = ArrayHelper::map($data['inquiry_recieved'] , "totalRecords", "totalFee");

		$data['quotation_sent'] = CrmQuotations::find()
		->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['in', 'quotation_status', [1] ])
            ->andWhere(['crm_quotations.trashed' => null])

		->asArray()->all();
		$data['quotation_sent'] = ArrayHelper::map($data['quotation_sent'] , "totalRecords", "totalFee");

		$data['toe_sent'] = CrmQuotations::find()
		->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['in', 'quotation_status', [3] ])
            ->andWhere(['crm_quotations.trashed' => null])
		->asArray()->all();
		$data['toe_sent'] = ArrayHelper::map($data['toe_sent'] , "totalRecords", "totalFee");

        $data['payment_recieved'] = CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['in', 'quotation_status', [6] ])
            ->andWhere(['crm_quotations.trashed' => null])
            ->asArray()->all();
        $data['payment_recieved'] = ArrayHelper::map($data['payment_recieved'] , "totalRecords", "totalFee");


		$data['toe_Sign_and_Rec'] = CrmQuotations::find()
		->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['toe_signed_and_received' => 1])
            ->andWhere(['crm_quotations.trashed' => null])
		->asArray()->all();
		$data['toe_Sign_and_Rec'] = ArrayHelper::map($data['toe_Sign_and_Rec'] , "totalRecords", "totalFee");



		$data['on_hold'] = CrmQuotations::find()
		->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['in', 'quotation_status', [10] ])
            ->andWhere(['crm_quotations.trashed' => null])
		->asArray()->all();
		$data['on_hold'] = ArrayHelper::map($data['on_hold'] , "totalRecords", "totalFee");


        $data['quotation_approved'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['in', 'quotation_status', [2] ])
            ->andWhere(['crm_quotations.trashed' => null])
            ->asArray()->all();
        $data['quotation_approved'] = ArrayHelper::map($data['quotation_approved'] , "totalRecords", "totalFee");

		$data['quotation_rejected'] = CrmQuotations::find()
		->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['in', 'quotation_status', [7] ])
            ->andWhere(['crm_quotations.trashed' => null])
		->asArray()->all();
		$data['quotation_rejected'] = ArrayHelper::map($data['quotation_rejected'] , "totalRecords", "totalFee");

		$data['toe_rejected']  = CrmQuotations::find()
		->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['in', 'quotation_status', [8] ])
            ->andWhere(['crm_quotations.trashed' => null])
		->asArray()->all();
		$data['toe_rejected'] = ArrayHelper::map($data['toe_rejected'] , "totalRecords", "totalFee");

		$data['cancelled']  = CrmQuotations::find()
		->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['in', 'quotation_status', [11] ])
            ->andWhere(['crm_quotations.trashed' => null])
		->asArray()->all();
		$data['cancelled'] = ArrayHelper::map($data['cancelled'] , "totalRecords", "totalFee");

		$data['regretted']  = CrmQuotations::find()
		->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where([
                'between', 'crm_quotations.created_at', $from_date, $to_date
            ])
            ->andWhere(['in', 'quotation_status', [12] ])
            ->andWhere(['crm_quotations.trashed' => null])
		->asArray()->all();
		$data['regretted'] = ArrayHelper::map($data['regretted'] , "totalRecords", "totalFee");

		$data['closed'] = CrmQuotations::find()
		->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
		->andFilterWhere(['in', 'quotation_status', [13]])
		->asArray()->all();
		$data['closed'] = ArrayHelper::map($data['closed'] , "totalRecords", "totalFee");




        echo \Yii::$app->view->renderFile('@app/widgets/quotation-all-time-widgets.php',[
            'data' => $data,
        ]);  
        }
    }