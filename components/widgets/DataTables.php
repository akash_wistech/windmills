<?php

namespace app\components\widgets;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use app\assets\DataTablesBootstrapAsset;
use app\assets\DataTablesButtonsAsset;

class DataTables extends GridView
{
  public $createBtn = false;
  public $createBtnLink = ['create'];
  /**
  * @var array the HTML attributes for the container tag of the datatables view.
  */
  public $options = [];

  /**
  * @var array the HTML attributes for the datatables table element.
  */
  public $tableOptions = ["class"=>"table table-responsive-md mb-0 table-striped","cellspacing"=>"0", "width"=>"100%"];

  /**
  * @var array the HTML attributes for the datatables table element.
  */
  public $clientOptions = [];


  /**
   * Runs the widget.
   */
  public function run()
  {
      $clientOptions = $this->getClientOptions();
      $view = $this->getView();
      $id = $this->tableOptions['id'];

      //Bootstrap3 Asset by default
      DataTablesBootstrapAsset::register($view);

      //TableTools Asset if needed
      if (isset($clientOptions["tableTools"]) || (isset($clientOptions["dom"]) && strpos($clientOptions["dom"], 'T')>=0)){
          $tableTools = DataTablesButtonsAsset::register($view);
          //SWF copy and download path overwrite
          $clientOptions["tableTools"]["sSwfPath"] = $tableTools->baseUrl."/swf/copy_csv_xls_pdf.swf";
      }
      $options = Json::encode($clientOptions);
      $view->registerJs("jQuery('#$id').DataTable($options);");

      //base list view run
      if ($this->showOnEmpty || $this->dataProvider->getCount() > 0) {
          $content = preg_replace_callback("/{\\w+}/", function ($matches) {
              $content = $this->renderSection($matches[0]);
              return $content === false ? $matches[0] : $content;
          }, $this->layout);
      } else {
          $content = $this->renderEmpty();
      }
      $tag = ArrayHelper::remove($this->options, 'tag', 'div');
      echo Html::tag($tag, $content, $this->options);
  }

  /**
   * Initializes the datatables widget disabling some GridView options like
   * search, sort and pagination and using DataTables JS functionalities
   * instead.
   */
  public function init()
  {
      parent::init();

      //disable filter model by grid view
      $this->filterModel = null;

      //disable sort by grid view
      $this->dataProvider->sort = false;

      //disable pagination by grid view
      $this->dataProvider->pagination = false;

      //layout showing only items
      $this->layout = '
      <section class="card card-featured card-featured-warning">
      '.(
        $this->createBtn==true ?
        '
        <header class="card-header">
          <div style="right: 10px;position: absolute;top: 10px;">
            '.Html::a('<i class="fas fa-plus"></i>',$this->createBtnLink,['class'=>'mb-1 mt-1 mr-1 btn btn-xs btn-success']).'
          </div>
          <h2 class="card-title">'.$this->view->title.'</h2>
        </header>'
        :
        ''
      ).'
        <div class="card-body">
          {items}
        </div>
      </section>
      ';

      //the table id must be set
      if (!isset($this->tableOptions['id'])) {
          $this->tableOptions['id'] = 'datatables_'.$this->getId();
      }
  }

  /**
   * Returns the options for the datatables view JS widget.
   * @return array the options
   */
  protected function getClientOptions()
  {
      return $this->clientOptions;
  }

  public function renderTableBody()
  {
      $models = array_values($this->dataProvider->getModels());
      if (count($models) === 0) {
          return "<tbody>\n</tbody>";
      } else {
          return parent::renderTableBody();
      }
  }
}
