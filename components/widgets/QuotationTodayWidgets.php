<?php

namespace app\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use Yii;
use app\models\CrmQuotations;
use yii\db\Expression;

class QuotationTodayWidgets extends Widget
{
    
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        $data = [];
        $today_date = date("Y-m-d");
        $q_tbl = CrmQuotations::tableName();
        $from_date = date("Y-m-d")." 00:00:00";
        $to_date   = date("Y-m-d")." 23:59:59";
        
        $data['inquiry_recieved'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andFilterWhere(['like', $q_tbl.'.created_at', $today_date])
        ->asArray()->one();
        
        $data['quotation_approved'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andFilterWhere(['like', $q_tbl.'.approved_date', $today_date])
        ->asArray()->one();
        
        //wrong query ->where([$q_tbl.'.trashed' => "54678"])
        $data['document_requested'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => "54678"])
        ->asArray()->one();
        
        $data['quotation_sent'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andFilterWhere(['like', $q_tbl.'.quotation_sent_date', $today_date])
        ->asArray()->one();

        //wrong query ->where([$q_tbl.'.trashed' => "54678"])
        $data['quotation_accepted'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => "54678"])
        ->asArray()->one();
        
        $data['toe_sent'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andFilterWhere(['like', $q_tbl.'.toe_sent_date', $today_date])
        ->asArray()->one();
        
        $data['toe_signed_and_received'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andFilterWhere(['like', $q_tbl.'.toe_signed_and_received_date', $today_date])
        ->asArray()->one();
        
        $data['payment_received'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andFilterWhere(['like', $q_tbl.'.payment_received_date', $today_date])
        ->asArray()->one();
        
        $data['quotation_rejected'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andFilterWhere(['like', $q_tbl.'.quotation_rejected_date', $today_date])
        ->asArray()->one();
        
        $data['toe_rejected'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andFilterWhere(['like', $q_tbl.'.toe_rejected_date', $today_date])
        ->asArray()->one();
        
        $data['regretted'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andFilterWhere(['like', $q_tbl.'.regretted_date', $today_date])
        ->asArray()->one();



        //total ahead of time
        $query = CrmQuotations::find()->where([$q_tbl.'.trashed' => null])
        ->andFilterWhere(['like', $q_tbl.'.quotation_sent_date', $today_date])
        ->andFilterWhere(['like', $q_tbl.'.toe_sent_date', $today_date])
        ->all();

        $ahead_total_records = 0; 
        $ahead_total_amount = 0;
        $ontime_total_records = 0; 
        $ontime_total_amount = 0;
        $delay_total_records = 0; 
        $delay_total_amount = 0;

        foreach($query as $key => $model){
            $created_at_time = strtotime($model->created_at);
            $quotation_sent_time = strtotime($model->quotation_sent_date);
            $toe_sent_time = strtotime($model->toe_sent_date);

            $quotation_minutes_difference = round(($quotation_sent_time - $created_at_time) / 60);
            $toe_hours_difference = round(($toe_sent_time - $created_at_time) / 3600);
            
            if ($quotation_minutes_difference < 15 && $toe_hours_difference < 4) {
                $ahead_total_records += 1; 
                $ahead_total_amount  += $model->toe_final_fee;
            }
            elseif ($quotation_minutes_difference == 15 && $toe_hours_difference == 4) {
                $ontime_total_records += 1; 
                $ontime_total_amount  += $model->toe_final_fee;
            }
            elseif ($quotation_minutes_difference > 15 && $toe_hours_difference > 4) {
                $delay_total_records += 1; 
                $delay_total_amount  += $model->toe_final_fee;
            }
        }
        
        $data['ahead_of_time']['total_records'] = $ahead_total_records;
        $data['ahead_of_time']['total_amount']  = $ahead_total_amount;
        
        $data['on_time']['total_records'] = $ontime_total_records;
        $data['on_time']['total_amount']  = $ontime_total_amount;
        
        $data['delay']['total_records'] = $delay_total_records;
        $data['delay']['total_amount']  = $delay_total_amount;
        
        
        
        echo \Yii::$app->view->renderFile('@app/widgets/quotation-today-widgets.php',[
            'data' => $data,
        ]);  
    }
}