<?php
namespace app\components\widgets;

use Yii;
use yii\widgets\Pjax;

class CustomPjax extends Pjax
{
  public $timeout = 3000;
  /**
   * @inheritdoc
   */
  public function run()
  {
  	$view = $this->getView();
  	$view->registerJs('
  		var _targetContainer="#'.$this->options['id'].'";
  		$.pjax.defaults.timeout = 10000;
  		$(document).on("pjax:timeout", function(event) {
  			event.preventDefault()
  		});
  		$(document).on("pjax:send", function() {
  			App.blockUI({
  				message: "'.Yii::t('app','Please wait...').'",
  				target: _targetContainer,
  				overlayColor: "none",
  				cenrerY: true,
  				boxed: true
  			});
  		});
  		$(document).on("pjax:complete", function() {
        $(".blockUI").remove();
  		});
  	');
  	parent::run();
  }
}
