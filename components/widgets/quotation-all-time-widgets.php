

<div class="card card-outline card-success">
    <div class="card-header">
        <h3 class="card-title">Quotation All Time Widgets</h3>
    </div>
    <div class="card-body" style="background-color:#EEEEEE">


        <div class="row mx-2">
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-info" style="width:90px; height:80px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:60px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;">
                            <h3 class="card-category text-right" style="color:#757575">Inquiry Received
                                (<?= key($data['inquiry_recieved']) ?>)</h3>
                            <h4 class=" float-right" style="color:#757575">Total Fee:
                                <?= (reset($data['inquiry_recieved']) > 0) ? number_format(reset($data['inquiry_recieved'])) : 0 ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                            <a class="position-absolute text-info" style="left:53px;top:5px;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 0]) ?>">
                                <span style="font-size:20px;">Go to Inquiry Received</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:90px; height:80px; background-color:#FFA000;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:60px">business</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;">
                            <h3 class="card-category text-right" style="color:#757575">Quotation Sent
                                (<?= key($data['quotation_sent']) ?>)</h3>
                            <h4 class=" float-right" style="color:#757575">Total Fee:
                                <?= number_format(reset($data['quotation_sent'])) ?></h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#FFA000;">business</i>
                            <a class="position-absolute" style="left:53px;top:5px;color:#FFA000;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 1]) ?>">
                                <span style="font-size:20px;">Go to Quotation Sent</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2" style="width:90px; height:80px; background-color:#00695C">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:60px">money</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;">
                            <h3 class="card-category text-right" style="color:#757575">Toe Sent
                                (<?= key($data['toe_sent']) ?>)</h3>
                            <h4 class=" float-right" style="color:#757575">Total Fee:
                                <?= (reset($data['toe_sent'])>0)?number_format(reset($data['toe_sent'])):0 ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#00695C;">money</i>
                            <a class="position-absolute" style="left:53px;top:5px; " target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 3]) ?>">
                                <span style="font-size:20px; color:#00695C;">Go To Toe Sent</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:90px; height:80px; background-color:#C0CA33;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:60px">business</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;">
                            <h3 class="card-category text-right" style="color:#757575">Payment Received
                                (<?= key($data['payment_recieved']) ?>)</h3>
                            <h4 class=" float-right" style="color:#757575">total Fee:
                                <?= number_format(reset($data['payment_recieved'])) ?></h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#C0CA33;">business</i>
                            <a class="position-absolute" style="left:53px;top:5px;color:#C0CA33;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 6]) ?>">
                                <span style="font-size:20px;">Go to Payment Received</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:90px; height:80px; background-color:#1565C0;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:60px">business</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;">
                            <h3 class="card-category text-right" style="color:#757575">Toe Signed And Received
                                (<?= key($data['toe_Sign_and_Rec']) ?>)</h3>
                            <h4 class=" float-right" style="color:#757575">Total Fee:
                                <?= number_format(reset($data['toe_Sign_and_Rec'])) ?></h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#1565C0;">business</i>
                            <a class="position-absolute" style="left:53px;top:5px;color:#1565C0;"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[toe_signed_and_received]'=> 1]) ?>">
                                <span style="font-size:20px;">Go to Toe Signed And Received</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:90px; height:80px; background-color:#90A4AE;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:60px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;">
                            <h3 class="card-category text-right" style="color:#757575">On Hold
                                (<?= key($data['on_hold']) ?>)</h3>
                            <h4 class=" float-right" style="color:#757575">Total Fee:
                                <?= number_format(reset($data['on_hold'])) ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color: #90A4AE;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #90A4AE;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 10]) ?>">
                                <span style="font-size:20px;">Go to On Hold</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:90px; height:80px; background-color:#1565C0;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:60px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;">
                            <h3 class="card-category text-right" style="color:#1565C0">Approved
                                (<?= key($data['quotation_approved']) ?>)</h3>
                            <h4 class=" float-right" style="color:#1565C0">Total Fee:
                                <?= number_format(reset($data['quotation_approved'])) ?></h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color: #1565C0;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #1565C0;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> [2]]) ?>">
                                <span style="font-size:20px;">Go to Approved</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:90px; height:80px; background-color:#90A4AE;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:60px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;">
                            <h3 class="card-category text-right" style="color:#757575">Quotation Rejected
                                (<?= key($data['quotation_rejected']) ?>)</h3>
                            <h4 class=" float-right" style="color:#757575">Total Fee:
                                <?= number_format(reset($data['quotation_rejected'])) ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color: #90A4AE;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #90A4AE;"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 7 ]) ?>">
                                <span style="font-size:20px;">Go to Quotation Rejected</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:90px; height:80px; background-color:#90A4AE;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:60px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;">
                            <h3 class="card-category text-right" style="color:#757575">TOE Rejected
                                (<?= key($data['toe_rejected']) ?>)</h3>
                            <h4 class=" float-right" style="color:#757575">Total Fee:
                                <?= number_format(reset($data['toe_rejected'])) ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color: #90A4AE;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #90A4AE;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 8 ]) ?>">
                                <span style="font-size:20px;">Go to TOE Rejected</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:90px; height:80px; background-color:#90A4AE;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:60px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;">
                            <h3 class="card-category text-right" style="color:#757575">Cancelled
                                (<?= key($data['cancelled']) ?>)</h3>
                            <h4 class=" float-right">TotalFee:
                                <?= number_format(reset($data['cancelled'])) ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color: #90A4AE;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #90A4AE;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 11 ]) ?>">
                                <span style="font-size:20px;">Go to Cancelled</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-danger"
                            style="width:90px; height:80px; background-color:#90A4AE;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:60px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;">
                            <h3 class="card-category text-right" style="color:red">Regretted
                                (<?= key($data['regretted']) ?>)</h3>
                            <h4 class=" float-right">Total Fee:
                                <?= number_format(reset($data['regretted'])) ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3 " style="font-size:30px; color: red;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #90A4AE;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 12 ]) ?>">
                                <span style="font-size:20px;">Go to Regretted</span> </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>