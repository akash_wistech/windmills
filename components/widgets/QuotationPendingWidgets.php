<?php

namespace app\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use Yii;
use app\models\CrmQuotations;

class QuotationPendingWidgets extends Widget
{
    
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        $data = [];
        $today_date = date("Y-m-d");
        $q_tbl = CrmQuotations::tableName();
        
        $data['inquiry_recieved'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere(['not like', 'created_at', $today_date . '%', false])
        ->andWhere([$q_tbl.'.approved_date' => null])
        ->andWhere([$q_tbl.'.quotation_rejected_date' => null])
        ->andWhere([$q_tbl.'.cancelled_date' => null])
        ->andWhere([$q_tbl.'.payment_received_date' => null])
        ->andWhere([$q_tbl.'.toe_rejected_date' => null])
        ->andWhere(['<>', 'quotation_status', 9])
        ->asArray()->one();
        
        $data['quotation_approved'] = $data['inquiry_recieved'];
        // CrmQuotations::find()
        // ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        // ->where([$q_tbl.'.trashed' => null])
        // ->andWhere(['not like', 'created_at', $today_date . '%', false])
        // ->andWhere(['not', [$q_tbl.'.approved_date' => null]])
        // ->andWhere(['not', [$q_tbl.'.quotation_rejected_date' => null]])
        // ->asArray()->one();
    
        
        
        //wrong query ->where([$q_tbl.'.trashed' => "54678"])
        $data['document_requested'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => "54678"])
        ->asArray()->one();
        
        $data['quotation_sent'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere(['not like', 'created_at', $today_date . '%', false])
        ->andWhere([$q_tbl.'.approved_date' => null])
        ->andWhere([$q_tbl.'.quotation_rejected_date' => null])
        ->andWhere([$q_tbl.'.quotation_sent_date' => null])
        ->andWhere([$q_tbl.'.cancelled_date' => null])
        ->andWhere([$q_tbl.'.toe_rejected_date' => null])
        ->andWhere([$q_tbl.'.toe_sent_date' => null])
        ->andWhere([$q_tbl.'.payment_received_date' => null])
        ->andWhere([$q_tbl.'.on_hold_date' => null])
        ->andWhere(['<>', 'quotation_status', 9])
        ->asArray()->one();


        //wrong query ->where([$q_tbl.'.trashed' => "54678"])
        $data['quotation_accepted'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => "54678"])
        ->asArray()->one();
        
        $data['toe_sent'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere(['not like', 'created_at', $today_date . '%', false])
        ->andWhere([$q_tbl.'.approved_date' => null])
        ->andWhere([$q_tbl.'.quotation_rejected_date' => null])
        ->andWhere([$q_tbl.'.toe_sent_date' => null])
        ->andWhere([$q_tbl.'.toe_rejected_date' => null])
        ->andWhere([$q_tbl.'.on_hold_date' => null])
        ->andWhere(['<>', 'quotation_status', 9])
        ->asArray()->one();
        
        $data['toe_signed_and_received'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere(['not like', 'created_at', $today_date . '%', false])
        ->andWhere([$q_tbl.'.approved_date' => null])
        ->andWhere([$q_tbl.'.quotation_rejected_date' => null])
        ->andWhere([$q_tbl.'.toe_signed_and_received_date' => null])
        ->andWhere(['not', [$q_tbl.'.toe_signed_and_received' => 5]])
        ->asArray()->one();
        
        $data['payment_received'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere(['not like', 'created_at', $today_date . '%', false])
        ->andWhere([$q_tbl.'.approved_date' => null])
        ->andWhere([$q_tbl.'.quotation_rejected_date' => null])
        ->andWhere([$q_tbl.'.payment_received_date' => null])
        ->andWhere([$q_tbl.'.toe_rejected_date' => null])
        ->andWhere([$q_tbl.'.on_hold_date' => null])
        ->andWhere([$q_tbl.'.cancelled_date' => null])
        ->andWhere(['<>', 'quotation_status', 9])
        ->asArray()->one();
        
        $data['regretted'] = CrmQuotations::find()
        ->select(['COUNT(*) AS total_records', 'SUM(toe_final_fee) AS total_amount'])
        ->where([$q_tbl.'.trashed' => null])
        ->andWhere(['not like', 'created_at', $today_date . '%', false])
        ->andWhere(['not', [$q_tbl.'.regretted_date' => null]])
        ->andWhere([$q_tbl.'.approved_date' => null])
        ->andWhere([$q_tbl.'.quotation_rejected_date' => null])
        ->andWhere([$q_tbl.'.cancelled_date' => null])
        ->andWhere(['<>', 'quotation_status', 9])
        ->asArray()->one();


        // dd($data);

        
        echo \Yii::$app->view->renderFile('@app/widgets/quotation-pending-widgets.php',[
            'data' => $data,
        ]);  
    }
}