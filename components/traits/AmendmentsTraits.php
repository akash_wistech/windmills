<?php
namespace app\components\traits;


trait AmendmentsTraits   {

  public function getUniqueReference()
  {
    $previous_record = \app\models\AmendmentRequest::find()
    ->select([
      'id',
      'reference_no',
    ])
    // ->where(['parent_id' => null])
    ->orderBy(['id' => SORT_DESC])->asArray()->one();

    if ($previous_record <> null) {
      $prev_ref = explode("-", $previous_record['reference_no']);

      return 'MA-' . date('Y') . '-' . ($prev_ref[2] + 1);

    } else {
      return 'MA-' . date('Y') . '-1';
    }

  }

  public function getUsers()
  {
      $users = \app\models\User::find()
      ->select(['id', 'concat(firstname," ", lastname) as name'])
      ->orderBy(['name' => SORT_DESC])
      ->asArray()
      ->all();
      echo "<pre>"; print_r($users); echo "</pre>"; die;
  }

  public function getNumOfHoursArr()
  {
    return[
      1 => 'One',
      2 => 'Two',
      3 => 'Three',
      4 => 'Four',
      5 => 'Five',
      6 => 'Six',
      7 => 'Six',
      8 => 'Six',
      9 => 'Six',
      10 => 'Six',
      11 => 'Six',
      12 => 'Six',
      13 => 'Six',
      14 => 'Six',
      15 => 'Six',
      16 => 'Six',
      17 => 'Six',
    ];
  }
  public function getNumOfHoursLable()
  {
    return[
      1 => '<span class="badge grid-badge badge-success">One</span>',
      2 => '<span class="badge grid-badge badge-primary">Two</span>',
      3 => '<span class="badge grid-badge badge-info">Three</span>',
      4 => '<span class="badge grid-badge badge-secondary">Four</span>',
      5 => '<span class="badge grid-badge badge-warning">Five</span>',
      6 => '<span class="badge grid-badge badge-danger">Six</span>',
    ];
  }

  public function getPriorityArr()
  {
    return[
      1 => 1,
      2 => 2,
      3 => 3,
    ];
  }

  public function getPriorityLable()
  {
    return[
      1 => '<span class="badge grid-badge badge-success">1</span>',
      2 => '<span class="badge grid-badge badge-primary">2</span>',
      3 => '<span class="badge grid-badge badge-info">3</span>',
    ];
  }

  public function getYesNoArr()
  {
    return[
      1 => 'Yes',
      2 => 'No',
    ];
  }

  public function getYesNoLable()
  {
    return[
      1 => '<span class="badge grid-badge badge-success">Yes</span>',
      2 => '<span class="badge grid-badge badge-danger">No</span>',
    ];
  }

  public function getDateLable($value='')
  {
    if ($value<>null) {
      return '<span class="badge grid-badge badge-primary "> '.date('Y-m-d', strtotime($value)).'</span>';
    }
  }

  public function getUserName($id='')
  {
    if ($id<>null) {
      $result = \app\models\User::findOne($id);
      if ($result<>null) {
        return $result->firstname.' '.$result->lastname;
      }

    }
  }

}
