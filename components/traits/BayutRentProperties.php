<?php
namespace app\components\traits;


trait bayutRentProperties {

  public static function get_Bed_Bath_Area($bed_bath_area=null)
  {
    if ($bed_bath_area<>null) {
      $no_of_bedrooms = 0; $no_of_bathrooms = 0; $area = 0; $studio ='no';
      foreach ($bed_bath_area as $key => $value) {
        if ($key=='Beds' && $value!='Studio') {
            $explodeNumOfBeds = (explode(" ", $value));
            $no_of_bedrooms  = $explodeNumOfBeds[0];
        }
        if ($key=='Beds' && $value=='Studio') {
            $studio = 'yes';
        }
        if ($key=='Baths') {
            $explodeNumOfBaths = (explode(" ", $value));
            $no_of_bathrooms  = $explodeNumOfBaths[0];
        }
        if ($key=='Area') {
            $explodeArea = (explode(" ",$value));
            $area = str_replace(',', '', $explodeArea[0]);;
        }
      }
      return [
        'no_of_bedrooms' => $no_of_bedrooms,
        'no_of_bathrooms' => $no_of_bathrooms,
        'area' => $area,
        'studio' => $studio,
      ];
    }
  }

    public static function get_Bed_Bath_AreaPlot($area=null)
    {
        if ($bed_bath_area<>null) {
            $no_of_bedrooms = 0; $no_of_bathrooms = 0; $area = 0; $studio ='no';
            foreach ($bed_bath_area as $key => $value) {
                if ($key=='Beds' && $value!='Studio') {
                    $explodeNumOfBeds = (explode(" ", $value));
                    $no_of_bedrooms  = $explodeNumOfBeds[0];
                }
                if ($key=='Beds' && $value=='Studio') {
                    $studio = 'yes';
                }
                if ($key=='Baths') {
                    $explodeNumOfBaths = (explode(" ", $value));
                    $no_of_bathrooms  = $explodeNumOfBaths[0];
                }
                if ($key=='Area') {
                    $explodeArea = (explode(" ",$value));
                    $area = str_replace(',', '', $explodeArea[0]);;
                }
            }
            return [
                'no_of_bedrooms' => $no_of_bedrooms,
                'no_of_bathrooms' => $no_of_bathrooms,
                'area' => $area,
                'studio' => $studio,
            ];
        }
    }

  public static function getPropertyData($propertyInfoArr=null)
  {
    if ($propertyInfoArr<>null) {
        $type = $purpose = $ref_number = $furnishing = $truCheck_on = $added_on = '';
        foreach ($propertyInfoArr as $key => $value) {
          if($key == 'Type'){
            $type = $value;
          }
          if($key == 'Purpose'){
            $purpose = $value;
          }
          if($key == 'Reference no.'){
            $ref_number = $value;
          }
          if($key == 'Furnishing'){
            $furnishing = $value;
          }
          if($key == 'TruCheck on'){
            $truCheck_on = $value;
          }
           if($key == 'Added on'){
             $added_on = date('Y-m-d', strtotime($value));
           }
        }
       // $added_on = date('Y-m-d', strtotime(end($propertyInfoArr)));
        return [
          'type' => $type,
          'purpose' => $purpose,
          'ref_number' => $ref_number,
          'truCheck_on' => $truCheck_on,
          'added_on' => $added_on,
        ];
    }
  }



  public static function getBuaAndLandArea($propertyDescription=null, $MainLandOrBua=null, $no_of_bedrooms=null)
  {
        // echo "<pre>"; print_r($propertyDescription); echo "</pre><br>";  //die();
        // echo "num of bedrooms: ".$no_of_bedrooms."<br>"; die();
      $totalArea = str_replace(',', '', $MainLandOrBua);
      $percentageValue = $totalArea*10/100;
      $addInTotalArea = $totalArea+$percentageValue;
      $removeInTotalArea = $totalArea-$percentageValue;

      $buaWords = [
          'bua','built up','builtup','built-up','b u a','building area','building size','buildup','build up','b.u.a'
      ];
      $PlotWords = [
          'plot','land','total area','plot'
      ];

      $built_up_area ='';
      $land_size ='';

      if (strpos($propertyDescription, 'bua') !== false || strpos($propertyDescription, 'built up') !== false || strpos($propertyDescription, 'builtup') !== false || strpos($propertyDescription, 'built-up') !== false || strpos($propertyDescription, 'building area') !== false ||
          strpos($propertyDescription, 'building size') !== false || strpos($propertyDescription, 'buildup') !== false || strpos($propertyDescription, 'build up') !== false || strpos($propertyDescription, 'b.u.a')) {
            // echo "Bua found in description<br>"; die();
          foreach ($buaWords as $word) {
              $findWord = strpos($propertyDescription, $word);
              if ($findWord<>null) {
                  $nextChar = substr($propertyDescription, $findWord, 20);
                  $val = preg_replace('/[^0-9-,-.]/', '', $nextChar);
                  $finalBUA = str_replace(',', '', $val);
                  if ($val<>null && $val>0) {
                      $built_up_area = $finalBUA;
                      $land_size     = $totalArea;
                  }else{
                      $built_up_area = $totalArea;
                  }
              }
          }
//            print_r($built_up_area); die();
      }
      else{
//             echo "bua not found in description<br>"; die();
          $findInPreviousListing = \app\models\ListingsTransactions::find()
              ->where(['no_of_bedrooms'=>$no_of_bedrooms])
              ->andWhere(['and',['>=', 'built_up_area', $removeInTotalArea],['<=', 'built_up_area', $addInTotalArea]])
              ->asArray()->all();
          // echo "<pre>"; print_r($findInPreviousListing); echo "</pre>"; die();
          if ($findInPreviousListing<>null) {
              $totalBuildUpArea = 0;
              foreach ($findInPreviousListing as $key => $value) {
                  $totalBuildUpArea += $value['built_up_area'];
              }
              $built_up_areaa = $totalBuildUpArea/count($findInPreviousListing);
              $built_up_area = number_format((float)$built_up_areaa, 2, '.', '');
              // echo "average built up area: ".$built_up_area."<br>";
          }else{
              $built_up_area = $totalArea;
              // echo "<br>BUA not found in range now Bua Is: ".$built_up_area."<br>";
          }
      }
      // echo "<br><br>"; // die();

      // plot area array loop

      if (strpos($propertyDescription, 'plot') !== false || strpos($propertyDescription, 'land') !== false || strpos($propertyDescription, 'total area') !== false || strpos($propertyDescription, 'plot') !== false) {
//                echo "plot area fount in description"; die();
          $i=1;
          foreach ($PlotWords as $word) {
              // echo "i is equal to:- ".$i."<br>";
              $findWord = strpos($propertyDescription, $word);
              if ($findWord<>null) {
//                     echo "word find:- ".$word."<br>"; //die();
                  $nextChar = substr($propertyDescription, $findWord, 20);
                  $extraWords = strpos($nextChar, 'parks');
//                    echo $nextChar; echo "<br>";
//                    echo "Hello ".$extraWords; die();
                  if ($extraWords==null){
//                        echo "usaamamama"; //die();
                      $val = preg_replace('/[^0-9-,-.]/', '', $nextChar);
//                     echo "next char: ".$nextChar; echo "<br>";
//                     echo "val: ".$val; echo "<br>"; die();
                      $finalLandSize = str_replace(',', '', $val);
                      if ($val<>null && $val>0) {
                          // echo "val is >0:- ".$val."<br>";
                          $land_size = $finalLandSize;
                      }else{
                          $land_size = $totalArea;
                          // echo "else land size is total area: ".$land_size."<br>";
                      }
                  }

//


              }
              $i++;
          }
//            echo $land_size; die();
      }

      if($land_size==null){
//            echo "plot area not fount in description"; die();
          //if bua not found in description and plot area also not found in description then the area mentioned
          //in bayut is built up area.
          // $built_up_area = $totalArea;
          // echo "Word found but integer not found:- ".$word."<br>";
          $findInPreviousListing = \app\models\ListingsTransactions::find()
              ->where(['no_of_bedrooms'=>$no_of_bedrooms])
              ->andWhere(['and',['>=', 'land_size', $removeInTotalArea],['<=', 'land_size', $addInTotalArea]])
              ->asArray()->all();

          if ($findInPreviousListing<>null) {
              // echo "land size lie in bayut range<br>";
              $totalLandArea = 0;
              foreach ($findInPreviousListing as $key => $value) {
                  $totalLandArea += $value['built_up_area'];
              }
              $landArea = $totalLandArea/count($findInPreviousListing);
              $land_size = number_format((float)$landArea, 2, '.', '');
//                 echo "land size after range is:- ".$land_size."<br>";
          }
          else{
              $land_size = $totalArea;
              // echo "so land size is also equal to bayut area given in website: ".$land_size."<br>";
          }
      }



      //end plot area  loop
      return [
          'built_up_area'=>$built_up_area,
          'land_size'=>$land_size,
      ];

      // echo "<br><br>built up area: ".$built_up_area; echo "<br>";
      // echo "land size: ".$land_size; echo "<br>";
      // die();
  }




  public static function getAgentName($html='')
  {
      $agent_name='';
      if ($html<>null) {
          if (
              strpos($html, 'agent:')!==false
              || strpos($html, 'agent;')!==false
              || strpos($html, 'agen e')!==false
              || strpos($html, 'agene')!==false
              || strpos($html, 'agen t:')!==false
              || strpos($html, "agent'")!==false
              || strpos($html, "t:")!==false
          ) {
              $agentPosition = strpos($html, 'agent:');
              $agentPosition2 = strpos($html, 'agent;');
              $agentPosition3 = strpos($html, 'agen e');
              $agentPosition4 = strpos($html, 'agene');
              $agentPosition5 = strpos($html, 'agen t:');
              $agentPosition6 = strpos($html, "agent'");
              $agentPosition7 = strpos($html, "t:");
              if ($agentPosition<>null && $agentPosition>0) {
                //  echo "222<br>";
                  $nextChar = substr($html, $agentPosition, 35);
                  $chunks = explode("view",$nextChar);
                  $againChunks = explode(":",$chunks[0]);
                  $agent_name = $againChunks[1];
              }else if ($agentPosition2<>null && $agentPosition2>0) {
                //  echo "333<br>";
                  $nextChar2 = substr($html, $agentPosition2, 35);
                  $chunks2 = explode("view",$nextChar2);
                  $againChunks2 = explode(";",$chunks2[0]);
                  $agent_name = $againChunks2[1];
              }else if ($agentPosition3<>null && $agentPosition3>0) {
               //   echo "444<br>";
                  $nextChar3 = substr($html, $agentPosition3, 35);
                  $chunks3 = explode("view",$nextChar3);
                  $againChunks3 = explode(" e",$chunks3[0]);
                  $agent_name = $againChunks3[1];
              }else if ($agentPosition4<>null && $agentPosition4>0) {
              //    echo "555<br>";
                  $nextChar4 = substr($html, $agentPosition4, 35);
                  $chunks4 = explode("view",$nextChar4);
                  $againChunks4 = explode("agene ",$chunks4[0]);
                  $agent_name = $againChunks4[1];
              }else if ($agentPosition5<>null && $agentPosition5>0) {
               //   echo "666<br>";
                  $nextChar5 = substr($html, $agentPosition5, 35);
                  $chunks5 = explode("view",$nextChar5);
                  $againChunks5 = explode(":",$chunks5[0]);
                  $agent_name = $againChunks5[1];
              }else if ($agentPosition6<>null && $agentPosition6>0) {
               //   echo "777<br>";
                  $nextChar6 = substr($html, $agentPosition6, 35);
                  $chunks6 = explode("view",$nextChar6);
                  $againChunks6 = explode("agent'",$chunks6[0]);
                  $agent_name = $againChunks6[1];
              }else if ($agentPosition7<>null && $agentPosition7>0) {
              //    echo "888<br>";
                  $nextChar7 = substr($html, $agentPosition7, 35);
                  $chunks7 = explode("view",$nextChar7);
                  $againChunks7 = explode(":",$chunks7[0]);
                  $agent_name = $againChunks7[1];
              }
              return $agent_name;
          }
          else{}
      }
  }

}
