<?php

namespace app\components\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\models\ActivityLog;
use app\models\User;

class ActiveRecordTsBa extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * Enable / Disable the record
  * @return boolean
  */
  public function updateStatus()
  {
    $status=1;
    if($this->status==1)$status=0;
    $connection = \Yii::$app->db;
    $connection->createCommand("update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
    [
      ':status'=>$status,
      ':updated_at'=>date("Y-m-d H:i:s"),
      ':updated_by'=>Yii::$app->user->identity->id,
      ':id'=>$this->id,
    ])
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, ''.$this->recType.' ('.$this->recTitle.') '.($status==1 ? 'enabled' : 'disabled').' successfully');
    return true;
  }

  /**
  * Mark record as deleted
  * @return boolean
  */
  public function Softdelete()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand("update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
    [
      ':trashed' => 1,
      ':trashed_at' => date("Y-m-d H:i:s"),
      ':trashed_by' => Yii::$app->user->identity->id,
      ':id' => $this->id,
    ])
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, ''.$this->recType.' ('.$this->recTitle.') trashed successfully');
    return true;
  }
}
