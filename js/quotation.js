jQuery(document).ready(function () {

    $("body").on("change", "#crmreceivedproperties-building_info", function () {

        var building_id = $(this).val();
        var url_building = 'buildings/detail/' + building_id;

        heading = $(this).data("heading");
        $.ajax({
            url: url_building,
            dataType: "html",
            success: function (data) {
                var myarray = [1, 17, 28, 37];
                var response_building = JSON.parse(data);



                if (response_building.ptype != 0) {
                    $('#crmreceivedproperties-property_id').val(response_building.ptype).trigger('change');
                } else {
                    $('#crmreceivedproperties-property_id').val(1).trigger('change');
                }

                if (response_building.detail.property_category != 0) {
                    $('#crmreceivedproperties-property_category').val(response_building.detail.property_category).trigger('change');
                } else {
                    $('#crmreceivedproperties-property_category').val(1).trigger('change');
                }

                if (response_building.valuation_approach != 0) {
                    $('#crmreceivedproperties-valuation_approach').val(response_building.valuation_approach).trigger('change');
                } else {
                    $('#crmreceivedproperties-valuation_approach').val(1).trigger('change');
                }
                if (response_building.detail.tenure != 0) {
                    $('#crmreceivedproperties-tenure').val(response_building.detail.tenure).trigger('change');
                } else {
                    $('#crmreceivedproperties-tenure').val(1).trigger('change');
                }
                if (response_building.detail.building_number != '') {
                    $('#crmreceivedproperties-building_number').val(response_building.detail.building_number);
                }
                if (response_building.detail.plot_number != '') {
                    $('#crmreceivedproperties-plot_number').val(response_building.detail.plot_number);
                }
                if (response_building.detail.street != '') {
                    $('#crmreceivedproperties-street').val(response_building.detail.street);
                }
                if (response_building.detail.city != '') {
                    $('#crmreceivedproperties-city').val(response_building.detail.city);
                }

                if (response_building.detail.community != '') {
                    $('#crmreceivedproperties-community').val(response_building.detail.community);
                }
                if (response_building.detail.sub_community != '') {
                    $('#crmreceivedproperties-sub_community').val(response_building.detail.sub_community);
                }



                if (response_building.detail.typical_floors != '') {
                    $('#crmreceivedproperties-typical_floors').val(response_building.detail.typical_floors).trigger('change');
                }
                if (response_building.detail.basement_floors != '') {
                    $('#crmreceivedproperties-basement_floors').val(response_building.detail.basement_floors).trigger('change');
                }
                if (response_building.detail.parking_floors != '') {
                    $('#crmreceivedproperties-parking_floors').val(response_building.detail.parking_floors).trigger('change');
                }
                if (response_building.detail.complexity != '') {
                    var $complexity = response_building.detail.development_type;
                    if ($complexity == "Standard") { $complexity = "standard"; }
                    if ($complexity == "Non-Standard") { $complexity = "non-standard"; }

                    $('#crmreceivedproperties-complexity').val($complexity).trigger('change');
                }
                if (response_building.detail.furnished != '') {
                    $('#crmreceivedproperties-furnished').val(response_building.detail.furnished).trigger('change');
                }
                if (response_building.detail.no_of_units != '') {
                    var $number_of_unit = response_building.detail.no_of_units;

                    $number_of_unit = parseInt($number_of_unit);

                    switch (true) {
                        case ($number_of_unit == 0):
                            $number_of_unit = 0;
                            break;
                        case ($number_of_unit >= 1 && $number_of_unit <= 10):
                            $number_of_unit = 1;
                            break;
                        case ($number_of_unit >= 11 && $number_of_unit <= 25):
                            $number_of_unit = 2;
                            break;
                        case ($number_of_unit >= 26 && $number_of_unit <= 50):
                            $number_of_unit = 3;
                            break;
                        case ($number_of_unit >= 51 && $number_of_unit <= 100):
                            $number_of_unit = 4;
                            break;
                        case ($number_of_unit >= 101 && $number_of_unit <= 250):
                            $number_of_unit = 5;
                            break;
                        case ($number_of_unit >= 251 && $number_of_unit <= 500):
                            $number_of_unit = 6;
                            break;
                        case ($number_of_unit >= 501 && $number_of_unit <= 1000):
                            $number_of_unit = 7;
                            break;
                        case ($number_of_unit > 1000):
                            $number_of_unit = 8;
                            break;
                        // default:
                        //     $number_of_unit = 0;
                        //     break;
                    }

                    $('#crmreceivedproperties-no_of_units').val($number_of_unit).trigger('change');
                }
                if (response_building.detail.swimming_pools != '') {
                    $('#crmreceivedproperties-swimming_pools').val(response_building.detail.pool).trigger('change');
                }
                if (response_building.detail.no_of_gyms != '') {
                    $('#crmreceivedproperties-no_of_gyms').val(response_building.detail.gym).trigger('change');
                }
                if (response_building.detail.no_of_play_area != '') {
                    $('#crmreceivedproperties-no_of_play_area').val(response_building.detail.play_area).trigger('change');
                }
                if (response_building.detail.parking_space != '') {
                    $('#crmreceivedproperties-parking_space').val(response_building.detail.parking_space).trigger('change');
                }
                if (response_building.detail.no_of_jacuzzi != '') {
                    $('#crmreceivedproperties-no_of_jacuzzi').val(response_building.detail.jacuzzi).trigger('change');
                }
                if (response_building.detail.upgrades != '') {
                    $('#crmreceivedproperties-upgrades').val(response_building.detail.upgrades).trigger('change');
                }
                if (response_building.detail.mezzanine_floors != '') {
                    $('#crmreceivedproperties-mezzanine_floors').val(response_building.detail.mezzanine_floors).trigger('change');
                }
                if (response_building.detail.no_of_residential_units != '') {
                    $('#crmreceivedproperties-no_of_residential_units').val(response_building.detail.no_of_residential_units).trigger('change');
                }
                if (response_building.detail.no_of_commercial_units != '') {
                    $('#crmreceivedproperties-no_of_commercial_units').val(response_building.detail.no_of_commercial_units).trigger('change');
                }
                if (response_building.detail.retails_units != '') {
                    $('#crmreceivedproperties-retails_units').val(response_building.detail.no_of_retail_units).trigger('change');
                }
                if (response_building.detail.no_of_unit_types != '') {
                    $('#crmreceivedproperties-no_of_unit_types').val(response_building.detail.no_of_unit_types).trigger('change');
                }
                if (response_building.detail.meeting_rooms != '') {
                    $('#crmreceivedproperties-meeting_rooms').val(response_building.detail.meeting_rooms).trigger('change');
                }
                if (response_building.detail.no_of_health_club_spa != '') {
                    $('#crmreceivedproperties-no_of_health_club_spa').val(response_building.detail.no_of_health_club_spa).trigger('change');
                }
                if (response_building.detail.no_of_bbq_area != '') {
                    $('#crmreceivedproperties-no_of_bbq_area').val(response_building.detail.no_of_bbq_area).trigger('change');
                }
                if (response_building.detail.no_of_schools != '') {
                    $('#crmreceivedproperties-no_of_schools').val(response_building.detail.no_of_schools).trigger('change');
                }
                if (response_building.detail.no_of_clinics != '') {
                    $('#crmreceivedproperties-no_of_clinics').val(response_building.detail.no_of_clinics).trigger('change');
                }
                if (response_building.detail.no_of_sports_courts != '') {
                    $('#crmreceivedproperties-no_of_sports_courts').val(response_building.detail.no_of_sports_courts).trigger('change');
                }
                if (response_building.detail.no_of_mosques != '') {
                    $('#crmreceivedproperties-no_of_mosques').val(response_building.detail.no_of_mosques).trigger('change');
                }
                if (response_building.detail.restaurant != '') {
                    $('#crmreceivedproperties-restaurant').val(response_building.detail.restaurant).trigger('change');
                }
                if (response_building.detail.atms != '') {
                    $('#crmreceivedproperties-atms').val(response_building.detail.atms).trigger('change');
                }
                if (response_building.detail.no_of_coffee_shops != '') {
                    $('#crmreceivedproperties-no_of_coffee_shops').val(response_building.detail.no_of_coffee_shops).trigger('change');
                }
                if (response_building.detail.no_of_sign_boards != '') {
                    $('#crmreceivedproperties-no_of_sign_boards').val(response_building.detail.no_of_sign_boards).trigger('change');
                }
                if (response_building.detail.bars != '') {
                    $('#crmreceivedproperties-bars').val(response_building.detail.bars).trigger('change');
                }
                if (response_building.detail.night_clubs != '') {
                    $('#crmreceivedproperties-night_clubs').val(response_building.detail.night_clubs).trigger('change');
                }



                // if (jQuery.inArray(response_building.ptype, myarray) !== -1) {

                //     $('#land_size').hide();
                // } else {

                //     $('#land_size').show();
                // }

                if (response_building.valuation_approach == 1) {
                    $('.market').show();
                    $('.profit').hide();
                    $('.income').hide();
                    $('.income-profit-cost').hide();
                    $('.income-profit').hide();
                } else if (response_building.valuation_approach == 2) {
                    $('.market').hide();
                    $('.profit').hide();
                    $('.income').show();
                    $('.income-profit-cost').show();
                    $('.income-profit').show();
                }
                else if (response_building.valuation_approach == 3) {
                    $('.market').hide();
                    $('.profit').show();
                    $('.income').hide();
                    $('.income-profit-cost').show();
                    $('.income-profit').show();
                }
                else if (response_building.valuation_approach == 4) {
                    $('.market').hide();
                    $('.profit').show();
                    $('.income').hide();
                    $('.income-profit-cost').show();
                    $('.income-profit').hide();
                } else {
                    $('.market').hide();
                    $('.profit').hide();
                    $('.income').hide();
                    $('.income-profit-cost').hide();
                    $('.income-profit').hide();
                }

                /* if(response_building.detail.city != 0){
                    /!* ("#crmreceivedproperties-city").select2("destroy").val(response_building.detail.city).trigger("change").select2();*!/
 
                     $('#community_value').val(response_building.detail.community);
                     $('#crmreceivedproperties-city').val(response_building.detail.city).trigger('change');
 
                 }else{
                     $('#crmreceivedproperties-city').val(1).trigger('change');
                 }*/

            },
            error: bbAlert
        });
    });


    $("body").on("change", "#crmreceivedproperties-valuation_approach", function () {

        var approach_id = $(this).val();
        if (approach_id == 1) {
            $('.market').show();
            $('.profit').hide();
            $('.income').hide();
            $('.income-profit-cost').hide();
            $('.income-profit').hide();
        } else if (approach_id == 2) {
            $('.market').hide();
            $('.profit').hide();
            $('.income').show();
            $('.income-profit-cost').show();
            $('.income-profit').show();
        }
        else if (approach_id == 3) {
            $('.market').hide();
            $('.profit').show();
            $('.income').hide();
            $('.income-profit-cost').show();
            $('.income-profit').show();
        } else if (approach_id == 4) {
            $('.market').hide();
            $('.profit').show();
            $('.income').hide();
            $('.income-profit-cost').show();
            $('.income-profit').hide();

        } else {
            $('.market').hide();
            $('.profit').hide();
            $('.income').hide();
            $('.income-profit-cost').hide();
            $('.income-profit').hide();
        }
    });



    // $("body").on("change", "#crmreceivedproperties-property_id", function () {
    //     var myarray = [1, 17, 28, 37];
    //     var building_id = $(this).val();
    //     if (jQuery.inArray(building_id, myarray) !== -1) {

    //         $('#land_size').hide();
    //     } else {

    //         $('#land_size').show();
    //     }
    // });





});

// for fields validation 
$(document).ready(function () {

    $('form#w0').on('submit', function (e) {


        var visible_inputs = $(this).find(':input:visible');
        var visible_field_ids = [];

        visible_inputs.each(function () {
            var $input = $(this);

            var field_id = $input.attr('id');
            visible_field_ids.push(field_id);

        });


        
        if (jQuery.inArray("crmreceivedproperties-valuation_approach", visible_field_ids) !== -1) {
            makeTheDropDownFieldMandatory(e, "crmreceivedproperties-valuation_approach", "field-crmreceivedproperties-valuation_approach", "Valuation Approach");
        }
        if (jQuery.inArray("crmreceivedproperties-number_of_comparables", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-number_of_comparables", "field-crmreceivedproperties-number_of_comparables", "Number Of Comparables");
        }

        if (jQuery.inArray("crmreceivedproperties-unit_number", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "crmreceivedproperties-unit_number", "unit_number", "field-crmreceivedproperties-unit_number", "Unit Number");
        }
        if (jQuery.inArray("crmreceivedproperties-built_up_area", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "crmreceivedproperties-built_up_area", "built_up_area", "field-crmreceivedproperties-built_up_area", "Built Up Area");
        }
        if (jQuery.inArray("crmreceivedproperties-net_leasable_area", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "crmreceivedproperties-net_leasable_area", "net_leasable_area", "field-crmreceivedproperties-net_leasable_area", "Net Leasable Area");
        }
        if (jQuery.inArray("crmreceivedproperties-land_size", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "crmreceivedproperties-land_size", "land_size", "field-crmreceivedproperties-land_size", "Land Size");
        }
        if (jQuery.inArray("crmreceivedproperties-floor_number", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "crmreceivedproperties-floor_number", "floor_number", "field-crmreceivedproperties-floor_number", "Floor Number");
        }

        if (jQuery.inArray("crmreceivedproperties-complexity", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-complexity", "field-crmreceivedproperties-complexity", "Property Standard/Complexity");
        }
        if (jQuery.inArray("crmreceivedproperties-typical_floors", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-typical_floors", "field-crmreceivedproperties-typical_floors", "Number of Floors");
        }
        if (jQuery.inArray("crmreceivedproperties-basement_floors", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-basement_floors", "field-crmreceivedproperties-basement_floors", "Number of Basements Floors");
        }
        if (jQuery.inArray("crmreceivedproperties-parking_space", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-parking_space", "field-crmreceivedproperties-parking_space", "Number of Parking Spaces");
        }
        if (jQuery.inArray("crmreceivedproperties-swimming_pools", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-swimming_pools", "field-crmreceivedproperties-swimming_pools", "Number of Swimming Pools");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_jacuzzi", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_jacuzzi", "field-crmreceivedproperties-no_of_jacuzzi", "Number of Jacuzzi");
        }

        if (jQuery.inArray("crmreceivedproperties-upgrades", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-upgrades", "field-crmreceivedproperties-upgrades", "Upgrades Star Rating");
        }
        if (jQuery.inArray("crmreceivedproperties-furnished", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-furnished", "field-crmreceivedproperties-furnished", "Furnished");
        }
        if (jQuery.inArray("crmreceivedproperties-ready", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-ready", "field-crmreceivedproperties-ready", "Ready ?");
        }

        if (jQuery.inArray("crmreceivedproperties-no_of_buildings", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_buildings", "field-crmreceivedproperties-no_of_buildings", "Number of Buildings");
        }
        if (jQuery.inArray("crmreceivedproperties-mezzanine_floors", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-mezzanine_floors", "field-crmreceivedproperties-mezzanine_floors", "Number of Mezzanine Floors");
        }
        if (jQuery.inArray("crmreceivedproperties-parking_floors", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-parking_floors", "field-crmreceivedproperties-parking_floors", "Number of Parking Floors");
        }

        if (jQuery.inArray("crmreceivedproperties-no_of_residential_units", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_residential_units", "field-crmreceivedproperties-no_of_residential_units", "Number of Residential Unit");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_commercial_units", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_commercial_units", "field-crmreceivedproperties-no_of_commercial_units", "Number of Commercial Unit");
        }
        if (jQuery.inArray("crmreceivedproperties-retails_units", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-retails_units", "field-crmreceivedproperties-retails_units", "Number of Retail Unit");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_res_units_value", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_res_units_value", "field-crmreceivedproperties-no_of_res_units_value", "Total Number of Residential Unit");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_com_units_value", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_com_units_value", "field-crmreceivedproperties-no_of_com_units_value", "Total Number of Commercial Unit");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_ret_units_value", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_ret_units_value", "field-crmreceivedproperties-no_of_ret_units_value", "Total Number of Retail Unit");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_warehouse_units_value", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_warehouse_units_value", "field-crmreceivedproperties-no_of_warehouse_units_value", "Total Number of Warehouse Unit");
        }

        if (jQuery.inArray("crmreceivedproperties-no_of_unit_types", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_unit_types", "field-crmreceivedproperties-no_of_unit_types", "Types of Unit");
        }

        if (jQuery.inArray("crmreceivedproperties-meeting_rooms", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-meeting_rooms", "field-crmreceivedproperties-meeting_rooms", "Party Halls/Meeting Rooms");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_health_club_spa", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_health_club_spa", "field-crmreceivedproperties-no_of_health_club_spa", "Number of Health Clubs/Spa");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_bbq_area", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_bbq_area", "field-crmreceivedproperties-no_of_bbq_area", "Number of BBQ Areas");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_play_area", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_play_area", "field-crmreceivedproperties-no_of_play_area", "Number of Children Play Areas");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_gyms", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_gyms", "field-crmreceivedproperties-no_of_gyms", "Number of Gyms");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_schools", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_schools", "field-crmreceivedproperties-no_of_schools", "Number of Schools");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_clinics", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_clinics", "field-crmreceivedproperties-no_of_clinics", "Number of Clinics");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_sports_courts", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_sports_courts", "field-crmreceivedproperties-no_of_sports_courts", "Number of Sports Courts");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_mosques", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_mosques", "field-crmreceivedproperties-no_of_mosques", "Number of Mosque");
        }

        if (jQuery.inArray("crmreceivedproperties-restaurant", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-restaurant", "field-crmreceivedproperties-restaurant", "Number of Restaurants");
        }
        if (jQuery.inArray("crmreceivedproperties-atms", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-atms", "field-crmreceivedproperties-atms", "Number of ATM Machines");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_coffee_shops", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_coffee_shops", "field-crmreceivedproperties-no_of_coffee_shops", "Number of Coffee Shops");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_sign_boards", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_sign_boards", "field-crmreceivedproperties-no_of_sign_boards", "Number of Sign Boards");
        }
        if (jQuery.inArray("crmreceivedproperties-night_clubs", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-night_clubs", "field-crmreceivedproperties-night_clubs", "Number of Night Clubs");
        }
        if (jQuery.inArray("crmreceivedproperties-bars", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-bars", "field-crmreceivedproperties-bars", "Number of Bars");
        }

        if (jQuery.inArray("crmreceivedproperties-civil_drawing_available", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-civil_drawing_available", "field-crmreceivedproperties-civil_drawing_available", "Civil Drawing Available");
        }
        if (jQuery.inArray("crmreceivedproperties-mechanical_drawing_available", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-mechanical_drawing_available", "field-crmreceivedproperties-mechanical_drawing_available", "Mechanical Drawing Available");
        }
        if (jQuery.inArray("crmreceivedproperties-electrical_drawing_available", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-electrical_drawing_available", "field-crmreceivedproperties-electrical_drawing_available", "Electrical Drawing Available");
        }
        if (jQuery.inArray("crmreceivedproperties-plumbing_drawing_available", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-plumbing_drawing_available", "field-crmreceivedproperties-plumbing_drawing_available", "Plumbing Drawing Available");
        }
        if (jQuery.inArray("crmreceivedproperties-hvac_drawing_available", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-hvac_drawing_available", "field-crmreceivedproperties-hvac_drawing_available", "HVAC Drawing Available");
        }



       
        

    });

    

    // on quotations
    makeTheDropDownNonMandatoryOnChange("crmreceivedproperties-valuation_approach", "field-crmreceivedproperties-valuation_approach");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-number_of_comparables", "field-crmreceivedproperties-number_of_comparables");

    makeTheTextFieldNonMandatoryOnChange("crmreceivedproperties-unit_number", "unit_number");
    makeTheTextFieldNonMandatoryOnChange("crmreceivedproperties-built_up_area", "built_up_area");
    makeTheTextFieldNonMandatoryOnChange("crmreceivedproperties-net_leasable_area", "net_leasable_area");
    makeTheTextFieldNonMandatoryOnChange("crmreceivedproperties-land_size", "land_size");

    makeTheSelectField2NonMandatoryOnChange("crmreceivedproperties-floor_number", "field-crmreceivedproperties-floor_number", "floor_number");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-complexity", "field-crmreceivedproperties-complexity");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-typical_floors", "field-crmreceivedproperties-typical_floors");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-basement_floors", "field-crmreceivedproperties-basement_floors");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-parking_space", "field-crmreceivedproperties-parking_space");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-swimming_pools", "field-crmreceivedproperties-swimming_pools");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_jacuzzi", "field-crmreceivedproperties-no_of_jacuzzi");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-upgrades", "field-crmreceivedproperties-upgrades");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-furnished", "field-crmreceivedproperties-furnished");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-ready", "field-crmreceivedproperties-ready");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_buildings", "field-crmreceivedproperties-no_of_buildings");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-mezzanine_floors", "field-crmreceivedproperties-mezzanine_floors");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-parking_floors", "field-crmreceivedproperties-parking_floors");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_residential_units", "field-crmreceivedproperties-no_of_residential_units");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_commercial_units", "field-crmreceivedproperties-no_of_commercial_units");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-retails_units", "field-crmreceivedproperties-retails_units");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_unit_types", "field-crmreceivedproperties-no_of_unit_types");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-meeting_rooms", "field-crmreceivedproperties-meeting_rooms");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_health_club_spa", "field-crmreceivedproperties-no_of_health_club_spa");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_bbq_area", "field-crmreceivedproperties-no_of_bbq_area");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_play_area", "field-crmreceivedproperties-no_of_play_area");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_gyms", "field-crmreceivedproperties-no_of_gyms");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_schools", "field-crmreceivedproperties-no_of_schools");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_clinics", "field-crmreceivedproperties-no_of_clinics");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_sports_courts", "field-crmreceivedproperties-no_of_sports_courts");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_mosques", "field-crmreceivedproperties-no_of_mosques");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-restaurant", "field-crmreceivedproperties-restaurant");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-atms", "field-crmreceivedproperties-atms");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_coffee_shops", "field-crmreceivedproperties-no_of_coffee_shops");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_sign_boards", "field-crmreceivedproperties-no_of_sign_boards");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-night_clubs", "field-crmreceivedproperties-night_clubs");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-bars", "field-crmreceivedproperties-bars");
    
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-civil_drawing_available", "field-crmreceivedproperties-civil_drawing_available");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-mechanical_drawing_available", "field-crmreceivedproperties-mechanical_drawing_available");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-electrical_drawing_available", "field-crmreceivedproperties-electrical_drawing_available");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-plumbing_drawing_available", "field-crmreceivedproperties-plumbing_drawing_available");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-hvac_drawing_available", "field-crmreceivedproperties-hvac_drawing_available");



    function makeTheDropDownFieldMandatory(e, field_id, field_group_class, field_name) {
        if ($('#' + field_id).val() === '') {
            e.preventDefault();
            $("#" + field_id).focus();
            $("." + field_group_class + " .help-block").html(field_name + " cannot be blank.").show();
            $("." + field_group_class).addClass("has-error");
            $("." + field_group_class + " .form-control").css("border-color", "#a94442");
        }
        else {
            $("." + field_group_class + " .help-block").html("").hide();
            $("." + field_group_class).removeClass("has-error");
            $("." + field_group_class + " .form-control").css("border-color", "");
        }
    }

    function makeTheSelectFieldMandatory(e, field_id, field_group_class, field_name) {
        if ($('#' + field_id).val() === '') {
            e.preventDefault();
            $("#" + field_id).focus();
            $("." + field_group_class + " .help-block").html(field_name + " cannot be blank.").show();
            $("." + field_group_class).addClass("has-error");
            $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "#a94442");
        }
        else {
            $("." + field_group_class + " .help-block").html("").hide();
            $("." + field_group_class).removeClass("has-error");
            $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "");
        }
    }

    function makeTheSelectField2Mandatory(e, field_id, field_main_class, field_group_class, field_name) {
        if ($('#' + field_id).val() === '') {
            e.preventDefault();
            $("#" + field_id).focus();
            $("." + field_main_class).addClass("mb-3");
            $("." + field_main_class + " .help-block2").html(field_name + " cannot be blank.").show();
            $("." + field_group_class).addClass("has-error");
            $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "#a94442");
        }
        else {
            $("." + field_main_class).removeClass("mb-3");
            $("." + field_main_class + "  .help-block2").html("").hide();
            $("." + field_group_class).removeClass("has-error");
            $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "");
        }
    }

    function makeTheTextFieldMandatory(e, field_id, field_main_class, field_group_class, field_name) {
        if ($("#" + field_id).val() === "") {
            e.preventDefault();
            $("." + field_main_class + " .help-block2").html(field_name + " cannot be blank.").show();
            $("." + field_main_class).addClass("mb-3");
            $("." + field_group_class).addClass("has-error");
            $("#" + field_id).css("border-color", "#a94442");
        }
        else {
            $("." + field_main_class + " .help-block2").html("").hide();
            $("." + field_main_class).removeClass("mb-3");
            $("." + field_group_class).removeClass("has-error");
            $("#" + field_id).css("border-color", "");
        }
    }

    function makeTheTextField2Mandatory(e, field_id, field_main_class, field_group_class, field_name) {
        if ($("#" + field_id).val() === "") {
            e.preventDefault();
            $("." + field_main_class + " .help-block").html(field_name + " cannot be blank.").show();
            $("." + field_main_class).addClass("mb-3");
            $("." + field_group_class).addClass("has-error");
            $("#" + field_id).css("border-color", "#a94442");
        }
        else {
            $("." + field_main_class + " .help-block").html("").hide();
            $("." + field_main_class).removeClass("mb-3");
            $("." + field_group_class).removeClass("has-error");
            $("#" + field_id).css("border-color", "none");
        }
    }

    function makeTheDropDownNonMandatoryOnChange(field_id, field_group_class) {
        $('#' + field_id).on('change', function () {
            if ($(this).val() !== '') {
                $("." + field_group_class + " .help-block").html("").hide();
                $("." + field_group_class).removeClass("has-error");
                $("." + field_group_class + " .form-control").css("border-color", "");

            }
        });
    }

    function makeTheSelectFieldNonMandatoryOnChange(field_id, field_group_class) {
        $('#' + field_id).on('change', function () {
            if ($(this).val() !== '') {
                $("." + field_group_class + " .help-block").html("").hide();
                $("." + field_group_class).removeClass("has-error");
                $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "");

            }
        });
    }

    function makeTheSelectField2NonMandatoryOnChange(field_id, field_group_class, field_main_class) {
        $("#" + field_id).on("change", function () {
            if ($(this).val() !== "") {
                $("." + field_main_class + " .help-block2").html("").hide();
                $("." + field_main_class).removeClass("mb-3");
                $("." + field_group_class).removeClass("has-error");
                $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "");
            }
        });
    }

    function makeTheTextFieldNonMandatoryOnChange(field_id, field_main_class) {
        $("#" + field_id).on('change', function () {
            if ($(this).val() !== '') {
                $("." + field_main_class + " .help-block2").html("").hide();
                $("#" + field_id).css("border-color", "");
            }
        });
    }

    function makeTheTextField2NonMandatoryOnChange(field_id, field_main_class) {
        $("#" + field_id).on('change', function () {
            if ($(this).val() !== '') {
                $("." + field_main_class + " .help-block").html("").hide();
                $("#" + field_id).css("border-color", "none");
            }
        });
    }




});




function bbAlert(xhr, ajaxOptions, thrownError) {
    Swal.fire(thrownError, xhr.statusText + "\n" + xhr.responseText);
    $(".blockUI").remove();
}

$("body").on("click", ".check-property-verify", function () {
    quotation_id = $(this).data("id");
    title = $(this).data("title");
    url = $(this).data("url");
    shawlTitle = "Please Verify All properties Before";
    shawlTitle = shawlTitle.concat(" ", title);
    var data = { quotation_id: quotation_id };
    var ajaxUrl = 'crm-quotations/check-verification';
    $.ajax({
        url: ajaxUrl,
        data: data,
        method: "post",
        dataType: "html",
        success: function (data) {
            data = JSON.parse(data)
            if (data.msg == "allowAction") {
                window.open(url);
                return false;
            }
            if (data.msg == "actionNotAllowed") {
                swal({
                    title: shawlTitle,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#47a447",
                });
            }
        },
        error: bbAlert
    });
});