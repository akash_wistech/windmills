jQuery(document).ready(function() {

	$("body").on("change", "#listingstransactions-building_info", function () {
		modalId="general-modal";

		var building_id = $(this).val();
		var url_building = 'buildings/detail/'+building_id;

		heading=$(this).data("heading");
        $.ajax({
            url: url_building,
            dataType: "html",
            success: function(data) {

                var response_building = JSON.parse(data);
                //console.log(response_building);

                if(response_building.detail.property_category != 0){
                    $('#listingstransactions-property_category').val(response_building.detail.property_category).trigger('change');
                }else{
                    $('#listingstransactions-property_category').val(1).trigger('change');
                }
                if(response_building.detail.location != null){
                    $('#listingstransactions-location').val(response_building.detail.location).trigger('change');
                }else{
                    $('#listingstransactions-location').val(1).trigger('change');
                }
                if(response_building.detail.tenure != 0){
                    $('#listingstransactions-tenure').val(response_building.detail.tenure).trigger('change');
                }else{
                    $('#listingstransactions-tenure').val(1).trigger('change');
                }
                if(response_building.detail.property_placement != 0){
                    $('#listingstransactions-property_placement').val(response_building.detail.property_placement).trigger('change');
                }else{
                    $('#listingstransactions-property_placement').val(1).trigger('change');
                }
                if(response_building.detail.property_visibility != 0){
                    $('#listingstransactions-property_visibility').val(response_building.detail.property_visibility).trigger('change');
                }else{
                    $('#listingstransactions-property_visibility').val(1).trigger('change');
                }
                if(response_building.detail.property_exposure != 0){
                    $('#listingstransactions-property_exposure').val(response_building.detail.property_exposure).trigger('change');
                }else{
                    $('#listingstransactions-property_exposure').val(1).trigger('change');
                }

                if(response_building.detail.property_condition != 0){
                    $('#listingstransactions-property_condition').val(response_building.detail.property_condition).trigger('change');
                }else{
                    $('#listingstransactions-property_condition').val(1).trigger('change');
                }
                if(response_building.detail.development_type != null){
                    $('#listingstransactions-development_type').val(response_building.detail.development_type).trigger('change');
                }else{
                    $('#listingstransactions-development_type').val('Standard').trigger('change');
                }

                if(response_building.detail.utilities_connected != ''){
                    $('#listingstransactions-utilities_connected').val(response_building.detail.utilities_connected).trigger('change');
                }else{
                    $('#listingstransactions-utilities_connected').val('Yes').trigger('change');
                }


                if(response_building.detail.finished_status != null){
                    $('#listingstransactions-finished_status').val(response_building.detail.finished_status).trigger('change');
                }else{
                    $('#listingstransactions-finished_status').val('Shell & Core').trigger('change');
                }
                if(response_building.detail.pool != null){
                    $('#listingstransactions-pool').val(response_building.detail.pool).trigger('change');
                }else{
                    $('#listingstransactions-pool').val('Yes').trigger('change');
                }
                if(response_building.detail.gym != null){
                    $('#listingstransactions-gym').val(response_building.detail.gym).trigger('change');
                }else{
                    $('#listingstransactions-gym').val('Yes').trigger('change');
                }
                if(response_building.detail.play_area != null){
                    $('#listingstransactions-play_area').val(response_building.detail.play_area).trigger('change');
                }else{
                    $('#listingstransactions-play_area').val('Yes').trigger('change');
                }

                if(response_building.detail.other_facilities != null){
                    var facilities = response_building.detail.other_facilities;
                    var facilities = facilities.split(",");
                    $('#listingstransactions-other_facilities').val(facilities).trigger('change');
                }else{
                    $('#listingstransactions-other_facilities').val([]).trigger('change');
                }
                if(response_building.detail.completion_status != null){
                    $('#listingstransactions-completion_status').val(response_building.detail.completion_status).trigger('change');
                }else{
                    $('#listingstransactions-completion_status').val('').trigger('change');
                }

                if(response_building.detail.white_goods != null){
                    $('#listingstransactions-white_goods').val(response_building.detail.white_goods).trigger('change');
                }else{
                    $('#listingstransactions-white_goods').val('Yes').trigger('change');
                }

                if(response_building.detail.furnished != null){
                    $('#listingstransactions-furnished').val(response_building.detail.furnished).trigger('change');
                }else{
                    $('#listingstransactions-furnished').val('Yes').trigger('change');
                }

                if(response_building.detail.landscaping != null){
                    $('#listingstransactions-landscaping').val(response_building.detail.landscaping).trigger('change');
                }else{
                    $('#listingstransactions-landscaping').val('Yes').trigger('change');
                }

                if(response_building.detail.estimated_age != null){
                    $('#listingstransactions-estimated_age').val(response_building.detail.estimated_age).trigger('change');
                }else{
                    $('#listingstransactions-estimated_age').val(0).trigger('change');
                }
                if(response_building.detail.location_highway_drive != null){
                    $('#listingstransactions-location_highway_drive').val(response_building.detail.location_highway_drive).trigger('change');
                }else{
                    $('#listingstransactions-location_highway_drive').val('').trigger('change');
                }
                if(response_building.detail.location_school_drive != null){
                    $('#listingstransactions-location_school_drive').val(response_building.detail.location_school_drive).trigger('change');
                }else{
                    $('#listingstransactions-location_school_drive').val('').trigger('change');
                }
                if(response_building.detail.location_mall_drive != null){
                    $('#listingstransactions-location_mall_drive').val(response_building.detail.location_mall_drive).trigger('change');
                }else{
                    $('#listingstransactions-location_mall_drive').val('').trigger('change');
                }
                if(response_building.detail.location_sea_drive != null){
                    $('#listingstransactions-location_sea_drive').val(response_building.detail.location_sea_drive).trigger('change');
                }else{
                    $('#listingstransactions-location_sea_drive').val('').trigger('change');
                }
                if(response_building.detail.location_park_drive != null){
                    $('#listingstransactions-location_park_drive').val(response_building.detail.location_park_drive).trigger('change');
                }else{
                    $('#listingstransactions-location_park_drive').val('').trigger('change');
                }

                if(response_building.detail.typical_floors != null){
                    $('#listingstransactions-full_building_floors').val(response_building.detail.typical_floors);
                }else{
                    $('#listingstransactions-full_building_floors').val('');
                }
            },
            error: bbAlert
        });
	});

    $("body").on("click", "#bua_calculation", function () {
        modalId="general-modal";
        var landsize = $("#landsize").val();
        var land_mv = 0;

        var mv = $("#valuationapproversdata-estimated_market_value").val();
        var mvr = $("#valuationapproversdata-estimated_market_rent").val();
        var pmv = $("#valuationapproversdata-parking_market_value").val();
        if ($("#valuationapproversdata-estimated_market_value_land").length > 0) {
            land_mv = $("#valuationapproversdata-estimated_market_value_land").val();
        }
        var bua = $("#bua").val();
        var url_valuation = 'valuation/buacalculation?mv='+mv+'&mr='+mvr+'&pmv='+pmv+'&bua='+bua+'&ls='+landsize+'&lmv='+land_mv;

        heading=$(this).data("heading");
        $.ajax({
            url: url_valuation,
            dataType: "html",
            success: function(data) {

                var response = JSON.parse(data);
                $("#valuationapproversdata-estimated_market_value_sqf").val(response.mv);
                $("#valuationapproversdata-estimated_market_rent_sqf").val(response.mr);
                $("#valuationapproversdata-parking_market_value_sqf").val(response.pmv);
                $("#valuationapproversdata-market_value_sqf_pa").val(response.lsize);
                $("#valuationapproversdata-land_market_value_sqf_bua").val(response.lmvbua);
                $("#valuationapproversdata-land_market_value_sqf_pa").val(response.lmvpa);


            },
            error: bbAlert
        });
    });


    $("body").on("change", "#valuationapproversdata-status", function () {
        var status = $(this).val();
            if(status == 'Approve'){
                $("#reason_text").hide();
            }else{
                $("#reason_text").show();
            }

    });


    $("body").on("change", "#valuationconflict-related_to_buyer", function () {
        var status = $(this).val();
        if(status == 'Yes'){
            $("#related_to_buyer_reason").show();
        }else{
            $("#related_to_buyer_reason").hide();
        }

    });
    $("body").on("change", "#valuationconflict-related_to_seller", function () {
        var status = $(this).val();
        if(status == 'Yes'){
            $("#related_to_seller_reason").show();
        }else{
            $("#related_to_seller_reason").hide();
        }

    });

    $("body").on("change", "#valuationconflict-related_to_client", function () {
        var status = $(this).val();
        if(status == 'Yes'){
            $("#related_to_client_reason").show();
        }else{
            $("#related_to_client_reason").hide();
        }

    });

    $("body").on("change", "#valuationconflict-related_to_property", function () {
        var status = $(this).val();
        if(status == 'Yes'){
            $("#related_to_property_reason").show();
        }else{
            $("#related_to_property_reason").hide();
        }

    });




    $("body").on("change", "#crmvaluationconflict-related_to_buyer", function () {
        var status = $(this).val();
        if(status == 'Yes'){
            $("#related_to_buyer_reason").show();
        }else{
            $("#related_to_buyer_reason").hide();
        }

    });
    $("body").on("change", "#crmvaluationconflict-related_to_seller", function () {
        var status = $(this).val();
        if(status == 'Yes'){
            $("#related_to_seller_reason").show();
        }else{
            $("#related_to_seller_reason").hide();
        }

    });

    $("body").on("change", "#crmvaluationconflict-related_to_client", function () {
        var status = $(this).val();
        if(status == 'Yes'){
            $("#related_to_client_reason").show();
        }else{
            $("#related_to_client_reason").hide();
        }

    });

    $("body").on("change", "#crmvaluationconflict-related_to_property", function () {
        var status = $(this).val();
        if(status == 'Yes'){
            $("#related_to_property_reason").show();
        }else{
            $("#related_to_property_reason").hide();
        }

    });


    $("body").on("change", "#listingsrent-building_info", function () {
        modalId="general-modal";

        var building_id = $(this).val();
        var url_building = 'buildings/detail/'+building_id;

        heading=$(this).data("heading");
        $.ajax({
            url: url_building,
            dataType: "html",
            success: function(data) {

                var response_building = JSON.parse(data);
                //console.log(response_building);

                if(response_building.detail.property_category != 0){
                    $('#listingsrent-property_category').val(response_building.detail.property_category).trigger('change');
                }else{
                    $('#listingsrent-property_category').val(1).trigger('change');
                }
                if(response_building.detail.location != null){
                    $('#listingsrent-location').val(response_building.detail.location).trigger('change');
                }else{
                    $('#listingsrent-location').val(1).trigger('change');
                }
                if(response_building.detail.tenure != 0){
                    $('#listingsrent-tenure').val(response_building.detail.tenure).trigger('change');
                }else{
                    $('#listingsrent-tenure').val(1).trigger('change');
                }
                if(response_building.detail.property_placement != 0){
                    $('#listingsrent-property_placement').val(response_building.detail.property_placement).trigger('change');
                }else{
                    $('#listingsrent-property_placement').val(1).trigger('change');
                }
                if(response_building.detail.property_visibility != 0){
                    $('#listingsrent-property_visibility').val(response_building.detail.property_visibility).trigger('change');
                }else{
                    $('#listingsrent-property_visibility').val(1).trigger('change');
                }
                if(response_building.detail.property_exposure != 0){
                    $('#listingsrent-property_exposure').val(response_building.detail.property_exposure).trigger('change');
                }else{
                    $('#listingsrent-property_exposure').val(1).trigger('change');
                }

                if(response_building.detail.property_condition != 0){
                    $('#listingsrent-property_condition').val(response_building.detail.property_condition).trigger('change');
                }else{
                    $('#listingsrent-property_condition').val(1).trigger('change');
                }
                if(response_building.detail.development_type != null){
                    $('#listingsrent-development_type').val(response_building.detail.development_type).trigger('change');
                }else{
                    $('#listingsrent-development_type').val('Standard').trigger('change');
                }

                if(response_building.detail.utilities_connected != ''){
                    $('#listingsrent-utilities_connected').val(response_building.detail.utilities_connected).trigger('change');
                }else{
                    $('#listingsrent-utilities_connected').val('Yes').trigger('change');
                }


                if(response_building.detail.finished_status != null){
                    $('#listingsrent-finished_status').val(response_building.detail.finished_status).trigger('change');
                }else{
                    $('#listingsrent-finished_status').val('Shell & Core').trigger('change');
                }
                if(response_building.detail.pool != null){
                    $('#listingsrent-pool').val(response_building.detail.pool).trigger('change');
                }else{
                    $('#listingsrent-pool').val('Yes').trigger('change');
                }
                if(response_building.detail.gym != null){
                    $('#listingsrent-gym').val(response_building.detail.gym).trigger('change');
                }else{
                    $('#listingsrent-gym').val('Yes').trigger('change');
                }
                if(response_building.detail.play_area != null){
                    $('#listingsrent-play_area').val(response_building.detail.play_area).trigger('change');
                }else{
                    $('#listingsrent-play_area').val('Yes').trigger('change');
                }

                if(response_building.detail.other_facilities != null){
                    var facilities = response_building.detail.other_facilities;
                    var facilities = facilities.split(",");
                    $('#listingsrent-other_facilities').val(facilities).trigger('change');
                }else{
                    $('#listingsrent-other_facilities').val([]).trigger('change');
                }
                if(response_building.detail.completion_status != null){
                    $('#listingsrent-completion_status').val(response_building.detail.completion_status).trigger('change');
                }else{
                    $('#listingsrent-completion_status').val('').trigger('change');
                }

                if(response_building.detail.white_goods != null){
                    $('#listingsrent-white_goods').val(response_building.detail.white_goods).trigger('change');
                }else{
                    $('#listingsrent-white_goods').val('Yes').trigger('change');
                }

                if(response_building.detail.furnished != null){
                    $('#listingsrent-furnished').val(response_building.detail.furnished).trigger('change');
                }else{
                    $('#listingsrent-furnished').val('Yes').trigger('change');
                }

                if(response_building.detail.landscaping != null){
                    $('#listingsrent-landscaping').val(response_building.detail.landscaping).trigger('change');
                }else{
                    $('#listingsrent-landscaping').val('Yes').trigger('change');
                }

                if(response_building.detail.estimated_age != null){
                    $('#listingsrent-estimated_age').val(response_building.detail.estimated_age).trigger('change');
                }else{
                    $('#listingsrent-estimated_age').val(0).trigger('change');
                }
                if(response_building.detail.location_highway_drive != null){
                    $('#listingsrent-location_highway_drive').val(response_building.detail.location_highway_drive).trigger('change');
                }else{
                    $('#listingsrent-location_highway_drive').val('').trigger('change');
                }
                if(response_building.detail.location_school_drive != null){
                    $('#listingsrent-location_school_drive').val(response_building.detail.location_school_drive).trigger('change');
                }else{
                    $('#listingsrent-location_school_drive').val('').trigger('change');
                }
                if(response_building.detail.location_mall_drive != null){
                    $('#listingsrent-location_mall_drive').val(response_building.detail.location_mall_drive).trigger('change');
                }else{
                    $('#listingsrent-location_mall_drive').val('').trigger('change');
                }
                if(response_building.detail.location_sea_drive != null){
                    $('#listingsrent-location_sea_drive').val(response_building.detail.location_sea_drive).trigger('change');
                }else{
                    $('#listingsrent-location_sea_drive').val('').trigger('change');
                }
                if(response_building.detail.location_park_drive != null){
                    $('#listingsrent-location_park_drive').val(response_building.detail.location_park_drive).trigger('change');
                }else{
                    $('#listingsrent-location_park_drive').val('').trigger('change');
                }

                if(response_building.detail.typical_floors != null){
                    $('#listingsrent-full_building_floors').val(response_building.detail.typical_floors);
                }else{
                    $('#listingsrent-full_building_floors').val('');
                }
            },
            error: bbAlert
        });
    });



});
function bbAlert(xhr, ajaxOptions, thrownError){
    Swal.fire(thrownError,xhr.statusText + "\n" + xhr.responseText);
    $(".blockUI").remove();
}



