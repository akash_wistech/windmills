jQuery(document).ready(function() {

	$("body").on("change", "#previoustransactions-building_info", function () {
		modalId="general-modal";

		var building_id = $(this).val();
		var url_building = 'buildings/detail/'+building_id;

		heading=$(this).data("heading");
		$.ajax({
			url: url_building,
			dataType: "html",
			success: function(data) {

                var response_building = JSON.parse(data);
                console.log(response_building);

                if(response_building.detail.property_id != 0){
                    $('#previoustransactions-property_id').val(response_building.detail.property_id).trigger('change');
                }else{
                    $('#previoustransactions-property_id').val(1).trigger('change');
                }
                if(response_building.detail.location != null){
                    $('#previoustransactions-location').val(response_building.detail.location).trigger('change');
                }else{
                    $('#previoustransactions-location').val(1).trigger('change');
                }
                if(response_building.detail.property_category != 0){
                    $('#previoustransactions-property_category').val(response_building.detail.property_category).trigger('change');
                }else{
                    $('#previoustransactions-property_category').val(1).trigger('change');
                }
                if(response_building.detail.tenure != 0){
                    $('#previoustransactions-tenure').val(response_building.detail.tenure).trigger('change');
                }else{
                    $('#previoustransactions-tenure').val(1).trigger('change');
                }
                if(response_building.detail.utilities_connected != ''){
                    $('#previoustransactions-utilities_connected').val(response_building.detail.utilities_connected).trigger('change');
                }else{
                    $('#previoustransactions-utilities_connected').val('Yes').trigger('change');
                }
                if(response_building.detail.development_type != null){
                    $('#previoustransactions-development_type').val(response_building.detail.development_type).trigger('change');
                }else{
                    $('#previoustransactions-development_type').val('Standard').trigger('change');
                }
                if(response_building.detail.property_placement != 0){
                    $('#previoustransactions-property_placement').val(response_building.detail.property_placement).trigger('change');
                }else{
                    $('#previoustransactions-property_placement').val(1).trigger('change');
                }
                if(response_building.detail.property_visibility != 0){
                    $('#previoustransactions-property_visibility').val(response_building.detail.property_visibility).trigger('change');
                }else{
                    $('#previoustransactions-property_visibility').val(1).trigger('change');
                }
                if(response_building.detail.property_exposure != 0){
                    $('#previoustransactions-property_exposure').val(response_building.detail.property_exposure).trigger('change');
                }else{
                    $('#previoustransactions-property_exposure').val(1).trigger('change');
                }

                if(response_building.detail.property_condition != 0){
                    $('#previoustransactions-property_condition').val(response_building.detail.property_condition).trigger('change');
                }else{
                    $('#previoustransactions-property_condition').val(1).trigger('change');
                }
                if(response_building.detail.completion_status != null){
                    $('#previoustransactions-completion_status').val(response_building.detail.completion_status).trigger('change');
                }else{
                    $('#previoustransactions-completion_status').val('Ready').trigger('change');
                }
                if(response_building.detail.pool != null){
                    $('#previoustransactions-pool').val(response_building.detail.pool).trigger('change');
                }else{
                    $('#previoustransactions-pool').val('Yes').trigger('change');
                }
                if(response_building.detail.gym != null){
                    $('#previoustransactions-gym').val(response_building.detail.gym).trigger('change');
                }else{
                    $('#previoustransactions-gym').val('Yes').trigger('change');
                }
                if(response_building.detail.play_area != null){
                    $('#previoustransactions-play_area').val(response_building.detail.play_area).trigger('change');
                }else{
                    $('#previoustransactions-play_area').val('Yes').trigger('change');
                }


                if(response_building.detail.landscaping != null){
                    $('#previoustransactions-landscaping').val(response_building.detail.landscaping).trigger('change');
                }else{
                    $('#previoustransactions-landscaping').val('Yes').trigger('change');
                }
                if(response_building.detail.white_goods != null){
                    $('#previoustransactions-white_goods').val(response_building.detail.white_goods).trigger('change');
                }else{
                    $('#previoustransactions-white_goods').val('Yes').trigger('change');
                }
                if(response_building.detail.furnished != null){
                    $('#previoustransactions-furnished').val(response_building.detail.furnished).trigger('change');
                }else{
                    $('#previoustransactions-furnished').val('Yes').trigger('change');
                }
                if(response_building.detail.finished_status != null){
                    $('#previoustransactions-finished_status').val(response_building.detail.finished_status).trigger('change');
                }else{
                    $('#previoustransactions-finished_status').val('Shell & Core').trigger('change');
                }

                if(response_building.detail.plot_number != null){
                    $('#previoustransactions-plot_number').val(response_building.detail.plot_number);
                }else{
                    $('#previoustransactions-plot_number').val('');
                }
                if(response_building.detail.plot_number != null){
                    $('#previoustransactions-plot_number').val(response_building.detail.plot_number);
                }else{
                    $('#previoustransactions-plot_number').val('');
                }
			},
			error: bbAlert
		});
	});
});
function bbAlert(xhr, ajaxOptions, thrownError){
    Swal.fire(thrownError,xhr.statusText + "\n" + xhr.responseText);
    $(".blockUI").remove();
}
