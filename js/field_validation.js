// for fields validation 
$(document).ready(function () {

    $('form#w0').on('submit', function (e) {


        var visible_inputs = $(this).find(':input:visible');
        var visible_field_ids = [];

        visible_inputs.each(function () {
            var $input = $(this);

            var field_id = $input.attr('id');
            visible_field_ids.push(field_id);

        });


        // on quotations
        if (jQuery.inArray("crmreceivedproperties-valuation_approach", visible_field_ids) !== -1) {
            makeTheDropDownFieldMandatory(e, "crmreceivedproperties-valuation_approach", "field-crmreceivedproperties-valuation_approach", "Valuation Approach");
        }
        if (jQuery.inArray("crmreceivedproperties-number_of_comparables", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-number_of_comparables", "field-crmreceivedproperties-number_of_comparables", "Number Of Comparables");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_comparables_value", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_comparables_value", "field-crmreceivedproperties-no_of_comparables_value", "Number Of Comparables");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_comparables_value", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_comparables_value", "field-crmreceivedproperties-no_of_comparables_value", "Number Of Comparables");
        }

        if (jQuery.inArray("crmreceivedproperties-scope_of_service", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-scope_of_service", "field-crmreceivedproperties-scope_of_service", "Scope Of Service");
        }



        if (jQuery.inArray("crmreceivedproperties-unit_number", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "crmreceivedproperties-unit_number", "unit_number", "field-crmreceivedproperties-unit_number", "Unit Number");
        }
        if (jQuery.inArray("crmreceivedproperties-built_up_area", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "crmreceivedproperties-built_up_area", "built_up_area", "field-crmreceivedproperties-built_up_area", "Built Up Area");
        }
        if (jQuery.inArray("crmreceivedproperties-net_leasable_area", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "crmreceivedproperties-net_leasable_area", "net_leasable_area", "field-crmreceivedproperties-net_leasable_area", "Net Leasable Area");
        }
        if (jQuery.inArray("crmreceivedproperties-land_size", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "crmreceivedproperties-land_size", "land_size", "field-crmreceivedproperties-land_size", "Land Size");
        }
        if (jQuery.inArray("crmreceivedproperties-floor_number", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "crmreceivedproperties-floor_number", "floor_number", "field-crmreceivedproperties-floor_number", "Floor Number");
        }

        if (jQuery.inArray("crmreceivedproperties-complexity", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-complexity", "field-crmreceivedproperties-complexity", "Property Standard/Complexity");
        }
        if (jQuery.inArray("crmreceivedproperties-typical_floors", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-typical_floors", "field-crmreceivedproperties-typical_floors", "Number of Floors");
        }
        if (jQuery.inArray("crmreceivedproperties-basement_floors", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-basement_floors", "field-crmreceivedproperties-basement_floors", "Number of Basements Floors");
        }
        if (jQuery.inArray("crmreceivedproperties-parking_space", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-parking_space", "field-crmreceivedproperties-parking_space", "Number of Parking Spaces");
        }
        if (jQuery.inArray("crmreceivedproperties-swimming_pools", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-swimming_pools", "field-crmreceivedproperties-swimming_pools", "Number of Swimming Pools");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_jacuzzi", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_jacuzzi", "field-crmreceivedproperties-no_of_jacuzzi", "Number of Jacuzzi");
        }

        if (jQuery.inArray("crmreceivedproperties-upgrades", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-upgrades", "field-crmreceivedproperties-upgrades", "Upgrades Star Rating");
        }
        if (jQuery.inArray("crmreceivedproperties-furnished", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-furnished", "field-crmreceivedproperties-furnished", "Furnished");
        }
        if (jQuery.inArray("crmreceivedproperties-ready", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-ready", "field-crmreceivedproperties-ready", "Ready ?");
        }

        if (jQuery.inArray("crmreceivedproperties-no_of_buildings", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_buildings", "field-crmreceivedproperties-no_of_buildings", "Number of Buildings");
        }
        if (jQuery.inArray("crmreceivedproperties-mezzanine_floors", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-mezzanine_floors", "field-crmreceivedproperties-mezzanine_floors", "Number of Mezzanine Floors");
        }
        if (jQuery.inArray("crmreceivedproperties-parking_floors", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-parking_floors", "field-crmreceivedproperties-parking_floors", "Number of Parking Floors");
        }

        if (jQuery.inArray("crmreceivedproperties-no_of_residential_units", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_residential_units", "field-crmreceivedproperties-no_of_residential_units", "Number of Residential Unit");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_commercial_units", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_commercial_units", "field-crmreceivedproperties-no_of_commercial_units", "Number of Commercial Unit");
        }
        if (jQuery.inArray("crmreceivedproperties-retails_units", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-retails_units", "field-crmreceivedproperties-retails_units", "Number of Retail Unit");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_unit_types", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_unit_types", "field-crmreceivedproperties-no_of_unit_types", "Types of Unit");
        }

        if (jQuery.inArray("crmreceivedproperties-meeting_rooms", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-meeting_rooms", "field-crmreceivedproperties-meeting_rooms", "Party Halls/Meeting Rooms");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_health_club_spa", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_health_club_spa", "field-crmreceivedproperties-no_of_health_club_spa", "Number of Health Clubs/Spa");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_bbq_area", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_bbq_area", "field-crmreceivedproperties-no_of_bbq_area", "Number of BBQ Areas");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_play_area", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_play_area", "field-crmreceivedproperties-no_of_play_area", "Number of Children Play Areas");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_gyms", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_gyms", "field-crmreceivedproperties-no_of_gyms", "Number of Gyms");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_schools", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_schools", "field-crmreceivedproperties-no_of_schools", "Number of Schools");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_clinics", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_clinics", "field-crmreceivedproperties-no_of_clinics", "Number of Clinics");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_sports_courts", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_sports_courts", "field-crmreceivedproperties-no_of_sports_courts", "Number of Sports Courts");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_mosques", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_mosques", "field-crmreceivedproperties-no_of_mosques", "Number of Mosque");
        }

        if (jQuery.inArray("crmreceivedproperties-restaurant", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-restaurant", "field-crmreceivedproperties-restaurant", "Number of Restaurants");
        }
        if (jQuery.inArray("crmreceivedproperties-atms", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-atms", "field-crmreceivedproperties-atms", "Number of ATM Machines");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_coffee_shops", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_coffee_shops", "field-crmreceivedproperties-no_of_coffee_shops", "Number of Coffee Shops");
        }
        if (jQuery.inArray("crmreceivedproperties-no_of_sign_boards", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-no_of_sign_boards", "field-crmreceivedproperties-no_of_sign_boards", "Number of Sign Boards");
        }
        if (jQuery.inArray("crmreceivedproperties-night_clubs", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-night_clubs", "field-crmreceivedproperties-night_clubs", "Number of Night Clubs");
        }
        if (jQuery.inArray("crmreceivedproperties-bars", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmreceivedproperties-bars", "field-crmreceivedproperties-bars", "Number of Bars");
        }




    });



    // on quotations
    makeTheDropDownNonMandatoryOnChange("crmreceivedproperties-valuation_approach", "field-crmreceivedproperties-valuation_approach");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-number_of_comparables", "field-crmreceivedproperties-number_of_comparables");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_comparables_value", "field-crmreceivedproperties-no_of_comparables_value");

    makeTheTextFieldNonMandatoryOnChange("crmreceivedproperties-unit_number", "unit_number");
    makeTheTextFieldNonMandatoryOnChange("crmreceivedproperties-built_up_area", "built_up_area");
    makeTheTextFieldNonMandatoryOnChange("crmreceivedproperties-net_leasable_area", "net_leasable_area");
    makeTheTextFieldNonMandatoryOnChange("crmreceivedproperties-land_size", "land_size");

    makeTheSelectField2NonMandatoryOnChange("crmreceivedproperties-floor_number", "field-crmreceivedproperties-floor_number", "floor_number");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-complexity", "field-crmreceivedproperties-complexity");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-typical_floors", "field-crmreceivedproperties-typical_floors");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-basement_floors", "field-crmreceivedproperties-basement_floors");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-parking_space", "field-crmreceivedproperties-parking_space");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-swimming_pools", "field-crmreceivedproperties-swimming_pools");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_jacuzzi", "field-crmreceivedproperties-no_of_jacuzzi");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-upgrades", "field-crmreceivedproperties-upgrades");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-furnished", "field-crmreceivedproperties-furnished");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-ready", "field-crmreceivedproperties-ready");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_buildings", "field-crmreceivedproperties-no_of_buildings");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-mezzanine_floors", "field-crmreceivedproperties-mezzanine_floors");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-parking_floors", "field-crmreceivedproperties-parking_floors");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_residential_units", "field-crmreceivedproperties-no_of_residential_units");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_commercial_units", "field-crmreceivedproperties-no_of_commercial_units");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-retails_units", "field-crmreceivedproperties-retails_units");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_unit_types", "field-crmreceivedproperties-no_of_unit_types");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-meeting_rooms", "field-crmreceivedproperties-meeting_rooms");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_health_club_spa", "field-crmreceivedproperties-no_of_health_club_spa");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_bbq_area", "field-crmreceivedproperties-no_of_bbq_area");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_play_area", "field-crmreceivedproperties-no_of_play_area");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_gyms", "field-crmreceivedproperties-no_of_gyms");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_schools", "field-crmreceivedproperties-no_of_schools");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_clinics", "field-crmreceivedproperties-no_of_clinics");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_sports_courts", "field-crmreceivedproperties-no_of_sports_courts");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_mosques", "field-crmreceivedproperties-no_of_mosques");

    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-restaurant", "field-crmreceivedproperties-restaurant");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-atms", "field-crmreceivedproperties-atms");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_coffee_shops", "field-crmreceivedproperties-no_of_coffee_shops");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-no_of_sign_boards", "field-crmreceivedproperties-no_of_sign_boards");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-night_clubs", "field-crmreceivedproperties-night_clubs");
    makeTheSelectFieldNonMandatoryOnChange("crmreceivedproperties-bars", "field-crmreceivedproperties-bars");








});



$(document).ready(function () {

    $('form#w0').on('submit', function (e) {


        var visible_inputs = $(this).find(':input:visible');
        var visible_field_ids = [];

        visible_inputs.each(function () {
            var $input = $(this);

            var field_id = $input.attr('id');
            visible_field_ids.push(field_id);

        });



        // on valuations enter property details
        // if (jQuery.inArray("valuationdetail-unit_number", visible_field_ids) !== -1) {
        //     makeTheTextFieldMandatory(e, "valuationdetail-unit_number", "unit_number", "field-valuationdetail-unit_number", "Unit Number");
        // }
        // if (jQuery.inArray("valuationdetail-floor_number", visible_field_ids) !== -1) {
        //     makeTheTextFieldMandatory(e, "valuationdetail-floor_number", "floor_number", "field-valuationdetail-floor_number", "Floor Number");
        // }
        if (jQuery.inArray("valuationdetail-land_size", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "valuationdetail-land_size", "land_size", "field-valuationdetail-land_size", "Land Size");
        }
        if (jQuery.inArray("valuationdetail-built_up_area", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "valuationdetail-built_up_area", "built_up_area", "field-valuationdetail-built_up_area", "BUA / GFA");
        }






    });


    // on valuations enter property details
    // makeTheTextFieldNonMandatoryOnChange("valuationdetail-unit_number", "unit_number");
    // makeTheTextFieldNonMandatoryOnChange("valuationdetail-floor_number", "floor_number");
    makeTheTextFieldNonMandatoryOnChange("valuationdetail-land_size", "land_size");
    makeTheTextFieldNonMandatoryOnChange("valuationdetail-built_up_area", "built_up_area");


});


$(document).ready(function () {

    $('form#w0').on('submit', function (e) {


        var visible_inputs = $(this).find(':input:visible');
        var visible_field_ids = [];

        visible_inputs.each(function () {
            var $input = $(this);

            var field_id = $input.attr('id');
            visible_field_ids.push(field_id);

        });



        // on valuations enter property details
        // if (jQuery.inArray("valuation-client_id", visible_field_ids) !== -1) {
        //     makeTheSelectFieldMandatory(e, "valuation-client_id", "field-valuation-client_id", "Client Name");
        // }

        // on valuations enter valuation details
        if (jQuery.inArray("valuationdetaildata-other_intended_users", visible_field_ids) !== -1) {
            makeTheTextField2Mandatory(e, "valuationdetaildata-other_intended_users", "other_intended_users", "field-valuationdetaildata-other_intended_users", "Other Intended Users Value");
        }
        if (jQuery.inArray("valuationdetaildata-purpose_of_valuation", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "valuationdetaildata-purpose_of_valuation", "field-valuationdetaildata-purpose_of_valuation", "Purpose Of Valuation");
        }
        if (jQuery.inArray("valuationdetaildata-service_officer_name", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "valuationdetaildata-service_officer_name", "field-valuationdetaildata-service_officer_name", "Service Officer Name");
        }
        if (jQuery.inArray("valuationdetaildata-valuation_date", visible_field_ids) !== -1) {
            makeTheTextField2Mandatory(e, "valuationdetaildata-valuation_date", "valuation_date", "field-valuationdetaildata-valuation_date", "Valuation Date");
        }


        // on valuations enter inspection schedule
        if (jQuery.inArray("inspectioncancelreasonslog-reason_id", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectioncancelreasonslog-reason_id", "field-inspectioncancelreasonslog-reason_id", "Select reason for Email");
        }

        // on enter inspection schedule
        // if (jQuery.inArray("scheduleinspection-changed_inspection_officer", visible_field_ids) !== -1) {
        //     makeTheSelectFieldMandatory(e, "scheduleinspection-changed_inspection_officer", "field-scheduleinspection-changed_inspection_officer", "Changed Inspection Officer");
        // }
        // if (jQuery.inArray("scheduleinspection-transfer_reason", visible_field_ids) !== -1) {
        //     makeTheSelectFieldMandatory(e, "scheduleinspection-transfer_reason", "field-scheduleinspection-transfer_reason", "Transfer Reason");
        // }




    });

    // on valuation
    // makeTheSelectFieldNonMandatoryOnChange("valuation-client_id", "field-valuation-client_id");

    // on valuations enter valuation details
    makeTheTextField2NonMandatoryOnChange("valuationdetaildata-other_intended_users", "other_intended_users");
    makeTheSelectFieldNonMandatoryOnChange("valuationdetaildata-purpose_of_valuation", "field-valuationdetaildata-purpose_of_valuation");
    makeTheSelectFieldNonMandatoryOnChange("valuationdetaildata-service_officer_name", "field-valuationdetaildata-service_officer_name");
    makeTheTextField2NonMandatoryOnChange("valuationdetaildata-valuation_date", "valuation_date");


    // on valuations enter inspection schedule
    makeTheSelectFieldNonMandatoryOnChange("inspectioncancelreasonslog-reason_id", "field-inspectioncancelreasonslog-reason_id");

    // on enter inspection schedule
    makeTheSelectFieldNonMandatoryOnChange("scheduleinspection-changed_inspection_officer", "field-scheduleinspection-changed_inspection_officer");
    makeTheSelectFieldNonMandatoryOnChange("scheduleinspection-transfer_reason", "field-scheduleinspection-transfer_reason");


});

$(document).ready(function () {

    $('form#w0').on('submit', function (e) {


        var visible_inputs = $(this).find(':input:visible');
        var visible_field_ids = [];

        visible_inputs.each(function () {
            var $input = $(this);

            var field_id = $input.attr('id');
            visible_field_ids.push(field_id);

        });



        // on valuations enter valuation details
        // if (jQuery.inArray("crmquotations-purpose_of_valuation", visible_field_ids) !== -1) {
        //     makeTheTextField2Mandatory(e, "crmquotations-purpose_of_valuation", "other_intended_users", "field-crmquotations-purpose_of_valuation", "Other Intended Users Value");
        // }
        if (jQuery.inArray("crmquotations-purpose_of_valuation", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmquotations-purpose_of_valuation", "field-crmquotations-purpose_of_valuation", "Purpose Of Valuation");
        }
        if (jQuery.inArray("crmquotations-advance_payment_terms", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "crmquotations-advance_payment_terms", "field-crmquotations-advance_payment_terms", "Advance Payment Terms");
        }



    });


    // on valuations enter valuation details
    // makeTheTextField2NonMandatoryOnChange("crmquotations-purpose_of_valuation", "other_intended_users");
    makeTheSelectFieldNonMandatoryOnChange("crmquotations-purpose_of_valuation", "field-crmquotations-purpose_of_valuation");
    makeTheSelectFieldNonMandatoryOnChange("crmquotations-advance_payment_terms", "field-crmquotations-advance_payment_terms");



});


$(document).ready(function () {

    $('form#w0').on('submit', function (e) {


        var visible_inputs = $(this).find(':input:visible');
        var visible_field_ids = [];

        visible_inputs.each(function () {
            var $input = $(this);

            var field_id = $input.attr('id');
            visible_field_ids.push(field_id);

        });


        // on valuations enter inspect property
        if (jQuery.inArray("inspectproperty-makani_number", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-makani_number", "makani_number", "field-inspectproperty-makani_number", "Makani Number");
        }
        if (jQuery.inArray("inspectproperty-municipality_number", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-municipality_number", "municipality_number", "field-inspectproperty-municipality_number", "Municipality Number");
        }
        if (jQuery.inArray("inspectproperty-location_pin", visible_field_ids) !== -1) {
            makeTheTextField2Mandatory(e, "inspectproperty-location_pin", "location_pin", "field-inspectproperty-location_pin", "Location Pin");
        }
        if (jQuery.inArray("inspectproperty-built_up_area", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-built_up_area", "built_up_area", "field-inspectproperty-built_up_area", "Measured Gross BUA/GFA Size");
        }
        if (jQuery.inArray("inspectproperty-balcony_size", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-balcony_size", "balcony_size", "field-inspectproperty-balcony_size", "Measured Balcony Size");
        }
        if (jQuery.inArray("inspectproperty-service_area_size", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-service_area_size", "service_area_size", "field-inspectproperty-service_area_size", "Measured Service Block Area BUA Size");
        }
        if (jQuery.inArray("inspectproperty-net_built_up_area", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-net_built_up_area", "net_built_up_area", "field-inspectproperty-net_built_up_area", "Measured Net BUA/GFA Size");
        }
        if (jQuery.inArray("inspectproperty-permitted_bua", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-permitted_bua", "permitted_bua", "field-inspectproperty-permitted_bua", "Permitted BUA for Land");
        }
        if (jQuery.inArray("inspectproperty-permitted_gfa", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-permitted_gfa", "permitted_gfa", "field-inspectproperty-permitted_gfa", "Permitted GFA for Land");
        }
        if (jQuery.inArray("inspectproperty-finished_status", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-finished_status", "field-inspectproperty-finished_status", "Inspection Officer");
        }
        if (jQuery.inArray("inspectproperty-estimated_age", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-estimated_age", "estimated_age", "field-inspectproperty-estimated_age", "Estimated Age");
        }
        if (jQuery.inArray("inspectproperty-development_type", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-development_type", "field-inspectproperty-development_type", "Development Type");
        }

        if (jQuery.inArray("inspectproperty-acquisition_method", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-acquisition_method", "field-inspectproperty-acquisition_method", "Acquisition Method");
        }

        if (jQuery.inArray("inspectproperty-completion_status", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-completion_status", "completion_status", "field-inspectproperty-completion_status", "Completion Status");
        }

        if (jQuery.inArray("inspectproperty-developer_id", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-developer_id", "field-inspectproperty-developer_id", "Master Developer");
        }
        if (jQuery.inArray("inspectproperty-developer_id_property", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-developer_id_property", "field-inspectproperty-developer_id_property", "Property Developer");
        }
        if (jQuery.inArray("inspectproperty-number_of_building_on_land", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-number_of_building_on_land", "field-inspectproperty-number_of_building_on_land", "Number Of Building On Land");
        }
        if (jQuery.inArray("inspectproperty-total_units", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-total_units", "total_units", "field-inspectproperty-total_units", "Total Number of Units");
        }
        if (jQuery.inArray("inspectproperty-residential_units", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-residential_units", "residential_units", "field-inspectproperty-residential_units", "Total Number of Residential Units");
        }
        if (jQuery.inArray("inspectproperty-commercial_units", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-commercial_units", "commercial_units", "field-inspectproperty-commercial_units", "Total Number of Commercial Units");
        }
        if (jQuery.inArray("inspectproperty-retail_units", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-retail_units", "retail_units", "field-inspectproperty-retail_units", "Total Number of Retail Units");
        }
        if (jQuery.inArray("inspectproperty-full_building_floors", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-full_building_floors", "full_building_floors", "field-inspectproperty-full_building_floors", "Full Building Floors");
        }
        if (jQuery.inArray("inspectproperty-number_of_basement", visible_field_ids) !== -1) {
            makeTheTextFieldMandatory(e, "inspectproperty-number_of_basement", "number_of_basement", "field-inspectproperty-number_of_basement", "Number of Basements");
        }

        if (jQuery.inArray("inspectproperty-number_of_basement", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-number_of_basement", "field-inspectproperty-number_of_basement", "Number of Basements");
        }
        if (jQuery.inArray("inspectproperty-number_of_mezannines", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-number_of_mezannines", "field-inspectproperty-number_of_mezannines", "Number of Mezannines");
        }
        if (jQuery.inArray("inspectproperty-parking_floors", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "inspectproperty-parking_floors", "parking_floors", "field-inspectproperty-parking_floors", "Number of Parking Spaces");
        }
        if (jQuery.inArray("inspectproperty-number_of_levels", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "inspectproperty-number_of_levels", "number_of_levels", "field-inspectproperty-number_of_levels", "Number Of Levels");
        }
        if (jQuery.inArray("inspectproperty-ac_type", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "inspectproperty-ac_type", "ac_type", "field-inspectproperty-ac_type", "Ac Type");
        }
        if (jQuery.inArray("inspectproperty-utilities_connected", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-utilities_connected", "field-inspectproperty-utilities_connected", "Utilities Connected");
        }
        if (jQuery.inArray("inspectproperty-fridge", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-fridge", "field-inspectproperty-fridge", "Number of Fridges");
        }
        if (jQuery.inArray("inspectproperty-oven", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-oven", "field-inspectproperty-oven", "Number of Ovens");
        }
        if (jQuery.inArray("inspectproperty-washing_machine", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-washing_machine", "field-inspectproperty-washing_machine", "Washing Machine");
        }
        if (jQuery.inArray("inspectproperty-dish_washer", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-dish_washer", "field-inspectproperty-dish_washer", "Dish Washer");
        }
        if (jQuery.inArray("inspectproperty-no_studios", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "inspectproperty-no_studios", "no_studios", "field-inspectproperty-no_studios", "Number of Studio");
        }
        if (jQuery.inArray("inspectproperty-no_one_bedrooms", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "inspectproperty-no_one_bedrooms", "no_one_bedrooms", "field-inspectproperty-no_one_bedrooms", "Number of 1 Bedroom");
        }
        if (jQuery.inArray("inspectproperty-no_two_bedrooms", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "inspectproperty-no_two_bedrooms", "no_two_bedrooms", "field-inspectproperty-no_two_bedrooms", "Number of 2 Bedrooms");
        }
        if (jQuery.inArray("inspectproperty-no_three_bedrooms", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "inspectproperty-no_three_bedrooms", "no_three_bedrooms", "field-inspectproperty-no_three_bedrooms", "Number of 3 Bedrooms");
        }
        if (jQuery.inArray("inspectproperty-no_four_bedrooms", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "inspectproperty-no_four_bedrooms", "no_four_bedrooms", "field-inspectproperty-no_four_bedrooms", "Number of 4 Bedrooms");
        }
        if (jQuery.inArray("inspectproperty-no_penthouse", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "inspectproperty-no_penthouse", "no_penthouse", "field-inspectproperty-no_penthouse", "Number of Penthouse");
        }
        if (jQuery.inArray("inspectproperty-number_of_bbq_areas", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-number_of_bbq_areas", "field-inspectproperty-number_of_bbq_areas", "Number of BBQ Areas");
        }
        if (jQuery.inArray("inspectproperty-no_childrens_play_area", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-no_childrens_play_area", "field-inspectproperty-no_childrens_play_area", "Number of Children Play Area");
        }
        if (jQuery.inArray("inspectproperty-no_of_swimming_pool", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-no_of_swimming_pool", "field-inspectproperty-no_of_swimming_pool", "Number of Swimming Pool");
        }
        if (jQuery.inArray("inspectproperty-no_of_jacuzzi", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-no_of_jacuzzi", "field-inspectproperty-no_of_jacuzzi", "Number of Jacuzzi");
        }
        // if (jQuery.inArray("inspectproperty-no_of_gym_room", visible_field_ids) !== -1) {
        //     makeTheSelectFieldMandatory(e, "inspectproperty-no_of_gym_room", "field-inspectproperty-no_of_gym_room", "Number of Gym Rooms");
        // }
        if (jQuery.inArray("inspectproperty-no_of_school", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-no_of_school", "field-inspectproperty-no_of_school", "Number of Schools");
        }
        if (jQuery.inArray("inspectproperty-no_of_clinic", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-no_of_clinic", "field-inspectproperty-no_of_clinic", "Number of Clinics");
        }
        if (jQuery.inArray("inspectproperty-no_of_sports_court", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-no_of_sports_court", "field-inspectproperty-no_of_sports_court", "Number of Sports Courts");
        }
        if (jQuery.inArray("inspectproperty-no_of_masjid", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-no_of_masjid", "field-inspectproperty-no_of_masjid", "Number of Masjids");
        }
        if (jQuery.inArray("inspectproperty-no_of_community_center", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "inspectproperty-no_of_community_center", "field-inspectproperty-no_of_community_center", "Number of Community Centers");
        }
        if (jQuery.inArray("inspectproperty-no_of_shops", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "inspectproperty-no_of_shops", "no_of_shops", "field-inspectproperty-no_of_shops", "Number of Shops");
        }
        if (jQuery.inArray("inspectproperty-no_of_offices", visible_field_ids) !== -1) {
            makeTheSelectField2Mandatory(e, "inspectproperty-no_of_offices", "no_of_offices", "field-inspectproperty-no_of_shops", "Number of Offices");
        }


    });



    // on valuations enter inspect property
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-makani_number", "makani_number");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-municipality_number", "municipality_number");
    makeTheTextField2NonMandatoryOnChange("inspectproperty-location_pin", "location_pin");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-built_up_area", "built_up_area");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-balcony_size", "balcony_size");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-service_area_size", "service_area_size");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-net_built_up_area", "net_built_up_area");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-permitted_bua", "permitted_bua");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-permitted_gfa", "permitted_gfa");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-finished_status", "field-inspectproperty-finished_status");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-estimated_age", "estimated_age");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-development_type", "field-inspectproperty-development_type");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-acquisition_method", "field-inspectproperty-acquisition_method");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-developer_id", "field-inspectproperty-developer_id");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-developer_id_property", "field-inspectproperty-developer_id_property");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-number_of_building_on_land", "field-inspectproperty-number_of_building_on_land");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-total_units", "total_units");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-residential_units", "residential_units");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-commercial_units", "commercial_units");
    makeTheTextFieldNonMandatoryOnChange("inspectproperty-retail_units", "retail_units");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-number_of_basement", "field-inspectproperty-number_of_basement");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-number_of_mezannines", "field-inspectproperty-number_of_mezannines");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-parking_floors", "field-inspectproperty-parking_floors");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-ac_type", "field-inspectproperty-ac_type");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-utilities_connected", "field-inspectproperty-utilities_connected");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-fridge", "field-inspectproperty-fridge");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-oven", "field-inspectproperty-oven");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-washing_machine", "field-inspectproperty-washing_machine");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-dish_washer", "field-inspectproperty-dish_washer");
    makeTheSelectField2NonMandatoryOnChange("inspectproperty-no_studios", "field-inspectproperty-no_studios", "no_studios");
    makeTheSelectField2NonMandatoryOnChange("inspectproperty-no_one_bedrooms", "field-inspectproperty-no_one_bedrooms", "no_one_bedrooms");
    makeTheSelectField2NonMandatoryOnChange("inspectproperty-no_two_bedrooms", "field-inspectproperty-no_two_bedrooms", "no_two_bedrooms");
    makeTheSelectField2NonMandatoryOnChange("inspectproperty-no_three_bedrooms", "field-inspectproperty-no_three_bedrooms", "no_three_bedrooms");
    makeTheSelectField2NonMandatoryOnChange("inspectproperty-no_four_bedrooms", "field-inspectproperty-no_four_bedrooms", "no_four_bedrooms");
    makeTheSelectField2NonMandatoryOnChange("inspectproperty-no_penthouse", "field-inspectproperty-no_penthouse", "no_penthouse");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-number_of_bbq_areas", "field-inspectproperty-number_of_bbq_areas");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-no_childrens_play_area", "field-inspectproperty-no_childrens_play_area");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-no_of_swimming_pool", "field-inspectproperty-no_of_swimming_pool");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-no_of_jacuzzi", "field-inspectproperty-no_of_jacuzzi");
    // makeTheSelectFieldNonMandatoryOnChange("inspectproperty-no_of_gym_room", "field-inspectproperty-no_of_gym_room");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-no_of_school", "field-inspectproperty-no_of_school");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-no_of_clinic", "field-inspectproperty-no_of_clinic");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-no_of_sports_court", "field-inspectproperty-no_of_sports_court");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-no_of_masjid", "field-inspectproperty-no_of_masjid");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-no_of_community_center", "field-inspectproperty-no_of_community_center");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-no_of_shops", "field-inspectproperty-no_of_shops","no_of_shops");
    makeTheSelectFieldNonMandatoryOnChange("inspectproperty-no_of_offices", "field-inspectproperty-no_of_offices","no_of_offices");


});


$(document).ready(function () {

    $('form#w0').on('submit', function (e) {

        var visible_inputs = $(this).find(':input:visible');
        var visible_field_ids = [];

        visible_inputs.each(function () {
            var $input = $(this);

            var field_id = $input.attr('id');
            visible_field_ids.push(field_id);

        });

        // on cost details page
        if (jQuery.inArray("costdetails-original_purchase_date", visible_field_ids) !== -1) {
            if($("#costdetails-original_purchase_price_available").val() == "Yes"){
                makeTheTextField3Mandatory(e, "costdetails-original_purchase_date", "original_purchase_date", "field-costdetails-original_purchase_date", "Original Purchase Date");
            } else {   }
        }

        if (jQuery.inArray("costdetails-original_land_purchase_date", visible_field_ids) !== -1) {
            if($("#costdetails-original_land_purchase_price_available").val() == "Yes"){
                makeTheTextField3Mandatory(e, "costdetails-original_land_purchase_date", "original_land_purchase_date", "field-costdetails-original_land_purchase_date", "Original Land Purchase Date");
            } else {  }
        }

        if (jQuery.inArray("costdetails-land_lease_costs", visible_field_ids) !== -1) {
            if($("#costdetails-land_lease_cost_available").val() == "Yes"){
                makeTheTextFieldMandatory(e, "costdetails-land_lease_costs", "land_lease_costs", "field-costdetails-land_lease_costs", "Land Lease Cost");
            } else {}
        }
        if (jQuery.inArray("costdetails-land_lease_expiry_date", visible_field_ids) !== -1) {
            if($("#costdetails-land_lease_cost_available").val() == "Yes"){
                makeTheTextField3Mandatory(e, "costdetails-land_lease_expiry_date", "land_lease_expiry_date", "field-costdetails-land_lease_expiry_date", "Land Lease Expiry Date");
            } else {}
        }
        if (jQuery.inArray("costdetails-total_no_of_lease_years", visible_field_ids) !== -1) {
            if($("#costdetails-land_lease_cost_available").val() == "Yes"){
                makeTheTextField3Mandatory(e, "costdetails-total_no_of_lease_years", "total_no_of_lease_years", "field-costdetails-total_no_of_lease_years", "Total Number of Lease Years");
            } else {}
        }

        if (jQuery.inArray("costdetails-current_transaction_price_date", visible_field_ids) !== -1) {
            if($("#costdetails-current_transaction_price_available").val() == "Yes"){
                makeTheTextField3Mandatory(e, "costdetails-current_transaction_price_date", "current_transaction_price_date", "field-costdetails-current_transaction_price_date", "Current Transaction Price Date");
            } else { }
        }
        if (jQuery.inArray("costdetails-transaction_price", visible_field_ids) !== -1) {
            if($("#costdetails-current_transaction_price_available").val() == "Yes"){
                makeTheTextFieldMandatory(e, "costdetails-transaction_price", "transaction_price", "field-costdetails-transaction_price", "Current Transaction Price");
            } else {  }
        }





        if (jQuery.inArray("costdetails-acquisition_method", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "costdetails-acquisition_method", "field-costdetails-acquisition_method", "Acquisition Method");
        }
        if (jQuery.inArray("costdetails-land_acquisition_method", visible_field_ids) !== -1) {
            makeTheSelectFieldMandatory(e, "costdetails-land_acquisition_method", "field-costdetails-land_acquisition_method", "Land Acquisition Method");
        }




    });


    makeTheTextField3NonMandatoryOnChange("costdetails-original_purchase_date", "original_purchase_date");
    makeTheTextField3NonMandatoryOnChange("costdetails-original_land_purchase_date", "original_land_purchase_date");
    makeTheTextField3NonMandatoryOnChange("costdetails-land_lease_expiry_date", "land_lease_expiry_date");
    makeTheTextFieldNonMandatoryOnChange("costdetails-land_lease_costs", "land_lease_costs");
    makeTheTextField3NonMandatoryOnChange("costdetails-total_no_of_lease_years", "total_no_of_lease_years");
    makeTheTextField3NonMandatoryOnChange("costdetails-current_transaction_price_date", "current_transaction_price_date");
    makeTheTextFieldNonMandatoryOnChange("costdetails-transaction_price", "transaction_price");

    makeTheSelectFieldNonMandatoryOnChange("costdetails-acquisition_method", "field-costdetails-acquisition_method");
    makeTheSelectFieldNonMandatoryOnChange("costdetails-land_acquisition_method", "field-costdetails-land_acquisition_method");


});


function makeTheDropDownFieldMandatory(e, field_id, field_group_class, field_name) {
    if ($('#' + field_id).val() === '') {
        e.preventDefault();
        $("#" + field_id).focus();
        $("." + field_group_class + " .help-block").html(field_name + " cannot be blank.").show();
        $("." + field_group_class).addClass("has-error");
        $("." + field_group_class + " .form-control").css("border-color", "#a94442");
    }
    else {
        $("." + field_group_class + " .help-block").html("").hide();
        $("." + field_group_class).removeClass("has-error");
        $("." + field_group_class + " .form-control").css("border-color", "");
    }
}

// function makeTheSelectFieldMandatory(e, field_id, field_group_class, field_name) {
//     if ($('#' + field_id).val() === '') {
//         e.preventDefault();
//         $("#" + field_id).focus();
//         $("." + field_group_class + " .help-block").html(field_name + " cannot be blank.").show();
//         $("." + field_group_class).addClass("has-error");
//         $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "#a94442");
//     }
//     else {
//         $("." + field_group_class + " .help-block").html("").hide();
//         $("." + field_group_class).removeClass("has-error");
//         $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "");
//     }
// }

function makeTheSelectFieldMandatory(e, field_id, field_group_class, field_name) {
    var selectedValues = $('#' + field_id).val();
    if (!selectedValues || selectedValues.length === 0) {
        e.preventDefault();
        $("#" + field_id).focus();
        $("." + field_group_class + " .help-block").html(field_name + " cannot be blank.").show();
        $("." + field_group_class).addClass("has-error");
        $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "#a94442");
    } else {
        $("." + field_group_class + " .help-block").html("").hide();
        $("." + field_group_class).removeClass("has-error");
        $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "");
    }
}

function makeTheSelectField2Mandatory(e, field_id, field_main_class, field_group_class, field_name) {
    if ($('#' + field_id).val() === '') {
        e.preventDefault();
        $("#" + field_id).focus();
        $("." + field_main_class).addClass("mb-3");
        $("." + field_main_class + " .help-block2").html(field_name + " cannot be blank.").show();
        $("." + field_group_class).addClass("has-error");
        $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "#a94442");
    }
    else {
        $("." + field_main_class).removeClass("mb-3");
        $("." + field_main_class + "  .help-block2").html("").hide();
        $("." + field_group_class).removeClass("has-error");
        $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "");
    }
}

function makeTheTextFieldMandatory(e, field_id, field_main_class, field_group_class, field_name) {
    if ($("#" + field_id).val() === "") {
        e.preventDefault();
        // $("#" + field_id).focus();
        $("." + field_main_class + " .help-block2").html(field_name + " cannot be blank.").show();
        $("." + field_main_class).addClass("mb-3");
        $("." + field_group_class).addClass("has-error");
        // $("#" + field_id).css("border-color", "#a94442");
    }
    else {
        $("." + field_main_class + " .help-block2").html("").hide();
        $("." + field_main_class).removeClass("mb-3");
        $("." + field_group_class).removeClass("has-error");
        $("#" + field_id).css("border-color", "");
    }
}

function makeTheTextField2Mandatory(e, field_id, field_main_class, field_group_class, field_name) {
    if ($("#" + field_id).val() === "") {
        e.preventDefault();
        $("." + field_main_class + " .help-block").html(field_name + " cannot be blank.").show();
        $("." + field_main_class).addClass("mb-3");
        $("." + field_group_class).addClass("has-error");
        // $("#" + field_id).css("border-color", "#a94442");
    }
    else {
        $("." + field_main_class + " .help-block").html("").hide();
        $("." + field_main_class).removeClass("mb-3");
        $("." + field_group_class).removeClass("has-error");
        $("#" + field_id).css("border-color", "");
    }
}

function makeTheTextField3Mandatory(e, field_id, field_main_class, field_group_class, field_name) {
    if ($("#" + field_id).val() === "") {
        e.preventDefault();
        $("." + field_main_class + " .help-block").html(field_name + " cannot be blank.").show();
        $("." + field_main_class).addClass("mb-3");
        $("." + field_group_class).addClass("has-error");
    }
    else {
        $("." + field_main_class + " .help-block").html("").hide();
        $("." + field_main_class).removeClass("mb-3");
        $("." + field_group_class).removeClass("has-error");
    }
}

function makeTheDropDownNonMandatoryOnChange(field_id, field_group_class) {
    $('#' + field_id).on('change', function () {
        if ($(this).val() !== '') {
            $("." + field_group_class + " .help-block").html("").hide();
            $("." + field_group_class).removeClass("has-error");
            $("." + field_group_class + " .form-control").css("border-color", "");

        }
    });
}

function makeTheSelectFieldNonMandatoryOnChange(field_id, field_group_class) {
    $('#' + field_id).on('change', function () {
        if ($(this).val() !== '') {
            $("." + field_group_class + " .help-block").html("").hide();
            $("." + field_group_class).removeClass("has-error");
            $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "");

        }
    });
}

function makeTheSelectField2NonMandatoryOnChange(field_id, field_group_class, field_main_class) {
    $("#" + field_id).on("change", function () {
        if ($(this).val() !== "") {
            $("." + field_main_class + " .help-block2").html("").hide();
            $("." + field_main_class).removeClass("mb-3");
            $("." + field_group_class).removeClass("has-error");
            $("." + field_group_class + " .select2-container .select2-selection").css("border-color", "");
        }
    });
}

function makeTheTextFieldNonMandatoryOnChange(field_id, field_main_class) {
    $("#" + field_id).on('change', function () {
        if ($(this).val() !== '') {
            $("." + field_main_class + " .help-block2").html("").hide();
            $("#" + field_id).css("border-color", "");
        }
    });
}

function makeTheTextField2NonMandatoryOnChange(field_id, field_main_class) {
    $("#" + field_id).on('change', function () {
        if ($(this).val() !== '') {
            $("." + field_main_class + " .help-block").html("").hide();
            $("#" + field_id).css("border-color", "");
        }
    });
}

function makeTheTextField3NonMandatoryOnChange(field_id, field_main_class) {
    $("#" + field_id).on('change', function () {
        console.log('text4');
        if ($(this).val() !== '') {
            $("." + field_main_class + " .help-block").html("").hide();
            $("#" + field_id).css("border-color", "");
        }
    });
}


$(document).ready(function () {
    if ($('#incomeApproach').length > 0) {
        // Remove 'active' class from Process MA
        $('.nav-link:contains("Process MA Valuation")').closest('li').removeClass('menu-open');
        $('.nav-link:contains("Process MA Valuation")').closest('a').removeClass('active');

        // Add 'active' class to Process IA
        $('.nav-link:contains("Process IA Valuation")').closest('li').addClass('menu-open');
        $('.nav-link:contains("Process IA Valuation")').closest('a').addClass('active');
    }
});