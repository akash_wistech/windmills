jQuery(document).ready(function() {

	$("body").on("change", ".building", function () {
		_this=$(this);

		var building_id = $(this).val();
		// console.log(building_id);
		var url_building = 'valuation-quotation/detail?id='+building_id;

		//heading=$(this).data("heading");
		$.ajax({
			url: url_building,
			dataType: "html",
			success: function(data) {
				// console.log(data);
                var response_building = JSON.parse(data);
                console.log(response_building.required_documents);
								_this.parents(".repeat-section").find(".property").val(response_building.detail.property_id).trigger('change');
								_this.parents(".repeat-section").find(".property-category").val(response_building.detail.property_category).trigger('change');
								_this.parents(".repeat-section").find(".city").val(response_building.detail.city);
								_this.parents(".repeat-section").find(".community").val(response_building.detail.community);
								_this.parents(".repeat-section").find(".sub-community").val(response_building.detail.sub_community);
								_this.parents(".repeat-section").find(".no_of_floors").val(response_building.detail.typical_floors);

								_this.parents(".repeat-section").find(".building_number").val(response_building.detail.building_number);
								_this.parents(".repeat-section").find(".plot_number").val(response_building.detail.plot_number);
								// _this.parents(".repeat-section").find(".no_of_units").val(response_building.detail.no_of_units);
								_this.parents(".repeat-section").find(".latitude").val(response_building.detail.latitude);
								_this.parents(".repeat-section").find(".longitude").val(response_building.detail.longitude);
								_this.parents(".repeat-section").find(".street").val(response_building.detail.street);

								_this.parents(".repeat-section").find(".tenure").val(response_building.detail.tenure).trigger('change');
								_this.parents(".repeat-section").find(".valuation_approach").val(response_building.valuation_approach).trigger('change');
								// _this.parents(".repeat-section").find(".required_documents").val(response_building.required_documents).trigger('change');
								// _this.parents(".repeat-section").find(".documents").val(response_building.required_documents).trigger('change');

//hide bua when property is  land and building .. show bua when not land and building
if (response_building.detail.property_id==3 || response_building.detail.property_id==7 || response_building.detail.property_id==22 || response_building.detail.property_id==4 || response_building.detail.property_id==5 || response_building.detail.property_id==11 || response_building.detail.property_id==23 || response_building.detail.property_id==26 || response_building.detail.property_id==29) {
	_this.parents(".repeat-section").find(".bua").hide();
}else{
	_this.parents(".repeat-section").find(".bua").show();
}

// show land when property is land and hide land when property is not land type
if (response_building.detail.property_id==4 || response_building.detail.property_id==5 || response_building.detail.property_id==11 || response_building.detail.property_id==23 || response_building.detail.property_id==26 || response_building.detail.property_id==29) {
	_this.parents(".repeat-section").find(".land-size").show();
}else{
	_this.parents(".repeat-section").find(".land-size").hide();
}

var numOfPropertiesList = [];
//change value of num of units if property type is building
if (response_building.detail.property_id==3 || response_building.detail.property_id==7 || response_building.detail.property_id==22) {
  numOfPropertiesList = buildingNoArr;
}
//chage value of num of properties when property type is land
else{
		numOfPropertiesList = LandNoArr;
}
// else if(response_building.detail.property_id==4 || response_building.detail.property_id==5 || response_building.detail.property_id==11 || response_building.detail.property_id==23 || response_building.detail.property_id==26 || response_building.detail.property_id==29){
//     numOfPropertiesList = LandNoArr;
// }

	nplel = _this.parents(".repeat-section").find(".no_of_units");

if (numOfPropertiesList!="") {
	nplel.html("");
	$.each(numOfPropertiesList, function(key,value){
		nplel.append('<option value="'+key+'">'+value+'</option>');
	});
	nplel.trigger('change');
}
else{
	// _this.parents(".repeat-section").find(".num-unit").hide();
	nplel.html("");
}


// if (response_building.detail.property_id==1 || response_building.detail.property_id==28 || response_building.detail.property_id==17) {
// 		_this.parents(".repeat-section").find(".land-size").hide();
// 		// $(".land-size").hide();
// }else{
// 	_this.parents(".repeat-section").find(".land-size").show();
// }

									MasterCalculation(_this);


			},
			error: bbAlert
		});
	});



$("body").on("change", "#valuationquotation-client_type,#valuationquotation-advance_payment_terms,.property,.city,.tenure,.complexity,.repeat_valuation,.built_up_area,.type_of_valuation,.number_of_comparables,.no_of_units,.land", function () {
	_this=$(this);
		MasterCalculation(_this);
});


function MasterCalculation(_this)
{
	var clientType = $("#valuationquotation-client_type").val();
	var paymentTerms = $("#valuationquotation-advance_payment_terms").val();

var property = _this.parents(".repeat-section").find(".property").val();
 var city = _this.parents(".repeat-section").find(".city").val();
  var tenure = _this.parents(".repeat-section").find(".tenure").val();
   var complexity = _this.parents(".repeat-section").find(".complexity").val();
    var repeat_valuation = _this.parents(".repeat-section").find(".repeat_valuation").val();
    var built_up_area = _this.parents(".repeat-section").find(".built_up_area").val();
    var type_of_valuation = _this.parents(".repeat-section").find(".type_of_valuation").val();
   var number_of_comparables = _this.parents(".repeat-section").find(".number_of_comparables").val();
  var no_of_units = _this.parents(".repeat-section").find(".no_of_units").val();
 var land = _this.parents(".repeat-section").find(".land").val();
var built_up_area = _this.parents(".repeat-section").find(".built_up_area").val();

var url_address = 'valuation-quotation/calculation'
var data = {clientType:clientType,paymentTerms:paymentTerms,property:property,city:city,tenure:tenure,complexity:complexity,repeat_valuation:repeat_valuation,built_up_area:built_up_area,type_of_valuation:type_of_valuation,number_of_comparables:number_of_comparables,no_of_units:no_of_units,land:land};

      $.ajax({
          url: url_address,
          data: data,
          method: 'post',
          dataType: "JSON",
          success: function(data) {
          	console.log(data);
          	 _this.parents(".repeat-section").find(".property-quotation-fee").val(data.fee);
          	 // _this.parents(".repeat-section").find(".property-quotation-tat").val(data.tat);

      },
  });
}

		$("body").on("change", ".client", function () {
		_this=$(this);
			// alert("hello");
		var client_id = $(this).val();
		// console.log(client_id);
		var url_client = 'valuation-quotation/client-detail?id='+client_id;

		//heading=$(this).data("heading");
		$.ajax({
			url: url_client,
			dataType: "html",
			success: function(data) {
				// console.log(data);
                var response_client = JSON.parse(data);
                 // console.log(response_client);

           $(".client_reference").val(response_client.clientReference);
           $(".client_customer_name").val(response_client.clientCustomerName);
           $(".name").val(response_client.clientCustomerName);
           $(".email").val(response_client.email);

           if(response_client.phoneNumber.includes("+")==true){
          		var mobile = response_client.phoneNumber.replace("+", "");
          		// console.log(mobile);
          		// console.log(response_client.phoneNumber);
          		$(".phone-number").val(mobile);
            }
            else{
           		$(".phone-number").val(response_client.phoneNumber);
            }

			},
			error: bbAlert
		});
	});






	$("body").on("change", ".property", function () {
		_this=$(this);

	  var index = $(this).data("index")
		var property_id = $(this).val();

		var url = 'valuation-quotation/documents'
		var data = {property_id:property_id, index:index};

		$.ajax({
			url: url,
			data: data,
			method: 'post',
			dataType: "html",
			success: function(data) {
								_this.parents(".property-card").find(".document-section").html(data);
								},
								error: bbAlert
		});
	});






		$("body").on("change", ".doc-file", function () {
		// alert("hello");
    _this=$(this);
    var file_data = $(this).prop('files')[0];
   image_id = (this.dataset.unique_id);
    // console.log("hello");
    var form_data = new FormData();
    form_data.append('file', file_data);
    // data = {form_data:form_data, image_id:image_id }

 var url = 'valuation-quotation/upload'
    $.ajax({
      url: url,
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function(data){
          // console.log(data);
          _this.parents(".meri-marzi").find("img").attr("src",data.file.href);
          _this.parents(".meri-marzi").find(".attachment-address").val(data.file.href);
          // console.log(_this.parents(".meri-marzi").find("img").attr("src"));
          // _this.parents(".document-card").find(".attachment-address").val(data.href);

      }
   });

  });
















	$("body").on("change", "#valuationquotation-related_to_buyer", function () {
		var status = $(this).val();
		if(status == 'Yes'){
				$("#related_to_buyer_reason").show();
		}else{
				$("#related_to_buyer_reason").hide();
		}

});

$("body").on("change", "#valuationquotation-related_to_owner", function () {
		var status = $(this).val();
		if(status == 'Yes'){
				$("#related_to_owner_reason").show();
		}else{
				$("#related_to_owner_reason").hide();
		}

});

$("body").on("change", "#valuationquotation-related_to_client", function () {
		var status = $(this).val();
		if(status == 'Yes'){
				$("#related_to_client_reason").show();
		}else{
				$("#related_to_client_reason").hide();
		}

});

$("body").on("change", "#valuationquotation-related_to_property", function () {
		var status = $(this).val();
		if(status == 'Yes'){
				$("#related_to_property_reason").show();
		}else{
				$("#related_to_property_reason").hide();
		}

});



});

function bbAlert(xhr, ajaxOptions, thrownError){
    Swal.fire(thrownError,xhr.statusText + "\n" + xhr.responseText);
    $(".blockUI").remove();
}
