jQuery(document).ready(function() {

	$("body").on("change", "#soldtransaction-building_info", function () {
		modalId="general-modal";

		var building_id = $(this).val();
		var url_building = 'buildings/detail/'+building_id;

		heading=$(this).data("heading");
		$.ajax({
			url: url_building,
			dataType: "html",
			success: function(data) {

                var response_building = JSON.parse(data);
                console.log(response_building);

                if(response_building.detail.property_category != 0){
                    $('#soldtransaction-property_category').val(response_building.detail.property_category).trigger('change');
                }else{
                    $('#soldtransaction-property_category').val(1).trigger('change');
                }
                if(response_building.detail.location != null){
                    $('#soldtransaction-location').val(response_building.detail.location).trigger('change');
                }else{
                    $('#soldtransaction-location').val(1).trigger('change');
                }
                if(response_building.detail.property_category != 0){
                    $('#soldtransaction-property_category').val(response_building.detail.property_category).trigger('change');
                }else{
                    $('#soldtransaction-property_category').val(1).trigger('change');
                }
                if(response_building.detail.tenure != 0){
                    $('#soldtransaction-tenure').val(response_building.detail.tenure).trigger('change');
                }else{
                    $('#soldtransaction-tenure').val(1).trigger('change');
                }
                if(response_building.detail.utilities_connected != ''){
                    $('#soldtransaction-utilities_connected').val(response_building.detail.utilities_connected).trigger('change');
                }else{
                    $('#soldtransaction-utilities_connected').val('Yes').trigger('change');
                }
                if(response_building.detail.development_type != null){
                    $('#soldtransaction-development_type').val(response_building.detail.development_type).trigger('change');
                }else{
                    $('#soldtransaction-development_type').val('Standard').trigger('change');
                }
                if(response_building.detail.property_placement != 0){
                    $('#soldtransaction-property_placement').val(response_building.detail.property_placement).trigger('change');
                }else{
                    $('#soldtransaction-property_placement').val(1).trigger('change');
                }
                if(response_building.detail.property_visibility != 0){
                    $('#soldtransaction-property_visibility').val(response_building.detail.property_visibility).trigger('change');
                }else{
                    $('#soldtransaction-property_visibility').val(1).trigger('change');
                }
                if(response_building.detail.property_exposure != 0){
                    $('#soldtransaction-property_exposure').val(response_building.detail.property_exposure).trigger('change');
                }else{
                    $('#soldtransaction-property_exposure').val(1).trigger('change');
                }

                if(response_building.detail.property_condition != 0){
                    $('#soldtransaction-property_condition').val(response_building.detail.property_condition).trigger('change');
                }else{
                    $('#soldtransaction-property_condition').val(1).trigger('change');
                }
                if(response_building.detail.completion_status != null){
                    $('#soldtransaction-completion_status').val(response_building.detail.completion_status).trigger('change');
                }else{
                    $('#soldtransaction-completion_status').val('Ready').trigger('change');
                }
                if(response_building.detail.pool != null){
                    $('#soldtransaction-pool').val(response_building.detail.pool).trigger('change');
                }else{
                    $('#soldtransaction-pool').val('Yes').trigger('change');
                }
                if(response_building.detail.gym != null){
                    $('#soldtransaction-gym').val(response_building.detail.gym).trigger('change');
                }else{
                    $('#soldtransaction-gym').val('Yes').trigger('change');
                }
                if(response_building.detail.play_area != null){
                    $('#soldtransaction-play_area').val(response_building.detail.play_area).trigger('change');
                }else{
                    $('#soldtransaction-play_area').val('Yes').trigger('change');
                }


                if(response_building.detail.other_facilities != null){
                    var facilities = response_building.detail.other_facilities;
                    var facilities = facilities.split(",");
                    $('#soldtransaction-other_facilities').val(facilities).trigger('change');
                }else{
                    $('#soldtransaction-other_facilities').val([]).trigger('change');
                }
                if(response_building.detail.landscaping != null){
                    $('#soldtransaction-landscaping').val(response_building.detail.landscaping).trigger('change');
                }else{
                    $('#soldtransaction-landscaping').val('Yes').trigger('change');
                }
                if(response_building.detail.white_goods != null){
                    $('#soldtransaction-white_goods').val(response_building.detail.white_goods).trigger('change');
                }else{
                    $('#soldtransaction-white_goods').val('Yes').trigger('change');
                }
                if(response_building.detail.furnished != null){
                    $('#soldtransaction-furnished').val(response_building.detail.furnished).trigger('change');
                }else{
                    $('#soldtransaction-furnished').val('Yes').trigger('change');
                }
                if(response_building.detail.finished_status != null){
                    $('#soldtransaction-finished_status').val(response_building.detail.finished_status).trigger('change');
                }else{
                    $('#soldtransaction-finished_status').val('Shell & Core').trigger('change');
                }
			},
			error: bbAlert
		});
	});
});
function bbAlert(xhr, ajaxOptions, thrownError){
    Swal.fire(thrownError,xhr.statusText + "\n" + xhr.responseText);
    $(".blockUI").remove();
}
