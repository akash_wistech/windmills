jQuery(document).ready(function() {

    $("body").on("change", "#valuationdetail-building_info", function () {

        var building_id = $(this).val();
        var url_building = 'buildings/detail/'+building_id;

        heading=$(this).data("heading");
        $.ajax({
            url: url_building,
            dataType: "html",
            success: function(data) {

                var response_building = JSON.parse(data);
                console.log(response_building);

                if(response_building.detail.property_category != 0){
                    $('#valuationdetail-property_category').val(response_building.detail.property_category).trigger('change');
                }else{
                    $('#valuationdetail-property_category').val(1).trigger('change');
                }
                if(response_building.detail.property_id != 0){
                    $('#valuationdetail-property_id').val(response_building.detail.property_id).trigger('change');
                }else{
                    $('#valuationdetail-property_id').val(1).trigger('change');
                }
                if(response_building.detail.tenure != 0){
                    $('#valuationdetail-tenure').val(response_building.detail.tenure).trigger('change');
                }else{
                    $('#valuationdetail-tenure').val(1).trigger('change');
                }
                if(response_building.detail.building_number != ''){
                    $('#valuationdetail-building_number').val(response_building.detail.building_number);
                }
                if(response_building.detail.plot_number != ''){
                    $('#valuationdetail-plot_number').val(response_building.detail.plot_number);
                }
                if(response_building.detail.street != ''){
                    $('#valuationdetail-street').val(response_building.detail.street);
                }
                if(response_building.detail.city != ''){
                    $('#valuationdetail-city').val(response_building.detail.city);
                }
                if(response_building.detail.street != ''){
                    $('#valuationdetail-street').val(response_building.detail.street);
                }
                if(response_building.detail.city != ''){
                    $('#valuationdetail-country').val(response_building.country);
                }

                if(response_building.detail.community != ''){
                    $('#valuationdetail-community').val(response_building.detail.community);
                }
                if(response_building.detail.sub_community != ''){
                    $('#valuationdetail-sub_community').val(response_building.detail.sub_community);
                }

                /* if(response_building.detail.city != 0){
                 /!* ("#valuationdetail-city").select2("destroy").val(response_building.detail.city).trigger("change").select2();*!/

                 $('#community_value').val(response_building.detail.community);
                 $('#valuationdetail-city').val(response_building.detail.city).trigger('change');

                 }else{
                 $('#valuationdetail-city').val(1).trigger('change');
                 }*/

            },
            error: bbAlert
        });
    });

	$("body").on("change", "#valuation-building_info", function () {

		var building_id = $(this).val();
		var url_building = 'buildings/detail/'+building_id;

		heading=$(this).data("heading");
		$.ajax({
			url: url_building,
			dataType: "html",
			success: function(data) {

                var response_building = JSON.parse(data);
                console.log(response_building);

                if(response_building.detail.property_category != 0){
                    $('#valuation-property_category').val(response_building.detail.property_category).trigger('change');
                }else{
                    $('#valuation-property_category').val(1).trigger('change');
                }
                if(response_building.detail.property_id != 0){
                    $('#valuation-property_id').val(response_building.detail.property_id).trigger('change');
                }else{
                    $('#valuation-property_id').val(1).trigger('change');
                }
                if(response_building.detail.tenure != 0){
                    $('#valuation-tenure').val(response_building.detail.tenure).trigger('change');
                }else{
                    $('#valuation-tenure').val(1).trigger('change');
                }
                if(response_building.detail.building_number != ''){
                    $('#valuation-building_number').val(response_building.detail.building_number);
                }
                if(response_building.detail.plot_number != ''){
                    $('#valuation-plot_number').val(response_building.detail.plot_number);
                }
                if(response_building.detail.street != ''){
                    $('#valuation-street').val(response_building.detail.street);
                }
                if(response_building.detail.city != ''){
                    $('#valuation-city').val(response_building.detail.city);
                }

                if(response_building.detail.community != ''){
                    $('#valuation-community').val(response_building.detail.community);
                }
                if(response_building.detail.sub_community != ''){
                    $('#valuation-sub_community').val(response_building.detail.sub_community);
                }

               /* if(response_building.detail.city != 0){
                   /!* ("#valuation-city").select2("destroy").val(response_building.detail.city).trigger("change").select2();*!/

                    $('#community_value').val(response_building.detail.community);
                    $('#valuation-city').val(response_building.detail.city).trigger('change');

                }else{
                    $('#valuation-city').val(1).trigger('change');
                }*/

			},
			error: bbAlert
		});
	});

    $("body").on("change", "#brokerageinquires-building_info", function () {

        var building_id = $(this).val();
        var url_building = 'buildings/detail/'+building_id;

        heading=$(this).data("heading");
        $.ajax({
            url: url_building,
            dataType: "html",
            success: function(data) {

                var response_building = JSON.parse(data);
                console.log(response_building);

                if(response_building.detail.property_category != 0){
                    $('#brokerageinquires-property_category').val(response_building.detail.property_category).trigger('change');
                }else{
                    $('#brokerageinquires-property_category').val(1).trigger('change');
                }
                if(response_building.detail.property_id != 0){
                    $('#brokerageinquires-property_id').val(response_building.detail.property_id).trigger('change');
                }else{
                    $('#brokerageinquires-property_id').val(1).trigger('change');
                }
                if(response_building.detail.tenure != 0){
                    $('#brokerageinquires-tenure').val(response_building.detail.tenure).trigger('change');
                }else{
                    $('#brokerageinquires-tenure').val(1).trigger('change');
                }
                if(response_building.detail.building_number != ''){
                    $('#brokerageinquires-building_number').val(response_building.detail.building_number);
                }
                if(response_building.detail.plot_number != ''){
                    $('#brokerageinquires-plot_number').val(response_building.detail.plot_number);
                }
                if(response_building.detail.street != ''){
                    $('#brokerageinquires-street').val(response_building.detail.street);
                }
                if(response_building.detail.city != ''){
                    $('#brokerageinquires-city').val(response_building.detail.city);
                }

                if(response_building.detail.community != ''){
                    $('#brokerageinquires-community').val(response_building.detail.community);
                }
                if(response_building.detail.sub_community != ''){
                    $('#brokerageinquires-sub_community').val(response_building.detail.sub_community);
                }

                /* if(response_building.detail.city != 0){
                 /!* ("#valuation-city").select2("destroy").val(response_building.detail.city).trigger("change").select2();*!/

                 $('#community_value').val(response_building.detail.community);
                 $('#valuation-city').val(response_building.detail.city).trigger('change');

                 }else{
                 $('#valuation-city').val(1).trigger('change');
                 }*/

            },
            error: bbAlert
        });
    });

    $("body").on("change", "#clientvaluation-building_info", function () {

		var building_id = $(this).val();
		var url_building = 'buildings/detail/'+building_id;

		heading=$(this).data("heading");
		$.ajax({
			url: url_building,
			dataType: "html",
			success: function(data) {

                var response_building = JSON.parse(data);
                console.log(response_building);

                if(response_building.detail.property_category != 0){
                    $('#clientvaluation-property_category').val(response_building.detail.property_category).trigger('change');
                }else{
                    $('#clientvaluation-property_category').val(1).trigger('change');
                }
                if(response_building.detail.property_id != 0){
                    $('#clientvaluation-property_id').val(response_building.detail.property_id).trigger('change');
                }else{
                    $('#clientvaluation-property_id').val(1).trigger('change');
                }
                if(response_building.detail.tenure != 0){
                    $('#clientvaluation-tenure').val(response_building.detail.tenure).trigger('change');
                }else{
                    $('#clientvaluation-tenure').val(1).trigger('change');
                }
                if(response_building.detail.building_number != ''){
                    $('#clientvaluation-building_number').val(response_building.detail.building_number);
                }
                if(response_building.detail.plot_number != ''){
                    $('#clientvaluation-plot_number').val(response_building.detail.plot_number);
                }
                if(response_building.detail.street != ''){
                    $('#clientvaluation-street').val(response_building.detail.street);
                }
                if(response_building.detail.city != ''){
                    $('#clientvaluation-city').val(response_building.detail.city);
                }
                if(response_building.detail.city != ''){
                    $('#clientvaluation-country').val(response_building.country);
                }

                if(response_building.detail.community != ''){
                    $('#clientvaluation-community').val(response_building.detail.community);
                }
                if(response_building.detail.sub_community != ''){
                    $('#clientvaluation-sub_community').val(response_building.detail.sub_community);
                }

               /* if(response_building.detail.city != 0){
                   /!* ("#valuation-city").select2("destroy").val(response_building.detail.city).trigger("change").select2();*!/

                    $('#community_value').val(response_building.detail.community);
                    $('#valuation-city').val(response_building.detail.city).trigger('change');

                }else{
                    $('#valuation-city').val(1).trigger('change');
                }*/

			},
			error: bbAlert
		});
	});
});
function bbAlert(xhr, ajaxOptions, thrownError){
    Swal.fire(thrownError,xhr.statusText + "\n" + xhr.responseText);
    $(".blockUI").remove();
}
