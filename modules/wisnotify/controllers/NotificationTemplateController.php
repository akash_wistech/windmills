<?php
    // echo "string";
    // die();
namespace app\modules\wisnotify\controllers;
use Yii;
use app\modules\wisnotify\models\NotificationTemplate;
use app\modules\wisnotify\models\NotificationTemplateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class NotificationTemplateController extends Controller
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
  * Lists all NotificationTemplate models.
  * @return mixed
  */
  public function actionIndex()
  {

    $searchModel = new NotificationTemplateSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new NotificationTemplate model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    // $this->checkLogin();
    $model = new NotificationTemplate();

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Trigger saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing NotificationTemplate model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    // $this->checkLogin();
    $model = $this->findModel($id);
    if($model->langDetails!=null){
      foreach($model->langDetails as $langDetail){
        $model->subject[$langDetail->lang] = $langDetail->subject;
        $model->email_body[$langDetail->lang] = $langDetail->email_body;
        $model->sms_body[$langDetail->lang] = $langDetail->sms_body;
        $model->push_body[$langDetail->lang] = $langDetail->push_body;
      }
    }

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Trigger saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new NotificationTemplate;
    return $model;
  }

  /**
  * Finds the NotificationTemplate model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return NotificationTemplate the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = NotificationTemplate::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
    }
  }
}
