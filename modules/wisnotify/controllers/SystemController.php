<?php

namespace app\modules\wisnotify\controllers;

use Yii;
use app\modules\wisnotify\models\Setting;
use app\modules\wisnotify\models\UserSetting;
use app\modules\wisnotify\models\NotificationTrigger;
use app\modules\wisnotify\models\NotificationSettingsForm;
use app\components\helpers\DefController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
* SystemController implements the actions for notification system.
*/
class SystemController extends Controller
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Notification settings
  * @return mixed
  */
  public function actionSettings()
  {
    // $this->checkLogin();
    $model = new NotificationSettingsForm;

    $sValues['NotificationSettingsForm']=[];
    $notificationSettings = ['enable_email_notification','enable_sms_notification','enable_inapp_notification'];
    $settings=Setting::find()->where(['config_name'=>$notificationSettings])->asArray()->all();
    foreach($settings as $record){
      $sValues['NotificationSettingsForm'][$record['config_name']]=$record['config_value'];
    }
    $model->load($sValues);

    return $this->render('setting', [
      'model' => $model,
    ]);
  }

  /**
  * Update notification setting
  * @return json
  */
  public function actionSaveNotification($fld)
  {
    // $this->checkLogin();
    Yii::$app->response->format = Response::FORMAT_JSON;
    $model = Setting::find()->where(['config_name'=>$fld])->one();
    if($model!=null){
      if($model->config_value==0){
        $val=1;
      }else{
        $val=0;
      }
      $model->config_value=$val;
    }else{
      $model = new Setting;
      $model->config_name=$fld;
      $model->config_value=1;
    }
    $model->save();
    return [
      'success'=>[
        'heading'=>Yii::t('app','Success'),
        'msg'=>Yii::t('app','Settings updated successfully')
      ]
    ];
  }
}
