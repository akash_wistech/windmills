<?php
namespace app\modules\wisnotify\listners;

use Yii;
use yii\base\Event;
use yii\base\Component;
use app\modules\wisnotify\models\NotificationTrigger;
use app\modules\wisnotify\models\NotificationTemplate;
use app\models\User;
use yii\helpers\Html;

class NotifyEvent extends Component
{
    public static function fire($eventId,$data)
    {


        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client
                        // print_r($data);
                        //              die();
                        $toUser = $data['client'];


                        if($toUser->id == 1){
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => 'DIBUAEESDExternalEvaluation@dib.ae',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                            ];
                        }else if($toUser->id == 54514){
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => 'TanfeethICSHFevaluations@tanfeeth.ae',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                            ];
                        }
                        else {
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => $toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                            ];
                        }


                    }
                    elseif($userType==8 && $data['reviewer']!=null){
                        $toUser = $data['reviewer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname,
                            'email'=>$toUser->email,
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                        ];
                    }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();
                    //custom cc users

                    $custom_cc_status= array();
                    if($userType==0 && $data['client']!=null){
                         $mainClientStatuses  = $data['client']->customEmails;
                         if(!empty($mainClientStatuses)){
                             foreach ($mainClientStatuses as $mclients){
                                 $custom_cc_status[] = $mclients->email;
                             }
                         }
                    }

                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){



                                (new NotifyEvent)->sendEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments,$custom_cc_status);


                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }
    public static function fireMonthly($eventId,$data)
    {


        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client
                        // print_r($data);
                        //              die();
                        $toUser = $data['client'];


                        if($toUser->id == 1){
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => 'DIBUAEESDExternalEvaluation@dib.ae',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => '',
                            ];
                        }else if($toUser->id == 54514){
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => 'TanfeethICSHFevaluations@tanfeeth.ae',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                            ];
                        }else {
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => $toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => '',
                            ];
                        }


                    }
                    elseif($userType==8 && $data['reviewer']!=null){
                        $toUser = $data['reviewer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname,
                            'email'=>$toUser->email,
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                        ];
                    }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();
                    //custom cc users

                    $custom_cc_status= array();
                    if($userType==0 && $data['client']!=null){
                        $mainClientStatuses  = $data['client']->customEmails;
                        if(!empty($mainClientStatuses)){
                            foreach ($mainClientStatuses as $mclients){
                                $custom_cc_status[] = $mclients->email;
                            }
                        }
                    }

                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){



                                (new NotifyEvent)->sendEmailMonthly($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments,$custom_cc_status);


                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }
    public static function fireMonthlyAllBanks($eventId,$data)
    {


        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client
                        // print_r($data);
                        //              die();
                        $toUser = $data['client'];


                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => $toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                               // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => '',
                            ];



                    }
                    elseif($userType==8 && $data['reviewer']!=null){
                        $toUser = $data['reviewer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname,
                            'email'=>$toUser->email,
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                        ];
                    }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();
                    //custom cc users

                    $custom_cc_status= array();
                    if($userType==0 && $data['client']!=null){
                        $mainClientStatuses  = $data['client']->customEmails;
                        if(!empty($mainClientStatuses)){
                            foreach ($mainClientStatuses as $mclients){
                                $custom_cc_status[] = $mclients->email;
                            }
                        }
                    }

                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){



                                (new NotifyEvent)->sendEmailMonthlyAllClients($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments,$custom_cc_status);


                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }
    public static function fire1($eventId,$data)
    {


        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client
                        // print_r($data);
                        //              die();
                        $toUser = $data['client'];

                        if($toUser->id == 1){
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => 'DIBUAEESDExternalEvaluation@dib.ae',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                                'uid' => $data['uid'],
                                'valuer' => $data['valuer'],
                                'inspector' => $data['inspector'],
                            ];
                        }else if($toUser->id == 54514){
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => 'TanfeethICSHFevaluations@tanfeeth.ae',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                                'valuer' => $data['valuer'],
                                'inspector' => $data['inspector'],
                            ];
                        }else {


                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => $toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                                'uid' => $data['uid'],
                                'valuer' => $data['valuer'],
                                'inspector' => $data['inspector'],
                            ];
                        }


                    }
                    elseif($userType==8 && $data['reviewer']!=null){
                        $toUser = $data['reviewer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname,
                            'email'=>$toUser->email,
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                            'uid' => $data['uid'],
                            'valuer' => $data['valuer'],
                            'inspector' => $data['inspector'],
                        ];
                    }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                    'uid' => $data['uid'],
                                    'valuer' => $data['valuer'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();

                    //custom cc users
                    $custom_cc_val= array();
                    $custom_cc_status= array();
                    if(isset($data['client']) && $data['client']->id == 230){

                        if ($userType == 0 && $data['client'] != null) {
                            $mainClientVals = $data['client']->customEmailsVals;
                            if (!empty($mainClientVals)) {
                                if(isset($data['attachments']) && $data['attachments'] <> null) {
                                    foreach ($mainClientVals as $mclientv) {
                                        $custom_cc_val[] = $mclientv->email;
                                    }
                                }
                            }
                        }
                    }else {
                        if ($userType == 0 && $data['client'] != null) {
                            $mainClientVals = $data['client']->customEmailsVals;
                            if (!empty($mainClientVals)) {
                                foreach ($mainClientVals as $mclientv) {
                                    $custom_cc_val[] = $mclientv->email;
                                }
                            }
                            /* $mainClientStatuses  = $data['client']->customEmails;
                             if(!empty($mainClientStatuses)){
                                 foreach ($mainClientStatuses as $mclients){
                                     $custom_cc_status[] = $mclients->email;
                                 }
                             }*/
                            /*  echo "<pre>";
                              print_r($custom_cc_val);
                              print_r($custom_cc_status);
                              // print_r($mainClient->customEmailsVals);
                              die;*/

                        }
                    }
                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){


                               // if(trim($data['subject']) == "Evaluation request for : EV-123456-4 .") {
                                    (new NotifyEvent)->sendEmail1($moduleId, $template, $replacements, $toInfo, $ccUsers, $attachments,$custom_cc_val);

                                //}
                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }
    public static function firetest($eventId,$data)
    {


        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client
                        // print_r($data);
                        //              die();
                        $toUser = $data['client'];

                        if($toUser->id == 1){
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => 'DIBUAEESDExternalEvaluation@dib.ae',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                                'uid' => $data['uid'],
                                'valuer' => $data['valuer'],
                            ];
                        }else if($toUser->id == 54514){
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => 'TanfeethICSHFevaluations@tanfeeth.ae',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                            ];
                        }else {


                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => $toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                                'uid' => $data['uid'],
                                'valuer' => $data['valuer'],
                            ];
                        }


                    }
                    elseif($userType==8 && $data['reviewer']!=null){
                        $toUser = $data['reviewer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname,
                            'email'=>$toUser->email,
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                            'uid' => $data['uid'],
                            'valuer' => $data['valuer'],
                        ];
                    }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                    'uid' => $data['uid'],
                                    'valuer' => $data['valuer'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();

                    //custom cc users
                    $custom_cc_val= array();
                    $custom_cc_status= array();
                    if($userType==0 && $data['client']!=null){
                        $mainClientVals  = $data['client']->customEmailsVals;
                        if(!empty($mainClientVals)){
                            foreach ($mainClientVals as $mclientv){
                                $custom_cc_val[] = $mclientv->email;
                            }
                        }
                        /* $mainClientStatuses  = $data['client']->customEmails;
                         if(!empty($mainClientStatuses)){
                             foreach ($mainClientStatuses as $mclients){
                                 $custom_cc_status[] = $mclients->email;
                             }
                         }*/
                        /*  echo "<pre>";
                          print_r($custom_cc_val);
                          print_r($custom_cc_status);
                          // print_r($mainClient->customEmailsVals);
                          die;*/

                    }
                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){


                                // if(trim($data['subject']) == "Evaluation request for : EV-123456-4 .") {
                                (new NotifyEvent)->sendEmailtest($moduleId, $template, $replacements, $toInfo, $ccUsers, $attachments,$custom_cc_val);

                                //}
                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }
    public static function fire2($eventId,$data)
    {



        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client

                        //              die();
                        $toUser = $data['client'];

                        $toInfo[] = [
                            'name'=>$toUser->title,
                            'email'=>$toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                            // 'email'=>'akash@wistech.biz',
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                        ];


                    }
                    elseif($userType==8 && $data['reviewer']!=null){
                        $toUser = $data['reviewer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname,
                            'email'=>$toUser->email,
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                        ];
                    }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();


                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){



                                (new NotifyEvent)->sendEmail2($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);


                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }
    public static function fire23($eventId,$data)
    {



        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client

                        //              die();
                        $toUser = $data['client'];

                        // $toInfo[] = [
                        //     'name'=>$toUser->title,
                        //     'email'=>$toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                        //     // 'email'=>'akash@wistech.biz',
                        //     'mobile'=>'',
                        //     'deviceIdz' => null,
                        //     'subject' => $data['subject'],
                        //     'uid' => $data['uid'],
                        // ];

                        if($data['instructing_person_email']!=null && $toUser->primaryContact->email != $data['instructing_person_email']){

                            $toInfo[] = [
                                'name'=>$toUser->title,
                                'email'=>$data['instructing_person_email'] !== '' ? $data['instructing_person_email'] : '',
                                // 'email'=>'akash@wistech.biz',
                                'mobile'=>'',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                                'uid' => $data['uid'],
                            ];
                        } else {
                            $toInfo[] = [
                                'name'=>$toUser->title,
                                'email'=>$toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                                // 'email'=>'akash@wistech.biz',
                                'mobile'=>'',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                                'uid' => $data['uid'],
                            ];
                        }


                    }
                    elseif($userType==8 && $data['reviewer']!=null){
                        $toUser = $data['reviewer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname,
                            'email'=>$toUser->email,
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                            'uid' => $data['uid'],
                        ];
                    }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                    'uid' => $data['uid'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();

                    //custom cc users
                    $custom_cc_val= array();
                    $custom_cc_status= array();
                    if(isset($data['client']) && $data['client']->id == 230){

                        if ($userType == 0 && $data['client'] != null) {
                            $mainClientVals = $data['client']->customEmailsVals;
                            if (!empty($mainClientVals)) {
                                if(isset($data['attachments']) && $data['attachments'] <> null) {
                                    foreach ($mainClientVals as $mclientv) {
                                        $custom_cc_val[] = $mclientv->email;
                                    }
                                }
                            }
                        }
                    }else {
                        if ($userType == 0 && $data['client'] != null) {
                            $mainClientVals = $data['client']->customEmailsVals;
                            if (!empty($mainClientVals)) {
                                foreach ($mainClientVals as $mclientv) {
                                    $custom_cc_val[] = $mclientv->email;
                                }
                            }
                            /* $mainClientStatuses  = $data['client']->customEmails;
                            if(!empty($mainClientStatuses)){
                                foreach ($mainClientStatuses as $mclients){
                                    $custom_cc_status[] = $mclients->email;
                                }
                            }*/
                            /*  echo "<pre>";
                            print_r($custom_cc_val);
                            print_r($custom_cc_status);
                            // print_r($mainClient->customEmailsVals);
                            die;*/

                        }
                    }
                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){



                                (new NotifyEvent)->sendEmail23($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments,$custom_cc_val);


                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }
    public static function fireToGovt($eventId, $data)
    {
        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        $g_subject = isset($data['g_subject']) ? $data['g_subject'] : null;
        $toUser = $data['client'];
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                // echo "<pre>"; print_r($ccUserTypes); echo "</pre><br>"; //die;
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client

                        //              die();
                        $toUser = $data['client'];

                        $toInfo[] = [
                            'name'=>$toUser->title,
                            //'email'=>$toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                             'email'=>'akashahmed4all@gmail.com',
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                        ];


                    }
                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();
                    // echo "<pre>"; print_r($ccUsers); echo "</pre>"; die;


                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){
                                (new NotifyEvent)->sendEmailToGovt($moduleId, $template, $replacements, $ccUsers, $attachments,$g_subject,$toUser);
                            }
                        }
                    }
                }
            }
        }
    }
    public static function fireToApprovers($eventId, $data)
    {
        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        $g_subject = isset($data['g_subject']) ? $data['g_subject'] : null;
        $toUser = $data['client'];
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                // echo "<pre>"; print_r($ccUserTypes); echo "</pre><br>"; //die;
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client

                        //              die();
                        $toUser = $data['client'];

                        $toInfo[] = [
                            'name'=>$toUser->title,
                            //'email'=>$toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                            'email'=>$data['valuer'],
                            'valuer'=>$data['valuer'],
                            'inspector'=>$data['inspector'],
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                        ];


                    }
                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();
                    // echo "<pre>"; print_r($ccUsers); echo "</pre>"; die;

                    $ccUsers['valuer']= $data['valuer'];
                    $ccUsers['inspector']= $data['inspector'];


                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){
                                (new NotifyEvent)->sendEmailToApprovers($moduleId, $template, $replacements, $ccUsers, $attachments,$g_subject,$toUser);
                            }
                        }
                    }
                }
            }
        }
    }

    public static function fireToSupport($eventId, $data)
    {
        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        $g_subject = isset($data['subject']) ? $data['subject'] : null;
        $toUser = $data['client'];
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                // echo "<pre>"; print_r($ccUserTypes); echo "</pre><br>"; //die;
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client

                        //              die();
                        $toUser = $data['client'];

                        $toInfo[] = [
                            'name'=>$toUser->title,
                            //'email'=>$toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                            'email'=>'akash@wistech.biz',
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $g_subject,
                        ];


                    }
                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();
                    // echo "<pre>"; print_r($ccUsers); echo "</pre>"; die;


                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){
                                (new NotifyEvent)->sendEmailToSupport($moduleId, $template, $replacements, $ccUsers, $attachments,$g_subject,$toUser);
                            }
                        }
                    }
                }
            }
        }
    }


    public function sendEmail1($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments,$custom_cc_val){


        $emailBody = $template->langDetail->email_body;
        $emailBody = str_replace('{forward_text}', '', $emailBody);
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        if($toInfo!=null){

            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailer->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailer->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    //$mail->setTextBody(strip_tags($emailBody));
                    //$mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['notificationSenderEmail'] => $moduleId->params['nNotificationSenderName']]);
                    /* echo "<pre>";
                     print_r($info);
                     die;*/

                    /* $info['email'];
                     die;*/
                    // $to[] = 'akash@wistech.biz';
                    $to[] = $info['email'];
                   // $mail->setTo($info['email']);
                   // $mail->setReplyTo($info['email']);
                    $reply_to =$info['email'];
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }

                    }
                    if($info['valuer'] <> null) {
                        $ccEmails[] = $info['valuer'];
                    }
                    $ccEmails = array_merge($ccEmails,$custom_cc_val);


                    /* gmail connection,with port number 993 */
                    $host = '{imap.gmail.com:993/ssl/novalidate-cert}';
                    /* Your gmail credentials */
                    $user = 'reports@windmillsgroup.com';
                    $password = '2023#moti@';
                    /*  $user = 'support@windmillsgroup.com';
                      $password = 'wmsupport2021#';*/

                    /* Establish a IMAP connection */
                    $conn = imap_open($host, $user, $password)
                    or die('unable to connect Gmail: ' . imap_last_error());

                    $allmessages = array();
                    $cc_addaresses=array();
                    $ccEmailscomplete = array();
                    $ccEmailscompleteUsers = array();

                    if ($info['subject']<>null) {

                        /* Search emails from gmail inbox*/
                       // $CheckSubject = imap_search($conn, 'SUBJECT "'.trim($info['subject']).'"');
                        if (str_contains($info['subject'], '&')) {
                            $sub_array = explode('&',$info['subject']);
                            $CheckSubject = imap_search($conn, 'SUBJECT "'.trim($sub_array[0]).'"');
                        }else if (str_contains($info['subject'], '–')) {

                            $sub_array = explode('-',$info['subject']);
                            $CheckSubject = imap_search($conn, 'SUBJECT "'.trim($sub_array[0]).'"');
                        }else{

                            $CheckSubject = imap_search($conn, 'SUBJECT "'.trim($info['subject']).'"');
                        }

                        if (!empty($CheckSubject)) {


                            foreach ($CheckSubject as $key => $Singelemail) {
                                $headers = imap_header($conn, $Singelemail, 0);

                                if($headers->subject == trim($info['subject'])){
                                    $allmessages[] = $headers->Msgno;
                                }



                                // $UserEmail=$headers->from[0]->mailbox.'@'.$headers->from[0]->host;
                                /* if ($UserEmail==$info['email']) {
                                     $mail->setSubject('Re:'.$info['subject']);
                                 }else {
                                     $mail->setSubject($info['subject']);
                                 }*/
                            }

                            if (!empty($allmessages)) {
                                //$mail->setSubject('Re:'.$info['subject']);
                                $mail->setSubject($info['subject']);
                            }else {
                                $mail->setSubject($info['subject']);
                            }
                        }else {
                            $mail->setSubject($info['subject']);
                        }

                        /* echo "<pre>";
                         print_r($CheckSubject);
                       //  print_r($CheckSubject);
                         die;*/

                        /* print_r($headers->ccaddress);
                         print_r($cc_addaresses);
                         print_r(quoted_printable_decode($message));
                         die;*/

                        if($template->id == 10 && !empty($allmessages)) {
                           /* $message = imap_fetchbody($conn,end($allmessages),3.2);
                            if($message <> null){

                            }else {
                                $message = imap_fetchbody($conn, end($allmessages), 2.2);
                                if($message <> null){

                                }else {
                                    $message = imap_fetchbody($conn, end($allmessages), 1);
                                    if($message <> null){

                                    }else {
                                        $message = imap_fetchbody($conn, end($allmessages), 1.2);
                                    }

                                }
                            }*/
                            //$message = imap_fetchbody($conn, end($allmessages), 1.2);
                           /* if($message <> null){

                            }else {
                                $message = imap_fetchbody($conn, end($allmessages), 1.1);
                            }*/
                           // $headers = imap_headerinfo($conn, end($allmessages));
                            $headers = imap_headerinfo($conn, $allmessages[0]);

                            if($headers->cc <> null && !empty($headers->cc) ){


                                foreach ($headers->cc as $key => $Singelemail) {


                                    $cc_addaresses[]=$Singelemail->mailbox.'@'.$Singelemail->host;
                                }

                            }
                            if($headers->to <> null && !empty($headers->to) ){
                                foreach ($headers->to as $keys => $to_email) {
                                    $cc_addaresses[]=$to_email->mailbox.'@'.$to_email->host;
                                }
                            }
                            if(isset($headers->reply_to) && $headers->reply_to <> null && !empty($headers->reply_to)){
                               // $reply_to = $headers->reply_to[0]->mailbox.'@'.$headers->reply_to[0]->host;
                            }



                           // $client_email_body = quoted_printable_decode($message);
                           // $client_email_body = $headers->subject;
                            if($headers->subject <> null) {
                                $client_email_body = $info['subject'];
                            }
                           /* echo "<pre>";
                            print_r($headers->subject);
                            die;*/

                            if($client_email_body <> null){
                                $email_with_respect_to_client = 'With respect to following email:<br><br>';
                                $email_with_respect_to_client .=$client_email_body;
                                $emailBody = str_replace("{WITHRESPECTTO}", $email_with_respect_to_client, $emailBody);
                            }else{
                                $emailBody = str_replace("{WITHRESPECTTO}", '', $emailBody);
                            }
                        }else if(!empty($allmessages)){

                            $headers = imap_headerinfo($conn,  $allmessages[0]);

                            if($headers->cc <> null && !empty($headers->cc) ){


                                foreach ($headers->cc as $key => $Singelemail) {


                                    $cc_addaresses[]=$Singelemail->mailbox.'@'.$Singelemail->host;
                                }

                            }
                            if($headers->to <> null && !empty($headers->to) ){
                                foreach ($headers->to as $keys => $to_email) {
                                    $cc_addaresses[]=$to_email->mailbox.'@'.$to_email->host;
                                }
                            }
                            if(isset($headers->reply_to) && $headers->reply_to <> null && !empty($headers->reply_to)){
                              //  $reply_to = $headers->reply_to[0]->mailbox.'@'.$headers->reply_to[0]->host;
                            }
                            $emailBody = str_replace("{WITHRESPECTTO}", '', $emailBody);

                        }else{
                            $emailBody = str_replace("{WITHRESPECTTO}", '', $emailBody);
                        }



                        /* echo "<pre>";
                         print_r($emailBody);
                         die;*/
                        $mail->setHtmlBody($emailBody);
                        $ccEmails = array_merge($ccEmails,$cc_addaresses);
                        /* echo "<pre>";
                         print_r($ccEmails);
                         die;*/
                        if($info['email'] == 'Santunu.Barua@rakbank.ae'){
                            $ccEmails[]= 'anand.neelakantan@rakbank.ae';
                        }

                        foreach ($ccEmails as $ccEmail){
                            if($ccEmail != "acena@windmillsgroup.com"  && $ccEmail != "inquiry@windmillsgroup.com" && $ccEmail != "akash@wistech.biz" && $ccEmail != "ShaikhaSA@emiratesislamic.ae" && $ccEmail != "NileshB@tanfeeth.ae" && $ccEmail != "MunaMHA@emiratesislamic.ae" && $ccEmail != 'team@windmillsgroup.com') {
                                $ccEmailscomplete[] = $ccEmail;
                                }
                        }




                    }
                    else{
                        $mail->setSubject($template->langDetail->subject);
                    }

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }


                       // $valuers = array('team@windmillsgroup.com','pjayshwal@windmillsgroup.com','rmeshram@windmillsgroup.com','mgkhan@windmillsgroup.com','valuation@windmillsgroup.com','asawad@windmillsgroup.com','mbohra@windmillsgroup.com','aakthar@windmillsgroup.com','ssharma@windmillsgroup.com','dbegg@windmillsgroup.com');

                       /* $ccEmailscompleteUsers[]= 'rthakur@windmillsgroup.com';
                        $ccEmailscompleteUsers[]= 'bilalmoti@windmillsgroup.com';*/
                    }else{

                           //     $ccEmailscompleteUsers = $ccEmailscomplete;
                    }
                    $needle   = 'windmillsgroup.com';
                    $checked = array();
                    foreach ($ccEmailscomplete as $ccEmailc){
                        if (str_contains($ccEmailc, $needle)) {
                            $checked[]=$ccEmailc;
                        }else{
                            $ccEmailscompleteUsers[] = $ccEmailc;
                        }
                    }

                    $ccEmailscompleteUsers[]= 'reports@windmillsgroup.com';
                   // $ccEmailscompleteUsers[]= $info['valuer'];
                    if( isset($info['valuer']) && ($info['valuer'] <> null)){
                        $ccEmailscompleteUsers[]= $info['valuer'];
                    }
                    if( isset($info['inspector']) && ($info['inspector'] <> null)){
                        $ccEmailscompleteUsers[]= $info['inspector'];
                    }
                   /* echo "<pre>";
                    print_r($info);
                    print_r($ccEmailscompleteUsers);
                    die;*/
                    $ccEmailscompleteUsers_final = array();
                    foreach ($ccEmailscompleteUsers as $ccEmail_client){
                        if(trim($ccEmail_client) != "praveen@windowlineuae.com" && trim($ccEmail_client) != "imranhaq71@gmail.com") {
                            $ccEmailscompleteUsers_final[] = $ccEmail_client;
                        }
                    }



                  //  $ccEmailscompleteUsers[]= 'akash@wistech.biz';
                    if($ccEmailscompleteUsers_final!=null)$mail->setCc($ccEmailscompleteUsers_final);
                    if($info['email'] == 'DIBUAEESDExternalEvaluation@dib.ae'){
                        $mail->setTo('DIBUAEESDExternalEvaluation@dib.ae');
                        $mail->setReplyTo('DIBUAEESDExternalEvaluation@dib.ae');
                    }else{
                        $mail->setTo($reply_to);
                        $mail->setReplyTo($reply_to);
                    }


                    /*echo $reply_to;
                    die;*/

                    $mail->setHeader('Sender', 'reports@windmillsgroup.com');
                    //  $mail->setHeader('Message-ID', '8b825002b011b834a1e2bc9abeb7aa81@local.windmills');
                    // $mail->setHeader('In-Reply-To', $info['uid'].'@local.windmills');
                    // $mail->setHeader('Thread-Index', $info['uid'].'@local.windmills');
                    $mail->setHeader('References', $info['uid'].'@local.windmills');

                    $mail->send();
                    return true;
                }
            }
        }
    }
    public function sendEmailtest($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments,$custom_cc_val){


        $emailBody = $template->langDetail->email_body;
        $emailBody = str_replace('{forward_text}', '', $emailBody);
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        if($toInfo!=null){

            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailer->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailer->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    //$mail->setTextBody(strip_tags($emailBody));
                    //$mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['notificationSenderEmail'] => $moduleId->params['nNotificationSenderName']]);


                    /* $info['email'];
                     die;*/
                    // $to[] = 'akash@wistech.biz';
                    $to[] = $info['email'];
                    // $mail->setTo($info['email']);
                    // $mail->setReplyTo($info['email']);
                    $reply_to =$info['email'];
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }

                    }
                    if($info['valuer'] <> null) {
                        $ccEmails[] = $info['valuer'];
                    }
                    $ccEmails = array_merge($ccEmails,$custom_cc_val);


                    /* gmail connection,with port number 993 */
                    $host = '{imap.gmail.com:993/ssl/novalidate-cert}';
                    /* Your gmail credentials */
                    $user = 'reports@windmillsgroup.com';
                    $password = '2023#moti@';
                    /*  $user = 'support@windmillsgroup.com';
                      $password = 'wmsupport2021#';*/

                    /* Establish a IMAP connection */
                    $conn = imap_open($host, $user, $password)
                    or die('unable to connect Gmail: ' . imap_last_error());

                    $allmessages = array();
                    $cc_addaresses=array();

                    if ($info['subject']<>null) {


                        /* Search emails from gmail inbox*/
                        // $CheckSubject = imap_search($conn, 'SUBJECT "'.trim($info['subject']).'"');
                        if (str_contains($info['subject'], '&')) {
                            $sub_array = explode('&',$info['subject']);
                            $CheckSubject = imap_search($conn, 'SUBJECT "'.trim($sub_array[0]).'"');
                        }else if (str_contains($info['subject'], '–')) {

                            $sub_array = explode('-',$info['subject']);
                            $CheckSubject = imap_search($conn, 'SUBJECT "'.trim($sub_array[0]).'"');
                        }else{

                            $CheckSubject = imap_search($conn, 'SUBJECT "'.trim($info['subject']).'"');

                        }

                        if (!empty($CheckSubject)) {


                            foreach ($CheckSubject as $key => $Singelemail) {
                                $headers = imap_header($conn, $Singelemail, 0);

                                if($headers->subject == trim($info['subject'])){
                                    $allmessages[] = $headers->Msgno;
                                }



                                // $UserEmail=$headers->from[0]->mailbox.'@'.$headers->from[0]->host;
                                /* if ($UserEmail==$info['email']) {
                                     $mail->setSubject('Re:'.$info['subject']);
                                 }else {
                                     $mail->setSubject($info['subject']);
                                 }*/
                            }

                            if (!empty($allmessages)) {
                                //$mail->setSubject('Re:'.$info['subject']);
                                $mail->setSubject($info['subject']);
                            }else {
                                $mail->setSubject($info['subject']);
                            }
                        }else {
                            $mail->setSubject($info['subject']);
                        }

                        /* echo "<pre>";
                         print_r($CheckSubject);
                       //  print_r($CheckSubject);
                         die;*/

                        /* print_r($headers->ccaddress);
                         print_r($cc_addaresses);
                         print_r(quoted_printable_decode($message));
                         die;*/

                        if($template->id == 10 && !empty($allmessages)) {
                            /* $message = imap_fetchbody($conn,end($allmessages),3.2);
                             if($message <> null){

                             }else {
                                 $message = imap_fetchbody($conn, end($allmessages), 2.2);
                                 if($message <> null){

                                 }else {
                                     $message = imap_fetchbody($conn, end($allmessages), 1);
                                     if($message <> null){

                                     }else {
                                         $message = imap_fetchbody($conn, end($allmessages), 1.2);
                                     }

                                 }
                             }*/
                            //$message = imap_fetchbody($conn, end($allmessages), 1.2);
                            /* if($message <> null){

                             }else {
                                 $message = imap_fetchbody($conn, end($allmessages), 1.1);
                             }*/
                            // $headers = imap_headerinfo($conn, end($allmessages));
                            $headers = imap_headerinfo($conn, $allmessages[0]);

                            if($headers->cc <> null && !empty($headers->cc) ){


                                foreach ($headers->cc as $key => $Singelemail) {


                                    $cc_addaresses[]=$Singelemail->mailbox.'@'.$Singelemail->host;
                                }

                            }
                            if($headers->to <> null && !empty($headers->to) ){
                                foreach ($headers->to as $keys => $to_email) {
                                    $cc_addaresses[]=$to_email->mailbox.'@'.$to_email->host;
                                }
                            }
                            if(isset($headers->reply_to) && $headers->reply_to <> null && !empty($headers->reply_to)){
                                $reply_to = $headers->reply_to[0]->mailbox.'@'.$headers->reply_to[0]->host;
                            }



                            // $client_email_body = quoted_printable_decode($message);
                            // $client_email_body = $headers->subject;
                            if($headers->subject <> null) {
                                $client_email_body = $info['subject'];
                            }
                            /* echo "<pre>";
                             print_r($headers->subject);
                             die;*/

                            if($client_email_body <> null){
                                $email_with_respect_to_client = 'With respect to following email:<br><br>';
                                $email_with_respect_to_client .=$client_email_body;
                                $emailBody = str_replace("{WITHRESPECTTO}", $email_with_respect_to_client, $emailBody);
                            }else{
                                $emailBody = str_replace("{WITHRESPECTTO}", '', $emailBody);
                            }
                        }else if(!empty($allmessages)){

                            $headers = imap_headerinfo($conn,  $allmessages[0]);

                            if($headers->cc <> null && !empty($headers->cc) ){


                                foreach ($headers->cc as $key => $Singelemail) {


                                    $cc_addaresses[]=$Singelemail->mailbox.'@'.$Singelemail->host;
                                }

                            }
                            if($headers->to <> null && !empty($headers->to) ){
                                foreach ($headers->to as $keys => $to_email) {
                                    $cc_addaresses[]=$to_email->mailbox.'@'.$to_email->host;
                                }
                            }
                            if(isset($headers->reply_to) && $headers->reply_to <> null && !empty($headers->reply_to)){
                                $reply_to = $headers->reply_to[0]->mailbox.'@'.$headers->reply_to[0]->host;
                            }
                            $emailBody = str_replace("{WITHRESPECTTO}", '', $emailBody);

                        }else{
                            $emailBody = str_replace("{WITHRESPECTTO}", '', $emailBody);
                        }



                        /* echo "<pre>";
                         print_r($emailBody);
                         die;*/
                        $mail->setHtmlBody($emailBody);
                        $ccEmails = array_merge($ccEmails,$cc_addaresses);
                        /* echo "<pre>";
                         print_r($ccEmails);
                         die;*/
                        if($info['email'] == 'Santunu.Barua@rakbank.ae'){
                            $ccEmails[]= 'anand.neelakantan@rakbank.ae';
                        }
                        $ccEmailscomplete = array();
                        foreach ($ccEmails as $ccEmail){
                            if($ccEmail != "akash@wistech.biz" && $ccEmail != "ShaikhaSA@emiratesislamic.ae" && $ccEmail != "NileshB@tanfeeth.ae" && $ccEmail != "MunaMHA@emiratesislamic.ae") {
                                $ccEmailscomplete[] = $ccEmail;
                            }
                        }

                        if($ccEmailscomplete!=null)$mail->setCc($ccEmailscomplete);


                    }
                    else{
                        $mail->setSubject($template->langDetail->subject);
                    }

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }

                    if($info['email'] == 'DIBUAEESDExternalEvaluation@dib.ae'){
                        $mail->setTo('DIBUAEESDExternalEvaluation@dib.ae');
                        $mail->setReplyTo('DIBUAEESDExternalEvaluation@dib.ae');
                    }else{
                        $mail->setTo($reply_to);
                        $mail->setReplyTo($reply_to);
                    }


                    /*echo $reply_to;
                    die;*/

                    $mail->setHeader('Sender', 'reports@windmillsgroup.com');
                    //  $mail->setHeader('Message-ID', '8b825002b011b834a1e2bc9abeb7aa81@local.windmills');
                    // $mail->setHeader('In-Reply-To', $info['uid'].'@local.windmills');
                    // $mail->setHeader('Thread-Index', $info['uid'].'@local.windmills');
                    $mail->setHeader('References', $info['uid'].'@local.windmills');

                    $mail->send();
                    return true;
                }
            }
        }
    }
    /*
    * Send Emsil
    */
    public function sendEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments,$custom_cc_status){

        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailer->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailer->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['notificationSenderEmail'] => $moduleId->params['nNotificationSenderName']]);
                    $mail->setTo($info['email']);
                  /*  $mail->setTo('akash@wistech.biz');
                    $ccEmails[]='szakir@windmillsgroup.com';*/
                    $ccEmails = array();
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }


                    }
                    $ccEmails = array_merge($ccEmails,$custom_cc_status);
                    $ccEmailscomplete = array();
                    foreach ($ccEmails as $ccEmail){
                        if($ccEmail != "akash@wistech.biz" && $ccEmail != "ShaikhaSA@emiratesislamic.ae" && $ccEmail != "NileshB@tanfeeth.ae" && $ccEmail != "MunaMHA@emiratesislamic.ae") {
                            $ccEmailscomplete[] = $ccEmail;
                        }
                    }

                    if($ccEmailscomplete!=null)$mail->setCc($ccEmailscomplete);

                   // if($ccEmails!=null)$mail->setCc($ccEmails);
                    // if($ccEmails!=null)$mail->setCc($ccEmails);
                    /* gmail connection,with port number 993 */
                    $host = '{imap.gmail.com:993/ssl/novalidate-cert}';
                    /* Your gmail credentials */
                    $user = 'reports@windmillsgroup.com';
                    $password = '2023#moti@';

                    /* Establish a IMAP connection */
                    $conn = imap_open($host, $user, $password)
                    or die('unable to connect Gmail: ' . imap_last_error());



                    if ($info['subject']<>null) {
                        /* Search emails from gmail inbox*/
                        $CheckSubject = imap_search($conn, 'SUBJECT "'.$info['subject'].'"');
                        if (!empty($CheckSubject)) {

                            foreach ($CheckSubject as $key => $Singelemail) {
                                $headers = imap_header($conn, $Singelemail, 0);
                                $UserEmail=$headers->from[0]->mailbox.'@'.$headers->from[0]->host;
                                if ($UserEmail==$info['email']) {
                                    $mail->setSubject('Re:'.$info['subject']);
                                    // print_r($UserEmail);
                                    // die();
                                }else {
                                    $mail->setSubject($info['subject']);
                                }
                            }
                        }else {
                            $mail->setSubject($info['subject']);
                        }
                    }
                    else{
                        $mail->setSubject($template->langDetail->subject);
                    }

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }
                   
                    $mail->send();
                }
            }
        }
    }

    public function sendEmailMonthly($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments,$custom_cc_status){

        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailer->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailer->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['notificationSenderEmail'] => $moduleId->params['nNotificationSenderName']]);
                    $mail->setTo($info['email']);
                    /*  $mail->setTo('akash@wistech.biz');
                      $ccEmails[]='szakir@windmillsgroup.com';*/
                    $ccEmails = array();
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }


                    }
                    $ccEmails = array_merge($ccEmails,$custom_cc_status);
                    if($ccEmails!=null)$mail->setCc($ccEmails);
                    $currentMonth = date('F');
                    $month= Date('F', strtotime($currentMonth . " last month"));
                    $year= date('Y') -1;

                    $subject = 'Invoice for the Month of '.$month.' '.$year;

                    $mail->setSubject($subject);
                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }

                    $mail->send();
                }
            }
        }
    }

    public function sendEmailMonthlyAllClients($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments,$custom_cc_status){

        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);



                    Yii::$app->mailer->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailer->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['notificationSenderEmail'] => $moduleId->params['nNotificationSenderName']]);
                    $mail->setTo($info['email']);
                    /*  $mail->setTo('akash@wistech.biz');
                      $ccEmails[]='szakir@windmillsgroup.com';*/
                    $ccEmails = array();
                    $ccEmails[]='bilalmoti@windmillsgroup.com';
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }


                    }
                    $ccEmails = array_merge($ccEmails,$custom_cc_status);
                    if($ccEmails!=null)$mail->setCc($ccEmails);

                    $last_month = date('F', strtotime('last month'));
                    $date_value = $last_month.' '. date('Y');
                    $subject = 'Thank You For Your Trusting Us With your Valuation Requirements';


                    $mail->setSubject($subject);
                    if($attachments!=null){
                        foreach($attachments as $attachment){
                         //   $mail->attach('D:\xampp\htdocs\windmills\uploads\thank_you.png');
                        }
                    }
                  //  $mail->attach('/home/u468188240/domains/maxima.windmillsgroup.com/public_html/uploads/thank_you.png');

                    $mail->send();
                }
            }
        }
    }
    public function sendEmail2($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments){
       
        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        if($toInfo!=null){


            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
                    $mail->setTo($info['email']);
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }
                        $ccEmails[]= 'support@windmillsgroup.com';
                        $ccEmailscomplete = array();
                        foreach ($ccEmails as $ccEmail){
                            if($ccEmail != "akash@wistech.biz" && $ccEmail != "ShaikhaSA@emiratesislamic.ae" && $ccEmail != "NileshB@tanfeeth.ae" && $ccEmail != "MunaMHA@emiratesislamic.ae") {
                                $ccEmailscomplete[] = $ccEmail;
                            }
                        }
                        if($ccEmails!=null)$mail->setCc($ccEmailscomplete);

                    }

                    $mail->setSubject($template->langDetail->subject);
                  /*  echo "<pre>";
                    print_r($toInfo);
                    echo $toInfo[0]['subject'];
                    die;*/
                  //  $mail->setSubject($toInfo[0]['subject']);

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }


                      $mail->send();


                }
            }
        }
    }
    public function sendEmail23($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments,$custom_cc_val){

        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        if($toInfo!=null){


            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {

                    if($toInfo[0]['subject'] <> null) {
                        $client_email_body = $toInfo[0]['subject'];
                    }
                    /* echo "<pre>";
                     print_r($headers->subject);
                     die;*/

                    if($client_email_body <> null){
                        $email_with_respect_to_client = 'With respect to following email subject:<br>';
                        $email_with_respect_to_client .=$client_email_body;
                        $email_with_respect_to_client .= '<br>';
                        $emailBody = str_replace("{WITHRESPECTTO}", $email_with_respect_to_client, $emailBody);
                    }else{
                        $emailBody = str_replace("{WITHRESPECTTO}", '', $emailBody);
                    }

                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
                    $mail->setTo($info['email']);
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }
                        //  $ccEmails[]= 'support@windmillsgroup.com';
                        $ccEmailscomplete = array();
                        
                        $ccEmails = array_merge($ccEmails,$custom_cc_val);

                        foreach ($ccEmails as $ccEmail){
                            if($ccEmail != "akash@wistech.biz" && $ccEmail != "ShaikhaSA@emiratesislamic.ae" && $ccEmail != "NileshB@tanfeeth.ae" && $ccEmail != "MunaMHA@emiratesislamic.ae") {
                                $ccEmailscomplete[] = $ccEmail;
                            }
                        }
                        if($ccEmails!=null)$mail->setCc($ccEmailscomplete);

                    }

                    // $mail->setSubject($template->langDetail->subject);
                    /*  echo "<pre>";
                      print_r($toInfo);
                      echo $toInfo[0]['subject'];
                      die;*/
                    $mail->setSubject($toInfo[0]['subject']);

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }
                    $mail->setHeader('Sender', 'support@windmillsgroup.com');
                    //  $mail->setHeader('Message-ID', '8b825002b011b834a1e2bc9abeb7aa81@local.windmills');
                    // $mail->setHeader('In-Reply-To', $info['uid'].'@local.windmills');
                    // $mail->setHeader('Thread-Index', $info['uid'].'@local.windmills');
                    $mail->setHeader('References', $info['uid'].'@local.windmills');
                    $mail->send();


                }
            }
        }
    }
    public function sendEmailToGovtOld($moduleId, $template, $replacements, $ccUsers, $attachments, $g_subject,$client)
    {

        Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
        $mail = Yii::$app->mailerb->compose();
        $cc = '';

        if($attachments!=null){
            foreach($attachments as $attachment){
                $mail->attach($attachment);
            }
        }

        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
        $mail->setTo('valuation@ajmanre.gov.ae');
        $mail->setSubject($g_subject);
        $mail->setCc(['reports@windmillsgroup.com']);


        $date = date("D, M j, Y");
        $time = date("h:i A");

        $html = '';
        $html .= '---------- Forwarded message ---------';
        $html .= '<br>From: '.$moduleId->params['sNotificationSenderName'].' <'.Html::tag('a', Html::encode($moduleId->params['snotificationSenderEmail']), ['href'=>''.$moduleId->params['snotificationSenderEmail'].'', 'target'=>'_blank']).'>';
        $html .= '<br>Date: '.$date.' at '.$time.'';
        $html .= '<br>Subject: '.$g_subject.'';
        $html .= '<br>To: <'.Html::tag('a', Html::encode($client->primaryContact->email), ['href'=>''.$client->primaryContact->email.'', 'target'=>'_blank']).'>';
        $html .= '<br>Cc: '.$cc.'<br><br><br>';

        $emailBody = str_replace("{forward_text}",$html,$emailBody);

        $mail->setTextBody(strip_tags($emailBody));
        $mail->setHtmlBody($emailBody);
        $mail->send();
    }
    public function sendEmailToGovt($moduleId, $template, $replacements, $ccUsers, $attachments, $g_subject,$client)
    {

        Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
        $mail = Yii::$app->mailerb->compose();
        // compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);


        $cc = '';
        if($ccUsers!=null){
            foreach($ccUsers as $ccUser){
                if ($ccUser->email !='') {
                    $ccEmails[]=$ccUser->email;

                    $cc .= '<'.Html::tag('a', Html::encode($ccUser->email), ['href'=>''.$ccUser->email.'', 'target'=>'_blank']).'>, ';

                }
            }
            //$ccEmails[]= 'support@windmillsgroup.com';
            $cc .= '<'.Html::tag('a', Html::encode('support@windmillsgroup.com'), ['href'=>'support@windmillsgroup.com', 'target'=>'_blank']).'>';
            // if($ccEmails!=null)$mail->setCc($ccEmails);
        }

        if($attachments!=null){
         /*   echo "<pre>";
            print_r($attachments);
            die('ajaman');*/

            foreach($attachments as $attachment){
                $mail->attach($attachment);
            }
        }
        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
        $mail->setTo('valuation@ajmanre.gov.ae');
        $mail->setSubject($g_subject);
        $mail->setCc(['reports@windmillsgroup.com']);


        $mail->setTextBody(strip_tags($emailBody));
        $mail->setHtmlBody($emailBody);
        $mail->setHeader('Sender', 'reports@windmillsgroup.com');
        //  $mail->setHeader('Message-ID', '8b825002b011b834a1e2bc9abeb7aa81@local.windmills');
        // $mail->setHeader('In-Reply-To', $info['uid'].'@local.windmills');
        // $mail->setHeader('Thread-Index', $info['uid'].'@local.windmills');
        $mail->setHeader('References', $g_subject);
        $mail->send();
    }
    public function sendEmailToApprovers($moduleId, $template, $replacements, $ccUsers, $attachments, $g_subject,$client)
    {

        Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
        $mail = Yii::$app->mailerb->compose();
        // compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);


       /* if($ccUsers!=null){
            foreach($ccUsers as $ccUser){
                if ($ccUser->email !='') {
                    $ccEmails[]=$ccUser->email;
                }
            }
        }*/

        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
       /* if($ccUsers['valuer'] <> null){
            $mail->setTo($ccUsers['valuer']);
        }else{
            $mail->setTo('');
        }*/

        $mail->setTo('support@windmillsgroup.com');
        $mail->setSubject($g_subject);

        if($ccUsers['valuer'] <> null){
            $mail->setCc($ccUsers['valuer']);
        }
        if($ccUsers['inspector'] <> null){
            $mail->setCc([$ccUsers['inspector']]);
        }else{
            $mail->setCc(['reports@windmillsgroup.com']);
           // $mail->setCc(['reports@windmillsgroup.com']);
        }

        $emailBody = str_replace("{forward_text}",'',$emailBody);

        $mail->setTextBody(strip_tags($emailBody));
        $mail->setHtmlBody($emailBody);
        $mail->send();
    }
    public function sendEmailToSupport($moduleId, $template, $replacements, $ccUsers, $attachments, $g_subject,$client)
    {

        Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
        $mail = Yii::$app->mailerb->compose();
        // compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);


        $cc = '';
        if($ccUsers!=null){
            foreach($ccUsers as $ccUser){
                if ($ccUser->email !='') {
                    $ccEmails[]=$ccUser->email;
                }
            }
        }

        if($attachments!=null){
            /*   echo "<pre>";
               print_r($attachments);
               die('ajaman');*/

            foreach($attachments as $attachment){
                $mail->attach($attachment);
            }
        }
        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
        $mail->setTo('support@windmillsgroup.com');
        $mail->setSubject($g_subject);
        $mail->setCc(['finance@windmillsgroup.com']);


        $date = date("D, M j, Y");
        $time = date("h:i A");

        $html = '';

        $emailBody = str_replace("{forward_text}",$html,$emailBody);

        $mail->setTextBody(strip_tags($emailBody));
        $mail->setHtmlBody($emailBody);
        $mail->send();
    }
    public function sendEmailToGovt_old($moduleId, $template, $replacements,$toInfo, $ccUsers, $attachments)
    {

        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }
        if($toInfo!=null) {


            foreach ($toInfo as $key => $info) {
                $emailBody = str_replace("{name}", $info['name'], $emailBody);
                $emailBody = str_replace("{clientName}", $info['name'], $emailBody);
            }
        }

        Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
        $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
        $mail->setTextBody(strip_tags($emailBody));
        $mail->setHtmlBody($emailBody);
        $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
       // $mail->setTo('valuation@ajmanre.gov.ae');
        $mail->setTo('zaheer96syed@gmail.com');
        if($ccUsers!=null){
            foreach($ccUsers as $ccUser){
                if ($ccUser->email !='') {
                    $ccEmails[]=$ccUser->email;
                }
            }
            $ccEmails1[]= 'akash@wistech.biz';
            if($ccEmails!=null)$mail->setCc($ccEmails1);

        }

        $mail->setSubject($template->langDetail->subject);

        if($attachments!=null){
            foreach($attachments as $attachment){
                $mail->attach($attachment);
            }
        }

      //  $mail->send();
    }
    /*
    * Send Sms
    */
    public function sendSms($moduleId,$template,$replacements,$toInfo){
        $smsBody = $template->langDetail->sms_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $smsBody = str_replace($rKey,$rVal,$smsBody);
            }
        }
        if($toInfo!=null){
            foreach($toInfo as $key=>$info){
                if($info['mobile']!=''){
                    $smsBody = str_replace("{name}",$toInfo['name'],$smsBody);
                }
            }
        }
    }

    /*
    * Send Push Notification
    */
    public function sendPush($moduleId,$template,$replacements,$toInfo){
        $pushBody = $template->langDetail->push_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $pushBody = str_replace($rKey,$rVal,$pushBody);
            }
        }
        $pushBody = str_replace("{name}",$toInfo['name'],$pushBody);
    }
    
    
    
    
    //function calls when save meeting form (send email to client and all attendess (from client side and windmills side))
    public static function fireMeeting2($eventId,$data)
    {
        
        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client
                        //              die();
                        $toUser = $data['client'];

                        $toInfo[] = [
                            'name'=>$toUser->title,
                            'email'=>$toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                            // 'email'=>'akash@wistech.biz',
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                            'attendeeIds' => (isset($data['attendeeIds'])&&$data['attendeeIds']<>null)?$data['attendeeIds']:'',
                        ];


                    }
                    elseif($userType==8 && $data['reviewer']!=null){
                        $toUser = $data['reviewer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname,
                            'email'=>$toUser->email,
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                        ];
                    }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();
                    // dd($ccUsers);


                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){



                                (new NotifyEvent)->sendEmailMeeting2($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);


                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }
    
    public function sendEmailMeeting2($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments)
    {
        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }
        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
                    $mail->setTo($info['email']);
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }
                        // $ccEmails[]= 'support@windmillsgroup.com';
                        
                        if(isset($info['attendeeIds']) && $info['attendeeIds']<>null){
                            $attendeeUsersCC = yii\helpers\ArrayHelper::map(User::find()->where(['in', 'id', $info['attendeeIds']])
                            // ->andWhere(['status'=>1])
                            ->asArray()->all(), 'email','email');
                            if($attendeeUsersCC<>null){
                                $mergedCC = array_merge($ccEmails, array_values($attendeeUsersCC));
                                $ccEmails = array_unique($mergedCC);
                            }
                        }
                        $cc= array();
                        if($ccEmails!=null && !empty($ccEmails)){
                            foreach ($ccEmails as $key => $email){
                                if($email !=  $info['email'] ){
                                    $cc[]= $email;
                                }

                            }
                        }

                        if($ccEmails!=null)$mail->setCc($cc);

                    }


                    $mail->setSubject($template->langDetail->subject);

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }


                      $mail->send();


                }
            }
        }
    }
    
    
    
    
    //function calls when inspection is today (manuall call not automatic)
    public static function fireScheduleReminder($eventId,$data)
    {
        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');
    
        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);

        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];


                    if($data['valuer']!=null){
                        $toUser = $data['valuer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname.' '.$toUser->lastname,
                            'email'=>$toUser->email !== '' ? $toUser->email : '',
                            // 'email'=>'akash@wistech.biz',
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                            'inspector' => (isset($data['inspector'])&&$data['inspector']<>null)?$data['inspector']:'',
                        ];


                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();
                    // dd($ccUsers);


                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){
                                (new NotifyEvent)->sendScheduleReminderEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);
                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }

    public function sendScheduleReminderEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments)
    {
        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }
        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
                    $mail->setTo($info['email']);
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }
                        // $ccEmails[]= 'support@windmillsgroup.com';
                        
                        if(isset($info['inspector']) && $info['inspector']<>null){
                            $inspector = yii\helpers\ArrayHelper::map(User::find()->where(['in', 'id', $info['inspector']])
                            // ->andWhere(['status'=>1])
                            ->asArray()->all(), 'email','email');
                            if($inspector<>null){
                                $mergedCC = array_merge($ccEmails, array_values($inspector));
                                $ccEmails = array_unique($mergedCC);
                            }
                        }   
                        if($ccEmails!=null)$mail->setCc($ccEmails);

                    }                 

                    $mail->setSubject($template->langDetail->subject);

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }
                      $mail->send();
                }
            }
        }
    }
    
    
    
    
    
    

    //function calls before 3 hours to client meeting
    public static function fireReminderEmail($eventId, $data)
    {        
        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    $toInfo=[];

                    if($data['scheduler']!=null){
                        $toUser = $data['scheduler'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname.' '.$toUser->lastname,
                            'email'=>$toUser->email !== '' ? $toUser->email : '',
                            // 'email'=>'akash@wistech.biz',
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                            'attendeeIds' => (isset($data['attendeeIds'])&&$data['attendeeIds']<>null)?$data['attendeeIds']:'',
                        ];
                        
                    }

                    
                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();
                
                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){
                                (new NotifyEvent)->sendReminderMeetingEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);
                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }

    }

    public function sendReminderMeetingEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments)
    {
        // dd($toInfo);
        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }
        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
                    
                    $mail->setTo($info['email']);
                    // $mail->setTo("admin@windmills.com");
                    // dd($mail);
                    
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }
                        // $ccEmails[]= 'support@windmillsgroup.com';
                        
                        if(isset($info['attendeeIds']) && $info['attendeeIds']<>null){
                            $attendeeUsersCC = yii\helpers\ArrayHelper::map(User::find()->where(['in', 'id', $info['attendeeIds']])
                            // ->andWhere(['status'=>1])
                            ->asArray()->all(), 'email','email');
                            if($attendeeUsersCC<>null){
                                $mergedCC = array_merge($ccEmails, array_values($attendeeUsersCC));
                                $ccEmails = array_unique($mergedCC);
                            }
                        }   

                        // dd($ccEmails);
    
                        if($ccEmails!=null)$mail->setCc($ccEmails);

                    }                 

                    $mail->setSubject($template->langDetail->subject);

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }

                    // dd($mail);


                      $mail->send();


                }
            }
        }
    }
    
    
    









    public static function fireErrorEmail($eventId,$data)
    {
        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);

        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                        if(isset($data['approver']) && $data['approver']<>null){
                            $toUsers = User::find()
                            ->where(['in', 'id', $data['approver']])
                            ->andWhere(['status'=>1])->all();

                            if($toUsers!=null){
                                foreach($toUsers as $toUser){
                                    $toInfo[]=[
                                        'name'=>$toUser->name,
                                        'email'=>$toUser->email,
                                        'mobile'=>'',
                                        'deviceIdz' => null,
                                        'subject' => null,
                                    ];
                                }
                            }
                        }

                    $ccUsers = User::find()->where(['in', 'id', $data['cc_user_ids']])->andWhere(['status'=>1])->all();

                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){
                                (new NotifyEvent)->sendErrorEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);
                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }

    public function sendErrorEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments)
    {
        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }
        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
                    $mail->setTo($info['email']);
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }
                        // $ccEmails[]= 'support@windmillsgroup.com';  
                        if($ccEmails!=null)$mail->setCc($ccEmails);
                    }                 

                    $mail->setSubject($template->langDetail->subject);
                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }
                      $mail->send();
                }
            }
        }
    }

    public static function fireQuotationFeeDifference($eventId, $data)
    {



        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    $toInfo=[];

                    if($data['client']!=null){
                        $toUser = $data['client'];
                        $toInfo[] = [
                            'name'=> 'Maxima',
                            // 'email'=>$toUser->email !== '' ? $toUser->email : '',
                            'email'=>'maxima@windmillsgroup.com',
                            'mobile'=>'',
                            'subject' => $data['subject'],
                        ];
                    }


                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();

                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){
                                (new NotifyEvent)->sendQuotationFeeDifference($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);
                            }
                            // //Sms
                            // if($ntVal=='sms'){
                            //     (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            // }
                            // //Push
                            // if($ntVal=='push'){
                            //     (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            // }
                        }
                    }
                    // }
                }
            }
        }

    }

    public function sendQuotationFeeDifference($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments)
    {
        // dd($toInfo);

        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }
        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {

                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);

                    $mail->setTo($info['email']);
                    // $mail->setTo("admin@windmills.com");
                    // dd($mail);

                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }

                        if($ccEmails!=null)$mail->setCc($ccEmails);

                    }

                    $mail->setSubject($template->langDetail->subject);

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }

                    // dd($mail);


                    $mail->send();


                }
            }
        }
    }

    public static function fireValuationFeeDifference($eventId, $data)
    {



        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    $toInfo=[];

                    if($data['client']!=null){
                        $toUser = $data['client'];
                        $toInfo[] = [
                            'name'=> 'Maxima',
                            // 'email'=>$toUser->email !== '' ? $toUser->email : '',
                            'email'=>'maxima@windmillsgroup.com',
                            'mobile'=>'',
                            'subject' => $data['subject'],
                        ];
                    }


                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();

                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){
                                (new NotifyEvent)->sendValuationFeeDifference($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);
                            }
                            // //Sms
                            // if($ntVal=='sms'){
                            //     (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            // }
                            // //Push
                            // if($ntVal=='push'){
                            //     (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            // }
                        }
                    }
                    // }
                }
            }
        }

    }

    public function sendValuationFeeDifference($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments)
    {
        // dd($toInfo);

        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }
        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {

                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);

                    $mail->setTo($info['email']);
                    // $mail->setTo("admin@windmills.com");
                    // dd($mail);

                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                //$ccEmails[]=$ccUser->email;
                            }
                        }

                        if($ccEmails!=null)$mail->setCc($ccEmails);

                    }
                    $ccEmails[]= 'akash@wistech.biz';

                    $mail->setSubject($template->langDetail->subject);

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }

                    // dd($mail);


                    $mail->send();


                }
            }
        }
    }
    public static function fireAnnualReminderEmail($eventId, $data)
    {
        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;

        // dd($data);

        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);


                if($userType!=null && $userType!=''){
                    $toInfo=[];

                    $toUser = $data['client'];

                    $toInfo[] = [
                        'name'=>$toUser->title,
                        'email'=>$toUser->primaryContact->email !== null ? $toUser->primaryContact->email : '',
                        // 'email'=>'akash@wistech.biz',
                        'mobile'=>'',
                        'deviceIdz' => null,
                        'subject' => $data['subject'],
                    ];



                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();

                    // dd($toInfo, $ccUsers);

                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){
                                (new NotifyEvent)->sendAnnualReminderEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);
                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }

    }

    public function sendAnnualReminderEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments)
    {
        // dd($toInfo);
        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }
        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {

                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);

                    $mail->setTo($info['email']);
                    // $mail->setTo("admin@windmills.com");
                    // dd($mail);

                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }
                        // $ccEmails[]= 'support@windmillsgroup.com';

                        if(isset($info['attendeeIds']) && $info['attendeeIds']<>null){
                            $attendeeUsersCC = yii\helpers\ArrayHelper::map(User::find()->where(['in', 'id', $info['attendeeIds']])
                                // ->andWhere(['status'=>1])
                                ->asArray()->all(), 'email','email');
                            if($attendeeUsersCC<>null){
                                $mergedCC = array_merge($ccEmails, array_values($attendeeUsersCC));
                                $ccEmails = array_unique($mergedCC);
                            }
                        }

                        // dd($ccEmails);

                        if($ccEmails!=null)$mail->setCc($ccEmails);

                    }

                    $mail->setSubject($template->langDetail->subject);

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }

                    // dd($mail);


                    $mail->send();


                }
            }
        }
    }

    public static function fireQuotationFollowupEmail($eventId, $data)
    {
        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;

        // dd($data);

        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);


                if($userType!=null && $userType!=''){
                    $toInfo=[];

                    $toUser = $data['client'];

                    $toInfo[] = [
                        'name'=>$toUser->title,
                        'email'=>$toUser->primaryContact->email !== null ? $toUser->primaryContact->email : '',
                        // 'email'=>'akash@wistech.biz',
                        'mobile'=>'',
                        'deviceIdz' => null,
                        'subject' => $data['subject'],
                        'reference_number' => $data['reference_number'],
                    ];



                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();

                    // dd($toInfo, $ccUsers);

                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){
                                (new NotifyEvent)->sendQuotationFollowupEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);
                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }

    }

    public function sendQuotationFollowupEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments)
    {
        // dd($toInfo);
        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }
        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {

                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);

                    $mail->setTo($info['email']);
                    // $mail->setTo("admin@windmills.com");
                    // dd($mail);

                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }
                        // $ccEmails[]= 'support@windmillsgroup.com';

                        if(isset($info['attendeeIds']) && $info['attendeeIds']<>null){
                            $attendeeUsersCC = yii\helpers\ArrayHelper::map(User::find()->where(['in', 'id', $info['attendeeIds']])
                                // ->andWhere(['status'=>1])
                                ->asArray()->all(), 'email','email');
                            if($attendeeUsersCC<>null){
                                $mergedCC = array_merge($ccEmails, array_values($attendeeUsersCC));
                                $ccEmails = array_unique($mergedCC);
                            }
                        }

                        // dd($ccEmails);

                        if($ccEmails!=null)$mail->setCc($ccEmails);

                    }

                    $emailSubject = $template->langDetail->subject .': '. $info['reference_number'];
                    // $mail->setSubject($template->langDetail->subject);
                    $mail->setSubject($emailSubject);

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }

                    // dd($mail);


                    $mail->send();


                }
            }
        }
    }

    public static function fireFeedback($eventId,$data)
    {

        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);


                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client
                        // print_r($data);
                        //              die();
                        $toUser = $data['client'];
                        $valuation = $data['valuation'];

                        if($toUser->client_type == "bank"){
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => $toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                                'uid' => $data['uid'],
                                'valuer' => $data['valuer'],
                                'inspector' => $data['inspector'],
                            ];
                        }else{
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => $toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                                'uid' => $data['uid'],
                                'valuer' => $data['valuer'],
                                'inspector' => $data['inspector'],
                            ];
                        }

                        // if($toUser->id == 1){
                        //     $toInfo[] = [
                        //         'name' => $toUser->title,
                        //         'email' => 'DIBUAEESDExternalEvaluation@dib.ae',
                        //         // 'email'=>'akash@wistech.biz',
                        //         'mobile' => '',
                        //         'deviceIdz' => null,
                        //         'subject' => $data['subject'],
                        //         'uid' => $data['uid'],
                        //         'valuer' => $data['valuer'],
                        //         'inspector' => $data['inspector'],
                        //     ];
                        // }else if($toUser->id == 54514){
                        //     $toInfo[] = [
                        //         'name' => $toUser->title,
                        //         'email' => 'TanfeethICSHFevaluations@tanfeeth.ae',
                        //         // 'email'=>'akash@wistech.biz',
                        //         'mobile' => '',
                        //         'deviceIdz' => null,
                        //         'subject' => $data['subject'],
                        //         'valuer' => $data['valuer'],
                        //         'inspector' => $data['inspector'],
                        //     ];
                        // }else {


                        //     $toInfo[] = [
                        //         'name' => $toUser->title,
                        //         'email' => $toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                        //         // 'email'=>'akash@wistech.biz',
                        //         'mobile' => '',
                        //         'deviceIdz' => null,
                        //         'subject' => $data['subject'],
                        //         'uid' => $data['uid'],
                        //         'valuer' => $data['valuer'],
                        //         'inspector' => $data['inspector'],
                        //     ];
                        // }


                    }
                    // elseif($userType==8 && $data['reviewer']!=null){
                    //     $toUser = $data['reviewer'];
                    //     $toInfo[] = [
                    //         'name'=>$toUser->firstname,
                    //         'email'=>$toUser->email,
                    //         'mobile'=>'',
                    //         'deviceIdz' => null,
                    //         'subject' => $data['subject'],
                    //         'uid' => $data['uid'],
                    //         'valuer' => $data['valuer'],
                    //         'inspector' => $data['inspector'],
                    //     ];
                    // }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                    'uid' => $data['uid'],
                                    'valuer' => $data['valuer'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();

                    //custom cc users
                    $custom_cc_val= array();
                    $custom_cc_status= array();
                    if(isset($data['client']) && $data['client']->id == 230){

                        if ($userType == 0 && $data['client'] != null) {
                            $mainClientVals = $data['client']->customEmailsVals;
                            if (!empty($mainClientVals)) {
                                if(isset($data['attachments']) && $data['attachments'] <> null) {
                                    foreach ($mainClientVals as $mclientv) {
                                        $custom_cc_val[] = $mclientv->email;
                                    }
                                }
                            }
                        }
                    }else {
                        if ($userType == 0 && $data['client'] != null) {
                            $mainClientVals = $data['client']->customEmailsVals;
                            if (!empty($mainClientVals)) {
                                foreach ($mainClientVals as $mclientv) {
                                    $custom_cc_val[] = $mclientv->email;
                                }
                            }
                            /* $mainClientStatuses  = $data['client']->customEmails;
                             if(!empty($mainClientStatuses)){
                                 foreach ($mainClientStatuses as $mclients){
                                     $custom_cc_status[] = $mclients->email;
                                 }
                             }*/
                            /*  echo "<pre>";
                              print_r($custom_cc_val);
                              print_r($custom_cc_status);
                              // print_r($mainClient->customEmailsVals);
                              die;*/

                        }
                    }
                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){


                                // if(trim($data['subject']) == "Evaluation request for : EV-123456-4 .") {
                                (new NotifyEvent)->sendFeedbackEmail($moduleId, $template, $replacements, $toInfo, $ccUsers, $attachments,$custom_cc_val);

                                //}
                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }

    public function sendFeedbackEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments,$custom_cc_val){


        $emailBody = $template->langDetail->email_body;
        $emailBody = str_replace('{forward_text}', '', $emailBody);
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        if($toInfo!=null){

            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {

                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);

                    $mail->setTo($info['email']);
                    // $mail->setTo("admin@windmills.com");
                    // dd($mail);

                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }
                        // $ccEmails[]= 'support@windmillsgroup.com';

                        if(isset($info['attendeeIds']) && $info['attendeeIds']<>null){
                            $attendeeUsersCC = yii\helpers\ArrayHelper::map(User::find()->where(['in', 'id', $info['attendeeIds']])
                                // ->andWhere(['status'=>1])
                                ->asArray()->all(), 'email','email');
                            if($attendeeUsersCC<>null){
                                $mergedCC = array_merge($ccEmails, array_values($attendeeUsersCC));
                                $ccEmails = array_unique($mergedCC);
                            }
                        }

                        // dd($ccEmails);

                        if($ccEmails!=null)$mail->setCc($ccEmails);

                    }

                    // $ccEmails = array_merge($ccEmails,$custom_cc_val);

                    $mail->setSubject($template->langDetail->subject);

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }

                    // dd($mail);


                    $mail->send();


                }
            }
        }
    }



}
