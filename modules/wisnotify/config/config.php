<?php
return [
    'params' => [
        'enable_sms' => true,
        'enable_apppush' => true,
        'notificationSenderEmail' => 'support@windmillsgroup.com',
        'snotificationSenderEmail' => 'services@windmillsgroup.com',
        'nNotificationSenderName' => 'Windmills Client Support Team',
        'sNotificationSenderName' => 'Windmills Services Team',
    ],
];
