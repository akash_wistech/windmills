<?php
namespace app\modules\wisnotify\helpers;

use  app\modules\wisnotify\models\Setting;
use  app\modules\wisnotify\models\NotificationTemplate;
use  app\modules\wisnotify\models\NotificationTemplateDetail;
use app\models\AdminGroup;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
* This is a helper class for notification lists
*/
class NotificationListHelper
{
  /**
  * return all languages
  */
  public static function getSystemLanguages()
  {
    return [
      'en'=>'English',
      // 'ar'=>'Arabic',
    ];
  }

  /**
  * return all rtl languages
  */
  public static function getSystemRtlLanguages()
  {
    return [
      'ar',
    ];
  }

  /**
  * return all user types
  */
  public static function getSystemUserTypes()
  {
    $arr = [];
    $arr[0]='Client';
    $roles = (new NotificationListHelper)->getStaffRolesList();
    if($roles!=null){
      foreach($roles as $role){
        $arr[$role['id']]=$role['title'];
      }
    }
    return $arr;
  }

  /**
  * return events
  */
  public static function getSystemNotificationEventsArr()
  {
    return [
      'Evaluation'=>[
        // 'evaluation.created'=>['shortLabel'=>'Submitted','fullLabel'=>'Evaluation Submitted'],
        // 'evaluation.approved'=>['shortLabel'=>'Approved','fullLabel'=>'Evaluation Approved'],
        // 'evaluation.rejected'=>['shortLabel'=>'Rejected','fullLabel'=>'Evaluation Rejected'],
      ],
      'Quotation'=>[
        'quotation.received'=>['shortLabel'=>'Quotation Received','fullLabel'=>'Quotation Received'],
        'quotation.docs'=>['shortLabel'=>'Quotation Docs','fullLabel'=>'Quotation Docs'],
        'quotation.docsfe'=>['shortLabel'=>'Quotation Docs Follow-up','fullLabel'=>'Quotation Docs Follow-up'],
        'quotation.approved'=>['shortLabel'=>'Quotation Approved','fullLabel'=>'Quotation Approved'],
        'quotation.onholdfe'=>['shortLabel'=>'Quotation On Hold Follow-up','fullLabel'=>'Quotation On Hold Follow-up'],
        'quotation.rejected'=>['shortLabel'=>'Quotation Rejected','fullLabel'=>'Quotation Rejected'],
        'toe.rejected'=>['shortLabel'=>'TOE Rejected','fullLabel'=>'TOE Rejected'],
        'quotation.send'=>['shortLabel'=>'Quotation Send','fullLabel'=>'Quotation Send'],
        'toe.send'=>['shortLabel'=>'Toe Send','fullLabel'=>'Toe Send'],
        'payment.received'=>['shortLabel'=>'Payment Received','fullLabel'=>'Payment Received'],
        'toe.signed'=>['shortLabel'=>'Toe Signed','fullLabel'=>'Toe Signed'],
        'toe.sendfe'=>['shortLabel'=>'Toe Send Follow-up','fullLabel'=>'Toe Send Follow-up'],
        'quotation.taxinvoice'=>['shortLabel'=>'Quotation Tax Invoice','fullLabel'=>'Quotation Tax Invoice'],
        'quotation.taxinvoiceFirstHalf'=>['shortLabel'=>'Quotation Tax Invoice First 50%','fullLabel'=>'Quotation Tax Invoice First Half Payment'],
        'quotation.taxinvoiceSecondHalf'=>['shortLabel'=>'Quotation Tax Invoice Final 50%','fullLabel'=>'Quotation Tax Invoice Second Half Payment'], 
        'quotation.feeDifference'=>['shortLabel'=>'Quotation Fee more than 10% Difference','fullLabel'=>'Quotation Fee more than 10% Difference'],
          'quotation.verified'=>['shortLabel'=>"Quotation Verified",'fullLabel'=>"Quotation Verified(Follow Up Email)"],
        // 'quote.submitted'=>['shortLabel'=>'Submitted','fullLabel'=>'Quotation Submitted'],
        // 'quote.accepted'=>['shortLabel'=>'Accepted','fullLabel'=>'Quotation Accepted'],
      ],
       'Valuation'=>[
        'schedule.send'=>['shortLabel'=>'Schedule Inspection Send','fullLabel'=>'Schedule Inspection Send'],
        'schedule.reminder'=>['shortLabel'=>'Schedule Inspection Reminder','fullLabel'=>'Schedule Inspection Reminder'],
        'reason.send'=>['shortLabel'=>'Unable to Schedule Inspection','fullLabel'=>'Unable to Schedule Inspection'],
        'summary.send'=>['shortLabel'=>'Summary Send','fullLabel'=>'Summary  Send'],
        'review.send'=>['shortLabel'=>'Review Send','fullLabel'=>'Review Send'],
        'Valuation.approval.send'=>['shortLabel'=>'Approval Send','fullLabel'=>'Approval Send'],
         'valuation.monthlymorethanten'=>['shortLabel'=>'Monthly more than Ten Instructions','fullLabel'=>'Monthly more than Ten Instructions'],
         'valuation.yearly'=>['shortLabel'=>'Thank You For Your Trusting','fullLabel'=>'Thank You For Your Trusting Us With your Valuation Requirements'],
         'valuation.monthlylessthanten'=>['shortLabel'=>'Monthly less than Ten Instructions','fullLabel'=>'Monthly less than Ten Instructions'],
         'valuation.monthlyzero'=>['shortLabel'=>'Monthly zero Instruction','fullLabel'=>'Monthly zero Instruction'],
         'valuation.ajman'=>['shortLabel'=>'Ajman RERA','fullLabel'=>'Ajman RERA'],
         'valuation.clientnotification'=>['shortLabel'=>'Ajman RERA Client Notification','fullLabel'=>'Ajman RERA Client Notification'],
         'inspectProperty.send'=>['shortLabel'=>'Inspect Property Send','fullLabel'=>'Inspect Property Send'],
         'Received.Valuation'=>['shortLabel'=>'Received.Valuation','fullLabel'=>'Received.Valuation'],
         'Received.Doc'=>['shortLabel'=>'Received Documents','fullLabel'=>'Received Valuation Documents'],
         'valuation.contested'=>['shortLabel'=>'Valuation Contested','fullLabel'=>'Valuation Contested'],
         'valuation.mistake'=>['shortLabel'=>'Error/Mistake in the Valuation ','fullLabel'=>'Error/Mistake in the Valuation'],
         'valuation.clientupdate'=>['shortLabel'=>'Client Update ','fullLabel'=>'Client Update'],
         'Inspect.Property'=>['shortLabel'=>'Inspect Property','fullLabel'=>'Inspect.Property'],
         'Client.valuations.send'=>['shortLabel'=>'Client Valuations Send','fullLabel'=>'Client Valuations Send'],
         'Dib.invAndVal'=>['shortLabel'=>'DIB Inv And Val','fullLabel'=>'Dib.invAndVal'],
         'Received.Client.Valuation'=>['shortLabel'=>'Received.Client.Valuation','fullLabel'=>'Received.Client.Valuation'],
         'valuation.feeDifference' => ['shortLabel' => 'Valuation Fee more than 10% Difference', 'fullLabel' => 'Valuation Fee more than 10% Difference'],
         'ajman.valuations.send'=>['shortLabel'=>'Ajman Monthly Valuations','fullLabel'=>'Ajman Monthly Valuations'],
         'ajmanrera.followupgovt.send'=>['shortLabel'=>'Ajman Rera Follow Up','fullLabel'=>'Ajman Rera Follow Up'],
         'ajmanrera.followupclient.send'=>['shortLabel'=>'Ajman RERA Client Notification Follow Up','fullLabel'=>'Ajman RERA Client Notification Follow Up'],
         //'valuation.partialinspectionclient'=>['shortLabel'=>'Ajman RERA','fullLabel'=>'Ajman RERA'],
         'valuation.partialinspectionclientprivacy'=>['shortLabel'=>' Not fully inspected due to privacy concern','fullLabel'=>' Not fully inspected due to privacy concern'],
         'valuation.partialinspectionclientdoor'=>['shortLabel'=>' Not fully inspected since the door is locked.','fullLabel'=>' Not fully inspected since the door is locked.'],
         'valuation.partialinspectionclientphoto'=>['shortLabel'=>' Inspection is partially completed but photo is not allowed','fullLabel'=>' Inspection is partially completed but photo is not allowed'],
         'onlineval.website'=>['shortLabel'=>' Online Valuation Received','fullLabel'=>' Online Valuation Received'],
       ],
       'Company'=>[
        'Valuation.Feedback.Form'=>['shortLabel'=>'Valuation Feedback Form','fullLabel'=>'Valuation Feedback Form'],
        'Proposal.Feedback.Form'=>['shortLabel'=>'Proposal Feedback Form','fullLabel'=>'Proposal Feedback Form'],
        'Client.pdf.send'=>['shortLabel'=>'Client Pdf Send','fullLabel'=>'Client Pdf Send'],
        'Client.monthly.invoices'=>['shortLabel'=>"Client's Monthly Invoices",'fullLabel'=>"Client's Monthly Invoices"],
        'client.annual.valuation.reminder'=>['shortLabel'=>"Annual Valuation Reminder",'fullLabel'=>"Client's Annual Valuation Reminder"],
      ],
        'Transactions'=>[
            'Sold.uploaded'=>['shortLabel'=>'Sold not uploaded','fullLabel'=>'Sold not uploaded'],
        ],
        'Sales And Marketing'=>[
            'sm.meeting'=>['shortLabel'=>'Meeting Schedule','fullLabel'=>'Meeting Schedule'],
            'sm.reminderemail'=>['shortLabel'=>'Meeting Email Reminder','fullLabel'=>'Meeting Email Reminder'],
            'sm.feedbackemail'=>['shortLabel'=>'Meeting Feedback Email','fullLabel'=>'Meeting Feedback Email'],
        ],
        'V1 Valuations'=>[
            'v1.contested'=>['shortLabel'=>'V1 Contested Valuations','fullLabel'=>'V1 Contested Valuations'],
            'v1.error.mistake'=>['shortLabel'=>'V1 Error/Mistake Valuations ','fullLabel'=>'V1 Error/Mistake Valuations '],
            'v1.client.reminder'=>['shortLabel'=>'V1 Client Reminder','fullLabel'=>'V1 Client Reminder'],
        ],
    ];
  }

  /**
  * return events for triggers dropdown
  */
  public static function getSystemNotificationEvents()
  {
    $arr = [];
    $events = (new NotificationListHelper)::getSystemNotificationEventsArr();
    if($events!=null){
      foreach($events as $mkey=>$eventHeadings){
        $subArr=[];
        foreach($eventHeadings as $key=>$eventInfo){
          $subArr[$key]=$eventInfo['shortLabel'];
        }
        $arr[$mkey]=$subArr;
      }
    }
    return $arr;
  }

  /**
  * return events labels for grid
  */
  public static function getSystemNotificationEventLabels()
  {
    $arr = [];
    $events = (new NotificationListHelper)::getSystemNotificationEventsArr();
    if($events!=null){
      foreach($events as $mkey=>$eventHeadings){
        foreach($eventHeadings as $key=>$eventInfo){
          $arr[$key]=$eventInfo['fullLabel'];
        }
      }
    }
    return $arr;
  }

  /**
  * return notification services
  */
  public static function getNotificationTypes()
  {
    $allowedServices=[];
    $settings=Setting::find()->where(['config_name'=>['enable_email_notification','enable_sms_notification','enable_inapp_notification']])->all();
    foreach($settings as $record){
      if($record['config_name']=='enable_email_notification' && $record['config_value']==1)$allowedServices['email']='Email';
      if($record['config_name']=='enable_sms_notification' && $record['config_value']==1)$allowedServices['sms']='SMS';
      if($record['config_name']=='enable_inapp_notification' && $record['config_value']==1)$allowedServices['push']='Push Notification';
    }
    return $allowedServices;
  }

  /**
  * return templates
  */
  public static function getNotificationTemplates()
  {
    return NotificationTemplate::find()
    ->select([
      NotificationTemplate::tableName().'.id',
      'CONCAT(subject," (",keyword,")") AS tmpsubject'
    ])
    ->innerJoin(NotificationTemplateDetail::tableName(),NotificationTemplateDetail::tableName().".[[notification_template_id]]=".NotificationTemplate::tableName().".[[id]]")
    ->where([
      'and',
      ['lang'=>'en'],
      ['is','deleted_at',new Expression('null')]
    ])
    ->asArray()->all();
  }
  /**
  * return templates array
  */
  public static function getNotificationTemplatesArr()
  {
    return ArrayHelper::map((new NotificationListHelper)->getNotificationTemplates(),"id","tmpsubject");
  }

  /**
  * return staff roles
  */
  public static function getStaffRolesList()
  {
    return AdminGroup::find()->where(['and',['!=','id',1],['status'=>1,'trashed'=>0]])->asArray()->all();
  }

  /**
  * return staff roles array
  */
  public static function getStaffRolesListArr()
  {
    return ArrayHelper::map((new NotificationListHelper)->getStaffRolesList(),"id","title");
  }
}
