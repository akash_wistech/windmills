<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NotificationTemplate */

$this->title = Yii::t('app', 'Notification Templates');
$cardTitle = Yii::t('app','Notification Template:  {nameAttribute}', [
    'nameAttribute' => $model->langDetail->subject,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="notification-template-update">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
