<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app','Notification Settings');
$cardTitle = $this->title;
$this->params['breadcrumbs'][] = $cardTitle;

$this->registerJs('
$("body").on("change", ".notifyCb", function (e) {
  val=0;
  if($(this).is(":checked")){
    val=1;
  }else{
    val=0;
  }
  fld=$(this).attr("name");
  url = "'.Url::to(['system/save-notification','fld'=>'']).'"+fld;
  makeSilentAjaxRequest(url,"settings-card");
});
');
?>
<div class="change-password">
  <section class="change-password-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['id' => 'notification-form']); ?>
    <header class="card-header">
      <h2 class="card-title"><?= $cardTitle?></h2>
    </header>
    <div class="card-body">

      <div class="form-group row">
        <div class="col-6">
        </div>
        <div class="col-2">
          <?= Yii::t('app','Email')?>
        </div>
        <div class="col-2">
          <?= Yii::t('app','SMS')?>
        </div>
        <div class="col-2">
          <?= Yii::t('app','Push Notification')?>
        </div>
      </div>
      <div class="form-group row">
        <div class="col-6">
          <?= Yii::t('app','Enable Notification')?>
        </div>
        <div class="col-2">
          <span class="switch switch-outline switch-icon switch-success">
            <label>
              <input type="checkbox" class="notifyCb" value="1"<?= $model->enable_email_notification==1 ? ' checked="checked"' : ''?> name="enable_email_notification"/>
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-2">
          <span class="switch switch-outline switch-icon switch-success">
            <label>
              <input type="checkbox" class="notifyCb" value="1"<?= $model->enable_sms_notification==1 ? ' checked="checked"' : ''?> name="enable_sms_notification"/>
              <span></span>
            </label>
          </span>
        </div>
        <div class="col-2">
          <span class="switch switch-outline switch-icon switch-success">
            <label>
              <input type="checkbox" class="notifyCb" value="1"<?= $model->enable_inapp_notification==1 ? ' checked="checked"' : ''?> name="enable_inapp_notification"/>
              <span></span>
            </label>
          </span>
        </div>
      </div>
    </div>
    <?php ActiveForm::end(); ?>
  </section>
</div>
