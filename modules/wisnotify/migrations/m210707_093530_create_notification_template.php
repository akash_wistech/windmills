<?php

use yii\db\Migration;

/**
 * Class m210707_093530_create_notification_template
 */
class m210707_093530_create_notification_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210707_093530_create_notification_template cannot be reverted.\n";

        return false;
    }

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
      $this->createTable('notification_templates', [
            'id' => $this->primaryKey(),
            'keyword' => $this->char(50),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'deleted_by' => $this->integer()
        ]);
    }

    public function down()
    {
      $this->dropTable('notification_templates');
    }
}
