<?php

use yii\db\Migration;

/**
 * Class m210707_093715_create_notification_template_detail
 */
class m210707_093715_create_notification_template_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210707_093715_create_notification_template_detail cannot be reverted.\n";

        return false;
    }

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
      $this->createTable('notification_templates_detail', [
            'id' => $this->primaryKey(),
            'notification_template_id' => $this->integer(),
            'lang' => $this->char(10),
            'subject' => $this->string(150),
            'email_body' => $this->text(),
            'push_body' => $this->text(),
            'sms_body' => $this->text()
        ]);
    }

    public function down()
    {
      $this->dropTable('notification_templates_detail');
    }
}
