<?php

namespace app\modules\wisnotify\models;

use Yii;
use app\components\models\ActiveRecordFull;
use app\modules\wisnotify\helpers\NotificationListHelper;

/**
* This is the model class for table "{{%notification_triggers}}".
*
* @property integer $id
* @property string $event_id
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property string $deleted_at
* @property integer $deleted_by
*/
class NotificationTrigger extends ActiveRecordFull
{
	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%notification_triggers}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['event_id','user_type','cc_roles','template_id','notification_triggers'], 'required'],
			[['user_type', 'notification_triggers', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
			[['template_id', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
			[['event_id'], 'string', 'max' => 255],
      [['event_id'],'trim'],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'event_id' => Yii::t('app', 'Event'),
			'user_type' => Yii::t('app', 'To'),
			'cc_roles' => Yii::t('app', 'Cc'),
			'template_id' => Yii::t('app', 'Template'),
			'notification_triggers' => Yii::t('app', 'Notification Type'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'deleted_at' => Yii::t('app', 'Trashed At'),
			'deleted_by' => Yii::t('app', 'Trashed By'),
		];
	}

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->event_id;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'Notification Trigger';
  }

	/**
	* @inheritdoc
	* Generates auth_key if it is a new user
	*/
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			// $this->user_type=json_encode($this->user_type);
			$this->notification_triggers=json_encode($this->notification_triggers);
			$this->cc_roles=json_encode($this->cc_roles);
			return true;
		}
		return false;
	}

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getTemplate()
  {
    return $this->hasOne(NotificationTemplate::className(), ['id' => 'template_id']);
  }

  /**
  * Get To User types.
  */
  public function getUserTypeLabels()
  {
    $html='';
    if($this->user_type!=null){
      $systemUserTypes = NotificationListHelper::getSystemUserTypes();
			$html = '<span class="badge badge-sm badge-primary">'.$systemUserTypes[$this->user_type].'</span>';
      // $types=json_decode($this->user_type);
      // foreach($types as $key=>$val){
      //   $html.=$html!='' ? ' ' : '';
      //   $html.='<span class="badge badge-sm badge-primary">'.$systemUserTypes[$val].'</span>';
      // }
    }
    return $html;
  }

  /**
  * Get Cc User types.
  */
  public function getCcUserTypeLabels()
  {
    $html='';
    if($this->cc_roles!=null){
      $systemUserRoles = NotificationListHelper::getStaffRolesListArr();
			$types=json_decode($this->cc_roles);
      foreach($types as $key=>$val){
        $html.=$html!='' ? ' ' : '';
        $html.='<span class="badge badge-sm badge-primary">'.$systemUserRoles[$val].'</span>';
      }
    }
    return $html;
  }

  /**
  * Get notifications services.
  */
  public function getNotificationTypeLabels()
  {
    $html='';
    if($this->notification_triggers!=null){
      $systemNotificationTypes = NotificationListHelper::getNotificationTypes();
      $types=json_decode($this->notification_triggers);
      foreach($types as $key=>$val){
        $html.=$html!='' ? ' ' : '';
        $html.='<span class="badge badge-sm badge-info">'.$systemNotificationTypes[$val].'</span>';
      }
    }
    return $html;
  }
}
