<?php

namespace app\modules\wisnotify\models;

use Yii;
use yii\db\ActiveRecord;

class NotificationTemplateDetail extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%notification_templates_detail}}';
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getMainRow()
  {
    return $this->hasOne(NotificationTemplate::className(), ['id' => 'notification_template_id']);
  }

}
