<?php

namespace app\modules\wisnotify\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%setting}}".
*
* @property integer $id
* @property string $config_name
* @property string $config_value
*/
class Setting extends ActiveRecord
{
  public $enable_email_notification,$enable_sms_notification,$enable_inapp_notification;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%setting}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['config_name','config_value'],'required'],
      [['config_name','config_value'],'safe'],
      [['config_name','config_value'],'trim'],
    ];
  }
}
