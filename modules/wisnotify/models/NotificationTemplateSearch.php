<?php

namespace app\modules\wisnotify\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\wisnotify\models\NotificationTemplate;
use yii\db\Expression;

/**
* NotificationTemplateSearch represents the model behind the search form about `app\models\NotificationTemplate`.
*/
class NotificationTemplateSearch extends NotificationTemplate
{
  public $subject,$pageSize;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'created_by', 'updated_by', 'trashed', 'trashed_by','pageSize'], 'integer'],
      [['subject', 'keyword', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    $query = NotificationTemplate::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
      // 'event_id' => $this->event_id,
      // 'user_type' => $this->user_type,
      // 'template_id' => $this->template_id,
      // 'notification_triggers' => $this->notification_triggers,
    ]);
    $query->andWhere([
        'is','deleted_at',new Expression('null')
    ]);

    // $query->andFilterWhere(['like', 'title', $this->title]);

    return $dataProvider;
  }
}
