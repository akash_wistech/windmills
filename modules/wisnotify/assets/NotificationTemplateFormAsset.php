<?php
namespace app\modules\wisnotify\assets;

use yii\web\AssetBundle;

class NotificationTemplateFormAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\TinyMceAsset',
  ];
}
