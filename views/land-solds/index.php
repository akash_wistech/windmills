<?php

use yii\helpers\Html;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LandSoldsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Land Solds';
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;


$createBtn = true;

if (Yii::$app->menuHelperFunction->checkActionAllowed('import')) {
    $import = true;
}
$import = true;
$actionBtns .= '{view}';



?>
<div class="sold-ad-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'import' => $import,
        'columns' => [
            'id',
            'reidin_ref',
            'transaction_type',
            'transaction_date',
            'community',
            'property',
            'property_type',
            'size_sqf',
            'land_size',
            'amount',
            'sqf',
            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'status',
                'label' => Yii::t('app', 'Status Verification'),
                'value' => function ($model) {
                    if($model->status == 0){
                        return '<span class="badge grid-badge badge-danger"> Pending</span>';
                    }else if($model->status == 1){
                        return '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> Converted</span>';
                    }else if($model->status == 2){
                        return '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> Duplicate</span>';
                    }else if($model->status == 3){
                        return '<span class="badge grid-badge badge-danger"><i class="fa fa-check"></i> Not Converted</span>';
                    }
                    // return Yii::$app->appHelperFunctions->statusVerifyLabel[$model->status];
                },
                'filter' => array(0=>'Pending', 2 => 'Duplicate', 1 => 'Converted',3=>'Not Found')
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
                'contentOptions' => ['class' => 'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              ' . $actionBtns . '
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'status' => function ($url, $model) {
                        if ($model['status'] == 1) {
                            return Html::a('<i class="fas fa-eye-slash"></i> ' . Yii::t('app', 'Disable'), $url, [
                                'title' => Yii::t('app', 'Disable'),
                                'class' => 'dropdown-item text-1',
                                'data-confirm' => Yii::t('app', 'Are you sure you want to disable this item?'),
                                'data-method' => "post",
                            ]);
                        } else {
                            return Html::a('<i class="fas fa-eye"></i> ' . Yii::t('app', 'Enable'), $url, [
                                'title' => Yii::t('app', 'Enable'),
                                'class' => 'dropdown-item text-1',
                                'data-confirm' => Yii::t('app', 'Are you sure you want to enable this item?'),
                                'data-method' => "post",
                            ]);
                        }
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> ' . Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'dropdown-item text-1',
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'data-method' => "post",
                            'data-pjax' => "0",
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div>

