<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LandSolds */

$this->title = 'Update Land Solds: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Land Solds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="land-solds-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
