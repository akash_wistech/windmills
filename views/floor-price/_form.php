<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use  app\components\widgets\StatusVerified;
/* @var $this yii\web\View */
/* @var $model app\models\FloorPrice */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="active-zone-form card">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <?php
                    echo $form->field($model, 'client')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\Company::find()
                            ->where(['status' => 1])
                            ->andWhere(['client_type' => 'bank'])
                            ->andWhere([
                                'or',
                                ['data_type' => 0],
                                ['data_type' => null],
                            ])
                            ->orderBy(['title' => SORT_ASC,])
                            ->all(), 'id', 'title'),

                        'options' => ['placeholder' => 'Select a Client ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Client Name');
                    ?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <?php
                    echo $form->field($model, 'property_type')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy([
                            'title' => SORT_ASC,
                        ])->all(), 'id', 'title'),
                        'options' => ['placeholder' => 'All Property Types','class'=> 'client-cls'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <?php
                    echo $form->field($model, 'city')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->emiratedListArrSla,
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <?= $form->field($model, 'fee')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

        </div>
        <?php
        if(Yii::$app->menuHelperFunction->checkActionAllowed('Verified')){
            echo StatusVerified::widget([ 'model' => $model, 'form' => $form ]);
        }
        ?>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
