<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FloorPriceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="floor-price-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'client') ?>

    <?= $form->field($model, 'property_type') ?>

    <?= $form->field($model, 'sub_property_type') ?>

    <?= $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'tenure') ?>

    <?php // echo $form->field($model, 'tat') ?>

    <?php // echo $form->field($model, 'fee') ?>

    <?php // echo $form->field($model, 'valuation_approach') ?>

    <?php // echo $form->field($model, 'inspection_type') ?>

    <?php // echo $form->field($model, 'priority_type') ?>

    <?php // echo $form->field($model, 'revalidation_fee') ?>

    <?php // echo $form->field($model, 'independent_villa_fee') ?>

    <?php // echo $form->field($model, 'community_villa_fee') ?>

    <?php // echo $form->field($model, 'staff_fee') ?>

    <?php // echo $form->field($model, 'status_verified') ?>

    <?php // echo $form->field($model, 'status_verified_at') ?>

    <?php // echo $form->field($model, 'status_verified_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
