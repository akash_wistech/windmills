<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FloorPrice */

$this->title = 'Update Floor Price: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Floor Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="floor-price-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
