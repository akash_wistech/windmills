<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FloorPrice */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Floor Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="floor-price-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'client',
            'property_type',
            'sub_property_type',
            'city',
            'tenure',
            'tat',
            'fee',
            'valuation_approach',
            'inspection_type',
            'priority_type',
            'revalidation_fee',
            'independent_villa_fee',
            'community_villa_fee',
            'staff_fee',
            'status_verified',
            'status_verified_at',
            'status_verified_by',
            'created_at',
            'updated_at',
            'deleted_at',
            'created_by',
            'updated_by',
            'deleted_by',
        ],
    ]) ?>

</div>
