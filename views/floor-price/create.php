<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FloorPrice */

$this->title = 'Create Floor Price';
$this->params['breadcrumbs'][] = ['label' => 'Floor Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="floor-price-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
