<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SoldData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sold-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'building_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bedrooms')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bua')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plot_area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'property_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'listing_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
