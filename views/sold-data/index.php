<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SoldDataReasonsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Auto Sold Transactions');
$cardTitle = Yii::t('app', 'Auto Sold Transactions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sold-data-index">


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'import' => $import,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],

            'building_title',
            'bedrooms',
            'bua',
            'plot_area',
            'unit_number',
            'property_type',
            'listing_date',
            'price',


        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>

