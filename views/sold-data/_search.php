<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SoldDataReasonsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sold-data-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'building_title') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'bedrooms') ?>

    <?= $form->field($model, 'bua') ?>

    <?php // echo $form->field($model, 'plot_area') ?>

    <?php // echo $form->field($model, 'unit_number') ?>

    <?php // echo $form->field($model, 'property_type') ?>

    <?php // echo $form->field($model, 'listing_date') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
