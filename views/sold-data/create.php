<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SoldData */

$this->title = 'Create Sold Data';
$this->params['breadcrumbs'][] = ['label' => 'Sold Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sold-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
