<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientValuation */

$this->title = 'Enter New Valuation';
// $this->params['breadcrumbs'][] = ['label' => 'Enter New Valuation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="sales-and-marketing-purpose-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
