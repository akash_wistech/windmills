<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ValuationFormAsset;
use app\components\widgets\StatusVerified;

ValuationFormAsset::register($this);
use kartik\depdrop\DepDrop;


$client_type = Yii::$app->user->identity->company->client_type;
// dd($client_type);


$show_number_of_units = array_merge(array(3, 10, 42, 81));
$show_unit_number = array_merge(array(1, 2, 6, 17, 28));
$show_floor_number = array_merge(array(1, 17, 28));
$show_extended = array_merge(array(1, 2, 6, 10, 17, 28));


$this->registerJs('

    $("#onlinevaluation_valuation_date").datetimepicker({
        allowInputToggle: true,
        // viewMode: "months",
        format: "DD-MMM-YYYY",
        useCurrent: false,
    });

    $("#onlinevaluation_inspection_date").datetimepicker({
        allowInputToggle: true,
        format: "DD-MMM-YYYY", // Use "hh:mm A" for 12-hour time format
        useCurrent: false, // Do not default to the current date/time
        showClear: true, // Show a clear button
        showTodayButton: true, // Show a "Today" button
        minDate: moment().startOf("day"), // Set the minimum date to today
        icons: {
            time: "fas fa-clock", // Use Font Awesome icons for time
            date: "fas fa-calendar", // Use Font Awesome icons for date
            up: "fas fa-chevron-up", // Use Font Awesome icons for up arrow
            down: "fas fa-chevron-down", // Use Font Awesome icons for down arrow
            previous: "fas fa-chevron-left", // Use Font Awesome icons for previous button
            next: "fas fa-chevron-right", // Use Font Awesome icons for next button
            today: "fas fa-calendar-day", // Use Font Awesome icons for "Today" button
            clear: "fas fa-trash-alt", // Use Font Awesome icons for "Clear" button
            close: "fas fa-times" // Use Font Awesome icons for close button
        } 
    });

    $("#onlinevaluation-inspection_time").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "HH:mm",
        showClear: true, // Show a clear button
        showTodayButton: true, // Show a "Today" button
    });
    

    $(document).on("click", function (e) {
        if ($(e.target).closest("#onlinevaluation_valuation_date,#onlinevaluation_inspection_date").length === 0) {
            $("#onlinevaluation_valuation_date,#onlinevaluation_inspection_date").datetimepicker("hide");
        }
    });

    $("body").on("click", ".delete-file", function (e) {

        id = $(this).attr("id")
        data = {id:id}
    
        swal({
            title: "' . Yii::t('app', 'Are you sure you want to delete file from bucket ?') . '",
            html: "' . Yii::t('app', '') . '",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#47a447",
            confirmButtonText: "' . Yii::t('app', 'Yes, Delete it!') . '",
            cancelButtonText: "' . Yii::t('app', 'Cancel') . '",
        },function(result) {
            if (result) {
                $.ajax({
                    data : data,
                    url: "' . Url::to(['valuation/delete-file']) . '",
                    dataType: "html",
                    type: "POST",
                    success: function(data) {
                        data = JSON.parse(data)
                        if(data.msg == "success"){
    
                            deleted = "#deleted-"+id;
                            $(deleted). attr("src", "' . Yii::$app->params['uploadIcon'] . '");
    
                            removed = ".removed-"+id;
                            $(removed).remove()
    
                            removedDelBtn = "#del-btn-"+id;
                            $(removedDelBtn).remove()
    
                            swal({
                                title: "' . Yii::t('app', 'Successfully Deleted') . '",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#47a447",
                            });
                        }
                    },
                    error: bbAlert
                });
            }
        });
    });


    $(document).ready(function(){

        var key = $("#onlinevaluation-key").val(); 
        var propertyId = $("#onlinevaluation-property_id").val(); 
        var extended = $("#onlinevaluation-extended").val();
        var completionStatus = $("#onlinevaluation-completion_status").val();
        var city = $("#onlinevaluation-city").val();
        var numberOfUnits = $("#onlinevaluation-number_of_units").val();

        $.ajax({
            // url: "' . Yii::$app->urlManager->createUrl('online-valuation/online-valuation-documents?id=') . $model->id . '",
            url: "' . Yii::$app->urlManager->createUrl('online-valuation/online-valuation-documents') . '",
            method: "POST",
            data: {propertyId: propertyId, key: key, extended: extended, completionStatus: completionStatus, city: city, numberOfUnits: numberOfUnits}, 
            success: function(response) {
                $("#property-documents-container").html(response);
            }
        });

        $("#onlinevaluation-property_id").change(function(){
            var propertyId = $(this).val(); 
            var extended = $("#onlinevaluation-extended").val();
            var key = $("#onlinevaluation-key").val(); 
            var completionStatus = $("#onlinevaluation-completion_status").val();
            var city = $("#onlinevaluation-city").val();
            var numberOfUnits = $("#onlinevaluation-number_of_units").val();
            
            $.ajax({
                // url: "online-valuation/update-variable", 
                // url: "' . Yii::$app->urlManager->createUrl('online-valuation/online-valuation-documents?id=') . $model->id . '",
                url: "' . Yii::$app->urlManager->createUrl('online-valuation/online-valuation-documents') . '",
                method: "POST",
                data: {propertyId: propertyId, key: key, extended: extended, completionStatus: completionStatus, city: city, numberOfUnits: numberOfUnits}, 
                success: function(response) {
                    $("#property-documents-container").html(response);
                }
            });
        });

        $("#onlinevaluation-extended").change(function(){
            var extended = $(this).val(); 
            var propertyId = $("#onlinevaluation-property_id").val(); 
            var key = $("#onlinevaluation-key").val(); 
            var completionStatus = $("#onlinevaluation-completion_status").val();
            var city = $("#onlinevaluation-city").val();
            var numberOfUnits = $("#onlinevaluation-number_of_units").val();
            
            $.ajax({
                // url: "online-valuation/update-variable", 
                // url: "' . Yii::$app->urlManager->createUrl('online-valuation/online-valuation-documents?id=') . $model->id . '",
                url: "' . Yii::$app->urlManager->createUrl('online-valuation/online-valuation-documents') . '",
                method: "POST",
                data: {propertyId: propertyId, key: key, extended: extended, completionStatus: completionStatus, city: city, numberOfUnits: numberOfUnits}, 
                success: function(response) {
                    $("#property-documents-container").html(response);
                }
            });
        });

        $("#onlinevaluation-completion_status").change(function(){
            var completionStatus = $(this).val(); 
            var propertyId = $("#onlinevaluation-property_id").val(); 
            var extended = $("#onlinevaluation-extended").val();
            var key = $("#onlinevaluation-key").val(); 
            var city = $("#onlinevaluation-city").val();
            var numberOfUnits = $("#onlinevaluation-number_of_units").val();
            
            $.ajax({
                // url: "online-valuation/update-variable", 
                // url: "' . Yii::$app->urlManager->createUrl('online-valuation/online-valuation-documents?id=') . $model->id . '",
                url: "' . Yii::$app->urlManager->createUrl('online-valuation/online-valuation-documents') . '",
                method: "POST",
                data: {propertyId: propertyId, key: key, extended: extended, completionStatus: completionStatus, city: city, numberOfUnits: numberOfUnits}, 
                success: function(response) {
                    $("#property-documents-container").html(response);
                }
            });
        });

        $("#onlinevaluation-number_of_units").change(function(){
            var numberOfUnits = $(this).val();
            var completionStatus = $("#onlinevaluation-completion_status").val();
            var propertyId = $("#onlinevaluation-property_id").val(); 
            var extended = $("#onlinevaluation-extended").val();
            var key = $("#onlinevaluation-key").val(); 
            var city = $("#onlinevaluation-city").val();
            
            $.ajax({
                // url: "online-valuation/update-variable", 
                // url: "' . Yii::$app->urlManager->createUrl('online-valuation/online-valuation-documents?id=') . $model->id . '",
                url: "' . Yii::$app->urlManager->createUrl('online-valuation/online-valuation-documents') . '",
                method: "POST",
                data: {propertyId: propertyId, key: key, extended: extended, completionStatus: completionStatus, city: city, numberOfUnits: numberOfUnits}, 
                success: function(response) {
                    $("#property-documents-container").html(response);
                }
            });
        });

    });


    $(document).ready(function(){
        var $bua_value = $("#onlinevaluation-built_up_area_value");
        var $bua_unit = $("#onlinevaluation-built_up_area_unit");
        var $bua_auto = $("#onlinevaluation-built_up_area");
        var $nla_value = $("#onlinevaluation-net_leasable_area_value");
        var $nla_unit = $("#onlinevaluation-net_leasable_area_unit");
        var $nla_auto = $("#onlinevaluation-net_leasable_area");
        var $plot_value = $("#onlinevaluation-plot_area_value");
        var $plot_unit = $("#onlinevaluation-plot_area_unit");
        var $plot_auto = $("#onlinevaluation-plot_area");

        convertSqMtrToSqFt($bua_value,$bua_unit,$bua_auto);
        convertSqMtrToSqFt($nla_value,$nla_unit,$nla_auto);
        convertSqMtrToSqFt($plot_value,$plot_unit,$plot_auto);

        function convertSqMtrToSqFt($field_value,$field_unit,$field_auto){

            $field_value.on("input", function () {
                var field_value = $field_value.val().replace(",", ".");
                if (field_value !== "") {
                    if ($field_unit.val() == 2) {
                        var sq_meter = parseFloat(field_value);
                        if (!isNaN(sq_meter)) {
                            var sq_feet = (sq_meter * 10.763915).toFixed(2); 
                            $field_auto.val(sq_feet);
                        }
                    } else {
                        $field_auto.val(field_value);
                    }
                } else {
                    $field_auto.val("");
                }
            });
        
            $field_unit.on("change", function () {
                var field_value = $field_value.val().replace(",", ".");
                if (field_value !== "") {
                    if ($field_unit.val() == 1) {
                        $field_auto.val(field_value);
                    } else {
                        var sq_meter = parseFloat(field_value);
                        if (!isNaN(sq_meter)) {
                            var sq_feet = (sq_meter * 10.763915).toFixed(2); 
                            $field_auto.val(sq_feet);
                        }
                    }
                } else {
                    $field_auto.val("");
                }
            });
        }

    });

    $(document).ready(function(){

  $("#onlinevaluation-community").html("");
                $("#onlinevaluation-building_info").html("");
                $("#onlinevaluation-sub_community").html("");
                $("#onlinevaluation-city").val("");
                $("#onlinevaluation-country").val("");
        var community_id = $("#onlinevaluation-community").val();
        var community_url = "communities/communitydetail/"+community_id;

        // communitySelection(community_id,community_url);
        
        $("#onlinevaluation-city_id").on("change", function () {

            var city_id = $(this).val();
            var city_url = "communities/communitydetailbycity/"+city_id;
             
            citySelection(city_id,city_url);            
        });
        
         function citySelection(city_id,city_url){
            heading=$(this).data("heading");
            if (city_id) {
                $.ajax({
                    url: city_url, 
                    success: function(data){
                        var response = JSON.parse(data);
                        // Populate fields with received data
                        
                        $("#onlinevaluation-community").html(response.communitiesOptionsHtml);
                       
                        $("#onlinevaluation-city").val(response.city);
                        $("#onlinevaluation-country").val(response.country);
                        
                $("#onlinevaluation-building_info").html("");
                $("#onlinevaluation-sub_community").html("");
               
                    },
                    error: function(xhr, status, error) {
                        console.error("AJAX error:", error);
                    }
                });
            } else {
                // Clear fields if no community selected
                $("#onlinevaluation-community").html("");
                $("#onlinevaluation-building_info").html("");
                $("#onlinevaluation-sub_community").html("");
                $("#onlinevaluation-city").val("");
                $("#onlinevaluation-country").val("");
            }
        }
        
        
        

        $("#onlinevaluation-community").on("change", function () {

            var community_id = $(this).val();
            var community_url = "communities/communitydetail/"+community_id;
             
            communitySelection(community_id,community_url);            
        });

        function communitySelection(community_id,community_url){
            heading=$(this).data("heading");
            if (community_id) {
                $.ajax({
                    url: community_url, 
                    success: function(data){
                        var response = JSON.parse(data);
                        // Populate fields with received data
                        
                        $("#onlinevaluation-building_info").html(response.buildingOptions);
                        $("#onlinevaluation-sub_community").html(response.subCommunityOptions);
                        $("#onlinevaluation-city").val(response.city);
                        $("#onlinevaluation-country").val(response.country);
                    },
                    error: function(xhr, status, error) {
                        console.error("AJAX error:", error);
                    }
                });
            } else {
                // Clear fields if no community selected
                $("#onlinevaluation-building_info").html("");
                $("#onlinevaluation-sub_community").html("");
                $("#onlinevaluation-city").val("");
                $("#onlinevaluation-country").val("");
            }
        }

    });


    


    


    $(document).ready(function () {
        var $building_info = $("#onlinevaluation-building_info");
        var $property_id = $("#onlinevaluation-property_id");
    
        function toggleFieldsVisibilityByPropertyType(propertyTypeArray, className) {
            if (propertyTypeArray.includes(parseInt($property_id.val()))) {
                $("." + className).show();
            } else {
                $("." + className).hide();
            }
        }
    
        var showFieldUnitNumber= ' . json_encode($show_unit_number) . ';    
        var showFieldFloorNumber= ' . json_encode($show_floor_number) . ';    
        var showFieldNumberOfUnits= ' . json_encode($show_number_of_units) . ';
        var showFieldExtended= ' . json_encode($show_extended) . ';

        function toggleFunctions(){   
            toggleFieldsVisibilityByPropertyType(showFieldUnitNumber, "unit_number");
            toggleFieldsVisibilityByPropertyType(showFieldFloorNumber, "floor_number");
            toggleFieldsVisibilityByPropertyType(showFieldNumberOfUnits, "number_of_units");
            toggleFieldsVisibilityByPropertyType(showFieldExtended, "extended");
        }
    
        $building_info.on("change", function () {
            toggleFunctions();
        });
    
        $property_id.on("change", function () {
            toggleFunctions();
        });
    
        if($building_info.val() != ""){ 
            toggleFunctions();
        }

    
    }); 

    
    // $("#w0").on("submit", function (e) {
    //     e.preventDefault();
        
    //     var flag = 0;
    //     var mandatory_doc = $(".mandatory-doc");
    //     for(var i = 0; i < mandatory_doc.length; i++){
    //         var id = mandatory_doc.eq(i).attr("id");
    //         if($("#"+id).val() == ""){
    //             $("#image-row" + i + " .help-block2").html("This document cannot be blank").show();
    //             flag += 0;
    //         }else{
    //             $("#image-row" + i + " .help-block2").html("").hide();
    //             flag += 1;
    //         }
    //     }
    //     if(flag == mandatory_doc.length){
    //         this.submit(); // Directly submit the form
    //     }
    // });
    
$("body").on("change", "#onlinevaluation-conflict_relation", function () {
           console.log($(this).val());
           if($(this).val() == "Yes"){
            $("#iuser_conflict_relation_type").show();
            $("#iuser_conflict_relation_name").show();
                   }else{
                    $("#onlinevaluation-conflict_relation_name").val("");
                    $("#iuser_conflict_relation_type").hide();
                    $("#iuser_conflict_relation_name").hide();
                   }
       
    });
    
    $(\'#check_same_client\').change(function() {
        if ($(this).is(\':checked\')) {
            // Checkbox is checked
           $("#onlinevaluation-client_prefix").val($("#onlinevaluation-client_customer_prefix").val());
           $("#onlinevaluation-client_fname").val($("#onlinevaluation-client_customer_fname").val());
           $("#onlinevaluation-client_lname").val($("#onlinevaluation-client_customer_lname").val());
           $("#onlinevaluation-client_phone_code_id").val($("#idonlinevaluation-client_customer_phone_code_id").val());
           $("#onlinevaluation-client_phone").val($("#onlinevaluation-client_customer_phone").val());
           $("#onlinevaluation-client_landline_code_id").val($("#onlinevaluation-client_customer_landline_code_id").val());
           $("#onlinevaluation-client_landline").val($("#onlinevaluation-client_customer_landline").val());
           $("#onlinevaluation-client_email").val($("#onlinevaluation-client_customer_email").val());
           $("#onlinevaluation-client_company").val($("#onlinevaluation-client_customer_company").val());
           $("#onlinevaluation-client_email").val($("#onlinevaluation-client_customer_email").val());
           $("#onlinevaluation-client_prefix").trigger("change");
        } else {
            $("#onlinevaluation-client_prefix").val("Mr.");
           $("#onlinevaluation-client_fname").val("");
           $("#onlinevaluation-client_lname").val("");
           $("#onlinevaluation-client_phone_code_id").val("050");
           $("#onlinevaluation-client_phone").val("");
           $("#onlinevaluation-client_landline_code_id").val("04");
           $("#onlinevaluation-client_landline").val("");
           $("#onlinevaluation-client_email").val("");
           $("#onlinevaluation-client_company").val("");
           $("#onlinevaluation-client_email").val("");
            $("#onlinevaluation-client_prefix").trigger("change");
        }
    });

');
$image_row = 0;

?>


<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }

    .phone-code {
        width: 74px !important;
        border-radius: 4px !important;
    }

    .phone-number {
        border-radius: 4px !important;
    }

    .phone .phone-parent {
        display: flex;
    }

    .phone .input-group-prepend {
        flex: 1;

    }

    .phone .form-group {
        flex: 5;

    }

    .help-block2 {
        color: #a94442;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -6px;
        font-family: inherit;
    }

    .card {
        display: flex;
        margin-left: 20px;
        margin-right: 20px;
    }

    .navbar {
        margin-bottom: 0;
    }

    .navbar-nav > li > a {
        padding-top: 8px;
    }

    .online-valuation .has-success .control-label {
        color: inherit;
    }

    .online-valuation .has-success .form-control {
        border: 1px solid #ccc;
    }

    .online-valuation .has-success.select2-container--krajee-bs3 .select2-dropdown, .online-valuation .has-success .select2-container--krajee-bs3 .select2-selection {
        border: 1px solid #ccc;
    }

    .bootstrap-datetimepicker-widget.dropdown-menu {
        width: 76%;
    }

    .bootstrap-datetimepicker-widget table td.active, .bootstrap-datetimepicker-widget table td.active:hover {
        background-color: #007bff !important;
    }

    .content-header h1 {
        font-size: 1.8rem;
        margin: 0;

        font-weight: 400;
        line-height: 1.5;
        color: #212529;
    }

    .card-primary.card-outline {
        border-top: 3px solid #003366;
    }

    .heading_right {
        padding-top: 20px;
        float: right;
        text-align: right;
        padding-right: 35px;
    }

    body {
        font-family: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css"
      integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg=="
      crossorigin="anonymous" referrerpolicy="no-referrer"/>
<section class="sales-and-marketing-purpose-form card card-outline card-primary online-valuation">
    <?php $form = ActiveForm::begin();
    $key = '';
    for ($i = 0; $i < 10; $i++) {
        $key .= rand(0, 9);
    }

    ?>
    <?php echo $form->field($model, 'key')->hiddenInput(['maxlength' => true, 'value' => ($model->key <> null) ? $model->key : $key])->label(false) ?>
    <?php echo $form->field($model, 'client_reference')->hiddenInput(['maxlength' => true, 'value' => (Yii::$app->appHelperFunctions->getUniqueOnlineValuationReference())])->label(false) ?>


    <div class="container" style="display: contents;">
        <div class="card mb-4 shadow-lg rounded" style="margin-top: 20px;">
            <header class="card-header" style="background-color: #F1582E; color: #ffffff;">
                <h2 class="card-title ">General Details</h2>
            </header>
            <div class="card-body">
                <div class="row g-3">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'inspection_type')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->inspectionTypeArr,
                            'options' => ['placeholder' => 'Select Inspection Type'],
                            'pluginOptions' => ['allowClear' => true],
                        ]); ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'urgency')->widget(Select2::classname(), [
                            'data' => ['0' => 'Normal - 2 working days', '1' => 'Urgent - 1 working day'],
                            'pluginOptions' => ['allowClear' => true],
                        ])->label('Priority Type'); ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'purpose_of_valuation')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->purposeOfValuationArr,
                            'options' => ['placeholder' => 'Select Purpose of Valuation'],
                            'pluginOptions' => ['allowClear' => true],
                        ]); ?>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 valuation_date" id="valuation_date_id">
                        <?php

                        $Timestamp = strtotime(date('Y-m-d'));
                        $TotalTimeStamp = strtotime('+ 2 days', $Timestamp);
                        $default_date = date('d-M-Y', $TotalTimeStamp);

                        $valuation_date = ($model->valuation_date <> null) ? date('d-M-Y', strtotime($model->valuation_date)) : $default_date ?>
                        <?= $form->field($model, 'valuation_date', [
                            'template' => '
                    {label}
                    <div class="input-group date" style="display: flex" id="onlinevaluation_valuation_date" data-target-input="nearest">
                    {input}
                    <div class="input-group-append" data-target="#onlinevaluation_valuation_date" data-toggle="datetimepicker">
                        <div class="input-group-text" style="background-color: #F1582E; color: #ffffff;"><i class="fa fa-calendar"></i></div>
                    </div>
                    </div>
                    {hint}{error}
                    '
                        ])->textInput(['maxlength' => true, 'value' => $valuation_date]) ?>
                    </div>


                </div>
            </div>
        </div>

        <div class="card mb-4 shadow-lg rounded">
            <header class="card-header" style="background-color: #F1582E; color: #ffffff;">
                <h2 class="card-title">Property Address Details</h2>
            </header>
            <div class="card-body">
                <div class="row g-3">


                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?php
                        $emirates_array = Yii::$app->appHelperFunctions->emiratedListArr;

                        $keysToUnset = ['148893', '2459', '102858', '2461', '102851', '85521', '102864', '102874'];

                        // Loop through keys to unset
                        foreach ($keysToUnset as $key) {
                            if (isset($emirates_array[$key])) {
                                unset($emirates_array[$key]);
                            }
                        }


                        ?>
                        <?= $form->field($model, 'city_id')->dropDownList($emirates_array, ['prompt' => Yii::t('app', 'Select Emirate')])->label('Select Emirate') ?>
                    </div>


                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'community')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Communities::find()->where(['city' => 3510, 'trashed' => 0])->andWhere(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select Community'],
                            'pluginOptions' => ['allowClear' => true],
                        ])->label('Community') ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'sub_community')->widget(Select2::classname(), [
                            'data' => ($model->sub_community ? [$model->sub_community => $model->subCommunities->title] : []),
                            'options' => ['placeholder' => 'Select Sub Community'],
                            'pluginOptions' => ['allowClear' => true],
                        ])->label('Sub Community'); ?>
                    </div>


                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'building_info')->widget(Select2::classname(), [
                            'data' => ($model->building_info ? [$model->building_info => $model->building->title] : []),
                            'options' => ['placeholder' => 'Select Building'],
                            'pluginOptions' => ['allowClear' => true],
                        ])->label('Building/Project Name') ?>
                    </div>

                        <?= $form->field($model, 'city')->hiddenInput(['maxlength' => true, 'readonly' => true, 'placeholder' => 'Emirate'])->label(false) ?>


                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'plot_number')->textInput(['maxlength' => true, 'placeholder' => 'Enter Plot Number']) ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true, 'placeholder' => 'Enter Unit Number']) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mb-4 shadow-lg rounded">
            <header class="card-header" style="background-color: #F1582E; color: #ffffff;">
                <h2 class="card-title">Property Details</h2>
            </header>
            <div class="card-body">
                <div class="row g-3">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'property_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select Property Type'],
                            'pluginOptions' => ['allowClear' => true],
                        ])->label('Property Type') ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'property_category')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                            'pluginOptions' => ['allowClear' => true],
                        ]); ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'tenure')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                            'pluginOptions' => ['allowClear' => true],
                        ]); ?>
                    </div>
                </div>
                <div class="row g-3">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true, 'placeholder' => 'Enter Built Up Area', 'type' => 'number', 'step' => 'any', 'min' => 0])->label('Built Up Area') ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'net_leasable_area')->textInput(['maxlength' => true, 'placeholder' => 'Enter Net Leasable Area', 'type' => 'number', 'step' => 'any', 'min' => 0])->label('Net Leasable Area') ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'plot_area')->textInput(['maxlength' => true, 'placeholder' => 'Enter Plot Area', 'type' => 'number', 'step' => 'any', 'min' => 0])->label('Plot Area') ?>
                    </div>
                </div>
                <div class="row g-3">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'development_type')->widget(Select2::classname(), [
                            'data' => ['Standard' => 'Standard / By Developer', 'Upgraded' => 'Upgraded'],
                            'options' => ['placeholder' => 'Select Development Type', 'value' => 'Standard'],
                            'pluginOptions' => ['allowClear' => true, 'initialize' => true],
                        ]); ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'extended')->widget(Select2::classname(), [
                            'data' => ['No' => 'No', 'Yes' => 'Yes'],
                            'pluginOptions' => ['allowClear' => true, 'initialize' => true],
                        ]); ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'completion_status')->widget(Select2::classname(), [
                            'data' => ['1' => 'Ready', '2' => 'Under Construction'],
                            'pluginOptions' => ['allowClear' => true, 'initialize' => true],
                        ]); ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 number_of_units">
                        <?php
                        echo $form->field($model, 'number_of_units')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->getCrmOptionZeroToNumber(1000),
                            'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mb-4 shadow-lg rounded">
            <header class="card-header" style="background-color: #F1582E; color: #ffffff;">
                <h2 class="card-title">Instructing Person Details</h2>
            </header>
            <div class="card-body">
                <div class="row g-3">

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?php
                        echo $form->field($model, 'client_customer_prefix')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->smHelper->getPrefixArr(),
                            'options' => ['placeholder' => 'Select...', 'value' => "Mr."],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'initialize' => true,
                            ],
                        ])->label('Prefix');
                        ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'client_customer_fname')->textInput(['maxlength' => true, 'placeholder' => 'Enter first name'])->label('First Name'); ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'client_customer_lname')->textInput(['maxlength' => true, 'placeholder' => 'Enter last name'])->label('Last Name'); ?>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 phone">
                        <label class="control-label" for="onlinevaluation-client_customer_phone">Client Customer Phone
                            <span class="text-danger"></span></label>
                        <div class="input-group phone-parent">
                            <div class="input-group-prepend">
                                <select name='OnlineValuation[client_customer_phone_code]'
                                        id="client_customer_phone_code_id" class="form-control phone-code">
                                    <option value="050" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '050')) {
                                        echo 'selected';
                                    } ?>>050
                                    </option>
                                    <option value="052" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '052')) {
                                        echo 'selected';
                                    } ?>>052
                                    </option>
                                    <option value="054" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '054')) {
                                        echo 'selected';
                                    } ?>>054
                                    </option>
                                    <option value="055" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '055')) {
                                        echo 'selected';
                                    } ?>>055
                                    </option>
                                    <option value="056" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '056')) {
                                        echo 'selected';
                                    } ?>>056
                                    </option>
                                    <option value="058" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '058')) {
                                        echo 'selected';
                                    } ?>>058
                                    </option>
                                </select>
                            </div>
                            <?= $form->field($model, 'client_customer_phone')->textInput(['maxlength' => true, 'class' => 'form-control phone-number', 'placeholder' => 'Enter phone number'])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 phone">
                        <label class="control-label" for="onlinevaluation-client_customer_landline">Client Customer
                            Landline <span class="text-danger"> </span></label>
                        <div class="input-group phone-parent">
                            <div class="input-group-prepend">
                                <select name="OnlineValuation[client_customer_landline_code]"
                                        id="client_customer_landline_code_id" class="form-control phone-code">
                                    <option value="04" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '04')) {
                                        echo 'selected';
                                    } ?>>04
                                    </option>
                                    <option value="02" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '02')) {
                                        echo 'selected';
                                    } ?>>02
                                    </option>
                                    <option value="03" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '03')) {
                                        echo 'selected';
                                    } ?>>03
                                    </option>
                                    <option value="06" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '06')) {
                                        echo 'selected';
                                    } ?>>06
                                    </option>
                                    <option value="07" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '07')) {
                                        echo 'selected';
                                    } ?>>07
                                    </option>
                                    <option value="09" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '09')) {
                                        echo 'selected';
                                    } ?>>09
                                    </option>
                                </select>
                            </div>
                            <?= $form->field($model, 'client_customer_landline')->textInput(['maxlength' => true, 'class' => 'form-control phone-number', 'placeholder' => 'Enter landline number'])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 ">
                        <?= $form->field($model, 'client_customer_email')->textInput(['maxlength' => true, 'placeholder' => 'Enter email'])->label('Email'); ?>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'client_customer_company')->textInput(['maxlength' => true, 'placeholder' => 'Enter Company Name'])->label('Company Name'); ?>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'client_customer_position')->textInput(['maxlength' => true, 'placeholder' => 'Enter Position'])->label('Position'); ?>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?php
                        echo $form->field($model, 'client_customer_authority')->widget(\kartik\select2\Select2::classname(), [
                            'data' => ['Yes' => 'Yes', 'No' => 'No'],

                            'pluginOptions' => [
                            ],
                        ])->label('Authority');
                        ?>
                    </div>


                </div>
            </div>
        </div>


        <div class="card mb-4 shadow-lg rounded">
            <header class="card-header" style="background-color: #F1582E; color: #ffffff;">
                <h2 class="card-title">Client Details</h2>
                <div class="form-check" style="float: right;">
                    <input type="checkbox" class="form-check-input" id="check_same_client" value="1"><label
                            class="form-check-label" for="check1">&nbsp;&nbsp;&nbsp;&nbsp;Same As Instructing
                        person?</label>
                </div>
            </header>
            <div class="card-body">
                <div class="row g-3">

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?php
                        echo $form->field($model, 'client_prefix')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->smHelper->getPrefixArr(),
                            'options' => ['placeholder' => 'Select...', 'value' => "Mr."],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'initialize' => true,
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'client_fname')->textInput(['maxlength' => true, 'placeholder' => 'Enter first name']); ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'client_lname')->textInput(['maxlength' => true, 'placeholder' => 'Enter last name']); ?>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 phone">
                        <label class="control-label" for="onlinevaluation-client_phone">Client Customer Phone <span
                                    class="text-danger"></span></label>
                        <div class="input-group phone-parent">
                            <div class="input-group-prepend">
                                <select name='onlinevaluation[client_phone_code]' id="client_phone_code_id"
                                        class="form-control phone-code">
                                    <option value="050" <?php if (isset($model->client_phone_code) && ($model->client_customer_phone_code == '050')) {
                                        echo 'selected';
                                    } ?>>050
                                    </option>
                                    <option value="052" <?php if (isset($model->client_phone_code) && ($model->client_customer_phone_code == '052')) {
                                        echo 'selected';
                                    } ?>>052
                                    </option>
                                    <option value="054" <?php if (isset($model->client_phone_code) && ($model->client_customer_phone_code == '054')) {
                                        echo 'selected';
                                    } ?>>054
                                    </option>
                                    <option value="055" <?php if (isset($model->client_phone_code) && ($model->client_customer_phone_code == '055')) {
                                        echo 'selected';
                                    } ?>>055
                                    </option>
                                    <option value="056" <?php if (isset($model->client_phone_code) && ($model->client_customer_phone_code == '056')) {
                                        echo 'selected';
                                    } ?>>056
                                    </option>
                                    <option value="058" <?php if (isset($model->client_phone_code) && ($model->client_customer_phone_code == '058')) {
                                        echo 'selected';
                                    } ?>>058
                                    </option>
                                </select>
                            </div>
                            <?= $form->field($model, 'client_phone')->textInput(['maxlength' => true, 'class' => 'form-control phone-number', 'placeholder' => 'Enter phone number'])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 phone">
                        <label class="control-label" for="onlinevaluation-client_landline">Client Customer Landline
                            <span class="text-danger"> </span></label>
                        <div class="input-group phone-parent">
                            <div class="input-group-prepend">
                                <select name="onlinevaluation[client_landline_code]" id="client_landline_code_id"
                                        class="form-control phone-code">
                                    <option value="04" <?php if (isset($model->client_landline_code) && ($model->client_landline_code == '04')) {
                                        echo 'selected';
                                    } ?>>04
                                    <option value="02" <?php if (isset($model->client_landline_code) && ($model->client_landline_code == '02')) {
                                        echo 'selected';
                                    } ?>>02
                                    </option>
                                    <option value="03" <?php if (isset($model->client_landline_code) && ($model->client_landline_code == '03')) {
                                        echo 'selected';
                                    } ?>>03
                                    </option>
                                    <option value="06" <?php if (isset($model->client_landline_code) && ($model->client_landline_code == '06')) {
                                        echo 'selected';
                                    } ?>>06
                                    </option>
                                    <option value="07" <?php if (isset($model->client_landline_code) && ($model->client_landline_code == '07')) {
                                        echo 'selected';
                                    } ?>>07
                                    </option>
                                    <option value="09" <?php if (isset($model->client_landline_code) && ($model->client_landline_code == '09')) {
                                        echo 'selected';
                                    } ?>>09
                                    </option>
                                </select>
                            </div>
                            <?= $form->field($model, 'client_landline')->textInput(['maxlength' => true, 'class' => 'form-control phone-number', 'placeholder' => 'Enter landline number'])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 ">
                        <?= $form->field($model, 'client_email')->textInput(['maxlength' => true, 'placeholder' => 'Enter email']); ?>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'client_company')->textInput(['maxlength' => true, 'placeholder' => 'Enter Company Name']); ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?= $form->field($model, 'client_type')->dropDownList(yii::$app->quotationHelperFunctions->clienttype) ?>
                    </div>

                </div>
            </div>
        </div>


        <div class="card mb-4 shadow-lg rounded">
            <header class="card-header" style="background-color: #F1582E; color: #ffffff;">
                <h2 class="card-title">Conflict Of Interest</h2>
            </header>
            <div class="card-body">
                <div class="row g-3">

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?php
                        echo $form->field($model, 'conflict_relation')->widget(Select2::classname(), [
                            'data' => array('No' => 'No', 'Yes' => 'Yes'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Do you know anyone in Windmills ');
                        ?>
                    </div>
                    <?php
                    if ($model->conflict_relation == 'Yes') {
                        $style = "block";
                    } else {
                        $style = "none";
                    }


                    ?>

                    <div class="col-lg-4 col-md-6 col-sm-12 conflict_relation_type" id="iuser_conflict_relation_type"
                         style="display: <?= $style ?>">
                        <?php
                        echo $form->field($model, 'conflict_relation_type')->widget(Select2::classname(), [
                            'data' => array('Relative?' => 'Relative', 'Friend' => 'Friend'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Typy Of Relation ');
                        ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 conflict_relation_name" id="iuser_conflict_relation_name"
                         style="display: <?= $style ?>">
                        <?= $form->field($model, 'conflict_relation_name')->textInput(['maxlength' => true])->label('Enter Name'); ?>
                        <div class="help-block2"></div>
                    </div>


                </div>
            </div>
        </div>

        <div class="card mb-4 shadow-lg rounded">
            <header class="card-header" style="background-color: #F1582E; color: #ffffff;">
                <h2 class="card-title">Documents Details</h2>
            </header>
            <div class="card-body">
                <?php $unit_row = 0; ?>
                <div class="row g-3" id="property-documents-container">


                </div>
            </div>
        </div>
        <div class="card mb-4 shadow-lg rounded">

            <div class="card-body">
                <?= $form->field($model, 'terms')->checkbox([
                    'label' => 'I agree to the <a href="#" id="open-terms" data-toggle="modal" data-target="#myModal">Terms And Conditions</a>.',
                    'encode' => false,
                ]) ?>
            </div>
        </div>

        <!-- <div class="card mb-4 shadow-lg rounded">
             <header class="card-header" style="background-color: #F1582E; color: #ffffff;">
                 <h2 class="card-title">Payment Details</h2>
             </header>
             <div class="card-body">


             </div>
         </div>-->


    </div>
    <div class="card-footer text-right" style="background-color: #f0f0f0;">
        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-arrow-circle-right"></i> Proceed To Payment '), ['class' => 'btn', 'style' => 'background-color: #F1582E; color: #ffffff;']) ?>

        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-secondary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>


<script type="text/javascript">
    var unit_row = <?= $unit_row ?>;


    var uploadAttachment = function (attachmentId) {


        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function (json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);


                            console.log(json.file.href);
                            var str1 = json['file'].href;
                            var str2 = ".pdf";
                            var str3 = ".doc";
                            var str4 = ".docx";
                            var str5 = ".xlsx";
                            var str6 = ".xls";
                            if (str1.indexOf(str2) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                            } else if (str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                            } else {
                                $('#upload-document' + attachmentId + ' img').prop('src', json['file'].cache_path);

                            }
                            $('#upload-document-view' + attachmentId).prop('href', json['file'].re);
                            $('#input-attachment' + attachmentId).val(json['file'].href);


                            /*  $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                             $('#input-attachment' + attachmentId).val(json['file'].href);*/
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }

</script>

<?php

$this->registerJs('


    var atch_count = "' . $unit_row . '";

    $("body").on("click", ".add-km-image", function (e) {
        
        call_url = "' . \yii\helpers\Url::to(['suggestion/getclientvalimagehtml']) . '";
        $.ajax({
            url: call_url,
            data: {atch_count:atch_count},
            dataType: "html",
            success: function(data) {
                data = JSON.parse(data);
                console.log(data)
                $("#km-table").append(data.col);
                atch_count++;                    
            },
            error: bbAlert
        });
    });


    
    $("body").on("click", ".open-img-window", function (e) {
        target_id = $(this).attr("data-uploadid");
        uploadAttachment(target_id)
    });



    
    // window.addEventListener("load", initialize);
 
$(\'#open-terms\').on(\'click\', function (e) {
 e.preventDefault();
     
        
        var cl_fname = $("#onlinevaluation-client_customer_fname").val();
        var cl_lname = $("#onlinevaluation-client_customer_lname").val();
        var sp_city = $("#onlinevaluation-city").val();
        var sp_building_name = $("#onlinevaluation-building_info option:selected").text();
        var sp_community = $("#onlinevaluation-community option:selected").text();
        var sp_purpose = $("#onlinevaluation-purpose_of_valuation option:selected").text();
        var sp_plot_number = $("#onlinevaluation-plot_number").val();
      
        $("#purpose_val").html("The purpose of the subject valuation is "+sp_purpose);
        
           // var numberOfUnits = $(this).val();
         
           var buildingId = $("#onlinevaluation-building_info").val(); 
           var client_type = $("#onlinevaluation-client_type").val(); 
            var inspection_type = $("#onlinevaluation-inspection_type").val();
            var tenure = $("#onlinevaluation-tenure").val();
            var propertyId = $("#onlinevaluation-property_id").val(); 
            var urgency = $("#onlinevaluation-urgency").val();
            var property_category = $("#onlinevaluation-property_category").val();
            var built_up_area = $("#onlinevaluation-built_up_area").val();
            var net_leasable_area = $("#onlinevaluation-net_leasable_area").val();
            var number_of_units = $("#onlinevaluation-number_of_units").val();
            var plot_area = $("#onlinevaluation-plot_area").val();
      
         
        
        
        
         $.ajax({
        
            url: "' . Yii::$app->urlManager->createUrl('online-valuation/online-valuation-fee') . '",
            method: "POST",
            data: {propertyId: propertyId, buildingId: buildingId, client_type: client_type, inspection_type: inspection_type, tenure: tenure, urgency: urgency,property_category:property_category,built_up_area:built_up_area,net_leasable_area:net_leasable_area,number_of_units:number_of_units,plot_area:plot_area}, 
            success: function(response) {
                 $("#fee_text").html(response);
            }
        });
        
        
        
        
    
        $("#client_name_html").html(cl_fname+" "+cl_lname);
        $("#sp_purpose_html").html("The purpose of the subject valuation is "+sp_purpose);
        $("#subject_property").html("Plot#"+sp_plot_number+", "+sp_building_name+", "+sp_community+", "+sp_city);
       // e.preventDefault(); // Prevent default link behavior
        $(\'#myModal\').modal(\'show\'); // Show the modal
    });
    
        
');
$purposeOfValuation = Yii::$app->appHelperFunctions->getPurpose();
$standardReport = Yii::$app->quotationHelperFunctions->standardReport();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
    table {

    }

    td.detailheading {
        color: #0D47A1;
        font-weight: normal;
        font-size: 11px;
        /* font-weight:bold; */
        /* border-bottom: 1px solid #64B5F6; */
    }

    td.toeheading {
        color: #4A148C;
        font-size: 11px;
        font-weight: bold;
        /* font-weight: normal; */
        border-top: 1px solid #4A148C;
        border-bottom: 1px solid #4A148C;
        border-left: 1px solid #4A148C;
        border-right: 1px solid #4A148C;
        text-align: center;
    }

    td.onlymainheading {
        color: #4A148C;
        font-size: 11px;
        /* font-weight:bold; */
        font-weight: normal;
        border-bottom: 1px solid #000000;
    }

    td.detailtext {
        /* background-color:#E8F5E9; */
        font-size: 11px;
        text-align: justify;
    }

    span.spantag {
        /* background-color:#E8F5E9; */
        font-size: 11px;
        text-align: justify;
        font-weight: normal;
        color: black;
    }

    td.sixpoints {
        font-size: 11px;
        color: #0D47A1;
    }

    tr.color {
        background-color: #ECEFF1;
    }

    td.subject {
        font-weight: bold;
        border-bottom: 1px solid #4A148C;
    }

    td.fontsizeten {
        font-size: 11px;
    }

    span.fontsize12 {
        /* background-color:#E8F5E9; */
        font-size: 14px;
    }

    span.b-number {
        color: black;
        font-weight: bold;
        font-size: 11px;
    }
</style>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">

                <style>
                    table {

                    }

                    td.detailheading {
                        color: #0D47A1;
                        font-weight: normal;
                        font-size: 11px;
                        /* font-weight:bold; */
                        /* border-bottom: 1px solid #64B5F6; */
                    }

                    td.toeheading {
                        color: #4A148C;
                        font-size: 11px;
                        font-weight: bold;
                        /* font-weight: normal; */
                        /* border-top: 1px solid #4A148C;*/
                        border-bottom: 1px solid #4A148C;
                        /*border-left: 1px solid #4A148C;*/
                        /*border-right: 1px solid #4A148C;*/
                        text-align: center;
                    }

                    td.onlymainheading {
                        color: #4A148C;
                        font-size: 11px;
                        /* font-weight:bold; */
                        font-weight: normal;
                        border-bottom: 1px solid #000000;
                    }

                    td.detailtext {
                        /* background-color:#E8F5E9; */
                        font-size: 11px;
                        text-align: justify;
                    }

                    span.spantag {
                        /* background-color:#E8F5E9; */
                        font-size: 11px;
                        text-align: justify;
                        font-weight: normal;
                        color: black;
                    }

                    td.sixpoints {
                        font-size: 11px;
                        color: #0D47A1;
                    }

                    tr.color {
                        background-color: #ECEFF1;
                    }

                    td.subject {
                        font-weight: bold;
                        border-bottom: 1px solid #4A148C;
                    }

                    td.fontsizeten {
                        font-size: 11px;
                    }

                    span.fontsize12 {
                        /* background-color:#E8F5E9; */
                        font-size: 14px;
                    }

                    span.b-number {
                        color: black;
                        font-weight: bold;
                        font-size: 11px;
                    }
                </style>

                <table>
                    <tr>
                        <td class="firstpage fontsizeten">December 9, 2024</td>
                    </tr>
                    <br>


                    <tr>
                        <td class="firstpage fontsizeten"></td>
                    </tr>


                    <tr>
                        <td class="firstpage fontsizeten">Dear Sir/Madam,</td>
                    </tr>
                    <br>

                    <tr>
                        <td class="detailheading fontsizeten subject" colspan="2"><h4>Terms of Engagement for Valuation
                                Services</h4>
                        </td>
                    </tr>
                    <br>
                    <tr>
                        <td style="text-align:justify;" class="firstpage fontsizeten"><p><span
                                        style="font-weight: 400;">Good day.</span></p>
                            <p><span style="font-weight: 400;">Windmills is among the top rated valuation firms ranked by RERA and Dubai Land Department, achieving a gold rating from the said authorities for 3 consecutive years. We value real estate, industrial, and business assets as per the local and international valuation standards. Windmills is empanelled with most of the banks in UAE, and have valued for hundreds of corporate clients including government authorities, family groups, insurance companies and many other significant public and private companies.</span>
                            </p>
                            <p><span style="font-weight: 400;">We have valued more than 40,000 assets with market value of higher than AED 150 billion, for over 1,000 clients, across all Emirates of the UAE, GCC and internationally. Our team has more than 50 years of aggregate valuation experience in total in the region, consisting of MRICS qualified members, civil engineers and finance specialists.&nbsp;</span>
                            </p>
                            <p><span style="font-weight: 400;">We have extensive technical experience and appropriate capacity to complete this assignment. Our 4 senior valuation executives are registered members of RICS and are ranked 5-star valuers by DLD and RERA. All our services and reports are compliant with International and Emirates Valuation Standards. Furthermore, we have a quality assurance policy and standard operating procedures to ensure an objective and quality conclusion of assignments for your good office.</span>
                            </p>
                            <p><span style="font-weight: 400;">Our </span><strong>competitive propositions</strong><span
                                        style="font-weight: 400;"> are scientific assessment, transparent disclosure to the clients, fast turnaround time, affordable fees and extensive market research. We have an in house robust technological platform called Maxima, which facilitates a scientific assessment process. It characterizes each and every part and factor of the project by way of its weightage to overall costs and ratings for quality that also culminates into precise valuation.</span>
                            </p>
                            <p><span style="font-weight: 400;">Please see our corporate profile or visit </span><a
                                        href="http://www.windmillsgroup.com/"><span style="font-weight: 400;">www.windmillsgroup.com</span>
                                </a><span style="font-weight: 400;">for knowing more about us. If you have any questions, you may contact us anytime.</span>
                            </p>
                            <p><span style="font-weight: 400;">We look forward to receiving your kind approval and valuing for you.&nbsp;</span>
                            </p></td>
                    </tr>

                    <tr>
                        <td class="firstpage fontsizeten"><br/><br/>
                            Thank you very much.<br><br>Sincerely,<br/><br/>
                            <img src="/images/ahmet_sig.jpeg" style="height: 70px; ">

                            <br>
                            Ahmet Ozgur<br>Head of Valuations<br>

                        </td>

                    </tr>

                </table>
                <br>
                <br>


                <table class="main-class">
                    <tr>
                        <td class="detailheading onlymainheading" colspan="2"><h3>Terms of Engagement</h3></td>
                    </tr>
                </table>
                <br><br>


                <table>
                    <tr>
                        <td class="detailheading onlymainheading" colspan="2"><h4>1. Client and Intended Users</h4></td>
                    </tr>

                </table>


                <table>

                    <tr><br>
                        <td class="detailheading"><h4>1.1. Client</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext" id="client_name_html"></td>
                    </tr>
                    <tr>
                        <td class="detailtext"></td>
                    </tr>
                </table>
                <br/><br/>

                <table>

                    <tr>
                        <td class="detailheading"><h4>1.2. Service Provider</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"> Windmills Real Estate Valuation Services LLC</td>
                    </tr>
                </table>
                <br/><br/>

                <table>
                    <br>
                    <tr>
                        <td class="detailheading"><h4>1.3. Other Intended Users:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext">None.</td>
                    </tr>
                </table>
                <br><br>


                <table>
                    <tr>
                        <td class="detailheading onlymainheading"><h4>2. Subject Properties to be Valued (Scope of
                                Service)</h4></td>
                    </tr>
                </table>
                <br>

                <table>
                    <br>
                    <tr>
                        <br>
                        <td class="detailheading"><h4>2.1. Scope of Service</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext">
                            <br>Estimate Market Value
                        </td>
                    </tr>
                </table>
                <br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>2.2. Subject Property</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext" id="subject_property"><h4>2.2. Subject Property</h4></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>2.3 Purpose of Valuation:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext" id="purpose_val">The purpose of the subject valuation
                            is
                        </td>
                    </tr>
                </table>


                <table>
                    <tr>
                        <td class="detailheading onlymainheading"><h4>3. Documentary and information Requirements:</h4>
                        </td>
                    </tr>
                    <tr><br>
                        <td class="detailtext">For the purpose of carrying out this valuation, please provide the
                            following documents
                            related to the subject property:
                        </td>
                    </tr>


                </table>
                <br><br>


                <table>
                    <tr>
                        <td class="detailheading onlymainheading"><h4>4. Business Terms and Conditions</h4></td>
                    </tr>
                </table>
                <table>
                    <!-- VatInTotalValuationFee -->
                    <tr><br>
                        <td class="detailheading"><h4>4.1. Fee</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext">
                            Total Fee = including VAT will therefore be:<br>
                            AED <span id="fee_text">0.00</span>/=<br>
                            ( Dirhams Only)
                        </td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>4.2. Payment Terms</h4></td>
                    </tr>
                    <tr>
                        <!-- <td class="detailtext">The fee is payable <? /*= $mainTableData['advance_payment_terms'] */ ?> in advance (Payment
            Structure). The fee is non-refundable
        </td>-->
                        <td class="detailtext">The fee is payable in advance (Payment
                            Structure). The fee is non-refundable under any circumstances.
                        </td>


                    </tr>
                </table>
                <br><br>


                <table>
                    <tr>
                        <td colspan="2" class="detailheading"><h4>4.3. Payment Method</h4></td>
                        <!-- <td class="detailheading"></td> -->
                    </tr>
                    <tr>
                        <td colspan="2" class="detailtext"><p>Online</p></td>
                        <!-- <td class="detailtext"></td> -->
                    </tr>

                </table>
                <br><br>


                <table>
                    <tr>
                        <td class="detailheading"><h4>4.4. Timings</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext">The delivery time for this assignment will be 2 Working Days
                            from and after the:
                            <ul>
                                <li style="text-align: left;"><span style="font-weight: 400;">Terms of Engagement is approved and signed by you on all pages,&nbsp;</span>
                                </li>
                                <li style="font-weight: 400; text-align: left;" aria-level="1"><span
                                            style="font-weight: 400;">Date of inspection of the subject property is done,</span>
                                </li>
                                <li style="font-weight: 400; text-align: left;" aria-level="1"><span
                                            style="font-weight: 400;">Date the payment is received by us in our account, and,</span>
                                </li>
                                <li style="font-weight: 400; text-align: left;" aria-level="1"><span
                                            style="font-weight: 400;">Date on which all required information and documents are received by us, whichever is later (please refer to Section 3 above).</span>
                                </li>
                            </ul>
                            <p>&nbsp;</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>4.5. RICS Valuation Standards and Departures</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>The Valuation and Report will be prepared in accordance with the
                                Standards (and RICS Valuation &ndash; Global Standards effective 31 January 2022. In
                                accordance with your instructions in preparing our valuation report we will depart from
                                the mandatory requirements of Red Book in the following regards: None.</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>4.6. Valuation Approaches</h4></td>
                    </tr>

                </table>
                <br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>4.7. Special Assumptions</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading onlymainheading"><h4>5. Identification and Status of the Valuer</h4>
                        </td>
                    </tr>
                </table>

                <table>
                    <tr>

                        <td class="detailtext sixpoints" style="width: 30%"><h4>5.1 Valuer Name</h4></td>
                        <td class="detailtext">Ahmet Ozgur<br>(referred to in this document as “Valuer”).
                        </td>
                    </tr>

                    <tr>
                        <br>
                        <td class="detailtext sixpoints"><h4>5.2. Valuer Firm</h4></td>
                        <td class="detailtext"><br>(referred to in this document as “Firm”).
                        </td>
                    </tr>

                    <tr>
                        <br>
                        <td class="detailtext sixpoints"><h4>5.3. Valuer Qualifications</h4></td>
                        <td class="detailtext">Member of Royal Institution of Chartered Surveyors.
                            BA in International Trade.
                            MSc. of Financial Economics/ Quantitative Finance.
                        </td>
                    </tr>

                    <tr>
                        <br>
                        <td class="detailtext sixpoints"><h4>5.4. Valuer Status</h4></td>
                        <td class="detailtext">The Valuer is registered with RICS and RERA. The Valuer is in a position
                            to provide an objective and unbiased valuation, and is competent to undertake the valuation
                            assignment.
                        </td>
                    </tr>
                </table>
                <br><br>


                <table>
                    <tr>
                        <td class="detailheading"><h4>5.5. Valuer Experience and Expertise</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext">He has more than 19 years of experience in real estate valuation and
                            consultancy business. He involved many comprehensive studies for leading international
                            companies in EMEA region including Turkey, Bosnia and Herzegovina, Georgia, Slovenia and
                            Syria. Throughout his career, he has been involved in numerous valuation and consulting
                            activities including portfolio and single asset
                            valuations, highest and best use analysis, feasibility studies, market analysis and
                            discounted cash flow analysis. His areas of specialty include Office, Retail, Hospitality,
                            Industrial and Residential Projects.
                        </td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>5.6. Valuer Duties and Supervision</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The Valuer may be assisted under this subject valuation by his experienced team members, including but not limited to the tasks of interacting with clients, obtaining/processing property documents, property inspection, market comparison and analysis, preparing valuation and writing the draft report.</span>
                            </p>
                            <p><span style="font-weight: 400;">The Valuer will be involved to fulfill his obligations as per these terms of engagement. He will supervise the process of valuation to the best of his capabilities and capacity, which includes understanding client instructions, checking conflict of interest, understanding subject property, application of valuation approach, supervision of site inspection (if valuation is instructed to be based on physical inspection), checking market data and valuation analysis, approving market value, and signing valuation report.</span>
                            </p>
                            <p><span style="font-weight: 400;">He will also be reasonably available to the client for any material inquiry about the valuation that fits into the scope of the valuation instruction and these terms of engagement, and can be formally communicated with by email. </span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>5.7. Internal/External Status of the Valuer</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>The Valuer will be acting as an external and independent valuer in
                                this valuation.</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>5.8. Previous involvement and Conflict of Interest</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>The Firm and the Valuer does not have any material connection or
                                involvement or conflict of interest associated with the subject property(ies) / asset(s)
                                or the parties related to this valuation assignment.</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>5.9. Report Handover</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>If the client requires us to share the valuation report with his/her
                                representative, the client will have to provide the written permission instructing us to
                                do so.</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading onlymainheading"><h4>6. Valuation Terms </h4></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <br>
                    <tr>
                        <td class="detailheading"><h4>6.1. Currency:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>United Arab Emirates Dirhams (AED).</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <br>
                    <tr>
                        <td class="detailheading"><h4>6.2. Basis of Value:</h4></td>
                    </tr>

                    <tr>
                        <td class="detailtext"><p>The basis of value that the valuer will adopt in the subject valuation
                                is the Market Value.</p>
                            <p>For information purposes, the RICS Red Book recognizes four bases of value:</p>
                            <ol>
                                <li>Market Value</li>
                                <li>Market Rent</li>
                                <li>Investment Value (worth)</li>
                                <li>Fair Value</li>
                            </ol>
                            <p>The following definitions of Basis of Value will be used. They are derived from the
                                International Valuation Standards.</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>6.3. Market Value:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>&ldquo;The estimated amount for which an asset or liability should
                                exchange on the valuation date between a willing buyer and a willing seller in an arm&rsquo;s
                                length transaction after proper marketing and where the parties had each acted
                                knowledgeably, prudently and without compulsion.&rdquo;</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>6.4. Statutory definition of Market Value (capital gains tax,
                                inheritance tax and
                                stamp duty land tax).</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>&ldquo;Summary definition derived from legislation: 'The price which
                                the property might reasonably be expected to fetch if sold in the open market at that
                                time, but that price shall not be assumed to be reduced on the grounds that the whole
                                property is to be placed on the market at one and the same time.' (Source: section 272
                                Taxation and Chargeable Gains Act 1992. Section 160 Inheritance Tax Act 1984, Section
                                118 Finance Act 2003).&rdquo;</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>6.5. Market Rent:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>&ldquo;The estimated amount for which an interest in real property
                                should be leased on the valuation date between a willing lessor and a willing lessee on
                                appropriate lease terms in an arm&rsquo;s length transaction, after proper marketing and
                                where the parties had each acted knowledgeably, prudently and without compulsion. Our
                                assumptions of what are &lsquo;appropriate lease terms&rsquo; for this property will be
                                set out in our report.&rdquo;</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>6.6. Investment Value (Worth)</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>&ldquo;The value of an asset to the owner or a prospective owner for
                                individual investment or operational objectives (see IVS 104 paragraph 60.1). (May also
                                be known as worth.)&rdquo;</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>6.7. Fair Value</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>&ldquo;The price that would be received to sell an asset, or paid to
                                transfer a liability, in an orderly transaction between market participants at the
                                measurement date.&rsquo; (This definition derives from International Financial Reporting
                                Standards IFRS 13.)&rdquo;</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>6.8. Valuation Date</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>The valuation date is specific and is agreed to be the date of the
                                inspection.</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>6.9. Modified Properties</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>If the subject property has been structurally modified through for
                                example, partitions, fit out, extensions, which make it significantly different from the
                                standard property originally, then the client agrees that a new Terms of Engagement will
                                be drafted. Additional documents will be required such as, NOC/permission from
                                developer, property management company, local building authority, and the final
                                completion certificate.</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading onlymainheading"><h4>7. General Assumptions</h4></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <br>
                    <tr>
                        <td class="detailheading"><h4>7.1 Assumptions, Extent of Investigations, Limitations on the
                                Scope of Work:</h4>
                        </td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>Considering that the Firm is to provide a valuation report within a
                                reasonable timeframe and at an economic cost agreed, the following limitations in the
                                scope of inspections and due diligence in enquiries are agreed together with the
                                necessary assumptions which will be adopted to cover uncertainties:</p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>7.2 Physical Inspection</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">For an inspection-based valuation, the inspector may inspect each and every area of the subject property, photograph them, and measure them. Client agrees to arrange for providing full cooperation in all possible ways in this regard. Where valuations have to be made on restricted inspection, the nature of the restrictions will be addressed in the valuation report. In the event that the inspector is not able to complete his inspection tasks, the Firm and the Valuer assumes no responsibility or liability arising out of this non-cooperation and its impact on our valuation.</span>
                            </p>
                            <p><span style="font-weight: 400;">Inspections and investigations will be carried out to the extent reasonable, accessible, permitted by the owner, tenant and/or the client&rsquo;s representative, and necessary to produce a valuation, which adequately meets the purpose of the valuation. The Firm and the Valuer will neither inspect those parts of the property which are covered, unexposed or inaccessible, nor will the Valuer raise floorboards, remove building fabric or parts, move any fixed apparatus or arrange for a test of the electrical, heating or other services.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>7.3. Desktop or Drive by Valuation</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">We recommend the Client to consider instructing us and arranging for a physical inspection-based valuation. The physical inspection makes our understanding of the property satisfactory. Similarly as per RICS standards, valuation companies must inspect the property that is being valued for the first time by the company.&nbsp;</span>
                            </p>
                            <p><span style="font-weight: 400;">Desktop valuation may be acceptable by Local and International Valuation Standards conditionally during revaluation. </span><span
                                        style="font-weight: 400;">In case the Client instructs us to carry out a desktop or drive by valuation, the Client agrees to the following:&nbsp;</span>
                            </p>
                            <ul>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">It is indeed not possible to inspect the property due to an unavoidable reason.</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">The property is in a good and normal state and condition legally, structurally, and technically.</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">There is no material change/difference in the property legally, structurally, technically compared to standard and similar properties in the neighborhood and the same subject property since it was last inspected by us.</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">The valuation report is for the client&rsquo;s internal management purposes and will not be shared with third parties or published.</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Since it is desktop or a drive by valuation, the Valuer will have a very limited insight and capacity in the subject property to make a valuation estimate related to desktop or drive by valuations. The Firm and the Valuer assumes no responsibility or liability with regard to the results of the limited insight into the property due to desktop and/or drive-by instructions, as the desktop valuation is carried out based purely on the instructions of the Client.&nbsp;</span>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>7.4 Structural and Technical Survey</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">Since we are not the experts/professionals on structural and technical developments of the subject property, the Firm and the Valuer will not carry out a building structural and technical survey of any sort (i.e., including but not limited to the air- conditioning, civil, mechanical, electrical or plumbing). We advise the Client to take experts/professional opinions in these regards.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>7.5 Conditions and State of Repair and Maintenance of the
                                Property</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">Our valuation will assume that except for any material defects notified to us by the Client or generally observed during our inspection (or not if it is agreed to be a desktop valuation) and noted in our report, the property and all the fitted installations, machinery and equipment therein are in a good condition and state of repair and maintenance overall.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>7.6 Contamination, and Ground and Environmental
                                Considerations</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">Since we are not environmental experts/professionals, the Firm and the Valuer will not carry out an environment survey of any sort in or around the property or in the neighborhood. We recommend that the Client secures independent and professional advice about the environmental risk and conditions associated with the property. Our valuation will assume that the property is free from all and any risks associated with the factors including but not limited to the Deleterious or Radon Gas or Hazardous Material or Asbestos, Adverse Ground Conditions. Pollution and Contamination, Flooding, Mining, Fire, Medical Conditions/Diseases, Fire, Disabled access, Coastal Erosion, Brine Extraction, Incapacity of Development or Redevelopment and any other matters, which may affect the market value in this valuation.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>7.7 Statutory and regulatory requirements</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">Since we are not statutory and regulatory experts/professionals, the Firm and the Valuer will not carry out any compliance related survey of any sort in or around the property. We recommend that the Client secures independent and professional advice about the Statutory and regulatory requirements. Our valuation will assume that all relevant statutory and regulatory requirements relating to the property have been complied with.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>7.8 Title, Tenancies and Property Documents:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The Client agrees to provide or arrange for providing all necessary and valid documents, data and information timely, correctly and completely.</span>
                            </p>
                            <p><span style="font-weight: 400;">Since we are not the legal experts/professionals, the Firm and the Valuer will not carry out any survey or verification of the documents related to the property including but not limited to title documents and tenancy documents. We will also not check or seek to verify details about any liabilities, finance charges or mortgages associated with the property. We recommend that the Client secures independent and professional advice about the Title, Tenancies and Property Documents.</span>
                            </p>
                            <p><span style="font-weight: 400;">We assume that all the documents, data and information provided about the subject property being valued by the Client are correct, complete, legally enforceable/valid and reliable. We will not verify the authenticity and validity of the property documents provided by the Client and/or 3</span><span
                                        style="font-weight: 400;">rd</span><span style="font-weight: 400;"> party engaged by the Client. The Firm recommends that the Client seeks an independent opinion about the authenticity/validity of the property documents from the local real estate authorities and/or suitably qualified lawyers/experts.</span>
                            </p>
                            <p><br/><span style="font-weight: 400;">In case the required documents and information (mentioned above in Section 3) are not arranged or provided by the client and/or 3</span><span
                                        style="font-weight: 400;">rd</span><span style="font-weight: 400;"> party engaged by the Client timely, and/or they are not complete, correct, and/or valid, the Firm and the Valuer carry no responsibility or liability associated with the documents and information provided and the ensuring results in the valuation report completed under these terms of engagement. In such cases, the Firm and the Valuer however may assume the information based on his best judgment possible and will assume no liability or responsibility for doing so. </span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>7.9 Planning and Highway Enquiries:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">We will assume in this valuation that the subject property meets the local planning requirements. We may make informal enquiries of the local planning and highway authorities and also rely on information that is publicly published or available free of charge. Any published information obtained will be assumed to be correct. No local searches will be instigated. Except where stated to the contrary, we shall further assume that there are no local authority planning or highway proposals that might involve the use of compulsory purchase powers or otherwise directly affect the property. As the Firm andFirm and the Valuer, we are not legal and/or land planning experts/professionals, it is recommended that the Client secures separate professional advice as to the subject property&rsquo;s compliance in all such matters.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>7.10 Plant and Equipment:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">We will include in our valuations those items of the equipment that are normally considered to be a fitted, integral and/or immovable part of the service installations to a building and which would normally pass with the property on a sale or letting. We will exclude all items of process equipment, together with their special foundations and supports, furniture and furnishings, vehicles, stock and loose tools, and tenants&rsquo; fixtures and fittings. We will not check and verify the condition and quality of such plants and equipment considered to be a part of service installation. We recommended that the Client secures independent and professional advice about the plant, machinery and equipment. As we are not technical experts/professionals on the plant, machinery and equipment, we will assume that they are in good and working condition. Unless otherwise specified, no allowance is made for the cost of repairing any damage caused by the removal from the premises of items of plant, machinery, fixtures and fittings.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>7.11. Development Properties:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">For properties in the course of development, we may reflect and report the stage reached in construction and the costs remaining to be spent on the date of valuation, as per our own opinion. Since we are not development, contracting and/or quantity surveying experts/professionals, our such opinion must be considered for guidance only. The client Is recommended to take an independent professional/expert&rsquo;s opinion on the development of the property and the exact and specific stage thereat. We have regard to the contractual liabilities of the parties involved in the development and any cost estimates that have been prepared by the professional advisers to the project. For recently completed developments, we will take no account of any retention, nor will we make allowance for any outstanding development costs, fees, or other expenditure for which there may be a liability.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>
                    <tr>
                        <td class="detailheading"><h4>7.12. Insurance Re-Instatement Cost (If applicable):</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">Our opinion is a broad estimate of reinstatement costs for insurance purposes, and should be used as guidance only for an actual insured amount. When given, our advice is normally subject to the following (unless stated otherwise):</span>
                            </p>
                            <ul>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Buildings are in their present form.&nbsp;</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Buildings under construction are completed.</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Includes cost of site clearance and professional fees but excludes:&nbsp;</span>
                                </li>
                                <ul>
                                    <li style="font-weight: 400;" aria-level="2"><span style="font-weight: 400;">Loss of rent/business profits.</span>
                                    </li>
                                    <li style="font-weight: 400;" aria-level="2"><span style="font-weight: 400;">Cost of alternative accommodation for the reinstatement period&nbsp;</span>
                                    </li>
                                    <li style="font-weight: 400;" aria-level="2"><span style="font-weight: 400;">Cost of decontamination of the land</span>
                                    </li>
                                </ul>
                            </ul>
                            <p><span style="font-weight: 400;">In the event any of the above-mentioned assumptions prove to be incorrect or inappropriate, we reserve a right to review and revise our valuations without any responsibility and liability.</span>
                            </p></td>
                    </tr>
                </table>


                <table>

                    <tr>
                        <br>
                        <td class="detailheading onlymainheading"><h4>8. Nature and Sources of Information and Documents
                                to be Relied
                                Upon</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The Client agrees to provide or arrange for providing all necessary and valid documents, data, and information timely, correctly and completely. We will also apply our professional judgment on the documents, data and information provided.</span>
                            </p>
                            <p><br/><span style="font-weight: 400;">The Firm will rely on the following source of information:</span>
                            </p>
                            <ul>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Documents provided by the Client (as stated in the section 3 above). Inspection observations and measurements.</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Dubai Land Department Data about the subject property from the DLD website (applicable for Subject Property in Dubai only).</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Dubai REST mobile application (applicable for Subject Property in Dubai only).</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Sold Transactions from REIDIN database, that uses Dubai Land Department Data (applicable for Subject Property in Dubai only).</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Listings from Sales and Rent from online portals like Property Finder, Bayut, Dubizzle etc.&nbsp;</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Similar Valuations completed by us in the past.</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Discussion with various Real Estate Broker Agencies.</span>
                                </li>
                                <li style="font-weight: 400;" aria-level="1"><span style="font-weight: 400;">Any other public and private source that is deemed appropriate and relevant.</span>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>
                        <td class="detailheading onlymainheading"><h4>9. Description of Report:</h4></td>
                    </tr>
                    <tr>

                        <td class="detailtext"><p>When valuing a property, we will comply with the following minimum
                                contents requirement of RICS Valuation &ndash; Global Standards VPS 3.2:</p>
                            <ul>
                                <li>Identification and status of the valuer</li>
                                <li>Identification of the client and any other intended users</li>
                                <li>Purpose of the valuation</li>
                                <li>Identification of the asset(s) or liability(ies) valued</li>
                                <li>Basis(es) of value adopted</li>
                                <li>Valuation date</li>
                                <li>Extent of investigation</li>
                                <li>Nature and source(s) of the information relied upon</li>
                                <li>Assumptions and special assumptions</li>
                                <li>Restrictions on use, distribution and publication of the report</li>
                                <li>Confirmation that the valuation has been undertaken in accordance with the IVS</li>
                                <li>Valuation approach and reasoning</li>
                                <li>Amount of the valuation or valuations</li>
                                <li>Date of the valuation report</li>
                                <li>Commentary on any material uncertainty in relation to the valuation where it is
                                    essential to ensure clarity on the part of the valuation user
                                </li>
                                <li>A statement setting out any limitations on liability that have been agreed.</li>
                            </ul>
                            <p><br/>The report will be made available in the soft copy in PDF version by email and one
                                original hard copy print out version to the Client.<br/><br/>If the client request to
                                change the counter parties in the report (i.e. intended user of the report), there will
                                be an administrative charge applicable of 25% of the total valuation fee.</p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>
                        <td class="detailheading onlymainheading"><h4>10. Client Acceptance of the Valuation Report</h4>
                        </td>
                    </tr>
                    <tr>

                        <td class="detailtext"><p><span style="font-weight: 400;">The client agrees to review our draft valuation report within 5 working days of receipt of our valuation report. The client is welcome to ask any question and/or share any feedback about the subject valuation formally (by email or letter) within the said period. In case there are no questions or comments, or feedback raised to the Valuer and/or the Firm in the said manner within the said time period, the client agrees that our draft report will be deemed accepted by the Client. Thereafter, the final signed valuation report will then be submitted to you by the Valuer, and the subject valuation will be considered closed.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>
                        <td class="detailheading onlymainheading"><h4>11. Restrictions on Publications</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>The valuation report will be strictly confidential between the
                                addressed party(ies) and the Valuer. We strongly recommend not to share this valuation
                                report with any other party and to maintain confidentiality. No part of the report or
                                the whole report may be copied, extracted from, re-produced, published and/or
                                distributed and/or without our prior consent from the Valuer in writing. Where the
                                purpose of the report requires a published reference to it, the valuer must agree to
                                provide a draft statement for inclusion in the publication before its publication.</p>
                        </td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>12. Third party liability</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The Firm or Valuer or any of its employees assume no responsibility or liability to the Client or any third party, who may breach confidentiality of the report and use or rely upon the whole or any part of the contents of this report without a formal consent from the undersigned Valuer. Where the Client is a lender - In the event of placement of loans secured on the subject asset with a syndicate, The Firm and the Valuer shall have no liability whatsoever to such 3rd parties in the syndicate, unless a written permission is obtained from the Valuer.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>13. Valuation Uncertainty</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">Valuation is a professional opinion, and not a fact. The opinion should be read in conjunction with the contents of the whole valuation report in entirety including the assumptions made.</span>
                            </p>
                            <p><span style="font-weight: 400;">Our market value estimate of the Property in this report represents our best professional opinion/judgment, based on the relevant data provided by the client, owner and various other sources used, and considering the overall market conditions/trends at the date of valuation.</span>
                            </p>
                            <p><span style="font-weight: 400;">We would like to highlight that there are certain direct and indirect existing and potential risks related to the property, which may not be reflected/controlled in the valuation report, like possible political, economic and social changes/developments and/or force majeure events. The risks may also be related to the changes in the sector with regards to the processes, technological advancement, level of availability and transparency of the market (and the real estate sector data), changes in the government regulations, volatility of real estate market (including changes in demand, supply), misleading information received from various sources deployed, and fee tariff of the sector, amongst others.</span>
                            </p>
                            <p><span style="font-weight: 400;">It is beyond the scope of our valuation services to ensure the consistency and all possible events/scenarios/volatility/risks inclusions in values due to changing circumstances. Accordingly, we advise you that the uncertainty must be considered by the Client when any financial decision is made based on this valuation report. We also suggest that adequate protective arrangements (including insurance cover) are made. We also recommend the Client to arrange for frequent valuations of the Property to monitor its value marked to market.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>14. Complaints:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">In the event that a complaint should arise, we confirm that we have a Complaints Handling Procedure, a copy of which will be provided to the Client on request. In the event that a complaint is not successfully dealt with internally, redress is offered via our preferred Alternative Dispute Resolution (ADR) body. The number of arbitrators shall be one. The seat, or legal place, of arbitration shall be UAE. The language and governing law to be used in the arbitration shall be English, and the RERA laws in force.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>15. RICS Monitoring</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The Valuer is registered with RICS. The valuation report may be subject to monitoring under the conduct and disciplinary regulations of the Royal Institution of Chartered Surveyors and as part of routine monitoring Valuer Registration programs.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>16. RERA Monitoring</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The Valuer is registered with RERA. The Valuation report may be subject to monitoring under the conduct and disciplinary regulations of the Real Estate Regulatory Agency of Dubai and as part of routine monitoring under RERA Regulated Firm and Valuer Registration programs.</span>
                            </p>
                            <p><span style="font-weight: 400;">The Client agrees that as per the regulatory reporting requirements enforced by RERA today or in the future, The Firm is permitted to provide such required data, information and reports pertaining to all relevant valuations carried out by us which may include this subject valuation, without any responsibility or liability on the Firm&rsquo;s part.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>17. Limitations on liability</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The amount of any claim associated with this property valuation is limited to an amount that does not exceed the fees received by the Firm from the Client for the valuation.</span>
                            </p>
                            <p><span style="font-weight: 400;">There shall be no personal liability whatsoever of the individual valuers or the Manager or the Director or Shareholding Partners of the Firm involved in the assignment in any situation or claim whatsoever.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>18. Liability & Duty of Care:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The Firm owes to the Client a duty to act with reasonable skill and care in providing the service and complying with the client&rsquo;s instructions where those instructions do not conflict with these terms or applicable laws, international valuation standards and/or local valuation standards.</span>
                            </p>
                            <p><span style="font-weight: 400;">We assume no responsibility liability for the consequences, including delay in or failure to provide the services (including delays as a result of &lsquo;Force Majeure&rsquo; or any situation beyond the Valuer&rsquo;s and the Firm&rsquo;s control, or any failure by the Client or any agent of the Client to promptly to provide information or other material reasonably requested, or where that material is inaccurate or incomplete, or to follow our advice or recommendations.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>19. Extent of Fee </h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The Firm would like to confirm that the valuation service fee received from the subject valuation under the terms of engagement is within a range of five percentage points of the total income of the Firm.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>20. Indemnification:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The Client will indemnify the Firm and keep them fully and effectively indemnified at all times against any action against them or any costs or expenses incurred, or any loss suffered by them if a claim arises from or is delayed as a result of dishonesty, fraud, willful concealment by the Client.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>21. Professional Indemnity Insurance:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">We confirm that the Firm holds professional indemnity insurance in respect of the service to be provided. In any event, the Firm will have no liability whatsoever unless proceedings are carried out within two months of the cause of action arising.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>22. Client’s Obligations:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The Client agrees to pay the fees for the valuation service carried out in accordance with the terms and conditions set out in these terms of engagement. &nbsp; The client agrees that the Firm will make an independent opinion on the market value, and the Client will pay the valuation fee to the Firm irrespective of the amount of market value estimated by the Firm in the valuation report.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>23. Validity of the Terms of Engagement:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">The terms of engagement are valid for 10 working days from the date of this proposal. After the said period expires without the client accepting and signing these terms of engagement, these terms of engagement are no longer valid, and does not bind the Valuer and the Valuation Firm for any liability or responsibility related to these terms of engagement and valuation in any way. The firm is not responsible or liable to engage in or even commence the valuation of the subject property under this agreement until and unless the accepted, signed and stamped terms of engagement are received by the Firm.</span>
                            </p>
                            <p><span style="font-weight: 400;">After the terms of engagement has been accepted and signed by the client, the client has 5 working days to pay the fees for the valuation service from the date of the approval (i.e., date of signature and/or stamp) by the client. After the said period expires without the Firm receiving the payment from the client, these terms of engagement are no longer valid and does not bind the Valuer or the Firm for any liability or responsibility related to these terms of engagement and valuation in any way. The firm is not responsible or liable to engage in or even commence the valuation of the subject property under this agreement until and unless the payment is received by the Firm.</span>
                            </p>
                            <p><span style="font-weight: 400;">The Firm would like to receive necessary documents and/or information required from the client in a good time in order to make the valuation process efficient and professional. After the proposal has been accepted and signed by the client and the payment has been made, the client has 5 working days from the date of the approval (i.e., date of signature and/or stamp) to provide all necessary documents and/or information related to the subject property required by the Firm. After the said period is expired without the Firm receiving the required documents and information from the client, in the interest of time and efficiency, the Firm will have to make necessary assumptions about the subject property at its own discretion professionally, and proceed with the valuation completion without further delay and without any liability or responsibility on the part of the Firm arising from the non-availability of the documents and/or information.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>

                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>24. Jurisdiction:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p>The terms of this Agreement shall be governed by, and construed in
                                accordance with, the local laws and the federal laws of the United Arab Emirates and the
                                Parties hereby irrevocably agree that the courts of Dubai, United Arab Emirates shall
                                have jurisdiction to hear and determine any suit, action or proceeding, and to settle
                                any disputes, which may arise out of or in connection with this Agreement and, for such
                                purpose, irrevocably submit to the jurisdiction of Dubai courts.</p></td>
                    </tr>
                </table>
                <br><br>


                <table>

                    <tr>

                        <td class="detailheading onlymainheading"><h4>25. Acceptance of Terms of Engagement:</h4></td>
                    </tr>
                    <tr>
                        <td class="detailtext"><p><span style="font-weight: 400;">We accept these terms of engagement as an accurate summary of the instructions to provide a valuation.</span>
                            </p></td>
                    </tr>
                </table>
                <br><br>


                <style>
                    td.centre {
                        text-align: left;
                        /*vertical-align: middle;*/
                        font-size: 11px;
                    }
                </style>

                <table>
                    <tr>

                        <td colspan="5" class="centre">Signed (Firm)</td>
                        <!-- <td colspan="1" class="centre"></td> -->
                        <td colspan="1" class="centre"></td>
                        <td colspan="4" class="centre">Signed (Client)</td>


                    </tr>
                    <tr>


                        <td colspan="5" class="centre detailtext">
                            <img src="/images/ahmet_sig.jpeg" style="height: 100px; ">
                            <br>Ahmet Ozgur<br>Head of Valuations
                        </td>


                        <!-- <td colspan="1" class="centre"></td> -->
                        <td colspan="1" class="centre"></td>
                        <td colspan="4" class="centre"></td>


                    </tr>
                    <br>


                    <tr>


                        <td colspan="5" class="centre">For and on behalf of
                            <br><br></td>
                        <!-- <td colspan="1" class="centre"></td> -->
                        <td colspan="1" class="centre"></td>
                        <td colspan="4" class="centre">For and on behalf of
                            <br><br></td>

                    </tr>
                    <br><br>

                    <!-- <tr>
                         <td colspan="4" class="centre">Windmills Real Estate Valuation Services LLC</td>
                         <td colspan="1" class="centre"></td>
                         <td colspan="1" class="centre"></td>

                         <td colspan="4" class="centre"></td>

                     </tr>-->
                </table>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>