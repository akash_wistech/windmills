<?php

use app\models\Valuation;

$netValuationFee = $rateAndItemAmount;

if ($model->client->vat == 1) {
  $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
}

$finalFeePayable = $netValuationFee + $VAT;

$clientAddress = \app\models\Company::find()->select(['address'])->where(['id' => $model->client_id])->one();

?>

<br><br>

<style>
  .box1 {
    background-color: #E3F2FD;
    width: 90px;
    /*margin-top: -20px*/
    border-bottom: 0.5px solid #1E88E5;
  }

  .box2 {
    background-color: #1E88E5;
    width: 90px;
    border-bottom: 0.5px solid #1E88E5;
  }

  .box3 {
    background-color: #E3F2FD;
    width: 90px;
    border-bottom: 0.5px solid #1E88E5;
    /* margin-right: 100px; */
  }

  .airal-family {
    font-family: Arial, Helvetica, sans-serif;
  }

  .text-dec {
    font-size: 10px;
    text-align: center;
  }

  /*td.detailtext{*/
  /*background-color:#BDBDBD; */
  /*font-size:9px;*/
  /*border-top: 0.4px solid grey;*/
  /*}*/

  td.detailtexttext {
    /* background-color:#EEEEEE; */
    font-size: 11px;
  }

  th.detailtext {
    /* background-color:#BDBDBD; */
    font-size: 9px;
    border-top: 1px solid black;
  }

  /*.border {
  border: 1px solid black;
  }*/

  th.amount-heading {
    color: #039BE5;
  }

  td.amount-heading {
    color: #039BE5;
  }

  th.total-due {
    font-size: 16px;
  }

  td.total-due {
    font-size: 16px;
  }

  span.size-8 {
    font-size: 10px;
    font-weight: bold;
  }

  td.first-section {
    padding-left: 10px;
    /* background-color: black; */
  }

</style>

<hr style="height: 1.2px;">
<table>
  <thead>
    <tr><br>
      <td colspan="2" class="detailtexttext first-section" style="">
        <br><span class="size-8 airal-family">ADDRESS</span>
        <?php
        if ($model->id == 1896 || $model->id == 1945) { ?>
          <br><span>AL HAMAD BLDG CONT. CO. L.L.C</span>
        <?php } else { ?>
          <br><span><?= $model->client->title ?></span>
        <?php } ?>
        <?php if ($model->id == 1896 || $model->id == 1945) { ?>

        <?php } else { ?>
          <br><span><?= $clientAddress->address ?></span>
        <?php } ?>

        <?php
        if ($model->client->id != 9167) { ?>
          <br><span>UAE.</span>
        <?php
        }
        ?>
        <?php if ($model->id == 1896 || $model->id == 1945) { ?>
          <br><span>TRN: 100049814500003</span>
        <?php
        }
        ?>
      </td>
      <td></td>
      <td class="box1 text-dec airal-family">
        <p style="color: #29B6F6">
          DATE<br>
          <?= date('d/m/Y') ?>
          <br>
        </p>
      </td>
      <td class="box2 text-dec airal-family">
        <p style="color: white;">
          TOTAL<br>
          <b>AED <?= number_format($finalFeePayable, 2) ?></b>
        </p>
      </td>

      <td class="box3 text-dec airal-family" style="margin-right:10px;">
        <p>

        </p>
      </td>
    </tr>
  </thead>
</table><br><br>

<table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <hr style="height: 0.2px;">
    <tr style="">
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align:left; border-bottom: 0.2px solid black;">DATE</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; width:150px; border-bottom: 0.2px solid black;">DESCRIPTION</td>
      <td class="airal-family" style="width: 100px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">TAX</td>
      <td class="airal-family" style="width: 50px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">QTY</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">RATE</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align:right; border-bottom: 0.2px solid black;">AMOUNT</td>
    </tr>
  </thead>
  <tbody>
    <tr style="">
      <td class="airal-family" style="font-size:9px;"><?= date('d M Y', strtotime($model->created_at)); ?></td>
      <td class="airal-family" style="font-size:9px; width:150px;"><?= $model->building->title; ?></td>
      <td class="airal-family" style="width: 100px; font-size:9px; text-align: center;">SR Standard Rated (DXB)</td>
      <td class="airal-family" style="width: 50px; font-size:9px; text-align: center;">1</td>
      <td class="airal-family" style="font-size:9px; text-align: center;"><?= number_format($rateAndItemAmount, 2) ?></td>
      <td class="airal-family" style="font-size:9px; text-align:right;"><?= number_format($rateAndItemAmount, 2) ?></td>
    </tr>
    <tr>
      <td></td>
      <td class="airal-family" style="font-size: 9px; width:180px;"></td>
    </tr>
  </tbody>
</table>
<hr style="height: 0.3px;">

<table cellspacing="1" cellpadding="2">
  <tr>
    <td></td>
    <th colspan="2" class="airal-family" style="font-size:9px;  color: #039BE5;">Net Fee</th>
    <td class="airal-family" style="text-align:right; font-size:9px;"><?= number_format($rateAndItemAmount, 2) ?></td>
  </tr>
  <tr>
    <td></td>
    <?php if ($model->client->vat == 1) { ?>
      <th colspan="2" class="airal-family" style="font-size:9px;  color: #039BE5;">VAT TOTAL</th>
      <td class="airal-family" style="text-align:right; font-size:9px;"><?= number_format($VAT, 2) ?></td>
    <?php } else { ?>
      <th class="airal-family" style="font-size:9px;  color: #039BE5;">No VAT</th>
      <td class="airal-family" style="text-align:right; font-size:9px;">0</td>
    <?php } ?>
  </tr>
  <tr>
    <td></td>
    <th colspan="2" class="airal-family" style="font-size:10px; border-top: 0.3px solid grey; border-bottom: 0.3px solid #64B5F6; color: #039BE5; padding-bottom: 20px;">TOTAL</th>
    <td class="airal-family" style="text-align:right; border-top: 0.3px solid grey; border-bottom: 0.3px solid #64B5F6; font-size:16px; color: #039BE5;">AED <?= number_format($finalFeePayable) ?></td>
  </tr>
  <tr>
    <td colspan="3"></td>
    <td class="airal-family" style="text-align:right; font-size:10px; color: #039BE5;">THANK YOU.</td>
  </tr>
</table>

<br><br>
<table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <tr style="">
      <td class="airal-family" style="font-size:10px; text-align:left;">Accepted By</td>
      <td class="airal-family" style="width: 90px; font-size:10px; text-align:left;">Accepted Date</td>
    </tr>
  </thead>
</table>



