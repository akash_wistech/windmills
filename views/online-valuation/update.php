<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientValuation */

$this->title = 'Update Valuation Instruction: '.$model->client_reference ;
// $this->params['breadcrumbs'][] = ['label' => 'Valuation Instruction', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sales-and-marketing-purpose-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
