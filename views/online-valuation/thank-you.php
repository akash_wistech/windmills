<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ValuationFormAsset;
use app\components\widgets\StatusVerified;



?>



<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }



    .phone-code {
        width:74px !important;
        border-radius: 4px !important;
    }
    .phone-number {
        border-radius: 4px !important;
    }
    .phone .phone-parent { display: flex;}
    .phone .input-group-prepend {
        flex: 1;

    }
    .phone .form-group  {
        flex: 5;

    }
    .help-block2 {
        color: #a94442;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -6px;
        font-family: inherit;
    }
    .card{
        display: flex;
        margin-left: 20px;
        margin-right: 20px;
    }
    .navbar { margin-bottom: 0;}
    .navbar-nav > li > a { padding-top: 8px; }
    .online-valuation .has-success .control-label{ color: inherit;}
    .online-valuation .has-success .form-control { border: 1px solid #ccc;}
    .online-valuation .has-success.select2-container--krajee-bs3 .select2-dropdown, .online-valuation .has-success .select2-container--krajee-bs3 .select2-selection{ border: 1px solid #ccc;}
    .bootstrap-datetimepicker-widget.dropdown-menu { width: 76%;}
    .bootstrap-datetimepicker-widget table td.active, .bootstrap-datetimepicker-widget table td.active:hover {background-color: #007bff !important;}
    .content-header h1 {
        font-size: 1.8rem;
        margin: 0;

        font-weight: 400;
        line-height: 1.5;
        color: #212529;
    }
    .card-primary.card-outline {
        border-top: 3px solid #003366;
    }
    .heading_right{
        padding-top: 20px;
        float: right;
        text-align: right;
        padding-right: 35px;
    }
    body {font-family: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css"
      integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg=="
      crossorigin="anonymous" referrerpolicy="no-referrer"/>
<section class="sales-and-marketing-purpose-form card card-outline card-primary online-valuation">


    <div class="container"  style="display: contents;">
        <div class="alert alert-success">
            <h3 class="text-center">Thank You</h3>
            <p>Dear Candidate,<br><br>
                Good day. We sincerely hope that you and your colleagues are doing well.
                <br>
                Thank you very much for giving this opportunity to value for you.
                <br> <br>
                We have received your valuation instruction(s), and will execute it as soon as possible.
                <br>
                <br>
                Best regards,
                <br>
                <b>Windmills Group</b>
            </p>

        </div>


    </div>

</section>

