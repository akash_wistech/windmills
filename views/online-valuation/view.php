<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ScopeOfService */


$this->title = 'Valuation Instruction: '.$model->client_reference ;
$this->params['breadcrumbs'][] = ['label' => 'Valuation Instruction', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View';

\yii\web\YiiAsset::register($this);
?>
<div class="sales-and-marketing-purpose-update">


    <?= $this->render('_form_locked', [
        'model' => $model,
        'valuation' => $valuation,
    ]) ?>

</div>
