<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use kartik\depdrop\DepDrop;
use app\assets\ValuationFormAsset;
ValuationFormAsset::register($this);


ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Receive Valuation');
$cardTitle = Yii::t('app', 'Receive Valuation:  {nameAttribute}', [
    'nameAttribute' => $model->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_0/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');
$paymentTypes = yii::$app->crmQuotationHelperFunctions->paymentTerms;
?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
</style>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 0,'quotation'=>$quotation, 'showAlert' =>$showAlert]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">


                                <div class="card-header">
                                    <h3 class="card-title">
                                        <i class="fas fa-edit"></i>
                                        <b>General Details</b>
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <?= $form->field($model, 'reference_number')->textInput(['maxlength' => true, 'readonly' => true,
                                            ])
                                            ?>
                                        </div>
                                        <div class="col-4">
                                            <?= $form->field($model, 'client_name')->widget(Select2::classname(), [
                                                'data' =>ArrayHelper::map(\app\models\Company::find()
                                                    ->where(['status' => 1])
                                                    ->andWhere([
                                                        'or',
                                                        ['data_type' => 0],
                                                        ['data_type' => null],
                                                    ])
                                                    ->orderBy(['title' => SORT_ASC,])
                                                    ->all(), 'id', 'title'),
                                                'options' => ['placeholder' => 'Select a Client ...', 'class'=>'client'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>
                                        <div class="col-4">
                                            <?= $form->field($model, 'client_customer_name')->textInput(['maxlength' => true, 'class'=>'form-control client_customer_name'])->label('Instructing Party') ?>
                                        </div>

                                        <div class="col-4">
                                            <?= $form->field($model, 'client_reference')->textInput(['maxlength' => true, 'class'=>'form-control client_reference']) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'other_intended_users')->textInput(['maxlength' => true]) ?>
                                        </div>

                                        <div class="col-4">
                                        <?= $form->field($model, 'scope_of_service')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(\app\models\CrmScopeOfService::find()
                                            ->where(['status' => 1])
                                            ->orderBy(['title' => SORT_ASC,])
                                            ->all(), 'id', 'title'),
                                            'options' => ['placeholder' => 'Select a Assumption...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]); ?>
                                        </div>
                                        <div class="col-4">
                                        <?= $form->field($model, 'assumptions')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(\app\models\CrmAssumptions::find()
                                            ->where(['status' => 1])
                                            ->orderBy(['title' => SORT_ASC,])
                                            ->all(), 'id', 'title'),
                                            'options' => ['placeholder' => 'Select a Assumption...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]); ?>
                                        </div>


                                        <div class="col-4">
                                            <?= $form->field($model, 'advance_payment_terms')->widget(Select2::classname(), [
                                                'data' => $paymentTypes,
                                                'options' => ['placeholder' => 'Select a Payment Term...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>


                                        <div class="col-4">
                                            <?= $form->field($model, 'no_of_properties')->textInput(['maxlength' => true,'type' => 'number']) ?>
                                        </div>

                                    </div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>
