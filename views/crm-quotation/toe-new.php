<?php
use app\models\UserProfileInfo;
$mainTableData = yii::$app->quotationHelperFunctions->getMainTableDataNew($id);
$multipleProperties = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id);
$multipleProperty = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id);

/* echo "<pre>";
 print_r($multipleProperties);
 echo "</pre>";
 die();*/
$ValuerName= \app\models\User::find()
    ->select(['fullname'=>'CONCAT(firstname," ",lastname)'])
    ->where(['id' => $mainTableData['valuer_name']])
    ->asArray()
    ->one();
$clientName = yii::$app->quotationHelperFunctions->getClientName($mainTableData['client_name']);
$purposeOfValuation = Yii::$app->appHelperFunctions->getPurpose();
$standardReport = Yii::$app->quotationHelperFunctions->standardReport();
// $valuerFirstName = Yii::$app->user->identity->firstname;
// $valuerLastName = Yii::$app->user->identity->lastname;
// $valuerName = $valuerFirstName.' '.$valuerLastName;
$valuerQualifications = Yii::$app->user->identity->profileInfo->valuer_qualifications;
$valuerStatus = Yii::$app->user->identity->profileInfo->valuer_status;
$valuerExperienceExpertise = Yii::$app->user->identity->profileInfo->valuer_experience_expertise;


      $bankName=$iban=$trn='';
      $property = Yii::$app->appHelperFunctions->getFirstProperty($id);
      if ($property<>null) {
        $branchDetail = \app\models\Branch::find()->where(['zone_list' => $property->building->city])->one();
        if ($branchDetail<>null) {
            $bankName=$branchDetail->bank_name;
            $iban=$branchDetail->iban_number;
            $trn=$branchDetail->trn_number;
        }
      }


        $special_assumptions = Yii::$app->appHelperFunctions->getAssumptionTitle($mainTableData['assumptions']);
        $scope_of_service = Yii::$app->appHelperFunctions->getScopeOfServiceTitle($mainTableData['scope_of_service']);


?>




<style>
    table {

    }

    td.detailheading{
        color:#0D47A1;
        font-weight: normal;
        font-size:9px;
        /* font-weight:bold; */
        /* border-bottom: 1px solid #64B5F6; */
    }
    td.toeheading{
        color:#4A148C;
        font-size:9px;
        font-weight:bold;
        /* font-weight: normal; */
        border-top: 1px solid #4A148C;
        border-bottom: 1px solid #4A148C;
        border-left: 1px solid #4A148C;
        border-right: 1px solid #4A148C;
        text-align: center;
    }
    td.onlymainheading{
        color:#4A148C;
        font-size:9px;
        /* font-weight:bold; */
        font-weight: normal;
        border-bottom: 1px solid #000000;
    }
    td.detailtext{
        /* background-color:#E8F5E9; */
        font-size:9px;
        text-align:justify;
    }
    span.spantag{
        /* background-color:#E8F5E9; */
        font-size:9px;
        text-align:justify;
        font-weight: normal;
        color: black;
    }
    td.sixpoints{
        font-size:9px;
        color:#0D47A1;
    }
    tr.color{
        background-color:#ECEFF1;
    }
    td.subject{
        font-weight:bold;
        border-bottom: 1px solid #4A148C;
    }
    td.fontsizeten{
        font-size:10px;
    }
    span.fontsize12{
        /* background-color:#E8F5E9; */
        font-size:12px;
    }
    span.b-number{
        color: black;
        font-weight: bold;
        font-size: 9px;
    }
</style>

<table>
    <tr>
        <td class="firstpage fontsizeten"><?= date("F j, Y", strtotime(date("Y-m-d"))); ?></td>
    </tr><br>


    <tr>
        <td class="firstpage fontsizeten"><?= $clientName['title'] ?></td>
    </tr>

    <?php
    if ($clientName['address']!=null) {?>
        <tr>
            <td class="firstpage fontsizeten"><?= $clientName['address'] ?></td>
        </tr><br>
        <?php
        // code...
    }
    ?>



    <tr>
        <td class="firstpage fontsizeten">Dear Sir/Madam,</td>
    </tr><br>

    <tr>
        <td class="detailheading fontsizeten subject" colspan="2"><h4>Terms of Engagement for Valuation Services</h4></td>
    </tr><br>
    <tr>
        <td style="text-align:justify;" class="firstpage fontsizeten"><?= $standardReport['firstpage_content'] ?></td>
    </tr><br>

    <tr>
        <td class="firstpage fontsizeten"><?= $standardReport['firstpage_footer'] ?>
        </td>
    </tr>

</table><br pagebreak="true" />

<table cellspacing="1" cellpadding="8" class="main-class">
    <tr><td class="detailheading toeheading" colspan="2"><h4>Terms of Engagement
                <br>Windmills Real Estate Valuation Services LLC
            </h4></td></tr>
</table><br><br>



<table>
    <tr><td class="detailheading onlymainheading" colspan="2"><h4>1. Client and Intended Users</h4></td></tr>
</table><br>


<table>
    <tr><td class="detailheading"><h4>1.1.	Client</h4></td></tr>
    <tr><td class="detailtext"><?= $clientName['title'] ?></td></tr>
    <tr><td class="detailtext"><?= $clientName['address'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>1.2.	Service Provider</h4></td></tr>
    <tr><td class="detailtext">Windmills Real Estate Valuation Services LLC</td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>1.3. Other Intended Users:</h4></td></tr>
    <tr><td class="detailtext"><?= ($mainTableData['other_intended_users'] <> null)? $mainTableData['other_intended_users']: 'None.'  ?></td></tr>
</table><br><br>


<table>
    <tr><td class="detailheading onlymainheading"><h4>2. Subject Properties to be Valued (Scope of Service)</h4></td></tr>
</table><br>

<table>
    <tr><td class="detailheading"><h4>2.1.	Scope of Service</h4></td></tr>
    <tr><td class="detailtext"><?= $scope_of_service ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>2.2. Subject Property</h4></td></tr>
    <?php
    if ($multipleProperties!=null) {

        $i=1;
        foreach ($multipleProperties as $multipleProperty) {
            $buildingName = yii::$app->quotationHelperFunctions->getBuildingName($multipleProperty['building_info']);
            $building = yii::$app->quotationHelperFunctions->getBuildingData($multipleProperty['building_info']);
            $multipleProperty['city'] = Yii::$app->appHelperFunctions->emiratedListArr[$building->city];
            $multipleProperty['community']= $building->communities->title;
            $multipleProperty['sub_community'] = $building->subCommunities->title;
            if ($mainTableData['no_of_properties']>1) {
                // echo "string";die()
                ?>
                <tr>
                    <td class="detailtext"><span class="b-number">Project No: <?= $i ?></span>
                        <br>Street: <?=$multipleProperty['street'] ?>,
                        <br>Unit no: <?=$multipleProperty['unit_number'] ?>,
                        <br>Plot no: <?=$multipleProperty['plot_number'] ?>,
                        <br>Building Name: <?=$buildingName['title']?>,
                        <br>Sub Community: <?=$multipleProperty['sub_community']?>,
                        <br>Community: <?=$multipleProperty['community']?>,
                        <br>City: <?=$multipleProperty['city']?>
                        <br>The purpose of the subject valuation is <?= Yii::$app->appHelperFunctions->purposeOfValuationArr[$multipleProperty['purpose_of_valuation']] ?>
                    </td>
                </tr><br>
                <?php
            }else{?>
                <tr>
                    <td class="detailtext">Street: <?=$multipleProperty['street'] ?>,
                        <br>Unit no: <?=$multipleProperty['unit_number'] ?>,
                        <br>Plot no: <?=$multipleProperty['plot_number'] ?>,
                        <br>Building Name: <?=$buildingName['title']?>,
                        <br>Sub Community: <?=$multipleProperty['sub_community']?>,
                        <br>Community: <?=$multipleProperty['community']?>,
                        <br>City: <?=$multipleProperty['city']?>
                        <br>The purpose of the subject valuation is <?= Yii::$app->appHelperFunctions->purposeOfValuationArr[$multipleProperty['purpose_of_valuation']] ?>
                    </td>
                </tr>
                <?php
            }
            $i++;
        }
    }
    ?>
</table><br><br>

<!--<table>
    <tr><td class="detailheading"><h4>2.3 Purpose of Valuation:</h4></td></tr>
    <tr><td class="detailtext">The purpose of the subject valuation is <?/*= Yii::$app->appHelperFunctions->purposeOfValuationArr['purpose_of_valuation'] */?></td></tr>
</table><br><br>-->



<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>3. Documentary and information Requirements:</h4></td>
    </tr>
    <tr><td class="detailtext">For the purpose of carrying out this valuation, please provide the following documents related to the subject property:</td></tr>

    <?php

    $k=1;
    $multipleProperties = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id);
    if ($multipleProperties!=null) {
        foreach ($multipleProperties as $multipleDocuments) {

            if($k>1){?>
                <tr><td>&nbsp;</td></tr>
                <?php
            }
            $buildingName = yii::$app->quotationHelperFunctions->getBuildingName($multipleDocuments['building_info']);
            $documents = yii::$app->quotationHelperFunctions->getDcouments($multipleDocuments['property_id']);
            if ($documents['required_documents']!=null) {
                $explodeDocuments = (explode(",",$documents['required_documents']));
                $i=1;?>
                <tr><td class="detailheading">Project Name:-<?=$buildingName['title']?></td></tr>
                <?php foreach ($explodeDocuments as $key => $value) {?>

                    <tr><td class="detailtext"><?= $i?>.<?= Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$value] ?></td></tr>
                    <?php
                    $i++;
                }
                $k++;
            }else{?>
                <tr><td class="detailheading">Project Name:-<?=$buildingName['title']?></td></tr>
                <tr><td class="detailtext">No Documents</td></tr>
                <?php
            }
            ?>


<?php
        }
    }

    ?>


</table><br><br>




<?php
$totalValuationFee = 0;
$totalTurnAroundTime = 0;
/*foreach ($multipleProperties as $value) {
    $totalValuationFee += $value['toe_fee'];
    $totalTurnAroundTime += $value['toe_tat'];
}*/

$totalValuationFee = $mainTableData['toe_final_fee'];
$discount = 0;
if ($mainTableData['relative_discount_toe']!=null) {

    $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($totalValuationFee,$mainTableData['relative_discount_toe']);

}
$totalValuationFee = $totalValuationFee-$discount;
$totalTurnAroundTime = $mainTableData['quotation_turn_around_time'];
?>
<?php
$VatInTotalValuationFee=0;

$totalValuationFeeInWORDS = yii::$app->quotationHelperFunctions->numberTowords($totalValuationFee);
if($model->client->vat==1){
    $VatInTotalValuationFee = yii::$app->quotationHelperFunctions->getVatTotal($totalValuationFee);
}
$totalFee= $totalValuationFee+$VatInTotalValuationFee;
$totalFeeInWords = yii::$app->quotationHelperFunctions->numberTowords($totalFee);

?>
<table>
    <tr><td class="detailheading onlymainheading"><h4>4. Business Terms and Conditions</h4></td></tr>
</table>
<table>
    <tr><td class="detailheading"><h4>4.1.	Fee</h4></td></tr>
    <tr><td class="detailtext">
            Our total valuation fee for the subject properties will be:<br>
            AED <?= number_format($totalValuationFee,2)?>/= <br>
            (<?= $totalValuationFeeInWORDS ?>) <br>
            <?= ($model->client->vat==1 AND $VatInTotalValuationFee>0) ? 'plus 5% VAT' :'plus 0% VAT' ?>. <br><br>
            Total Fee = including VAT will therefore be:<br>
            AED <?= number_format($totalFee).'.00'?>/=<br>
            (<?= $totalFeeInWords ?>)
        </td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>4.2.	Payment Terms</h4></td></tr>
    <tr><td class="detailtext">The fee is payable <?= $mainTableData['advance_payment_terms'] ?> in advance (Payment Structure). The fee is non-refundable</td></tr>
</table><br><br>


<table>
    <tr>
        <td colspan="2" class="detailheading"><h4>4.3.	Payment Method</h4></td>
        <!-- <td class="detailheading"></td> -->
    </tr>
    <tr>
        <td colspan="2" class="detailtext"><?= $standardReport['payment_method'] ?></td>
        <!-- <td class="detailtext"></td> -->
    </tr>
    <tr>
        <td class="detailtext" style="width: 80px;">Bank</td>
        <td class="detailtext"><?= $bankName ?></td>
    </tr>
    <tr>
        <td class="detailtext" style="width: 80px;">IBAN</td>
        <td class="detailtext" ><?= $iban ?></td>
    </tr>
    <tr>
        <td class="detailtext" style="width: 80px;">Our TRN is</td>
        <td class="detailtext" ><?= $trn ?></td>
    </tr>
</table><br><br>


<table>
    <tr><td class="detailheading"><h4>4.4. Timings</h4></td></tr>
    <tr><td class="detailtext">The delivery time for this assignment will be <?= $totalTurnAroundTime ?> Working Days from and after the:<?= $standardReport['timings'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>4.5.	RICS Valuation Standards and Departures</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['rics_valuation_standards_departures'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>4.6.  Valuation Approaches</h4></td></tr>
    <?php
    if ($multipleProperties !=null) {
        $n=1;
        foreach ($multipleProperties as $value) {

            if($value['property_id']==4 || $value['property_id']==39 || $value['property_id']==5 || $value['property_id']==11 || $value['property_id']==23 || $value['property_id']==26){
                ?>

                <tr>
                    <td class="detailtext">Project No: <?=$n?></td>
                </tr>
                <tr>
                    <td class="detailtext"><?= $standardReport['valuation_approaches'] ?></td>
                </tr><br>


                <?php
            }else{?>
                <tr>
                    <td class="detailtext">Project No: <?=$n?></td>
                </tr>

                <tr>
                    <td class="detailtext">
                        <?= Yii::$app->appHelperFunctions->valuationApproachListArr[$value['valuation_approach']]?>
                      <!--  --><?/*= Yii::$app->quotationHelperFunctions->getValuationApproachArr()[$value['valuation_approach']] */?>
                    </td>
                </tr><br>

                <?php
            }
            $n++;
        }
    }
    ?>

</table><br><br>

<table>
    <tr><td class="detailheading"><h4>4.7.	Special Assumptions</h4></td></tr>
    <tr><td class="detailtext"><?= $special_assumptions ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>5. Identification and Status of the Valuer: (from staff member profile)</h4></td>
    </tr>
</table>

<table>
    <tr>
        <td class="detailtext sixpoints">5.1	Valuer Name </td>
        <td class="detailtext"><?= $ValuerName['fullname'] ?>
            <br>(referred to in this document as “Valuer”).
        </td>
    </tr>

    <tr>
        <td class="detailtext sixpoints">5.2.	Valuer Firm</td>
        <td class="detailtext">Windmills Real Estate Valuation Services LLC
            <br>(referred to in this document as “Firm”).
        </td>
    </tr>

    <tr>
        <td class="detailtext sixpoints">5.3.	Valuer Qualifications	</td>
        <td class="detailtext">Member of Royal Institution of Chartered Surveyors
            Masters of Business Administration (International Business Management)
            Bachelors in Civil Engineering
        </td>
    </tr>

    <tr>
        <td class="detailtext sixpoints">5.4.	Valuer Status</td>
        <td class="detailtext">The Valuer is registered with RICS and RERA as valuer.</td>
    </tr>
</table><br><br>



<table>
    <tr><td class="detailheading"><h4>5.5.  Valuer Experience and Expertise</h4></td></tr>
    <tr><td class="detailtext">The Valuer has 30 years of international experience in real estate developments and valuations globally. He has wide experience in valuating residential, commercial, industrial and business properties across all Emirates of UAE over the years for the purposes of secured financing, financial reporting, insurance, investment, among others. He has sufficient, knowledge, experience of the local market, and analytical and decision-making skills to undertake the valuation competently.</td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>5.6. Valuer Duties and Supervision</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['valuer_duties_supervision'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>5.7. Internal/External Status of the Valuer</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['internal_external_status_of_the_valuer'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>5.8. Previous involvement and Conflict of Interest</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['previous_involvement_and_conflict_of_interest'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>5.9. Valuer Duties and Supervision</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['decleration_of_independence_and_objectivity'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>5.10. Report Handover</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['report_handover'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading onlymainheading"><h4>6. Valuation Terms </h4></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>6.1. Currency:</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['currency'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>6.2. Basis of Value:</h4></td></tr>
    <?php
    if($multipleProperty['purpose_of_valuation'] == 3) {
        $standardReport['basis_of_value'] = str_replace("{market_fair}", 'Fair', $standardReport['basis_of_value']);
    }else{
        $standardReport['basis_of_value'] = str_replace("{market_fair}", 'Market', $standardReport['basis_of_value']);
    }


    ?>

    <tr><td class="detailtext"><?= $standardReport['basis_of_value'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>6.3. Market Value:</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['market_value'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>6.4. Statutory definition of Market Value (capital gains tax, inheritance tax and stamp duty land tax).</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['statutory_definition_of_market_value'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>6.5. Market Rent:</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['market_rent'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>6.6. Investment Value (Worth)</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['investment_value'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>6.7. Fair Value</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['fair_value'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>6.8. Valuation Date</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['valuation_date'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>6.9. Property</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['property'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading onlymainheading"><h4>7. General Assumptions</h4></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.1 	Assumptions, Extent of Investigations, Limitations on the Scope of Work:</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['assumptions_ext_of_investi_limi_scope_of_work'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.2 	Physical Inspection</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['physical_inspection'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.3.	Desktop or Drive by Valuation</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['desktop_or_driven_by_valuation'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.4	Structural and Technical Survey</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['structural_and_technical_survey'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.5	Conditions and State of Repair and Maintenance of the Property</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['conditions_state_repair_maintenance_property'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.6	Contamination, and Ground and Environmental Considerations</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['contamination_ground_environmental_consideration'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.7	Statutory and regulatory requirements</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['statutory_and_regulatory_requirements'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.8	Title, Tenancies and Property Documents:</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['title_tenancies_and_property_document'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.9	Planning and Highway Enquiries:</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['planning_and_highway_enquiries'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.10	Plant and Equipment:</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['plant_and_equipment'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.11.	Development Properties:</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['development_properties'] ?></td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.12.	Disposal Costs and Liabilities:</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['disposal_costs_and_liabilities'] ?> </td></tr>
</table><br><br>

<table>
    <tr><td class="detailheading"><h4>7.13	Insurance Re-Instatement Cost (If applicable):</h4></td></tr>
    <tr><td class="detailtext"><?= $standardReport['insurance_reinstalement_cost'] ?></td></tr>
</table><br><br>


<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>8.	Nature and Sources of Information and Documents to be Relied Upon</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['nature_source_information_documents_relied_upon'] ?>
        </td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>9. Description of Report:</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['description_of_report'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>10.	Client Acceptance of the Valuation Report</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['client_acceptance_of_the_valuation_report'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>11.	 Restrictions on Publications</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['restrictions_on_publications'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>12.	Third party liability</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['third_party_liability'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>13.	Valuation Uncertainty</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['valuation_uncertaninty'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>14.	Complaints:</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['complaints'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>15.	RICS Monitoring</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['rics_monitoring'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>16.	RERA Monitoring</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['rera_monitoring'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>17.	Limitations on liability</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['limitions_on_liabity'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>18.	Liability & Duty of Care:</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['liability_and_duty_of_care'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>19.	Extent of Fee </h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['extent_of_fee'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>20.	Indemnification:</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['indemnification'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>21.	Professional Indemnity Insurance:</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['professional_indemmnity_insurance'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>22.	Client’s Obligations:</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['clients_obligations'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>23.	Validity of the Terms of Engagement:</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['proposal_validity'] ?></td></tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>24.	Jurisdiction:</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['jurisdiction'] ?></td></tr>
</table><br><br>

<!--<br pagebreak="true" /><br>-->

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>25.	Acceptance of Terms of Engagement:</h4></td>
    </tr>
    <tr><td class="detailtext"><?= $standardReport['acceptance_of_terms_of_engagement'] ?></td></tr>
</table><br><br>



<style>
    td.centre {
        text-align: left;
        /*vertical-align: middle;*/
        font-size: 9px;
    }
</style>

<table>
    <tr>
        <td colspan="1" class="centre"></td>
        <td colspan="4" class="centre">Signed ……………………… (Firm)</td>
        <td colspan="4" class="centre">Signed ……………………… (Client)</td>
        <td colspan="1" class="centre"></td>
    </tr><br><br>

    <tr>
        <td colspan="1" class="centre"></td>
        <td colspan="4" class="centre">For and on behalf of ………………</td>
        <td colspan="4" class="centre">For and on behalf of ……………….</td>
        <td colspan="1" class="centre"></td>
    </tr><br><br>

    <tr>
        <td colspan="1" class="centre"></td>
        <td colspan="4" class="centre">Windmills Real Estate Valuation Services LLC</td>
        <td colspan="4" class="centre"></td>
        <td colspan="1" class="centre"></td>
    </tr>
</table>
