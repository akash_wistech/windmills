<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\TinyMceAsset;
TinyMceAsset::register($this);

$this->registerJs('


tinymce.init({
  selector: ".editor",

  menubar: false,
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table paste code help wordcount"
  ],
  toolbar: "undo redo | formatselect | " +
  "bold italic backcolor | alignleft aligncenter " +
  "alignright alignjustify | bullist numlist outdent indent | " +
  "removeformat | help",
});





');


/* @var $this yii\web\View */
/* @var $model app\models\EmailTemplates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-templates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'body')->textarea(['class'=>'editor','rows'=>'15']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
