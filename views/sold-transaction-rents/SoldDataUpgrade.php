<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sold_data_upgrade".
 *
 * @property int $id
 * @property string|null $building_name
 * @property string|null $no_of_rooms
 * @property string|null $bua
 * @property string|null $plot_area
 * @property string|null $type
 * @property string|null $sold_date
 * @property string|null $price
 * @property string|null $price_sqf
 */
class SoldDataUpgrade extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sold_data_upgrade';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_name', 'no_of_rooms', 'bua', 'plot_area', 'type', 'sold_date', 'price', 'price_sqf'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'building_name' => 'Building Name',
            'no_of_rooms' => 'No Of Rooms',
            'bua' => 'Bua',
            'plot_area' => 'Plot Area',
            'type' => 'Type',
            'sold_date' => 'Sold Date',
            'price' => 'Price',
            'price_sqf' => 'Price Sqf',
        ];
    }
}
