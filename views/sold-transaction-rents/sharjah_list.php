<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

use app\assets\SortBootstrapAsset;
SortBootstrapAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Sharjah Transactions');
$cardTitle = Yii::t('app', 'Sharjah Transactions List');
$this->params['breadcrumbs'][] = $this->title;
?>


<style>
    th {
        color: #0056b3;
        font-size: 16px;
     /*  // text-align: right !important;*/
        padding-right: 30px !important;
    }
    td {
        font-size: 16px;
     /*   //text-align: right;*/
        padding-right: 50px;
    }
}
</style>

<div class="bank-revenue-index">
    <div class="card card-outline card-primary">
        <div class="card-body">
            <table id="bank-revenue" class="table table-striped dataTable">
                <thead>
                    <tr>
                        <th class="">Updated Date</th>
                        <th class="">Area</th>
                        <th class="">Property</th>
                        <th class="">Price</th>


                    </tr>
                </thead>
                <tbody>
                    <?php 
                    if(count($all_data)>0){
                        foreach($all_data as $model){
                    ?>
                        <tr>
                            <td><?=  date('d-M-Y', strtotime($model->created_date))  ?></td>
                            <td><?= ucwords($model->area); ?></td>
                            <td><?= $model->property_type ?></td>
                            <td><?= Yii::$app->appHelperFunctions->wmFormate($model->price) ?></td>
                        </tr>
                    <?php
                        }
                    }else{
                        
                    }
                    ?>
                </tbody>

            </table>
        </div>
    </div>
</div>
<iframe src="https://app.powerbi.com/reportEmbed?reportId=6afa9a88-8bd6-4c62-a206-d6153b63302e&amp;config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly9XQUJJLVdFU1QtRVVST1BFLUItUFJJTUFSWS1yZWRpcmVjdC5hbmFseXNpcy53aW5kb3dzLm5ldCIsImVtYmVkRmVhdHVyZXMiOnsibW9kZXJuRW1iZWQiOnRydWUsInVzYWdlTWV0cmljc1ZOZXh0Ijp0cnVlfX0%3d&amp;uid=yp2or2" scrolling="no" allowfullscreen="true" style="width: 100%; height: 100%;" title="Embedded report"></iframe>

<?php 
    $this->registerJs('
        $("#bank-revenue").DataTable({
            order: [[0, "desc"]],
            pageLength: 50,
        });
    ');
?>




