<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReinstatementCosts */

$this->title = 'Reinstatement Costs Values';
$this->params['breadcrumbs'][] = ['label' => 'Reinstatement Costs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reinstatement-costs-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
