<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReinstatementCosts */

$this->title = 'Create Reinstatement Costs';
$this->params['breadcrumbs'][] = ['label' => 'Reinstatement Costs', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Reinstatement Costs';
?>
<div class="reinstatement-costs-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
