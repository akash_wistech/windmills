<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReinstatementCosts */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Reinstatement Costs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="reinstatement-costs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'source',
            'property_id',
            'low',
            'middle',
            'high',
            'published_date',
            'status',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'trashed',
            'trashed_at',
            'trashed_by',
            'approved_by',
            'approved_at',
            'status_verified_at',
            'status_verified_by',
        ],
    ]) ?>

</div>
