<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReinstatementCostsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $this yii\web\View */
/* @var $searchModel app\models\CommunitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Reinstatement Costs');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;
$createBtn=true;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    $createBtn=true;
}
// if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
//     $actionBtns.='{view}';
// }

if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
// if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
//     $actionBtns.='{delete}';
// }
$actionBtns.='{get-history}';
?>




<div class="reinstatement-costs-index">
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
            ['attribute' => 'published_date',
                'label' => Yii::t('app', 'Published Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->published_date));
                },
            ],
            ['attribute' => 'source',
                'label' => Yii::t('app', 'Source'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->reinstatementSources[$model['source']];
                },
                'filter' => Yii::$app->appHelperFunctions->reinstatementSources
            ],
            ['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
                return Yii::$app->helperFunctions->arrStatusIconList[$model['status']];
            },'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>Yii::$app->helperFunctions->arrFilterStatusList],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'status' => function ($url, $model) {
                        if($model['status']==1){
                            return Html::a('<i class="fas fa-eye-slash"></i> '.Yii::t('app', 'Disable'), $url, [
                                'title' => Yii::t('app', 'Disable'),
                                'class'=>'dropdown-item text-1',
                                'data-confirm'=>Yii::t('app','Are you sure you want to disable this item?'),
                                'data-method'=>"post",
                            ]);
                        }else{
                            return Html::a('<i class="fas fa-eye"></i> '.Yii::t('app', 'Enable'), $url, [
                                'title' => Yii::t('app', 'Enable'),
                                'class'=>'dropdown-item text-1',
                                'data-confirm'=>Yii::t('app','Are you sure you want to enable this item?'),
                                'data-method'=>"post",
                            ]);
                        }
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>
</div>