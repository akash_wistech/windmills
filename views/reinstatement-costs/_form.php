<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\PropertiesAssets;

PropertiesAssets::register($this);
use  app\components\widgets\StatusVerified;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\ReinstatementCosts */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('

    $("#reinstatementcosts-published_date").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "YYYY-MM-DD"
    });
    

');
?>

<section class="reinstatement-costs-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle ?></h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'source')->dropDownList(Yii::$app->appHelperFunctions->reinstatementSources, ['prompt' => Yii::t('app', 'Select')]) ?>
            </div>

            <?php

            if ($model->published_date <> null) {

            } else {
                $model->published_date = date('Y-m-d');
            }

            ?>
            <div class="col-sm-6" id="valuation_date_id">
                <?= $form->field($model, 'published_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="reinstatementcosts-published_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#reinstatementcosts-published_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>


        </div>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title" style="font-weight: bold"><?= Yii::t('app', 'Residential Properties') ?></h2>
            </header>
            <div class="card-body">
                <table class="table table-striped">
                    <!--<thead>
                    <tr>
                        <th  style="width:10%">Title</th>
                        <th  style="width: 85%">
                        </th>

                    </tr>
                    </thead>-->
                    <tbody>

                    <tr>
                        <?php

                        $row_1 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 1, 'parent_id' => $model->id])
                            ->asArray()->one();

                        ?>
                        <td style="width:10%">
                            <strong>Appartment</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Specification</th>
                                    <th>Middle Specification</th>
                                    <th>High Specification</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        From
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[1][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_1['low']) && $row_1['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_1['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[1][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_1['middle']) && $row_1['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_1['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[1][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_1['high']) && $row_1['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_1['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        To
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[1][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_1['low_to']) && $row_1['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_1['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[1][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_1['middle_to']) && $row_1['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_1['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[1][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_1['high_to']) && $row_1['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_1['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_1['low'] + $row_1['low_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_1['middle'] + $row_1['middle_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_1['high'] + $row_1['high_to'])/2) ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>

                    </tr>
                    <tr>
                        <?php

                        $row_2 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 2, 'parent_id' => $model->id])
                            ->asArray()->one();

                        ?>
                        <td style="width:10%">
                            <strong>Villa/Townhouse</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Standard Specification</th>
                                    <th>Middle Standard Specification</th>
                                    <th>High Standard Specification</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        From
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[2][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_2['low']) && $row_2['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_2['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[2][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_2['middle']) && $row_2['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_2['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[2][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_2['high']) && $row_2['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_2['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        To
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[2][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_2['low_to']) && $row_2['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_2['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[2][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_2['middle_to']) && $row_2['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_2['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[2][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_2['high_to']) && $row_2['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_2['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_2['low'] + $row_2['low_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_2['middle'] + $row_2['middle_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_2['high'] + $row_2['high_to'])/2) ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <?php

                        $row_3 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 3, 'parent_id' => $model->id])
                            ->asArray()->one();

                        ?>
                        <td style="width:10%">
                            <strong>Building</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Specification</th>
                                    <th>Middle Specification</th>
                                    <th>High Specification</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        Low Rise (3 Star)
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[3][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_3['low']) && $row_3['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_3['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[3][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_3['middle']) && $row_3['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_3['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[3][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_3['high']) && $row_3['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_3['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        Middle Rise (4 Star)
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[3][low_m]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_3['low_m']) && $row_3['low_m'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_3['low_m']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[3][middle_m]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_3['middle_m']) && $row_3['middle_m'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_3['middle_m']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[3][high_m]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_3['high_m']) && $row_3['high_m'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_3['high_m']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        High Rise (5 Star)
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[3][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_3['low_to']) && $row_3['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_3['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[3][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_3['middle_to']) && $row_3['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_3['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[3][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_3['high_to']) && $row_3['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_3['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_3['low']+ $row_3['low_m'] + $row_3['low_to'])/3) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_3['middle'] + $row_3['middle_m'] + $row_3['middle_to'])/3) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_3['high'] + $row_3['high_m'] + $row_3['high_to'])/3) ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title" style="font-weight: bold"><?= Yii::t('app', 'Commercial Properties') ?></h2>
            </header>
            <div class="card-body">
                <table class="table table-striped">
                   <!-- <thead>
                    <tr>
                        <th>Title</th>
                        <th>Standard Specification</th>
                        <th>Middle Specification</th>
                        <th>High Specification</th>

                    </tr>
                    </thead>-->
                    <tbody>
                    <tr>
                        <?php

                        $row_4 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 4, 'parent_id' => $model->id])
                            ->asArray()->one();

                        ?>
                        <td style="width:10%">
                            <strong>Office</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Specification</th>
                                    <th>Middle Specification</th>
                                    <th>High Specification</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        From
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[4][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_4['low']) && $row_4['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_4['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[4][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_4['middle']) && $row_4['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_4['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[4][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_4['high']) && $row_4['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_4['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        To
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[4][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_1['low_to']) && $row_1['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_1['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[4][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_1['middle_to']) && $row_1['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_1['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[4][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_1['high_to']) && $row_1['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_1['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_4['low'] + $row_4['low_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_4['middle'] + $row_4['middle_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_4['high'] + $row_4['high_to'])/2) ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <?php
                        $row_5 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 5, 'parent_id' => $model->id])
                            ->asArray()->one();
                        ?>
                        <td style="width:10%">
                            <strong>Building</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Specification</th>
                                    <th>Middle Specification</th>
                                    <th>High Specification</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        Low Rise (3 Star)
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[5][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_5['low']) && $row_5['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_5['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[5][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_5['middle']) && $row_5['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_5['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[5][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_5['high']) && $row_5['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_5['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        Middle Rise (4 Star)
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[5][low_m]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_5['low_m']) && $row_5['low_m'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_5['low_m']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[5][middle_m]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_5['middle_m']) && $row_5['middle_m'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_5['middle_m']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[5][high_m]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_5['high_m']) && $row_5['high_m'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_5['high_m']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        High Rise
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[5][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_5['low_to']) && $row_5['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_5['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[5][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_5['middle_to']) && $row_5['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_5['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[5][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_5['high_to']) && $row_5['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_5['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_5['low'] + $row_5['low_m'] + $row_5['low_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_5['middle']+ $row_5['middle_m'] + $row_5['middle_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_5['high'] + $row_5['high_m'] + $row_5['high_to'])/2) ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title" style="font-weight: bold"><?= Yii::t('app', 'Industrial Properties') ?></h2>
            </header>
            <div class="card-body">
                <table class="table table-striped">
                  <!--  <thead>
                    <tr>
                        <th>Title</th>
                        <th>Standard Specification</th>
                        <th>Middle Specification</th>
                        <th>High Specification</th>

                    </tr>
                    </thead>-->
                    <tbody>
                    <tr>
                        <?php
                        $row_6 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 6, 'parent_id' => $model->id])
                            ->asArray()->one();
                        ?>
                        <td style="width:10%">
                            <strong>Warehouse</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Specification</th>
                                    <th>Middle Specification</th>
                                    <th>High Specification</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        From
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[6][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_6['low']) && $row_6['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_6['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[6][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_6['middle']) && $row_6['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_6['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[6][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_6['high']) && $row_6['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_6['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        To
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[6][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_6['low_to']) && $row_6['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_6['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[6][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_6['middle_to']) && $row_6['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_6['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[6][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_6['high_to']) && $row_6['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_6['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_6['low'] + $row_6['low_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_6['middle'] + $row_6['middle_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_6['high'] + $row_6['high_to'])/2) ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <?php
                        $row_7 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 7, 'parent_id' => $model->id])
                            ->asArray()->one();
                        ?>
                        <td style="width:10%">
                            <strong>Labor Camp</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Specification</th>
                                    <th>Middle Specification</th>
                                    <th>High Specification</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        From
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[7][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_7['low']) && $row_7['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_7['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[7][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_7['middle']) && $row_7['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_7['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[7][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_7['high']) && $row_7['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_7['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        To
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[7][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_7['low_to']) && $row_7['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_7['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[7][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_7['middle_to']) && $row_7['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_7['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[7][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_7['high_to']) && $row_7['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_7['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_7['low'] + $row_7['low_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_7['middle'] + $row_7['middle_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_7['high'] + $row_7['high_to'])/2) ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </section>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title" style="font-weight: bold"><?= Yii::t('app', 'Retail Properties') ?></h2>
            </header>
            <div class="card-body">
                <table class="table table-striped">
                 <!--   <thead>
                    <tr>
                        <th>Title</th>
                        <th>Standard Specification</th>
                        <th>Middle Specification</th>
                        <th>High Specification</th>

                    </tr>
                    </thead>-->
                    <tbody>
                    <tr>
                        <?php
                        $row_8 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 8, 'parent_id' => $model->id])
                            ->asArray()->one();
                        ?>
                        <td style="width:10%">
                            <strong>Shop</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Specification</th>
                                    <th>Middle Specification</th>
                                    <th>High Specification</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        From
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[8][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_8['low']) && $row_8['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_8['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[8][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_8['middle']) && $row_8['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_8['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[8][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_8['high']) && $row_8['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_8['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        To
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[8][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_8['low_to']) && $row_8['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_8['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[8][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_8['middle_to']) && $row_8['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_8['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[8][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_8['high_to']) && $row_8['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_8['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_8['low'] + $row_8['low_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_8['middle'] + $row_8['middle_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_8['high'] + $row_8['high_to'])/2) ?>
                                    </td>
                                </tr>

                                </tbody>
                            </table>

                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </section>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title" style="font-weight: bold"><?= Yii::t('app', 'Trading Properties') ?></h2>
            </header>
            <div class="card-body">
                <table class="table table-striped">
                 <!--   <thead>
                    <tr>
                        <th>Title</th>
                        <th>Standard Specification</th>
                        <th>Middle Specification</th>
                        <th>High Specification</th>

                    </tr>
                    </thead>-->
                    <tbody>
                    <tr>
                        <?php
                        $row_9 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 9, 'parent_id' => $model->id])
                            ->asArray()->one();
                        ?>
                        <td style="width:10%">
                            <strong>Mall</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Specification</th>
                                    <th>Middle Specification</th>
                                    <th>High Specification</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        From
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[9][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_9['low']) && $row_9['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_9['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[9][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_9['middle']) && $row_9['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_9['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[9][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_9['high']) && $row_9['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_9['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        To
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[9][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_9['low_to']) && $row_9['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_9['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[9][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_9['middle_to']) && $row_9['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_9['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[9][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_9['high_to']) && $row_9['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_9['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_9['low'] + $row_9['low_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_9['middle'] + $row_9['middle_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_9['high'] + $row_9['high_to'])/2) ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <?php
                        $row_10 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 10, 'parent_id' => $model->id])
                            ->asArray()->one();
                        ?>
                        <td style="width:10%">
                            <strong>School</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Specification</th>
                                    <th>Middle Specification</th>
                                    <th>High Specification</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        From
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[10][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_10['low']) && $row_10['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_10['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[10][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_10['middle']) && $row_10['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_10['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[10][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_10['high']) && $row_10['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_10['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        To
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[10][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_10['low_to']) && $row_10['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_10['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[10][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_10['middle_to']) && $row_10['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_10['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[10][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_10['high_to']) && $row_10['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_10['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_10['low'] + $row_10['low_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_10['middle'] + $row_10['middle_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_10['high'] + $row_10['high_to'])/2) ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <?php
                        $row_11 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 11, 'parent_id' => $model->id])
                            ->asArray()->one();
                        ?>
                        <td style="width:10%">
                            <strong>Hotel</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Specification</th>
                                    <th>Middle Specification</th>
                                    <th>High Specification</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        From
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[11][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_11['low']) && $row_11['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_11['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[11][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_11['middle']) && $row_11['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_11['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[11][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_11['high']) && $row_11['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_11['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        To
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[11][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_11['low_to']) && $row_11['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_11['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[11][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_11['middle_to']) && $row_11['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_11['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[11][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_11['high_to']) && $row_11['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_11['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_11['low'] + $row_11['low_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_11['middle'] + $row_11['middle_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_11['high'] + $row_11['high_to'])/2) ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <?php
                        $row_12 = \app\models\ReinstatementCostsValues::find()
                            ->where(['property_id' => 12, 'parent_id' => $model->id])
                            ->asArray()->one();
                        ?>
                        <td style="width:10%">
                            <strong>Hospital</strong>
                        </td>
                        <td style="width:95%">
                            <table style="width: -webkit-fill-available">
                                <thead>
                                <tr>
                                    <th>Range</th>
                                    <th>Standard Specification</th>
                                    <th>Middle Specification</th>
                                    <th>High Specification</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="background-color: inherit">
                                    <td style="width:10%">
                                        From
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[12][low]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_12['low']) && $row_12['low'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_12['low']) : 0])->label(false) ?>
                                    </td >
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[12][middle]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_12['middle']) && $row_12['middle'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_12['middle']) : 0])->label(false) ?>
                                    </td>
                                    <td style="width:30%">
                                        <?= $form->field($model, 'cost_values[12][high]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_12['high']) && $row_12['high'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_12['high']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        To
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[12][low_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_12['low_to']) && $row_12['low_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_12['low_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[12][middle_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_12['middle_to']) && $row_12['middle_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_12['middle_to']) : 0])->label(false) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= $form->field($model, 'cost_values[12][high_to]')->textInput(['required' => true, 'type' => 'number', 'value' => (isset($row_12['high_to']) && $row_12['high_to'] <> null) ? Yii::$app->appHelperFunctions->wmFormatenocomma($row_12['high_to']) : 0])->label(false) ?>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="width:10%">
                                        Average
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_12['low'] + $row_12['low_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_12['middle'] + $row_12['middle_to'])/2) ?>
                                    </td>
                                    <td  style="width:30%">
                                        <?= Yii::$app->appHelperFunctions->wmFormatenocomma(($row_12['high'] + $row_12['high_to'])/2) ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>


        <?= StatusVerified::widget(['model' => $model, 'form' => $form]) ?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if ($model <> null && $model->id <> null) {
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
