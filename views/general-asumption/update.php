<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GeneralAsumption */

$this->title = Yii::t('app', 'Update General Asumption: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'General Asumptions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="general-asumption-update">


    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $this->title,
    ]) ?>

</div>
