<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\GeneralAsumption */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'General Asumptions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="general-asumption-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'general_asumption:ntext',
        ],
    ]) ?>

</div>
