<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GeneralAsumption */

$this->title = Yii::t('app', 'Create General Asumption');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'General Asumptions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="general-asumption-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $this->title,
    ]) ?>

</div>
