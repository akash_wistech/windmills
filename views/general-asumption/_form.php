<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;

/* @var $this yii\web\View */
/* @var $model app\models\GeneralAsumption */
/* @var $form yii\widgets\ActiveForm */
?>


<section class="contact-form card card-outline card-primary">
	<?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $cardTitle?></h2>
	</header>
	<div class="card-body">
		<div class="row">
			<div class="col">
				<?= $form->field($model, 'general_asumption')->textArea(['rows'=> 5]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col">
			<?= $form->field($model, 'type')->dropDownList(Yii::$app->appHelperFunctions->getAssumptionTypesArr(),['prompt'=>Yii::t('app','Select')])?>
			</div>
		</div>
		
	<?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
	</div>
	<div class="card-footer">
		<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
		<?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</section>


