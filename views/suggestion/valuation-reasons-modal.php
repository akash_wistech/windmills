<!-- <div class="modal fade" id="valuation-reasons-modal"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="border-top:3px solid orange">
                <h4 class="modal-title text-primary"><?= $model->reference_number ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
      

                <?php if(isset($model->cancelReasons) && ($model->cancelReasons <> null)){ ?>
                <table class="table table-striped table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th style="width: 75%;">Reason</th>
                      <th>Email Sent Date/Time</th>
                    </tr>
                  </thead>
                  <tbody>
                        <?php foreach ($model->cancelReasons as $reason){
                            $date = new DateTime($reason->created_at);
                            $date_time = $date->format('d F Y / h:i A');    
                        ?>
                        <tr>
                            <td><?= $reason->reason->title ?></td>
                            <td><?= $date_time ?></td>
                        </tr>
                        <?php } ?>
                  </tbody>
                </table>
                <?php }else{ ?>
                        <div class="row text-center text-danger text-bold">
                            <div class="col">
                                No Reasons found in table.
                            </div>
                        </div>
                <?php } ?>
            </div>

            <div class="modal-footer">
                <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>


        </div>
    </div>
<!-- </div> -->
