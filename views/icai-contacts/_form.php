<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;

?>

<section class="contact-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title">Create</h2>
    </header>
    <div class="card-body">
        <div class="row">

            <div class="col-4">
                <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-4">
                <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-4">
                <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-4">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-4">
                <label for="phone">Phone number:</label>
                <div class="input-group input-group-md mb-3">
                    <div class="input-group-prepend">
                    <select name='IcaiContacts[phone_code]' id="country-code" class="form-control">
                            <option value="050"
                                <?php if(isset($model->phone_code) && ($model->phone_code == '050')){ echo 'selected';} ?>>
                                050</option>
                            <option value="052"
                                <?php if(isset($model->phone_code) && ($model->phone_code == '052')){ echo 'selected';} ?>>
                                052</option>
                            <option value="054"
                                <?php if(isset($model->phone_code) && ($model->phone_code == '054')){ echo 'selected';} ?>>
                                054</option>
                            <option value="055"
                                <?php if(isset($model->phone_code) && ($model->phone_code == '055')){ echo 'selected';} ?>>
                                055</option>
                            <option value="056"
                                <?php if(isset($model->phone_code) && ($model->phone_code == '056')){ echo 'selected';} ?>>
                                056</option>
                            <option value="058"
                                <?php if(isset($model->phone_code) && ($model->phone_code == '058')){ echo 'selected';} ?>>
                                058</option>
                        </select>
                    </div>
                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true,'class' => 'form-control','placeholder' => 'Enter phone number'])->label(false) ?>
                        <!-- <input type="text" id="icaicontacts-phone" class="form-control" name="IcaiContacts[phone]" placeholder="Enter phone number" aria-invalid="false"> -->
                </div>
            </div>

            <div class="col-4">
                <label for="phone">Land Line number:</label>
                <div class="input-group input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name="IcaiContacts[land_line_code]" id="country-code" class="form-control">
                            <option value="02"
                                <?php if(isset($model->land_line_code) && ($model->land_line_code == '02')){ echo 'selected';} ?>>
                                02</option>
                            <option value="04"
                                <?php if(isset($model->land_line_code) && ($model->land_line_code == '04')){ echo 'selected';} ?>>
                                04</option>
                            <option value="03"
                                <?php if(isset($model->land_line_code) && ($model->land_line_code == '03')){ echo 'selected';} ?>>
                                03</option>
                            <option value="06"
                                <?php if(isset($model->land_line_code) && ($model->land_line_code == '06')){ echo 'selected';} ?>>
                                06</option>
                            <option value="07"
                                <?php if(isset($model->land_line_code) && ($model->land_line_code == '07')){ echo 'selected';} ?>>
                                07</option>
                            <option value="09"
                                <?php if(isset($model->land_line_code) && ($model->land_line_code == '09')){ echo 'selected';} ?>>
                                09</option>
                        </select>
                    </div>
                    <?= $form->field($model, 'landline')->textInput(['maxlength' => true,'class' => 'form-control','placeholder' => 'Enter phone number'])->label(false) ?>
                    <!-- <input type="text" id="icaicontacts-landline" class="form-control" name="IcaiContacts[landline]" placeholder="Enter phone number" aria-invalid="false"> -->
                </div>
            </div>



            <div class="col-4">
                <?= $form->field($model, 'jobtitle')->textInput(['maxlength' => true]) ?>
            </div>
            
            

            <div class="col-4">
                <?= $form->field($model, 'linkedin')->textInput(['maxlength' => true]) ?>
            </div>
            
            

        </div>

        <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>



    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php 
            if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => get_class($model),
                ]);
            }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>