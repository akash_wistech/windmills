<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IcaiContacts */

$this->title = 'Create Icai Contacts';
$this->params['breadcrumbs'][] = ['label' => 'Icai Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="icai-contacts-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
