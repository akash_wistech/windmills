<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IcaiContacts */

$this->title = 'Update Icai Contacts: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Icai Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="icai-contacts-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
