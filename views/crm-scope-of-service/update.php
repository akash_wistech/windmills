<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CrmScopeOfService */

$this->title = 'Update Crm Scope Of Service: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Crm Scope Of Service', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="valuation-purposes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
