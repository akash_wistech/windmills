<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Developments */

$this->title = Yii::t('app', 'Crm Scope Of Service');
$cardTitle = Yii::t('app', 'New Crm Scope Of Service');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle
?>
<div class="developments-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>