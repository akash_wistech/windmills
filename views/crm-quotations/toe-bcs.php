<?php
use app\models\UserProfileInfo;
use app\models\ProposalMasterFile;
use app\models\Valuation;
use app\models\CrmReceivedProperties;

$mainTableData = yii::$app->quotationHelperFunctions->getMainTableDataNew($id);
$multipleProperties = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id);
$multipleProperty = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id);
$detail= \app\models\ValuationDetailData::find()->where(['valuation_id' => $model->id])->one();
$multiScopeProperties = yii::$app->quotationHelperFunctions->getMultipleScopeProperties($id);

$total_no_of_prperties = count($multipleProperties);
$no_of_property_discount = 0;
$total_discount = 0;
$total_discount_amount = 0;
$aprvd_total_discount_amount = 0;
$discount = 0;
$total_net_fee = 0;

//old query
// $toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');

//new query
$toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');

$toe_fee_building_totals = CrmReceivedProperties::find()
    ->select(['multiscope_id', 'SUM(toe_fee) as toe_fee_sum'])
    ->where(['quotation_id' => $id])
    ->groupBy('multiscope_id')
    ->orderBy('property_index')
    ->asArray()
    ->all();




//Advance Payment Terms

if ($model->client->client_type != 'bank') {
    //Advance Payment Terms
    $toe_fee_total = $toe_fee_total + $model->advance_payment_terms_final_amount;

} else {
    $advance_payment_terms['amount'] = 0;
    $advance_payment_terms['value'] = 0;
}


$total_discount = 0;
$total_discount_amount = 0;
$aprvd_total_discount_amount = 0;
$discount = 0;
$netValuationFee = $toe_fee_total - $discount;
$aprvd_netValuationFee = $toe_fee_total - $discount;

if ($model->no_of_property_discount > 0) {
    $no_of_property_discount_amount = $model->no_of_property_discount_final_amount;
    $total_discount = $total_discount + $model->no_of_property_discount;
    $total_discount_amount = $total_discount_amount + $no_of_property_discount_amount;
}
// for approved no of property discount
if($model->aprvd_no_of_property_discount > 0) {
    $aprvd_no_of_property_discount_amount = $model->aprvd_no_of_property_discount_amount;
    $aprvd_total_discount= $aprvd_total_discount + $model->aprvd_no_of_property_discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_no_of_property_discount_amount;
}

if ($model->same_building_discount > 0) {
    $same_building_discount_amount = $model->no_of_units_same_building_final_amount;
    $total_discount = $total_discount + $model->same_building_discount;
    $total_discount_amount = $total_discount_amount + $same_building_discount_amount;
}
// for approved same building discount
if($model->aprvd_same_building_discount > 0) {
    $aprvd_same_building_discount_amount = $model->aprvd_same_building_discount_amount ;
    $aprvd_total_discount= $aprvd_total_discount + $model->aprvd_same_building_discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_same_building_discount_amount;
}

if ($model->first_time_discount > 0) {
    $first_time_fee_discount_amount = $model->first_time_discount_final_amount;
    $total_discount = $total_discount + $model->first_time_discount;
    $total_discount_amount = $total_discount_amount + $first_time_fee_discount_amount;
}
// for approved first time bulding
if($model->aprvd_first_time_discount > 0) {
    $aprvd_first_time_discount_amount = $model->aprvd_first_time_discount_amount;
    $aprvd_total_discount= $aprvd_total_discount + $model->aprvd_first_time_discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_first_time_discount_amount;
}

if ($model->relative_discount != null) {
    $discount = $model->relationship_discount_final_amount;
    $total_discount_amount = $total_discount_amount + $discount;
    $total_discount = $total_discount + $model->relative_discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $discount;

}


$netValuationFee = $netValuationFee - $total_discount_amount;
$discount_quotations = 0;


if ($model->relative_discount != null) {
    $discount_quotations = $model->relationship_discount_final_amount;
}

$discount_net_fee = $netValuationFee;

// for approved fee 

if($aprvd_total_discount_amount > 0) {
    $aprvd_netValuationFee = $aprvd_netValuationFee-$aprvd_total_discount_amount;
}else {
    $aprvd_netValuationFee = $aprvd_netValuationFee-$total_discount_amount;
}
$aprvd_discount_net_fee = $aprvd_netValuationFee;


//urgencyfee check
if ($model->tat_requirements > 0) {
    $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $netValuationFee);
    $netValuationFee = $netValuationFee + $model->tat_requirements_final_amount;

    // for approved
    $aprvd_urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $aprvd_netValuationFee);
    $aprvd_netValuationFee = $aprvd_netValuationFee + $model->aprvd_tat_requirements_final_amount;


}


$VatInTotalValuationFee = 0;
$aprvd_VatInTotalValuationFee = 0;
if ($model->client->vat == 1) {
    $VatInTotalValuationFee = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
    $aprvd_VatInTotalValuationFee = yii::$app->quotationHelperFunctions->getVatTotal($aprvd_netValuationFee);
}

$finalFeePayable = $netValuationFee + $VatInTotalValuationFee;
$aprvd_finalFeePayable = $aprvd_netValuationFee + $aprvd_VatInTotalValuationFee;

$totalTurnAroundTime = $mainTableData['quotation_turn_around_time'];
$totalValuationFeeInWORDS = yii::$app->quotationHelperFunctions->numberTowords($toe_fee_total);

$finalFeePayable = Yii::$app->appHelperFunctions->wmFormate($finalFeePayable);
$aprvd_finalFeePayable = Yii::$app->appHelperFunctions->wmFormate($aprvd_finalFeePayable);


$finalFeePayableInWords = yii::$app->quotationHelperFunctions->numberTowords(str_replace(',', '', $finalFeePayable));
$aprvd_finalFeePayableInWords = yii::$app->quotationHelperFunctions->numberTowords(str_replace(',', '', $aprvd_finalFeePayable));


//end usama

/* echo "<pre>";
 print_r($multipleProperties);
 echo "</pre>";
 die();*/
$ValuerName = \app\models\User::find()
    ->select(['fullname' => 'CONCAT(firstname," ",lastname)'])
    ->where(['id' => $mainTableData['valuer_id']])
    ->asArray()
    ->one();
$clientName = yii::$app->quotationHelperFunctions->getClientName($mainTableData['client_name']);
$purposeOfValuation = Yii::$app->appHelperFunctions->getPurpose();
$standardReport = Yii::$app->quotationHelperFunctions->getBcsReport();
// $valuerFirstName = Yii::$app->user->identity->firstname;
// $valuerLastName = Yii::$app->user->identity->lastname;
// $valuerName = $valuerFirstName.' '.$valuerLastName;
$valuerQualifications = Yii::$app->user->identity->profileInfo->valuer_qualifications;
$valuerStatus = Yii::$app->user->identity->profileInfo->valuer_status;
$valuerExperienceExpertise = Yii::$app->user->identity->profileInfo->valuer_experience_expertise;

$qoutApprover = \app\models\User::find()
    ->select(['quotation_approver'])
    ->where(['id' => Yii::$app->user->id])
    ->one();

if( $model->quotation_status == "16" && $model->toe_recommended == "1" && $qoutApprover->quotation_approver == "1" )
{
    $model->valuer_id = Yii::$app->user->id;
}else{
    $model->valuer_id =  $model->valuer_id;
}

$ValuerName = \app\models\User::find()->select(['firstname', 'lastname'])->where(['id' => $model->valuer_id])->one();

$valuerOther = \app\models\UserProfileInfo::find()->select(['valuer_qualifications', 'valuer_status', 'valuer_experience_expertise', 'signature_img_name'])->where(['user_id' => $model->valuer_id])->one();

$valuerJob = \app\models\UserProfileInfo::find()->select(['job_title_id'])->where(['user_id' => $model->valuer_id])->one();
$valuerJobName = \app\models\JobTitle::find()->select(['title'])->where(['id' => $valuerJob->job_title_id])->one();

$bankName = $iban = $trn = '';
$property = Yii::$app->appHelperFunctions->getFirstProperty($id);
if ($property <> null) {
    $branchDetail = \app\models\Branch::find()->where(['zone_list' => $property->building->city])->one();

    if ($branchDetail <> null) {
        $bankName = $branchDetail->bank_name;
        $iban = $branchDetail->iban_number;
        $trn = $branchDetail->trn_number;
        $branchCompany = $branchDetail->company;
        $branchAddress = $branchDetail->address;
        $branchPhone = $branchDetail->office_phone;
    }
}


// $scope_of_service = Yii::$app->appHelperFunctions->getScopeOfServiceTitle($mainTableData['scope_of_service']);
// $scope_of_service = Yii::$app->appHelperFunctions->scopeOfWorkArr[$mainTableData['scope_of_service']];
// $scope_of_service = Yii::$app->appHelperFunctions->ScopeOfWorkList[$mainTableData['scope_of_service']];



// $general_assumptions = explode(",", $mainTableData['assumptions']);
$general_assumptions = explode(",", $property->property_general_asumption);
$generalAssumptionsNames = \app\models\GeneralAsumption::find()
    ->select(['general_asumption'])
    ->where(['in', 'id', $general_assumptions])
    ->all();
$generalAssumptionsNamesConcat = '';
$i = 0;

foreach ($generalAssumptionsNames as $key => $assumption) {
    $generalAssumptionsNamesConcat .= $assumption->general_asumption;
    if (!count($generalAssumptionsNames) == $i) {
        $generalAssumptionsNamesConcat .= ', ';
    }
    $i++;
}


// $special_assumptions = explode(",", $mainTableData['special_assumptions']);
$special_assumptions = explode(",", $property->property_special_asumption);
$specialAssumptionsNames = \app\models\GeneralAsumption::find()
    ->select(['general_asumption'])
    ->where(['in', 'id', $special_assumptions])
    ->all();
$specialAssumptionsNamesConcat = '';
$i = 0;
// dd(count($specialAssumptionsNames));
foreach ($specialAssumptionsNames as $key => $assumption) {
    $specialAssumptionsNamesConcat .= $assumption->general_asumption;
    if (!count($specialAssumptionsNames) < $i) {
        $specialAssumptionsNamesConcat .= ', ';
    }
    $i++;
}

// dd("here");
$ceo = \app\models\UserProfileInfo::find()->select(['valuer_qualifications', 'valuer_status', 'valuer_experience_expertise', 'signature_img_name'])->where(['user_id' => 6319])->one();
/*echo "<pre>";
echo 'https://maxima-media.s3.eu-central-1.amazonaws.com/images/'.$ceo['signature_img_name'];
print_r($ceo);
die;*/
?>


<style>
    table {

    }

    td.detailheading {
        color: #0D47A1;
        font-weight: normal;
        font-size: 11px;
        /* font-weight:bold; */
        /* border-bottom: 1px solid #64B5F6; */
    }
    .subheading {
        color: #0D47A1;
        font-weight: normal;
        font-size: 11px;
        /* font-weight:bold; */
        /* border-bottom: 1px solid #64B5F6; */
    }

    td.toeheading {
        color: #4A148C;
        font-size: 11px;
        font-weight: bold;
        /* font-weight: normal; */
        border-top: 1px solid #4A148C;
        border-bottom: 1px solid #4A148C;
        border-left: 1px solid #4A148C;
        border-right: 1px solid #4A148C;
        text-align: center;
    }

    td.onlymainheading {
        color: #4A148C;
        font-size: 11px;
        /* font-weight:bold; */
        font-weight: normal;
        border-bottom: 1px solid #000000;
    }

    td.detailtext {
        /* background-color:#E8F5E9; */
        font-size: 11px;
        text-align: justify;
    }
    
    span.spantag {
        /* background-color:#E8F5E9; */
        font-size: 11px;
        text-align: justify;
        font-weight: normal;
        color: black;
    }

    td.sixpoints {
        font-size: 11px;
        color: #0D47A1;
    }

    tr.color {
        background-color: #ECEFF1;
    }

    td.subject {
        font-weight: bold;
        border-bottom: 1px solid #4A148C;
    }

    td.fontsizeten {
        font-size: 11px;
    }

    span.fontsize12 {
        /* background-color:#E8F5E9; */
        font-size: 14px;
    }

    span.b-number {
        color: black;
        font-weight: bold;
        font-size: 11px;
    }
    .subtable {
        padding-left: 30px;
    }

</style>

<table>
    <tr>
        <td class="firstpage fontsizeten"><?= date("F j, Y", strtotime(date("Y-m-d"))); ?></td>
    </tr>
    <br>


    <tr>
        <td class="firstpage fontsizeten"><?= $clientName['title'] ?></td>
    </tr>

    <?php
    if ($clientName['address'] != null) { ?>
        <tr>
            <td class="firstpage fontsizeten"><?= $clientName['address'] ?></td>
        </tr><br>
        <?php
        // code...
    }
    ?>


    <tr>
        <td class="firstpage fontsizeten">Dear Sir/Madam,</td>
    </tr>
    <br>

    <tr>
        <td class="detailheading fontsizeten subject" colspan="2"><h4>Terms of Engagement for Valuation Services</h4>
        </td>
    </tr>
    <br>
    <tr>
        <td style="text-align:justify;" class="firstpage fontsizeten"><?= $standardReport['firstpage_content'] ?></td>
    </tr>

    <tr>
        <td class="firstpage fontsizeten"><br/><br/>
            Thank you very much.<br>Sincerely,<br/><br/>
            <?php 
                if ( $model->valuer_id == 21 ) {  
                    $sigImg = \yii\helpers\Url::to('@web/images/lksign.png'); ?>
                    <img src="<?php echo $sigImg ?>" style="height: 40px; ">
                    <?php 
                } else 
                if ( $model->valuer_id == 111558 ) { 
                    $sigImg = \yii\helpers\Url::to('@web/images/ahmet_sig.jpeg'); ?>
                    <img src="<?php echo $sigImg ?>" style="height: 70px; ">
                    <?php 
                } else if($model->valuer_id == 38){
                    $sigImg = \yii\helpers\Url::to('@web/images/feb_sig6537cbcf709e8.png'); ?>
                    <img src="<?php echo $sigImg ?>" style="height: 50px; ">
                    <?php 
                }else if($model->valuer_id == 110821) {
                    $sigImg = \yii\helpers\Url::to('@web/images/IMG_387765b2070e17fe2.jpg');?>
                    <img src="<?php echo $sigImg ?>" style="height: 50px; ">
                    <?php 
                } 
            ?>

            <br>
            <td class="detailtext"><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?><br><?= $valuerJobName['title'] ?><br><?= $branchCompany; ?>
            </td>
            <?php /*-- <img src="<?php echo '/images/lksign.png' ?>" style="height: 40px; ">
            <?= $standardReport['firstpage_footer'] ?> */ ?>
        </td>
        
    </tr>

</table><br pagebreak="true"/>

<table cellspacing="1" cellpadding="8" class="main-class">
    <tr>
        <td class="detailheading toeheading" colspan="2"><h4>Terms of Engagement
                <br><?= $branchCompany; ?>
            </h4></td>
    </tr>
</table><br><br>


<table>
    <tr>
        <td class="detailheading onlymainheading" colspan="2"><h4>1. Client and Intended Users</h4></td>
    </tr>

</table><br><br>


<table>
    <tr>
        <td class="detailheading"><h4>1.1. Client</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $clientName['title'] ?></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $clientName['address'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>1.2. Service Provider</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $branchCompany; ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>2. Subject Properties to be Valued (Scope of Service)</h4></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>2.1. Subject Property</h4></td>
    </tr>
    <?php
    if ($multiScopeProperties != null) {

        $i = 1;
        foreach ($multiScopeProperties as $multipleProperty) {
            // dd($multipleProperty);
            $buildingName = yii::$app->quotationHelperFunctions->getBuildingName($multipleProperty['building_info']);
            $building = yii::$app->quotationHelperFunctions->getBuildingData($multipleProperty['building_info']);
            $multipleProperty['city'] = Yii::$app->appHelperFunctions->emiratedListArr[$building->city];
            $multipleProperty['community'] = $building->communities->title;
            $multipleProperty['sub_community'] = $building->subCommunities->title;

            $scopesOfSerivices = explode(',', $multipleProperty['scope_of_service']);
            sort($scopesOfSerivices);
          

            $multiScopeProperty = \app\models\CrmReceivedProperties::find()
                ->where(['multiscope_id' => $multipleProperty->multiscope_id])
                ->orderBy(['id' => SORT_DESC])->all();

            if ($mainTableData['no_of_properties'] > 1) {?>
                <tr>
                    <td class="detailtext"><span class="b-number">Project No: <?= $i ?></span>
                    </td>
                </tr>
                <tr>
                    <td class="detailtext">
                        <br>Street: <?= $multipleProperty['street'] ?>,
                        <!-- <br>Unit no: <?= $multipleProperty['unit_number'] ?>, -->
                        <br>Plot no: <?= $multipleProperty['plot_number'] ?>,
                        <br>Building Name: <?= $buildingName['title'] ?>,
                        <br>Community: <?= $multipleProperty['community'] ?>,
                        <br>Sub Community: <?= $multipleProperty['sub_community'] ?>,
                        <br>City: <?= $multipleProperty['city'] ?>,
                        <br>Property Type: <?= $multipleProperty->property['title'] ?>,
                        <?php if(count($scopesOfSerivices) > 1) { ?>
                        <br>Scope of Service: <?php 
                            foreach($scopesOfSerivices as $scope){
                                echo Yii::$app->appHelperFunctions->ScopeOfWorkList[$scope].", "; 
                            }
                        ?>
                        <?php } ?>
                        <!--<br>The purpose of the subject valuation is --><?/*= Yii::$app->appHelperFunctions->purposeOfValuationArr[$mainTableData['purpose_of_valuation']] */ ?>
                    </td>
                </tr><br>
                <?php
            } else {

                ?>
                <tr>
                    <td class="detailtext">Street: <?= $multipleProperty['street'] ?>,
                        <!-- <br>Unit no: <?= $multipleProperty['unit_number'] ?>, -->
                        <br>Plot no: <?= $multipleProperty['plot_number'] ?>,
                        <br>Building Name: <?= $buildingName['title'] ?>,
                        <br>Sub Community: <?= $multipleProperty['sub_community'] ?>,
                        <br>Community: <?= $multipleProperty['community'] ?>,
                        <br>City: <?= $multipleProperty['city'] ?>,
                        <br>Property Type: <?= $multipleProperty->property['title'] ?>,
                        <?php if(count($scopesOfSerivices) > 1) { ?>
                        <br>Scope of Service: <?php 
                            foreach($scopesOfSerivices as $scope){
                                echo Yii::$app->appHelperFunctions->ScopeOfWorkList[$scope].", "; 
                            }
                        ?>
                        <?php } ?>
                        <!--      <br>The purpose of the subject valuation is --><?/*= Yii::$app->appHelperFunctions->purposeOfValuationArr[$mainTableData['purpose_of_valuation']] */
                        ?>
                    </td>
                </tr><br>
                <?php
            }
            $i++;
        }
    }
    ?>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>2.2. Scope of Service</h4></td>
    </tr>
    <tr>
        <td class="detailtext">Our scope of work will include the following but will not be limited to the following:
        <?php $uniqueServices = array_values(array_unique(array_reduce($multiScopeProperties, function ($carry, $property) {
            return array_merge($carry, explode(',', $property['scope_of_service']));
        }, [])));
        sort($uniqueServices); 
        foreach ($uniqueServices as $service) {
            // RFS - get from master file
            if($service == 6){
                echo $standardReport['rfs_sos_details'] . "<br>";
            }
            // SCS - get from master file
            if($service == 7){
                echo $standardReport['scs_sos_details'] . "<br>";
            }
            // BCA - get from master file
            if($service == 8){
                echo $standardReport['bca_sos_details'] . "<br>";
            }            
            // RICA - get from master file
            if($service == 9){
                echo $standardReport['rica_sos_details'] . "<br>";
            }            
            // TS - get from master file
            if($service == 10){
                echo $standardReport['ts_sos_details'] . "<br>";
            }            
            // ERR - get from master file
            if($service == 11){
                echo $standardReport['err_sos_details'] . "<br>";
            }            
            // ARCT - get from master file
            if($service == 12){
                echo $standardReport['arct_sos_details'] . "<br>";
            }            
            // ARCT - get from master file
            if($service == 13){
                echo $standardReport['rfv_sos_details'] . "<br>";
            }            
        }     
        ?>
        </td>
    </tr>
</table><br>


<table>
    <tr>
        <td class="detailheading"><h4>2.3. Exclusions:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['exclusions'] ?></td>
    </tr>
</table><br><br>



<table>
    <tr>
    <td class="detailheading onlymainheading"><h4>3. Documentary and Information Requirements:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><br><?= $standardReport['required_documents'] ?></td>
    </tr>
</table><br><br>



<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>4. Commercial Terms and Conditions</h4></td>
    </tr>
</table>
<table>
    <!-- VatInTotalValuationFee -->
    <tr><br>
        <td class="detailheading"><h4>4.1. Fee</h4></td>
    </tr>
    <tr>
        <td class="detailtext">
            Our total valuation fee for the subject properties will be:<br>
            <?php 
            // foreach ($toe_fee_building_totals as $key1 => $toe_fee_builiding) {
            //     echo 'AED '.number_format($toe_fee_builiding['toe_fee_sum'], 2);
            // }
            ?>
            AED <?= number_format($toe_fee_total, 2) ?>/= <br>
            (<?= $totalValuationFeeInWORDS ?>) <br>
            <?= ($total_discount > 0) ?
                ($aprvd_total_discount > 0) ?
                    'Total Discount (-) = ' . $aprvd_total_discount_amount . '<br>' :
                    'Total Discount (-) = ' . $total_discount_amount . '<br>' : '' ?>
            Net Valuation Fee = <?=
            ($aprvd_total_discount_amount <> null) ?
                number_format($toe_fee_total - $aprvd_total_discount_amount, 2) :
                number_format($toe_fee_total - $total_discount_amount, 2) ?><br>
            <?php if ($urgencyfee['value'] > 0) {
                $total_net_fee = 1;
                ?>
                Urgency Fee (+)= <?=
                ($aprvd_urgencyfee <> null) ?
                    number_format($aprvd_urgencyfee['amount'], 2) :
                    number_format($urgencyfee['amount'], 2)
                ?><br>

            <?php } ?>

            <?php if ($total_net_fee > 0) { ?>
                Total Net Valuation Fee = <?=
                ($aprvd_netValuationFee <> null) ?
                    number_format($aprvd_netValuationFee, 2) :
                    number_format($netValuationFee, 2) ?><br>
            <?php } ?>
            <?= ($model->client->vat == 1 AND $VatInTotalValuationFee > 0) ?
                ($aprvd_VatInTotalValuationFee <> null && $aprvd_VatInTotalValuationFee > 0) ?
                    'plus 5% VAT = ' . number_format($aprvd_VatInTotalValuationFee, 2) :
                    'plus 5% VAT = ' . number_format($VatInTotalValuationFee, 2) : 'plus 0% VAT' ?>
            . <br><br>
            Total Fee = including VAT will therefore be:<br>
            AED <?= ($aprvd_finalFeePayable <> null)? $aprvd_finalFeePayable : $finalFeePayable ?>/=<br>
            (<?= ($aprvd_finalFeePayableInWords <> null)? $aprvd_finalFeePayableInWords : $finalFeePayableInWords ?> Dirhams Only)
        </td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td colspan="2" class="detailheading"><h4>4.2. Payment Method</h4></td>
        <!-- <td class="detailheading"></td> -->
    </tr>
    <tr>
        <td colspan="2" class="detailtext"><?= $standardReport['payment_method'] ?></td>
        <!-- <td class="detailtext"></td> -->
    </tr>
    <tr>
        <td class="detailtext" style="width: 80px;">Bank</td>
        <td class="detailtext"><?= $bankName ?></td>
    </tr>
    <tr>
        <td class="detailtext" style="width: 80px;">IBAN</td>
        <td class="detailtext"><?= $iban ?></td>
    </tr>
    <tr>
        <td class="detailtext" style="width: 80px;">Our TRN is</td>
        <td class="detailtext"><?= $trn ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>4.3. Payment Terms</h4></td>
    </tr>
    <tr><?php
        if($mainTableData['advance_payment_terms'] == '50%'){ ?>
            <td class="detailtext">The fee is payable <?= $mainTableData['advance_payment_terms'] ?> in advance (Payment
                Structure). The remaining 50% is to be paid within 3 days after releasing the draft valuation report to the client unconditionally. Both advance and remaining fees are non-refundable under any circumstances.<br>Note:
                <?= $standardReport['payment_terms'] ?>
            </td>
        <?php }else{?>
            <?php if($model->id == 3482){ ?>
                <td class="detailtext">The fee is payable <?= $mainTableData['advance_payment_terms'] ?> in advance (Payment Structure). The remaining 75% is to be paid before releasing the draft report to the client unconditionally.Both advance and remaining fees are non-refundable under any circumstances.<br>Note:
                    <?= $standardReport['payment_terms'] ?>
                </td>

                <?php }else{ ?>


            <td class="detailtext">The fee is payable <?= $mainTableData['advance_payment_terms'] ?> in advance (Payment
                Structure). The fee is non-refundable.<br>Note:
                <?= $standardReport['payment_terms'] ?>
            </td>
                <?php } ?>
        <?php } ?>


    </tr>
</table><br><br>


<table>
    <tr>
        <td class="detailheading"><h4>4.4. Timings</h4></td>
    </tr>
    <tr>
        <td class="detailtext">The delivery time for this assignment will be <?= $totalTurnAroundTime ?> working days
            from and after the:<?= $standardReport['timings'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>4.5. Limitations on Liability</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['limitions_on_liabity'] ?></td>
    </tr>
</table><br><br>


<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>5. Identification and Status of the Surveyor</h4></td>
    </tr>
</table>

<table>
    <tr>
        <br>
        <td class="detailtext sixpoints"><h4>5.1. Surveyor Name </h4></td>
        <td class="detailtext"><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?>
            <br>(referred to in this document as Surveyor).
        </td>
    </tr>

    <tr>
        <br>
        <td class="detailtext sixpoints"><h4>5.2. Surveyor Firm</h4></td>
        <td class="detailtext"><?= $branchCompany; ?>
            <br>(referred to in this document as “Firm”).
        </td>
    </tr>

    <tr>
        <br>
        <td class="detailtext sixpoints"><h4>5.3. Surveyor Qualifications</h4></td>
        <td class="detailtext"><?= $valuerOther['valuer_qualifications'] ?>
        </td>
    </tr>

    <tr>
        <br>
        <td class="detailtext sixpoints"><h4>5.4. Surveyor Status</h4></td>
        <td class="detailtext"><?= str_replace('Valuer','Surveyor',$valuerOther['valuer_status']) ?></td>
    </tr>
</table><br><br>


<table>
    <tr>
        <td class="detailheading"><h4>5.5. Surveyor Experience and Expertise</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= str_replace('Valuer','Surveyor',$valuerOther['valuer_experience_expertise']) ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>5.6. Surveyor Duties and Supervision</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['valuer_duties_supervision'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>5.7. Internal/External Status of the Surveyor</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['internal_external_status_of_the_valuer'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>5.8. Previous Involvement and Potential Conflict of Interest</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['previous_involvement_and_conflict_of_interest'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>5.9. Declaration of Independence and Objectivity</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['decleration_of_independence_and_objectivity'] ?></td>
    </tr>
</table><br><br>


<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>6. Assumption, Limitation and Consideration </h4></td>
    </tr>
</table><br><br>



<?php 


    // Initialize variables
    $subpoint1 = 1;
    $subpoint2 = 2;
    $subpoint3 = 3;
    $subpoint4 = 4;
    $subpoint5 = 5;
    $subpoint6 = 6;
    $subpoint7 = 7;
    $subpoint8 = 8;

    $subpoints = [
        6 => &$subpoint1,
        7 => &$subpoint2,
        8 => &$subpoint3,
        9 => &$subpoint4,
        10 => &$subpoint5,
        11 => &$subpoint6,
        12 => &$subpoint7,
        13 => &$subpoint8,
    ];

    // Check for presence of each service
    foreach ($subpoints as $service => &$subpoint) {
        if (in_array($service, $uniqueServices)) {
            $subpoint = 1;
        }
    }

    // Adjust subpoint values for all combinations
    if (count(array_intersect($uniqueServices, [6, 7, 8, 9, 10, 11, 12, 13])) == 8) {
        $subpoint1 = 1;
        $subpoint2 = 2;
        $subpoint3 = 3;
        $subpoint4 = 4;
        $subpoint5 = 5;
        $subpoint6 = 6;
        $subpoint7 = 7;
        $subpoint8 = 8;
    } else {
        $i = 1;
        foreach ($uniqueServices as $service) {
            if (isset($subpoints[$service])) {
                $subpoints[$service] = $i;
                $i++;
            }
        }
    }


    foreach ($uniqueServices as $service) {     
        // RFS - get from master file
        if($service == 6){ 
             echo str_replace("{subpoint1}", $subpoint1, $standardReport['assumptions_rfs']);
        }
        // SCS - get from master file
        if($service == 7){ 
             echo str_replace("{subpoint2}", $subpoint2, $standardReport['assumptions_scs']);
        }
        // BCA - get from master file
        if($service == 8){ 
             echo str_replace("{subpoint3}", $subpoint3, $standardReport['assumptions_bca']);
        }            
        // RICA - get from master file
        if($service == 9){ 
             echo str_replace("{subpoint4}", $subpoint4, $standardReport['assumptions_rica']);
        }            
        // TS - get from master file
        if($service == 10){ 
             echo str_replace("{subpoint5}", $subpoint5, $standardReport['assumptions_ts']);
        }            
        // ERR - get from master file
        if($service == 11){ 
             echo str_replace("{subpoint6}", $subpoint6, $standardReport['assumptions_err']);
        }            
        // ARCT - get from master file
        if($service == 12){ 
             echo str_replace("{subpoint7}", $subpoint7, $standardReport['assumptions_arct']);
        }            
        // RFV - get from master file
        if($service == 13){ 
             echo str_replace("{subpoint8}", $subpoint8, $standardReport['assumptions_rfv']);
        }            
    }    

?>


<br><br>


<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>7. Standared Terms And Condition</h4></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.1 Terms versus Agreement</h4>
        </td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['terms_versus_agreement'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.2 Providing the Service</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['providing_the_service'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.3. Liability</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['liability'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.4 Remuneration</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['remuneration'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.5 Communication</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['communication'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.6 Estimates</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['estimates'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.7 Confidentiality</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['confidentiality'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.8 Termination</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['termination'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.9 Miscellaneous</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['miscellaneous'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.10 Force Majeure</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['force_majeure'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.11. Waiver and Severance</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['waiver_and_severance'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.12. No Responsibility</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['no_responsibility'] ?> </td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.13 No Partnership</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['no_partnership'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.14 Conflict</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['conflict'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.15 Dispute Resolution</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['dispute_resolution'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.16 Complaints Handling Procedure</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['complaints_handling_procedure'] ?></td>
    </tr>
</table><br><br>




<br pagebreak="true"/><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>8. Acceptance of Terms of Engagement:</h4></td>
    </tr>
    <tr><br>
        <td class="detailtext"><?= $standardReport['acceptance_of_terms_of_engagement'] ?></td>
    </tr>
</table><br><br>


<style>
    td.centre {
        text-align: left;
        /*vertical-align: middle;*/
        font-size: 11px;
    }
</style>

<table>
    <tr>

        <td colspan="5" class="centre">Signed (Firm)</td>
        <!-- <td colspan="1" class="centre"></td> -->
        <td colspan="1" class="centre"></td>
        <td colspan="4" class="centre">Signed (Client)</td>


    </tr>
    <tr>

        <?php if ( $model->valuer_id == 21 ) {  
            $sigImg = \yii\helpers\Url::to('@web/images/lksign.png'); ?>
            <td colspan="5" class="centre detailtext">
                <img src="<?php echo $sigImg ?>" style="height: 40px; ">    
                <br><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?><br><?= $valuerJobName['title'] ?> 
            </td>
        <?php } else if ( $model->valuer_id == 111558 ) {
            $sigImg = \yii\helpers\Url::to('@web/images/ahmet_sig.jpeg'); ?>
            <td colspan="5" class="centre detailtext">
                <img src="<?php echo $sigImg ?>" style="height: 100px; ">
                <br><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?><br><?= $valuerJobName['title'] ?> 
            </td>    
        <?php } else if ( $model->valuer_id == 381 ) {
            $sigImg = \yii\helpers\Url::to('@web/images/feb_sig6537cbcf709e8.png');  ?>
            <td colspan="5" class="centre detailtext">
                <img src="<?php echo $sigImg ?>" style="height: 80px; ">
                <br><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?><br><?= $valuerJobName['title'] ?> 
            </td>    
        <?php } else if ( $model->valuer_id == 110821 ) {
            $sigImg = \yii\helpers\Url::to('@web/images/IMG_387765b2070e17fe2.jpg');  ?>
            <td colspan="5" class="centre detailtext">
                <img src="<?php echo $sigImg ?>" style="height: 60px; ">
                <br><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?><br><?= $valuerJobName['title'] ?> 
            </td>    
        <?php } ?>
        
        <!-- <td colspan="1" class="centre"></td> -->
        <td colspan="1" class="centre"></td>
        <td colspan="4" class="centre"></td>


    </tr>
    <br>


    <tr>


        <td colspan="5" class="centre">For and on behalf of
            <br><br><?= $branchCompany; ?>
        </td>
        <!-- <td colspan="1" class="centre"></td> -->
        <td colspan="1" class="centre"></td>
        <td colspan="4" class="centre">For and on behalf of
            <br><br><?= $clientName['title']; ?></td>

    </tr>
    <br><br>

    <!-- <tr>
         <td colspan="4" class="centre">Windmills Real Estate Valuation Services LLC</td>
         <td colspan="1" class="centre"></td>
         <td colspan="1" class="centre"></td>

         <td colspan="4" class="centre"></td>

     </tr>-->
</table>
