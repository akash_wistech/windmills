<?php

use app\models\CrmReceivedProperties;

$receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
$quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
$toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
$toe_fee_total_ad = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');

if($model->payment_status == 1){


    $quotation_fee_total= number_format(($quotation_fee_total * ('0.'.$model->first_half_payment)), 2, '.', '');
    $toe_fee_total= number_format(($toe_fee_total * ('0.'.$model->first_half_payment)), 2, '.', '');
}


//same as TOE

$relative_discount=0;
$no_of_property_discount=0;
$first_time_discount=0;
$general_discount=0;

$total_no_of_prperties = count($receivedProperties);





$total_discount=0;
$total_discount_amount=0;
$discount = 0;
$netValuationFee = $toe_fee_total-$discount;
//$netValuationFee_ad = $toe_fee_total-$discount;
//$netValuationFee_ad = $toe_fee_total_ad;
if($model->no_of_property_discount > 0) {
    $no_of_property_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $toe_fee_total);
    $total_discount= $total_discount + $model->no_of_property_discount;
    $total_discount_amount = $total_discount_amount + $no_of_property_discount_amount;
}
if($model->same_building_discount > 0) {
    $same_building_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $toe_fee_total);
    $total_discount= $total_discount + $model->same_building_discount;
    $total_discount_amount = $total_discount_amount + $same_building_discount_amount;
}
if($model->first_time_discount > 0) {
    $first_time_fee_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $toe_fee_total);
    $total_discount= $total_discount + $model->first_time_discount;
    $total_discount_amount = $total_discount_amount + $first_time_fee_discount_amount;
}
$netValuationFee = $netValuationFee - $total_discount_amount;

$discount_quotations = 0;
if ($model->relative_discount_toe!=null) {
    $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total,$model->relative_discount_toe);
    $netValuationFee = $netValuationFee - $discount;


}
if ($model->relative_discount!=null) {
    $discount_quotations =  yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total,$model->relative_discount);
}

$discount_net_fee =$netValuationFee;

//urgencyfee check
if ($model->tat_requirements > 0) {
    $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $netValuationFee);
    $netValuationFee = $netValuationFee + $urgencyfee['amount'];
}

//Advance Payment Terms
if ($model->client->client_type != 'bank') {
    $advance_payment_terms_data = explode('%', $model->advance_payment_terms);

    $advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($advance_payment_terms_data[0], $netValuationFee);
}else{
    $advance_payment_terms['amount'] = 0;
    $advance_payment_terms['value'] = 0;
}
/*if($model->payment_status == 1){


    $netValuationFee= number_format(($netValuationFee * ('0.'.$model->first_half_payment)), 2, '.', '');
}*/

$netValuationFee = $netValuationFee + $advance_payment_terms['amount'];


if($model->client->vat== 1) {
    $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
}

$finalFeePayable = $netValuationFee+$VAT;
/*echo $finalFeePayable;
die;*/
//End same as TOE

$clientAddress = \app\models\Company::find()->select(['address'])->where(['id'=>$model->client_name])->one();

?>


<br><br>

<style>
  .box1{
    background-color: #E3F2FD;
    width: 90px;
/*margin-top: -20px*/
border-bottom: 0.5px solid #1E88E5;
}
.box2{
  background-color: #1E88E5;
  width: 90px;
  border-bottom: 0.5px solid #1E88E5;
}
.box3{
  background-color: #E3F2FD;
  width: 90px;
  border-bottom: 0.5px solid #1E88E5;
/* margin-right: 100px; */
}
/*.box4{
background-color: #BBDEFB;
width: 70px;
/*margin-top: -20px*/
}*/
</style>

<!-- <style>
p.line {
  min-height: 2.5px !important;
  background-color: #1E88E5;
}
</style> -->

<!-- <p class="line"></p> -->


<hr style="height: 1.2px;">
<table>
  <thead>
    <tr><br>
      <td colspan="2" class="detailtexttext first-section" style="background-color: ;">
        <br><span class="size-8 airal-family">ADDRESS</span>
          <?php
          if ($model->id ==1896 || $model->id== 1945) {?>
          <br><span>AL HAMAD BLDG CONT. CO. L.L.C</span>
          <?php }else{?>
        <br><span><?= $model->client->title ?></span>
          <?php } ?>
          <?php if ($model->id ==1896 || $model->id== 1945) {?>

          <?php }else{?>
              <br><span><?= $clientAddress->address ?></span>
          <?php } ?>

          <?php
          if ($model->client->id !=9167) {?>
        <br><span>UAE.</span>
              <?php
          }
          ?>
         <?php if ($model->id ==1896 || $model->id== 1945) {?>
          <br><span>TRN: 100049814500003</span>
          <?php
          }
          ?>
      </td>
      <td></td>
      <td class="box1 text-dec airal-family">
        <p style="color: #29B6F6">
          DATE<br>
          <?= date('d/m/Y') ?>
          <br>
        </p>
      </td>
      <td class="box2 text-dec airal-family">
        <p style="color: white;">
          TOTAL<br>
          <b>AED <?= number_format($finalFeePayable,2) ?></b>
        </p>
      </td>

      <td class="box3 text-dec airal-family" style="margin-right:10px;">
        <p>

        </p>
      </td>
    </tr>
  </thead>
</table><br><br>








<table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <hr style="height: 0.2px;">
    <tr style="background-color: ;">
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align:left; border-bottom: 0.2px solid black;">DATE</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; width:150px; border-bottom: 0.2px solid black;">DESCRIPTION</td>
      <td class="airal-family" style="width: 100px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">TAX</td>
      <td class="airal-family" style="width: 50px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">QTY</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">RATE</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align:right; border-bottom: 0.2px solid black;">AMOUNT</td>
    </tr>
  </thead>

  <tbody>
    <?php
    $i=1;
    if($receivedProperties <> null && !empty($receivedProperties)) {
      foreach($receivedProperties as $key => $property){

          if($model->payment_status == 1){


              $property->toe_fee= number_format(($property->toe_fee * ('0.'.$model->first_half_payment)), 2, '.', '');
          }


        ?>
        <tr style="background-color: ;">
          <td class="airal-family" style="font-size:9px;"><?= $property->instruction_date; ?></td>
          <td class="airal-family" style="font-size:9px; width:150px;"><?= $property->building->title; ?></td>
          <td class="airal-family" style="width: 100px; font-size:9px; text-align: center;">SR Standard Rated (DXB)</td>
          <td class="airal-family" style="width: 50px; font-size:9px; text-align: center;">1</td>
          <td class="airal-family" style="font-size:9px; text-align: center;"><?= number_format($property->quotation_fee,2) ?></td>
          <td class="airal-family" style="font-size:9px; text-align:right;"><?= number_format($property->quotation_fee,2) ?></td>
        </tr>

        <?php
        $i++;
        if ($i<=count($receivedProperties)) {?>
          <hr style="height: 0.2px;">

          <?php
        }
      }
    }
    ?>
    <tr>
        <br pagebreak="true"/><br>

        <td></td>
      <td class="airal-family" style="font-size: 9px; width:180px;">Payment Terms : <?= $model->advance_payment_terms ?> Advance Payment</td>
    </tr>
  </tbody>
</table>
<hr style="height: 0.3px;">









<table cellspacing="1" cellpadding="2">
  <tr>
    <td ></td>
    <th colspan="2" class="airal-family" style="font-size:9px; color: #039BE5;">SUBTOTAL</th>
    <td class="airal-family" style="font-size:9px; text-align:right;"><?= number_format($toe_fee_total,2) ?></td>
  </tr>
    <?php
    if ($model->no_of_property_discount>0) {?>
    <tr>
        <td></td>
        <th colspan="2" class="airal-family" style="font-size:9px; color: #039BE5;">Multiple Properties Discount - (<?= $model->no_of_property_discount ?>) %</th>
        <td class="airal-family" style="font-size:9px; text-align:right;">- <?= number_format($no_of_property_discount_amount,2) ?></td>
    </tr>
    <?php } ?>

    <?php
    if ($model->same_building_discount>0) {?>
        <tr>
            <td></td>
            <th colspan="2" class="airal-family" style="font-size:9px; color: #039BE5;">Number Of Units In The Same Building Discount - (<?= $model->same_building_discount ?>) %</th>
            <td class="airal-family" style="font-size:9px; text-align:right;">- <?= number_format($same_building_discount_amount,2) ?></td>
        </tr>
    <?php } ?>

    <?php
    if ($model->first_time_discount>0) {?>
        <tr>
            <td></td>
            <th colspan="2" class="airal-family" style="font-size:9px; color: #039BE5;">First Time Discount - (<?= $model->first_time_discount ?>) %</th>
            <td class="airal-family" style="font-size:9px; text-align:right;">- <?= number_format($first_time_fee_discount_amount,2) ?></td>
        </tr>
    <?php } ?>


    <?php
  if ($model->relative_discount>0) {
    if ($model->relative_discount =='base-fee') {?>
      <tr style="background-color: ">
        <td ></td>
        <th colspan="2" class="airal-family" style="font-size:9px;  color: #039BE5;">Base Fee Discount</th>
        <td class="airal-family" style="text-align:right; font-size:9px;">- <?= number_format($discount,2) ?></td>
      </tr>
      <?php
    } else{?>



      <tr style="background-color: ">
        <td ></td>
        <th colspan="2" class="airal-family" style="font-size:9px;  color: #039BE5;"><?= $model->relative_discount_toe ?>% Discount</th>
        <td class="airal-family" style="text-align:right; font-size:9px;">- <?= number_format($discount,2) ?></td>
      </tr>
      <?php
    }
    ?>
    <?php
  }
  ?>

  <tr >
    <td ></td>
    <th colspan="2" class="airal-family" style="font-size:9px;  color: #039BE5;">Net Fee</th>
    <td class="airal-family" style="text-align:right; font-size:9px;"><?= number_format($discount_net_fee,2) ?></td>
  </tr>
    <?php
    if ($urgencyfee['value']>0) {?>
        <tr>
            <td></td>
            <th colspan="2" class="airal-family" style="font-size:9px; color: #039BE5;">Urgency Fee + (<?= $urgencyfee['value'] ?>) %</th>
            <td class="airal-family" style="font-size:9px; text-align:right;">+ <?= number_format($urgencyfee['amount'],2) ?></td>
        </tr>
    <?php } ?>

    <?php
    if ($advance_payment_terms['value']>0) {?>
        <tr>
            <td></td>
            <th colspan="2" class="airal-family" style="font-size:9px; color: #039BE5;">Advance Payment Fee + (<?= $advance_payment_terms['value'] ?>) %</th>
            <td class="airal-family" style="font-size:9px; text-align:right;">+ <?= number_format($advance_payment_terms['amount'],2) ?></td>
        </tr>
    <?php } ?>

  <tr >
    <td ></td>
      <?php if($model->client->vat == 1){ ?>
      <th colspan="2" class="airal-family" style="font-size:9px;  color: #039BE5;">VAT TOTAL</th>
      <td class="airal-family" style="text-align:right; font-size:9px;"><?= number_format($VAT,2) ?></td>
     <?php }else{?>
          <th class="airal-family" style="font-size:9px;  color: #039BE5;">No VAT</th>
          <td class="airal-family" style="text-align:right; font-size:9px;">0</td>
      <?php } ?>

  </tr>

  <tr>
    <td ></td>
    <th colspan="2" class="airal-family" style="font-size:10px; border-top: 0.3px solid grey; border-bottom: 0.3px solid #64B5F6; color: #039BE5; padding-bottom: 20px;">TOTAL</th>
    <td class="airal-family" style="text-align:right; border-top: 0.3px solid grey; border-bottom: 0.3px solid #64B5F6; font-size:16px; color: #039BE5;">AED <?= number_format($finalFeePayable) ?></td>
  </tr>
  <tr>
    <td colspan="3"></td>
    <td class="airal-family" style="text-align:right; font-size:10px; color: #039BE5;">THANK YOU.</td>
  </tr>
</table>







<br><br>
<table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <tr style="background-color: ;">
      <td class="airal-family" style="font-size:10px; text-align:left;">Accepted By</td>
      <td class="airal-family" style="width: 90px; font-size:10px; text-align:left;">Accepted Date</td>
    </tr>
  </thead>
</table>


<style>
  .airal-family{
    font-family: Arial, Helvetica, sans-serif;
  }
</style>


<style>

  .text-dec{
    font-size: 10px;
    text-align: center;
  }

/*td.detailtext{*/
/*background-color:#BDBDBD; */
/*font-size:9px;*/
/*border-top: 0.4px solid grey;*/
/*}*/

td.detailtexttext{
/* background-color:#EEEEEE; */
font-size:11px;
}
th.detailtext{
/* background-color:#BDBDBD; */
font-size:9px;
border-top: 1px solid black;
}
/*.border {
border: 1px solid black;
}*/

th.amount-heading{
  color: #039BE5;
}
td.amount-heading{
  color: #039BE5;
}
th.total-due{
  font-size: 16px;
}
td.total-due{
  font-size: 16px;
}
span.size-8{
  font-size: 10px;
  font-weight: bold;
}
td.first-section{
  padding-left: 10px;
/* background-color: black; */
}
</style>

