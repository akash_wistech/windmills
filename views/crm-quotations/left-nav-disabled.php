
<style>
  .fade {
    opacity: 1;
  }

  .nav-tabs.flex-column .nav-link.active {
    background-color: #007bff;
    color: white;
    font-weight: bold;
  }

  .disabled-link {
    color: #007bff !important; 
    cursor:pointer;
    cursor: not-allowed !important;
  }
  .disabled-link:hover{
    color: grey;
  }

</style>

<div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">

  <a class="nav-link active" id="vert-tabs-home-tab" data-toggle="pill" href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home" aria-selected="false">Enter Inquiry/RFQ Details</a>

  <a class="nav-link disabled-link" aria-selected="false">Enter Assignment Details</a>

  <a class="nav-link active" style="background-color:#4f5962;" aria-selected="false">Quotation Calculations
  </a>

  <!-- <a class="nav-link disabled-link" aria-selected="false">Rview Fee Summary</a> -->

  <a class="nav-link disabled-link" aria-selected="false">View Quotation</a>

  <a class="nav-link disabled-link" aria-selected="false">Amend Quotation</a>

  <a class="nav-link disabled-link" aria-selected="false">Accept/Reject Quotation</a>

  <a class="nav-link active" style="background-color:#4f5962;" aria-selected="false">TOE Details</a>

  <a class="nav-link disabled-link" aria-selected="false">Accept/Reject TOE</a>

  <?php //if ($quotation->instruction_person <> null) { ?>
    <a class="nav-link disabled-link" aria-selected="false">Scan TOE </a>
  <?php //} ?>

  <a class="nav-link disabled-link" aria-selected="false">Payment Slip </a>

  <a class="nav-link disabled-link" target="_blank" aria-selected="false">Status Logs</a>


</div>