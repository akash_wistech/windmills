<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */


$this->title = Yii::t('app', 'Quotations');
$cardTitle = Yii::t('app', 'New Quotation');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle
?>
<div class="valuation-create">

    <?= $this->render('inquirers-details', [ //_form
        'model' => $model,
        'cardTitle' => $cardTitle,
        'existing_client_array' => $existing_client_array,
        'typeOfQuot' => $typeOfQuot,
    ]) ?>

</div>

