<?php
use app\models\ProposalMasterFile;
use app\models\Valuation;
use app\models\CrmReceivedProperties;

$mainTableData = yii::$app->quotationHelperFunctions->getMainTableDataNew($id);
$client = yii::$app->quotationHelperFunctions->getClientAddress($mainTableData['client_name']);
$properties = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id);
// echo "<pre>";
// print_r($properties);
// echo "</pre>";
// die();
$standardReport = Yii::$app->quotationHelperFunctions->standardReport();



$quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
$quotation_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('tat');
// dd($quotation_tat_total);



$relative_discount=0;
$no_of_property_discount=0;
$first_time_discount=0;
$general_discount=0;

$total_no_of_prperties = count($properties);
$relative_discount = $mainTableData['relative_discount'];


if($total_no_of_prperties > 5){
	$result = ProposalMasterFile::find()->where(['heading'=>'No Of Property Discount', 'sub_heading'=>6])->one();
	$no_of_property_discount =  $result['values'];
}else if($total_no_of_prperties > 0 && $total_no_of_prperties < 6){
	$result = ProposalMasterFile::find()->where(['heading'=>'No Of Property Discount', 'sub_heading'=>$total_no_of_prperties])->one();
	$no_of_property_discount =  $result['values'];
}

$clinet_first_time_check = Valuation::find()->where(['client_id'=>$model->client_name])->one();
if($clinet_first_time_check == null){
	$first_time_fee = ProposalMasterFile::find()->where(['heading'=>'First Time Discount', 'sub_heading'=>'first-time-discount'])->one();
	$first_time_discount =  $first_time_fee['values'];
}

$general_discount_fee = ProposalMasterFile::find()->where(['heading'=>'General Discount', 'sub_heading'=>'general-discount'])->one();
if($general_discount_fee <> null){
	$general_discount =  $general_discount_fee['values'];
}



// echo"<pre>"; print_r("relative_discount: ". $relative_discount); echo"</pre>"; 
// echo"<pre>"; print_r("no_of_property_discount: ". $no_of_property_discount); echo"</pre>"; 
// echo"<pre>"; print_r("first_time_discount: ". $first_time_discount); echo"</pre>"; 
// echo"<pre>"; print_r("general_discount: ". $general_discount); echo"</pre>"; 
// die;

$total_discount=0;
$total_discount_amount=0;
$discount = 0;

if ($relative_discount!=null) {
    $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($quotation_fee_total,$relative_discount);
    $total_discount= $total_discount + $relative_discount;
    $total_discount_amount = $total_discount_amount + $discount;
}

$netValuationFee = $quotation_fee_total-$discount;


if ($first_time_discount!=null) {
    $first_time_discount_rupee =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($quotation_fee_total,$first_time_discount);
    $netValuationFee =$netValuationFee-$first_time_discount_rupee;
    $total_discount= $total_discount + $first_time_discount;
    $total_discount_amount = $total_discount_amount + $first_time_discount_rupee;
}
if ($no_of_property_discount!=null) {
    $discount_no_of_properties =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($quotation_fee_total,$no_of_property_discount);
    $netValuationFee =$netValuationFee-$discount_no_of_properties;
    $total_discount= $total_discount + $no_of_property_discount;
    $total_discount_amount = $total_discount_amount + $discount_no_of_properties;
}



if ($general_discount!=null) {
    $general_discount_rupee =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($quotation_fee_total,$general_discount);

    $netValuationFee =$netValuationFee-$general_discount_rupee;
    $total_discount= $total_discount + $general_discount;
    $total_discount_amount = $total_discount_amount + $general_discount_rupee;
}

if($client['vat']== 1) {
	$VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
}

$finalFeePayable = $netValuationFee+$VAT;







?>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="datamain"><?= date("F j, Y", strtotime(date("Y-m-d"))); ?></td>
    </tr><br>
    <tr>
        <td class="datamain">
            <?= $client['title'] ?>
        </td>
    </tr>
    <?php  
	if ($client['address']!=null) {?>
    <tr>
        <td class="datamain">
            <?= $client['address'] ?>
        </td>
    </tr><br>
    <?php 
	}
?>


    <tr>
        <td class="data">
            <?= $standardReport['quotation_last_paragraph'] ?>
        </td>
    </tr>
</table>

<style>
.table tr .datamain {
    font-size: 10px;
}
</style>

<br pagebreak="true" />

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">
            Date
        </td>
    </tr>
    <tr>
        <td class="data">
            <?= date("F j, Y", strtotime(date("Y-m-d"))) ?>
        </td>
    </tr>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">
            Quotation Reference No.
        </td>
    </tr>
    <tr>
        <td class="data">
            <?= $mainTableData['reference_number'] ?>
        </td>
    </tr>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">
            Client
        </td>
    </tr>
    <tr>
        <td class="data">
            <?= $client['title'] ?>
        </td>
    </tr>

    <?php  
	if ($client['address']!=null) {?>
    <tr>
        <td class="datamain">
            <?= $client['address'] ?>
        </td>
    </tr>
    <?php 
	}
?>
    <?php
    if ($mainTableData['client_name'] != 9167) {?>
    <!--<tr>
		<td class="data">
			Dubai, United Arab Emirates.
		</td>
	</tr>-->
    <?php
    }
    ?>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">
            Service
        </td>
    </tr>
    <tr>
        <td class="data"><?= yii::$app->appHelperFunctions->scopeOfWorkArr[$mainTableData['scope_of_service']]?>
            <!--Valuation of Real Estate Interests of Subject Properties-->
        </td>
    </tr>
</table>

<style>
.table tr .heading {
    border: 1px solid black;
    font-size: 11px;
    color: #0091EA;
    text-indent: 5px;
}

.table tr .data {
    font-size: 10px;
    text-indent: 5px;
}
</style>

<br><br>

<table class="property-table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading" colspan="10">
            Subject Properties
        </td>
    </tr>


    <tr>
        <td class="property-heading" colspan="2" style="border-left: 1px solid black;">Properties</td>

        <td class="property-heading" colspan="2">Property Address</td>
        <td class="property-heading" colspan="2">Property Description</td>
        <td class="property-heading" colspan="2">Turn Around Time</td>
        <td class="property-heading" colspan="2" style="border-right: 1px solid black;">Fee</td>
    </tr>
    <br>
    <?php
	$totalValuationFee = 0;
	$totalTurnAroundTime = 0;
	if ($properties!=null) {

		foreach ($properties as $ky => $value) {
            $property = \app\models\CrmReceivedProperties::find()->where(['id' => $value['id']])->one();
            $address = '';
            if($value['unit_number'] != ''){
                $address= $value['unit_number'].', '.$value['street'].', '.Yii::$app->appHelperFunctions->emiratedListArr[$property->building->city];
            }else{
                $address= $value['street'].', '.Yii::$app->appHelperFunctions->emiratedListArr[$property->building->city];
            }

            $prperty_description = '';

           



	?>
    <tr>
        <td class="data" colspan="2" style="text-indent: 5px;"><?= $property->building->title ?>,<br>
            <?=  Yii::$app->appHelperFunctions->getPropertiesCategoriesListArr()[$value['property_category']] ?>
        </td>
        <td class="data" colspan="2"><?=  $address?></td>
        <td class="data" colspan="2" style="text-indent: 5px;"></td>

        <td class="data" colspan="2" style="text-indent: 5px;"><?=  $value['tat'] ?></td>
        <td class="data" colspan="2" style="text-indent: 5px;"><?=  $value['quotation_fee'] ?></td>
    </tr><br>
    <?php
			$totalValuationFee += $value['quotation_fee'];
			$totalTurnAroundTime += $value['tat'];
		}
	}
	?>
</table>



<style>
.property-table tr .heading {
    border-top: 1px solid black;
    border-left: 1px solid black;
    border-right: 1px solid black;
    font-size: 11px;
    color: #0091EA;
    text-indent: 5px;
}

.property-table tr .property-heading {
    font-size: 10px;
    text-indent: 5px;
    border-top: 1px solid black;
    border-bottom: 1px solid black;
    /*text-align:center;*/
}

.property-table tr .data {
    font-size: 10px;

}
</style>

<br><br>

<table cellspacing="1" cellpadding="2" class="bill-table">


    <tr>
        <td class="bill-data" style="border-left: 1px solid black;">Total Number of Properties </td>
        <td class="bill-data"><?= $mainTableData['no_of_properties'] ?></td>
        <td class="bill-data">Total Valuation Fee</td>
        <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($totalValuationFee,2) ?></td>
    </tr>

    <tr>
        <td class="bill-data" style="border-left: 1px solid black;">Turn Around Time</td>
        <td class="bill-data"><?= $totalTurnAroundTime ?> Days</td>

        <td class="bill-data"></td>
        <td class="bill-data" style="border-right: 1px solid black;"></td>
    </tr>

    <tr>
        <td class="bill-data" style="border-left: 1px solid black;">Payment Terms</td>
        <td class="bill-data"><?= $mainTableData['advance_payment_terms'] ?> Advance</td>

        <td class="bill-data"></td>
        <td class="bill-data" style="border-right: 1px solid black;"></td>
    </tr>


	<?php if ($relative_discount>0) {?>
	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
		<?php
		if ($relative_discount>0) {
		if ($mainTableData['relative_discount']=='base-fee') {?>
        <td class="bill-data">Relationship Discount</td>
        <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($discount,2) ?></td>
        <?php
		}else{?>
        <td class="bill-data">Relationship Discount (<?= $mainTableData['relative_discount'] ?>%)</td>
        <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($discount,2) ?></td>
        <?php
			}
		}else{?>
        <td class="bill-data">Relationship Discount (0%)</td>
        <td class="bill-data" style="border-right: 1px solid black;">0</td>
        <?php
		}
		?>
    </tr>
	<?php } ?>

<?php if ($discount_no_of_properties>0) {?>
	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
		<?php if ($discount_no_of_properties>0) { ?>
			<td class="bill-data">Multiple Properties D (<?= $no_of_property_discount ?>%)</td>
			<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($discount_no_of_properties,2) ?></td>
		<?php
		}else{?>
			<td class="bill-data">Properties Discount D (0%)</td>
       		<td class="bill-data" style="border-right: 1px solid black;">0</td>
		<?php
		}
		?>
    </tr>
	<?php } ?>

	<?php if($general_discount > 0){ ?>
	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
        <?php if($general_discount > 0){ ?>
			<td class="bill-data">General Discount (<?= $general_discount ?>%)</td>
			<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($general_discount_rupee,2) ?></td>
        <?php }else{ ?>
			<td class="bill-data">General Discount (0%)</td>
       		<td class="bill-data" style="border-right: 1px solid black;">0</td>
        <?php } ?>
    </tr>
	<?php } ?>

	<?php if($first_time_discount > 0){ ?>
	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
        <?php if($first_time_discount > 0){ ?>
			<td class="bill-data">First Time Discount (<?= $first_time_discount ?>%)</td>
			<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($first_time_discount_rupee,2) ?></td>
        <?php }else{ ?>
			<td class="bill-data">First Time Discount (0%)</td>
       		<td class="bill-data" style="border-right: 1px solid black;">0</td>
        <?php } ?>
    </tr>
	<?php } ?>

	
	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
		<?php if($total_discount>0){?>
			<td class="bill-data">Total Discount (<?= $total_discount ?>%)</td>
			<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($total_discount_amount,2) ?></td>
		<?php }else{ ?>
			<td class="bill-data">Total Discount (0%)</td>
			<td class="bill-data" style="border-right: 1px solid black;"><?= number_format(0,2) ?></td>
		<?php } ?>
    </tr>

	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
		<td class="bill-data">Net Valuation Fee</td>
		<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($netValuationFee,2) ?></td>
    </tr>


    <tr>
        <!--		<td class="bill-data" style="border-left: 1px solid black;">Quotation Expiry Date</td>
		<td class="bill-data"><?/*= date("F j, Y", strtotime($mainTableData['expiry_date'])); */?></td>-->

        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
        <?php if($client['vat'] == 1){ ?>
        <td class="bill-data">5% VAT</td>
        <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($VAT,2) ?></td>
        <?php }else{ ?>
        <td class="bill-data">VAT</td>
        <td class="bill-data" style="border-right: 1px solid black;">0</td>
        <?php } ?>
    </tr>

    <tr>
        <td class="bill-data" style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
        <td class="bill-data" style="border-bottom: 1px solid black;"></td>
        <td class="bill-data" style="border-bottom: 1px solid black;">Final Fee Payable </td>
        <td class="bill-data" style="border-right: 1px solid black;border-bottom: 1px solid black;"><?= number_format($finalFeePayable,2) ?></td>
    </tr>


</table>

<style>
.bill-table tr .bill-data {
    font-size: 10px;
    text-indent: 5px;
    color: #0091EA;
    border-top: 1px solid black;
}
</style>