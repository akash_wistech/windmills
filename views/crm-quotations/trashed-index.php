<?php

use yii\helpers\Html;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CrmQuotationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Crm Trashed Quotations');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
//if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    // $createBtn=true;
//}
//if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    // $actionBtns.='{update}';
    $actionBtns.='{delete}';
//}
//if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
    // $actionBtns.='{status}';
//}
$actionBtns .= '{step_0}';
$actionBtns .= '{move_to_active_quotations}';
$actionBtns .= '{revise}';
// $createBtn=true;

$dAlertTxt = 'Do You Want To Delete This Quotation! This action cannot be undone!';
$dAlertTxtMove = 'Do You Want To Active This Quotation!';

$this->registerJs('
$("body").on("click", ".delete-btn", function (e) {
    e.preventDefault();
    href = $(this).data("url")
    swal({
      title: "'.Yii::t('app', $dAlertTxt).'",
      html: "'.Yii::t('app','').'",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#47a447",
      confirmButtonText: "'.Yii::t('app','Yes').'",
      cancelButtonText: "'.Yii::t('app','Cancel').'",
      },function(result) {
        if (result) {
         window.location.replace(href);
       }
       });
       });


    $("body").on("click", ".move-to-active-btn", function (e) {
    e.preventDefault();
    href = $(this).data("url")
    swal({
      title: "'.Yii::t('app', $dAlertTxtMove).'",
      html: "'.Yii::t('app','').'",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#47a447",
      confirmButtonText: "'.Yii::t('app','Yes').'",
      cancelButtonText: "'.Yii::t('app','Cancel').'",
      },function(result) {
        if (result) {
         window.location.replace(href);
       }
       });
       });

    $("body").on("click", ".del-all-btn", function (e) {
    e.preventDefault();
    
    swal({
      title: "'.Yii::t('app', 'Do you want to Permently Delete All Quotations').'",
      html: "'.Yii::t('app','').'",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#47a447",
      confirmButtonText: "'.Yii::t('app','Yes').'",
      cancelButtonText: "'.Yii::t('app','Cancel').'",
      },function(result) {
        if (result) {
            window.location.replace("'.Url::to(['crm-quotations/delete-all']).'");
       }
       });
       });
 
 
 
    $("body").on("click", ".ative-all-btn", function (e) {
    e.preventDefault();
    
    swal({
      title: "'.Yii::t('app', 'Do you want to Active All Quotations').'",
      html: "'.Yii::t('app','').'",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#47a447",
      confirmButtonText: "'.Yii::t('app','Yes').'",
      cancelButtonText: "'.Yii::t('app','Cancel').'",
      },function(result) {
        if (result) {
            window.location.replace("'.Url::to(['crm-quotations/active-all']).'");
       }
       });
       });
');

$deleteAllbtn = true;
$activeAllbtn = true;

?>

<div class="crm-quotations-index">
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'deleteAllbtn' => $deleteAllbtn,
        'activeAllbtn' => $activeAllbtn,
        'columns' => [
            'reference_number',

            [
                'attribute' => 'client_name',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                    return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],

            

            [
                'attribute' => 'client_type',
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:15%'],
                'label' => Yii::t('app', 'client Type'),
                'value' => function ($model) {
                    $value= Yii::$app->quotationHelperFunctions->clientType[$model->client->client_type];
                    return $value;
                },
                'contentOptions' => ['class' => ' pt-3 pl-2'],
                'filter' => Yii::$app->quotationHelperFunctions->clientType
            ],





             'client_reference',
            'client_customer_name',
            'no_of_properties',
            'toe_final_fee',
            'toe_final_turned_around_time',
            ['attribute' => 'id',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    $properties = \app\models\CrmReceivedProperties::find()->where(['quotation_id' => $model->id])->all();
    $city = '';
    if(!empty($properties)){
        $property = \app\models\CrmReceivedProperties::find()->where(['quotation_id' => $model->id, 'property_index' => 0])->one();
                 $city = $property->building->city;
        return Yii::$app->appHelperFunctions->emiratedListArr[$city];
    }


                },
            ],

            [
                'format' => 'raw',
                'attribute' => 'quotation_status',
                'label' => Yii::t('app', 'Quotation Status'),
                'value' => function ($model) {

                    $value=Yii::$app->crmQuotationHelperFunctions->quotationStatusListArrLabel[$model['quotation_status']];

    // target="_blank"
                    return Html::a($value, \yii\helpers\Url::to(['crm-quotation/step_0','id'=>$model->id]), [
                        'title' => $value,
                        // 'class' => 'dropdown-item text-1',
                        // 'data-pjax' => "0",
                    ]);
                },
                'contentOptions' => ['class' => ' pt-3 pl-2'],
                'filter' => Yii::$app->crmQuotationHelperFunctions->quotationStatusListArr
            ],

     /*   [
            'format' => 'raw',
            'headerOptions' => ['style' => 'width:12%'],
            'attribute' => 'status_change_date',
            'label' => Yii::t('app', 'Status Update Date'),
            'value' => function ($model) {
                // echo $model->status_change_date; die();
                if ($model->status_change_date<>null) {
                   return '<span class="badge grid-badge badge-primary my-3"> '.date('Y-m-d', strtotime($model->status_change_date)).'</span>';
                }

            },
        ],


        [
            'format' => 'raw',
            // 'headerOptions' => ['style' => 'width:12%'],
            'attribute' => 'inquiry_received_date',
            'label' => Yii::t('app', 'I-R Date'),
            'value' => function ($model) {
                // echo $model->status_change_date; die();
                if ($model->inquiry_received_date<>null) {
                   return '<span class="badge grid-badge badge-success my-3"> '.date('Y-m-d', strtotime($model->inquiry_received_date)).'</span>';
                }

            },
        ],

        [
            'format' => 'raw',
            'headerOptions' => ['style' => 'width:10%'],
            'attribute' => 'quotation_sent_date',
            'label' => Yii::t('app', 'Q-S Date'),
            'value' => function ($model) {
                // echo $model->status_change_date; die();
                if ($model->quotation_sent_date<>null) {
                   return '<span class="badge grid-badge badge-primary my-3"> '.date('Y-m-d', strtotime($model->quotation_sent_date)).'</span>';
                }

            },
        ],

        [
            'format' => 'raw',
            // 'headerOptions' => ['style' => 'width:12%'],
            'attribute' => 'toe_sent_date',
            'label' => Yii::t('app', 'ToeSend Date'),
            'value' => function ($model) {
                // echo $model->status_change_date; die();
                if ($model->toe_sent_date<>null) {
                   return '<span class="badge grid-badge badge-info my-3"> '.date('Y-m-d', strtotime($model->toe_sent_date)).'</span>';
                }

            },
        ],

        [
            'format' => 'raw',
            'headerOptions' => ['style' => 'width:10%'],
            'attribute' => 'payment_received_date',
            'label' => Yii::t('app', 'P-R Date'),
            'value' => function ($model) {
                // echo $model->status_change_date; die();
                if ($model->payment_received_date<>null) {
                   return '<span class="badge grid-badge badge-warning my-3"> '.date('Y-m-d', strtotime($model->payment_received_date)).'</span>';
                }

            },
        ],

        [
            'format' => 'raw',
            'headerOptions' => ['style' => 'width:12%'],
            'attribute' => 'toe_signed_and_received',
            'label' => Yii::t('app', 'Toe Signed & Recieved'),
            'value' => function ($model) {
                // echo $model->status_change_date; die();
                if ($model->toe_signed_and_received<>null) {
                    return '<span class="badge grid-badge badge-secondary my-3"> '.Yii::$app->crmQuotationHelperFunctions->getQuotationStatusListArr()[$model['toe_signed_and_received']].'</span>';
                }
            },
        ],

        [
            'format' => 'raw',
            'headerOptions' => ['style' => 'width:12%'],
            'attribute' => 'toe_signed_and_received_date',
            'label' => Yii::t('app', 'ToeSend & Recieve Date'),
            'value' => function ($model) {
                // echo $model->status_change_date; die();
                if ($model->toe_signed_and_received_date<>null) {
                   return '<span class="badge grid-badge badge-secondary my-3"> '.date('Y-m-d', strtotime($model->toe_signed_and_received_date)).'</span>';
                }
            },
        ],


        [
            'format' => 'raw',
            'headerOptions' => ['style' => 'width:10%'],
            'attribute' => 'on_hold_date',
            'label' => Yii::t('app', 'On-H Date'),
            'value' => function ($model) {
                // echo $model->status_change_date; die();
                if ($model->on_hold_date<>null) {
                   return '<span class="badge grid-badge badge-primary my-3"> '.date('Y-m-d', strtotime($model->on_hold_date)).'</span>';
                }
            },
        ],*/





            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
                'buttons' => [
                  /*  'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },*/
                    
                    'step_0' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Update'), $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    /*
                    'revise' => function ($url, $model) {
                        return Html::a('<i class="far fa-copy"></i> ' . Yii::t('app', 'Copy Quotation'), $url, [
                            'title' => Yii::t('app', 'Copy Quotation'),
                            'class' => 'dropdown-item text-1 sav-btn1',
                            'data-url' => $url,
                            'data-text' => 'Quotation will be Copied.',

                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'status' => function ($url, $model) {
                        if($model['status']==1){
                            return Html::a('<i class="fas fa-eye-slash"></i> '.Yii::t('app', 'Disable'), $url, [
                                'title' => Yii::t('app', 'Disable'),
                                'class'=>'dropdown-item text-1',
                                'data-confirm'=>Yii::t('app','Are you sure you want to disable this item?'),
                                'data-method'=>"post",
                            ]);
                        }else{
                            return Html::a('<i class="fas fa-eye"></i> '.Yii::t('app', 'Enable'), $url, [
                                'title' => Yii::t('app', 'Enable'),
                                'class'=>'dropdown-item text-1',
                                'data-confirm'=>Yii::t('app','Are you sure you want to enable this item?'),
                                'data-method'=>"post",
                            ]);
                        }
                    },*/

                    
                    'move_to_active_quotations' => function ($url, $model) {                        
                        return Html::a('<i class="fas fa-check"></i> '.Yii::t('app', 'Active'), "javascript:;", [
                            'title' => Yii::t('app', 'Move To Active'),
                            'class'=>'dropdown-item text-1 move-to-active-btn',
                            'data-url' => Url::toRoute(['crm-quotations/move-to-active', 'id' => $model->id]),
                            'data-pjax'=>"0",
                        ]);
                    },
                    
                    
                    'delete' => function ($url, $model) {                        
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), "javascript:;", [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1 delete-btn',
                            'data-url' => Url::toRoute(['crm-quotations/permently-delete', 'id' => $model->id]),
                            // 'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>
</div>