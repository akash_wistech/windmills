<?php
use app\models\ProposalMasterFile;
use app\models\Valuation;
use app\models\CrmReceivedProperties;
/*$test = yii::$app->quotationHelperFunctions->getPropertyPercentageAmount(4000,1000,1000);
echo $test;
die;*/


$mainTableData = yii::$app->quotationHelperFunctions->getMainTableDataNew($id);
$client = yii::$app->quotationHelperFunctions->getClientAddress($mainTableData['client_name']);
$properties = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id);
$multiScopeProperties = yii::$app->quotationHelperFunctions->getMultipleScopeProperties($id);


$standardReport = Yii::$app->quotationHelperFunctions->standardReport();
$quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
$quotation_fee_total_cal = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
$quotation_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('tat');





$relative_discount=0;
$no_of_property_discount=0;
$first_time_discount=0;
$general_discount=0;

$total_no_of_prperties = count($properties);
$relative_discount = $mainTableData['relative_discount'];




$total_discount=0;
$total_discount_amount=0;
$discount = 0;
if ($model->client->client_type != 'bank') {
//Advance Payment Terms
    $quotation_fee_total = $quotation_fee_total + $model->advance_payment_terms_final_amount;
}else{
    $advance_payment_terms['amount'] = 0;
    $advance_payment_terms['value'] = 0;
}
if($model->no_of_property_discount > 0) {
    $no_of_property_discount_amount = $model->no_of_property_discount_final_amount;
    $total_discount= $total_discount + $model->no_of_property_discount;
    $total_discount_amount = $total_discount_amount + $no_of_property_discount_amount;
}
// for approved no of property discount
if($model->aprvd_no_of_property_discount > 0) {
    $aprvd_no_of_property_discount_amount = $model->aprvd_no_of_property_discount_amount;
    $aprvd_total_discount= $aprvd_total_discount + $model->aprvd_no_of_property_discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_no_of_property_discount_amount;
}

if($model->same_building_discount > 0) {
    $same_building_discount_amount = $model->no_of_units_same_building_final_amount ;
    $total_discount= $total_discount + $model->same_building_discount;
    $total_discount_amount = $total_discount_amount + $same_building_discount_amount;
}
// for approved same building discount
if($model->aprvd_same_building_discount > 0) {
    $aprvd_same_building_discount_amount = $model->aprvd_same_building_discount_amount ;
    $aprvd_total_discount= $aprvd_total_discount + $model->aprvd_same_building_discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_same_building_discount_amount;
}

if($model->first_time_discount > 0) {
    $first_time_fee_discount_amount = $model->first_time_discount_final_amount;
    $total_discount= $total_discount + $model->first_time_discount;
    $total_discount_amount = $total_discount_amount + $first_time_fee_discount_amount;
}

// for approved first time bulding
if($model->aprvd_first_time_discount > 0) {
    $aprvd_first_time_discount_amount = $model->aprvd_first_time_discount_amount;
    $aprvd_total_discount= $aprvd_total_discount + $model->aprvd_first_time_discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_first_time_discount_amount;
}

if ($relative_discount!=null) {
    $discount =  $model->relationship_discount_final_amount;
    $total_discount= $total_discount + $relative_discount;
    $total_discount_amount = $total_discount_amount + $discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $discount;
}

$netValuationFee = $quotation_fee_total-$total_discount_amount;
$discount_net_fee = $netValuationFee;

// for approved fee


if($aprvd_total_discount_amount > 0){
    $aprvd_netValuationFee = $quotation_fee_total-$aprvd_total_discount_amount;
} else {
    $aprvd_netValuationFee = $quotation_fee_total-$total_discount_amount;
}
// $aprvd_netValuationFee = $quotation_fee_total-$aprvd_total_discount_amount;
$aprvd_discount_net_fee = $aprvd_netValuationFee;



//urgencyfee check
if ($model->tat_requirements > 0) {
    $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $netValuationFee);
    $netValuationFee = $netValuationFee + $model->tat_requirements_final_amount;

    // for approved
    $aprvd_urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $aprvd_netValuationFee);
    $aprvd_netValuationFee = $aprvd_netValuationFee + $model->aprvd_tat_requirements_final_amount;


}
// dd($model->tat_requirements_final_amount);


if($client['vat']== 1) {
	$VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
	$aprvd_VAT = yii::$app->quotationHelperFunctions->getVatTotal($aprvd_netValuationFee);
}

$finalFeePayable = $netValuationFee+$VAT;
$aprvd_finalFeePayable = $aprvd_netValuationFee+$aprvd_VAT;







?>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="datamain"><?= date("F j, Y", strtotime(date("Y-m-d"))); ?></td>
    </tr><br>
    <tr>
        <td class="datamain"><?= $client['title'] ?></td>
    </tr>
    <?php  
	if ($client['address']!=null) {?>
    <tr>
        <td class="datamain"><?= $client['address'] ?></td>
    </tr><br>
    <?php 
	}
?>


    <tr>
        <td class="data"><?= $standardReport['quotation_last_paragraph'] ?></td>
    </tr>
</table>

<style>
.table tr .datamain {
    font-size: 10px;
}
</style>

<br pagebreak="true" />

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">Date</td>
    </tr>
    <tr>
        <td class="data"><?= date("F j, Y", strtotime(date("Y-m-d"))) ?></td>
    </tr>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">Quotation Reference No.</td>
    </tr>
    <tr>
        <td class="data"><?= $mainTableData['reference_number'] ?></td>
    </tr>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">Client</td>
    </tr>
    <tr>
        <td class="data"><?= $client['title'] ?></td>
    </tr>

    <?php  
	if ($client['address']!=null) {?>
    <tr>
        <td class="data"><?= $client['address'] ?></td>
    </tr>
    <?php 
	}
?>
    <?php
    if ($mainTableData['client_name'] != 9167) {?>
    <!--<tr>
		<td class="data">
			Dubai, United Arab Emirates.
		</td>
	</tr>-->
    <?php
    }
    ?>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">Service</td>
    </tr>
    <tr>
        <td class="data"><?php 
        $uniqueServices = array_values(array_unique(array_reduce($multiScopeProperties, function ($carry, $property) {
            return array_merge($carry, explode(',', $property['scope_of_service']));
        }, [])));
        
        foreach ($uniqueServices as $service) {
            echo str_replace('Property(ies).', 'Asset(s)', Yii::$app->appHelperFunctions->ScopeOfWorkList[$service]) . "<br>&nbsp;&nbsp;";
        }    
        
        ?>
            <!--Valuation of Real Estate Interests of Subject Properties-->
        </td>
    </tr>
</table>

<style>
.table tr .heading {
    border: 1px solid black;
    font-size: 11px;
    color: #0091EA;
    text-indent: 5px;
}

.table tr .data {
    font-size: 10px;
    text-indent: 5px;
}
</style>

<br><br>
<?php if($mainTableData['quotaion_with_prop_fee'] == 1) { ?>
<table class="property-table" cellspacing="1" cellpadding="2">
    <tr>
        <?php if($model->fee_crm == 1 ){?>
        <td class="heading" colspan="10">Subject Assets</td>
        <?php }else{ ?>
            <td class="heading" colspan="8">Subject Assets</td>
        <?php } ?>
    </tr>
    <tr>
        <td class="property-heading" colspan="2" style="border-left: 1px solid black;">Scope of Service</td>
        <td class="property-heading" colspan="2" >Asset Category</td>
        <td class="property-heading" colspan="1">Quantity</td>
        <td class="property-heading" colspan="2">Complexity</td>
        <td class="property-heading" colspan="1">TAT</td>
        <?php if($model->fee_crm == 1 ){?>
        <td class="property-heading" colspan="2" style="border-right: 1px solid black; text-align: right">Fee</td>
        <?php } ?>
    </tr>
    <br>
    <?php
	$totalValuationFee = 0;
	$totalTurnAroundTime = 0;
	if ($multiScopeProperties!=null) {

		foreach ($multiScopeProperties as $keys => $value) {
            $property = \app\models\CrmReceivedProperties::find()->where(['id' => $value['id']])->one();


            $prperty_description = '';

            $multiScopeProperty = \app\models\CrmReceivedProperties::find()
            ->where(['multiscope_id' => $property->multiscope_id])
            ->orderBy(['id' => SORT_DESC])->all();
           
            ?>
            <?php //if((count($multiScopeProperties) > 1) ) { ?>
            <tr>
                <td class="data" style="border-bottom:#cacaca solid 1px" colspan="10">Asset <?= $keys+1 ?>: <?= $property->building->title ?></td>
            </tr>
            <?php //} ?>
            <?php 
                
                $buildingTotalFee = 0;

                foreach ($multiScopeProperty as $key => $multiProperty) {

                    $address = '';
                    if($multiProperty['unit_number'] != ''){
                        $address= $multiProperty['unit_number'].', '.$multiProperty['street'].', '.Yii::$app->appHelperFunctions->emiratedListArr[$property->building->city];
                    }else{
                        $address = $multiProperty['street'].', '.Yii::$app->appHelperFunctions->emiratedListArr[$property->building->city];
                    }
                    
                    ?>
                    <tr>
                        <td class="data" colspan="2" style=""><?= Yii::$app->appHelperFunctions->ScopeOfWorkList[$multiProperty->scope_of_service_saved] ?>
                        </td>
                        <td class="data" colspan="2" style="text-indent: 5px;"><?= $multiProperty->assetCategory->title ?>
                        </td>
                        <td class="data text-center" colspan="1" style="text-indent: 15px;"><?=  $multiProperty->no_of_asset ?></td>
                        <td class="data" colspan="2" style="text-indent: 5px;"><?= ucwords($multiProperty->asset_complexity); ?></td>

                        <td class="data" colspan="1" style="text-indent: 5px;"><?=  $multiProperty['aprvd_tat'] ?></td>
                    <?php if($model->fee_crm == 1 ){?>
                        <td class="data" colspan="2" style="text-indent: 5px; text-align: right;"><?=  number_format(($multiProperty['quotation_fee'] + yii::$app->quotationHelperFunctions->getPropertyPercentageAmount($quotation_fee_total_cal,$model->advance_payment_terms_final_amount,$multiProperty['quotation_fee'])),2) ?></td>
                    <?php } ?>
                    </tr>
                <?php
                if($model->advance_payment_terms_final_amount == 0) {
                    $totalValuationFee += $multiProperty['quotation_fee'];
                }else{
                    $totalValuationFee += $multiProperty['quotation_fee'] + yii::$app->quotationHelperFunctions->getPropertyPercentageAmount($quotation_fee_total_cal,$model->advance_payment_terms_final_amount,$multiProperty['quotation_fee']);
                }
                

                $totalTurnAroundTime += $multiProperty['aprvd_tat'];

                $buildingTotalFee = $buildingTotalFee + $multiProperty['quotation_fee'];
            }?>

                <?php //if($model->no_of_properties > 1) { ?>
                <?php if((count($multiScopeProperties) > 1) && (count($multiScopeProperty) > 1)) { ?>
                <tr>
                    <td class="data" colspan="7"></td>
                    <td class="data" colspan="2" style="border-top:#cacaca solid 1px; text-indent: 5px; text-align: right;">Building <?= $keys+1 ?> Total:</td>
                    <td class="data" style="border-top:#cacaca solid 1px; text-indent: 5px; text-align: right;"><?= number_format($buildingTotalFee,2); ?></td>
                </tr>
                <?php } ?>

            <?php
            
            

            echo '<br/>';
        }
    }
	?>
</table>

<?php } else { ?>

<table class="property-table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading" colspan="8">Subject Assets</td>
    </tr>


    <tr>
        <td class="property-heading" colspan="2" style="border-left: 1px solid black;text-indent: 10px;">Scope of Service</td>
        <td class="property-heading" colspan="2" >Asset Category</td>
        <td class="property-heading" colspan="1">Quantity</td>
        <td class="property-heading" colspan="2">Complexity</td>
        <td class="property-heading" colspan="1" style="border-right: 1px solid black; text-align: right">Turn Around Time</td>
        
    </tr>
    <br>
    <?php
	$totalValuationFee = 0;
	$totalTurnAroundTime = 0;
	if ($properties!=null) {

		foreach ($properties as $ky => $value) {
            $property = \app\models\CrmReceivedProperties::find()->where(['id' => $value['id']])->one();
            $address = '';
            if($value['unit_number'] != ''){
                $address= $value['unit_number'].', '.$value['street'].', '.Yii::$app->appHelperFunctions->emiratedListArr[$property->building->city];
            }else{
                $address= $value['street'].', '.Yii::$app->appHelperFunctions->emiratedListArr[$property->building->city];
            }

            $prperty_description = '';

           

            // dd("usa");

            ?>
            <tr>
                <td class="data" colspan="2" style=""><?= Yii::$app->appHelperFunctions->ScopeOfWorkList[$multiProperty->scope_of_service_saved] ?>
                </td>
                <td class="data" colspan="2" style="text-indent: 5px;"><?= $multiProperty->assetCategory->title ?>
                </td>
                <td class="data text-center" colspan="1" style="text-indent: 15px;"><?=  $multiProperty->no_of_asset ?></td>
                <td class="data" colspan="2" style="text-indent: 5px;"><?= ucwords($multiProperty->asset_complexity); ?></td>

                <td class="data" colspan="1" style="text-indent: 5px; text-align: right"><?=  $multiProperty['aprvd_tat'] ?></td>
            </tr><br>
            <?php
            if($model->advance_payment_terms_final_amount == 0) {
                $totalValuationFee += $value['quotation_fee'];
            }else{
                $totalValuationFee += $value['quotation_fee'] + yii::$app->quotationHelperFunctions->getPropertyPercentageAmount($quotation_fee_total_cal,$model->advance_payment_terms_final_amount,$value['quotation_fee']);
            }
			$totalTurnAroundTime += $value['aprvd_tat'];
		}
	}
	?>
</table>

<?php } ?>


<style>
.property-table tr .heading {
    border-top: 1px solid black;
    border-left: 1px solid black;
    border-right: 1px solid black;
    font-size: 11px;
    color: #0091EA;
    text-indent: 5px;
}

.property-table tr .property-heading {
    font-size: 10px;
    text-indent: 5px;
    border-top: 1px solid black;
    border-bottom: 1px solid black;
    /*text-align:center;*/
}

.property-table tr .data {
    font-size: 10px;

}
</style>

<br><br>

<?php if($model->id == 1601){?>
    <br pagebreak="true" />
<?php } ?>
<table cellspacing="1" cellpadding="2" class="bill-table" style="text-align: right">


    <tr>
        <td class="bill-data" style="border-left: 1px solid black;">Total Number of Assets <?= $mainTableData['no_of_properties'] ?></td>

        <td colspan="2" class="bill-data">Total Valuation Fee</td>
        <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($totalValuationFee,2) ?></td>
    </tr>

    <tr>
        <td class="bill-data" style="border-left: 1px solid black;">Turn Around Time <?= $totalTurnAroundTime ?> Days</td>

        <td colspan="3" class="bill-data" style="border-right: 1px solid black;"></td>
    </tr>

   <!-- <tr>
        <td class="bill-data" style="border-left: 1px solid black;">Payment Terms</td>
        <td class="bill-data"><?/*= $mainTableData['advance_payment_terms'] */?> Advance</td>

        <td class="bill-data"></td>
        <td class="bill-data" style="border-right: 1px solid black;"></td>
    </tr>-->



<?php if ($model->no_of_property_discount>0) {?>
	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>

		<?php if ($model->no_of_property_discount>0) { ?>
			<td colspan="2" class="bill-data">Multiple Asset Discount</td>
			<td class="bill-data" style="border-right: 1px solid black;">
            <?= 
            ($model->aprvd_no_of_property_discount_amount <> null && $model->aprvd_no_of_property_discount_amount <> $model->no_of_property_discount_final_amount) ? 
            number_format($model->aprvd_no_of_property_discount_amount,2) :
            number_format($model->no_of_property_discount_final_amount,2) 
            ?></td>
		<?php
		}else{?>
			<td colspan="2" class="bill-data">Multiple Asset Discount (0%)</td>
       		<td class="bill-data" style="border-right: 1px solid black;">0</td>
		<?php
		}
		?>
    </tr>
	<?php } ?>

    <?php if ($model->same_building_discount>0) {?>
        <tr>
            <td class="bill-data" style="border-left: 1px solid black;"></td>

            <?php if ($model->no_of_property_discount>0) { ?>
                <td colspan="2" class="bill-data">Number Of Units In The Same Building Discount</td>
                <td class="bill-data" style="border-right: 1px solid black;"><?= 
                ($model->aprvd_same_building_discount_amount <> null && $model->aprvd_same_building_discount_amount <> $model->no_of_units_same_building_final_amount) ? 
                number_format($model->aprvd_same_building_discount_amount,2) :
                number_format($model->no_of_units_same_building_final_amount,2) ?></td>
                <?php
            }else{?>
                <td colspan="2" class="bill-data">Number Of Units In The Same Building Discount (0%)</td>
                <td class="bill-data" style="border-right: 1px solid black;">0</td>
                <?php
            }
            ?>
        </tr>
    <?php } ?>

    <?php if ($model->first_time_discount>0) {?>
        <tr>
            <td class="bill-data" style="border-left: 1px solid black;"></td>

            <?php if ($model->first_time_discount>0) { ?>
                <td colspan="2" class="bill-data">First Time Discount </td>
                <td class="bill-data" style="border-right: 1px solid black;"><?= 
                ($model->aprvd_first_time_discount_amount <> null && $model->aprvd_first_time_discount_amount <> $model->first_time_discount_final_amount) ? 
                number_format($model->aprvd_first_time_discount_amount,2) :                
                number_format($model->first_time_discount_final_amount,2) ?></td>
                <?php
            }else{?>
                <td colspan="2" class="bill-data">First Time Discount (0%)</td>
                <td class="bill-data" style="border-right: 1px solid black;">0</td>
                <?php
            }
            ?>
        </tr>
    <?php } ?>


    <?php if ($relative_discount>0) {?>
        <tr>
            <td class="bill-data" style="border-left: 1px solid black;"></td>
            <td class="bill-data"></td>
            <?php
            if ($relative_discount>0) {
                if ($mainTableData['relative_discount']=='base-fee') {?>
                    <td class="bill-data">Relationship Discount</td>
                    <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($model->relationship_discount_final_amount,2) ?></td>
                    <?php
                }else{?>
                    <td class="bill-data">Relationship Discount </td>
                    <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($model->relationship_discount_final_amount,2) ?></td>
                    <?php
                }
            }else{?>
                <td class="bill-data">Relationship Discount (0%)</td>
                <td class="bill-data" style="border-right: 1px solid black;">0</td>
                <?php
            }
            ?>
        </tr>
    <?php } ?>
	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
		<?php if($total_discount>0){?>
			<td class="bill-data">Total Discount(-)</td>
			<td class="bill-data" style="border-right: 1px solid black;">
            <?php
             $totalDiscount = (($model->aprvd_no_of_property_discount_amount !== null) ? $model->aprvd_no_of_property_discount_amount : $model->no_of_property_discount_final_amount)+(($model->aprvd_same_building_discount_amount !== null) ? $model->aprvd_same_building_discount_amount : $model->no_of_units_same_building_final_amount)+(($model->aprvd_first_time_discount_amount !== null) ? $model->aprvd_first_time_discount_amount : $model->first_time_discount_final_amount);
            ?>
            
            <?= number_format(($totalDiscount + $model->relationship_discount_final_amount ),2); ?></td>
		<?php }else{ ?>
			<td class="bill-data">Total Discount (0%)</td>
			<td class="bill-data" style="border-right: 1px solid black;"><?= number_format(0,2) ?></td>
		<?php } ?>
    </tr>
    <tr>
        <td class="bill-data" style="border-left: 1px solid black;" colspan="2"></td>
        <td class="bill-data" style="border-right: 1px solid black;" colspan="2"></td>
    </tr>
	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
		<td class="bill-data">Net Valuation Fee</td>
		<td class="bill-data" style="border-right: 1px solid black;"><?= 
        ($aprvd_discount_net_fee <> null) ?
        number_format($aprvd_discount_net_fee,2) :
        number_format($discount_net_fee,2) ;
        ?></td>
    </tr>


    <?php if ($urgencyfee['value']>0) {?>
        <tr>
            <td class="bill-data" style="border-left: 1px solid black;"></td>

            <?php if ($urgencyfee['value']>0) { ?>
                <td colspan="2" class="bill-data">Urgency Fee (+) </td>
                <td class="bill-data" style="border-right: 1px solid black;"><?= 
                ($aprvd_urgencyfee <> null) ?
                number_format($model->aprvd_tat_requirements_final_amount,2) :
                number_format($model->tat_requirements_final_amount,2); ?></td>
                <?php
            }
            ?>
        </tr>
    <?php } ?>


    <tr>
        <td class="bill-data" style="border-left: 1px solid black;" colspan="2"></td>
        <td class="bill-data" style="border-right: 1px solid black;" colspan="2"></td>
    </tr>

   <!-- <tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
        <td class="bill-data">Net Valuation Fee</td>
        <td class="bill-data" style="border-right: 1px solid black;"><?/*= number_format($netValuationFee,2) */?></td>
    </tr>-->



    <tr>
        <!--		<td class="bill-data" style="border-left: 1px solid black;">Quotation Expiry Date</td>
		<td class="bill-data"><?/*= date("F j, Y", strtotime($mainTableData['expiry_date'])); */?></td>-->

        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
        <?php if($client['vat'] == 1){ ?>
        <td class="bill-data">5% VAT</td>
        <td class="bill-data" style="border-right: 1px solid black;"><?= 
        ($aprvd_VAT <> null) ?
        number_format($aprvd_VAT,2) :
        number_format($VAT,2) ?></td>
        <?php }else{ ?>
        <td class="bill-data">VAT</td>
        <td class="bill-data" style="border-right: 1px solid black;">0</td>
        <?php } ?>
    </tr>

    <tr>
        <td class="bill-data" style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
        <td class="bill-data" style="border-bottom: 1px solid black;"></td>
        <td class="bill-data" style="border-bottom: 1px solid black;">Final Fee Payable </td>
        <td class="bill-data" style="border-right: 1px solid black;border-bottom: 1px solid black;"><?= 
        ($aprvd_finalFeePayable <> null) ? number_format($aprvd_finalFeePayable) :
        number_format($finalFeePayable) ?></td>
    </tr>


</table>

<style>
.bill-table tr .bill-data {
    font-size: 10px;
    text-indent: 5px;
    color: #0091EA;
    border-top: 1px solid black;
}
</style>