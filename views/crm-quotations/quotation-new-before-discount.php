<?php
$mainTableData = yii::$app->quotationHelperFunctions->getMainTableDataNew($id);
$client = yii::$app->quotationHelperFunctions->getClientAddress($mainTableData['client_name']);
$properties = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id);
// echo "<pre>";
// print_r($properties);
// echo "</pre>";
// die();
$standardReport = Yii::$app->quotationHelperFunctions->standardReport();



?>

<table class="table" cellspacing="1" cellpadding="2">
	<tr>
    <td class="datamain"><?= date("F j, Y", strtotime(date("Y-m-d"))); ?></td>
  </tr><br>
	<tr>
		<td class="datamain">
			<?= $client['title'] ?>
		</td>
	</tr>
<?php  
	if ($client['address']!=null) {?>
		<tr>
		<td class="datamain">
			<?= $client['address'] ?>
		</td>
	</tr><br>
<?php 
	}
?>
	

	<tr>
		<td class="data">
			<?= $standardReport['quotation_last_paragraph'] ?>
		</td>
	</tr>
</table>

<style>
.table tr .datamain{
	font-size: 10px;
}
</style>

<br pagebreak="true" />

<table class="table" cellspacing="1" cellpadding="2">
	<tr>
		<td class="heading">
			Date
		</td>
	</tr>
	<tr>
		<td class="data">
			<?= date("F j, Y", strtotime(date("Y-m-d"))) ?>
		</td>
	</tr>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
	<tr>
		<td class="heading">
			Quotation Reference No.
		</td>
	</tr>
	<tr>
		<td class="data">
			<?= $mainTableData['reference_number'] ?>
		</td>
	</tr>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
	<tr>
		<td class="heading">
			Client
		</td>
	</tr>
	<tr>
		<td class="data">
			<?= $client['title'] ?>
		</td>
	</tr>

	<?php  
	if ($client['address']!=null) {?>
		<tr>
		<td class="datamain">
			  <?= $client['address'] ?>
		</td>
	</tr>
<?php 
	}
?>
    <?php
    if ($mainTableData['client_name'] != 9167) {?>
	<!--<tr>
		<td class="data">
			Dubai, United Arab Emirates.
		</td>
	</tr>-->
        <?php
    }
    ?>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
	<tr>
		<td class="heading">
			Service
		</td>
	</tr>
	<tr>
		<td class="data"><?= $mainTableData['scope_of_service']?>
			<!--Valuation of Real Estate Interests of Subject Properties-->
		</td>
	</tr>
</table>

<style>
.table tr .heading{
	border: 1px solid black;
	font-size: 11px;
	color: #0091EA;
	text-indent: 5px;
}
.table tr .data{
	font-size: 10px;
	text-indent: 5px;
}
</style>

<br><br>

<table class="property-table" cellspacing="1" cellpadding="2">
	<tr>
		<td class="heading" colspan="10">
			Subject Properties
		</td>
	</tr>


	<tr>
		<td class="property-heading" colspan="2" style="border-left: 1px solid black;">Properties</td>
		<td class="property-heading" colspan="2">Property Type</td>
		<td class="property-heading" colspan="2">Property Address</td>
		<td class="property-heading" colspan="2">Turn Around Time</td>
		<td class="property-heading" colspan="2" style="border-right: 1px solid black;">Fee</td>
	</tr>
	<br>
	<?php
	$totalValuationFee = 0;
	$totalTurnAroundTime = 0;
	if ($properties!=null) {

		foreach ($properties as $ky => $value) {
            $property = \app\models\CrmReceivedProperties::find()->where(['id' => $value['id']])->one();
            $address = '';
            if($value['unit_number'] != ''){
                $address= $value['unit_number'].', '.$value['street'].', '.Yii::$app->appHelperFunctions->emiratedListArr[$property->building->city];
            }else{
                $address= $value['street'].', '.Yii::$app->appHelperFunctions->emiratedListArr[$property->building->city];
            }



	?>
			<tr>
				<td class="data" colspan="2" style="text-indent: 5px;"><?= $property->building->title ?></td>
				<td class="data" colspan="2" style="text-indent: 5px;"><?=  Yii::$app->appHelperFunctions->getPropertiesCategoriesListArr()[$value['property_category']] ?></td>
				<td class="data" colspan="2"><?=  $address?></td>
				<td class="data" colspan="2" style="text-indent: 5px;"><?=  $value['tat'] ?></td>
				<td class="data" colspan="2" style="text-indent: 5px;"><?=  $value['quotation_fee'] ?></td>
			</tr><br>
	<?php
			$totalValuationFee += $value['quotation_fee'];
			$totalTurnAroundTime += $value['tat'];
		}
	}
	?>
</table>


<style>
.property-table tr .heading{
	border-top: 1px solid black;
	border-left: 1px solid black;
	border-right: 1px solid black;
	font-size: 11px;
	color: #0091EA;
	text-indent: 5px;
}

.property-table tr .property-heading{
	font-size: 10px;
	text-indent: 5px;
	border-top: 1px solid black;
	border-bottom: 1px solid black;
	/*text-align:center;*/
}

.property-table tr .data{
	font-size: 10px;

}
</style>

<br><br>

<table cellspacing="1" cellpadding="2" class="bill-table">


	<tr>
		<td class="bill-data" style="border-left: 1px solid black;">Total Number of Properties </td>
		<td class="bill-data"><?= $mainTableData['no_of_properties'] ?></td>
		<td class="bill-data">Total Valuation Fee</td>
		<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($totalValuationFee,2) ?></td>
	</tr>


	<?php
	if ($mainTableData['relative_discount']!=null) {
		$discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($totalValuationFee,$mainTableData['relative_discount']);
			// print_r($discount);
			// die();
	}
	?>

	<tr>
		<td class="bill-data" style="border-left: 1px solid black;">Turn Around Time</td>
		<td class="bill-data"><?= $totalTurnAroundTime ?> Days</td>

		<?php
		if ($discount>0) {
			if ($mainTableData['relative_discount']=='base-fee') {?>
				<td class="bill-data">Base Fee Discount</td>
				<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($discount,2) ?></td>
				<?php
			} else{?>

				<td class="bill-data"><?= $mainTableData['relative_discount'] ?>% Discount</td>
				<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($discount,2) ?></td>
				<?php
			}
			?>
			<?php
		}else{?>
			<td class="bill-data">No Discount</td>
			<td class="bill-data" style="border-right: 1px solid black;">0</td>
			<?php
		}
		?>

	</tr>


	<?php
    $VAT =0;
	$netValuationFee = $totalValuationFee-$discount;
    if($client['vat']== 1) {

        $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
    }
	?>

	<tr>
		<td class="bill-data" style="border-left: 1px solid black;">Payment Terms</td>
		<td class="bill-data"><?= $mainTableData['advance_payment_terms'] ?> Advance</td>
		<td class="bill-data">Net Valuation Fee</td>
		<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($netValuationFee,2) ?></td>
	</tr>


	<tr>
<!--		<td class="bill-data" style="border-left: 1px solid black;">Quotation Expiry Date</td>
		<td class="bill-data"><?/*= date("F j, Y", strtotime($mainTableData['expiry_date'])); */?></td>-->

        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
        <?php if($client['vat'] == 1){ ?>
		<td class="bill-data">5% VAT</td>
		<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($VAT,2) ?></td>
        <?php }else{ ?>
            <td class="bill-data">No VAT</td>
            <td class="bill-data" style="border-right: 1px solid black;">0</td>
        <?php } ?>
	</tr>

	<?php
	$finalFeePayable = $netValuationFee+$VAT;
	?>
	<tr>
		<td class="bill-data" style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
		<td class="bill-data" style="border-bottom: 1px solid black;"></td>
		<td class="bill-data" style="border-bottom: 1px solid black;">Final Fee Payable	</td>
		<td class="bill-data" style="border-right: 1px solid black;border-bottom: 1px solid black;"><?= number_format($finalFeePayable,2) ?></td>
	</tr>


</table>

<style>
.bill-table tr .bill-data{
	font-size: 10px;
	text-indent: 5px;
	color: #0091EA;
	border-top: 1px solid black;
}
</style>
