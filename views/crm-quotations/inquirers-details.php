<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use kartik\depdrop\DepDrop;
use app\assets\ValuationFormAsset;

ValuationFormAsset::register($this);


ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enter Inquiry/RFQ Details');
$cardTitle = Yii::t('app', 'Ref No:  {nameAttribute}', [
    'nameAttribute' => $model->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_0/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');


$clientBanks = yii::$app->appHelperFunctions->clientBanksList;

$clientBankIds = [];
foreach ($clientBanks as $clientBank) {
    $clientBankIds[] = "$clientBank->id";
}

// dd($banks);

// dd($typeOfQuot,$model->type_of_service);

$this->registerJs('

$("#listingstransactions-instruction_date, #listingstransactions-inquiry_received_day").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "DD-MMM-YYYY", 
    defaultDate: new Date(), 
    useCurrent: true
});


$("#quotation-irtime").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "HH:mm",
    defaultDate: moment(),
    focusOnShow: false,
});

$(document).on("click", function (e) {
    if ($(e.target).closest("#listingstransactions-instruction_date, #listingstransactions-inquiry_received_day").length === 0) {
        $("#listingstransactions-instruction_date, #listingstransactions-inquiry_received_day").datetimepicker("hide");
    }
});

$(document).on("click", function (e) {
    if ($(e.target).closest("#quotation-irtime").length === 0) {
        $("#quotation-irtime").datetimepicker("hide");
    }
});


$(document).ready(function () {
    // Get references to the select box and email subject field
    var $clientNameSelect = $("#crmquotations-client_name");
    var $emailSubjectField = $(".email-subject-field");
    var $clientCustomerName = $(".client-customer-name");
    var $clientContactPersonName = $(".client-contact-person-name");
    var $instructingPersonCompany = $(".instructing-person-company");

    // Hide the email subject field and its label initially
    $emailSubjectField.hide();

    // Add an event listener to the client name select box
    $clientNameSelect.on("change", function () {
        var selectedValue = $clientNameSelect.val() ;
        // var showFieldForValues = ["1"]; // Replace with the specific values that should show the field
        
        // Check if the select box value is 1
        // if ($clientNameSelect.val() == 1 ) {

        var showFieldForValues = ' . json_encode($clientBankIds) . ';

        if (showFieldForValues.includes(selectedValue)) {
            // Show the email subject field and its label, and make it mandatory
            $emailSubjectField.show();
            $emailSubjectField.find("input").prop("required", true);
            $clientCustomerName.show();
            $instructingPersonCompany.hide();
            $clientContactPersonName.hide();
        } else {
            // Hide the email subject field and its label, and remove the mandatory requirement
            $emailSubjectField.hide();
            $emailSubjectField.find("input").prop("required", false);
            $emailSubjectField.find("input").val("");
            $clientCustomerName.hide();
            $instructingPersonCompany.show();
            $clientContactPersonName.show();
        }
    });
});


$("body").on("change", ".client", function () {
    // console.log($(this).val());
    if($(this).val() == 1){
     $("#client_fixed_fee").show();
            }else{
             $("#client_fixed_fee").hide();
            }
 $(".appentWali-row").remove();
 _this = $(this);
 GetOtherInstructingPersons(_this);
});

var other_instructing_person_id =  "' . $model->other_instructing_person . '";
var instruction_person_id =  "' . $model->instruction_person . '";

function GetOtherInstructingPersons(_this){
    var keyword = "CrmQuotations";
    var client_name = _this.val();      
    if (client_name!= "") {
        var data = {client_name:client_name, other_instructing_person_id:other_instructing_person_id, instruction_person_id:instruction_person_id, keyword:keyword};
        $.ajax({
            url: "' . Url::to(['client/other-instructing-persons-crm-enabled']) . '",
            data: data,
            method: "post",
            dataType: "html",
            success: function(data) {
            //  console.log(data);
                $(".parent-row-ff").html(data);

                // Disable the select field
                // $("#other-instructing-person").attr("disabled", true);
                // $("#other-instructing-person").css("pointer-events", "none");
                // $("#other-instructing-person").css("background", "#E9ECEF");
                
                var element = $("#other-instructing-person");
                var option = $(\'option:selected\', element).attr(\'data_phone\');
                $("#crm-q_instructing_person").val(option);
                
                $("#other-instructing-person").on("change",function(){
                var element = $("#other-instructing-person");
                var option = $(\'option:selected\',element).attr(\'data_phone\');
                $("#crm-q_instructing_person").val(option);
                
                });
               // console.log(option);
            },
            error: bbAlert
        });
    }
    else{
        $(".appentWali-row").remove();
    }
     

}

$(".client").trigger("change");


$(document).ready(function() {
    // Function to show or hide the portfolio field based on the selected value of type_of_service
    function togglePortfolioField() {
        var selectedValue = $("#type_of_service").val();
        if (selectedValue == "1" ) {
            $("#portfolio-field").show();
        } else if (selectedValue == "3" || selectedValue == "2") {
            $("#portfolio-field").hide();
        }
    }

    function changeReferenceNumber() {
        var selectedValue = $("#type_of_service").val();
        if (selectedValue == "3" ) {
            console.log($("#crmquotations-reference_number").val());
            // $("#crmquotations-reference_number").val() = "";
        }
            
    }

    // Initial check on page load
    togglePortfolioField();


    // Event listener for changes on type_of_service
    $("#type_of_service").on("change", function() {
        togglePortfolioField();
        changeReferenceNumber();
    });
});



');
$paymentTypes = yii::$app->crmQuotationHelperFunctions->paymentTerms;

// $this->registerJs('

// $("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
//     allowInputToggle: true,
//     viewMode: "months",
//     format: "DD-MM-YYYY"
// });
// ');


?>
<style>
    .datepicker-days .table>thead>tr>th,
    .table>tbody>tr>th,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>tbody>tr>td,
    .table>tfoot>tr>td {
        padding: inherit !important;
    }

    .fade {
        opacity: 1;
    }

    .nav-tabs.flex-column .nav-link.active {
        background-color: #007bff;
        color: white;
        font-weight: bold;
    }
</style>

<div class="card card-primary card-outline">
    <div class="card-header">

        <div class="row">
            <div class="col-6">
                <h3 class="card-title">
                    New Proposal
                </h3>
            </div>
            <div class="col-6">
                <h3 class="card-title float-none text-md-right">
                    <?= $cardTitle; ?>
                </h3>
            </div>
        </div>

    </div>
    <div class="card-body">
        <div class="row">

            <div class="col-5 col-sm-3">
                <?php echo $this->render('left-nav-disabled'); ?>

            </div>

            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                        aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form">
                            <?php $form = ActiveForm::begin(); ?>

                            <!-- <div class="card-body"> -->

                            <div class="card card-outline card-primary mb-3">
                                <header class="card-header">
                                    <h2 class="card-title">Reference Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <?= $form->field($model, 'reference_number')->textInput([
                                                'maxlength' => true,
                                                'readonly' => true,
                                            ])
                                                ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?= $form->field($model, 'client_reference')->textInput(['maxlength' => true, 'class' => 'form-control client_reference', 'placeholder' => 'Enter Client Reference...']) ?>
                                        </div>

                                        <!-- <div class="col-md-4">
                                            <?php 
                                            /*
                                            echo $form->field($model, 'type_of_service')->widget(Select2::classname(), [
                                                'data' => Yii::$app->crmQuotationHelperFunctions->TypeOfServiceArr,
                                                'options' => ['id' => 'type_of_service'],
                                                'pluginOptions' => [
                                                    'disabled' => true
                                                ],
                                            ])->label('Type of Service');
                                            */
                                            ?>
                                        </div> -->
                                        <?php 
                                            echo $form->field($model, 'type_of_service')->hiddenInput(['value' => $model->type_of_service])->label(false);
                                        ?>

                                    </div>
                                </div>
                            </div>

                            <div class="card card-outline card-primary mb-3">
                                <header class="card-header">
                                    <h2 class="card-title">Inquiry Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <?php $instruction_date = ($model->instruction_date <> null) ? $model->instruction_date : date('d-M-Y') ?>
                                            <?= $form->field($model, 'instruction_date', [
                                                'template' => '
                                                {label}
                                                <div class="input-group date" style="display: flex" id="listingstransactions-instruction_date" data-target-input="nearest">
                                                {input}
                                                <div class="input-group-append" data-target="#listingstransactions-instruction_date" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                                </div>
                                                {hint}{error}
                                                '
                                            ])->textInput(['maxlength' => true, 'value' => $instruction_date])->label('Inquiry Received Date') ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?= $form->field($model, 'inquiry_received_time', [
                                                'template' => '
                                                {label}
                                                <div class="input-group date" style="display: flex" id="quotation-irtime" data-target-input="nearest">
                                                {input}
                                                <div class="input-group-append" data-target="#quotation-irtime" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                                </div>
                                                </div>
                                                {hint}{error}
                                                '
                                            ])->textInput(['maxlength' => true]) ?>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="card card-outline card-primary mb-3">
                                <header class="card-header">
                                    <h2 class="card-title">Stakeholders Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?php
                                            echo $form->field($model, 'client_name')->widget(Select2::classname(), [
                                                'data' => $existing_client_array,

                                                'options' => ['placeholder' => 'Select a Client ...', 'class'=> 'client'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Client Name');
                                            ?>

                                        </div>


                                        <div class="col-md-4 parent-row-ff">
                                            <div class="form-group field-crm-q_instructing_person">
                                                <label class="control-label" for="crm-q_instructing_person">Instruction
                                                    Person Name <span class="text-danger">*</span></label>
                                                <input type="text" readonly id="crm-q_instructing_person"
                                                    class="form-control">

                                                <div class="help-block"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 instructing-person-company">
                                            <?= $form->field($model, 'instructing_party_company')->textInput(['maxlength' => true, 'class'=>'form-control'])->label('Instructing Person Company Name') ?>
                                        </div>

                                        <div class="col-md-4 email-subject-field" style="display: none;">
                                                <?php
                                                echo $form->field($model, 'email_subject')->textInput(['placeholder' => 'Enter Email Subject for Banks...'])->label('Client Email Subject (Banks Only)<span style="color:red "> *</span>');
                                                ?>
                                        </div>

                                        <div class="col-md-4 client-contact-person-name">
                                            <?= $form->field($model, 'client_contact_person')->textInput(['maxlength' => true, 'class'=>'form-control'])->label('Client Contact Person Name') ?>
                                        </div>

                                        <div class="col-md-4 client-customer-name"  style="display: none;">
                                            <?= $form->field($model, 'client_customer_name')->textInput(['maxlength' => true, 'class' => 'form-control client_customer_name', 'placeholder' => 'Enter Client Customer Name...'])->label('Client Customer Name (Banks Only)') ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php
                                            echo $form->field($model, 'client_is_owner')->widget(Select2::classname(), [
                                                'data' => Yii::$app->helperFunctions->arrYesNo,
                                                'options' => ['id' => 'client_is_owner', 'placeholder' => 'Select a value ...'],
                                            ])->label('Client is Owner?');
                                            ?>
                                        </div>
                                        <div class="col-sm-4"  id="portfolio-field">
                                            <?php
                                            echo $form->field($model, 'portfolio')->widget(Select2::classname(), [
                                                'data' => array('0' => 'No', '1' => 'Yes'),
                                            ])->label('Portfolio Valuaion? <span class="text-danger">*</span>');
                                            ?>
                                        </div>

                                        <!-- <div class="col-md-4 quotaion_with_prop_fee">
                                            <?php
                                            /* echo $form->field($model, 'quotaion_with_prop_fee')->widget(Select2::classname(), [
                                                'data' => array_reverse(Yii::$app->helperFunctions->arrYesNo),
                                                'options' => ['id' => 'quotaion_with_prop_fee', 'placeholder' => 'Select a value ...'],
                                            ])->label('Quotation with Property Fee'); */
                                            ?>
                                        </div> -->

                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                               
                                <?= Html::SubmitButton('<i class="fa fa-save"></i> Save', ['class' => "btn btn-success", 'onClick' => "document.getElementById('w0').action = 'crm-quotations/create?t={$typeOfQuot}';"]); ?>

                                <?= Html::SubmitButton('<i class="fa fa-paper-plane" aria-hidden="true"></i> Confirm & Email Client', ['class' => "btn btn-info", 'onClick' => "document.getElementById('w0').action = 'crm-quotations/create-and-mail?t={$typeOfQuot}';"]); ?>

                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>