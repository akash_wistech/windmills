<?php


//maxima
if($InspectProperty->no_studios > 0){
    $all_units_maxima[1]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'list', 'income_type'=> 1])->orderBy(['id' => SORT_DESC])->one();
    $all_units_maxima[1]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'sold', 'income_type'=> 1])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_maxima[1]['list']->mv_total_price) && ($all_units_maxima[1]['list']->mv_total_price > 0)) && (isset($all_units_maxima[1]['sold']->mv_total_price) && ($all_units_maxima[1]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_maxima[1]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_maxima[1]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_maxima[1]['list']->mv_total_price) && ($all_units_maxima[1]['list']->mv_total_price > 0)){
        $list_25 = $all_units_maxima[1]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_maxima[1]['sold']->mv_total_price;
    }
    $all_units_maxima[1]['list_total'] = $list_25;
    $all_units_maxima[1]['sold_total'] = $ejari_75;


   // $all_units_maxima[1]['total'] = $list_25 + $ejari_75;
    $all_units_maxima[1]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_maxima[1]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_studios;
    $all_units_maxima[1]['nla'] = $InspectProperty->no_studios_nla;
    $all_units_maxima[1]['qty'] = $InspectProperty->no_studios ;
    $all_units_maxima[1]['rsf'] = round($all_units_maxima[1]['total']/$InspectProperty->no_studios_nla) ;
    $gross_rental_income_total_maxima = $gross_rental_income_total_maxima + $all_units_maxima[1]['total'];







}

if($InspectProperty->no_one_bedrooms > 0){
    $all_units_maxima[2]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'list', 'income_type'=> 2])->orderBy(['id' => SORT_DESC])->one();
    $all_units_maxima[2]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'sold', 'income_type'=> 2])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_maxima[2]['list']->mv_total_price) && ($all_units_maxima[2]['list']->mv_total_price > 0)) && (isset($all_units_maxima[2]['sold']->mv_total_price) && ($all_units_maxima[2]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_maxima[2]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_maxima[2]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_maxima[2]['list']->mv_total_price) && ($all_units_maxima[2]['list']->mv_total_price > 0)){
        $list_25 = $all_units_maxima[2]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_maxima[2]['sold']->mv_total_price;
    }
    $all_units_maxima[2]['list_total'] = $list_25;
    $all_units_maxima[2]['sold_total'] = $ejari_75;


    // $all_units_maxima[2]['total'] = $list_25 + $ejari_75;
    $all_units_maxima[2]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_maxima[2]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_one_bedrooms;
    $all_units_maxima[2]['nla'] = $InspectProperty->no_one_bedrooms_nla;
    $all_units_maxima[2]['qty'] = $InspectProperty->no_one_bedrooms ;
    $all_units_maxima[2]['rsf'] = round($all_units_maxima[2]['total']/$InspectProperty->no_one_bedrooms_nla) ;
    $gross_rental_income_total_maxima = $gross_rental_income_total_maxima + $all_units_maxima[2]['total'];


}


if($InspectProperty->no_two_bedrooms > 0){
    $all_units_maxima[3]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'list', 'income_type'=> 3])->orderBy(['id' => SORT_DESC])->one();
    $all_units_maxima[3]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'sold', 'income_type'=> 3])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_maxima[3]['list']->mv_total_price) && ($all_units_maxima[3]['list']->mv_total_price > 0)) && (isset($all_units_maxima[3]['sold']->mv_total_price) && ($all_units_maxima[3]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_maxima[3]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_maxima[3]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_maxima[3]['list']->mv_total_price) && ($all_units_maxima[3]['list']->mv_total_price > 0)){
        $list_25 = $all_units_maxima[3]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_maxima[3]['sold']->mv_total_price;
    }
    $all_units_maxima[3]['list_total'] = $list_25;
    $all_units_maxima[3]['sold_total'] = $ejari_75;


   // $all_units_maxima[3]['total'] = $list_25 + $ejari_75;
    $all_units_maxima[3]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_maxima[3]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_two_bedrooms;
    $all_units_maxima[3]['nla'] = $InspectProperty->no_two_bedrooms_nla;
    $all_units_maxima[3]['qty'] = $InspectProperty->no_two_bedrooms ;
    $all_units_maxima[3]['rsf'] = round($all_units_maxima[3]['total']/$InspectProperty->no_two_bedrooms_nla) ;
    $gross_rental_income_total_maxima = $gross_rental_income_total_maxima + $all_units_maxima[3]['total'];
}


if($InspectProperty->no_three_bedrooms > 0){
    $all_units_maxima[4]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'list', 'income_type'=> 4])->orderBy(['id' => SORT_DESC])->one();
    $all_units_maxima[4]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'sold', 'income_type'=> 4])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_maxima[4]['list']->mv_total_price) && ($all_units_maxima[4]['list']->mv_total_price > 0)) && (isset($all_units_maxima[4]['sold']->mv_total_price) && ($all_units_maxima[4]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_maxima[4]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_maxima[4]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_maxima[4]['list']->mv_total_price) && ($all_units_maxima[4]['list']->mv_total_price > 0)){
        $list_25 = $all_units_maxima[4]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_maxima[4]['sold']->mv_total_price;
    }
    $all_units_maxima[4]['list_total'] = $list_25;
    $all_units_maxima[4]['sold_total'] = $ejari_75;


   // $all_units_maxima[4]['total'] = $list_25 + $ejari_75;
    $all_units_maxima[4]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_maxima[4]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_three_bedrooms;
    $all_units_maxima[4]['nla'] = $InspectProperty->no_three_bedrooms_nla;
    $all_units_maxima[4]['qty'] = $InspectProperty->no_three_bedrooms ;
    $all_units_maxima[4]['rsf'] = round($all_units_maxima[4]['total']/$InspectProperty->no_three_bedrooms_nla) ;
    $gross_rental_income_total_maxima = $gross_rental_income_total_maxima + $all_units_maxima[4]['total'];
}

if($InspectProperty->no_four_bedrooms > 0){
    $all_units_maxima[5]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'list', 'income_type'=> 5])->orderBy(['id' => SORT_DESC])->one();
    $all_units_maxima[5]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'sold', 'income_type'=> 5])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_maxima[5]['list']->mv_total_price) && ($all_units_maxima[5]['list']->mv_total_price > 0)) && (isset($all_units_maxima[5]['sold']->mv_total_price) && ($all_units_maxima[5]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_maxima[5]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_maxima[5]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_maxima[5]['list']->mv_total_price) && ($all_units_maxima[5]['list']->mv_total_price > 0)){
        $list_25 = $all_units_maxima[5]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_maxima[5]['sold']->mv_total_price;
    }
    $all_units_maxima[5]['list_total'] = $list_25;
    $all_units_maxima[5]['sold_total'] = $ejari_75;


  //  $all_units_maxima[5]['total'] = $list_25 + $ejari_75;
    $all_units_maxima[5]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_maxima[5]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_four_bedrooms;
    $all_units_maxima[5]['nla'] = $InspectProperty->no_four_bedrooms_nla;
    $all_units_maxima[5]['qty'] = $InspectProperty->no_four_bedrooms ;
    $all_units_maxima[5]['rsf'] = round($all_units_maxima[5]['total']/$InspectProperty->no_four_bedrooms_nla) ;
    $gross_rental_income_total_maxima = $gross_rental_income_total_maxima + $all_units_maxima[5]['total'];
}

if($InspectProperty->no_penthouse > 0){
    $all_units_maxima[6]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'list', 'income_type'=> 6])->orderBy(['id' => SORT_DESC])->one();
    $all_units_maxima[6]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'sold', 'income_type'=> 6])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_maxima[6]['list']->mv_total_price) && ($all_units_maxima[6]['list']->mv_total_price > 0)) && (isset($all_units_maxima[6]['sold']->mv_total_price) && ($all_units_maxima[6]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_maxima[6]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_maxima[6]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_maxima[6]['list']->mv_total_price) && ($all_units_maxima[6]['list']->mv_total_price > 0)){
        $list_25 = $all_units_maxima[6]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_maxima[6]['sold']->mv_total_price;
    }
    $all_units_maxima[6]['list_total'] = $list_25;
    $all_units_maxima[6]['sold_total'] = $ejari_75;


    //$all_units_maxima[6]['total'] = $list_25 + $ejari_75;
    $all_units_maxima[6]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_maxima[6]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_penthouse;
    $all_units_maxima[6]['nla'] = $InspectProperty->no_penthouse_nla;
    $all_units_maxima[6]['qty'] = $InspectProperty->no_penthouse ;
    $all_units_maxima[6]['rsf'] = round($all_units_maxima[6]['total']/$InspectProperty->no_penthouse_nla) ;
    $gross_rental_income_total_maxima = $gross_rental_income_total_maxima + $all_units_maxima[6]['total'];
}

if($InspectProperty->no_of_shops > 0){
    $all_units_maxima[7]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'list', 'income_type'=> 7])->orderBy(['id' => SORT_DESC])->one();
    $all_units_maxima[7]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'sold', 'income_type'=> 7])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_maxima[7]['list']->mv_total_price) && ($all_units_maxima[7]['list']->mv_total_price > 0)) && (isset($all_units_maxima[7]['sold']->mv_total_price) && ($all_units_maxima[7]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_maxima[7]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_maxima[7]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_maxima[7]['list']->mv_total_price) && ($all_units_maxima[7]['list']->mv_total_price > 0)){
        $list_25 = $all_units_maxima[7]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_maxima[7]['sold']->mv_total_price;
    }
    $all_units_maxima[7]['list_total'] = $list_25;
    $all_units_maxima[7]['sold_total'] = $ejari_75;


   // $all_units_maxima[7]['total'] = $list_25 + $ejari_75;
    $all_units_maxima[7]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_maxima[7]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_of_shops;
    $all_units_maxima[7]['nla'] = $InspectProperty->no_of_shops_nla;
    $all_units_maxima[7]['qty'] = $InspectProperty->no_of_shops ;
    $all_units_maxima[7]['rsf'] = round($all_units_maxima[7]['total']/$InspectProperty->no_of_shops_nla) ;
    $gross_rental_income_total_maxima = $gross_rental_income_total_maxima + $all_units_maxima[7]['total'];
}

if($InspectProperty->no_of_offices > 0){
    $all_units_maxima[8]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'list', 'income_type'=> 8])->orderBy(['id' => SORT_DESC])->one();
    $all_units_maxima[8]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 1, 'type'=> 'sold', 'income_type'=> 8])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_maxima[8]['list']->mv_total_price) && ($all_units_maxima[8]['list']->mv_total_price > 0)) && (isset($all_units_maxima[8]['sold']->mv_total_price) && ($all_units_maxima[8]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_maxima[8]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_maxima[8]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_maxima[8]['list']->mv_total_price) && ($all_units_maxima[8]['list']->mv_total_price > 0)){
        $list_25 = $all_units_maxima[8]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_maxima[8]['sold']->mv_total_price;
    }
    $all_units_maxima[8]['list_total'] = $list_25;
    $all_units_maxima[8]['sold_total'] = $ejari_75;


   // $all_units_maxima[8]['total'] = $list_25 + $ejari_75;
    $all_units_maxima[7]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_maxima[7]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_of_offices;
    $all_units_maxima[7]['nla'] = $InspectProperty->no_of_offices_nla;
    $all_units_maxima[8]['qty'] = $InspectProperty->no_of_offices ;
    $all_units_maxima[8]['rsf'] = round($all_units_maxima[8]['total']/$InspectProperty->no_of_offices_nla) ;
    $gross_rental_income_total_maxima = $gross_rental_income_total_maxima + $all_units_maxima[8]['total'];
}

//valuer
if($InspectProperty->no_studios > 0){



    $all_units_valuer[1]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'list', 'income_type'=> 1])->orderBy(['id' => SORT_DESC])->one();
    $all_units_valuer[1]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'sold', 'income_type'=> 1])->orderBy(['id' => SORT_DESC])->one();

    /*   echo "<pre>";
       print_r($all_units_valuer[1]['sold']);
       die;*/
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_valuer[1]['list']->mv_total_price) && ($all_units_valuer[1]['list']->mv_total_price > 0)) && (isset($all_units_valuer[1]['sold']->mv_total_price) && ($all_units_valuer[1]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_valuer[1]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_valuer[1]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_valuer[1]['list']->mv_total_price) && ($all_units_valuer[1]['list']->mv_total_price > 0)){
        $list_25 = $all_units_valuer[1]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_valuer[1]['sold']->mv_total_price;
    }
    $all_units_valuer[1]['list_total'] = $list_25;
    $all_units_valuer[1]['sold_total'] = $ejari_75;


    //$all_units_valuer[1]['total'] = $list_25 + $ejari_75;
    $all_units_valuer[1]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_valuer[1]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_studios;
    $all_units_valuer[1]['nla'] = $InspectProperty->no_studios_nla;
    $all_units_valuer[1]['qty'] = $InspectProperty->no_studios ;
    $all_units_valuer[1]['rsf'] = round($all_units_valuer[1]['total']/$InspectProperty->no_studios_nla) ;
    $gross_rental_income_total_valuer = $gross_rental_income_total_valuer + $all_units_valuer[1]['total'];






}

if($InspectProperty->no_one_bedrooms > 0){
    $all_units_valuer[2]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'list', 'income_type'=> 2])->orderBy(['id' => SORT_DESC])->one();
    $all_units_valuer[2]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'sold', 'income_type'=> 2])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_valuer[2]['list']->mv_total_price) && ($all_units_valuer[2]['list']->mv_total_price > 0)) && (isset($all_units_valuer[2]['sold']->mv_total_price) && ($all_units_valuer[2]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_valuer[2]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_valuer[2]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_valuer[2]['list']->mv_total_price) && ($all_units_valuer[2]['list']->mv_total_price > 0)){
        $list_25 = $all_units_valuer[2]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_valuer[2]['sold']->mv_total_price;
    }
    $all_units_valuer[2]['list_total'] = $list_25;
    $all_units_valuer[2]['sold_total'] = $ejari_75;


   // $all_units_valuer[2]['total'] = $list_25 + $ejari_75;
    $all_units_valuer[2]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_valuer[2]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_one_bedrooms;
    $all_units_valuer[2]['nla'] = $InspectProperty->no_one_bedrooms_nla;
    $all_units_valuer[2]['qty'] = $InspectProperty->no_one_bedrooms ;
    $all_units_valuer[2]['rsf'] = round($all_units_valuer[2]['total']/$InspectProperty->no_one_bedrooms_nla) ;
    $gross_rental_income_total_valuer = $gross_rental_income_total_valuer + $all_units_valuer[2]['total'];


}


if($InspectProperty->no_two_bedrooms > 0){
    $all_units_valuer[3]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'list', 'income_type'=> 3])->orderBy(['id' => SORT_DESC])->one();
    $all_units_valuer[3]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'sold', 'income_type'=> 3])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_valuer[3]['list']->mv_total_price) && ($all_units_valuer[3]['list']->mv_total_price > 0)) && (isset($all_units_valuer[3]['sold']->mv_total_price) && ($all_units_valuer[3]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_valuer[3]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_valuer[3]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_valuer[3]['list']->mv_total_price) && ($all_units_valuer[3]['list']->mv_total_price > 0)){
        $list_25 = $all_units_valuer[3]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_valuer[3]['sold']->mv_total_price;
    }
    $all_units_valuer[3]['list_total'] = $list_25;
    $all_units_valuer[3]['sold_total'] = $ejari_75;


   // $all_units_valuer[3]['total'] = $list_25 + $ejari_75;
    $all_units_valuer[3]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_valuer[3]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_two_bedrooms;
    $all_units_valuer[3]['nla'] = $InspectProperty->no_two_bedrooms_nla;
    $all_units_valuer[3]['qty'] = $InspectProperty->no_two_bedrooms ;
    $all_units_valuer[3]['rsf'] = round($all_units_valuer[3]['total']/$InspectProperty->no_two_bedrooms_nla) ;
    $gross_rental_income_total_valuer = $gross_rental_income_total_valuer + $all_units_valuer[3]['total'];
}


if($InspectProperty->no_three_bedrooms > 0){
    $all_units_valuer[4]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'list', 'income_type'=> 4])->orderBy(['id' => SORT_DESC])->one();
    $all_units_valuer[4]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'sold', 'income_type'=> 4])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_valuer[4]['list']->mv_total_price) && ($all_units_valuer[4]['list']->mv_total_price > 0)) && (isset($all_units_valuer[4]['sold']->mv_total_price) && ($all_units_valuer[4]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_valuer[4]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_valuer[4]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_valuer[4]['list']->mv_total_price) && ($all_units_valuer[4]['list']->mv_total_price > 0)){
        $list_25 = $all_units_valuer[4]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_valuer[4]['sold']->mv_total_price;
    }
    $all_units_valuer[4]['list_total'] = $list_25;
    $all_units_valuer[4]['sold_total'] = $ejari_75;


   // $all_units_valuer[4]['total'] = $list_25 + $ejari_75;
    $all_units_valuer[4]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_valuer[4]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_three_bedrooms;
    $all_units_valuer[4]['nla'] = $InspectProperty->no_three_bedrooms_nla;
    $all_units_valuer[4]['qty'] = $InspectProperty->no_three_bedrooms ;
    $all_units_valuer[4]['rsf'] = round($all_units_valuer[4]['total']/$InspectProperty->no_three_bedrooms_nla) ;
    $gross_rental_income_total_valuer = $gross_rental_income_total_valuer + $all_units_valuer[4]['total'];
}

if($InspectProperty->no_four_bedrooms > 0){
    $all_units_valuer[5]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'list', 'income_type'=> 5])->orderBy(['id' => SORT_DESC])->one();
    $all_units_valuer[5]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'sold', 'income_type'=> 5])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_valuer[5]['list']->mv_total_price) && ($all_units_valuer[5]['list']->mv_total_price > 0)) && (isset($all_units_valuer[5]['sold']->mv_total_price) && ($all_units_valuer[5]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_valuer[5]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_valuer[5]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_valuer[5]['list']->mv_total_price) && ($all_units_valuer[5]['list']->mv_total_price > 0)){
        $list_25 = $all_units_valuer[5]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_valuer[5]['sold']->mv_total_price;
    }
    $all_units_valuer[5]['list_total'] = $list_25;
    $all_units_valuer[5]['sold_total'] = $ejari_75;


   // $all_units_valuer[5]['total'] = $list_25 + $ejari_75;
    $all_units_valuer[5]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_valuer[5]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_four_bedrooms;
    $all_units_valuer[5]['nla'] = $InspectProperty->no_four_bedrooms_nla;
    $all_units_valuer[5]['qty'] = $InspectProperty->no_four_bedrooms ;
    $all_units_valuer[5]['rsf'] = round($all_units_valuer[5]['total']/$InspectProperty->no_four_bedrooms_nla) ;
    $gross_rental_income_total_valuer = $gross_rental_income_total_valuer + $all_units_valuer[5]['total'];
}

if($InspectProperty->no_penthouse > 0){
    $all_units_valuer[6]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'list', 'income_type'=> 6])->orderBy(['id' => SORT_DESC])->one();
    $all_units_valuer[6]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'sold', 'income_type'=> 6])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_valuer[6]['list']->mv_total_price) && ($all_units_valuer[6]['list']->mv_total_price > 0)) && (isset($all_units_valuer[6]['sold']->mv_total_price) && ($all_units_valuer[6]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_valuer[6]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_valuer[6]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_valuer[6]['list']->mv_total_price) && ($all_units_valuer[6]['list']->mv_total_price > 0)){
        $list_25 = $all_units_valuer[6]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_valuer[6]['sold']->mv_total_price;
    }
    $all_units_valuer[6]['list_total'] = $list_25;
    $all_units_valuer[6]['sold_total'] = $ejari_75;


    //$all_units_valuer[6]['total'] = $list_25 + $ejari_75;
    $all_units_valuer[6]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_valuer[6]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_penthouse;
    $all_units_valuer[6]['nla'] = $InspectProperty->no_penthouse_nla;
    $all_units_valuer[6]['qty'] = $InspectProperty->no_penthouse ;
    $all_units_valuer[6]['rsf'] = round($all_units_valuer[6]['total']/$InspectProperty->no_penthouse_nla) ;
    $gross_rental_income_total_valuer = $gross_rental_income_total_valuer + $all_units_valuer[6]['total'];
}

if($InspectProperty->no_of_shops > 0){
    $all_units_valuer[7]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'list', 'income_type'=> 7])->orderBy(['id' => SORT_DESC])->one();
    $all_units_valuer[7]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'sold', 'income_type'=> 7])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_valuer[7]['list']->mv_total_price) && ($all_units_valuer[7]['list']->mv_total_price > 0)) && (isset($all_units_valuer[7]['sold']->mv_total_price) && ($all_units_valuer[7]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_valuer[7]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_valuer[7]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_valuer[7]['list']->mv_total_price) && ($all_units_valuer[7]['list']->mv_total_price > 0)){
        $list_25 = $all_units_valuer[7]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_valuer[7]['sold']->mv_total_price;
    }
    $all_units_valuer[7]['list_total'] = $list_25;
    $all_units_valuer[7]['sold_total'] = $ejari_75;


   // $all_units_valuer[7]['total'] = $list_25 + $ejari_75;
    $all_units_valuer[7]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_valuer[7]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_of_shops;
    $all_units_valuer[7]['nla'] = $InspectProperty->no_of_shops_nla;
    $all_units_valuer[7]['qty'] = $InspectProperty->no_of_shops ;
    $all_units_valuer[7]['rsf'] = round($all_units_valuer[7]['total']/$InspectProperty->no_of_shops_nla) ;
    $gross_rental_income_total_valuer = $gross_rental_income_total_valuer + $all_units_valuer[7]['total'];
}

if($InspectProperty->no_of_offices > 0){
    $all_units_valuer[8]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'list', 'income_type'=> 8])->orderBy(['id' => SORT_DESC])->one();
    $all_units_valuer[8]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 2, 'type'=> 'sold', 'income_type'=> 8])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units_valuer[8]['list']->mv_total_price) && ($all_units_valuer[8]['list']->mv_total_price > 0)) && (isset($all_units_valuer[8]['sold']->mv_total_price) && ($all_units_valuer[8]['sold']->mv_total_price > 0))){
        $list_25 = $all_units_valuer[8]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units_valuer[8]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units_valuer[8]['list']->mv_total_price) && ($all_units_valuer[8]['list']->mv_total_price > 0)){
        $list_25 = $all_units_valuer[8]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units_valuer[8]['sold']->mv_total_price;
    }
    $all_units_valuer[8]['list_total'] = $list_25;
    $all_units_valuer[8]['sold_total'] = $ejari_75;


   // $all_units_valuer[8]['total'] = $list_25 + $ejari_75;
    $all_units_valuer[8]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units_valuer[8]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_of_offices;
    $all_units_valuer[8]['nla'] = $InspectProperty->no_of_offices_nla;
    $all_units_valuer[8]['qty'] = $InspectProperty->no_of_offices ;
    $all_units_valuer[8]['rsf'] = round($all_units_valuer[8]['total']/$InspectProperty->no_of_offices_nla) ;
    $gross_rental_income_total_valuer = $gross_rental_income_total_valuer + $all_units_valuer[8]['total'];
}





// approver

if($InspectProperty->no_studios > 0){
    $all_units[1]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'list', 'income_type'=> 1])->orderBy(['id' => SORT_DESC])->one();
    $all_units[1]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'sold', 'income_type'=> 1])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units[1]['list']->mv_total_price) && ($all_units[1]['list']->mv_total_price > 0)) && (isset($all_units[1]['sold']->mv_total_price) && ($all_units[1]['sold']->mv_total_price > 0))){
        $list_25 = $all_units[1]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units[1]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units[1]['list']->mv_total_price) && ($all_units[1]['list']->mv_total_price > 0)){
        $list_25 = $all_units[1]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units[1]['sold']->mv_total_price;
    }
    $all_units[1]['list_total'] = $list_25;
    $all_units[1]['sold_total'] = $ejari_75;


    //$all_units[1]['total'] = $list_25 + $ejari_75;
    $all_units[1]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units[1]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_studios;
    $all_units[1]['nla'] = $InspectProperty->no_studios_nla;
    $all_units[1]['qty'] = $InspectProperty->no_studios ;
    $all_units[1]['rsf'] = round($all_units[1]['total']/$InspectProperty->no_studios_nla) ;
    $gross_rental_income_total = $gross_rental_income_total + $all_units[1]['total'];
}

if($InspectProperty->no_one_bedrooms > 0){
    $all_units[2]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'list', 'income_type'=> 2])->orderBy(['id' => SORT_DESC])->one();
    $all_units[2]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'sold', 'income_type'=> 2])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units[2]['list']->mv_total_price) && ($all_units[2]['list']->mv_total_price > 0)) && (isset($all_units[2]['sold']->mv_total_price) && ($all_units[2]['sold']->mv_total_price > 0))){
        $list_25 = $all_units[2]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units[2]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units[2]['list']->mv_total_price) && ($all_units[2]['list']->mv_total_price > 0)){
        $list_25 = $all_units[2]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units[2]['sold']->mv_total_price;
    }
    $all_units[2]['list_total'] = $list_25;
    $all_units[2]['sold_total'] = $ejari_75;


   // $all_units[2]['total'] = $list_25 + $ejari_75;
    $all_units[2]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units[2]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_one_bedrooms;
    $all_units[2]['nla'] = $InspectProperty->no_one_bedrooms_nla;
    $all_units[2]['qty'] = $InspectProperty->no_one_bedrooms ;
    $all_units[2]['rsf'] = round($all_units[2]['total']/$InspectProperty->no_one_bedrooms_nla) ;
    $gross_rental_income_total = $gross_rental_income_total + $all_units[2]['total'];


}



if($InspectProperty->no_two_bedrooms > 0){
    $all_units[3]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'list', 'income_type'=> 3])->orderBy(['id' => SORT_DESC])->one();
    $all_units[3]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'sold', 'income_type'=> 3])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units[3]['list']->mv_total_price) && ($all_units[3]['list']->mv_total_price > 0)) && (isset($all_units[3]['sold']->mv_total_price) && ($all_units[3]['sold']->mv_total_price > 0))){
        $list_25 = $all_units[3]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units[3]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units[3]['list']->mv_total_price) && ($all_units[3]['list']->mv_total_price > 0)){
        $list_25 = $all_units[3]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units[3]['sold']->mv_total_price;
    }
    $all_units[3]['list_total'] = $list_25;
    $all_units[3]['sold_total'] = $ejari_75;


  //  $all_units[3]['total'] = $list_25 + $ejari_75;
    $all_units[3]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units[3]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_two_bedrooms;
    $all_units[3]['nla'] = $InspectProperty->no_two_bedrooms_nla;
    $all_units[3]['qty'] = $InspectProperty->no_two_bedrooms ;
    $all_units[3]['rsf'] = round($all_units[3]['total']/$InspectProperty->no_two_bedrooms_nla) ;
    $gross_rental_income_total = $gross_rental_income_total + $all_units[3]['total'];
}


if($InspectProperty->no_three_bedrooms > 0){
    $all_units[4]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'list', 'income_type'=> 4])->orderBy(['id' => SORT_DESC])->one();
    $all_units[4]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'sold', 'income_type'=> 4])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units[4]['list']->mv_total_price) && ($all_units[4]['list']->mv_total_price > 0)) && (isset($all_units[4]['sold']->mv_total_price) && ($all_units[4]['sold']->mv_total_price > 0))){
        $list_25 = $all_units[4]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units[4]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units[4]['list']->mv_total_price) && ($all_units[4]['list']->mv_total_price > 0)){
        $list_25 = $all_units[4]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units[4]['sold']->mv_total_price;
    }
    $all_units[4]['list_total'] = $list_25;
    $all_units[4]['sold_total'] = $ejari_75;


  //  $all_units[4]['total'] = $list_25 + $ejari_75;
    $all_units[4]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units[4]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_three_bedrooms;
    $all_units[4]['nla'] = $InspectProperty->no_three_bedrooms_nla;
    $all_units[4]['qty'] = $InspectProperty->no_three_bedrooms ;
    $all_units[4]['rsf'] = round($all_units[4]['total']/$InspectProperty->no_three_bedrooms_nla) ;
    $gross_rental_income_total = $gross_rental_income_total + $all_units[4]['total'];
}

if($InspectProperty->no_four_bedrooms > 0){
    $all_units[5]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'list', 'income_type'=> 5])->orderBy(['id' => SORT_DESC])->one();
    $all_units[5]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'sold', 'income_type'=> 5])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units[5]['list']->mv_total_price) && ($all_units[5]['list']->mv_total_price > 0)) && (isset($all_units[5]['sold']->mv_total_price) && ($all_units[5]['sold']->mv_total_price > 0))){
        $list_25 = $all_units[5]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units[5]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units[5]['list']->mv_total_price) && ($all_units[5]['list']->mv_total_price > 0)){
        $list_25 = $all_units[5]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units[5]['sold']->mv_total_price;
    }
    $all_units[5]['list_total'] = $list_25;
    $all_units[5]['sold_total'] = $ejari_75;


   // $all_units[5]['total'] = $list_25 + $ejari_75;
    $all_units[5]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units[5]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_four_bedrooms;
    $all_units[5]['nla'] = $InspectProperty->no_four_bedrooms_nla;
    $all_units[5]['qty'] = $InspectProperty->no_four_bedrooms ;
    $all_units[5]['rsf'] = round($all_units[5]['total']/$InspectProperty->no_four_bedrooms_nla) ;
    $gross_rental_income_total = $gross_rental_income_total + $all_units[5]['total'];
}

if($InspectProperty->no_penthouse > 0){
    $all_units[6]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'list', 'income_type'=> 6])->orderBy(['id' => SORT_DESC])->one();
    $all_units[6]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'sold', 'income_type'=> 6])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units[6]['list']->mv_total_price) && ($all_units[6]['list']->mv_total_price > 0)) && (isset($all_units[6]['sold']->mv_total_price) && ($all_units[6]['sold']->mv_total_price > 0))){
        $list_25 = $all_units[6]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units[6]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units[6]['list']->mv_total_price) && ($all_units[6]['list']->mv_total_price > 0)){
        $list_25 = $all_units[6]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units[6]['sold']->mv_total_price;
    }
    $all_units[6]['list_total'] = $list_25;
    $all_units[6]['sold_total'] = $ejari_75;


   // $all_units[6]['total'] = $list_25 + $ejari_75;
    $all_units[6]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units[6]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_penthouse;
    $all_units[6]['nla'] = $InspectProperty->no_penthouse_nla;
    $all_units[6]['qty'] = $InspectProperty->no_penthouse ;
    $all_units[6]['rsf'] = round($all_units[6]['total']/$InspectProperty->no_penthouse_nla) ;
    $gross_rental_income_total = $gross_rental_income_total + $all_units[6]['total'];
}

if($InspectProperty->no_of_shops > 0){
    $all_units[7]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'list', 'income_type'=> 7])->orderBy(['id' => SORT_DESC])->one();
    $all_units[7]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'sold', 'income_type'=> 7])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units[7]['list']->mv_total_price) && ($all_units[7]['list']->mv_total_price > 0)) && (isset($all_units[7]['sold']->mv_total_price) && ($all_units[7]['sold']->mv_total_price > 0))){
        $list_25 = $all_units[7]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units[7]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units[7]['list']->mv_total_price) && ($all_units[7]['list']->mv_total_price > 0)){
        $list_25 = $all_units[7]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units[7]['sold']->mv_total_price;
    }
    $all_units[7]['list_total'] = $list_25;
    $all_units[7]['sold_total'] = $ejari_75;


  //  $all_units[7]['total'] = $list_25 + $ejari_75;
    $all_units[7]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units[7]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_of_shops;
    $all_units[7]['nla'] = $InspectProperty->no_of_shops_nla;
    $all_units[7]['qty'] = $InspectProperty->no_of_shops ;
    $all_units[7]['rsf'] = round($all_units[7]['total']/$InspectProperty->no_of_shops_nla) ;
    $gross_rental_income_total = $gross_rental_income_total + $all_units[7]['total'];
}

if($InspectProperty->no_of_offices > 0){
    $all_units[8]['list'] = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'list', 'income_type'=> 8])->orderBy(['id' => SORT_DESC])->one();
    $all_units[8]['sold']  = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $valuation->id, 'search_type'=> 0, 'type'=> 'sold', 'income_type'=> 8])->orderBy(['id' => SORT_DESC])->one();
    $list_25 = 0;
    $ejari_75 = 0;
    if((isset($all_units[8]['list']->mv_total_price) && ($all_units[8]['list']->mv_total_price > 0)) && (isset($all_units[8]['sold']->mv_total_price) && ($all_units[8]['sold']->mv_total_price > 0))){
        $list_25 = $all_units[8]['list']->mv_total_price * 0.25;
        $ejari_75 = $all_units[8]['sold']->mv_total_price * 0.75;
    }else if(isset($all_units[8]['list']->mv_total_price) && ($all_units[8]['list']->mv_total_price > 0)){
        $list_25 = $all_units[8]['list']->mv_total_price;
        $ejari_75 = 0;
    }else{
        $list_25 = 0;
        $ejari_75 = $all_units[8]['sold']->mv_total_price;
    }
    $all_units[8]['list_total'] = $list_25;
    $all_units[8]['sold_total'] = $ejari_75;


   // $all_units[8]['total'] = $list_25 + $ejari_75;
    $all_units[8]['total_per_unit'] = $list_25 + $ejari_75;
    $all_units[8]['total'] = ($list_25 + $ejari_75) * $InspectProperty->no_of_offices;
    $all_units[]['nla'] = $InspectProperty->no_of_offices_nla;
    $all_units[8]['qty'] = $InspectProperty->no_of_offices ;
    $all_units[8]['rsf'] = round($all_units[8]['total']/$InspectProperty->no_of_offices_nla) ;
    $gross_rental_income_total = $gross_rental_income_total + $all_units[8]['total'];
}