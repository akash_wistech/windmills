<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use kartik\depdrop\DepDrop;
use app\assets\ValuationFormAsset;

ValuationFormAsset::register($this);
ListingsFormAsset::register($this);
use app\components\widgets\StatusVerified;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enter Property Details');
$cardTitle = Yii::t('app', 'Ref No:  {nameAttribute}', [
    'nameAttribute' => $quotation->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['crm-quotations/step_1?id=' . $quotation->id . '&']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');


$bcsScopesArrayFull = [6, 7, 8, 9, 10];


$villas = Yii::$app->appHelperFunctions->villaIdArr;
$lands = Yii::$app->appHelperFunctions->landIdArr;

$landIds = [];
foreach ($lands as $land) {
    $landIds[] = $land->id;
}

$villaIds = [];
foreach ($villas as $villa) {
    $villaIds[] = $villa->id;
}

$no_show_building_number = array_merge($landIds);

$no_show_unit_number = array_merge(array(42, 81, 3, 25, 10), $landIds);
$no_show_property_standard = array_merge($landIds);

$no_show_floor_number = array_merge(array(42, 81, 3, 25, 10, 6), $landIds, $villaIds);
$no_show_number_floors = array_merge(array(28));
$no_show_basement_floor = array_merge(array(28, 17, 24, 1));
$no_show_parking_space = array_merge($landIds);
$no_show_swimming_pool = array_merge(array(28, 17, 24), $landIds);
$no_show_jacuzzi = array_merge(array(28, 17, 24, 1, 6, 81, 3, 25), $landIds, $villaIds);

$no_show_bua = array_merge(array(28, 17, 24));
$no_show_net_leasable_area = array_merge(array(1, 6), $villaIds);
$no_show_land_size = array_merge(array(1, 17, 28));

$no_show_upgrade_star_rating = array_merge(array(), $landIds);
$no_show_furnished = array_merge(array(24, 3, 25), $landIds);
$no_show_ready = array_merge(array(), $landIds);

$no_show_number_of_building = array_merge(array(1, 6, 28, 17, 24, 10), $villaIds);
$no_show_mezzanine_floor = array_merge(array(1, 6, 28, 17, 10), $villaIds);
$no_show_parking_floor = array_merge(array(1, 6, 28, 17, 24, 10), $villaIds);

$no_show_number_of_units = array_merge(array(1, 6, 28, 17 ), $villaIds, $landIds);
$no_show_residential_units = array_merge(array(1, 6, 28, 17, 81, 42, 25, 28, 17, 41, 2), $landIds);
$no_show_commercial_units = array_merge(array(1, 6, 28, 17, 81, 42, 25, 28, 17, 41, 2), $landIds);
$no_show_retail_units = array_merge(array(1, 6, 28, 17, 42, 81, 28, 17, 41, 2), $landIds);
$no_show_types_of_units = array_merge(array(1, 6, 24, 28, 17, 24, 28, 17, 41, 2), $landIds);

$no_show_no_of_units_value = array_merge(array(1, 6, 28, 17 ), $villaIds, $landIds);
$no_show_no_of_res_units_value = array_merge(array(1, 6, 28, 17, 81, 42, 25, 28, 17, 41, 2), $landIds);
$no_show_no_of_com_units_value = array_merge(array(1, 6, 28, 17, 81, 42, 25, 28, 17, 41, 2), $landIds);
$no_show_no_of_ret_units_value = array_merge(array(1, 6, 28, 17, 42, 81, 28, 17, 41, 2), $landIds);

$no_show_meeting_room = array_merge(array(1, 6, 24, 28, 17, 25, 24, 28, 17, 41, 2), $landIds);
$no_show_nbbq = array_merge(array(1, 6, 24, 28, 17, 25, 24, 28, 17, 42, 41, 2), $landIds);
$no_show_health_spa = array_merge(array(1, 6, 24, 28, 17, 25, 24, 28, 17, 41, 2), $landIds);
$no_show_child_play_area = array_merge(array(1, 6, 24, 28, 17, 25, 24, 28, 17, 41, 2), $landIds);
$no_show_gyms = array_merge(array(1, 6, 24, 28, 17, 24, 28, 17, 41, 2), $landIds);
$no_show_schools = array_merge(array(1, 6, 24, 28, 17, 25, 24, 28, 17, 41, 2), $landIds);
$no_show_clinics = array_merge(array(1, 6, 24, 28, 17, 24, 28, 17, 41, 2), $landIds);
$no_show_sports_courts = array_merge(array(1, 6, 24, 28, 17, 25, 24, 28, 17, 41, 2), $landIds);
$no_show_mosque = array_merge(array(1, 6, 24, 28, 17, 24, 28, 17, 41, 2), $landIds);

$no_show_restaurants = array_merge(array(1, 6, 24, 28, 17, 24, 28, 17, 41, 2), $landIds);
$no_show_natm = array_merge(array(1, 6, 24, 28, 17, 25, 24, 28, 17, 41, 2), $landIds);
$no_show_coffee_shops = array_merge(array(1, 6, 24, 28, 17, 24, 28, 17, 41, 2), $landIds);
$no_show_sign_boards = array_merge(array(1, 6, 24, 28, 17, 25, 24, 28, 17, 41, 2), $landIds);
$no_show_night_clubs = array_merge(array(1, 6, 24, 28, 17, 25, 24, 28, 17, 41, 2), $landIds);
$no_show_nbars = array_merge(array(1, 6, 24, 28, 17, 25, 24, 28, 17, 41, 2), $landIds);




$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});


$(document).ready(function () {
    var $building_info = $("#crmreceivedproperties-building_info");
    var $property_id = $("#crmreceivedproperties-property_id");
    var $valuation_approach = $("#crmreceivedproperties-valuation_approach");
    var $purp_of_valuation = ' . json_encode($quotation->purpose_of_valuation) . ';

    function toggleFieldsVisibilityByPropertyType(propertyTypeArray, className) {
        if (propertyTypeArray.includes(parseInt($property_id.val()))) {
            $("." + className).hide();
        } else {
            $("." + className).show();
        }
    }

    var noShowFieldBuildingNumber= ' . json_encode($no_show_building_number) . ';

    var noShowFieldUnitNumber= ' . json_encode($no_show_unit_number) . ';
    var noShowFieldPropertyStandard= ' . json_encode($no_show_property_standard) . ';

    var noShowFieldFloorNumber= ' . json_encode($no_show_floor_number) . ';
    var noShowFieldNumberFloors= ' . json_encode($no_show_number_floors) . ';
    var noShowFieldBasementFloors= ' . json_encode($no_show_basement_floor) . ';
    var noShowFieldParkingSpace= ' . json_encode($no_show_parking_space) . ';
    var noShowFieldSwimmingPools= ' . json_encode($no_show_swimming_pool) . ';
    var noShowFieldJacuzzi= ' . json_encode($no_show_jacuzzi) . ';

    var noShowFieldBUA= ' . json_encode($no_show_bua) . ';
    var noShowFieldNetLeasableArea= ' . json_encode($no_show_net_leasable_area) . ';
    var noShowFieldNetLandSize= ' . json_encode($no_show_land_size) . ';

    var noShowFieldUpgradeStarRating= ' . json_encode($no_show_upgrade_star_rating) . ';
    var noShowFieldFurnished= ' . json_encode($no_show_furnished) . ';
    var noShowFieldReady= ' . json_encode($no_show_ready) . ';

    var noShowFieldNumberOfBuilding= ' . json_encode($no_show_number_of_building) . ';
    var noShowFieldMezzanineFloor= ' . json_encode($no_show_mezzanine_floor) . ';
    var noShowFieldParkingFloor= ' . json_encode($no_show_parking_floor) . ';

    var noShowFieldNumberOfUnits= ' . json_encode($no_show_number_of_units) . ';
    var noShowFieldResidentialUnits= ' . json_encode($no_show_residential_units) . ';
    var noShowFieldCommercialUnits= ' . json_encode($no_show_commercial_units) . ';
    var noShowFieldRetailUnits= ' . json_encode($no_show_retail_units) . ';
    var noShowFieldTypesOfUnits= ' . json_encode($no_show_types_of_units) . ';

    var noShowFieldNoOfUnitsValue= ' . json_encode($no_show_no_of_units_value) . ';
    var noShowFieldNoOfResUnitsValue= ' . json_encode($no_show_no_of_res_units_value) . ';
    var noShowFieldNoOfComUnitsValue= ' . json_encode($no_show_no_of_com_units_value) . ';
    var noShowFieldNoOfRetUnitsValue= ' . json_encode($no_show_no_of_ret_units_value) . ';

    var noShowFieldMeetingRooms= ' . json_encode($no_show_meeting_room) . ';
    var noShowFieldHealthSpa= ' . json_encode($no_show_health_spa) . ';
    var noShowFieldBBQ= ' . json_encode($no_show_nbbq) . ';
    var noShowFieldChildPlayArea= ' . json_encode($no_show_child_play_area) . ';
    var noShowFieldGyms= ' . json_encode($no_show_gyms) . ';
    var noShowFieldSchools= ' . json_encode($no_show_schools) . ';
    var noShowFieldClinics= ' . json_encode($no_show_clinics) . ';
    var noShowFieldSportsCourts= ' . json_encode($no_show_sports_courts) . ';
    var noShowFieldMosque= ' . json_encode($no_show_mosque) . ';

    var noShowFieldRestaurants= ' . json_encode($no_show_restaurants) . ';
    var noShowFieldAtms= ' . json_encode($no_show_natm) . ';
    var noShowFieldCoffeeShops= ' . json_encode($no_show_coffee_shops) . ';
    var noShowFieldSignBoards= ' . json_encode($no_show_sign_boards) . ';
    var noShowFieldNightClubs= ' . json_encode($no_show_night_clubs) . ';
    var noShowFieldBars= ' . json_encode($no_show_nbars) . ';
    

    function toggleFunctions(){   

        toggleFieldsVisibilityByPropertyType(noShowFieldBuildingNumber, "building_number");

        toggleFieldsVisibilityByPropertyType(noShowFieldUnitNumber, "unit_number");
        toggleFieldsVisibilityByPropertyType(noShowFieldPropertyStandard, "standard_comlexity");

        toggleFieldsVisibilityByPropertyType(noShowFieldFloorNumber, "floor_number");
        toggleFieldsVisibilityByPropertyType(noShowFieldNumberFloors, "no_of_floors");
        toggleFieldsVisibilityByPropertyType(noShowFieldBasementFloors, "no_of_basement_floors");
        toggleFieldsVisibilityByPropertyType(noShowFieldParkingSpace, "no_of_parking_space");
        toggleFieldsVisibilityByPropertyType(noShowFieldSwimmingPools, "no_of_swimming_pools");
        toggleFieldsVisibilityByPropertyType(noShowFieldJacuzzi, "no_of_jacuzzi");

        toggleFieldsVisibilityByPropertyType(noShowFieldBUA, "built_up_area");
        toggleFieldsVisibilityByPropertyType(noShowFieldNetLeasableArea, "net_leasable_area");
        toggleFieldsVisibilityByPropertyType(noShowFieldNetLandSize, "land_size");

        toggleFieldsVisibilityByPropertyType(noShowFieldUpgradeStarRating, "upgrades_star_rating");
        toggleFieldsVisibilityByPropertyType(noShowFieldFurnished, "furnished");
        toggleFieldsVisibilityByPropertyType(noShowFieldReady, "ready");

        toggleFieldsVisibilityByPropertyType(noShowFieldNumberOfBuilding, "no_of_buildings");
        toggleFieldsVisibilityByPropertyType(noShowFieldMezzanineFloor, "mezzanine_floors");
        toggleFieldsVisibilityByPropertyType(noShowFieldParkingFloor, "parking_floors");

        toggleFieldsVisibilityByPropertyType(noShowFieldNumberOfUnits, "no_of_units");
        toggleFieldsVisibilityByPropertyType(noShowFieldResidentialUnits, "no_of_residential_units");
        toggleFieldsVisibilityByPropertyType(noShowFieldCommercialUnits, "no_of_commercial_units");
        toggleFieldsVisibilityByPropertyType(noShowFieldRetailUnits, "no_of_retails_units");
        toggleFieldsVisibilityByPropertyType(noShowFieldTypesOfUnits, "types_of_unit");

        toggleFieldsVisibilityByPropertyType(noShowFieldNoOfUnitsValue, "no_of_units_value");
        toggleFieldsVisibilityByPropertyType(noShowFieldNoOfResUnitsValue, "no_of_res_units_value");
        toggleFieldsVisibilityByPropertyType(noShowFieldNoOfComUnitsValue, "no_of_com_units_value");
        toggleFieldsVisibilityByPropertyType(noShowFieldNoOfRetUnitsValue, "no_of_ret_units_value");

        toggleFieldsVisibilityByPropertyType(noShowFieldMeetingRooms, "meeting_rooms");
        toggleFieldsVisibilityByPropertyType(noShowFieldHealthSpa, "health_club_spa");
        toggleFieldsVisibilityByPropertyType(noShowFieldBBQ, "bbq_area");
        toggleFieldsVisibilityByPropertyType(noShowFieldChildPlayArea, "children_play_area");
        toggleFieldsVisibilityByPropertyType(noShowFieldGyms, "number_gyms");
        toggleFieldsVisibilityByPropertyType(noShowFieldSchools, "number_schools");
        toggleFieldsVisibilityByPropertyType(noShowFieldClinics, "number_clinics");
        toggleFieldsVisibilityByPropertyType(noShowFieldSportsCourts, "sports_courts");
        toggleFieldsVisibilityByPropertyType(noShowFieldMosque, "number_mosque");

        toggleFieldsVisibilityByPropertyType(noShowFieldRestaurants, "number_restaurants");
        toggleFieldsVisibilityByPropertyType(noShowFieldAtms, "number_atms");
        toggleFieldsVisibilityByPropertyType(noShowFieldCoffeeShops, "number_coffee_shops");
        toggleFieldsVisibilityByPropertyType(noShowFieldSignBoards, "number_sign_boards");
        toggleFieldsVisibilityByPropertyType(noShowFieldNightClubs, "number_night_clubs");
        toggleFieldsVisibilityByPropertyType(noShowFieldBars, "number_bars");
    }


    $building_info.on("change", function () {
        toggleFunctions();
        valuationApproach();
    });

    $property_id.on("change", function () {
        toggleFunctions();
        valuationApproach();
    });

    if($building_info.val() != ""){ 
        toggleFunctions();
        valuationApproach();
    }

    valuationApproach();
    
    function valuationApproach() {
        if($property_id.val() == 42 ){

            if($purp_of_valuation == 14 ){
                $valuation_approach.val("4");
            }
            // else if($purp_of_valuation == 1 ){
            //     $valuation_approach.val("2");
            // }
            // else{
            //     $valuation_approach.val("1");
            // }
        }
        // else{
        //     $valuation_approach.val("1");
        // }
        
    }


}); 
    
    
 

    

');
?>
<!--<script>
    $("#crmreceivedproperties-property_id").on('change',function(){
        if($(this).val()==1){
            $("#crmreceivedproperties-no_of_units").hide();
        }else{
            $("#crmreceivedproperties-no_of_units").show();
        }

    });
</script>-->
<?php
$numOfProp = [];
if ($model->property_id == 3 || $model->property_id == 7 || $model->property_id == 22 || $model->property_id == 24) {
    $numOfProp = yii::$app->quotationHelperFunctions->numberofUnitsInBuildingArr;
} else {
    $numOfProp = yii::$app->quotationHelperFunctions->numberofUnitsInLandArr;
}
$numOfProp = yii::$app->quotationHelperFunctions->numberofUnitsInBuildingArr;
$companyAddLink = '';
if (Yii::$app->menuHelperFunction->checkActionAllowed('create', 'client')) {
    $companyAddLink = ' <a href="javascript:;" class="btn btn-xs btn-success load-modal" data-url="' . Url::to(['buildings/create-crm?id=865&property_index=0']) . '" data-heading="' . Yii::t('app', 'New Project') . '">';
    $companyAddLink .= '  <i class="fa fa-plus"></i>';
    $companyAddLink .= '</a>';
}


$numOfUnitFloors = [];
if ($model->property_id == 1 ) {
    $numOfUnitFloors = Yii::$app->appHelperFunctions->crmNoOfFloors;
} else {
    $numOfUnitFloors = Yii::$app->appHelperFunctions->crmNoOfApartmentFloors;
}


// dd($quotation->purpose_of_valuation);


?>
<style>
    .datepicker-days .table>thead>tr>th,
    .table>tbody>tr>th,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>tbody>tr>td,
    .table>tfoot>tr>td {
        padding: inherit !important;
    }

    .help-block2 {
        color: red;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -14px;
        font-family: inherit;
    }
</style>
<style>
    /* .profit {
        display: none;
    }

    .income {
        display: none;
    } */

    .income-profit-cost {
        display: none;
    }

    .income-profit {
        display: none;
    }
</style>
<?php if ($model->valuation_approach == 1) { ?>
    <style>
        /* .profit-income {
                        display: none;
                    }

                    .only-profit {
                        display: none;
                    } */

        /* .profit{
                                    display: none;
                                    }
                                    .income{
                                    display: none;
                                    } */
    </style>
<?php } ?>
<?php if ($model->valuation_approach == 2) { ?>
    <style>
        /* .only-profit {
                        display: none;
                    } */

        /* .profit{
                                    display: none;
                                    } */
    </style>
<?php } ?>
<?php if ($model->valuation_approach == 3) { ?>
    <style>
        /* .income{
                                    display: none;
                                    } */
    </style>
<?php } ?>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title float-none text-md-right ">
            <i class="fas fa-edit"></i>
            <?= $cardTitle ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 1, 'quotation' => $quotation, 'property_index' => $property_index]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form ">


                            <?php $form = ActiveForm::begin(); ?>
                            <div class="repeat-section">

                                <div class="card card-outline card-primary mb-3" id="address_details">
                                    <header class="card-header">
                                        <h2 class="card-title">Address Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">

                                            <div class="col-sm-4">h
                                                <?php
                                                echo $form->field($model, 'building_info')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(\app\models\Buildings::find()->where(['status' => [1,2]])->orderBy([
                                                        'title' => SORT_ASC,
                                                    ])->all(), 'id', 'title'),
                                                    'options' => ['placeholder' => 'Select a Building ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Building Name / Project Name' . $companyAddLink);
                                                ?>
                                            </div>

                                            <div class="col-sm-4 building_number">
                                                <?= $form->field($model, 'building_number')->textInput(['maxlength' => true])->label('Building Number / Project Number'); ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'sub_community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'plot_number')->textInput(['maxlength' => true]) ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'street')->textInput(['maxlength' => true])->label('Street Name'); ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="card card-outline card-primary mb-3" id="property_details">
                                    <header class="card-header">
                                        <h2 class="card-title">Property Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">

                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy([
                                                        'title' => SORT_ASC,
                                                    ])->all(), 'id', 'title'),
                                                    'options' => ['placeholder' => 'Select a Property Type ...', 'class' => 'property_type'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Property Type');
                                                ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                                ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                                ?>
                                            </div>


                                            <div class="col-sm-4 standard_comlexity">
                                                <?= $form->field($model, 'complexity')->widget(Select2::classname(), [
                                                    'data' => yii::$app->quotationHelperFunctions->getComplexity(),
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control complexity'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Property Standard/Complexity <span class="text-danger">*</span>'); ?>
                                            </div>

                                            <?php
                                            if (in_array($quotation->scope_of_service, $bcsScopesArrayFull)) { ?>
                                                <!-- <div class="col-sm-4">
                                                    <?php

                                                // $model->drawings_available = explode(',', ($model->drawings_available <> null) ? $model->drawings_available : "");
                                                // echo $form->field($model, 'drawings_available')->widget(Select2::classname(), [
                                                //     'data' => Yii::$app->appHelperFunctions->drawingsAvailable,
                                                //     'options' => ['placeholder' => 'Select ...'],
                                                //     'pluginOptions' => [
                                                //         'placeholder' => 'Select Drawing Available ...',
                                                //         'multiple' => true,
                                                //         'allowClear' => true
                                                //     ],
                                                // ]);

                                                ?>
                                                   
                                                </div> -->

                                                <div class="col-sm-4 civil_drawing_available">
                                                    <?php
                                                    echo $form->field($model, 'civil_drawing_available')->widget(Select2::classname(), [
                                                        'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                                        'options' => ['placeholder' => 'Select ...', 'class' => 'form-control'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Civil Drawing Available? <span class="text-danger">*</span>');
                                                    ?>
                                                </div>
                                                <div class="col-sm-4 mechanical_drawing_available">
                                                    <?php
                                                    echo $form->field($model, 'mechanical_drawing_available')->widget(Select2::classname(), [
                                                        'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                                        'options' => ['placeholder' => 'Select ...', 'class' => 'form-control'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Mechanical Drawing Available? <span class="text-danger">*</span>');
                                                    ?>
                                                </div>
                                                <div class="col-sm-4 electrical_drawing_available">
                                                    <?php
                                                    echo $form->field($model, 'electrical_drawing_available')->widget(Select2::classname(), [
                                                        'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                                        'options' => ['placeholder' => 'Select ...', 'class' => 'form-control'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Electrical Drawing Available? <span class="text-danger">*</span>');
                                                    ?>
                                                </div>
                                                <div class="col-sm-4 plumbing_drawing_available">
                                                    <?php
                                                    echo $form->field($model, 'plumbing_drawing_available')->widget(Select2::classname(), [
                                                        'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                                        'options' => ['placeholder' => 'Select ...', 'class' => 'form-control'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Plumbing Drawing Available? <span class="text-danger">*</span>');
                                                    ?>
                                                </div>
                                                <div class="col-sm-4 hvac_drawing_available">
                                                    <?php
                                                    echo $form->field($model, 'hvac_drawing_available')->widget(Select2::classname(), [
                                                        'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                                        'options' => ['placeholder' => 'Select ...', 'class' => 'form-control'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('HVAC Drawing Available? <span class="text-danger">*</span>');
                                                    ?>
                                                </div>
                                            <?php } ?>



                                        </div>
                                    </div>
                                </div>

                                <div class="card card-outline card-primary mb-3" id="valuation_details">
                                    <header class="card-header">
                                        <h2 class="card-title">Valuation Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <?php
                                            if (in_array($quotation->scope_of_service, $bcsScopesArrayFull)) {} else{ ?>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, 'valuation_approach')->dropDownList(Yii::$app->appHelperFunctions->valuationApproachListArr)->label('Valuation Approach <span class="text-danger">*</span>'); ?>
                                                </div>
                                            <?php } ?>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'repeat_valuation')->widget(Select2::classname(), [
                                                    'data' => yii::$app->quotationHelperFunctions->getNewRepeatValuation(),
                                                    'options' => ['class' => 'repeat_valuation'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Repeat Valuation/Assignment'); ?>
                                            </div>

                                            <?php if($quotation->id < 2237){ ?>
                                                <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull)) {} else{ ?>
                                                    <div class="col-sm-4">
                                                        <?= $form->field($model, 'number_of_comparables')->widget(Select2::classname(), [
                                                            'data' => yii::$app->quotationHelperFunctions->getNumberofComparables(),
                                                            'options' => ['placeholder' => 'Select', 'class' => 'number_of_comparables'],
                                                            'pluginOptions' => [
                                                                'allowClear' => true
                                                            ],
                                                        ])->label('Number Of Comparables <span class="text-danger">*</span>'); ?>
                                                    </div>
                                                <?php } ?>
                                            <?php }else { ?>
                                                <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull)) {} else{ ?>
                                                    <div class="col-sm-4 no_of_comparables_value">
                                                        <?php
                                                        echo $form->field($model, 'no_of_comparables_value')->widget(Select2::classname(), [
                                                            'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                            'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                        ])->label('Number Of Comparables <span class="text-danger">*</span>');
                                                        ?>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>


                                        </div>
                                    </div>
                                </div>

                                <div class="card card-outline card-primary mb-3" id="unit_details">
                                    <header class="card-header">
                                        <h2 class="card-title">Unit Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">

                                            <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull) && $quotation->scope_of_service != 8 && $quotation->scope_of_service != 10) {} else{ ?>
                                                <div class="col-sm-4 m unit_number">
                                                    <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true])->label('Unit Number <span class="text-danger">*</span>'); ?>
                                                    <div class="help-block2"></div>
                                                </div>
                                            <?php } ?>

                                            <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull) && $quotation->scope_of_service != 8 && $quotation->scope_of_service != 10) {} else{ ?>
                                                <div class="col-sm-4 floor_number">
                                                    <?php
                                                    echo $form->field($model, 'floor_number')->widget(Select2::classname(), [
                                                        'data' => Yii::$app->appHelperFunctions->crmFloorNumber,
                                                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    ])->label('Floor Number  <span class="text-danger">*</span>');
                                                    ?>
                                                    <div class="help-block2"></div>
                                                </div>
                                            <?php } ?>



                                            <div class="col-sm-4 no_of_floors">
                                                <?php
                                                echo $form->field($model, 'typical_floors')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmNoOfFloors,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                ])->label('Number of Unit Floors <span class="text-danger">*</span>');
                                                ?>
                                            </div>


                                            <div class="col-sm-4 no_of_basement_floors">
                                                <?php
                                                echo $form->field($model, 'basement_floors')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToFiftyNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                ])->label('Number of Basements Floors <span class="text-danger">*</span>');
                                                ?>
                                            </div>


                                            <div class="col-sm-4 no_of_parking_space">
                                                <?php
                                                echo $form->field($model, 'parking_space')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToFiftyNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                ])->label('Number of Parking Spaces <span class="text-danger">*</span>');
                                                ?>
                                            </div>


                                            <div class="col-sm-4 no_of_swimming_pools">
                                                <?php
                                                echo $form->field($model, 'swimming_pools')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                ])->label('Number of Swimming Pools <span class="text-danger">*</span>');
                                                ?>
                                            </div>

                                            <?php
                                            if (isset($model->property_id) && in_array($model->property_id, $no_show_jacuzzi)) { ?>
                                            <?php } else { ?>
                                                <div class="col-sm-4 no_of_jacuzzi">
                                                    <?php
                                                    echo $form->field($model, 'no_of_jacuzzi')->widget(Select2::classname(), [
                                                        'data' => Yii::$app->appHelperFunctions->crmOptionZeroToFiftyNumber,
                                                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    ])->label('Number of Jacuzzi <span class="text-danger">*</span>');
                                                    ?>
                                                </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="card card-outline card-primary mb-3" id="size_details">
                                    <header class="card-header">
                                        <h2 class="card-title">Size Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">


                                            <div class="col-sm-4 built_up_area">
                                                <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true])->label('Built Up Area (SQ.FT) <span class="text-danger">*</span>'); ?>
                                                <div class="help-block2"></div>
                                            </div>


                                            <div class="col-sm-4 net_leasable_area">
                                                <?= $form->field($model, 'net_leasable_area')->textInput(['maxlength' => true])->label('Net Leasable Area <span class="text-danger">*</span>'); ?>
                                                <div class="help-block2"></div>
                                            </div>



                                            <div class="col-sm-4 land_size">
                                                <?= $form->field($model, 'land_size')->textInput(['maxlength' => true])->label('Land Size <span class="text-danger">*</span>'); ?>
                                                <div class="help-block2"></div>
                                            </div>


                                        </div>
                                    </div>
                                </div>



                                <div class="card card-outline card-primary mb-3" id="quality_details">
                                    <header class="card-header">
                                        <h2 class="card-title">Quality Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">


                                            <div class="col-sm-4 upgrades_star_rating">
                                                <?php
                                                echo $form->field($model, 'upgrades')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->upgrades,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                ])->label('Upgrades Star Rating <span class="text-danger">*</span>');
                                                ?>
                                            </div>



                                            <div class="col-sm-4 furnished">
                                                <?php
                                                echo $form->field($model, 'furnished')->widget(Select2::classname(), [
                                                    'data' => array('No' => 'No', 'Yes' => 'Yes', 'Semi-Furnished' => 'Semi-Furnished'),
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Furnished <span class="text-danger">*</span>');
                                                ?>
                                            </div>


                                            <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull)) {} else{ ?>
                                                <div class="col-sm-4 ready">
                                                    <?php
                                                    echo $form->field($model, 'ready')->widget(Select2::classname(), [
                                                        'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Ready ?<span class="text-danger">*</span>');
                                                    ?>
                                                </div>
                                            <?php } ?>


                                        </div>
                                    </div>
                                </div>

                                <div class="card card-outline card-primary mb-3" id="property_details_2">
                                    <header class="card-header">
                                        <h2 class="card-title">Property Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">

                                            <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull)) {} else{ ?>
                                                <div class="col-sm-4 no_of_buildings">
                                                    <?php
                                                    echo $form->field($model, 'no_of_buildings')->widget(Select2::classname(), [
                                                        'data' => Yii::$app->appHelperFunctions->crmOptionZeroToFiftyNumber,
                                                    ])->label('Number of Buildings <span class="text-danger">*</span>');
                                                    ?>
                                                </div>
                                            <?php } ?>




                                            <div class="col-sm-4 mezzanine_floors">
                                                <?php
                                                echo $form->field($model, 'mezzanine_floors')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToFiftyNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                ])->label('Number of Mezzanine Floors <span class="text-danger">*</span>');
                                                ?>
                                            </div>



                                            <div class="col-sm-4 parking_floors">
                                                <?php
                                                echo $form->field($model, 'parking_floors')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToFiftyNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                ])->label('Number of Parking Floors <span class="text-danger">*</span>');
                                                ?>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="card card-outline card-primary mb-3" id="configuration_details">
                                    <header class="card-header">
                                        <h2 class="card-title">Configuration Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">


                                            <?php if($quotation->id < 2237){ ?>

                                                <div class="col-sm-4 num-unit no_of_units" id="unit_simple">
                                                    <?= $form->field($model, 'no_of_units')->widget(Select2::classname(), [
                                                        'data' => $numOfProp,
                                                        'options' => ['placeholder' => 'Select', 'class' => 'no_of_units form-control'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Number of Units '); ?>
                                                </div>

                                                <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull) && $quotation->scope_of_service != 9 && $quotation->scope_of_service != 10) {} else{ ?>
                                                    <div class="col-sm-4 no_of_residential_units">
                                                        <?= $form->field($model, 'no_of_residential_units')->widget(Select2::classname(), [
                                                            'data' => $numOfProp,
                                                            'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                            'pluginOptions' => [
                                                                'allowClear' => true
                                                            ],
                                                        ])->label('Total Number of Residential Unit <span class="text-danger">*</span>'); ?>
                                                    </div>
                                                <?php } ?>

                                                <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull) && $quotation->scope_of_service != 9 && $quotation->scope_of_service != 10) {} else{ ?>
                                                    <div class="col-sm-4 no_of_commercial_units">
                                                        <?= $form->field($model, 'no_of_commercial_units')->widget(Select2::classname(), [
                                                            'data' => $numOfProp,
                                                            'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                            'pluginOptions' => [
                                                                'allowClear' => true
                                                            ],
                                                        ])->label('Total Number of Commercial Unit <span class="text-danger">*</span>'); ?>
                                                    </div>
                                                <?php } ?>

                                                <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull) && $quotation->scope_of_service != 9 && $quotation->scope_of_service != 10) {} else{ ?>
                                                    <div class="col-sm-4 no_of_retails_units">
                                                        <?= $form->field($model, 'retails_units')->widget(Select2::classname(), [
                                                            'data' => $numOfProp,
                                                            'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                            'pluginOptions' => [
                                                                'allowClear' => true
                                                            ],
                                                        ])->label('Total Number of Retail Unit <span class="text-danger">*</span>'); ?>
                                                    </div>
                                                <?php } ?>

                                            <?php } else { ?>
                                                <div class="col-sm-4 no_of_units_value">
                                                    <?php
                                                    echo $form->field($model, 'no_of_units_value')->widget(Select2::classname(), [
                                                        'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    ])->label('Number of Units');
                                                    ?>
                                                </div>

                                                <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull) && $quotation->scope_of_service != 9 && $quotation->scope_of_service != 10) {} else{ ?>
                                                    <div class="col-sm-4 no_of_res_units_value">
                                                        <?php
                                                        echo $form->field($model, 'no_of_res_units_value')->widget(Select2::classname(), [
                                                            'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                            'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                        ])->label('Total Number of Residential Unit <span class="text-danger">*</span>');
                                                        ?>
                                                    </div>
                                                <?php } ?>


                                                <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull) && $quotation->scope_of_service != 9 && $quotation->scope_of_service != 10) {} else{ ?>
                                                    <div class="col-sm-4 no_of_com_units_value">
                                                        <?php
                                                        echo $form->field($model, 'no_of_com_units_value')->widget(Select2::classname(), [
                                                            'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                            'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                        ])->label('Total Number of Commercial Unit <span class="text-danger">*</span>');
                                                        ?>
                                                    </div>
                                                <?php } ?>

                                                <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull) && $quotation->scope_of_service != 9 && $quotation->scope_of_service != 10) {} else{ ?>
                                                    <div class="col-sm-4 no_of_ret_units_value">
                                                        <?php
                                                        echo $form->field($model, 'no_of_ret_units_value')->widget(Select2::classname(), [
                                                            'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                            'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                        ])->label('Total Number of Retail Unit <span class="text-danger">*</span>');
                                                        ?>
                                                    </div>
                                                <?php } ?>

                                            <?php } ?>





                                            <?php if (in_array($quotation->scope_of_service, $bcsScopesArrayFull) && $quotation->scope_of_service != 9 && $quotation->scope_of_service != 10) {} else{ ?>
                                                <div class="col-sm-4 types_of_unit">
                                                    <?= $form->field($model, 'no_of_unit_types')->widget(Select2::classname(), [
                                                        'data' => Yii::$app->appHelperFunctions->typesOfUnitsArr,
                                                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Types of Unit <span class="text-danger">*</span>'); ?>
                                                </div>
                                            <?php } ?>




                                        </div>
                                    </div>
                                </div>

                                <div class="card card-outline card-primary mb-3" id="property_facilities_details">
                                    <header class="card-header">
                                        <h2 class="card-title">Property Facilities Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">



                                            <div class="col-sm-4 meeting_rooms">
                                                <?= $form->field($model, 'meeting_rooms')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Party Halls/Meeting Rooms <span class="text-danger">*</span>'); ?>
                                            </div>


                                            <div class="col-sm-4 health_club_spa">
                                                <?= $form->field($model, 'no_of_health_club_spa')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Health Clubs/Spa <span class="text-danger">*</span>'); ?>
                                            </div>



                                            <div class="col-sm-4 bbq_area">
                                                <?= $form->field($model, 'no_of_bbq_area')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of BBQ Areas <span class="text-danger">*</span>'); ?>
                                            </div>



                                            <div class="col-sm-4 children_play_area">
                                                <?= $form->field($model, 'no_of_play_area')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Children Play Areas <span class="text-danger">*</span>'); ?>
                                            </div>



                                            <div class="col-sm-4 number_gyms">
                                                <?= $form->field($model, 'no_of_gyms')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Gyms <span class="text-danger">*</span>'); ?>
                                            </div>


                                            <div class="col-sm-4 number_schools">
                                                <?= $form->field($model, 'no_of_schools')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Schools <span class="text-danger">*</span>'); ?>
                                            </div>


                                            <div class="col-sm-4 number_clinics">
                                                <?= $form->field($model, 'no_of_clinics')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Clinics <span class="text-danger">*</span>'); ?>
                                            </div>


                                            <div class="col-sm-4 sports_courts">
                                                <?= $form->field($model, 'no_of_sports_courts')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Sports Courts <span class="text-danger">*</span>'); ?>
                                            </div>


                                            <div class="col-sm-4 number_mosque">
                                                <?= $form->field($model, 'no_of_mosques')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Mosque <span class="text-danger">*</span>'); ?>
                                            </div>




                                        </div>
                                    </div>
                                </div>

                                <div class="card card-outline card-primary mb-3" id="other_icome_details">
                                    <header class="card-header">
                                        <h2 class="card-title">Other Facilities</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">


                                            <div class="col-sm-4 number_restaurants">
                                                <?= $form->field($model, 'restaurant')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Restaurants <span class="text-danger">*</span>'); ?>
                                            </div>



                                            <div class="col-sm-4 number_atms">
                                                <?= $form->field($model, 'atms')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of ATM Machines <span class="text-danger">*</span>'); ?>
                                            </div>



                                            <div class="col-sm-4 number_coffee_shops">
                                                <?= $form->field($model, 'no_of_coffee_shops')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Coffee Shops <span class="text-danger">*</span>'); ?>
                                            </div>



                                            <div class="col-sm-4 number_sign_boards">
                                                <?= $form->field($model, 'no_of_sign_boards')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Sign Boards <span class="text-danger">*</span>'); ?>
                                            </div>



                                            <div class="col-sm-4 number_night_clubs">
                                                <?= $form->field($model, 'night_clubs')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->CrmOptionZeroToTenNumber,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Night Clubs <span class="text-danger">*</span>'); ?>
                                            </div>




                                            <div class="col-sm-4 number_bars">
                                                <?= $form->field($model, 'bars')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Number of Bars <span class="text-danger">*</span>'); ?>
                                            </div>



                                            <?php /*

                                          <div class="col-sm-4 profit income profit-income" id="unit_rooms">
                                              <?= $form->field($model, 'number_of_rooms_building')->widget(Select2::classname(), [
                                                  'data' => array('0' => '0', '1' => '1-50', '2' => '51-100', '3' => '101-200', '4' => '201-350', '5' => '351-750', '6' => 'Above 750'),
                                                  'options' => ['placeholder' => 'Select', 'class' => 'no_of_units form-control'],
                                                  'pluginOptions' => [
                                                      'allowClear' => true
                                                  ],
                                              ]); ?>
                                          </div>


                                      
                                  
                                          <div class="col-sm-4 profit income profit-income">
                                              <?= $form->field($model, 'spa')->widget(Select2::classname(), [
                                                  'data' => array('0' => 'No', '1' => 'Yes'),
                                                  'options' => ['placeholder' => 'Select', 'class' => 'no_of_units form-control'],
                                                  'pluginOptions' => [
                                                      'allowClear' => true
                                                  ],
                                              ])->label('Spa'); ?>
                                          </div>

                                          <div class="col-sm-4 profit income profit-income">
                                              <?= $form->field($model, 'health_club')->widget(Select2::classname(), [
                                                  'data' => array('0' => 'No', '1' => 'Yes'),
                                                  'options' => ['class' => 'no_of_units form-control'],
                                                  'pluginOptions' => [
                                                      'allowClear' => true
                                                  ],
                                              ])->label('Health Clubs'); ?>
                                          </div>

                                          <div class="col-sm-4 profit income profit-income">
                                              <?= $form->field($model, 'beach_access')->widget(Select2::classname(), [
                                                  'data' => array('0' => 'No', '1' => 'Yes'),
                                                  'options' => ['class' => 'no_of_units form-control'],
                                                  'pluginOptions' => [
                                                      'allowClear' => true
                                                  ],
                                              ])->label('Beach Access'); ?>
                                          </div>

                                          <div class="col-sm-4 profit income profit-income">
                                              <?= $form->field($model, 'parking_sale')->widget(Select2::classname(), [
                                                  'data' => array('0' => '0', '1' => 'Less than 10', '2' => '11-50', '3' => 'Above 50'),
                                                  'options' => ['class' => 'no_of_units form-control'],
                                                  'pluginOptions' => [
                                                      'allowClear' => true
                                                  ],
                                              ]); ?>
                                          </div>
                                  */?>



                                        </div>
                                    </div>
                                </div>


                                <?php if ($model->valuation_approach == 3) { ?>
                                    <div class="card card-outline card-primary mb-3" id="other_details">
                                        <header class="card-header">
                                            <h2 class="card-title"></h2>
                                        </header>
                                        <div class="card-body">
                                            <div class="row">

                                                <div class="col-sm-4 profit only-profit">
                                                    <?= $form->field($model, 'ballrooms')->widget(Select2::classname(), [
                                                        'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3'),
                                                        'options' => ['class' => 'no_of_units form-control'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); ?>
                                                </div>



                                                <div class="col-sm-4 profit only-profit">
                                                    <?php
                                                    echo $form->field($model, 'last_3_years_finance')->widget(\kartik\select2\Select2::classname(), [
                                                        'data' => array('0' => 'No', '1' => 'Yes'),
                                                    ])->label('Last 3 years Finance Provide');
                                                    ?>
                                                </div>

                                                <div class="col-sm-4 profit only-profit">
                                                    <?php
                                                    echo $form->field($model, 'projections_10_years')->widget(\kartik\select2\Select2::classname(), [
                                                        'data' => array('0' => 'No', '1' => 'Yes'),
                                                    ])->label('10 years projections to be provide');
                                                    ?>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>




                                <?php
                                if (Yii::$app->menuHelperFunction->checkActionAllowed('Verified')) {
                                    echo StatusVerified::widget(['model' => $model, 'form' => $form]);
                                }
                                ?>
                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>


                            <?php ActiveForm::end(); ?>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>