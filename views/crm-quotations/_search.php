<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CrmQuotationsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="crm-quotations-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'reference_number') ?>

    <?= $form->field($model, 'client_name') ?>

    <?= $form->field($model, 'client_type') ?>

    <?= $form->field($model, 'client_customer_name') ?>

    <?php // echo $form->field($model, 'other_intended_users') ?>

    <?php // echo $form->field($model, 'scope_of_service') ?>

    <?php // echo $form->field($model, 'inquiry_date') ?>

    <?php // echo $form->field($model, 'expiry_date') ?>

    <?php // echo $form->field($model, 'purpose_of_valuation') ?>

    <?php // echo $form->field($model, 'advance_payment_terms') ?>

    <?php // echo $form->field($model, 'no_of_properties') ?>

    <?php // echo $form->field($model, 'related_to_buyer') ?>

    <?php // echo $form->field($model, 'related_to_owner') ?>

    <?php // echo $form->field($model, 'related_to_client') ?>

    <?php // echo $form->field($model, 'related_to_property') ?>

    <?php // echo $form->field($model, 'related_to_buyer_reason') ?>

    <?php // echo $form->field($model, 'related_to_owner_reason') ?>

    <?php // echo $form->field($model, 'related_to_client_reason') ?>

    <?php // echo $form->field($model, 'related_to_property_reason') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'phone_number') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'recommended_fee') ?>

    <?php // echo $form->field($model, 'relative_discount') ?>

    <?php // echo $form->field($model, 'final_fee_approved') ?>

    <?php // echo $form->field($model, 'turn_around_time') ?>

    <?php // echo $form->field($model, 'valuer_name') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'quotation_status') ?>

    <?php // echo $form->field($model, 'payment_slip') ?>

    <?php // echo $form->field($model, 'toe_document') ?>

    <?php // echo $form->field($model, 'quotation_recommended_fee') ?>

    <?php // echo $form->field($model, 'quotation_turn_around_time') ?>

    <?php // echo $form->field($model, 'toe_final_fee') ?>

    <?php // echo $form->field($model, 'toe_final_turned_around_time') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'assumptions') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
