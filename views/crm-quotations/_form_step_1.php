<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ValuationFormAsset;
use app\assets\QuotationBuildingAsset;

ValuationFormAsset::register($this);
QuotationBuildingAsset::register($this);
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */

$paymentTypes = yii::$app->crmQuotationHelperFunctions->paymentTerms;
$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date, #listingstransactions-inquiry_received_day").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

$("#quotation-irtime").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "HH:mm"
});


$("body").on("click", ".sav-btn", function (e) {
swal({
title: "'.Yii::t('app','Valuation status will be saved and Email will be sent to Clients ?').'",
html: "'.Yii::t('app','').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({

  url: "'.Url::to(['valuation/receive-crm-email','id'=>$model->id,'step'=>0]).'",
  dataType: "html",
  type: "POST",
  });
    }
});
});
');
?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }

    .fade {
        opacity: 1;
    }

    .nav-tabs.flex-column .nav-link.active {
        background-color: #007bff;
        color: white;
        font-weight: bold;
    }
</style>

<section class="valuation-form card card-outline card-primary">

    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle ?></h2>
    </header>
    <div class="card-body">


        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-edit"></i>
                <b>General Details</b>
            </h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <?php $instruction_date = ($model->instruction_date <> null)? $model->instruction_date: date('Y-m-d') ?>
                    <?= $form->field($model, 'instruction_date', ['template' => '
                    {label}
                    <div class="input-group date" style="display: flex" id="listingstransactions-instruction_date" data-target-input="nearest">
                    {input}
                    <div class="input-group-append" data-target="#listingstransactions-instruction_date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    </div>
                    {hint}{error}
                    '])->textInput(['maxlength' => true,'value'=>$instruction_date])->label('Client Inquiry Date') ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'inquiry_received_time', ['template' => '
                    {label}
                    <div class="input-group date" style="display: flex" id="quotation-irtime" data-target-input="nearest">
                    {input}
                    <div class="input-group-append" data-target="#quotation-irtime" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    </div>
                    {hint}{error}
                    '])->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'client_name')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\Company::find()
                                ->where(['status' => 1])
                                ->andWhere(['allow_for_valuation'=>1])
                                // ->andWhere([
                                //     'or',
                                //     ['data_type' => 0],
                                //     ['data_type' => null],
                                // ])
                                ->orderBy(['title' => SORT_ASC,])
                                ->all(), 'id', 'title'),
                        'options' => ['placeholder' => 'Select a Client ...', 'class'=>'client'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'client_reference')->textInput(['maxlength' => true, 'class'=>'form-control client_reference']) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'client_customer_name')->textInput(['maxlength' => true, 'class'=>'form-control client_customer_name'])->label('Instructing Party Name') ?>
                </div>




                <div class="col-sm-4">
                    <?php $target_date = ($model->target_date <> null)? $model->target_date:  date('Y-m-d', strtotime("+2 days")); ?>
                    <?= $form->field($model, 'target_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-target_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-target_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true,'value'=> $target_date])->label('Client Target Date') ?>
                </div>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'email_subject')->textInput()->label('Client Email Subject');
                    ?>

                </div>


            </div>
        </div>



    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
