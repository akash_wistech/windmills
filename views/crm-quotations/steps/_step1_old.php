<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use kartik\depdrop\DepDrop;
use app\assets\ValuationFormAsset;
ValuationFormAsset::register($this);
ListingsFormAsset::register($this);
use  app\components\widgets\StatusVerified;
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enter Property Details');
$cardTitle = Yii::t('app', 'Property Details:  {nameAttribute}', [
    'nameAttribute' => $quotation->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['crm-quotations/step_1?id=' . $quotation->id.'&']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

        if($("#crmreceivedproperties-property_id").val()==1 || $("#crmreceivedproperties-property_id").val()==2 || $("#crmreceivedproperties-property_id").val()==6){
            $(".num-unit").hide();
        }else{
            $(".num-unit").show();
        }
  $("#crmreceivedproperties-property_id").on(\'change\',function(){
 
        if($(this).val()==1 || $(this).val()==2 || $(this).val()==6){
            $(".num-unit").hide();
        }else{
            $(".num-unit").show();
        }

    });
    
    
 

    

');
?>
<!--<script>
    $("#crmreceivedproperties-property_id").on('change',function(){
        if($(this).val()==1){
            $("#crmreceivedproperties-no_of_units").hide();
        }else{
            $("#crmreceivedproperties-no_of_units").show();
        }

    });
</script>-->
<?php
$numOfProp = [];
if ($model->property_id==3 || $model->property_id==7 || $model->property_id==22 || $model->property_id==24) {
    $numOfProp = yii::$app->quotationHelperFunctions->numberofUnitsInBuildingArr;
}else{
    $numOfProp =  yii::$app->quotationHelperFunctions->numberofUnitsInLandArr;
}
$numOfProp = yii::$app->quotationHelperFunctions->numberofUnitsInBuildingArr;
$companyAddLink='';
if(Yii::$app->menuHelperFunction->checkActionAllowed('create','client')){
    $companyAddLink =' <a href="javascript:;" class="btn btn-xs btn-success load-modal" data-url="'.Url::to(['buildings/create-crm?id=865&property_index=0']).'" data-heading="'.Yii::t('app','New Project').'">';
    $companyAddLink.='  <i class="fa fa-plus"></i>';
    $companyAddLink.='</a>';
}
?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $quotation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 1,'quotation'=>$quotation,'property_index'=> $property_index]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body repeat-section">


                                <div class="row">
                                   <!-- <div class="col-sm-4">
                                        <?/*= $form->field($model, 'client_name_passport')->textInput(['maxlength' => true])->label('Customer Name') */?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php /*$no_of_owners = ($model->no_of_owners <> null)? $model->no_of_owners: 1 */?>
                                        <?/*= $form->field($model, 'no_of_owners')->textInput(['maxlength' => true,'value'=> $no_of_owners]);*/?>
                                    </div>-->


                                <section class="card card-outline card-info">
                                    <header class="card-header">
                                        <h2 class="card-title"><?= Yii::t('app', 'Property Information') ?></h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'complexity')->widget(Select2::classname(), [
                                                    'data' => yii::$app->quotationHelperFunctions->getComplexity(),
                                                    'options' => ['class'=>'complexity'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>



                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'building_info')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                                                        'title' => SORT_ASC,
                                                    ])->all(), 'id', 'title'),
                                                    'options' => ['placeholder' => 'Select a Building ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Building / Project'.$companyAddLink);
                                                ?>

                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy([
                                                        'title' => SORT_ASC,
                                                    ])->all(), 'id', 'title'),
                                                    'options' => ['placeholder' => 'Select a Property Type ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Property Type');
                                                ?>

                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                                ?>

                                            </div>

                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                                ?>

                                            </div>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'sub_community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'building_number')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'plot_number')->textInput(['maxlength' => true]) ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <!--<div class="col-sm-4">
                        <?php
                                            /*                        echo $form->field($model, 'payment_plan')->widget(Select2::classname(), [
                                                                        'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                                        'pluginOptions' => [
                                                                            'allowClear' => true
                                                                        ],
                                                                    ]);
                                                                    */?>
                    </div>-->
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true]) ?>
                                            </div>
                                           <!-- <div class="col-sm-4">
                                                <?/*= $form->field($model, 'floor_number')->textInput(['maxlength' => true]) */?>
                                            </div>-->

                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'floor_number')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->crmFloorNumber,
                                                ]);
                                                ?>

                                            </div>

                                            <!--  <div class="col-sm-4">
                        <?php
                                            /*                        echo $form->field($model, 'instruction_person')->widget(Select2::classname(), [
                                                                        'data' => Yii::$app->appHelperFunctions->staffMemberListArr,
                                                                        'options' => ['placeholder' => 'Select a Person ...'],
                                                                        'pluginOptions' => [
                                                                            'allowClear' => true
                                                                        ],
                                                                    ]);
                                                                    */?>

                    </div>-->
                                            <?php
                                            $no_land = array(1,17,28,37);

                                            if (isset($model->property_id) && in_array($model->property_id, $no_land))
                                            {}
                                            else
                                            {?>
                                                <div class="col-sm-4" id="land_size">
                                                    <?= $form->field($model, 'land_size')->textInput(['maxlength' => true]) ?>
                                                </div>

                                            <?php }?>



                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true])->label('Built Up Area (SQ.FT)') ?>
                                            </div>

                                         <!--   <div class="col-sm-4">
                                                <?php
/*                                                echo $form->field($model, 'valuation_scope')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->valuationScopeArr,
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                                */?>

                                            </div>-->

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'valuation_approach')->dropDownList(Yii::$app->appHelperFunctions->valuationApproachListArr)?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'repeat_valuation')->widget(Select2::classname(), [
                                                    'data' => yii::$app->quotationHelperFunctions->getNewRepeatValuation(),
                                                    'options' => ['class'=>'repeat_valuation'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>
                                            <!--<div class="col-sm-4">
                                                <?/*= $form->field($model, 'type_of_valuation')->widget(Select2::classname(), [
                                                    'data' => yii::$app->quotationHelperFunctions->getTypeofvaluation(),
                                                    'options' => ['class'=>'type_of_valuation'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); */?>
                                            </div>-->


                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'number_of_comparables')->widget(Select2::classname(), [
                                                    'data' => yii::$app->quotationHelperFunctions->getNumberofComparables(),
                                                    'options' => ['class'=>'number_of_comparables'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>




                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'no_of_units')->widget(Select2::classname(), [
                                                    'data' => $numOfProp,
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>
                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'no_of_unit_types')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->typesOfUnitsArr,
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>

                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'number_of_rooms_building')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => '1-50', '2' => '51-100', '3' => '101-200', '4' => '201-350', '5' => '351-750','6'=> 'Above 750'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>

                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'restaurant')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>

                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'ballrooms')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>
                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'atms')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>

                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'retails_units')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => 'Less than 3', '2' => '4-10', '3' => 'Above 10'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>

                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'night_clubs')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>
                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'bars')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>

                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'health_club')->widget(Select2::classname(), [
                                                    'data' => array('0' => 'No', '1' => 'Yes'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>
                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'meeting_rooms')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>
                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'spa')->widget(Select2::classname(), [
                                                    'data' => array('0' => 'No', '1' => 'Yes'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>
                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'beach_access')->widget(Select2::classname(), [
                                                    'data' => array('0' => 'No', '1' => 'Yes'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Beach Access'); ?>
                                            </div>

                                           

                                            <div class="col-sm-4 num-unit">
                                                <?= $form->field($model, 'parking_sale')->widget(Select2::classname(), [
                                                    'data' => array('0' => '0', '1' => 'Less than 10', '2' => '11-50', '3' => 'Above 50'),
                                                    'options' => ['class'=>'no_of_units form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>


                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'upgrades')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->upgrades,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4" >
                                                <?php
                                                echo $form->field($model, 'last_3_years_finance')->widget(\kartik\select2\Select2::classname(), [
                                                    'data' => array('0' => 'No', '1' => 'Yes'),
                                                ])->label('Last 3 years Finance Provide');
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'projections_10_years')->widget(\kartik\select2\Select2::classname(), [
                                                    'data' => array('0' => 'No', '1' => 'Yes'),
                                                ])->label('10 years projections to be provide');
                                                ?>
                                            </div>




                                        </div>
                                    </div>

                                    <?php
                                    if(Yii::$app->menuHelperFunction->checkActionAllowed('Verified')){
                                        echo StatusVerified::widget([ 'model' => $model, 'form' => $form ]);
                                    }
                                    ?>
                                </section>



                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>



