<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

$this->registerJs('


$(".imgInps").change(function(){
    readURL(this);
});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if(input.value.includes(".pdf")== true){
                $("#blah").attr("src","'.Yii::$app->params['uploadPdfIcon'].'");
              }
              else {
                  $("#blah").attr("src", e.target.result);
               }
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(".imgInps").change(function(){
    readURL(this);
});


$("body").on("click", ".remove_file", function () {
_this=$(this);
swal({
title: "'.Yii::t('app','Confirmation').'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({
  url: "'.Url::to(['valuation/signaturedelete','id'=>$model->id]).'",
  dataType: "html",
  type: "POST",
  success: function(html) {
  $("#blah").attr("src", "'.Yii::$app->params['dummy_image_address'].'");
  },
  error: function(xhr,ajaxoptions,thownError){

    alert(xhr.responseText);
  }
  });
    }
});
});


');

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Signed TOE');
$cardTitle = Yii::t('app', 'Signed TOE:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_24/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->id ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 10,'quotation'=>$quotation]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">


                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                            <header class="card-header">
                                <h2 class="card-title">Add Signed TOE</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">







                                    <div class="col-sm-6" style="padding-top: 20px;">
                                        <p style="font-weight: bold; font-size:15px">Upload Only Pdf</p>
                                        <?= $form->field($model, 'toe_image', ['labelOptions'=>['style'=>'padding:9px; color:#E1F5FE; background-color:#0277BD;border-radius:3px;']])->fileInput(['class'=>'imgInps d-none'])->label('Upload TOE') ?>


                                        <?php

                                        $imgname = Yii::$app->params['dummy_image_address'];
                                        if ($model->toe_image != null) {
                                            $imgname = Yii::$app->get('s3bucket')->getUrl('toe/images/'.$model->toe_image);
                                            $attachment_link= $model->toe_image;

                                                $attachment_src = Yii::$app->params['uploadDocsIcon'];

                                            $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/',$attachment);
                                            if($model->id > 3040) {
                                                $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl('toe/images/'.$model->toe_image, '+10 minutes');
                                            }else {

                                                    $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl('toe/images/'.$model->toe_image, '+10 minutes');

                                            }


                                          //  $imgname = Yii::$app->get('s3bucket')->getUrl('toe/images/'.$model->toe_image);
                                            $link=$attachment_link;
                                        }
                                        ?>
                                        <div style='width:350px; margin-bottom:30px; position:relative;'>

                                            <?php

                                            if (strpos($imgname,'.pdf') == true) {
                                                $imgname=Yii::$app->params['uploadPdfIcon'];
                                            }

                                            ?>
                                            <img id="blah" src="<?= $imgname ?>" alt="No Image is selected." width="350px"/>


                                            <?php
                                            if ($model->toe_image != null) {
                                                ?>
                                                <a style="color:#ffffff; font-weight: bold; padding-left:7px; padding-right :7px; font-size:22px; position:absolute;  bottom:10px; left:20px; background-color:#558B2F;";
                                                   href="<?= $link ?>" target="_blank"><i class="fas fa-eye"></i></a>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                                 <?php

                                 if($model->quotation_status > 3 && $model->quotation_status <= 6){


                                 if($model->toe_signed_and_received == 5){ ?>
                                     <?php if($model->payment_status == 1){ ?>
                                         <a href="<?= Url::toRoute(['valuation/invoice_toe_pdf_half_first_qt?qid='.$model->id]) ?>" target="_blank"
                                         <button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Tax
                                             Invoice</button>
                                         </a>
                                         <?php }else{ ?>
                                <a href="<?= Url::toRoute(['valuation/invoice_toe_qt?qid='.$model->id]) ?>" target="_blank"
                                    <button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Tax
                                        Invoice</button>
                                </a>
                                         <?php } ?>
                                <?php }
                                 } ?>


                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /.card -->
</div>


<script type="text/javascript">




</script>
