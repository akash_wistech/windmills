<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Inspect Property');
$cardTitle = Yii::t('app', 'Inspect Property:  {nameAttribute}', [
    'nameAttribute' => $model->quotation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_5/' . $quotation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');


$i=1;
?>
<script>

</script>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->quotation_id ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 5,'quotation'=>$quotation,'property_index'=> $property_index]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">
<div class="row">
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'makani_number')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>
                                </div>
    <div class="col-sm-4">
        <?php
        echo $form->field($model, 'utilities_connected')->widget(Select2::classname(), [
            'data' => array('Yes' => 'Yes', 'No' => 'No'),
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>
</div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_placement')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertyPlacementListArr,
                                        'options' => ['placeholder' => 'Select a Placement ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>

                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'acquisition_method')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->acquisitionMethodsArr,
                                        'options' => ['placeholder' => 'Select a Placement ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_visibility')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertyVisibilityListArr,
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_exposure')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertyExposureListArr,
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>

                                </div>
                                </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_condition')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertyConditionListArr,
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_defect')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertyDefectsArr,
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'development_type')->widget(Select2::classname(), [
                                        'data' => array('Standard' => 'Standard', 'Non-Standard' => 'Non-Standard'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                </div>
                                    <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'finished_status')->widget(Select2::classname(), [
                                        'data' => array('Shell & Core' => 'Shell & Core', 'Fitted' => 'Fitted'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'developer_id')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(\app\models\Developers::find()->orderBy([
                                            'title' => SORT_ASC,
                                        ])->all(), 'id', 'title'),
                                        'options' => ['placeholder' => 'Select a Developer ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>

                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'estimated_age')->textInput(['maxlength' => true]) ?>
                                </div>
                                    </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'estimated_remaining_life')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'balcony_size')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'service_area_size')->textInput(['maxlength' => true]) ?>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'net_built_up_area')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'number_of_basement')->textInput(['maxlength' => true]) ?>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'number_of_levels')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->listingLevelsListArr,
                                        'options' => ['placeholder' => 'Select a Level ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>

                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'pool')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes', 'No' => 'No'),

                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                    <div class="col-sm-4">
                                        <?php
                                      /*  echo "<pre>";
                                        print_r($model->other_facilities);
                                        die;
                                        echo $model->other_facilities;
                                        die;
                                        if($model->other_facilities <> null) {
                                            $model->other_facilities = explode(',', ($model->other_facilities <> null) ? $model->other_facilities : "");
                                        }*/
                                        $model->other_facilities = explode(',', ($model->other_facilities <> null) ? $model->other_facilities : "");
                                        echo $form->field($model, 'other_facilities')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(\app\models\OtherFacilities::find()->orderBy([
                                                'title' => SORT_ASC,
                                            ])->all(), 'id', 'title'),
                                            'options' => ['placeholder' => 'Select a Facilities ...'],
                                            'pluginOptions' => [
                                                'placeholder' => 'Select a Facilities',
                                                'multiple' => true,
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                               <!-- <div class="col-sm-4">
                                    <?php
/*                                    echo $form->field($model, 'gym')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    */?>
                                </div>-->
                                </div>
                                <div class="row">
                               <!-- <div class="col-sm-4">
                                    <?php
/*                                    echo $form->field($model, 'play_area')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    */?>
                                </div>-->




                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'occupancy_status')->widget(Select2::classname(), [
                                            'data' => array('Owner Occupied' => 'Owner Occupied', 'Tenanted' => 'Tenanted', 'Vacant' => 'Vacant'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'full_building_floors')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'parking_floors')->textInput(['maxlength' => true])->label("Parking Space") ?>
                                </div>
                                </div>
                                <div class="row">

                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'landscaping')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes', 'No' => 'No', 'Semi-Landscape' => 'Semi-Landscape'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'fridge')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'listing_property_type')->widget(Select2::classname(), [
                                            // 'data' => Yii::$app->appHelperFunctions->listingsPropertyTypeListArr,
                                            'data' => ArrayHelper::map(\app\models\ListingSubTypes::find()->orderBy([
                                                'title' => SORT_ASC,
                                            ])->all(), 'title', 'title'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>
                                    <!--<div class="col-sm-4">
                                        <?/*= $form->field($model, 'listing_property_type')->textInput(['maxlength' => true]) */?>
                                    </div>-->
                                </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'oven')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'cooker')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">





                                    <?php

                                    $model->ac_type = explode(',', ($model->ac_type <> null) ? $model->ac_type : "");
                                    echo $form->field($model, 'ac_type')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->acTypesArr,
                                        'options' => ['placeholder' => 'Select a AC Type ...'],
                                        'pluginOptions' => [
                                            'placeholder' => 'Select a AC Type',
                                            'multiple' => true,
                                            'allowClear' => true
                                        ],
                                    ]);



                                 /*   echo $form->field($model, 'ac_type')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->acTypesArr,
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);*/
                                    ?>

                                </div>
                                </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'washing_machine')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'furnished')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unfurnished','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'white_goods')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'mode_of_transport')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->modesOfTransportArr,
                                            'options' => ['placeholder' => 'Select a Transport Mode'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'start_kilometres')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'end_kilometres')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'plot_area_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArr,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Source of Plot Area');
                                        ?>

                                    </div>

                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'plot_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArr,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Plot Source');
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'parking_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArr,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Parking Source');
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'age_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArr,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Age Source');
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'bua_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArr,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Source of BUA');
                                        ?>

                                    </div>

                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'measurement')->textInput(['maxlength' => true])->label("Measurement") ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'extension')->textInput(['maxlength' => true])->label("extension") ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'extension_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArr,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label("Extension Permission Document");
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'completion_status')->textInput(['maxlength' => true])?>
                                    </div>
                                </div>

                                <section class="card card-outline card-info">
                                    <header class="card-header">
                                        <h2 class="card-title"><?= Yii::t('app', 'Location Attributes') ?></h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <?php
                                                echo $form->field($model, 'location_highway_drive')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                echo $form->field($model, 'location_school_drive')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                echo $form->field($model, 'location_mall_drive')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                echo $form->field($model, 'location_sea_drive')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                echo $form->field($model, 'location_park_drive')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                                ]);
                                                ?>
                                            </div>


                                        </div>

                                    </div>

                                </section>

                                <section class="card card-outline card-info">
                                    <header class="card-header">
                                        <h2 class="card-title"><?= Yii::t('app', 'View Attributes') ?></h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_community')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewCommunityAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_pool')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_burj')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                ]);
                                                ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_sea')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_marina')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_mountains')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_lake')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_golf_course')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_park')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_special')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                        </div>

                                    </div>

                                </section>


                                <div class="clearfix"></div>
                                <!-- Configration Information-->
                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Configration Information</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_bedrooms')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_bathrooms')->textInput(['maxlength' => true]) ?>
                                            </div>


                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_kitchen')->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_living_area')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_dining_area')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_maid_rooms')->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_laundry_area')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_store')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_service_block')->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_garage')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_balcony')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_family_room')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_powder_room')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_study_room')->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>

                                    </div>
                                </section>
                        </section>


                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>

                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>





<script type="text/javascript">
    var row = <?= $row ?>;
    function addAttachment() {

        html = '<tr id="image-row' + row + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="InspectProperty[customAttachments][' + row + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="number" class="form-control"  name="InspectProperty[customAttachments][' + row + '][quantity]" value="" placeholder="Quantity" required />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment tbody').append(html);

        row++;
    }

    function deleteRow(rowId, docID, type) {



        var url = '<?= \yii\helpers\Url::to('/valuation/remove-attachment'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if(response.status == 'exist'){
                            swal("Warning!", response.message, "warning");
                        }else{
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
</script>