<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use kartik\depdrop\DepDrop;
use app\assets\ValuationFormAsset;
ValuationFormAsset::register($this);


ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Receive Quotation');
$cardTitle = Yii::t('app', 'Receive Quotation:  {nameAttribute}', [
    'nameAttribute' => $model->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_0/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');


$discount = 0;
$discount_quotations = 0;

$total_discount=0;
$total_discount_amount=0;
$discount_no_of_properties  = 0;
$discount_first_time=0;
$general_discount=0;
$VAT = 0;

if ($model->relative_discount_toe!=null) {
    $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total,$model->relative_discount_toe);
    $total_discount= $total_discount + $model->relative_discount_toe;
    $total_discount_amount = $total_discount_amount + $discount;
}
if ($model->relative_discount!=null && $model->id > 987) {
    $discount_quotations =  yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total,$model->relative_discount);
}

$netValuationFee = $toe_fee_total-$discount;
if ($model->first_time_discount!=null && $model->id > 987) {
    $first_time_discount =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($toe_fee_total,$model->first_time_discount);
    $netValuationFee =$netValuationFee-$first_time_discount;
    $total_discount= $total_discount + $model->first_time_discount;
    $total_discount_amount = $total_discount_amount + $first_time_discount;
}

if ($model->no_of_property_discount!=null && $model->id > 987) {
    $discount_no_of_properties =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($toe_fee_total,$model->no_of_property_discount);
    $netValuationFee =$netValuationFee-$discount_no_of_properties;
    $total_discount= $total_discount + $model->no_of_property_discount;
    $total_discount_amount = $total_discount_amount + $discount_no_of_properties;
}

if ($model->general_discount!=null && $model->id > 987) {
    $general_discount =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($toe_fee_total,$model->general_discount);
    $netValuationFee =$netValuationFee-$general_discount;
    $total_discount= $total_discount + $model->general_discount;
    $total_discount_amount = $total_discount_amount + $general_discount;
}





if($model->client->vat == 1){
    $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
}





$finalFeePayable = $netValuationFee+$VAT;


?>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 9,'quotation'=>$quotation]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                        aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">


                                <div class="card-header">
                                    <h3 class="card-title">
                                        <i class="fas fa-edit"></i>
                                        <b>TOE Details</b>
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="control-label"
                                                    for="crmquotations-reference_number">Relative Discount
                                                    Quotation(%)</label>
                                                <input type="text" class="form-control"
                                                    value="<?= $model->relative_discount;  ?>" readonly="">
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="control-label"
                                                    for="crmquotations-reference_number">Relative Discount Quotation
                                                    (Amount) </label>
                                                <input type="text" class="form-control"
                                                    value="<?= $discount_quotations; ?>" readonly="">
                                            </div>
                                        </div>






                                        <div class="col-sm-12">
                                            <?= $form->field($model, 'relative_discount_toe')->widget(Select2::classname(), [
                                                'data' => yii::$app->quotationHelperFunctions->relativediscount,
                                                'options' => ['placeholder' => 'Select a Relative Discount ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                ])->label('Relative Discount TOE'); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Building / Project</th>
                                                    <th>Recommended Fee</th>
                                                    <th>Quotation TAT</th>
                                                    <th>Quotation Fee</th>
                                                    <th>TOE TAT</th>
                                                    <th>TOE Fee (Final)</th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if($receivedProperties <> null && !empty($receivedProperties)) {
                                                    foreach($receivedProperties as $key => $property){
                                                        ?>
                                                <tr>
                                                    <td> <?= $property->building->title; ?></td>
                                                    <td> <input type="number"
                                                            name="CrmQuotations[calculations][<?= $property->id ?>][recommended_fee]"
                                                            value="<?= $property->recommended_fee ?>" placeholder=""
                                                            class="form-control" readonly=""></td>

                                                    <td><input type="number"
                                                            name="CrmQuotations[calculations][<?= $property->id ?>][tat]"
                                                            value="<?= $property->tat ?>" placeholder=""
                                                            class="form-control" <?= $quotation_readonly ?>></td>

                                                    <td> <input type="number"
                                                            name="CrmQuotations[calculations][<?= $property->id ?>][quotation_fee]"
                                                            value="<?= $property->quotation_fee ?>" placeholder=""
                                                            class="form-control" <?= $quotation_readonly ?>></td>
                                                    <td> <input type="number"
                                                            name="CrmQuotations[calculations][<?= $property->id ?>][toe_tat]"
                                                            value="<?= $property->toe_tat ?>" placeholder=""
                                                            class="form-control" <?= $toe_readonly ?>></td>
                                                    <td> <input type="number"
                                                            name="CrmQuotations[calculations][<?= $property->id ?>][toe_fee]"
                                                            value="<?= $property->toe_fee ?>" placeholder=""
                                                            class="form-control" <?= $toe_readonly ?>></td>


                                                </tr>
                                                <?php
                                                    }
                                                }
                                                ?>
                                                <tr>
                                                    <td><b>Total</b></td>
                                                    <td><?= $recommended_fee ?></td>
                                                    <td><?= $quotation_tat_total ?></td>
                                                    <td><?= $quotation_fee_total ?></td>
                                                    <td><?= $toe_tat_total ?></td>
                                                    <td><?= $toe_fee_total ?></td>

                                                </tr>

                                                <tr>
                                                    <td colspan="4"></td>

                                                </tr>

                                                <?php if ($discount>0) { ?>
                                                <tr>
                                                    <td><b>Relationship Discount</b></td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <?php
                                                    
                                                if ($model->relative_discount =='base-fee') {?>
                                                    <td class="bill-data">Base Fee Discount</td>
                                                    <td class="bill-data" style="border-right: 1px solid black;">
                                                        <?= number_format($discount,2) ?></td>
                                                    <?php
                                                        } else{?>

                                                    <td class="bill-data"><?= $model->relative_discount_toe ?>% Discount
                                                    </td>
                                                    <td class="bill-data" style="border-right: 1px solid black;">
                                                        <?= number_format($discount,2) ?></td>
                                                    <?php
                                                        }
                                                        ?>
                                                   
                                                </tr>
                                                <?php  } ?>

                                                <?php if ($discount_no_of_properties>0) { ?>
                                                <tr>
                                                    <td><b>Multiple Properties Discount</b></td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td class="bill-data"><?= $model->no_of_property_discount ?>%
                                                        Discount</td>
                                                    <td class="bill-data" style="border-right: 1px solid black;">
                                                        <?= number_format($discount_no_of_properties,2) ?></td>
                                                </tr>
                                                <?php  } ?>

                                                <?php if ($general_discount>0) { ?>
                                                <tr>
                                                    <td><b>General Discount</b></td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td class="bill-data"><?= $model->general_discount ?>% Discount</td>
                                                    <td class="bill-data" style="border-right: 1px solid black;">
                                                        <?= number_format($general_discount,2) ?></td>
                                                </tr>
                                                <?php  } ?>

                                                <?php if ($first_time_discount>0) { ?>
                                                <tr>
                                                    <td><b>First Time Discount</b></td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td class="bill-data"><?= $model->first_time_discount ?>% Discount</td>
                                                    <td class="bill-data" style="border-right: 1px solid black;">
                                                        <?= number_format($first_time_discount,2) ?></td>
                                                </tr>
                                                <?php  } ?>

                                                <tr>
                                                    <td><b>Total Discount</b></td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td class="bill-data"><?= $total_discount ?>%
                                                        Discount</td>
                                                    <td class="bill-data" style="border-right: 1px solid black;">
                                                        <?= number_format($total_discount_amount,2) ?></td>
                                                </tr>

                                                <tr>
                                                    <td><b>Net Valuation Fee</b></td>
                                                    <td colspan="4"></td>
                                                    <td><?= number_format($netValuationFee,2) ?></td>

                                                </tr>
                                                <tr>
                                                    <td><b>Vat</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><?= ($VAT<>null AND $VAT>0) ? '5%' : 'No VAT' ?></td>
                                                    <td><?= ($VAT<>null AND $VAT>0) ? number_format($VAT,2) : 0 ?></td>


                                                </tr>
                                                <tr>
                                                    <td><b>Final Fee Payable</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><?= number_format($finalFeePayable,2) ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <?php
                echo $form->field($model, 'status_approve_toe')->widget(Select2::classname(), [
                    'data' => array('Pending'=> 'Pending','Approve' => 'Approve', 'Reject' => 'Reject'),
                    'options' => ['placeholder' => 'Select Status ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    ])->label('Status Approve TOE');
                    ?>
                                </div>

                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mr-2']) ?>

                                <a href="javascript:;" data-id="<?= $model->id ?>" class=" check-property-verify"
                                    data-title="Sending TOE"
                                    data-url="<?= Url::toRoute(['crm-quotations/send-toe?id='.$model->id]) ?>">
                                    <button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Send
                                        TOE</button>
                                </a>
                                <a href="javascript:;" data-id="<?= $model->id ?>" class=" check-property-verify"
                                    data-title="Downloading TOE"
                                    data-url="<?= Url::toRoute(['crm-quotations/toe?id='.$model->id]) ?>">
                                    <button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Download
                                        TOE</button>
                                </a>

                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>