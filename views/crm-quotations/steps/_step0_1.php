<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use kartik\depdrop\DepDrop;
use app\assets\ValuationFormAsset;
ValuationFormAsset::register($this);


ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enter General Valuation Inquiry Details');
$cardTitle = Yii::t('app', 'Enter General Valuation:  {nameAttribute}', [
    'nameAttribute' => $model->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_0/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');


// dd("hey");


$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date, #listingstransactions-inquiry_received_day").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

$("#quotation-irtime").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "HH:mm"
});
');
$paymentTypes = yii::$app->crmQuotationHelperFunctions->paymentTerms;
?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
</style>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 15,'quotation'=>$quotation, 'showAlert' =>$showAlert]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">


                                <div class="card-header">
                                    <h3 class="card-title">
                                        <i class="fas fa-edit"></i>
                                        <b>General Details</b>
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <?= $form->field($model, 'reference_number')->textInput(['maxlength' => true, 'readonly' => true, 'value'=> $reference
                                            ])
                                            ?>
                                        </div>
                                        
                                        <div class="col-sm-4">
                                            <?php $instruction_date = ($model->instruction_date <> null)? $model->instruction_date: date('Y-m-d') ?>
                                            <?= $form->field($model, 'instruction_date', ['template' => '
                                            {label}
                                            <div class="input-group date" style="display: flex" id="listingstransactions-instruction_date" data-target-input="nearest">
                                            {input}
                                            <div class="input-group-append" data-target="#listingstransactions-instruction_date" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                            </div>
                                            {hint}{error}
                                            '])->textInput(['maxlength' => true,'value'=>$instruction_date])->label('Inquiry Received Date') ?>
                                        </div>
                                        <div class="col-4">
                                            <?= $form->field($model, 'inquiry_received_time', ['template' => '
                                            {label}
                                            <div class="input-group date" style="display: flex" id="quotation-irtime" data-target-input="nearest">
                                            {input}
                                            <div class="input-group-append" data-target="#quotation-irtime" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                            </div>
                                            {hint}{error}
                                            '])->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-4">
                                            <?= $form->field($model, 'client_name')->widget(Select2::classname(), [
                                                'data' =>ArrayHelper::map(\app\models\Company::find()
                                                    ->where(['status' => 1])
                                                    ->andWhere(['allow_for_valuation' => 1])
                                                    // ->andWhere([
                                                    //     'or',
                                                    //     ['data_type' => 0],
                                                    //     ['data_type' => null],
                                                    // ])
                                                    ->orderBy(['title' => SORT_ASC,])
                                                    ->all(), 'id', 'title'),
                                                'options' => ['placeholder' => 'Select a Client ...', 'class'=>'client'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>

                                        <div class="col-4">
                                            <?= $form->field($model, 'client_reference')->textInput(['maxlength' => true, 'class'=>'form-control client_reference']) ?>
                                        </div>
                                        <div class="col-4">
                                            <?= $form->field($model, 'client_customer_name')->textInput(['maxlength' => true, 'class'=>'form-control client_customer_name'])->label('Instructing Party') ?>
                                        </div>

                         





                                        <div class="col-sm-4">
                                            <?php $target_date = ($model->target_date <> null)? $model->target_date:  date('Y-m-d', strtotime("+2 days")); ?>
                                            <?= $form->field($model, 'target_date', ['template' => '
                                            {label}
                                            <div class="input-group date" style="display: flex" id="listingstransactions-target_date" data-target-input="nearest">
                                            {input}
                                            <div class="input-group-append" data-target="#listingstransactions-target_date" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                            </div>
                                            {hint}{error}
                                            '])->textInput(['maxlength' => true,'value'=> $target_date])->label('Client Target Date') ?>
                                        </div>

                                        <div class="col-sm-4">
                                            <?php
                                            echo $form->field($model, 'email_subject')->textInput()->label('Client Email Subject');
                                            ?>

                                        </div>
                                        <div class="col-sm-4" >
                                            <?php
                                            echo $form->field($model, 'valuer_id')->widget(\kartik\select2\Select2::classname(), [
                                                'data' => array('21' => 'Laxmi Kumar', '38' => 'Febrian Rosyadi'),
                                            ])->label('Valuer Name');
                                            ?>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>
