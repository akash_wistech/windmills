<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'General/Special Asumption');
$cardTitle = Yii::t('app', 'General/Special Asumption:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_7/'.$quotation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->id ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                    <?php  echo $this->render('../left-nav', ['model' => $model,'step' => 7,'quotation'=>$quotation,'property_index'=> $property_index]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">
                                <div class="row">


                                    <div class="col-4">
                                        <?php
                                        if($model->property_general_asumption<>null){
                                            $model->property_general_asumption =  explode(',', $model->property_general_asumption);
                                        }else{
                                            $model->property_general_asumption =  explode(',', $quotation->assumptions);
                                        }
                                        echo $form->field($model, 'property_general_asumption')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(\app\models\GeneralAsumption::find()
                                                ->where(['type' => 1])
                                                ->orderBy(['general_asumption' => SORT_ASC,])
                                                ->all(), 'id', 'general_asumption'),
                                            'options' => ['placeholder' => 'General Assumptions ...'],
                                            'pluginOptions' => [
                                                'placeholder' => 'General Assumptions',
                                                'multiple' => true,
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>

                                    <div class="col-4">
                                        <?php
                                        if($model->property_special_asumption<>null){
                                            $model->property_special_asumption =  explode(',', $model->property_special_asumption);
                                        }else{
                                            $model->property_special_asumption =  explode(',', $quotation->special_assumptions);
                                        }
                                        echo $form->field($model, 'property_special_asumption')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(\app\models\GeneralAsumption::find()
                                                ->where(['type' => 2])
                                                ->orderBy(['general_asumption' => SORT_ASC,])
                                                ->all(), 'id', 'general_asumption'),
                                            'options' => ['placeholder' => 'Special Assumptions ...'],
                                            'pluginOptions' => [
                                                'placeholder' => 'Special Assumptions',
                                                'multiple' => true,
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>



                                </div>


                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>



