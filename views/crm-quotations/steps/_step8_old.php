<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use kartik\depdrop\DepDrop;
use app\assets\ValuationFormAsset;
ValuationFormAsset::register($this);


ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Receive Quotation');
$cardTitle = Yii::t('app', 'Receive Quotation:  {nameAttribute}', [
    'nameAttribute' => $model->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_0/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');
$discount = 0;
if ($model->relative_discount!=null) {

    $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($quotation_fee_total,$model->relative_discount);

}
$netValuationFee = $quotation_fee_total-$discount;
$VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
$finalFeePayable = $netValuationFee+$VAT;





?>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 8,'quotation'=>$quotation]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">


                                <div class="card-header">
                                    <h3 class="card-title">
                                        <i class="fas fa-edit"></i>
                                        <b>Quotation Details</b>
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'final_fee_approved')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'quotation_turn_around_time')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                        <?= $form->field($model, 'relative_discount')->widget(Select2::classname(), [
                                            'data' => yii::$app->quotationHelperFunctions->relativediscount,
                                            'options' => ['placeholder' => 'Select a Relative Discount ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Building / Project</th>
                                                <th>Tat</th>
                                                <th>Recomended Fee</th>
                                                <th>Final Fee</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if($receivedProperties <> null && !empty($receivedProperties)) {
                                                foreach($receivedProperties as $key => $property){
                                                    ?>
                                                    <tr>
                                                        <td> <?= $property->building->title; ?></td>
                                                        <td> <?= $property->tat ?></td>
                                                        <td><?= $property->recommended_fee ?></td>
                                                        <td> <?= $property->quotation_fee ?></td>

                                                    </tr>
                                            <?php
                                                }
                                            }
                                            ?>
                                            <tr>
                                                <td><b>Total</b></td>
                                                <td><?= $quotation_tat_total ?></td>
                                                <td><?= $recommended_fee ?></td>
                                                <td><?= $quotation_fee_total ?></td>

                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>

                                            </tr>

                                            <tr>
                                                <td><b>Discount</b></td>
                                                <td> </td>

                                                    <?php
                                                    if ($discount>0) {
                                                    if ($model->relative_discount =='base-fee') {?>
                                                <td class="bill-data">Base Fee Discount</td>
                                                <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($discount,2) ?></td>
                                                <?php
                                                } else{?>

                                                    <td class="bill-data"><?= $model->relative_discount ?>% Discount</td>
                                                    <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($discount,2) ?></td>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                }else{?>
                                                    <td class="bill-data">No Discount</td>
                                                    <td class="bill-data" style="border-right: 1px solid black;">0</td>
                                                    <?php
                                                }
                                                ?>



                                            </tr>
                                            <tr>
                                                <td><b>Net Valuation Fee</b></td>
                                                <td colspan="2"></td>
                                                <td><?= number_format($netValuationFee,2) ?></td>

                                            </tr>
                                            <tr>
                                                <td><b>Vat</b></td>
                                                <td></td>
                                                <td>5%</td>
                                                <td><?= number_format($VAT,2) ?></td>

                                            </tr>
                                            <tr>
                                                <td><b>Final Fee Payable</b></td>
                                                <td></td>
                                                <td></td>
                                                <td><?= number_format($finalFeePayable,2) ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mr-2']) ?>
                                <a href="<?= Url::toRoute(['crm-quotations/send-quotation?id='.$model->id]) ?>"><button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Send Quotation</button></a>

                                <a href="<?= Url::toRoute(['crm-quotations/qpdf?id='.$model->id]) ?>"><button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Download Quotation</button></a>

                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>



