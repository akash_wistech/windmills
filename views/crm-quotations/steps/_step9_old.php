<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use kartik\depdrop\DepDrop;
use app\assets\ValuationFormAsset;
ValuationFormAsset::register($this);


ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Receive Quotation');
$cardTitle = Yii::t('app', 'Receive Quotation:  {nameAttribute}', [
    'nameAttribute' => $model->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_0/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');
$discount = 0;
$discount_percentage =0;
if ($model->relative_discount!=null) {
    $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($quotation_fee_total,$model->relative_discount);
    $discount_percentage = $model->relative_discount;
}
$netValuationFee = $quotation_fee_total-$discount;
$VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
$finalFeePayable = $netValuationFee+$VAT;





?>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 9,'quotation'=>$quotation]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">


                                <div class="card-header">
                                    <h3 class="card-title">
                                        <i class="fas fa-edit"></i>
                                        <b>TOE Details</b>
                                    </h3>
                                </div>
                                <div class="card-body">

                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'recommended_fee')->textInput(['maxlength' => true, 'readonly'=>true, 'value'=> $recommended_fee]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'final_fee_approved')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="">Final Fee Approved After Discount (<?= $discount_percentage ?> %)</label>
                                            <input type="text"  class="form-control"  value="<?=$netValuationFee ?>" readonly="" aria-invalid="false">

                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'quotation_turn_around_time')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                                    </div>

                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'toe_final_fee')->textInput(['maxlength' => true,'readonly'=>true])->label('TOE Final Fee') ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'toe_final_turned_around_time')->textInput(['maxlength' => true,'readonly'=>true])->label('TOE Final Turn Around Time') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mr-2']) ?>
                                <a href="<?= Url::toRoute(['crm-quotations/send-toe?id='.$model->id]) ?>"><button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Send Toe</button></a>
                                <a href="<?= Url::toRoute(['crm-quotations/toe?id='.$model->id]) ?>"><button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Download Toe</button></a>

                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>



