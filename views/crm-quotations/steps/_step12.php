<style>
    .summary-table td{ padding: 0.25rem 0.5rem;}
</style>

<?php
$this->title = Yii::t('app', 'Review Fee Summary');
$cardTitle = Yii::t('app', 'Ref No:  {nameAttribute}', [
    'nameAttribute' => $model->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_0/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title float-none text-md-right ">
            <i class="fas fa-edit"></i>
            <?= $cardTitle ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 12, 'quotation' => $quotation]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <header class="card-header">
                                <h2 class="card-title fw-bold " style="font-weight:bold;">Review Quotation Summery</h2>
                            </header>
                            <div class="card-body">

                                <div class="row mb-3 ">
                                    <div class="col-4"  style="font-weight:bold; font-size:16px;b-4">Client Name: <span class="text-primary"><?php echo $model->client->title ?></span></div>
                                   
                                </div>

                                <?php
                                if ($amount <> null) {
                                    foreach ($amount as $key => $amt) {
                                    ?>

                                        <div class="row">
                                            <div class="col">
                                                <div class="card">
                                                    <div class="card-header card-outline card-info">
                                                        <h3 class="card-title"><strong>Property No.<?= $key + 1 ?>:</strong>
                                                            <span class="text-primary text-bold"><?= $amt['building_title']['title'] ?></span>
                                                        </h3>
                                                    </div>
                                                    <div class="card-body p-0">

                                                        <table class="table table-bordered summary-table">
                                                            <tbody>
                                                                <?php
                                                                foreach ($amt as $key2 => $value2) {
                                                                    // echo"<pre>"; print_r($value2['title']); echo "</pre>"; die;
                                                                ?>
                                                                    <tr>
                                                                        <td class="text-" style="font-size: 16px;"><strong><?= ucwords(str_replace('_', ' ', $key2)) ?></strong></td>
                                                                        <?php
                                                                        if (is_numeric($value2['title'])) {
                                                                        ?>
                                                                            <td class="text-" style="font-size: 16px;"><strong> <?= ucwords(str_replace('_', ' ', number_format($value2['title']))) ?></strong> </strong></td>
                                                                        <?php
                                                                        } else { ?>
                                                                            <td class="text-" style="font-size: 16px;"><strong> <?= ucwords(str_replace('_', ' ', $value2['title'])) ?></strong> </strong></td>
                                                                        <?php
                                                                        }
                                                                        ?>

                                                                        <td>
                                                                            <?php
                                                                            if ($key2 == 'building_title') { ?>
                                                                                <!-- <strong class="text-primary"><?php //echo $key2 
                                                                                                                    ?></strong> -->
                                                                            <?php
                                                                            } else { ?>
                                                                                <strong class="text-" style="font-size: 16px;"><?= number_format($value2['fee']) ?></strong>
                                                                            <?php
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                }
                                ?>
                                <div class="row">
                                    <div class="col">
                                        <div class="card card-primary ">
                                        <header class="card-header">
                                            <h2 class="card-title fw-bold " style="font-weight:bold;">Total Fee</h2>
                                        </header>
                                            <div class="card-body p-0">
                                                <table class="table table-bordered summary-table">
                                                    <tbody class="" style="font-size: 16px;">
                                                        <tr>
                                                            <td colspan="2"><mark><strong>Net Recommended Fee</strong></mark></td>
                                                            <td><mark><strong class="text-primary"><?= number_format($net_recommended_fee, 2) ?></strong></mark></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>Reference Fee 3rd Party</strong></td>
                                                            <td><?php if ($model->reference_fee_3rdparty_check === 0) { ?>
                                                                    <strong class="text-primary"><?=
                                                                    ($model->reference_fee_3rdparty != null) ? $model->reference_fee_3rdparty : 0 ?> %</strong>
                                                                <?php } else { ?>
                                                                    <strong class="text-primary"> Amount</strong>
                                                                <?php } ?>
                                                            </td>
                                                            <td><strong class="text-primary"><?= number_format($refFee3rdParty, 2) ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>Reference Fee Staff</strong></td>
                                                            <td><strong class="text-primary"><?= ($model->reference_fee_staff != null) ? $model->reference_fee_staff : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary"><?= number_format($refFeeStaff, 2) ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2"><mark><strong>Net Recommended Quotation Fee</strong></mark></td>
                                                            <td><mark><strong class="text-primary"><?= number_format($net_recommended_fee+$refFeeStaff+$refFee3rdParty, 2) ?></strong></mark></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2"><mark><strong>Net Approved Quotation Fee</strong></mark></td>
                                                            <td><mark><strong class="text-primary"><?= number_format($net_quotation_fee, 2) ?></strong></mark></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>Relationship Discount</strong></td>
                                                            <td><strong class="text-primary"><?= ($model->relative_discount_toe != null) ? $model->relative_discount_toe : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary"><?= ($relative_discount_toe['amount'] > 0) ? number_format($relative_discount_toe['amount'], 2) : '0.00' ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>First Time Discount</strong></td>
                                                            <td><strong class="text-primary"><?= 
                                                            ($model->first_time_discount != null) ? 
                                                            ($model->aprvd_first_time_discount != null) ? 
                                                            $model->aprvd_first_time_discount :
                                                            $model->first_time_discount : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary"><?= 
                                                            ($first_time_fee_discount['amount'] > 0) ? 
                                                            ($aprvd_first_time_fee_discount['amount'] > 0) ? 
                                                            number_format($aprvd_first_time_fee_discount['amount'], 2) :
                                                            number_format($first_time_fee_discount['amount'], 2) : '0.00' ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>Multiple Properties Discount</strong></td>
                                                            <td><strong class="text-primary"><?= 
                                                            ($model->no_of_property_discount != null) ? 
                                                            ($model->aprvd_no_of_property_discount != null) ? 
                                                            $model->aprvd_no_of_property_discount :
                                                            $model->no_of_property_discount : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary"><?= 
                                                            ($no_of_property_discount['amount'] > 0) ? 
                                                            ($aprvd_no_of_property_discount['amount'] > 0) ? 
                                                            number_format($aprvd_no_of_property_discount['amount'], 2) :
                                                            number_format($no_of_property_discount['amount'], 2) : '0.00' ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>Same Building Discount</strong></td>
                                                            <td><strong class="text-primary"><?= 
                                                            ($model->same_building_discount != null) ? 
                                                            ($model->aprvd_same_building_discount != null) ? 
                                                            $model->aprvd_same_building_discount :
                                                            $model->same_building_discount : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary"><?= 
                                                            ($same_building_discount['amount'] > 0) ? 
                                                            ($aprvd_same_building_discount['amount'] > 0) ?
                                                            number_format($aprvd_same_building_discount['amount'], 2) :
                                                            number_format($same_building_discount['amount'], 2) : '0.00' ?></strong></td>
                                                        </tr>

                                                            <!-- <tr>
                                                                <td><strong>General Discount</strong></td>
                                                                <td><strong><?= ($model->general_discount != null) ? $model->general_discount : 0 ?> %</strong></td>
                                                                <td><strong><?= ($general_discount > 0) ? number_format($general_discount, 2) : '0.00' ?></strong></td>
                                                            </tr> -->

                                                        <tr>
                                                            <td><strong>Total Discount</strong></td>
                                                            <td><strong class="text-primary"><?= 
                                                            ($total_discount_num != null) ? 
                                                            ($aprvd_total_discount_num != null) ? 
                                                            $aprvd_total_discount_num :
                                                            $total_discount_num : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary"><?= 
                                                            ($total_discount > 0) ? 
                                                            ($aprvd_total_discount > 0) ? 
                                                            number_format($aprvd_total_discount, 2) :
                                                            number_format($total_discount, 2) : '0.00' ?></strong></td>
                                                        </tr>

                                                        

                                                        <tr>
                                                            <td colspan="2"><mark><strong>Net Valuation Fee</strong></mark></td>
                                                            <td><mark><strong class="text-primary"><?=
                                                            ($aprvd_discount_net_fee <> null) ? 
                                                            number_format($aprvd_discount_net_fee, 2) : 
                                                            number_format($discount_net_fee, 2) 
                                                            ?></strong></mark></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2"><strong>Urgency Fee</strong></td>
                                                            <td><strong class="text-primary"><?=
                                                            ($urgencyfee_amount <> null) ? 
                                                            ($aprvd_urgencyfee_amount <> null) ? 
                                                            number_format($aprvd_urgencyfee_amount, 2) : 
                                                            number_format($urgencyfee_amount, 2) : '0.00'
                                                            ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>VAT</strong></td>
                                                            <td><strong class="text-primary"><?= ($model->client->vat == 1) ? 5 : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary"><?= 
                                                            ($VAT > 0) ? 
                                                            ($aprvd_VAT > 0) ? 
                                                            number_format($aprvd_VAT, 2) :
                                                            number_format($VAT, 2) : '0.00' ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2"> <mark><strong> Final Fee Payable </strong></mark></td>
                                                            <td><mark><strong class="text-primary"><?= 
                                                            ($finalFeePayable) ? 
                                                            ($aprvd_finalFeePayable) ? 
                                                            number_format($aprvd_finalFeePayable) :
                                                            number_format($finalFeePayable) : '0.00' ?></strong></mark></td>
                                                        </tr>




                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>