<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

$this->registerJs('


$(".imgInps").change(function(){
    readURL(this);
});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if(input.value.includes(".pdf")== true){
                $("#image_id").attr("src","'.Yii::$app->params['uploadPdfIcon'].'");
              }
              else {
                  $("#image_id").attr("src", e.target.result);
               }
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(".imgInps").change(function(){
    readURL(this);
});

$("body").on("click", ".remove_file", function () {
_this=$(this);
swal({
title: "'.Yii::t('app','Confirmation').'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({
  url: "'.Url::to(['valuation/signaturedelete','id'=>$model->id]).'",
  dataType: "html",
  type: "POST",
  success: function(html) {
  $("#image_id").attr("src", "'.Yii::$app->params['dummy_image_address'].'");
  },
  error: function(xhr,ajaxoptions,thownError){

    alert(xhr.responseText);
  }
  });
    }
});
});


$(".imgInps2").change(function(){
    readURL2(this);
});


function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if(input.value.includes(".pdf")== true){
                $("#image_2_id").attr("src","'.Yii::$app->params['uploadPdfIcon'].'");
              }
              else {
                  $("#image_2_id").attr("src", e.target.result);
               }
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(".imgInps2").change(function(){
    readURL2(this);
});

$("body").on("click", ".remove_file_2", function () {
_this=$(this);
swal({
title: "'.Yii::t('app','Confirmation').'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({
  url: "'.Url::to(['valuation/signaturedelete','id'=>$model->id]).'",
  dataType: "html",
  type: "POST",
  success: function(html) {
  $("#image_2_id").attr("src", "'.Yii::$app->params['dummy_image_address'].'");
  },
  error: function(xhr,ajaxoptions,thownError){

    alert(xhr.responseText);
  }
  });
    }
});
});



');

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Payment Slip');
$cardTitle = Yii::t('app', 'Payment Slip:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_24/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->id ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 11,'quotation'=>$quotation]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">


                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                            <header class="card-header">
                                <h2 class="card-title">Add Payment Slip</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-sm-6">
                                        <?php

                                        if($model->payment_status == 0){
                                            $label= 'Payment Slip';
                                        }else{
                                            $label= 'Payment Slip(50%)';
                                        }
                                        ?>
                                        <?= $form->field($model, 'payment_image', ['labelOptions'=>['style'=>'padding:9px; color:#E1F5FE; background-color:#0277BD;border-radius:3px;']])->fileInput(['class'=>'imgInps d-none'])->label($label) ?>

                                        <p style="font-weight: bold; font-size:18px">Upload Only Pdf</p>
                                        <?php

                                        $imgname = Yii::$app->params['dummy_image_address'];
                                        if ($model->payment_image != null) {

                                            // echo 'working';
                                            // print_r($model->signature_img);
                                            // die();
                                            $imgname = Yii::$app->get('s3bucket')->getUrl('payments/images/'.$model->payment_image);

                                            $attachment_link= $imgname;

                                            $attachment_src = Yii::$app->params['uploadDocsIcon'];

                                            if($model->id > 3040) {
                                                $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl('payments/images/'.$model->payment_image, '+10 minutes');
                                            }else {

                                                $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl('payments/images/'.$model->payment_image, '+10 minutes');

                                            }



                                          //  $link=$imgname;
                                            $link=$attachment_link;
                                        }
                                        ?>
                                        <div style='width:350px; margin-bottom:30px; position:relative;'>

                                            <?php

                                            if (strpos($imgname,'.pdf') == true) {
                                                $imgname=Yii::$app->params['uploadPdfIcon'];
                                            }

                                            ?>
                                            <img id="image_id" src="<?= $imgname ?>" alt="No Image is selected." width="350px"/>


                                            <?php
                                            if ($model->payment_image != null) {
                                                ?>
                                                <a style="color:#ffffff; font-weight: bold; padding-left:7px; padding-right :7px; font-size:22px; position:absolute;  bottom:10px; left:20px; background-color:#558B2F;";
                                                   href="<?= $link ?>" target="_blank"><i class="fas fa-eye"></i></a>
                                            <?php } ?>

                                        </div>
                                    </div>
                                <?php if($model->payment_status == 1){?>
                                    <div class="col-sm-6">

                                        <?= $form->field($model, 'payment_image_2', ['labelOptions'=>['style'=>'padding:9px; color:#E1F5FE; background-color:#0277BD;border-radius:3px;']])->fileInput(['class'=>'imgInps2 d-none'])->label('Final Payment Slip(50%)') ?>

                                        <p style="font-weight: bold; font-size:18px">Upload Only Pdf</p>
                                        <?php

                                        $imgname = Yii::$app->params['dummy_image_address'];
                                        if ($model->payment_image_2 != null) {

                                            // echo 'working';
                                            // print_r($model->signature_img);
                                            // die();
                                            $imgname = Yii::$app->get('s3bucket')->getUrl('payments/images/'.$model->payment_image_2);
                                            if($model->id > 3040) {
                                                $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl('payments/images/'.$model->payment_image_2, '+10 minutes');
                                            }else {

                                                $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl('payments/images/'.$model->payment_image_2, '+10 minutes');

                                            }

                                            $link=$attachment_link;
                                        }
                                        ?>
                                        <div style='width:350px; margin-bottom:30px; position:relative;'>

                                            <?php

                                            if (strpos($imgname,'.pdf') == true) {
                                                $imgname=Yii::$app->params['uploadPdfIcon'];
                                            }

                                            ?>
                                            <img id="image_2_id" src="<?= $imgname ?>" alt="No Image is selected." width="350px"/>


                                            <?php
                                            if ($model->payment_image_2 != null) {
                                                ?>
                                                <a style="color:#ffffff; font-weight: bold; padding-left:7px; padding-right :7px; font-size:22px; position:absolute;  bottom:10px; left:20px; background-color:#558B2F;";
                                                   href="<?= $link ?>" target="_blank"><i class="fas fa-eye"></i></a>
                                            <?php } ?>

                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /.card -->
</div>


<script type="text/javascript">




</script>
