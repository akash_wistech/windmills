<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use kartik\depdrop\DepDrop;
use app\assets\ValuationFormAsset;

ValuationFormAsset::register($this);


ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enter Valuation Details');
$cardTitle = Yii::t('app', 'Ref No:  {nameAttribute}', [
    'nameAttribute' => $model->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_0/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');


// dd("hey");


$this->registerJs('



    $("#listingstransactions-target_date").datetimepicker({
        allowInputToggle: true,
        format: "DD-MMM-YYYY",
        
    });

    $(document).on("click", function (e) {
        if ($(e.target).closest("#listingstransactions-target_date").length === 0) {
            $("#listingstransactions-target_date").datetimepicker("hide");
        }
    });

    $("#listingstransactions-valuation_date").datetimepicker({
        allowInputToggle: true,
        format: "DD-MMM-YYYY",
        
    });

    $(document).on("click", function (e) {
        if ($(e.target).closest("#listingstransactions-valuation_date").length === 0) {
            $("#listingstransactions-valuation_date").datetimepicker("hide");
        }
    });

    $("body").on("change", "#crmquotations-other_intended_users_check_1", function () {
        // console.log($(this).val());
        if($(this).val() == "Yes"){
            $("#other_intended_users_value").show();
        }else{
            $("#crmquotations-other_intended_users_check_1").val("No");
            $("#other_intended_users_value").hide();
        }
    });

     $("body").on("change", "#crmquotations-fee_crm", function () {
        // console.log($(this).val());
        if($(this).val() == 1){
            $("#fee_crm_client_request_value").show();
        }else{
            $("#crmquotations-fee_crm").val(0);
            $("#fee_crm_client_request_value").hide();
        }
    });


    $(document).ready(function () {
        // Get references to the relevant form elements
        var $noOfUnitsField = $("#crmquotations-no_of_units_same_building"); 
        var $noOfPropertiesField = $("#crmquotations-no_of_properties"); 
        var $errorSpan = $("#units-error");
    
        // Add an event listener to the no_of_units_same_building field
        $noOfUnitsField.on("input", function () {
            var unitsValue = parseInt($noOfUnitsField.val(), 10);
            var propertiesValue = parseInt($noOfPropertiesField.val(), 10);
    
            if (isNaN(unitsValue) || isNaN(propertiesValue)) {
                // Do nothing if the input is not a valid number
                return;
            }
    
            if (unitsValue > propertiesValue) {
                // Display the error message and prevent form submission
                $errorSpan.css("display", "block");
            } else {
                // Hide the error message
                $errorSpan.css("display", "none");
            }
        });


        var scopeOfService = $("#crmquotations-scope_of_service");
        if (scopeOfService.val() == 6 || scopeOfService.val() == 7 || scopeOfService.val() == 8) {
                
            $(".inspection_type").hide();
            $(".no_of_units_same_building").hide();
            $(".purpose_of_valuation").hide();
        } else {
            $(".inspection_type").show();
            $(".no_of_units_same_building").show();
            $(".purpose_of_valuation").show();
        }

        $(scopeOfService).on("change", function () {
            
            if (scopeOfService.val() == 6 || scopeOfService.val() == 7 || scopeOfService.val() == 8) {
                
                $(".inspection_type").hide();
                $(".no_of_units_same_building").hide();
                $(".purpose_of_valuation").hide();
            } else {
                $(".inspection_type").show();
                $(".no_of_units_same_building").show();
                $(".purpose_of_valuation").show();
            }
        });


        $("form#w0").on("input", "#crmquotations-reference_fee_3rdparty", function() {
            var value = parseFloat($(this).val());
            var selectedOption = $("#crmquotations-reference_fee_3rdparty_check").val();
    
            if (selectedOption === "0" && value > 100) {
                $(this).val(100); // Set the value to 100 if its above 100
                $("#percentage-error").html("Value cannot exceed 100 when Percentage (%) is selected.").show();
            } else {
                $("#percentage-error").html("").hide();
            }

            
        });
    
        // Handle the change event of the select field
        $("form#w0").on("change", "#crmquotations-reference_fee_3rdparty_check", function() {
            var selectedOption = $(this).val();
            var value = parseFloat($("#crmquotations-reference_fee_3rdparty").val());
    
            if (selectedOption === "0" && value > 100) {
                $("#crmquotations-reference_fee_3rdparty").val(100); // Set the value to 100 if its above 100
                $("#percentage-error").html("Value cannot exceed 100 when Percentage (%) is selected.").show();
            } else {
                $("#percentage-error").html("").hide();
            }

           
        });

    });








');
$paymentTypes = yii::$app->crmQuotationHelperFunctions->paymentTerms;
?>
<style>
    .datepicker-days .table>thead>tr>th,
    .table>tbody>tr>th,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>tbody>tr>td,
    .table>tfoot>tr>td {
        padding: inherit !important;
    }

    .help-block2 {
        color: red;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -14px;
        font-family: inherit;
    }
</style>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title float-none text-md-right ">
            <i class="fas fa-edit"></i>
            <?= $cardTitle ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../../left-nav', ['model' => $model, 'step' => 0, 'quotation' => $quotation, 'showAlert' => $showAlert]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form">
                            <?php $form = ActiveForm::begin(); ?>

                            <div class="card card-outline card-primary mb-3">
                                <header class="card-header">
                                    <h2 class="card-title">Other Intended Users Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <?php
                                            echo $form->field($model, 'other_intended_users_check_1')->widget(Select2::classname(), [
                                                'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                        <?php
                                        if ($model->other_intended_users_check_1 == 'Yes') {
                                            $style = "block";
                                        } else {
                                            $style = "none";
                                        }
                                        ?>
                                        <div class="col-md-4" id="other_intended_users_value" style="display: <?= $style ?>">
                                            <?= $form->field($model, 'other_intended_users')->textInput(['maxlength' => true])->label('Other Intended Users Value <span class="text-danger">*</span>'); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="card card-outline card-primary mb-3">
                                <header class="card-header">
                                    <h2 class="card-title">Valuation Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <?php if ($quotation->id < 3030) { ?>
                                            <div class="col-md-4">
                                                <?= $form->field($model, 'scope_of_service')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->ScopeOfWorkList,
                                                    'options' => ['placeholder' => 'Select Scope Of Service...', 'class' => 'client'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]); ?>
                                            </div>
                                        <?php } ?>

                                        <div class="col-md-4">
                                            <?= $form->field($model, 'no_of_properties')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->getCrmOptionOneToNumber(200),
                                                'options' => [
                                                    'placeholder' => 'Select No of Assets...',
                                                    'value' => !empty($model->no_of_properties) ? $model->no_of_properties : 1,
                                                ],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('No of Assets'); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php
                                            echo $form->field($model, 'industry')->widget(Select2::classname(), [
                                                'data' => Yii::$app->crmQuotationHelperFunctions->IndustryList,
                                                'options' => ['id' => 'industry', 'placeholder' => 'Select Industry'],
                                            ])->label('Industry');
                                            ?>
                                        </div>

                                        <div class="col-sm-4 location">
                                            <?= $form->field($model, 'location')->widget(
                                                Select2::classname(),
                                                [
                                                    'data' => Yii::$app->appHelperFunctions->emiratedListArr,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]
                                            )->label('Location'); ?>
                                        </div>

                                        <div class="col-sm-4 no_of_location">
                                            <?php
                                            echo $form->field($model, 'no_of_location')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->getCrmOptionOneToNumber(200),
                                                'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                            ])->label('Number Of Location');
                                            ?>
                                        </div>


                                        <div class="col-md-4 inspection_type">
                                            <?php
                                            echo $form->field($model, 'inspection_type')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->inspectionTypeArr,
                                                'options' => [
                                                    'placeholder' => 'Select',
                                                ],
                                            ]);
                                            ?>
                                        </div>

                                        <?php /* ?>
                                        <div class="col-md-4 no_of_units_same_building">
                                            <?= $form->field($model, 'no_of_units_same_building')->textInput(['maxlength' => true, 'type' => 'number', 'value' => !empty($model->no_of_units_same_building) ? $model->no_of_units_same_building : 1])->label('Units In Same Building'); ?>
                                            <span id="units-error" class="help-block2" style="display: none">Units in
                                                the same building cannot be greater than the number of
                                                properties.</span>
                                        </div>
                                        <?php */ ?>


                                        <div class="col-md-4 purpose_of_valuation">
                                            <?php
                                            echo $form->field($model, 'purpose_of_valuation')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->purposeOfValuationArr,
                                                'options' => ['placeholder' => 'Select a Purpose ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Purpose Of Valuation <span class="text-danger">*</span>');
                                            ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php
                                            echo $form->field($model, 'reference_fee_staff')->widget(Select2::classname(), [
                                                'data' => array('0' => '0 %', '5' => '5 %', '10' => '10 %'),

                                            ])->label('Reference Fee Staff');
                                            ?>
                                        </div>

                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'reference_fee_staff_name')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(\app\models\User::find()
                                                    ->where(['status' => 1])
                                                    ->andWhere(['user_type' => 10])
                                                    ->orderBy(['firstname' => SORT_ASC])
                                                    ->all(), 'id', function ($user) {
                                                    return $user->firstname . ' ' . $user->lastname; // Concatenate first name and last name
                                                }),
                                                'options' => ['placeholder' => 'Select staff ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Reference Fee Staff Name'); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php
                                            echo $form->field($model, 'reference_fee_3rdparty_check')->widget(Select2::classname(), [
                                                'data' => array('0' => 'Percentage (%)', '1' => 'Amount'),
                                            ])->label('Reference Fee Third Party In');
                                            ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?= $form->field($model, 'reference_fee_3rdparty')->textInput([
                                                'type' => 'number', 'value' => ($model->reference_fee_3rdparty) ? $model->reference_fee_3rdparty : '0',
                                                'min' => '0',
                                                'class' => 'form-control'
                                            ])->label('Reference Fee Third Party') ?>
                                            <div id="percentage-error" class="text-danger help-block2" style="display: none;"></div>
                                        </div>






                                    </div>
                                </div>
                            </div>

                            <div class="card card-outline card-primary mb-3">
                                <header class="card-header">
                                    <h2 class="card-title">Client Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <?php
                                        if ($model->client_name == 1) { ?>
                                            <div class="col-md-4" id="client_invoice_type">
                                                <?php
                                                echo $form->field($model, 'client_invoice_type')->widget(\kartik\select2\Select2::classname(), [
                                                    'data' => array('0' => 'Client Name', '1' => 'Customer Name'),

                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Client Invoice Name');
                                                ?>
                                            </div>
                                        <?php } ?>

                                        <div class="col-md-4 advance_payment_terms">
                                            <?= $form->field($model, 'advance_payment_terms')->widget(Select2::classname(), [
                                                'data' => $paymentTypes,
                                                'options' => ['placeholder' => 'Advance Payment...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Advance Payment Terms <span class="text-danger">*</span>'); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="card card-outline card-primary mb-3">
                                <header class="card-header">
                                    <h2 class="card-title">Urgency Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <?php
                                            echo $form->field($model, 'client_urgency')->widget(Select2::classname(), [
                                                'data' => array_reverse(Yii::$app->helperFunctions->arrYesNo),
                                                'options' => ['id' => 'client-urgency'],
                                            ])->label('Client Urgency');
                                            ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php
                                            echo $form->field($model, 'tat_requirements')->widget(\kartik\select2\Select2::classname(), [
                                                'data' => array('0' => 'Standard', '1' => 'Urgent Same Day', '2' => 'Urgent 1 day', '3' => 'Urgent 2 days'),
                                            ])->label('TAT Requirements');
                                            ?>
                                        </div>

                                        <div class="col-sm-4">
                                            <?php
                                            $target_date = ($model->target_date <> null) ? date('d-M-Y', strtotime($model->target_date)) : date('d-M-Y', strtotime("+2 days"));
                                            ?>
                                            <?= $form->field($model, 'target_date', [
                                                'template' => '
                                                {label}
                                                <div class="input-group date" style="display: flex" id="listingstransactions-target_date" data-target-input="nearest">
                                                {input}
                                                <div class="input-group-append" data-target="#listingstransactions-target_date" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                                </div>
                                                {hint}{error}
                                            '
                                            ])->textInput(['maxlength' => true, 'value' => $target_date])->label('Client Assignment Target Date') ?>

                                        </div>

                                        <!-- <div class="col-sm-4">
                                            <?php
                                            $valuation_date = ($model->valuation_date <> null) ? date('d-M-Y', strtotime($model->valuation_date)) : '';
                                            ?>
                                            <?= $form->field($model, 'valuation_date', [
                                                'template' => '
                                                {label}
                                                <div class="input-group date" style="display: flex" id="listingstransactions-valuation_date" data-target-input="nearest">
                                                {input}
                                                <div class="input-group-append" data-target="#listingstransactions-valuation_date" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                                </div>
                                                {hint}{error}
                                            '
                                            ])->textInput(['maxlength' => true, 'value' => $valuation_date])->label('Valuation Date') ?>

                                        </div> -->


                                    </div>
                                </div>
                            </div>

                            <div class="card card-outline card-primary mb-3">
                                <header class="card-header">
                                    <h2 class="card-title">Valuer Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'valuer_id')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(\app\models\User::find()
                                                    ->where(['status' => 1])
                                                    ->andWhere(['user_type' => 10])
                                                    ->andWhere(['quotation_approver' => 1])
                                                    ->orderBy(['firstname' => SORT_DESC])
                                                    ->all(), 'id', function ($user) {
                                                    return $user->firstname . ' ' . $user->lastname; // Concatenate first name and last name
                                                }),
                                                'options' => ['placeholder' => 'Select a Valuer ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Valuer/BCS Manager Name'); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="card card-outline card-primary mb-3">
                                <header class="card-header">
                                    <h2 class="card-title">Quotation Fee Format for Properties</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <?php
                                            echo $form->field($model, 'fee_crm')->widget(Select2::classname(), [
                                                'data' => array(0=>'Total Fee',1=>'Individual Fee'),
                                            ])->label('Fee Format');
                                            ?>
                                        </div>
                                        <?php
                                        if ($model->fee_crm == 1) {
                                            $style = "block";
                                        } else {
                                            $style = "none";
                                        }
                                        ?>
                                        <div class="col-md-4" id="fee_crm_client_request_value" style="display: <?= $style ?>">


                                            <?php
                                            echo $form->field($model, 'fee_crm_client_request')->widget(Select2::classname(), [
                                                'data' => array(0=>'No',1=>'Yes'),
                                            ])->label('Requested by Client?');
                                            ?>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i> Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>