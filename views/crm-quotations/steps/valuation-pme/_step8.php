<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use kartik\depdrop\DepDrop;
use app\assets\ValuationFormAsset;

ValuationFormAsset::register($this);


ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Receive Quotation');
$cardTitle = Yii::t('app', 'Receive Quotation:  {nameAttribute}', [
    'nameAttribute' => $model->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_0/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');

$total_discount = 0;
$aprvd_total_discount = 0;
$total_discount_amount = 0;
$bo_rec_total_discount_amount = 0;
$rev_total_discount_amount = 0;
$aprvd_total_discount_amount = 0;
$discount = 0;
$aprvd_discount = 0;
$bo_rec_discount = 0;
$rev_discount = 0;


$discount_no_of_properties = 0;
$discount_first_time = 0;
$same_building_discount = 0;
$aprvd_first_time_discount = 0;
$aprvd_discount_no_of_properties = 0;
$aprvd_same_building_discount = 0;
$bo_rec_same_building_discount = 0;
$rev_same_building_discount = 0;
$urgencyfee = 0;
$advance_paymentfee = 0;
$VAT = 0;

// $netValuationFee = $quotation_fee_total - $discount;
if($model->id > $old_qid_30012024){$netValuationFee = ($recommended_fee_total) - $discount;}
else {$netValuationFee = ($recommended_fee_total + $reference_fee_total) - $discount;}
$aprvd_netValuationFee = $quotation_fee_total - $aprvd_discount;
$bo_rec_netValuationFee = $bo_recommended_fee_total - $bo_rec_discount;
$rev_netValuationFee = $reviewed_fee_total - $rev_discount;


if ($model->no_of_property_discount != null) {
    $netValuationFee = $netValuationFee - $no_of_property_discount_data['amount'];
    $total_discount = $total_discount + $model->no_of_property_discount;
    $total_discount_amount = $total_discount_amount + $no_of_property_discount_data['amount'];

    $bo_rec_netValuationFee = $bo_rec_netValuationFee - $bo_recommended_no_of_property_discount_data['amount'];
    $bo_rec_total_discount = $bo_rec_total_discount + $model->no_of_property_discount;
    $bo_rec_total_discount_amount = $bo_rec_total_discount_amount + $bo_recommended_no_of_property_discount_data['amount'];

    $rev_netValuationFee = $rev_netValuationFee - $reviewed_no_of_property_discount_data['amount'];
    $rev_total_discount = $rev_total_discount + $model->no_of_property_discount;
    $rev_total_discount_amount = $rev_total_discount_amount + $reviewed_no_of_property_discount_data['amount'];

}

// added for approved no of property discount
if ($model->aprvd_no_of_property_discount != null) {
    $aprvd_netValuationFee = $aprvd_netValuationFee - $aprvd_no_of_property_discount_data['amount'];
    $aprvd_total_discount = $aprvd_total_discount + $model->aprvd_no_of_property_discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_no_of_property_discount_data['amount'];
}

if ($model->same_building_discount != null) {
    $netValuationFee = $netValuationFee - $same_building_discount_data['amount'];
    $total_discount = $total_discount + $model->same_building_discount;
    $total_discount_amount = $total_discount_amount + $same_building_discount_data['amount'];

    $bo_rec_netValuationFee = $bo_rec_netValuationFee - $bo_recommended_same_building_discount_data['amount'];
    $bo_rec_total_discount = $bo_rec_total_discount + $model->same_building_discount;
    $bo_rec_total_discount_amount = $bo_rec_total_discount_amount + $bo_recommended_same_building_discount_data['amount'];

    $rev_netValuationFee = $rev_netValuationFee - $reviewed_same_building_discount_data['amount'];
    $rev_total_discount = $rev_total_discount + $model->same_building_discount;
    $rev_total_discount_amount = $rev_total_discount_amount + $reviewed_same_building_discount_data['amount'];

    

}

// added for approved same building discount
if ($model->aprvd_same_building_discount != null) {
    $aprvd_netValuationFee = $aprvd_netValuationFee - $aprvd_same_building_discount_data['amount'];
    $aprvd_total_discount = $aprvd_total_discount + $model->aprvd_same_building_discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_same_building_discount_data['amount'];
}



if ($model->first_time_discount != null) {
    $netValuationFee = $netValuationFee - $first_time_fee_discount_data['amount'];
    $total_discount = $total_discount + $model->first_time_discount;
    $total_discount_amount = $total_discount_amount + $first_time_fee_discount_data['amount'];

    $bo_rec_netValuationFee = $bo_rec_netValuationFee - $bo_recommended_first_time_fee_discount_data['amount'];
    $bo_rec_total_discount = $bo_rec_total_discount + $model->first_time_discount;
    $bo_rec_total_discount_amount = $bo_rec_total_discount_amount + $bo_recommended_first_time_fee_discount_data['amount'];

    $rev_netValuationFee = $rev_netValuationFee - $reviewed_first_time_fee_discount_data['amount'];
    $rev_total_discount = $rev_total_discount + $model->first_time_discount;
    $rev_total_discount_amount = $rev_total_discount_amount + $reviewed_first_time_fee_discount_data['amount'];
}

// added for approved first time discount
// if ($model->aprvd_first_time_discount != null) {
if ($model->first_time_discount != null) {
    $aprvd_netValuationFee = $aprvd_netValuationFee - $aprvd_first_time_fee_discount_data['amount'];
    $aprvd_total_discount = $aprvd_total_discount + $model->aprvd_first_time_discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_first_time_fee_discount_data['amount'];
}


if ($model->relative_discount != null) {
    $discount = yii::$app->quotationHelperFunctions->getDiscountRupee($recommended_fee_total, $model->relative_discount);
    $aprvd_discount = yii::$app->quotationHelperFunctions->getDiscountRupee($quotation_fee_total, $model->relative_discount);
    $bo_rec_discount = yii::$app->quotationHelperFunctions->getDiscountRupee($bo_recommended_fee_total, $model->relative_discount);
    $rev_discount = yii::$app->quotationHelperFunctions->getDiscountRupee($reviewed_fee_total, $model->relative_discount);

    // dd($aprvd_discount);
    $total_discount = $total_discount + $model->relative_discount;
    $total_discount_amount = $total_discount_amount + $discount;
    $netValuationFee = $netValuationFee - $discount;

    $bo_rec_total_discount = $bo_rec_total_discount + $model->relative_discount;
    $bo_rec_total_discount_amount = $bo_rec_total_discount_amount + $bo_rec_discount;
    $bo_rec_netValuationFee = $bo_rec_netValuationFee - $bo_rec_discount;

    $rev_total_discount = $rev_total_discount + $model->relative_discount;
    $rev_total_discount_amount = $rev_total_discount_amount + $rev_discount;
    $rev_netValuationFee = $rev_netValuationFee - $rev_discount;

    // added for approved total relative discount
    $aprvd_total_discount = $aprvd_total_discount + $model->relative_discount;
    $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_discount;
    $aprvd_netValuationFee = $aprvd_netValuationFee - $aprvd_discount;

}

$discount_net_fee = $netValuationFee;
$bo_rec_discount_net_fee = $bo_rec_netValuationFee;
$rev_discount_net_fee = $rev_netValuationFee;

// added for approved discount net fee
// $aprvd_discount_net_fee = $aprvd_netValuationFee;

if ($aprvd_total_discount == 0 && $netValuationFee == $aprvd_netValuationFee) {
    $aprvd_netValuationFee = $netValuationFee;
}



if ($aprvd_total_discount > 0 || $netValuationFee != $aprvd_netValuationFee) {
    $aprvd_discount_net_fee = $aprvd_netValuationFee;
} else {
    $aprvd_discount_net_fee = $netValuationFee;
}


if ($urgencyfee_data != null) {
    $netValuationFee = $netValuationFee + $recommended_urgencyfee_data['amount'];
    $bo_rec_netValuationFee = $bo_rec_netValuationFee + $bo_recommended_urgencyfee_data['amount'];
    $rev_netValuationFee = $rev_netValuationFee + $reviewed_urgencyfee_data['amount'];
}

// added for approved net valuation fee
if ($aprvd_urgencyfee_data != null) {
    $aprvd_netValuationFee = $aprvd_netValuationFee + $aprvd_urgencyfee_data['amount'];
}


if ($advance_payment_terms_data != null) {
    $netValuationFee = $netValuationFee + $advance_payment_terms_data['amount'];
    $bo_rec_netValuationFee = $bo_rec_netValuationFee + $advance_payment_terms_data['amount'];
    $rev_netValuationFee = $rev_netValuationFee + $advance_payment_terms_data['amount'];

    // added for approved net valuation fee
    $aprvd_netValuationFee = $aprvd_netValuationFee + $advance_payment_terms_data['amount'];
}


if ($model->client->vat == 1) {
    $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
    $aprvd_VAT = yii::$app->quotationHelperFunctions->getVatTotal($aprvd_netValuationFee);
    $bo_rec_VAT = yii::$app->quotationHelperFunctions->getVatTotal($bo_rec_netValuationFee);
    $rev_VAT = yii::$app->quotationHelperFunctions->getVatTotal($rev_netValuationFee);
}



$finalFeePayable = $netValuationFee + $VAT;
$bo_rec_finalFeePayable = $bo_rec_netValuationFee + $bo_rec_VAT;
$rev_finalFeePayable = $rev_netValuationFee + $rev_VAT;

// added for applying vat approved fee
$aprvd_finalFeePayable = $aprvd_netValuationFee + $aprvd_VAT;




if ($aprvd_finalFeePayable <> $finalFeePayable) {
    Yii::$app->db->createCommand()
        ->update('crm_quotations', ['final_quoted_fee' => $aprvd_finalFeePayable], 'id=' . $model->id . '')
        ->execute();
} else {
    Yii::$app->db->createCommand()
        ->update('crm_quotations', ['final_quoted_fee' => $finalFeePayable], 'id=' . $model->id . '')
        ->execute();
}



// dd($receivedProperties);


?>
<style>
    .th-sub-title {
        font-size: 9px;
        font-style:italic;
        color:#cacaca;
    }
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
</style>
<style>
    /* .proposal-table th:first-of-type { padding-left: 5px !important;  } */
    /* .proposal-table td:first-of-type { padding-left: 5px !important;} */
    .proposal-table th { padding: 5px 5px; font-size: 15px; background-color:#fff; background-color: #efefef;}
    .proposal-table td { padding: 2px 5px; font-size: 14px; vertical-align:middle; max-width: 110px;}
    .proposal-table tr.trGreyBg { background-color:#eaeaea;  }
    .proposal-table tr.trWhiteBg { background-color:#fff; }
    .proposal-table tr.trBrdr { border-top: 4px solid #ccc; }
    .proposal-table tr.trBrdr td { border-bottom: none; }
    .proposal-table tr.trBrdrTopWhite { border-bottom: none !important; }
    .proposal-table tr.trBrdrTopWhite td {  border-top: none  !important; border-bottom:none  !important; font-weight:bold}
    .proposal-table { background-color: #fff;}
    /* .proposal-table td{ padding: 5px 5px;} */
    .left-panel {
            -ms-flex: 0 0 3%;
            flex: 0 0 3%;
            max-width: 3%;
            max-height: 100vh;
            transition: max-width 0.3s ease;
    }
    .left-panel #vert-tabs-tab {
        -ms-flex: 0 0 0%;
        flex: 0 0 0%;
        max-width: 0%;
        overflow: hidden;
        transition: max-width 0.3s ease;
    }
    .right-panel {
        -ms-flex: 0 0 97%;
        flex: 0 0 97%;
        max-width: 97%;
        transition: max-width 0.3s ease;
    }
    .hide-left-panel.left-panel {
        -ms-flex: 0 0 20%;
        flex: 0 0 20%;
        max-width: 20%;
        max-height: 100%;
    }
    .right-panel.full-right-panel {
        -ms-flex: 0 0 80%;
        flex: 0 0 80%;
        max-width: 80%;
    }
    .hide-left-panel #vert-tabs-tab {
        -ms-flex: 0 0 100%;
        flex: 0 0 100%;
        max-width: 100%;
    }
    .toggleButton { padding:2px 10px; color: #007bff; cursor: pointer;}
    input.form-control { padding: 0.375rem 0.10rem 0.375rem 0.55rem;}
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title float-none text-md-right">
            <i class="fas fa-edit"></i> Ref No:
            <?= $model->reference_number ?>
        </h3>

    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-3 left-panel"><div class="toggleButton" id="togglePanel" title="Left Panel" ><i class="fas fa-bars"></i></div>
                <?php echo $this->render('../../left-nav', ['model' => $model, 'step' => ($step_num <> null) ? $step_num : 8, 'quotation' => $quotation]); ?>
            </div>
            <div class="col-9 right-panel">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                        aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title fw-bold " style="font-weight:bold;">Client Name: <span class="text-primary"><?php echo ucwords($model->client->title) ?></span></h2>
                            </header>
                            <div class="card-body">


                                    <div class="row">

                                        <div class="col-sm-6"></div>
                                        <div class="col-sm-6">
                                            <table class="table table-sm table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Time Period</th>
                                                        <th>Highest Fee Approved</th>
                                                        <th>Lowest Fee Approved</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-bold">Last 3 Months</td>
                                                        <td class="text-center">
                                                            <?= number_format($high_low_fee['last_3_months']->highest_fee) ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <?= number_format($high_low_fee['last_3_months']->lowest_fee) ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-bold">Last 6 Months</td>
                                                        <td class="text-center">
                                                            <?= number_format($high_low_fee['last_6_months']->highest_fee) ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <?= number_format($high_low_fee['last_6_months']->lowest_fee) ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-bold">Last Year</td>
                                                        <td class="text-center">
                                                            <?= number_format($high_low_fee['last_1_year']->highest_fee) ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <?= number_format($high_low_fee['last_1_year']->lowest_fee) ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                        <div class="row">
                                        <table class="table table-bordered proposal-table">
                                            <thead>
                                                <tr>
                                                    <th>SOS</th>
                                                    <!-- <th>Client</br>Type</th> -->
                                                    <th>Asset Cateory</th>
                                                    <th>Quantity</th>
                                                    <th>Complexity</th>
                                                    <th>Location</th>
                                                    <th>No of Location</th>
                                                    <!-- <th>Working Days</th> -->
                                                    <th>Asset Age</th>
                                                    <th>Comparables</th>
                                                    <th>OIU</th>
                                                    <th>Maxima</br> Fee</th>
                                                    <th>Recomm.</br> Fee</th>
                                                    <th>Reviewed</br> Fee</th>
                                                    <th>Approved</br> Fee</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                if ($multiScopeProperties <> null && !empty($multiScopeProperties)) {
                                                    foreach ($multiScopeProperties as $keys => $property) {
                                                        // dd($property);

                                                        $multiScopeProperty = \app\models\CrmReceivedProperties::find()
                                                            ->where(['multiscope_id' => $property->multiscope_id])
                                                            ->orderBy(['id' => SORT_DESC])->all();

                                                        ?>
                                                        
                                                        <!-- <tr class="trBrdr">
                                                            <td colspan="22"><strong>Asset <?= $keys+1 ?>: <?= $property->building->title ?></strong> </td>
                                                        </tr> -->
                                                        <?php 
                                                        

                                                           
                                                        $buildingTotalRecTat = 0;
                                                        $buildingTotalBoRecTat = 0;
                                                        $buildingTotalRevTat = 0;
                                                        $buildingTotalAprvdTat = 0;
                                                        
                                                        $buildingTotalRecFee = 0;
                                                        $buildingTotalBoRecFee = 0;
                                                        $buildingTotalRevFee = 0;
                                                        $buildingTotalAprvdFee = 0;

                                                        foreach ($multiScopeProperty as $key => $multiproperty) {

                                                            // dd($multiScopeProperty);
                                                            ?> 
                                                            <tr class="trBrdr">
                                                                <td>
                                                                    <?php echo Yii::$app->appHelperFunctions->ScopeOfWorkListShort[$multiproperty->scope_of_service_saved]; ?>
                                                                </td>
                                                                <td>
                                                                    <?= $multiproperty->assetCategory->title; ?>
                                                                </td>
                                                                <td>
                                                                    <?= $multiproperty->no_of_asset; ?>
                                                                </td>
                                                                <td>
                                                                    <?= ucwords($multiproperty->asset_complexity); ?>
                                                                </td>
                                                                <td>
                                                                    <?= Yii::$app->appHelperFunctions->emiratedListArr[$model->location]; ?>
                                                                </td>
                                                                <td>
                                                                    <?= $model->no_of_location; ?>
                                                                </td>
                                                                <!-- <td>
                                                                    <?= $multiproperty->working_days; ?>
                                                                </td> -->
                                                                <td>
                                                                    <?= $multiproperty->asset_age; ?>
                                                                </td>
                                                        
                                                                <td> 
                                                                    <?php  $comparables = $multiproperty->no_of_comparables_value; ?>
                                                                    <?= ($comparables <> null) ? $comparables : "NA"; ?>
                                                                
                                                                </td>
                                                                <td>
                                                                    <?= ($quotation->other_intended_users_check_1 <> null) ? $quotation->other_intended_users_check_1 : "NA"; ?>
                                                                </td>
                                                                <?php if ($model->quotation_status == 0 || $model->quotation_status == 9 || $model->quotation_status == 14 || $model->quotation_status == 17) { ?>                                                            
                                                                        <td > 
                                                                            <div> <input type="number"
                                                                            name="CrmQuotations[calculations][<?= $multiproperty->id ?>][tat]"
                                                                            value="<?= $multiproperty->tat ?>" placeholder=""
                                                                            class="form-control" style="width: 35px; float: left;" readonly=""  required></div>
                                                                        </td>
                                                                        <td > 
                                                                            <div><input type="number" min="0"
                                                                            name="CrmQuotations[calculations][<?= $multiproperty->id ?>][bo_recommended_tat]"
                                                                            value="<?=  ($multiproperty->bo_recommended_tat !== null) ? $multiproperty->bo_recommended_tat : $multiproperty->tat ?>"
                                                                            placeholder="" class="form-control" style="width: 35px" <?= $reommended_readonly ?> >
                                                                            </div>
                                                                        </td>
                                                                        <td > 
                                                                            <div><input type="number" min="0"
                                                                            name="CrmQuotations[calculations][<?= $multiproperty->id ?>][reviewed_tat]"
                                                                            value="<?= ($multiproperty->reviewed_tat !== null ) ? $multiproperty->reviewed_tat : $multiproperty->tat ?>"
                                                                            placeholder="" class="form-control" style="width: 35px" <?= $quotation_readonly ?> ></div>

                                                                        </td>
                                                                        <td > 
                                                                            <div><input type="number" min="0"
                                                                            name="CrmQuotations[calculations][<?= $multiproperty->id ?>][aprvd_tat]"
                                                                            value="<?= ($multiproperty->aprvd_tat !== null ) ? $multiproperty->aprvd_tat : $multiproperty->tat  ?>"
                                                                            placeholder="" class="form-control" style="width: 35px" <?= $quotation_readonly ?> ></div>
                                                                        </td>
                                                                <?php } else { ?>
                                                                        <td > 
                                                                            <div><?= $multiproperty->tat ?> </div>
                                                                        </td>
                                                                        <td > 
                                                                            <div><?= ($multiproperty->bo_recommended_tat !== null) ? $multiproperty->bo_recommended_tat : $multiproperty->tat ?> </div>
                                                                        </td>
                                                                        <td > 
                                                                            <div><?= ($multiproperty->reviewed_tat !== null) ? $multiproperty->reviewed_tat : $multiproperty->tat ?> </div>
                                                                        </td>
                                                                        <td > 
                                                                            <div><?= ($multiproperty->aprvd_tat !== null) ? $multiproperty->aprvd_tat : $multiproperty->tat ?> </div>
                                                                        </td>
                                                                <?php } ?>
                                                                
                                                                
                                                            
                                                            </tr>
                                                            <tr  class="trBrdrTopWhite" >
                                                                <td></td>
                                                                <td>
                                                                    <?= $amount_summary[$multiproperty->id]['asset_category']['fee'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $amount_summary[$multiproperty->id]['no_of_asset']['fee'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $amount_summary[$multiproperty->id]['asset_complexity']['fee'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $amount_summary[$multiproperty->id]['location']['fee'] ?>  
                                                                </td>
                                                                <td>
                                                                    <?= $amount_summary[$multiproperty->id]['no_of_location']['fee'] ?>  
                                                                </td>
                                                                <!-- <td>
                                                                    <?= $amount_summary[$multiproperty->id]['working_days']['fee'] ?>
                                                                </td> -->
                                                                <td>
                                                                    <?= $amount_summary[$multiproperty->id]['asset_age']['fee'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $amount_summary[$multiproperty->id]['number_of_comparables']['fee'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $amount_summary[$multiproperty->id]['other_intended_users']['fee'] ?>
                                                                </td>
                                                                    <?php if ($model->quotation_status == 0 || $model->quotation_status == 9 || $model->quotation_status == 14 || $model->quotation_status == 17) { ?>                                                            
                                                                        <td > 
                                                                            <div>
                                                                            <input type="number"
                                                                            name="CrmQuotations[calculations][<?= $multiproperty->id ?>][recommended_fee]"
                                                                            value="<?= $multiproperty->recommended_fee ?>" style="width: 70px;" placeholder=""
                                                                            class="form-control" readonly=""  required>
                                                                            </div>
                                                                        </td>
                                                                        <td > 
                                                                            <div><input type="number" min="0"
                                                                            name="CrmQuotations[calculations][<?= $multiproperty->id ?>][bo_recommended_fee]"
                                                                            step='any' value="<?= $multiproperty->bo_recommended_fee ?>"
                                                                            placeholder="" class="form-control" style="width: 70px;" <?= $reommended_readonly ?> required></div>
                                                                        </td>
                                                                        <td > 
                                                                            <div><input type="number" min="0"
                                                                            name="CrmQuotations[calculations][<?= $multiproperty->id ?>][reviewed_fee]"
                                                                            step='any' value="<?= $multiproperty->reviewed_fee ?>"
                                                                            placeholder="" class="form-control" style="width: 70px;" <?= $quotation_readonly ?> required>
                                                                            </div>

                                                                        </td>
                                                                        <td > 
                                                                            <div><input type="number" min="0"
                                                                            name="CrmQuotations[calculations][<?= $multiproperty->id ?>][quotation_fee]"
                                                                            step='any' value="<?= $multiproperty->quotation_fee ?>"
                                                                            placeholder="" class="form-control" style="width: 70px;" <?= $quotation_readonly ?> required>
                                                                            </div>
                                                                        </td>
                                                                <?php } else {?>
                                                                        <td > 
                                                                            <div><strong><?= $multiproperty->recommended_fee ?></strong></div>
                                                                        </td>
                                                                        <td > 
                                                                            <div><strong><?= $multiproperty->bo_recommended_fee ?></strong></div>
                                                                        </td>
                                                                        <td > 
                                                                            <div><strong><?= $multiproperty->reviewed_fee ?></strong></div>
                                                                        </td>
                                                                        <td > 
                                                                            <div><strong><?= $multiproperty->quotation_fee ?></strong></div>
                                                                        </td>
                                                                <?php } ?>
                                                            </tr>
                                                            <?php 
                                                            $buildingTotalRecTat = $buildingTotalRecTat + $multiproperty->tat;
                                                            $buildingTotalBoRecTat = $buildingTotalBoRecTat + $multiproperty->bo_recommended_tat;
                                                            $buildingTotalRevTat = $buildingTotalRevTat + $multiproperty->reviewed_tat;
                                                            $buildingTotalAprvdTat = $buildingTotalAprvdTat + $multiproperty->aprvd_tat;

                                                            $buildingTotalRecFee = $buildingTotalRecFee + $multiproperty->recommended_fee;
                                                            $buildingTotalBoRecFee = $buildingTotalBoRecFee + $multiproperty->bo_recommended_fee;
                                                            $buildingTotalRevFee = $buildingTotalRevFee + $multiproperty->reviewed_fee;
                                                            $buildingTotalAprvdFee = $buildingTotalAprvdFee + $multiproperty->quotation_fee;
                                                            
                                                        }
                                                        ?>

                                                        <?php //if($model->no_of_properties > 1) { ?>
                                                        <?php if((count($multiScopeProperties) > 1) && (count($multiScopeProperty) > 1)) { ?>
                                                                <tr class="trBrdr">
                                                                    <td colspan="9" style="text-align:right;">
                                                                        <div><b>Asset <?= $keys+1 ?> TAT</b></div>
                                                                        <div><b>Asset <?= $keys+1 ?> Fee</b></div>
                                                                    
                                                                    </td>
                                                                    <td>
                                                                        <div><?= $buildingTotalRecTat; ?></div>
                                                                        <div><b><?= $buildingTotalRecFee; ?></b></div>
                                                                    </td>
                                                                    <td>
                                                                        <div><?= ($buildingTotalBoRecTat !== 0 ) ? $buildingTotalBoRecTat : $buildingTotalRecTat ; ?></div>
                                                                        <div><b><?= ($buildingTotalBoRecFee !== 0 ) ? $buildingTotalBoRecFee : $buildingTotalRecFee ; ?></b></div>
                                                                    </td>
                                                                    <td>
                                                                        <div><?= ($buildingTotalRevTat !== 0 ) ? $buildingTotalRevTat : $buildingTotalRecTat ; ?></div>
                                                                        <div><b><?= ($buildingTotalRevFee !== 0 ) ? $buildingTotalRevFee : $buildingTotalRecFee ; ?></b></div>
                                                                    </td>
                                                                    <td>
                                                                        <div><?= ($buildingTotalAprvdTat !== 0 ) ? $buildingTotalAprvdTat : $buildingTotalRecTat ; ?></div>
                                                                        <div><b><?= ($buildingTotalAprvdFee !== 0 ) ? $buildingTotalAprvdFee : $buildingTotalRecFee ; ?></b></div>
                                                                    </td>
                                                                </tr>
                                                        <?php
                                                            }
                                                    }
                                                }
                                                ?>
                                                <tr class="trBrdr">
                                                    <td colspan="9" style="text-align:right;">
                                                        <div><b>Total TAT</b></div>
                                                        <div><b>Total Fee</b></div>
                                                    
                                                    </td>
                                                   
                                                    <td>
                                                        <div><?= $quotation_tat_total ?></div>
                                                        <div><strong><?= $recommended_fee ?></strong></div>
                                                    </td>
                                                    <td> 
                                                        <!-- <input type="text" name="CrmQuotations[final_fee_bo_recommended]"
                                                            value="<?= $bo_recommended_fee_total   ?>"
                                                            placeholder="" class="form-control"
                                                            id="final_fee_bo_recommended"> -->
                                                            <div><?= ($bo_recommended_tat_total <> null) ? $bo_recommended_tat_total : $quotation_tat_total ?></div>
                                                            <div><strong><?= $bo_recommended_fee_total ?></strong></div>
                                                    </td>
                                                    <td>
                                                        <!-- <input type="text" name="CrmQuotations[final_fee_reviewed]"
                                                            value="<?= $reviewed_fee_total  ?>"
                                                            placeholder="" class="form-control"
                                                            id="final_fee_reviewed"> -->
                                                            <div><?= ($reviewed_tat_total <> null) ? $reviewed_tat_total : $quotation_tat_total ?></div>
                                                            <div><strong><?= $reviewed_fee_total ?></strong></div>
                                                    </td>
                                                    <td>
                                                        <!-- <input type="text" name="CrmQuotations[final_fee_approved]"
                                                            value="<?= $quotation_fee_total ?>"
                                                            placeholder="" class="form-control"
                                                            id="final_fee_approved"> -->
                                                    
                                                            <div><?= ($quotation_aprvd_tat_total <> null) ? $quotation_aprvd_tat_total : $quotation_tat_total ?></div>
                                                            <div><strong><?= $quotation_fee_total ?></strong></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="13"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="13">
                                                        <div class="col-sm-6">
                                                            <?php if ($model->quotation_status == 0 || $model->quotation_status == 9 || $model->quotation_status == 14 || $model->quotation_status == 17) { $disabled = false; } else { $disabled = true;} ?>
                                                            <?= $form->field($model, 'relative_discount')->widget(Select2::classname(), [
                                                                'data' => yii::$app->quotationHelperFunctions->relativediscount,
                                                                'options' => ['placeholder' => 'Select a Relationship Discount ...'],
                                                                'pluginOptions' => [
                                                                    'allowClear' => true,
                                                                    'disabled' => $disabled
                                                                ],
                                                            ])->label('Relationship Discount (%)'); ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php 
                                                    if ($discount > 0) { ?>
                                                        <tr>
                                                            <td colspan="9"><b>Relationship Discount</b></td>

                                                            <?php // maxima recommended
                                                            if ($discount > 0) {
                                                                if ($model->relative_discount == 'base-fee') { ?>
                                                                    <td class="bill-data" colspan="2">Base Fee Discount</td>
                                                                    <td class="bill-data" colspan="1">
                                                                        <?= number_format($discount, 2) ?>
                                                                    </td>
                                                                    <?php
                                                                } else { ?>

                                                                    <td class="bill-data" colspan="2">(-)
                                                                        <?= $model->relative_discount ?>% Discount
                                                                    </td>
                                                                    <td class="bill-data" colspan="1">
                                                                        <?= number_format($discount, 2) ?>
                                                                    </td>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <?php
                                                            } else { ?>
                                                                <td class="bill-data" colspan="2">No Discount</td>
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>

                                                            <?php // bo recommended
                                                            if ($discount > 0) {
                                                                if ($model->relative_discount == 'base-fee') { ?>
                                                            
                                                                    <td class="bill-data" colspan="1">
                                                                        <?= number_format($bo_rec_discount, 2) ?>
                                                                    </td>
                                                                    <?php
                                                                } else { ?>

                                                            
                                                                    <td class="bill-data" colspan="1">
                                                                        <?= number_format($bo_rec_discount, 2) ?>
                                                                    </td>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <?php
                                                            } else { ?>
                                                    
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>

                                                            <?php // reviewed
                                                            if ($discount > 0) {
                                                                if ($model->relative_discount == 'base-fee') { ?>
                                                            
                                                                    <td class="bill-data" colspan="1">
                                                                        <?= number_format($rev_discount, 2) ?>
                                                                    </td>
                                                                    <?php
                                                                } else { ?>
                                                                    <td class="bill-data" colspan="1">
                                                                        <?= number_format($rev_discount, 2) ?>
                                                                    </td>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <?php
                                                            } else { ?>
                                                    
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>
                                                            
                                                            
                                                            <?php // approved
                                                            if ($discount > 0) {
                                                                if ($model->relative_discount == 'base-fee') { ?>
                                                                    <!-- <td class="bill-data" colspan="2">Base Fee Discount</td> -->
                                                                    <td class="bill-data" colspan="1">
                                                                        <?= number_format($aprvd_discount, 2) ?>
                                                                    </td>
                                                                    <?php
                                                                } else { ?>
                                                                    <!-- <td class="bill-data" colspan="2">(-)
                                                                        <?= $model->relative_discount ?>% Discount
                                                                        </td> -->
                                                                    <td class="bill-data" colspan="1">
                                                                        <?= number_format($aprvd_discount, 2) ?>
                                                                    </td>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <?php
                                                            } else { ?>
                                                                <!-- <td class="bill-data" colspan="1">No Discount</td> -->
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>
                                                                    <!-- <td colspan="2"> </td> -->
                                                                    <!-- <td> </td> -->

                                                        </tr>
                                                        <?php 
                                                    } ?>
                                                
                                                

                                                    <?php
                                                    if ($no_of_property_discount_data['amount'] > 0) 
                                                    {
                                                        ?>
                                                        <tr>
                                                            <td colspan="7"><b>Multiple Assets Discount</b></td>
                                                                <?php // maxima recommended
                                                                if ($no_of_property_discount_data['amount'] > 0) {
                                                                    ?>
                                                                    <td class="bill-data" colspan="2">(-)
                                                                        <?= $no_of_property_discount_data['value'] ?>% Discount
                                                                    </td>
                                                                    <td class="bill-data" colspan="1">
                                                                        <?= number_format($no_of_property_discount_data['amount'], 2) ?>
                                                                    </td>

                                                                    <?php
                                                                } else { ?>
                                                                    <td class="bill-data" colspan="2">No Discount</td>
                                                                    <td class="bill-data" colspan="1">0</td>
                                                                    <?php
                                                                }
                                                                ?>
                                                           

                                                                <?php // bo recommended
                                                                if ($no_of_property_discount_data['amount'] > 0) {
                                                                    ?>
                                                                    <td class="bill-data" colspan="1">
                                                                        <div id="aprvd_no_of_porperty_discount">
                                                                            <?= number_format($bo_recommended_no_of_property_discount_data['amount'], 2) ?>
                                                                        </div>
                                                                    </td>

                                                                    <?php
                                                                } else { ?>
                                                                    <td class="bill-data" colspan="1">0</td>
                                                                    <?php
                                                                }
                                                                ?>

                                                                <?php // reviewed
                                                                if ($no_of_property_discount_data['amount'] > 0) {
                                                                    ?>
                                                                    <td class="bill-data" colspan="1">
                                                                        <div id="aprvd_no_of_porperty_discount">
                                                                            <?= number_format($reviewed_no_of_property_discount_data['amount'], 2) ?>
                                                                        </div>
                                                                    </td>

                                                                    <?php
                                                                } else { ?>
                                                                    <td class="bill-data" colspan="1">0</td>
                                                                    <?php
                                                                }
                                                                ?>

                                                                <?php // approved
                                                                if ($no_of_property_discount_data['amount'] > 0) {
                                                                    ?>
                                                                    <!-- <td class="bill-data" colspan="2">
                                                                    <input type="text" name="CrmQuotations[aprvd_no_of_property_discount]" value="<?= ($model->aprvd_no_of_property_discount <> null) ?  $model->aprvd_no_of_property_discount : $no_of_property_discount_data['value']; ?>" placeholder="" class="form-control" id="aprvd_no_of_property_discount"> % Discount
                                                                    </td> -->
                                                                    <td class="bill-data" colspan="1">
                                                                        <div id="aprvd_no_of_porperty_discount">
                                                                        <?=
                                                                         number_format(($model->aprvd_no_of_property_discount_amount <> null) ? $model->aprvd_no_of_property_discount_amount : $no_of_property_discount_data['amount'], 2) ?>
                                                                        <!-- <input type="text" name="CrmQuotations[arpvd_no_of_property_discount_amount]"  value="<?= ($model->aprvd_no_of_property_discount_amount <> null) ? $model->aprvd_no_of_property_discount_amount : $no_of_property_discount_data['amount'] ?>"
                                                                        placeholder="" class="form-control" readonly="" id="aprvd_no_of_property_discount_amount"> -->
                                                                        </div>
                                                                    </td>

                                                                    <?php
                                                                } else { ?>
                                                                    <!-- <td class="bill-data" colspan="2">No Discount</td> -->
                                                                    <td class="bill-data" colspan="1">0</td>
                                                                    <?php
                                                                }
                                                                ?>
                                                                    
                                                                </tr>
                                                        <?php 
                                                    } ?>

                                                    <?php
                                                    if ($same_building_discount_data['amount'] > 0) 
                                                    {
                                                        ?>
                                                        <tr>
                                                            <td colspan="7"><b>Number Of Units In The Same Building Discount</b></td>

                                                            <?php // maxima recommended
                                                            if ($same_building_discount_data['amount'] > 0) {
                                                                ?>
                                                                        <td class="bill-data" colspan="2">(-)
                                                                            <?= $same_building_discount_data['value'] ?>% Discount
                                                                        </td>
                                                                        <td class="bill-data" colspan="1">
                                                                            <?= number_format($same_building_discount_data['amount'], 2) ?>
                                                                        </td>

                                                                        <?php
                                                            } else { ?>
                                                                        <td class="bill-data" colspan="2">No Discount</td>
                                                                        <td class="bill-data" colspan="1">0</td>
                                                                        <?php
                                                            }
                                                            ?>

                                                            <?php // bo recommended
                                                            if ($same_building_discount_data['amount'] > 0) {
                                                                ?>
                                                                        <td class="bill-data" colspan="1">
                                                                            <div id="aprvd_no_of_porperty_discount">
                                                                            <?= number_format($bo_recommended_same_building_discount_data['amount'], 2) ?>
                                                                            </div>
                                                                        </td>

                                                                        <?php
                                                            } else { ?>
                                                                        <td class="bill-data" colspan="1">0</td>
                                                                        <?php
                                                            }
                                                            ?>

                                                            <?php // reviewed
                                                            if ($same_building_discount_data['amount'] > 0) {
                                                                ?>
                                                                        <td class="bill-data" colspan="1">
                                                                            <div id="aprvd_no_of_porperty_discount">
                                                                            <?= number_format($reviewed_same_building_discount_data['amount'], 2) ?>
                                                                            </div>
                                                                        </td>

                                                                        <?php
                                                            } else { ?>
                                                                        <td class="bill-data" colspan="1">0</td>
                                                                        <?php
                                                            }
                                                            ?>
                                                                    
                                                                    
                                                            <?php // approved
                                                            if ($same_building_discount_data['amount'] > 0) {
                                                                ?>
                                                                        <!-- <td class="bill-data" colspan="2">
                                                                            <input type="text"
                                                                                name="CrmQuotations[aprvd_same_building_discount]"
                                                                                value="<?= ($model->aprvd_same_building_discount <> null) ? $model->aprvd_same_building_discount : $same_building_discount_data['value']; ?>"
                                                                                placeholder="" class="form-control"
                                                                                id="aprvd_same_building_discount">
                                                                            % Discount
                                                                        </td> -->
                                                                        <td class="bill-data" colspan="1">
                                                                            <div id="aprvd_no_of_porperty_discount">
                                                                            <?= number_format(($model->aprvd_same_building_discount_amount <> null) ? $model->aprvd_same_building_discount_amount : $same_building_discount_data['amount'], 2) ?>

                                                                                <!-- <input type="text"
                                                                                    name="CrmQuotations[aprvd_same_building_discount_amount]"
                                                                                    value="<?= ($model->aprvd_same_building_discount_amount <> null) ? $model->aprvd_same_building_discount_amount : $same_building_discount_data['amount']; ?>"
                                                                                    placeholder="" class="form-control" readonly=""
                                                                                    id="aprvd_same_building_discount_amount"> -->

                                                                            </div>
                                                                        </td>

                                                                        <?php
                                                            } else { ?>
                                                                        <!-- <td class="bill-data" colspan="2">No Discount</td> -->
                                                                        <td class="bill-data" colspan="1">0</td>
                                                                        <?php
                                                            }
                                                            ?>
                                                                    
                                                        </tr>
                                                        <?php 
                                                    } ?>


                                                    <?php
                                                    if ($first_time_fee_discount_data['amount'] > 0) 
                                                    {
                                                        ?>
                                                        <tr>
                                                            <td colspan="7"><b>First Time Discount</b></td>

                                                            <?php // maxima recommended
                                                            if ($first_time_fee_discount_data['amount'] > 0) {
                                                                ?>
                                                                <td class="bill-data" colspan="2">(-)
                                                                    <?= $first_time_fee_discount_data['value'] ?>% Discount
                                                                </td>
                                                                <td class="bill-data" colspan="1">
                                                                    <?= number_format($first_time_fee_discount_data['amount'], 2) ?>
                                                                </td>
                                                                <?php
                                                            } else { ?>
                                                                <td class="bill-data" colspan="2">No Discount</td>
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>
                                                                    
                                                            <?php // bo recommended
                                                            if ($first_time_fee_discount_data['amount'] > 0) {
                                                                ?>
                                                                <td class="bill-data" colspan="1">
                                                                    <?= number_format($bo_recommended_first_time_fee_discount_data['amount'], 2) ?>
                                                                </td>
                                                                <?php
                                                            } else { ?>
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>
                                                            <?php // reviewed
                                                            if ($first_time_fee_discount_data['amount'] > 0) {
                                                                ?>
                                                                <td class="bill-data" colspan="1">
                                                                    <?= number_format($reviewed_first_time_fee_discount_data['amount'], 2) ?>
                                                                </td>
                                                                <?php
                                                            } else { ?>
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>

                                                            <?php // approved
                                                            if ($first_time_fee_discount_data['amount'] > 0) {
                                                                ?>
                                                                <!-- <td class="bill-data" colspan="2">
                                                                    <input type="text"
                                                                        name="CrmQuotations[aprvd_first_time_discount]"
                                                                        value="<?= ($model->aprvd_first_time_discount <> null) ? $model->aprvd_first_time_discount : $first_time_fee_discount_data['value']; ?>"
                                                                        placeholder="" class="form-control"
                                                                        id="aprvd_first_time_discount">
                                                                    % Discount
                                                                </td> -->
                                                                <td class="bill-data" colspan="1">
                                                                    <?= ($model->aprvd_first_time_discount_amount <> null) ? $model->aprvd_first_time_discount_amount : $first_time_fee_discount_data['amount']; ?>
                                                                </td>
                                                                <?php
                                                            } else { ?>
                                                                <!-- <td class="bill-data" colspan="2">No Discount</td> -->
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tr>
                                                        <?php 
                                                    } ?>

                                                    <?php
                                                    if ($total_discount_amount > 0) 
                                                    {
                                                        ?>
                                                        <tr>
                                                            <td colspan="7"><b>Total Discount</b></td>


                                                            <?php // maxima recommended
                                                            if ($total_discount_amount > 0) {
                                                                ?>
                                                                <td class="bill-data" colspan="2">
                                                                    <?= $total_discount ?>% Discount
                                                                </td>
                                                                <td class="bill-data" colspan="1">
                                                                    <?= number_format($total_discount_amount, 2) ?>
                                                                </td>

                                                                <?php
                                                            } else { ?>
                                                                <td class="bill-data" colspan="2">No Discount</td>
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>
                                                                   
                                                                   
                                                            <?php // bo recommended
                                                            if ($total_discount_amount > 0) {
                                                                ?>
                                                                <td class="bill-data" colspan="1">
                                                                    <?= number_format($bo_rec_total_discount_amount, 2) ?>
                                                                </td>

                                                                <?php
                                                            } else { ?>
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>

                                                            <?php // reviewed
                                                            if ($total_discount_amount > 0) {
                                                                ?>
                                                                <td class="bill-data" colspan="1">
                                                                    <?= number_format($rev_total_discount_amount, 2) ?>
                                                                </td>

                                                                <?php
                                                            } else { ?>
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>

                                                            <?php // approved
                                                            if ($total_discount_amount > 0) {
                                                                ?>
                                                                <!-- <td class="bill-data" colspan="2">
                                                                    <?= ($aprvd_total_discount <> null) ? $aprvd_total_discount :
                                                                        $total_discount ?>% Discount
                                                                </td> -->
                                                                <td class="bill-data" colspan="1">
                                                                    <?= ($aprvd_total_discount_amount <> null) ?
                                                                        number_format($aprvd_total_discount_amount, 2) :
                                                                        number_format($total_discount_amount, 2)
                                                                        ?>
                                                                </td>

                                                                <?php
                                                            } else { ?>
                                                                <!-- <td class="bill-data" colspan="2">No Discount</td> -->
                                                                <td class="bill-data" colspan="1">0</td>
                                                                <?php
                                                            }
                                                            ?>
                                                                    
                                                        </tr>
                                                        <?php 
                                                    } ?>



                                                <tr>
                                                    <td colspan="9"><b>Total Valuation Fee</b></td>
                                                    <td colspan="1"> <?= number_format($discount_net_fee, 2) ?> </td>
                                                    <td colspan="1"> <?= number_format($bo_rec_discount_net_fee, 2) ?> </td>
                                                    <td colspan="1"> <?= number_format($rev_discount_net_fee, 2) ?> </td>
                                                    <td colspan="1"> <?= number_format($aprvd_discount_net_fee, 2) ?> </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="13"></td>

                                                </tr>

                                                <?php
                                                    if ($urgencyfee_data['amount'] > 0) 
                                                    {
                                                        ?>
                                                        <tr>
                                                            <td colspan="9"><b>Urgency Fee</b></td>


                                                            <?php // maxima recommended
                                                            if ($urgencyfee_data['amount'] > 0) {
                                                                ?>
                                                                <td class="bill-data" colspan="2">(+)
                                                                    <?= $urgencyfee_data['value'] ?>% Fee
                                                                </td>
                                                                <td class="bill-data" colspan="1">
                                                                    <?= number_format($recommended_urgencyfee_data['amount'], 2) ?>
                                                                </td>

                                                                <?php
                                                            } else { ?>
                                                                <td class="bill-data" colspan="2"></td>
                                                                <td class="bill-data" colspan="1"></td>
                                                                <?php
                                                            }
                                                            ?>

                                                            <?php // bo recommended
                                                            if ($urgencyfee_data['amount'] > 0) {
                                                                ?>
                                                                <td class="bill-data" colspan="1">
                                                                    <?= number_format($bo_recommended_urgencyfee_data['amount'], 2) ?>
                                                                </td>

                                                                        <?php
                                                            } else { ?>
                                                                <td class="bill-data" colspan="1"></td>
                                                                <?php
                                                            }
                                                            ?>

                                                            <?php // reviewed
                                                            if ($urgencyfee_data['amount'] > 0) {
                                                                ?>
                                                                <td class="bill-data" colspan="1">
                                                                    <?= number_format($reviewed_urgencyfee_data['amount'], 2) ?>
                                                                </td>

                                                                        <?php
                                                            } else { ?>
                                                                <td class="bill-data" colspan="1"></td>
                                                                <?php
                                                            }
                                                            ?>

                                                            <?php // approved
                                                            if ($urgencyfee_data['amount'] > 0) {
                                                                ?>
                                                                <!-- <td class="bill-data" colspan="2">(+)
                                                                    <?= $urgencyfee_data['value'] ?>% Fee
                                                                </td> -->
                                                                <td class="bill-data" colspan="1">
                                                                    <?=
                                                                        ($aprvd_urgencyfee_data <> null) ?
                                                                        number_format($aprvd_urgencyfee_data['amount'], 2) :
                                                                        number_format($urgencyfee_data['amount'], 2);
                                                                    ?>
                                                                </td>

                                                                <?php
                                                            } else { ?>
                                                                <!-- <td class="bill-data" colspan="2"></td> -->
                                                                <td class="bill-data" colspan="1"></td>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tr>
                                                        <?php
                                                    } ?>
                                                <?php
                                                    if ($advance_payment_terms_data['amount'] > 0) 
                                                    {
                                                        ?>
                                                        <tr>
                                                            <td colspan="9"><b>Advance Payment Fee</b></td>

                                                            <?php // maxima recommended
                                                            if ($advance_payment_terms_data['amount'] > 0) {
                                                                ?>
                                                                        <td class="bill-data" colspan="2">(+)
                                                                            <?= $advance_payment_terms_data['value'] ?>% Fee
                                                                        </td>
                                                                        <td class="bill-data" colspan="1">
                                                                            <?= number_format($advance_payment_terms_data['amount'], 2) ?>
                                                                        </td>

                                                                        <?php
                                                            } else { ?>
                                                                        <td class="bill-data" colspan="2"></td>
                                                                        <td class="bill-data" colspan="1"></td>
                                                                        <?php
                                                            }
                                                            ?>
                                                                
                                                            <?php // bo recommended
                                                            if ($advance_payment_terms_data['amount'] > 0) {
                                                                ?>
                                                                        <td class="bill-data" colspan="1">
                                                                            <?= number_format($advance_payment_terms_data['amount'], 2) ?>
                                                                        </td>

                                                                        <?php
                                                            } else { ?>
                                                                        <td class="bill-data" colspan="1"></td>
                                                                        <?php
                                                            }
                                                            ?>
                                                            <?php // reviewed
                                                            if ($advance_payment_terms_data['amount'] > 0) {
                                                                ?>
                                                                        <td class="bill-data" colspan="1">
                                                                            <?= number_format($advance_payment_terms_data['amount'], 2) ?>
                                                                        </td>

                                                                        <?php
                                                            } else { ?>
                                                                        <td class="bill-data" colspan="1"></td>
                                                                        <?php
                                                            }
                                                            ?>
                                                            <?php // approved
                                                            if ($advance_payment_terms_data['amount'] > 0) {
                                                                ?>
                                                                        <!-- <td class="bill-data" colspan="2">(+)
                                                                            <?= $advance_payment_terms_data['value'] ?>% Fee
                                                                        </td> -->
                                                                        <td class="bill-data" colspan="1">
                                                                            <?= number_format($advance_payment_terms_data['amount'], 2) ?>
                                                                        </td>

                                                                        <?php
                                                            } else { ?>
                                                                        <!-- <td class="bill-data" colspan="2"></td> -->
                                                                        <td class="bill-data" colspan="1"></td>
                                                                        <?php
                                                            }
                                                            ?>

                                                        </tr>
                                                        <?php 
                                                    } ?>
                                                    <tr>
                                                        <td colspan="9"><b>Net Valuation Fee</b></td>
                                                        <td colspan="1">
                                                            <?= number_format($netValuationFee, 2) ?>
                                                        </td>                                                       
                                                        <td colspan="1">
                                                            <?= number_format($bo_rec_netValuationFee, 2) ?>
                                                        </td>
                                                        <td colspan="1">
                                                            <?= number_format($rev_netValuationFee, 2) ?>
                                                        </td>
                                                        <td colspan="1">
                                                            <?= number_format($aprvd_netValuationFee, 2) ?>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td colspan="7"><b>Vat</b></td>

                                                        <td colspan="2">
                                                            <?= ($VAT <> null && $VAT > 0) ? '5%' : 'No VAT' ?>
                                                        </td>
                                                        <td colspan="1">
                                                            <?= ($VAT <> null && $VAT > 0) ? number_format($VAT, 2) : 0 ?>
                                                        </td>                                                        
                                                        <td colspan="1">
                                                            <?= ($bo_rec_VAT <> null && $bo_rec_VAT > 0) ? number_format($bo_rec_VAT, 2) : 0 ?>
                                                        </td>
                                                        <td colspan="1">
                                                            <?= ($rev_VAT <> null && $rev_VAT > 0) ? number_format($rev_VAT, 2) : 0 ?>
                                                        </td>
                                                        <td colspan="1">
                                                            <?= ($aprvd_VAT <> null && $aprvd_VAT > 0) ? number_format($aprvd_VAT, 2) : 0 ?>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td colspan="9"><b>Final Fee Payable</b></td>
                                                        <td colspan="1">
                                                            <strong><?= number_format($finalFeePayable) ?></strong>
                                                        </td>
                                                        <td colspan="1">
                                                            <strong><?= number_format($bo_rec_finalFeePayable) ?></strong>
                                                        </td>
                                                        <td colspan="1">
                                                            <strong><?= number_format($rev_finalFeePayable) ?></strong>
                                                        </td>
                                                        <td colspan="1">
                                                            <strong><?= number_format($aprvd_finalFeePayable) ?></strong>
                                                        </td>
                                                    </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    
                                    


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php
                                            // Get the current user's ID
                                            $currentUserId = Yii::$app->user->id;

                                            // Define the full data array
                                            $allStatuses = array(
                                                'Recommended' => 'Quotation Recommended',
                                                'Pending' => 'Quotation Pending',
                                                'Approve' => 'Quotation Verified',
                                                'client_accepted' => 'Quotation Approved',
                                                'Reject' => 'Quotation Reject',
                                                'toe_Pending' => 'TOE Pending',
                                                'toe_verified' => 'TOE Verified',
                                                'toe_Accept' => 'TOE Accepted',
                                                'toe_Reject' => 'TOE Rejected',
                                                'on-hold' => 'Quotation On Hold',
                                                'cancelled' => 'Quotation Cancelled',
                                                'regretted' => 'Quotation Regretted',
                                            );

                                            // Define the restricted data array for specific users
                                            $restrictedStatuses = array(
                                                'toe_verified' => 'TOE Verified',
                                            );

                                            // Determine which statuses to show
                                            if (in_array($currentUserId, [111558, 21])) {
                                                $statusesToShow = $restrictedStatuses;
                                            } else {
                                                $statusesToShow = $allStatuses;
                                            }

                                            echo $form->field($model, 'status_approve')->widget(Select2::classname(), [
                                                'data' => $statusesToShow,
                                                'options' => ['placeholder' => 'Select Status ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Status Approve');
                                            ?>
                                        </div>


                                        <div class="col-sm-6">
                                            <?php
                                            echo $form->field($model, 'quotation_cancel_reason')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(\app\models\QuotationCancelReasons::find()->orderBy([
                                                    'title' => SORT_ASC,
                                                ])->all(), 'id', 'title'),
                                                'options' => ['placeholder' => 'Select Cancle Reason ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Quotation Cancel Reason');
                                            ?>
                                        </div>
                                    </div>



                            </div>
                            <div class="card-footer">
                                <?php /* <?=  Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success mr-2']) ?> */?>

                                <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success', 'onClick' => "document.getElementById('w0').action = 'crm-quotations/step_8?id=' + {$model->id}; return true;"]); ?>

                                <?php
                                if (Yii::$app->user->identity->permission_group_id == 1 || Yii::$app->user->identity->permission_group_id == 17 || Yii::$app->user->identity->permission_group_id == 44 || Yii::$app->user->identity->permission_group_id == 4) {
                                    if ($model->quotation_status == 0 || $model->quotation_status == 14) {
                                        ?>  
                                                    <?= Html::submitButton('<i class="fa fa-hand-point-right"></i> Recommend Quotation', ['class' => 'btn btn-info', 'onClick' => "document.getElementById('w0').action = 'crm-quotations/step_8?id={$model->id}&quotation_status=17'; "]); ?>
                                    <?php }
                                } ?>

                                <?php
                                if (Yii::$app->user->identity->permission_group_id == 1 || Yii::$app->user->identity->permission_group_id == 17 || Yii::$app->user->identity->permission_group_id == 32 || Yii::$app->user->identity->permission_group_id == 2 || Yii::$app->user->identity->permission_group_id == 35 || Yii::$app->user->identity->id == 53) {
                                    if ($model->quotation_status == 0 || $model->quotation_status == 9 || $model->quotation_status == 17) {
                                        ?>          
                                                    <?= Html::submitButton('<i class="fa fa-thumbs-up"></i> Verify Quotation', ['class' => 'btn btn-primary', 'onClick' => "document.getElementById('w0').action = 'crm-quotations/step_8?id={$model->id}&quotation_status=2'; ", 'disabled']); ?>
                                    <?php }
                                } ?>

                                <!--                                <a href="<? /*= Url::toRoute(['crm-quotations/send-quotation?id='.$model->id]) */?>"><button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Send Quotation</button></a>

                                <a href="<? /*= Url::toRoute(['crm-quotations/qpdf?id='.$model->id]) */?>"><button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Download Quotation</button></a>-->



                                <!-- <a href="javascript:;" data-id="<?= $model->id ?>" class=" check-property-verify"
                                   data-title="Sending TOE"
                                   data-url="<?= Url::toRoute(['crm-quotations/send-toe?id=' . $model->id]) ?>">
                                    <button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Send
                                        TOE
                                    </button>
                                </a> -->
                                <a href="javascript:;" data-id="<?= $model->id ?>" class=" check-property-verify"
                                    data-title="Downloading TOE"
                                    data-url="<?= Url::toRoute(['crm-quotations/toe?id=' . $model->id]) ?>">
                                    <button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Download
                                        TOE
                                    </button>
                                </a>

                                <!-- <a href="javascript:;" data-id="<?= $model->id ?>" class=" check-property-verify"
                                   data-title = "Sending Quotation"
                                   data-url="<?= Url::toRoute(['crm-quotations/send-quotation?id=' . $model->id]) ?>">
                                    <button type="button"
                                            class="btn btn-info btn-sm my-2 mx-1 float-right">Send
                                        Quotation</button>
                                </a>                                 -->
                                <a href="javascript:;" data-id="<?= $model->id ?>" class=" check-property-verify"
                                    data-title="Downloading Quotation"
                                    data-url="<?= Url::toRoute(['crm-quotations/qpdf?id=' . $model->id]) ?>">
                                    <button type="button" class="btn btn-info btn-sm my-2 mx-1 float-right">Download
                                        Quotation</button>
                                </a>




                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
    $(document).ready(function () {
        $('#togglePanel').click(function () {
            $('.left-panel').toggleClass('hide-left-panel');
            $('.right-panel').toggleClass('full-right-panel');
            $(this).find('i').toggleClass('fa-arrow-left fa-bars');
        });
    });
</script>