<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use kartik\depdrop\DepDrop;
use app\assets\ValuationFormAsset;

ValuationFormAsset::register($this);
ListingsFormAsset::register($this);

use app\components\widgets\StatusVerified;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */


$entity = ($quotation->type_of_service == 2) ? "Asset" : "Property";

$this->title = Yii::t('app', 'Enter ' . $entity . ' Details');
$cardTitle = Yii::t('app', 'Ref No:  {nameAttribute}', [
    'nameAttribute' => $quotation->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['crm-quotations/step_1?id=' . $quotation->id . '&']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');



$multiscope_id = '';
for ($i = 0; $i < 10; $i++) {
    $multiscope_id .= rand(0, 9);
}



$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

$("#crmreceivedproperties-asset_category").autocomplete({
    serviceUrl: "' . Url::to(['suggestion/assetategories']) . '",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#crmreceivedproperties-asset_category").val()!=suggestion.value){
        $("#crmreceivedproperties-asset_category").val(suggestion.value);
      }
    },
    onInvalidateSelection: function() {
    //   $("#crmreceivedproperties-asset_category").val("");
    }
  });

  

');
?>



<style>
    .datepicker-days .table>thead>tr>th,
    .table>tbody>tr>th,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>tbody>tr>td,
    .table>tfoot>tr>td {
        padding: inherit !important;
    }

    .help-block2 {
        color: red;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -14px;
        font-family: inherit;
    }

    .autocomplete-suggestion {
        padding: 5px 5px !important;
    }
</style>
<style>
    /* .profit {
        display: none;
    }

    .income {
        display: none;
    } */

    .income-profit-cost {
        display: none;
    }

    .income-profit {
        display: none;
    }
</style>
<?php if ($model->valuation_approach == 1) { ?>
    <style>
        /* .profit-income {
                display: none;
            }

            .only-profit {
                display: none;
            } */

        /* .profit{
            display: none;
            }
            .income{
            display: none;
            } */
    </style>
<?php } ?>
<?php if ($model->valuation_approach == 2) { ?>
    <style>
        /* .only-profit {
                display: none;
            } */

        /* .profit{
            display: none;
            } */
    </style>
<?php } ?>
<?php if ($model->valuation_approach == 3) { ?>
    <style>
        /* .income{
            display: none;
            } */
    </style>
<?php } ?>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title float-none text-md-right ">
            <i class="fas fa-edit"></i>
            <?= $cardTitle ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../../left-nav', ['model' => $model, 'step' => 1, 'quotation' => $quotation, 'property_index' => $property_index]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form ">


                            <?php $form = ActiveForm::begin(); ?>
                            <?php echo $form->field($model, 'multiscope_id')->hiddenInput(['maxlength' => true, 'value' => ($model->multiscope_id <> null) ? $model->multiscope_id : $multiscope_id])->label(false) ?>
                            <div class="repeat-section">



                                <div class="card card-outline card-primary mb-3" id="property_details">
                                    <header class="card-header">
                                        <h2 class="card-title">Asset Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">


                                            <div class="col-sm-4 asset_category">
                                                <!-- <?= $form->field($model, 'asset_category')->textInput(['maxlength' => true, 'spellcheck' => 'true', 'autocomplete' => 'off'])->label('Asset Category'); ?> -->
                                                <?= $form->field($model, 'asset_category')->textInput(['maxlength' => true, 'spellcheck' => 'true', 'autocomplete' => 'off'])->label('Asset Category')->textInput(['value' => $model->assetCategory->title]) ?>

                                            </div>

                                            <div class="col-sm-4 no_of_asset">
                                                <?php
                                                echo $form->field($model, 'no_of_asset')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->getCrmOptionOneToNumber(2000),
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                ])->label('Number Of Asset Quantity');
                                                ?>
                                            </div>

                                            <div class="col-sm-4 asset_complexity">
                                                <?= $form->field($model, 'asset_complexity')->widget(
                                                    Select2::classname(),
                                                    [
                                                        'data' => yii::$app->crmQuotationHelperFunctions->assetComplexity,
                                                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]
                                                )->label('Complexity'); ?>
                                            </div>



                                            <div class="col-sm-4 asset_age">
                                                <?php
                                                echo $form->field($model, 'asset_age')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->getCrmOptionOneToNumber(200),
                                                    'options' => ['class' => 'form-control'],
                                                ])->label('Age');
                                                ?>
                                            </div>

                                            <div class="col-sm-4 country_of_manufacturing">
                                                <?php
                                                echo $form->field($model, 'country_of_manufacturing')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->countryListArr,
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                ])->label('Country of Manufacturing');
                                                ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="card card-outline card-primary mb-3" id="valuation_details">
                                    <header class="card-header">
                                        <h2 class="card-title">Assignment Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">


                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'valuation_approach')->dropDownList(Yii::$app->appHelperFunctions->valuationApproachListArr)->label('Valuation Approach <span class="text-danger">*</span>'); ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'repeat_valuation')->widget(Select2::classname(), [
                                                    'data' => yii::$app->quotationHelperFunctions->getNewRepeatValuationAssignment(),
                                                    'options' => ['class' => 'repeat_valuation'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Repeat Valuation/Assignment'); ?>
                                            </div>

                                            <div class="col-sm-4 working_days">
                                                <?php
                                                echo $form->field($model, 'working_days')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->getCrmOptionOneToNumber(200),
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                ])->label('Working Days Required <span class="text-danger">*</span>');
                                                ?>
                                            </div>

                                            <div class="col-sm-4 no_of_comparables_value">
                                                <?php
                                                echo $form->field($model, 'no_of_comparables_value')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->getCrmOptionZeroToNumber(200),
                                                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                                                ])->label('Number Of Comparables <span class="text-danger">*</span>');
                                                ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?php
                                                $preferredIds = [1, 2, 3, 4, 5]; // Replace with your desired scope ids

                                                // Filter the data array to include only items with preferred IDs
                                                $scopeOfWorkList = array_filter(Yii::$app->appHelperFunctions->ScopeOfWorkList, function ($key) use ($preferredIds) {
                                                    return in_array($key, $preferredIds);
                                                }, ARRAY_FILTER_USE_KEY);

                                                $model->scope_of_service = explode(',', $model->scope_of_service);
                                                echo $form->field($model, 'scope_of_service')->widget(Select2::classname(), [
                                                    'data' => $scopeOfWorkList,
                                                    'options' => ['placeholder' => 'Select Scope Of Service ...'],
                                                    'pluginOptions' => [
                                                        'placeholder' => 'Scope Of Service',
                                                        'multiple' => true,
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                                ?>
                                            </div>


                                        </div>
                                    </div>
                                </div>



                                <?php
                                if (Yii::$app->menuHelperFunction->checkActionAllowed('Verified')) {
                                    echo StatusVerified::widget(['model' => $model, 'form' => $form]);
                                }
                                ?>
                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                                <?php
                                if($model<>null && $model->id<>null){
                                    echo Yii::$app->appHelperFunctions->getLastActionHitory([
                                        'model_id' => $model->id,
                                        'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                                    ]);
                                }
                                ?>
                            </div>


                            <?php ActiveForm::end(); ?>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>