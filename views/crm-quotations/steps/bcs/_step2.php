<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Receive Info/Documents');
$cardTitle = Yii::t('app', 'Receive Info/Documents:  {nameAttribute}', [
    'nameAttribute' => $quotation->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['crm-quotations/step_2/' . $quotation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

$("body").on("click", ".sav-btn1", function (e) {
    _this=$(this);
    e.preventDefault();
    swal({
        title: "'.Yii::t('app',$AlertText).'",
        html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#47a447",
        confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
        cancelButtonText: "'.Yii::t('app','Cancel').'",
    },function(result) {
        if (result) {
            console.log("Hello 123")
            $("#w0").unbind("submit").submit();
        }
    });
});


var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});
');

$image_row = 0;

// dd($valuation->upgrades);
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }
    .help-block2 {
        color: #a94442;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -6px;
        font-family: inherit;
    }
    body {font-family: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";}
    .nav > .nav-link {color: #007bff !important;}
    .nav > .active {color: #fff !important;}
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $quotation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../../left-nav', ['model' => $valuation, 'step' => 2,'quotation'=>$quotation,'property_index'=> $property_index]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">
                                <!-- Owners Information-->

                                <!-- Media Information-->
                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Documents Information</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row" style="padding: 10px">

                                            <?php

                                            if (isset($valuation->property->required_documents) && ($valuation->property->required_documents <> null)) {
                                                $required_documents = explode(',', $valuation->property->required_documents);
                                            }
                                            ?>

                                            <?php

                                            if (isset($valuation->property->optional_documents) && ($valuation->property->optional_documents <> null)) {
                                                $optional_documents = explode(',', $valuation->property->optional_documents);
                                            }
                                            ?>


                                            <?php $unit_row = 0; ?>

                                            <?php if($required_documents <> null && !empty($required_documents)) { ?>
                                            <table id="requestTypes"
                                                   class="table table-striped table-bordered table-hover images-table">
                                                <thead>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>Attachment</td>
                                                    <td></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php

                                                foreach ($required_documents as $required_document) {

                                                    if($required_document != 2 && $required_document != 8){
                                                        continue;
                                                    }

                                                    $attachment_Details = \app\models\CrmReceivedDocsFiles::find()->where(["quotation_id" => $quotation->id, "document_id" => $required_document,'property_index'=>$valuation->property_index])->one();
                                                    $attachment = $attachment_Details->attachment;
                                                    ?>

                                                    <tr id="image-row<?php echo $unit_row; ?>">

                                                        <td class="text-left">
                                                            <div class="required">
                                                                <label  class="control-label">
                                                                    <?= Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] ?>
                                                                </label>
                                                                <div class="help-block2" style="display:none"></div>
                                                            </div>

                                                        </td>

                                                        <td class="text-left upload-docs">
                                                            <div class="form-group">
                                                                <a href="javascript:;"
                                                                   id="upload-document<?= $unit_row; ?>"
                                                                   onclick="uploadAttachment(<?= $unit_row; ?>)"
                                                                   data-toggle="tooltip"
                                                                   class="img-thumbnail"
                                                                   title="Upload Document">
                                                                    <?php

                                                                    if ($attachment <> null) {

                                                                        if (strpos($attachment, '.pdf') !== false) {
                                                                            ?>
                                                                            <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>

                                                                            <?php

                                                                        } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                                            ?>
                                                                            <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <?php

                                                                        } else {
                                                                            ?>
                                                                            <img src="<?php echo $attachment; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                                             alt="" title=""
                                                                             data-placeholder="no_image.png"/>
                                                                        <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                                                           target="_blank">
                                                                            <span class="glyphicon glyphicon-eye-open"></span>
                                                                        </a>
                                                                        <?php
                                                                    }

                                                                    ?>
                                                                </a>
                                                                <input type="hidden" class="mandatory-doc"
                                                                       name="CrmReceivedDocs[received_docs][<?php echo $unit_row; ?>][attachment]"
                                                                       value="<?= $attachment; ?>"
                                                                       id="input-attachment<?php echo $unit_row; ?>"/>
                                                                <input type="hidden"
                                                                       name="CrmReceivedDocs[received_docs][<?php echo $unit_row; ?>][document_id]"
                                                                       value="<?= $required_document; ?>"
                                                                       id="input-attachment<?php echo $unit_row; ?>"/>

                                                                <input type="hidden"
                                                                       name="CrmReceivedDocs[received_docs][<?php echo $unit_row; ?>][quotation_id]"
                                                                       value="<?= $quotation->id; ?>"
                                                                       id="input-history_id<?php echo $unit_row; ?>"/>
                                                                <input type="hidden"
                                                                       name="CrmReceivedDocs[received_docs][<?php echo $unit_row; ?>][needed]"
                                                                       value="0"
                                                                       id="input-history_id<?php echo $unit_row; ?>"/>


                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php $unit_row++; ?>

                                                <?php }

                                                
                                                foreach ($optional_documents as $optinal_document) {

                                                    if($optinal_document == 52 || $optinal_document == 66 || $optinal_document == 67 ){
                                                        if (in_array($valuation->upgrades, [4,5])) {
                                                        }else{
                                                            continue;
                                                        }
                                                    }

                                                    $attachment_Details = \app\models\CrmReceivedDocsFiles::find()->where(["quotation_id" => $quotation->id, "document_id" => $optinal_document,'property_index'=>$valuation->property_index])->one();
                                                    $attachment = $attachment_Details->attachment;
                                                    ?>

                                                    <tr id="image-row<?php echo $unit_row; ?>">

                                                        <td class="text-left ">
                                                            <div class="required">


                                                            <label>
                                                            <?= Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$optinal_document] ?>
                                                            </label>
                                                            </div>
                                                        </td>

                                                        <td class="text-left upload-docs">
                                                            <div class="form-group">
                                                                <a href="javascript:;"
                                                                   id="upload-document<?= $unit_row; ?>"
                                                                   onclick="uploadAttachment(<?= $unit_row; ?>)"
                                                                   data-toggle="tooltip"
                                                                   class="img-thumbnail"
                                                                   title="Upload Document">
                                                                    <?php

                                                                    if ($attachment <> null) {

                                                                        if (strpos($attachment, '.pdf') !== false) {
                                                                            ?>
                                                                            <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>

                                                                            <?php

                                                                        } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                                            ?>
                                                                            <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <?php

                                                                        } else {
                                                                            ?>
                                                                            <img src="<?php echo $attachment; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                                             alt="" title=""
                                                                             data-placeholder="no_image.png"/>
                                                                        <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                                                           target="_blank">
                                                                            <span class="glyphicon glyphicon-eye-open"></span>
                                                                        </a>
                                                                        <?php
                                                                    }

                                                                    ?>
                                                                </a>
                                                                <input type="hidden"
                                                                       name="CrmReceivedDocs[received_docs][<?php echo $unit_row; ?>][attachment]"
                                                                       value="<?= $attachment; ?>"
                                                                       id="input-attachment<?php echo $unit_row; ?>"/>
                                                                <input type="hidden"
                                                                       name="CrmReceivedDocs[received_docs][<?php echo $unit_row; ?>][document_id]"
                                                                       value="<?= $optinal_document; ?>"
                                                                       id="input-attachment<?php echo $unit_row; ?>"/>

                                                                <input type="hidden"
                                                                       name="CrmReceivedDocs[received_docs][<?php echo $unit_row; ?>][quotation_id]"
                                                                       value="<?= $quotation->id; ?>"
                                                                       id="input-history_id<?php echo $unit_row; ?>"/>
                                                                <input type="hidden"
                                                                       name="CrmReceivedDocs[received_docs][<?php echo $unit_row; ?>][needed]"
                                                                       value="1"
                                                                       id="input-history_id<?php echo $unit_row; ?>"/>


                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php $unit_row++; ?>

                                                <?php }

                                                ?>
                                                </tbody>
                                                <tfoot>

                                                </tfoot>
                                            </table>
                                            <?php } ?>


                                        </div>
                                    </div>

                                </section>


                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1a']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>


<script type="text/javascript">
    var owner_row = <?= $owner_row ?>;

    function addOwner() {
        html = '<tr id="owner_data_row' + owner_row + '" class="padding_5">';


        html += '  <td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="CrmReceivedDocs[owners_data][' + owner_row + '][name]" value="" placeholder="Name" class="form-control" required />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="CrmReceivedDocs[owners_data][' + owner_row + '][percentage]" value="" placeholder="Percentage" class="form-control" required />';
        html += '    </div>';
        html += '  </td>';
        html += ' <input type="hidden" name="CrmReceivedDocs[owners_data][' + owner_row + '][valuation_id]" value="<?= $valuation->id; ?>" id="input-valuation_id' + owner_row + '" />';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#owner_data_row' + owner_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        owner_row++;

        $('#owner_container tbody').append(html);
    }
</script>


<script type="text/javascript">
    var unit_row = <?= $unit_row ?>;

    function addUnit() {
        html = '<tr id="image-row' + unit_row + '">';


        html += '  <td class="text-left">';
        html += ' <div class="form-group">';
        html += ' <a href="javascript:;" id="upload-document' + unit_row + '" onclick="uploadAttachment(' + unit_row + ')" data-toggle="tooltip" class="img-thumbnail" title="Upload Document">';
        html += '<img src="<?= Yii::getAlias('@web') . '/images/upload-image.png' ?>" width="100" alt="" title="" data-placeholder="<?= Yii::getAlias('@web') . '/themes/wisdom/images/upload.png' ?>" />';
        html += '<i></i>';
        html += '</a>';
        html += ' <input type="hidden" name="Ticket[attachments][' + unit_row + '][attachment]" value="" id="input-attachment' + unit_row + '" />';
        html += ' <input type="hidden" name="Ticket[attachments][' + unit_row + '][history_id]" value="" id="input-history_id' + unit_row + '" />';
        html += ' <input type="hidden" name="Ticket[attachments][' + unit_row + '][user_id]" value="" id="input-user_id' + unit_row + '" />';
        html += '</div>';
        html += '  </td>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + unit_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger">-</button></td>';

        html += '</tr>';

        unit_row++;

        $('#requestTypes tbody').append(html);
    }

    var uploadAttachment = function (attachmentId) {


        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function (json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);


                            console.log(json.file.href);
                            var str1 = json['file'].href;
                            var str2 = ".pdf";
                            var str3 = ".doc";
                            var str4 = ".docx";
                            var str5 = ".xlsx";
                            var str6 = ".xls";
                            if (str1.indexOf(str2) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                            } else if (str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                            } else {
                                $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                            }
                            $('#input-attachment' + attachmentId).val(json['file'].href);


                            /*  $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                             $('#input-attachment' + attachmentId).val(json['file'].href);*/
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }

</script>



