<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Schedule Inspection');
$cardTitle = Yii::t('app', 'Schedule Inspection:  {nameAttribute}', [
    'nameAttribute' => $model->quotation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_4/'.$quotation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');


$this->registerJs('

$("#listingstransactions-valuation_date,#listingstransactions-inspection_date,#listingstransactions-client_deadline,#listingstransactions-valuation_report_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
$("#listingstransactions-inspection_time").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "hh:mm"
});

var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});

');


?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
</style>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->quotation_id ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                    <?php  echo $this->render('../left-nav', ['model' => $model,'step' => 4,'quotation'=>$quotation,'property_index'=> $property_index]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">
                                <div class="row">

                                    <input type="hidden" id="inspection_type_id" value="<?= $valuation->inspection_type ?>">


                                    <div class="col-sm-4" id="valuation_date_id">
                                        <?= $form->field($model, 'valuation_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-valuation_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-valuation_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                    </div>


                                    <div class="col-sm-4" id="inspection_date_id">

                                        <label for="html" id='inpect_date_label'>Inspection Date</label><br>
                                        <?= $form->field($model, 'inspection_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-inspection_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-inspection_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])->label(false) ?>
                                    </div>

                                    <div class="col-sm-4" id="inspection_time_id">
                                        <?= $form->field($model, 'inspection_time', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-inspection_time" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-inspection_time" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                    </div>



                                    <div class="col-sm-4" id="client_deadline_id">
                                        <?= $form->field($model, 'client_deadline', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-client_deadline" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-client_deadline" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                    </div>

                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'valuation_report_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-valuation_report_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-valuation_report_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                    </div>

                                    <div class="col-sm-4" id="inspection_officer_id">
                                        <?php
                                        echo $form->field($model, 'inspection_officer')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->staffMemberListArr,
                                            'options' => ['placeholder' => 'Select a Person ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>


                                    <div class="col-sm-4" id="contact_person_name_id">
                                        <?= $form->field($model, 'contact_person_name')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4" id="contact_email_id">
                                        <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>
                                    </div>

                                    <div class="col-sm-4" id="contact_phone_no_id">
                                        <?= $form->field($model, 'contact_phone_no')->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<script>

    /* document.getElementById("ajaxSubmitBtn").onclick = function(){
     var calltime =document.getElementById("requests-call_time").value;
     var visittime  =document.getElementById("requests-visit_time").value;
     var descp  =document.getElementById("requests-description").value;
     if (calltime !== '' && visittime  !== '' && descp !== '' ) {
     setInterval(function(){
     document.getElementById("ajaxSubmitBtn").disabled = true;
     },100);
     }
     } */

    var inspectiontype =document.getElementById("inspection_type_id").value;

    if(inspectiontype==2){
        document.getElementById("client_deadline_id").classList.add("d-none");
        document.getElementById("inspection_date_id").classList.remove("d-none");
        document.getElementById("inspection_time_id").classList.remove("d-none");
        document.getElementById("inspection_officer_id").classList.remove("d-none");
        document.getElementById("contact_person_name_id").classList.remove("d-none");
        document.getElementById("contact_email_id").classList.remove("d-none");
        document.getElementById("contact_phone_no_id").classList.remove("d-none");

        document.getElementById("inpect_date_label").innerHTML = "Physical Inspection";
    }
    if(inspectiontype==1){

        document.getElementById("inspection_date_id").classList.remove("d-none");
        document.getElementById("inspection_time_id").classList.remove("d-none");
        document.getElementById("inspection_officer_id").classList.remove("d-none");
        document.getElementById("client_deadline_id").classList.add("d-none");
        document.getElementById("contact_person_name_id").classList.add("d-none");
        document.getElementById("contact_email_id").classList.add("d-none");
        document.getElementById("contact_phone_no_id").classList.add("d-none");

        document.getElementById("inpect_date_label").innerHTML = "Drive By";
    }
    if(inspectiontype==3){
        document.getElementById("inspection_date_id").classList.add("d-none");
        document.getElementById("inspection_time_id").classList.add("d-none");
        document.getElementById("client_deadline_id").classList.add("d-none");
        document.getElementById("inspection_officer_id").classList.add("d-none");
        document.getElementById("contact_person_name_id").classList.add("d-none");
        document.getElementById("contact_email_id").classList.add("d-none");
        document.getElementById("contact_phone_no_id").classList.add("d-none");
        document.getElementById("client_deadline_id").classList.add("d-none");

        document.getElementById("inpect_date_label").innerHTML = "Desktop";
    }


    document.getElementById("inspection_type_id").onchange = function(){
        var inspectiontype =document.getElementById("crmscheduleinspection-inspection_type").value;

        if(inspectiontype==2){
            document.getElementById("client_deadline_id").classList.add("d-none");
            document.getElementById("inspection_date_id").classList.remove("d-none");
            document.getElementById("inspection_time_id").classList.remove("d-none");
            document.getElementById("inspection_officer_id").classList.remove("d-none");
            document.getElementById("contact_person_name_id").classList.remove("d-none");
            document.getElementById("contact_email_id").classList.remove("d-none");
            document.getElementById("contact_phone_no_id").classList.remove("d-none");

            document.getElementById("inpect_date_label").innerHTML = "Physical Inspection";
        }
        if(inspectiontype==1){

            document.getElementById("inspection_date_id").classList.remove("d-none");
            document.getElementById("inspection_time_id").classList.remove("d-none");
            document.getElementById("inspection_officer_id").classList.remove("d-none");
            document.getElementById("client_deadline_id").classList.add("d-none");
            document.getElementById("contact_person_name_id").classList.add("d-none");
            document.getElementById("contact_email_id").classList.add("d-none");
            document.getElementById("contact_phone_no_id").classList.add("d-none");

            document.getElementById("inpect_date_label").innerHTML = "Drive By";
        }
        if(inspectiontype==3){
            document.getElementById("inspection_date_id").classList.add("d-none");
            document.getElementById("inspection_time_id").classList.add("d-none");
            document.getElementById("client_deadline_id").classList.add("d-none");
            document.getElementById("inspection_officer_id").classList.add("d-none");
            document.getElementById("contact_person_name_id").classList.add("d-none");
            document.getElementById("contact_email_id").classList.add("d-none");
            document.getElementById("contact_phone_no_id").classList.add("d-none");
            document.getElementById("client_deadline_id").classList.add("d-none");

            document.getElementById("inpect_date_label").innerHTML = "Desktop";
        }


        console.log(Driveby);
    }





</script>
