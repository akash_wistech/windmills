<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Check Conflict Of Interest');
$cardTitle = Yii::t('app', 'Check Conflict Of Interest:  {nameAttribute}', [
    'nameAttribute' => $quotation->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_3/'.$quotation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>


    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-edit"></i>
                <?= $quotation->id ?>
            </h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-5 col-sm-3">
                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                        <?php  echo $this->render('../left-nav', ['model' => $model,'step' => 3,'quotation'=>$quotation,'property_index'=> $property_index]); ?>
                    </div>
                </div>
                <div class="col-7 col-sm-9">
                    <div class="tab-content" id="vert-tabs-tabContent">
                        <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                            <section class="valuation-form card card-outline card-primary">

                                <?php $form = ActiveForm::begin(); ?>
                                <header class="card-header">
                                    <h2 class="card-title"><?= $cardTitle ?> - Related To Buyer</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php
                                            echo $form->field($model, 'related_to_buyer')->widget(Select2::classname(), [
                                                'data' => array( 'No' => 'No','Yes' => 'Yes'),
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'disabled' => true
                                                ],
                                            ]);
                                            ?>
                                        </div>

                                        <div class="col-sm-12" id="related_to_buyer_reason"  style="<?= ($model->related_to_buyer == 'Yes')? "": 'display:none;' ?>">

                                            <?= $form->field($model, 'related_to_buyer_reason')->textarea(['rows' => '6']) ?>



                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Reference Number</th>
                                                    <th>Building Name</th>
                                                    <th>Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php

                                                if($buyer_data <> null && !empty($buyer_data)) {
                                                    foreach ($buyer_data as $buyer) {

                                                        ?>
                                                        <tr>
                                                            <td><?= $buyer->reference_number ?></td>
                                                            <td><?= $buyer->building->title ?></td>
                                                            <td><?= Yii::$app->formatter->asDate($buyer->date_submitted) ?></td>
                                                        </tr>

                                                        <?php
                                                    }
                                                }?>

                                                </tbody>
                                            </table>


                                        </div>






                                    </div>


                                </div>
                            </section>

                            <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title"><?= $cardTitle ?> - Related To Seller</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">




                                        <div class="col-sm-6">
                                            <?php
                                            echo $form->field($model, 'related_to_seller')->widget(Select2::classname(), [
                                                'data' => array('No' => 'No','Yes' => 'Yes' ),
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'disabled' => true
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                        <div class="col-sm-12" id="related_to_seller_reason"  style="<?= ($model->related_to_seller == 'Yes')? "": 'display:none;' ?>">

                                            <?= $form->field($model, 'related_to_seller_reason')->textarea(['rows' => '6']) ?>
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Reference Number</th>
                                                    <th>Building Name</th>
                                                    <th>Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php

                                                if($seller_data <> null && !empty($seller_data)) {
                                                    foreach ($seller_data as $seller) {

                                                        ?>
                                                        <tr>
                                                            <td><?= $seller->reference_number ?></td>
                                                            <td><?= $seller->building->title ?></td>
                                                            <td><?=Yii::$app->formatter->asDate($buyer->date_submitted) ?></td>
                                                        </tr>

                                                        <?php
                                                    }
                                                }?>

                                                </tbody>
                                            </table>

                                        </div>

                                    </div>


                                </div>
                            </section>


                           <!-- <section class="valuation-form card card-outline card-primary">
                                <header class="card-header">
                                    <h2 class="card-title"><?/*= $cardTitle */?> - Related To Client</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <?php
/*                                            echo $form->field($model, 'related_to_client')->widget(Select2::classname(), [
                                                'data' => array('No' => 'No','Yes' => 'Yes' ),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            */?>
                                        </div>
                                        <div class="col-sm-12" id="related_to_client_reason"  style="<?/*= ($model->related_to_client == 'Yes')? "": 'display:none;' */?>">

                                            <?/*= $form->field($model, 'related_to_client_reason')->textarea(['rows' => '6']) */?>

                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Reference Number</th>
                                                    <th>Building Name</th>
                                                    <th>Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php
/*
                                                if($client_data <> null && !empty($client_data)) {
                                                    foreach ($client_data as $client) {

                                                        */?>
                                                        <tr>
                                                            <td><?/*= $client->reference_number */?></td>
                                                            <td><?/*= $client->building->title */?></td>
                                                            <td><?/*= Yii::$app->formatter->asDate($buyer->date_submitted) */?></td>
                                                        </tr>

                                                        <?php
/*                                                    }
                                                }*/?>

                                                </tbody>
                                            </table>

                                        </div>

                                    </div>


                                </div>

                            </section>-->


                            <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title"><?= $cardTitle ?> - Related To Property</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <?php
                                            echo $form->field($model, 'related_to_property')->widget(Select2::classname(), [
                                                'data' => array( 'No' => 'No','Yes' => 'Yes',),
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'disabled' => true
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                        <div class="col-sm-12" id="related_to_property_reason"  style="<?= ($model->related_to_property == 'Yes')? "": 'display:none;' ?>">
                                            <?= $form->field($model, 'related_to_property_reason')->textarea(['rows' => '6']) ?>

                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Reference Number</th>
                                                    <th>Building Name</th>
                                                    <th>Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php

                                                if($property_data <> null && !empty($property_data)) {
                                                    foreach ($property_data as $property) {

                                                        ?>
                                                        <tr>
                                                            <td><?= $property->reference_number ?></td>
                                                            <td><?= $property->building->title ?></td>
                                                            <td><?= Yii::$app->formatter->asDate($buyer->date_submitted) ?></td>
                                                        </tr>

                                                        <?php
                                                    }
                                                }?>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>


                                </div>
                                <div class="card-footer">
                                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </section>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>



