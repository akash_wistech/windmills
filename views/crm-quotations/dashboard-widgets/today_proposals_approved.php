<?php 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use app\assets\SortBootstrapAsset;
SortBootstrapAsset::register($this);

use app\assets\DateRangePickerAsset2;
use app\models\CrmReceivedProperties;
use app\models\Properties;

DateRangePickerAsset2::register($this);

$this->title = Yii::t('app', $searchModel->page_title);

$clientsArr = ArrayHelper::map(\app\models\Company::find()
->select([
  'id','title',
  'cname_with_nick' => 'CONCAT(title," ",nick_name)',
])
  ->where(['status' => 1])
  ->andWhere([
      'or',
      ['data_type' => 0],
      ['data_type' => null],
  ])
  ->orderBy(['title' => SORT_ASC,])
  ->all(), 'id', 'title');
  $clientsArr = ['' => 'Select'] + $clientsArr;

  
  $citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
  $citiesArr = ['' => 'Select'] + $citiesArr;

?>

<style>


.dataTable th {
      color: #0056b3;
      font-size: 15px;
      text-align: left !important;
  }

  .table th,  .table td { padding: 3px 0px 3px 0px;}

  .dataTable td {
    font-size: 16px;
    text-align: left;
    padding-left: 3px;
    vertical-align:middle;
  }

  .content-header h1 {
    font-size: 16px !important;
  }

  .content-header .row {
    margin-bottom: 0px !important;
  }

  .content-header {
    padding: 0px !important;
  }

  .number_row {
    text-align: left !important;
    padding-left: 10px !important;
  }
  .fee_column {
    text-align: right !important;
    padding-right: 10px !important;
  }

  .table.dataTable thead > tr > th.sorting { padding-left: 2px; padding-right: 5px !important; }
  .table.dataTable thead>tr>th.sorting:before { bottom: 75% !important;}
  .table.dataTable thead>tr>th.sorting:after { top: 25% !important;}

  
</style>

<div class="bank-revenue-index col-12">
    <div class="card card-outline card-info">
    <div class="card-header">
          <div class="row">
              <div class="col-12">
                  <h3 class="card-title "><strong><?= $searchModel->page_title ?></strong></span></h3>
              </div>
          </div>
      </div>
        <div class="card-body">
            <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                <thead>
                    <tr>

                        <th  style="min-width: 88px; max-width: 90px;"> Inquiry <br>Date
                            <input type="date" id="inquiry_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 88px; max-width: 90px;"> Recomm <br>Date
                            <input type="date" id="recommended_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <?php // Verified Date (Approval Date label changed to Verified Date) ?>
                        <th style="min-width: 88px; max-width: 90px;"> Verified <br>Date
                            <input type="date" id="approved_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 100px; max-width: 110px;">Windmills <br>Ref
                            <input type="text" class="custom-search-input form-control" placeholder="WM Ref">
                        </th>

                        <th style="min-width: 96px; max-width: 98px;">Client <br>Ref
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>

                        <th style="min-width: 120px; max-width: 120px;">Client<br>&nbsp;
                          <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder' => 'Client']); ?>
                        </th>

                        <th style="min-width: 110px; max-width: 110px;">Property<br>&nbsp;
                          <input type="text" class="custom-search-input form-control" placeholder="Property">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">Maxima <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">Highest <br>Accepted
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">Recom. <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">Approved <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">TAT <br>Taken
                            <input type="text" class="custom-search-input form-control" placeholder="TAT">
                        </th>

                        <th style="min-width: 110px; max-width: 110px;">Status<br>&nbsp;
                            <input type="text" class="custom-search-input form-control" placeholder="Status">
                        </th>

                        <?php 
                          if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <th>Action</th>
                        <?php
                          }
                        ?>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        foreach($dataProvider->getModels() as $model){
                          $hours_diff = Yii::$app->crmQuotationHelperFunctions->getHoursDiff($model);

                          $final_approved_fee += $model->final_quoted_fee;
                          $maxima_fin_fee += Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model);
                          $high_accep_fee += Yii::$app->crmQuotationHelperFunctions->getLast3MonthsApprovedFee($model);
                          $recom_fee += Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model);
                    ?>
                    <tr class="active">

                    <!-- Inquiry Date -->
                    <td>
                      <?php echo ($model->instruction_date<>null) ? date("d-M-Y", strtotime($model->instruction_date)) : ''; 
                            echo "<br>";
                            echo Yii::$app->appHelperFunctions->formatTimeAmPm(trim($model->inquiry_received_time));
                      ?>
                    </td>

                    <!-- Recommended Date -->
                    <td>
                      <?php echo ($model->quotation_recommended_date<>null) ? date("d-M-Y", strtotime($model->quotation_recommended_date)) : ''; 
                            echo "<br>";
                            echo ($model->quotation_recommended_date <> null) ? date("h:i A", strtotime($model->quotation_recommended_date)) : ''; ?>
                    </td>

                    <!-- Approval Date -->
                    <?php // Verified Date (Approval Date label changed to Verified Date) ?>
                    <td>
                      <?php echo ($model->approved_date<>null) ? date("d-M-Y", strtotime($model->approved_date)) : '';
                      echo "<br>";
                      echo ($model->approved_date <> null) ? date("h:i A", strtotime($model->approved_date)) : ''; ?>
                    </td>

                    <!-- WM ref -->
                    <td><?php echo trim(str_replace(' ', '', $model->reference_number)); ?></td>

                    <!-- Client ref -->
                    <td><?php echo trim($model->client_reference<>null)?$model->client_reference: '(not set)'; ?></td>

                    <!-- Client -->
                    <td><?php echo trim($model->client->nick_name<>null)?$model->client->nick_name:$model->client->title; ?></td>

                    <!-- Property -->
                    <td>
                      <?php 
                        $get_prop = CrmReceivedProperties::find()->where(['quotation_id' => $model->id])->andWhere(['property_index' => 0])->one();
                        $get_prop_data = Properties::find()->where(['id' => $get_prop->property_id])->one();
                        echo $get_prop_data->title ;
                      ?>
                    </td>

                    <!-- Maxima fee -->
                    <td class="fee_column"><?php echo number_format(Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model)) ?></td>

                    <!-- Highest Accepted fee -->
                    <td class="fee_column"><?php echo number_format(Yii::$app->crmQuotationHelperFunctions->getLast3MonthsApprovedFee($model)) ?></td>

                    <!-- Recommended fee -->
                    <td class="fee_column"><?php echo number_format(Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model)) ?></td>

                    <!-- Approved fee -->
                    <td class="fee_column"><?php echo trim(number_format( $model->final_quoted_fee )); ?></td>

                    <!-- TAT Taken -->
                    <td>
                        <?php 
                          $startDate = ($model->quotation_recommended_date<>null) ? $model->quotation_recommended_date : ($model->instruction_date .' ' .$model->inquiry_received_time) ;
                          $endDate = $model->approved_date;
      
      
                          $startDateTime = new DateTime($startDate); 
                          $endDateTime = new DateTime($endDate);
                          
      
                          $workingHours= Yii::$app->appHelperFunctions->getCalculateWorkingHours2($startDateTime, $endDateTime);
      
                          $workingDays = abs(number_format(($workingHours/8.5),1));
      
                          $s = ($workingDays > 2) ? 's' : '';
      
                          echo $workingDays.' Day'.$s.'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';
                        ?>
                    </td>
                      
                    <!-- Status -->
                    <td><?php echo Yii::$app->crmQuotationHelperFunctions->quotationStatusListArrLabelTextWithColor[$model->quotation_status] ?></td>
                      
                      <?php 
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                      ?>
                      <td class="noprint actions">
                          <div class="btn-group flex-wrap">
                              <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                  data-toggle="dropdown">
                                  <span class="caret"></span>
                              </button>
                              <div class="dropdown-menu" role="menu">
                                  <a class="dropdown-item text-1"
                                      href="<?= yii\helpers\Url::to(['crm-quotations/step_0?id='.$model->id]) ?>"
                                      title="Edit" data-pjax="0">
                                      <i class="fas fa-edit"></i> Edit
                                  </a>
                              </div>
                          </div>
                      </td>
                      <?php
                        }
                      ?>
                    </tr>
                    <?php
                }
            }
        ?>
                </tbody>
                <?php if(count($dataProvider->getModels())>0){ ?>
                <tfoot>
                <th> </th>
                  <th> </th>
                  <th> </th>
                  <th> </th>
                  <th> </th>
                  <th> </th>
                  <th> Total Fee : </th>
                  <th style="text-align:right !important; padding-right:10px !important"> <?php echo number_format($maxima_fin_fee); ?></th>
                  <th style="text-align:right !important; padding-right:10px !important"> <?php echo number_format($high_accep_fee); ?></th>
                  <th style="text-align:right !important; padding-right:10px !important"> <?php echo number_format($recom_fee); ?></th>
                  <th style="text-align:right !important; padding-right: 10px !important;"><?php echo number_format($final_approved_fee); ?></th>
                  <th> </th>
                  <th> </th>
                  <?php if(Yii::$app->user->id==1 || Yii::$app->user->id==14){ ?>
                  <th> </th>
                  <?php } ?>                 
                </tfoot>
                <?php } ?>
            </table>
        </div>
    </div>
</div>




<?php
    $this->registerJs('
    
    
        // $("#bank-revenue").DataTable({
        //     order: [[0, "asc"]],
        //     pageLength: 50,
        //     searching: false,
        // });
        
          var dataTable = $("#bank-revenue").DataTable({
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [{
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }],
            order: [[0, "desc"]]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          //for date search inquiry_date
          $("#inquiry_date").on("change", function () {
            if($("#inquiry_date").val() != ""){
              var format1 = moment($("#inquiry_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search recommended_date
          $("#recommended_date").on("change", function () {
            if($("#recommended_date").val() != ""){
              var format1 = moment($("#recommended_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search approved_date
          $("#approved_date").on("change", function () {
            if($("#approved_date").val() != ""){
              var format1 = moment($("#approved_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(2).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });

          $(".custom-search-input-client").on("change", function () {
              $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    dataTable.search(data).draw();
                },
                // error: bbAlert
                error: function(jqXHR, textStatus, errorThrown) {
                  // Handle the error here
                  dataTable.search("").draw();
                }
              });
          });
                
          $("#bank-revenue_filter").css({
            "display":"none",
          });
        
        $(".custom-search-input").on("click", function (event) {
          event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
          event.stopPropagation();
        });
        

        // Move the length menu to the table footer
        // var lengthMenu = $(".dataTables_length");
        // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");

        // Create a custom footer element
        var customFooter = $("<div class=\"custom-footer\"></div>");
      
        // Move the length menu to the custom footer
        var lengthMenu = $(".dataTables_length");
        lengthMenu.detach().appendTo(customFooter);
      
        // Append the custom footer to the DataTable wrapper
        $("#bank-revenue_wrapper").append(customFooter);
        
        

    ');
?>