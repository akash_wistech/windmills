<?php 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use app\assets\SortBootstrapAsset;
SortBootstrapAsset::register($this);

use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);

$this->title = Yii::t('app', $searchModel->page_title);

$clientsArr = ArrayHelper::map(\app\models\Company::find()
->select([
  'id','title',
  'cname_with_nick' => 'CONCAT(title," ",nick_name)',
])
  ->where(['status' => 1])
  ->andWhere([
      'or',
      ['data_type' => 0],
      ['data_type' => null],
  ])
  ->orderBy(['title' => SORT_ASC,])
  ->all(), 'id', 'title');
  $clientsArr = ['' => 'select'] + $clientsArr;

  
  $citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
  $citiesArr = ['' => 'select'] + $citiesArr;


?>

<style>
.dataTable th {
      color: #0056b3;
      font-size: 15px;
      text-align: left !important;
      padding-right: 30px !important;
  }
  .dataTable td {
      font-size: 16px;
      text-align: left;
      /* padding-right: 50px; */
      padding-top: 3px;
  }

.content-header h1 {
    font-size: 16px !important;
}

.content-header .row {
    margin-bottom: 0px !important;
}

.content-header {
    padding: 0px !important;
}


<style>
   th:nth-child(1) {
      min-width: 170px;
      max-width: 170px;
    }
   th:nth-child(2) {
      min-width: 250px;
      max-width: 250px;
    }
   th:nth-child(3) {
      min-width: 70px;
      max-width: 70px;
    }
   th:nth-child(4) {
      min-width: 180px;
      max-width: 180px;
    }
   th:nth-child(5) {
      min-width: 70px;
      max-width: 70px;
    }
   th:nth-child(6) {
      min-width: 60px;
      max-width: 60px;
    }
   th:nth-child(7) {
      min-width: 100px;
      max-width: 100px;
    }
   th:nth-child(8) {
      min-width: 70px;
      max-width: 70px;
    }
   th:nth-child(9) {
      min-width: 70px;
      max-width: 70px;
    }
</style>

<div class="bank-revenue-index col-12">
    <div class="card card-outline card-info">
        <div class="card-body">
            <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                <thead>
                    <tr>

                        <th class="">Reference Number
                            <input type="text" class="custom-search-input form-control" placeholder="ref#">
                        </th>
                        <th class="">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder'=>'client']); ?>
                        </th>
                        <th class="">Client Type
                            <input type="text" class="custom-search-input form-control" placeholder="client type">
                        </th>
                        <th class="">Client Reference
                            <input type="text" class="custom-search-input form-control" placeholder="client reference">
                        </th>
                        <th class="">No of Properties
                            <input type="text" class="custom-search-input form-control" placeholder="properties">
                        </th>
                        <th class="">TOE Final TAT
                            <input type="text" class="custom-search-input form-control" placeholder="toe TAT">
                        </th>
                        <th class="">City
                            <?php echo Html::dropDownList('city', null, $citiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'city']); ?>
                        </th>
                        <th class="">Quotation Final Fee
                            <input type="text" class="custom-search-input form-control" placeholder="quotation fee">
                        </th>
                        <th class="">TOE Final Fee
                            <input type="text" class="custom-search-input form-control" placeholder="toe fee">
                        </th>

                        <?php 
                          if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <th></th>
                        <?php
                          }
                        ?>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($dataProvider->getModels() as $model){
                    ?>
                    <tr class="active">

                        <td><?php echo trim(str_replace(' ', '', $model->reference_number)); ?></td>
                        <td><?php echo trim($model->client->title); ?></td>
                        <td><?php echo trim(Yii::$app->quotationHelperFunctions->clientType[$model->client->client_type]); ?></td>
                        <td><?php echo trim($model->client_reference); ?></td>
                        <td><?php echo trim($model->no_of_properties); ?></td>
                        <td><?php echo trim($model->toe_final_turned_around_time); ?></td>
                        <td><?php echo trim($model->zeroIndexProperty->building->cityName->title); ?></td>
                        <td><?php echo trim(number_format($model->quotation_final_fee)); ?></td>
                        <td><?php echo trim(number_format($model->grand_final_toe_fee)); ?></td>
                        
                        <?php 
                          if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <td class="noprint actions">
                            <div class="btn-group flex-wrap">
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                    data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a class="dropdown-item text-1"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/step_0?id='.$model->id]) ?>"
                                        title="Update" data-pjax="0">
                                        <i class="fas fa-edit"></i> Update
                                    </a>
                                </div>
                            </div>
                        </td>
                        <?php
                          }
                        ?>
                    </tr>
                    <?php
                }
            }
        ?>
                </tbody>
                <tfoot>
                  
                
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th><strong>Total Fee:</strong></th>
                  <th  style="padding-right: 10px!important; text-align:right!important;">0.00</th>
                  
                  

        
                <?php 
                  if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                ?>
                <th></th>
                <?php
                  }
                ?>
                  
                </tfoot>
            </table>
        </div>
    </div>
</div>




<?php
    $this->registerJs('
    
    
        // $("#bank-revenue").DataTable({
        //     order: [[0, "asc"]],
        //     pageLength: 50,
        //     searching: false,
        // });
        
          var dataTable = $("#bank-revenue").DataTable({
            order: [],
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            columnDefs: [{
              targets: -1,  // The last column index
              orderable: false  // Disable sorting on the last column
            }]
          });
        
          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input-client").on("change", function () {
              $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                  console.log(data);
                  dataTable.search(data).draw();
                },
                error: bbAlert
            });

          });
                
          $("#bank-revenue_filter").css({
            "display":"none",
          });
        
        $(".custom-search-input").on("click", function (event) {
          event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
          event.stopPropagation();
        });
        

        // Move the length menu to the table footer
        // var lengthMenu = $(".dataTables_length");
        // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");

        // Create a custom footer element
        var customFooter = $("<div class=\"custom-footer\"></div>");
      
        // Move the length menu to the custom footer
        var lengthMenu = $(".dataTables_length");
        lengthMenu.detach().appendTo(customFooter);
      
        // Append the custom footer to the DataTable wrapper
        $("#bank-revenue_wrapper").append(customFooter);
        
        

    ');
?>