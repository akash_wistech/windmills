<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use app\assets\SortBootstrapAsset;

SortBootstrapAsset::register($this);

use app\assets\DateRangePickerAsset2;

DateRangePickerAsset2::register($this);
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


$this->title = Yii::t('app', $searchModel->page_title);

$clientsArr = ArrayHelper::map(\app\models\Company::find()
    ->select([
        'id',
        'title',
        'cname_with_nick' => 'CONCAT(title," ",nick_name)',
    ])
    ->where(['status' => 1])
    ->andWhere([
        'or',
        ['data_type' => 0],
        ['data_type' => null],
    ])
    ->orderBy(['title' => SORT_ASC,])
    ->all(), 'id', 'title');
$clientsArr = ['' => 'Select'] + $clientsArr;


$citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
$citiesArr = ['' => 'select'] + $citiesArr;


$recommTAT_sum = 0;
$approvedTAT_sum = 0;
$acceptedTAT_sum = 0;
$toeAcceptedTAT_sum = 0;
$paymentTAT_sum = 0;
$totalTAT_sum = 0;

$total_rows  = (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ;

$recRowCount = 0;
$appRowCount = 0;
$accRowCount = 0;
$toeAccRowCount = 0;
$payRowCount = 0;
$totTatRowCount = 0;

?>

<style>


    .dataTable th {
        color: #0056b3;
        font-size: 15px;
        text-align: left !important;
        /* padding-right: 30px !important; */
    }

    .table th,  .table td { padding: 3px 0px 3px 0px;}

    .dataTable td {
        font-size: 16px;
        text-align: left;
        /* padding-right: 50px; */
        /* padding-top: 3px; */
        padding-left: 3px;
        vertical-align:middle;
    }

    .content-header h1 {
        font-size: 16px !important;
    }

    .content-header .row {
        margin-bottom: 0px !important;
    }

    .content-header {
        padding: 0px !important;
    }

    .number_row {
        text-align: left !important;
        padding-left: 10px !important;
    }
    .fee_column {
        text-align: right !important;
        padding-right: 10px !important;
    }

    .table.dataTable thead > tr > th.sorting { padding-left: 2px; padding-right: 5px !important; }
    .table.dataTable thead>tr>th.sorting:before { bottom: 75% !important;}
    .table.dataTable thead>tr>th.sorting:after { top: 25% !important;}


</style>

<?php
// echo "<pre>";
// print_r($model);
// echo "<pre>";die;
?>

<?php if ($searchModel->page_title == "Total Proposals Overview") { ?>
    <div class="inquiry-overview-index col-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h3 class="card-title "><strong><?= $searchModel->page_title ?></strong></span></h3>
                    </div>
                </div>
            </div>

            <div class="valuation-search card-body mb-0 pb-0">
                <?php $form = ActiveForm::begin([
                    'action' => ['dashboard-index'],
                    'method' => 'get',
                ]); ?>
                <?= $form->field($searchModel, 'widget_type')->hiddenInput(['value' => 'all'])->label(false); ?>
                <?= $form->field($searchModel, 'status_type')->hiddenInput(['value' => 'inquiry_overview'])->label(false); ?>
                <div class="row">
                    <div class="col-sm-4 text-left">
                        <?php
                        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->reportPeriod,
                            'options' => ['placeholder' => 'Time Frame ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>
                    </div>
                    <div class="col-sm-4" id="date_range_array" <?php if ($custom == 0) {
                        echo 'style="display:none;"';
                    } ?>>
                        <div class="" id="end_date_id">
                            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class' => 'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="text-left">
                            <div class="form-group">
                                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div class="card-body">
                <table id="inquiry_overview" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>
                        <th style="min-width: 86px; max-width: 86px;">Inquiry <br>Date
                            <input type="date" id="inquiry_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th  style="min-width: 86px; max-width: 86px;">Target <br>Date
                            <input type="date" id="target_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <?php // Verified Date (Approved Date label changed to Verified) ?>
                        <th  style="min-width: 86px; max-width: 86px;">Verified <br>Date
                            <input type="date" id="approved_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <?php // Approved Date (Accepted Date label changed to Approved) ?>
                        <th  style="min-width: 86px; max-width: 86px;">Approved <br>Date
                            <input type="date" id="accepted_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th  style="min-width: 86px; max-width: 86px;">TOE <br>Signed
                            <input type="date" id="toe_signed_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 130px; max-width: 130px;">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder' => 'client']); ?>
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">No. of <br>Properties
                            <input type="text" id="no_of_properties" class="custom-search-input form-control" placeholder="No. of Properties">
                        </th>

                        <!-- <th style="min-width: 70px; max-width: 70px;">Sourcing RM
                          <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th> -->

                        <th style="min-width: 76px; max-width: 76px;">Approved <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 80px; max-width: 80px;">Status
                            <input type="text" class="custom-search-input form-control" placeholder="Status">
                        </th>

                        <th style="min-width: 86px; max-width: 86px;">TAT <br>Taken
                            <input type="text" class="custom-search-input form-control" placeholder="TAT Taken">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">Reason
                            <input type="text" class="custom-search-input form-control" placeholder="">
                        </th>

                        <th style="min-width: 180px; max-width: 180px;">Instruting <br>Person
                            <input type="text" class="custom-search-input form-control" placeholder="Contact Person">
                        </th>

                        <th style="min-width: 110px; max-width: 110px;">Instruting <br>Person Landline
                            <input type="text" class="custom-search-input form-control" placeholder="Landline">
                        </th>

                        <th style="min-width: 96px; max-width: 96px;">Instruting <br>Person Mobile
                            <input type="text" class="custom-search-input form-control" placeholder="Mobile">
                        </th>

                        <th>Action</th>

                        <th style="min-width: 130px; max-width: 130px;">Windmills <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Reference">
                        </th>

                        <!-- <th style="min-width: 120px; max-width: 120px; ">Client <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Reference">
                        </th> -->


                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (count($dataProvider->getModels()) > 0) {
                        // dd(count($dataProvider->getModels()));
                        $final_approved_fee = 0;
                        foreach ($dataProvider->getModels() as $model) {
                            $clientInquiryContactsArr = \app\models\User::find()
                                ->select([
                                    \app\models\User::tableName() . ".id",
                                    \app\models\User::tableName() . ".firstname",
                                    \app\models\User::tableName() . ".lastname",
                                    \app\models\UserProfileInfo::tableName() . ".mobile",
                                    \app\models\UserProfileInfo::tableName() . ".phone1",
                                ])
                                ->where(['company_id' => $model->client_name])
                                ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                                ->andWhere([\app\models\UserProfileInfo::tableName() . ".primary_contact" => 1])
                                ->one();

                            $final_approved_fee += $model->final_quoted_fee;
                            // dd($model);
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?php
                                    if (isset($model->instruction_date)) {
                                        echo date('d-M-Y', strtotime($model->instruction_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->inquiry_received_time)) {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm(trim($model->inquiry_received_time));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Target Date -->
                                <td>kk
                                    <?php
                                    if (isset($model->target_date)) {
                                        echo date('d-M-Y', strtotime($model->target_date));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <?php // Verified Date (Approved Date label changed to Verfied) ?>
                                <td>
                                    <?php
                                    if (isset($model->approved_date)) {
                                        echo date('d-M-Y', strtotime($model->approved_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->approved_date)) {
                                        echo date('h:i A', strtotime($model->approved_date));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <?php // Approved Date (Accepted Date label changed to Approved) ?>
                                <td>
                                    <?php
                                    if (isset($model->quotation_accepted_date)) {
                                        echo date('d-M-Y', strtotime($model->quotation_accepted_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->quotation_accepted_date)) {
                                        echo date('h:i A', strtotime($model->quotation_accepted_date));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- TOE Signed Date -->
                                <td>
                                    <?php
                                    if (isset($model->toe_signed_and_received_date)) {
                                        echo date('d-M-Y', strtotime($model->toe_signed_and_received_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->toe_signed_and_received_date)) {
                                        echo date('h:i A', strtotime($model->toe_signed_and_received_date));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?php
                                    $client = $model->client->nick_name;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client, 4);

                                    ?>
                                </td>

                                <!-- No. of Properties -->
                                <td>
                                    <?php
                                    echo $model->no_of_properties;
                                    ?>
                                </td>

                                <!-- Sourcing RM -->
                                <!-- <td>
                                          <?php

                                ?>
                                      </td> -->

                                <!-- Approved fee -->
                                <td class="fee_column"><?php echo trim(number_format($model->final_quoted_fee)); ?></td>

                                <!-- Status -->
                                <td><?php echo Yii::$app->crmQuotationHelperFunctions->quotationStatusListArrLabelTextWithColor[$model->quotation_status] ?></td>

                                <!-- TAT Taken -->
                                <td>
                                    <?php
                                    $startDate = $model->instruction_date . ' ' . $model->inquiry_received_time;
                                    $endDate = $model->payment_received_date;
                                    // $endDate = date("Y-m-d  H:i:s");

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));
                                    $s = ($workingDays > 2) ? 's' : '';

                                    echo $workingDays . ' Day' . $s . '<br>' . '<strong>' . $workingHours . ' Hours' . '</strong>';
                                    ?>
                                </td>

                                <!-- Reason -->
                                <td  class="text-center" >
                                    <?php // Client Hold/Reject/Cancel Reason ?>
                                    <?php if ($model->quotation_cancel_reason <> null) { ?>
                                        <a class="reasonview" style="cursor: pointer" data_id="<?= $model->id ?>"> <i class="fas fa-eye"></i> </a>
                                    <?php } //echo trim($cancelReasonArr->title); ?>
                                </td>

                                <!-- Contact Person Name -->
                                <td>
                                    <?php
                                    $contact_person = trim($clientInquiryContactsArr->firstname . ' ' . $clientInquiryContactsArr->lastname);
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($contact_person, 6);
                                    ?>
                                </td>

                                <!-- Landline -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->phone1); ?>
                                </td>

                                <!-- Mobile -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->mobile); ?>
                                </td>

                                <td class="noprint actions text-center">
                                    <div class="btn-group flex-wrap">
                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <a class="dropdown-item text-1"
                                               href="<?= yii\helpers\Url::to(['crm-quotations/step_0', 'id' => $model->id]) ?>"
                                               title="Step 1" data-pjax="0">
                                                <i class="fas fa-edit"></i> Edit
                                            </a>
                                        </div>
                                    </div>
                                </td>


                                <!-- WM Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Reference -->
                                <!-- <td>
                                          <?php
                                // $client_ref = $model->client_reference;
                                // echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client_ref, 3);
                                ?>
                                      </td> -->

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <?php if (count($dataProvider->getModels()) > 0) { ?>
                        <tfoot>

                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <!-- <th> </th> -->
                        <th> Total Fee : </th>
                        <th style="text-align:right !important; padding-right: 10px !important;"><?php echo number_format($final_approved_fee); ?></th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <!-- <th> </th> -->

                        </tfoot>
                    <?php } ?>
                </table>
            </div>

        </div>
    </div>

    <?php
    $this->registerJs('
     
                    
       
                
          var dataTable = $("#inquiry_overview").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [{
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }],
            order: [[0, "desc"]]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });

          $("#no_of_properties").on("keyup", function () {
            dataTable.column(6).search(this.value).draw();
          });

          //for date search inquiry_date
          $("#inquiry_date").on("change", function () {
            if($("#inquiry_date").val() != ""){
              var format1 = moment($("#inquiry_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search target_date
          $("#target_date").on("change", function () {
            if($("#target_date").val() != ""){
              var format1 = moment($("#target_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search approved_date
          $("#approved_date").on("change", function () {
            if($("#approved_date").val() != ""){
              var format1 = moment($("#approved_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(2).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search accepted_date
          $("#accepted_date").on("change", function () {
            if($("#accepted_date").val() != ""){
              var format1 = moment($("#accepted_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(3).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search toe_signed_date
          $("#toe_signed_date").on("change", function () {
            if($("#toe_signed_date").val() != ""){
              var format1 = moment($("#toe_signed_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(4).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                  url: "' . yii\helpers\Url::to(['suggestion/getclient']) . '/"+this.value,
                  method: "post",
                  dataType: "html",
                  success: function(data) {
                      console.log(data);
                      dataTable.search(data).draw();
                  },
                  // error: bbAlert
                  error: function(jqXHR, textStatus, errorThrown) {
                    // Handle the error here
                    dataTable.search("").draw();
                  }
                });
            });
                
          $("#pending_received_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_received_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_received_wrapper").append(customFooter);

          $(".reasonview").on("click", function () {           
            $.ajax({
                url: "' . yii\helpers\Url::to(['crm-quotations/reason-view-data']) . '?id="+$(this).attr(\'data_id\'),
                method: "post",
                dataType: "html",
                success: function(data) {
                  
                    $("#model_html2").html(data);
                    $("#reasonViewModal").show();
                    
                },
                error: bbAlert
            });
  
          });
          $(".close,.close_footer").on("click", function () {
              $("#model_html2").html("");
              $("#editModal").hide();
              $("#reasonViewModal").hide();
          });


          $(".div1").daterangepicker({
            autoUpdateInput: false,
              locale: {
                format: "YYYY-MM-DD"
              }
          });
         
          $(".div1").on("apply.daterangepicker", function(ev, picker) {
             $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
          });
          $(document).ready(function() {
              // Code to execute when the document is ready
              var period = $("#crmquotationssearch-time_period").val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $("#date_range_array").hide();
              }
              
              // Change event handler for select input
              $("#crmquotationssearch-time_period").on("change", function() {
                  var period = $(this).val();
                  
                  if (period == 9) {
                      $(".div1").prop("required", true);
                      $("#date_range_array").show();
                  } else {
                      $(".div1").prop("required", false);
                      $(".div1").val("");
                      $("#date_range_array").hide();
                  }
              });
          });
          
          
          
    ');
    ?>

<?php } ?>

<?php if ($searchModel->page_title == "Total Proposals Recommended") { ?>
    <div class="total-recommended-index col-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h3 class="card-title "><strong><?= $searchModel->page_title ?></strong></span></h3>
                    </div>
                </div>
            </div>

            <div class="valuation-search card-body mb-0 pb-0">
                <?php $form = ActiveForm::begin([
                    'action' => ['dashboard-index'],
                    'method' => 'get',
                ]); ?>
                <?= $form->field($searchModel, 'widget_type')->hiddenInput(['value' => 'all'])->label(false); ?>
                <?= $form->field($searchModel, 'status_type')->hiddenInput(['value' => 'quotation_recommended'])->label(false); ?>
                <div class="row">
                    <div class="col-sm-4 text-left">
                        <?php
                        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->reportPeriod,
                            'options' => ['placeholder' => 'Time Frame ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>
                    </div>
                    <div class="col-sm-4" id="date_range_array" <?php if ($custom == 0) {
                        echo 'style="display:none;"';
                    } ?>>
                        <div class="" id="end_date_id">
                            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class' => 'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="text-left">
                            <div class="form-group">
                                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div class="card-body">
                <table id="quotation_recommended" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>
                        <th style="min-width: 86px; max-width: 86px;">Inquiry <br>Date
                            <input type="date" id="inquiry_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <!-- <th style="min-width: 86px; max-width: 86px;">Recomm. <br>Date
                          <input type="date" id="recommended_date" class="custom-search-input form-control" placeholder="Ref#">
                      </th> -->

                        <?php // Verified Date (Approved Date label changed to Verified) ?>
                        <th style="min-width: 86px; max-width: 86px;">Verified <br>Date
                            <input type="date" id="approved_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 120px; max-width: 120px;">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder' => 'client']); ?>
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">No. of <br>Properties
                            <input type="text" id="no_of_properties" class="custom-search-input form-control" placeholder="No. of Property">
                        </th>

                        <th style="min-width: 120px; max-width: 120px;">Propoerty
                            <input type="text" class="custom-search-input form-control" placeholder="Propoerty">
                        </th>

                        <th style="min-width: 90px; max-width: 90px;">Community
                            <input type="text" class="custom-search-input form-control" placeholder="Community">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">City
                            <input type="text" class="custom-search-input form-control" placeholder="City">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">Proposed <br>By
                            <input type="text" class="custom-search-input form-control" placeholder="Proposed By">
                        </th>

                        <th style="min-width: 76px; max-width: 76px;">Recom. <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Recom. Fee">
                        </th>

                        <th style="min-width: 86px; max-width: 86px;">TAT <br>Taken
                            <input type="text" class="custom-search-input form-control" placeholder="TAT">
                        </th>

                        <th style="min-width: 180px; max-width: 180px;">Contact <br>Person
                            <input type="text" class="custom-search-input form-control" placeholder="Contact">
                        </th>

                        <th style="min-width: 90px; max-width: 90px;">Contact <br>Landline
                            <input type="text" class="custom-search-input form-control" placeholder="Landline">
                        </th>

                        <th style="min-width: 96px; max-width: 96px;">Contact <br>Mobile
                            <input type="text" class="custom-search-input form-control" placeholder="Mobile">
                        </th>

                        <th>Action</th>

                        <th style="min-width: 130px; max-width: 130px;">Windmills <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="WM Reference">
                        </th>

                        <th style="min-width: 120px; max-width: 120px; ">Client <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Client Ref">
                        </th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (count($dataProvider->getModels()) > 0) {
                        // dd(count($dataProvider->getModels()));
                        $recome_fee = 0;
                        foreach ($dataProvider->getModels() as $model) {
                            $clientInquiryContactsArr = \app\models\User::find()
                                ->select([
                                    \app\models\User::tableName() . ".id",
                                    \app\models\User::tableName() . ".firstname",
                                    \app\models\User::tableName() . ".lastname",
                                    \app\models\UserProfileInfo::tableName() . ".mobile",
                                    \app\models\UserProfileInfo::tableName() . ".phone1",
                                ])
                                ->where(['company_id' => $model->client_name])
                                ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                                ->andWhere([\app\models\UserProfileInfo::tableName() . ".primary_contact" => 1])
                                ->one();

                            $recome_fee += Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model);
                            // dd($model);
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?php
                                    if (isset($model->instruction_date)) {
                                        echo date('d-M-Y', strtotime($model->instruction_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->inquiry_received_time)) {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm(trim($model->inquiry_received_time));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Recommended Date -->
                                <!-- <td>
                                    <?php
                                if (isset($model->quotation_recommended_date)) {
                                    echo date('d-M-Y', strtotime($model->quotation_recommended_date));
                                }
                                echo "<br>";
                                if (isset($model->quotation_recommended_date)) {
                                    echo date('h:i A', strtotime($model->quotation_recommended_date));
                                } else {
                                    echo "";
                                }
                                ?>
                                </td> -->

                                <?php // Verified Date (Approved Date label changed to Verified) ?>
                                <td>
                                    <?php
                                    if (isset($model->approved_date)) {
                                        echo date('d-M-Y', strtotime($model->approved_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->approved_date)) {
                                        echo date('h:i A', strtotime($model->approved_date));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?php
                                    $client = $model->client->nick_name;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client, 4);
                                    ?>
                                </td>

                                <!-- No. of Properties -->
                                <td>
                                    <?php
                                    echo $model->no_of_properties;
                                    ?>
                                </td>

                                <!-- Property -->
                                <td>
                                    <?php
                                    echo $model->zeroIndexProperty->property->title;
                                    ?>
                                </td>

                                <!-- Community -->
                                <td>
                                    <?php
                                    $community = $model->zeroIndexProperty->building->communities->title;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($community, 3);
                                    ?>
                                </td>

                                <!-- City -->
                                <td>
                                    <?php
                                    echo $model->zeroIndexProperty->building->cityName->title;
                                    ?>
                                </td>

                                <!-- Proposed By -->
                                <td>
                                    <?php echo trim($model->userData->lastname); ?>
                                </td>

                                <!-- Recommended fee -->
                                <td  class="fee_column"><?php echo number_format(Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model)) ?></td>

                                <!-- TAT Taken -->
                                <td>
                                    <?php
                                    $startDate = $model->instruction_date . ' ' . $model->inquiry_received_time;
                                    $endDate = $model->quotation_recommended_date;
                                    // $endDate = date("Y-m-d  H:i:s");

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));
                                    $s = ($workingDays > 2) ? 's' : '';

                                    echo $workingDays . ' Day' . $s . '<br>' . '<strong>' . $workingHours . ' Hours' . '</strong>';
                                    ?>
                                </td>

                                <!-- Contact Person Name -->
                                <td>
                                    <?php
                                    $contact_person = trim($clientInquiryContactsArr->firstname . ' ' . $clientInquiryContactsArr->lastname);
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($contact_person, 6);
                                    ?>
                                </td>

                                <!-- Landline -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->phone1); ?>
                                </td>

                                <!-- Mobile -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->mobile); ?>
                                </td>

                                <td class="noprint actions text-center">
                                    <div class="btn-group flex-wrap">
                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <a class="dropdown-item text-1"
                                               href="<?= yii\helpers\Url::to(['crm-quotations/step_0', 'id' => $model->id]) ?>"
                                               title="Step 1" data-pjax="0">
                                                <i class="fas fa-edit"></i> Edit
                                            </a>
                                        </div>
                                    </div>
                                </td>


                                <!-- WM Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Reference -->
                                <td>
                                    <?php $client_ref = $model->client_reference;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client_ref, 3);
                                    ?>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <?php if (count($dataProvider->getModels()) > 0) { ?>
                        <tfoot>

                        <th> </th>
                        <!-- <th> </th> -->
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> Total Fee : </th>
                        <th style="text-align:right !important; padding-right: 10px !important; "><?php echo number_format($recome_fee); ?></th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>

                        </tfoot>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>

    <?php
    $this->registerJs('
     
                
        var dataTable = $("#quotation_recommended").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }],
            order: [[0, "desc"]]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $("#no_of_properties").on("keyup", function () {
            dataTable.column(3).search(this.value).draw();
          });

          //for date search inquiry_date
          $("#inquiry_date").on("change", function () {
            if($("#inquiry_date").val() != ""){
              var format1 = moment($("#inquiry_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search approved_date
          $("#approved_date").on("change", function () {
            if($("#approved_date").val() != ""){
              var format1 = moment($("#approved_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          
          $(".custom-search-input-client").on("change", function () {
              $.ajax({
                url: "' . yii\helpers\Url::to(['suggestion/getclient']) . '/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    dataTable.search(data).draw();
                },
                // error: bbAlert
                error: function(jqXHR, textStatus, errorThrown) {
                  // Handle the error here
                  dataTable.search("").draw();
                }
              });
          });
                
          $("#pending_received_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });

  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_received_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_received_wrapper").append(customFooter);


          $(".div1").daterangepicker({
            autoUpdateInput: false,
              locale: {
                format: "YYYY-MM-DD"
              }
          });
         
          $(".div1").on("apply.daterangepicker", function(ev, picker) {
             $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
          });
          $(document).ready(function() {
              // Code to execute when the document is ready
              var period = $("#crmquotationssearch-time_period").val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $("#date_range_array").hide();
              }
              
              // Change event handler for select input
              $("#crmquotationssearch-time_period").on("change", function() {
                  var period = $(this).val();
                  
                  if (period == 9) {
                      $(".div1").prop("required", true);
                      $("#date_range_array").show();
                  } else {
                      $(".div1").prop("required", false);
                      $(".div1").val("");
                      $("#date_range_array").hide();
                  }
              });
          });
          
          
    ');
    ?>
<?php } ?>

<?php if ($searchModel->page_title == "All Document Requested") { ?>
    <div class="documents-requested-index col-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h3 class="card-title "><strong><?= $searchModel->page_title ?></strong></span></h3>
                    </div>
                </div>
            </div>

            <div class="valuation-search card-body mb-0 pb-0">
                <?php $form = ActiveForm::begin([
                    'action' => ['dashboard-index'],
                    'method' => 'get',
                ]); ?>
                <?= $form->field($searchModel, 'widget_type')->hiddenInput(['value' => 'all'])->label(false); ?>
                <?= $form->field($searchModel, 'status_type')->hiddenInput(['value' => 'document_requested'])->label(false); ?>
                <div class="row">
                    <div class="col-sm-4 text-left">
                        <?php
                        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->reportPeriod,
                            'options' => ['placeholder' => 'Time Frame ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>
                    </div>
                    <div class="col-sm-4" id="date_range_array" <?php if ($custom == 0) {
                        echo 'style="display:none;"';
                    } ?>>
                        <div class="" id="end_date_id">
                            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class' => 'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="text-left">
                            <div class="form-group">
                                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>


            <div class="card-body">
                <table id="documents_requested" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>
                        <th style="min-width: 86px; max-width: 86px;">Inquiry <br>Date
                            <input type="date" id="inquiry_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 86px; max-width: 86px;">Requested <br>Date
                            <input type="date" id="requested_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <?php // Verified Date (Approved Date label changed to Verified) ?>
                        <th style="min-width: 86px; max-width: 86px;">Verified <br>Date
                            <input type="date" id="approval_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 130px; max-width: 130px;">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder' => 'client']); ?>
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">No. of <br>Properties
                            <input type="text" id="no_of_properties" class="custom-search-input form-control" placeholder="No. of Properties">
                        </th>

                        <th style="min-width: 129px; max-width: 129px;">Propoerty
                            <input type="text" class="custom-search-input form-control" placeholder="Property">
                        </th>

                        <th style="min-width: 90px; max-width: 90px;">Community
                            <input type="text" class="custom-search-input form-control" placeholder="Community">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">City
                            <input type="text" class="custom-search-input form-control" placeholder="City">
                        </th>

                        <!-- <th style="min-width: 70px; max-width: 70px;">Sourcing RM
                          <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                      </th> -->

                        <?php // Verified By (Approved By label changed to Verfied) ?>
                        <th style="min-width: 70px; max-width: 70px;">Verified <by>By
                                <input type="text" class="custom-search-input form-control" placeholder="Verified By">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">Maxima <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 86px; max-width: 86px;">TAT <br>Taken
                            <input type="text" class="custom-search-input form-control" placeholder="TAT Taken">
                        </th>

                        <th style="min-width: 180px; max-width: 180px;">Contact <br>Person
                            <input type="text" class="custom-search-input form-control" placeholder="Contact Person">
                        </th>

                        <th style="min-width: 90px; max-width: 90px;">Contact <br>Landline
                            <input type="text" class="custom-search-input form-control" placeholder="Landline">
                        </th>

                        <th style="min-width: 96px; max-width: 96px;">Contact <br>Mobile
                            <input type="text" class="custom-search-input form-control" placeholder="Mobile">
                        </th>

                        <th>Action</th>

                        <th style="min-width: 130px; max-width: 130px;">Windmills <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>

                        <th style="min-width: 120px; max-width: 120px; ">Client <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (count($dataProvider->getModels()) > 0) {
                        // dd(count($dataProvider->getModels()));
                        $maxima_final_fee = 0;
                        foreach ($dataProvider->getModels() as $model) {
                            $clientInquiryContactsArr = \app\models\User::find()
                                ->select([
                                    \app\models\User::tableName() . ".id",
                                    \app\models\User::tableName() . ".firstname",
                                    \app\models\User::tableName() . ".lastname",
                                    \app\models\UserProfileInfo::tableName() . ".mobile",
                                    \app\models\UserProfileInfo::tableName() . ".phone1",
                                ])
                                ->where(['company_id' => $model->client_name])
                                ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                                ->andWhere([\app\models\UserProfileInfo::tableName() . ".primary_contact" => 1])
                                ->one();

                            $maxima_final_fee += Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model);
                            // dd($model);
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?php
                                    if (isset($model->instruction_date)) {
                                        echo date('d-M-Y', strtotime($model->instruction_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->inquiry_received_time)) {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm(trim($model->inquiry_received_time));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Request Date -->
                                <td>
                                    <?php
                                    if (isset($model->document_requested_date)) {
                                        echo date('d-M-Y', strtotime($model->document_requested_date));
                                    } else {
                                        echo "";
                                    }
                                    echo "<br>";
                                    if (isset($model->document_requested_date)) {
                                        echo date('h:i A', strtotime($model->document_requested_date));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <?php // Verified Date (Approved Date label changed to Verified) ?>
                                <td>
                                    <?php
                                    if (isset($model->approved_date)) {
                                        echo date('d-M-Y', strtotime($model->approved_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->approved_date)) {
                                        echo date('h:i A', strtotime($model->approved_date));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?php
                                    $client = $model->client->nick_name;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client, 4);
                                    ?>
                                </td>

                                <!-- No. of Properties -->
                                <td>
                                    <?php
                                    echo $model->no_of_properties;
                                    ?>
                                </td>

                                <!-- Property -->
                                <td>
                                    <?php
                                    echo $model->zeroIndexProperty->property->title;
                                    ?>
                                </td>

                                <!-- Community -->
                                <td>
                                    <?php
                                    $community = $model->zeroIndexProperty->building->communities->title;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($community, 4);

                                    ?>
                                </td>

                                <!-- City -->
                                <td>
                                    <?php
                                    echo $model->zeroIndexProperty->building->cityName->title;
                                    ?>
                                </td>

                                <!-- Sourcing RM -->
                                <!-- <td>
                                    <?php

                                ?>
                                </td> -->

                                <?php // Verified By (Approved By label changed to Verified) ?>
                                <td>
                                    <?php echo trim($model->approvedUserData->lastname); ?>
                                </td>

                                <!-- Maxima fee -->
                                <td  class="fee_column"><?php echo number_format(Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model)) ?></td>

                                <!-- TAT Taken -->
                                <td>
                                    <?php
                                    $startDate = $model->instruction_date . ' ' . $model->inquiry_received_time;
                                    $endDate = $model->document_requested_date;
                                    // $endDate = date("Y-m-d  H:i:s");

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));
                                    $s = ($workingDays > 2) ? 's' : '';

                                    echo $workingDays . ' Day' . $s . '<br>' . '<strong>' . $workingHours . ' Hours' . '</strong>';
                                    ?>
                                </td>

                                <!-- Contact Person Name -->
                                <td>
                                    <?php
                                    $contact_person = trim($clientInquiryContactsArr->firstname . ' ' . $clientInquiryContactsArr->lastname);
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($contact_person, 6); ?>
                                </td>

                                <!-- Landline -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->phone1); ?>
                                </td>

                                <!-- Mobile -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->mobile); ?>
                                </td>

                                <td class="noprint actions">
                                    <div class="btn-group flex-wrap">
                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <a class="dropdown-item text-1"
                                               href="<?= yii\helpers\Url::to(['crm-quotations/step_0', 'id' => $model->id]) ?>"
                                               title="Step 1" data-pjax="0">
                                                <i class="fas fa-edit"></i> Edit
                                            </a>
                                        </div>
                                    </div>
                                </td>


                                <!-- WM Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Reference -->
                                <td>
                                    <?php $client_ref = $model->client_reference;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client_ref, 3);
                                    ?>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <?php if (count($dataProvider->getModels()) > 0) { ?>
                        <tfoot>

                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <!-- <th> </th> -->
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> Total Fee : </th>
                        <th style="text-align:right !important; padding-right: 10px !important;"> <?php echo number_format($maxima_final_fee); ?></th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>

                        </tfoot>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>

    <?php
    $this->registerJs('
     
                
        var dataTable = $("#documents_requested").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }],
            order: [[0, "desc"]]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $("#no_of_properties").on("keyup", function () {
            dataTable.column(4).search(this.value).draw();
          });

          //for date search inquiry_date
          $("#inquiry_date").on("change", function () {
            if($("#inquiry_date").val() != ""){
              var format1 = moment($("#inquiry_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search requested_date
          $("#requested_date").on("change", function () {
            if($("#requested_date").val() != ""){
              var format1 = moment($("#requested_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search approval_date
          $("#approval_date").on("change", function () {
            if($("#approval_date").val() != ""){
              var format1 = moment($("#approval_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(2).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                  url: "' . yii\helpers\Url::to(['suggestion/getclient']) . '/"+this.value,
                  method: "post",
                  dataType: "html",
                  success: function(data) {
                      console.log(data);
                      dataTable.search(data).draw();
                  },
                  // error: bbAlert
                  error: function(jqXHR, textStatus, errorThrown) {
                    // Handle the error here
                    dataTable.search("").draw();
                  }
                });
            });
                
          $("#pending_received_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_received_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_received_wrapper").append(customFooter);

          $(".div1").daterangepicker({
            autoUpdateInput: false,
              locale: {
                format: "YYYY-MM-DD"
              }
          });
         
          $(".div1").on("apply.daterangepicker", function(ev, picker) {
             $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
          });
          $(document).ready(function() {
              // Code to execute when the document is ready
              var period = $("#crmquotationssearch-time_period").val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $("#date_range_array").hide();
              }
              
              // Change event handler for select input
              $("#crmquotationssearch-time_period").on("change", function() {
                  var period = $(this).val();
                  
                  if (period == 9) {
                      $(".div1").prop("required", true);
                      $("#date_range_array").show();
                  } else {
                      $(".div1").prop("required", false);
                      $(".div1").val("");
                      $("#date_range_array").hide();
                  }
              });
          });
          
          
    ');
    ?>
<?php } ?>

<?php if ($searchModel->page_title == "All Quotation Verified") { ?>
    <div class="quotation-approved-index col-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h3 class="card-title "><strong><?= $searchModel->page_title ?></strong></span></h3>
                    </div>
                </div>
            </div>

            <div class="valuation-search card-body mb-0 pb-0">
                <?php $form = ActiveForm::begin([
                    'action' => ['dashboard-index'],
                    'method' => 'get',
                ]); ?>
                <?= $form->field($searchModel, 'widget_type')->hiddenInput(['value' => 'all'])->label(false); ?>
                <?= $form->field($searchModel, 'status_type')->hiddenInput(['value' => 'quotation_approved'])->label(false); ?>
                <div class="row">
                    <div class="col-sm-4 text-left">
                        <?php
                        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->reportPeriod,
                            'options' => ['placeholder' => 'Time Frame ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>
                    </div>
                    <div class="col-sm-4" id="date_range_array" <?php if ($custom == 0) {
                        echo 'style="display:none;"';
                    } ?>>
                        <div class="" id="end_date_id">
                            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class' => 'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="text-left">
                            <div class="form-group">
                                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>


            <div class="card-body">
                <table id="quotation_approved" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>
                        <th style="min-width: 86px; max-width: 86px;">Inquiry <br>Date
                            <input type="date" id="inquiry_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 130px; max-width: 130px;">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder' => 'client']); ?>
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">No. of <br>Properties
                            <input type="text" id="no_of_properties" class="custom-search-input form-control" placeholder="No. of Properties">
                        </th>

                        <th style="min-width: 120px; max-width: 120px;">Propoerty
                            <input type="text" class="custom-search-input form-control" placeholder="Property">
                        </th>

                        <th style="min-width: 76px; max-width: 76px;">Approved <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 76px; max-width: 76px;">Maxima <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 76px; max-width: 76px;">Highest <br>Accepted
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 76px; max-width: 76px;">Recom. <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 86px; max-width: 86px;">TAT <br>Taken
                            <input type="text" class="custom-search-input form-control" placeholder="TAT Taken">
                        </th>

                        <th style="min-width: 180px; max-width: 180px;">Contact <br>Person
                            <input type="text" class="custom-search-input form-control" placeholder="Contact Person">
                        </th>

                        <th style="min-width: 90px; max-width: 90px;">Contact <br>Landline
                            <input type="text" class="custom-search-input form-control" placeholder="Landline">
                        </th>

                        <th style="min-width: 96px; max-width: 96px;">Contact <br>Mobile
                            <input type="text" class="custom-search-input form-control" placeholder="Mobile">
                        </th>

                        <th>Action</th>

                        <th style="min-width: 130px; max-width: 130px;">Windmills <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>

                        <th style="min-width: 120px; max-width: 120px; ">Client <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (count($dataProvider->getModels()) > 0) {
                        // dd(count($dataProvider->getModels()));

                        $appr_fee = 0;
                        $maxima_fin_fee = 0;
                        $high_accep_fee = 0;
                        $recom_fee = 0;
                        foreach ($dataProvider->getModels() as $model) {
                            $clientInquiryContactsArr = \app\models\User::find()
                                ->select([
                                    \app\models\User::tableName() . ".id",
                                    \app\models\User::tableName() . ".firstname",
                                    \app\models\User::tableName() . ".lastname",
                                    \app\models\UserProfileInfo::tableName() . ".mobile",
                                    \app\models\UserProfileInfo::tableName() . ".phone1",
                                ])
                                ->where(['company_id' => $model->client_name])
                                ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                                ->andWhere([\app\models\UserProfileInfo::tableName() . ".primary_contact" => 1])
                                ->one();

                            $appr_fee += $model->final_quoted_fee;
                            $maxima_fin_fee += Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model);
                            $high_accep_fee += Yii::$app->crmQuotationHelperFunctions->getLast3MonthsApprovedFee($model);
                            $recom_fee += Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model);
                            // dd($model);
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?php
                                    if (isset($model->instruction_date)) {
                                        echo date('d-M-Y', strtotime($model->instruction_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->inquiry_received_time)) {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm(trim($model->inquiry_received_time));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?php
                                    $client = $model->client->nick_name;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client, 4);
                                    ?>
                                </td>

                                <!-- No. of Properties -->
                                <td>
                                    <?php
                                    echo $model->no_of_properties;
                                    ?>
                                </td>

                                <!-- Property -->
                                <td>
                                    <?php
                                    echo $model->zeroIndexProperty->property->title;
                                    ?>
                                </td>

                                <!-- Approved fee -->
                                <td class="fee_column"><?php echo trim(number_format($model->final_quoted_fee)); ?></td>

                                <!-- Maxima fee -->
                                <td class="fee_column"><?php echo number_format(Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model)) ?></td>

                                <!-- Highest Accepted fee -->
                                <td class="fee_column"><?php echo number_format(Yii::$app->crmQuotationHelperFunctions->getLast3MonthsApprovedFee($model)) ?></td>

                                <!-- Recommended fee -->
                                <td class="fee_column"><?php
                                    echo number_format(Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model))
                                    ?></td>

                                <!-- TAT Taken -->
                                <td>
                                    <?php
                                    $startDate = $model->instruction_date . ' ' . $model->inquiry_received_time;
                                    $endDate = $model->approved_date;
                                    // $endDate = date("Y-m-d  H:i:s");

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));
                                    $s = ($workingDays > 2) ? 's' : '';

                                    echo $workingDays . ' Day' . $s . '<br>' . '<strong>' . $workingHours . ' Hours' . '</strong>';
                                    ?>
                                </td>

                                <!-- Contact Person Name -->
                                <td>
                                    <?php
                                    $contact_person = trim($clientInquiryContactsArr->firstname . ' ' . $clientInquiryContactsArr->lastname);
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($contact_person, 6); ?>
                                </td>

                                <!-- Landline -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->phone1); ?>
                                </td>

                                <!-- Mobile -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->mobile); ?>
                                </td>

                                <td class="noprint actions">
                                    <div class="btn-group flex-wrap">
                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <a class="dropdown-item text-1"
                                               href="<?= yii\helpers\Url::to(['crm-quotations/step_0', 'id' => $model->id]) ?>"
                                               title="Step 1" data-pjax="0">
                                                <i class="fas fa-edit"></i> Edit
                                            </a>
                                        </div>
                                    </div>
                                </td>


                                <!-- WM Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Reference -->
                                <td>
                                    <?php $client_ref = $model->client_reference;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client_ref, 3);
                                    ?>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <?php if (count($dataProvider->getModels()) > 0) { ?>
                        <tfoot>

                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> Total Fee : </th>
                        <th style="text-align:right !important; padding-right:10px !important"> <?php echo number_format($appr_fee); ?></th>
                        <th style="text-align:right !important; padding-right:10px !important"> <?php echo number_format($maxima_fin_fee); ?></th>
                        <th style="text-align:right !important; padding-right:10px !important"> <?php echo number_format($high_accep_fee); ?></th>
                        <th style="text-align:right !important; padding-right:10px !important"> <?php echo number_format($recom_fee); ?></th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>

                        </tfoot>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>

    <?php
    $this->registerJs('
     
                
        var dataTable = $("#quotation_approved").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }],
            order: [[0, "desc"]]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          $(".custom-search-input-client").on("change", function () {
            $.ajax({
              url: "' . yii\helpers\Url::to(['suggestion/getclient']) . '/"+this.value,
              method: "post",
              dataType: "html",
              success: function(data) {
                  console.log(data);
                  dataTable.search(data).draw();
              },
              // error: bbAlert
              error: function(jqXHR, textStatus, errorThrown) {
                // Handle the error here
                dataTable.search("").draw();
              }
            });
          });
                
          $("#pending_received_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });

          $("#no_of_properties").on("keyup", function () {
            dataTable.column(2).search(this.value).draw();
          });

          //for date search inquiry_date
          $("#inquiry_date").on("change", function () {
            if($("#inquiry_date").val() != ""){
              var format1 = moment($("#inquiry_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_received_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_received_wrapper").append(customFooter);

          $(".div1").daterangepicker({
            autoUpdateInput: false,
              locale: {
                format: "YYYY-MM-DD"
              }
          });
         
          $(".div1").on("apply.daterangepicker", function(ev, picker) {
             $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
          });
          $(document).ready(function() {
              // Code to execute when the document is ready
              var period = $("#crmquotationssearch-time_period").val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $("#date_range_array").hide();
              }
              
              // Change event handler for select input
              $("#crmquotationssearch-time_period").on("change", function() {
                  var period = $(this).val();
                  
                  if (period == 9) {
                      $(".div1").prop("required", true);
                      $("#date_range_array").show();
                  } else {
                      $(".div1").prop("required", false);
                      $(".div1").val("");
                      $("#date_range_array").hide();
                  }
              });
          });
          
          
    ');
    ?>
<?php } ?>

<?php if ($searchModel->page_title == "Total Quotations On Hold") { ?>
    <div class="on-hold-index col-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h3 class="card-title "><strong><?= $searchModel->page_title ?></strong></span></h3>
                    </div>
                </div>
            </div>

            <div class="valuation-search card-body mb-0 pb-0">
                <?php $form = ActiveForm::begin([
                    'action' => ['dashboard-index'],
                    'method' => 'get',
                ]); ?>
                <?= $form->field($searchModel, 'widget_type')->hiddenInput(['value' => 'all'])->label(false); ?>
                <?= $form->field($searchModel, 'status_type')->hiddenInput(['value' => 'quotation_onhold'])->label(false); ?>
                <div class="row">
                    <div class="col-sm-4 text-left">
                        <?php
                        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->reportPeriod,
                            'options' => ['placeholder' => 'Time Frame ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>
                    </div>
                    <div class="col-sm-4" id="date_range_array" <?php if ($custom == 0) {
                        echo 'style="display:none;"';
                    } ?>>
                        <div class="" id="end_date_id">
                            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class' => 'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="text-left">
                            <div class="form-group">
                                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div class="card-body">
                <table id="on_hold" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>
                        <th style="min-width: 86px; max-width: 86px;">Inquiry <br>Date
                            <input type="date" id="inquiry_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <?php // Verified Date (Approved Date label changed to Verified) ?>
                        <th style="min-width: 86px; max-width: 86px;">Verified <br>Date
                            <input type="date" id="approval_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 130px; max-width: 130px;">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder' => 'client']); ?>
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">No. of <br>Properties
                            <input type="text" id="no_of_properties" class="custom-search-input form-control" placeholder="No. of Properties">
                        </th>

                        <!-- <th style="min-width: 70px; max-width: 70px;">Sourcing <br>RM
                          <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                      </th> -->

                        <th style="min-width: 76px; max-width: 76px;">Approved <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 86px; max-width: 86px;">TAT <br>Taken
                            <input type="text" class="custom-search-input form-control" placeholder="TAT Taken">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">Reason
                            <input type="text" class="custom-search-input form-control" placeholder="Reason">
                        </th>

                        <th style="min-width: 180px; max-width: 180px;">Contact <br>Person
                            <input type="text" class="custom-search-input form-control" placeholder="Contact Person">
                        </th>

                        <th style="min-width: 90px; max-width: 90px;">Contact <br>Landline
                            <input type="text" class="custom-search-input form-control" placeholder="Landline">
                        </th>

                        <th style="min-width: 96px; max-width: 96px;">Contact <br>Mobile
                            <input type="text" class="custom-search-input form-control" placeholder="Monile">
                        </th>

                        <th>Action</th>

                        <th style="min-width: 120px; max-width: 120px;">WM Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>

                        <th style="min-width: 130px; max-width: 130px;">Client Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (count($dataProvider->getModels()) > 0) {
                        // dd(count($dataProvider->getModels()));
                        $approved_fee_final = 0;
                        foreach ($dataProvider->getModels() as $model) {
                            $clientInquiryContactsArr = \app\models\User::find()
                                ->select([
                                    \app\models\User::tableName() . ".id",
                                    \app\models\User::tableName() . ".firstname",
                                    \app\models\User::tableName() . ".lastname",
                                    \app\models\UserProfileInfo::tableName() . ".mobile",
                                    \app\models\UserProfileInfo::tableName() . ".phone1",
                                ])
                                ->where(['company_id' => $model->client_name])
                                ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                                ->andWhere([\app\models\UserProfileInfo::tableName() . ".primary_contact" => 1])
                                ->one();

                            $approved_fee_final += $model->final_quoted_fee;
                            // dd($model);
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?php
                                    if (isset($model->instruction_date)) {
                                        echo date('d-M-Y', strtotime($model->instruction_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->inquiry_received_time)) {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm(trim($model->inquiry_received_time));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <?php // Verified Date (Approved Date label changed to Verified) ?>
                                <td>
                                    <?php
                                    if (isset($model->approved_date)) {
                                        echo date('d-M-Y', strtotime($model->approved_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->approved_date)) {
                                        echo date('h:i A', strtotime($model->approved_date));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?php
                                    $client = $model->client->nick_name;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client, 4);
                                    ?>
                                </td>

                                <!-- No. of Properties -->
                                <td>
                                    <?php
                                    echo $model->no_of_properties;
                                    ?>
                                </td>

                                <!-- Sourcing RM -->
                                <!-- <td>
                                    <?php

                                ?>
                                </td> -->

                                <!-- Approved fee -->
                                <td class="fee_column"><?php echo trim(number_format($model->final_quoted_fee)); ?></td>

                                <!-- TAT Taken -->
                                <td>
                                    <?php
                                    $startDate = $model->instruction_date . ' ' . $model->inquiry_received_time;
                                    $endDate = $model->approved_date;
                                    // $endDate = date("Y-m-d  H:i:s");

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));
                                    $s = ($workingDays > 2) ? 's' : '';

                                    echo $workingDays . ' Day' . $s . '<br>' . '<strong>' . $workingHours . ' Hours' . '</strong>';
                                    ?>
                                </td>

                                <!-- Reason -->
                                <td class="text-center" >
                                    <?php

                                    ?>
                                </td>

                                <!-- Contact Person Name -->
                                <td>
                                    <?php
                                    $contact_person = trim($clientInquiryContactsArr->firstname . ' ' . $clientInquiryContactsArr->lastname);
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($contact_person, 6); ?>
                                </td>

                                <!-- Landline -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->phone1); ?>
                                </td>

                                <!-- Mobile -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->mobile); ?>
                                </td>

                                <td class="noprint actions">
                                    <div class="btn-group flex-wrap">
                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <a class="dropdown-item text-1"
                                               href="<?= yii\helpers\Url::to(['crm-quotations/step_0', 'id' => $model->id]) ?>"
                                               title="Step 1" data-pjax="0">
                                                <i class="fas fa-edit"></i> Edit
                                            </a>
                                        </div>
                                    </div>
                                </td>


                                <!-- WM Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Reference -->
                                <td>
                                    <?php $client_ref = $model->client_reference;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client_ref, 3);
                                    ?>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <?php if (count($dataProvider->getModels()) > 0) { ?>
                        <tfoot>

                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <!-- <th> </th> -->
                        <th> Total Fee : </th>
                        <th style="text-align:right !important; padding-right:10px !important"> <?php echo number_format($approved_fee_final) ?> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>

                        </tfoot>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>

    <?php
    $this->registerJs('
     
                
        var dataTable = $("#on_hold").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }],
            order: [[0, "desc"]]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $("#no_of_properties").on("keyup", function () {
            dataTable.column(2).search(this.value).draw();
          });

          //for date search inquiry_date
          $("#inquiry_date").on("change", function () {
            if($("#inquiry_date").val() != ""){
              var format1 = moment($("#inquiry_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search approval_date
          $("#approval_date").on("change", function () {
            if($("#approval_date").val() != ""){
              var format1 = moment($("#approval_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                  url: "' . yii\helpers\Url::to(['suggestion/getclient']) . '/"+this.value,
                  method: "post",
                  dataType: "html",
                  success: function(data) {
                      console.log(data);
                      dataTable.search(data).draw();
                  },
                  // error: bbAlert
                  error: function(jqXHR, textStatus, errorThrown) {
                    // Handle the error here
                    dataTable.search("").draw();
                  }
                });
            });
                
          $("#pending_received_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_received_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_received_wrapper").append(customFooter);

          $(".div1").daterangepicker({
            autoUpdateInput: false,
              locale: {
                format: "YYYY-MM-DD"
              }
          });
         
          $(".div1").on("apply.daterangepicker", function(ev, picker) {
             $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
          });
          $(document).ready(function() {
              // Code to execute when the document is ready
              var period = $("#crmquotationssearch-time_period").val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $("#date_range_array").hide();
              }
              
              // Change event handler for select input
              $("#crmquotationssearch-time_period").on("change", function() {
                  var period = $(this).val();
                  
                  if (period == 9) {
                      $(".div1").prop("required", true);
                      $("#date_range_array").show();
                  } else {
                      $(".div1").prop("required", false);
                      $(".div1").val("");
                      $("#date_range_array").hide();
                  }
              });
          });
          
          
    ');
    ?>
<?php } ?>

<?php if ($searchModel->page_title == "Total Quotations Cancelled") { ?>
    <div class="quotations-cancelled-index col-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h3 class="card-title "><strong><?= $searchModel->page_title ?></strong></span></h3>
                    </div>
                </div>
            </div>

            <div class="valuation-search card-body mb-0 pb-0">
                <?php $form = ActiveForm::begin([
                    'action' => ['dashboard-index'],
                    'method' => 'get',
                ]); ?>
                <?= $form->field($searchModel, 'widget_type')->hiddenInput(['value' => 'all'])->label(false); ?>
                <?= $form->field($searchModel, 'status_type')->hiddenInput(['value' => 'quotation_cancelled'])->label(false); ?>
                <div class="row">
                    <div class="col-sm-4 text-left">
                        <?php
                        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->reportPeriod,
                            'options' => ['placeholder' => 'Time Frame ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>
                    </div>
                    <div class="col-sm-4" id="date_range_array" <?php if ($custom == 0) {
                        echo 'style="display:none;"';
                    } ?>>
                        <div class="" id="end_date_id">
                            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class' => 'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="text-left">
                            <div class="form-group">
                                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div class="card-body">
                <table id="quotations_cancelled" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>
                        <th style="min-width: 86px; max-width: 86px;">Inquiry <br>Date
                            <input type="date" id="inquiry_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <?php // Verified Date (Approved Date label changed to Verified) ?>
                        <th style="min-width: 86px; max-width: 86px;">Verified <br>Date
                            <input type="date" id="approval_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 130px; max-width: 130px;">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder' => 'client']); ?>
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">No. of <br>Properties
                            <input type="text" id="no_of_properties" class="custom-search-input form-control" placeholder="No. of Properties">
                        </th>

                        <!-- <th style="min-width: 70px; max-width: 70px;">Sourcing <br>RM
                          <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                      </th> -->

                        <th style="min-width: 70px; max-width: 70px;">Approved <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 86px; max-width: 86px;">TAT <br>Taken
                            <input type="text" class="custom-search-input form-control" placeholder="TAT Taken">
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">Reason
                            <input type="text" class="custom-search-input form-control" placeholder="Reason">
                        </th>

                        <th style="min-width: 180px; max-width: 180px;">Contact <br>Person
                            <input type="text" class="custom-search-input form-control" placeholder="Contac Person">
                        </th>

                        <th style="min-width: 90px; max-width: 90px;">Contact <br>Landline
                            <input type="text" class="custom-search-input form-control" placeholder="Landline">
                        </th>

                        <th style="min-width: 96px; max-width: 96px;">Contact <br>Mobile
                            <input type="text" class="custom-search-input form-control" placeholder="Mobile">
                        </th>

                        <th>Action</th>

                        <th style="min-width: 120px; max-width: 120px;">Windmills <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>

                        <th style="min-width: 130px; max-width: 130px; ">Client <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (count($dataProvider->getModels()) > 0) {
                        // dd(count($dataProvider->getModels()));
                        $fin_appr_fee = 0;
                        foreach ($dataProvider->getModels() as $model) {
                            $clientInquiryContactsArr = \app\models\User::find()
                                ->select([
                                    \app\models\User::tableName() . ".id",
                                    \app\models\User::tableName() . ".firstname",
                                    \app\models\User::tableName() . ".lastname",
                                    \app\models\UserProfileInfo::tableName() . ".mobile",
                                    \app\models\UserProfileInfo::tableName() . ".phone1",
                                ])
                                ->where(['company_id' => $model->client_name])
                                ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                                ->andWhere([\app\models\UserProfileInfo::tableName() . ".primary_contact" => 1])
                                ->one();

                            $cancelReasonArr = \app\models\QuotationCancelReasons::find()
                                ->select([
                                    \app\models\QuotationCancelReasons::tableName() . ".id",
                                    \app\models\QuotationCancelReasons::tableName() . ".title"
                                ])
                                ->where([\app\models\QuotationCancelReasons::tableName() . ".id" => $model->quotation_cancel_reason])
                                ->one();
                            // dd($model);

                            $fin_appr_fee += $model->final_quoted_fee;
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?php
                                    if (isset($model->instruction_date)) {
                                        echo date('d-M-Y', strtotime($model->instruction_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->inquiry_received_time)) {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm(trim($model->inquiry_received_time));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <?php // Verified Date (Approved Date label changed to Verified) ?>
                                <td>
                                    <?php
                                    if (isset($model->approved_date)) {
                                        echo date('d-M-Y', strtotime($model->approved_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->approved_date)) {
                                        echo date('h:i A', strtotime($model->approved_date));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?php
                                    $client = $model->client->nick_name;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client, 4);
                                    ?>
                                </td>

                                <!-- No. of Properties -->
                                <td>
                                    <?php
                                    echo $model->no_of_properties;
                                    ?>
                                </td>

                                <!-- Sourcing RM -->
                                <!-- <td>
                                    <?php

                                ?>
                                </td> -->

                                <!-- Approved fee -->
                                <td class="fee_column"><?php echo trim(number_format($model->final_quoted_fee)); ?></td>

                                <!-- TAT Taken -->
                                <td>
                                    <?php
                                    $startDate = $model->instruction_date . ' ' . $model->inquiry_received_time;
                                    $endDate = $model->cancelled_date;
                                    // $endDate = date("Y-m-d  H:i:s");

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));
                                    $s = ($workingDays > 2) ? 's' : '';

                                    echo $workingDays . ' Day' . $s . '<br>' . '<strong>' . $workingHours . ' Hours' . '</strong>';
                                    ?>
                                </td>

                                <!-- Reason -->
                                <td class="text-center" >
                                    <?php // Client Hold/Reject/Cancel Reason ?>
                                    <?php if ($model->quotation_cancel_reason <> null) { ?>
                                        <a class="reasonview" style="cursor: pointer" data_id="<?= $model->id ?>"> <i class="fas fa-eye"></i> </a>
                                    <?php } //echo trim($cancelReasonArr->title); ?>
                                </td>

                                <!-- Contact Person Name -->
                                <td>
                                    <?php
                                    $contact_person = trim($clientInquiryContactsArr->firstname . ' ' . $clientInquiryContactsArr->lastname);
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($contact_person, 6); ?>
                                </td>

                                <!-- Landline -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->phone1); ?>
                                </td>

                                <!-- Mobile -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->mobile); ?>
                                </td>

                                <td class="noprint actions">
                                    <div class="btn-group flex-wrap">
                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <a class="dropdown-item text-1"
                                               href="<?= yii\helpers\Url::to(['crm-quotations/step_0', 'id' => $model->id]) ?>"
                                               title="Step 1" data-pjax="0">
                                                <i class="fas fa-edit"></i> Edit
                                            </a>
                                        </div>
                                    </div>
                                </td>


                                <!-- WM Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Reference -->
                                <td>
                                    <?php $client_ref = $model->client_reference;
                                    echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client_ref, 3);
                                    ?>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <?php if (count($dataProvider->getModels()) > 0) { ?>
                        <tfoot>

                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <!-- <th> </th> -->
                        <th> Total Fee : </th>
                        <th style="text-align:right !important; padding-right: 10px !important;"> <?php echo number_format($fin_appr_fee); ?></th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>

                        </tfoot>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>

    <?php
    $this->registerJs('
     
                
        var dataTable = $("#quotations_cancelled").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }],
            order: [[0, "desc"]]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $("#no_of_properties").on("keyup", function () {
            dataTable.column(3).search(this.value).draw();
          });

          //for date search inquiry_date
          $("#inquiry_date").on("change", function () {
            if($("#inquiry_date").val() != ""){
              var format1 = moment($("#inquiry_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search approval_date
          $("#approval_date").on("change", function () {
            if($("#approval_date").val() != ""){
              var format1 = moment($("#approval_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                  url: "' . yii\helpers\Url::to(['suggestion/getclient']) . '/"+this.value,
                  method: "post",
                  dataType: "html",
                  success: function(data) {
                      console.log(data);
                      dataTable.search(data).draw();
                  },
                  // error: bbAlert
                  error: function(jqXHR, textStatus, errorThrown) {
                    // Handle the error here
                    dataTable.search("").draw();
                  }
                });
            });
                
          $("#pending_received_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_received_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_received_wrapper").append(customFooter);


          $(".reasonview").on("click", function () {           
            $.ajax({
                url: "' . yii\helpers\Url::to(['crm-quotations/reason-view-data']) . '?id="+$(this).attr(\'data_id\'),
                method: "post",
                dataType: "html",
                success: function(data) {
                  
                    $("#model_html2").html(data);
                    $("#reasonViewModal").show();
                    
                },
                error: bbAlert
            });
    
          });
          $(".close,.close_footer").on("click", function () {
              $("#model_html2").html("");
              $("#editModal").hide();
              $("#reasonViewModal").hide();
          });


          $(".div1").daterangepicker({
            autoUpdateInput: false,
              locale: {
                format: "YYYY-MM-DD"
              }
          });
         
          $(".div1").on("apply.daterangepicker", function(ev, picker) {
             $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
          });
          $(document).ready(function() {
              // Code to execute when the document is ready
              var period = $("#crmquotationssearch-time_period").val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $("#date_range_array").hide();
              }
              
              // Change event handler for select input
              $("#crmquotationssearch-time_period").on("change", function() {
                  var period = $(this).val();
                  
                  if (period == 9) {
                      $(".div1").prop("required", true);
                      $("#date_range_array").show();
                  } else {
                      $(".div1").prop("required", false);
                      $(".div1").val("");
                      $("#date_range_array").hide();
                  }
              });
          });
          
          
    ');
    ?>
<?php } ?>

<?php if ($searchModel->page_title == "All Quotation Approved") { ?>
    <div class="quotations_accepted-index col-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h3 class="card-title "><strong><?= $searchModel->page_title ?></strong></span></h3>
                    </div>
                </div>
            </div>

            <div class="valuation-search card-body mb-0 pb-0">
                <?php $form = ActiveForm::begin([
                    'action' => ['dashboard-index'],
                    'method' => 'get',
                ]); ?>
                <?= $form->field($searchModel, 'widget_type')->hiddenInput(['value' => 'all'])->label(false); ?>
                <?= $form->field($searchModel, 'status_type')->hiddenInput(['value' => 'quotation_accepted'])->label(false); ?>
                <div class="row">
                    <div class="col-sm-4 text-left">
                        <?php
                        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->reportPeriod,
                            'options' => ['placeholder' => 'Time Frame ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>
                    </div>
                    <div class="col-sm-4" id="date_range_array" <?php if ($custom == 0) {
                        echo 'style="display:none;"';
                    } ?>>
                        <div class="" id="end_date_id">
                            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class' => 'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="text-left">
                            <div class="form-group">
                                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>


            <div class="card-body">
                <table id="quotations_accepted" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>
                        <th style="min-width: 86px; max-width: 86px;">Inquiry <br>Date
                            <input type="date" id="inquiry_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <?php // Verified Date (Approved Date label changed to Verified) ?>
                        <th style="min-width: 86px; max-width: 86px;">Verified <br>Date
                            <input type="date" id="approval_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 130px; max-width: 130px;">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder' => 'client']); ?>
                        </th>

                        <th style="min-width: 70px; max-width: 70px;">No. of <br>Properties
                            <input type="text" id="no_of_properties" class="custom-search-input form-control" placeholder="No. of Properties">
                        </th>

                        <!-- <th style="min-width: 70px; max-width: 70px;">Sourcing <br>RM
                          <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                      </th> -->

                        <th style="min-width: 76px; max-width: 76px;">Approved <br>Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th style="min-width: 86px; max-width: 86px;">TAT <br>Taken
                            <input type="text" class="custom-search-input form-control" placeholder="TAT Taken">
                        </th>

                        <th style="min-width: 180px; max-width: 180px;">Contact <br>Person
                            <input type="text" class="custom-search-input form-control" placeholder="Contact Person">
                        </th>

                        <th style="min-width: 90px; max-width: 90px;">Contact <br>Landline
                            <input type="text" class="custom-search-input form-control" placeholder="Landline">
                        </th>

                        <th style="min-width: 96px; max-width: 96px;">Contact <br>Mobile
                            <input type="text" class="custom-search-input form-control" placeholder="Mobile">
                        </th>

                        <th>Action</th>

                        <th style="min-width: 120px; max-width: 120px;">Windmills <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="status">
                        </th>

                        <th style="min-width: 130px; max-width: 130px; ">Client <br>Reference
                            <input type="text" class="custom-search-input form-control" placeholder="status">
                        </th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (count($dataProvider->getModels()) > 0) {
                        // dd(count($dataProvider->getModels()));
                        foreach ($dataProvider->getModels() as $model) {
                            $clientInquiryContactsArr = \app\models\User::find()
                                ->select([
                                    \app\models\User::tableName() . ".id",
                                    \app\models\User::tableName() . ".firstname",
                                    \app\models\User::tableName() . ".lastname",
                                    \app\models\UserProfileInfo::tableName() . ".mobile",
                                    \app\models\UserProfileInfo::tableName() . ".phone1",
                                ])
                                ->where(['company_id' => $model->client_name])
                                ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                                ->andWhere([\app\models\UserProfileInfo::tableName() . ".primary_contact" => 1])
                                ->one();

                            $app_fee_fin += $model->final_quoted_fee;
                            // dd($model);
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?php
                                    if (isset($model->instruction_date)) {
                                        echo date('d-M-Y', strtotime($model->instruction_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->inquiry_received_time)) {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm(trim($model->inquiry_received_time));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <?php // Verified Date (Approved Date label changed to Verified) ?>
                                <td>
                                    <?php
                                    if (isset($model->approved_date)) {
                                        echo date('d-M-Y', strtotime($model->approved_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->approved_date)) {
                                        echo date('h:i A', strtotime($model->approved_date));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?php
                                    echo $model->client->nick_name;
                                    ?>
                                </td>

                                <!-- No. of Properties -->
                                <td>
                                    <?php
                                    echo $model->no_of_properties;
                                    ?>
                                </td>

                                <!-- Sourcing RM -->
                                <!-- <td>
                                    <?php

                                ?>
                                </td> -->

                                <!-- Approved fee -->
                                <td class="fee_column"><?php echo trim(number_format($model->final_quoted_fee)); ?></td>

                                <!-- TAT Taken -->
                                <td>
                                    <?php
                                    $startDate = $model->instruction_date . ' ' . $model->inquiry_received_time;
                                    $endDate = $model->quotation_accepted_date;
                                    // $endDate = date("Y-m-d  H:i:s");

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));
                                    $s = ($workingDays > 2) ? 's' : '';

                                    echo $workingDays . ' Day' . $s . '<br>' . '<strong>' . $workingHours . ' Hours' . '</strong>';
                                    ?>
                                </td>

                                <!-- Contact Person Name -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->firstname . ' ' . $clientInquiryContactsArr->lastname); ?>
                                </td>

                                <!-- Landline -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->phone1); ?>
                                </td>

                                <!-- Mobile -->
                                <td>
                                    <?php echo trim($clientInquiryContactsArr->mobile); ?>
                                </td>

                                <td class="noprint actions">
                                    <div class="btn-group flex-wrap">
                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <a class="dropdown-item text-1"
                                               href="<?= yii\helpers\Url::to(['crm-quotations/step_0', 'id' => $model->id]) ?>"
                                               title="Step 1" data-pjax="0">
                                                <i class="fas fa-edit"></i> Edit
                                            </a>
                                        </div>
                                    </div>
                                </td>


                                <!-- WM Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Reference -->
                                <td>
                                    <?php echo $model->client_reference; ?>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>

                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <!-- <th> </th> -->
                    <th> Total Fee : </th>
                    <th style="text-align:right !important; padding-right: 10px !important;"> <?php echo number_format($app_fee_fin); ?> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>

                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php
    $this->registerJs('
     
                
        var dataTable = $("#quotations_accepted").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }],
            order: [[0, "desc"]]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $("#no_of_properties").on("keyup", function () {
            dataTable.column(3).search(this.value).draw();
          });

          //for date search inquiry_date
          $("#inquiry_date").on("change", function () {
            if($("#inquiry_date").val() != ""){
              var format1 = moment($("#inquiry_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search approval_date
          $("#approval_date").on("change", function () {
            if($("#approval_date").val() != ""){
              var format1 = moment($("#approval_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                  url: "' . yii\helpers\Url::to(['suggestion/getclient']) . '/"+this.value,
                  method: "post",
                  dataType: "html",
                  success: function(data) {
                      console.log(data);
                      dataTable.search(data).draw();
                  },
                  // error: bbAlert
                  error: function(jqXHR, textStatus, errorThrown) {
                    // Handle the error here
                    dataTable.search("").draw();
                  }
                });
            });
                
          $("#pending_received_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_received_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_received_wrapper").append(customFooter);

          $(".div1").daterangepicker({
            autoUpdateInput: false,
              locale: {
                format: "YYYY-MM-DD"
              }
          });
         
          $(".div1").on("apply.daterangepicker", function(ev, picker) {
             $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
          });
          $(document).ready(function() {
              // Code to execute when the document is ready
              var period = $("#crmquotationssearch-time_period").val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $("#date_range_array").hide();
              }
              
              // Change event handler for select input
              $("#crmquotationssearch-time_period").on("change", function() {
                  var period = $(this).val();
                  
                  if (period == 9) {
                      $(".div1").prop("required", true);
                      $("#date_range_array").show();
                  } else {
                      $(".div1").prop("required", false);
                      $(".div1").val("");
                      $("#date_range_array").hide();
                  }
              });
          });
          
          
    ');
    ?>
<?php } ?>

<?php if ($searchModel->page_title == "Total TAT Performance") { ?>
    <div class="inquiry-overview-index col-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h3 class="card-title "><strong><?= $searchModel->page_title ?></strong></span></h3>
                    </div>
                </div>
            </div>

            <div class="valuation-search card-body mb-0 pb-0">
                <?php $form = ActiveForm::begin([
                    'action' => ['dashboard-index'],
                    'method' => 'get',
                ]); ?>
                <?= $form->field($searchModel, 'widget_type')->hiddenInput(['value' => 'all'])->label(false); ?>
                <?= $form->field($searchModel, 'status_type')->hiddenInput(['value' => 'tat_performance'])->label(false); ?>
                <div class="row">
                    <div class="col-sm-4 text-left">
                        <?php
                        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->reportPeriod,
                            'options' => ['placeholder' => 'Time Frame ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>
                    </div>
                    <div class="col-sm-4" id="date_range_array" <?php if ($custom == 0) {
                        echo 'style="display:none;"';
                    } ?>>
                        <div class="" id="end_date_id">
                            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class' => 'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="text-left">
                            <div class="form-group">
                                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div class="card-body">
                <table id="inquiry_overview" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>



                        <th style="min-width: 90px; max-width: 90px;">Inquiry <br>Date
                            <input type="date" id="inquiry_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 120px; max-width: 120px;">Recommended <br>TAT
                            <input type="date" id="recommended_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <?php // Verified Date (Approved Date label changed to Verified) ?>
                        <th style="min-width: 120px; max-width: 120px;">Verified <br>TAT
                            <input type="date" id="approved_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <?php // Approved Date (Accepted Date label changed to Approved) ?>
                        <th style="min-width: 120px; max-width: 120px;">Approved <br>TAT
                            <input type="date" id="accepted_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 120px; max-width: 120px;">TOE Accepted <br>TAT
                            <input type="date" id="toe_accepted_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 120px; max-width: 120px;">Payment Rece <br>TAT
                            <input type="date" id="payment_received_date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th style="min-width: 120px; max-width: 120px;">Total <br>TAT
                            <input type="text" class="custom-search-input form-control" placeholder="TAT Taken">
                        </th>

                        <th style="min-width: 140px; max-width: 140px;">Reference <br>No
                            <input type="text" class="custom-search-input form-control" placeholder="Reference">
                        </th>

                        <th style="min-width: 100px; max-width: 100px;">Proposed <br>By
                            <input type="text" class="custom-search-input form-control" placeholder="Proposed By">
                        </th>

                        <th style="min-width: 100px; max-width: 100px;">Status
                            <input type="text" class="custom-search-input form-control" placeholder="Status">
                        </th>

                        <th>Action</th>




                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (count($dataProvider->getModels()) > 0) {
                        // dd(count($dataProvider->getModels()));
                        $final_approved_fee = 0;
                        foreach ($dataProvider->getModels() as $model) {
                            $clientInquiryContactsArr = \app\models\User::find()
                                ->select([
                                    \app\models\User::tableName() . ".id",
                                    \app\models\User::tableName() . ".firstname",
                                    \app\models\User::tableName() . ".lastname",
                                    \app\models\UserProfileInfo::tableName() . ".mobile",
                                    \app\models\UserProfileInfo::tableName() . ".phone1",
                                ])
                                ->where(['company_id' => $model->client_name])
                                ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                                ->andWhere([\app\models\UserProfileInfo::tableName() . ".primary_contact" => 1])
                                ->one();

                            $final_approved_fee += $model->final_quoted_fee;
                            // dd($model);
                            ?>
                            <tr class="active">



                                <!-- Instruction -->
                                <td>
                                    <?php
                                    if (isset($model->instruction_date)) {
                                        echo date('d-M-Y', strtotime($model->instruction_date));
                                    }
                                    echo "<br>";
                                    if (isset($model->inquiry_received_time)) {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm(trim($model->inquiry_received_time));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Recommended TAT Taken -->
                                <td>
                                    <?php
                                    $startDate = $model->instruction_date . ' ' . $model->inquiry_received_time;
                                    // $endDate = ($model->quotation_recommended_date <> null) ? $model->quotation_recommended_date : $model->instruction_date . ' ' . $model->inquiry_received_time;
                                    // $endDate = date("Y-m-d  H:i:s");
                                    $endDate = $model->quotation_recommended_date;

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));

                                    //$s = ($workingDays > 2) ? 's' : '';
                                    // echo $workingDays.' Day'.$s;

                                    if (isset($endDate)) {
                                        echo date('d-M-Y', strtotime($endDate));
                                    }
                                    echo "<br>";
                                    if (isset($endDate)) {
                                        echo date('h:i A', strtotime($endDate));
                                    } else {
                                        echo "";
                                    }

                                    if (isset($endDate)) {
                                        echo '<br>';
                                        echo '<strong>' . $workingHours . ' Hours' . '</strong>';
                                        $recommTAT_sum += $workingHours;
                                        $recRowCount += 1;
                                    } else {
                                        echo "";
                                    }


                                    ?>
                                </td>

                                <?php // Verified TAT (Approved TAT label changed to Verified) ?>
                                <td>
                                    <?php
                                    $startDate = $model->quotation_recommended_date;
                                    if($model->approved_date <> null){
                                        $endDate = $model->approved_date;
                                    }else {
                                        $endDate = $model->quotation_sent_date;
                                    }

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));

                                    // $s = ($workingDays > 2) ? 's' : '';
                                    // echo $workingDays.' Day'.$s.'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';

                                    if (isset($endDate)) {
                                        echo date('d-M-Y', strtotime($endDate));
                                    }
                                    echo "<br>";
                                    if (isset($endDate)) {
                                        echo date('h:i A', strtotime($endDate));
                                    } else {
                                        echo "";
                                    }

                                    if (isset($endDate)) {
                                        echo '<br>';
                                        echo '<strong>' . $workingHours . ' Hours' . '</strong>';
                                        $approvedTAT_sum += $workingHours;
                                        $appRowCount += 1;
                                    } else {
                                        echo "";
                                    }


                                    ?>
                                </td>

                                <!-- Accepted TAT Taken -->
                                <?php // Approved TAT Taken (Accepted TAT label changed to Approved) ?>
                                <td>
                                    <?php

                                    // $startDate = $model->approved_date;
                                    if($model->approved_date <> null){
                                        $startDate = $model->approved_date;
                                    }else {
                                        $startDate = $model->quotation_sent_date;
                                    }
                                    if($model->quotation_accepted_date <> null){
                                        $endDate = $model->quotation_accepted_date;
                                    }elseif($model->toe_sent_date <> null){
                                        $endDate = $model->toe_sent_date;
                                    }
                                    else{
                                        $endDate = $model->toe_approved_date;
                                    }


                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));

                                    // $s = ($workingDays > 2) ? 's' : '';
                                    // echo $workingDays.' Day'.$s.'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';

                                    if (isset($endDate)) {
                                        echo date('d-M-Y', strtotime($endDate));
                                    }
                                    echo "<br>";
                                    if (isset($endDate)) {
                                        echo date('h:i A', strtotime($endDate));
                                    } else {
                                        echo "";
                                    }

                                    if (isset($endDate)) {
                                        echo '<br>';
                                        echo '<strong>' . $workingHours . ' Hours' . '</strong>';
                                        $acceptedTAT_sum += $workingHours;
                                        $accRowCount += 1;
                                    } else {
                                        echo "";
                                    }

                                    ?>
                                </td>


                                <!-- TOE Accepted TAT Taken -->
                                <td>
                                    <?php
                                    // $startDate = $model->quotation_accepted_date;
                                    if($model->quotation_accepted_date <> null){
                                        $startDate = $model->quotation_accepted_date;
                                    }elseif($model->toe_sent_date <> null){
                                        $startDate = $model->toe_sent_date;
                                    }else{
                                        $startDate = $model->toe_approved_date;
                                    }
                                    $endDate = $model->toe_signed_and_received_date;

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);


                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));

                                    // $s = ($workingDays > 2) ? 's' : '';
                                    // echo $workingDays.' Day'.$s.'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';

                                    if (isset($endDate)) {
                                        echo date('d-M-Y', strtotime($endDate));
                                    }
                                    echo "<br>";
                                    if (isset($endDate)) {
                                        echo date('h:i A', strtotime($endDate));
                                    } else {
                                        echo "";
                                    }

                                    if (isset($endDate)) {
                                        echo '<br>';
                                        echo '<strong>' . $workingHours . ' Hours' . '</strong>';
                                        $toeAcceptedTAT_sum += $workingHours;
                                        $toeAccRowCount += 1;
                                    } else {
                                        echo "";
                                    }

                                    ?>
                                </td>

                                <!-- Payment received TAT Taken -->
                                <td>
                                    <?php
                                    //$startDate = $model->instruction_date .' ' .$model->inquiry_received_time;
                                    $startDate = $model->toe_signed_and_received_date;
                                    $endDate = $model->payment_received_date;

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);


                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));

                                    // $s = ($workingDays > 2) ? 's' : '';
                                    // echo $workingDays.' Day'.$s.'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';

                                    if (isset($endDate)) {
                                        echo date('d-M-Y', strtotime($endDate));
                                    }
                                    echo "<br>";
                                    if (isset($endDate)) {
                                        echo date('h:i A', strtotime($endDate));
                                    } else {
                                        echo "";
                                    }

                                    if (isset($endDate)) {
                                        echo '<br>';
                                        echo '<strong>' . $workingHours . ' Hours' . '</strong>';
                                    } else {
                                        echo "";
                                    }

                                    if($endDate){
                                        $paymentTAT_sum += $workingHours;
                                        $payRowCount += 1;
                                    }
                                    ?>
                                </td>



                                <!-- Total TAT Taken -->
                                <td>
                                    <?php
                                    $startDate = $model->instruction_date . ' ' . $model->inquiry_received_time;
                                    $endDate = $model->payment_received_date;

                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);


                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    $workingDays = abs(number_format(($workingHours / 8.5), 1));

                                    $s = ($workingDays > 2) ? 's' : '';
                                    echo $workingDays . ' Day' . $s . '<br>' . '<strong>' . $workingHours . ' Hours' . '</strong>';

                                    if($endDate){
                                        $totalTAT_sum += $workingHours;
                                        $totTatRowCount += 1;
                                    }

                                    ?>
                                </td>

                                <!-- WM Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Proposed By -->
                                <td>
                                    <?php echo trim($model->userData->lastname); ?>
                                </td>

                                <!-- Status -->
                                <td><?php echo Yii::$app->crmQuotationHelperFunctions->quotationStatusListArrLabelTextWithColor[$model->quotation_status] ?></td>



                                <td class="noprint actions text-center ">
                                    <div class="btn-group flex-wrap">
                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <a class="dropdown-item text-1"
                                               href="<?= yii\helpers\Url::to(['crm-quotations/step_0', 'id' => $model->id]) ?>"
                                               title="Step 1" data-pjax="0">
                                                <i class="fas fa-edit"></i> Edit
                                            </a>
                                        </div>
                                    </div>
                                </td>




                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <?php if (count($dataProvider->getModels()) > 0) { ?>
                        <tfoot>

                        <th>Average: </th>
                        <th><?= number_format(($recommTAT_sum/$recRowCount),1).' Hour(s)' ?></th>
                        <th><?= number_format(($approvedTAT_sum/$appRowCount),1).' Hour(s)' ?></th>
                        <th><?= number_format(($acceptedTAT_sum/$accRowCount),1).' Hour(s)' ?></th>
                        <th><?= number_format(($toeAcceptedTAT_sum/$toeAccRowCount),1).' Hour(s)' ?></th>
                        <th><?= number_format(($paymentTAT_sum/$payRowCount),1).' Hour(s)' ?></th>
                        <th><?= number_format(($totalTAT_sum/$total_rows),1).' Hour(s)' ?> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th></th>
                        </tfoot>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>

    <?php
    $this->registerJs('
     
                    
        $(".reasonview").on("click", function () {           
          $.ajax({
              url: "' . yii\helpers\Url::to(['crm-quotations/reason-view-data']) . '?id="+$(this).attr(\'data_id\'),
              method: "post",
              dataType: "html",
              success: function(data) {
                
                  $("#model_html2").html(data);
                  $("#reasonViewModal").show();
                  
              },
              error: bbAlert
          });

        });
        $(".close,.close_footer").on("click", function () {
            $("#model_html2").html("");
            $("#editModal").hide();
            $("#reasonViewModal").hide();
        });
                
        var dataTable = $("#inquiry_overview").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [{
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }],
            order: [[0, "desc"]]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $("#no_of_properties").on("keyup", function () {
            dataTable.column(6).search(this.value).draw();
          });

          //for date search inquiry_date
          $("#inquiry_date").on("change", function () {
            if($("#inquiry_date").val() != ""){
              var format1 = moment($("#inquiry_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search recommended_date
          $("#recommended_date").on("change", function () {
            if($("#recommended_date").val() != ""){
              var format1 = moment($("#recommended_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search approved_date
          $("#approved_date").on("change", function () {
            if($("#approved_date").val() != ""){
              var format1 = moment($("#approved_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(2).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search accepted_date
          $("#accepted_date").on("change", function () {
            if($("#accepted_date").val() != ""){
              var format1 = moment($("#accepted_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(3).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search toe_accepted_date
          $("#toe_accepted_date").on("change", function () {
            if($("#toe_accepted_date").val() != ""){
              var format1 = moment($("#toe_accepted_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(4).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          //for date search payment_received_date
          $("#payment_received_date").on("change", function () {
            if($("#payment_received_date").val() != ""){
              var format1 = moment($("#payment_received_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(4).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                  url: "' . yii\helpers\Url::to(['suggestion/getclient']) . '/"+this.value,
                  method: "post",
                  dataType: "html",
                  success: function(data) {
                      console.log(data);
                      dataTable.search(data).draw();
                  },
                  // error: bbAlert
                  error: function(jqXHR, textStatus, errorThrown) {
                    // Handle the error here
                    dataTable.search("").draw();
                  }
                });
            });
                
          $("#pending_received_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_received_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_received_wrapper").append(customFooter);


          $(".div1").daterangepicker({
            autoUpdateInput: false,
              locale: {
                format: "YYYY-MM-DD"
              }
          });
         
          $(".div1").on("apply.daterangepicker", function(ev, picker) {
             $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
          });
          $(document).ready(function() {
              // Code to execute when the document is ready
              var period = $("#crmquotationssearch-time_period").val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $("#date_range_array").hide();
              }
              
              // Change event handler for select input
              $("#crmquotationssearch-time_period").on("change", function() {
                  var period = $(this).val();
                  
                  if (period == 9) {
                      $(".div1").prop("required", true);
                      $("#date_range_array").show();
                  } else {
                      $(".div1").prop("required", false);
                      $(".div1").val("");
                      $("#date_range_array").hide();
                  }
              });
          });
          
          
          
    ');
    ?>

<?php } ?>

<div class="modal" id="reasonViewModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= $reason; ?> Reason</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="model_html2">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close_footer" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
