<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use app\assets\SortBootstrapAsset;

SortBootstrapAsset::register($this);

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);

// use app\assets\DateRangePickerAsset2;

// DateRangePickerAsset2::register($this);

$this->title = Yii::t('app', $searchModel->page_title);


$clientsArr = ArrayHelper::map(\app\models\Company::find()
->select([
  'id','title',
  'cname_with_nick' => 'CONCAT(title," ",nick_name)',
])
  ->where(['status' => 1])
  ->andWhere([
      'or',
      ['data_type' => 0],
      ['data_type' => null],
  ])
  ->orderBy(['title' => SORT_ASC,])
  ->all(), 'id', 'title');
  $clientsArr = ['' => 'select'] + $clientsArr;


$reasonArr = ArrayHelper::map(\app\models\QuotationCancelReasons::find()->orderBy([
  'title' => SORT_ASC,
])->all(), 'title', 'title');
$reasonArr = ['' => 'Select'] + $reasonArr;

$statusArr = Yii::$app->crmQuotationHelperFunctions->quotationStatusListTextArr;


$citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
$citiesArr = ['' => 'select'] + $citiesArr;

$list_columns = Yii::t('app', $searchModel->list_columns);


if($list_columns == 'quotation_onhold') { $reason = "On Hold"; }
elseif($list_columns == 'quotation_cancelled') { $reason = "Cancel";}
elseif($list_columns == 'quotation_regretted') { $reason = "Regretted";} 


?>

<style>
  .dataTable th {
    color: #0056b3;
    font-size: 15px;
    text-align: left !important;
    /* padding-right: 30px !important; */
  }

  .table th,  .table td { padding: 3px 0px 3px 0px;}

  .dataTable td {
    font-size: 16px;
    text-align: left;
    /* padding-right: 50px; */
    /* padding-top: 3px; */
    padding-left: 3px;
    vertical-align:middle;
  }

  .content-header h1 {
    font-size: 16px !important;
  }

  .content-header .row {
    margin-bottom: 0px !important;
  }

  .content-header {
    padding: 0px !important;
  }

  .number_row {
    text-align: left !important;
    padding-left: 10px !important;
  }
  .fee_column {
    text-align: right !important;
    padding-right: 10px !important;
  }

  .table.dataTable thead > tr > th.sorting { padding-left: 2px; padding-right: 5px !important; }
  .table.dataTable thead>tr>th.sorting:before { bottom: 75% !important;}
  .table.dataTable thead>tr>th.sorting:after { top: 25% !important;}

  
</style>

<div class="bank-revenue-index col-12">
  <div class="card card-outline card-info">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <h3 class="card-title "><strong><?= $searchModel->page_title ?></strong></span></h3>
            </div>
        </div>
    </div>
    


    <div class="valuation-search card-body mb-0 pb-0">

      <?php $form = ActiveForm::begin([
          'action' => ['dashboard-index'],
          'method' => 'get',
      ]); ?>
      <?= $form->field($searchModel, 'widget_type')->hiddenInput(['value' => 'pending'])->label(false); ?>
      <?= $form->field($searchModel, 'status_type')->hiddenInput(['value' => $list_columns])->label(false); ?>
      
      <div class="row">
        
        
        <div class="col-sm-4 text-left">
          <?php
          echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
              'data' => Yii:: $app->appHelperFunctions->reportPeriod,
              'options' => ['placeholder' => 'Time Frame ...'],
              'pluginOptions' => [
                'allowClear' => true
              ],
              ])->label(false);
              ?>
        </div>
        <div class="col-sm-4" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
          <div class="" id="end_date_id">
          
            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>

          </div>
        </div>
        <div class="col-sm-4">
          <div class="text-left">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            </div>
          </div>
        </div>
      </div>
      
          
      
        <?php ActiveForm::end(); ?>
    </div>


    <div class="card-body"  style="width:100% !important; overflow-x: scroll;">
      <table id="bank-revenue" class="table table-striped dataTable table-responsive" >
        <thead>
          <tr>

            <th  style="min-width: 86px; max-width: 86px;" id="inquiry_date_th">Inquiry <br>Date
              <input type="date" id="inquiry_date" class="custom-search-input form-control" placeholder="Inquiry Date">
            </th>

            <?php if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 86px; max-width: 86px;" id="target_date_th">Target <br>Date
                <input type="date" id="target_date" class="custom-search-input form-control"
                  placeholder="Target Date">
              </th>
            <?php } ?>

            <?php if ($list_columns == 'document_requested') { ?>
              <th  style="min-width: 86px; max-width: 86px;" id="doc_req_date_th">Doc. Req. <br>Date
                <input type="date" id="doc_req_date" class="custom-search-input form-control" placeholder="Documents Date">
              </th>
            <?php } ?>

            <th  style="min-width: 86px; max-width: 86px;" id="approval_date_th" >Verified <br>Date
              <input type="date" id="approval_date"  class="custom-search-input form-control" placeholder="Verified Date">
            </th>

            <?php if ($list_columns == 'performance_overview') { ?>
              <th  style="min-width: 86px; max-width: 86px;" id="acceptance_date_th" >Approved <br>Date
                <input type="date" id="acceptance_date"  class="custom-search-input form-control" placeholder="Approved Date">
              </th>
            <?php } ?>

            <?php if ($list_columns == 'performance_overview') { ?>
              <th  style="min-width: 86px; max-width: 86px;" id="toe_signed_date_th" >TOE Signed <br>Date
                <input type="date" id="toe_signed_date"  class="custom-search-input form-control" placeholder="TOE Signed Date">
              </th>
            <?php } ?>

            <th style="min-width: 120px; max-width: 120px;">Client <br>(Nick Name)
              <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder' => 'Client']); ?>
            </th>

            <th style="min-width: 70px; max-width: 70px;" id="no_of_properties_th">No. of <br>Properties
              <input type="text" id="no_of_properties" class="custom-search-input form-control" placeholder="Number of Properties">
            </th>

            <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'document_requested') { ?>
              <th style="min-width: 86px; max-width: 88px;">Property <br>Type
                <input type="text" class="custom-search-input form-control" placeholder="Property">
              </th>
            <?php } ?>

            <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'document_requested') { ?>
              <th style="min-width: 84px; max-width: 86px;">Community
                <input type="text" class="custom-search-input form-control" placeholder="Community">
              </th>
            <?php } ?>

            <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'document_requested') { ?>
              <th style="min-width: 70px; max-width: 70px;">City
                <input type="text" class="custom-search-input form-control" placeholder="City">
              </th>
            <?php } ?>

            <?php /* if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance' || $list_columns == 'document_requested' || $list_columns == 'quotation_accepted' || $list_columns == 'toe_sent' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled') { ?>
              <th style="min-width: 70px; max-width: 70px;">Sourcing <br>RM
                <input type="text" class="custom-search-input form-control" placeholder="Sourcing RM">
              </th>
            <?php } */ ?>

            <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px;">Proposed <br>By
                <input type="text" class="custom-search-input form-control" placeholder="Proposed by">
              </th>
            <?php } ?>

            <?php if ($list_columns == 'document_requested' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px;">Approved <br>By
                <input type="text" class="custom-search-input form-control" placeholder="Approved by">
              </th>
            <?php } ?>

            <?php if ($list_columns == 'quotation_approved' || $list_columns == 'document_requested' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px;">Maxima <br>Fee
                <input type="text" class="custom-search-input form-control" placeholder="Recommended TAT">
              </th>
            <?php } ?>

            <?php if ($list_columns == 'quotation_approved' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px;">Highest <br>Accepted
                <input type="text" class="custom-search-input form-control" placeholder="Last Approved Fee">
              </th>
            <?php } ?>

            <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px;">Recom. <br>Fee
                <input type="text" class="custom-search-input form-control" placeholder="Recom. Fee">
              </th>
            <?php } ?>

            <?php if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance' || $list_columns == 'quotation_approved' || $list_columns == 'quotation_accepted' || $list_columns == 'toe_sent' || $list_columns == 'toe_recommended' || $list_columns == 'toe_signed_and_received' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled') { ?>
              <th style="min-width: 70px; max-width: 70px;">Approved <br>Fee
                <input type="text" class="custom-search-input form-control" placeholder="Approved Fee">
              </th>
            <?php } ?>

            <?php /* if($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'quotation_accepted' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled' || $list_columns == 'tat_performance') { ?>
            <th style="min-width: 70px; max-width: 70px;">Client Required Fee
              <input type="text" class="custom-search-input form-control" placeholder="Client Required Fee">
            </th>
            <?php } */?>

            <?php /* if($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'quotation_accepted' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled' || $list_columns == 'tat_performance') { ?>
            <th style="min-width: 70px; max-width: 70px;">Revised Approved Fee
              <input type="text" class="custom-search-input form-control" placeholder="Revised Approved Fee">
            </th>
            <?php } */?>

            <!-- <th style="min-width: 70px; max-width: 70px;">Entered By
              <input type="text" class="custom-search-input form-control" placeholder="entered by">
            </th> -->
            <?php if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px;">Status
                <!-- <input type="text" class="custom-search-input form-control" placeholder="Status"> -->
                <?php echo Html::dropDownList('quotation_cancel_reason', null, $statusArr, ['class' => 'custom-search-input-reason form-control', 'id'=>'cancel_reason', 'placeholder' => 'Reason']); ?>
              </th>
            <?php } ?>

            <th style="min-width: 84px; max-width: 86px;">Total TAT <br>Taken
              <input type="text" class="custom-search-input form-control" placeholder="Total TAT">
            </th>

            <?php if ($list_columns == 'document_requested') { ?>
              <th style="min-width: 70px; max-width: 70px;">Document <br>Requested
                <input type="text" class="custom-search-input form-control" placeholder="Document Requested Button">
              </th>
            <?php } ?>

            <?php if ($list_columns == 'performance_overview' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled') { ?>
              <th style="min-width: 90px; max-width: 90px;"><?= $reason; ?> <br>Reason
              <?php echo Html::dropDownList('quotation_cancel_reason', null, $reasonArr, ['class' => 'custom-search-input-reason form-control', 'id'=>'cancel_reason', 'placeholder' => 'Reason']); ?>
              </th>
            <?php } ?>

            <th style="min-width: 150px; max-width: 150px;">Instructing <br>Person
              <input type="text" class="custom-search-input form-control" placeholder="Person">
            </th>
            <th style="min-width: 110px; max-width: 110px;">Instructing <br>Person Landline
              <input type="text" class="custom-search-input form-control" placeholder="Landline">
            </th>
            <th style="min-width: 120px; max-width: 120px;">Instructing <br>Person Mobile
              <input type="text" class="custom-search-input form-control" placeholder="Mobile">
            </th>

            <?php
            if (Yii::$app->user->id == 1 || Yii::$app->user->id == 14) {
              ?>
              <th>Action</th>
              <?php
            }
            ?>

            <th style="min-width: 120px; max-width: 120px;">Windmills <br>Reference
              <input type="text" class="custom-search-input form-control" placeholder="Windmills Reference">
            </th>
            <!-- <th style="min-width: 110px; max-width: 110px; ">Client <br>Reference
              <input type="text" class="custom-search-input form-control" placeholder="Client Reference">
            </th> -->



          </tr>
        </thead>
        <tbody>
          <?php
          if (count($dataProvider->getModels()) > 0) {

            $total_maxima_fee = 0;
            $total_highest_acceptance_fee = 0;
            $total_recomended_fee = 0;
            $total_approved_fee = 0;

            foreach ($dataProvider->getModels() as $model) { //dd($model);
          
              $hours_diff = Yii::$app->crmQuotationHelperFunctions->getHoursDiff($model);

              $total_maxima_fee += Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model);
              $total_highest_acceptance_fee += Yii::$app->crmQuotationHelperFunctions->getLast3MonthsApprovedFee($model);
              $total_recommended_fee += Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model);
              $total_approved_fee += $model->final_quoted_fee;

              // client inquirty contact person
              $clientInquiryContactsArr = \app\models\User::find()
                ->select([
                  \app\models\User::tableName() . ".id", \app\models\User::tableName() . ".firstname", \app\models\User::tableName() . ".lastname", \app\models\UserProfileInfo::tableName() . ".mobile", \app\models\UserProfileInfo::tableName() . ".phone1",
                ])
                ->where(['company_id' => $model->client_name])
                ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                ->andWhere([\app\models\UserProfileInfo::tableName() . ".primary_contact" => 1])
                ->one();

              // quotation cancel reason
              $cancelReasonArr = \app\models\QuotationCancelReasons::find()
                ->select([
                  \app\models\QuotationCancelReasons::tableName() . ".id", \app\models\QuotationCancelReasons::tableName() . ".title"])
                ->where([\app\models\QuotationCancelReasons::tableName() . ".id" => $model->quotation_cancel_reason])
                ->one();

              // // valuation created
              // $valuationsArr = \app\models\Valuation::find()
              //   ->select([\app\models\Valuation::tableName() . ".created_by"])
              //   ->where([\app\models\Valuation::tableName() . ".quotation_id" => $model->id])
              //   ->one();

              //   dd($valuationsArr);
              ?>
              <tr class="active">

                <td style="padding-left:3px;">
                  <?php // Inquiry Date ?>
                  <?php
                      if(isset($model->instruction_date))
                      {
                          echo date('d-M-Y', strtotime($model->instruction_date));
                      }
                      echo "<br>";
                      if(isset($model->inquiry_received_time))
                      {
                          echo Yii::$app->appHelperFunctions->formatTimeAmPm(trim($model->inquiry_received_time));
                      }else{
                          echo "";
                      }
                    ?>
                </td>

                <?php if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance') { ?>
                  <td>
                    <?php // Client Target Vauation Report Date ?>
                    <?php echo ($model->target_date <> null) ? date("d-M-Y", strtotime($model->target_date)) : ''; ?>
                  </td>
                <?php } ?>

                <?php if ($list_columns == 'document_requested') { ?>
                  <td>
                    <?php // Document Requested Date ?>
                    <?php
                      if(isset($model->document_requested_date))
                      {
                          echo date('d-M-Y', strtotime($model->document_requested_date));
                      }
                      echo "<br>";
                      if(isset($model->document_requested_date))
                      {
                          echo date('h:i A', strtotime($model->document_requested_date));
                      }else{
                          echo "";
                      }
                      ?>
                  </td>
                <?php } ?>

                <td>
                  <?php // Quotation Approval Date ?>
                  <?php
                      if(isset($model->approved_date))
                      {
                          echo date('d-M-Y', strtotime($model->approved_date));
                      }
                      echo "<br>";
                      if(isset($model->approved_date))
                      {
                          echo date('h:i A', strtotime($model->approved_date));
                      }else{
                          echo "";
                      }
                  ?>
                </td>

                <?php if ($list_columns == 'performance_overview') { ?>
                  <td>
                    <?php // Quotaition Acceptance Date ?>
                    <?php
                        if(isset($model->quotation_accepted_date))
                        {
                            echo date('d-M-Y', strtotime($model->quotation_accepted_date));
                        }
                        echo "<br>";
                        if(isset($model->quotation_accepted_date))
                        {
                            echo date('h:i A', strtotime($model->quotation_accepted_date));
                        }else{
                            echo "";
                        }
                    ?>
                  </td>
                <?php } ?>

                <?php if ($list_columns == 'performance_overview') { ?>
                  <td>
                    <?php // TOE Signed Date ?>
                    <?php
                        if(isset($model->toe_signed_and_received_date))
                        {
                            echo date('d-M-Y', strtotime($model->toe_signed_and_received_date));
                        }
                        echo "<br>";
                        if(isset($model->toe_signed_and_received_date))
                        {
                            echo date('h:i A', strtotime($model->toe_signed_and_received_date));
                        }else{
                            echo "";
                        }
                    ?>
                  </td>
                <?php } ?>

                <td>
                  <?php // Client (Nick Name) ?>
                  <?php echo trim($model->client->nick_name <> null) ? $model->client->nick_name : $model->client->title; ?>
                </td>

                <td class="number_row">
                  <?php // Number of Properties ?>
                  <?php echo ($model->no_of_properties <> null) ? $model->no_of_properties : ''; ?>
                </td>

                <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'document_requested') { ?>
                  <td>
                    <?php // Property ?>
                    <?php echo trim($model->zeroIndexProperty->building->property->title); ?>
                  </td>
                <?php } ?>

                <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'document_requested') { ?>
                  <td>
                    <?php // Community ?>
                    <?php echo trim($model->zeroIndexProperty->building->communities->title); ?>
                  </td>
                <?php } ?>

                <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'document_requested') { ?>
                  <td>
                    <?php // City ?>
                    <?php echo trim($model->zeroIndexProperty->building->cityName->title); ?>
                  </td>
                <?php } ?>

                <?php /* if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance' || $list_columns == 'document_requested' || $list_columns == 'quotation_accepted' || $list_columns == 'toe_sent' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled') { ?>
                  <td>
                    <?php // Sourcing RM ?>
                  </td>
                <?php } */ ?>

                <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'tat_performance') { ?>
                  <td>
                    <?php // Proposed by ?>
                    <?php echo trim($model->userData->lastname); ?>
                  </td>
                <?php } ?>

                <?php if ($list_columns == 'document_requested' || $list_columns == 'tat_performance') { ?>
                  <td>
                    <?php // Approved by ?>
                    <?php echo trim($model->approvedUserData->lastname); ?>
                  </td>
                <?php } ?>

                <?php if ($list_columns == 'quotation_approved' || $list_columns == 'document_requested' || $list_columns == 'tat_performance') { ?>
                  <td class="fee_column ">
                    <?php // Maxima Fee ?>
                    <?php echo number_format(Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model)); ?>
                  </td>
                <?php } ?>

                <?php if ($list_columns == 'quotation_approved' || $list_columns == 'tat_performance' || $list_columns == 'tat_performance') { ?>
                  <td class="fee_column">
                    <?php // Highest Accpted Fee ?>
                    <?php echo number_format(Yii::$app->crmQuotationHelperFunctions->getLast3MonthsApprovedFee($model)) ?>
                  </td>
                <?php } ?>

                <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'tat_performance') { ?>
                  <td class="fee_column">
                    <?php // Recommeded Fee ?>
                    <?php echo number_format(Yii::$app->crmQuotationHelperFunctions->getMaximaRecommendedFee($model)) ?>
                  </td>
                <?php } ?>

                <?php if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance' || $list_columns == 'quotation_approved' || $list_columns == 'quotation_accepted' || $list_columns == 'toe_sent' || $list_columns == 'toe_recommended' || $list_columns == 'toe_signed_and_received' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled') { ?>
                  <td class="fee_column" >
                    <?php // Approved Fee ?>
                    <?php echo trim(number_format($model->final_quoted_fee)); ?>
                  </td>
                <?php } ?>

                <?php /* if($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'quotation_accepted' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled' || $list_columns == 'tat_performance') { ?>
               <td class="number_row">
               <?php // Client Required Fee ?>
                 <?php echo trim(number_format($model->final_quoted_fee)); ?>
               </td>
               <?php } */?>

                <?php /* if($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'quotation_accepted' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled' || $list_columns == 'tat_performance') { ?>
               <td class="number_row">
                 <?php // Reviced approved fee ?>
               </td>
               <?php }*/?>

                <!-- <td>
                  <?php //echo trim($model->userData->lastname); ?>
                </td> -->

                <?php if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance') { ?>
                  <td>
                    <?php // Status ?>
                    <?php echo Yii::$app->crmQuotationHelperFunctions->quotationStatusListArrLabelTextWithColor[$model->quotation_status] ?>
                  </td>
                <?php } ?>

                <td>
                  <?php // Total TAT Taken ?>
                  <?php 
                    $startDate = $model->instruction_date .' ' .$model->inquiry_received_time;
                    $endDate = date("Y-m-d  H:i:s");



                    $startDateTime = new DateTime($startDate); // Start date and time
                    $endDateTime = new DateTime($endDate);


                    $workingHours= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                    $workingDays = abs(number_format(($workingHours/8.5),1));

                    $s = ($workingDays > 2) ? 's' : '';

                    echo $workingDays.' Day'.$s.'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';

                  ?> 
                </td>

                <?php if ($list_columns == 'document_requested') { ?>
                  <td class="text-center">
                    <?php // Docuement Requested ?>
                    <a class="edit" style="cursor: pointer" data_id="<?= $model->id  ?>"> <i class="fas fa-eye"></i> </a>
                  </td>
                <?php } ?>

                <?php if ($list_columns == 'performance_overview' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled') { ?>
                  <td class="text-center">
                    <?php // Client Hold/Reject/Cancel Reason ?>
                    <?php if ($model->quotation_cancel_reason <> null) { 
                      $resonShort = explode(':', trim($cancelReasonArr->title));
                      echo $resonShort[0].'...<br/>';
                      ?>
                      <a class="reasonview" style="cursor: pointer" data_id="<?= $model->id ?>"> <i class="fas fa-eye"></i> </a>
                    <?php } ?>
                  </td>
                <?php } ?>

                <td>
                  <?php // Client Inquiry Contact Person ?>
                  <?php echo trim($clientInquiryContactsArr->firstname . ' ' . $clientInquiryContactsArr->lastname); ?>
                </td>
                <td>
                  <?php // Client Inquiry Contact Person landline ?>
                  <?php //echo trim($clientInquiryContactsArr->phone1); ?>
                  <?php if($clientInquiryContactsArr->phone1 <> null) {echo Yii::$app->crmQuotationHelperFunctions->getNormalizeUaePhoneNumber($clientInquiryContactsArr->phone1);}
                   else { echo '(not set)'; } ?>
                </td>
                <td>
                  <?php // Client Inquiry Contact Person Mobile ?>
                  <?php //echo trim($clientInquiryContactsArr->mobile); ?>
                  <?php 
                        if($clientInquiryContactsArr->mobile <> null) {echo Yii::$app->crmQuotationHelperFunctions->getNormalizeUaePhoneNumber($clientInquiryContactsArr->mobile);}
                        else { echo '(not set)'; } ?>
                </td>

                <?php
                if (Yii::$app->user->id == 1 || Yii::$app->user->id == 14) {
                  ?>
                  <td class="noprint actions">
                    <?php // Action ?>

                    <div class="btn-group flex-wrap">
                      <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                        data-toggle="dropdown">
                        <span class="caret"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item text-1"
                          href="<?= yii\helpers\Url::to(['crm-quotations/step_0?id=' . $model->id]) ?>" title="Edit"
                          data-pjax="0">
                          <i class="fas fa-edit"></i> Edit
                        </a>
                      </div>
                    </div>
                  </td>
                  <?php
                }
                ?>

                <td>
                  <?php // Windmills Reference ?>
                  <?php echo trim(str_replace(' ', '', $model->reference_number)); ?>
                </td>
                <!-- <td style="overflow: hidden; text-overflow: ellipsis;white-space: nowrap; max-width: 110px;">
                  <?php // Client Reference ?>
                  <?php //$client_ref =  ($model->client_reference <> null) ? $model->client_reference : '(not set)';
                    //echo Yii::$app->crmQuotationHelperFunctions->getSubWords($client_ref, 3);
                  ?>
                  <?php //echo trim(str_replace(' ', '', $model->client_reference)); ?>
                </td> -->


              </tr>
              <?php
            }
          }
          ?>
        </tbody>
        <tfoot>
        <?php
          if (count($dataProvider->getModels()) > 0) { ?>
          <tr>
            <?php // Inquiry Date ?>
            <th style="min-width: 68px; max-width: 68px;"> </th>

            <?php // Client Target Vauation Report Date ?>
            <?php if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 68px; max-width: 68px;"> </th>
            <?php } ?>

            <?php // Document Requested Date ?>
            <?php if ($list_columns == 'document_requested') { ?>
              <th style="min-width: 70px; max-width: 70px;"> </th>
            <?php } ?>

            <?php // Quotation Approval Date ?>
            <th style="min-width: 68px; max-width: 68px;"></th>

            <?php // Quotaition Acceptance Date ?>
            <?php if ($list_columns == 'performance_overview') { ?>
              <th style="min-width: 75px; max-width: 75px;"></th>
            <?php } ?>

            <?php // TOE Signed Date ?>
            <?php if ($list_columns == 'performance_overview') { ?>
              <th style="min-width: 68px; max-width: 68px;"></th>
            <?php } ?>

            <?php // Client (Nick Name) ?>
            <th style="min-width: 70px; max-width: 70px;"></th>

            <?php // Number of Properties ?>
            <th style="min-width: 70px; max-width: 70px;">Total Fee:</th>

            <?php // Property ?>
            <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'document_requested') { ?>
              <th style="min-width: 70px; max-width: 70px;"></th>
            <?php } ?>

            <?php // Community ?>
            <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'document_requested') { ?>
              <th style="min-width: 80px; max-width: 80px;"></th>
            <?php } ?>

            <?php // City ?>
            <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'document_requested') { ?>
              <th style="min-width: 70px; max-width: 70px;"></th>
            <?php } ?>

            <?php // Sourcing RM ?>
            <?php /* if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance' || $list_columns == 'document_requested' || $list_columns == 'quotation_accepted' || $list_columns == 'toe_sent'|| $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled') { ?>
              <th style="min-width: 70px; max-width: 70px;"></th>
            <?php } */ ?>

            <?php // Proposed by ?>
            <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px;"></th>
            <?php } ?>

            <?php // Approved by ?>
            <?php if ($list_columns == 'document_requested' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px;"></th>
            <?php } ?>

            <?php // Maxima Fee ?>
            <?php if ($list_columns == 'quotation_approved' || $list_columns == 'document_requested' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px; text-align: right !important; padding-right: 10px;">
                <?php echo number_format($total_maxima_fee); ?>
              </th>
            <?php } ?>

            <?php // Highest Accpted Fee ?>
            <?php if ($list_columns == 'quotation_approved' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px; text-align: right !important; padding-right: 10px;">
                <?php echo number_format($total_highest_acceptance_fee); ?>
              </th>
            <?php } ?>

            <?php // Recommeded Fee ?>
            <?php if ($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px; text-align: right !important; padding-right: 10px;">
                <?php echo number_format($total_recommended_fee); ?>
              </th>
            <?php } ?>

            <?php // Approved Fee ?>
            <?php if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance' || $list_columns == 'quotation_approved' || $list_columns == 'quotation_accepted' || $list_columns == 'toe_sent' || $list_columns == 'toe_recommended' || $list_columns == 'toe_signed_and_received' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled') { ?>
              <th style="min-width: 70px; max-width: 70px; text-align: right !important; padding-right: 10px;">
                <?php echo number_format($total_approved_fee); ?>
              </th>
            <?php } ?>

            <?php // Client Required Fee ?>
            <?php /* if($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'quotation_accepted' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled' || $list_columns == 'tat_performance') { ?>
             <th style="min-width: 70px; max-width: 70px;"><?php echo number_format($total_maxima_fee); ?></th>
             <?php } */?>

            <?php // Reviced approved fee ?>
            <!-- <?php //if($list_columns == 'quotation_recommended' || $list_columns == 'quotation_approved' || $list_columns == 'quotation_accepted' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled' || $list_columns == 'tat_performance') { ?>
                  <th style="min-width: 70px; max-width: 70px;"><?php //echo number_format($total_revised_fee); ?></th>
                  <?php // } ?> -->

            <!-- <th style="min-width: 70px; max-width: 70px;"></th> -->

            <?php // Status ?>
            <?php if ($list_columns == 'performance_overview' || $list_columns == 'tat_performance') { ?>
              <th style="min-width: 70px; max-width: 70px;"></th>
            <?php } ?>

            <?php // Total TAT Taken ?>
            <th style="min-width: 70px; max-width: 70px;"></th>

            <?php // Docuement Requested ?>
            <?php if ($list_columns == 'document_requested') { ?>
              <th style="min-width: 70px; max-width: 70px;"></th>
            <?php } ?>

            <?php // Client Hold/Reject/Cancel Reason ?>
            <?php if ($list_columns == 'performance_overview' || $list_columns == 'quotation_onhold' || $list_columns == 'quotation_cancelled') { ?>
              <th style="min-width: 70px; max-width: 70px;"></th>
            <?php } ?>

            <?php // Client Inquiry Contact Person ?>
            <th style="min-width: 100px; max-width: 100px;"></th>
            <?php // Client Inquiry Contact Person landline ?>
            <th style="min-width: 70px; max-width: 70px;"></th>
            <?php // Client Inquiry Contact Person Mobile ?>
            <th style="min-width: 70px; max-width: 70px;"></th>

            <?php
            if (Yii::$app->user->id == 1 || Yii::$app->user->id == 14) {
              ?>
              <th></th>
              <?php
            }
            ?>

            <?php // Windmills Reference ?>
            <th style="min-width: 100px; max-width: 100px;"></th>
            <?php // Client Reference ?>
            <!-- <th style="min-width: 100px; max-width: 100px;"></th> -->
          </tr>

          <?php 
        } ?>


          <?php
          // if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
          ?>
          <!-- <th></th> -->
          <?php
          // }
          ?>

        </tfoot>
      </table>
    </div>
  </div>
</div>




<?php
$this->registerJs('
    
    
        // $("#bank-revenue").DataTable({
        //     order: [[0, "asc"]],
        //     pageLength: 50,
        //     searching: false,
        // });
        
         var dataTable = $("#bank-revenue").DataTable({
            order: [[0, "desc"]],
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu",
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }]
          });
        
          // $(".custom-search-input").on("change", function () {
          //   dataTable.search(this.value).draw();
          // });

          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $("#no_of_properties").on("keyup", function () {
            var columnNumber = getColumnNumberById("no_of_properties_th");
            dataTable.column(columnNumber).search(this.value).draw();
          });

          // inquiry_date
          $("#inquiry_date").on("change", function () {
            // dataTable.search(this.value).draw();
            if($("#inquiry_date").val() != ""){
              var columnNumber = getColumnNumberById("inquiry_date_th");
              var format1 = moment($("#inquiry_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(columnNumber).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          // target_date
          $("#target_date").on("change", function () {
            // dataTable.search(this.value).draw();
            if($("#target_date").val() != ""){
              var columnNumber = getColumnNumberById("target_date_th");
              var format1 = moment($("#target_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(columnNumber).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          // doc_req_date
          $("#doc_req_date").on("change", function () {
            // dataTable.search(this.value).draw();
            if($("#doc_req_date").val() != ""){
              var columnNumber = getColumnNumberById("doc_req_date_th");
              var format1 = moment($("#doc_req_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(columnNumber).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          // approval_date
          $("#approval_date").on("change", function () {
            // dataTable.search(this.value).draw();
            if($("#approval_date").val() != ""){
              var columnNumber = getColumnNumberById("approval_date_th");
              var format1 = moment($("#approval_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(columnNumber).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          // acceptance_date
          $("#acceptance_date").on("change", function () {
            // dataTable.search(this.value).draw();
            if($("#acceptance_date").val() != ""){
              var columnNumber = getColumnNumberById("acceptance_date_th");
              var format1 = moment($("#acceptance_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(columnNumber).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          // toe_signed_date
          $("#toe_signed_date").on("change", function () {
            // dataTable.search(this.value).draw();
            if($("#toe_signed_date").val() != ""){
              var columnNumber = getColumnNumberById("toe_signed_date_th");
              var format1 = moment($("#toe_signed_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(columnNumber).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });



          function getColumnNumberById(filterFieldId) {
            var columnNumber = -1;
            dataTable.columns().every(function(index) {
                var column = this;
                var headerId = $(column.header()).attr("id");
                if (headerId === filterFieldId) {
                    columnNumber = index;
                    return false; // Exit the loop
                }
              });
              return columnNumber;
          }
          

          $(".custom-search-input-client").on("change", function () {
              $.ajax({
              url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
              method: "post",
              dataType: "html",
              success: function(data) {
                  dataTable.search(data).draw();
              },
              error: bbAlert
              });
          });

          $(document).on("change", ".custom-search-input-reason", function () {
            var data = this.value.split(":");
            if (this.value !== "") {
                dataTable.search(data[0].trim()).draw();
            } else {
                dataTable.search("").columns().search("").draw();
            }
          });
                
          $("#bank-revenue_filter").css({
            "display":"none",
          });
        
        $(".custom-search-input").on("click", function (event) {
          event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
          event.stopPropagation();
        });
        $(".custom-search-input-reason").on("click", function (event) {
          event.stopPropagation();
        });
        

        // Move the length menu to the table footer
        // var lengthMenu = $(".dataTables_length");
        // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");

        // Create a custom footer element
        var customFooter = $("<div class=\"custom-footer\"></div>");
      
        // Move the length menu to the custom footer
        var lengthMenu = $(".dataTables_length");
        lengthMenu.detach().appendTo(customFooter);
      
        // Append the custom footer to the DataTable wrapper
        $("#bank-revenue_wrapper").append(customFooter);
        
        $(".edit").on("click", function () {           
          $.ajax({
             // url: "\'.yii\helpers\Url::to([\'crm-quotations/requested-docs-data\']).\'/"+$(this).attr(\'data_id\'),
              url: "'.yii\helpers\Url::to(['crm-quotations/requested-docs-data']).'?id="+$(this).attr(\'data_id\'),
              method: "post",
              dataType: "html",
              success: function(data) {
                
                  $("#model_html").html(data);
                  $("#editModal").show();
                  
              },
              error: bbAlert
          });
        });
        $(".reasonview").on("click", function () {           
          $.ajax({
              url: "'.yii\helpers\Url::to(['crm-quotations/reason-view-data']).'?id="+$(this).attr(\'data_id\'),
              method: "post",
              dataType: "html",
              success: function(data) {
                
                  $("#model_html2").html(data);
                  $("#reasonViewModal").show();
                  
              },
              error: bbAlert
          });
  
        });
        $(".close,.close_footer").on("click", function () {
            $("#model_html2").html("");
            $("#editModal").hide();
            $("#reasonViewModal").hide();
        });


        $(".div1").daterangepicker({
          autoUpdateInput: false,
            locale: {
              format: "YYYY-MM-DD"
            }
        });
       
        $(".div1").on("apply.daterangepicker", function(ev, picker) {
           $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
        });
        $(document).ready(function() {
            // Code to execute when the document is ready
            var period = $("#crmquotationssearch-time_period").val();
            
            if (period == 9) {
                $(".div1").prop("required", true);
                $("#date_range_array").show();
            } else {
                $(".div1").prop("required", false);
                $("#date_range_array").hide();
            }
            
            // Change event handler for select input
            $("#crmquotationssearch-time_period").on("change", function() {
                var period = $(this).val();
                
                if (period == 9) {
                    $(".div1").prop("required", true);
                    $("#date_range_array").show();
                } else {
                    $(".div1").prop("required", false);
                    $(".div1").val("");
                    $("#date_range_array").hide();
                }
            });
        });

    ');
?>



<div class="modal" id="editModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Requested Documents</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="model_html">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close_footer" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="reasonViewModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= $reason; ?> Reason</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="model_html2">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close_footer" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>



</script>