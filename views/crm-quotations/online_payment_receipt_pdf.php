<?php

use app\models\CrmReceivedProperties;



// if($model->payment_status == 1){


//   $quotation_fee_total= number_format(($quotation_fee_total * ('0.'.$model->first_half_payment)), 2, '.', '');
//   $toe_fee_total= number_format(($toe_fee_total * ('0.'.$model->first_half_payment)), 2, '.', '');
// }

$relative_discount=0;
$no_of_property_discount=0;
$first_time_discount=0;
$general_discount=0;



$total_discount=0;
$total_discount_amount=0;
$discount = 0;


$netValuationFee = $model->total_fee ;
$VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);

$finalFeePayable = $netValuationFee+$VAT;

$datetimeString = $quotation->payment_received_date;
$dateTime = new DateTime($datetimeString);
$QuotationDateOnly = $dateTime->format("d M Y");


$clientAddress = \app\models\Company::find()->select(['title','address','city'])->where(['id'=>$model->client_id])->one();
$payment_amt = number_format(($finalFeePayable),0)
?>


<br><br>

<style>
.box1{
  background-color: #E3F2FD;
  width: 90px;
  border-bottom: 0.5px solid #1E88E5;
}
.box2{
  background-color: #19B65F;
  width: 150px;
  border-bottom: 0.5px solid #1E88E5;
  height: 75px;
}
.box3{
  background-color: #E3F2FD;
  width: 90px;
  border-bottom: 0.5px solid #1E88E5;
}
</style>
<style>
  .airal-family{
    font-family: Arial, Helvetica, sans-serif;
  }
</style>


<style>

  .text-dec{
    font-size: 11px;
    text-align: center;
  }

/*td.detailtext{*/
/*background-color:#BDBDBD; */
/*font-size:9px;*/
/*border-top: 0.4px solid grey;*/
/*}*/

td.detailtexttext{
/* background-color:#EEEEEE; */
font-size:11px;
}
th.detailtext{
/* background-color:#BDBDBD; */
font-size:9px;
border-top: 1px solid black;
}
/*.border {
border: 1px solid black;
}*/

th.amount-heading{
  color: #039BE5;
}
td.amount-heading{
  color: #039BE5;
}
th.total-due{
  font-size: 16px;
}
td.total-due{
  font-size: 16px;
}
span.size-8{
  font-size: 10px;
  font-weight: bold;
}
span.size-10{
  font-size: 12px;
  font-weight: bold;
}
td.first-section{
  padding-left: 10px;
/* background-color: black; */
}
</style>

<hr style="height: 0.2px;">

<table>
<br><br><span class="size-10 airal-family text-dec">PAYMENT RECEIPT</span><br><br><br>
    <tr>
        <td class="airal-family" style="font-size:10px"; >Payment Date : </td>
        <td style="width:10px"></td>

        <td colspan="3" class="airal-family" style="font-size:10px; border-bottom: 1px solid #cacaca";><?php echo $QuotationDateOnly?></td>
        <td style="width:20px"></td>
        <td colspan="0" rowspan="3" class="box2 text-dec airal-family">
          <p style="color: white;"> AMOUNT RECEIVED <br> <b> AED <?php echo $payment_amt ?> </b>
          </p>
        </td>
    </tr>
    <br>
    <tr>
        <td class="airal-family" style="font-size:10px"; >Reference Number: </td>
        <td style="width:10px"></td>

        <td colspan="3" class="airal-family" style="font-size:10px;  border-bottom: 1px solid #cacaca";><?php echo $model->reference_number ?></td>
        <td style="width:20px"></td>

    </tr>
    <br>
    <tr>
        <td class="airal-family" style="font-size:10px";>Payment Method : </td>
        <td style="width:10px"></td>

        <td colspan="3" class="airal-family" style="font-size:10px;  border-bottom: 1px solid #cacaca";>Online</td>
        <td style="width:20px"></td>

    </tr>
</table>

<br><br><br><br><br>
<span class="size-8 airal-family">Bill To</span>
<p style="font-size:10px"; class="airal-family"><b><?= ucwords($clientAddress->title) ?></b><br><?=$clientAddress->address?><br><?=$clientAddress->city?><br><br></p>
<br><br><br><br><br>


<span class="size-8 airal-family">Payment For</span>
<table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <hr style="height: 0.2px;">
    <tr style="">
      <td class="airal-family" style="font-size:9px; font-weight: bold; width:150px; border-bottom: 0.2px solid black;">Description</td>
      <td class="airal-family" style="width: 150px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">Date</td>
      <td class="airal-family" style="width: 120px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">Invoice Amount</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align: right; border-bottom: 0.2px solid black;">Payment Amount</td>
    </tr>
  </thead>

  <tbody>
    <?php
    $i=1;


        ?>
        <tr style="">
          <td class="airal-family" style="font-size:9px; width:150px; ">Property Valuation</td>
          <td class="airal-family" style="width: 150px; font-size:9px; text-align: center;"><?php echo $QuotationDateOnly ?></td>
          <td class="airal-family" style="width: 150px; font-size:9px; text-align: center;">AED <?php echo number_format($finalFeePayable,0) ?></td>
          <td class="airal-family" style="font-size:9px; text-align: center;">AED <?php echo $payment_amt ?></td>
        </tr>


  </tbody>
</table>
<hr style="height: 0.3px;">

<br><br>
<!-- <table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <tr style="background-color: ;">
      <td class="airal-family" style="font-size:10px; text-align:left;">Accepted By</td>
      <td class="airal-family" style="width: 90px; font-size:10px; text-align:left;">Accepted Date</td>
    </tr>
  </thead>
</table> -->




