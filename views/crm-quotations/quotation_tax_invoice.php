
<style>
    .box1{
        background-color: #e6e6e6;
        width: 110px;


        border-right: 5px white;
        border-left: 5px white;


    }
    .box2{
        background-color: #cccccc;
        width: 90px;
        margin-right: 2px;
        border-right: 5px white;

    }
    .box3{
        background-color: #cccccc;
        width: 90px;

        /* margin-right: 100px; */
    }
    .border {
        width: 100%;
        border-top-width: 0.05px;
        border-bottom-width: 0.05px;
        border-left-width: 0.05px;
        border-right-width: 0.05px;
        padding: 5px;
    }
    .font_color{
        color: #4472C4;
    }

</style>


<table class="border" width="547">

    <tr>
        <td colspan="5" class="detailtexttext first-section" >
            <br><span ><?= $branch_address->company; ?> </span>
            <br><span><?= $branch_address->address; ?></span>
            <br><span><?= $branch_address->office_phone; ?></span>
            <br><span>finance@windmillsgroup.com, services@windmillsgroup.com </span>
            <br><span><?= $branch_address->website; ?></span>
            <br><span>TRN: <?= $branch_address->trn_number; ?></span>
        </td>


        <td colspan="3" class="detailtexttext first-section">
            <br><span>Bank Details: </span>
            <br><span>Emirates NBD </span>
            <br><span>Al Quoz Branch  </span>
            <br><span>Account No: 1015286631901 </span>
            <br><span>IBAN: <?= $branch_address->iban_number; ?> </span>
            <br><span>Swift code: EBILAEAD</span>
        </td>
    </tr>

</table><br><br>



<table>
    <thead>
    <tr>
        <td colspan="2" class="detailtexttext first-section" style="background-color: ;">
            <br><span class="size-8 airal-family font_color">Client</span>

            <br><span><?php
                if(	$model->client_invoice_type == 1){
                ?><?= $model->client_customer_name;?></span>
            <?php }else{ ?><?= $model->client->title ?></span>
            <?php } ?>

            <br><span><?= $model->client->address ?></span>

            <?php
            if ($model->client->id !=9167) { ?>
                <br><span>UAE.</span>
                <?php
            }
            ?>

            <?php if ($model->client->trn_number <> null ) { ?>
                <br>TRN: <span><?= $model->client->trn_number ?></span>
                <?php
            }
            ?>
        </td>

        <td class="box1 text-dec airal-family" style="margin-top: 20px">
       <span style="color: #4472C4;font-size: 11.5px;font-weight: bold"><br>Invoice Number<br>
           <?= Yii::$app->appHelperFunctions->getInvoiceNumberQt($model);
           ?>
           <br>
        </span>
        </td>

        <td class="box1 text-dec airal-family" style="margin-top: 20px">
        <span style="color: #4472C4;font-size: 11.5px;font-weight: bold"><br>Issue Date<br><?= date('d-M-Y') ?>
            <br>
        </span>
        </td>
        <td class="box1 text-dec airal-family" style="margin-top: 20px">
        <span style="color: #4472C4;font-size: 11.5px;font-weight: bold"><br>Total Amount<br><?= number_format($fee['finalFeePayable'],2) ?>
            <br>
        </span>
        </td>

    </tr>
    </thead>
</table><br><br>



<br>

<table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <hr style="height: 0.2px;">
    <tr style="background-color: ;">
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align:left; border-bottom: 0.2px solid black;">DATE</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; width:150px; border-bottom: 0.2px solid black;">DESCRIPTION</td>
      <td class="airal-family" style="width: 100px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">TAX</td>
      <td class="airal-family" style="width: 50px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">QTY</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">RATE</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align:right; border-bottom: 0.2px solid black;">AMOUNT</td>
    </tr>
  </thead>

  <tbody>
    <?php
    $i=1;
    if($properties <> null && !empty($properties)) {
      foreach($properties as $key => $property){
        ?>
        <tr style="background-color: ;">
          <td class="airal-family" style="font-size:9px;"><?= date('d-M-Y',strtotime($model->instruction_date)); ?></td>
          <td class="airal-family" style="font-size:9px; width:150px;"><?= $property->building->title; ?></td>
          <td class="airal-family" style="width: 100px; font-size:9px; text-align: center;">SR Standard Rated (DXB)</td>
          <td class="airal-family" style="width: 50px; font-size:9px; text-align: center;">1</td>
          <td class="airal-family" style="font-size:9px; text-align: center;"><?= number_format($property->quotation_fee,2) ?></td>
          <td class="airal-family" style="font-size:9px; text-align:right;"><?= number_format($property->quotation_fee,2) ?></td>
        </tr>

        <?php
        $i++;
        if ($i<=count($properties)) {?>
          <hr style="height: 0.2px;">
          <?php
        }
      }
    }
    ?>
    <tr>
      <td class="airal-family" style="font-size: 9px; width:180px;"><b>Payment Terms:</b> <?= $advance_payment_terms ?> Advance Payment</td>
      <td></td>
    </tr>
  </tbody>
</table>
<hr style="height: 0.3px;">
<!--<br pagebreak="true"/>-->


<br><br><br><br><br>
<table class="border airal-family" cellspacing="1" cellpadding="5" width="547">
    <tbody>
    <?php
    if($model <> null && !empty($model)) {

        ?>
        <tr style="background-color: ;">
            <td colspan="2"  style="font-size:9px;text-align:left;font-weight:bold">Gross Fee</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($fee['netToeFee'],2); ?></td>
            <td colspan="4"  style=" font-size:9px; text-align: center;"></td>

        </tr>

        <?php 
        if($model->relative_discount_toe>0){?>
        <tr style="background-color: ;">
            <td colspan="2"   style="font-size:9px;text-align:left;font-weight:bold">Relationship Discount (<?=$model->relative_discount_toe?>%)</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($fee['relative_discount_toe'],2); ?></td>
            <td colspan="4"  style=" font-size:9px; text-align: center;font-weight:bold"></td>
        </tr>
        <?php
        }
        ?>

        <?php 
        if($model->first_time_discount>0){?>
        <tr style="background-color: ;">
            <td colspan="2"  style="font-size:9px;text-align:left;font-weight:bold">First Time Discount (<?=$model->first_time_discount?>%)</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($fee['first_time_discount'],2); ?></td>
            <td colspan="4"  style=" font-size:9px; text-align: center;font-weight:bold"></td>
        </tr>
        <?php
        }
        ?>

        <?php 
        if($model->no_of_property_discount>0){?>
        <tr style="background-color: ;">
            <td colspan="2"   style="font-size:9px;text-align:left;font-weight:bold">Properties Discount (<?= $model->no_of_property_discount ?>%)</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($fee['discount_no_of_properties'],2); ?></td>
            <td colspan="4"  style=" font-size:9px; text-align: center;font-weight:bold"></td>
        </tr>
        <?php
        }
        ?>

        <?php 
        if($model->general_discount>0){?>
        <tr style="background-color: ;">
            <td colspan="2"  style="font-size:9px;text-align:left;font-weight:bold">General Discount (<?=$model->general_discount?>%)</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($fee['general_discount'],2); ?></td>
            <td colspan="4"  style=" font-size:9px; text-align: center;font-weight:bold"></td>
        </tr>
        <?php
        }
        ?>

        <?php 
        if($fee['total_discount']>0){?>
        <tr style="background-color: ;">
            <td colspan="2"  style="font-size:9px;text-align:left;font-weight:bold">Total Discount (<?= $fee['total_discount'] ?>%)</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($fee['total_discount_amount'],2); ?></td>
            <td colspan="4"  style=" font-size:9px; text-align: center;font-weight:bold"></td>
        </tr>
        <?php
        }
        ?>


        <tr style="background-color: ;">
            <td colspan="2"   style="font-size:9px;text-align:left;font-weight:bold">Fee</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($fee['netAfterDiscountFee'],2); ?></td>
            <td colspan="4"  style=" font-size:9px; text-align: center;"></td>

        </tr>
        <tr style="background-color: ;">
            <td colspan="2"   style="font-size:9px;text-align:left;font-weight:bold">VAT (5%)</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($fee['vat'],2); ?></td>
            <td colspan="4"  style=" font-size:9px; text-align: center;"></td>

        </tr>
        <tr style="background-color: ;">
            <td colspan="2"   style="font-size:9px;text-align:left;font-weight:bold">Net Fee</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($fee['finalFeePayable'],2); ?></td>
            <td colspan="4"  style=" font-size:9px; text-align: left;font-weight:bold"><?= $fee['fee_to_words']." only." ?></td>

        </tr>


    <?php }
    ?>

    </tbody>
</table>









<br><br>
<table cellspacing="1" cellpadding="3" width="466">
    <thead>
    <tr style="background-color: ;">
        <td class="airal-family" style="font-size:10px; text-align:left;">We sincerely thank you for giving us this privilege to work for you. </td>

    </tr>
    </thead>
</table>


<br>
<br>
<br>
<br>
<br>
<hr style="height: 0.2px; margin-top: 100px;">
<br>
<table cellspacing="1" cellpadding="3" width="466">
    <thead>
    <!--<tr style="background-color: ;">
        <td class="airal-family" style="font-size:10px; text-align:left;">Ali Raza </td>
    </tr>-->
    <tr style="background-color: ;">
        <td class="airal-family" style="font-size:10px; text-align:left;">Finance Manager </td>
    </tr>
    </thead>
</table>

<style>
    .airal-family{
        font-family: Arial, Helvetica, sans-serif;
    }
</style>


<style>

    .text-dec{
        font-size: 10px;
        text-align: center;
        padding-right: 10px;
    }

    /*td.detailtext{*/
    /*background-color:#BDBDBD; */
    /*font-size:9px;*/
    /*border-top: 0.4px solid grey;*/
    /*}*/

    td.detailtexttext{
        /* background-color:#EEEEEE; */
        font-size:10px;
        font-weight: 100;

    }
    th.detailtext{
        /* background-color:#BDBDBD; */
        font-size:9px;
        border-top: 1px solid black;
    }
    /*.border {
    border: 1px solid black;
    }*/

    th.amount-heading{
        color: #039BE5;
    }
    td.amount-heading{
        color: #039BE5;
    }
    th.total-due{
        font-size: 16px;
    }
    td.total-due{
        font-size: 16px;
    }
    span.size-8{
        font-size: 10px;
        font-weight: bold;
    }
    td.first-section{
        padding-left: 10px;
        /* background-color: black; */
    }
</style>

