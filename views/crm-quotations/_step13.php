

<?php
    $this->title = Yii::t('app', 'Review Fee Summary');
    $cardTitle = Yii::t('app', 'Ref No:  {nameAttribute}', ['nameAttribute' => $model->reference_number]);
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['crm-quotations/step_8/?id=' . $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('app', 'Summary');

?>
<style>
    .summary-property-table th:first-of-type { padding-left: 5px !important;  }
    .summary-property-table td:first-of-type { padding-left: 5px !important;}
    .summary-property-table th { padding: 5px 5px; font-size: 15px; background-color:#fff; background-color: #efefef;}
    .summary-property-table td { padding: 5px 5px; font-size: 14px; vertical-align:middle; max-width: 100px;border-top: 0px solid #fff; border-bottom: 0px solid #fff;}
    .summary-property-table tr.trGreyBg { background-color:#eaeaea;  }
    .summary-property-table tr.trWhiteBg { background-color:#fff; }
    .summary-property-table tr.trBrdr { border-top: 4px solid #ccc; }
    .summary-property-table tr.trBrdrTopWhite { border-top: 1px solid #fff !important; }
    .summary-property-table td.tdBrdrTopWhite { border-top: 1px solid #fff; border-bottom: 1px solid #fff; }
    .summary-table { font-size: 15px;}
    .summary-table td{ padding: 5px 5px;}
    .left-panel {
            -ms-flex: 0 0 3%;
            flex: 0 0 3%;
            max-width: 3%;
            max-height: 100vh;
            transition: max-width 0.3s ease;
    }
    .left-panel #vert-tabs-tab {
        -ms-flex: 0 0 0%;
        flex: 0 0 0%;
        max-width: 0%;
        overflow: hidden;
        transition: max-width 0.3s ease;
    }
    .right-panel {
        -ms-flex: 0 0 97%;
        flex: 0 0 97%;
        max-width: 97%;
        transition: max-width 0.3s ease;
    }
    .hide-left-panel.left-panel {
        -ms-flex: 0 0 20%;
        flex: 0 0 20%;
        max-width: 20%;
        max-height: 100%;
    }
    .right-panel.full-right-panel {
        -ms-flex: 0 0 80%;
        flex: 0 0 80%;
        max-width: 80%;
    }
    .hide-left-panel #vert-tabs-tab {
        -ms-flex: 0 0 100%;
        flex: 0 0 100%;
        max-width: 100%;
    }
    .toggleButton { padding:2px 10px; color: #007bff; cursor: pointer;}
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <h3 class="card-title float-none text-md-right "><i class="fas fa-edit"></i><?= $cardTitle ?></h3>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-3 left-panel" ><div class="toggleButton" id="togglePanel" title="Left Panel" ><i class="fas fa-bars"></i></div>
                <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 13, 'quotation' => $quotation]); ?>
            </div>
            <div class="col-9 right-panel">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <header class="card-header">
                                <h2 class="card-title fw-bold " style="font-weight:bold;">Review Quotation Summary</h2>
                            </header>
                            <div class="card-body">

                                <!-- <div class="row mb-3 ">
                                    <div class="col-12" style="font-weight:bold; font-size:16px;b-4">Client Name: <span class="text-primary"><?php echo $model->client->title ?></span></div>
                                </div> -->
                                

                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                        <header class="card-header">
                                            <h2 class="card-title fw-bold " style="font-weight:bold;">Client Name: <span class="text-primary"><?php echo $model->client->title ?></span></h2>
                                        </header>
                                            <div class="card-body p-0" style=" overflow-x: scroll;">
                                                <table class="table table-bordered summary-property-table" >
                                                    <tbody>
                                                    <?php //dd($amount[0]['drawings_not_available']);
                                                        if ($amount <> null) { ?>
                                                            <tr class="trWhiteBg">
                                                                <th>#</th>
                                                                <!-- <th style="min-width:100px;">Building<br>Title</th> -->
                                                                <th>Client<br>Type</th>
                                                                <th style="width:120px;">Property</th>
                                                                <th style="width:80px;">Height</th>
                                                                <th>No Of</br>Units</th>
                                                                <th>Resid<br>Units</th>
                                                                <th>Comm<br>Units</th>
                                                                <th>Retai<br>Units</th>
                                                                <th>Types<br>Resi</th>
                                                                <!-- <th>Sub Community</th> -->
                                                                <th style="width:70px;">City</th>
                                                                <th>Land<br>Size</th>
                                                                <!-- <?php //if($amount[0]['no_of_rooms'] <> null){ ?><th>No Of <br>Rooms</th><?php // } ?> -->
                                                                <th>BUA</th>
                                                                <th>Tenure</th>
                                                                <th>Complexity</th>
                                                                <th>Upgrades</th>
                                                                <th>Compar-<br>ables</th>
                                                                <!-- <th>OIU</th> -->
                                                                <!-- <?php //if($amount[0]['drawings_not_available'] <> null){ ?><th>No<br>Drawings</th><?php // } ?> -->
                                                                <th>Recomm.<br>Fee</th>
                                                                <th>Reviewed<br>Fee</th>
                                                            </tr>
                                                            <?php 
                                                            $counter = 0;
                                                            foreach ($amount as $key => $amt) {
                                                                $counter++;
                                                                $rowClass = $counter % 2 == 0 ? 'trWhiteBg' : 'trGreyBg';   
                                                                ?>
                                                                <?php /* if($key + 1 == 1){ ?>
                                                                    <tr><th style="font-size: 14px;">#</th>
                                                                    <?php 
                                                                        foreach ($amt as $key2 => $value2) { 
                                                                        ?>
                                                                            <th  style="font-size: 14px;"><strong><?= ucwords(str_replace('_', ' ', $key2)) ?></strong></th>
                                                                            <?php
                                                                        }
                                                                    ?>
                                                                    </tr>
                                                                    <?php 
                                                                } */ ?>
                                                                <tr class="trBrdr">
                                                                    <td><?= $key + 1 ?></td>
                                                                    <?php
                                                                    foreach ($amt as $key2 => $value2) {
                                                                        ?>
                                                                        <?php
                                                                        if (is_numeric($value2['title'])) {
                                                                        ?>
                                                                            <td><?= ucwords(str_replace('_', ' ', number_format($value2['title']))) ?></td>
                                                                        <?php
                                                                        } else { ?>
                                                                            <td><?= ucwords(str_replace('_', ' ', $value2['title'])) ?></td>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tr>
                                                                <tr class="trBrdrTopWhite" >
                                                                    <td></td>
                                                                    <?php
                                                                    foreach ($amt as $key2 => $value2) { 
                                                                        ?>  
                                                                        <td>
                                                                            <?php if ($key2 == 'building_title') { ?>
                                                                                
                                                                                <?php
                                                                            } else { ?>
                                                                                <strong class="text-" style="font-size: 14px;"><?= number_format($value2['fee']) ?></strong>
                                                                                <?php
                                                                            } 
                                                                            ?>
                                                                        </td>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tr>

                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="card card-primary ">
                                            <header class="card-header">
                                                <h2 class="card-title fw-bold " style="font-weight:bold;">Total Fee</h2>
                                            </header>
                                            <div class="card-body p-0">
                                                <table class="table table-bordered summary-table">
                                                    <tbody class="" style="font-size: 16px;">
                                                        <tr>
                                                            <td colspan="2"><mark><strong>Net Recommended Fee</strong></mark></td>
                                                            <td><mark><strong class="text-primary"><?= number_format($net_recommended_fee, 2) ?></strong></mark></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>Reference Fee 3rd Party</strong></td>
                                                            <td><?php if ($model->reference_fee_3rdparty_check === 0) { ?>
                                                                    <strong class="text-primary">
                                                                        <?= ($model->reference_fee_3rdparty != null) ? $model->reference_fee_3rdparty : 0 ?> %</strong>
                                                                <?php } else { ?>
                                                                    <strong class="text-primary"> Amount</strong>
                                                                <?php } ?>
                                                            </td>
                                                            <td><strong class="text-primary"><?= number_format($refFee3rdParty, 2) ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>Reference Fee Staff</strong></td>
                                                            <td><strong class="text-primary"><?= ($model->reference_fee_staff != null) ? $model->reference_fee_staff : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary"><?= number_format($refFeeStaff, 2) ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2"><mark><strong>Net Recommended Quotation Fee</strong></mark></td>
                                                            <td><mark><strong class="text-primary"><?= number_format($net_recommended_fee + $refFeeStaff + $refFee3rdParty, 2) ?></strong></mark></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2"><mark><strong>Net Approved Quotation Fee</strong></mark></td>
                                                            <td><mark><strong class="text-primary"><?= number_format($net_quotation_fee, 2) ?></strong></mark></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>Relationship Discount</strong></td>
                                                            <td><strong class="text-primary"><?= ($model->relative_discount_toe != null) ? $model->relative_discount_toe : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary"><?= ($relative_discount_toe['amount'] > 0) ? number_format($relative_discount_toe['amount'], 2) : '0.00' ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>First Time Discount</strong></td>
                                                            <td><strong class="text-primary">
                                                                <?= ($model->first_time_discount != null) ? 
                                                                    (($model->aprvd_first_time_discount != null) ? 
                                                                    $model->aprvd_first_time_discount : $model->first_time_discount) : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary">
                                                                <?= ($first_time_fee_discount['amount'] > 0) ? 
                                                                (($aprvd_first_time_fee_discount['amount'] > 0) ? 
                                                                number_format($aprvd_first_time_fee_discount['amount'], 2) : number_format($first_time_fee_discount['amount'], 2)) : '0.00' ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>Multiple Properties Discount</strong></td>
                                                            <td><strong class="text-primary">
                                                                <?= ($model->no_of_property_discount != null) ?
                                                                (($model->aprvd_no_of_property_discount != null) ?
                                                                $model->aprvd_no_of_property_discount :
                                                                $model->no_of_property_discount) : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary"><?= ($no_of_property_discount['amount'] > 0) ?
                                                                (($aprvd_no_of_property_discount['amount'] > 0) ?
                                                                number_format($aprvd_no_of_property_discount['amount'], 2) :
                                                                number_format($no_of_property_discount['amount'], 2)) : '0.00' ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>Same Building Discount</strong></td>
                                                            <td><strong class="text-primary">
                                                                <?= ($model->same_building_discount != null) ?
                                                                (($model->aprvd_same_building_discount != null) ?
                                                                $model->aprvd_same_building_discount : 
                                                                $model->same_building_discount) : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary">
                                                                <?= ($same_building_discount['amount'] > 0) ?
                                                                (($aprvd_same_building_discount['amount'] > 0) ?
                                                                number_format($aprvd_same_building_discount['amount'], 2) : 
                                                                number_format($same_building_discount['amount'], 2)) : '0.00' ?></strong></td>
                                                        </tr>

                                                        <!-- <tr>
                                                                <td><strong>General Discount</strong></td>
                                                                <td><strong><?= ($model->general_discount != null) ? $model->general_discount : 0 ?> %</strong></td>
                                                                <td><strong><?= ($general_discount > 0) ? number_format($general_discount, 2) : '0.00' ?></strong></td>
                                                            </tr> -->

                                                        <tr>
                                                            <td><strong>Total Discount</strong></td>
                                                            <td><strong class="text-primary">
                                                                <?= ($total_discount_num != null) ?
                                                                (($aprvd_total_discount_num != null) ?
                                                                $aprvd_total_discount_num :
                                                                $total_discount_num) : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary">
                                                                <?= ($total_discount > 0) ?
                                                                (($aprvd_total_discount > 0) ?
                                                                number_format($aprvd_total_discount, 2) :
                                                                number_format($total_discount, 2)) : '0.00' ?></strong></td>
                                                        </tr>



                                                        <tr>
                                                            <td colspan="2"><mark><strong>Net Valuation Fee</strong></mark></td>
                                                            <td><mark><strong class="text-primary">
                                                                <?= ($aprvd_discount_net_fee <> null) ?
                                                                number_format($aprvd_discount_net_fee, 2) :
                                                                number_format($discount_net_fee, 2) ?></strong></mark></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2"><strong>Urgency Fee</strong></td>
                                                            <td><strong class="text-primary">
                                                                <?= ($urgencyfee_amount <> null) ?
                                                                (($aprvd_urgencyfee_amount <> null) ?
                                                                number_format($aprvd_urgencyfee_amount, 2) :
                                                                number_format($urgencyfee_amount, 2)) : '0.00' ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>VAT</strong></td>
                                                            <td><strong class="text-primary"><?= ($model->client->vat == 1) ? 5 : 0 ?> %</strong></td>
                                                            <td><strong class="text-primary">
                                                                <?= ($VAT > 0) ?
                                                                (($aprvd_VAT > 0) ?
                                                                number_format($aprvd_VAT, 2) :
                                                                number_format($VAT, 2)) : '0.00' ?></strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2"> <mark><strong> Final Fee Payable </strong></mark></td>
                                                            <td><mark><strong class="text-primary">
                                                                <?= ($finalFeePayable) ?
                                                                (($aprvd_finalFeePayable) ?
                                                                number_format($aprvd_finalFeePayable) :
                                                                number_format($finalFeePayable)) : '0.00' ?></strong></mark></td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
    $(document).ready(function () {
        $('#togglePanel').click(function () {
            $('.left-panel').toggleClass('hide-left-panel');
            $('.right-panel').toggleClass('full-right-panel');
            $(this).find('i').toggleClass('fa-arrow-left fa-bars');
        });
    });
</script>