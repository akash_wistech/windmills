<?php
use app\models\UserProfileInfo;
use app\models\ProposalMasterFile;
use app\models\Valuation;
use app\models\CrmReceivedProperties;


$detail= \app\models\ValuationDetailData::find()->where(['valuation_id' => $model->id])->one();
$building = \app\models\Buildings::find()->where(['id' => $model->building_info])->one();
/*echo "<pre>";
print_r($building);
die;*/


//$total_no_of_prperties = count($multipleProperties);
$no_of_property_discount = 0;
$total_discount = 0;
$total_discount_amount = 0;
$aprvd_total_discount_amount = 0;
$discount = 0;
$total_net_fee = 0;

//old query
// $toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');

//new query
$toe_fee_total = $model->total_fee;
$netValuationFee = $model->total_fee;

$discount_net_fee = $netValuationFee;
$VatInTotalValuationFee = 0;
$VatInTotalValuationFee = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);



$finalFeePayable = $netValuationFee + $VatInTotalValuationFee;


$totalTurnAroundTime = 2;
$totalValuationFeeInWORDS = yii::$app->quotationHelperFunctions->numberTowords($toe_fee_total);

$finalFeePayable = Yii::$app->appHelperFunctions->wmFormate($finalFeePayable);
$finalFeePayableInWords = yii::$app->quotationHelperFunctions->numberTowords(str_replace(',', '', $finalFeePayable));


$ValuerName = \app\models\User::find()
    ->select(['fullname' => 'CONCAT(firstname," ",lastname)'])
    ->where(['id' => 111558])
    ->asArray()
    ->one();
$clientName = yii::$app->quotationHelperFunctions->getClientName($model->client_id);
$purposeOfValuation = Yii::$app->appHelperFunctions->getPurpose();
$standardReport = Yii::$app->quotationHelperFunctions->standardReport();
// $valuerFirstName = Yii::$app->user->identity->firstname;
// $valuerLastName = Yii::$app->user->identity->lastname;
// $valuerName = $valuerFirstName.' '.$valuerLastName;
$valuerQualifications = Yii::$app->user->identity->profileInfo->valuer_qualifications;
$valuerStatus = Yii::$app->user->identity->profileInfo->valuer_status;
$valuerExperienceExpertise = Yii::$app->user->identity->profileInfo->valuer_experience_expertise;

$model_valuer_id =  111558;
$ValuerName = \app\models\User::find()->select(['firstname', 'lastname'])->where(['id' => $model_valuer_id])->one();

$valuerOther = \app\models\UserProfileInfo::find()->select(['valuer_qualifications', 'valuer_status', 'valuer_experience_expertise', 'signature_img_name'])->where(['user_id' => $model_valuer_id])->one();

$valuerJob = \app\models\UserProfileInfo::find()->select(['job_title_id'])->where(['user_id' => $model_valuer_id])->one();
$valuerJobName = \app\models\JobTitle::find()->select(['title'])->where(['id' => $valuerJob->job_title_id])->one();

$bankName = $iban = $trn = '';

if ($model <> null) {
    $branchDetail = \app\models\Branch::find()->where(['zone_list' => $model->building->city])->one();

    if ($branchDetail <> null) {
        $bankName = $branchDetail->bank_name;
        $iban = $branchDetail->iban_number;
        $trn = $branchDetail->trn_number;
        $branchCompany = $branchDetail->company;
        $branchAddress = $branchDetail->address;
        $branchPhone = $branchDetail->office_phone;
    }
}


// $scope_of_service = Yii::$app->appHelperFunctions->getScopeOfServiceTitle($mainTableData['scope_of_service']);
// $scope_of_service = Yii::$app->appHelperFunctions->scopeOfWorkArr[$mainTableData['scope_of_service']];
// $scope_of_service = Yii::$app->appHelperFunctions->ScopeOfWorkList[$mainTableData['scope_of_service']];



$scope_of_service = 'Estimate Market Value';

/*
// $general_assumptions = explode(",", $mainTableData['assumptions']);
$general_assumptions = explode(",", $property->property_general_asumption);
$generalAssumptionsNames = \app\models\GeneralAsumption::find()
    ->select(['general_asumption'])
    ->where(['in', 'id', $general_assumptions])
    ->all();
$generalAssumptionsNamesConcat = '';
$i = 0;

foreach ($generalAssumptionsNames as $key => $assumption) {
    $generalAssumptionsNamesConcat .= $assumption->general_asumption;
    if (!count($generalAssumptionsNames) == $i) {
        $generalAssumptionsNamesConcat .= ', ';
    }
    $i++;
}


// $special_assumptions = explode(",", $mainTableData['special_assumptions']);
$special_assumptions = explode(",", $property->property_special_asumption);
$specialAssumptionsNames = \app\models\GeneralAsumption::find()
    ->select(['general_asumption'])
    ->where(['in', 'id', $special_assumptions])
    ->all();
$specialAssumptionsNamesConcat = '';
$i = 0;
// dd(count($specialAssumptionsNames));
foreach ($specialAssumptionsNames as $key => $assumption) {
    $specialAssumptionsNamesConcat .= $assumption->general_asumption;
    if (!count($specialAssumptionsNames) < $i) {
        $specialAssumptionsNamesConcat .= ', ';
    }
    $i++;
}*/

// dd("here");
$ceo = \app\models\UserProfileInfo::find()->select(['valuer_qualifications', 'valuer_status', 'valuer_experience_expertise', 'signature_img_name'])->where(['user_id' => 6319])->one();
/*echo "<pre>";
echo 'https://maxima-media.s3.eu-central-1.amazonaws.com/images/'.$ceo['signature_img_name'];
print_r($ceo);
die;*/
?>


<style>
    table {

    }

    td.detailheading {
        color: #0D47A1;
        font-weight: normal;
        font-size: 11px;
        /* font-weight:bold; */
        /* border-bottom: 1px solid #64B5F6; */
    }

    td.toeheading {
        color: #4A148C;
        font-size: 11px;
        font-weight: bold;
        /* font-weight: normal; */
        border-top: 1px solid #4A148C;
        border-bottom: 1px solid #4A148C;
        border-left: 1px solid #4A148C;
        border-right: 1px solid #4A148C;
        text-align: center;
    }

    td.onlymainheading {
        color: #4A148C;
        font-size: 11px;
        /* font-weight:bold; */
        font-weight: normal;
        border-bottom: 1px solid #000000;
    }

    td.detailtext {
        /* background-color:#E8F5E9; */
        font-size: 11px;
        text-align: justify;
    }

    span.spantag {
        /* background-color:#E8F5E9; */
        font-size: 11px;
        text-align: justify;
        font-weight: normal;
        color: black;
    }

    td.sixpoints {
        font-size: 11px;
        color: #0D47A1;
    }

    tr.color {
        background-color: #ECEFF1;
    }

    td.subject {
        font-weight: bold;
        border-bottom: 1px solid #4A148C;
    }

    td.fontsizeten {
        font-size: 11px;
    }

    span.fontsize12 {
        /* background-color:#E8F5E9; */
        font-size: 14px;
    }

    span.b-number {
        color: black;
        font-weight: bold;
        font-size: 11px;
    }
</style>

<table>
    <tr>
        <td class="firstpage fontsizeten"><?= date("F j, Y", strtotime(date("Y-m-d"))); ?></td>
    </tr>
    <br>


    <tr>
        <td class="firstpage fontsizeten"><?= $clientName['title'] ?></td>
    </tr>

    <?php
    if ($clientName['address'] != null) { ?>
        <tr>
            <td class="firstpage fontsizeten"><?= $clientName['address'] ?></td>
        </tr><br>
        <?php
        // code...
    }
    ?>


    <tr>
        <td class="firstpage fontsizeten">Dear Sir/Madam,</td>
    </tr>
    <br>

    <tr>
        <td class="detailheading fontsizeten subject" colspan="2"><h4>Terms of Engagement for Valuation Services</h4>
        </td>
    </tr>
    <br>
    <tr>
        <td style="text-align:justify;" class="firstpage fontsizeten"><?= $standardReport['firstpage_content'] ?></td>
    </tr>

    <tr>
        <td class="firstpage fontsizeten"><br/><br/>
            Thank you very much.<br><br>Sincerely,<br/><br/>
            <?php 
                if ( $model_valuer_id == 21 ) {  
                    $sigImg = \yii\helpers\Url::to('@web/images/lksign.png'); ?>
                    <img src="<?php echo $sigImg ?>" style="height: 40px; ">
                    <?php 
                } else 
                if ( $model_valuer_id == 111558 ) { 
                    $sigImg = \yii\helpers\Url::to('@web/images/ahmet_sig.jpeg'); ?>
                    <img src="<?php echo $sigImg ?>" style="height: 70px; ">
                    <?php 
                } else if($model_valuer_id == 38){
                    $sigImg = \yii\helpers\Url::to('@web/images/feb_sig6537cbcf709e8.png'); ?>
                    <img src="<?php echo $sigImg ?>" style="height: 50px; ">
                    <?php 
                }else if($model_valuer_id == 110821) {
                    $sigImg = \yii\helpers\Url::to('@web/images/IMG_387765b2070e17fe2.jpg');?>
                    <img src="<?php echo $sigImg ?>" style="height: 50px; ">
                    <?php 
                } 
            ?>

            <br>
            <td class="detailtext"><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?><br><?= $valuerJobName['title'] ?><br><?= $branchCompany; ?>
            </td>
            <?php /*-- <img src="<?php echo '/images/lksign.png' ?>" style="height: 40px; ">
            <?= $standardReport['firstpage_footer'] ?> */ ?>
        </td>
        <!--<td colspan="2" class="container">

            <img class="alignnone" src="<?php /*echo '/images/rt.png' */ ?>" style="width: 100px;" >

        </td>-->
        <!-- <td style="text-align: center">

         </td>-->
    </tr>

</table><br pagebreak="true"/>

<table cellspacing="1" cellpadding="8" class="main-class">
    <tr>
        <td class="detailheading toeheading" colspan="2"><h4>Terms of Engagement
            </h4></td>
    </tr>
</table><br><br>


<table>
    <tr>
        <td class="detailheading onlymainheading" colspan="2"><h4>1. Client and Intended Users</h4></td>
    </tr>

</table>


<table>

    <tr><br>
        <td class="detailheading"><h4>1.1. Client</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $clientName['title'] ?></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $clientName['address'] ?></td>
    </tr>
</table><br/><br/>

<table>

    <tr>
        <td class="detailheading"><h4>1.2. Service Provider</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $branchCompany; ?></td>
    </tr>
</table><br/><br/>

<table>
    <br>
    <tr>
        <td class="detailheading"><h4>1.3. Other Intended Users:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= ($mainTableData['other_intended_users'] <> null && $mainTableData['other_intended_users_check_1'] == "Yes") ? $mainTableData['other_intended_users'] : 'None.' ?></td>
    </tr>
</table><br><br>


<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>2. Subject Properties to be Valued (Scope of Service)</h4></td>
    </tr>
</table><br>

<table>
    <br>
    <tr>
        <br>
        <td class="detailheading"><h4>2.1. Scope of Service</h4></td>
    </tr>
    <tr>
        <td class="detailtext">
            <?php //echo $scope_of_service ?><br><?php

                echo $scope_of_service;


        ?>
        
    </td>
    </tr>
</table><br>

<table>
    <tr>
        <td class="detailheading"><br><h4>2.2. Subject Property</h4></td>
    </tr>
    <tr>
        <td class="detailtext">Plot Number <?= $model->plot_number.', '.$model->building->title.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
        </td>
    </tr>

</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>2.3 Purpose of Valuation:</h4></td>
    </tr>
    <tr>
        <td class="detailtext">The purpose of the subject valuation
            is <?= Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation] ?></td>
    </tr>
</table><br><br>


<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>3. Documentary and information Requirements:</h4></td>
    </tr>
    <tr><br>
        <td class="detailtext">For the purpose of carrying out this valuation, please provide the following documents
            related to the subject property:
        </td>
    </tr>

    <?php

    $k = 1;
    if($model->id > 3029){
        $multipleProperties = yii::$app->quotationHelperFunctions->getMultiplePropertiesMuliscope($id);
    }else{
        $multipleProperties = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id);
    }

    if ($multipleProperties != null) {
        foreach ($multipleProperties as $multipleDocuments) {

            if ($k > 1) {
                ?>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <?php
            }
            $buildingName = yii::$app->quotationHelperFunctions->getBuildingName($multipleDocuments['building_info']);
            $documents = yii::$app->quotationHelperFunctions->getDcouments($multipleDocuments['property_id']);
            if ($documents['required_documents'] != null) {
                $explodeDocuments = (explode(",", $documents['required_documents']));
                $i = 1; ?>
                <tr>
                    <td class="detailheading">Project Name:-<?= $buildingName['title'] ?></td>
                </tr>
                <?php foreach ($explodeDocuments as $key => $value) { 
                    
                    if($value != 2 && $value != 6 && $value != 8 && $value != 9){
                        continue;
                    }
                    
                    ?>

                    <tr>
                        <td class="detailtext"><?= $i ?>. <?= Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$value] ?></td>
                    </tr>
                    <?php
                    $i++;
                }
                $k++;
            } else {
                ?>
                <tr>
                    <td class="detailheading">Project Name:-<?= $buildingName['title'] ?></td>
                </tr>
                <tr>
                    <td class="detailtext">No Documents</td>
                </tr>
                <?php
            }
            ?>


            <?php
        }
    }

    ?>


</table><br><br>


<?php
?>
<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>4. Business Terms and Conditions</h4></td>
    </tr>
</table>
<table>
    <!-- VatInTotalValuationFee -->
    <tr><br>
        <td class="detailheading"><h4>4.1. Fee</h4></td>
    </tr>
    <tr>
        <td class="detailtext">

            Net Valuation Fee = <?=
            ($aprvd_total_discount_amount <> null) ?
                number_format($toe_fee_total - $aprvd_total_discount_amount, 2) :
                number_format($toe_fee_total - $total_discount_amount, 2) ?><br>
            <?php if ($urgencyfee['value'] > 0) {
                $total_net_fee = 1;
                ?>
                Urgency Fee (+)= <?=
                ($aprvd_urgencyfee <> null) ?
                    number_format($aprvd_urgencyfee['amount'], 2) :
                    number_format($urgencyfee['amount'], 2)
                ?><br>

            <?php } ?>

            <?php if ($total_net_fee > 0) { ?>
                Total Net Valuation Fee = <?=
                ($aprvd_netValuationFee <> null) ?
                    number_format($aprvd_netValuationFee, 2) :
                    number_format($netValuationFee, 2) ?><br>
            <?php } ?>
            <?= ($model->client->vat == 1 AND $VatInTotalValuationFee > 0) ?
                ($aprvd_VatInTotalValuationFee <> null && $aprvd_VatInTotalValuationFee > 0) ?
                    'plus 5% VAT = ' . number_format($aprvd_VatInTotalValuationFee, 2) :
                    'plus 5% VAT = ' . number_format($VatInTotalValuationFee, 2) : 'plus 0% VAT' ?>
            . <br><br>
            Total Fee = including VAT will therefore be:<br>
            AED <?= ($aprvd_finalFeePayable <> null)? $aprvd_finalFeePayable : $finalFeePayable ?>/=<br>
            (<?= ($aprvd_finalFeePayableInWords <> null)? $aprvd_finalFeePayableInWords : $finalFeePayableInWords ?> Dirhams Only)
        </td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>4.2. Payment Terms</h4></td>
    </tr>
    <tr>
        <!-- <td class="detailtext">The fee is payable <?/*= $mainTableData['advance_payment_terms'] */?> in advance (Payment
            Structure). The fee is non-refundable
        </td>-->
        <?php
        if($mainTableData['advance_payment_terms'] == '50%'){ ?>
            <td class="detailtext">The fee is payable <?= $mainTableData['advance_payment_terms'] ?> in advance (Payment
                Structure). The remaining 50% is to be paid within 3 days after releasing the draft valuation report to the client unconditionally. Both advance and remaining fees are non-refundable under any circumstances.
            </td>
        <?php }else{?>
            <td class="detailtext">The fee is payable <?= $mainTableData['advance_payment_terms'] ?> in advance (Payment
                Structure). The fee is non-refundable under any circumstances.
            </td>
        <?php } ?>


    </tr>
</table><br><br>


<table>
    <tr>
        <td colspan="2" class="detailheading"><h4>4.3. Payment Method</h4></td>
        <!-- <td class="detailheading"></td> -->
    </tr>
    <tr>
        <td colspan="2" class="detailtext"><?= $standardReport['payment_method'] ?></td>
        <!-- <td class="detailtext"></td> -->
    </tr>
    <tr>
        <td class="detailtext" style="width: 80px;">Bank</td>
        <td class="detailtext"><?= $bankName ?></td>
    </tr>
    <tr>
        <td class="detailtext" style="width: 80px;">IBAN</td>
        <td class="detailtext"><?= $iban ?></td>
    </tr>
    <tr>
        <td class="detailtext" style="width: 80px;">Our TRN is</td>
        <td class="detailtext"><?= $trn ?></td>
    </tr>
</table><br><br>


<table>
    <tr>
        <td class="detailheading"><h4>4.4. Timings</h4></td>
    </tr>
    <tr>
        <td class="detailtext">The delivery time for this assignment will be <?= $totalTurnAroundTime ?> Working Days
            from and after the:<?= $standardReport['timings'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>4.5. RICS Valuation Standards and Departures</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['rics_valuation_standards_departures'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>4.6. Valuation Approaches</h4></td>
    </tr>
    <tr>
    <td class="detailtext">
    <?php
   echo Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach];
    ?>
    </td>
    </tr>

</table><br>

<table>
    <tr>
        <td class="detailheading"><h4>4.7. Special Assumptions</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $specialAssumptionsNamesConcat ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>5. Identification and Status of the Valuer</h4></td>
    </tr>
</table>

<table>
    <tr>
        <br>
        <td class="detailtext sixpoints"><h4>5.1 Valuer Name </h4></td>
        <td class="detailtext"><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?>
            <br>(referred to in this document as “Valuer”).
        </td>
    </tr>

    <tr>
        <br>
        <td class="detailtext sixpoints"><h4>5.2. Valuer Firm</h4></td>
        <td class="detailtext"><?= $branchCompany; ?>
            <br>(referred to in this document as “Firm”).
        </td>
    </tr>

    <tr>
        <br>
        <td class="detailtext sixpoints"><h4>5.3. Valuer Qualifications</h4></td>
        <td class="detailtext"><?= $valuerOther['valuer_qualifications'] ?>
        </td>
    </tr>

    <tr>
        <br>
        <td class="detailtext sixpoints"><h4>5.4. Valuer Status</h4></td>
        <td class="detailtext"><?= $valuerOther['valuer_status'] ?></td>
    </tr>
</table><br><br>


<table>
    <tr>
        <td class="detailheading"><h4>5.5. Valuer Experience and Expertise</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $valuerOther['valuer_experience_expertise'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>5.6. Valuer Duties and Supervision</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['valuer_duties_supervision'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>5.7. Internal/External Status of the Valuer</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['internal_external_status_of_the_valuer'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>5.8. Previous involvement and Conflict of Interest</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['previous_involvement_and_conflict_of_interest'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>5.9. Report Handover</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['report_handover'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>6. Valuation Terms </h4></td>
    </tr>
</table><br><br>

<table>
    <br>
    <tr>
        <td class="detailheading"><h4>6.1. Currency:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['currency'] ?></td>
    </tr>
</table><br><br>

<table>
    <br>
    <tr>
        <td class="detailheading"><h4>6.2. Basis of Value:</h4></td>
    </tr>
    <?php
    if ($multipleProperty['purpose_of_valuation'] == 3) {
        $standardReport['basis_of_value'] = str_replace("{market_fair}", 'Fair', $standardReport['basis_of_value']);
        $standardReport['basis_of_value'] = str_replace("{value_rent}", 'Value', $standardReport['basis_of_value']);
        
    } else {
        $standardReport['basis_of_value'] = str_replace("{market_fair}", 'Market', $standardReport['basis_of_value']);
        if($mainTableData['scope_of_service'] == 5){
            $standardReport['basis_of_value'] = str_replace("{value_rent}", 'Rent', $standardReport['basis_of_value']);
        }else {
            $standardReport['basis_of_value'] = str_replace("{value_rent}", 'Value', $standardReport['basis_of_value']);
        }
    }


    ?>

    <tr>
        <td class="detailtext"><?= $standardReport['basis_of_value'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>6.3. Market Value:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['market_value'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>6.4. Statutory definition of Market Value (capital gains tax, inheritance tax and
                stamp duty land tax).</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['statutory_definition_of_market_value'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>6.5. Market Rent:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['market_rent'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>6.6. Investment Value (Worth)</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['investment_value'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>6.7. Fair Value</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['fair_value'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>6.8. Valuation Date</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= ($mainTableData['valuation_date'] !== null) ? "The valuation date is specific and is agreed to be ".date("F j, Y", strtotime($mainTableData['valuation_date'])) : $standardReport['valuation_date'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>6.9. Modified Properties</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['property'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading onlymainheading"><h4>7. General Assumptions</h4></td>
    </tr>
</table><br><br>

<table>
    <br>
    <tr>
        <td class="detailheading"><h4>7.1 Assumptions, Extent of Investigations, Limitations on the Scope of Work:</h4>
        </td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['assumptions_ext_of_investi_limi_scope_of_work'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.2 Physical Inspection</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['physical_inspection'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.3. Desktop or Drive by Valuation</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['desktop_or_driven_by_valuation'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.4 Structural and Technical Survey</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['structural_and_technical_survey'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.5 Conditions and State of Repair and Maintenance of the Property</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['conditions_state_repair_maintenance_property'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.6 Contamination, and Ground and Environmental Considerations</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['contamination_ground_environmental_consideration'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.7 Statutory and regulatory requirements</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['statutory_and_regulatory_requirements'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.8 Title, Tenancies and Property Documents:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['title_tenancies_and_property_document'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.9 Planning and Highway Enquiries:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['planning_and_highway_enquiries'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.10 Plant and Equipment:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['plant_and_equipment'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.11. Development Properties:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['development_properties'] ?></td>
    </tr>
</table><br><br>

<table>
    <tr>
        <td class="detailheading"><h4>7.12. Insurance Re-Instatement Cost (If applicable):</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['insurance_reinstalement_cost'] ?></td>
    </tr>
</table>


<table>

    <tr>
        <br>
        <td class="detailheading onlymainheading"><h4>8. Nature and Sources of Information and Documents to be Relied
                Upon</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['nature_source_information_documents_relied_upon'] ?>
        </td>
    </tr>
</table><br><br>

<table>

    <tr>
        <td class="detailheading onlymainheading"><h4>9. Description of Report:</h4></td>
    </tr>
    <tr>

        <td class="detailtext"><?= $standardReport['description_of_report'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>
        <td class="detailheading onlymainheading"><h4>10. Client Acceptance of the Valuation Report</h4></td>
    </tr>
    <tr>

        <td class="detailtext"><?= $standardReport['client_acceptance_of_the_valuation_report'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>
        <td class="detailheading onlymainheading"><h4>11. Restrictions on Publications</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['restrictions_on_publications'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>12. Third party liability</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['third_party_liability'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>13. Valuation Uncertainty</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['valuation_uncertaninty'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>14. Complaints:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['complaints'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>15. RICS Monitoring</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['rics_monitoring'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>16. RERA Monitoring</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['rera_monitoring'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>17. Limitations on liability</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['limitions_on_liabity'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>18. Liability & Duty of Care:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['liability_and_duty_of_care'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>19. Extent of Fee </h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['extent_of_fee'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>20. Indemnification:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['indemnification'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>21. Professional Indemnity Insurance:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['professional_indemmnity_insurance'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>22. Client’s Obligations:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['clients_obligations'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>23. Validity of the Terms of Engagement:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['proposal_validity'] ?></td>
    </tr>
</table><br><br>

<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>24. Jurisdiction:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['jurisdiction'] ?></td>
    </tr>
</table><br><br>


<table>

    <tr>

        <td class="detailheading onlymainheading"><h4>25. Acceptance of Terms of Engagement:</h4></td>
    </tr>
    <tr>
        <td class="detailtext"><?= $standardReport['acceptance_of_terms_of_engagement'] ?></td>
    </tr>
</table><br><br>


<style>
    td.centre {
        text-align: left;
        /*vertical-align: middle;*/
        font-size: 11px;
    }
</style>

<table>
    <tr>

        <td colspan="5" class="centre">Signed (Firm)</td>
        <!-- <td colspan="1" class="centre"></td> -->
        <td colspan="1" class="centre"></td>
        <td colspan="4" class="centre">Signed (Client)</td>


    </tr>
    <tr>
                <?php
                ?>

                

        <?php if ( $model_valuer_id == 21 ) {  
            $sigImg = \yii\helpers\Url::to('@web/images/lksign.png'); ?>
            <td colspan="5" class="centre detailtext">
                <img src="<?php echo $sigImg ?>" style="height: 40px; ">    
                <br><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?><br><?= $valuerJobName['title'] ?> 
            </td>
        <?php } else if ( $model_valuer_id == 111558 ) {
            $sigImg = \yii\helpers\Url::to('@web/images/ahmet_sig.jpeg'); ?>
            <td colspan="5" class="centre detailtext">
                <img src="<?php echo $sigImg ?>" style="height: 100px; ">
                <br><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?><br><?= $valuerJobName['title'] ?> 
            </td>    
        <?php } else if ( $model_valuer_id == 381 ) {
            $sigImg = \yii\helpers\Url::to('@web/images/feb_sig6537cbcf709e8.png');  ?>
            <td colspan="5" class="centre detailtext">
                <img src="<?php echo $sigImg ?>" style="height: 80px; ">
                <br><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?><br><?= $valuerJobName['title'] ?> 
            </td>    
        <?php } else if ( $model_valuer_id == 110821 ) {
            $sigImg = \yii\helpers\Url::to('@web/images/IMG_387765b2070e17fe2.jpg');  ?>
            <td colspan="5" class="centre detailtext">
                <img src="<?php echo $sigImg ?>" style="height: 60px; ">
                <br><?= $ValuerName['firstname'] . ' ' . $ValuerName['lastname'] ?><br><?= $valuerJobName['title'] ?> 
            </td>    
        <?php } ?>


        <!-- <td colspan="1" class="centre"></td> -->
        <td colspan="1" class="centre"></td>
        <td colspan="4" class="centre"></td>


    </tr>
    <br>


    <tr>


        <td colspan="5" class="centre">For and on behalf of
            <br><br><?= $branchCompany; ?>
        </td>
        <!-- <td colspan="1" class="centre"></td> -->
        <td colspan="1" class="centre"></td>
        <td colspan="4" class="centre">For and on behalf of
            <br><br><?= $clientName['title']; ?></td>

    </tr>
    <br><br>

    <!-- <tr>
         <td colspan="4" class="centre">Windmills Real Estate Valuation Services LLC</td>
         <td colspan="1" class="centre"></td>
         <td colspan="1" class="centre"></td>

         <td colspan="4" class="centre"></td>

     </tr>-->
</table>
