<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CrmQuotations */

$this->title = 'Update Crm Quotations: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Crm Quotations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="crm-quotations-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
