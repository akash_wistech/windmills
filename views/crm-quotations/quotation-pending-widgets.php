<style>
    h4 {
        font-size: 16px !important;
        font-weight: bold;
        font-family: sans-serif !important;
        width: fit-content;
    }

    .card-header {
        padding: 10px !important;
        padding-bottom: 20px !important;
    }

    .card-footer {
        padding: 0px !important;
    }

    .card-footer span {
        font-size: 13.5px !important;
    }

    .pending-text-info {
        color: #08A86D !important;
    }
</style>

<?php
$amountAllowedId = [1, 14, 33];

// total inquiry received
$pending_total_inquiries = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 0])
    ->andWhere(['status_approve' => null])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// total proposals recommended
$total_recommended = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 17])
    ->orWhere(['not', ['quotation_recommended_date' => null]])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    // ->andWhere(['>=', $q_tbl . '.id', 2300])
    // ->andWhere(['converted' => null])
    // ->andWhere(['status_approve' => 'Recommended'])
    ->asArray()
    ->all();

// total proposals document requested
$total_documents_requested = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 14])
    ->orWhere(['not', ['document_requested_date' => null]])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// total proposals approved
$total_quotation_approved = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 2])
    ->orWhere(['not', ['approved_date' => null]])
    // ->orWhere(['not', ['quotation_sent_date' => null]])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// total proposals sent
$total_quotation_sent = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 1])
    ->orWhere(['not', ['quotation_sent_date' => null]])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// total proposals accepted
$total_quotations_accepted = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => [16, 3, 4, 5, 6, 8]])
    ->orWhere(['not', ['quotation_accepted_date' => null]])
    // ->orWhere(['not', ['toe_sent_date' => null]])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

$total_toe_recommended = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee, COUNT(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->andWhere(['toe_recommended' => 1])
    // ->andWhere(['status_approve' => "toe_verified"])
    ->andWhere(['not', ['toe_recommended' => null]])
    ->orWhere(['not', ['toe_recommended_date' => null]])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// total proposals toe sent
$total_toe_sent = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 3])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// total proposals toe signed
$total_toe_signed = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->andWhere(['quotation_status' => 5])
    ->andWhere(['not', ['toe_signed_and_received_date' => null]])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// total proposals toe rejected
$total_toe_rejected = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    ->andWhere(['quotation_status' => 8])
    ->andWhere(['not', ['toe_rejected_date' => null]])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// total payment received
$total_payment_received = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    ->andWhere(['not', ['payment_received_date' => null]])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();


// total proposals overview
$total_overview = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// total proposals on hold
$total_hold = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 10])
    ->andWhere(['not', ['on_hold_date' => null]])
    ->andWhere(['trashed' => null])
    ->andWhere(['converted' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();


// total proposals rejected
$total_quotations_rejected = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => [7]])
    ->andWhere(['not', ['quotation_rejected_date' => null]])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// total proposals cancelled
$total_cancelled = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => [11]])
    ->andWhere(['not', ['cancelled_date' => null]])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// total proposals regretted
$total_proposals_regretted = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => [12]])
    ->andWhere(['not', ['regretted_date' => null]])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();


// total proposals tat performance
$total_tat_performance = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => [6]])
    ->andWhere(['trashed' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();


?>


<div class="col-lg-4 text-center">
    <button type="button" class="button success" data-toggle="modal" data-target=".bd-example-modal-xl-6">
        <center>
            <div class=" col-12">
                <h4 class="card-category">
                    <span class="t-color-2"> Total Proposals Performance </span>
                    <br />
                    <span style="float:left"><?= number_format($total_overview[0]['totalRecords']) ?></span>
                    <span style="float:right">
                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                            <?= number_format($total_overview[0]['totalFee']) ?>
                        <?php } ?>
                    </span>
                </h4>
            </div>
        </center>
    </button>
</div>

<div class="modal fade bd-example-modal-xl-6" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> Total Proposals Performance </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-body" style="background-color:#E5E4E2">

                <div class="row px-4">

                <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_proposals_overview')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right " style="color:#757575">
                                        <span class="pending-text-info">Total Proposals Overview</span><br>
                                        <?= number_format($total_overview[0]['totalRecords']) ?></h4>
                                    <h4 class=" float-right " style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_overview[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase" style="left:53px;top:5px;"
                                            target="_blank"
                                            href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all','CrmQuotationsSearch[time_period]'=>'4', 'CrmQuotationsSearch[status_type]'=>'inquiry_overview']) ?>">
                                            <span style="font-size:20px;">Go to Total Proposals Overview</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_quotations_recommended')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right "
                                        style="color:#757575">
                                        <span class="pending-text-info">Total Proposals Recommended</span><br>
                                        <?= number_format($total_recommended[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right " style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_recommended[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[time_period]' => '4', 'CrmQuotationsSearch[status_type]' => 'quotation_recommended']) ?>">
                                        <span style="font-size:20px;">Go to Total Proposals Recommended</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?> 

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_quotations_documents')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Total Documents Requested</span><br>
                                        <?= number_format($total_documents_requested[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_documents_requested[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase "
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[time_period]' => '4', 'CrmQuotationsSearch[status_type]' => 'document_requested']) ?>">
                                        <span style="font-size:20px;">Go to Total Documents Requested</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?> 

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_quotations_verified')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Total Proposals Verified</span><br>
                                        <?= number_format($total_quotation_approved[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_quotation_approved[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase "
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[time_period]' => '4', 'CrmQuotationsSearch[status_type]' => 'quotation_approved']) ?>">
                                        <span style="font-size:20px;">Go to Total Proposals Verified</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?> 

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_quotations_hold')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Total Proposals On Hold</span><br>
                                        <?= number_format($total_hold[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_hold[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase "
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[time_period]' => '4', 'CrmQuotationsSearch[status_type]' => 'quotation_onhold']) ?>">
                                        <span style="font-size:20px;">Go to Total Proposals On Hold</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?> 

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_quotations_approved')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Total Proposals Approved</span><br>
                                        <?= number_format($total_quotations_accepted[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_quotations_accepted[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[time_period]' => '4', 'CrmQuotationsSearch[status_type]' => 'quotation_accepted']) ?>">
                                        <span style="font-size:20px;">Go to Total Proposals Approved</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?> 

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_quotations_rejected')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Total Proposals Rejected</span><br>
                                        <?= number_format($total_quotations_rejected[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_quotations_rejected[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[time_period]' => '4', 'CrmQuotationsSearch[status_type]' => 'quotation_rejected']) ?>">
                                        <span style="font-size:20px;">Go to Total Proposals Rejected</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?> 

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_quotations_cancelled')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Total Proposals Cancelled</span><br>
                                        <?= number_format($total_cancelled[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_cancelled[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase "
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[time_period]' => '4', 'CrmQuotationsSearch[status_type]' => 'quotation_cancelled']) ?>">
                                        <span style="font-size:20px;">Go to Total Proposals Cancelled</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_toe_verified')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#616161">
                                        <span class="pending-text-info">Total TOE Recommended</span><br>
                                        <?= number_format($total_toe_recommended[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#616161">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_toe_recommended[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase text-primary-dynamic"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[status_type]' => 'toe_recommended']) ?>">
                                        <span style="font-size:20px;">Go to Total TOE Recommended</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_toe_signed')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right "
                                        style="color:#757575">
                                        <span class="pending-text-info">Total TOE Signed</span><br>
                                        <?= number_format($total_toe_signed[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right " style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_toe_signed[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[status_type]' => 'toe_signed_and_received']) ?>">
                                        <span style="font-size:20px;">Go to Total TOE Signed</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_toe_rejected')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right "
                                        style="color:#757575">
                                        <span class="pending-text-info">Total TOE Rejected</span><br>
                                        <?= number_format($total_toe_rejected[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right " style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_toe_rejected[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[status_type]' => 'toe_rejected']) ?>">
                                        <span style="font-size:20px;">Go to Total TOE Rejected</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_payment_received')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Total Payment Received</span><br>
                                        <?= number_format($total_payment_received[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_payment_received[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase "
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[status_type]' => 'payment_received']) ?>">
                                        <span style="font-size:20px;">Go to Total Payment Received</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_windmills_regretted')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Total Windmills Regretted</span><br>
                                        <?= number_format($total_proposals_regretted[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_proposals_regretted[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase "
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[status_type]' => 'regretted']) ?>">
                                        <span style="font-size:20px;">Go to Total Windmills Regretted</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_quotations_tat')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right "
                                        style="color:#757575">
                                        <span class="pending-text-info">Total TAT Performance</span><br>
                                        <?= number_format($total_tat_performance[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right " style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($total_tat_performance[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'all', 'CrmQuotationsSearch[time_period]' => '4', 'CrmQuotationsSearch[status_type]' => 'tat_performance']) ?>">
                                        <span style="font-size:20px;">Go to Total TAT Performance</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                </div>
              
            </div>

        </div>
    </div>
</div>
