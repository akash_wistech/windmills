<?php

use app\models\CrmReceivedProperties;

$receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
$quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
$toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
$toe_fee_total_ad = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');

// if($model->payment_status == 1){


//   $quotation_fee_total= number_format(($quotation_fee_total * ('0.'.$model->first_half_payment)), 2, '.', '');
//   $toe_fee_total= number_format(($toe_fee_total * ('0.'.$model->first_half_payment)), 2, '.', '');
// }

$relative_discount=0;
$no_of_property_discount=0;
$first_time_discount=0;
$general_discount=0;

$total_no_of_prperties = count($receivedProperties);


$total_discount=0;
$total_discount_amount=0;
$discount = 0;
$netValuationFee = $toe_fee_total-$discount;
//$netValuationFee_ad = $toe_fee_total-$discount;
//$netValuationFee_ad = $toe_fee_total_ad;
if($model->no_of_property_discount > 0) {
    $no_of_property_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $toe_fee_total);
    $total_discount= $total_discount + $model->no_of_property_discount;
    $total_discount_amount = $total_discount_amount + $no_of_property_discount_amount;
}
if($model->same_building_discount > 0) {
    $same_building_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $toe_fee_total);
    $total_discount= $total_discount + $model->same_building_discount;
    $total_discount_amount = $total_discount_amount + $same_building_discount_amount;
}
if($model->first_time_discount > 0) {
    $first_time_fee_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $toe_fee_total);
    $total_discount= $total_discount + $model->first_time_discount;
    $total_discount_amount = $total_discount_amount + $first_time_fee_discount_amount;
}
$netValuationFee = $netValuationFee - $total_discount_amount;

$discount_quotations = 0;
if ($model->relative_discount_toe!=null) {
    $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total,$model->relative_discount_toe);
    $netValuationFee = $netValuationFee - $discount;


}
if ($model->relative_discount!=null) {
    $discount_quotations =  yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total,$model->relative_discount);
}

$discount_net_fee =$netValuationFee;

//urgencyfee check
if ($model->tat_requirements > 0) {
    $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $netValuationFee);
    $netValuationFee = $netValuationFee + $urgencyfee['amount'];
}

//Advance Payment Terms
if ($model->client->client_type != 'bank') {
    $advance_payment_terms_data = explode('%', $model->advance_payment_terms);

    $advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($advance_payment_terms_data[0], $netValuationFee);
}else{
    $advance_payment_terms['amount'] = 0;
    $advance_payment_terms['value'] = 0;
}
/*if($model->payment_status == 1){


    $netValuationFee= number_format(($netValuationFee * ('0.'.$model->first_half_payment)), 2, '.', '');
}*/

$netValuationFee = $netValuationFee + $advance_payment_terms['amount'];


if($model->client->vat== 1) {
    $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
}

$finalFeePayable = $netValuationFee+$VAT;


$quotation = \app\models\CrmQuotations::find()->select(['advance_payment_terms','second_half_payment','first_half_payment','payment_status','scope_of_service','payment_received_date','reference_number','quotation_sent_date','email_status_pr'])->where(['id'=>$id])->one();

$datetimeString = $quotation->payment_received_date;
$dateTime = new DateTime($datetimeString);
$QuotationDateOnly = $dateTime->format("d M Y");


if(isset($quotation->payment_status) && $quotation->payment_status == "1" )
{
  if( isset($quotation->first_half_payment) && $quotation->email_status_pr == 0 )
  {
    $payment_amt = number_format((($finalFeePayable/100)*$quotation->first_half_payment),0);
    $description = $quotation->first_half_payment.' % of Invoice';
  }else if( isset($quotation->second_half_payment) && $quotation->email_status_pr == 1 )
  {
    $payment_amt = number_format((($finalFeePayable/100)*$quotation->second_half_payment),0);
    $description = $quotation->second_half_payment.' % of Invoice';
  }

}elseif( $quotation->advance_payment_terms == "100%" || $quotation->advance_payment_terms == "0%" )
{
  $payment_amt = number_format((($finalFeePayable/100)*100),0);
  $description = '100% of Invoice';
}

$clientAddress = \app\models\Company::find()->select(['title','address','city'])->where(['id'=>$model->client_name])->one();

?>


<br><br>

<style>
.box1{
  background-color: #E3F2FD;
  width: 90px;
  border-bottom: 0.5px solid #1E88E5;
}
.box2{
  background-color: #19B65F;
  width: 150px;
  border-bottom: 0.5px solid #1E88E5;
  height: 75px;
}
.box3{
  background-color: #E3F2FD;
  width: 90px;
  border-bottom: 0.5px solid #1E88E5;
}
</style>
<style>
  .airal-family{
    font-family: Arial, Helvetica, sans-serif;
  }
</style>


<style>

  .text-dec{
    font-size: 11px;
    text-align: center;
  }

/*td.detailtext{*/
/*background-color:#BDBDBD; */
/*font-size:9px;*/
/*border-top: 0.4px solid grey;*/
/*}*/

td.detailtexttext{
/* background-color:#EEEEEE; */
font-size:11px;
}
th.detailtext{
/* background-color:#BDBDBD; */
font-size:9px;
border-top: 1px solid black;
}
/*.border {
border: 1px solid black;
}*/

th.amount-heading{
  color: #039BE5;
}
td.amount-heading{
  color: #039BE5;
}
th.total-due{
  font-size: 16px;
}
td.total-due{
  font-size: 16px;
}
span.size-8{
  font-size: 10px;
  font-weight: bold;
}
span.size-10{
  font-size: 12px;
  font-weight: bold;
}
td.first-section{
  padding-left: 10px;
/* background-color: black; */
}
</style>

<hr style="height: 0.2px;">

<table>
<br><br><span class="size-10 airal-family text-dec">PAYMENT RECEIPT</span><br><br><br>
    <tr>
        <td class="airal-family" style="font-size:10px"; >Payment Date : </td>
        <td style="width:10px"></td>

        <td colspan="3" class="airal-family" style="font-size:10px; border-bottom: 1px solid #cacaca";><?php echo $QuotationDateOnly?></td>
        <td style="width:20px"></td>
        <td colspan="0" rowspan="3" class="box2 text-dec airal-family">
          <p style="color: white;"> AMOUNT RECEIVED <br> <b> AED <?php echo $payment_amt ?> </b>
          </p>
        </td>
    </tr>
    <br>
    <tr>
        <td class="airal-family" style="font-size:10px"; >Quotation Number : </td>
        <td style="width:10px"></td>

        <td colspan="3" class="airal-family" style="font-size:10px;  border-bottom: 1px solid #cacaca";><?php echo $quotation->reference_number ?></td>
        <td style="width:20px"></td>

    </tr>
    <br>
    <tr>
        <td class="airal-family" style="font-size:10px";>Payment Method : </td>
        <td style="width:10px"></td>

        <td colspan="3" class="airal-family" style="font-size:10px;  border-bottom: 1px solid #cacaca";>Bank Transfer</td>
        <td style="width:20px"></td>

    </tr>
</table>

<br><br><br><br><br>
<span class="size-8 airal-family">Bill To</span>
<p style="font-size:10px"; class="airal-family"><b><?= ucwords($clientAddress->title) ?></b><br><?=$clientAddress->address?><br><?=$clientAddress->city?><br><br></p>
<br><br><br><br><br>


<span class="size-8 airal-family">Payment For</span>
<table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <hr style="height: 0.2px;">
    <tr style="">
      <td class="airal-family" style="font-size:9px; font-weight: bold; width:150px; border-bottom: 0.2px solid black;">Description</td>
      <td class="airal-family" style="width: 150px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">Quotation Date</td>
      <td class="airal-family" style="width: 120px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">Invoice Amount</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align: right; border-bottom: 0.2px solid black;">Payment Amount</td>
    </tr>
  </thead>

  <tbody>
    <?php
    $i=1;
    if($receivedProperties <> null && !empty($receivedProperties)) {

        ?>
        <tr style="">
          <td class="airal-family" style="font-size:9px; width:150px; "><?= Yii::$app->appHelperFunctions->ScopeOfWorkList[$quotation->scope_of_service]. ' ' .$description ?></td>
          <td class="airal-family" style="width: 150px; font-size:9px; text-align: center;"><?php echo $QuotationDateOnly ?></td>
          <td class="airal-family" style="width: 150px; font-size:9px; text-align: center;">AED <?php echo number_format($finalFeePayable,0) ?></td>
          <td class="airal-family" style="font-size:9px; text-align: center;">AED <?php echo $payment_amt ?></td>
        </tr>

        <?php
        $i++;
        if ($i<=count($receivedProperties)) {?>
          <hr style="height: 0.2px;">

          <?php
        }

    }
    ?>

  </tbody>
</table>
<hr style="height: 0.3px;">

<br><br>
<!-- <table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <tr style="background-color: ;">
      <td class="airal-family" style="font-size:10px; text-align:left;">Accepted By</td>
      <td class="airal-family" style="width: 90px; font-size:10px; text-align:left;">Accepted Date</td>
    </tr>
  </thead>
</table> -->




