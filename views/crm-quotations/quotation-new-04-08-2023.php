<?php
use app\models\ProposalMasterFile;
use app\models\Valuation;
use app\models\CrmReceivedProperties;

$mainTableData = yii::$app->quotationHelperFunctions->getMainTableDataNew($id);
$client = yii::$app->quotationHelperFunctions->getClientAddress($mainTableData['client_name']);
$properties = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id);

$standardReport = Yii::$app->quotationHelperFunctions->standardReport();
$quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
$quotation_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('tat');



$relative_discount=0;
$no_of_property_discount=0;
$first_time_discount=0;
$general_discount=0;

$total_no_of_prperties = count($properties);
$relative_discount = $mainTableData['relative_discount'];




$total_discount=0;
$total_discount_amount=0;
$discount = 0;
if ($model->client->client_type != 'bank') {
//Advance Payment Terms
    $advance_payment_terms_data = explode('%', $model->advance_payment_terms);

    $advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($advance_payment_terms_data[0], $quotation_fee_total);
}else{
    $advance_payment_terms['amount'] = 0;
    $advance_payment_terms['value'] = 0;
}
$quotation_fee_total = $quotation_fee_total + $advance_payment_terms['amount'];
if($model->no_of_property_discount > 0) {
    $no_of_property_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $quotation_fee_total);
    $total_discount= $total_discount + $model->no_of_property_discount;
    $total_discount_amount = $total_discount_amount + $no_of_property_discount_amount;
}
if($model->same_building_discount > 0) {
    $same_building_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $quotation_fee_total);
    $total_discount= $total_discount + $model->same_building_discount;
    $total_discount_amount = $total_discount_amount + $same_building_discount_amount;
}
if($model->first_time_discount > 0) {
    $first_time_fee_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $quotation_fee_total);
    $total_discount= $total_discount + $model->first_time_discount;
    $total_discount_amount = $total_discount_amount + $first_time_fee_discount_amount;
}

if ($relative_discount!=null) {
    $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($quotation_fee_total,$relative_discount);
    $total_discount= $total_discount + $relative_discount;
    $total_discount_amount = $total_discount_amount + $discount;
}

$netValuationFee = $quotation_fee_total-$total_discount_amount;
$discount_net_fee =$netValuationFee;

//urgencyfee check
if ($model->tat_requirements > 0) {
    $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $netValuationFee);
    $netValuationFee = $netValuationFee + $urgencyfee['amount'];
}



if($client['vat']== 1) {
	$VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
}

$finalFeePayable = $netValuationFee+$VAT;







?>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="datamain"><?= date("F j, Y", strtotime(date("Y-m-d"))); ?></td>
    </tr><br>
    <tr>
        <td class="datamain">
            <?= $client['title'] ?>
        </td>
    </tr>
    <?php  
	if ($client['address']!=null) {?>
    <tr>
        <td class="datamain">
            <?= $client['address'] ?>
        </td>
    </tr><br>
    <?php 
	}
?>


    <tr>
        <td class="data">
            <?= $standardReport['quotation_last_paragraph'] ?>
        </td>
    </tr>
</table>

<style>
.table tr .datamain {
    font-size: 10px;
}
</style>

<br pagebreak="true" />

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">
            Date
        </td>
    </tr>
    <tr>
        <td class="data">
            <?= date("F j, Y", strtotime(date("Y-m-d"))) ?>
        </td>
    </tr>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">
            Quotation Reference No.
        </td>
    </tr>
    <tr>
        <td class="data">
            <?= $mainTableData['reference_number'] ?>
        </td>
    </tr>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">
            Client
        </td>
    </tr>
    <tr>
        <td class="data">
            <?= $client['title'] ?>
        </td>
    </tr>

    <?php  
	if ($client['address']!=null) {?>
    <tr>
        <td class="datamain">
            <?= $client['address'] ?>
        </td>
    </tr>
    <?php 
	}
?>
    <?php
    if ($mainTableData['client_name'] != 9167) {?>
    <!--<tr>
		<td class="data">
			Dubai, United Arab Emirates.
		</td>
	</tr>-->
    <?php
    }
    ?>
</table>

<br><br>

<table class="table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading">
            Service
        </td>
    </tr>
    <tr>
        <td class="data"><?= yii::$app->appHelperFunctions->scopeOfWorkArr[$mainTableData['scope_of_service']]?>
            <!--Valuation of Real Estate Interests of Subject Properties-->
        </td>
    </tr>
</table>

<style>
.table tr .heading {
    border: 1px solid black;
    font-size: 11px;
    color: #0091EA;
    text-indent: 5px;
}

.table tr .data {
    font-size: 10px;
    text-indent: 5px;
}
</style>

<br><br>

<table class="property-table" cellspacing="1" cellpadding="2">
    <tr>
        <td class="heading" colspan="10">
            Subject Properties
        </td>
    </tr>


    <tr>
        <td class="property-heading" colspan="2" style="border-left: 1px solid black;">Properties</td>
        <td class="property-heading" colspan="2">Property Address</td>
        <td class="property-heading" colspan="2">Property Description</td>
        <td class="property-heading" colspan="2">Turn Around Time</td>
        <td class="property-heading" colspan="2" style="border-right: 1px solid black; text-align: right">Fee</td>
    </tr>
    <br>
    <?php
	$totalValuationFee = 0;
	$totalTurnAroundTime = 0;
	if ($properties!=null) {

		foreach ($properties as $ky => $value) {
            $property = \app\models\CrmReceivedProperties::find()->where(['id' => $value['id']])->one();
            $address = '';
            if($value['unit_number'] != ''){
                $address= $value['unit_number'].', '.$value['street'].', '.Yii::$app->appHelperFunctions->emiratedListArr[$property->building->city];
            }else{
                $address= $value['street'].', '.Yii::$app->appHelperFunctions->emiratedListArr[$property->building->city];
            }

            $prperty_description = '';

           

            // dd("usa");

	?>
    <tr>
        <td class="data" colspan="2" style="text-indent: 5px;"><?= $property->building->title ?>,<br>
            <?=  Yii::$app->appHelperFunctions->getPropertiesCategoriesListArr()[$value['property_category']] ?>
        </td>
        <td class="data" colspan="2"><?=  $address?></td>
        <td class="data" colspan="2" style="text-indent: 5px;"></td>

        <td class="data" colspan="2" style="text-indent: 5px;"><?=  $value['tat'] ?></td>
        <td class="data" colspan="2" style="text-indent: 5px; text-align: right"><?=  $value['quotation_fee'] ?></td>
    </tr><br>
    <?php
			$totalValuationFee += $value['quotation_fee'];
			$totalTurnAroundTime += $value['tat'];
		}
	}
	?>
</table>



<style>
.property-table tr .heading {
    border-top: 1px solid black;
    border-left: 1px solid black;
    border-right: 1px solid black;
    font-size: 11px;
    color: #0091EA;
    text-indent: 5px;
}

.property-table tr .property-heading {
    font-size: 10px;
    text-indent: 5px;
    border-top: 1px solid black;
    border-bottom: 1px solid black;
    /*text-align:center;*/
}

.property-table tr .data {
    font-size: 10px;

}
</style>

<br><br>

<table cellspacing="1" cellpadding="2" class="bill-table" style="text-align: right">


    <tr>
        <td class="bill-data" style="border-left: 1px solid black;">Total Number of Properties <?= $mainTableData['no_of_properties'] ?></td>

        <td colspan="2" class="bill-data">Total Valuation Fee</td>
        <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($totalValuationFee,2) ?></td>
    </tr>

    <tr>
        <td class="bill-data" style="border-left: 1px solid black;">Turn Around Time <?= $totalTurnAroundTime ?> Days</td>

        <td colspan="3" class="bill-data" style="border-right: 1px solid black;"></td>
    </tr>

   <!-- <tr>
        <td class="bill-data" style="border-left: 1px solid black;">Payment Terms</td>
        <td class="bill-data"><?/*= $mainTableData['advance_payment_terms'] */?> Advance</td>

        <td class="bill-data"></td>
        <td class="bill-data" style="border-right: 1px solid black;"></td>
    </tr>-->




<?php if ($model->no_of_property_discount>0) {?>
	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>

		<?php if ($model->no_of_property_discount>0) { ?>
			<td colspan="2" class="bill-data">Multiple Properties Discount -(<?= $model->no_of_property_discount ?>%)</td>
			<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($no_of_property_discount_amount,2) ?></td>
		<?php
		}else{?>
			<td colspan="2" class="bill-data">Multiple Properties Discoun (0%)</td>
       		<td class="bill-data" style="border-right: 1px solid black;">0</td>
		<?php
		}
		?>
    </tr>
	<?php } ?>

    <?php if ($model->same_building_discount>0) {?>
        <tr>
            <td class="bill-data" style="border-left: 1px solid black;"></td>

            <?php if ($model->no_of_property_discount>0) { ?>
                <td colspan="2" class="bill-data">Number Of Units In The Same Building Discount -(<?= $model->same_building_discount ?>%)</td>
                <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($same_building_discount_amount,2) ?></td>
                <?php
            }else{?>
                <td colspan="2" class="bill-data">Number Of Units In The Same Building Discount (0%)</td>
                <td class="bill-data" style="border-right: 1px solid black;">0</td>
                <?php
            }
            ?>
        </tr>
    <?php } ?>

    <?php if ($model->first_time_discount>0) {?>
        <tr>
            <td class="bill-data" style="border-left: 1px solid black;"></td>

            <?php if ($model->first_time_discount>0) { ?>
                <td colspan="2" class="bill-data">First Time Discount -(<?= $model->first_time_discount ?>%)</td>
                <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($first_time_fee_discount_amount,2) ?></td>
                <?php
            }else{?>
                <td colspan="2" class="bill-data">First Time Discount (0%)</td>
                <td class="bill-data" style="border-right: 1px solid black;">0</td>
                <?php
            }
            ?>
        </tr>
    <?php } ?>


    <?php if ($relative_discount>0) {?>
        <tr>
            <td class="bill-data" style="border-left: 1px solid black;"></td>
            <td class="bill-data"></td>
            <?php
            if ($relative_discount>0) {
                if ($mainTableData['relative_discount']=='base-fee') {?>
                    <td class="bill-data">Relationship Discount</td>
                    <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($discount,2) ?></td>
                    <?php
                }else{?>
                    <td class="bill-data">Relationship Discount -(<?= $mainTableData['relative_discount'] ?>%)</td>
                    <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($discount,2) ?></td>
                    <?php
                }
            }else{?>
                <td class="bill-data">Relationship Discount (0%)</td>
                <td class="bill-data" style="border-right: 1px solid black;">0</td>
                <?php
            }
            ?>
        </tr>
    <?php } ?>
	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
		<?php if($total_discount>0){?>
			<td class="bill-data">Total Discount -(<?= $total_discount ?>%)</td>
			<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($total_discount_amount,2) ?></td>
		<?php }else{ ?>
			<td class="bill-data">Total Discount (0%)</td>
			<td class="bill-data" style="border-right: 1px solid black;"><?= number_format(0,2) ?></td>
		<?php } ?>
    </tr>
    <tr>
        <td class="bill-data" style="border-left: 1px solid black;" colspan="2"></td>
        <td class="bill-data" style="border-right: 1px solid black;" colspan="2"></td>
    </tr>
	<tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
		<td class="bill-data">Net Valuation Fee</td>
		<td class="bill-data" style="border-right: 1px solid black;"><?= number_format($discount_net_fee,2) ?></td>
    </tr>


    <?php if ($urgencyfee['value']>0) {?>
        <tr>
            <td class="bill-data" style="border-left: 1px solid black;"></td>

            <?php if ($urgencyfee['value']>0) { ?>
                <td colspan="2" class="bill-data">Urgency Fee +(<?= $urgencyfee['value'] ?>%)</td>
                <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($urgencyfee['amount'],2) ?></td>
                <?php
            }
            ?>
        </tr>
    <?php } ?>

    <?php if ($advance_payment_terms['value']>0) {?>
        <tr>
            <td class="bill-data" style="border-left: 1px solid black;"></td>

            <?php if ($advance_payment_terms['value']>0) { ?>
                <td colspan="2" class="bill-data">Advance Payment Fee +(<?= $advance_payment_terms['value'] ?>%)</td>
                <td class="bill-data" style="border-right: 1px solid black;"> <?= number_format($advance_payment_terms['amount'],2) ?></td>
                <?php
            }
            ?>
        </tr>
    <?php } ?>
    <tr>
        <td class="bill-data" style="border-left: 1px solid black;" colspan="2"></td>
        <td class="bill-data" style="border-right: 1px solid black;" colspan="2"></td>
    </tr>

   <!-- <tr>
        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
        <td class="bill-data">Net Valuation Fee</td>
        <td class="bill-data" style="border-right: 1px solid black;"><?/*= number_format($netValuationFee,2) */?></td>
    </tr>-->



    <tr>
        <!--		<td class="bill-data" style="border-left: 1px solid black;">Quotation Expiry Date</td>
		<td class="bill-data"><?/*= date("F j, Y", strtotime($mainTableData['expiry_date'])); */?></td>-->

        <td class="bill-data" style="border-left: 1px solid black;"></td>
        <td class="bill-data"></td>
        <?php if($client['vat'] == 1){ ?>
        <td class="bill-data">5% VAT</td>
        <td class="bill-data" style="border-right: 1px solid black;"><?= number_format($VAT,2) ?></td>
        <?php }else{ ?>
        <td class="bill-data">VAT</td>
        <td class="bill-data" style="border-right: 1px solid black;">0</td>
        <?php } ?>
    </tr>

    <tr>
        <td class="bill-data" style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
        <td class="bill-data" style="border-bottom: 1px solid black;"></td>
        <td class="bill-data" style="border-bottom: 1px solid black;">Final Fee Payable </td>
        <td class="bill-data" style="border-right: 1px solid black;border-bottom: 1px solid black;"><?= number_format($finalFeePayable) ?></td>
    </tr>


</table>

<style>
.bill-table tr .bill-data {
    font-size: 10px;
    text-indent: 5px;
    color: #0091EA;
    border-top: 1px solid black;
}
</style>