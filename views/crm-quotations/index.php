<?php

use yii\helpers\Html;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;



use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\CrmQuotationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Crm Quotations');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
//if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
$createBtn=true;
//}
//if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
$actionBtns.='{update}';
$actionBtns.='{delete}';
//}
//if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
$actionBtns.='{status}';
//}
$actionBtns .= '{step_0}';
$actionBtns .= '{revise}';
$createBtn=true;

$advance_search = false;

$dAlertTxt = 'Do You Want To Delete This Quotation! This action cannot be undone!';

$this->registerJs('
            $("body").on("click", ".delete-btn", function (e) {
                e.preventDefault();
                href = $(this).data("url")
                swal({
                    title: "'.Yii::t('app', $dAlertTxt).'",
                    html: "'.Yii::t('app','').'",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#47a447",
                    confirmButtonText: "'.Yii::t('app','Yes').'",
                    cancelButtonText: "'.Yii::t('app','Cancel').'",
                },function(result) {
                    if (result) {
                        window.location.replace(href);
                    }
                });
            });

            $(".div1").daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: "YYYY-MM-DD"
                }
            });

            $(".div1").on("apply.daterangepicker", function(ev, picker) {
                $(this).val(picker.startDate.format("YYYY-MM-DD") + " to " + picker.endDate.format("YYYY-MM-DD"));
            });

            $("#crmquotationssearch-date_period").on("change", function(){
                value = $(this).val();
                if(value==9){
                  $(".custom-date-col").show();
                }else{
                  $(".custom-date-col").hide();
                }
            });

            ');

?>

<?php
$hide = 'yes';
if ($searchModel->date_range<>null) {
    $hide = 'no';
}
?>

<section class="card">
    <div class="card-body">
        <?php $form = ActiveForm::begin([
            'action' => ['crm-quotations/index'],
            'options' => [
                'class' => '',
                'style' => ''
            ],
            'method' => 'get',
        ]); ?>
        <div class="row">
            <?= $form->field($searchModel, 'dashboard_filter')->textInput(['value'=>'yes', 'class'=>'d-none'])->label(false) ?>
            <div class="col-3">
                <div class="form-group">
                    <?= $form->field($searchModel, 'quotation_status')->dropDownList(yii::$app->crmQuotationHelperFunctions->getQuotationStatusListArr(), ['prompt'=>'Select'])?>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <?= $form->field($searchModel, 'date_period')->dropDownList(yii::$app->crmQuotationHelperFunctions->getReportPeriod(), ['prompt'=>'Select'])?>
                </div>
            </div>
            <div class="col-3 custom-date-col"  style=" <?= ($hide=='yes')?'display:none;' : '' ?>">
                <div class="form-group">
                    <?= $form->field($searchModel, 'date_range')->textInput(['class'=>'form-control div1'])->label('Select Custom Dates'); ?>
                </div>
            </div>
            <div class="col-3 py-4 my-2">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>


<div class="modal fade" id="advance-search">

    <?php $form = ActiveForm::begin([
        'action' => ['crm-quotations/index'],
        'options' => [
            'class' => '',
            'style' => ''
        ],
        'method' => 'get',
    ]); ?>

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Advance Search</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <?= $form->field($searchModel, 'dashboard_filter')->textInput(['value'=>'yes', 'class'=>'d-none'])->label(false) ?>
                    <div class="col-12">
                        <?= $form->field($searchModel, 'quotation_status')->dropDownList(yii::$app->crmQuotationHelperFunctions->getQuotationStatusListArr(), ['prompt'=>'Select'])?>
                    </div>
                    <div class="col-12">
                        <?= $form->field($searchModel, 'date_range')->textInput(['class'=>'form-control div1'])->label('Date'); ?>
                    </div>
                </div>


            </div>

            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<div class="crm-quotations-index">
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'advance_search' => $advance_search,



        'columns' => [
            'reference_number',

            [
                'attribute' => 'client_name',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                    return $model->client->title;
                },
                'filter' =>  ArrayHelper::map(\app\models\Company::find()
                            ->where(['status' => 1])
                            ->andWhere(['allow_for_valuation'=>1])
                            // ->andWhere([
                            //     'or',
                            //     ['data_type' => 0],
                            //     ['data_type' => null],
                            // ])
                            ->orderBy(['title' => SORT_ASC,])
                            ->all(), 'id', 'title'),
            ],



            [
                'attribute' => 'client_type',
                'enableSorting' => true,
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:15%'],
                'label' => Yii::t('app', 'client Type'),
                'value' => function ($model) {
                    $value= Yii::$app->quotationHelperFunctions->clientType[$model->client->client_type];
                    return $value;
                },
                'contentOptions' => ['class' => ' pt-3 pl-2'],
                'filter' => Yii::$app->quotationHelperFunctions->clientType
            ],





            'client_reference',
            'client_customer_name',
            'no_of_properties',

            'quotation_final_fee',

            [
                'attribute' => 'grand_final_toe_fee',
                'label' => Yii::t('app', 'Toe Final Fee'),
                'value' => function ($model) {
                    return $model->grand_final_toe_fee;
                }
            ],

            'toe_final_turned_around_time',

            ['attribute' => 'city',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    $properties = \app\models\CrmReceivedProperties::find()->where(['quotation_id' => $model->id])->all();
                    $city = '';
                    if(!empty($properties)){
                        $property = \app\models\CrmReceivedProperties::find()->where(['quotation_id' => $model->id, 'property_index' => 0])->one();
                        $city = $property->building->city;
                        return Yii::$app->appHelperFunctions->emiratedListArr[$city];
                    }


                },
            ],

            [
                'format' => 'raw',
                'attribute' => 'quotation_status',
                'label' => Yii::t('app', 'Quotation Status'),
                'value' => function ($model) {
                    
                    if (($model->toe_recommended == "1" && $model->status_approve == "toe_verified") || ($model->toe_recommended == "1" && $model->status_approve == "Approve")) {
                        $icon = '<span class="badge grid-badge badge-warning">TOE Recommended</span>';
                    } else {
                        $icon = '';
                    }
            
                    $value = Yii::$app->crmQuotationHelperFunctions->quotationStatusListArrLabel[$model['quotation_status']];
            
                    return Html::a($icon . ' ' . $value, Url::to(['crm-quotation/step_0', 'id' => $model->id]), [
                        'title' => $value,
                    ]);
                },
                'contentOptions' => ['class' => 'pt-3 pl-2'],
                'filter' => Yii::$app->crmQuotationHelperFunctions->quotationStatusListArr,
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'status_change_date',
                'label' => Yii::t('app', 'Status Update Date'),
                'value' => function ($model) {

                    if ($model->status_change_date<>null) {
                        return '<span class="badge grid-badge badge-primary my-3"> '.date('Y-m-d', strtotime($model->status_change_date)).'</span>';
                    }

                },
            ],


            [
                'format' => 'raw',
                // 'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'inquiry_received_date',
                'label' => Yii::t('app', 'I-R Date'),
                'value' => function ($model) {

                    if ($model->inquiry_received_date<>null) {
                        return '<span class="badge grid-badge badge-success my-3"> '.date('Y-m-d', strtotime($model->inquiry_received_date)).'</span>';
                    }

                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'quotation_sent_date',
                'label' => Yii::t('app', 'Q-S Date'),
                'value' => function ($model) {

                    if ($model->quotation_sent_date<>null) {
                        return '<span class="badge grid-badge badge-primary my-3"> '.date('Y-m-d', strtotime($model->quotation_sent_date)).'</span>';
                    }

                },
            ],

            [
                'format' => 'raw',
                // 'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'toe_sent_date',
                'label' => Yii::t('app', 'ToeSend Date'),
                'value' => function ($model) {

                    if ($model->toe_sent_date<>null) {
                        return '<span class="badge grid-badge badge-info my-3"> '.date('Y-m-d', strtotime($model->toe_sent_date)).'</span>';
                    }

                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'payment_received_date',
                'label' => Yii::t('app', 'P-R Date'),
                'value' => function ($model) {

                    if ($model->payment_received_date<>null) {
                        return '<span class="badge grid-badge badge-warning my-3"> '.date('Y-m-d', strtotime($model->payment_received_date)).'</span>';
                    }

                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'toe_signed_and_received',
                'label' => Yii::t('app', 'Toe Signed & Recieved'),
                'value' => function ($model) {

                    if ($model->toe_signed_and_received<>null) {
                        return '<span class="badge grid-badge badge-success my-3"> '.Yii::$app->crmQuotationHelperFunctions->getToeSignedAndRecievedtArr()[$model['toe_signed_and_received']].'</span>';
                    }
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'toe_signed_and_received_date',
                'label' => Yii::t('app', 'ToeSend & Recieve Date'),
                'value' => function ($model) {

                    if ($model->toe_signed_and_received_date<>null) {
                        return '<span class="badge grid-badge badge-secondary my-3"> '.date('Y-m-d', strtotime($model->toe_signed_and_received_date)).'</span>';
                    }
                },
            ],


            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'on_hold_date',
                'label' => Yii::t('app', 'On-H Date'),
                'value' => function ($model) {

                    if ($model->on_hold_date<>null) {
                        return '<span class="badge grid-badge badge-primary my-3"> '.date('Y-m-d', strtotime($model->on_hold_date)).'</span>';
                    }
                },
            ],





            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
                            <div class="btn-group flex-wrap">
                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            </button>
                            <div class="dropdown-menu" role="menu">
                            '.$actionBtns.'
                            </div>
                            </div>',
                'buttons' => [
                    /*  'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },*/
                    'step_0' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Update'), $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'revise' => function ($url, $model) {
                        return Html::a('<i class="far fa-copy"></i> ' . Yii::t('app', 'Copy Quotation'), $url, [
                            'title' => Yii::t('app', 'Copy Quotation'),
                            'class' => 'dropdown-item text-1 sav-btn1',
                            'data-url' => $url,
                            'data-text' => 'Quotation will be Copied.',

                        ]);
                    },
                    /*  'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },*/
                    /* 'status' => function ($url, $model) {
                        if($model['status']==1){
                            return Html::a('<i class="fas fa-eye-slash"></i> '.Yii::t('app', 'Disable'), $url, [
                                'title' => Yii::t('app', 'Disable'),
                                'class'=>'dropdown-item text-1',
                                'data-confirm'=>Yii::t('app','Are you sure you want to disable this item?'),
                                'data-method'=>"post",
                            ]);
                        }else{
                            return Html::a('<i class="fas fa-eye"></i> '.Yii::t('app', 'Enable'), $url, [
                                'title' => Yii::t('app', 'Enable'),
                                'class'=>'dropdown-item text-1',
                                'data-confirm'=>Yii::t('app','Are you sure you want to enable this item?'),
                                'data-method'=>"post",
                            ]);
                        }
                    },*/
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), "javascript:;", [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1 delete-btn',
                            'data-url' => $url,
                            // 'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>
</div>
