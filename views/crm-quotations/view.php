<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CrmQuotations */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Crm Quotations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="crm-quotations-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'reference_number',
            'client_name',
            'client_type',
            'client_customer_name',
            'other_intended_users',
            'scope_of_service',
            'inquiry_date',
            'expiry_date',
            'purpose_of_valuation',
            'advance_payment_terms',
            'no_of_properties',
            'related_to_buyer',
            'related_to_owner',
            'related_to_client',
            'related_to_property',
            'related_to_buyer_reason:ntext',
            'related_to_owner_reason:ntext',
            'related_to_client_reason:ntext',
            'related_to_property_reason:ntext',
            'name',
            'phone_number',
            'email:email',
            'recommended_fee',
            'relative_discount',
            'final_fee_approved',
            'turn_around_time',
            'valuer_name',
            'date',
            'quotation_status',
            'payment_slip',
            'toe_document',
            'quotation_recommended_fee',
            'quotation_turn_around_time',
            'toe_final_fee',
            'toe_final_turned_around_time',
            'status',
            'assumptions:ntext',
            'created_at',
            'updated_at',
            'deleted_at',
            'created_by',
            'updated_by',
            'deleted_by',
        ],
    ]) ?>

</div>
