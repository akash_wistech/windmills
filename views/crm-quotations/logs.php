<?php

// echo "<pre>"; print_r($logs); echo "</pre>";  die();
use Carbon\Carbon;
?>
<div class="col-md-12">
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">
				<i class="fas fa-bullhorn"></i>
				Status History
			</h3>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-12">
					<div class="timeline">

						<?php
						$i=1;
						foreach ($logs as $key => $log) {
							if ($i==1) {$class = 'bg-info';}
							if ($i==2) {$class = 'bg-primary';}
							if ($i==3) {$class = 'bg-warning';}
							if ($i==4) {$class = 'bg-success';}
							if ($i==5) {$class = 'bg-dark';}
							if ($i==6) {$class = 'bg-danger';}
							if ($i==7) {$class = 'bg-light';}
							if ($i==8) {$class = 'bg-secondary';}

							$datetime1 = new \DateTime($log['date']);
							$datetime2 = new \DateTime(date('Y-m-d h:i:s'));
							$difference = $datetime1->diff($datetime2);

							$timeAgo = Yii::$app->crmQuotationHelperFunctions->formatOutput($difference);
							// echo $timeAgo; die();
							?>
							<div>
								<i class="fas fa-history <?= $class ?>"></i>
								<div class="timeline-item">
									<span class="time"><i class="fas fa-clock"></i> <?= $timeAgo ?> </span>
									<h3 class="timeline-header no-border"> <span class="badge grid-badge badge-light" style="background-color: #E0E0E0;"><?= Yii::$app->crmQuotationHelperFunctions->getQuotationStatusListArr()[$log['status_id']]; ?></span> 
										at 
										<span class="badge grid-badge badge-light" style="background-color: #E0E0E0;"><?= date('d-F-Y', strtotime($log['date'])); ?></span> 
										on
										<span class="badge grid-badge badge-light" style="background-color: #E0E0E0;"><?= date('h:i:s', strtotime($log['date'])); ?></span> 
									</h3>
								</div>
							</div>
							<?php
							$i++;
							if ($i==9) {
								$i=1;
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
