<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\ValuationReportsSearch;



$this->title = Yii::t('app', 'Valuation Status Lists');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$monthsArrForLoop =  Yii:: $app->appHelperFunctions->getMonthsArrForLoopArr();

$model = new \app\models\Valuation();

if($data['valuation_month']<>null){
    $model->valuation_month = $data['valuation_month'];
}else{
    $data['valuation_month'] = 'monthly';
    $model->valuation_month = $data['valuation_month'];
}



if($data['valuation_year']<>null){
    $model->valuation_year = $data['valuation_year'];
}else{
    $data['valuation_year'] = date("Y");
    $model->valuation_year = $data['valuation_year'];
}
if($data['valuation_year']=='all-time'){
    $model->valuation_month = null;
    $data['valuation_month'] = null;
}

if($data['valuation_month'] == 'yearly'){
    $model->valuation_year = null;
}


?>

<div class="card">
    <div class="card-body">
        <?php $form = ActiveForm::begin([
    'action' => ['valuation-status-all'],
    'method' => 'get',
]); ?>
        <div class="row">
            <div class="col-4">
                <?php
echo $form->field($model, 'valuation_month')->widget(Select2::classname(), [
    'data' => Yii:: $app->appHelperFunctions->getMQYArr(),
    'options' => ['placeholder' => 'Select Month ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
    ])->label(false);
    ?>
            </div>
            <div class="col-4">
                <?php
    echo $form->field($model, 'valuation_year')->widget(Select2::classname(), [
        'data' => Yii:: $app->appHelperFunctions->getYearArr(),
        'options' => ['placeholder' => 'Select Year ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
        ])->label(false);
        ?>
            </div>
            <div class="col-2">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary ']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
        if($data['valuation_month'] == 'monthly'){
            echo $this->render('../valuation-status/by-month', [
                'data' => $data,
                'monthsArrForLoop' => $monthsArrForLoop,
            ]);
        }
        else if($data['valuation_month'] == 'yearly'){
            echo $this->render('../valuation-status/by-year', [
                'data' => $data,
                'monthsArrForLoop' => $monthsArrForLoop,
                'yearsArrHelper' => Yii:: $app->appHelperFunctions->getYearArr(),
            ]);
        }
        else if($data['valuation_month']=='quarterly'){
            echo $this->render('../valuation-status/by-quarter', [
                'data' => $data,
                'monthsArrForLoop' => $monthsArrForLoop,
            ]);
        }
        