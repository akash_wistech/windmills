<?php 
use app\models\Valuation;
use app\models\ReceivedDocsFiles;
use app\models\ScheduleInspection;
use yii\db\Expression;





// $date_array = Yii::$app->appHelperFunctions->getThisQuarterNew($data['valuation_month'], $data['valuation_year']);
// $date_from = date('Y-m-d', $date_array['start_date']);
// $date_to = date('Y-m-d', $date_array['end_date']);
?>



<div class="row">
    <div class="col-md-12">
        <div class="card  card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title"><b>Quarterly - (<?= $data['valuation_year'] ?>)</b></h3>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-sm table-hover text-nowrap table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="text-primary">TVR (%)</th>
                            <th class="text-primary">Inspection Done (%)</th>
                            <th class="text-primary">Recieved Document Date (%)</th>
                            <th class="text-primary">Approval Date (%)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $total_val_recieved = $per_total_val_recieved = 0;
                        $total_inspect_requested = $per_total_inspect_requested = 0;
                        $total_doc_requested = $per_total_doc_requested = 0;
                        $total_approved = $per_total_approved = 0;

                            for ($i=1; $i <=4 ; $i++) {
                                if($i==1){$num='1st'; $data['valuation_month'] = '1-quarter';}
                                if($i==2){$num='2nd'; $data['valuation_month'] = '2-quarter';}
                                if($i==3){$num='3rd'; $data['valuation_month'] = '3-quarter';}
                                if($i==4){$num='4th'; $data['valuation_month'] = '4-quarter';}
                                
                                
                                $result = Yii:: $app->appHelperFunctions->getQuarterlyResultArr($data);

                                
                                $total_val_recieved += $result['total_val_recieved'];
                                $per_total_val_recieved += round($result['per_total_val_recieved'], 2);

                                $total_inspect_requested += $result['total_inspect_requested'];
                                $per_total_inspect_requested += round($result['per_total_inspect_requested'], 2);

                                $total_doc_requested += $result['total_doc_requested'];
                                $per_total_doc_requested += round($result['per_total_doc_requested'], 2);

                                $total_approved += $result['total_approved'];
                                $per_total_approved += round($result['per_total_approved'], 2);
                            ?>
                        <tr>
                            <td class="text-primary"><strong><?= $num ?> Quarter</strong></td>
                            <td class="text-left">
                                <span class="badge bg-light mx-2"><?=$result['total_val_recieved'] ?></span>
                                <span class="badge bg-light"> (<?= round($result['per_total_val_recieved'], 2) ?>
                                    %)</span>
                            </td>
                            <td class="text-left">
                                <span class="badge bg-light mx-2"><?=$result['total_inspect_requested'] ?></span>
                                <span class="badge bg-light">(<?= round($result['per_total_inspect_requested'], 2) ?>
                                    %)</span>
                            </td>
                            <td class="text-left">
                                <span class="badge bg-light mx-2"><?=$result['total_doc_requested'] ?></span>
                                <span class="badge bg-light">(<?= round($result['per_total_doc_requested'], 2) ?>
                                    %)</span>
                            </td>
                            <td class="text-left">
                                <span class="badge bg-light mx-2"><?=$result['total_approved'] ?></span>
                                <span class="badge bg-light">(<?= round($result['per_total_approved'], 2) ?> %)</span>
                            </td>
                        </tr>
                        <?php  
                        }
                        ?>
                        
                        <tr>
                            <td></td><td></td><td></td><td></td><td></td>
                        </tr>
                        <tr class="bg-info">
                            <td class="text-light"><strong>Total</strong>:</td>
                            <td>
                                <span class="badge bg-light mx-2"><?= $total_val_recieved ?></span>
                                <span class="badge bg-light">(<?= round($per_total_val_recieved);  ?>%)</span>
                            </td>
                            <td>
                                <span class="badge bg-light mx-2"><?= $total_inspect_requested ?></span>
                                <span class="badge bg-light">(<?= round($per_total_inspect_requested);  ?>%)</span>
                            </td>
                            <td>
                                <span class="badge bg-light mx-2"><?= $total_doc_requested ?></span>
                                <span class="badge bg-light">(<?= round($per_total_doc_requested);  ?>%)</span>
                            </td>
                            <td>
                                <span class="badge bg-light mx-2"><?= $total_approved ?></span>
                                <span class="badge bg-light">(<?= round(100);  ?>%)</span>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>