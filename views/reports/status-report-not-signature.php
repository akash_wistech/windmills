<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Valuations');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;
if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) {
    $actionBtns .= '{step_1}';
}

?>


<div class="valuation-index mx-5">
  <style media="screen">
    .selectpicker {display: block; width: 800px}
  </style>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <?php CustomPjax::begin(['id' => 'grid-container']);
 ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => 'This is Grid View Without Signature. ',
        'createBtn' => $createBtn,
        'columns' => [
         //   ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['format' => 'raw','attribute' => 'reference_number', 'label' => Yii::t('app', 'Reference'),
                'value' => function ($model) {
                    return Html::a($model->reference_number, Url::to(['valuation/step_1','id'=>$model->id]), [
                        'title' => Yii::t('app', 'Reference'),
                        'target'=>"_blank",
                        // 'class' => 'dropdown-item text-1',
                        'data-pjax' => "0",
                    ]);
                },
            ],

            ['attribute' => 'client_id',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                    return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title;
                },
                'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Building'),
                'value' => function ($model) {
                    return $model->building->title;
                },
                'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Community'),
                'value' => function ($model) {
                    return $model->building->communities->title;
                },
                // 'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                //     'title' => SORT_ASC,
                // ])->all(), 'id', 'community')
            ],
           // ['attribute' => 'client_reference', 'label' => Yii::t('app', 'Client Reference')],
            // ['attribute' => 'signature_img', 'label' => Yii::t('app', 'Signature Image')],
           /*  ['attribute' => 'signature_img',
                'label' => Yii::t('app', 'Signature Image'),
                'value' => function ($model) {
                    return $model->signature_img;
                },
                 'filter' => ArrayHelper::map(\app\models\Valuation::find()->where(['signature_img'=>''])->orderBy([
                    'signature_img' => SORT_ASC,
                ])->all(), 'id', 'signature_img')
             ],*/
            [
                'attribute' => 'city',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];
                },
                'filter' => Yii::$app->appHelperFunctions->emiratedListArr
            ],
            ['attribute' => 'instruction_date',
                'label' => Yii::t('app', 'Instruction Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->instruction_date));
                },
            ],
            ['attribute' => 'id',
                'label' => Yii::t('app', 'Valuation Report Date'),
                'value' => function ($model) {
                    // return date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
                    if($model->scheduleInspection->valuation_report_date <> null) {
                        return date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
                    }else{

                    }
                },
            ],
            ['attribute' => 'id',
                'label' => Yii::t('app', 'Fee'),
                'value' => function ($model) {
                    /*if (Yii::$app->appHelperFunctions->getClientRevenue($model->id)!=null) {
                      $total+=(int)Yii::$app->appHelperFunctions->getClientRevenue($model->id);
                    }*/
                    return Yii::$app->appHelperFunctions->getClientRevenue($model->id);
                },
                // 'footer' => '<b>Fee Total</b> '.\app\models\Valuation::getTotal($dataProvider->models, 'id'),
            ],

            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
                },
                'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
                    'firstname' => SORT_ASC,
                ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];})
            ],
            [
                'attribute' => 'id',
                'label' => Yii::t('app', 'Market Value'),
                'value' => function ($model) {
                    return (isset($model->approverData) && ($model->approverData <> null))? (number_format($model->approverData->estimated_market_value)): '';
                },
            ],
            ['format' => 'raw','attribute' => 'valuation_status',
                'label' => Yii::t('app', 'Valuation Status'),
                'value' => function ($model) {
                    $value=Yii::$app->helperFunctions->valuationStatusListArrLabel[$model['valuation_status']];
                    return Html::a($value, Url::to(['valuation/step_1','id'=>$model->id]), [
                        'title' => $value,
                    ]);
                },
                'contentOptions' => ['class' => ' pt-3 pl-2'],
                'filter' => Yii::$app->helperFunctions->valuationStatusListArr
            ],


        ],
    ]); ?>
    <?php CustomPjax::end(); ?>

</div>




