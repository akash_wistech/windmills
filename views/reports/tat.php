<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', $type);
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
$actionBtns = '';
$createBtn = false;
$import = false;
?>




<div class="valuation-search">

  <?php
      $form = ActiveForm::begin([
          'action' => ['tat-reports'],
          'method' => 'get',
      ]);
  ?>
    <div class="row">
        <div class="col-3"></div>
        <div class="col-sm-6">
            <?php
            echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                'data' => Yii:: $app->appHelperFunctions->reportPeriodNew,
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(false);
            ?>
        </div>
    </div>
    <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
        <div class="col-sm-4"></div>
        <div class="col-sm-4" id="end_date_id">

            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
        </div>
        <div class="col-sm-4"></div>
    </div>

    <div class="row">
    <div class="col-sm-3">
        <?php
            echo $form->field($searchModel, 'property_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title'),
                'options' => ['placeholder' => 'Select a Property Type ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'tenure')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                'options' => ['placeholder' => 'Select a Tenure Type ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'client_type')->widget(Select2::classname(), [
                'data' => yii::$app->quotationHelperFunctions->clienttype,
                'options' => ['placeholder' => 'Select a Client Type ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?php
                echo $form->field($searchModel, 'valuation_approach')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->valuationApproachListArr,
                    'options' => ['placeholder' => 'Select a Valuation Approach ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
            ?>
        </div>
    </div>



    <div class="text-center">
        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>


<div class="valuation-index col-12">

    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>

    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
     //   'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'import' => $import,
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'reference_number',
                'label' => Yii::t('app', 'Reference Number'),
                'value' => function ($model) {
                   return $model->reference_number ;
                },
                'footer' => '<b>Total:</b> <br><br> <b>Average:</b>',
            ],

            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Instruction Date - Submission date'),
                'value' => function ($model)  use ($sub_inst,$total_rows) {
                    
                    $startDate = $model->instruction_date.' '.date('H:i:s', strtotime($model->created_at));
                    $endDate   = $model->submission_approver_date;
                    
                    // print_r( abs( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate ), 2) ) ); echo "<br>"; 
                    // die;
                    
                    return abs( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate ), 2) );
                
                },
                'footer' => number_format($sub_inst,2) .'<br><br>'. number_format(($sub_inst/$total_rows),2),
                // 'footer' => number_format($sub_inst),
            ],


            ['attribute' => 'created_at',
                'label' => Yii::t('app', 'Instruction Date - Inspection Date'),
                'value' => function ($model)  use ($insp_sub,$total_rows) {

                    $inspection_time = $model->inspection_time.":00";
                    if($model->inspection_time==null){
                        $inspection_time = '00:00:00';
                    }

                    $startDate = $model->instruction_date.' '.date('H:i:s', strtotime($model->created_at));
                    $endDate   = $model->inspection_date.' '.$inspection_time;
                    // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                    return abs( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate ), 2) );

                },
                'footer' => number_format($insp_sub,2) .'<br><br>'. number_format(($insp_sub/$total_rows),2),
            ],
           /* ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection Date - Instruction Date'),
                'value' => function ($model)  use ($insp_sub,$total_rows) {

                    $diff = strtotime($model->inspection_date) - strtotime($model->instruction_date);
                    return abs(round($diff / 86400));
                },
                'footer' => number_format($insp_sub) .'<br><br>'. number_format(($insp_sub/$total_rows),2),
            ],*/


           /* ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Submission date - Instruction Date'),
                'value' => function ($model)  use ($sub_inst,$total_rows) {
                    $submission_date = date('Y-m-d', strtotime($model->approverData->created_at));;
                    $diff = strtotime($submission_date) - strtotime($model->instruction_date);
                    return abs(round($diff / 86400));
                },
                'footer' => number_format($sub_inst) .'<br><br>'. number_format(($sub_inst/$total_rows),2),
               // 'footer' => number_format($sub_inst),
            ],*/

            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection Date - Submission Date'),
                'value' => function ($model) use ($sub_insp,$total_rows){

                    $inspection_time = $model->inspection_time.":00";
                    if($model->inspection_time==null){
                        $inspection_time = '00:00:00';
                    }
                    
                    $startDate = $model->inspection_date.' '.$inspection_time;
                    $endDate   = $model->submission_approver_date;
                    // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                    return  abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) );
                },
                'footer' => number_format($sub_insp,2) .'<br><br>'. number_format(($sub_insp/$total_rows),2),
                //'footer' => number_format($sub_insp),
            ],
           /* ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Submission Date - Inspection Date'),
                'value' => function ($model) use ($sub_insp,$total_rows){
                    $submission_date = date('Y-m-d', strtotime($model->approverData->created_at));;
                    $diff = strtotime($submission_date) - strtotime($model->inspection_date);
                    return abs(round($diff / 86400));
                },
                'footer' => number_format($sub_insp) .'<br><br>'. number_format(($sub_insp/$total_rows),2),
                //'footer' => number_format($sub_insp),
            ],*/
           /* ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Approver Date - Instruction Date'),
                'value' => function ($model)  use ($app_insp) {
                    $submission_date = date('Y-m-d', strtotime($model->approverData->created_at));;
                    $diff = strtotime($submission_date) - strtotime($model->instruction_date);
                    return abs(round($diff / 86400));
                },
                'footer' => number_format($app_insp),
            ],*/
            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
                },
            ],
            [
                'attribute' => 'inspection_officer_name',
                'label' => Yii::t('app', 'Inspection Officer'),
                'value' => function ($model) {
                //    dd( $model->inspection_officer_name );
                    return (isset($model->inspection_officer_name) && ($model->inspection_officer_name <> null))? (Yii::$app->appHelperFunctions->staffMemberListArr[$model->inspection_officer_name]): '';
                },
            ],
            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title;
                }
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Community'),
                'value' => function ($model) {
                    return $model->building->communities->title;
                },
            ],
            ['attribute' => 'tenure',
                'label' => Yii::t('app', 'Tenure'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure];

                }
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];

                }
            ],
            [
                'attribute' => 'approver',
                'label' => Yii::t('app', 'Approver'),
                'value' => function ($model) {
                    return $model->approverData->user->firstname ;
                    //return $model->valuationParent->approverData->user->id.' '.$model->valuationParent->approverData->user->lastname;
                },

            ],
        ],
    ]); ?>

    <?php CustomPjax::end(); ?>
</div>
<?php
$this->registerJs('
$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});
 $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
');
?>