<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);

$this->title = Yii::t('app', 'Client Revenue - '.$type);
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;



$year        = $searchModel->year;
$time_period = $searchModel->time_period;
if($searchModel->time_period==4){
    $searchModel->year = null;
}

    
?>




<div class="valuation-search">

<?php
    if($type == 'Banks') {
        $form = ActiveForm::begin(['action' => ['client-revenue-only'], 'method' => 'get',]);
    }else if($type == 'Corporate'){
        $form = ActiveForm::begin(['action' => ['client-revenue-corporate'], 'method' => 'get',]);
    }else if($type == 'Individual'){
        $form = ActiveForm::begin(['action' => ['client-revenue-individual'], 'method' => 'get',]);
    }

?>

    <div class="row justify-content-center">
        <div class="col-sm-4 ">
            <?php
            echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                'data' => Yii:: $app->revenueHelper->getReportPeriodRev(),
                'options' => ['placeholder' => 'Select Year ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                ])->label(false);
                ?>
        </div>
        <div class="col-sm-4 ">
            <?php
            echo $form->field($searchModel, 'year')->widget(Select2::classname(), [
                'data' => Yii:: $app->appHelperFunctions->getYearArr(),
                'options' => ['placeholder' => 'Select Year ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                ])->label(false);
                ?>
        </div>
    </div>

    <div class="text-center">
        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>



<?php 
    $columns[] = ['attribute' => 'client_id',
    'label' => Yii::t('app', 'Client'),
    'value' => function ($model) {
        return $model->client->title;
    },
    'footer' => '<br><b>Total</b> <br> <b>Average</b> <br> <b>Percentage (%)</b>',
    'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
        'title' => SORT_ASC,
        ])->all(), 'id', 'title')
    ];

    if($time_period == 1){
        for ($month=1; $month <=12; $month++) { 
            $dateObj   = DateTime::createFromFormat('!m', $month);
            $monthName = $dateObj->format('F');
            $columns[] = ['format'=>'raw','attribute'=>$monthName,'value'=>function($model) use($month, $year, $time_period){
                $query = Yii::$app->revenueHelper->getClientValuations($model->client_id, $month, $year, $time_period, '', 'client_revenue');

                if($query->client_revenue>0){
                    return number_format($query->client_revenue, 2);
                }else{
                    return 0;
                }
            },
            'footer' => Yii::$app->revenueHelper->getFooterValues($revenue[$month],  $total_revenue, $totalNoOfClients),
        ];
        }
    }




    



    if($time_period == 2){
        for ($quarter=1; $quarter <=4 ; $quarter++) { 
            $qtr = $quarter."-quarter";
            $date_array = Yii::$app->appHelperFunctions->getThisQuarterNew($qtr, $year);
            $date['date_from'] = date('Y-m-d', $date_array['start_date']);
            $date['date_to']   = date('Y-m-d', $date_array['end_date']);
            
            $columns[] = ['format'=>'raw','attribute'=>$qtr,'value'=>function($model) use($month, $year, $time_period, $date){
                $query = Yii::$app->revenueHelper->getClientValuations($model->client_id, $month, $year, $time_period, $date, 'client_revenue');
   
                if($query->client_revenue>0){
                    return number_format($query->client_revenue,2);
                }else{
                    return 0;
                }
            },
            'footer' => Yii::$app->revenueHelper->getFooterValues($revenue[$quarter], $total_revenue ,$totalNoOfClients),
            ];   
        }
    }










    if($time_period == 3){
        
        for ($yealry=1; $yealry <=2 ; $yealry++) { 
            $qtr = $yealry."-Half";
            $date_array = Yii::$app->appHelperFunctions->getThisQuarterNew($qtr, $year);
            $date['date_from'] = date('Y-m-d', $date_array['start_date']);
            $date['date_to']   = date('Y-m-d', $date_array['end_date']);

            $columns[] = ['format'=>'raw','attribute'=>$qtr,'value'=>function($model) use($month, $year, $time_period, $date){
                $client_id = $model->client_id;
                $query = Yii::$app->revenueHelper->getClientValuations($client_id, $month, $year, $time_period, $date, 'client_revenue');
                if($query->client_revenue>0){
                    number_format($query->client_revenue, 2);
                }else{
                    return 0;
                }
            },
            'footer' => Yii::$app->revenueHelper->getFooterValues($revenue[$yealry], $total_revenue, $totalNoOfClients),
        ];   
        }
    }


    if($time_period == 4){
        foreach(Yii:: $app->appHelperFunctions->getYearArr() as $key=>$year_num){
            $columns[] = ['format'=>'raw','attribute'=>$year_num,'value'=>function($model) use($year_num, $time_period){
                // dd($model);
                $client_id = $model->client_id;
                $query = Yii::$app->revenueHelper->getClientValuations($client_id, $month=null, $year_num, $time_period, '', 'client_revenue');
                if($query->client_revenue>0){
                    return number_format($query->client_revenue, 2);
                }else{
                    return 0;
                }
            },
            'footer' => Yii::$app->revenueHelper->getFooterValues($revenue[$year_num], $total_revenue, $totalNoOfClients),
        ];
        }
    }
    
    $columns[] = ['attribute' => 'client_revenue', 'label' => Yii::t('app', 'Fee'),'value' => function ($model) use($year, &$fee_total, $time_period) {
        $query = Yii::$app->revenueHelper->getClientRevenueBank( $model->client_id, $month=null, $year, $time_period);
        if($query->client_revenue>0){
                return number_format($query->client_revenue, 2);
            }else{
                return 0;
            }
        },
        'footer' => Yii::$app->revenueHelper->getLastFooterValue($total_revenue, $totalNoOfClients),
    ];




    $columns[] = ['attribute' => 'id',
    'label' => Yii::t('app', 'Percentage'),
    'value' => function ($model) use ($year, $time_period, $total_revenue) {
        $query = Yii::$app->revenueHelper->getClientRevenueBank( $model->client_id, $month=null, $year, $time_period);
        if ($total_revenue>0){
            return number_format( ($query->client_revenue/$total_revenue) * 100,2). ' %';
        }
    },
    'footer' => '<br>100 %',

    ];





 
 
    // $columns[] = ['attribute' => 'client_revenue',
    // 'label' => Yii::t('app', 'Fee'),
    // 'value' => function ($model) use ($allvalues) {
    //     return number_format($model->client_revenue);
    // },
    // 'footer' => number_format($allvalues),
    // ];

// $columns[] = ['attribute' => 'id',
// 'label' => Yii::t('app', 'Percentage'),
// 'value' => function ($model) use ($allvalues) {
//     // dd($model->client_revenue);
//     return number_format( ($model->client_revenue/$allvalues) * 100,2). ' %';
// },
// 'footer' => '100 %',

// ];


?>


<div class="valuation-index col-12">
    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
    <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    // 'filterModel' => $searchModel,
    'cardTitle' => $cardTitle,
    // 'createBtn' => $createBtn,
    'showFooter' => true,
    'columns' => $columns,
    
    
]);
?>
    <?php CustomPjax::end(); ?>


</div>

<?php



$this->registerJs('

');
?>

<script>


</script>