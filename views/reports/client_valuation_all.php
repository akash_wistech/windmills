<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', 'Client Valuation - '.$type);
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
$time_period = $searchModel->time_period;

// $columns[] = ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']];
$columns[] = ['attribute' => 'client_type',
    'label' => Yii::t('app', 'Client Type'),
    'value' => function ($model) {
        return ucfirst($model->client_type);
    },
    'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
        'title' => SORT_ASC,
        ])->all(), 'id', 'title')
    ];

for ($i=1; $i <=12; $i++) { 
    $dateObj   = DateTime::createFromFormat('!m', $i);
    $monthName = $dateObj->format('F');
    $columns[] = ['format'=>'raw','attribute'=>$monthName,'value'=>function($model) use($i, $time_period){
        $revenue_fee = Yii::$app->appHelperFunctions->getMonthlyValuations($i, $time_period, $model->client_type);
        if($revenue_fee[0]['total_valuations']>0){
            return number_format($revenue_fee[0]['total_valuations']);
        }else{
            return 0;
        }
    }
];
}

$columns[] =     [
    'attribute' => 'total_valuations',
    'label' => Yii::t('app', 'Total Valuations'),
    'value' => function ($model) use ($allvalues) {
        return $model->total_valuations;
    },
    'footer' => number_format($allvalues),
    
];

$columns[] =     [
    'attribute' => 'id',
    'label' => Yii::t('app', 'Percentage'),
    'value' => function ($model) use ($allvalues) {
        // dd($allvalues);
        return number_format( ($model->total_valuations/$allvalues) * 100,2). ' %';
    },
    'footer' => '100 %',
    
];

?>




<div class="valuation-search">

<?php $form = ActiveForm::begin([
    'action' => ['client-valuation-all'],
    'method' => 'get',
]); ?>

<div class="row">
<div class="col-sm-3"></div>

<div class="col-sm-6 text-center">
<?php
echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
    'data' => Yii:: $app->appHelperFunctions->reportPeriod,
    'options' => ['placeholder' => 'Time Frame ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
    ])->label(false);
    ?>
    </div>
    </div>
    
    <div class="text-center">
    <div class="form-group">
    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>
    </div>
    
    <?php ActiveForm::end(); ?>
    
    </div>
    
    
    <div class="valuation-index col-12">
        
    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        // 'createBtn' => $createBtn,
        'showFooter' => true,
        'columns' => $columns,

        
    ]);
    ?>
    <?php CustomPjax::end(); ?>
    
    
    </div>
    
    <?php
    
    
    
    $this->registerJs('
    
    ');
    ?>
    
    <script>
    
    
    </script>