<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', $type);
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
$actionBtns = '';
$createBtn = false;
$import = false;
?>


<div class="valuation-index col-12">

    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>

    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
      //'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'import' => $import,
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'reference_number',
                'label' => Yii::t('app', 'Reference Number'),
                'format' => 'html',
                'value' => function ($model) {
                   return $model->reference_number ;
                },
                'footer' => '<br><br> <b>Average:</b>',
            ],
            ['attribute' => 'instruction_date',
                'label' => Yii::t('app', 'Instruction'),
                'format' => 'html',
                'value' => function ($model)  {
                    return  date('d-M-Y', strtotime($model->instruction_date)).'<br>  '.Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time);
                },
            ],
            ['attribute' => 'created_at',
                'label' => Yii::t('app', 'Valuation Created'),
                'format' => 'html',
                'value' => function ($model)  {


                    if($model->instruction_date != '' && $model->created_at !='') {

                         $startDate = $model->created_at;
                        $endDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));


                        $startDateTime = new DateTime($startDate); // Start date and time
                        $endDateTime = new DateTime($endDate);

                        $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                    }else{
                        $workingHours = 0;
                    }





                    return  date('d-M-Y', strtotime($model->created_at)).'<br>  '.date('h:i A',strtotime($model->created_at)).'<br><br><b>'.$workingHours.'</b> Hours';
                },
                'footer' => '<br><br>'. number_format(($diff_1/$total_rows),1).' Hours',
            ],
            ['attribute' => 'created_at',
                'label' => Yii::t('app', 'Inspection Created'),
                'format' => 'html',
                'value' => function ($model)  {
                    if($model->inspection_type != 3) {

                        if($model->scheduleInspection->created_at != '' && $model->created_at !='') {

                            $startDate = $model->created_at;
                            $endDate = $model->scheduleInspection->created_at;


                            $startDateTime = new DateTime($startDate); // Start date and time
                            $endDateTime = new DateTime($endDate);

                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                            if($model->reference_number == 'REV-2023-10737'){
                                $workingHours = $workingHours - 8.5;
                            }
                        }else{
                            $workingHours = 0;
                        }




                        return date('d-M-Y', strtotime($model->scheduleInspection->created_at)) . '<br>  ' . date('h:i A', strtotime($model->scheduleInspection->created_at)).'<br><br><b>'.$workingHours.'</b> Hours';

                    }else{
                        return '';
                    }
                },
                'footer' => '<br><br>'. number_format(($diff_2/$total_rows),1).' Hours',
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection Schedule'),
                'format' => 'html',
                'value' => function ($model)  {
                    if($model->inspection_type != 3) {
                        $inspection_time = $model->inspection_time . ":00";
                        if ($model->inspection_time == null) {
                            $inspection_time = '00:00:00';
                        }
                        if($model->scheduleInspection->created_at != '' && $model->scheduleInspection->inspection_date !='') {

                            $minus = 0;

                            $startDate = $model->scheduleInspection->created_at;
                            $endDate = $model->scheduleInspection->inspection_date.' '.$inspection_time;


                            $startDateTime = new DateTime($startDate); // Start date and time
                            $endDateTime = new DateTime($endDate);

                            $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                            if($model->reference_number == 'REV-2023-10772'){
                                $workingHours_1 = $workingHours_1 - 8.5;
                            }


                        }else{
                            $workingHours_1 = 0;
                        }



                        return date('d-M-Y', strtotime($model->scheduleInspection->inspection_date)) . '<br>  ' . Yii::$app->appHelperFunctions->formatTimeAmPm($model->scheduleInspection->inspection_time).'<br><br><b>'.$workingHours_1.'</b> Hours';
                    }else{
                        return '';
                    }
                },
                'footer' => '<br><br>'. number_format(($diff_3/$total_rows),1).' Hours',
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection Done'),
                'format' => 'html',
                'value' => function ($model)  {
                    if($model->inspection_type != 3) {

                        $inspection_time = $model->inspection_time . ":00";
                        if ($model->inspection_time == null) {
                            $inspection_time = '00:00:00';
                        }
                        if($model->inspection_done_date_time != '' && $model->scheduleInspection->inspection_date !='') {

                            $minus = 0;

                          //  $startDate = $model->scheduleInspection->created_at;
                            $startDate = $model->scheduleInspection->inspection_date.' '.$inspection_time;
                            $endDate = $model->inspection_done_date_time;


                            $startDateTime = new DateTime($startDate); // Start date and time
                            $endDateTime = new DateTime($endDate);

                            $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                           /* if($model->reference_number == 'REV-2023-10772'){
                                $workingHours = $workingHours - 8.5;
                            }*/


                        }else{
                            $workingHours_2 = 0;
                        }



                        return date('d-M-Y', strtotime($model->inspection_done_date_time)) . '<br>  ' . date('h:i A', strtotime($model->inspection_done_date_time)).'<br><br><b>'.$workingHours_2.'</b> Hours';
                    }else{
                        return '';
                    }
                },
                'footer' => '<br><br>'. number_format(($diff_4/$total_rows),1).' Hours',
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Recommended'),
                'format' => 'html',
                'value' => function ($model)  {

                    if($model->inspection_done_date_time != '' && $model->valuerData->created_at!='') {

                        $minus = 0;

                        $startDate = $model->inspection_done_date_time;
                        $endDate = $model->valuerData->created_at;


                        $startDateTime = new DateTime($startDate); // Start date and time
                        $endDateTime = new DateTime($endDate);

                        $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                         if($model->reference_number == 'REV-2023-10756'){
                             $workingHours_2 = $workingHours_2 - 8.5;
                         }


                    }else{
                        $workingHours_2 = 0;
                    }




                        return date('d-M-Y', strtotime($model->valuerData->created_at)) . '<br>  ' . date('h:i A', strtotime($model->valuerData->created_at)).'<br><br><b>'.$workingHours_2.'</b> Hours';

                    },
                'footer' => '<br><br>'. number_format(($diff_5/$total_rows),1).' Hours',
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Approval_TAT'),
                'format' => 'html',
                'value' => function ($model)  {

                    if($model->valuerData->created_at != '' && $model->valuerData->created_at!='') {

                        $minus = 0;



                        $startDate = $model->valuerData->created_at;
                        $endDate = $model->submission_approver_date;


                        $startDateTime = new DateTime($startDate); // Start date and time
                        $endDateTime = new DateTime($endDate);

                        $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                       /* if($model->reference_number == 'REV-2023-10756'){
                            $workingHours_2 = $workingHours_2 - 8.5;
                        }*/


                    }else{
                        $workingHours_2 = 0;
                    }


                    return  date('d-M-Y', strtotime($model->submission_approver_date)).'<br>  '.date('h:i A',strtotime($model->submission_approver_date)).'<br><br><b>'.$workingHours_2.'</b> Hours';

                },
                'footer' => '<br><br>'. number_format(($diff_6/$total_rows),1).' Hours',
            ],
            ['attribute' => 'created_at',
                'label' => Yii::t('app', 'Instruction - Inspection'),
                'format' => 'html',
                'value' => function ($model)  use ($insp_sub,$total_rows) {

                    if($model->inspection_type != 3) {
                        $inspection_time = $model->inspection_time . ":00";
                        if ($model->inspection_time == null) {
                            $inspection_time = '00:00:00';
                        }



                        if($model->instruction_date != '' && $model->inspection_date !='') {
                            $startDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                            $endDate = $model->inspection_date . ' ' . $inspection_time;


                            $startDateTime = new DateTime($startDate); // Start date and time
                            $endDateTime = new DateTime($endDate);

                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                            if($model->reference_number == 'REV-2023-10737' || $model->reference_number == 'REV-2023-10756'){
                                $workingHours = $workingHours - 8.5;
                            }
                        }else{
                            $workingHours = 0;
                        }

                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                     //   $test = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1)) . ' <br> ' . abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));
                        $test = abs(number_format(($workingHours/8.5),2)).'Days<br><br><b>'.abs($workingHours).'</b> Hours';
                        return $test;
                    }else{
                        return '';
                    }
                },
                'footer' => '<br><br>'. number_format(($diff_7/$total_rows),1).' Hours',
              //  'footer' => '<br><br>'. number_format(($insp_sub/$total_rows),1),
            ],

            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection - Submission'),
                'format' => 'html',
                'value' => function ($model) use ($sub_insp,$total_rows){
                    if($model->inspection_type != 3) {
                        $inspection_time = $model->inspection_time . ":00";
                        if ($model->inspection_time == null) {
                            $inspection_time = '00:00:00';
                        }

                        $startDate = $model->inspection_date . ' ' . $inspection_time;
                        $endDate = $model->submission_approver_date;

                        if($model->inspection_date !='' && $model->submission_approver_date !=''){
                            $startDateTime = new DateTime($startDate); // Start date and time
                            $endDateTime = new DateTime($endDate);
                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                           /* if($model->reference_number == 'REV-2023-10772' || $model->reference_number == 'REV-2023-10756'){
                                $workingHours = $workingHours - 8.5;
                            }*/
                        }else{
                            $workingHours=0;
                        }

                        $test = abs(number_format(($workingHours/8.5),2)).'Days<br><br><b>'.abs($workingHours).'</b> Hours';
                        return $test;

                        //return $workingHours;
                    }else{
                        return '';

                        }
                },
                'footer' => '<br><br><b>'. number_format(($diff_8/$total_rows),1).'</b> Hours',
               // 'footer' => '<br><br>'. number_format(($sub_insp/$total_rows),1),
                //'footer' => number_format($sub_insp),
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Total - TAT'),
                'format' => 'html',
                'value' => function ($model)  use ($sub_inst,$total_rows) {

                    if($model->inspection_type != 3) {

                        $inspection_time = $model->inspection_time . ":00";
                        if ($model->inspection_time == null) {
                            $inspection_time = '00:00:00';
                        }

                        if($model->instruction_date !='' && $model->inspection_date !='') {

                            $startDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                            $endDate = $model->inspection_date . ' ' . $inspection_time;

                            $startDateTime = new DateTime($startDate); // Start date and time
                            $endDateTime = new DateTime($endDate);
                            $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                        }else{
                            $workingHours_1 =0;
                        }
                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                      //  $time_1 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1));
                      //  $time_1_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));

                        if($model->inspection_date !='' && $model->submission_approver_date !='') {
                            $startDate_2 = $model->inspection_date . ' ' . $inspection_time;
                            $endDate_2 = $model->submission_approver_date;
                            $startDateTime = new DateTime($startDate_2); // Start date and time
                            $endDateTime = new DateTime($endDate_2);
                            /*echo $startDate_2.' '.$endDate_2;
                            die;*/
                            $workingHours_2 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                        }else{
                            $workingHours_2=0;
                        }
                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                      //  $time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                       // $time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));



                        // print_r( abs( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate ), 2) ) ); echo "<br>";
                        // die;
                        $workingHours = $workingHours_1 + $workingHours_2;
                        if($model->reference_number == 'REV-2023-10737' || $model->reference_number == 'REV-2023-10756'){
                            $workingHours = $workingHours - 8.5;
                        }
                        $test = abs(number_format(($workingHours/8.5),2)).'Days<br><br><b>'.abs($workingHours).'</b> Hours';
                        return $test;

                    }else{


                        $startDate_2 = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                        $endDate_2 = $model->submission_approver_date;
                        $startDateTime = new DateTime($startDate_2); // Start date and time
                        $endDateTime = new DateTime($endDate_2);
                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                        //$time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                        //$time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));
                        if($model->instruction_date !='' && $model->submission_approver_date !='') {
                            $startDateTime = new DateTime($startDate_2); // Start date and time
                            $endDateTime = new DateTime($endDate_2);
                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                            if($model->reference_number == 'REV-2023-10772' || $model->reference_number == 'REV-2023-10756'){
                                $workingHours = $workingHours - 8.5;
                            }
                            $test = abs(number_format(($workingHours/8.5),2)).'<br><br><b>'.abs($workingHours).'</b> Hours';
                            return $test;


                        }else{

                        }

                    }
                },
                'footer' => '<br><br><b>'. number_format(($diff_9/$total_rows),1).'</b> Hours',
              //  'footer' => '<br><br>'. number_format(($sub_inst/$total_rows),1),
                // 'footer' => number_format($sub_inst),
            ],

            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
                },
            ],
            [
                'attribute' => 'inspection_officer_name',
                'label' => Yii::t('app', 'Inspection Officer'),
                'value' => function ($model) {
                //    dd( $model->inspection_officer_name );
                    return (isset($model->inspection_officer_name) && ($model->inspection_officer_name <> null))? (Yii::$app->appHelperFunctions->staffMemberListArr[$model->inspection_officer_name]): '';
                },
            ],
            [
                'attribute' => 'approver',
                'label' => Yii::t('app', 'Approver'),
                'value' => function ($model) {
                    return $model->approverData->user->firstname ;
                    //return $model->valuationParent->approverData->user->id.' '.$model->valuationParent->approverData->user->lastname;
                },

            ],
        ],
    ]); ?>

    <?php CustomPjax::end(); ?>
</div>
<?php
$this->registerJs('
$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});
 $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
');
?>