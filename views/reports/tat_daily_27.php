<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', $type);
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
$actionBtns = '';
$createBtn = false;
$import = false;
?>


<div class="valuation-index col-12">

    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>

    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
      //'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'import' => $import,
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'reference_number',
                'label' => Yii::t('app', 'Reference Number'),
                'value' => function ($model) {
                   return $model->reference_number ;
                },
                'footer' => '<br><br> <b>Average:</b>',
            ],
            ['attribute' => 'instruction_date',
                'label' => Yii::t('app', 'Instruction Date'),
                'value' => function ($model)  {
                    return  date('d-M-Y', strtotime($model->instruction_date)).'  '.Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time);
                },
            ],
            ['attribute' => 'created_at',
                'label' => Yii::t('app', 'Valuation Created Date'),
                'value' => function ($model)  {
                    return  date('d-M-Y', strtotime($model->created_at)).'  '.date('h:i A',strtotime($model->created_at));
                },
            ],
            ['attribute' => 'created_at',
                'label' => Yii::t('app', 'Inspection Created Date'),
                'value' => function ($model)  {
                    if($model->inspection_type != 3) {
                        return date('d-M-Y', strtotime($model->scheduleInspection->created_at)) . '  ' . date('h:i A', strtotime($model->scheduleInspection->created_at));
                    }else{
                        return '';
                    }
                },
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection Schedule Date'),
                'value' => function ($model)  {
                    if($model->inspection_type != 3) {
                        return date('d-M-Y', strtotime($model->scheduleInspection->inspection_date)) . '  ' . Yii::$app->appHelperFunctions->formatTimeAmPm($model->scheduleInspection->inspection_time);
                    }else{
                        return '';
                    }
                },
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection Done Date'),
                'value' => function ($model)  {
                    if($model->inspection_type != 3) {
                        return date('d-M-Y', strtotime($model->inspection_done_date_time)) . '  ' . date('h:i A', strtotime($model->inspection_done_date_time));
                    }else{
                        return '';
                    }
                },
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Recommended Date'),
                'value' => function ($model)  {


                        return date('d-M-Y', strtotime($model->valuerData->created_at)) . '  ' . date('h:i A', strtotime($model->valuerData->created_at));

                    },
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Approval Date'),
                'value' => function ($model)  {

                    return  date('d-M-Y', strtotime($model->submission_approver_date)).'  '.date('h:i A',strtotime($model->submission_approver_date));

                },
            ],
            ['attribute' => 'created_at',
                'label' => Yii::t('app', 'Instruction Date - Inspection Date'),
                'format' => 'html',
                'value' => function ($model)  use ($insp_sub,$total_rows) {

                    if($model->inspection_type != 3) {
                        $inspection_time = $model->inspection_time . ":00";
                        if ($model->inspection_time == null) {
                            $inspection_time = '00:00:00';
                        }

                        $startDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                        $endDate = $model->inspection_date . ' ' . $inspection_time;

                        $startDateTime = new DateTime('2023-09-26 10:00:00'); // Start date and time
                        $endDateTime = new DateTime('2023-09-27 14:00:00');   // End date and time

                        $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDate, $endDate);



                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                        $test = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1)) . ' <br> ' . abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));
                        return $test;
                    }else{
                        return '';
                    }
                },
                'footer' => '<br><br>'. number_format(($insp_sub/$total_rows),1),
            ],

            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection Date - Submission Date'),
                'format' => 'html',
                'value' => function ($model) use ($sub_insp,$total_rows){
                    if($model->inspection_type != 3) {
                        $inspection_time = $model->inspection_time . ":00";
                        if ($model->inspection_time == null) {
                            $inspection_time = '00:00:00';
                        }

                        $startDate = $model->inspection_date . ' ' . $inspection_time;
                        $endDate = $model->submission_approver_date;
                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                        $insp_date = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1)) . '<br>' . abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));
                        return $insp_date;
                    }else{
                        return '';

                        }
                },
                'footer' => '<br><br>'. number_format(($sub_insp/$total_rows),1),
                //'footer' => number_format($sub_insp),
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Total - TAT'),
                'format' => 'html',
                'value' => function ($model)  use ($sub_inst,$total_rows) {

                    if($model->inspection_type != 3) {

                        $inspection_time = $model->inspection_time . ":00";
                        if ($model->inspection_time == null) {
                            $inspection_time = '00:00:00';
                        }

                        $startDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                        $endDate = $model->inspection_date . ' ' . $inspection_time;
                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                        $time_1 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1));
                        $time_1_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));


                        $startDate_2 = $model->inspection_date . ' ' . $inspection_time;
                        $endDate_2 = $model->submission_approver_date;
                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                        $time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                        $time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));



                        // print_r( abs( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate ), 2) ) ); echo "<br>";
                        // die;

                        return abs($time_1 + $time_2) . ' <br>' . abs($time_1_hours + $time_2_hours);
                    }else{


                        $startDate_2 = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                        $endDate_2 = $model->submission_approver_date;
                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                        $time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                        $time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));

                        return abs( $time_2) . ' <br>' . abs($time_2_hours);

                    }
                },
                'footer' => '<br><br>'. number_format(($sub_inst/$total_rows),1),
                // 'footer' => number_format($sub_inst),
            ],

            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
                },
            ],
            [
                'attribute' => 'inspection_officer_name',
                'label' => Yii::t('app', 'Inspection Officer'),
                'value' => function ($model) {
                //    dd( $model->inspection_officer_name );
                    return (isset($model->inspection_officer_name) && ($model->inspection_officer_name <> null))? (Yii::$app->appHelperFunctions->staffMemberListArr[$model->inspection_officer_name]): '';
                },
            ],
            [
                'attribute' => 'approver',
                'label' => Yii::t('app', 'Approver'),
                'value' => function ($model) {
                    return $model->approverData->user->firstname ;
                    //return $model->valuationParent->approverData->user->id.' '.$model->valuationParent->approverData->user->lastname;
                },

            ],
        ],
    ]); ?>

    <?php CustomPjax::end(); ?>
</div>
<?php
$this->registerJs('
$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});
 $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
');
?>