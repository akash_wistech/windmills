<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);

use yii\data\ArrayDataProvider;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', 'Client Revenue - '.$type);
$cardTitle = Yii::t('app', 'Revenue By '.$type);
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;

$this->registerJs('
$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});
');


$totalVal = 0;
$totalfee = 0;
$totalMarketVal = 0;


foreach ($dataProvider->allModels as $entry) {
    foreach ($entry as $value) {
        $parts = explode(",", $value);
        // Assuming index 1 is a numeric value, add it to the sum
        $totalVal += floatval($parts[1]);
        $totalfee += floatval($parts[2]);
        $totalMarketVal += floatval($parts[3]);
    }
}

if (!in_array(Yii::$app->user->identity->id, [1, 14, 33, 110465]))
{
    $totalfee = "";
}

?>


<div class="valuation-search">

    <?php
    if($type == 'Banks') {
        $form = ActiveForm::begin(['action' => ['client-revenue-banks'], 'method' => 'get',]);
    }else if($type == 'Corporate'){
        $form = ActiveForm::begin(['action' => ['client-revenue-corporate'], 'method' => 'get',]);
    }else if($type == 'Individual'){
        $form = ActiveForm::begin(['action' => ['client-revenue-individual'], 'method' => 'get',]);
    }

    ?>

    <div class="row">
        <div class="col-sm-3"></div>

        <div class="col-sm-6 text-center">
            <?php
            echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                'options' => ['placeholder' => 'Time Frame ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(false);
            ?>
        </div>
        <div class="col-sm-3"></div>

    </div>
    <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
        <div class="col-sm-4"></div>
        <div class="col-sm-4" id="end_date_id">

            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
        </div>
        <div class="col-sm-4"></div>
    </div>

    <div class="text-center">
        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>


<div class="valuation-index col-12">



    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'cardTitle' => $cardTitle,
        'showFooter' => true,
        'options' => [
            'id' => 'sortable-table',
        ],
        'columns' => [
            //  ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],

            [ 'attribute' => 'client',
                'label' => Yii::t('app', $type),
                'value' => function ($model) {
                    $result = '';

                    foreach ($model as $key => $name) {
                        $company =\app\models\Company::find()->where(['id' => $key])->one();
                        $clients =\app\models\Clients::find()->where(['id' => $company->main_client_id])->one();
                        $heading = '<b>'.$clients->title.'</b>';
                        break;
                    }

                    $result .= '<b>'.$heading.'</b><br>';
                    foreach ($model as $name) {
                        $parts = explode(",", $name);
                        $result .= ' - '.$parts[0] . '<br>';
                    }

                    return $result;
                },
                'format' => 'raw', // Enable HTML formatting
                'footer' => '<b>Total:</b> <br><br> <b>Average:</b>',
            ],

            [ 'attribute' => 'valuations',
                'label' => Yii::t('app', 'Total Valuations'),
                'value' => function ($model) {
                    $result = '';
                    $single_val = 0;
                    $result .= '<br>';
                    foreach ($model as $name) {
                        $parts = explode(",", $name);
                        $valueWithoutDecimal = intval($parts[1]);
                        $single_val += $valueWithoutDecimal;
                        $formattedValue = number_format($valueWithoutDecimal);
                        $result .= $formattedValue . '<br>';
                    }

                    return '<b>'.number_format($single_val).'</b>'.$result;

                },
                'format' => 'raw', // Enable HTML formatting
                'contentOptions' => ['style' => 'text-align: right;'],
                'headerOptions' => ['style' => 'color:#017BFE; text-align: right;' , 'class' => 'sortableVal'],
                'footerOptions' => ['style' => 'text-align: right;'],
                'footer' => '<b> '.number_format($totalVal).' </b><br><br> <b> '.number_format($totalVal/count($dataProvider->allModels)).' </b>',
            ],

            [ 'attribute' => 'fee',
                'label' => Yii::t('app', 'Total Fee'),
                'value' => function ($model) {
                    if (in_array(Yii::$app->user->identity->id, [1, 14, 33, 110465])) {
                        $result = '';
                        $single_fee = 0;
                        $result .= '<br>';
                        foreach ($model as $name) {
                            $parts = explode(",", $name);
                            $valueWithoutDecimal = intval($parts[2]); // Convert to integer to remove decimal values
                            $single_fee += $valueWithoutDecimal;
                            $formattedValue = number_format($valueWithoutDecimal); // Format as a number
                            $result .= $formattedValue . '<br>';
                        }
                        return '<b>'.number_format($single_fee).'</b>'.$result;
                    }else{
                        return "";
                    }
                },
                'format' => 'raw', // Enable HTML formatting
                'headerOptions' => ['style' => 'color:#017BFE; text-align: right;' , 'class' => 'sortableFee'],
                'contentOptions' => ['style' => 'text-align: right;'],
                'footerOptions' => ['style' => 'text-align: right;'],
                'footer' => '<b> '.number_format($totalfee).' </b><br><br><b> '.number_format($totalfee/count($dataProvider->allModels)).' </b>',
            ],

            [ 'attribute' => 'value',
                'label' => Yii::t('app', 'Market value'),
                'value' => function ($model) {
                    $result = '';
                    $single_market = 0;
                    $result .= '<br>';
                    foreach ($model as $name) {
                        $parts = explode(",", $name);
                        $valueWithoutDecimal = intval($parts[3]); // Convert to integer to remove decimal values
                        $single_market += $valueWithoutDecimal;
                        $formattedValue = number_format($valueWithoutDecimal); // Format as a number
                        $result .= $formattedValue . '<br>';
                    }
                    return '<b>'.number_format($single_market).'</b>'.$result;
                },
                'format' => 'raw', // Enable HTML formatting
                'headerOptions' => ['style' => 'color:#017BFE; text-align: right;' , 'class' => 'sortableMarket'],
                'contentOptions' => ['style' => 'text-align: right;'],
                'footerOptions' => ['style' => 'text-align: right;'],
                'footer' => '<b> '.number_format($totalMarketVal).' </b><br><br><b> '.number_format($totalMarketVal/count($dataProvider->allModels)).' </b>',
            ],

            ['attribute' => 'value',
                'label' => Yii::t('app', 'Percentage (%)'),
                'value' => function ($model) use ($totalfee) {
                    if (in_array(Yii::$app->user->identity->id, [1, 14, 33, 110465])) {
                        $result = '';
                        $result .= '<br>';
                        $single_percentage = 0;
                        foreach ($model as $name) {
                            $parts = explode(",", $name);
                            $valueWithoutDecimal = intval($parts[2]);
                            $formattedValue = number_format($valueWithoutDecimal);

                            // Calculate the percentage
                            $percentage = ($valueWithoutDecimal / $totalfee) * 100;
                            $formattedPercentage = number_format($percentage, 2);
                            $single_percentage += $formattedPercentage;
                            $result .= ' '. $formattedPercentage . '%<br>';
                        }

                        return '<b>'.$single_percentage.'%'.'</b>'.$result;
                    }else{
                        return "";
                    }
                },
                'format' => 'raw', // Enable HTML formatting
                'headerOptions' => ['style' => 'text-align: right;'],
                'contentOptions' => ['style' => 'text-align: right;'],
                'footerOptions' => ['style' => 'text-align: right;'],
                'footer' => '<b> 100% </b><br><br><b> '.number_format(100/count($dataProvider->allModels)).'% </b>',
            ],

        ],
    ]);
    ?>
    <?php CustomPjax::end(); ?>


</div>

<?php



$this->registerJs('
 $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });

    $(".sortableVal").click(function () {
        var index = $(this).index();

        var rows = $("#sortable-table tbody tr").get();

        rows.sort(function (a, b) {
            var aValue = $(a).find("td:eq(1) b").text().replace(/,/g, "");
            var bValue = $(b).find("td:eq(1) b").text().replace(/,/g, "");

            aValue = !isNaN(aValue) ? parseFloat(aValue) : aValue;
            bValue = !isNaN(bValue) ? parseFloat(bValue) : bValue;

            return bValue - aValue; // Sort numeric values
        });

        $.each(rows, function (index, row) {
            $("#sortable-table tbody").append(row);
        });

    });

    $(".sortableMarket").click(function () {
        var index = $(this).index();

        var rows = $("#sortable-table tbody tr").get();

        rows.sort(function (a, b) {
            var aValue = $(a).find("td:eq(1) b").text().replace(/,/g, "");
            var bValue = $(b).find("td:eq(1) b").text().replace(/,/g, "");

            aValue = !isNaN(aValue) ? parseFloat(aValue) : aValue;
            bValue = !isNaN(bValue) ? parseFloat(bValue) : bValue;

            return bValue - aValue; // Sort numeric values
        });

        $.each(rows, function (index, row) {
            $("#sortable-table tbody").append(row);
        });

    });

    $(".sortableFee").click(function () {
        var index = $(this).index();

        var rows = $("#sortable-table tbody tr").get();

        rows.sort(function (a, b) {
            var aValue = $(a).find("td:eq(1) b").text().replace(/,/g, "");
            var bValue = $(b).find("td:eq(1) b").text().replace(/,/g, "");

            aValue = !isNaN(aValue) ? parseFloat(aValue) : aValue;
            bValue = !isNaN(bValue) ? parseFloat(bValue) : bValue;

            return bValue - aValue; // Sort numeric values
        });

        $.each(rows, function (index, row) {
            $("#sortable-table tbody").append(row);
        });

    });

    $( document ).ready(function() {
        var index = $(this).index();

        var rows = $("#sortable-table tbody tr").get();

        rows.sort(function (a, b) {
            var aValue = $(a).find("td:eq(1) b").text().replace(/,/g, ""); // Remove commas
            var bValue = $(b).find("td:eq(1) b").text().replace(/,/g, "");
            
            aValue = !isNaN(aValue) ? parseFloat(aValue) : aValue;
            bValue = !isNaN(bValue) ? parseFloat(bValue) : bValue;

            return bValue - aValue; // Sort numeric values
        });

        $.each(rows, function (index, row) {
            $("#sortable-table tbody").append(row);
        });
    });

');


?>
