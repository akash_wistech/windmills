<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', 'Prices, Rent & Yield by Property Type');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;

?>




<div class="valuation-search">

  <?php $form = ActiveForm::begin([
      'action' => ['gross-yield-by-property-type'],
      'method' => 'get',
  ]); ?>

    <div class="row">


        <div class="col-sm-4 text-center">
            <?php
          echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                    'data' => Yii:: $app->appHelperFunctions->reportPeriodNew,
                   // 'options' => ['placeholder' => 'Time Frame ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Main');
                ?>
        </div>

        <div class="col-sm-4 text-center">
            <?php
             echo $form->field($searchModel, 'city_filter')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->emiratedListArr,
                    'options' => ['placeholder' => 'UAE'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('City');
                ?>
        </div>

        <div class="col-sm-4 text-center">
            <?php
               echo $form->field($searchModel, 'time_period_compare')->widget(Select2::classname(), [
                    'data' => Yii:: $app->appHelperFunctions->reportPeriodNew,
                    // 'options' => ['placeholder' => 'Time Frame ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Compare');
                ?>
        </div>
</div>

    <div class="row">
        <div class="col-sm-4">
            <div  id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
            </div>
        </div>
        <div class="col-sm-4">
            <?php
            echo $form->field($searchModel, 'community')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(\app\models\Communities::find()->where(['status'=>1])->andWhere(['trashed'=>0])->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title'),
                'options' => ['placeholder' => 'Select a Community ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-4" >

          <div id="date_range_array_compare" <?php if($custom_compare == 0){echo 'style="display:none;"';} ?>>
            <?= $form->field($searchModel, 'custom_date_btw_compare')->textInput(['class'=>'form-control div2'])->label('Select Custom Date Range Compare'); ?>
          </div>
        </div>
    </div>



    <div class="text-center">
        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php
foreach ($dataProvider as $item => $value){ ?>


    <div class="row">
        <div class="valuation-index col-6">
            <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
            <?= CustomGridView::widget([
                'dataProvider' => $dataProvider[$item],
                'cardTitle' => Yii::$app->appHelperFunctions->propertiesCategoriesList[$item],
                'showFooter' => true,
                'columns' => [
                    //  ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],

                    ['attribute' => 'title',
                        'label' => Yii::t('app', 'Property'),
                        'value' => function ($model) {
                            return $model->title;
                        },
                        'footer'=> '<b>Average:</b>'
                    ],
                    ['attribute' => 'id',
                        'label' => Yii::t('app', 'SP/psf'),
                        'value' => function ($model) use ($dataProvider_report,$dataProvider_report_avg) {

                            if (array_key_exists($model->id, $dataProvider_report))
                            {
                                return number_format($dataProvider_report[$model->id]->mv_psf,2);

                            }
                            else
                            {
                                return 0;
                            }
                        },
                        'footer' => $dataProvider_report_avg[$item]['mv_avg'],
                    ],
                    ['attribute' => 'id',
                        'label' => Yii::t('app', 'RP/psf'),
                        'value' => function ($model) use ($dataProvider_report,$dataProvider_report_avg) {
                            return number_format($dataProvider_report[$model->id]->rv_psf,2);
                        },
                        'footer' => $dataProvider_report_avg[$item]['rv_avg'],
                    ],
                    ['attribute' => 'id',
                        'label' => Yii::t('app', 'GY'),
                        'value' => function ($model) use ($dataProvider_report,$dataProvider_report_avg) {
                            if($dataProvider_report[$model->id]->rv_psf > 0 && $dataProvider_report[$model->id]->mv_psf > 0){
                                return  number_format((($dataProvider_report[$model->id]->rv_psf/$dataProvider_report[$model->id]->mv_psf)* 100),2);
                            }else{
                                return 0;
                            }
                        },
                        'footer' => $dataProvider_report_avg[$item]['gy_avg'],
                    ],
                ],

            ]); ?>
            <?php CustomPjax::end(); ?>

        </div>
        <div class="valuation-index col-6">
            <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
            <?= CustomGridView::widget([
                'dataProvider' => $dataProvider[$item],
                // 'filterModel' => $searchModel,
                'cardTitle' => Yii::$app->appHelperFunctions->propertiesCategoriesList[$item],
                // 'createBtn' => $createBtn,
                'showFooter' => true,
                'columns' => [
                    //  ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
                    ['attribute' => 'title',
                        'label' => Yii::t('app', 'Property'),
                        'value' => function ($model) {
                            return $model->title;
                        },
                        'footer' => '<b>Average:</b>',
                    ],
                    ['attribute' => 'id',
                        'label' => Yii::t('app', 'SP/psf'),
                        'value' => function ($model) use ($dataProvider_report_compare,$dataProvider_report_compare_avg) {

                            if (array_key_exists($model->id, $dataProvider_report_compare))
                            {
                                return number_format($dataProvider_report_compare[$model->id]->mv_psf,2);

                            }
                            else
                            {
                                return 0;
                            }
                        },
                        'footer' => $dataProvider_report_compare_avg[$item]['mv_avg'],
                    ],
                    ['attribute' => 'id',
                        'label' => Yii::t('app', 'RP/psf'),
                        'value' => function ($model) use ($dataProvider_report_compare,$dataProvider_report_compare_avg) {
                            return number_format($dataProvider_report_compare[$model->id]->rv_psf,2);
                        },
                        'footer' => $dataProvider_report_compare_avg[$item]['rv_avg'],
                    ],
                    ['attribute' => 'id',
                        'label' => Yii::t('app', 'GY'),
                        'value' => function ($model) use ($dataProvider_report_compare,$dataProvider_report_compare_avg) {
                            if($dataProvider_report_compare[$model->id]->rv_psf > 0 && $dataProvider_report_compare[$model->id]->mv_psf > 0){
                                return  number_format((($dataProvider_report_compare[$model->id]->rv_psf/$dataProvider_report_compare[$model->id]->mv_psf)* 100),2);
                            }else{
                                return 0;
                            }
                        },
                        'footer' => $dataProvider_report_compare_avg[$item]['gy_avg'],
                    ],


                ],

            ]);
            ?>
            <?php CustomPjax::end(); ?>


        </div>

    </div>


<?php
}

?>

<?php
$this->registerJs('
$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});

$(".div2").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div2").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});
 $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
    
     $(\'#valuationreportssearch-time_period_compare\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array_compare\').show();
        }else{
            $(\'#date_range_array_compare\').hide();
        }
    });
');
?>

