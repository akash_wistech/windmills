<?php

use app\models\Valuation;
use app\models\ReceivedDocsFiles;
use app\models\ScheduleInspection;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig\Sum;
use yii\db\Expression;

if($type == 'banks' || $type == '')
{
    $clientList =  Yii:: $app->appHelperFunctions->getAllFeeBankClients();
}elseif($type == 'corporate')
{
    $clientList =  Yii:: $app->appHelperFunctions->getAllFeeCorporateClients();
}

?>

<style>
    .blank_row
    {
        height: 20px !important; /* overwrites any other rules */
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="card  card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title"><b>Periodic Fee by Clients - Report </b></h3>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-sm table-hover text-nowrap table-striped">
                    <thead>
                        <tr>
                            <th class="text-primary"> Clients </th>

                            <?php foreach ($yearList as $year_key => $year) { ?>

                                <th class="text-primary text-right"> <?= $year ?> </th>

                            <?php } ?>

                           

                            <th class="text-primary text-right">Total Valuations</th>
                            <th class="text-primary text-right">Fee</th>
                            <th class="text-primary text-right">Fee (%)</th>
                            <th class="text-primary text-right">Market Values</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $data_array = array();
                        $static_value = 0;
                        $market_tot_price = 0;
                        $total_count = 0;
                        foreach ($clientList as $client_key => $client_name) {

                            if($clientList[$client_key]['nick_name'] == '' || $clientList[$client_key]['nick_name'] == 'NULL')
                            {
                                $data_array[$client_key][] = $clientList[$client_key]['title'];
                            }else{
                                $data_array[$client_key][] = $clientList[$client_key]['nick_name'];
                            }

                            foreach ($yearList as $year_key => $year) {

                                $result = Yii::$app->appHelperFunctions->getBankTotalYearly($clientList[$client_key]['id'] , $year);
                                $result_all = Yii::$app->appHelperFunctions->getBankTotalYearlyFee($clientList[$client_key]['id']);
                                $data_array[$client_key][] = $result['total_fee'];
                            }
                            $data_array[$client_key][] = $result_all['total_valuations'];
                            $data_array[$client_key][] = $result_all['total_fee'];
                            $data_array[$client_key][] = $result_all['market_price'];
                            $static_value += $result_all['total_fee'];
                            $market_tot_price += $result_all['market_price'];
                        }
                        // echo "<pre>";
                        //         print_r($data_array);
                        //         DIE;

                        foreach ($data_array as $key => $data) {

                            $count = 0;

                            $first_year_data += $data[$count + 1];
                            $second_year_data += $data[$count + 2];
                            $third_year_data += $data[$count + 3];

                        ?>

                            <tr>
                                <td class='text-dark'><strong><?= $data[$count] ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 1])){echo number_format($data[$count + 1]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 2])){echo number_format($data[$count + 2]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 3])){echo number_format($data[$count + 3]); }else{echo 0;} ?></strong></td>

                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 5])){echo number_format($data[$count + 5]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 5])){echo number_format($data[$count + 5]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format(  ($data[$count + 5]/$static_value) * 100, 2 ). ' %'; ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 6])){echo number_format($data[$count + 6]); }else{echo 0;} ?></strong></td>
                            </tr>

                        <?php $total_count ++; } ?>

                        <tr class="blank_row">
                            <td colspan="9"></td>
                        </tr>

                        <tr style="background-color: #cde2ff;">
                            <td class='text-dark'><strong>Total ( <?= $total_count; ?> )</strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($first_year_data)){echo number_format($first_year_data); }else{echo 0;} ?></strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($second_year_data)){echo number_format($second_year_data); }else{echo 0;} ?></strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($third_year_data)){echo number_format($third_year_data); }else{echo 0;} ?></strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($static_value)){ echo number_format  ($static_value);}else{echo 0;}  ?></strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($static_value)){ echo number_format  ($static_value);}else{echo 0;}  ?></strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($static_value)){ echo number_format(  ($static_value/$static_value) * 100, 2 ). ' %';}else{echo 0;}  ?></td>
                            <td class='text-dark text-right'><strong><?php if (isset($market_tot_price)){echo number_format($market_tot_price); }else{echo 0;} ?></strong></td>
                        </tr>

                        <tr class="blank_row">
                            <td colspan="9"></td>
                        </tr>

                        <tr style="background-color: #cde2ff;">
                            <td class='text-dark'><strong>Average</strong></td>
                            <td class='text-dark text-right'><strong><?= number_format($first_year_data/$total_count , 1) ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format($second_year_data/$total_count , 1) ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format($third_year_data/$total_count , 1) ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format($static_value/$total_count , 1) ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format($static_value/$total_count , 1) ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format( ($static_value/$static_value * 100)/$total_count , 1) .' '. '%' ?></td>
                            <td class='text-dark text-right'><strong><?= number_format($market_tot_price/$total_count , 1) ?></strong></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
// write js here
$this->registerJs('');
?>