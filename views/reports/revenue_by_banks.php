<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\ValuationReportsSearch;

$this->title = Yii::t('app', 'Periodical Valuations By Clients');
$cardTitle = Yii::t('app', 'Periodical Valuations By Clients');
$this->params['breadcrumbs'][] = $this->title;

$monthsArrForLoop =  Yii:: $app->appHelperFunctions->getMonthsForLoopArr();
$quartersList =  Yii:: $app->appHelperFunctions->getQuartersForLoop();
$yearList =  Yii:: $app->appHelperFunctions->getYearArr();


$model = new \app\models\Valuation();

if($duration_yearly<>null){
    $model->valuation_year = $duration_yearly;
}else{
    $duration_yearly = date("Y");
    $model->valuation_year = $duration_yearly;
}

if($type == 'banks' || $type == '')
{
    $clientList =  Yii:: $app->appHelperFunctions->getBankClients($duration_yearly);
}elseif($type == 'corporate')
{
    $clientList =  Yii:: $app->appHelperFunctions->getCorporateClients($duration_yearly);
}

if($duration<>null){
    $model->valuation_month = $duration;
}else{
    $duration = 'monthly';
    $model->valuation_month = $duration;
}

if($type<>null){
    $model->valuation_type = $type;
}else{
    $type = 'banks';
    $model->valuation_type = $type;
}
// if($data['valuation_year']=='all-time'){
//     $model->valuation_month = null;
//     $data['valuation_month'] = null;
// }

// if($data['valuation_month'] == 'yearly'){
//     $model->valuation_year = null;
// }

// echo "<pre>";
// print_r($clientList);
// echo "</pre>";die;

?>

<div class="card">
    <div class="card-body">
        <?php $form = ActiveForm::begin([
    'action' => ['client-revenue-banks-statistics'],
    'method' => 'get',
]); ?>
        <div class="row">
            <div class="col-4">
                <?php
                    echo $form->field($model, 'valuation_type')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->getCBArr(),
                        'options' => ['placeholder' => 'Select Type ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        ])->label(false);
                ?>
            </div>
            <div class="col-4">
                <?php
                    echo $form->field($model, 'valuation_month')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->getMQYArr(),
                        'options' => ['placeholder' => 'Select Month ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        ])->label(false);
                ?>
            </div>
            <div class="col-4" id="year_div">
                <?php
                    echo $form->field($model, 'valuation_year')->widget(Select2::classname(), [
                    'data' => Yii:: $app->appHelperFunctions->getYearArr(),
                    'options' => ['placeholder' => 'Select Year ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    ])->label(false);
                ?>
            </div>

        </div>

        <div class="row">
            <div class="col-4">
                    
            </div>

            <div class="col-4 text-center">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary ']) ?>
            </div>

            <div class="col-4">
                    
            </div>
        </div>

            
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
    if($duration == 'monthly'){
        echo $this->render('banks-revenues/monthly', [
            'data' => $data,
            'monthsArrForLoop' => $monthsArrForLoop,
            'clientList' => $clientList,
            'duration_yearly' => $duration_yearly,
            'duration' => $duration,
            'type' => $type,
        ]);
    }
    else if($duration=='quarterly'){

        echo $this->render('banks-revenues/quarterly', [
            'data' => $data,
            'clientList' => $clientList,
            'quartersList' => $quartersList,
            'duration' => $duration,
            'duration_yearly' => $duration_yearly,
            'type' => $type,
        ]);
    }
    else if($duration == 'yearly'){

        $this->registerJs('$("#year_div").hide();');

        echo $this->render('banks-revenues/yearly', [
            'data' => $data,
            'yearList' => $yearList,
            // 'clientList' => $clientList,
            'duration_yearly' => $duration_yearly,
            'duration' => $duration,
            'type' => $type,
            // 'yearsArrHelper' => Yii:: $app->appHelperFunctions->getYearArr(),
        ]);
    }
?>


<?php
// write js here
$this->registerJs('

    $("#valuation-valuation_month").change(function(){
        $data = $("#select2-valuation-valuation_month-container").attr("title");
        if($data == "Yearly")
        {
            $("#year_div").hide();
        }else{
            $("#year_div").show();
        }
        
      });

');
?>

        