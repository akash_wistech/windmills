<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', 'Client Revenue - '.$type);
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;

$this->registerJs('
$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});
');
?>



<div class="valuation-search">

  <?php
  if($type == 'Banks') {
      $form = ActiveForm::begin(['action' => ['client-revenue-banks'], 'method' => 'get',]);
  }else if($type == 'Corporate'){
      $form = ActiveForm::begin(['action' => ['client-revenue-corporate'], 'method' => 'get',]);
  }else if($type == 'Individual'){
      $form = ActiveForm::begin(['action' => ['client-revenue-individual'], 'method' => 'get',]);
  }

  ?>

    <div class="row">
        <div class="col-sm-3"></div>

        <div class="col-sm-6 text-center">
            <?php
            echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                'options' => ['placeholder' => 'Time Frame ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(false);
            ?>
        </div>
        <div class="col-sm-3"></div>

    </div>
    <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
        <div class="col-sm-4"></div>
        <div class="col-sm-4" id="end_date_id">

            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
        </div>
        <div class="col-sm-4"></div>
    </div>

    <div class="text-center">
        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>


<div class="valuation-index col-12">



    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
       // 'createBtn' => $createBtn,
        'showFooter' => true,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],

            ['attribute' => 'client_id',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {

                    return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title'),
                'footer' => '<b>Total:</b> <br><br> <b>Average:</b>',

            ],
            ['attribute' => 'total_valuations',
                'label' => Yii::t('app', 'Total Valuations'),
                'value' => function ($model) use ($allvalues) {

                    return number_format($model->total_valuations);
                },
                'footer' => number_format($allvalues[1]).'<br><br>'.number_format(($allvalues[1]/$total_working_days),2),
            ],
          /*  ['attribute' => 'total_valuations',
                'label' => Yii::t('app', 'Total Valuations'),
                'footer' => '100 <br><br> 80',
            ],*/
          /*  ['attribute' => 'client_revenue',
                'label' => Yii::t('app', 'Fee'),
                'value' => function ($model) use ($allvalues) {
                 return number_format($model->client_revenue);
                },
                'footer' => number_format($allvalues[0]).'<br><br>'.number_format(($allvalues[0]/$total_working_days),2),
            ],*/
            [
                'attribute' => 'id',
                'label' => Yii::t('app', 'Percentage'),
                'value' => function ($model) use ($allvalues) {
                    return number_format( ($model->client_revenue/$allvalues[0]) * 100,2). ' %';
                },
                'footer' => '100 % <br><br>'.number_format((100/$total_working_days),2). ' %',

            ],
           /* [
                'attribute' => 'id',
                'label' => Yii::t('app', 'Fee/Days'),
                'value' => function ($model) use ($allvalues,$total_working_days) {
                    return number_format( ($model->client_revenue/$total_working_days),2);
                },
                'footer' => number_format($allvalues[0]) .'<br><br>'.number_format(($allvalues[0]/$total_working_days),2),

            ],*/
        ],
    ]);
?>
    <?php CustomPjax::end(); ?>


</div>

<?php



$this->registerJs('
 $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
');
 ?>
