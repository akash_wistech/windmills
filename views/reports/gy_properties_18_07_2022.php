<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', 'Prices, Rent & Yield by Property Type');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;

?>




<div class="valuation-search">

  <?php $form = ActiveForm::begin([
      'action' => ['gross-yield-by-property-type'],
      'method' => 'get',
  ]); ?>

    <div class="row">


        <div class="col-sm-4 text-center">
            <?php
            echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                'data' => Yii:: $app->appHelperFunctions->reportPeriodNew,
               // 'options' => ['placeholder' => 'Time Frame ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Main');
            ?>
        </div>

        <div class="col-sm-4 text-center">
            <?php
            echo $form->field($searchModel, 'city_filter')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->emiratedListArr,
               // 'options' => ['placeholder' => 'Time Frame ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('City');
            ?>
        </div>

        <div class="col-sm-4 text-center">
            <?php
            echo $form->field($searchModel, 'time_period_compare')->widget(Select2::classname(), [
                'data' => Yii:: $app->appHelperFunctions->reportPeriodNew,
                // 'options' => ['placeholder' => 'Time Frame ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Compare');
            ?>
        </div>
    </div>

    <div class="text-center">
        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<div class="row">
<div class="valuation-index col-6">



    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
       // 'createBtn' => $createBtn,
        'showFooter' => true,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],

            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title. ' - '.$model->property->id;
                },
                'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],


            ['attribute' => 'id',
                'label' => Yii::t('app', 'SP/psf'),
                'value' => function ($model) {
                 return number_format($model->mv_psf);
                },
               // 'footer' => number_format($allvalues),
            ],
            ['attribute' => 'id',
                'label' => Yii::t('app', 'RP/psf'),
                'value' => function ($model) {
                    return number_format($model->rv_psf);
                },
                // 'footer' => number_format($allvalues),
            ],
            ['attribute' => 'id',
                'label' => Yii::t('app', 'GY'),
                'value' => function ($model) {

                    if($model->rv_psf > 0 && $model->mv_psf > 0){
                      return  number_format((($model->rv_psf/$model->mv_psf)* 100),2);
                    }else{
                        return 0;
                    }
                },
                // 'footer' => number_format($allvalues),
            ],


        ],

    ]);
?>
    <?php CustomPjax::end(); ?>


</div>
<div class="valuation-index col-6">



    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider_compare,
        // 'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        // 'createBtn' => $createBtn,
        'showFooter' => true,
        'columns' => [
            //  ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],

            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title. ' - '.$model->property->id;
                },
                'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],


            ['attribute' => 'id',
                'label' => Yii::t('app', 'SP/psf'),
                'value' => function ($model) {
                    return number_format($model->mv_psf);
                },
                // 'footer' => number_format($allvalues),
            ],
            ['attribute' => 'id',
                'label' => Yii::t('app', 'RP/psf'),
                'value' => function ($model) {
                    return number_format($model->rv_psf);
                },
                // 'footer' => number_format($allvalues),
            ],
            ['attribute' => 'id',
                'label' => Yii::t('app', 'GY'),
                'value' => function ($model) {

                    if($model->rv_psf > 0 && $model->mv_psf > 0){
                        return  number_format((($model->rv_psf/$model->mv_psf)* 100),2);
                    }else{
                        return 0;
                    }
                },
                // 'footer' => number_format($allvalues),
            ],


        ],

    ]);
    ?>
    <?php CustomPjax::end(); ?>


</div>
</div>

<?php



$this->registerJs('

');
 ?>

 <script>


 </script>
