
<?php

use app\models\User;
use app\models\ValuationApproversData;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$propertiesArr = ['' => 'select'] + $propertiesArr;

$communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$communitiesArr = ['' => 'select'] + $communitiesArr;

$citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
$citiesArr = ['' => 'select'] + $citiesArr;


$ref_no = '65px';
$instruction_date_width = '70px'; 
$val_created = '70px';
$insp_created = '70px';
$insp_schd = '70px'; 
$insp_done = '70px';
$recom = '70px';
$app_tat = '70px';
$tot_vat = '70px';
$valuer_width = '50px';
$io_width = '40px';
$approver_width = '40px';

$this->title = Yii::t('app', $type);
$this->params['breadcrumbs'][] = $this->title;

?>

<style>


.dataTable th {
      color: #0056b3;
      font-size: 15px;
      text-align: left !important;
      /* padding-right: 30px !important; */
  }
  .dataTable td {
      font-size: 16px;
      text-align: left;
      /* padding-right: 50px; */
      padding-top: 3px;
      max-width: 107px;
  }

.content-header h1 {
    font-size: 16px !important;

}

.content-header .row {
    margin-bottom: 0px !important;
}

.content-header {
    padding: 0px !important;
}

    th:nth-child(1) {
        min-width: 75px;
        max-width: 75px;
    }
    th:nth-child(2) {
      min-width: <?= $instruction_date_width ?>;
      max-width: <?= $instruction_date_width ?>;
    }
    th:nth-child(3) {
      min-width: <?= $val_created ?>;
      max-width: <?= $val_created ?>;
    }

    th:nth-child(4) {
      min-width: <?= $insp_created ?>;
      max-width: <?= $insp_created ?>;
    }
    th:nth-child(5) {
      min-width: <?= $insp_schd ?>;
      max-width: <?= $insp_schd ?>;
    }
    th:nth-child(6) {
      min-width: <?= $insp_done ?>;
      max-width: <?= $insp_done ?>;
    }
    th:nth-child(7) {
      min-width: <?= $recom ?>;
      max-width: <?= $recom ?>;
    }
    th:nth-child(8) {
      min-width: <?= $app_tat ?>;
      max-width: <?= $app_tat ?>;
    }

    th:nth-child(9) {
      min-width: <?= $tot_vat ?>;
      max-width: <?= $tot_vat ?>;
    }
    th:nth-child(10) {
      min-width: 60px;
      max-width: 60px;
    }

</style>

<?php
// echo "<pre>";
// print_r($model);
// echo "<pre>";die;
?>

<div class="card card-outline card-warning mx-2">
    <div class="card-body search_filter">
        <div class="col-3">
            <div class="form-group">
                <label> General Search : </label>
                <input type="search" id="custom-search-input-text" placeholder="Search from datatable" class="form-control custom-search-input-text">
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="comm-id" value='<?php echo $page_title; ?>'>
<div class="bank-revenue-index col-12">
    <div class="card card-outline card-info">
        <div class="card-body">
        <span><strong><?= $type; ?></strong></span><br /><br /><br />
            <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                <thead>
                    <tr>
                        <th class="">Instruction Date
                        <input id="intruction_date" type="date" class="form-control" placeholder="date">
                        </th>

                        <th class="">Inspection Schedule
                        <input id="inspection_schedule" type="date" class="form-control" placeholder="date">
                        </th>

                        <th class="">Approval Date
                        <input id="approval_date" type="date" class="form-control" placeholder="date">
                        </th>

                        <th class="">Reference Number
                            <input id="reference_number" type="text" class="custom-search-input form-control" placeholder="Ref. No.">
                        </th>

                        <th class="">Client Reference
                            <input id="Client Ref" type="text" class="custom-search-input form-control" placeholder="Ref. No.">
                        </th>

                        <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th>

                        <th class="">Reviewer
                            <input type="text" class="custom-search-input form-control" placeholder="Reviewer">
                        </th>

                        <th class="">Approver
                            <input type="text" class="custom-search-input form-control" placeholder="Approver">
                        </th>

                        <th class="">Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th class="">Total TAT
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <?php 
                          if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <th>Action</th>
                        <?php
                          }
                        ?>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd($dataProvider->getModels());
                        foreach($dataProvider->getModels() as $model){
                    ?>
                    <tr class="active">

                        <!-- Instruction -->
                        <td>
                            <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                            <br>
                            
                            <?php
                                if(isset($model->instruction_time))
                                {
                                    echo Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time);
                                }else{
                                    echo "";
                                }
                            ?>
                        </td>

                        <!-- Inspection schedule date  -->
                        <td>
                            <?=  date('d-M-Y', strtotime($model->scheduleInspection->inspection_date)) ?>
                            <br>
                            <?= Yii::$app->appHelperFunctions->formatTimeAmPm($model->scheduleInspection->inspection_time) ?>
                        </td>

                        <!-- Approval date  -->
                        <td>
                            <?php
                            if(isset($model->submission_approver_date))
                            {
                                // dd($model->submission_approver_date);
                                echo date('d-M-Y', strtotime($model->submission_approver_date));
                                echo "<br>";
                                echo date('h:i A', strtotime($model->submission_approver_date));
                            }else{
                                echo "";
                            }
                                
                            ?>
                        </td>

                        <!-- Reference Number -->
                        <td>
                            <?= $model->reference_number ?>
                        </td>

                        <!-- Client Ref Number -->
                        <td>
                        <?= $model->client_reference ?>
                        </td>

                        <!-- Valuer -->
                        <td>
                            <?php
                                echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                            ?>
                        </td>

                        <!-- Reviewer -->
                        <td>
                            <?php
                                $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                ->andWhere(['approver_type' => "reviewer"])
                                ->orderBy(['id' => SORT_DESC])
                                ->one();

                                $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                echo $get_name->lastname ;
                            ?>
                        </td>

                        <!-- Approver -->
                        <td>
                            <?php
                            echo $model->approverData->user->lastname ;
                            ?>
                        </td>

                        <!-- Fee -->
                        <td>
                            <?= Yii::$app->appHelperFunctions->wmFormate($model->total_fee) ?>
                        </td>

                        <!-- Total TAT -->
                        <td>
                            <?php
                                if($model->scheduleInspection->inspection_type != 3) {
                                    $inspection_time = $model->scheduleInspection->inspection_time . ":00";
                                    if ($inspection_time == null) {
                                        $inspection_time = '00:00:00';
                                    }

                                    // dd($model->submission_approver_date);

                                    if(!empty($model->scheduleInspection->inspection_date) && !empty($model->submission_approver_date))
                                    {
                                        $startDate = $model->scheduleInspection->inspection_date . ' ' .$inspection_time;
                                        $endDate = $model->submission_approver_date;

                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);

                                        $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                                        echo $test = abs(number_format(($workingHours/8.5),1)).'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';
                                    }
                                }else{
                                    echo "";
                                }
                            ?>
                        </td>

                        <?php 
                          if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <td class="noprint actions">
                            <div class="btn-group flex-wrap">
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                    data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a class="dropdown-item text-1"
                                        href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                        title="Step 1" data-pjax="0">
                                        <i class="fas fa-edit"></i> View 
                                    </a>
                                </div>
                            </div>
                        </td>
                        <?php
                          }
                        ?>

                    </tr>
                    <?php
                }
            }
        ?>
                </tbody>
                <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.css" />
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

<?php
    $this->registerJs('
     
     
    let table = new DataTable("#bank-revenue", {
        responsive: true,
        order: [[0, "desc"]],
        dom:"lrtip",
        pageLength: 50,
    });


    $(".custom-search-input").on("keyup", function () {
        table.search(this.value).draw();
      });
      
      $(".custom-search-input-text").on("keyup", function () {
        table.search(this.value).draw();
      });
      $(".custom-search-input").on("change", function () {
        table.search(this.value).draw();
      });
      
      $(".custom-search-input-client").on("change", function () {
            $.ajax({
            url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
            method: "post",
            dataType: "html",
            success: function(data) {
                console.log(data);
                table.search(data).draw();
            },
            error: bbAlert
            });
        });
            
      $("#bank-revenue_filter").css({
        "display":"none",
      });

      $(".custom-search-input").on("click", function (event) {
        event.stopPropagation();
      });
      $(".custom-search-input-client").on("click", function (event) {
        event.stopPropagation();
      });

    //for intruction_date search
    $("#intruction_date").on("change", function () {
    $(".custom-search-input").val("");
    var format1 = moment($("#intruction_date").val()).format("DD-MMM-YYYY"); 
    table.search(format1).draw();
    });

    //for inspection_schedule search
    $("#inspection_schedule").on("change", function () {
    $(".custom-search-input").val("");
    var format1 = moment($("#inspection_schedule").val()).format("DD-MMM-YYYY"); 
    table.search(format1).draw();
    });

    //for approval_date search
    $("#approval_date").on("change", function () {
    $(".custom-search-input").val("");
    var format1 = moment($("#approval_date").val()).format("DD-MMM-YYYY"); 
    table.search(format1).draw();
    });

');
?>