<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Valuations');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;
if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) {
    $actionBtns .= '{step_1}';
}
?>

<div class="valuation-index">


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'instruction_date',
                'label' => Yii::t('app', 'Instruction Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->instruction_date));
                },
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection Date'),
                'value' => function ($model) {
                    // return $model->scheduleInspection->inspection_date;
                    return date('d-m-Y', strtotime($model->scheduleInspection->inspection_date));
                },
                // 'filter' => ArrayHelper::map(\app\models\ScheduleInspection::find()->orderBy([
                //     'inspection_date' => SORT_ASC,
                // ])->all(), 'id', 'inspection_date')
            ],
            ['attribute' => 'inspection_time',
                'label' => Yii::t('app', 'Inspection Time'),
                'value' => function ($model) {
                    return $model->scheduleInspection->inspection_time;
                },
                // 'filter' => ArrayHelper::map(\app\models\ScheduleInspection::find()->orderBy([
                //     'inspection_date' => SORT_ASC,
                // ])->all(), 'id', 'inspection_date')
            ],
            ['attribute' => 'target_date',
                'label' => Yii::t('app', 'Target Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->target_date));
                },
            ],
            ['format' => 'raw','attribute' => 'reference_number', 'label' => Yii::t('app', 'Reference'),
                'value' => function ($model) {
                    return Html::a($model->reference_number, Url::to(['valuation/step_1','id'=>$model->id]), [
                        'title' => Yii::t('app', 'Reference'),
                        'target'=>"_blank",
                        // 'class' => 'dropdown-item text-1',
                        'data-pjax' => "0",
                    ]);
                },
            ],

            ['attribute' => 'client_id',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                    return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'client_reference', 'label' => Yii::t('app', 'Client Reference')],

            [
                'attribute' => 'sub_community',
                'label' => Yii::t('app', 'Sub Community'),
                'value' => function ($model) {
                    return $model->building->subCommunities->title;
                },
                'filter' => ArrayHelper::map(\app\models\SubCommunities::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            [
                'attribute' => 'city',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];
                },
                'filter' => Yii::$app->appHelperFunctions->emiratedListArr
            ],
            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
                },
                'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
                    'firstname' => SORT_ASC,
                ])->all(), 'id', 'firstname')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Building'),
                'value' => function ($model) {
                    return $model->building->title;
                },
                'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],



          /*  ['attribute' => 'purpose_of_valuation',
                'label' => Yii::t('app', 'Purpose'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->purposeOfValuationArr[$model['purpose_of_valuation']];
                },
                'filter' => Yii::$app->appHelperFunctions->purposeOfValuationArr
            ],*/
            ['format' => 'raw','attribute' => 'valuation_status',
                'label' => Yii::t('app', 'Valuation Status'),
                'value' => function ($model) {
                    $value=Yii::$app->helperFunctions->valuationStatusListArrLabel[$model['valuation_status']];

// target="_blank"
                    return Html::a($value, Url::to(['valuation/step_1','id'=>$model->id]), [
                        'title' => $value,
                        // 'class' => 'dropdown-item text-1',
                        // 'data-pjax' => "0",
                    ]);
                },
                'contentOptions' => ['class' => ' pt-3 pl-2'],
                'filter' => Yii::$app->helperFunctions->valuationStatusListArr
            ],

            // [ 'attribute' => 'status', 'label' => Yii::t('app', 'Status'), 'value' => function ($model) {
            //     return Yii::$app->helperFunctions->arrStatusIcon[$model['status']];
            // }, 'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'], 'filter' => Yii::$app->helperFunctions->arrFilterStatus],

            // [

            // ['format' => 'raw', 'attribute' => 'status', 'label' => Yii::t('app', 'Status'), 'value' => function ($model) {
            //     return Yii::$app->helperFunctions->arrStatusIcon[$model['status']];
            // }, 'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'], 'filter' => Yii::$app->helperFunctions->arrFilterStatus],
            //      [
            //       'class' => 'yii\grid\ActionColumn',
            //       'header' => '',
            //       'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
            //       'contentOptions' => ['class' => 'noprint actions'],
            //       'template' => '
            // <div class="btn-group flex-wrap">
            // 	<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
            //     <span class="caret"></span>
            //   </button>
            // 	<div class="dropdown-menu" role="menu">
            //     ' . $actionBtns . '
            // 	</div>
            // </div>',
            //       'buttons' => [
            //           'view' => function ($url, $model) {
            //               return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
            //                   'title' => Yii::t('app', 'View'),
            //                   'class' => 'dropdown-item text-1',
            //                   'data-pjax' => "0",
            //               ]);
            //           },
            //
            //           'step_1' => function ($url, $model) {
            //               return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Step 1'), $url, [
            //                   'title' => Yii::t('app', 'Step 1'),
            //                   'class' => 'dropdown-item text-1',
            //                   'data-pjax' => "0",
            //               ]);
            //           },
            //           'status' => function ($url, $model) {
            //               if ($model['status'] == 1) {
            //                   return Html::a('<i class="fas fa-eye-slash"></i> ' . Yii::t('app', 'Disable'), $url, [
            //                       'title' => Yii::t('app', 'Disable'),
            //                       'class' => 'dropdown-item text-1',
            //                       'data-confirm' => Yii::t('app', 'Are you sure you want to disable this item?'),
            //                       'data-method' => "post",
            //                   ]);
            //               } else {
            //                   return Html::a('<i class="fas fa-eye"></i> ' . Yii::t('app', 'Enable'), $url, [
            //                       'title' => Yii::t('app', 'Enable'),
            //                       'class' => 'dropdown-item text-1',
            //                       'data-confirm' => Yii::t('app', 'Are you sure you want to enable this item?'),
            //                       'data-method' => "post",
            //                   ]);
            //               }
            //           },
            //           'delete' => function ($url, $model) {
            //               return Html::a('<i class="fas fa-trash"></i> ' . Yii::t('app', 'Delete'), $url, [
            //                   'title' => Yii::t('app', 'Delete'),
            //                   'class' => 'dropdown-item text-1',
            //                   'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            //                   'data-method' => "post",
            //                   'data-pjax' => "0",
            //               ]);
            //           },
            //       ],
            //   ],
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>
