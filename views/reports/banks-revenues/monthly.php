<?php

use app\models\Valuation;
use app\models\ReceivedDocsFiles;
use app\models\ScheduleInspection;
use yii\db\Expression;

?>

<style>
    .blank_row
    {
        height: 20px !important; /* overwrites any other rules */
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="card  card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title"><b>Periodic Valuations by Clients - Report (<?= $duration_yearly ?>)</b></h3>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-sm table-hover text-nowrap table-striped">
                    <thead>
                        <tr>
                            <th class="text-primary"> Clients </th>

                            <?php foreach ($monthsArrForLoop as $month_key => $month) { ?>

                                <th class="text-primary text-right"> <?= $month ?> </th>

                            <?php } ?>

                            <th class="text-primary text-right">Valuation %</th>
                            <th class="text-primary text-right">Fee</th>
                            <th class="text-primary text-right">Fee (%)</th>
                            <th class="text-primary text-right">Market Values</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $data_array = array();
                        $static_value = 0;
                        $value_perecent = 0;
                        $market_tot_price = 0;
                        $total_count = 0;
                        foreach ($clientList as $client_key => $client_name) {

                            if($clientList[$client_key]['nick_name'] == '' || $clientList[$client_key]['nick_name'] == 'NULL')
                            {
                                $data_array[$client_key][] = $clientList[$client_key]['title'];
                            }else{
                                $data_array[$client_key][] = $clientList[$client_key]['nick_name'];
                            }

                            foreach ($monthsArrForLoop as $month_key => $month) {
                                
                                $result = Yii::$app->appHelperFunctions->getBankTotal($clientList[$client_key]['id'], $month_key, $duration_yearly);
                                $data_array[$client_key][] = $result['total_valuations'];
                            }
                            $data_array[$client_key][] = $result['total_valuations'];
                            $data_array[$client_key][] = $result['total_fee'];
                            $data_array[$client_key][] = $result['market_price'];
                            $static_value += $result['total_fee'];
                            $value_perecent += $result['total_valuations'];
                            $market_tot_price += $result['market_price'];
                        }





                        foreach ($data_array as $key => $data) {

                            $count = 0;

                            $Jan_data += $data[$count + 1];
                            $Feb_data += $data[$count + 2];
                            $Mar_data += $data[$count + 3];
                            $Apr_data += $data[$count + 4];
                            $May_data += $data[$count + 5];
                            $Jun_data += $data[$count + 6];
                            $Jul_data += $data[$count + 7];
                            $Aug_data += $data[$count + 8];
                            $Sep_data += $data[$count + 9];
                            $Oct_data += $data[$count + 10];
                            $Nov_data += $data[$count + 11];
                            $Dec_data += $data[$count + 12];
                            
                        ?>


                            <tr>
                                <td class='text-dark'><strong><?= $data[$count] ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 1])){echo number_format($data[$count + 1]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 2])){echo number_format($data[$count + 2]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 3])){echo number_format($data[$count + 3]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 4])){echo number_format($data[$count + 4]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 5])){echo number_format($data[$count + 5]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 6])){echo number_format($data[$count + 6]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 7])){echo number_format($data[$count + 7]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 8])){echo number_format($data[$count + 8]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 9])){echo number_format($data[$count + 9]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 10])){echo number_format($data[$count + 10]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 11])){echo number_format($data[$count + 11]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 12])){echo number_format($data[$count + 12]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 14])){echo number_format($data[$count + 14]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format(  ($data[$count + 14]/$value_perecent) * 100, 2 ). ' %'; ?></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 15])){echo number_format($data[$count + 15]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format(  ($data[$count + 15]/$static_value) * 100, 2 ). ' %'; ?></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 16])){echo number_format($data[$count + 16]); }else{echo 0;} ?></strong></td>
                            </tr>
                            
                        <?php $total_count ++;} ?>

                            <tr class="blank_row">
                                <td colspan="18"></td>
                            </tr>

                            <tr style="background-color: #cde2ff;">
                                <td class='text-dark'><strong>Total ( <?= $total_count; ?> )</strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($Jan_data)){echo number_format($Jan_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($Feb_data)){echo number_format($Feb_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($Mar_data)){echo number_format($Mar_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($Apr_data)){echo number_format($Apr_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($May_data)){echo number_format($May_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($Jun_data)){echo number_format($Jun_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($Jul_data)){echo number_format($Jul_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($Aug_data)){echo number_format($Aug_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($Sep_data)){echo number_format($Sep_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($Oct_data)){echo number_format($Oct_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($Nov_data)){echo number_format($Nov_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($Dec_data)){echo number_format($Dec_data); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($value_perecent)){ echo number_format($value_perecent); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($value_perecent)){ echo number_format(($value_perecent/$value_perecent) * 100, 2 ). ' %';}else{echo 0;} ?></td>
                                <td class='text-dark text-right'><strong><?php if (isset($static_value)){ echo number_format  ($static_value);}else{echo 0;}  ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($static_value)){ echo number_format(  ($static_value/$static_value) * 100, 2 ). ' %';}else{echo 0;}  ?></td>
                                <td class='text-dark text-right'><strong><?php if (isset($market_tot_price)){echo number_format($market_tot_price); }else{echo 0;} ?></strong></td>
                            </tr>

                            <tr class="blank_row">
                                <td colspan="18"></td>
                            </tr>

                            <tr style="background-color: #cde2ff;">
                                <td class='text-dark'><strong>Average</strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($Jan_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($Feb_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($Mar_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($Apr_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($May_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($Jun_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($Jul_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($Aug_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($Sep_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($Oct_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($Nov_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($Dec_data/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($value_perecent/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format( ($value_perecent/$value_perecent * 100)/$total_count , 1) .' '. '%' ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format($static_value/$total_count , 1) ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format( ($static_value/$static_value * 100)/$total_count , 1) .' '. '%' ?></td>
                                <td class='text-dark text-right'><strong><?= number_format($market_tot_price/$total_count , 1) ?></strong></td>
                            </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
// write js here
$this->registerJs('');
?>