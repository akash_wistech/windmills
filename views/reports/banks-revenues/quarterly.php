<?php

use app\models\Valuation;
use app\models\ReceivedDocsFiles;
use app\models\ScheduleInspection;
use yii\db\Expression;

?>

<style>
    .blank_row
    {
        height: 20px !important; /* overwrites any other rules */
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="card  card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title"><b>Periodic Valuations by Clients - Report (<?= $duration_yearly ?>)</b></h3>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-sm table-hover text-nowrap table-striped">
                    <thead>
                        <tr>
                            <th class="text-primary"> Clients </th>

                            <?php foreach ($quartersList as $quarter_key => $quarter) { ?>

                                <th class="text-primary text-right"> <?= $quarter ?> </th>

                            <?php } ?>
                            <th class="text-primary text-right">Valuation %</th>
                            <th class="text-primary text-right">Fee</th>
                            <th class="text-primary text-right">Fee Percentage</th>
                            <th class="text-primary text-right">Market Values</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $data_array = array();
                        $static_value = 0;
                        $value_perecent = 0;
                        $market_tot_price = 0;
                        $total_count = 0;
                        foreach ($clientList as $client_key => $client_name) {

                            if($clientList[$client_key]['nick_name'] == '' || $clientList[$client_key]['nick_name'] == 'NULL')
                            {
                                $data_array[$client_key][] = $clientList[$client_key]['title'];
                            }else{
                                $data_array[$client_key][] = $clientList[$client_key]['nick_name'];
                            }

                            foreach ($quartersList as $quarter_key => $quarter) {
                                // echo($month_key);
                                
                                $result = Yii::$app->appHelperFunctions->getBankTotalQuarterly($clientList[$client_key]['id'], $quarter_key, $duration_yearly);
                                $data_array[$client_key][] = $result['total_valuations'];
                            }
                            $data_array[$client_key][] = $result['total_valuations'];
                            $data_array[$client_key][] = $result['total_fee'];
                            $data_array[$client_key][] = $result['market_price'];
                            $static_value += $result['total_fee'];
                            $value_perecent += $result['total_valuations'];
                            $market_tot_price += $result['market_price'];
                        }
                        // echo "<pre>";
                        //         print_r($data_array);
                        //         DIE;


                        foreach ($data_array as $key => $data) {

                            $count = 0;

                            $first_quarter_data += $data[$count + 1];
                            $second_quarter_data += $data[$count + 2];
                            $third_quarter_data += $data[$count + 3];
                            $last_quarter_data += $data[$count + 4];

                        ?>

                            <tr>
                                <td class='text-dark'><strong><?= $data[$count] ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 1])){echo number_format($data[$count + 1]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 2])){echo number_format($data[$count + 2]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 3])){echo number_format($data[$count + 3]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 4])){echo number_format($data[$count + 4]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 5])){echo number_format($data[$count + 5]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format(  ($data[$count + 5]/$value_perecent) * 100, 2 ). ' %'; ?></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 7])){echo number_format($data[$count + 7]); }else{echo 0;} ?></strong></td>
                                <td class='text-dark text-right'><strong><?= number_format(  ($data[$count + 7]/$static_value) * 100, 2 ). ' %'; ?></td>
                                <td class='text-dark text-right'><strong><?php if (isset($data[$count + 8])){echo number_format($data[$count + 8]); }else{echo 0;} ?></strong></td>
                            </tr>

                        <?php $total_count ++; } ?>

                        <tr class="blank_row">
                            <td colspan="10"></td>
                        </tr>

                        <tr style="background-color: #cde2ff;">
                            <td class='text-dark'><strong>Total ( <?= $total_count; ?> )</strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($first_quarter_data)){echo number_format($first_quarter_data); }else{echo 0;} ?></strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($second_quarter_data)){echo number_format($second_quarter_data); }else{echo 0;} ?></strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($third_quarter_data)){echo number_format($third_quarter_data); }else{echo 0;} ?></strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($last_quarter_data)){echo number_format($last_quarter_data); }else{echo 0;} ?></strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($value_perecent)){ echo number_format($value_perecent); }else{echo 0;} ?></strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($value_perecent)){ echo number_format(($value_perecent/$value_perecent) * 100, 2 ). ' %';}else{echo 0;} ?></td>
                            <td class='text-dark text-right'><strong><?php if (isset($static_value)){ echo number_format  ($static_value);}else{echo 0;}  ?></strong></td>
                            <td class='text-dark text-right'><strong><?php if (isset($static_value)){ echo number_format(  ($static_value/$static_value) * 100, 2 ). ' %';}else{echo 0;}  ?></td>
                            <td class='text-dark text-right'><strong><?php if (isset($market_tot_price)){echo number_format($market_tot_price); }else{echo 0;} ?></strong></td>
                        </tr>

                        <tr class="blank_row">
                            <td colspan="10"></td>
                        </tr>

                        <tr style="background-color: #cde2ff;">
                            <td class='text-dark'><strong>Average</strong></td>
                            <td class='text-dark text-right'><strong><?= number_format($first_quarter_data/$total_count , 1) ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format($second_quarter_data/$total_count , 1) ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format($third_quarter_data/$total_count , 1) ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format($last_quarter_data/$total_count , 1) ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format($value_perecent/$total_count , 1) ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format( ($value_perecent/$value_perecent * 100)/$total_count , 1) .' '. '%' ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format($static_value/$total_count , 1) ?></strong></td>
                            <td class='text-dark text-right'><strong><?= number_format( ($static_value/$static_value * 100)/$total_count , 1) .' '. '%' ?></td>
                            <td class='text-dark text-right'><strong><?= number_format($market_tot_price/$total_count , 1) ?></strong></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
// write js here
$this->registerJs('');
?>