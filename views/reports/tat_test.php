<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', $type);
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
$actionBtns = '';
$createBtn = false;
$import = false;
?>




<div class="valuation-search">

  <?php
      $form = ActiveForm::begin([
          'action' => ['tat-test-reports'],
          'method' => 'get',
      ]);
  ?>
    <div class="row">
        <div class="col-sm-3"></div>

        <div class="col-sm-6 text-center">
            <?php
            echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                'data' => Yii:: $app->appHelperFunctions->reportPeriodNew,
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(false);
            ?>
        </div>
    </div>
    <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
        <div class="col-sm-4"></div>
        <div class="col-sm-4" id="end_date_id">

            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
        </div>
        <div class="col-sm-4"></div>
    </div>

    <div class="text-center">
        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>


<div class="valuation-index col-12">

    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>

    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
     //   'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'import' => $import,
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'reference_number',
                'label' => Yii::t('app', 'Reference Number'),
                'value' => function ($model) {
                   return $model->reference_number ;
                },
                'footer' => '<b>Total:</b> <br><br> <b>Average:</b>',
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Submission date - Instruction Date'),
                'value' => function ($model)  use ($sub_inst,$total_rows) {


                    $instruction_time = date('H:i:s', strtotime($model->created_at));
                    $datetime1 = new DateTime($model->approverData->created_at);
                    $datetime2 = new DateTime($model->instruction_date.' '.	$instruction_time);
                    $hours = round(($datetime2->getTimestamp() - $datetime1->getTimestamp()) / 3600, 2);
                  //  return abs(number_format($hours/24,2));
                    return ($hours.'-'.$model->approverData->created_at .' - '.$model->instruction_date.' '.	$instruction_time );
                },
                'footer' => number_format($sub_inst,2) .'<br><br>'. number_format(($sub_inst/$total_rows),2),
                // 'footer' => number_format($sub_inst),
            ],
            ['attribute' => 'created_at',
                'label' => Yii::t('app', 'Inspection Date - Instruction Date'),
                'value' => function ($model)  use ($insp_sub,$total_rows) {
                    $instruction_time = date('H:i:s', strtotime($model->created_at));
                    $datetime1 = new DateTime($model->inspection_date.' '.	$model->inspection_time.':00');
                    $datetime2 = new DateTime($model->instruction_date.' '.	$instruction_time);
                    $hours = round(($datetime2->getTimestamp() - $datetime1->getTimestamp()) / 3600, 2);
                  //   return abs(number_format($hours/24,2));
                    return ($hours.'-'.$model->instruction_date.' '.$instruction_time .' - '.$model->inspection_date.' '.	$model->inspection_time.':00' );
                },
                'footer' => number_format($insp_sub,2) .'<br><br>'. number_format(($insp_sub/$total_rows),2),
            ],
           /* ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection Date - Instruction Date'),
                'value' => function ($model)  use ($insp_sub,$total_rows) {

                    $diff = strtotime($model->inspection_date) - strtotime($model->instruction_date);
                    return abs(round($diff / 86400));
                },
                'footer' => number_format($insp_sub) .'<br><br>'. number_format(($insp_sub/$total_rows),2),
            ],*/


           /* ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Submission date - Instruction Date'),
                'value' => function ($model)  use ($sub_inst,$total_rows) {
                    $submission_date = date('Y-m-d', strtotime($model->approverData->created_at));;
                    $diff = strtotime($submission_date) - strtotime($model->instruction_date);
                    return abs(round($diff / 86400));
                },
                'footer' => number_format($sub_inst) .'<br><br>'. number_format(($sub_inst/$total_rows),2),
               // 'footer' => number_format($sub_inst),
            ],*/

            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Submission Date - Inspection Date'),
                'value' => function ($model) use ($sub_insp,$total_rows){

                    $instruction_time = date('H:i:s', strtotime($model->created_at));
                    $datetime1 = new DateTime($model->approverData->created_at);
                    $datetime2 =  new DateTime($model->inspection_date.' '.	$model->inspection_time.':00');
                    $hours = round(($datetime1->getTimestamp() - $datetime2->getTimestamp()) / 3600, 2);
                   // return abs(number_format($hours/24,2));
                    return ($hours.'-'.$model->approverData->created_at .' - '.$model->inspection_date.' '.	$model->inspection_time.':00' );
                },
                'footer' => number_format($sub_insp,2) .'<br><br>'. number_format(($sub_insp/$total_rows),2),
                //'footer' => number_format($sub_insp),
            ],
           /* ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Submission Date - Inspection Date'),
                'value' => function ($model) use ($sub_insp,$total_rows){
                    $submission_date = date('Y-m-d', strtotime($model->approverData->created_at));;
                    $diff = strtotime($submission_date) - strtotime($model->inspection_date);
                    return abs(round($diff / 86400));
                },
                'footer' => number_format($sub_insp) .'<br><br>'. number_format(($sub_insp/$total_rows),2),
                //'footer' => number_format($sub_insp),
            ],*/
           /* ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Approver Date - Instruction Date'),
                'value' => function ($model)  use ($app_insp) {
                    $submission_date = date('Y-m-d', strtotime($model->approverData->created_at));;
                    $diff = strtotime($submission_date) - strtotime($model->instruction_date);
                    return abs(round($diff / 86400));
                },
                'footer' => number_format($app_insp),
            ],*/
            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
                },
            ],
            [
                'attribute' => 'inspection_officer_name',
                'label' => Yii::t('app', 'Inspection Officer'),
                'value' => function ($model) {
                  //  return $model->inspection_officer_name;
                    return (isset($model->inspection_officer_name) && ($model->inspection_officer_name <> null))? (Yii::$app->appHelperFunctions->staffMemberListArr[$model->inspection_officer_name]): '';
                },
            ],
            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title;
                }
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Community'),
                'value' => function ($model) {
                    return $model->building->communities->title;
                },
            ],
            ['attribute' => 'tenure',
                'label' => Yii::t('app', 'Tenure'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure];

                }
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];

                }
            ],
            [
                'attribute' => 'approver',
                'label' => Yii::t('app', 'Approver'),
                'value' => function ($model) {
                    return $model->approverData->user->firstname ;
                    //return $model->valuationParent->approverData->user->id.' '.$model->valuationParent->approverData->user->lastname;
                },

            ],
        ],
    ]); ?>

    <?php CustomPjax::end(); ?>
</div>
<?php
$this->registerJs('
$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});
 $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
');
?>