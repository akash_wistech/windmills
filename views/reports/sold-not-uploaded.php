<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', $type);
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
$actionBtns = '';
$createBtn = false;
$import = false;
?>




<div class="valuation-search">

  <?php
      $form = ActiveForm::begin([
          'action' => ['sold-not-uploaded'],
          'method' => 'get',
      ]);
  ?>
    <div class="row">
        <div class="col-sm-3"></div>

        <div class="col-sm-3 text-center">
            <?php
            $searchModel->years = date('Y');
            echo $form->field($searchModel, 'years')->widget(Select2::classname(), [
                'data' => $years,
            ])->label(false);
            ?>
        </div>
        <div class="col-sm-3 text-center">
            <?php
            $searchModel->months = date('m');
            echo $form->field($searchModel, 'months')->widget(Select2::classname(), [
                'data' => $months,
            ])->label(false);
            ?>
        </div>
    </div>
    <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
        <div class="col-sm-4"></div>
        <div class="col-sm-4" id="end_date_id">

            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
        </div>
        <div class="col-sm-4"></div>
    </div>

    <div class="text-center">
        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>


<div class="valuation-index col-12">

    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>

    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
     //   'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'import' => $import,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'created_at',
                'label' => Yii::t('app', 'Transaction Date'),
                'value' => function ($model) {
                    $transDate=$model['created_at'];
                    if($transDate!='' && $transDate!=null)$transDate=Yii::$app->formatter->asDate($transDate);
                    return $transDate;
                },
            ],

           // ['attribute' => 'duplicates', 'label' => Yii::t('app', 'No. of Duplicates')],

        ],
    ]); ?>

    <?php CustomPjax::end(); ?>
</div>

<?php
$this->registerJs('
$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});
 $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
');
?>