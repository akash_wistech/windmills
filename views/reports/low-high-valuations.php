<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', $type);
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;

?>




<div class="valuation-search">

  <?php

  if($type == 'Low-High Valuations') {
      $form = ActiveForm::begin([
          'action' => ['low-high-valuations'],
          'method' => 'get',
      ]);
  }else if($type == 'Modified Valuations'){
      $form = ActiveForm::begin([
          'action' => ['modified-valuations'],
          'method' => 'get',
      ]);
  }else if($type == 'Errors In Valuations'){
      $form = ActiveForm::begin([
          'action' => ['errors-valuations'],
          'method' => 'get',
      ]);
  }



  ?>
    <div class="row">
        <div class="col-sm-3"></div>

        <div class="col-sm-6 text-center">
            <?php
            echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                'options' => ['placeholder' => 'Time Frame ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(false);
            ?>
        </div>
    </div>
    <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
        <div class="col-sm-4"></div>
        <div class="col-sm-4" id="end_date_id">

            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
        </div>
        <div class="col-sm-4"></div>
    </div>

    <div class="text-center">
        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>


<div class="valuation-index col-12">

    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
       // 'createBtn' => $createBtn,
        'showFooter' => true,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'client_id',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {

                    return $model->client->title;
                },
                'footer' => '<b>Total:</b> <br><br> <b>Average:</b>',
            ],
            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title;
                },
                'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'parent_estimated_market_value',
                'label' => Yii::t('app', 'Original Value'),
                'value' => function ($model) {
        return number_format($model->valuationParent->approverData->estimated_market_value);
                },
            ],
            ['attribute' => 'estimated_market_value',
                'label' => Yii::t('app', 'Revised'),
                'value' => function ($model) {
                    return number_format($model->estimated_market_value);
                },
            ],
            [
                'attribute' => 'percentage_value',
                'label' => Yii::t('app', 'Percentage Value'),
                'value' => function ($model) use ($change_average,$average){

                    if($model->valuationParent->approverData->estimated_market_value > $model->estimated_market_value){
                            $updated_amount = ($model->valuationParent->approverData->estimated_market_value - $model->estimated_market_value);
                        return -number_format( ($updated_amount/$model->valuationParent->approverData->estimated_market_value) * 100,2). ' %';
                    }else{
                        $updated_amount = ($model->estimated_market_value - $model->valuationParent->approverData->estimated_market_value);
                        return number_format( ($updated_amount/$model->valuationParent->approverData->estimated_market_value) * 100,2). ' %';
                    }
                },
                'footer' => number_format($change_average,2).' <br><br>'.$average,
            ],
            [
                'attribute' => 'approver',
                'label' => Yii::t('app', 'Approver'),
                'value' => function ($model) {
                    return $model->valuationParent->approverData->user->firstname ;
                    //return $model->valuationParent->approverData->user->id.' '.$model->valuationParent->approverData->user->lastname;
                },

            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Community'),
                'value' => function ($model) {
                    return $model->building->communities->title;
                },
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];

                },
            ],
            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
                },
                'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
                    'firstname' => SORT_ASC,
                ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];})
            ],

        ],

    ]);
?>
    <?php CustomPjax::end(); ?>
</div>

<?php
$this->registerJs('
$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});
 $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
');
?>