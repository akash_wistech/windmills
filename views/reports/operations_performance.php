<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', 'Operations Team Performance');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;

?>




    <div class="valuation-search" style=" padding-top: 50px;">

        <?php $form = ActiveForm::begin([
            'action' => ['operationsteam'],
            'method' => 'get',
        ]); ?>
        <div class="row">
            <div class="col-sm-3"></div>

            <div class="col-sm-6 text-center">
                <?php
                echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                    'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                    'options' => ['placeholder' => 'Time Frame ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false);
                ?>
            </div>
        </div>
        <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
            <div class="col-sm-4"></div>
            <div class="col-sm-4" id="end_date_id">

                <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
            </div>
            <div class="col-sm-4"></div>
        </div>

        <div class="text-center">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>


    <div class="valuation-index col-12">

        <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
        <?= CustomGridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'cardTitle' => $cardTitle,
            // 'createBtn' => $createBtn,
            'showFooter' => true,
            'columns' => [
                //  ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
                [
                    'attribute' => 'created_by',
                    'label' => Yii::t('app', 'Valuer'),
                    'value' => function ($model) {
                        $clientsArr =\app\models\User::find()
                            ->select([
                                'id,CONCAT(firstname, " ", lastname) AS fullname',
                            ])
                            ->where(['id'=>$model->created_by])
                            ->asArray()->one();
                        return $clientsArr['fullname'];
                    },
                    'footer' => '<b>Total:</b> <br><br> <b>Average:</b>',
                ],
                ['attribute' => 'total_valuations',
                    'label' => Yii::t('app', 'Total Valuations'),
                    'value' => function ($model) use ($allvalues_count) {

                        return Yii::$app->appHelperFunctions->wmFormate($model->total_valuations);
                    },
                    'footer' => Yii::$app->appHelperFunctions->wmFormate($allvalues_count).'<br><br>'.Yii::$app->appHelperFunctions->wmFormate(($allvalues_count/$total_working_days)),
                ],


                [
                    'attribute' => 'id',
                    'label' => Yii::t('app', 'Percentage By Valuations '),
                    'value' => function ($model) use ($allvalues_count) {
            if($allvalues_count > 0) {
                return number_format(($model->total_valuations / $allvalues_count) * 100, 2) . ' %';
            }else{
                return 0;
            }
                    },
                    'footer' => '100 % <br><br>'.number_format((100/$total_working_days),2). ' %',

                ],
                [
                    'attribute' => 'id',
                    'label' => Yii::t('app', 'Total Inspections'),
                    'value' => function ($model)  use ($inspection_array,$total_inspections){
                        return $inspection_array[$model->created_by];
                        // return ($model->tat - $weekdays);
                    },
                    'footer' => Yii::$app->appHelperFunctions->wmFormate($total_inspections).'<br><br>'.Yii::$app->appHelperFunctions->wmFormate(($total_inspections/$total_working_days)),
                ],

                [
                    'attribute' => 'id',
                    'label' => Yii::t('app', 'Total Stored Reports'),
                    'value' => function ($model)  use ($stored_array,$total_stored){
                        return $stored_array[$model->created_by];
                        // return ($model->tat - $weekdays);
                    },
                    'footer' => Yii::$app->appHelperFunctions->wmFormate($total_stored).'<br><br>'.Yii::$app->appHelperFunctions->wmFormate(($total_stored/$total_working_days)),
                ],

            ],

        ]);
        ?>
        <?php CustomPjax::end(); ?>
    </div>

<?php
$this->registerJs('
$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});
 $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
');
?>