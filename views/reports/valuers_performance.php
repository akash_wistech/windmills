<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', 'Valuers Performance');
$cardTitle = Yii::t('app', 'Valuers Performance');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
$workingHours_2 = 0;

if(isset($_SESSION['total_revenue']) ? $_SESSION['total_revenue'] : '');
if(isset($_SESSION['total_valuations']) ? $_SESSION['total_valuations'] : '');
if(isset($_SESSION['total_inspections']) ? $_SESSION['total_inspections'] : '');
if(isset($_SESSION['client_business']) ? $_SESSION['client_business'] : '');
if(isset($_SESSION['low_valuations']) ? $_SESSION['low_valuations'] : '');
if(isset($_SESSION['high_valuations']) ? $_SESSION['high_valuations'] : '');
if(isset($_SESSION['error_valuations']) ? $_SESSION['error_valuations'] : '');
if(isset($_SESSION['count_val']) ? $_SESSION['count_val'] : '');

?>




    <div class="valuation-search">

        <?php $form = ActiveForm::begin([
            'action' => ['valuers-performance-data'],
            'method' => 'get',
        ]); ?>
        <div class="row">
            <div class="col-sm-3"></div>

            <div class="col-sm-6 text-center">
                <?php
                echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                    'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                    'options' => ['placeholder' => 'Time Frame ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false);
                ?>
            </div>
        </div>
        <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
            <div class="col-sm-4"></div>
            <div class="col-sm-4" id="end_date_id">

                <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
            </div>
            <div class="col-sm-4"></div>
        </div>

        <div class="text-center">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>


    <div class="valuation-index col-12">

        <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
        <?= CustomGridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            // 'cardTitle' => $cardTitle,
            //    'createBtn' => $createBtn,
            'showFooter' => true,
            'options' => [
                'id' => 'sortable-table',
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:1px;']],
                [
                    'attribute' => 'service_officer',
                    'label' => Yii::t('app', 'Valuer'),
                    'headerOptions' => ['style' => 'color:#017BFE;'],
                    'value' => function ($model) {
                        return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                    },
                    'footer' => '<b>Total:'.'<br>'.'Average:',
                ],
                ['attribute' => 'total_valuations',
                    'label' => Yii::t('app', 'Total Valuations'),
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'headerOptions' => ['style' => 'color:#017BFE;' , 'class' => 'sortable'],
                    'value' => function ($model) {
                        return $model->total_valuations;
                    },
                    'footer' => Yii::$app->appHelperFunctions->wmFormate($_SESSION['total_valuations']).'<br>'.(Yii::$app->appHelperFunctions->wmFormate($_SESSION['total_valuations']/$_SESSION['count_val'])),
                ],

                [
                    'attribute' => 'total_inspections',
                    'label' => Yii::t('app', 'Total Inspections'),
                    'headerOptions' => ['style' => 'color:#017BFE;' , 'class' => 'sortable'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) {
                        return ($model->total_inspections);
                    },
                    'footer' => Yii::$app->appHelperFunctions->wmFormate($_SESSION['total_inspections']).'<br>'.(Yii::$app->appHelperFunctions->wmFormate($_SESSION['total_inspections']/$_SESSION['count_val'])),
                ],

                ['attribute' => 'total_assignments',
                    'label' => Yii::t('app', 'Total Assignments'),
                    'headerOptions' => ['style' => 'color:#017BFE;'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) {
                        return ($model->total_valuations+$model->total_inspections) / 2;
                    },
                    'footer' => (($_SESSION['total_valuations'] + $_SESSION['total_inspections'])/2).'<br>'.(Yii::$app->appHelperFunctions->wmFormate((($_SESSION['total_valuations'] + $_SESSION['total_inspections'])/2)/$_SESSION['count_val'])),
                ],


                ['attribute' => 'client_revenue',
                    'label' => Yii::t('app', 'Fee'),
                    'headerOptions' => ['style' => 'color:#017BFE;'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) {
                        return Yii::$app->appHelperFunctions->wmFormate($model->client_revenue);
                    },
                    'footer' => Yii::$app->appHelperFunctions->wmFormate($_SESSION['total_revenue']).'<br>'.(Yii::$app->appHelperFunctions->wmFormate($_SESSION['total_revenue']/$_SESSION['count_val'])),
                ],
                [
                    'attribute' => 'fee_precentage',
                    'label' => Yii::t('app', 'Fee Percentage'),
                    'headerOptions' => ['style' => 'color:#017BFE;'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) {
                        return number_format( ($model->client_revenue/$_SESSION['total_revenue']) * 100,2). ' %';
                    },
                    'footer' => '100 %'.'<br>'.number_format(100/$_SESSION['count_val'],2).'',

                ],

                [
                    'attribute' => 'avg_done_time',
                    'label' => Yii::t('app', 'AVG Inspection Done Time'),
                    'headerOptions' => ['style' => 'color:#017BFE;' , 'class' => 'sortable'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) use ($start_date,$end_date) {
                        return Yii::$app->appHelperFunctions->wmFormate(Yii::$app->appHelperFunctions->getCalculateAvgVals1($model->service_officer_name,$start_date,$end_date));
                    },

                    'footer' => '<br>'.number_format($_SESSION['time']/$_SESSION['count_val'],2),

                ],

                [
                    'attribute' => 'avg_recom_time',
                    'label' => Yii::t('app', 'AVG Recommend Time'),
                    'headerOptions' => ['style' => 'color:#017BFE;' , 'class' => 'sortable'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) use ($start_date,$end_date) {
                        return Yii::$app->appHelperFunctions->wmFormate(Yii::$app->appHelperFunctions->getCalculateAvgVals2($model->service_officer_name,$start_date,$end_date));

                    },
                    'footer' => '<br>'.number_format($_SESSION['recom_time']/$_SESSION['count_val'],2),

                ],


                [
                    'attribute' => 'tat',
                    'label' => Yii::t('app', 'TAT / Valuation'),
                    'headerOptions' => ['style' => 'color:#017BFE;' , 'class' => 'sortable'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) use ($start_date,$end_date) {
                        return Yii::$app->appHelperFunctions->wmFormate(Yii::$app->appHelperFunctions->getCalculateAvgVals3($model->service_officer_name,$start_date,$end_date));
                    },
                    'footer' => '<br>'.number_format($_SESSION['tat_time']/$_SESSION['count_val'],2),
                ],

                [
                    'attribute' => 'Valuation/Working Day',
                    'label' => Yii::t('app', 'Valuation / Working Day'),
                    'headerOptions' => ['style' => 'color:#017BFE;' , 'class' => 'sortable'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) use ($start_date,$end_date) {
                        return Yii::$app->appHelperFunctions->wmFormate(Yii::$app->appHelperFunctions->getCalculateAvgVals4($model->service_officer_name,$start_date,$end_date));
                    },
                    'footer' => '<br>'.number_format($_SESSION['working_time']/$_SESSION['count_val'],2),
                ],


                [
                    'attribute' => 'client_reminders',
                    'label' => Yii::t('app', 'Client Reminders'),
                    'headerOptions' => ['style' => 'color:#017BFE;' , 'class' => 'sortable'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) {
                        return $model->client_business;
                    },
                    'footer' => $_SESSION['client_business'].'<br>'.(Yii::$app->appHelperFunctions->wmFormate($_SESSION['client_business']/$_SESSION['count_val'])),
                ],
                [
                    'attribute' => 'low_vals',
                    'label' => Yii::t('app', 'Low Valuations'),
                    'headerOptions' => ['style' => 'color:#017BFE;' , 'class' => 'sortable'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) {
                        return $model->low_vals;
                    },
                    'footer' => $_SESSION['low_valuations'].'<br>'.(Yii::$app->appHelperFunctions->wmFormate($_SESSION['low_valuations']/$_SESSION['count_val'])),
                ],

                [
                    'attribute' => 'high_vals',
                    'label' => Yii::t('app', 'High Valuations'),
                    'headerOptions' => ['style' => 'color:#017BFE;' , 'class' => 'sortable'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) {
                        return $model->high_vals;
                    },
                    'footer' => $_SESSION['high_valuations'].'<br>'.(Yii::$app->appHelperFunctions->wmFormate($_SESSION['high_valuations']/$_SESSION['count_val'])),
                ],
                [
                    'attribute' => 'errors_vals',
                    'label' => Yii::t('app', 'Errors Valuations'),
                    'headerOptions' => ['style' => 'color:#017BFE;' , 'class' => 'sortable'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) {
                        return $model->errors_vals;
                    },
                    'footer' => $_SESSION['error_valuations'].'<br>'.(Yii::$app->appHelperFunctions->wmFormate($_SESSION['error_valuations']/$_SESSION['count_val'])),
                ],

                [
                    'attribute' => 'rating',
                    'label' => Yii::t('app', 'Rating'),
                    'headerOptions' => ['style' => 'color:#017BFE;' , 'class' => 'sortable'],
                    'contentOptions' => ['style' => 'text-align:right;'],
                    'footerOptions' => ['style' => 'text-align:right;'],
                    'value' => function ($model) {
                        $valuations = $model->total_valuations;
                        $inspecions = $model->total_inspections;

                        $valuer_target = $model->total_valuations*2500;
                        $valuer_earned = $model->client_revenue;

                        $total_fee = $valuer_earned-$valuer_target;
                        $final_fee = $total_fee/2500;

                        $positive_percentage = 3;
                        $val_percentage = ($positive_percentage / 100) * $valuations;
                        $insp_percentage = ($positive_percentage / 100) * $inspecions;
                        $rev_percentage = ($positive_percentage / 100) * $final_fee;

                        $positive_weightage = $val_percentage + $insp_percentage + $rev_percentage;

                        $error_rem_percentage = 0.5;
                        $low_high_percentage = 1;
                        $error_percentage = ($error_rem_percentage / 100) * $model->errors_vals;
                        $reminders_percentage = ($error_rem_percentage / 100) * $model->client_business;
                        $low_percentage = ($low_high_percentage / 100) * $model->low_vals;
                        $high_percentage = ($low_high_percentage / 100) * $model->high_vals;

                        $negative_weightage = $reminders_percentage + $low_percentage + $high_percentage + $error_percentage;

                        $rating_sum += ($positive_weightage - $negative_weightage);
                        $rating_sum = $_SESSION['rating'];

                        $footer_val = number_format($rating_sum/$_SESSION['count_val']);
                        $_SESSION['rating_final'] = $footer_val;

                        return number_format($positive_weightage - $negative_weightage,2);
                    },
                    'footer' => '<br>'. Yii::$app->appHelperFunctions->wmFormate($_SESSION['rating_final']) ,
                ],
            ],

        ]);
        ?>
        <?php CustomPjax::end(); ?>
    </div>

<?php

//  echo "<pre>"; print_r($rating);die;
// echo "<pre>"; print_r($_SESSION);

unset($_SESSION['total_revenue']);
unset($_SESSION['total_valuations']);
unset($_SESSION['total_inspections']);
unset($_SESSION['client_business']);
unset($_SESSION['low_valuations']);
unset($_SESSION['high_valuations']);
unset($_SESSION['count_val']);
unset($_SESSION['error_valuations']);
unset($_SESSION['rating']);
// unset($_SESSION['rating_final']);





$this->registerJs('
$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});
 $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });

    $(".sortable").click(function () {
        var index = $(this).index();
        var rows = $("#sortable-table tbody tr").get();
        rows.sort(function (a, b) {
            var aValue = $(a).children("td").eq(index).text();
            var bValue = $(b).children("td").eq(index).text();

        //   return $.isNumeric(aValue) && $.isNumeric(bValue) ?
        //   aValue - bValue :
        //   aValue.localeCompare(bValue);

            return $.isNumeric(bValue) && $.isNumeric(aValue) ?
            bValue - aValue :
            bValue.localeCompare(aValue);
        });

        $.each(rows, function (index, row) {
            $("#sortable-table tbody").append(row);
        });
    });

    $( document ).ready(function() {
        var index = $(this).index();
        var rows = $("#sortable-table tbody tr").get();
        rows.sort(function (a, b) {
            var aValue = $(a).children("td").eq(index).text();
            var bValue = $(b).children("td").eq(index).text();

        //   return $.isNumeric(aValue) && $.isNumeric(bValue) ?
        //   aValue - bValue :
        //   aValue.localeCompare(bValue);

            return $.isNumeric(bValue) && $.isNumeric(aValue) ?
            bValue - aValue :
            bValue.localeCompare(aValue);
        });

        $.each(rows, function (index, row) {
            $("#sortable-table tbody").append(row);
        });
    });




');
?>