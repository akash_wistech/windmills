
<?php

use app\models\User;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


$propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$propertiesArr = ['' => 'select'] + $propertiesArr;

$communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$communitiesArr = ['' => 'select'] + $communitiesArr;

$citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
$citiesArr = ['' => 'select'] + $citiesArr;


$ref_no = '65px';
$instruction_date_width = '70px';
$val_created = '70px';
$insp_created = '70px';
$insp_schd = '70px';
$insp_done = '70px';
$recom = '70px';
$app_tat = '75px';
$tot_vat = '60px';
$valuer_width = '45px';
$io_width = '40px';
$approver_width = '40px';

$this->title = Yii::t('app', $type);
$cardTitle = Yii::t('app', 'Today TAT Performance');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
$actionBtns = '';
$createBtn = false;
$import = false;
?>

<style>
    /* .dataTable th {*/
    /*    color: #0056b3;*/
    /*    font-size: 14px;*/
    /*    text-align: right !important;*/
    /*    padding-right: 30px !important;*/
    /*}*/
    /*.dataTable td {*/
    /*    font-size: 16px;*/
    /*    text-align: right;*/
    /*    padding-right: 50px;*/
    /*}*/
    /*.content-header h1 {*/
    /*       font-size: 16px !important;*/

    /*    }*/
    /*.content-header .row {*/
    /*    margin-bottom: 0px !important;*/
    /*}*/
    /*.content-header {*/
    /*    padding: 4px !important;*/
    /*} */



    .dataTable th {
        color: #0056b3;
        font-size: 14px;
        text-align: left !important;
        /* padding-right: 30px !important; */
    }
    .dataTable td {
        font-size: 14px;
        text-align: left;
        /* padding-right: 50px; */
        padding-top: 3px;
        max-width: 100px;
    }


    .content-header h1 {
        font-size: 16px !important;

    }

    .content-header .row {
        margin-bottom: 0px !important;
    }

    .content-header {
        padding: 0px !important;
    }

    th:nth-child(1) {
        min-width: 50px;
        max-width: 50px;
    }
    th:nth-child(2) {
        min-width: 70px;
        max-width: 70px;
    }
    th:nth-child(3) {
        min-width: 68px;
        max-width: 68px;
    }

    th:nth-child(4) {
        min-width: 68px;
        max-width: 68px;
    }
    th:nth-child(5) {
        min-width: 60px;
        max-width: 60px;
    }
    th:nth-child(6) {
        min-width: 68px;
        max-width: 68px;
    }
    th:nth-child(7) {
        min-width: 60px;
        max-width: 60px;
    }
    th:nth-child(8) {
        min-width: 60px;
        max-width: 60px;
    }

    th:nth-child(9) {
        min-width: 57px;
        max-width: 57px;
    }
    th:nth-child(10) {
        min-width: 40px;
        max-width: 40px;
    }
    th:nth-child(11) {
        min-width: <?= $io_width ?>;
        max-width: <?= $io_width ?>;
    }
    th:nth-child(12) {
        min-width: 40px;
        max-width: 40px;
    }

</style>

<?php
// echo "<pre>";
// print_r($model);
// echo "<pre>";die;
?>

<div class="card card-outline card-warning mx-2">
    <div class="card-body search_filter">
        <div class="col-3">
            <div class="form-group">
                <label> General Search : </label>
                <input type="search" id="custom-search-input-text" placeholder="Search from datatable" class="form-control custom-search-input-text">
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="comm-id" value='<?php echo $page_title; ?>'>
<div class="bank-revenue-index col-12">
    <div class="card card-outline card-info">
        <div class="card-body">
            <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
            <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                <thead>
                <tr>
                    <th class="">Ref No.
                        <input type="text" class="custom-search-input form-control" placeholder="ref#">
                    </th>

                    <th class="">Instruction Date
                        <input id="intruction_date" type="date" class="form-control" placeholder="date">
                    </th>

                    <th class="">Inspection Created
                        <input id="inspection_created" type="date" class="form-control" placeholder="date">
                    </th>

                    <th class="">Inspection Schedule
                        <input id="inspection_schedule" type="date" class="form-control" placeholder="date">
                    </th>

                    <th class="">Inspector Arrival
                        <input id="inspection_schedule" type="date" class="form-control" placeholder="date">
                    </th>

                    <th class="">Inspection Done
                        <input id="inspection_done" type="date" class="form-control" placeholder="date">
                    </th>

                    <th class="">Recommended
                        <input id="recom_date" type="date" class="form-control" placeholder="date">
                    </th>

                    <th class="">Approval TAT
                        <input id="approval_tat" type="date" class="form-control" placeholder="date">
                    </th>

                    <th class="">Total TAT
                        <input type="text" class="custom-search-input form-control" placeholder="TAT">
                    </th>

                    <th class="">Valuer
                        <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                    </th>

                    <th class="">Inspector
                        <input type="text" class="custom-search-input form-control" placeholder="I/O">
                    </th>

                    <th class="">Approver
                        <input type="text" class="custom-search-input form-control" placeholder="Approver">
                    </th>

                    <?php
                    if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <th>Action</th>
                        <?php
                    }
                    ?>

                </tr>
                </thead>
                <tbody>
                <?php
                if(count($dataProvider->getModels())>0){
                    // dd($dataProvider->getModels());
                    foreach($dataProvider->getModels() as $model){
                        // dd($model->valuationDetail->arrived_date_time);
                        ?>
                        <tr class="active">

                            <!-- Reference Number -->
                            <td>
                                <?= $model->reference_number ?>
                            </td>

                            <!-- Instruction -->
                            <td>
                                <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                                <br>
                                <?= Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time) ?>
                            </td>

                            <!-- Inspection Created -->
                            <td>
                                <?php
                                if($model->inspection_type != 3) {
                                    if($model->scheduleInspection->created_at != '' && $model->instruction_date !='' && $model->instruction_time !='') {

                                        $startDate = $model->instruction_date . ' ' .$model->instruction_time. ":00";
                                        $endDate = $model->scheduleInspection->created_at;

                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);

                                        $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                                        if($model->reference_number == 'REV-2023-10737'){
                                            $workingHours = $workingHours - 8.5;
                                        }
                                    }else{
                                        $workingHours = 0;
                                    }

                                    echo date('d-M-Y', strtotime($model->scheduleInspection->created_at)) . '<br>  ' . date('h:i A', strtotime($model->scheduleInspection->created_at)).'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';

                                }else{
                                    echo 'Desktop Valuation';
                                }
                                ?>
                            </td>

                            <!-- Inspection Schedule -->
                            <td>
                                <?php
                                if($model->inspection_type != 3) {
                                    $inspection_time = $model->inspection_time . ":00";
                                    if ($model->inspection_time == null) {
                                        $inspection_time = '00:00:00';
                                    }
                                    if($model->scheduleInspection->created_at != '' && $model->scheduleInspection->inspection_date !='') {

                                        $minus = 0;

                                        $startDate = $model->scheduleInspection->created_at; //inspection created
                                        $endDate = $model->scheduleInspection->inspection_date.' '.$inspection_time;

                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);

                                        $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                                    }else{
                                        $workingHours_1 = 0;
                                    }
                                    echo date('d-M-Y', strtotime($model->scheduleInspection->inspection_date)) . '<br>  ' . Yii::$app->appHelperFunctions->formatTimeAmPm($model->scheduleInspection->inspection_time).'<br>'.'<strong>'.$workingHours_1.' Hours'.'</strong>';

                                }else{
                                    echo 'Desktop Valuation';
                                }
                                ?>
                            </td>

                            <!-- Inspector Arrival Time -->
                            <td>
                                <?php
                                if($model->inspection_type != 3) {
                                    $inspection_time = $model->inspection_time . ":00";
                                    if ($model->inspection_time == null) {
                                        $inspection_time = '00:00:00';
                                    }
                                    if($model->scheduleInspection->created_at != '' && $model->scheduleInspection->inspection_date !='') {

                                        $minus = 0;

                                        $startDate = $model->scheduleInspection->inspection_date.' '.$inspection_time;
                                        $endDate = $model->valuationDetail->arrived_date_time;

                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);

                                        // dd($startDateTime);

                                        $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                                    }else{
                                        $workingHours_1 = 0;
                                    }
                                    echo date('d-M-Y', strtotime($model->valuationDetail->arrived_date_time)) . '<br>  ' . date('h:i A', strtotime($model->valuationDetail->arrived_date_time)) .'<br>'.'<strong>'.$workingHours_1.' Hours'.'</strong>';

                                }else{
                                    echo 'Desktop Valuation';
                                }
                                ?>
                            </td>

                            <!-- Inspection Done -->
                            <td>
                                <?php
                                if($model->inspection_type != 3) {

                                    $inspection_time = $model->scheduleInspection->inspection_time . ":00";
                                    if ($model->scheduleInspection->inspection_time == null) {
                                        $inspection_time = '00:00:00';
                                    }
                                    if($model->inspection_done_date_time != '' && $model->scheduleInspection->inspection_date !='') {
                                        $minus = 0;

                                        //  $startDate = $model->scheduleInspection->created_at;
                                        $startDate = $model->valuationDetail->arrived_date_time;
                                        $endDate = $model->inspection_done_date_time;

                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);

                                        $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    }else{
                                        $workingHours_2 = 0;
                                    }
                                    echo date('d-M-Y', strtotime($model->inspection_done_date_time)) . '<br>  ' . date('h:i A', strtotime($model->inspection_done_date_time)).'<br>'.'<strong>'.$workingHours_2.' Hours'.'</strong>';
                                }else{
                                    echo 'Desktop Valuation';
                                }
                                ?>
                            </td>

                            <!-- Recommended -->
                            <td>
                                <?php
                                if($model->inspection_done_date_time != '' && $model->valuerData->created_at!='') {

                                    $minus = 0;

                                    $startDate = $model->inspection_done_date_time;
                                    $endDate = $model->valuerData->created_at;


                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                                }else{
                                    $workingHours_2 = 0;
                                }
                                echo date('d-M-Y', strtotime($model->valuerData->created_at)) . '<br>  ' . date('h:i A', strtotime($model->valuerData->created_at)).'<br>'.'<strong>'.$workingHours_2.' Hours'.'</strong>';
                                ?>
                            </td>

                            <!-- Approval TAT -->
                            <td>
                                <?php
                                if($model->valuerData->created_at != '' && $model->valuerData->created_at!='') {

                                    $minus = 0;

                                    $startDate = $model->valuerData->created_at;
                                    $endDate = $model->submission_approver_date;


                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                                }else{
                                    $workingHours_2 = 0;
                                }

                                echo date('d-M-Y', strtotime($model->submission_approver_date)).'<br>  '.date('h:i A',strtotime($model->submission_approver_date)).'<br>'.'<strong>'.$workingHours_2.' Hours'.'</strong>';
                                ?>
                            </td>

                            <!-- Total TAT -->
                            <td>
                                <?php
                                if($model->inspection_type != 3) {
                                    $inspection_time = $model->inspection_time . ":00";
                                    if ($model->inspection_time == null) {
                                        $inspection_time = '00:00:00';
                                    }
                                    if($model->instruction_date !='' && $model->inspection_date !='') {

                                        $startDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                        $endDate = $model->inspection_date . ' ' . $inspection_time;

                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);
                                        $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    }else{
                                        $workingHours_1 =0;
                                    }
                                    // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                    //  $time_1 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1));
                                    //  $time_1_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));
                                    if($model->inspection_date !='' && $model->submission_approver_date !='') {
                                        $startDate_2 = $model->inspection_date . ' ' . $inspection_time;
                                        $endDate_2 = $model->submission_approver_date;
                                        $startDateTime = new DateTime($startDate_2); // Start date and time
                                        $endDateTime = new DateTime($endDate_2);
                                        /*echo $startDate_2.' '.$endDate_2;
                                        die;*/
                                        $workingHours_2 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    }else{
                                        $workingHours_2=0;
                                    }
                                    // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                    //  $time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                    // $time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));



                                    // print_r( abs( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate ), 2) ) ); echo "<br>";
                                    // die;
                                    $workingHours = $workingHours_1 + $workingHours_2;
                                    if($model->reference_number == 'REV-2023-10737' || $model->reference_number == 'REV-2023-10756'){
                                        $workingHours = $workingHours - 8.5;
                                    }
                                    $test = abs(number_format(($workingHours/8.5),2)).'<br>'.'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';
                                    echo $test;

                                }else{
                                    $startDate_2 = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                    $endDate_2 = $model->submission_approver_date;
                                    $startDateTime = new DateTime($startDate_2); // Start date and time
                                    $endDateTime = new DateTime($endDate_2);
                                    // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                    //$time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                    //$time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));
                                    if($model->instruction_date !='' && $model->submission_approver_date !='') {
                                        $startDateTime = new DateTime($startDate_2); // Start date and time
                                        $endDateTime = new DateTime($endDate_2);
                                        $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        if($model->reference_number == 'REV-2023-10772' || $model->reference_number == 'REV-2023-10756'){
                                            $workingHours = $workingHours - 8.5;
                                        }
                                        $test = abs(number_format(($workingHours/8.5),1)).'<br>'.'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';
                                        echo $test;
                                    }else{
                                        echo '';
                                    }

                                }
                                ?>
                            </td>

                            <!-- Valuer -->
                            <td>
                                <?php
                                echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                ?>
                            </td>

                            <!-- Inspection Officer -->
                            <td>
                                <?php
                                if($model->inspection_type != 3) {
                                    echo (isset($model->inspection_officer_name) && ($model->inspection_officer_name <> null))? (Yii::$app->appHelperFunctions->staffMemberListArrLastName[$model->scheduleInspection->inspection_officer]): '';
                                }else{
                                    echo "Desktop Valuation";
                                }
                                ?>
                            </td>

                            <!-- Approver -->
                            <td>
                                <?php
                                $type = $model->approverData->approver_type;
                                if($type == "approver")
                                {
                                    $get_name = User::find()->where(['id' => $model->approverData->created_by])->one();
                                    echo $get_name->lastname ;
                                }else{
                                    echo '';
                                }
                                ?>
                            </td>

                            <?php
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                                ?>
                                <td class="noprint actions">
                                    <div class="btn-group flex-wrap">
                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <a class="dropdown-item text-1"
                                               href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                               title="Step 1" data-pjax="0">
                                                <i class="fas fa-edit"></i> View
                                            </a>
                                        </div>
                                    </div>
                                </td>
                                <?php
                            }
                            ?>

                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
                <tfoot>
                <th>Average:</th>
                <th></th>
                <th><?= number_format(($diff_1/$total_rows),1).' Hours' ?></th>
                <!-- <th><?= number_format(($diff_2/$total_rows),1).' Hours' ?></th> -->
                <th><?= number_format(($diff_3/$total_rows),1).' Hours' ?></th>
                <th><?= number_format(($diff_10/$total_rows),1).' Hours' ?></th>
                <th><?= number_format(($diff_4/$total_rows),1).' Hours' ?></th>
                <th><?= number_format(($diff_5/$total_rows),1).' Hours' ?></th>
                <th><?= number_format(($diff_6/$total_rows),1).' Hours' ?></th>

                <th><?= number_format(($diff_9/$total_rows),1).' Hours' ?></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.css" />
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

<?php
$this->registerJs('
     
     
    let table = new DataTable("#bank-revenue", {
        responsive: true,
        order: [[0, "desc"]],
        dom:"lrtip",
        pageLength: 50,
    });


    $(".custom-search-input").on("keyup", function () {
        table.search(this.value).draw();
      });
      
      $(".custom-search-input-text").on("keyup", function () {
        table.search(this.value).draw();
      });
      $(".custom-search-input").on("change", function () {
        table.search(this.value).draw();
      });
      
      $(".custom-search-input-client").on("change", function () {
            $.ajax({
            url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
            method: "post",
            dataType: "html",
            success: function(data) {
                console.log(data);
                table.search(data).draw();
            },
            error: bbAlert
            });
        });
            
      $("#bank-revenue_filter").css({
        "display":"none",
      });

      $(".custom-search-input").on("click", function (event) {
        event.stopPropagation();
      });
      $(".custom-search-input-client").on("click", function (event) {
        event.stopPropagation();
      });

    //for intruction_date search
    $("#intruction_date").on("change", function () {
    $(".custom-search-input").val("");
    var format1 = moment($("#intruction_date").val()).format("DD-MMM-YYYY"); 
    table.search(format1).draw();
    });

    //for inspection_created search
    $("#inspection_created").on("change", function () {
    $(".custom-search-input").val("");
    var format1 = moment($("#inspection_created").val()).format("DD-MMM-YYYY"); 
    table.search(format1).draw();
    });

    //for inspection_schedule search
    $("#inspection_schedule").on("change", function () {
    $(".custom-search-input").val("");
    var format1 = moment($("#inspection_schedule").val()).format("DD-MMM-YYYY"); 
    table.search(format1).draw();
    });

    //for inspection_done search
    $("#inspection_done").on("change", function () {
    $(".custom-search-input").val("");
    var format1 = moment($("#inspection_done").val()).format("DD-MMM-YYYY"); 
    table.search(format1).draw();
    });

    //for recom_date search
    $("#recom_date").on("change", function () {
    $(".custom-search-input").val("");
    var format1 = moment($("#recom_date").val()).format("DD-MMM-YYYY"); 
    table.search(format1).draw();
    });

    //for approval_tat search
    $("#approval_tat").on("change", function () {
    $(".custom-search-input").val("");
    var format1 = moment($("#approval_tat").val()).format("DD-MMM-YYYY"); 
    table.search(format1).draw();
    });



    

      


');
?>