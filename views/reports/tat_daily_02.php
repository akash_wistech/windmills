
<?php 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


$propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$propertiesArr = ['' => 'select'] + $propertiesArr;

$communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$communitiesArr = ['' => 'select'] + $communitiesArr;

$citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
$citiesArr = ['' => 'select'] + $citiesArr;


$ref_no = '90px';
$instruction_date_width = '90px'; 
$val_created = '90px';
$insp_created = '90px';
$insp_schd = '90px'; 
$insp_done = '90px';
$recom = '90px';
$app_tat = '90px';
$inst_insp = '90px';  
$insp_sub = '90px'; 
$tot_vat = '90px';
$valuer_width = '90px';
$io_width = '90px';
$approver_width = '90px';

$this->title = Yii::t('app', $type);
$cardTitle = Yii::t('app', 'Today TAT Performance');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
$actionBtns = '';
$createBtn = false;
$import = false;
?>

<style>
/* .dataTable th {*/
/*    color: #0056b3;*/
/*    font-size: 14px;*/
/*    text-align: right !important;*/
/*    padding-right: 30px !important;*/
/*}*/
/*.dataTable td {*/
/*    font-size: 16px;*/
/*    text-align: right;*/
/*    padding-right: 50px;*/
/*}*/
/*.content-header h1 {*/
/*       font-size: 16px !important;*/

/*    }*/
/*.content-header .row {*/
/*    margin-bottom: 0px !important;*/
/*}*/
/*.content-header {*/
/*    padding: 4px !important;*/
/*} */



.dataTable th {
      color: #0056b3;
      font-size: 15px;
      text-align: left !important;
      /* padding-right: 30px !important; */
  }
  .dataTable td {
      font-size: 16px;
      text-align: left;
      /* padding-right: 50px; */
      padding-top: 3px;
  }

.content-header h1 {
    font-size: 16px !important;

}

.content-header .row {
    margin-bottom: 0px !important;
}

.content-header {
    padding: 0px !important;
}

    th:nth-child(1) {
      min-width: <?= $ref_no ?> ;
    }
    th:nth-child(2) {
      min-width: <?= $instruction_date_width ?>;
      max-width: <?= $instruction_date_width ?>;
    }
    th:nth-child(3) {
      min-width: <?= $val_created ?>;
      max-width: <?= $val_created ?>;
    }

    th:nth-child(4) {
      min-width: <?= $insp_created ?>;
      max-width: <?= $insp_created ?>;
    }
    th:nth-child(5) {
      min-width: <?= $insp_schd ?>;
      max-width: <?= $insp_schd ?>;
    }
    th:nth-child(6) {
      min-width: <?= $insp_done ?>;
      max-width: <?= $insp_done ?>;
    }
    th:nth-child(7) {
      min-width: <?= $recom ?>;
      max-width: <?= $recom ?>;
    }
    th:nth-child(8) {
      min-width: <?= $app_tat ?>;
      max-width: <?= $app_tat ?>;
    }
    th:nth-child(9) {
      min-width: <?= $inst_insp ?>;
      max-width: <?= $inst_insp ?>;
    }
    th:nth-child(10) {
      min-width: <?= $insp_sub ?>;
      max-width: <?= $insp_sub ?>;
    }
    th:nth-child(11) {
      min-width: <?= $tot_vat ?>;
      max-width: <?= $tot_vat ?>;
    }
    th:nth-child(12) {
      min-width: <?= $valuer_width ?>;
      max-width: <?= $valuer_width ?>;
    }
    th:nth-child(13) {
      min-width: <?= $io_width ?>;
      max-width: <?= $io_width ?>;
    }
    th:nth-child(14) {
      min-width: <?= $approver_width ?>;
      max-width: <?= $approver_width ?>;
    }
</style>

<?php
// echo "<pre>";
// print_r($model);
// echo "<pre>";die;
?>
<input type="hidden" id="comm-id" value='<?php echo $page_title; ?>'>
<div class="bank-revenue-index col-12">
    <div class="card card-outline card-info">
        <div class="card-body">
        <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
            <table id="bank-revenue" class="table table-bordered table-striped dataTable table-responsive">
                <thead>
                    <tr>
                        <th class="">Reference Number
                            <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                        </th>
                 
                        <th class="">Instruction Date
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Valuation Created
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Created
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Schedule
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Done
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Recommended
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Approval TAT
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Instruction - Inspection
                          <input type="text" class="custom-search-input form-control" placeholder="Inst-Insp">
                        </th>

                        <th class="">Inspection - Submission
                            <input type="text" class="custom-search-input form-control" placeholder="Insp-Subm">
                        </th>
                        
                        <th class="">Total VAT
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>

                        <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th>

                        <th class="">Inspection Officer
                            <input type="text" class="custom-search-input form-control" placeholder="I/O">
                        </th>

                        <th class="">Approver
                            <input type="text" class="custom-search-input form-control" placeholder="Approver">
                        </th>

                        <?php 
                          if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <th>Action</th>
                        <?php
                          }
                        ?>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd($dataProvider->getModels());
                        foreach($dataProvider->getModels() as $model){
                    ?>
                    <tr class="active">

                        <!-- Reference Number -->
                        <td>
                            <?= $model->reference_number ?>
                        </td>

                        <!-- Instruction -->
                        <td>
                            <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                            <br>
                            <?= Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time) ?>
                        </td>

                        <!-- Valuation Created -->
                        <td>
                            <?php
                                if($model->instruction_date != '' && $model->created_at !='') {

                                    $startDate = $model->created_at;
                                    $endDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
            
            
                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);
            
                                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
            
                                }else{
                                    $workingHours = 0;
                                }
                                echo date('d-M-Y', strtotime($model->created_at)).'<br>  '.date('h:i A',strtotime($model->created_at)).'<br>'.'<strong>'.$workingHours.'</strong>'.' Hour(s)';
                            ?>
                        </td>

                        <!-- Inspection Created -->
                        <td>
                            <?php
                                if($model->inspection_type != 3) {

                                    if($model->scheduleInspection->created_at != '' && $model->created_at !='') {
            
                                        $startDate = $model->created_at;
                                        $endDate = $model->scheduleInspection->created_at;
            
            
                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);
            
                                        $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        if($model->reference_number == 'REV-2023-10737'){
                                            $workingHours = $workingHours - 8.5;
                                        }
                                    }else{
                                        $workingHours = 0;
                                    }

                                    echo date('d-M-Y', strtotime($model->scheduleInspection->created_at)) . '<br>  ' . date('h:i A', strtotime($model->scheduleInspection->created_at)).'<br>'.'<strong>'.$workingHours.'</strong>'.' Hour(s)';
            
                                }else{
                                    echo '';
                                }
                            ?>
                        </td>

                        <!-- Inspection Schedule -->
                        <td>
                            <?php
                                if($model->inspection_type != 3) {
                                    $inspection_time = $model->inspection_time . ":00";
                                    if ($model->inspection_time == null) {
                                        $inspection_time = '00:00:00';
                                    }
                                    if($model->scheduleInspection->created_at != '' && $model->scheduleInspection->inspection_date !='') {
            
                                        $minus = 0;
            
                                        $startDate = $model->scheduleInspection->created_at;
                                        $endDate = $model->scheduleInspection->inspection_date.' '.$inspection_time;
            
            
                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);
            
                                        $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        if($model->reference_number == 'REV-2023-10772'){
                                            $workingHours_1 = $workingHours_1 - 8.5;
                                        }
            
            
                                    }else{
                                        $workingHours_1 = 0;
                                    }

                                    echo date('d-M-Y', strtotime($model->scheduleInspection->inspection_date)) . '<br>  ' . Yii::$app->appHelperFunctions->formatTimeAmPm($model->scheduleInspection->inspection_time).'<br>'.'<strong>'.$workingHours_1.'</strong>'.' Hour(s)';
                                }else{
                                   echo '';
                                }
                            ?>
                        </td>

                        <!-- Inspection Done -->
                        <td>
                            <?php
                                if($model->inspection_type != 3) {

                                    $inspection_time = $model->inspection_time . ":00";
                                    if ($model->inspection_time == null) {
                                        $inspection_time = '00:00:00';
                                    }
                                    if($model->inspection_done_date_time != '' && $model->scheduleInspection->inspection_date !='') {
                                        $minus = 0;
            
                                        //  $startDate = $model->scheduleInspection->created_at;
                                        $startDate = $model->scheduleInspection->inspection_date.' '.$inspection_time;
                                        $endDate = $model->inspection_done_date_time;
            
                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);
            
                                        $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        /* if($model->reference_number == 'REV-2023-10772'){
                                            $workingHours = $workingHours - 8.5;
                                        }*/
                                    }else{
                                        $workingHours_2 = 0;
                                    }
                                    echo date('d-M-Y', strtotime($model->inspection_done_date_time)) . '<br>  ' . date('h:i A', strtotime($model->inspection_done_date_time)).'<br>'.'<strong>'.$workingHours_2.'</strong>'.' Hour(s)';
                                }else{
                                    echo '';
                                }
                            ?>
                        </td>
                                
                        <!-- Recommended -->
                        <td>
                            <?php
                                if($model->inspection_done_date_time != '' && $model->valuerData->created_at!='') {

                                    $minus = 0;

                                    $startDate = $model->inspection_done_date_time;
                                    $endDate = $model->valuerData->created_at;


                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);

                                    $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    if($model->reference_number == 'REV-2023-10756'){
                                        $workingHours_2 = $workingHours_2 - 8.5;
                                    }


                                }else{
                                    $workingHours_2 = 0;
                                }

                                echo date('d-M-Y', strtotime($model->valuerData->created_at)) . '<br>  ' . date('h:i A', strtotime($model->valuerData->created_at)).'<br>'.'<strong>'.$workingHours_2.'</strong>'.' Hour(s)';
                            ?>
                        </td>

                        <!-- Approval TAT -->
                        <td>
                            <?php
                                if($model->valuerData->created_at != '' && $model->valuerData->created_at!='') {

                                    $minus = 0;
            
            
            
                                    $startDate = $model->valuerData->created_at;
                                    $endDate = $model->submission_approver_date;
            
            
                                    $startDateTime = new DateTime($startDate); // Start date and time
                                    $endDateTime = new DateTime($endDate);
            
                                    $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    /* if($model->reference_number == 'REV-2023-10756'){
                                        $workingHours_2 = $workingHours_2 - 8.5;
                                    }*/
            
            
                                }else{
                                    $workingHours_2 = 0;
                                }
            
                                echo date('d-M-Y', strtotime($model->submission_approver_date)).'<br>  '.date('h:i A',strtotime($model->submission_approver_date)).'<br>'.'<strong>'.$workingHours_2.'</strong>'.' Hour(s)';
                            ?>
                        </td>

                        <!-- Instruction-Inspection -->
                        <td>
                            <?php
                                if($model->inspection_type != 3) {
                                    $inspection_time = $model->inspection_time . ":00";
                                    if ($model->inspection_time == null) {
                                        $inspection_time = '00:00:00';
                                    }

                                    if($model->instruction_date != '' && $model->inspection_date !='') {
                                        $startDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                        $endDate = $model->inspection_date . ' ' . $inspection_time;
            
            
                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);
            
                                        $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        if($model->reference_number == 'REV-2023-10737' || $model->reference_number == 'REV-2023-10756'){
                                            $workingHours = $workingHours - 8.5;
                                        }
                                    }else{
                                        $workingHours = 0;
                                    }
            
                                    // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                    //   $test = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1)) . ' <br> ' . abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));
                                    $test = abs(number_format(($workingHours/8.5),2)).'<br>'.'<strong>'.abs($workingHours).'</strong>'.' Hour(s)';
                                    echo $test;
                                }else{
                                    echo '';
                                }
                            ?>
                        </td>

                        <!-- Inspection-Submission -->
                        <td>
                            <?php
                                if($model->inspection_type != 3) {
                                    $inspection_time = $model->inspection_time . ":00";
                                    if ($model->inspection_time == null) {
                                        $inspection_time = '00:00:00';
                                    }
            
                                    $startDate = $model->inspection_date . ' ' . $inspection_time;
                                    $endDate = $model->submission_approver_date;
            
                                    if($model->inspection_date !='' && $model->submission_approver_date !=''){
                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);
                                        $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        /* if($model->reference_number == 'REV-2023-10772' || $model->reference_number == 'REV-2023-10756'){
                                            $workingHours = $workingHours - 8.5;
                                        }*/
                                    }else{
                                        $workingHours=0;
                                    }
            
                                    $test = abs(number_format(($workingHours/8.5),2)).'<br>'.'<strong>'.abs($workingHours).'</strong>'.' Hour(s)';
                                    echo $test;
            
                                    //return $workingHours;
                                }else{
                                    echo '';
                                }
                            ?>
                        </td>

                        <!-- Total VAT -->
                        <td>
                            <?php
                                if($model->inspection_type != 3) {

                                    $inspection_time = $model->inspection_time . ":00";
                                    if ($model->inspection_time == null) {
                                        $inspection_time = '00:00:00';
                                    }
            
                                    if($model->instruction_date !='' && $model->inspection_date !='') {
            
                                        $startDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                        $endDate = $model->inspection_date . ' ' . $inspection_time;
            
                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);
                                        $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                    }else{
                                        $workingHours_1 =0;
                                    }
                                    // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                    //  $time_1 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1));
                                    //  $time_1_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));
            
                                    if($model->inspection_date !='' && $model->submission_approver_date !='') {
                                        $startDate_2 = $model->inspection_date . ' ' . $inspection_time;
                                        $endDate_2 = $model->submission_approver_date;
                                        $startDateTime = new DateTime($startDate_2); // Start date and time
                                        $endDateTime = new DateTime($endDate_2);
                                        /*echo $startDate_2.' '.$endDate_2;
                                        die;*/
                                        $workingHours_2 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
            
                                    }else{
                                        $workingHours_2=0;
                                    }
                                    // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                    //  $time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                    // $time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));
            
            
            
                                    // print_r( abs( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate ), 2) ) ); echo "<br>";
                                    // die;
                                    $workingHours = $workingHours_1 + $workingHours_2;
                                    if($model->reference_number == 'REV-2023-10737' || $model->reference_number == 'REV-2023-10756'){
                                        $workingHours = $workingHours - 8.5;
                                    }
                                    $test = abs(number_format(($workingHours/8.5),2)).'<br>'.'<strong>'.abs($workingHours).'</strong>'.' Hour(s)';
                                    echo $test;
            
                                }else{
                                    $startDate_2 = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                    $endDate_2 = $model->submission_approver_date;
                                    $startDateTime = new DateTime($startDate_2); // Start date and time
                                    $endDateTime = new DateTime($endDate_2);
                                    // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                    //$time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                    //$time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));
                                    if($model->instruction_date !='' && $model->submission_approver_date !='') {
                                        $startDateTime = new DateTime($startDate_2); // Start date and time
                                        $endDateTime = new DateTime($endDate_2);
                                        $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        if($model->reference_number == 'REV-2023-10772' || $model->reference_number == 'REV-2023-10756'){
                                            $workingHours = $workingHours - 8.5;
                                        }
                                        $test = abs(number_format(($workingHours/8.5),2)).'<br>'.'<strong>'.abs($workingHours).'</strong>'.' Hour(s)';
                                        echo $test;
                                    }else{
                                        echo '';
                                    }
            
                                }
                            ?>
                        </td>

                        <!-- Valuer -->
                        <td>
                            <?php
                                echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                            ?>
                        </td>

                        <!-- Inspection Officer -->
                        <td>
                            <?php
                                echo (isset($model->inspection_officer_name) && ($model->inspection_officer_name <> null))? (Yii::$app->appHelperFunctions->staffMemberListArrLastName[$model->scheduleInspection->inspection_officer]): '';
                            ?>
                        </td>

                        <!-- Approver -->
                        <td>
                            <?php
                            echo $model->approverData->user->lastname ;
                            ?>
                        </td>

                        <?php 
                          if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <td class="noprint actions">
                            <div class="btn-group flex-wrap">
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                    data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a class="dropdown-item text-1"
                                        href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                        title="Step 1" data-pjax="0">
                                        <i class="fas fa-edit"></i> View 
                                    </a>
                                </div>
                            </div>
                        </td>
                        <?php
                          }
                        ?>

                    </tr>
                    <?php
                }
            }
        ?>
                </tbody>
                <tfoot>
                    <th>Average:</th>
                    <th></th>
                    <th><?= number_format(($diff_1/$total_rows),1).' Hour(s)' ?></th>
                    <th><?= number_format(($diff_2/$total_rows),1).' Hour(s)' ?></th>
                    <th><?= number_format(($diff_3/$total_rows),1).' Hour(s)' ?></th>
                    <th><?= number_format(($diff_4/$total_rows),1).' Hour(s)' ?></th>
                    <th><?= number_format(($diff_5/$total_rows),1).' Hour(s)' ?></th>
                    <th><?= number_format(($diff_6/$total_rows),1).' Hour(s)' ?></th>
                    <th><?= number_format(($diff_7/$total_rows),1).' Hour(s)' ?></th>
                    <th><?= number_format(($diff_8/$total_rows),1).' Hour(s)' ?></th>
                    <th><?= number_format(($diff_9/$total_rows),1).' Hour(s)' ?></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.css" />
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.js"></script>

<?php
    $this->registerJs('
     
     
    let table = new DataTable("#bank-revenue", {
        responsive: true,
        order: [[0, "desc"]],

    });

    // table
    // .on("order.dt search.dt", function () {
    //     var i = 0;
 
    //     table
    //         .cells(null, 0, { search: "applied", order: "applied" })
    //         .every(function (cell) {
    //             this.data(i++);
    //         });
    // })
    // .draw();

');
?>