<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use app\assets\QuotationBuildingAsset;
QuotationBuildingAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\ValuationQuotation */

$this->title = Yii::t('app', 'Quotations');
$this->params['breadcrumbs'][] = ['label' => 'Valuation Quotations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$properties = yii::$app->quotationHelperFunctions->getExistingProperties($model->id);
// echo "<pre>";
// print_r($properties);
// echo "</pre>";

?>
<!-- uppersection -->
<div class="valuation-quotation-view card card-primary card-outline">

  <div class="card-header">
    <h3 class="card-title">
      Client General Details
    </h3>
    <div class="card-tools">
      <a href="<?= Url::to(['update', 'id' => $model['id']]) ?>" class="btn btn-tool">
          <i class="fas fa-edit"></i>
      </a>
    </div>
  </div>

    <div class="card-body">
      <div class="row">
        <div class="col-4 my-2">
          <label><?=Yii::t('app', 'Reference Number')?></label>
          <input type="text" class="form-control"
            value="<?= $model->reference_number ?>" readonly="" maxlength="255" aria-invalid="false">
        </div>
        <div class="col-4 my-2">
          <label><?=Yii::t('app', 'Client Name')?></label>
          <input type="text" class="form-control"
            value="<?= $model->client->title ?>" readonly="" maxlength="255" aria-invalid="false">
        </div>
        <div class="col-4 my-2">
          <label><?=Yii::t('app', 'Client Type')?></label>
          <input type="text" class="form-control"
            value="<?= $model->client_type ?>" readonly="" maxlength="255" aria-invalid="false">
        </div>
        <div class="col-4 my-2">
          <label><?=Yii::t('app', 'Client Reference')?></label>
          <input type="text" class="form-control"
            value="<?= $model->client_reference ?>" readonly="" maxlength="255" aria-invalid="false">
        </div>
        <div class="col-4 my-2">
          <label><?=Yii::t('app', 'Client Customer Name')?></label>
          <input type="text" class="form-control"
            value="<?= $model->client_customer_name ?>" readonly="" maxlength="255" aria-invalid="false">
        </div>
        <div class="col-4 my-2">
          <label><?=Yii::t('app', 'Purpose Of Valuation')?></label>
          <input type="text"class="form-control"
            value="<?= Yii::$app->appHelperFunctions->getPurposeOfValuationArr()[$model->purpose_of_valuation] ?>" readonly="" maxlength="255" aria-invalid="false">
        </div>
        <div class="col-4 my-2">
          <label><?=Yii::t('app', 'Inquiry Date')?></label>
          <input type="text" class="form-control"
            value="<?= $model->inquiry_date ?>" readonly="" maxlength="255" aria-invalid="false">
        </div>
        <div class="col-4 my-2">
          <label><?=Yii::t('app', 'Expiry Date')?></label>
          <input type="text" class="form-control"
            value="<?= $model->expiry_date ?>" readonly="" maxlength="255" aria-invalid="false">
        </div>
        <div class="col-4 my-2">
          <label><?=Yii::t('app', 'Advance Payment Terms')?></label>
          <input type="text" class="form-control"
            value="<?= $model->advance_payment_terms ?>" readonly="" maxlength="255" aria-invalid="false">
        </div>
        <div class="col-4 my-2">
          <label><?=Yii::t('app', 'No Of Properties')?></label>
          <input type="text" class="form-control"
            value="<?= $model->no_of_properties ?>" readonly="" maxlength="255" aria-invalid="false">
        </div>
    </div>

  </div>
<!-- end upper section -->



<section class="valuation-quotation-form card card-info card-outline mx-4">

<header class="card-header">
    <h2 class="card-title">Check Conflict Of Interest - Related To Buyer</h2>
</header>
<div class="card-body">
    <div class="row">
        <div class="col">
          <label class="control-label" for="valuationquotation-related_to_buyer_reason">Related To Buyer</label>
          <input type="text" class="form-control"
                 id="valuationquotation-related_to_buyer"
                 name="valuationquotation[related_to_buyer]"
            value="<?= $model->related_to_buyer ?>" readonly="" maxlength="255" aria-invalid="false">
        </div>

      <div class="col-12" id="related_to_buyer_reason"  style="<?= ($model->related_to_buyer == 'Yes')? "": 'display:none;' ?>">
        <label class="control-label my-2" for="valuationquotation-related_to_buyer_reason">Related To Buyer Reason</label>
        <textarea id="valuationquotation-related_to_buyer_reason" class="form-control" name="ValuationQuotation[related_to_buyer_reason]" rows="6" readonly=""><?= $model->related_to_buyer_reason ?></textarea>



    <table class="table table-bordered my-2">
        <thead>
        <tr>
            <th>Reference Number</th>
            <th>Building Name</th>
            <th>Date</th>
        </tr>
        </thead>
        <tbody>

        <?php

        if($buyer_data <> null && !empty($buyer_data)) {
            foreach ($buyer_data as $buyer) {

                ?>
                <tr>
                    <td><?= $buyer->reference_number ?></td>
                    <td><?= $buyer->building->title ?></td>
                    <td><?= Yii::$app->formatter->asDate($buyer->date_submitted) ?></td>
                </tr>

                <?php
            }
        }?>

        </tbody>
    </table>
</div>

    </div>
</div>
</section>




<section class="valuation-quotation-form card card-info card-outline mx-4">

<header class="card-header">
    <h2 class="card-title">Check Conflict Of Interest - Related To Seller</h2>
</header>
<div class="card-body">
    <div class="row">
      <div class="col">
        <label class="control-label" for="valuationquotation-related_to_seller_reason">Related To Seller</label>
        <input type="text" class="form-control"
               id="valuationquotation-related_to_seller"
               name="valuationquotation[related_to_seller]"
          value="<?= $model->related_to_seller ?>" readonly="" maxlength="255" aria-invalid="false">

      </div>

    <div class="col-12" id="related_to_seller_reason"  style="<?= ($model->related_to_seller == 'Yes')? "": 'display:none;' ?>">
      <label class="control-label my-2" for="valuationquotation-related_to_seller_reason">Related To Seller Reason</label>
      <textarea id="valuationquotation-related_to_seller_reason" class="form-control" name="ValuationQuotation[related_to_seller_reason]" rows="6"readonly=""><?= $model->related_to_seller_reason ?></textarea>


        <table class="table table-bordered my-2">
            <thead>
            <tr>
                <th>Reference Number</th>
                <th>Building Name</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>

            <?php

            if($seller_data <> null && !empty($seller_data)) {
                foreach ($seller_data as $seller) {

                    ?>
                    <tr>
                        <td><?= $seller->reference_number ?></td>
                        <td><?= $seller->building->title ?></td>
                        <td><?=Yii::$app->formatter->asDate($buyer->date_submitted) ?></td>
                    </tr>

                    <?php
                }
            }?>

            </tbody>
        </table>
      </div>

    </div>
</div>
</section>



<section class="valuation-quotation-form card card-info card-outline mx-4">

<header class="card-header">
    <h2 class="card-title">Check Conflict Of Interest - Related To Client</h2>
</header>
<div class="card-body">
    <div class="row">
      <div class="col">
        <label class="control-label" for="valuationquotation-related_to_client_reason">Related To Client</label>
        <input type="text" class="form-control"
               id="valuationquotation-related_to_client"
               name="valuationquotation[related_to_client]"
          value="<?= $model->related_to_client ?>" readonly="" maxlength="255" aria-invalid="false">
      </div>

<div class="col-12" id="related_to_client_reason"  style="<?= ($model->related_to_client == 'Yes')? "": 'display:none;' ?>">
      <label class="control-label my-2" for="valuationquotation-related_to_client_reason">Related To Client Reason</label>
      <textarea id="valuationquotation-related_to_client_reason" class="form-control" name="ValuationQuotation[related_to_client_reason]" rows="6"readonly=""><?= $model->related_to_client_reason ?></textarea>


  <table class="table table-bordered my-2">
      <thead>
      <tr>
          <th>Reference Number</th>
          <th>Building Name</th>
          <th>Date</th>
      </tr>
      </thead>
      <tbody>

      <?php

      if($client_data <> null && !empty($client_data)) {
          foreach ($client_data as $client) {

              ?>
              <tr>
                  <td><?= $client->reference_number ?></td>
                  <td><?= $client->building->title ?></td>
                  <td><?= Yii::$app->formatter->asDate($buyer->date_submitted) ?></td>
              </tr>

              <?php
          }
      }?>

      </tbody>
  </table>
</div>

    </div>
</div>
</section>



<section class="valuation-quotation-form card card-info card-outline mx-4">

<header class="card-header">
    <h2 class="card-title">Check Conflict Of Interest - Related To Property</h2>
</header>
<div class="card-body">
    <div class="row">
      <div class="col">
        <label class="control-label" for="valuationquotation-related_to_property_reason">Related To Property</label>
        <input type="text" class="form-control"
               id="valuationquotation-related_to_property"
               name="valuationquotation[related_to_property]"
          value="<?= $model->related_to_property ?>" readonly="" maxlength="255" aria-invalid="false">
      </div>

  <div class="col-12" id="related_to_property_reason"  style="<?= ($model->related_to_property == 'Yes')? "": 'display:none;' ?>">
      <label class="control-label my-2" for="valuationquotation-related_to_property_reason">Related To Property Reason</label>
      <textarea id="valuationquotation-related_to_property_reason" class="form-control" name="ValuationQuotation[related_to_property_reason]" rows="6"readonly=""><?= $model->related_to_property_reason ?></textarea>


  <table class="table table-bordered my-2">
      <thead>
      <tr>
          <th>Reference Number</th>
          <th>Building Name</th>
          <th>Date</th>
      </tr>
      </thead>
      <tbody>

      <?php

      if($property_data <> null && !empty($property_data)) {
          foreach ($property_data as $property) {

              ?>
              <tr>
                  <td><?= $property->reference_number ?></td>
                  <td><?= $property->building->title ?></td>
                  <td><?= Yii::$app->formatter->asDate($buyer->date_submitted) ?></td>
              </tr>

              <?php
          }
      }?>

      </tbody>
  </table>
</div>

    </div>
</div>
</section>











<?php
        if ($properties!=null) {
          foreach ($properties as $propertyData) {
            // echo "<pre>";
            // print_r($properties);
            // echo "</pre>";
        ?>

            <!-- property section -->
            <div class="valuation-quotation-view card card-warning card-outline mx-4">
              <div class="card-header">
                <h3 class="card-title">
                   Subject Property Details
                </h3>
              </div>

              <div class="card-body">
                <div class="row">
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Building')?></label>
                    <?php
                    // echo "<pre>";
                      // print_r($propertyData['building']);
                      // echo "</pre>";
                     ?>
                    <input type="text" class="form-control"
                      value="<?= $propertyData->buildingTitle->title ?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Property')?></label>
                    <input type="text" class="form-control"
                      value="<?= $propertyData->propertyTitle->title ?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Property Category')?></label>
                    <input type="text" class="form-control"
                      value="<?= $propertyData['property_category'] ?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Tenure')?></label>
                    <input type="text" class="form-control"
                      value="<?= Yii::$app->appHelperFunctions->getBuildingTenureArr()[$propertyData['tenure']]  ?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'City')?></label>
                    <input type="text" class="form-control"
                      value="<?= $propertyData['city'] ?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Community')?></label>
                    <input type="text"class="form-control"
                      value="<?= $propertyData['community'] ?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Sub Community')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['sub_community']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Owner Name')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['owner_name']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Building Number')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['building_number']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Plot Number')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['plot_number']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Unit Number')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['unit_number']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Floor Number')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['floor_number']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'No Of Floors')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['no_of_floors']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'No Of Units')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['no_of_units']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Land Size')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['land_size']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Built Up Area')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['built_up_area']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Street')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['street']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Location')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['location']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Valuation Type')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['valuation_type']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Repeat Valuation')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['repeat_valuation']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Valuation Date')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['valuation_date']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Complexity')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['complexity']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Type Of Valuation')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['type_of_valuation']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col-3 my-2">
                    <label><?=Yii::t('app', 'Number Of Comparables')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['number_of_comparables']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>
                  <div class="col my-2">
                    <label><?=Yii::t('app', 'Valuation Approach')?></label>
                    <input type="text" class="form-control"
                      value="<?=$propertyData['valuation_approach']?>" readonly="" maxlength="255" aria-invalid="false">
                  </div>

              </div>
            </div>

                  <div class="card card-primary card-outline mx-4">
                    <div class="card-header">
                      <h3 class="card-title">
                        <i class="fas fa-edit"></i>
                        Contact Details Form
                      </h3>
                    </div>
                    <div class="card-body">
                      <div class="row py-2">
                        <div class="col-4">
                          <label><?=Yii::t('app', 'Name')?></label>
                          <input type="text" class="form-control"
                            value="<?=$propertyData['name']?>" readonly="" maxlength="255" aria-invalid="false">
                          </div>
                        <div class="col-4">
                          <label><?=Yii::t('app', 'Phone Number')?></label>
                          <input type="text" class="form-control"
                            value="<?=$propertyData['phone_number']?>" readonly="" maxlength="255" aria-invalid="false">
                          </div>
                        <div class="col-4">
                          <label><?=Yii::t('app', 'Email')?></label>
                          <input type="text" class="form-control"
                            value="<?=$propertyData['email']?>" readonly="" maxlength="255" aria-invalid="false">
                          </div>
                      </div>
                  </div>
                  </div>

            </div>
            <!-- end property section -->
<?php
      }
    }
 ?>




<!-- lower section -->
  <div class="valuation-quotation-view card card-success card-outline mx-4">

    <div class="card-header">
      <h3 class="card-title">
         Recommended Fee Form
      </h3>
    </div>

      <div class="card-body">
        <div class="row">
          <div class="col-4 my-2">
            <label><?=Yii::t('app', 'Recommended Fee')?></label>
            <input type="text" class="form-control"
              value="<?= $model->recommended_fee ?>" readonly="" maxlength="255" aria-invalid="false">
          </div>
          <div class="col-4 my-2">
            <label><?=Yii::t('app', 'Relative Discount')?></label>
            <input type="text" class="form-control"
              value="<?= $model->relative_discount ?>" readonly="" maxlength="255" aria-invalid="false">
          </div>
          <div class="col-4 my-2">
            <label><?=Yii::t('app', 'Turn Around Time')?></label>
            <input type="text" class="form-control"
              value="<?= $model->turn_around_time ?>" readonly="" maxlength="255" aria-invalid="false">
          </div>
          <div class="col-4 my-2">
            <label><?=Yii::t('app', 'Valuer Name')?></label>
            <input type="text" class="form-control"
              value="<?= $model->valuer_name ?>" readonly="" maxlength="255" aria-invalid="false">
          </div>
          <div class="col-4 my-2">
            <label><?=Yii::t('app', 'Date')?></label>
            <input type="text" class="form-control"
              value="<?= $model->date ?>" readonly="" maxlength="255" aria-invalid="false">
          </div>
          <div class="col-4 my-2">
            <label><?=Yii::t('app', 'Final Fee Approved')?></label>
            <input type="text"class="form-control"
              value="<?= $model->final_fee_approved ?>" readonly="" maxlength="255" aria-invalid="false">
          </div>
          <div class="col-4 my-2">
            <label><?=Yii::t('app', 'Status')?></label>
            <input type="text" class="form-control"
              value="<?= yii::$app->quotationHelperFunctions->getStatusArr()[$model->status] ?>" readonly="" maxlength="255" aria-invalid="false">
          </div>

      </div>
    </div>
  </div>


</div>
