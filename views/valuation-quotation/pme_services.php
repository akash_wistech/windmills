<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\components\widgets\StatusVerified;
use yii\helpers\Url;

$this->title = 'PME Services';
$this->params['breadcrumbs'][] = $this->title;


$pmeService = 'pme';
?>

<?php $form = ActiveForm::begin(); ?>
<section class="master-form card card-outline card-primary">
    <header class="card-header">
        <h2 class="card-title"><strong class="">Fee for Plant Machinery Equipments</strong></h2>
        <div class="card-tools">
            <a href="<?= Url::toRoute(['valuation-quotation/get-master-file-history?file_type=pme']) ?>" target="_blank"
                class="btn btn-info btn-sm">History</a>
        </div>
    </header>

    <section class="master-form card card-outline card-success mx-4 mt-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Asset Category (Base fee) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed ">
                        <thead></thead>
                        <tbody>
                            <?php
                            $asset_category = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'asset_category'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');
                            ?>
                            <?php $loopIndex = 0; ?>
                            <tr>
                                <?php foreach ($asset_categories as $category) { ?>
                                    <td>
                                        <div style="padding-bottom:10px"><strong><?php echo $category->title ?></strong>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" min="0" step="0.01"
                                                name="QuotationFeeMasterFile[asset_category][<?= $category->id ?>]"
                                                value="<?= isset($asset_category[$category->id]) ? $asset_category[$category->id] : 0 ?>"
                                                placeholder="1" class="form-control" required />
                                        </div>
                                    </td>
                                    <?php if (($loopIndex + 1) % 5 == 0 && ($loopIndex + 1) < count($asset_categories)) { ?>
                                    </tr>
                                    <tr>
                                    <?php } ?>
                                    <?php $loopIndex++; ?>
                                <?php } ?>
                            </tr>
                        </tbody>
                    </table>





                </div>
            </div>
        </div>
    </section>


    <!--Subject Property Information-->
    <?php /* ?>
<section class="master-form card card-outline card-primary mx-4">

   <header class="card-header">
       <h2 class="card-title"><strong>Type of Subject Property (Base fee) (+)</strong></h2>
   </header>

   <div class="card-body">
       <div class="row justify-content-md-center">
           <div class="col-sm-12">

               <table class="table table-striped table-condensed table-hover">

                   <thead>
                       <tr>
                           <td>
                               <strong>Building - Residential</strong>
                           </td>
                           <td>
                               <strong>Building - Commercial</strong>
                           </td>
                           <td>
                               <strong>Building - Mixed Use</strong>
                           </td>
                           <td>
                               <strong>Villa Compound</strong>
                           </td>
                           <!-- <td>
                               <strong>Townhouse - Commercial</strong>
                           </td>
                           <td>
                               <strong>Restaurant</strong>
                           </td> -->

                           <!-- <td>
                               <strong>Shop</strong>
                           </td>
                           <td>
                               <strong>Warehouse </strong>
                           </td> -->


                       </tr>
                   </thead>

                   <tbody>

                       <tr>
                           <?php
                           $subject_property = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                               ->where(['heading' => 'subject_property'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');
                           ?>
                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][42]"
                                       value="<?= (isset($subject_property['42'])) ? $subject_property['42'] : 0 ?>"
                                       placeholder=" 1" class="form-control" required />
                               </div>
                           </td>
                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][81]"
                                       value="<?= (isset($subject_property['81'])) ? $subject_property['81'] : 0 ?>"
                                       placeholder=" 1" class="form-control" required />
                               </div>
                           </td>
                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][3]"
                                       value="<?= (isset($subject_property['3'])) ? $subject_property['3'] : 0 ?>"
                                       placeholder=" 1" class="form-control" required />
                               </div>
                           </td>
                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][10]"
                                       value="<?= (isset($subject_property['10'])) ? $subject_property['10'] : 0 ?>"
                                       placeholder=" 1" class="form-control" required />
                               </div>
                           </td>
                           <!-- <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][11]" value="<?= (isset($subject_property['11'])) ? $subject_property['11'] : 0 ?>"
               placeholder=" 1" class="form-control" required />
                               </div>
                           </td>
                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][15]" value="<?= (isset($subject_property['15'])) ? $subject_property['15'] : 0 ?>"
               placeholder=" 1" class="form-control" required />
                               </div>
                           </td> -->

                           <!-- <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][17]" value="<?= (isset($subject_property['17'])) ? $subject_property['17'] : 0 ?>"
               placeholder=" 1" class="form-control" required />
                               </div>
                           </td>
                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][24]" value="<?= (isset($subject_property['24'])) ? $subject_property['24'] : 0 ?>"
               placeholder=" 1" class="form-control" required />
                               </div>
                           </td> -->



                       </tr>


                   </tbody>

                   <thead>
                       <tr>


                           <td>
                               <strong>Labour Accommodation</strong>
                           </td>

                           <!-- <td>
                               <strong>Office</strong>
                           </td>

                           <td>
                               <strong>Factory</strong>
                           </td>
                           <td>
                               <strong>Villa - Commercial</strong>
                           </td> -->

                           <td>
                               <strong>Hospital</strong>
                           </td>
                           <td>
                               <strong>School</strong>
                           </td>
                           <td>
                               <strong>Hotel</strong>
                           </td>


                       </tr>
                   </thead>

                   <tbody>

                       <tr>



                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][25]"
                                       value="<?= (isset($subject_property['25'])) ? $subject_property['25'] : 0 ?>"
                                       placeholder=" 1" class="form-control" required />
                               </div>
                           </td>

                           <!-- <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][28]" value="<?= (isset($subject_property['28'])) ? $subject_property['28'] : 0 ?>"
               placeholder=" 1" class="form-control" required />
                               </div>
                           </td>

                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][38]" value="<?= (isset($subject_property['38'])) ? $subject_property['38'] : 0 ?>"
               placeholder=" 1" class="form-control" required />
                               </div>
                           </td>

                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][41]" value="<?= (isset($subject_property['41'])) ? $subject_property['41'] : 0 ?>"
               placeholder=" 1" class="form-control" required />
                               </div>
                           </td> -->


                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][43]"
                                       value="<?= (isset($subject_property['43'])) ? $subject_property['43'] : 0 ?>"
                                       placeholder=" 1" class="form-control" required />
                               </div>
                           </td>
                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][16]"
                                       value="<?= (isset($subject_property['16'])) ? $subject_property['16'] : 0 ?>"
                                       placeholder=" 1" class="form-control" required />
                               </div>
                           </td>
                           <td>
                               <div class="form-group">
                                   <input type="number" step=".01"
                                       name="QuotationFeeMasterFile[subject_property][9]"
                                       value="<?= (isset($subject_property['9'])) ? $subject_property['9'] : 0 ?>"
                                       placeholder=" 1" class="form-control" required />
                               </div>
                           </td>

                       </tr>


                   </tbody>
               </table>
           </div>
       </div>
   </div>
</section>
<?php */ ?>


    <!--Client Information-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Client Segment(%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Bank</strong>
                                </td>
                                <td>
                                    <strong>Corporate</strong>
                                </td>
                                <td>
                                    <strong>Individual</strong>
                                </td>
                                <td>
                                    <strong>Partner</strong>
                                </td>
                                <td>
                                    <strong>Foreign Individual</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <input type="hidden" name="QuotationFeeMasterFile[approach_type]" value="pme">
                                <?php

                                $client_type = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'client_type'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');


                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[client_type][bank]"
                                            value="<?= (isset($client_type['bank'])) ? $client_type['bank'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][corporate]"
                                            value="<?= (isset($client_type['corporate'])) ? $client_type['corporate'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][individual]"
                                            value="<?= (isset($client_type['individual'])) ? $client_type['individual'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][partner]"
                                            value="<?= (isset($client_type['partner'])) ? $client_type['partner'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][foreign-individual]"
                                            value="<?= (isset($client_type['foreign-individual'])) ? $client_type['foreign-individual'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <!--Location Information-->

    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Location (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Abu Dhabi</strong>
                                </td>
                                <td>
                                    <strong>Dubai</strong>
                                </td>
                                <td>
                                    <strong>Sharjah/UQ</strong>
                                </td>
                                <td>
                                    <strong>Rak, Fujairah</strong>
                                </td>
                                <td>
                                    <strong>Ajman</strong>
                                </td>
                                <td>
                                    <strong>Al Ain</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $location = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'location'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');

                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[location][abu_dhabi]"
                                            value="<?= (isset($location['abu_dhabi'])) ? $location['abu_dhabi'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[location][dubai]"
                                            value="<?= (isset($location['dubai'])) ? $location['dubai'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[location][sharjah]"
                                            value="<?= (isset($location['sharjah'])) ? $location['sharjah'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[location][rak_fuj]"
                                            value="<?= (isset($location['rak_fuj'])) ? $location['rak_fuj'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[location][ajman]"
                                            value="<?= (isset($location['ajman'])) ? $location['ajman'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[location][others]"
                                            value="<?= (isset($location['others'])) ? $location['others'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>



    <!--Complexity Information-->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Complexity (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Low</strong>
                                </td>
                                <td>
                                    <strong>Medium</strong>
                                </td>
                                <td>
                                    <strong>High</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $asset_complexity = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'asset_complexity'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[asset_complexity][low]"
                                            value="<?= (isset($asset_complexity['low'])) ? $asset_complexity['low'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[asset_complexity][medium]"
                                            value="<?= (isset($asset_complexity['medium'])) ? $asset_complexity['medium'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[asset_complexity][high]"
                                            value="<?= (isset($asset_complexity['high'])) ? $asset_complexity['high'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>






    <!--Number of Location-->

    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number of Locations (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Less Than 3</strong>
                                </td>
                                <td>
                                    <strong>3 - 5</strong>
                                </td>
                                <td>
                                    <strong>6 - 10</strong>
                                </td>
                                <td>
                                    <strong>11 - 15</strong>
                                </td>
                                <td>
                                    <strong>Above 15</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $no_of_location = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'no_of_location'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');
                                
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_location][less_than_3]"
                                            value="<?= (isset($no_of_location['less_than_3'])) ? $no_of_location['less_than_3'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_location][3_5]"
                                            value="<?= (isset($no_of_location['3_5'])) ? $no_of_location['3_5'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_location][6_10]"
                                            value="<?= (isset($no_of_location['6_10'])) ? $no_of_location['6_10'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_location][11_15]"
                                            value="<?= (isset($no_of_location['11_15'])) ? $no_of_location['11_15'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_location][above_15]"
                                            value="<?= (isset($no_of_location['above_15'])) ? $no_of_location['above_15'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <!--Number of Comparables-->

    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number of Comparables (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Less Than 3</strong>
                                </td>
                                <td>
                                    <strong>3 - 5</strong>
                                </td>
                                <td>
                                    <strong>6 - 10</strong>
                                </td>
                                <td>
                                    <strong>11 - 15</strong>
                                </td>
                                <td>
                                    <strong>Above 15</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $no_of_Comparables = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'number_of_comparables_data'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');
                                
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_comparables][less_than_3]"
                                            value="<?= (isset($no_of_Comparables['less_than_3'])) ? $no_of_Comparables['less_than_3'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_comparables][3_5]"
                                            value="<?= (isset($no_of_Comparables['3_5'])) ? $no_of_Comparables['3_5'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_comparables][6_10]"
                                            value="<?= (isset($no_of_Comparables['6_10'])) ? $no_of_Comparables['6_10'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_comparables][11_15]"
                                            value="<?= (isset($no_of_Comparables['11_15'])) ? $no_of_Comparables['11_15'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_comparables][above_15]"
                                            value="<?= (isset($no_of_Comparables['above_15'])) ? $no_of_Comparables['above_15'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>




    <!--Urgency Fee-->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Timing(Urgency Fee) (%)(+)</strong></h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Standard</strong>
                                </td>
                                <td>
                                    <strong>Urgent Same Week</strong>
                                </td>
                                <td>
                                    <strong>Urgent 2 Weeks</strong>
                                </td>
                                <td>
                                    <strong>Urgent 3 Weeks</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $urgency_fee = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'urgency_fee'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[urgency_fee][standard]"
                                            value="<?= (isset($urgency_fee['standard'])) ? $urgency_fee['standard'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[urgency_fee][urgent_same_week]"
                                            value="<?= (isset($urgency_fee['urgent_same_week'])) ? $urgency_fee['urgent_same_week'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[urgency_fee][urgent_2_weeks]"
                                            value="<?= (isset($urgency_fee['urgent_2_weeks'])) ? $urgency_fee['urgent_2_weeks'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[urgency_fee][urgent_3_weeks]"
                                            value="<?= (isset($urgency_fee['urgent_3_weeks'])) ? $urgency_fee['urgent_3_weeks'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
    <!--Number of assets-->
    <section class="master-form card card-outline card-warning mx-4">
        <header class="card-header">
            <h2 class="card-title"><strong>Number Of Assets (%)(+)</strong>
            </h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>1</strong>
                                </td>
                                <td>
                                    <strong>2</strong>
                                </td>
                                <td>
                                    <strong>3</strong>
                                </td>
                                <td>
                                    <strong>4</strong>
                                </td>
                                <td>
                                    <strong>5</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $no_of_asset = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'no_of_asset'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');
                                // echo "<pre>";
                                // print_r($no_of_property_dis);
                                // die();
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[no_of_asset][1]"
                                            value="<?= (isset($no_of_asset['1'])) ? $no_of_asset['1'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[no_of_asset][2]"
                                            value="<?= (isset($no_of_asset['2'])) ? $no_of_asset['2'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[no_of_asset][3]"
                                            value="<?= (isset($no_of_asset['3'])) ? $no_of_asset['3'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[no_of_asset][4]"
                                            value="<?= (isset($no_of_asset['4'])) ? $no_of_asset['4'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[no_of_asset][5]"
                                            value="<?= (isset($no_of_asset['5'])) ? $no_of_asset['5'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>
                        </tbody>
                        <thead>
                            <tr>

                                <td>
                                    <strong>6 - 10</strong>
                                </td>
                                <td>
                                    <strong>11 - 20</strong>
                                </td>
                                <td>
                                    <strong>21 - 50</strong>
                                </td>
                                <td>
                                    <strong>51 - 100</strong>
                                </td>
                                <td>
                                    <strong> >100 </strong>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[no_of_asset][6]"
                                            value="<?= (isset($no_of_asset['6'])) ? $no_of_asset['6'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[no_of_asset][7]"
                                            value="<?= (isset($no_of_asset['7'])) ? $no_of_asset['7'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[no_of_asset][8]"
                                            value="<?= (isset($no_of_asset['8'])) ? $no_of_asset['8'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[no_of_asset][9]"
                                            value="<?= (isset($no_of_asset['9'])) ? $no_of_asset['9'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[no_of_asset][10]"
                                            value="<?= (isset($no_of_asset['10'])) ? $no_of_asset['10'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Number of assets-->
    <section class="master-form card card-outline card-info mx-4">
        <header class="card-header">
            <h2 class="card-title"><strong>Asset Age (%)(+)</strong>
            </h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>1</strong>
                                </td>
                                <td>
                                    <strong>2</strong>
                                </td>
                                <td>
                                    <strong>3</strong>
                                </td>
                                <td>
                                    <strong>4</strong>
                                </td>
                                <td>
                                    <strong>5</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $asset_age = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'asset_age'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');
                                // echo "<pre>";
                                // print_r($no_of_property_dis);
                                // die();
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[asset_age][1]"
                                            value="<?= (isset($asset_age['1'])) ? $asset_age['1'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[asset_age][2]"
                                            value="<?= (isset($asset_age['2'])) ? $asset_age['2'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[asset_age][3]"
                                            value="<?= (isset($asset_age['3'])) ? $asset_age['3'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[asset_age][4]"
                                            value="<?= (isset($asset_age['4'])) ? $asset_age['4'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[asset_age][5]"
                                            value="<?= (isset($asset_age['5'])) ? $asset_age['5'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>
                        </tbody>
                        <thead>
                            <tr>

                                <td>
                                    <strong>6 - 10</strong>
                                </td>
                                <td>
                                    <strong>11 - 20</strong>
                                </td>
                                <td>
                                    <strong>21 - 50</strong>
                                </td>
                                <td>
                                    <strong>51 - 100</strong>
                                </td>
                                <td>
                                    <strong> >100 </strong>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[asset_age][6]"
                                            value="<?= (isset($asset_age['6'])) ? $asset_age['6'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[asset_age][7]"
                                            value="<?= (isset($asset_age['7'])) ? $asset_age['7'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[asset_age][8]"
                                            value="<?= (isset($asset_age['8'])) ? $asset_age['8'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[asset_age][9]"
                                            value="<?= (isset($asset_age['9'])) ? $asset_age['9'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[asset_age][10]"
                                            value="<?= (isset($asset_age['10'])) ? $asset_age['10'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Working Days-->
    <?php /* ?>
    <section class="master-form card card-outline card-primary mx-4">
        <header class="card-header">
            <h2 class="card-title"><strong>Working Days (%)(+)</strong>
            </h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>1</strong>
                                </td>
                                <td>
                                    <strong>2</strong>
                                </td>
                                <td>
                                    <strong>3</strong>
                                </td>
                                <td>
                                    <strong>4</strong>
                                </td>
                                <td>
                                    <strong>5</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $working_days = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'working_days'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');
                                // echo "<pre>";
                                // print_r($no_of_property_dis);
                                // die();
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[working_days][1]"
                                            value="<?= (isset($working_days['1'])) ? $working_days['1'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[working_days][2]"
                                            value="<?= (isset($working_days['2'])) ? $working_days['2'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[working_days][3]"
                                            value="<?= (isset($working_days['3'])) ? $working_days['3'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[working_days][4]"
                                            value="<?= (isset($working_days['4'])) ? $working_days['4'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[working_days][5]"
                                            value="<?= (isset($working_days['5'])) ? $working_days['5'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>
                        </tbody>
                        <thead>
                            <tr>

                                <td>
                                    <strong>6 - 10</strong>
                                </td>
                                <td>
                                    <strong>11 - 20</strong>
                                </td>
                                <td>
                                    <strong>21 - 50</strong>
                                </td>
                                <td>
                                    <strong>51 - 100</strong>
                                </td>
                                <td>
                                    <strong> >100 </strong>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[working_days][6]"
                                            value="<?= (isset($working_days['6'])) ? $working_days['6'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[working_days][7]"
                                            value="<?= (isset($working_days['7'])) ? $working_days['7'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[working_days][8]"
                                            value="<?= (isset($working_days['8'])) ? $working_days['8'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[working_days][9]"
                                            value="<?= (isset($working_days['9'])) ? $working_days['9'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[working_days][10]"
                                            value="<?= (isset($working_days['10'])) ? $working_days['10'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <?php */ ?>


    <!--Other Intended Users-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Other Intended Users</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Yes</strong>
                                </td>
                                <td>
                                    <strong>No</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                    $other_intended_users = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'other_intended_users'])->andWhere(['approach_type' => $pmeService])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[other_intended_users][yes]" value="<?= (isset($other_intended_users['yes'])) ? $other_intended_users['yes'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[other_intended_users][no]" value="<?= (isset($other_intended_users['no'])) ? $other_intended_users['no'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--New / Repeat/ Revalidation Client-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>New / Repeat/ Revalidation Client Discount (per property) (%)(-)</strong>
            </h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>New Client</strong>
                                </td>
                                <td>
                                    <strong>Repeat Client</strong>
                                </td>
                                <td>
                                    <strong>Bank Revalidation</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $new_repeat_valuation = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'new_repeat_valuation_data'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][new_client]"
                                            value="<?= (isset($new_repeat_valuation['new_client'])) ? $new_repeat_valuation['new_client'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][repeat_client]"
                                            value="<?= (isset($new_repeat_valuation['repeat_client'])) ? $new_repeat_valuation['repeat_client'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][bank_revalidation]"
                                            value="<?= (isset($new_repeat_valuation['bank_revalidation'])) ? $new_repeat_valuation['bank_revalidation'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Advance Payment-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Advance Payment</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>100%</strong>
                                </td>
                                <td>
                                    <strong>75%</strong>
                                </td>
                                <td>
                                    <strong>50%</strong>
                                </td>
                                <td>
                                    <strong>25%</strong>
                                </td>
                                <td>
                                    <strong>0%</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $payment = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'payment_terms'])->andWhere(['approach_type' => $pmeService ])->all(), 'sub_heading', 'values');

                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][100]"
                                            value="<?= (isset($payment['100'])) ? $payment['100'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[payment_terms][75]"
                                            value="<?= (isset($payment['75'])) ? $payment['75'] : 0 ?>" placeholder="1"
                                            class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[payment_terms][50]"
                                            value="<?= (isset($payment['50'])) ? $payment['50'] : 0 ?>" placeholder="1"
                                            class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[payment_terms][25]"
                                            value="<?= (isset($payment['25'])) ? $payment['25'] : 0 ?>" placeholder="1"
                                            class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[payment_terms][0]"
                                            value="<?= (isset($payment['0'])) ? $payment['0'] : 0 ?>" placeholder="1"
                                            class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="row mx-3">
        <div class="col">
            <?= StatusVerified::widget(['model' => $model, 'form' => $form]) ?>
        </div>
    </div>

    <?php
    $latestUpdatedUser = '';
    $query = \app\models\History::find()->where(['rec_type' => 'masterfile', 'file_type' => $pmeService ])->orderBy(['id' => SORT_DESC])->one();
    if ($query <> null) {
        $latestUpdatedUser = $query->user->firstname . ' ' . $query->user->lastname;
    }
    ?>

    <div class="card-footer bg-light">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mx-2']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if ($latestUpdatedUser <> null) {
            ?>
            <span class="badge grid-badge badge-dark mx-2"><mark>Last Updated By
                    <?= $latestUpdatedUser ?>
                </mark></span>
            <?php
        }
        ?>
    </div>
</section>

<?php ActiveForm::end(); ?>