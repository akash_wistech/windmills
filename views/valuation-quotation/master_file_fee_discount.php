<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\components\widgets\StatusVerified;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Fee Master File - Fee Discount');

$approach_type = 'discount';
// $property_type = 'residential_land_villa';

?>

<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
</style>

<?php $form = ActiveForm::begin(); ?>
<input type="hidden" name="QuotationFeeMasterFile[approach_type]" value="<?= $approach_type ?>">

<section class="master-form card card-outline card-primary">
    <header class="card-header">
        <h2 class="card-title"><strong class=""></strong></h2>
        <div class="card-tools">
            <a href="<?= Url::toRoute(['valuation-quotation/get-master-file-history?file_type='.$approach_type]) ?>" target="_blank" class="btn btn-info btn-sm">History</a>
        </div>
    </header>

    <!--Number of properties discount-->
    <section class="master-form card card-outline card-warning mx-4 mt-4">
        <header class="card-header">
            <h2 class="card-title"><strong>Number Of Properties Discount (On total fee of all properties)(%)(-)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1</strong>
                            </td>
                            <td>
                                <strong>2</strong>
                            </td>
                            <td>
                                <strong>3</strong>
                            </td>
                            <td>
                                <strong>4</strong>
                            </td>
                            <td>
                                <strong>5</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_property_discount = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'no_of_property_discount'])->andWhere(['approach_type' => $approach_type])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_property_dis);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][1]"
                                            value="<?= (isset($no_of_property_discount['1'])) ? $no_of_property_discount['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][2]"
                                            value="<?= (isset($no_of_property_discount['2'])) ? $no_of_property_discount['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][3]"
                                            value="<?= (isset($no_of_property_discount['3'])) ? $no_of_property_discount['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][4]"
                                            value="<?= (isset($no_of_property_discount['4'])) ? $no_of_property_discount['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][5]"
                                            value="<?= (isset($no_of_property_discount['5'])) ? $no_of_property_discount['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>
                        </tbody>
                        <thead>
                        <tr>

                            <td>
                                <strong>6 - 10</strong>
                            </td>
                            <td>
                                <strong>11 - 20</strong>
                            </td>
                            <td>
                                <strong>21 - 50</strong>
                            </td>
                            <td>
                                <strong>51 - 100</strong>
                            </td>
                            <td>
                                <strong> >100 </strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][6]"
                                            value="<?= (isset($no_of_property_discount['6'])) ? $no_of_property_discount['6'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][7]"
                                            value="<?= (isset($no_of_property_discount['7'])) ? $no_of_property_discount['7'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][8]"
                                            value="<?= (isset($no_of_property_discount['8'])) ? $no_of_property_discount['8'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][9]"
                                            value="<?= (isset($no_of_property_discount['9'])) ? $no_of_property_discount['9'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][10]"
                                            value="<?= (isset($no_of_property_discount['10'])) ? $no_of_property_discount['10'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Number of units in the same building-->
    <section class="master-form card card-outline card-warning mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number Of Units In The Same Building Discount (on total fee of all properties) (%)(-)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 - (One)</strong>
                            </td>
                            <td>
                                <strong>2 - (Two)</strong>
                            </td>
                            <td>
                                <strong>3 - (Three)</strong>
                            </td>
                            <td>
                                <strong>4 - (Four)</strong>
                            </td>
                            <td>
                                <strong>5 - (Five)</strong>
                            </td>
                            <td>
                                <strong> >5 - (More than five)</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_units_same_building_dis = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'no_of_units_same_building_discount'])->andWhere(['approach_type' => $approach_type])->all(), 'sub_heading', 'values');

                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][1]"
                                            value="<?= (isset($no_of_units_same_building_dis['1'])) ? $no_of_units_same_building_dis['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][2]"
                                            value="<?= (isset($no_of_units_same_building_dis['2'])) ? $no_of_units_same_building_dis['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][3]"
                                            value="<?= (isset($no_of_units_same_building_dis['3'])) ? $no_of_units_same_building_dis['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][4]"
                                            value="<?= (isset($no_of_units_same_building_dis['4'])) ? $no_of_units_same_building_dis['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][5]"
                                            value="<?= (isset($no_of_units_same_building_dis['5'])) ? $no_of_units_same_building_dis['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][6]"
                                            value="<?= (isset($no_of_units_same_building_dis['6'])) ? $no_of_units_same_building_dis['6'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--New / Repeat/ Revalidation Client-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>New / Repeat/ Revalidation Client Discount (per property) (%)(-)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>New Client</strong>
                            </td>
                            <td>
                                <strong>Repeat Client</strong>
                            </td>
                            <td>
                                <strong>Bank Revalidation</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $new_repeat_valuation = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'new_repeat_valuation_data'])->andWhere(['approach_type' => $approach_type])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][new_client]"
                                            value="<?= (isset($new_repeat_valuation['new_client'])) ? $new_repeat_valuation['new_client'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][repeat_client]"
                                            value="<?= (isset($new_repeat_valuation['repeat_client'])) ? $new_repeat_valuation['repeat_client'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][bank_revalidation]"
                                            value="<?= (isset($new_repeat_valuation['bank_revalidation'])) ? $new_repeat_valuation['bank_revalidation'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Advance Payment-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Advance Payment</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>100%</strong>
                            </td>
                            <td>
                                <strong>75%</strong>
                            </td>
                            <td>
                                <strong>50%</strong>
                            </td>
                            <td>
                                <strong>25%</strong>
                            </td>
                            <td>
                                <strong>0%</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $payment = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'payment_terms'])->andWhere(['approach_type' => $approach_type])->all(), 'sub_heading', 'values');

                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][100]"
                                            value="<?= (isset($payment['100'])) ? $payment['100'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][75]"
                                            value="<?= (isset($payment['75'])) ? $payment['75'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][50]"
                                            value="<?= (isset($payment['50'])) ? $payment['50'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][25]"
                                            value="<?= (isset($payment['25'])) ? $payment['25'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][0]"
                                            value="<?= (isset($payment['0'])) ? $payment['0'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="row mx-3">
        <div class="col">
            <?= StatusVerified::widget(['model' => $model, 'form' => $form]) ?>
        </div>
    </div>
    
    <?php
    $latestUpdatedUser = '';
    $query = \app\models\History::find()->where(['rec_type' => 'masterfile', 'file_type' => 'market'])->orderBy(['id' => SORT_DESC])->one();
    if ($query <> null) {
        $latestUpdatedUser = $query->user->firstname . ' ' . $query->user->lastname;
    }
    ?>

    <div class="card-footer bg-light">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mx-2']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if ($latestUpdatedUser <> null) { ?>
                <span class="badge grid-badge badge-dark mx-2"><mark>Last Updated By <?= $latestUpdatedUser ?></mark></span>
        <?php
        }
        ?>
    </div>
</section>

<?php ActiveForm::end(); ?>
