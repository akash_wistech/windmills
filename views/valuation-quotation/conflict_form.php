<?php
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
?>

<section class="card card-danger card-outline mx-4">
  <header class="card-header">
      <h2 class="card-title"><b>Conflict Area</b></h2>
  </header>
<div class="card-body">

<!-- Check Conflict Of Interest - Related To Buyer -->
<section class="valuation-quotation-form card card-danger card-outline mx-4">

        <header class="card-header">
            <h2 class="card-title">Check Conflict Of Interest - Related To Buyer</h2>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <?php
                    echo $form->field($model, 'related_to_buyer')->widget(Select2::classname(), [
                        'data' => array( 'No' => 'No','Yes' => 'Yes'),
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-sm-12" id="related_to_buyer_reason"  style="<?= ($model->related_to_buyer == 'Yes')? "": 'display:none;' ?>">

                  <?= $form->field($model, 'related_to_buyer_reason')->textarea(['rows' => '6']) ?>



                  <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Reference Number</th>
                              <th>Building Name</th>
                              <th>Date</th>
                          </tr>
                      </thead>
                      <tbody>

                          <?php

                          if($buyer_data <> null && !empty($buyer_data)) {
                              foreach ($buyer_data as $buyer) {

                                  ?>
                                  <tr>
                                      <td><?= $buyer->reference_number ?></td>
                                      <td><?= $buyer->building->title ?></td>
                                      <td><?= Yii::$app->formatter->asDate($buyer->date_submitted) ?></td>
                                  </tr>

                                  <?php
                              }
                          }?>

                      </tbody>
                  </table>


              </div>

          </div>
      </div>
</section>

<!-- Check Conflict Of Interest - Related To Owner -->
<section class="valuation-quotation-form card card-danger card-outline mx-4">

    <header class="card-header">
        <h2 class="card-title">Check Conflict Of Interest - Related To Owner</h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col">
                <?php
                echo $form->field($model, 'related_to_owner')->widget(Select2::classname(), [
                    'data' => array( 'No' => 'No','Yes' => 'Yes'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-12" id="related_to_owner_reason"  style="<?= ($model->related_to_owner == 'Yes')? "": 'display:none;' ?>">

                <?= $form->field($model, 'related_to_owner_reason')->textarea(['rows' => '6']) ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Reference Number</th>
                            <th>Building Name</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php

                        if($seller_data <> null && !empty($seller_data)) {
                            foreach ($seller_data as $seller) {

                                ?>
                                <tr>
                                    <td><?= $seller->reference_number ?></td>
                                    <td><?= $seller->building->title ?></td>
                                    <td><?=Yii::$app->formatter->asDate($buyer->date_submitted) ?></td>
                                </tr>

                                <?php
                            }
                        }?>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
</section>

<!-- Check Conflict Of Interest - Related To Client -->
<section class="valuation-quotation-form card card-danger card-outline mx-4" style="display:none;">

    <header class="card-header">
        <h2 class="card-title">Check Conflict Of Interest - Related To Client</h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col">
                <?php
                echo $form->field($model, 'related_to_client')->widget(Select2::classname(), [
                    'data' => array( 'No' => 'No','Yes' => 'Yes'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-12" id="related_to_client_reason"  style="<?= ($model->related_to_client == 'Yes')? "": 'display:none;' ?>">

                <?= $form->field($model, 'related_to_client_reason')->textarea(['rows' => '6']) ?>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Reference Number</th>
                            <th>Building Name</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php

                        if($client_data <> null && !empty($client_data)) {
                            foreach ($client_data as $client) {

                                ?>
                                <tr>
                                    <td><?= $client->reference_number ?></td>
                                    <td><?= $client->building->title ?></td>
                                    <td><?= Yii::$app->formatter->asDate($buyer->date_submitted) ?></td>
                                </tr>

                                <?php
                            }
                        }?>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
</section>

<!-- Check Conflict Of Interest - Related To Property -->
<section class="valuation-quotation-form card card-danger card-outline mx-4">

    <header class="card-header">
        <h2 class="card-title">Check Conflict Of Interest - Related To Property</h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col">
                <?php
                echo $form->field($model, 'related_to_property')->widget(Select2::classname(), [
                    'data' => array( 'No' => 'No','Yes' => 'Yes'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-12" id="related_to_property_reason"  style="<?= ($model->related_to_property == 'Yes')? "": 'display:none;' ?>">
                <?= $form->field($model, 'related_to_property_reason')->textarea(['rows' => '6']) ?>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Reference Number</th>
                            <th>Building Name</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php

                        if($property_data <> null && !empty($property_data)) {
                            foreach ($property_data as $property) {

                                ?>
                                <tr>
                                    <td><?= $property->reference_number ?></td>
                                    <td><?= $property->building->title ?></td>
                                    <td><?= Yii::$app->formatter->asDate($buyer->date_submitted) ?></td>
                                </tr>

                                <?php
                            }
                        }?>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
</section>

</div>
</section>
