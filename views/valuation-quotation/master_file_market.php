<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use  app\components\widgets\StatusVerified;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin(); ?>
<section class="master-form card card-outline card-primary">
  <header class="card-header">
      <h2 class="card-title"><strong class="">Fee For Market Approach</strong></h2>
        <div class="card-tools">
            <a href="<?= Url::toRoute(['valuation-quotation/get-master-file-history?file_type=market']) ?>" target="_blank" class="btn btn-info btn-sm">History</a>
        </div>
  </header>

<!--Propety Information -->
<section class="master-form card card-outline card-success mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Type of Subject Property (Base fee) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Apartment</strong>
                            </td>
                            <td>
                                <strong>Hotel Apartment</strong>
                            </td>
                            <td>
                                <strong>Land - Agricultural</strong>
                            </td>
                            <td>
                                <strong>Land - Commercial Building</strong>
                            </td>
                            <td>
                                <strong>Land - Commercial Townhouse</strong>
                            </td>
                            <td>
                                <strong>Land - Commercial Villa</strong>
                            </td>
                            <td>
                                <strong>Land - Hospital Building/strong>
                            </td>
                            <td>
                                <strong>Land - Hotel Apartment Building</strong>
                            </td>

                            <td>
                                <strong>Land - Hotel Building</strong>
                            </td>


                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $subject_property =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'subject_property'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][1]"
                                            value="<?= (isset($subject_property['1'])) ? $subject_property['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][12]"
                                            value="<?= (isset($subject_property['12'])) ? $subject_property['12'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][23]"
                                            value="<?= (isset($subject_property['23'])) ? $subject_property['23'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][4]"
                                            value="<?= (isset($subject_property['4'])) ? $subject_property['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][47]"
                                            value="<?= (isset($subject_property['47'])) ? $subject_property['47'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][46]"
                                            value="<?= (isset($subject_property['46'])) ? $subject_property['46'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][50]"
                                            value="<?= (isset($subject_property['50'])) ? $subject_property['50'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][53]"
                                            value="<?= (isset($subject_property['53'])) ? $subject_property['53'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][48]"
                                            value="<?= (isset($subject_property['48'])) ? $subject_property['48'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>



                        </tr>


                        </tbody>

                        <thead>
                        <tr>


                            <td>
                                <strong>Land - Industrial</strong>
                            </td>

                            <td>
                                <strong>Land - Mixed Use Building</strong>
                            </td>

                            <td>
                                <strong>Land - Residential Building</strong>
                            </td>
                            <td>
                                <strong>Land - Residential Townhouse</strong>
                            </td>
                            <td>
                                <strong>Land - Residential Villa</strong>
                            </td>
                            <td>
                                <strong>Land - School Building</strong>
                            </td>
                            <td>
                                <strong>Townhouse</strong>
                            </td>
                            <td>
                                <strong>Villa</strong>
                            </td>

                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                       


                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][29]"
                                            value="<?= (isset($subject_property['29'])) ? $subject_property['29'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][39]"
                                            value="<?= (isset($subject_property['39'])) ? $subject_property['39'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][5]"
                                            value="<?= (isset($subject_property['5'])) ? $subject_property['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][26]"
                                            value="<?= (isset($subject_property['26'])) ? $subject_property['26'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][20]"
                                            value="<?= (isset($subject_property['20'])) ? $subject_property['20'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][49]"
                                            value="<?= (isset($subject_property['49'])) ? $subject_property['49'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][6]"
                                            value="<?= (isset($subject_property['6'])) ? $subject_property['6'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][2]"
                                            value="<?= (isset($subject_property['2'])) ? $subject_property['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>






                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--Client Information-->
<section class="master-form card card-outline card-secondary mx-4 my-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Client Segment(%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Bank</strong>
                            </td>
                            <td>
                                <strong>Corporate</strong>
                            </td>
                            <td>
                                <strong>Individual</strong>
                            </td>
                            <td>
                                <strong>Partner</strong>
                            </td>
                            <td>
                                <strong>Foreign Individual</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <input type="hidden" name="QuotationFeeMasterFile[approach_type]" value="market" >
                            <?php

                            $client_type =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'client_type'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');


                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][bank]"
                                            value="<?= (isset($client_type['bank'])) ? $client_type['bank'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][corporate]"
                                            value="<?= (isset($client_type['corporate'])) ? $client_type['corporate'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][individual]"
                                            value="<?= (isset($client_type['individual'])) ? $client_type['individual'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][partner]"
                                            value="<?= (isset($client_type['partner'])) ? $client_type['partner'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][foreign-individual]"
                                            value="<?= (isset($client_type['foreign-individual'])) ? $client_type['foreign-individual'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required/>
                                </div>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--City Information-->
<section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>City (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Abu Dhabi</strong>
                            </td>
                            <td>
                                <strong>Dubai</strong>
                            </td>
                            <td>
                                <strong>Sharjah/UQ</strong>
                            </td>
                            <td>
                                <strong>Rak, Fujairah</strong>
                            </td>
                            <td>
                                <strong>Ajman</strong>
                            </td>
                            <td>
                                <strong>Al Ain</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $city =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'city'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');

                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][abu_dhabi]"
                                            value="<?= (isset($city['abu_dhabi'])) ? $city['abu_dhabi'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][dubai]"
                                            value="<?= (isset($city['dubai'])) ? $city['dubai'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][sharjah]"
                                            value="<?= (isset($city['sharjah'])) ? $city['sharjah'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][rak_fuj]"
                                            value="<?= (isset($city['rak_fuj'])) ? $city['rak_fuj'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][ajman]"
                                            value="<?= (isset($city['ajman'])) ? $city['ajman'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][others]"
                                            value="<?= (isset($city['others'])) ? $city['others'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--Tenure Information-->
<section class="master-form card card-outline card-dark mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Tenure (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Freehold</strong>
                            </td>
                            <td>
                                <strong>Non-Freehold</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $tenure =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'tenure'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($tenure);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[tenure][freehold]"
                                            value="<?= (isset($tenure['freehold'])) ? $tenure['freehold'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[tenure][non_freehold]"
                                            value="<?= (isset($tenure['non_freehold'])) ? $tenure['non_freehold'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--Complexity Information-->
<section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Complexity (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Non-Standard</strong>
                            </td>
                            <td>
                                <strong>Standard</strong>
                            </td>
                            <td>
                                <strong>Penthouse</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $complexity =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'complexity'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[complexity][non_standard]"
                                            value="<?= (isset($complexity['non_standard'])) ? $complexity['non_standard'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[complexity][standard]"
                                            value="<?= (isset($complexity['standard'])) ? $complexity['standard'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[complexity][penthouse]"
                                            value="<?= (isset($complexity['penthouse'])) ? $complexity['penthouse'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--Upgrades Information-->
<section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Upgrades/Age Ratings (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 Star</strong>
                            </td>
                            <td>
                                <strong>2 Star</strong>
                            </td>
                            <td>
                                <strong>3 Star</strong>
                            </td>
                            <td>
                                <strong>4 Star</strong>
                            </td>
                            <td>
                                <strong>5 Star</strong>
                            </td>

                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $upgrades_ratings =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'upgrades_ratings'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r(upgrades_ratings);
                            // die();
                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][1]"
                                            value="<?= (isset($upgrades_ratings['1'])) ? $upgrades_ratings['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][2]"
                                            value="<?= (isset($upgrades_ratings['2'])) ? $upgrades_ratings['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][3]"
                                            value="<?= (isset($upgrades_ratings['3'])) ? $upgrades_ratings['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][4]"
                                            value="<?= (isset($upgrades_ratings['4'])) ? $upgrades_ratings['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][5]"
                                            value="<?= (isset($upgrades_ratings['5'])) ? $upgrades_ratings['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>



                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--Land size Information-->
<section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Standard Land size (only applicable on land valuation) (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 - 5,000</strong>
                            </td>
                            <td>
                                <strong>5,001 - 7,500</strong>
                            </td>
                            <td>
                                <strong>7,501 - 10,000</strong>
                            </td>
                            <td>
                                <strong>10,001 - 15,000</strong>
                            </td>
                            <td>
                                <strong>15,00 - 25,000</strong>
                            </td>
                            <td>
                                <strong>Above 25,000</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $land =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'land'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($land);
                            // die();
                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[land][1_5000]"
                                            value="<?= (isset($land['1_5000'])) ? $land['1_5000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[land][5001_7500]"
                                            value="<?= (isset($land['5001_7500'])) ? $land['5001_7500'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[land][7501_10000]"
                                            value="<?= (isset($land['7501_10000'])) ? $land['7501_10000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[land][10001_15000]"
                                            value="<?= (isset($land['10001_15000'])) ? $land['10001_15000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[land][15001_25000]"
                                            value="<?= (isset($land['15001_25000'])) ? $land['15001_25000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[land][above_25000]"
                                            value="<?= (isset($land['above_25000'])) ? $land['above_25000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--Built Up Area of subject property-->
<section class="master-form card card-outline card-success mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Built Up Area of subject property (%)(+)</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>1 - 2,000</strong>
                        </td>
                        <td>
                            <strong>2,001 - 4,000</strong>
                        </td>
                        <td>
                            <strong>4,001 - 6,000</strong>
                        </td>
                        <td>
                            <strong>6,001 - 8,000</strong>
                        </td>
                        <td>
                            <strong>8,000+</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $BUA =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                      ->where(['heading' => 'built_up_area_of_subject_property'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($BUA);
                          // die();
                      ?>

                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][1_2000]"
                            value="<?= (isset($BUA['1_2000'])) ? $BUA['1_2000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                      <td>
                          <div class="form-group">
                              <input  type="number" step=".01"
                                      name="QuotationFeeMasterFile[built_up_area_of_subject_property][2001_4000]"
                                      value="<?= (isset($BUA['2001_4000'])) ? $BUA['2001_4000'] : 0 ?>"
                                      placeholder="1" class="form-control"
                                      required
                              />
                          </div>
                      </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][4001_6000]"
                            value="<?= (isset($BUA['4001_6000'])) ? $BUA['4001_6000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][6001_8000]"
                            value="<?= (isset($BUA['6001_8000'])) ? $BUA['6001_8000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                     <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][above_8000]"
                            value="<?= (isset($BUA['above_8000'])) ? $BUA['above_8000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>
<!--Built Up Area of subject property for Building Land-->
<section class="master-form card card-outline card-success mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Built Up Area of Subject Property For Building land(%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 - 25,000</strong>
                            </td>
                            <td>
                                <strong>25,001 - 75,000</strong>
                            </td>
                            <td>
                                <strong>75,001 - 200,000</strong>
                            </td>
                            <td>
                                <strong>200,001 - 400,000</strong>
                            </td>
                            <td>
                                <strong>400,000 - 750,000</strong>
                            </td>
                            <td>
                                <strong>750,000+</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $BUA_b_land =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'built_up_area_of_subject_property_b_land'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                           /*  echo "<pre>";
                             print_r($BUA_b_land);
                             die();*/
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property_b_land][1_25000]"
                                            value="<?= (isset($BUA_b_land['1_25000'])) ? $BUA_b_land['1_25000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property_b_land][25001_75000]"
                                            value="<?= (isset($BUA_b_land['25001_75000'])) ? $BUA_b_land['25001_75000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property_b_land][75001_200000]"
                                            value="<?= (isset($BUA_b_land['75001_200000'])) ? $BUA_b_land['75001_200000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property_b_land][200001_400000]"
                                            value="<?= (isset($BUA_b_land['200001_400000'])) ? $BUA_b_land['200001_400000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property_b_land][400001_750000]"
                                            value="<?= (isset($BUA_b_land['400001_750000'])) ? $BUA_b_land['400001_750000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property_b_land][above_750000]"
                                            value="<?= (isset($BUA_b_land['above_750000'])) ? $BUA_b_land['above_750000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--Number of Comparables-->
<section class="master-form card card-outline card-info mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Number of Comparables (%) (+)</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>Less Than 3</strong>
                        </td>
                        <td>
                            <strong>3 - 5</strong>
                        </td>
                        <td>
                            <strong>6 - 10</strong>
                        </td>
                        <td>
                            <strong>11 - 15</strong>
                        </td>
                        <td>
                            <strong>Above 15</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $no_of_Comparables =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                      ->where(['heading' => 'number_of_comparables_data'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($no_of_Comparables);
                          // die();
                      ?>

                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="QuotationFeeMasterFile[number_of_comparables][less_than_3]"
                            value="<?= (isset($no_of_Comparables['less_than_3'])) ? $no_of_Comparables['less_than_3'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="QuotationFeeMasterFile[number_of_comparables][3_5]"
                            value="<?= (isset($no_of_Comparables['3_5'])) ? $no_of_Comparables['3_5'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="QuotationFeeMasterFile[number_of_comparables][6_10]"
                            value="<?= (isset($no_of_Comparables['6_10'])) ? $no_of_Comparables['6_10'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="QuotationFeeMasterFile[number_of_comparables][11_15]"
                            value="<?= (isset($no_of_Comparables['11_15'])) ? $no_of_Comparables['11_15'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="QuotationFeeMasterFile[number_of_comparables][above_15]"
                            value="<?= (isset($no_of_Comparables['above_15'])) ? $no_of_Comparables['above_15'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>
<!--Other intended users-->
<section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Other intended users</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Yes</strong>
                            </td>
                            <td>
                                <strong>No</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $other_intended_users =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'other_intended_users'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[other_intended_users][yes]"
                                            value="<?= (isset($other_intended_users['yes'])) ? $other_intended_users['yes'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[other_intended_users][no]"
                                            value="<?= (isset($other_intended_users['no'])) ? $other_intended_users['no'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--Urgency Fee-->
<section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Timing(Urgency Fee) (%)(+)</strong></h2>
        </header>
    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

                <table class="table table-striped table-condensed table-hover">

                    <thead>
                    <tr>
                        <td>
                            <strong>Standard</strong>
                        </td>
                        <td>
                            <strong>Urgent Same Day</strong>
                        </td>
                        <td>
                            <strong>Urgent 1 Day</strong>
                        </td>
                        <td>
                            <strong>Urgent 2 Days</strong>
                        </td>
                    </tr>
                    </thead>

                    <tbody>

                    <tr>
                        <?php
                        $urgency_fee =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                            ->where(['heading' => 'urgency_fee'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                        ?>

                        <td>
                            <div class="form-group">
                                <input  type="number" step=".01"
                                        name="QuotationFeeMasterFile[urgency_fee][standard]"
                                        value="<?= (isset($urgency_fee['standard'])) ? $urgency_fee['standard'] : 0 ?>"
                                        placeholder="1" class="form-control"
                                        required
                                />
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input  type="number" step=".01"
                                        name="QuotationFeeMasterFile[urgency_fee][urgent_same_day]"
                                        value="<?= (isset($urgency_fee['urgent_same_day'])) ? $urgency_fee['urgent_same_day'] : 0 ?>"
                                        placeholder="1" class="form-control"
                                        required
                                />
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input  type="number" step=".01"
                                        name="QuotationFeeMasterFile[urgency_fee][urgent_1_day]"
                                        value="<?= (isset($urgency_fee['urgent_1_day'])) ? $urgency_fee['urgent_1_day'] : 0 ?>"
                                        placeholder="1" class="form-control"
                                        required
                                />
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input  type="number" step=".01"
                                        name="QuotationFeeMasterFile[urgency_fee][urgent_2_days]"
                                        value="<?= (isset($urgency_fee['urgent_2_days'])) ? $urgency_fee['urgent_2_days'] : 0 ?>"
                                        placeholder="1" class="form-control"
                                        required
                                />
                            </div>
                        </td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    </section>
<!--Number of properties discount-->
<section class="master-form card card-outline card-warning mx-4">
        <header class="card-header">
            <h2 class="card-title"><strong>Number Of Properties Discount (On total fee of all properties)(%)(-)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1</strong>
                            </td>
                            <td>
                                <strong>2</strong>
                            </td>
                            <td>
                                <strong>3</strong>
                            </td>
                            <td>
                                <strong>4</strong>
                            </td>
                            <td>
                                <strong>5</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_property_discount =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'no_of_property_discount'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_property_dis);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][1]"
                                            value="<?= (isset($no_of_property_discount['1'])) ? $no_of_property_discount['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][2]"
                                            value="<?= (isset($no_of_property_discount['2'])) ? $no_of_property_discount['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][3]"
                                            value="<?= (isset($no_of_property_discount['3'])) ? $no_of_property_discount['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][4]"
                                            value="<?= (isset($no_of_property_discount['4'])) ? $no_of_property_discount['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][5]"
                                            value="<?= (isset($no_of_property_discount['5'])) ? $no_of_property_discount['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>
                        </tbody>
                        <thead>
                        <tr>

                            <td>
                                <strong>6 - 10</strong>
                            </td>
                            <td>
                                <strong>11 - 20</strong>
                            </td>
                            <td>
                                <strong>21 - 50</strong>
                            </td>
                            <td>
                                <strong>51 - 100</strong>
                            </td>
                            <td>
                                <strong> >100 </strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][6]"
                                            value="<?= (isset($no_of_property_discount['6'])) ? $no_of_property_discount['6'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][7]"
                                            value="<?= (isset($no_of_property_discount['7'])) ? $no_of_property_discount['7'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][8]"
                                            value="<?= (isset($no_of_property_discount['8'])) ? $no_of_property_discount['8'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][9]"
                                            value="<?= (isset($no_of_property_discount['9'])) ? $no_of_property_discount['9'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][10]"
                                            value="<?= (isset($no_of_property_discount['10'])) ? $no_of_property_discount['10'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--Number of units in the same building-->
<section class="master-form card card-outline card-warning mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number Of Units In The Same Building Discount (on total fee of all properties) (%)(-)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 - (One)</strong>
                            </td>
                            <td>
                                <strong>2 - (Two)</strong>
                            </td>
                            <td>
                                <strong>3 - (Three)</strong>
                            </td>
                            <td>
                                <strong>4 - (Four)</strong>
                            </td>
                            <td>
                                <strong>5 - (Five)</strong>
                            </td>
                            <td>
                                <strong> >5 - (More than five)</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_units_same_building_dis =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'no_of_units_same_building_discount'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                           
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][1]"
                                            value="<?= (isset($no_of_units_same_building_dis['1'])) ? $no_of_units_same_building_dis['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][2]"
                                            value="<?= (isset($no_of_units_same_building_dis['2'])) ? $no_of_units_same_building_dis['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][3]"
                                            value="<?= (isset($no_of_units_same_building_dis['3'])) ? $no_of_units_same_building_dis['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][4]"
                                            value="<?= (isset($no_of_units_same_building_dis['4'])) ? $no_of_units_same_building_dis['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][5]"
                                            value="<?= (isset($no_of_units_same_building_dis['5'])) ? $no_of_units_same_building_dis['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][6]"
                                            value="<?= (isset($no_of_units_same_building_dis['6'])) ? $no_of_units_same_building_dis['6'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--New / Repeat/ Revalidation Client-->
<section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>New / Repeat/ Revalidation Client Discount (per property) (%)(-)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>New Client</strong>
                            </td>
                            <td>
                                <strong>Repeat Client</strong>
                            </td>
                            <td>
                                <strong>Bank Revalidation</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $new_repeat_valuation =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'new_repeat_valuation_data'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][new_client]"
                                            value="<?= (isset($new_repeat_valuation['new_client'])) ? $new_repeat_valuation['new_client'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][repeat_client]"
                                            value="<?= (isset($new_repeat_valuation['repeat_client'])) ? $new_repeat_valuation['repeat_client'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][bank_revalidation]"
                                            value="<?= (isset($new_repeat_valuation['bank_revalidation'])) ? $new_repeat_valuation['bank_revalidation'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--Advance Payment-->
<section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Advance Payment</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>100%</strong>
                            </td>
                            <td>
                                <strong>75%</strong>
                            </td>
                            <td>
                                <strong>50%</strong>
                            </td>
                            <td>
                                <strong>25%</strong>
                            </td>
                            <td>
                                <strong>0%</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $payment =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'payment_terms'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');

                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][100]"
                                            value="<?= (isset($payment['100'])) ? $payment['100'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][75]"
                                            value="<?= (isset($payment['75'])) ? $payment['75'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][50]"
                                            value="<?= (isset($payment['50'])) ? $payment['50'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][25]"
                                            value="<?= (isset($payment['25'])) ? $payment['25'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][0]"
                                            value="<?= (isset($payment['0'])) ? $payment['0'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

<div class="row mx-3">
        <div class="col">
            <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
        </div>
    </div>
    
<?php  
    $latestUpdatedUser = '';
    $query = \app\models\History::find()->where(['rec_type'=>'masterfile','file_type'=>'market'])->orderBy(['id' => SORT_DESC])->one();
    if($query<>null){
        $latestUpdatedUser = $query->user->firstname.' '.$query->user->lastname;
    }
?>

<div class="card-footer bg-light">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mx-2']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
            <?php 
            if($latestUpdatedUser<>null){?>
                <span class="badge grid-badge badge-dark mx-2"><mark>Last Updated By <?= $latestUpdatedUser ?></mark></span>
        <?php
            }
        ?>
</div>
</section>

<?php ActiveForm::end(); ?>
