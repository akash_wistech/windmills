<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\TinyMceAsset;
TinyMceAsset::register($this);
use  app\components\widgets\StatusVerified;
use yii\helpers\Url;

$this->title = 'BCS Service Report';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('


tinymce.init({
  selector: ".editor",

  menubar: false,
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table paste code help wordcount code",
  ],
  toolbar: "undo redo | formatselect | " +
  "bold italic backcolor | alignleft aligncenter " +
  "alignright alignjustify | bullist numlist outdent indent | " +
  "removeformat | code",
});

');

?>
<section class="proposal-standard-report-form card card-outline card-primary">

    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title">BCS Service Report</h2>
        <div class="card-tools">
            <a href="<?= Url::toRoute(['valuation-quotation/get-history?id='.$model->id.'&m_name=ProposalStandardReport']) ?>" target="_blank" class="btn btn-info btn-sm">History</a>
        </div>
    </header>

    <div class="card-body">
        <div class="row">

          <div class="col-sm-12">
            <?= $form->field($model, 'firstpage_content')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'firstpage_footer')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'rfs_sos_details')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'scs_sos_details')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'bca_sos_details')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'rica_sos_details')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'ts_sos_details')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'err_sos_details')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'arct_sos_details')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'rfv_sos_details')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'exclusions')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'required_documents')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'payment_method')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'payment_terms')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
             <?= $form->field($model, 'timings')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'limitions_on_liabity')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'valuer_duties_supervision')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'internal_external_status_of_the_valuer')->textarea(['class'=>'editor']) ?>
          </div>    
          
          <div class="col-sm-12">
            <?= $form->field($model, 'previous_involvement_and_conflict_of_interest')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'decleration_of_independence_and_objectivity')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'assumptions_rfs')->textarea(['class'=>'editor', 'rows' => 15]) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'assumptions_scs')->textarea(['class'=>'editor', 'rows' => 15]) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'assumptions_bca')->textarea(['class'=>'editor', 'rows' => 15]) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'assumptions_rica')->textarea(['class'=>'editor', 'rows' => 15]) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'assumptions_ts')->textarea(['class'=>'editor', 'rows' => 15]) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'assumptions_err')->textarea(['class'=>'editor', 'rows' => 15]) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'assumptions_arct')->textarea(['class'=>'editor', 'rows' => 15]) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'assumptions_rfv')->textarea(['class'=>'editor', 'rows' => 15]) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'terms_versus_agreement')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'providing_the_service')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'liability')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'remuneration')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'communication')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'estimates')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'confidentiality')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'termination')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'miscellaneous')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'force_majeure')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'waiver_and_severance')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'no_responsibility')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'no_partnership')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'conflict')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'dispute_resolution')->textarea(['class'=>'editor']) ?>
          </div>
          <div class="col-sm-12">
            <?= $form->field($model, 'complaints_handling_procedure')->textarea(['class'=>'editor']) ?>
          </div>
          


          

          <div class="col-sm-12">
            <?= $form->field($model, 'acceptance_of_terms_of_engagement')->textarea(['class'=>'editor']) ?>
          </div>

           <div class="col-sm-12">
            <?= $form->field($model, 'quotation_last_paragraph')->textarea(['class'=>'editor']) ?>
          </div>

        </div>
          <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
    </div>

    <div class="card-footer bg-light">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mr-2']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</section>
