<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\components\widgets\StatusVerified;
use yii\helpers\Url;

$this->title = 'BCS Services';
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $form = ActiveForm::begin(); ?>
<section class="master-form card card-outline card-primary">
    <header class="card-header">
        <h2 class="card-title"><strong class="">Fee for BCS</strong></h2>
        <div class="card-tools">
            <a href="<?= Url::toRoute(['valuation-quotation/get-master-file-history?file_type=bcs']) ?>" target="_blank"
                class="btn btn-info btn-sm">History</a>
        </div>
    </header>

    <!--BCS Information -->
    <section class="master-form card card-outline card-success mx-4 mt-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Type of Service (Base fee) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">
                        <thead>
                            <tr>
                                <td>
                                    <strong>Reserve Fund Study</strong>
                                </td>
                                <td>
                                    <strong>Service Cost Study</strong>
                                </td>
                                <td>
                                    <strong>Building Condition Audit</strong>
                                </td>
                                <td>
                                    <strong>Rinstatement Cost</strong>
                                </td>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php

                                $bcs_type = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'bcs_type'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');


                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" min="0" step=".01"
                                            name="QuotationFeeMasterFile[bcs_type][rfs]"
                                            value="<?= (isset($bcs_type['rfs'])) ? $bcs_type['rfs'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" min="0" step=".01"
                                            name="QuotationFeeMasterFile[bcs_type][scas]"
                                            value="<?= (isset($bcs_type['scas'])) ? $bcs_type['scas'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" min="0" step=".01"
                                            name="QuotationFeeMasterFile[bcs_type][bca]"
                                            value="<?= (isset($bcs_type['bca'])) ? $bcs_type['bca'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" min="0" step=".01"
                                            name="QuotationFeeMasterFile[bcs_type][rica]"
                                            value="<?= (isset($bcs_type['rica'])) ? $bcs_type['rica'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                
                            </tr>
                        </tbody>
                        <thead>
                            <tr>
                                <td>
                                    <strong>Technical Snagging</strong>
                                </td>
                                <td>
                                    <strong>Escrow Retention Release</strong>
                                </td>
                                <td>
                                    <strong>Asset Register Creation & Tagging</strong>
                                </td>
                                <td>
                                    <strong>Reserve Fund Validation</strong>
                                </td>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php

                                $bcs_type = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'bcs_type'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');


                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" min="0" step=".01"
                                            name="QuotationFeeMasterFile[bcs_type][ts]"
                                            value="<?= (isset($bcs_type['ts'])) ? $bcs_type['ts'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" min="0" step=".01"
                                            name="QuotationFeeMasterFile[bcs_type][err]"
                                            value="<?= (isset($bcs_type['err'])) ? $bcs_type['err'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" min="0" step=".01"
                                            name="QuotationFeeMasterFile[bcs_type][arct]"
                                            value="<?= (isset($bcs_type['arct'])) ? $bcs_type['arct'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" min="0" step=".01"
                                            name="QuotationFeeMasterFile[bcs_type][rfv]"
                                            value="<?= (isset($bcs_type['rfv'])) ? $bcs_type['rfv'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>




                </div>
            </div>
        </div>
    </section>
    <!--Subject Property Information-->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Type of Subject Property (Base fee) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Building - Residential</strong>
                                </td>
                                <td>
                                    <strong>Building - Commercial</strong>
                                </td>
                                <td>
                                    <strong>Building - Mixed Use</strong>
                                </td>
                                <td>
                                    <strong>Villa Compound</strong>
                                </td>
                                <!-- <td>
                                    <strong>Townhouse - Commercial</strong>
                                </td>
                                <td>
                                    <strong>Restaurant</strong>
                                </td> -->

                                <!-- <td>
                                    <strong>Shop</strong>
                                </td>
                                <td>
                                    <strong>Warehouse </strong>
                                </td> -->


                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $subject_property = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'subject_property'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][42]"
                                            value="<?= (isset($subject_property['42'])) ? $subject_property['42'] : 0 ?>"
                                            placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][81]"
                                            value="<?= (isset($subject_property['81'])) ? $subject_property['81'] : 0 ?>"
                                            placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][3]"
                                            value="<?= (isset($subject_property['3'])) ? $subject_property['3'] : 0 ?>"
                                            placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][10]"
                                            value="<?= (isset($subject_property['10'])) ? $subject_property['10'] : 0 ?>"
                                            placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>
                                <!-- <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][11]" value="<?= (isset($subject_property['11'])) ? $subject_property['11'] : 0 ?>"
                    placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][15]" value="<?= (isset($subject_property['15'])) ? $subject_property['15'] : 0 ?>"
                    placeholder=" 1" class="form-control" required />
                                    </div>
                                </td> -->

                                <!-- <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][17]" value="<?= (isset($subject_property['17'])) ? $subject_property['17'] : 0 ?>"
                    placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][24]" value="<?= (isset($subject_property['24'])) ? $subject_property['24'] : 0 ?>"
                    placeholder=" 1" class="form-control" required />
                                    </div>
                                </td> -->



                            </tr>


                        </tbody>

                        <thead>
                            <tr>


                                <td>
                                    <strong>Labour Accommodation</strong>
                                </td>

                                <!-- <td>
                                    <strong>Office</strong>
                                </td>

                                <td>
                                    <strong>Factory</strong>
                                </td>
                                <td>
                                    <strong>Villa - Commercial</strong>
                                </td> -->

                                <td>
                                    <strong>Hospital</strong>
                                </td>
                                <td>
                                    <strong>School</strong>
                                </td>
                                <td>
                                    <strong>Hotel</strong>
                                </td>


                            </tr>
                        </thead>

                        <tbody>

                            <tr>



                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][25]"
                                            value="<?= (isset($subject_property['25'])) ? $subject_property['25'] : 0 ?>"
                                            placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>

                                <!-- <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][28]" value="<?= (isset($subject_property['28'])) ? $subject_property['28'] : 0 ?>"
                    placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][38]" value="<?= (isset($subject_property['38'])) ? $subject_property['38'] : 0 ?>"
                    placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][41]" value="<?= (isset($subject_property['41'])) ? $subject_property['41'] : 0 ?>"
                    placeholder=" 1" class="form-control" required />
                                    </div>
                                </td> -->


                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][43]"
                                            value="<?= (isset($subject_property['43'])) ? $subject_property['43'] : 0 ?>"
                                            placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][16]"
                                            value="<?= (isset($subject_property['16'])) ? $subject_property['16'] : 0 ?>"
                                            placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][9]"
                                            value="<?= (isset($subject_property['9'])) ? $subject_property['9'] : 0 ?>"
                                            placeholder=" 1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Client Information-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Client Segment(%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Bank</strong>
                                </td>
                                <td>
                                    <strong>Corporate</strong>
                                </td>
                                <td>
                                    <strong>Individual</strong>
                                </td>
                                <td>
                                    <strong>Partner</strong>
                                </td>
                                <td>
                                    <strong>Foreign Individual</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <input type="hidden" name="QuotationFeeMasterFile[approach_type]" value="bcs">
                                <?php

                                $client_type = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'client_type'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');


                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[client_type][bank]"
                                            value="<?= (isset($client_type['bank'])) ? $client_type['bank'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][corporate]"
                                            value="<?= (isset($client_type['corporate'])) ? $client_type['corporate'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][individual]"
                                            value="<?= (isset($client_type['individual'])) ? $client_type['individual'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][partner]"
                                            value="<?= (isset($client_type['partner'])) ? $client_type['partner'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][foreign-individual]"
                                            value="<?= (isset($client_type['foreign-individual'])) ? $client_type['foreign-individual'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--City Information-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>City (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Abu Dhabi</strong>
                                </td>
                                <td>
                                    <strong>Dubai</strong>
                                </td>
                                <td>
                                    <strong>Sharjah/UQ</strong>
                                </td>
                                <td>
                                    <strong>Rak, Fujairah</strong>
                                </td>
                                <td>
                                    <strong>Ajman</strong>
                                </td>
                                <td>
                                    <strong>Al Ain</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $city = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'city'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');

                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][abu_dhabi]"
                                            value="<?= (isset($city['abu_dhabi'])) ? $city['abu_dhabi'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][dubai]"
                                            value="<?= (isset($city['dubai'])) ? $city['dubai'] : 0 ?>" placeholder="1"
                                            class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][sharjah]"
                                            value="<?= (isset($city['sharjah'])) ? $city['sharjah'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][rak_fuj]"
                                            value="<?= (isset($city['rak_fuj'])) ? $city['rak_fuj'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][ajman]"
                                            value="<?= (isset($city['ajman'])) ? $city['ajman'] : 0 ?>" placeholder="1"
                                            class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][others]"
                                            value="<?= (isset($city['others'])) ? $city['others'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Tenure Information-->
    <section class="master-form card card-outline card-dark mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Tenure (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Freehold</strong>
                                </td>
                                <td>
                                    <strong>Non-Freehold</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $tenure = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'tenure'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
                                // echo "<pre>";
                                // print_r($tenure);
                                // die();
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[tenure][freehold]"
                                            value="<?= (isset($tenure['freehold'])) ? $tenure['freehold'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[tenure][non_freehold]"
                                            value="<?= (isset($tenure['non_freehold'])) ? $tenure['non_freehold'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Complexity Information-->
    <!-- <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Complexity (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Non-Standard</strong>
                                </td>
                                <td>
                                    <strong>Standard</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $complexity = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'complexity'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[complexity][non_standard]"
                                            value="<?= (isset($complexity['non_standard'])) ? $complexity['non_standard'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[complexity][standard]"
                                            value="<?= (isset($complexity['standard'])) ? $complexity['standard'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section> -->
    <!--Drawings Availability Information-->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Drawings Available ? (M,E,P, HVAC) (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Civil Drawing Available</strong>
                                </td>
                                <td>
                                    <strong>Mechanical Drawing Available</strong>
                                </td>
                                <td>
                                    <strong>Electrical Drawing Available</strong>
                                </td>
                                <td>
                                    <strong>Plumbing Drawing Available</strong>
                                </td>
                                <td>
                                    <strong>HVAC Drawing Available</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $drawings_available = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'drawings_available'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[drawings_available][civil]"
                                            value="<?= (isset($drawings_available['civil'])) ? $drawings_available['civil'] : 0 ?>"
                                            placeholder="1" class="form-control" required />

                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[drawings_available][mechanical]"
                                            value="<?= (isset($drawings_available['mechanical'])) ? $drawings_available['mechanical'] : 0 ?>"
                                            placeholder="1" class="form-control" required />

                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[drawings_available][electrical]"
                                            value="<?= (isset($drawings_available['electrical'])) ? $drawings_available['electrical'] : 0 ?>"
                                            placeholder="1" class="form-control" required />

                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[drawings_available][plumbing]"
                                            value="<?= (isset($drawings_available['plumbing'])) ? $drawings_available['plumbing'] : 0 ?>"
                                            placeholder="1" class="form-control" required />

                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[drawings_available][hvac]"
                                            value="<?= (isset($drawings_available['hvac'])) ? $drawings_available['hvac'] : 0 ?>"
                                            placeholder="1" class="form-control" required />

                                    </div>
                                </td>
                                <!-- <td>
                                    <div class="form-group">
                                        <?= $form->field($model, 'drawings_available[mechanical]')->dropDownList([
                                            '1' => 'Yes',
                                            '0' => 'No',
                                        ], [
                                            'prompt' => 'Select ',
                                            'class' => 'form-control',
                                            'required' => true,
                                            'value' => isset($drawings_available['mechanical']) ? $drawings_available['mechanical'] : null,
                                        ])->label('') ?>
                                    </div>
                                </td> -->



                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Upgrades Information-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Development Upgrades/Ratings (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>1 Star</strong>
                                </td>
                                <td>
                                    <strong>2 Star</strong>
                                </td>
                                <td>
                                    <strong>3 Star</strong>
                                </td>
                                <td>
                                    <strong>4 Star</strong>
                                </td>
                                <td>
                                    <strong>5 Star</strong>
                                </td>

                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $upgrades_ratings = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'upgrades_ratings'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
                                // echo "<pre>";
                                // print_r(upgrades_ratings);
                                // die();
                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][1]"
                                            value="<?= (isset($upgrades_ratings['1'])) ? $upgrades_ratings['1'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][2]"
                                            value="<?= (isset($upgrades_ratings['2'])) ? $upgrades_ratings['2'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][3]"
                                            value="<?= (isset($upgrades_ratings['3'])) ? $upgrades_ratings['3'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][4]"
                                            value="<?= (isset($upgrades_ratings['4'])) ? $upgrades_ratings['4'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][5]"
                                            value="<?= (isset($upgrades_ratings['5'])) ? $upgrades_ratings['5'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>


                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Land size Information-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Standard Land size (only applicable on land valuation) (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>1 - 5000</strong>
                                </td>
                                <td>
                                    <strong>5001 - 10000</strong>
                                </td>
                                <td>
                                    <strong>10001 - 20000</strong>
                                </td>
                                <td>
                                    <strong>20001 - 40000</strong>
                                </td>
                                <td>
                                    <strong>40001 - 75000</strong>
                                </td>
                                <td>
                                    <strong>Above 75000</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $land = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'land'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
                                // echo "<pre>";
                                // print_r($land);
                                // die();
                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][1_5000]"
                                            value="<?= (isset($land['1_5000'])) ? $land['1_5000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][5001_10000]"
                                            value="<?= (isset($land['5001_10000'])) ? $land['5001_10000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][10001_20000]"
                                            value="<?= (isset($land['10001_20000'])) ? $land['10001_20000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][20001_40000]"
                                            value="<?= (isset($land['20001_40000'])) ? $land['20001_40000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][40001_75000]"
                                            value="<?= (isset($land['40001_75000'])) ? $land['40001_75000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][above_75000]"
                                            value="<?= (isset($land['above_75000'])) ? $land['above_75000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Built Up Area of subject property-->
    <section class="master-form card card-outline card-success mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Built Up Area of subject property</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>1-10000</strong>
                                </td>
                                <td>
                                    <strong>10001-25000</strong>
                                </td>
                                <td>
                                    <strong>25001 - 50000</strong>
                                </td>
                                <td>
                                    <strong>50,001 - 100000</strong>
                                </td>
                                <td>
                                    <strong>100001-250000</strong>
                                </td>
                                <td>
                                    <strong>250001-500000</strong>
                                </td>
                                <td>
                                    <strong>500001-1000000</strong>
                                </td>
                                <td>
                                    <strong>1000000+</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $BUA = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'built_up_area_of_subject_property'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
                                // echo "<pre>";
                                // print_r($BUA);
                                // die();
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][1_10000]"
                                            value="<?= (isset($BUA['1_10000'])) ? $BUA['1_10000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][10001_25000]"
                                            value="<?= (isset($BUA['10001_25000'])) ? $BUA['10001_25000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][25001_50000]"
                                            value="<?= (isset($BUA['25001_50000'])) ? $BUA['25001_50000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][50001_100000]"
                                            value="<?= (isset($BUA['50001_100000'])) ? $BUA['50001_100000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][100001_250000]"
                                            value="<?= (isset($BUA['100001_250000'])) ? $BUA['100001_250000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][250001_500000]"
                                            value="<?= (isset($BUA['250001_500000'])) ? $BUA['250001_500000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][500001_1000000]"
                                            value="<?= (isset($BUA['500001_1000000'])) ? $BUA['500001_1000000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][above_1000000]"
                                            value="<?= (isset($BUA['above_1000000'])) ? $BUA['above_1000000'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Built Up Area of subject property for Building Land-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number of units in the subject property</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>1-10</strong>
                                </td>
                                <td>
                                    <strong>11-25</strong>
                                </td>
                                <td>
                                    <strong>26-50</strong>
                                </td>
                                <td>
                                    <strong>51-100</strong>
                                </td>
                                <td>
                                    <strong>101-250</strong>
                                </td>
                                <td>
                                    <strong>251-500</strong>
                                </td>
                                <td>
                                    <strong>501-1000</strong>
                                </td>
                                <td>
                                    <strong>>1000+</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $no_of_units_in_building = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'number_of_units_building'])->all(), 'sub_heading', 'values');
                                // echo "<pre>";
                                // print_r($no_of_units_in_building);
                                // die();
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][1]"
                                            value="<?= (isset($no_of_units_in_building['1'])) ? $no_of_units_in_building['1'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][2]"
                                            value="<?= (isset($no_of_units_in_building['2'])) ? $no_of_units_in_building['2'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][3]"
                                            value="<?= (isset($no_of_units_in_building['3'])) ? $no_of_units_in_building['3'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][4]"
                                            value="<?= (isset($no_of_units_in_building['4'])) ? $no_of_units_in_building['4'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][5]"
                                            value="<?= (isset($no_of_units_in_building['5'])) ? $no_of_units_in_building['5'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][6]"
                                            value="<?= (isset($no_of_units_in_building['6'])) ? $no_of_units_in_building['6'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][7]"
                                            value="<?= (isset($no_of_units_in_building['7'])) ? $no_of_units_in_building['7'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][8]"
                                            value="<?= (isset($no_of_units_in_building['8'])) ? $no_of_units_in_building['8'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <?php /* ?>
<section class="master-form card card-outline card-danger mx-4">

<header class="card-header">
<h2 class="card-title"><strong>Types of units in the subject property</strong></h2>
</header>

<div class="card-body">
<div class="row justify-content-md-center">
<div class="col-sm-12">

<table class="table table-striped table-condensed table-hover">

<thead>
<tr>
<td>
  <strong>1 type</strong>
</td>
<td>
  <strong>2 types</strong>
</td>
<td>
  <strong>3 types</strong>
</td>
<td>
  <strong>4 types</strong>
</td>
<td>
  <strong>5 types</strong>
</td>
<td>
  <strong>6 types</strong>
</td>
<td>
  <strong>7 types</strong>
</td>
<td>
  <strong>8 types</strong>
</td>
<td>
  <strong>9 types</strong>
</td>
<td>
  <strong>10 types</strong>
</td>
</tr>
</thead>

<tbody>

<tr>
<?php
$no_of_units_in_building = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
  ->where(['heading' => 'number_of_types'])->all(), 'sub_heading', 'values');
// echo "<pre>";
// print_r($no_of_units_in_building);
// die();
?>

<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_types][1]"
             value="<?= (isset($no_of_units_in_building['1'])) ? $no_of_units_in_building['1'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_types][2]"
             value="<?= (isset($no_of_units_in_building['2'])) ? $no_of_units_in_building['2'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_types][3]"
             value="<?= (isset($no_of_units_in_building['3'])) ? $no_of_units_in_building['3'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_types][4]"
             value="<?= (isset($no_of_units_in_building['4'])) ? $no_of_units_in_building['4'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_types][5]"
             value="<?= (isset($no_of_units_in_building['5'])) ? $no_of_units_in_building['5'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_types][6]"
             value="<?= (isset($no_of_units_in_building['6'])) ? $no_of_units_in_building['6'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_types][7]"
             value="<?= (isset($no_of_units_in_building['7'])) ? $no_of_units_in_building['7'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_types][8]"
             value="<?= (isset($no_of_units_in_building['8'])) ? $no_of_units_in_building['8'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_types][9]"
             value="<?= (isset($no_of_units_in_building['9'])) ? $no_of_units_in_building['9'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_types][10]"
             value="<?= (isset($no_of_units_in_building['10'])) ? $no_of_units_in_building['10'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>


</tr>

</tbody>
</table>
</div>
</div>
</div>
</section>
<?php */?>

    <!--Number of Comparables-->
    <?php /* ?>
<section class="master-form card card-outline card-info mx-4">

<header class="card-header">
<h2 class="card-title"><strong>Number of Comparables (%) (+)</strong></h2>
</header>

<div class="card-body">
<div class="row justify-content-md-center">
<div class="col-sm-12">

<table class="table table-striped table-condensed table-hover">

<thead>
<tr>
<td>
  <strong>Less Than 3</strong>
</td>
<td>
  <strong>3 - 5</strong>
</td>
<td>
  <strong>6 - 10</strong>
</td>
<td>
  <strong>11 - 15</strong>
</td>
<td>
  <strong>Above 15</strong>
</td>
</tr>
</thead>

<tbody>

<tr>
<?php
$no_of_Comparables = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
  ->where(['heading' => 'number_of_comparables_data'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
// echo "<pre>";
// print_r($no_of_Comparables);
// die();
?>

<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_comparables][less_than_3]"
             value="<?= (isset($no_of_Comparables['less_than_3'])) ? $no_of_Comparables['less_than_3'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_comparables][3_5]"
             value="<?= (isset($no_of_Comparables['3_5'])) ? $no_of_Comparables['3_5'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_comparables][6_10]"
             value="<?= (isset($no_of_Comparables['6_10'])) ? $no_of_Comparables['6_10'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>

<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_comparables][11_15]"
             value="<?= (isset($no_of_Comparables['11_15'])) ? $no_of_Comparables['11_15'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[number_of_comparables][above_15]"
             value="<?= (isset($no_of_Comparables['above_15'])) ? $no_of_Comparables['above_15'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>

</tr>

</tbody>
</table>
</div>
</div>
</div>
</section>
<?php */?>

    <!--Other intended users-->
    <?php /* ?>
<section class="master-form card card-outline card-secondary mx-4">

<header class="card-header">
<h2 class="card-title"><strong>Other intended users</strong></h2>
</header>

<div class="card-body">
<div class="row justify-content-md-center">
<div class="col-sm-12">

<table class="table table-striped table-condensed table-hover">

<thead>
<tr>
<td>
  <strong>Yes</strong>
</td>
<td>
  <strong>No</strong>
</td>
</tr>
</thead>

<tbody>

<tr>
<?php
$other_intended_users = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
  ->where(['heading' => 'other_intended_users'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
?>

<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[other_intended_users][yes]"
             value="<?= (isset($other_intended_users['yes'])) ? $other_intended_users['yes'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>
<td>
  <div class="form-group">
      <input type="number" step=".01"
             name="QuotationFeeMasterFile[other_intended_users][no]"
             value="<?= (isset($other_intended_users['no'])) ? $other_intended_users['no'] : 0 ?>"
             placeholder="1" class="form-control"
             required
      />
  </div>
</td>

</tr>

</tbody>
</table>
</div>
</div>
</div>
</section>
<?php */?>

    <!--Urgency Fee-->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Timing(Urgency Fee) (%)(+)</strong></h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Standard</strong>
                                </td>
                                <td>
                                    <strong>Urgent Same Week</strong>
                                </td>
                                <td>
                                    <strong>Urgent 2 Weeks</strong>
                                </td>
                                <td>
                                    <strong>Urgent 3 Weeks</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $urgency_fee = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'urgency_fee'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[urgency_fee][standard]"
                                            value="<?= (isset($urgency_fee['standard'])) ? $urgency_fee['standard'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[urgency_fee][urgent_same_week]"
                                            value="<?= (isset($urgency_fee['urgent_same_week'])) ? $urgency_fee['urgent_same_week'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[urgency_fee][urgent_2_weeks]"
                                            value="<?= (isset($urgency_fee['urgent_2_weeks'])) ? $urgency_fee['urgent_2_weeks'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[urgency_fee][urgent_3_weeks]"
                                            value="<?= (isset($urgency_fee['urgent_3_weeks'])) ? $urgency_fee['urgent_3_weeks'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
    <!--Number of properties discount-->
    <section class="master-form card card-outline card-warning mx-4">
        <header class="card-header">
            <h2 class="card-title"><strong>Number Of Assignment Discount (On total fee of all properties)(%)(-)</strong>
            </h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>1</strong>
                                </td>
                                <td>
                                    <strong>2</strong>
                                </td>
                                <td>
                                    <strong>3</strong>
                                </td>
                                <td>
                                    <strong>4</strong>
                                </td>
                                <td>
                                    <strong>5</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $no_of_assignment_discount = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'no_of_assignment_discount'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
                                // echo "<pre>";
                                // print_r($no_of_property_dis);
                                // die();
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_assignment_discount][1]"
                                            value="<?= (isset($no_of_assignment_discount['1'])) ? $no_of_assignment_discount['1'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_assignment_discount][2]"
                                            value="<?= (isset($no_of_assignment_discount['2'])) ? $no_of_assignment_discount['2'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_assignment_discount][3]"
                                            value="<?= (isset($no_of_assignment_discount['3'])) ? $no_of_assignment_discount['3'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_assignment_discount][4]"
                                            value="<?= (isset($no_of_assignment_discount['4'])) ? $no_of_assignment_discount['4'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_assignment_discount][5]"
                                            value="<?= (isset($no_of_assignment_discount['5'])) ? $no_of_assignment_discount['5'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>
                        </tbody>
                        <thead>
                            <tr>

                                <td>
                                    <strong>6 - 10</strong>
                                </td>
                                <td>
                                    <strong>11 - 20</strong>
                                </td>
                                <td>
                                    <strong>21 - 50</strong>
                                </td>
                                <td>
                                    <strong>51 - 100</strong>
                                </td>
                                <td>
                                    <strong> >100 </strong>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_assignment_discount][6]"
                                            value="<?= (isset($no_of_assignment_discount['6'])) ? $no_of_assignment_discount['6'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_assignment_discount][7]"
                                            value="<?= (isset($no_of_assignment_discount['7'])) ? $no_of_assignment_discount['7'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_assignment_discount][8]"
                                            value="<?= (isset($no_of_assignment_discount['8'])) ? $no_of_assignment_discount['8'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_assignment_discount][9]"
                                            value="<?= (isset($no_of_assignment_discount['9'])) ? $no_of_assignment_discount['9'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_assignment_discount][10]"
                                            value="<?= (isset($no_of_assignment_discount['10'])) ? $no_of_assignment_discount['10'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Number of units in the same building-->
    <section class="master-form card card-outline card-warning mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number Of Building In The Same Community Discount (on total fee of all
                    properties) (%)(-)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>1 - (One)</strong>
                                </td>
                                <td>
                                    <strong>2 - (Two)</strong>
                                </td>
                                <td>
                                    <strong>3 - (Three)</strong>
                                </td>
                                <td>
                                    <strong>4 - (Four)</strong>
                                </td>
                                <td>
                                    <strong>5 - (Five)</strong>
                                </td>
                                <td>
                                    <strong> >5 - (More than five)</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $no_of_buildings_same_community_discount = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'no_of_buildings_same_community_discount'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');

                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_buildings_same_community_discount][1]"
                                            value="<?= (isset($no_of_buildings_same_community_discount['1'])) ? $no_of_buildings_same_community_discount['1'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_buildings_same_community_discount][2]"
                                            value="<?= (isset($no_of_buildings_same_community_discount['2'])) ? $no_of_buildings_same_community_discount['2'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_buildings_same_community_discount][3]"
                                            value="<?= (isset($no_of_buildings_same_community_discount['3'])) ? $no_of_buildings_same_community_discount['3'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_buildings_same_community_discount][4]"
                                            value="<?= (isset($no_of_buildings_same_community_discount['4'])) ? $no_of_buildings_same_community_discount['4'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_buildings_same_community_discount][5]"
                                            value="<?= (isset($no_of_buildings_same_community_discount['5'])) ? $no_of_buildings_same_community_discount['5'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_buildings_same_community_discount][6]"
                                            value="<?= (isset($no_of_buildings_same_community_discount['6'])) ? $no_of_buildings_same_community_discount['6'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--New / Repeat/ Revalidation Client-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>New / Repeat/ Revalidation Client Discount (per property) (%)(-)</strong>
            </h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>New Client</strong>
                                </td>
                                <td>
                                    <strong>Repeat Client</strong>
                                </td>
                                <td>
                                    <strong>Bank Revalidation</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $new_repeat_valuation = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'new_repeat_valuation_data'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][new_client]"
                                            value="<?= (isset($new_repeat_valuation['new_client'])) ? $new_repeat_valuation['new_client'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][repeat_client]"
                                            value="<?= (isset($new_repeat_valuation['repeat_client'])) ? $new_repeat_valuation['repeat_client'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][bank_revalidation]"
                                            value="<?= (isset($new_repeat_valuation['bank_revalidation'])) ? $new_repeat_valuation['bank_revalidation'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Advance Payment-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Advance Payment</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>100%</strong>
                                </td>
                                <td>
                                    <strong>75%</strong>
                                </td>
                                <td>
                                    <strong>50%</strong>
                                </td>
                                <td>
                                    <strong>25%</strong>
                                </td>
                                <td>
                                    <strong>0%</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $payment = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'payment_terms'])->andWhere(['approach_type' => 'bcs'])->all(), 'sub_heading', 'values');

                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][100]"
                                            value="<?= (isset($payment['100'])) ? $payment['100'] : 0 ?>"
                                            placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[payment_terms][75]"
                                            value="<?= (isset($payment['75'])) ? $payment['75'] : 0 ?>" placeholder="1"
                                            class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[payment_terms][50]"
                                            value="<?= (isset($payment['50'])) ? $payment['50'] : 0 ?>" placeholder="1"
                                            class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[payment_terms][25]"
                                            value="<?= (isset($payment['25'])) ? $payment['25'] : 0 ?>" placeholder="1"
                                            class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[payment_terms][0]"
                                            value="<?= (isset($payment['0'])) ? $payment['0'] : 0 ?>" placeholder="1"
                                            class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="row mx-3">
        <div class="col">
            <?= StatusVerified::widget(['model' => $model, 'form' => $form]) ?>
        </div>
    </div>

    <?php
    $latestUpdatedUser = '';
    $query = \app\models\History::find()->where(['rec_type' => 'masterfile', 'file_type' => 'bcs'])->orderBy(['id' => SORT_DESC])->one();
    if ($query <> null) {
        $latestUpdatedUser = $query->user->firstname . ' ' . $query->user->lastname;
    }
    ?>

    <div class="card-footer bg-light">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mx-2']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if ($latestUpdatedUser <> null) {
            ?>
            <span class="badge grid-badge badge-dark mx-2"><mark>Last Updated By
                    <?= $latestUpdatedUser ?>
                </mark></span>
            <?php
        }
        ?>
    </div>
</section>

<?php ActiveForm::end(); ?>