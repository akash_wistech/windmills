<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>

<?php $form = ActiveForm::begin(); ?>
<section class="master-form card card-outline card-primary">
  <header class="card-header">
      <h2 class="card-title"><strong class="">Proposal Master File</strong></h2>
  </header>

  <!-- 1- Client Type -->
  <!-- Client type -->
<section class="master-form card card-outline card-secondary mx-4 my-4">

      <header class="card-header">
          <h2 class="card-title"><strong>Client Type</strong></h2>
      </header>

      <div class="card-body">
          <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>Bank</strong>
                        </td>
                        <td>
                            <strong>Corporate</strong>
                        </td>
                        <td>
                            <strong>Individual</strong>
                        </td>
                        <td>
                            <strong>Partner</strong>
                        </td>
                        <td>
                            <strong>Foreign Individual</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                      <?php
                      $client_type =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                        ->where(['heading' => 'Client Type'])->all(), 'sub_heading', 'values');
            // echo "<pre>";
            // print_r($client_type);
            // die();
                        ?>
                        <td>
                          <div class="form-group">
                              <input  type="number" step=".01"
                              name="ProposalMasterFile[client_type][bank]"
                              value="<?= (isset($client_type['bank'])) ? $client_type['bank'] : 0 ?>"
                              placeholder="1" class="form-control"
                              required/>
                          </div>
                      </td>
                      <td>
                          <div class="form-group">
                              <input  type="number" step=".01"
                              name="ProposalMasterFile[client_type][corporate]"
                              value="<?= (isset($client_type['corporate'])) ? $client_type['corporate'] : 0 ?>"
                              placeholder="1" class="form-control"
                              required/>
                          </div>
                      </td>
                      <td>
                          <div class="form-group">
                              <input  type="number" step=".01"
                              name="ProposalMasterFile[client_type][individual]"
                              value="<?= (isset($client_type['individual'])) ? $client_type['individual'] : 0 ?>"
                              placeholder="1" class="form-control"
                              required/>
                          </div>
                      </td>
                      <td>
                          <div class="form-group">
                              <input  type="number" step=".01"
                              name="ProposalMasterFile[client_type][partner]"
                              value="<?= (isset($client_type['partner'])) ? $client_type['partner'] : 0 ?>"
                              placeholder="1" class="form-control"
                              required/>
                          </div>
                      </td>
                      <td>
                          <div class="form-group">
                              <input  type="number" step=".01"
                              name="ProposalMasterFile[client_type][foreign-individual]"
                              value="<?= (isset($client_type['foreign-individual'])) ? $client_type['foreign-individual'] : 0 ?>"
                              placeholder="1" class="form-control"
                              required/>
                          </div>
                      </td>
                  </tr>

              </tbody>
          </table>
      </div>
  </div>
</div>
</section>

<!-- 2- Subject Property -->
<section class="master-form card card-outline card-success mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Type of Subject Property</strong></h2>
    </header>

    <div class="card-body">
      <div class="row justify-content-md-center">
        <div class="col-sm-12">

          <table class="table table-striped table-condensed table-hover">

            <thead>
                <tr>
                    <td>
                        <strong>Land</strong>
                    </td>
                    <td>
                        <strong>Apartment</strong>
                    </td>
                    <td>
                        <strong>Office</strong>
                    </td>
                    <td>
                        <strong>Shop</strong>
                    </td>
                    <td>
                        <strong>Villa</strong>
                    </td>
                    <td>
                        <strong>Warehouse</strong>
                    </td>
                    <td>
                        <strong>Building Land</strong>
                    </td>
                    <td>
                        <strong>Mixed Use</strong>
                    </td>
                </tr>
            </thead>

            <tbody>

              <tr>
                <?php
                $subject_property =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                  ->where(['heading' => 'Subject Property'])->all(), 'sub_heading', 'values');
                  // echo "<pre>";
                  // print_r($subject_property);
                  // echo "</pre>";
                  // die();
                  ?>
                  <td>
                    <div class="form-group">
                        <input  type="number" step=".01"
                        name="ProposalMasterFile[subject_property][Land]"
                        value="<?= (isset($subject_property['Land'])) ? $subject_property['Land'] : 0 ?>"
                        placeholder="1" class="form-control"
                        required
                        />
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input  type="number" step=".01"
                        name="ProposalMasterFile[subject_property][Apartment]"
                        value="<?= (isset($subject_property['Apartment'])) ? $subject_property['Apartment'] : 0 ?>"
                        placeholder="1" class="form-control"
                        required
                        />
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input  type="number" step=".01"
                        name="ProposalMasterFile[subject_property][Office]"
                        value="<?= (isset($subject_property['Office'])) ? $subject_property['Office'] : 0 ?>"
                        placeholder="1" class="form-control"
                        required
                        />
                    </div>
                </td>
                 <td>
                    <div class="form-group">
                        <input  type="number" step=".01"
                        name="ProposalMasterFile[subject_property][Shop]"
                        value="<?= (isset($subject_property['Shop'])) ? $subject_property['Shop'] : 0 ?>"
                        placeholder="1" class="form-control"
                        required
                        />
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input  type="number" step=".01"
                        name="ProposalMasterFile[subject_property][Villa]"
                        value="<?= (isset($subject_property['Villa'])) ? $subject_property['Villa'] : 0 ?>"
                        placeholder="1" class="form-control"
                        required
                        />
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input  type="number" step=".01"
                        name="ProposalMasterFile[subject_property][Warehouse]"
                        value="<?= (isset($subject_property['Warehouse'])) ? $subject_property['Warehouse'] : 0 ?>"
                        placeholder="1" class="form-control"
                        required
                        />
                    </div>
                </td>
                 <td>
                    <div class="form-group">
                        <input  type="number" step=".01"
                        name="ProposalMasterFile[subject_property][others]"
                        value="<?= (isset($subject_property['others'])) ? $subject_property['others'] : 0 ?>"
                        placeholder="1" class="form-control"
                        required
                        />
                    </div>
                </td>
                  <td>
                      <div class="form-group">
                          <input  type="number" step=".01"
                                  name="ProposalMasterFile[subject_property][Mixed]"
                                  value="<?= (isset($subject_property['Mixed'])) ? $subject_property['Mixed'] : 0 ?>"
                                  placeholder="1" class="form-control"
                                  required
                          />
                      </div>
                  </td>

            </tr>


        </tbody>
    </table>
</div>
</div>
</div>
</section>

<!-- 3- City -->
<section class="master-form card card-outline card-danger mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>City</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>Abu Dhabi</strong>
                        </td>
                        <td>
                            <strong>Dubai</strong>
                        </td>
                        <td>
                            <strong>Sharjah/UQ</strong>
                        </td>
                        <td>
                            <strong>Rak, Fuj</strong>
                        </td>
                        <td>
                            <strong>Ajman</strong>
                        </td>
                        <td>
                            <strong>Others</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $city =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'City'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($city);
                          // die();
                      ?>
                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[city][Abu Dhabi]"
                            value="<?= (isset($city['Abu Dhabi'])) ? $city['Abu Dhabi'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[city][dubai]"
                            value="<?= (isset($city['dubai'])) ? $city['dubai'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[city][sharjah]"
                            value="<?= (isset($city['sharjah'])) ? $city['sharjah'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[city][rak-fuj]"
                            value="<?= (isset($city['rak-fuj'])) ? $city['rak-fuj'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[city][ajman]"
                            value="<?= (isset($city['ajman'])) ? $city['ajman'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                     <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[city][others]"
                            value="<?= (isset($city['others'])) ? $city['others'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>


            </tbody>
        </table>
    </div>
</div>
</div>
</section>

<!-- 5- Payment Terms -->
<section class="master-form card card-outline card-info mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Payment Terms</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>100%</strong>
                        </td>
                        <td>
                            <strong>75%</strong>
                        </td>
                        <td>
                            <strong>50%</strong>
                        </td>
                        <td>
                            <strong>25%</strong>
                        </td>
                        <td>
                            <strong>0%</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $payment =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Payment Terms'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($payment);
                          // die();
                      ?>
                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[payment_terms][100%]"
                            value="<?= (isset($payment['100%'])) ? $payment['100%'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[payment_terms][75%]"
                            value="<?= (isset($payment['75%'])) ? $payment['75%'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[payment_terms][50%]"
                            value="<?= (isset($payment['50%'])) ? $payment['50%'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[payment_terms][25%]"
                            value="<?= (isset($payment['25%'])) ? $payment['25%'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[payment_terms][0%]"
                            value="<?= (isset($payment['0%'])) ? $payment['0%'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>


<!-- 6- Tenure -->
<section class="master-form card card-outline card-dark mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Tenure</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>Freehold</strong>
                        </td>
                        <td>
                            <strong>Non-Freehold</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $tenure =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Tenure'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($tenure);
                          // die();
                      ?>

                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[tenure][Freehold]"
                            value="<?= (isset($tenure['Freehold'])) ? $tenure['Freehold'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[tenure][Non-Freehold]"
                            value="<?= (isset($tenure['Non-Freehold'])) ? $tenure['Non-Freehold'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
    </div>
    </div>
</section>

<!-- 7- Complexity -->
<section class="master-form card card-outline card-primary mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Complexity</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>Non-Standard</strong>
                        </td>
                        <td>
                            <strong>Standard</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $complexity =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Complexity'])->all(), 'sub_heading', 'values');
                    //       echo "<pre>";
                    //       print_r($complexity);
                    //       die();
                    // ?>

                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[complexity][non-standard]"
                            value="<?= (isset($complexity['non-standard'])) ? $complexity['non-standard'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[complexity][standard]"
                            value="<?= (isset($complexity['standard'])) ? $complexity['standard'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>

<!-- 8- New / Repeat Valuation -->
<section class="master-form card card-outline card-secondary mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>New / Repeat Valuation</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>New Valuation</strong>
                        </td>
                        <td>
                            <strong>Repeat/Revalidation</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $new_repeat_valuation =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'New Repeat Valuation Data'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($new_repeat_valuation);
                          // die();
                      ?>

                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[new_repeat_valuation][new-valuation]"
                            value="<?= (isset($new_repeat_valuation['new-valuation'])) ? $new_repeat_valuation['new-valuation'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[new_repeat_valuation][repeat-valuation]"
                            value="<?= (isset($new_repeat_valuation['repeat-valuation'])) ? $new_repeat_valuation['repeat-valuation'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
    </div>
    </div>
</section>

<!-- 9- Built Up Area of subject property -->
<section class="master-form card card-outline card-success mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Built Up Area of subject property</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>1 - 2,000</strong>
                        </td>
                        <td>
                            <strong>2,001 - 4,000</strong>
                        </td>
                        <td>
                            <strong>4,001 - 6,000</strong>
                        </td>
                        <td>
                            <strong>6,001 - 8,000</strong>
                        </td>
                        <td>
                            <strong>8,001 - 10,000</strong>
                        </td>
                        <td>
                            <strong>10,000+</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $BUA =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Build up Area of Subject Property'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($BUA);
                          // die();
                      ?>

                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[built_up_area_of_subject_property][1 - 2000]"
                            value="<?= (isset($BUA['1 - 2000'])) ? $BUA['1 - 2000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                      <td>
                          <div class="form-group">
                              <input  type="number" step=".01"
                                      name="ProposalMasterFile[built_up_area_of_subject_property][2001 - 4000]"
                                      value="<?= (isset($BUA['2001 - 4000'])) ? $BUA['2001 - 4000'] : 0 ?>"
                                      placeholder="1" class="form-control"
                                      required
                              />
                          </div>
                      </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[built_up_area_of_subject_property][4001 - 6000]"
                            value="<?= (isset($BUA['4001 - 6000'])) ? $BUA['4001 - 6000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[built_up_area_of_subject_property][6001 - 8000]"
                            value="<?= (isset($BUA['6001 - 8000'])) ? $BUA['6001 - 8000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                     <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[built_up_area_of_subject_property][8001 - 10000]"
                            value="<?= (isset($BUA['8001 - 10000'])) ? $BUA['8001 - 10000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                     <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[built_up_area_of_subject_property][above 10000]"
                            value="<?= (isset($BUA['above 10000'])) ? $BUA['above 10000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>

<!-- 11- Type of valuation -->
<section class="master-form card card-outline card-warning mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Type of valuation</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>Desktop</strong>
                        </td>
                        <td>
                            <strong>Physical Inspection</strong>
                        </td>
                        <td>
                            <strong>Drive by</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $type_of_valuation =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Type of Valuation'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($type_of_valuation);
                          // die();
                      ?>

                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[type_of_valuation][desktop]"
                            value="<?= (isset($type_of_valuation['desktop'])) ? $type_of_valuation['desktop'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[type_of_valuation][physical-inspection]"
                            value="<?= (isset($type_of_valuation['physical-inspection'])) ? $type_of_valuation['physical-inspection'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[type_of_valuation][drive-by]"
                            value="<?= (isset($type_of_valuation['drive-by'])) ? $type_of_valuation['drive-by'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>

<!-- 12- Number of Comparables -->
<section class="master-form card card-outline card-info mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Number of Comparables</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>Less Than 3</strong>
                        </td>
                        <td>
                            <strong>3 - 5</strong>
                        </td>
                        <td>
                            <strong>6 - 10</strong>
                        </td>
                        <td>
                            <strong>11 - 15</strong>
                        </td>
                        <td>
                            <strong>Above 15</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $no_of_Comparables =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Number of Comparables Data'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($no_of_Comparables);
                          // die();
                      ?>

                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_comparables][lessThan-3]"
                            value="<?= (isset($no_of_Comparables['lessThan-3'])) ? $no_of_Comparables['lessThan-3'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_comparables][3-5]"
                            value="<?= (isset($no_of_Comparables['3-5'])) ? $no_of_Comparables['3-5'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_comparables][6-10]"
                            value="<?= (isset($no_of_Comparables['6-10'])) ? $no_of_Comparables['6-10'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_comparables][11-15]"
                            value="<?= (isset($no_of_Comparables['11-15'])) ? $no_of_Comparables['11-15'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_comparables][above-15]"
                            value="<?= (isset($no_of_Comparables['above-15'])) ? $no_of_Comparables['above-15'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>


<!-- 10- Number of Units on the land -->
<section class="master-form card card-outline card-danger mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Upgrades Ratings</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>Low</strong>
                        </td>
                        <td>
                            <strong>Medium</strong>
                        </td>
                        <td>
                            <strong>High</strong>
                        </td>

                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $number_of_units_land =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Number of Units Land'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($number_of_units_land);
                          // die();
                      ?>
                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_units_land][1]"
                            value="<?= (isset($number_of_units_land['1'])) ? $number_of_units_land['1'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_units_land][2]"
                            value="<?= (isset($number_of_units_land['2'])) ? $number_of_units_land['2'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_units_land][3]"
                            value="<?= (isset($number_of_units_land['3'])) ? $number_of_units_land['3'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>



                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>

<!-- 16- Land size -->
<section class="master-form card card-outline card-danger mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Standard Land size (only applicable on land valuation)</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>1 - 5,000</strong>
                        </td>
                        <td>
                            <strong>5,001 - 7,500</strong>
                        </td>
                        <td>
                            <strong>7,501 - 10,000</strong>
                        </td>
                        <td>
                            <strong>10,001 - 15,000</strong>
                        </td>
                         <td>
                            <strong>15,00 - 25,000</strong>
                        </td>
                         <td>
                            <strong>Above 25,000</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $land =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Land'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($land);
                          // die();
                      ?>
                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[land][1-5000]"
                            value="<?= (isset($land['1-5000'])) ? $land['1-5000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                      <td>
                          <div class="form-group">
                              <input  type="number" step=".01"
                                      name="ProposalMasterFile[land][5001-7500]"
                                      value="<?= (isset($land['5001-7500'])) ? $land['5001-7500'] : 0 ?>"
                                      placeholder="1" class="form-control"
                                      required
                              />
                          </div>
                      </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[land][7501-10000]"
                            value="<?= (isset($land['7501-10000'])) ? $land['7501-10000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[land][10001-15000]"
                            value="<?= (isset($land['10001-15000'])) ? $land['10001-15000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[land][15001-25000]"
                            value="<?= (isset($land['15001-25000'])) ? $land['15001-25000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[land][above-25000]"
                            value="<?= (isset($land['above-25000'])) ? $land['above-25000'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>

<!-- 10- Number of Units on the building -->
<section class="master-form card card-outline card-danger mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Number of units in the building (only applicable on building valuation)</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>1-20 units</strong>
                        </td>
                        <td>
                            <strong>21 - 50 units</strong>
                        </td>
                        <td>
                            <strong>50 - 100 units</strong>
                        </td>
                         <td>
                            <strong>Greater Than 100 units</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $no_of_units_in_building =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'No Of Units Building'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($no_of_units_in_building);
                          // die();
                      ?>

                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_units_building][1-20 units]"
                            value="<?= (isset($no_of_units_in_building['1-20 units'])) ? $no_of_units_in_building['1-20 units'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_units_building][21-50 units]"
                            value="<?= (isset($no_of_units_in_building['21-50 units'])) ? $no_of_units_in_building['21-50 units'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_units_building][50-100 units]"
                            value="<?= (isset($no_of_units_in_building['50-100 units'])) ? $no_of_units_in_building['50-100 units'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_units_building][GreaterThan 100 units]"
                            value="<?= (isset($no_of_units_in_building['GreaterThan 100 units'])) ? $no_of_units_in_building['GreaterThan 100 units'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>


<!-- 4- Number of properties(discount) -->
<section class="master-form card card-outline card-warning mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Number of properties (discount (%))</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>1 - (One)</strong>
                        </td>
                        <td>
                            <strong>2 - (Two)</strong>
                        </td>
                        <td>
                            <strong>3 - (Three)</strong>
                        </td>
                        <td>
                            <strong>4 - (Four)</strong>
                        </td>
                        <td>
                            <strong>5 - (Five)</strong>
                        </td>
                        <td>
                            <strong> >5 - (More than five)</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $no_of_property_dis =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'No Of Property Discount'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($no_of_property_dis);
                          // die();
                      ?>

                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[no_of_property_dis][1]"
                            value="<?= (isset($no_of_property_dis['1'])) ? $no_of_property_dis['1'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[no_of_property_dis][2]"
                            value="<?= (isset($no_of_property_dis['2'])) ? $no_of_property_dis['2'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[no_of_property_dis][3]"
                            value="<?= (isset($no_of_property_dis['3'])) ? $no_of_property_dis['3'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[no_of_property_dis][4]"
                            value="<?= (isset($no_of_property_dis['4'])) ? $no_of_property_dis['4'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[no_of_property_dis][5]"
                            value="<?= (isset($no_of_property_dis['5'])) ? $no_of_property_dis['5'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                      <td>
                          <div class="form-group">
                              <input  type="number" step=".01"
                                      name="ProposalMasterFile[no_of_property_dis][6]"
                                      value="<?= (isset($no_of_property_dis['6'])) ? $no_of_property_dis['6'] : 0 ?>"
                                      placeholder="1" class="form-control"
                                      required
                              />
                          </div>
                      </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>
    <!-- first time discount -->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>First time Valuation Discount (%)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>First time Valuation Discount</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $first_time_discount =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                                ->where(['heading' => 'First Time Discount'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($baseFeeOthers);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="ProposalMasterFile[first_time_discount][first-time-discount]"
                                            value="<?= (isset($first_time_discount['first-time-discount'])) ? $first_time_discount['first-time-discount'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- General discount -->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>General Discount (%)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>General Discount</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $general_discount =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                                ->where(['heading' => 'General Discount'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="ProposalMasterFile[general_discount][general-discount]"
                                            value="<?= (isset($general_discount['general-discount'])) ? $general_discount['general-discount'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- 14- Base Fee of Buildings-->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Base Fee of Buildings</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Base Fee of Buildings</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $baseFeeBuilding =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                                ->where(['heading' => 'Base Fee Building'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($baseFeeBuilding);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="ProposalMasterFile[base_fee_building][base-fee-building]"
                                            value="<?= (isset($baseFeeBuilding['base-fee-building'])) ? $baseFeeBuilding['base-fee-building'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

<!-- 14- Base Fee of Others-->
<section class="master-form card card-outline card-primary mx-4">

    <header class="card-header">
        <h2 class="card-title"><strong>Base Fee of all other properties</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>Base Fee of all other properties</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $baseFeeOthers =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Base Fee Others'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($baseFeeOthers);
                          // die();
                      ?>

                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[base_fee_others][base-fee-others]"
                            value="<?= (isset($baseFeeOthers['base-fee-others'])) ? $baseFeeOthers['base-fee-others'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>














<!-- 15- Relative Discount -->
<section class="master-form card card-outline card-success mx-4" style="display:none;">

    <header class="card-header">
        <h2 class="card-title"><strong>Relative Discount</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>Base Fee</strong>
                        </td>
                        <td>
                            <strong>95%</strong>
                        </td>
                        <td>
                            <strong>90%</strong>
                        </td>
                        <td>
                            <strong>80%</strong>
                        </td>
                        <td>
                            <strong>75%</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $discount =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Relative Discount Data'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($discount);
                          // die();
                      ?>

                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[relative_discount][base-fee]"
                            value="<?= (isset($discount['base-fee'])) ? $discount['base-fee'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[relative_discount][95%]"
                            value="<?= (isset($discount['95%'])) ? $discount['95%'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[relative_discount][90%]"
                            value="<?= (isset($discount['90%'])) ? $discount['90%'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[relative_discount][80%]"
                            value="<?= (isset($discount['80%'])) ? $discount['80%'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[relative_discount][75%]"
                            value="<?= (isset($discount['75%'])) ? $discount['75%'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>

<!-- 4- Number of properties -->
<section class="master-form card card-outline card-warning mx-4" style="display:none;">

    <header class="card-header">
        <h2 class="card-title"><strong>Number of properties</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>1 - (One)</strong>
                        </td>
                        <td>
                            <strong>2 - (Two)</strong>
                        </td>
                        <td>
                            <strong>3 - (Three)</strong>
                        </td>
                        <td>
                            <strong>4 - (Four)</strong>
                        </td>
                        <td>
                            <strong>5 - (Five)</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $no_of_property =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Number of Properties'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($no_of_property);
                          // die();
                      ?>

                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_properties][1]"
                            value="<?= (isset($no_of_property['1'])) ? $no_of_property['1'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_properties][2]"
                            value="<?= (isset($no_of_property['2'])) ? $no_of_property['2'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_properties][3]"
                            value="<?= (isset($no_of_property['3'])) ? $no_of_property['3'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_properties][4]"
                            value="<?= (isset($no_of_property['4'])) ? $no_of_property['4'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[number_of_properties][5]"
                            value="<?= (isset($no_of_property['5'])) ? $no_of_property['5'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>


<!-- 13- Valuation Approach -->
<section class="master-form card card-outline card-dark mx-4" style="display:none;">

    <header class="card-header">
        <h2 class="card-title"><strong>Valuation Approach</strong></h2>
    </header>

    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-12">

              <table class="table table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td>
                            <strong>Market Approach</strong>
                        </td>
                        <td>
                            <strong>Income Approach</strong>
                        </td>
                        <td>
                            <strong>Profit Approach</strong>
                        </td>
                        <td>
                            <strong>Residual Approach</strong>
                        </td>
                        <td>
                            <strong>DCF approach</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>

                  <tr>
                    <?php
                    $valuation_approach =  ArrayHelper::map(\app\models\ProposalMasterFile::find()
                      ->where(['heading' => 'Valuation Approach'])->all(), 'sub_heading', 'values');
                          // echo "<pre>";
                          // print_r($valuation_approach);
                          // die();
                      ?>

                      <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[valuation_approach][Market Approach]"
                            value="<?= (isset($valuation_approach['Market Approach'])) ? $valuation_approach['Market Approach'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[valuation_approach][Income Approach]"
                            value="<?= (isset($valuation_approach['Income Approach'])) ? $valuation_approach['Income Approach'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[valuation_approach][Profit Approach]"
                            value="<?= (isset($valuation_approach['Profit Approach'])) ? $valuation_approach['Profit Approach'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[valuation_approach][Residual Approach]"
                            value="<?= (isset($valuation_approach['Residual Approach'])) ? $valuation_approach['Residual Approach'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td> 
                    <td>
                        <div class="form-group">
                            <input  type="number" step=".01"
                            name="ProposalMasterFile[valuation_approach][DCF Approach]"
                            value="<?= (isset($valuation_approach['DCF Approach'])) ? $valuation_approach['DCF Approach'] : 0 ?>"
                            placeholder="1" class="form-control"
                            required
                            />
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
</section>



<div class="card-footer bg-light">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mx-2']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
</div>
</section>

<?php ActiveForm::end(); ?>
