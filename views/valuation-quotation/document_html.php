<?php
// echo $property_id.','.$property_index;
$documents = \app\models\Properties::find()->where(["id" => $property_id])->one();

if ($documents['required_documents']!=null) {
  $document_explode =  explode(',',$documents['required_documents']);
  // echo $document_explode; die();  
  ?>

  <section class="valuation-form card card-outline card-primary mx-4 document-card">

    <header class="card-header">
      <h2 class="card-title">
        <i class="fas fa-edit"></i>
        <b>Property Required Documents</b></h2>
      </header>
      <div class="card-body">
        <div class="row" style="padding: 10px">



          <table id="requestTypes"
          class="table table-striped table-bordered table-hover images-table">
          <thead>
            <tr>
              <td><b>Description</b></td>
              <td><b>Attachment</b></td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            <?php
                  // $i= 1;
            foreach ($document_explode as $document){
              ?>
              <tr>

                <td><?= Yii::$app->appHelperFunctions->getPropertiesDocumentsListArr()[$document] ?></td>

                <td>

                  <div class="meri-marzi" style="width:100px; margin-top:30px;">
                    <div class="form-group">
                      <div class="input-group" style="opacity:0;">
                        <div class="custom-file">
                          <input type="file"  style=" height:100px;" class="custom-file-input doc-file"
                          id="valuationquotation-property_doc"
                          name="ValuationQuotation[property_doc][<?= $property_index ?>][<?= $document ?>]"/>
                          <label class="custom-file-label">file</label>
                        </div>
                      </div>
                    </div>
                    <img src="<?= Yii::$app->params['uploadIcon'] ?>"
                    style="margin-top:-80px;"
                    width="100px"
                    alt="" title=""
                    data-placeholder="no_image.png"/>
                    <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                     target="_blank">
                     <span class="glyphicon glyphicon-eye-open"></span>
                   </a>



                   <input type="hidden"
                   name="ValuationQuotation[recieve_docs][<?= $property_index ?>][<?= $document ?>]"
                   value="" class="attachment-address"/>



                 </div>

               </td>

             </tr>
             <?php
               // $i++;
           }
           ?>

         </tbody>
       </table>
     </div>
   </div>
 </section>


 <?php 
}
?>






