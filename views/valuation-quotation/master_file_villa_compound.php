<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\components\widgets\StatusVerified;
use yii\helpers\Url;
use kartik\select2\Select2;

$this->title = Yii::t('app', 'Fee Master File - Villa Compound');

$approach_type = 'income';
$property_type = 'villa_compound';

?>

<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
</style>

<?php $form = ActiveForm::begin(); ?>
<input type="hidden" name="QuotationFeeMasterFile[approach_type]" value="<?= $approach_type ?>">
<input type="hidden" name="QuotationFeeMasterFile[property_type]" value="<?= $property_type ?>">

<section class="master-form card card-outline card-primary">
    <header class="card-header">
        <h2 class="card-title"><strong class=""><?= $this->title ?></strong></h2>
        <div class="card-tools">
            <a href="<?= Url::toRoute(['valuation-quotation/get-master-file-history?file_type='.$property_type]) ?>" target="_blank" class="btn btn-info btn-sm">History</a>
        </div>
    </header>


    <!--Property Fee-->
    <section class="master-form card card-outline card-success mx-4 mt-4 ">
        <header class="card-header">
            <h2 class="card-title"><strong>Property Fee</strong></h2>
        </header>
        <div class="row p-4">
            <div class="col-sm-4">
                <?php
                // echo $form->field($model, 'id')->widget(Select2::classname(), [
                //     'data' => $properties,
                //     'options' => ['placeholder' => 'Select a Property Type ...', 'class' => 'property_type'],
                //     'pluginOptions' => [
                //         'allowClear' => true
                //     ],
                // ])->label(false);
                ?>
                
                <?php
                    $property_fee = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                    ->where(['heading' => 'property_fee'])->andWhere(['property_type' => $property_type])->all(), 'sub_heading', 'values');
                ?>
                <input type="number" step="1" min="0" name="QuotationFeeMasterFile[property_fee][<?= $property_type ?>]" value="<?= (isset($property_fee[$property_type])) ? $property_fee[$property_type] : 0 ?>" placeholder="1" class="form-control" required />
            </div>
            <div class="col-sm-8">
            </div>
        </div>
    </section>

    <!--Client Information-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Client Segment(%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">
                        <thead>
                            <tr>
                                <td>
                                    <strong>Bank</strong>
                                </td>
                                <td>
                                    <strong>Corporate</strong>
                                </td>
                                <td>
                                    <strong>Individual</strong>
                                </td>
                                <td>
                                    <strong>Partner</strong>
                                </td>
                                <td>
                                    <strong>Foreign Individual</strong>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                    $client_type = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'client_type'])->andWhere(['property_type' => $property_type])->all(), 'sub_heading', 'values');
                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[client_type][bank]" value="<?= (isset($client_type['bank'])) ? $client_type['bank'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[client_type][corporate]" value="<?= (isset($client_type['corporate'])) ? $client_type['corporate'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[client_type][individual]" value="<?= (isset($client_type['individual'])) ? $client_type['individual'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[client_type][partner]" value="<?= (isset($client_type['partner'])) ? $client_type['partner'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[client_type][foreign-individual]" value="<?= (isset($client_type['foreign-individual'])) ? $client_type['foreign-individual'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--City Information-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>City (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">
                        <thead>
                            <tr>
                                <td>
                                    <strong>Abu Dhabi</strong>
                                </td>
                                <td>
                                    <strong>Dubai</strong>
                                </td>
                                <td>
                                    <strong>Sharjah/UQ</strong>
                                </td>
                                <td>
                                    <strong>Rak, Fujairah</strong>
                                </td>
                                <td>
                                    <strong>Ajman</strong>
                                </td>
                                <td>
                                    <strong>Al Ain</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <?php
                                    $city = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'city'])->andWhere(['property_type' => $property_type])->all(), 'sub_heading', 'values');
                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][abu_dhabi]" value="<?= (isset($city['abu_dhabi'])) ? $city['abu_dhabi'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][dubai]" value="<?= (isset($city['dubai'])) ? $city['dubai'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][sharjah]" value="<?= (isset($city['sharjah'])) ? $city['sharjah'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][rak_fuj]" value="<?= (isset($city['rak_fuj'])) ? $city['rak_fuj'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][ajman]" value="<?= (isset($city['ajman'])) ? $city['ajman'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[city][others]" value="<?= (isset($city['others'])) ? $city['others'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Tenure Information-->
    <section class="master-form card card-outline card-dark mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Tenure (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">
                        <thead>
                            <tr>
                                <td>
                                    <strong>Freehold</strong>
                                </td>
                                <td>
                                    <strong>Non-Freehold</strong>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                    $tenure = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'tenure'])->andWhere(['property_type' => $property_type])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[tenure][freehold]" value="<?= (isset($tenure['freehold'])) ? $tenure['freehold'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[tenure][non_freehold]" value="<?= (isset($tenure['non_freehold'])) ? $tenure['non_freehold'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Standard/Non-Standard Information-->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Standard/Non-Standard (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Non-Standard</strong>
                                </td>
                                <td>
                                    <strong>Standard</strong>
                                </td>
                                <!-- <td>
                                <strong>Penthouse</strong>
                            </td> -->
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                    $complexity = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'complexity'])->andWhere(['property_type' => $property_type])->all(), 'sub_heading', 'values');
                                ?>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[complexity][non_standard]" value="<?= (isset($complexity['non_standard'])) ? $complexity['non_standard'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[complexity][standard]" value="<?= (isset($complexity['standard'])) ? $complexity['standard'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <!-- <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[complexity][penthouse]"
                                            value="<?= (isset($complexity['penthouse'])) ? $complexity['penthouse'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                                </td> -->

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Land size Information-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Standard Land size (only applicable on land valuation) (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">
                        <thead>
                            <tr>
                                <td style="width:16.66%">
                                    <strong>Land Size Range 1</strong>
                                </td>
                                <td style="width:16.66%">
                                    <strong>Land Size Range 2</strong>
                                </td>
                                <td style="width:16.66%">
                                    <strong>Land Size Range 3</strong>
                                </td>
                                <td style="width:16.66%">
                                    <strong>Land Size Range 4</strong>
                                </td>
                                <td style="width:16.66%">
                                    <strong>Land Size Range 5</strong>
                                </td>
                                <td style="width:16.66%">
                                    <strong>Land Size Range 6</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <?php
                                    $land = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'land'])->andWhere(['property_type' => $property_type])->all(), 'sub_heading', 'values');
                                ?>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                            <input  type="number" step="1" min="1"
                                            name="QuotationFeeMasterFile[land][range1_from]"
                                            value="<?= (isset($land['range1_from'])) ? $land['range1_from'] : 1 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                            />
                                        </div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                            <input  type="number" step="1" min="0"
                                            name="QuotationFeeMasterFile[land][range1_to]"
                                            value="<?= (isset($land['range1_to'])) ? $land['range1_to'] : 5000 ?>"
                                            placeholder="5000" class="form-control"
                                            required
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][range1_fee]" value="<?= (isset($land['range1_fee'])) ? $land['range1_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                            <input  type="number" step="1" min="1"
                                            name="QuotationFeeMasterFile[land][range2_from]"
                                            value="<?= (isset($land['range2_from'])) ? $land['range2_from'] : 5001 ?>"
                                            placeholder="5001" class="form-control"
                                            required
                                            />
                                        </div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                            <input  type="number" step="1" min="0"
                                            name="QuotationFeeMasterFile[land][range2_to]"
                                            value="<?= (isset($land['range2_to'])) ? $land['range2_to'] : 7500 ?>"
                                            placeholder="7500" class="form-control"
                                            required
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][range2_fee]" value="<?= (isset($land['range2_fee'])) ? $land['range2_fee'] : 0 ?>" placeholder="1" class="form-control" required /> 
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                            <input  type="number" step="1" min="1"
                                            name="QuotationFeeMasterFile[land][range3_from]"
                                            value="<?= (isset($land['range3_from'])) ? $land['range3_from'] : 7501 ?>"
                                            placeholder="7501" class="form-control"
                                            required
                                            />
                                        </div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                            <input  type="number" step="1" min="0"
                                            name="QuotationFeeMasterFile[land][range3_to]"
                                            value="<?= (isset($land['range3_to'])) ? $land['range3_to'] : 10000 ?>"
                                            placeholder="10000" class="form-control"
                                            required
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][range3_fee]" value="<?= (isset($land['range3_fee'])) ? $land['range3_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                            <input  type="number" step="1" min="1"
                                            name="QuotationFeeMasterFile[land][range4_from]"
                                            value="<?= (isset($land['range4_from'])) ? $land['range4_from'] : 10001 ?>"
                                            placeholder="10001" class="form-control"
                                            required
                                            />
                                        </div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                            <input  type="number" step="1" min="0"
                                            name="QuotationFeeMasterFile[land][range4_to]"
                                            value="<?= (isset($land['range4_to'])) ? $land['range4_to'] : 15000 ?>"
                                            placeholder="15000" class="form-control"
                                            required
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][range4_fee]" value="<?= (isset($land['range4_fee'])) ? $land['range4_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                            <input  type="number" step="1" min="1"
                                            name="QuotationFeeMasterFile[land][range5_from]"
                                            value="<?= (isset($land['range5_from'])) ? $land['range5_from'] : 15001 ?>"
                                            placeholder="15001" class="form-control"
                                            required
                                            />
                                        </div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                            <input  type="number" step="1" min="0"
                                            name="QuotationFeeMasterFile[land][range5_to]"
                                            value="<?= (isset($land['range5_to'])) ? $land['range5_to'] : 25000 ?>"
                                            placeholder="25000" class="form-control"
                                            required
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][range5_fee]" value="<?= (isset($land['range5_fee'])) ? $land['range5_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-12"><strong>Above</strong>
                                            <input  type="number" step="1" min="1"
                                            name="QuotationFeeMasterFile[land][range6_from]"
                                            value="<?= (isset($land['range6_from'])) ? $land['range6_from'] : 25001 ?>"
                                            placeholder="25001" class="form-control"
                                            required
                                            />
                                        </div>
                                        
                                        
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[land][range6_fee]" value="<?= (isset($land['range6_fee'])) ? $land['range6_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Built Up Area of subject property-->
    <section class="master-form card card-outline card-success mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Built Up Area of subject property (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td style="width:12.5%">
                                    <strong>BUA Range 1</strong>
                                </td>
                                <td style="width:12.5%">
                                    <strong>BUA Range 2</strong>
                                </td>
                                <td style="width:12.5%">
                                    <strong>BUA Range 3</strong>
                                </td>
                                <td style="width:12.5%">
                                    <strong>BUA Range 4</strong>
                                </td>
                                <td style="width:12.5%">
                                    <strong>BUA Range 5</strong>
                                </td>
                                <td style="width:12.5%">
                                    <strong>BUA Range 6</strong>
                                </td>
                                <td style="width:12.5%">
                                    <strong>BUA Range 7</strong>
                                </td>
                                <td style="width:8%">
                                    <strong>BUA Range 8</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $BUA = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'built_up_area_of_subject_property'])->andWhere(['property_type' => $property_type])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                        <input  type="number" step="1" min="1"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range1_from]"
                                        value="<?= (isset($BUA['range1_from'])) ? $BUA['range1_from'] : 1 ?>"
                                        placeholder="1" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range1_to]"
                                        value="<?= (isset($BUA['range1_to'])) ? $BUA['range1_to'] : 10000 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[built_up_area_of_subject_property][range1_fee]" value="<?= (isset($BUA['range1_fee'])) ? $BUA['range1_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range2_from]"
                                        value="<?= (isset($BUA['range2_from'])) ? $BUA['range2_from'] : 10001 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range2_to]"
                                        value="<?= (isset($BUA['range2_to'])) ? $BUA['range2_to'] : 25000 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[built_up_area_of_subject_property][range2_fee]" value="<?= (isset($BUA['range2_fee'])) ? $BUA['range2_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range3_from]"
                                        value="<?= (isset($BUA['range3_from'])) ? $BUA['range3_from'] : 25001 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range3_to]"
                                        value="<?= (isset($BUA['range3_to'])) ? $BUA['range3_to'] : 50000 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[built_up_area_of_subject_property][range3_fee]" value="<?= (isset($BUA['range3_fee'])) ? $BUA['range3_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range4_from]"
                                        value="<?= (isset($BUA['range4_from'])) ? $BUA['range4_from'] : 50001 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range4_to]"
                                        value="<?= (isset($BUA['range4_to'])) ? $BUA['range4_to'] : 100000 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[built_up_area_of_subject_property][range4_fee]" value="<?= (isset($BUA['range4_fee'])) ? $BUA['range4_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range5_from]"
                                        value="<?= (isset($BUA['range5_from'])) ? $BUA['range5_from'] : 100001 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range5_to]"
                                        value="<?= (isset($BUA['range5_to'])) ? $BUA['range5_to'] : 250000 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[built_up_area_of_subject_property][range5_fee]" value="<?= (isset($BUA['range5_fee'])) ? $BUA['range5_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range6_from]"
                                        value="<?= (isset($BUA['range6_from'])) ? $BUA['range6_from'] : 250001 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range6_to]"
                                        value="<?= (isset($BUA['range6_to'])) ? $BUA['range6_to'] : 500000 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[built_up_area_of_subject_property][range6_fee]" value="<?= (isset($BUA['range6_fee'])) ? $BUA['range6_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range7_from]"
                                        value="<?= (isset($BUA['range7_from'])) ? $BUA['range7_from'] : 500001 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range7_to]"
                                        value="<?= (isset($BUA['range7_to'])) ? $BUA['range7_to'] : 1000000 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[built_up_area_of_subject_property][range7_fee]" value="<?= (isset($BUA['range7_fee'])) ? $BUA['range7_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-12"><strong>Above</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range8_from]"
                                        value="<?= (isset($BUA['range8_from'])) ? $BUA['range8_from'] : 1000001 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6">
                                        <!-- <strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[built_up_area_of_subject_property][range8_to]"
                                        value="<?= (isset($BUA['range8_to'])) ? $BUA['range8_to'] : 0 ?>"
                                        placeholder="" class="form-control"
                                        required
                                        /> -->
                                        </div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[built_up_area_of_subject_property][range8_fee]" value="<?= (isset($BUA['range8_fee'])) ? $BUA['range8_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Net Leasable Area-->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Net Leasable Area (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td  style="width:16.66%">
                                    <strong>NLA Range 1</strong>
                                </td>
                                <td  style="width:16.66%">
                                    <strong>NLA Range 2</strong>
                                </td>
                                <td  style="width:16.66%">
                                    <strong>NLA Range 3</strong>
                                </td>
                                <td  style="width:16.66%">
                                    <strong>NLA Range 4</strong>
                                </td>
                                <td  style="width:16.66%">
                                    <strong>NLA Range 5</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                    $NLA = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'net_leasable_area'])->andWhere(['property_type' => $property_type])->all(), 'sub_heading', 'values');

                                ?>

                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                        <input  type="number" step="1" min="1"
                                        name="QuotationFeeMasterFile[net_leasable_area][range1_from]"
                                        value="<?= (isset($NLA['range1_from'])) ? $NLA['range1_from'] : 1 ?>"
                                        placeholder="1" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[net_leasable_area][range1_to]"
                                        value="<?= (isset($NLA['range1_to'])) ? $NLA['range1_to'] : 2000 ?>"
                                        placeholder="2000" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[net_leasable_area][range1_fee]" value="<?= (isset($NLA['range1_fee'])) ? $NLA['range1_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                        <input  type="number" step="1" min="1"
                                        name="QuotationFeeMasterFile[net_leasable_area][range2_from]"
                                        value="<?= (isset($NLA['range2_from'])) ? $NLA['range2_from'] : 2001 ?>"
                                        placeholder="1" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[net_leasable_area][range2_to]"
                                        value="<?= (isset($NLA['range2_to'])) ? $NLA['range2_to'] : 4000 ?>"
                                        placeholder="2000" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[net_leasable_area][range2_fee]" value="<?= (isset($NLA['range2_fee'])) ? $NLA['range2_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                        <input  type="number" step="1" min="1"
                                        name="QuotationFeeMasterFile[net_leasable_area][range3_from]"
                                        value="<?= (isset($NLA['range3_from'])) ? $NLA['range3_from'] : 4001 ?>"
                                        placeholder="1" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[net_leasable_area][range3_to]"
                                        value="<?= (isset($NLA['range3_to'])) ? $NLA['range3_to'] : 6000 ?>"
                                        placeholder="2000" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[net_leasable_area][range3_fee]" value="<?= (isset($NLA['range3_fee'])) ? $NLA['range3_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-6"><strong>From</strong>
                                        <input  type="number" step="1" min="1"
                                        name="QuotationFeeMasterFile[net_leasable_area][range4_from]"
                                        value="<?= (isset($NLA['range4_from'])) ? $NLA['range4_from'] : 6001 ?>"
                                        placeholder="1" class="form-control"
                                        required
                                        /></div>
                                        <div  class="col-sm-6"><strong>To</strong>
                                        <input  type="number" step="1" min="0"
                                        name="QuotationFeeMasterFile[net_leasable_area][range4_to]"
                                        value="<?= (isset($NLA['range4_to'])) ? $NLA['range4_to'] : 8000 ?>"
                                        placeholder="2000" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[net_leasable_area][range4_fee]" value="<?= (isset($NLA['range4_fee'])) ? $NLA['range4_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="row"> 
                                        <div class="col-sm-12"><strong>Above</strong>
                                        <input  type="number" step="1" min="1"
                                        name="QuotationFeeMasterFile[net_leasable_area][range5_from]"
                                        value="<?= (isset($NLA['range5_from'])) ? $NLA['range5_from'] : 8001 ?>"
                                        placeholder="1" class="form-control"
                                        required
                                        /></div>
                                    </div>
                                    <div class="form-group mt-2"><strong>Fee</strong>
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[net_leasable_area][range5_fee]" value="<?= (isset($NLA['range5_fee'])) ? $NLA['range5_fee'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Number of Units/Rooms in Subject Property-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number of Units/Rooms in Subject Property</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>1-10</strong>
                                </td>
                                <td>
                                    <strong>11-25</strong>
                                </td>
                                <td>
                                    <strong>26-50</strong>
                                </td>
                                <td>
                                    <strong>51-100</strong>
                                </td>
                                <td>
                                    <strong>101-250</strong>
                                </td>
                                <td>
                                    <strong>251-500</strong>
                                </td>
                                <td>
                                    <strong>501-1000</strong>
                                </td>
                                <td>
                                    <strong>>1000+</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $no_of_units_in_building = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'number_of_units_building'])->all(), 'sub_heading', 'values');
                                // echo "<pre>";
                                // print_r($no_of_units_in_building);
                                // die();
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_units_building][1]" value="<?= (isset($no_of_units_in_building['1'])) ? $no_of_units_in_building['1'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_units_building][2]" value="<?= (isset($no_of_units_in_building['2'])) ? $no_of_units_in_building['2'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_units_building][3]" value="<?= (isset($no_of_units_in_building['3'])) ? $no_of_units_in_building['3'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_units_building][4]" value="<?= (isset($no_of_units_in_building['4'])) ? $no_of_units_in_building['4'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_units_building][5]" value="<?= (isset($no_of_units_in_building['5'])) ? $no_of_units_in_building['5'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_units_building][6]" value="<?= (isset($no_of_units_in_building['6'])) ? $no_of_units_in_building['6'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_units_building][7]" value="<?= (isset($no_of_units_in_building['7'])) ? $no_of_units_in_building['7'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_units_building][8]" value="<?= (isset($no_of_units_in_building['8'])) ? $no_of_units_in_building['8'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Types of Propety Units/Rooms-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Types of Propety Units/Rooms</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Residential</strong>
                                </td>
                                <td>
                                    <strong>Commercial</strong>
                                </td>
                                <td>
                                    <strong>Retail</strong>
                                </td>
                                <td>
                                    <strong>Industrial</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $types_of_property_unit = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'types_of_property_unit'])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[types_of_property_unit][residential]" value="<?= (isset($types_of_property_unit['residential'])) ? $types_of_property_unit['residential'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[types_of_property_unit][commercial]" value="<?= (isset($types_of_property_unit['commercial'])) ? $types_of_property_unit['commercial'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[types_of_property_unit][retail]" value="<?= (isset($types_of_property_unit['retail'])) ? $types_of_property_unit['retail'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[types_of_property_unit][industrial]" value="<?= (isset($types_of_property_unit['industrial'])) ? $types_of_property_unit['industrial'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>


                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Types of Resi Units/Rooms-->
    <section class="master-form card card-outline card-warning mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Types of Resi Units/Rooms</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>1 type</strong>
                                </td>
                                <td>
                                    <strong>2 types</strong>
                                </td>
                                <td>
                                    <strong>3 types</strong>
                                </td>
                                <td>
                                    <strong>4 types</strong>
                                </td>
                                <td>
                                    <strong>5 types</strong>
                                </td>
                                <td>
                                    <strong>6 types</strong>
                                </td>
                                <td>
                                    <strong>7 types</strong>
                                </td>
                                <td>
                                    <strong>8 types</strong>
                                </td>
                                <td>
                                    <strong>9 types</strong>
                                </td>
                                <td>
                                    <strong>10 types</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                    $no_of_units_in_building = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'number_of_types'])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_types][1]" value="<?= (isset($no_of_units_in_building['1'])) ? $no_of_units_in_building['1'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_types][2]" value="<?= (isset($no_of_units_in_building['2'])) ? $no_of_units_in_building['2'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_types][3]" value="<?= (isset($no_of_units_in_building['3'])) ? $no_of_units_in_building['3'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_types][4]" value="<?= (isset($no_of_units_in_building['4'])) ? $no_of_units_in_building['4'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_types][5]" value="<?= (isset($no_of_units_in_building['5'])) ? $no_of_units_in_building['5'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_types][6]" value="<?= (isset($no_of_units_in_building['6'])) ? $no_of_units_in_building['6'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_types][7]" value="<?= (isset($no_of_units_in_building['7'])) ? $no_of_units_in_building['7'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_types][8]" value="<?= (isset($no_of_units_in_building['8'])) ? $no_of_units_in_building['8'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_types][9]" value="<?= (isset($no_of_units_in_building['9'])) ? $no_of_units_in_building['9'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_types][10]" value="<?= (isset($no_of_units_in_building['10'])) ? $no_of_units_in_building['10'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>


                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Comparables-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Comparables (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Less Than 3</strong>
                                </td>
                                <td>
                                    <strong>3 - 5</strong>
                                </td>
                                <td>
                                    <strong>6 - 10</strong>
                                </td>
                                <td>
                                    <strong>11 - 15</strong>
                                </td>
                                <td>
                                    <strong>Above 15</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                    $no_of_Comparables = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'number_of_comparables_data'])->andWhere(['property_type' => $property_type])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_comparables][less_than_3]" value="<?= (isset($no_of_Comparables['less_than_3'])) ? $no_of_Comparables['less_than_3'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_comparables][3_5]" value="<?= (isset($no_of_Comparables['3_5'])) ? $no_of_Comparables['3_5'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_comparables][6_10]" value="<?= (isset($no_of_Comparables['6_10'])) ? $no_of_Comparables['6_10'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_comparables][11_15]" value="<?= (isset($no_of_Comparables['11_15'])) ? $no_of_Comparables['11_15'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[number_of_comparables][above_15]" value="<?= (isset($no_of_Comparables['above_15'])) ? $no_of_Comparables['above_15'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Other Intended Users-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Other Intended Users</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Yes</strong>
                                </td>
                                <td>
                                    <strong>No</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                    $other_intended_users = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'other_intended_users'])->andWhere(['property_type' => $property_type])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[other_intended_users][yes]" value="<?= (isset($other_intended_users['yes'])) ? $other_intended_users['yes'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[other_intended_users][no]" value="<?= (isset($other_intended_users['no'])) ? $other_intended_users['no'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Upgrades Information-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Upgrades/Age Ratings (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 Star</strong>
                            </td>
                            <td>
                                <strong>2 Star</strong>
                            </td>
                            <td>
                                <strong>3 Star</strong>
                            </td>
                            <td>
                                <strong>4 Star</strong>
                            </td>
                            <td>
                                <strong>5 Star</strong>
                            </td>

                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $upgrades_ratings = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'upgrades_ratings'])->andWhere(['approach_type' => 'market'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r(upgrades_ratings);
                            // die();
                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][1]"
                                            value="<?= (isset($upgrades_ratings['1'])) ? $upgrades_ratings['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][2]"
                                            value="<?= (isset($upgrades_ratings['2'])) ? $upgrades_ratings['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][3]"
                                            value="<?= (isset($upgrades_ratings['3'])) ? $upgrades_ratings['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][4]"
                                            value="<?= (isset($upgrades_ratings['4'])) ? $upgrades_ratings['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][5]"
                                            value="<?= (isset($upgrades_ratings['5'])) ? $upgrades_ratings['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>



                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Urgency Fee-->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Timing(Urgency Fee) (%)(+)</strong></h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                            <tr>
                                <td>
                                    <strong>Standard</strong>
                                </td>
                                <td>
                                    <strong>Urgent Same Day</strong>
                                </td>
                                <td>
                                    <strong>Urgent 1 Day</strong>
                                </td>
                                <td>
                                    <strong>Urgent 2 Days</strong>
                                </td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <?php
                                $urgency_fee = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                    ->where(['heading' => 'urgency_fee'])->andWhere(['property_type' => $property_type])->all(), 'sub_heading', 'values');
                                ?>

                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[urgency_fee][standard]" value="<?= (isset($urgency_fee['standard'])) ? $urgency_fee['standard'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[urgency_fee][urgent_same_day]" value="<?= (isset($urgency_fee['urgent_same_day'])) ? $urgency_fee['urgent_same_day'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[urgency_fee][urgent_1_day]" value="<?= (isset($urgency_fee['urgent_1_day'])) ? $urgency_fee['urgent_1_day'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" step=".01" name="QuotationFeeMasterFile[urgency_fee][urgent_2_days]" value="<?= (isset($urgency_fee['urgent_2_days'])) ? $urgency_fee['urgent_2_days'] : 0 ?>" placeholder="1" class="form-control" required />
                                    </div>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>


 

    <div class="row mx-3">
        <div class="col">
            <?= StatusVerified::widget(['model' => $model, 'form' => $form]) ?>
        </div>
    </div>

    <?php
    $latestUpdatedUser = '';
    $query = \app\models\History::find()->where(['rec_type' => 'masterfile', 'file_type' => $property_type])->orderBy(['id' => SORT_DESC])->one();
    if ($query <> null) {
        $latestUpdatedUser = $query->user->firstname . ' ' . $query->user->lastname;
    }
    ?>

    <div class="card-footer bg-light">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mx-2']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if ($latestUpdatedUser <> null) { ?>
            <span class="badge grid-badge badge-dark mx-2"><mark>Last Updated By <?= $latestUpdatedUser ?></mark></span>
        <?php
        }
        ?>
    </div>
</section>

<?php ActiveForm::end(); ?>