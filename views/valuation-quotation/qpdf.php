<?php
$mainTableData = yii::$app->quotationHelperFunctions->getMainTableData($id);
$clientAddress = yii::$app->quotationHelperFunctions->getClientAddress($mainTableData['client_name']);
$vatInQuotationRecommendedFee = yii::$app->quotationHelperFunctions->getVatTotal($mainTableData['quotation_recommended_fee']);
$standardReport = Yii::$app->quotationHelperFunctions->standardReport();
?>
<br><br><br>
<table>
  <tr><td class="detailheading"><h4>About Windmills and our Top 5 competitive propositions:</h4></td></tr><br>
  <tr><td class="detailtexttext"><?= $standardReport['quotation_last_paragraph'] ?></td></tr>
</table><br pagebreak="true" />
<table>
  <thead>
    <tr><br>
      <td colspan="2" class="detailtexttext first-section">
        <br><span class="size-8">BILL TO</span>
        <br><span><?= $clientAddress['title'] ?></span>
        <br><span><?= $clientAddress['address'] ?></span>
      </td>
      <td class="box1 text-dec">
        <p>
          DATE<br>
          <?= $mainTableData['inquiry_date'] ?>
          <br>
        </p>
      </td>
      <td class="box2 text-dec">
        <p>
          PLEASE APPROVE<br>  
          AED<?= number_format($mainTableData['quotation_recommended_fee']+$vatInQuotationRecommendedFee,2) ?>
        </p>
      </td>
      <td class="box4 text-dec">
        <p>
          TAT<br>
          <?= $mainTableData['quotation_turn_around_time'] ?> Days
          <br>
        </p>
      </td>
      <td class="box3 text-dec" style="margin-right:10px;">
        <p>
          EXPIRY DATE<br>
          <?= $mainTableData['expiry_date'] ?>
          <br>

        </p>
      </td>
    </tr>
  </thead>
</table><br><br>
<table>
  <!-- <tr><td class="detailtexttext" colspan="2">Dear Sir/Ma’am,</td></tr> -->
  <tr><td class="detailtexttext" colspan="2">Good day. I sincerely hope that you are doing well.</td></tr>

  <br><tr><td class="detailtexttext" colspan="2">Thank you very much again for approaching us for this very important valuation assignment. We are keen to value for you with the best of our professionalism, experience, results and service quality.</td></tr>

  <br><tr><td class="detailtexttext" colspan="2">We will be delighted to offer you the best possible quotation of (AED <?= number_format($mainTableData['quotation_recommended_fee']+$vatInQuotationRecommendedFee,2) ?>) - plus VAT of 5% with the turnaround time being of (<?= $mainTableData['quotation_turn_around_time'] ?>) working days after the physical inspection is completed and necessary documents are received.</td></tr>

  <br><tr><td class="detailtexttext" colspan="2">We look forward to receiving your kind approval so we can send you the full terms of engagement for final review and sign-off.</td></tr>
  <br><tr><td class="detailtexttext" colspan="2">Thank you very much.</td></tr>
</table><br><br>
<table cellspacing="1" cellpadding="5">
  <thead>
    <tr>
      <td class="detailtext tb-heading">DESCRIPTION</td>
      <td class="detailtext tb-heading">TAX</td>
      <td class="detailtext tb-heading">QTY</td>
      <td></td>
    </tr>
  </thead>
  <tbody>
    <?php
    $multipleProperties = yii::$app->quotationHelperFunctions->getMultipleProperties($id);
    if ($multipleProperties!=null) {
     foreach ($multipleProperties as $multipleProperty)
     {
      // echo "<pre>"; print_r($multipleProperty); echo "</pre>"; die();

       ?>
       <tr>
        <td class="detailtext"><?= $multipleProperty->buildingTitle->title ?></td>
        <td class="detailtext"> 5% VAT</td>
        <td class="detailtext">1</td>
      </tr>
      <?php
    }
  }
  ?>
</tbody>
</table>
<table cellspacing="1" cellpadding="5">
  <tr>
    <td colspan="2"></td>
    <th class="detailtext amount-heading">SUB TOTAL:</th>
    <td class="detailtext amount-heading"><?= number_format($mainTableData['quotation_recommended_fee'],2) ?></td>
  </tr>
  <tr>
    <td colspan="2"></td>
    <th class="detailtext amount-heading">VAT TOTAL:</th>
    <td class="detailtext amount-heading"><?= $vatInQuotationRecommendedFee ?></td>
  </tr>
  <tr>
    <td colspan="2"></td>
    <th class="detailtext amount-heading">TOTAL:</th>
    <td class="detailtext amount-heading"><?= number_format($mainTableData['quotation_recommended_fee']+$vatInQuotationRecommendedFee,2) ?></td>
  </tr>
  <tr>
    <td colspan="2"></td>
    <th class="detailtext amount-heading">QUOTATION TURN AROUND TIME:</th>
    <td class="detailtext amount-heading"><?= $mainTableData['quotation_turn_around_time'] ?> Working Days</td>
  </tr>
  <tr>
    <td colspan="2"></td>
    <th class="detailtext amount-heading total-due">TOTAL DUE:</th>
    <td class="detailtext amount-heading total-due">AED<?= number_format($mainTableData['quotation_recommended_fee']+$vatInQuotationRecommendedFee,2) ?></td>
  </tr>
</table>
<style>
.box1{
  background-color: #BBDEFB;
  width: 70px;
  margin-top: -20px
}
.box2{
  background-color: #42A5F5;
  width: 110px;
}
.box3{
  background-color: #82B1FF;
  width: 100px;
  /* margin-right: 100px; */
}
.box4{
  background-color: #BBDEFB;
  width: 70px;
  /*margin-top: -20px*/
}

.text-dec{
  font-size: 10px;
  text-align: center;
}
td.detailtext{
  /* background-color:#BDBDBD; */
  font-size:9px;
  border-top: 1px solid black;
}
td.detailtexttext{
  /* background-color:#EEEEEE; */
  font-size:11px;
}
th.detailtext{
  /* background-color:#BDBDBD; */
  font-size:9px;
  border-top: 1px solid black;
}
.border {
  border: 1px solid black;
}
td.tb-heading{
  font-weight: bold;
}
th.amount-heading{
  color: #039BE5;
}
td.amount-heading{
  color: #039BE5;
}
th.total-due{
  font-size: 16px;
}
td.total-due{
  font-size: 16px;
}
span.size-8{
  font-size: 8px;
  font-weight: bold;
}
td.first-section{
  padding-left: 10px;
  /* background-color: black; */
}
</style>

