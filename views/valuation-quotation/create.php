<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ValuationQuotation */

$this->title = 'Create Valuation Quotation';
$this->params['breadcrumbs'][] = ['label' => 'Valuation Quotations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="valuation-quotation-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
