<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\ValuationQuotation */

$this->title = 'Update Quotation: ' . $model->reference_number;

$existingProperties = yii::$app->quotationHelperFunctions->getExistingProperties($model->id);
// echo "<pre>"; print_r($existingProperties); echo "</pre>"; die();s
// echo "<pre>"; print_r($model); echo "</pre>"; die();?>

<div class="clearfix">
    <?php
    if ($existingProperties!=null) {
        $val=0;
        foreach ($existingProperties as $value) {
            $val += $value->property_toe_fee;
        }
    }
    if ($val!=null) {
        if ($model->quotation_status == 1 || $model->quotation_status == 2) {?>

            <a href="<?= Url::toRoute(['valuation-quotation/send-toe?id='.$model->id]) ?>"><button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Send Toe</button></a>
            <a href="<?= Url::toRoute(['valuation-quotation/toe?id='.$model->id]) ?>"><button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Download Toe</button></a>


            <?php
        }
    }



    if ($existingProperties!=null) {
        $points=0;
        foreach ($existingProperties as $value) {
            $points += $value->building>0 ? $value->building : 0;
        }

        if ($points>0) {?>
            <a href="<?= Url::toRoute(['valuation-quotation/send-quotation?id='.$model->id]) ?>"><button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Send Quotation</button></a>

            <a href="<?= Url::toRoute(['valuation-quotation/qpdf?id='.$model->id]) ?>"><button type="button" class="btn btn-primary btn-sm my-2 mx-1 float-right">Download Quotation</button></a>



            <?php
        }
    }
    ?>

</div>


<?php
$this->params['breadcrumbs'][] = ['label' => 'Valuation Quotations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="valuation-quotation-update">


    <?= $this->render('_form', [
        'model' => $model,
        'buyer_data' => $buyer_data,
        'seller_data' => $seller_data,
        'client_data' => $client_data,
        'property_data' => $property_data,
        // 'm_calc' => $m_calc,
    ]) ?>

</div>
