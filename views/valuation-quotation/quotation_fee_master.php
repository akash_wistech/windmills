<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ArrayDataProvider;

// $status = yii::$app->quotationHelperFunctions->getQuotationStatusArr();
// echo "<pre>";
// print_r($model);
// echo "</pre>";
// die();

/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationQuotationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quotations Fee Master';
$this->params['breadcrumbs'][] = $this->title;

$cardTitle = $this->title;

$data = [
  ['id' => 1, 'title' => 'Residential Land Villa', 'controller_id' => 'valuation-quotation', 'action_id' => 'residential-land-villa'],
  ['id' => 2, 'title' => 'Apartment', 'controller_id' => 'valuation-quotation', 'action_id' => 'apartment'],
  ['id' => 3, 'title' => 'Penthouse', 'controller_id' => 'valuation-quotation', 'action_id' => 'penthouse'],
  ['id' => 4, 'title' => 'Townhouse', 'controller_id' => 'valuation-quotation', 'action_id' => 'townhouse'],
  ['id' => 5, 'title' => 'Villa', 'controller_id' => 'valuation-quotation', 'action_id' => 'villa'],
  ['id' => 6, 'title' => 'Shop', 'controller_id' => 'valuation-quotation', 'action_id' => 'shop'],
  ['id' => 7, 'title' => 'Office', 'controller_id' => 'valuation-quotation', 'action_id' => 'office'],
  ['id' => 8, 'title' => 'Warehouse', 'controller_id' => 'valuation-quotation', 'action_id' => 'warehouse'],
  ['id' => 9, 'title' => 'Building Land', 'controller_id' => 'valuation-quotation', 'action_id' => 'building-land'],
  // ['id' => 10, 'title' => 'Building', 'controller_id' => 'valuation-quotation', 'action_id' => 'building'],
  ['id' => 10, 'title' => 'Building Residential', 'controller_id' => 'valuation-quotation', 'action_id' => 'building-residential'],
  ['id' => 11, 'title' => 'Building Commercial', 'controller_id' => 'valuation-quotation', 'action_id' => 'building-commercial'],
  ['id' => 12, 'title' => 'Building Mixed Use', 'controller_id' => 'valuation-quotation', 'action_id' => 'building-mixeduse'],
  ['id' => 13, 'title' => 'Villa Compound', 'controller_id' => 'valuation-quotation', 'action_id' => 'villa-compound'],
  ['id' => 14, 'title' => 'Warehouse Compound', 'controller_id' => 'valuation-quotation', 'action_id' => 'warehouse-compound'],
  ['id' => 15, 'title' => 'Labour Accommodation', 'controller_id' => 'valuation-quotation', 'action_id' => 'labour-accommodation'],
  ['id' => 16, 'title' => 'Mall (Leased)', 'controller_id' => 'valuation-quotation', 'action_id' => 'mall-leased'],
  ['id' => 17, 'title' => 'School (Leased)', 'controller_id' => 'valuation-quotation', 'action_id' => 'school-leased'],
  ['id' => 18, 'title' => 'Hotel', 'controller_id' => 'valuation-quotation', 'action_id' => 'hotel'],
  ['id' => 19, 'title' => 'School (Operated)', 'controller_id' => 'valuation-quotation', 'action_id' => 'school-operated'],
  ['id' => 20, 'title' => 'Mall (Operated)', 'controller_id' => 'valuation-quotation', 'action_id' => 'mall-operated'],
  ['id' => 21, 'title' => 'Hospital', 'controller_id' => 'valuation-quotation', 'action_id' => 'hospital'],
  ['id' => 22, 'title' => 'Hotel Apartment', 'controller_id' => 'valuation-quotation', 'action_id' => 'hotel-apartment'],
  ['id' => 23, 'title' => 'Building Hotel Apartment', 'controller_id' => 'valuation-quotation', 'action_id' => 'building-hotel-apartment'],
  // Add more data as needed
];

$dataProvider = new ArrayDataProvider([
  'allModels' => $data,
  'pagination' => [
    'pageSize' => 25, // Set the number of items per page
  ],
  'sort' => [
    'attributes' => ['title'], // Define sortable attributes
    'defaultOrder' => ['title' => SORT_ASC],
  ],
]);

?>
<style>
  .table thead {
    border-top: 1px solid #ddd;
    /* Adjust border color and width as needed */
  }

  .table td {
    vertical-align: middle;
    padding: 0.25rem 0.75rem;
  }
</style>
<div class="valuation-quotation-index">

  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    // 'filterModel' => $searchModel,
    'layout' => "
          <div class=\"card card-primary card-outline\">
            <div class=\"card-header\">
              <h3 class=\"card-title\">
                <strong>$cardTitle</strong>
              </h3>
            </div>

            <div class=\"card-body tbl-container table-responsive table-responsive-lg  table-striped table-hover mb-0\">
                {items}
            </div>

            <div class=\"card-footer\">
              <div class=\"row\">
              <div class=\"col-sm-3\">{summary}</div>
                <div class=\"col-sm-9 12 pager-container\">{pager}</div>
              </div>
            </div>
          </div>
          ",

    'columns' => [
      [
        'class' => 'yii\grid\SerialColumn',
        'headerOptions' => ['class' => 'noprint', 'style' => 'width:35px;']
      ],

      // 'title',
      [
        'attribute' => 'title',
        'label' => 'Property Type',
        'format' => 'raw',
        'value' => function ($data) {
            $url = Url::to([$data['controller_id'] . '/master-file-' . $data['action_id']]);
            return '<a href="' . $url . '" target="_blank">' . $data['title'] . '</a>';
          },
      ],
      [
        'class' => 'yii\grid\ActionColumn',
        'header' => 'Action',
        'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
        'contentOptions' => ['class' => 'noprint actions', 'style' => 'text-align:center'],
        'template' => '
                <div class="btn-group flex-wrap">
                  <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                  </button>
                  <div class="dropdown-menu" role="menu">
                     {update}
                  </div>  
                </div>',
        'buttons' => [
          'update' => function ($url, $data) {
              $url = ['master-file-' . $data['action_id']];
              return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Edit'), $url, [
                'title' => Yii::t('app', 'Edit'),
                'class' => 'dropdown-item text-1',
                'data-pjax' => "0",
              ]);
            },
        ],
      ],
    ],

    'pager' => [
      'maxButtonCount' => 5,
      'linkContainerOptions' => ['class' => 'paginate_button page-item'],
      'linkOptions' => ['class' => 'page-link'],
      'prevPageCssClass' => 'previous',
      'disabledPageCssClass' => 'disabled',
      'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link'],
    ],
  ]); ?>


</div>