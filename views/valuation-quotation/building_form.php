<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

// echo "<pre>";
// print_r($model->property);
// echo "</pre>";
// die();

$this->registerJs('

  $("#company-d-valuation_date'.$i.'").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
    });

    $("#company-d-inspection_date'.$i.'").datetimepicker({
      allowInputToggle: true,
      viewMode: "months",
      format: "YYYY-MM-DD"
      });

      ');

      ?>


      <div class="card property-card card-dark card-outline mx-4">
        <div class="card-header">
          <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <b>Subject Property Details</b>
          </h3>
        </div>
        <div class="card-body repeat-section">
          <div style="display:none">
                <?= $form->field($model, 'existingRecordIdz['.$i.']')->hiddenInput()->label(false) ?>
              </div>
              
          <div class="row py-2">
                 <div class="col-3">
                <?= $form->field($model, 'complexity['.$i.']')->widget(Select2::classname(), [
                  'data' => yii::$app->quotationHelperFunctions->getComplexity(),
                  'options' => ['class'=>'complexity'],
                  'pluginOptions' => [
                    'allowClear' => true
                  ],
                ]); ?>
              </div>
            <div class="col-3">
              
              <?= $form->field($model, 'building['.$i.']')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                  'title' => SORT_ASC,
                ])->all(), 'id', 'title'),

                'options' => ['placeholder' => 'Select a Building ...', 'class'=>'building'],
                'pluginOptions' => [
                  'allowClear' => true
                ],
              ]);


              ?>
            </div>
            <div class="col-3">


              <?= $form->field($model, 'property['.$i.']')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                  'title' => SORT_ASC,
                ])->all(), 'id', 'title'),
                'options' => ['placeholder' => 'Select a Property Type ...', 'class'=>'property', 'data-index'=> $i],
                'pluginOptions' => [
                  'allowClear' => true
                ],
              ]);?>
            </div>
            <div class="col-3">
              <?= $form->field($model, 'property_category['.$i.']')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                'options' => ['class'=>'property-category'],
                'pluginOptions' => [
                  'allowClear' => true
                ],
              ]);
              ?>
            </div>
            <div class="col-3">
              <?= $form->field($model, 'tenure['.$i.']')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                'options' => ['class'=>'tenure'],
                'pluginOptions' => [
                  'allowClear' => true
                ],
              ]);
              ?>
            </div>

            <div class="col-3">
              <?= $form->field($model, 'city['.$i.']')->textInput(['maxlength' => true,'readonly' => true,'class'=>'form-control city']) ?>
            </div>
            <div class="col-3">
              <?= $form->field($model, 'community['.$i.']')->textInput(['maxlength' => true,'readonly' => true,'class'=>'form-control community']) ?>
            </div>
            <div class="col-3">
              <?= $form->field($model, 'sub_community['.$i.']')->textInput(['maxlength' => true,'readonly' => true,'class'=>'form-control sub-community']) ?>
            </div>
            <div class="col-3">
              <?= $form->field($model, 'owner_name['.$i.']')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-3">
              <?= $form->field($model, 'building_number['.$i.']')->textInput(['class'=>'building_number form-control']) ?>
            </div>
            <div class="col-3">
              <?= $form->field($model, 'plot_number['.$i.']')->textInput(['class'=>'plot_number form-control']) ?>
            </div>
            <div class="col-3">
              <?= $form->field($model, 'unit_number['.$i.']')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-3">
              <?= $form->field($model, 'floor_number['.$i.']')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-3">
              <?= $form->field($model, 'no_of_floors['.$i.']')->textInput(['maxlength' => true,'class'=>'no_of_floors form-control']) ?>
            </div>




  <?php
  $numOfProp = [];
  if ($model->property[$i]==3 || $model->property[$i]==7 || $model->property[$i]==22) {
   $numOfProp = yii::$app->quotationHelperFunctions->numberofUnitsInBuildingArr;
 }else{
     $numOfProp =  yii::$app->quotationHelperFunctions->numberofUnitsInLandArr;
 }
 // else if($model->property[$i]==4 || $model->property[$i]==5 || $model->property[$i]==11 || $model->property[$i]==23 || $model->property[$i]==26 || $model->property[$i]==29){
 //   $numOfProp =  yii::$app->quotationHelperFunctions->numberofUnitsInLandArr;
 // }
 ?>
 <div class="col-3 num-unit">
  <?= $form->field($model, 'no_of_units['.$i.']')->widget(Select2::classname(), [
   'data' => $numOfProp,
   'options' => ['class'=>'no_of_units form-control'],
   'pluginOptions' => [
    'allowClear' => true
  ],
]); ?>
</div>










        <div class="col-3 bua" style="<?= ($model->property[$i]==3 || $model->property[$i]==7 || $model->property[$i]==22 || $model->property[$i]==4 || $model->property[$i]==5 || $model->property[$i]==11 || $model->property[$i]==23 || $model->property[$i]==26 || $model->property[$i]==19)
        ? 'display:none;': $model->built_up_area[$i] ?>">
        <?= $form->field($model, 'built_up_area['.$i.']')->widget(Select2::classname(), [
          'data' => yii::$app->quotationHelperFunctions->builtUpAreaArr,
          'options' => ['class'=>'built_up_area form-control'],
          'pluginOptions' => [
            'allowClear' => true
          ],
        ]); ?>
      </div>






      <div class="col-3 land-size" style="<?= ($model->property[$i]==4 || $model->property[$i]==5 || $model->property[$i]==11 || $model->property[$i]==23 || $model->property[$i]==36 || $model->property[$i]==29)
      ? $model->property[$i]: 'display:none;' ?>">
      <?= $form->field($model, 'land_size['.$i.']')->widget(Select2::classname(), [
        'data' => yii::$app->quotationHelperFunctions->getLandSizrArr(),
        'options' => ['class'=>'land'],
        'pluginOptions' => [
          'allowClear' => true
        ],
      ]); ?>
    </div>






    <div class="col-3">
      <?= $form->field($model, 'street['.$i.']')->textInput(['maxlength' => true,'class'=>'street form-control']) ?>
    </div>

    <div class="col-3">
      <?= $form->field($model, 'latitude['.$i.']')->textInput(['maxlength' => true,'class'=>'latitude form-control']) ?>
    </div>
    <div class="col-3">
      <?= $form->field($model, 'longitude['.$i.']')->textInput(['maxlength' => true,'class'=>'longitude form-control']) ?>
    </div>
    <div class="col-3">
      <?= $form->field($model, 'repeat_valuation['.$i.']')->widget(Select2::classname(), [
        'data' => yii::$app->quotationHelperFunctions->getNewRepeatValuation(),
        'options' => ['class'=>'repeat_valuation'],
        'pluginOptions' => [
          'allowClear' => true
        ],
      ]); ?>
    </div>

    <div class="col-3">
      <?= $form->field($model, 'valuation_date['.$i.']',['template'=>'
       {label}
       <div class="input-group date" id="company-d-valuation_date'.$i.'" data-target-input="nearest">
       {input}
       <div class="input-group-append" data-target="#company-d-valuation_date'.$i.'" data-toggle="datetimepicker">
       <div class="input-group-text"><i class="fa fa-calendar"></i></div>
       </div>
       </div>
       {hint}{error}
       '])->textInput() ?>
     </div>

    <div class="col-3">
      <?= $form->field($model, 'type_of_valuation['.$i.']')->widget(Select2::classname(), [
        'data' => yii::$app->quotationHelperFunctions->getTypeofvaluation(),
        'options' => ['class'=>'type_of_valuation'],
        'pluginOptions' => [
          'allowClear' => true
        ],
      ]); ?>
    </div>
    <div class="col-3">
      <?= $form->field($model, 'number_of_comparables['.$i.']')->widget(Select2::classname(), [
        'data' => yii::$app->quotationHelperFunctions->getNumberofComparables(),
        'options' => ['class'=>'number_of_comparables'],
        'pluginOptions' => [
          'allowClear' => true
        ],
      ]); ?>
    </div>

    <div class="col-3">
      <?= $form->field($model, 'date_of_inspection['.$i.']',['template'=>'
       {label}
       <div class="input-group date" id="company-d-inspection_date'.$i.'" data-target-input="nearest">
       {input}
       <div class="input-group-append" data-target="#company-d-inspection_date'.$i.'" data-toggle="datetimepicker">
       <div class="input-group-text"><i class="fa fa-calendar"></i></div>
       </div>
       </div>
       {hint}{error}
       '])->textInput() ?>
     </div>

     <div class="col-3">
      <?= $form->field($model, 'valuation_approach['.$i.']')->widget(Select2::classname(), [
                              'data' => Yii::$app->quotationHelperFunctions->valuationApproachArr,//yii::$app->quotationHelperFunctions->getValuationApproach(),
                              'options' => ['class'=>'valuation_approach'],
                              'pluginOptions' => [
                                'allowClear' => true
                              ],
                            ]); ?>
                          </div>

                          <div class="col-3">
                            <?= $form->field($model, 'property_quotation_fee['.$i.']')->textInput(['maxlength' => true,'class'=>'property-quotation-fee form-control']) ?>
                          </div>

                          <div class="col-3">
                            <?= $form->field($model, 'property_quotation_tat['.$i.']')->textInput(['maxlength' => true,'class'=>'property-quotation-tat form-control']) ?>
                          </div>



                          <?php
                          if ($model->quotation_status==1) {
                            if ($model->property_toe_fee[$i]=="") {
                              $model->property_toe_fee[$i] = $model->property_quotation_fee[$i];
                            }
                            if ($model->property_toe_tat[$i]=="") {
                              $model->property_toe_tat[$i] = $model->property_quotation_tat[$i];
                            }
                            ?>

                            <div class="col-3">
                              <?= $form->field($model, 'property_toe_fee['.$i.']')->textInput(['maxlength' => true]) ?>
                            </div>

                            <div class="col-3">
                              <?= $form->field($model, 'property_toe_tat['.$i.']')->textInput(['maxlength' => true]) ?>
                            </div>
                            <?php
                          }
                          ?>
                        </div>




<?php
$documents = \app\models\Properties::find()->where(["id" => $model->property[$i]])->one();
// echo "<pre>";
// print_r($documents);
// echo "</pre>";
// die();
?>
<div class="document-section">

<?php

if ($documents['required_documents']!=null) {
    if ($model->property[$i]>0) {

?>
     <section class="valuation-form card card-outline card-primary mx-4 document-card">

       <header class="card-header">
         <h2 class="card-title">
           <i class="fas fa-edit"></i>
           <b>Property Required Documents</b></h2>
         </header>
         <div class="card-body">
           <div class="row" style="padding: 10px">



                 <table id="requestTypes"
                 class="table table-striped table-bordered table-hover images-table">
                 <thead>
                   <tr>
                     <td><b>Description</b></td>
                     <td><b>Attachment</b></td>
                     <td></td>
                   </tr>
                 </thead>
                 <tbody>
                   <?php
                   $documents = \app\models\Properties::find()->where(["id" => $model->property[$i]])->one();
                   $document_explode =  explode(',',$documents['required_documents']);

                   foreach ($document_explode as $document){
                    $attachment_Details = \app\models\ProposalDocs::find()->where(["quotation_id" => $model->id, 'building_id' => $model->building[$i], 'property_id'=>$model->property[$i], "document_id" => $document])->one();
                    $attachment = $attachment_Details->attachment;
                    ?>
                    <tr>

                    <td><?= Yii::$app->appHelperFunctions->getPropertiesDocumentsListArr()[$document] ?></td>

                    <td>
                      <?php
                      if ($attachment <> null) {?>
                        <div class="meri-marzi" style="width:100px; margin-top:30px;">
                          <div class="form-group">
                            <div class="input-group" style="opacity:0;">
                              <div class="custom-file">
                                <input type="file"  style=" height:100px;" class="custom-file-input doc-file"
                                id="valuationquotation-property_doc"
                                name="ValuationQuotation[property_doc][<?= $i ?>][<?= $document ?>]"/>
                                <label class="custom-file-label">file</label>
                              </div>
                            </div>
                          </div>

                          <img src="<?php echo $attachment; ?>"
                          style="margin-top:-80px;"
                          width="100px"
                          alt="" title=""
                          data-placeholder="no_image.png"/>

                <!-- <a href="$attachment?>">
                <p style="color:#0288D1; margin-bottom:360px; margin-right:400px; margin-top:40px; font-weight: bold; padding-left:4px; padding-right :4px;
                font-size:22px; position:absolute; bottom:0; right:30px; background-color:#FFEBEE;";><i class="far fa-eye"></i></p><br></a>
                <p class="remove-multi-doc" style="color:#B71C1C; margin-bottom:330px; margin-right:400px; font-weight: bold; padding-left:4px;
                padding-right :4px; font-size:22px; position:absolute; bottom:0; right:30px; background-color:#FFEBEE;";><i class="fa fa-trash"></i></p><br> -->


                <input type="hidden"
                name="ValuationQuotation[recieve_docs][<?= $i ?>][<?= $document ?>]"
                value="<?= $attachment; ?>" class="attachment-address"/>
              </div>
              <?php
            }
            else {
              ?>
              <div class="meri-marzi" style="width:100px; margin-top:30px;">
                <div class="form-group">
                  <div class="input-group" style="opacity:0;">
                    <div class="custom-file">
                      <input type="file"  style=" height:100px;" class="custom-file-input doc-file"
                      id="valuationquotation-property_doc"
                      name="ValuationQuotation[property_doc][<?= $i ?>][<?= $document ?>]"/>
                      <label class="custom-file-label">file</label>
                    </div>
                  </div>
                </div>
                <img src="<?= Yii::$app->params['uploadIcon'] ?>"
                style="margin-top:-80px;"
                width="100px"
                alt="" title=""
                data-placeholder="no_image.png"/>
                <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                  target="_blank">
                  <span class="glyphicon glyphicon-eye-open"></span>
                </a>

                <input type="hidden"
                name="ValuationQuotation[recieve_docs][<?= $i ?>][<?= $document ?>]"
                value="" class="attachment-address"/>



              </div>
              <?php
            }
            ?>



          </td>

        </tr>
        <?php
      }
      ?>

    </tbody>
  </table>
</div>
</div>


</section>
<?php
 }
}
?>

</div>


</div>
</div>
