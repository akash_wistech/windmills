<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use  app\components\widgets\StatusVerified;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin(); ?>
<section class="master-form card card-outline card-primary">
    <header class="card-header">
        <h2 class="card-title"><strong class="">Fee Master File For Income Approach</strong></h2>
        <div class="card-tools">
            <a href="<?= Url::toRoute(['valuation-quotation/get-master-file-history?file_type=income']) ?>" target="_blank" class="btn btn-info btn-sm">History</a>
        </div>
    </header>

    <!-- 1- Client Type -->
    <!-- Client type -->
    <section class="master-form card card-outline card-secondary mx-4 my-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Client Type</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Bank</strong>
                            </td>
                            <td>
                                <strong>Corporate</strong>
                            </td>
                            <td>
                                <strong>Individual</strong>
                            </td>
                            <td>
                                <strong>Partner</strong>
                            </td>
                            <td>
                                <strong>Foreign Individual</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <input type="hidden" name="QuotationFeeMasterFile[approach_type]" value="income" >
                            <?php

                            $client_type =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'client_type'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');
                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][bank]"
                                            value="<?= (isset($client_type['bank'])) ? $client_type['bank'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][corporate]"
                                            value="<?= (isset($client_type['corporate'])) ? $client_type['corporate'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][individual]"
                                            value="<?= (isset($client_type['individual'])) ? $client_type['individual'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][partner]"
                                            value="<?= (isset($client_type['partner'])) ? $client_type['partner'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[client_type][foreign-individual]"
                                            value="<?= (isset($client_type['foreign-individual'])) ? $client_type['foreign-individual'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required/>
                                </div>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- 2- Subject Property -->
    <section class="master-form card card-outline card-success mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Type of Subject Property</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Warehouse</strong>
                            </td>
                            <td>
                                <strong>Residential - Building</strong>
                            </td>
                            <td>
                                <strong>Mixed Use - Building</strong>
                            </td>
                            <td>
                                <strong>Building - Commercial</strong>
                            </td>

                            <td>
                                <strong>Villa - Commercial</strong>
                            </td>
                            <td>
                                <strong>Leased Mall</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $subject_property =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'subject_property'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');
                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][warehouse]"
                                            value="<?= (isset($subject_property['warehouse'])) ? $subject_property['warehouse'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][residential_building]"
                                            value="<?= (isset($subject_property['residential_building'])) ? $subject_property['residential_building'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][mixed_use_building]"
                                            value="<?= (isset($subject_property['mixed_use_building'])) ? $subject_property['mixed_use_building'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][building_commercial]"
                                            value="<?= (isset($subject_property['building_commercial'])) ? $subject_property['building_commercial'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][villa_commercial]"
                                            value="<?= (isset($subject_property['villa_commercial'])) ? $subject_property['villa_commercial'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][leased_mall]"
                                            value="<?= (isset($subject_property['leased_mall'])) ? $subject_property['leased_mall'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- 3- City -->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>City</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Abu Dhabi</strong>
                            </td>
                            <td>
                                <strong>Dubai</strong>
                            </td>
                            <td>
                                <strong>Sharjah/UQ</strong>
                            </td>
                            <td>
                                <strong>Rak, Fuj</strong>
                            </td>
                            <td>
                                <strong>Ajman</strong>
                            </td>
                            <td>
                                <strong>Others</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $city =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'city'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');

                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][abu_dhabi]"
                                            value="<?= (isset($city['abu_dhabi'])) ? $city['abu_dhabi'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][dubai]"
                                            value="<?= (isset($city['dubai'])) ? $city['dubai'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][sharjah]"
                                            value="<?= (isset($city['sharjah'])) ? $city['sharjah'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][rak_fuj]"
                                            value="<?= (isset($city['rak_fuj'])) ? $city['rak_fuj'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][ajman]"
                                            value="<?= (isset($city['ajman'])) ? $city['ajman'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[city][others]"
                                            value="<?= (isset($city['others'])) ? $city['others'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- 5- Payment Terms -->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Payment Terms</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>100%</strong>
                            </td>
                            <td>
                                <strong>75%</strong>
                            </td>
                            <td>
                                <strong>50%</strong>
                            </td>
                            <td>
                                <strong>25%</strong>
                            </td>
                            <td>
                                <strong>0%</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $payment =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'payment_terms'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');

                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][100]"
                                            value="<?= (isset($payment['100'])) ? $payment['100'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][75]"
                                            value="<?= (isset($payment['75'])) ? $payment['75'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][50]"
                                            value="<?= (isset($payment['50'])) ? $payment['50'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][25]"
                                            value="<?= (isset($payment['25'])) ? $payment['25'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[payment_terms][0]"
                                            value="<?= (isset($payment['0'])) ? $payment['0'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <!-- 7- Complexity -->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Complexity</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Non-Standard</strong>
                            </td>
                            <td>
                                <strong>Standard</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $complexity =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'complexity'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[complexity][non_standard]"
                                            value="<?= (isset($complexity['non_standard'])) ? $complexity['non_standard'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[complexity][standard]"
                                            value="<?= (isset($complexity['standard'])) ? $complexity['standard'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- 8- New / Repeat Valuation -->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>New / Repeat Valuation</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>New Valuation</strong>
                            </td>
                            <td>
                                <strong>Repeat/Revalidation</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $new_repeat_valuation =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'new_repeat_valuation_data'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][new_valuation]"
                                            value="<?= (isset($new_repeat_valuation['new_valuation'])) ? $new_repeat_valuation['new_valuation'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[new_repeat_valuation][repeat_valuation]"
                                            value="<?= (isset($new_repeat_valuation['repeat_valuation'])) ? $new_repeat_valuation['repeat_valuation'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- 9- Built Up Area of subject property -->
    <section class="master-form card card-outline card-success mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Built Up Area of subject property</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1-10000</strong>
                            </td>
                            <td>
                                <strong>10001-25000</strong>
                            </td>
                            <td>
                                <strong>25001 - 50000</strong>
                            </td>
                            <td>
                                <strong>50,001 - 100000</strong>
                            </td>
                            <td>
                                <strong>100001-250000</strong>
                            </td>
                            <td>
                                <strong>250001-500000</strong>
                            </td>
                            <td>
                                <strong>500001-1000000</strong>
                            </td>
                            <td>
                                <strong>1000000+</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $BUA =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'built_up_area_of_subject_property'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($BUA);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][1_10000]"
                                            value="<?= (isset($BUA['1_10000'])) ? $BUA['1_10000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][10001_25000]"
                                            value="<?= (isset($BUA['10001_25000'])) ? $BUA['10001_25000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][25001_50000]"
                                            value="<?= (isset($BUA['25001_50000'])) ? $BUA['25001_50000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property]50001_100000]"
                                            value="<?= (isset($BUA['50001_100000'])) ? $BUA['50001_100000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][100001_250000]"
                                            value="<?= (isset($BUA['100001_250000'])) ? $BUA['100001_250000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][250001_500000]"
                                            value="<?= (isset($BUA['250001_500000'])) ? $BUA['250001_500000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][500001_1000000]"
                                            value="<?= (isset($BUA['500001_1000000'])) ? $BUA['500001_1000000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[built_up_area_of_subject_property][above_1000000]"
                                            value="<?= (isset($BUA['above_1000000'])) ? $BUA['above_1000000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number of units in the building (only applicable on building valuation)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1-10</strong>
                            </td>
                            <td>
                                <strong>11-25</strong>
                            </td>
                            <td>
                                <strong>26-50</strong>
                            </td>
                            <td>
                                <strong>51-100</strong>
                            </td>
                            <td>
                                <strong>101-250</strong>
                            </td>
                            <td>
                                <strong>251-500</strong>
                            </td>
                            <td>
                                <strong>501-1000</strong>
                            </td>
                            <td>
                                <strong>>1000+</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_units_in_building =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'number_of_units_building'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_units_in_building);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][1_10]"
                                            value="<?= (isset($no_of_units_in_building['1_10'])) ? $no_of_units_in_building['1_10'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][11_25]"
                                            value="<?= (isset($no_of_units_in_building['11_25'])) ? $no_of_units_in_building['11_25'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][26_50]"
                                            value="<?= (isset($no_of_units_in_building['26_50'])) ? $no_of_units_in_building['26_50'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][51_100]"
                                            value="<?= (isset($no_of_units_in_building['51_100'])) ? $no_of_units_in_building['51_100'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][101_250]"
                                            value="<?= (isset($no_of_units_in_building['101_250'])) ? $no_of_units_in_building['101_250'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][251_500]"
                                            value="<?= (isset($no_of_units_in_building['251_500'])) ? $no_of_units_in_building['251_500'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][501_1000]"
                                            value="<?= (isset($no_of_units_in_building['501_1000'])) ? $no_of_units_in_building['501_1000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_units_building][above_1000]"
                                            value="<?= (isset($no_of_units_in_building['above_1000'])) ? $no_of_units_in_building['above_1000'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- 11- Type of valuation -->
    <section class="master-form card card-outline card-warning mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Type of valuation</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Desktop</strong>
                            </td>
                            <td>
                                <strong>Physical Inspection</strong>
                            </td>
                            <td>
                                <strong>Drive by</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $type_of_valuation =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'type_of_valuation'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($type_of_valuation);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[type_of_valuation][desktop]"
                                            value="<?= (isset($type_of_valuation['desktop'])) ? $type_of_valuation['desktop'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[type_of_valuation][physical_inspection]"
                                            value="<?= (isset($type_of_valuation['physical_inspection'])) ? $type_of_valuation['physical_inspection'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[type_of_valuation][drive_by]"
                                            value="<?= (isset($type_of_valuation['drive_by'])) ? $type_of_valuation['drive_by'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- 12- Number of Comparables -->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number of Comparables</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Less Than 3</strong>
                            </td>
                            <td>
                                <strong>3 - 5</strong>
                            </td>
                            <td>
                                <strong>6 - 10</strong>
                            </td>
                            <td>
                                <strong>11 - 15</strong>
                            </td>
                            <td>
                                <strong>Above 15</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_Comparables =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'number_of_comparables_data'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_Comparables);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_comparables][less_than_3]"
                                            value="<?= (isset($no_of_Comparables['less_than_3'])) ? $no_of_Comparables['less_than_3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_comparables][3_5]"
                                            value="<?= (isset($no_of_Comparables['3_5'])) ? $no_of_Comparables['3_5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_comparables][6_10]"
                                            value="<?= (isset($no_of_Comparables['6_10'])) ? $no_of_Comparables['6_10'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_comparables][11_15]"
                                            value="<?= (isset($no_of_Comparables['11_15'])) ? $no_of_Comparables['11_15'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[number_of_comparables][above_15]"
                                            value="<?= (isset($no_of_Comparables['above_15'])) ? $no_of_Comparables['above_15'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <!-- 10- Number of Units on the land -->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Upgrades Ratings</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Low</strong>
                            </td>
                            <td>
                                <strong>Medium</strong>
                            </td>
                            <td>
                                <strong>High</strong>
                            </td>

                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $upgrades_ratings =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'upgrades_ratings'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r(upgrades_ratings);
                            // die();
                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][1]"
                                            value="<?= (isset($upgrades_ratings['1'])) ? $upgrades_ratings['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][2]"
                                            value="<?= (isset($upgrades_ratings['2'])) ? $upgrades_ratings['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[upgrades_ratings][3]"
                                            value="<?= (isset($upgrades_ratings['3'])) ? $upgrades_ratings['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>



                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="master-form card card-outline card-warning mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number of units in the same building (discount (%))</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 - (One)</strong>
                            </td>
                            <td>
                                <strong>2 - (Two)</strong>
                            </td>
                            <td>
                                <strong>3 - (Three)</strong>
                            </td>
                            <td>
                                <strong>4 - (Four)</strong>
                            </td>
                            <td>
                                <strong>5 - (Five)</strong>
                            </td>
                            <td>
                                <strong> >5 - (More than five)</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_units_same_building_dis =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'no_of_units_same_building_discount'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');

                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][1]"
                                            value="<?= (isset($no_of_units_same_building_dis['1'])) ? $no_of_units_same_building_dis['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][2]"
                                            value="<?= (isset($no_of_units_same_building_dis['2'])) ? $no_of_units_same_building_dis['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][3]"
                                            value="<?= (isset($no_of_units_same_building_dis['3'])) ? $no_of_units_same_building_dis['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][4]"
                                            value="<?= (isset($no_of_units_same_building_dis['4'])) ? $no_of_units_same_building_dis['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][5]"
                                            value="<?= (isset($no_of_units_same_building_dis['5'])) ? $no_of_units_same_building_dis['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_units_same_building_discount][6]"
                                            value="<?= (isset($no_of_units_same_building_dis['6'])) ? $no_of_units_same_building_dis['6'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- 4- Number of properties(discount) -->
    <section class="master-form card card-outline card-warning mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number of properties (discount (%))</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 - (One)</strong>
                            </td>
                            <td>
                                <strong>2 - (Two)</strong>
                            </td>
                            <td>
                                <strong>3 - (Three)</strong>
                            </td>
                            <td>
                                <strong>4 - (Four)</strong>
                            </td>
                            <td>
                                <strong>5 - (Five)</strong>
                            </td>
                            <td>
                                <strong> >5 - (More than five)</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_property_discount =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'no_of_property_discount'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_property_dis);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][1]"
                                            value="<?= (isset($no_of_property_discount['1'])) ? $no_of_property_discount['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][2]"
                                            value="<?= (isset($no_of_property_discount['2'])) ? $no_of_property_discount['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][3]"
                                            value="<?= (isset($no_of_property_discount['3'])) ? $no_of_property_discount['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][4]"
                                            value="<?= (isset($no_of_property_discount['4'])) ? $no_of_property_discount['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][5]"
                                            value="<?= (isset($no_of_property_discount['5'])) ? $no_of_property_discount['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[no_of_property_discount][6]"
                                            value="<?= (isset($no_of_property_discount['6'])) ? $no_of_property_discount['6'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- first time discount -->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>First time Valuation Discount (%)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>First time Valuation Discount</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $first_time_discount =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'first_time_discount'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($baseFeeOthers);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[first_time_discount][first_time_discount]"
                                            value="<?= (isset($first_time_discount['first_time_discount'])) ? $first_time_discount['first_time_discount'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- 14- Base Fee of Buildings-->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Urgency Fee</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Urgency Fee</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $baseFeeBuilding =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'urgency_fee'])->andWhere(['approach_type' => 'income'])->all(), 'sub_heading', 'values');

                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[urgency_fee][urgency_fee]"
                                            value="<?= (isset($baseFeeBuilding['urgency_fee'])) ? $baseFeeBuilding['urgency_fee'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="row mx-3">
        <div class="col">
            <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
        </div>
    </div>
    
<?php  
    $latestUpdatedUser = '';
    $query = \app\models\History::find()->where(['rec_type'=>'masterfile','file_type'=>'income'])->orderBy(['id' => SORT_DESC])->one();
    if($query<>null){
        $latestUpdatedUser = $query->user->firstname.' '.$query->user->lastname;
    }
?>

    <div class="card-footer bg-light">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mx-2']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                <?php 
            if($latestUpdatedUser<>null){?>
                <span class="badge grid-badge badge-dark mx-2"><mark>Last Updated By <?= $latestUpdatedUser ?></mark></span>
        <?php
            }
        ?>
    </div>
</section>

<?php ActiveForm::end(); ?>
