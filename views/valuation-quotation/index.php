 <?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

// $status = yii::$app->quotationHelperFunctions->getQuotationStatusArr();
// echo "<pre>";
// print_r($model);
// echo "</pre>";
// die();

/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationQuotationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quotations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="valuation-quotation-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' =>"
          <div class=\"card card-primary card-outline\">
            <div class=\"card-header\">
              <h3 class=\"card-title\">
                List
              </h3>
               <div class=\"card-tools\">
                    ".Html::a(Yii::t('app', '<i class="fa fa-plus" aria-hidden="true"></i>'), ['create'], ['class' => ['btn btn-primary','py-0 px-2 mr-2 ','font-weight-bold']])."
               </div>
            </div>


                <div class=\"card-body tbl-container table-responsive table-responsive-lg  table-striped table-hover mb-0\">
                    {items}
                </div>

                <div class=\"card-footer\">
                      <div class=\"row\">
                      <div class=\"col-sm-3\">{summary}</div>
                        <div class=\"col-sm-9 12 pager-container\">{pager}</div>
                      </div>
                    </div>
        </div>
          ",

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'reference_number',
            ['attribute' => 'client_name',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                    return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            // 'client_reference',
            'client_customer_name',
            'inquiry_date',
            'expiry_date',
            // 'quotation_status',
            ['attribute' => 'quotation_status',
                'label' => Yii::t('app', 'Status'),
                'value' => function ($model) {
                    return Yii::$app->quotationHelperFunctions->getQuotationStatusArr()[$model->quotation_status];
                },'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'],
            ],

            [
              'class' => 'yii\grid\ActionColumn',
              'header'=>'',
              'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
              'contentOptions'=>['class'=>'noprint actions'],
              'template' => '
                <div class="btn-group flex-wrap">
                  <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                  </button>
                  <div class="dropdown-menu" role="menu">
                     {update}
                  </div>
                </div>',
              'buttons' => [
                  'send-quotation' => function ($url, $model) {
                    return Html::a('<i class="fas fa-share"></i> '.Yii::t('app', 'Send Quotation'), "valuation-quotation/send-quotation?id=$model->id", [
                      'title' => Yii::t('app', 'Pdf'),
                      'class'=>'dropdown-item text-1',
                      'data-pjax'=>"0",
                    ]);
                  },
                  'send-toe' => function ($url, $model) {
                    return Html::a('<i class="fas fa-share"></i> '.Yii::t('app', 'Send TOE'), "valuation-quotation/send-toe?id=$model->id", [
                      'title' => Yii::t('app', 'Pdf'),
                      'class'=>'dropdown-item text-1',
                      'data-pjax'=>"0",
                    ]);
                  },
                  'pdf' => function ($url, $model) {
                    return Html::a('<i class="fas fa-file-pdf"></i> '.Yii::t('app', 'Download Quotation'), "valuation-quotation/qpdf?id=$model->id", [
                      'title' => Yii::t('app', 'Pdf'),
                      'class'=>'dropdown-item text-1',
                      'data-pjax'=>"0",
                    ]);
                  },
                  'toe' => function ($url, $model) {
                    return Html::a('<i class="fas fa-file-pdf"></i> '.Yii::t('app', 'Download TOE'), "valuation-quotation/toe?id=$model->id", [
                      'title' => Yii::t('app', 'TOE-Pdf'),
                      'class'=>'dropdown-item text-1',
                      'data-pjax'=>"0",
                    ]);
                  },
                  'view' => function ($url, $model) {
                    return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                      'title' => Yii::t('app', 'View'),
                      'class'=>'dropdown-item text-1',
                      'data-pjax'=>"0",
                    ]);
                  },
                  'update' => function ($url, $model) {
                    return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                      'title' => Yii::t('app', 'Edit'),
                      'class'=>'dropdown-item text-1',
                      'data-pjax'=>"0",
                    ]);
                  },
                  'delete' => function ($url, $model) {
                    return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                      'title' => Yii::t('app', 'Delete'),
                      'class'=>'dropdown-item text-1',
                      'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                      'data-method'=>"post",
                      'data-pjax'=>"0",
                    ]);
                  },
              ],
            ],


        ],

        'pager'=>[
            'maxButtonCount' => 5,
            'linkContainerOptions' => ['class'=>'paginate_button page-item'],
            'linkOptions' => ['class'=>'page-link'],
            'prevPageCssClass' => 'previous',
            'disabledPageCssClass' => 'disabled',
            'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link'],
          ],
    ]); ?>


</div>
