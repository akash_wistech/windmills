<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\assets\QuotationFormAsset;
QuotationFormAsset::register($this);
use app\assets\QuotationBuildingAsset;
QuotationBuildingAsset::register($this);

// $ref = Yii::$app->quotationHelperFunctions->uniqueReference;
// print_r($ref);
// die();

$ValuerName= \app\models\User::find()
->select(['id','fullname'=>'CONCAT(firstname," ",lastname)'])
->asArray()
->all();
// echo "string";
// echo "<pre>";
// print_r($ValuerName);
// echo "</pre>";
// die();


$paymentTypes = yii::$app->quotationHelperFunctions->paymentTerms;
$status = yii::$app->quotationHelperFunctions->getStatusArr();

$number = $model->no_of_properties;

if ($model->no_of_properties>10) {
    $model->no_of_properties="other";
}



$this->registerJs('


  $("body").on("change" ,".propertynumbers", function(){
      var number = $("#valuationquotation-no_of_properties").val();
      if (number=="other") {
          $("#show-other").show();
      }
      else {
          $("#valuationquotation-other").val("");
          AppBlockSave();
      }
      });

      $("body").on("change" ,"#valuationquotation-other", function(){
          AppBlockSave();
       });






  $("#company-d-start_date,#company-d-lead_date,#company-d-date").datetimepicker({
      allowInputToggle: true,
      viewMode: "months",
      format: "YYYY-MM-DD"
      });



  $("body").on("change","#valuationquotation-relative_discount,#valuationquotation-quotation_recommended_fee", function (){
      Discount();
      });






// payment file working
$(".payment-imgInps").change(function(){
  readURLPayment(this);
  });


  function readURLPayment(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if(input.value.includes(".pdf")==true){
                $("#payment").attr("src", "'.Yii::$app->params['pdfIcon'].'");
            }
            else if(input.value.includes(".docx")==true){
                $("#payment").attr("src", "'.Yii::$app->params['wordIcon'].'");
            }
            else{
                $("#payment").attr("src", e.target.result);
            }

        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("body").on("click", ".remove_payment_file", function () {
_this=$(this);
swal({
    title: "'.Yii::t('app','Confirmation').'",
    html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
    },function(result) {
        if (result) {
          $.ajax({
              url: "'.Url::to(['valuation-quotation/payment-delete','id'=>$model->id]).'",
              dataType: "html",
              type: "POST",
              success: function(html) {
                  $("#payment").attr("src", "'.Yii::$app->params['paymentDummyImage'].'");
                  },
                  error: function(xhr,ajaxoptions,thownError){

// alert(xhr.responseText);
      }
      });
  }
  });
  });
//end payment file Working







//Toe document file working
$(".toe-imgInps").change(function(){
  readURLToe(this);
  });


  function readURLToe(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if(input.value.includes(".pdf")==true){
                $("#toe").attr("src", "'.Yii::$app->params['pdfIcon'].'");
            }
            else if(input.value.includes(".docx")==true){
                $("#toe").attr("src", "'.Yii::$app->params['wordIcon'].'");
            }
            else{
                $("#toe").attr("src", e.target.result);
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("body").on("click", ".remove_toe_file", function () {

    _this=$(this);
    swal({
        title: "'.Yii::t('app','Confirmation').'",
        html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#47a447",
        confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
        cancelButtonText: "'.Yii::t('app','Cancel').'",
        },function(result) {
            if (result) {
              $.ajax({
                  url: "'.Url::to(['valuation-quotation/toe-delete','id'=>$model->id]).'",
                  dataType: "html",
                  type: "POST",
                  success: function(html) {
                      $("#toe").attr("src", "'.Yii::$app->params['paymentDummyImage'].'");
                      },
                      error: function(xhr,ajaxoptions,thownError){

// alert(xhr.responseText);
                      }
                      });
                  }
                  });
                  });
// Toe Document file working end


');

  ?>

  <script>



    function AppBlockSave(){
    App.blockUI({
        iconOnly: true,
        target: ".valuation-quotation-form-main",
        overlayColor: "none",
        cenrerY: true,
        boxed: true
    });
    document.getElementById("quotation-from").submit();
    //App.unblockUI($(_targetContainer));
}






var buildingNoArr = <?= json_encode(yii::$app->quotationHelperFunctions->numberofUnitsInBuildingArr) ?>;
var LandNoArr = <?= json_encode(yii::$app->quotationHelperFunctions->numberofUnitsInLandArr) ?>;





    function Discount(){
        var recommendedFee = $("#valuationquotation-recommended_fee").val();
        var QrecommendedFee = $("#valuationquotation-quotation_recommended_fee").val();
        var relativeDiscount = $("#valuationquotation-relative_discount").val();
        var final_time = $("#valuationquotation-quotation_turn_around_time").val();

        if (relativeDiscount!="" && QrecommendedFee>0) {
            // console.log("hosh");
            var url_address = 'valuation-quotation/discount'
            var data = {QrecommendedFee:QrecommendedFee, relative_discount:relativeDiscount}
            $.ajax({
                url: url_address,
                data: data,
                method: 'post',
                dataType: "JSON",
                success: function(data) {
                    // console.log(data);
                    $("#valuationquotation-quotation_recommended_fee").val(data);
                },
            });
        }
        // else if(relativeDiscount==null){
        //     $("#valuationquotation-quotation_recommended_fee").val(QrecommendedFee);
        // }

        // if (QrecommendedFee!="") {
        //   $("#valuationquotation-toe_final_fee").val(QrecommendedFee);
        // }
        //
        else {
            $("#valuationquotation-quotation_recommended_fee").val(QrecommendedFee);
        }
    }

</script>


<div class="valuation-quotation-form-main card card-primary card-outline">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id'=>'quotation-from']]); ?>

    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <b>Client General Details</b>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <?= $form->field($model, 'reference_number')->textInput(['maxlength' => true, 'readonly' => true,
                    // 'value' => ($model->reference_number <> null) ? $model->reference_number :
                    //  Yii::$app->quotationHelperFunctions->uniqueReference
            ])
            ?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'client_name')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title'),
                'options' => ['placeholder' => 'Select a Client ...', 'class'=>'client'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'client_type')->widget(Select2::classname(), [
                'data' => yii::$app->quotationHelperFunctions->clienttype,
                'options' => ['placeholder' => 'Select a Client Type...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'client_customer_name')->textInput(['maxlength' => true, 'class'=>'form-control client_customer_name']) ?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'other_intended_users')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'scope_of_service')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-4">
            <?=  $form->field($model, 'purpose_of_valuation')->widget(Select2::classname(), [
                'data' => Yii::$app->quotationHelperFunctions->purposeOfValuationForQuotationArr,
                'options' => ['placeholder' => 'Select a Purpose ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'inquiry_date',['template'=>'
             {label}
             <div class="input-group date" id="company-d-start_date" data-target-input="nearest">
             {input}
             <div class="input-group-append" data-target="#company-d-start_date" data-toggle="datetimepicker">
             <div class="input-group-text"><i class="fa fa-calendar"></i></div>
             </div>
             </div>
             {hint}{error}
             '])->textInput() ?>
         </div>

         <div class="col-4">
            <?= $form->field($model, 'expiry_date',['template'=>'
              {label}
              <div class="input-group date" id="company-d-lead_date" data-target-input="nearest">
              {input}
              <div class="input-group-append" data-target="#company-d-lead_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
              </div>
              {hint}{error}
              '])->textInput() ?>
          </div>
          <div class="col-4">
            <?= $form->field($model, 'advance_payment_terms')->widget(Select2::classname(), [
                'data' => $paymentTypes,
                'options' => ['placeholder' => 'Select a Payment Term...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
          <div class="col-4">
            <?= $form->field($model, 'assumptions')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'no_of_properties')->widget(Select2::classname(), [
                'data' => yii::$app->quotationHelperFunctions->howManyProperties,
                'options' => ['placeholder' => 'Select...','class'=>'form-control propertynumbers'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>
    <div class="row" id="show-other" <?=  $model->no_of_properties=="other" ? '' : ' style="display:none;"' ?>>
        <div class="col">
            <?= $form->field($model, 'other')->label(false) ->textInput() ?>
        </div>
    </div>
</div>




<div id="conflict-area">
    <?php
    if ($model->client_name>0) {
        // echo "string";die();
        echo $this->render('conflict_form',[
            'form'=>$form,
            'model'=>$model,
            'buyer_data' => $buyer_data,
            'seller_data' => $seller_data,
            'client_data' => $client_data,
            'property_data' => $property_data,
        ]);
    }

    ?>
</div>





<div id="subjectproperty">
    <?php

    $n=0;
    $existingProperties = yii::$app->quotationHelperFunctions->getExistingProperties($model->id);
    if($existingProperties !=null){
        foreach ($existingProperties as $existingProperty) {
            if ($n < $number) {
                $model->building[$n] = $existingProperty['building'];
                $model->property[$n] = $existingProperty['property'];
                $model->property_category[$n] = $existingProperty['property_category'];
                $model->tenure[$n] = $existingProperty['tenure'];
                $model->city[$n] = $existingProperty['city'];
                $model->community[$n] = $existingProperty['community'];
                $model->sub_community[$n] = $existingProperty['sub_community'];
                $model->owner_name[$n] = $existingProperty['owner_name'];
                $model->building_number[$n] = $existingProperty['building_number'];
                $model->plot_number[$n] = $existingProperty['plot_number'];
                $model->unit_number[$n] = $existingProperty['unit_number'];
                $model->floor_number[$n] = $existingProperty['floor_number'];
                $model->no_of_floors[$n] = $existingProperty['no_of_floors'];
                $model->no_of_units[$n] = $existingProperty['no_of_units'];
                $model->land_size[$n] = $existingProperty['land_size'];
                $model->built_up_area[$n] = $existingProperty['built_up_area'];
                $model->street[$n] = $existingProperty['street'];
                $model->latitude[$n] = $existingProperty['latitude'];
                $model->longitude[$n] = $existingProperty['longitude'];
                // $model->valuation_type[$n] = $existingProperty['valuation_type'];
                $model->repeat_valuation[$n] = $existingProperty['repeat_valuation'];
                $model->valuation_date[$n] = $existingProperty['valuation_date'];
                $model->date_of_inspection[$n] = $existingProperty['date_of_inspection'];
                // $model->phone_number[$n] = $existingProperty['phone_number'];
                // $model->email[$n] = $existingProperty['email'];
                $model->complexity[$n] = $existingProperty['complexity'];
                $model->type_of_valuation[$n] = $existingProperty['type_of_valuation'];
                $model->number_of_comparables[$n] = $existingProperty['number_of_comparables'];
                $model->valuation_approach[$n] = $existingProperty['valuation_approach'];
                $model->property_quotation_fee[$n] = $existingProperty['property_quotation_fee'];
                $model->property_quotation_tat[$n] = $existingProperty['property_quotation_tat'];
                $model->property_toe_fee[$n] = $existingProperty['property_toe_fee'];
                $model->property_toe_tat[$n] = $existingProperty['property_toe_tat'];


                // $model->required_documents[$n] =  explode(",",$existingProperty['required_documents']);
                // $model->documents[$n] =  explode(",",$existingProperty['required_documents']);

                $model->existingRecordIdz[$n] = $existingProperty['id'];

                echo $this->render('building_form',['i'=>$n,'form'=>$form,'model'=>$model]);
            }

            $n++;
        }
    }
    ?>
    <?php
    if ($number>0) {

        for ($i=$n; $i<$number ; $i++) {
            echo $this->render('building_form',['i'=>$i,'form'=>$form,'model'=>$model]);
        }
    }

        // $other_number = $model->other;
        // if ($other_number>0) {
        //     for ($i=$n; $i<$other_number ; $i++) {
        //           echo $this->render('building_form',['i'=>$i,'form'=>$form,'model'=>$model]);
        //         }
        // }

    ?>
</div>

<div class="card card-primary card-outline mx-4">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <b>Contact Details Form</b>
        </h3>
    </div>
    <div class="card-body">
        <div class="row py-2">
            <div class="col-4">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'form-control name']) ?>
            </div>


            <div class="col-4">

                <div class="form-group">
                  <label>Phone Number</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-plus"></i></span>
                    </div>
                    <input type="text"  class="form-control phone-number"
                           name="ValuationQuotation[phone_number]" onkeypress="isInputNumber(event)"
                           value="<?= (isset($model->phone_number)) ? $model->phone_number : '' ?>">
                  </div>
                </div>


            </div>

            <div class="col-4">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'class' => 'form-control email']) ?>
            </div>
        </div>
    </div>
</div>

<script>
function isInputNumber(evt){
  var ch = String.fromCharCode(evt.which);
    if(!(/[0-9]/.test(ch))){
        evt.preventDefault();
    }
}
</script>



<div class="card card-primary card-outline mx-4">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <b>Discount</b>
        </h3>
    </div>
    <div class="card-body">
        <div class="row py-2">
         <div class="col">
            <?= $form->field($model, 'relative_discount')->widget(Select2::classname(), [
                'data' => yii::$app->quotationHelperFunctions->relativediscount,
                'options' => ['placeholder' => 'Select a Relative Discount ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>
</div>
</div>


<div class="col-6" style="display:none;">
    <?=  $form->field($model, 'quotation_status')->widget(Select2::classname(), [
        'data' => yii::$app->quotationHelperFunctions->quotationStatusArr,
        'options' => [],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
</div>









<section class="valuation-form card card-outline card-primary mx-4">

    <header class="card-header">
        <i class="fas fa-edit"></i>
        <h2 class="card-title"><b>Documents Information</b></h2>
    </header>
    <div class="card-body">
        <div class="row" style="padding: 10px">
            <table id="requestTypes"
            class="table table-bordered images-table">
            <thead>
                <tr>
                    <td>
                        <div class="form-group">
                            <label for="exampleInputFile">Payment Slip</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input payment-imgInps" id="valuationquotation-payment_slip_file" name="ValuationQuotation[payment_slip_file]">
                                    <label class="custom-file-label">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <?php
                        $imgname = Yii::$app->params['paymentDummyImage'];
                        if ($model->payment_slip != null) {
                            $imgname = Yii::$app->get('s3bucket')->getUrl($model->payment_slip);
                                        // echo $imgname;die();
                        }
                        ?>
                        <div style='width:350px; margin-bottom:30px; position:relative;'>
                            <?php
                            if (strpos($imgname, '.pdf') !== false) {
                                ?>
                                <img src="<?= Yii::$app->params['pdfIcon']; ?>"  width="270"
                                alt="" title=""
                                data-placeholder="no_image.png"/>
                                <a href="<?= $imgname; ?>"
                                   target="_blank">
                                   <span class="glyphicon glyphicon-eye-open"></span>
                               </a>

                               <?php

                           }  else if (strpos($imgname, '.docx') !== false) {
                            ?>
                            <img src="<?= Yii::$app->params['wordIcon']; ?>"  width="270"
                            alt="" title=""
                            data-placeholder="no_image.png"/>
                            <a href="<?= $imgname; ?>"
                               target="_blank">
                               <span class="glyphicon glyphicon-eye-open"></span>
                           </a>
                           <?php
                       } else{?>
                        <img id="payment" src="<?= $imgname ?>" alt="No Image is selected." width="270"/>
                        <?php
                    }
                    ?>

                    <?php  if ($model->payment_slip != null) {    ?>
                        <p class="remove_payment_file" style="color:#B71C1C; margin-bottom:100px; margin-right:0px; font-weight: bold; padding-left:4px; padding-right :4px; font-size:22px; position:absolute; bottom:0; right:30px; background-color:#FFEBEE;";
                        ><i class="fa fa-trash"></i></p><br>

                        <a href="<?=$imgname?>">
                            <p style="color:#0288D1; margin-bottom:140px; margin-right:0px; margin-top:40px; font-weight: bold; padding-left:4px; padding-right :4px; font-size:22px; position:absolute; bottom:0; right:30px; background-color:#FFEBEE;";
                            ><i class="far fa-eye"></i></p><br></a>
                        <?php } ?>
                    </div>
                </td>


                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                <td>
                    <div class="form-group">
                        <label for="exampleInputFile">TOE Document</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input toe-imgInps" id="valuationquotation-toe_document_file" name="ValuationQuotation[toe_document_file]">
                                <label class="custom-file-label">Choose file</label>
                            </div>
                        </div>
                    </div>

                    <?php
                    $imgname = Yii::$app->params['toeDummyImage'];
                    if ($model->toe_document != null) {
                        $imgname = Yii::$app->get('s3bucket')->getUrl($model->toe_document);
                                         // echo $imgname;die();

                    }
                    ?>
                    <div style='width:350px; margin-bottom:30px; position:relative;'>
                        <?php
                        if (strpos($imgname, '.pdf') !== false) {
                            ?>
                            <img src="<?= Yii::$app->params['pdfIcon']; ?>"  width="270"
                            alt="" title=""
                            data-placeholder="no_image.png"/>
                            <a href="<?= $imgname; ?>"
                               target="_blank">
                               <span class="glyphicon glyphicon-eye-open"></span>
                           </a>

                           <?php

                       }  else if (strpos($imgname, '.docx') !== false) {
                        ?>
                        <img src="<?= Yii::$app->params['wordIcon']; ?>"  width="270"
                        alt="" title=""
                        data-placeholder="no_image.png"/>
                        <a href="<?= $imgname; ?>"
                           target="_blank">
                           <span class="glyphicon glyphicon-eye-open"></span>
                       </a>
                       <?php
                   } else{?>
                    <img id="toe" src="<?= $imgname ?>" alt="No Image is selected." width="270"/>
                    <?php
                }
                ?>

                <?php  if ($model->toe_document != null) {    ?>
                    <p class="remove_toe_file" style="color:#B71C1C;  margin-bottom:100px; margin-right:0px; font-weight: bold; padding-left:4px; padding-right :4px; font-size:22px; position:absolute; bottom:0; right:30px; background-color:#FFEBEE;";
                    ><i class="fa fa-trash"></i></p><br>
                    <a href="<?=$imgname?>">
                        <p style="color:#0288D1; margin-bottom:140px; margin-right:0px; margin-top:40px; font-weight: bold; padding-left:4px; padding-right :4px; font-size:22px; position:absolute; bottom:0; right:30px; background-color:#FFEBEE;";
                        ><i class="far fa-eye"></i></p><br></a>
                    <?php } ?>
                </div>

            </td>
        </tr>
    </thead>
</table>
</div>
</div>
</section>











<div class="row" style="display:none;">
    <div class="col">
        <?=  $form->field($model, 'status')->widget(Select2::classname(), [
            'data' => yii::$app->quotationHelperFunctions->statusArr,
            'options' => ['placeholder' => 'Select a Status Type ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
    </div>
</div>
</div>

<div class="card-footer bg-light">
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mr-2']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
    </div>
</div>

</div>

<?php ActiveForm::end(); ?>
