<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use  app\components\widgets\StatusVerified;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin(); ?>
<section class="master-form card card-outline card-primary">
    <header class="card-header">
        <h2 class="card-title"><strong class="">Fee Master File For Profit Approach</strong></h2>
        <div class="card-tools">
            <a href="<?= Url::toRoute(['valuation-quotation/get-master-file-history?file_type=profit']) ?>" target="_blank" class="btn btn-info btn-sm">History</a>
        </div>
    </header>

    <!--Propety Information -->
    <section class="master-form card card-outline card-success mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Type of Subject Property (Base fee) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Hotel</strong>
                            </td>
                            <td>
                                <strong>Mall</strong>
                            </td>
                            <td>
                                <strong>Hospital</strong>
                            </td>
                            <td>
                                <strong>Resort</strong>
                            </td>
                            <td>
                                <strong>School</strong>
                            </td>

                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $subject_property =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'subject_property'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][9]"
                                            value="<?= (isset($subject_property['9'])) ? $subject_property['9'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][18]"
                                            value="<?= (isset($subject_property['18'])) ? $subject_property['18'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][43]"
                                            value="<?= (isset($subject_property['43'])) ? $subject_property['43'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][85]"
                                            value="<?= (isset($subject_property['85'])) ? $subject_property['85'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[subject_property][16]"
                                            value="<?= (isset($subject_property['16'])) ? $subject_property['16'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>



                        </tr>


                        </tbody>


                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Client Information-->
    <section class="master-form card card-outline card-secondary mx-4 my-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Client Segment(%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Bank</strong>
                            </td>
                            <td>
                                <strong>Corporate</strong>
                            </td>
                            <td>
                                <strong>Individual</strong>
                            </td>
                            <td>
                                <strong>Partner</strong>
                            </td>
                            <td>
                                <strong>Foreign Individual</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <input type="hidden" name="QuotationFeeMasterFile[approach_type]" value="profit">
                            <?php

                            $client_type = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'client_type'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');


                            ?>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[client_type][bank]"
                                           value="<?= (isset($client_type['bank'])) ? $client_type['bank'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[client_type][corporate]"
                                           value="<?= (isset($client_type['corporate'])) ? $client_type['corporate'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[client_type][individual]"
                                           value="<?= (isset($client_type['individual'])) ? $client_type['individual'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[client_type][partner]"
                                           value="<?= (isset($client_type['partner'])) ? $client_type['partner'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required/>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[client_type][foreign-individual]"
                                           value="<?= (isset($client_type['foreign-individual'])) ? $client_type['foreign-individual'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required/>
                                </div>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--City Information-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>City (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Abu Dhabi</strong>
                            </td>
                            <td>
                                <strong>Dubai</strong>
                            </td>
                            <td>
                                <strong>Sharjah/UQ</strong>
                            </td>
                            <td>
                                <strong>Rak, Fujairah</strong>
                            </td>
                            <td>
                                <strong>Ajman</strong>
                            </td>
                            <td>
                                <strong>Al Ain</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $city = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'city'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');

                            ?>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[city][abu_dhabi]"
                                           value="<?= (isset($city['abu_dhabi'])) ? $city['abu_dhabi'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[city][dubai]"
                                           value="<?= (isset($city['dubai'])) ? $city['dubai'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[city][sharjah]"
                                           value="<?= (isset($city['sharjah'])) ? $city['sharjah'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[city][rak_fuj]"
                                           value="<?= (isset($city['rak_fuj'])) ? $city['rak_fuj'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[city][ajman]"
                                           value="<?= (isset($city['ajman'])) ? $city['ajman'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[city][others]"
                                           value="<?= (isset($city['others'])) ? $city['others'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <!--Upgrades Information-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Upgrades/Age Ratings (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 Star</strong>
                            </td>
                            <td>
                                <strong>2 Star</strong>
                            </td>
                            <td>
                                <strong>3 Star</strong>
                            </td>
                            <td>
                                <strong>4 Star</strong>
                            </td>
                            <td>
                                <strong>5 Star</strong>
                            </td>

                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $upgrades_ratings = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'upgrades_ratings'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r(upgrades_ratings);
                            // die();
                            ?>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[upgrades_ratings][1]"
                                           value="<?= (isset($upgrades_ratings['1'])) ? $upgrades_ratings['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[upgrades_ratings][2]"
                                           value="<?= (isset($upgrades_ratings['2'])) ? $upgrades_ratings['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[upgrades_ratings][3]"
                                           value="<?= (isset($upgrades_ratings['3'])) ? $upgrades_ratings['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[upgrades_ratings][4]"
                                           value="<?= (isset($upgrades_ratings['4'])) ? $upgrades_ratings['4'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[upgrades_ratings][5]"
                                           value="<?= (isset($upgrades_ratings['5'])) ? $upgrades_ratings['5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Land size Information-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Standard Land size (only applicable on land valuation) (%)(+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 - 10,000</strong>
                            </td>
                            <td>
                                <strong>10,001 - 25,000</strong>
                            </td>
                            <td>
                                <strong>25,001 - 50,000</strong>
                            </td>
                            <td>
                                <strong>50,001 - 150,000</strong>
                            </td>
                            <td>
                                <strong>150,001 - 300,000</strong>
                            </td>
                            <td>
                                <strong>300,001-500,000</strong>
                            </td>
                            <td>
                                <strong>500,001-750,000</strong>
                            </td>
                            <td>
                                <strong>Above 750,000</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $land = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'land'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($land);
                            // die();
                            ?>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[land][1_10000]"
                                           value="<?= (isset($land['1_10000'])) ? $land['1_10000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[land][10001_25000]"
                                           value="<?= (isset($land['10001_25000'])) ? $land['10001_25000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[land][25001_50000]"
                                           value="<?= (isset($land['25001_50000'])) ? $land['25001_50000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[land][50001_150000]"
                                           value="<?= (isset($land['50001_150000'])) ? $land['50001_150000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[land][150001_300000]"
                                           value="<?= (isset($land['150001_300000'])) ? $land['150001_300000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[land][300001_500000]"
                                           value="<?= (isset($land['300001_500000'])) ? $land['300001_500000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[land][500001_750000]"
                                           value="<?= (isset($land['500001_750000'])) ? $land['500001_750000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[land][above_750000]"
                                           value="<?= (isset($land['above_750000'])) ? $land['above_750000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Built Up Area of subject property-->
    <section class="master-form card card-outline card-success mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Built Up Area of subject property</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 - 50,000</strong>
                            </td>
                            <td>
                                <strong>50,001-125,000</strong>
                            </td>
                            <td>
                                <strong>125,001 - 250,000</strong>
                            </td>
                            <td>
                                <strong>250,001 - 750,000</strong>
                            </td>
                            <td>
                                <strong>750,001-1,500,000</strong>
                            </td>
                            <td>
                                <strong>1,500,001-2,500,000</strong>
                            </td>
                            <td>
                                <strong>2,500,001-3,750,000</strong>
                            </td>
                            <td>
                                <strong>Above 3,750,000</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $BUA = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'built_up_area_of_subject_property'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[built_up_area_of_subject_property][1_50000]"
                                           value="<?= (isset($BUA['1_50000'])) ? $BUA['1_50000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[built_up_area_of_subject_property][50001_125000]"
                                           value="<?= (isset($BUA['50001_125000'])) ? $BUA['50001_125000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[built_up_area_of_subject_property][125001_250000]"
                                           value="<?= (isset($BUA['125001_250000'])) ? $BUA['125001_250000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[built_up_area_of_subject_property][250001_750000]"
                                           value="<?= (isset($BUA['250001_750000'])) ? $BUA['250001_750000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[built_up_area_of_subject_property][750001_1500000]"
                                           value="<?= (isset($BUA['750001_1500000'])) ? $BUA['750001_1500000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[built_up_area_of_subject_property][1500001_2500000]"
                                           value="<?= (isset($BUA['1500001_2500000'])) ? $BUA['1500001_2500000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[built_up_area_of_subject_property][2500001_3750000]"
                                           value="<?= (isset($BUA['2500001_3750000'])) ? $BUA['2500001_3750000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[built_up_area_of_subject_property][above_3750000]"
                                           value="<?= (isset($BUA['above_3750000'])) ? $BUA['above_3750000'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Built Up Area of subject property for Building Land-->
    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number of rooms in the subject property</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 - 50</strong>
                            </td>
                            <td>
                                <strong>51 - 100</strong>
                            </td>
                            <td>
                                <strong>101 - 200</strong>
                            </td>
                            <td>
                                <strong>201 - 350</strong>
                            </td>
                            <td>
                                <strong>351 - 500</strong>
                            </td>
                            <td>
                                <strong>501 - 750</strong>
                            </td>
                            <td>
                                <strong>Above 750</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_units_in_building = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'number_of_rooms_building'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_units_in_building);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_rooms_building][1]"
                                           value="<?= (isset($no_of_units_in_building['1'])) ? $no_of_units_in_building['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_rooms_building][2]"
                                           value="<?= (isset($no_of_units_in_building['2'])) ? $no_of_units_in_building['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_rooms_building][3]"
                                           value="<?= (isset($no_of_units_in_building['3'])) ? $no_of_units_in_building['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_rooms_building][4]"
                                           value="<?= (isset($no_of_units_in_building['4'])) ? $no_of_units_in_building['4'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_rooms_building][5]"
                                           value="<?= (isset($no_of_units_in_building['5'])) ? $no_of_units_in_building['5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_rooms_building][6]"
                                           value="<?= (isset($no_of_units_in_building['6'])) ? $no_of_units_in_building['6'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_rooms_building][7]"
                                           value="<?= (isset($no_of_units_in_building['7'])) ? $no_of_units_in_building['7'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="master-form card card-outline card-danger mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Types of units in the subject property</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 type</strong>
                            </td>
                            <td>
                                <strong>2 types</strong>
                            </td>
                            <td>
                                <strong>3 types</strong>
                            </td>
                            <td>
                                <strong>4 types</strong>
                            </td>
                            <td>
                                <strong>5 types</strong>
                            </td>
                            <td>
                                <strong>6 types</strong>
                            </td>
                            <td>
                                <strong>7 types</strong>
                            </td>
                            <td>
                                <strong>8 types</strong>
                            </td>
                            <td>
                                <strong>9 types</strong>
                            </td>
                            <td>
                                <strong>10 types</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_units_in_building = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'number_of_types'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_units_in_building);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_types][1]"
                                           value="<?= (isset($no_of_units_in_building['1'])) ? $no_of_units_in_building['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_types][2]"
                                           value="<?= (isset($no_of_units_in_building['2'])) ? $no_of_units_in_building['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_types][3]"
                                           value="<?= (isset($no_of_units_in_building['3'])) ? $no_of_units_in_building['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_types][4]"
                                           value="<?= (isset($no_of_units_in_building['4'])) ? $no_of_units_in_building['4'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_types][5]"
                                           value="<?= (isset($no_of_units_in_building['5'])) ? $no_of_units_in_building['5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_types][6]"
                                           value="<?= (isset($no_of_units_in_building['6'])) ? $no_of_units_in_building['6'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_types][7]"
                                           value="<?= (isset($no_of_units_in_building['7'])) ? $no_of_units_in_building['7'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_types][8]"
                                           value="<?= (isset($no_of_units_in_building['8'])) ? $no_of_units_in_building['8'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_types][9]"
                                           value="<?= (isset($no_of_units_in_building['9'])) ? $no_of_units_in_building['9'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_types][10]"
                                           value="<?= (isset($no_of_units_in_building['10'])) ? $no_of_units_in_building['10'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Number of Comparables-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number of Comparables (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Less Than 3</strong>
                            </td>
                            <td>
                                <strong>3 - 5</strong>
                            </td>
                            <td>
                                <strong>6 - 10</strong>
                            </td>
                            <td>
                                <strong>11 - 15</strong>
                            </td>
                            <td>
                                <strong>Above 15</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_Comparables = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'number_of_comparables_data'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_Comparables);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_comparables][less_than_3]"
                                           value="<?= (isset($no_of_Comparables['less_than_3'])) ? $no_of_Comparables['less_than_3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_comparables][3_5]"
                                           value="<?= (isset($no_of_Comparables['3_5'])) ? $no_of_Comparables['3_5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_comparables][6_10]"
                                           value="<?= (isset($no_of_Comparables['6_10'])) ? $no_of_Comparables['6_10'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_comparables][11_15]"
                                           value="<?= (isset($no_of_Comparables['11_15'])) ? $no_of_Comparables['11_15'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[number_of_comparables][above_15]"
                                           value="<?= (isset($no_of_Comparables['above_15'])) ? $no_of_Comparables['above_15'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Number of Restaurant-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Restaurant (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1</strong>
                            </td>
                            <td>
                                <strong>2</strong>
                            </td>
                            <td>
                                <strong>3</strong>
                            </td>
                            <td>
                                <strong>4</strong>
                            </td>

                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_Comparables = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'restaurant'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_Comparables);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[restaurant][1]"
                                           value="<?= (isset($no_of_Comparables['1'])) ? $no_of_Comparables['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[restaurant][2]"
                                           value="<?= (isset($no_of_Comparables['2'])) ? $no_of_Comparables['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[restaurant][3]"
                                           value="<?= (isset($no_of_Comparables['3'])) ? $no_of_Comparables['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[restaurant][4]"
                                           value="<?= (isset($no_of_Comparables['4'])) ? $no_of_Comparables['4'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <!--Number of Ballroom/function place/Auditorium-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Ballroom/function place/Auditorium (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1</strong>
                            </td>
                            <td>
                                <strong>2</strong>
                            </td>
                            <td>
                                <strong>3</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_Comparables = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'ballrooms'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_Comparables);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[ballrooms][1]"
                                           value="<?= (isset($no_of_Comparables['1'])) ? $no_of_Comparables['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[ballrooms][2]"
                                           value="<?= (isset($no_of_Comparables['2'])) ? $no_of_Comparables['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[ballrooms][3]"
                                           value="<?= (isset($no_of_Comparables['3'])) ? $no_of_Comparables['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Number of ATMs-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>ATMs (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1</strong>
                            </td>
                            <td>
                                <strong>2</strong>
                            </td>
                            <td>
                                <strong>3</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_Comparables = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'atms'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_Comparables);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[atms][1]"
                                           value="<?= (isset($no_of_Comparables['1'])) ? $no_of_Comparables['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[atms][2]"
                                           value="<?= (isset($no_of_Comparables['2'])) ? $no_of_Comparables['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[atms][3]"
                                           value="<?= (isset($no_of_Comparables['3'])) ? $no_of_Comparables['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Number of Retails units-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Retails units (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong><3</strong>
                            </td>
                            <td>
                                <strong>4-10</strong>
                            </td>
                            <td>
                                <strong><10/strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $retails_units = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'retails_units'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_Comparables);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[retails_units][1]"
                                           value="<?= (isset($retails_units['1'])) ? $retails_units['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[retails_units][2]"
                                           value="<?= (isset($retails_units['2'])) ? $retails_units['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[retails_units][3]"
                                           value="<?= (isset($retails_units['3'])) ? $retails_units['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Number of Night Club-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Night Clubs (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1</strong>
                            </td>
                            <td>
                                <strong>2</strong>
                            </td>
                            <td>
                                <strong>3</strong>
                            </td>
                            <td>
                                <strong>4</strong>
                            </td>
                            <td>
                                <strong>5</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_Comparables = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'night_clubs'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_Comparables);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[night_clubs][1]"
                                           value="<?= (isset($no_of_Comparables['1'])) ? $no_of_Comparables['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[night_clubs][2]"
                                           value="<?= (isset($no_of_Comparables['2'])) ? $no_of_Comparables['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[night_clubs][3]"
                                           value="<?= (isset($no_of_Comparables['3'])) ? $no_of_Comparables['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[night_clubs][4]"
                                           value="<?= (isset($no_of_Comparables['4'])) ? $no_of_Comparables['4'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[night_clubs][5]"
                                           value="<?= (isset($no_of_Comparables['5'])) ? $no_of_Comparables['5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Number of Bar/cafe-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Bar/cafe (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1</strong>
                            </td>
                            <td>
                                <strong>2</strong>
                            </td>
                            <td>
                                <strong>3</strong>
                            </td>
                            <td>
                                <strong>4</strong>
                            </td>
                            <td>
                                <strong>5</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_Comparables = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'bars'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_Comparables);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[bars][1]"
                                           value="<?= (isset($no_of_Comparables['1'])) ? $no_of_Comparables['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[bars][2]"
                                           value="<?= (isset($no_of_Comparables['2'])) ? $no_of_Comparables['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[bars][3]"
                                           value="<?= (isset($no_of_Comparables['3'])) ? $no_of_Comparables['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[bars][4]"
                                           value="<?= (isset($no_of_Comparables['4'])) ? $no_of_Comparables['4'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[bars][5]"
                                           value="<?= (isset($no_of_Comparables['5'])) ? $no_of_Comparables['5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Health  club and swimming pool-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Health  club and swimming pool</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Yes</strong>
                            </td>
                            <td>
                                <strong>No</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $health_club = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'health_club'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[health_club][yes]"
                                           value="<?= (isset($health_club['yes'])) ? $health_club['yes'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[health_club][no]"
                                           value="<?= (isset($health_club['no'])) ? $health_club['no'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Number of Meeting Rooms-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Meeting Rooms (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1</strong>
                            </td>
                            <td>
                                <strong>2</strong>
                            </td>
                            <td>
                                <strong>3</strong>
                            </td>
                            <td>
                                <strong>4</strong>
                            </td>
                            <td>
                                <strong>5</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_Comparables = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'meeting_rooms'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_Comparables);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[meeting_rooms][1]"
                                           value="<?= (isset($no_of_Comparables['1'])) ? $no_of_Comparables['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[meeting_rooms][2]"
                                           value="<?= (isset($no_of_Comparables['2'])) ? $no_of_Comparables['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[meeting_rooms][3]"
                                           value="<?= (isset($no_of_Comparables['3'])) ? $no_of_Comparables['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[meeting_rooms][4]"
                                           value="<?= (isset($no_of_Comparables['4'])) ? $no_of_Comparables['4'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[meeting_rooms][5]"
                                           value="<?= (isset($no_of_Comparables['5'])) ? $no_of_Comparables['5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--spa-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>SPA</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Yes</strong>
                            </td>
                            <td>
                                <strong>No</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $spa = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'spa'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[spa][yes]"
                                           value="<?= (isset($spa['yes'])) ? $spa['yes'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[spa][no]"
                                           value="<?= (isset($spa['no'])) ? $spa['no'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--BEACH ACTIVITIES ACCESS-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Beach Activities Access</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Yes</strong>
                            </td>
                            <td>
                                <strong>No</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $beach_access = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'beach_access'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[beach_access][yes]"
                                           value="<?= (isset($beach_access['yes'])) ? $beach_access['yes'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[beach_access][no]"
                                           value="<?= (isset($beach_access['no'])) ? $beach_access['no'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <!--Number of Parking Sale-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Parking Sale (%) (+)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong><10</strong>
                            </td>
                            <td>
                                <strong>11-50</strong>
                            </td>
                            <td>
                                <strong>>50/strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $parking_sale = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'parking_sale'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_Comparables);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[parking_sale][1]"
                                           value="<?= (isset($retails_units['1'])) ? $retails_units['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[parking_sale][2]"
                                           value="<?= (isset($retails_units['2'])) ? $retails_units['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[parking_sale][3]"
                                           value="<?= (isset($retails_units['3'])) ? $retails_units['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Other intended users-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Other intended users</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Yes</strong>
                            </td>
                            <td>
                                <strong>No</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $other_intended_users = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'other_intended_users'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[other_intended_users][yes]"
                                           value="<?= (isset($other_intended_users['yes'])) ? $other_intended_users['yes'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[other_intended_users][no]"
                                           value="<?= (isset($other_intended_users['no'])) ? $other_intended_users['no'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Last 3 years Finance Provide</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Yes</strong>
                            </td>
                            <td>
                                <strong>No</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $last_three_years_finance =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'last_three_years_finance'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[last_three_years_finance][yes]"
                                            value="<?= (isset($last_three_years_finance['yes'])) ? $last_three_years_finance['yes'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[last_three_years_finance][no]"
                                            value="<?= (isset($last_three_years_finance['no'])) ? $last_three_years_finance['no'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>10 years projections to be provide</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Yes</strong>
                            </td>
                            <td>
                                <strong>No</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $ten_years_projections =  ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'ten_years_projections'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[ten_years_projections][yes]"
                                            value="<?= (isset($ten_years_projections['yes'])) ? $ten_years_projections['yes'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="QuotationFeeMasterFile[ten_years_projections][no]"
                                            value="<?= (isset($ten_years_projections['no'])) ? $ten_years_projections['no'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Urgency Fee-->
    <section class="master-form card card-outline card-primary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Timing(Urgency Fee) (%)(+)</strong></h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>Standard</strong>
                            </td>
                            <td>
                                <strong>Urgent Same Day</strong>
                            </td>
                            <td>
                                <strong>Urgent 1 Day</strong>
                            </td>
                            <td>
                                <strong>Urgent 2 Days</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $urgency_fee = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'urgency_fee'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[urgency_fee][standard]"
                                           value="<?= (isset($urgency_fee['standard'])) ? $urgency_fee['standard'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[urgency_fee][urgent_same_day]"
                                           value="<?= (isset($urgency_fee['urgent_same_day'])) ? $urgency_fee['urgent_same_day'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[urgency_fee][urgent_1_day]"
                                           value="<?= (isset($urgency_fee['urgent_1_day'])) ? $urgency_fee['urgent_1_day'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[urgency_fee][urgent_2_days]"
                                           value="<?= (isset($urgency_fee['urgent_2_days'])) ? $urgency_fee['urgent_2_days'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>

    <!--Number of properties discount-->
    <section class="master-form card card-outline card-warning mx-4">
        <header class="card-header">
            <h2 class="card-title"><strong>Number Of Properties Discount (On total fee of all properties)(%)(-)</strong>
            </h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1</strong>
                            </td>
                            <td>
                                <strong>2</strong>
                            </td>
                            <td>
                                <strong>3</strong>
                            </td>
                            <td>
                                <strong>4</strong>
                            </td>
                            <td>
                                <strong>5</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_property_discount = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'no_of_property_discount'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            // echo "<pre>";
                            // print_r($no_of_property_dis);
                            // die();
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_property_discount][1]"
                                           value="<?= (isset($no_of_property_discount['1'])) ? $no_of_property_discount['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_property_discount][2]"
                                           value="<?= (isset($no_of_property_discount['2'])) ? $no_of_property_discount['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_property_discount][3]"
                                           value="<?= (isset($no_of_property_discount['3'])) ? $no_of_property_discount['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_property_discount][4]"
                                           value="<?= (isset($no_of_property_discount['4'])) ? $no_of_property_discount['4'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_property_discount][5]"
                                           value="<?= (isset($no_of_property_discount['5'])) ? $no_of_property_discount['5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>
                        </tbody>
                        <thead>
                        <tr>

                            <td>
                                <strong>6 - 10</strong>
                            </td>
                            <td>
                                <strong>11 - 20</strong>
                            </td>
                            <td>
                                <strong>21 - 50</strong>
                            </td>
                            <td>
                                <strong>51 - 100</strong>
                            </td>
                            <td>
                                <strong> >100 </strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_property_discount][6]"
                                           value="<?= (isset($no_of_property_discount['6'])) ? $no_of_property_discount['6'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_property_discount][7]"
                                           value="<?= (isset($no_of_property_discount['7'])) ? $no_of_property_discount['7'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_property_discount][8]"
                                           value="<?= (isset($no_of_property_discount['8'])) ? $no_of_property_discount['8'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_property_discount][9]"
                                           value="<?= (isset($no_of_property_discount['9'])) ? $no_of_property_discount['9'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_property_discount][10]"
                                           value="<?= (isset($no_of_property_discount['10'])) ? $no_of_property_discount['10'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Number of units in the same building-->
    <section class="master-form card card-outline card-warning mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Number Of Units In The Same Building Discount (on total fee of all
                    properties) (%)(-)</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>1 - (One)</strong>
                            </td>
                            <td>
                                <strong>2 - (Two)</strong>
                            </td>
                            <td>
                                <strong>3 - (Three)</strong>
                            </td>
                            <td>
                                <strong>4 - (Four)</strong>
                            </td>
                            <td>
                                <strong>5 - (Five)</strong>
                            </td>
                            <td>
                                <strong> >5 - (More than five)</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $no_of_units_same_building_dis = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'no_of_units_same_building_discount'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');

                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_units_same_building_discount][1]"
                                           value="<?= (isset($no_of_units_same_building_dis['1'])) ? $no_of_units_same_building_dis['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_units_same_building_discount][2]"
                                           value="<?= (isset($no_of_units_same_building_dis['2'])) ? $no_of_units_same_building_dis['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_units_same_building_discount][3]"
                                           value="<?= (isset($no_of_units_same_building_dis['3'])) ? $no_of_units_same_building_dis['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_units_same_building_discount][4]"
                                           value="<?= (isset($no_of_units_same_building_dis['4'])) ? $no_of_units_same_building_dis['4'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_units_same_building_discount][5]"
                                           value="<?= (isset($no_of_units_same_building_dis['5'])) ? $no_of_units_same_building_dis['5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[no_of_units_same_building_discount][6]"
                                           value="<?= (isset($no_of_units_same_building_dis['6'])) ? $no_of_units_same_building_dis['6'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--New / Repeat/ Revalidation Client-->
    <section class="master-form card card-outline card-secondary mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>New / Repeat/ Revalidation Client Discount (per property) (%)(-)</strong>
            </h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>New Client</strong>
                            </td>
                            <td>
                                <strong>Repeat Client</strong>
                            </td>
                            <td>
                                <strong>Bank Revalidation</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $new_repeat_valuation = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'new_repeat_valuation_data'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');
                            ?>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[new_repeat_valuation][new_client]"
                                           value="<?= (isset($new_repeat_valuation['new_client'])) ? $new_repeat_valuation['new_client'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[new_repeat_valuation][repeat_client]"
                                           value="<?= (isset($new_repeat_valuation['repeat_client'])) ? $new_repeat_valuation['repeat_client'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[new_repeat_valuation][bank_revalidation]"
                                           value="<?= (isset($new_repeat_valuation['bank_revalidation'])) ? $new_repeat_valuation['bank_revalidation'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--Advance Payment-->
    <section class="master-form card card-outline card-info mx-4">

        <header class="card-header">
            <h2 class="card-title"><strong>Advance Payment</strong></h2>
        </header>

        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">

                    <table class="table table-striped table-condensed table-hover">

                        <thead>
                        <tr>
                            <td>
                                <strong>100%</strong>
                            </td>
                            <td>
                                <strong>75%</strong>
                            </td>
                            <td>
                                <strong>50%</strong>
                            </td>
                            <td>
                                <strong>25%</strong>
                            </td>
                            <td>
                                <strong>0%</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <?php
                            $payment = ArrayHelper::map(\app\models\QuotationFeeMasterFile::find()
                                ->where(['heading' => 'payment_terms'])->andWhere(['approach_type' => 'profit'])->all(), 'sub_heading', 'values');

                            ?>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[payment_terms][100]"
                                           value="<?= (isset($payment['100'])) ? $payment['100'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[payment_terms][75]"
                                           value="<?= (isset($payment['75'])) ? $payment['75'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[payment_terms][50]"
                                           value="<?= (isset($payment['50'])) ? $payment['50'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[payment_terms][25]"
                                           value="<?= (isset($payment['25'])) ? $payment['25'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input type="number" step=".01"
                                           name="QuotationFeeMasterFile[payment_terms][0]"
                                           value="<?= (isset($payment['0'])) ? $payment['0'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- 8- New / Repeat Valuation -->



    
    <div class="row mx-3">
        <div class="col">
            <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
        </div>
    </div>

<?php  
    $latestUpdatedUser = '';
    $query = \app\models\History::find()->where(['rec_type'=>'masterfile','file_type'=>'profit'])->orderBy(['id' => SORT_DESC])->one();
    if($query<>null){
        $latestUpdatedUser = $query->user->firstname.' '.$query->user->lastname;
    }
?>

    <div class="card-footer bg-light">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mx-2']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                <?php 
            if($latestUpdatedUser<>null){?>
                <span class="badge grid-badge badge-dark mx-2"><mark>Last Updated By <?= $latestUpdatedUser ?></mark></span>
        <?php
            }
        ?>
    </div>
</section>

<?php ActiveForm::end(); ?>
