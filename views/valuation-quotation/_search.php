<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ValuationQuotationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="valuation-quotation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'reference_number') ?>

    <?= $form->field($model, 'client_name') ?>

    <?= $form->field($model, 'client_reference') ?>

    <?= $form->field($model, 'client_customer_name') ?>

    <?php // echo $form->field($model, 'inquiry_date') ?>

    <?php // echo $form->field($model, 'expiry_date') ?>

    <?php // echo $form->field($model, 'purpose_of_valuation') ?>

    <?php // echo $form->field($model, 'no_of_properties') ?>

    <?php // echo $form->field($model, 'recommended_fee_form') ?>

    <?php // echo $form->field($model, 'recommended_fee') ?>

    <?php // echo $form->field($model, 'final_fee_approved') ?>

    <?php // echo $form->field($model, 'turn_around_time') ?>

    <?php // echo $form->field($model, 'valuer_name') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
