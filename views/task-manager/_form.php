<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use yii\helpers\ArrayHelper;


$clientsArr = ArrayHelper::map(\app\models\User::find()
    ->select([
        'id', 'CONCAT(firstname, " ", lastname) AS lastname',
    ])
    ->where(['status' => 1])
    ->andWhere(['user_type' => [10,20]])
    ->orderBy(['lastname' => SORT_ASC,])
    ->all(), 'id', 'lastname');

$clientsArr = ['' => 'select'] + $clientsArr;

$user_id = Yii::$app->user->identity->id;
?>

    <section class="contact-form card card-outline card-primary">
        <?php $form = ActiveForm::begin(); ?>
        <header class="card-header">
            <h2 class="card-title"><?php if(isset($page_title)) {echo $page_title;}else{echo "Create Task";} ?></h2>
        </header>
        <div class="card-body">

            <div class="row">
                <!-- <div class="col-4">
                <label for="modification">Modification Type</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='modification' id="modification" class="form-control">
                            <option value="add"
                                <?php if(isset($model->modification) && ($model->modification == 'add')){ echo 'selected';} ?>>
                                Add</option>
                            <option value="update"
                                <?php if(isset($model->modification) && ($model->modification == 'update')){ echo 'selected';} ?>>
                                Edit</option>
                            <option value="delete"
                                <?php if(isset($model->modification) && ($model->modification == 'delete')){ echo 'selected';} ?>>
                                Delete</option>
                        </select>
                    </div>
                </div>
            </div> -->

                <div class="col-12">
                    <label for="description">Task Description <span style="font-size:9px"> (Max. 40 Characters Only)</span></label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <textarea class="form-control" rows="1" type="text" name="description" id="description" maxlength="40"><?php if(isset($model->description)) echo $model->description; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <br>

            <div class="row">

                <div class="col-4">
                    <label for="department">Department</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name='department' id="department" class="form-control">
                                <option value="select"
                                    <?php if(isset($model->department) && ($model->department == 'select')){ echo 'selected';} ?>>
                                    Select</option>
                                <option value="operations"
                                    <?php if(isset($model->department) && ($model->department == 'operations')){ echo 'selected';} ?>>
                                    Operations</option>
                                <option value="finance"
                                    <?php if(isset($model->department) && ($model->department == 'finance')){ echo 'selected';} ?>>
                                    Finance</option>
                                <option value="hr"
                                    <?php if(isset($model->department) && ($model->department == 'hr')){ echo 'selected';} ?>>
                                    HR</option>
                                <option value="maxima"
                                    <?php if(isset($model->department) && ($model->department == 'maxima')){ echo 'selected';} ?>>
                                    Maxima</option>
                                <option value="administration"
                                    <?php if(isset($model->department) && ($model->department == 'administration')){ echo 'selected';} ?>>
                                    Administration</option>
                                <option value="it"
                                    <?php if(isset($model->department) && ($model->department == 'it')){ echo 'selected';} ?>>
                                    IT</option>
                                <option value="service"
                                    <?php if(isset($model->department) && ($model->department == 'service')){ echo 'selected';} ?>>
                                    New Service/Geography</option>
                                <option value="valuations"
                                    <?php if(isset($model->department) && ($model->department == 'valuations')){ echo 'selected';} ?>>
                                    Valuations</option>
                                <option value="support"
                                    <?php if(isset($model->department) && ($model->department == 'support')){ echo 'selected';} ?>>
                                    Valuations Support</option>
                                <option value="data_management"
                                    <?php if(isset($model->department) && ($model->department == 'data_management')){ echo 'selected';} ?>>
                                    Data Management</option>
                                <option value="sales"
                                    <?php if(isset($model->department) && ($model->department == 'sales')){ echo 'selected';} ?>>
                                    Sales</option>
                                <option value="marketing"
                                    <?php if(isset($model->department) && ($model->department == 'marketing')){ echo 'selected';} ?>>
                                    Marketing</option>
                                <option value="empanelment"
                                    <?php if(isset($model->department) && ($model->department == 'empanelment')){ echo 'selected';} ?>>
                                    Empanelment</option>
                                <option value="business_development"
                                    <?php if(isset($model->department) && ($model->department == 'business_development')){ echo 'selected';} ?>>
                                    Business Development</option>
                                <option value="bcs"
                                    <?php if(isset($model->department) && ($model->department == 'bcs')){ echo 'selected';} ?>>
                                    BCS</option>
                                <option value="pak"
                                    <?php if(isset($model->department) && ($model->department == 'pak')){ echo 'selected';} ?>>
                                    Pakistan(Karachi)</option>
                                <option value="sa"
                                    <?php if(isset($model->department) && ($model->department == 'sa')){ echo 'selected';} ?>>
                                    Saudi Arabia(Riyadh)</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4" id="module_div">
                    <label for="module">Module</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name='module' id="module" class="form-control">
                                <option value="none"
                                    <?php if(isset($model->module) && ($model->module == 'none')){ echo 'selected';} ?>>
                                    Select</option>
                                <option value="valuations"
                                    <?php if(isset($model->module) && ($model->module == 'valuations')){ echo 'selected';} ?>>
                                    Valuations</option>
                                <option value="proposals"
                                    <?php if(isset($model->module) && ($model->module == 'proposals')){ echo 'selected';} ?>>
                                    Proposals</option>
                                <option value="dashboard"
                                    <?php if(isset($model->module) && ($model->module == 'dashboard')){ echo 'selected';} ?>>
                                    Dashboard</option>
                                <option value="bcs"
                                    <?php if(isset($model->module) && ($model->module == 'bcs')){ echo 'selected';} ?>>
                                    BCS</option>
                                <option value="brokerage"
                                    <?php if(isset($model->module) && ($model->module == 'brokerage')){ echo 'selected';} ?>>
                                    Brokerage</option>
                                <option value="task_management"
                                    <?php if(isset($model->module) && ($model->module == 'task_management')){ echo 'selected';} ?>>
                                    Task Management</option>
                                <option value="reports"
                                    <?php if(isset($model->module) && ($model->module == 'reports')){ echo 'selected';} ?>>
                                    Reports</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="frequency">Task Frequency</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name='frequency' id="frequency" class="form-control">
                                <option value="one-time"
                                    <?php if(isset($model->frequency) && ($model->frequency == 'one-time')){ echo 'selected';} ?>>
                                    One Time</option>
                                <option value="daily"
                                    <?php if(isset($model->frequency) && ($model->frequency == 'daily')){ echo 'selected';} ?>>
                                    Daily</option>
                                <option value="weekly"
                                    <?php if(isset($model->frequency) && ($model->frequency == 'weekly')){ echo 'selected';} ?>>
                                    Weekly</option>
                                <option value="monthly"
                                    <?php if(isset($model->frequency) && ($model->frequency == 'monthly')){ echo 'selected';} ?>>
                                    Monthly</option>
                                <option value="quarterly"
                                    <?php if(isset($model->frequency) && ($model->frequency == 'quarterly')){ echo 'selected';} ?>>
                                    Quarterly</option>
                                <option value="yearly"
                                    <?php if(isset($model->frequency) && ($model->frequency == 'yearly')){ echo 'selected';} ?>>
                                    Yearly</option>
                                <option value="bi-annual"
                                    <?php if(isset($model->frequency) && ($model->frequency == 'bi-annual')){ echo 'selected';} ?>>
                                    Bi Annual</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="priority">Priority</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name='priority' id="priority" class="form-control">
                                <option value="high"
                                    <?php if(isset($model->priority) && ($model->priority == 'high')){ echo 'selected';} ?>>
                                    High</option>
                                <option value="medium"
                                    <?php if(isset($model->priority) && ($model->priority == 'medium')){ echo 'selected';} ?>>
                                    Medium</option>
                                <option value="low"
                                    <?php if(isset($model->priority) && ($model->priority == 'low')){ echo 'selected';} ?>>
                                    Low</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <br>

            <div class="row">
                <div class="col-4">
                    <label for="start_date">Task Start Date</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input class="form-control" type="datetime-local" id="start_date" name="start_date" value="<?php $dateTime = new DateTime($model->start_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->start_date)) {echo $formattedDateTime;} ?>">
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="end_date">End Date</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input class="form-control" type="datetime-local" id="end_date" name="end_date" value="<?php $dateTime = new DateTime($model->end_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->end_date)) {echo $formattedDateTime;} ?>">
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="meeting_date">Approval/Update Meeting Date</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input class="form-control" type="datetime-local" id="meeting_date" name="meeting_date" value="<?php $dateTime = new DateTime($model->meeting_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->meeting_date)) {echo $formattedDateTime;} ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-4">
                    <label for="frequency_start">Frequency Start Date</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input class="form-control" type="datetime-local" id="frequency_start" name="frequency_start" value="<?php $dateTime = new DateTime($model->frequency_start); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->frequency_start)) {echo $formattedDateTime;} ?>">
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="frequency_end">Frequency End Date</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input class="form-control" type="datetime-local" id="frequency_end" name="frequency_end" value="<?php $dateTime = new DateTime($model->frequency_end); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->frequency_end)) {echo $formattedDateTime;} ?>">
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="expiry_date">Expiry Date</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input class="form-control" type="datetime-local" id="expiry_date" name="expiry_date" value="<?php $dateTime = new DateTime($model->expiry_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->expiry_date)) {echo $formattedDateTime;} ?>">
                        </div>
                    </div>
                </div>
            </div>

            <br>

            <div class="row">
                <div class="col-4">
                    <label for="responsibility">Responsibility</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name="responsibility" id="responsibility" class="form-control">
                                <?php
                                foreach($clientsArr as $key => $users){

                                    if($model->responsibility == $key)
                                    {
                                        echo "<option value='$model->responsibility' selected>$users</option>";
                                    }else{
                                        echo "<option value='$key'> $users</option>";
                                    }

                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="assigned_to">Assigned To</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name="assigned_to" id="assigned_to" class="form-control">
                                <?php
                                foreach($clientsArr as $key => $users){

                                    if($model->assigned_to == $key)
                                    {
                                        echo "<option value='$model->assigned_to' selected>$users</option>";
                                    }else{
                                        echo "<option value='$key'> $users</option>";
                                    }

                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="entered_by">Entered By</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name="entered_by" class="form-control">
                                <?php
                                foreach($clientsArr as $key => $users){
                                    if($model->entered_by == $key)
                                    {
                                        echo "<option value='$model->entered_by' selected>$clientsArr[$key]</option>";
                                    }else{
                                        if($key == 86 || $key == 53 || $key == 110465 || $key == 21 || $key == 111558 || $key == 111811 || $key == 111811)
                                        {
                                            echo "<option value='$key'>$users</option>";
                                       }
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-4">
                    <label for="agreed_by">Agreed By</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name="agreed_by" class="form-control">
                                <?php
                                foreach($clientsArr as $key => $users){

                                    if($model->agreed_by == $key)
                                    {
                                        echo "<option value='$model->agreed_by' selected>$users</option>";
                                    }else{
                                        echo "<option value='$key'> $users</option>";
                                    }

                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4" style="<?php echo $user_id==1 || $user_id==14 || $user_id==6319 ? 'display: block;' : 'display: none;'; ?>">
                    <label for="verified_by">Verified By</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name="verified_by" class="form-control">
                                <?php
                                foreach($clientsArr as $key => $users){
                                    if($model->verified_by == $key)
                                    {
                                        echo "<option value='$model->verified_by' selected>$clientsArr[$key]</option>";
                                    }else{
                                        if($key == 14)
                                        {
                                            echo "<option value='$key'>$users</option>";
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4" style="<?php echo $user_id==1 || $user_id==14 ? 'display: block;' : 'display: none;'; ?>">
                    <label for="approved_by">Approved By</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name="approved_by" class="form-control">
                                <?php
                                foreach($clientsArr as $key => $users){
                                    if($model->approved_by == $key)
                                    {
                                        echo "<option value='$model->approved_by' selected>$clientsArr[$key]</option>";
                                    }else{
                                        if($key == 14)
                                        {
                                            echo "<option value='$key'>$users</option>";
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <br>

            <div class="row">
                <div class="col-4">
                    <label for="status">Status</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name='status' id="status" class="form-control">
                                <option value="pending"
                                    <?php if(isset($model->status) && ($model->status == 'pending')){ echo 'selected';} ?>>
                                    Pending</option>
                                <option value="completed"
                                    <?php if(isset($model->status) && ($model->status == 'completed')){ echo 'selected';} ?>>
                                    Completed</option>
                                <option value="cancelled"
                                    <?php if(isset($model->status) && ($model->status == 'cancelled')){ echo 'selected';} ?>>
                                    Cancelled</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="col-4">
                <label for="type">Type of Task</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='type' id="type" class="form-control">
                            <option value="project"
                                <?php if(isset($model->type) && ($model->type == 'project')){ echo 'selected';} ?>>
                                Project</option>
                            <option value="kpi"
                                <?php if(isset($model->type) && ($model->type == 'kpi')){ echo 'selected';} ?>>
                                KPI</option>
                        </select>
                    </div>
                </div>
            </div> -->

        </div>
        <div class="card-footer">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
            <?php
            if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => get_class($model),
                ]);
            }
            ?>
        </div>
        <?php ActiveForm::end(); ?>
    </section>

<?php
$this->registerJs('

var value = $("#department").val();
if(value == "maxima")
{
  $("#module_div").css("display", "block");
}else{
    $("#module_div").css("display", "none");
}

$("#department").change(function() {
    var val = $(this).val(); 
      if(val == "maxima")
      {
        $("#module_div").css("display", "block");
      }else{
        $("#module_div").val("");
        $("#module_div").css("display", "none");
      }
});

var dropdown1 = document.getElementById("responsibility");
var dropdown2 = document.getElementById("assigned_to");

dropdown1.addEventListener("change", function () {
    dropdown2.value = dropdown1.value;
});


var freq_val = $("#frequency").val();
if(freq_val == "one-time")
{
    document.getElementById("frequency_start").disabled=true;
    document.getElementById("frequency_end").disabled=true;
}else{
    document.getElementById("frequency_start").disabled=false;
    document.getElementById("frequency_end").disabled=false;
}

$("#frequency").change(function() {
    var val = $(this).val(); 
      if(val == "one-time")
      {
        document.getElementById("frequency_start").disabled=true;
        document.getElementById("frequency_end").disabled=true;
      }else{
        document.getElementById("frequency_start").disabled=false;
        document.getElementById("frequency_end").disabled=false;
      }
});
    
');
