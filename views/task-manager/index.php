<?php

use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\User;

use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GeneralAsumptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Task Manager";
$this->params['breadcrumbs'][] = $this->title;

$cardTitle = Yii::t('app', 'Tasks List');

// $createBtn=true;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    $createBtn=true;
}
// if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
//     $actionBtns.='{view}';
// }
// $actionBtns.='{update}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
// $actionBtns.='{delete}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
    $actionBtns.='{delete}';
}
// $actionBtns.='{get-history}';


?>

<style>
    .select2{
        width : 50px !important;
    }
</style>

<div class="task-index">

    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        // 'createBtn' => $createBtn,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:0px;']],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:0%'],
                'attribute' => 'priority',
                'label' => Yii::t('app', 'Lead'),
                'value' => function ($model) {
                    return ucfirst($model->priority);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'priority',
                    'data' => ['low' => 'Low', 
                    'medium' => 'Medium',
                    'high' => 'High',
                    ],
                    'options' => ['placeholder' => 'Select'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
            ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:1%'],
            //     'attribute' => 'modification',
            //     'label' => Yii::t('app', 'Change'),
            //     'value' => function ($model) {
            //         return ucfirst($model->modification);
            //     },
            // ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:22%'],
                'attribute' => 'description',
                'label' => Yii::t('app', 'Task'),
                'value' => function ($model) {
                    return ucfirst($model->description);
                },
            ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:1%'],
            //     'attribute' => 'module',
            //     'label' => Yii::t('app', 'Module'),
            //     'value' => function ($model) {

            //         if($model->module == "task_management")
            //         {
            //             return"Task Management";
            //         }elseif ( $model->module == "none" ){
            //             return " ";
            //         }else{
            //             return ucfirst($model->module);
            //         }

            //     },
            // ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:1%'],
                'attribute' => 'department',
                'label' => Yii::t('app', 'Department'),
                'value' => function ($model) {
                    if ($model->department == "business_development") {
                        return "Business Development";
                    } elseif ($model->department == "data_management") {
                        return "Data Management";
                    } elseif ($model->department == "it") {
                        return "IT";
                    } elseif ($model->department == "hr") {
                        return "HR";
                    } elseif ($model->department == "pak") {
                        return "Pakistan(Karachi)";
                    } elseif ($model->department == "sa") {
                        return "Saudi Arabia(Riyadh)";
                    } else {
                        return ucfirst($model->department);
                    }
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'department',
                    'data' => ['business_development' => 'Business Development', 
                    'data_management' => 'Data Management', 
                    'it' => 'IT', 
                    'operations' => 'Operations', 
                    'finance' => 'Finance', 
                    'maxima' => 'Maxima', 
                    'administration' => 'Administration', 
                    'service' => 'New Service/Geography', 
                    'valuations' => 'Valuations', 
                    'support' => 'Valuations Support', 
                    'sales' => 'Sales', 
                    'marketing' => 'Marketing', 
                    'empanelment' => 'Empanelment', 
                    'bcs' => 'BCS', 
                    'pak' => 'Pakistan(Karachi)',
                    'sa' => 'Saudi Arabia(Riyadh)',
                    'hr' => 'HR'],
                    'options' => ['placeholder' => 'Select...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
            ],
            

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:1%'],
            //     'attribute' => 'frequency',
            //     'label' => Yii::t('app', 'Frequency'),
            //     'value' => function ($model) {
            //         return ucfirst($model->frequency);
            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:1%'],
            //     'attribute' => 'entered_by',
            //     'label' => Yii::t('app', 'Entered By'),
            //     'value' => function ($model) {
            //         $get_name = User::find()->where(['id' => $model->entered_by])->one();
            //         return $get_name->firstname.'<br>'.$get_name->lastname;
            //     },
            // ],
            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:1%'],
                'attribute' => 'entered_by',
                'label' => 'Entered By', 
                'value' => function ($model) {
                    $get_name = User::find()->where(['id' => $model->entered_by])->one();
                    return $get_name->firstname.' '.$get_name->lastname;

                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'entered_by',
                    'data' => ArrayHelper::map(\app\models\User::find()
                                ->where([ 'IN', 'user_type' , [10,20]])
                                ->orderBy(['firstname' => SORT_ASC,])
                                ->all(), 'id',
                                function ($user) {
                                    return $user->firstname . ' ' . $user->lastname;
                                }
                            ),
                    'options' => ['placeholder' => 'Select...'],

                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'created_at',
                'label' => Yii::t('app', 'Entered Date'),
                'value' => function ($model) {
                    if(isset($model->created_at)) {
                        return date("d-M-Y", strtotime($model->created_at)) . "<br>" . date('h:i A', strtotime($model->created_at));
                    } else {
                        return "";
                    }
                },
                'filter' => '<input style="width:80px" type="date" class="form-control" name="TaskManagerSearch[created_at]" value="' . Yii::$app->request->get('TaskManagerSearch')['created_at'] . '">',
            ],
            

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:13%'],
                'attribute' => 'expiry_date',
                'label' => Yii::t('app', 'Expiry Date'),
                'value' => function ($model) {
                    if(isset($model->expiry_date))
                    {
                        return  date("d-M-Y", strtotime($model->expiry_date)) . "<br>" . date('h:i A', strtotime($model->expiry_date));
                    }else{
                        return "";
                    }

                },
                'filter' => '<input style="width:80px" type="date" class="form-control" name="TaskManagerSearch[expiry_date]" value="' . Yii::$app->request->get('TaskManagerSearch')['expiry_date'] . '">',
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:13%'],
                'attribute' => 'start_date',
                'label' => Yii::t('app', 'Start Date'),
                'value' => function ($model) {
                    return  date("d-M-Y", strtotime($model->start_date)) . "<br>" . date('h:i A', strtotime($model->start_date));
                },
                'filter' => '<input style="width:80px" type="date" class="form-control" name="TaskManagerSearch[start_date]" value="' . Yii::$app->request->get('TaskManagerSearch')['start_date'] . '">',
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:15%'],
                'attribute' => 'end_date',
                'label' => Yii::t('app', 'End Date'),
                'value' => function ($model) {
                    return  date("d-M-Y", strtotime($model->end_date)) . "<br>" . date('h:i A', strtotime($model->end_date));
                },
                'filter' => '<input style="width:80px" type="date" class="form-control" name="TaskManagerSearch[end_date]" value="' . Yii::$app->request->get('TaskManagerSearch')['end_date'] . '">',
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:18% ; color:#017BFE;'],
                'attribute' => 'days_hours',
                'label' => Yii::t('app', 'Days - Hours'),
                'value' => function ($model) {
                    $startDate = $model->start_date;
                    $endDate = $model->end_date;

                    $startDateTime = new DateTime($startDate); // Start date and time
                    $endDateTime = new DateTime($endDate);

                    $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                    return number_format($workingHours_1/9,2).'</b>'.'<br>'.$workingHours_1.' Hour';
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'meeting_date',
                'label' => Yii::t('app', 'Meeting Date'),
                'value' => function ($model) {
                    return  date("d-M-Y", strtotime($model->meeting_date)) . "<br>" . date('h:i A', strtotime($model->meeting_date));
                },
                'filter' => '<input style="width:80px" type="date" class="form-control" name="TaskManagerSearch[meeting_date]" value="' . Yii::$app->request->get('TaskManagerSearch')['meeting_date'] . '">',
            ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:1%'],
            //     'attribute' => 'responsibility',
            //     'label' => Yii::t('app', 'Assigned To'),
            //     'value' => function ($model) {
            //         $get_name = User::find()->where(['id' => $model->assigned_to])->one();
            //         return $get_name->firstname.'<br>'.$get_name->lastname;
            //     },
            // ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:1%'],
                'attribute' => 'responsibility',
                'label' => Yii::t('app', 'Assigned To'),
                'value' => function ($model) {
                    $get_name = User::find()->where(['id' => $model->assigned_to])->one();
                    return $get_name->firstname.'<br>'.$get_name->lastname;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'responsibility',
                    'data' => ArrayHelper::map(\app\models\User::find()
                                ->where([ 'IN', 'user_type' , [10,20]])
                                ->orderBy(['firstname' => SORT_ASC,])
                                ->all(), 'id',
                                function ($user) {
                                    return $user->firstname . ' ' . $user->lastname;
                                }
                            ),
                    'options' => ['placeholder' => 'Select...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                // 'contentOptions' => ['style' => 'width: 250px;'],
    
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:1% ; color:#017BFE;'],
                'attribute' => 'progress',
                'label' => Yii::t('app', 'Status'),
                'value' => function ($model) {
                    if( $model->entered_by != null && $model->verified_by == null && $model->approved_by == null)
                    {
                        return "Entered";
                    }elseif( $model->entered_by != null && $model->verified_by != null && $model->approved_by == null)
                    {
                        return "Verified";
                    }elseif( $model->entered_by != null && $model->verified_by != null && $model->approved_by != null)
                    {
                        return "Approved";
                    }else{
                        return "";
                    }
                },
            ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:7%'],
            //     'attribute' => 'completed_date',
            //     'label' => Yii::t('app', 'Completion Date'),
            //     'value' => function ($model) {
            //         if(isset($model->completed_date))
            //         {
            //             return date("d-M-Y", strtotime($model->completed_date)) . "<br>" . date('h:i A', strtotime($model->completed_date));
            //         }else{
            //             return "";
            //         }
            //     },
            // ],



            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:4%'],
                'attribute' => 'status',
                'label' => Yii::t('app', 'Progress'),
                'value' => function ($model) {
                    if($model->status == "overdue")
                    {
                        return "<span style=\"color:red\">".ucfirst($model->status)."</span>";
                    }elseif($model->status == "pending"){
                        return "<span style=\"color:#017BFE\">".ucfirst($model->status)."</span>";
                    }elseif($model->status == "completed"){
                        return "<span style=\"color:#EC9706\">".ucfirst($model->status)."</span>";
                    }elseif($model->status == "cancelled"){
                        return "<span style=\"color:grey\">".ucfirst($model->status)."</span>";
                    }elseif($model->status == "completed_ahead"){
                        return "<span style=\"color:green\">".'Completed'."</span>";
                    }
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => ['pending' => 'Pending', 
                    'completed' => 'Completed',
                    'overdue' => 'Overdue',
                    'completed_ahead' => 'Completed Ahead',
                    ],
                    'options' => ['placeholder' => 'Select'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:4% ; color:#017BFE;'],
                'attribute' => 'tat',
                'label' => Yii::t('app', 'Perfor mance'),
                'value' => function ($model) {

                    // if(isset($model->completed_date))
                    // {
                    //     $endDateTime = new \DateTime($model->end_date);
                    //     $formatEndDate = $endDateTime->format('Y-m-d');

                    //     $completeDateTime = new \DateTime($model->completed_date);
                    //     $formatCompleteDate = $completeDateTime->format('Y-m-d');
    
                    //     $startDateTime = new DateTime($formatEndDate); // Start date and time
                    //     $endDateTime = new DateTime($formatCompleteDate);

                    //     $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                    //     return number_format($workingHours_1/8.5,2).'</b>'.' Days';
                    // }else{
                    //     $currentDate = new \DateTime(date("Y/m/d H:i:s"));
                    //     $formattedDateTimeToday = strtotime($currentDate->format('Y-m-d'));

                    //     $endDate = new \DateTime($model->end_date);
                    //     $formattedEndDate = strtotime($endDate->format('Y-m-d'));

                    //     if($formattedEndDate <= $formattedDateTimeToday)
                    //     {
                    //         $diff = $formattedDateTimeToday - $formattedEndDate;
                    //         $days = floor($diff / (60 * 60 * 24));
                    //         $days = '+ '.$days;
                    //     }elseif($formattedEndDate >= $formattedDateTimeToday)
                    //     {
                    //         $diff = $formattedEndDate - $formattedDateTimeToday;
                    //         $days = floor($diff / (60 * 60 * 24));
                    //         $days = '- '.$days;
                    //     }
                    //     return $days.' Days';
                    // }
                    
                    if($model->status != "pending")
                    {
                        if( $model->completed_date <> '' || $model->completed_date <> null )
                        {
                            $currentDate = new \DateTime($model->completed_date);
                            $startDate = new \DateTime($model->start_date);
                            $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDate, $currentDate);
                            
                            return number_format($workingHours_1/9,2).'</b>'.' Days';
                        }else{
                            $currentDate = new \DateTime(date("Y/m/d H:i:s"));
                            $startDate = new \DateTime($model->start_date);
                            $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDate, $currentDate);
                            
                            return number_format($workingHours_1/9,2).'</b>'.' Days';
                        }

                    }else{
                        $currentDate = new \DateTime(date("Y/m/d H:i:s"));
                        $formattedDateTimeToday = strtotime($currentDate->format('Y-m-d'));

                        $endDate = new \DateTime($model->end_date);
                        $formattedEndDate = strtotime($endDate->format('Y-m-d'));

                        if($formattedEndDate <= $formattedDateTimeToday)
                        {
                            $diff = $formattedDateTimeToday - $formattedEndDate;
                            $days = floor($diff / (60 * 60 * 24));
                            $days = '+ '.$days;
                        }elseif($formattedEndDate >= $formattedDateTimeToday)
                        {
                            $diff = $formattedEndDate - $formattedDateTimeToday;
                            $days = floor($diff / (60 * 60 * 24));
                            $days = '- '.$days;
                        }
                        return $days.' Days';
                    }
                },
            ],



            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:5%'],
            //     'attribute' => 'department',
            //     'label' => Yii::t('app', 'Department'),
            //     'value' => function ($model) {
            //         if($model->department == "it")
            //         {
            //             return "IT";
            //         }elseif($model->department == "hr")
            //         {
            //             return "HR";
            //         }elseif($model->department == "service")
            //         {
            //             return "New Service/Geography";
            //         }else{
            //             return ucfirst($model->department);
            //         }

            //     },
            // ],


            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:10%'],
            //     'attribute' => 'target_date',
            //     'label' => Yii::t('app', 'Target Date'),
            //     'value' => function ($model) {
            //         return  date("d-M-Y", strtotime($model->target_date)) . "<br>" . date('h:i A', strtotime($model->target_date));
            //     },
            // ],


            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:5%'],
            //     'attribute' => 'verified_by',
            //     'label' => Yii::t('app', 'Verified By'),
            //     'value' => function ($model) {
            //         $get_name = User::find()->where(['id' => $model->verified_by])->one();
            //         return $get_name->lastname;
            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:5%'],
            //     'attribute' => 'approved_by',
            //     'label' => Yii::t('app', 'Approved By'),
            //     'value' => function ($model) {
            //         $get_name = User::find()->where(['id' => $model->approved_by])->one();
            //         return $get_name->lastname;
            //     },
            // ],




            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:0px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
                    <div class="dropdown-menu" role="menu">
              '.$actionBtns.'
                    </div>
                </div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },

                    'get-history' => function ($url, $model) {
                        // Yii::$app->session->set('model_name', Yii::$app->appHelperFunctions->getModelName());
                        return Html::a('<i class="fa fa-history"></i> '.Yii::t('app', 'History'), $url, [
                            'title' => Yii::t('app', 'History'),
                            'class'=>'dropdown-item text-1',
                            'target' => '_blank',
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>

</div>

