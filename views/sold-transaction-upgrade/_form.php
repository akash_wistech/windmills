<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\SoldFormAsset;

SoldFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\ListingsTransactions */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('

$("#listingstransactions-transaction_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');

?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
</style>

<section class="listings-transactions-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle ?></h2>
    </header>
    <div class="card-body">

        <div class="row">

            <div class="col-sm-4">
                <?= $form->field($model, 'transaction_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-transaction_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-transaction_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>



            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'type')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->soldTypesListArr,
                    'options' => ['placeholder' => 'Select a Type ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true]) ?>
            </div>

        </div>



        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Building Information') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'building_info')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select a Building ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>


                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                </div>




                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_placement')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertyPlacementListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_visibility')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertyVisibilityListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_exposure')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertyExposureListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_condition')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertyConditionListArrMaster,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'development_type')->widget(Select2::classname(), [
                            'data' => array('Standard' => 'Standard', 'Non-Standard' => 'Non-Standard'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'utilities_connected')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>

                </div>



                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'finished_status')->widget(Select2::classname(), [
                            'data' => array('Shell & Core' => 'Shell & Core', 'Fitted' => 'Fitted'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'pool')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'gym')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'play_area')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php

                        $model->other_facilities = explode(',', ($model->other_facilities <> null) ? $model->other_facilities : "");
                        echo $form->field($model, 'other_facilities')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\OtherFacilities::find()->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select a Facilities ...'],
                            'pluginOptions' => [
                                'placeholder' => 'Select a Facilities',
                                'multiple' => true,
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'completion_status')->textInput(['maxlength' => true])?>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'white_goods')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'furnished')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No', 'Semi-Furnished' => 'Semi-Furnished'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'landscaping')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No', 'Semi-Landscape' => 'Semi-Landscape'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationConditionsListArr,
                            'options' => ['placeholder' => 'Select a Location ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>



                </div>




        </section>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Property Other Information') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'no_of_bedrooms')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'upgrades')->widget(Select2::classname(), [
                            'data' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'full_building_floors')->textInput(['maxlength' => true]) ?>
                    </div>


                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <?= $form->field($model, 'parking_space')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'listing_property_type')->textInput(['maxlength' => true]) ?>
                    </div>


                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <?= $form->field($model, 'floor_number')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'number_of_levels')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->listingLevelsListArr,
                            'options' => ['placeholder' => 'Select a Level ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'land_size')->textInput(['maxlength' => true]) ?>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'balcony_size')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'market_value')->textInput(['maxlength' => true]) ?>
                    </div>
                    <!--<div class="col-sm-4">
                        <?/*= $form->field($model, 'land_size')->textInput(['maxlength' => true]) */?>
                    </div>-->
                </div>

            </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Price Information') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'listings_price')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'listings_rent')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'price_per_sqt')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'final_price')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'developer_margin')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>


                </div>

            </div>

        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Agent Information') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'agent_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'agent_phone_no')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'agent_company')->textInput(['maxlength' => true]) ?>
                    </div>


                </div>

            </div>
        </section>


    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>

