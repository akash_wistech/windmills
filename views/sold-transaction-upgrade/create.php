<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SoldTransactions */


$this->title = Yii::t('app', 'Sold Transactions Upgrade');
$cardTitle = Yii::t('app', 'New Listings Transaction Upgrade');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle
?>
<div class="sold-transactions-upgrade-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
