<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\ListingsTransactions */

$this->title = Yii::t('app', 'Sold Transactions');
$cardTitle = Yii::t('app', 'View Sold Transaction:  {nameAttribute}', [
    'nameAttribute' => $model->transaction_date,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

$otherCommunityFacilitiesListArr=\yii\helpers\ArrayHelper::map(\app\models\OtherFacilities::find()->orderBy([
    'title' => SORT_ASC,
])->all(), 'id', 'title');

$facilities = '';
if ($model->other_facilities <> null) {
    $facility_array = explode(',', ($model->other_facilities));
    if (!empty($facility_array)) {
        foreach ($facility_array as $item) {
            $facilities .= $otherCommunityFacilitiesListArr[$item] . ', ';
        }
    }
}


?>


<div class="developments-view">
    <section class="card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title"><?= $model->transaction_date ?></h2>
            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) { ?>
                <div class="card-tools">
                    <a href="<?= Url::to(['update', 'id' => $model['id']]) ?>" class="btn btn-tool">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            <?php } ?>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <?= '<strong>' . Yii::t('app', 'Transaction Date:') . '</strong> ' .(($model->transaction_date <> null) ? date('m-d-Y', strtotime($model->transaction_date)) : ''); ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= '<strong>' . Yii::t('app', 'Type:') . '</strong> ' . (isset(Yii::$app->appHelperFunctions->soldTypesListArr[$model->type]) ? Yii::$app->appHelperFunctions->soldTypesListArr[$model->type] : '-'); ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= '<strong>' . Yii::t('app', 'Unit Number:') . '</strong> ' . $model->unit_number; ?>
                                </div>


                            </div>


                            <section class="card card-outline card-info mt-5">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Property Information') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Category:') . '</strong> ' . (isset(Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category]) ? Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category] : '-'); ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Building Info:') . '</strong> ' . ($model->building!=null ? $model->building->title : '-'); ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Tenure:') . '</strong> ' . $model->tenure; ?>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Floor Number:') . '</strong> ' . $model->floor_number; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'No. of levels:') . '</strong> ' . (isset(Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->number_of_levels]) ? Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->number_of_levels] : '-'); ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Utilities Connected:') . '</strong> ' . $model->utilities_connected; ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Land Size:') . '</strong> ' . $model->land_size; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Built Up Area:') . '</strong> ' . $model->built_up_area; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Balcony Size:') . '</strong> ' . $model->balcony_size; ?>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Placement:') . '</strong> ' . (isset(Yii::$app->appHelperFunctions->propertyPlacementListArr[$model->property_placement]) ? Yii::$app->appHelperFunctions->propertyPlacementListArr[$model->property_placement] : ''); ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Visibility:') . '</strong> ' . (isset(Yii::$app->appHelperFunctions->propertyVisibilityListArr[$model->property_visibility]) ? Yii::$app->appHelperFunctions->propertyVisibilityListArr[$model->property_visibility] : ''); ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Exposure:') . '</strong> ' . (isset(Yii::$app->appHelperFunctions->propertyExposureListArr[$model->property_exposure]) ? Yii::$app->appHelperFunctions->propertyExposureListArr[$model->property_exposure] : ''); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Type:') . '</strong> ' . (isset(Yii::$app->appHelperFunctions->listingsPropertyTypeListArr[$model->listing_property_type]) ? Yii::$app->appHelperFunctions->listingsPropertyTypeListArr[$model->listing_property_type] : ''); ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Condition:') . '</strong> ' . (isset(Yii::$app->appHelperFunctions->propertyConditionListArrMaster[$model->property_condition]) ? Yii::$app->appHelperFunctions->propertyConditionListArrMaster[$model->property_condition] : ''); ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Development Type:') . '</strong> ' . $model->development_type; ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Finished Status:') . '</strong> ' . $model->finished_status; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Pool:') . '</strong> ' . $model->pool; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Gym:') . '</strong> ' . $model->gym; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Play Area:') . '</strong> ' . $model->play_area; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Other Facilities:') . '</strong> ' . $facilities; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Completion Status:') . '</strong> ' . $model->completion_status; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'No. Of Bedrooms:') . '</strong> ' . $model->no_of_bedrooms; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Upgrades:') . '</strong> ' . $model->upgrades; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Full Building Floors:') . '</strong> ' . $model->full_building_floors; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Parking Space:') . '</strong> ' . $model->parking_space; ?>
                                        </div>
                                       <!-- <div class="col-sm-4">
                                            <?/*= '<strong>' . Yii::t('app', 'View:') . '</strong> ' . (isset(Yii::$app->appHelperFunctions->otherPropertyViewListArr[$model->view]) ? Yii::$app->appHelperFunctions->otherPropertyViewListArr[$model->view] : '-'); */?>
                                        </div>-->
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Landscaping:') . '</strong> ' . $model->landscaping; ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'White Goods:') . '</strong> ' . $model->white_goods; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Furnished:') . '</strong> ' . $model->furnished; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Location:') . '</strong> ' . (isset(Yii::$app->appHelperFunctions->locationConditionsListArr[$model->location]) ? Yii::$app->appHelperFunctions->locationConditionsListArr[$model->location] : '-'); ?>
                                        </div>

                                    </div>
                                </div>
                            </section>

                            <section class="card card-outline card-warning">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Price Information') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Price:') . '</strong> ' . $model->listings_price; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Rent:') . '</strong> ' . $model->listings_rent; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Price Per Sqt:') . '</strong> ' . $model->price_per_sqt; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Final Price:') . '</strong> ' . $model->final_price; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Developer Margin:') . '</strong> ' . $model->developer_margin; ?>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="card card-outline card-success">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Agent Information') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Agent Name:') . '</strong> ' . $model->agent_name; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Agent Phone No:') . '</strong> ' . $model->agent_phone_no; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Agent Company:') . '</strong> ' . $model->agent_company; ?>
                                        </div>
                                    </div>
                                </div>
                            </section>

                        </div>
                </div>
    </section>
</div>
