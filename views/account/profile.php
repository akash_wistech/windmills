<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\ProfileFormAsset;
ProfileFormAsset::register($this);

/* @var $this yii\web\View */

$this->title = Yii::t('app','Update Profile');
$this->params['breadcrumbs'][] = $this->title;

$profileImage=Yii::$app->fileHelperFunctions->getImagePath('user',Yii::$app->user->identity->image,'small');
$fullProfileImage=Yii::$app->fileHelperFunctions->getImagePath('user',Yii::$app->user->identity->image,'full');

$this->registerJs('
$(document).on("click", "[data-toggle=\"lightbox\"]", function(event) {
  event.preventDefault();
  $(this).ekkoLightbox({
    alwaysShowClose: true
  });
});
');
?>
<div class="update-profile">
  <section class="update-profile-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['id' => 'update-profile-form']); ?>
    <div class="hidden">
      <?= $form->field($model, 'job_title_id')->textInput()?>
      <?= $form->field($model, 'department_id')->textInput()?>
    </div>
    <header class="card-header">
      <h2 class="card-title"><?= $this->title?></h2>
    </header>
    <div class="card-body">
      <?= $form->field($model, 'image',[
		    'template' => '
			    {label}
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text image-area">
                <a href="'.$fullProfileImage.'" data-toggle="lightbox"><img src="'.$profileImage.'" width="32" height="32" /></a>
              </span>
            </div>
            <div class="custom-file">
              {input}
              <label class="custom-file-label" for="customFile">'.Yii::t('app','Upload Profile Photo').'</label>
            </div>
          </div>
		    {error}'
		    ])->fileInput(['id'=>'customFile', 'class'=>'custom-file-input', 'data-img'=>'mcsc-photo', 'accept' => 'image/*']);?>
      <?php if(Yii::$app->user->identity->user_type==0){?>
        <?= $this->render('contact_profile',['model'=>$model,'form'=>$form])?>
      <?php }else{?>
        <?= $this->render('staff_profile',['model'=>$model,'form'=>$form])?>
      <?php }?>

    </div>
    <div class="card-footer">
      <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
      <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
      <?= Html::a('Logout', ['site/logout'], ['data' => ['method' => 'post']]) ?>
    </div>
    <?php ActiveForm::end(); ?>
  </section>
</div>
