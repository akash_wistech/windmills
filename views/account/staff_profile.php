<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;



$this->registerJs('
$("#profileform-job_title").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#profileform-job_title_id").val()!=suggestion.data){
      $("#profileform-job_title_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#profileform-job_title_id").val("0");
  }
});
$("#profileform-department_name").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/departments']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#profileform-department_id").val()!=suggestion.data){
      $("#profileform-department_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#profileform-department_id").val("0");
  }
});
');
?>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])?>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'job_title')->textInput(['maxlength' => true, 'autocomplete' => 'off'])?>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'department_name')->textInput(['maxlength' => true, 'autocomplete' => 'off'])?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])?>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'phone1')->textInput(['maxlength' => true])?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'phone2')->textInput(['maxlength' => true])?>
  </div>
</div>
<?= $form->field($model, 'address')->textArea(['rows' => 4])?>
