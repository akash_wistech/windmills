<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ListingSubTypes */

$this->title = Yii::t('app', 'Create Listing Sub Types');
$cardTitle = Yii::t('app','New Listing Sub Type');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="developers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
