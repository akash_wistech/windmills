<?php

/* @var $this yii\web\View */
/* @var $model app\models\Lead */

$this->title = Yii::t('app', 'Opportunities');
$cardTitle = Yii::t('app','View Opportunity:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');
?>
<?= $this->render('_view_details',['model'=>$model])?>
