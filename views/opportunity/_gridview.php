<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\LeadListAsset;
LeadListAsset::register($this);

$availableWorkFlowList=Yii::$app->appHelperFunctions->getWorkflowList($searchModel->moduleTypeId);
if($availableWorkFlowList!=null){
  $this->params['availableWorkFlows'][]=['r'=>['index'],'title'=>Yii::t('app','List'),'icon'=>'fa-table','active'=>($searchModel->wf==null ? true : false)];
  foreach($availableWorkFlowList as $workFlow){
    $this->params['availableWorkFlows'][]=['r'=>['index','OpportunitySearch[wf]'=>$workFlow['id']],'title'=>$workFlow['title'],'icon'=>'fa-sitemap','active'=>($searchModel->wf!=null && $searchModel->wf==$workFlow['id'] ? true : false)];
  }
}

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
  $actionBtns.='{view}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
$servicesListId = Yii::$app->appHelperFunctions->getSetting('service_list_id');

$this->registerJs('
$(document).on("pjax:success", function() {
  initScripts();
});
');

$columns[]=['attribute'=>'title'];
$columns[]=['attribute'=>'service_type','value'=>function($model){
  $pdItem = Yii::$app->appHelperFunctions->getPredefinedOptionsItem($model['service_type']);
  if($pdItem!=null){
    return $pdItem['title'];
  }
  return '';
},'filter'=>Yii::$app->appHelperFunctions->getPredefinedListOptionsArr($servicesListId)];
$columns[]='expected_close_date:date';
$columns[]='quote_amount';

$gridViewColumns=Yii::$app->inputHelperFunctions->getGridViewColumns($searchModel->moduleTypeId);
if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $colLabel=($gridViewColumn['short_name']!=null && $gridViewColumn['short_name']!='' ? $gridViewColumn['short_name'] : $gridViewColumn['title']);
    $columns[]=['format'=>'html','label'=>$colLabel,'value'=>function($model) use ($gridViewColumn){
      return Yii::$app->inputHelperFunctions->getGridValue('prospect',$model,$gridViewColumn);
    },'filter'=>Yii::$app->inputHelperFunctions->getGridViewFilters($gridViewColumn)];
  }
}
$columns[]=['attribute'=>'created_at','label'=>Yii::t('app', 'Created'),'format'=>'datetime','filterInputOptions'=>['class'=>'form-control dtrpicker','autocomplete'=>'off']];
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>Yii::t('app','Action'),
  'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
  'contentOptions'=>['class'=>'noprint actions'],
  'template' => '
    <div class="btn-group flex-wrap">
      <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
      </button>
      <div class="dropdown-menu" role="menu">
        '.$actionBtns.'
      </div>
    </div>',
  'buttons' => [
      'view' => function ($url, $model) {
        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
          'title' => Yii::t('app', 'View'),
          'class'=>'dropdown-item text-1',
          'data-pjax'=>"0",
        ]);
      },
      'update' => function ($url, $model) {
        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
          'title' => Yii::t('app', 'Edit'),
          'class'=>'dropdown-item text-1',
          'data-pjax'=>"0",
        ]);
      },
      'delete' => function ($url, $model) {
        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
          'title' => Yii::t('app', 'Delete'),
          'class'=>'dropdown-item text-1',
          'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
          'data-method'=>"post",
          'data-pjax'=>"0",
        ]);
      },
  ],
];
?>
<?php CustomPjax::begin(['id'=>'grid-container']); ?>
<?php if($searchModel->wf==null){?>
<?= CustomGridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'cardTitle' => $cardTitle,
  'createBtn' => $createBtn,
  'columns' => $columns,
]);?>
<?php }else{?>
<?= $this->render('/opportunity/_kanban',[
'dataProvider' => $dataProvider,
'searchModel' => $searchModel,
'cardTitle' => $cardTitle,
'actionBtns' => $actionBtns,
])?>
<?php }?>
<?php CustomPjax::end(); ?>
<script>
function initScripts(){
  if($(".dtrpicker").length>0){
    $(".dtrpicker").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
    $(".dtrpicker").on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
      $(this).trigger("change");
    });
    $(".dtrpicker").on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });
  }
}
</script>
