<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Opportunities');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opportunity-index">
  <?= $this->render('_gridview', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'cardTitle' => $cardTitle,
  ]) ?>
</div>
