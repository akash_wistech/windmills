<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\ActivityLogWidget;
use app\widgets\CustomFieldsDetailWidget;
use app\widgets\AttachmentsWidget;
use app\assets\OpportunityDetailAsset;
OpportunityDetailAsset::register($this);

$pdItem = Yii::$app->appHelperFunctions->getPredefinedOptionsItem($model['service_type']);
?>
<div class="contact-view">
  <section class="card card-outline card-primary">
    <header class="card-header">
      <h2 class="card-title"><?= $model->title?></h2>
      <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){?>
      <div class="card-tools">
        <a href="<?= Url::to(['update','id'=>$model['id']])?>" class="btn btn-tool">
          <i class="fas fa-edit"></i>
        </a>
      </div>
      <?php }?>
    </header>
    <div class="card-body multi-cards">
      <div class="row">
        <div class="col-sm-8">
          <section class="card card-outline card-info mb-3">
            <header class="card-header">
              <h2 class="card-title"><?= Yii::t('app','General Info')?></h2>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </header>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Created:').'</strong> '.Yii::$app->formatter->asDate($model->created_at);?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Expected Close Date:').'</strong>'.($model->expected_close_date!=null ? Yii::$app->formatter->asDate($model->expected_close_date) : '');?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <?= '<strong>'.Yii::t('app','Title:').'</strong>'.$model->title;?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Module:').'</strong> '.Yii::$app->appHelperFunctions->olModulesListArr[$model->module_type];?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Ref:').'</strong>'.$model->module_keyword;?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Service:').'</strong>'.($pdItem!=null ? $pdItem['title'] : '');?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Quote Amount:').'</strong>'.Yii::$app->helperFunctions->withCurrencySign($model->quote_amount);?>
                </div>
              </div>
              <div>
                <?php
                echo '<strong>'.Yii::t('app','Tags:').'</strong><br />';
                if($model->tagsListArray!=null){
                  foreach($model->tagsListArray as $key=>$val){
                    echo '<span class="badge badge-primary">'.$val.'</span>&nbsp;';
                  }
                }
                ?>
              </div>
            </div>
          </section>
          <section class="card card-outline card-info mb-3">
            <header class="card-header">
              <h2 class="card-title"><?= Yii::t('app','Description')?></h2>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </header>
            <div class="card-body">
              <?= nl2br($model->descp)?>
            </div>
          </section>
          <?= CustomFieldsDetailWidget::widget(['type'=>$model->moduleTypeId,'model'=>$model])?>
          <?= AttachmentsWidget::widget(['type'=>$model->moduleTypeId,'model'=>$model])?>
        </div>
        <div class="col-sm-4">
          <?= ActivityLogWidget::widget(['type'=>'lead','model'=>$model])?>
        </div>
      </div>
    </div>
  </section>
</div>
