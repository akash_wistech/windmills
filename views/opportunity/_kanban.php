<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\KanBanAsset;
KanBanAsset::register($this);
?>
<section class="card card card-outline card-primary">
  <header class="card-header">
  	<h2 class="card-title"><?= $searchModel->process->title?></h2>
  </header>
  <div class="card-body table-responsive">
    <?php
    $boards=Yii::$app->appHelperFunctions->getKanbanWorkflowStagesList($searchModel->wf);
    if($boards!=null){
      $cssStyles='';
      $boardIdzArr=[];
      $strBoardsJs="var boardsData={};";
      $strBoardItemsJs="var boardItems = {};\n";
      $strTmpBoardNamesJs="var tmpBoardNames=[];\n";
      $strTmpBoardClassNamesJs="var tmpBoardClassName=[];\n";
      foreach($boards as $board){
        echo '<input type="hidden" id="board_item_sort_order-_kb_'.$board['id'].'" value="" class="form-control" />';
        $boardIdzArr[]=$board['id'];
        $strBoardItemsJs.="boardItems[".$board['id']."]=[];\n";
        $strTmpBoardNamesJs.="tmpBoardNames[".$board['id']."]=\"".$board['title']."\";\n";
        $strTmpBoardClassNamesJs.="tmpBoardClassName[".$board['id']."]=\"cls".str_replace("#","",$board['color_code'])."\";\n";

        $textColor = Yii::$app->helperFunctions->getReverseColor($board['color_code']);
        $cssStyles.='.cls'.str_replace("#","",$board['color_code']).'{background:'.$board['color_code'].';color:'.$textColor.';}';
      }
      $strBoardsJs.=";";
      $strTmpBoardsJs="var tmpBoardsData=[".implode(",",$boardIdzArr)."];\n";
    $this->registerJs('

    App.blockUIProgress({
    	message: "'.Yii::t('app','Generating report, Please wait...').'",
    	target: _targetContainer,
    	overlayColor: "none",
    	cenrerY: false,
    	boxed: true
    });
    loadBoardData("'.Url::to(['suggestion/board-data','module'=>$searchModel->moduleTypeId,'wf'=>$searchModel->wf,'wfs'=>implode(",",$boardIdzArr)]).'");
    ');
    ?>
    <style>
    <?= $cssStyles?>
    .kanban-item .btn-tool{padding: .25rem .01rem;}
    </style>
    <div id="kanbanArea"></div>
    <script>
    var _targetContainer="grid-container";
    <?= $strBoardsJs?>
    <?= $strTmpBoardsJs?>
    <?= $strTmpBoardNamesJs?>
    <?= $strTmpBoardClassNamesJs?>
    <?= $strBoardItemsJs?>
    function loadBoardData(url)
    {
    	$.ajax({
    		url: url,
    		type: "POST",
    		dataType: "json",
    		success: function(response) {
          buildDataSet(response["data"]);

    			if(response["progress"]!=null && response["progress"]!=""){
    				//moveProgressBar(response["progress"]["percentage"]);
    			}
    			if(response["url"]!=null && response["url"]!=""){
    				loadBoardData(response["url"])
    			}else{
            buildBoards();
    			}
    		},
    		error: bbAlert
    	});
    }
    function buildDataSet(data)
    {
      $.each(data, function( index, row ) {
        wfsid=row.wfsid;
        newRow={}
        newRow['id']=row.id;
        newRow['title']=row.title;
        boardItems[wfsid].push(newRow);
      });
    }
    function buildBoards()
    {
      n=0;
      var boardsData = [];
      $.each(tmpBoardsData, function(index, value) {
        var boardItem = [];
        boardItem['id'] = '_kb_'+value;
        boardItem['bid'] = value;
        boardItem['title'] = tmpBoardNames[value];
        boardItem['class'] = tmpBoardClassName[value];
        boardItem['item'] = boardItems[value];
        boardsData.push(boardItem);
        n++;
      });
      // console.log(boardsData);
      var Kanban = new jKanban({
        element: "#kanbanArea",
        gutter: "10px",
        widthBoard: "350px",
        dragItems: true,
        boards: boardsData,
        dropBoard: function(el, target, source, sibling){
          var _target = $(target.parentElement);
          var bsorted = [];
          var nodes = _target.find(".kanban-board");
          var currentbOrder = 0;
          $.each( nodes, function( key, value ) {
            bsorted.push({
              "id": $(value).data("id"),
              "order": currentbOrder++
            })
          });
          $("#board_sort_order").val(JSON.stringify(bsorted));
          url = "<?= Url::to(['suggestion/save-board-order'])?>";
          data = {id: <?= $searchModel->wf?>, sort_order: $("#board_sort_order").val()};
          makeSilentAjaxRequestWithData(url,data,"");
        },
        dropEl: function(el, target, source, sibling) {
          var _target = $(target.parentElement);
          _targetBoardId = _target.data("id");
          //Sorting Target
          var sorted = [];
          var nodes = Kanban.getBoardElements(_targetBoardId);
          var currentOrder = 0;
          nodes.forEach(function(value, index, array) {
            sorted.push({
              "id": $(value).data("eid"),
              "order": currentOrder++
            })
          });
          $("#board_item_sort_order-"+_targetBoardId).val(JSON.stringify(sorted));
          let otbId = _targetBoardId;
          otbId = otbId.replace("_kb_", "");
          turl = "<?= Url::to(['suggestion/save-board-item-order'])?>";
          tdata = {module: "<?= $searchModel->moduleTypeId?>", wfid: <?= $searchModel->wf?>, stgid: otbId, sort_order: $("#board_item_sort_order-"+_targetBoardId).val()};
          makeSilentAjaxRequestWithData(turl,tdata,"");

          //Sorting Source
          var _source = $(source.parentElement);
          _sourceBoardId = _source.data("id");
          if(_targetBoardId!=_sourceBoardId){
            var ssorted = [];
            var snodes = Kanban.getBoardElements(_sourceBoardId);
            var scurrentOrder = 0;
            snodes.forEach(function(value, index, array) {
              ssorted.push({
                "id": $(value).data("eid"),
                "order": scurrentOrder++
              })
            });
            $("#board_item_sort_order-"+_sourceBoardId).val(JSON.stringify(ssorted));
            let osbId = _sourceBoardId;
            osbId = osbId.replace("_kb_", "");
            surl = "<?= Url::to(['suggestion/save-board-item-order'])?>";
            sdata = {module: "<?= $searchModel->moduleTypeId?>", wfid: <?= $searchModel->wf?>, stgid: osbId, sort_order: $("#board_item_sort_order-"+_sourceBoardId).val()};
            makeSilentAjaxRequestWithData(surl,sdata,"");
          }
        },
      });
      App.unblockUI($(_targetContainer));
      $(".blockUI").remove();
    }
    </script>
    <input type="hidden" id="board_sort_order" value="" />
    <?php
    }else{
    ?>
    <div class="alert alert-danger text-center"><?= Yii::t('app','No board found!')?></div>
    <?php
    }?>
  </div>
</section>
