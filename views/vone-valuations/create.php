<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VoneValuations */

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Vone Valuations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vone-valuations-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
