<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use  app\components\widgets\StatusVerified;


$this->registerJs('


$("body").on("change", "#vonevaluations-wm_reference", function (e) {
        
    call_url = "'.\yii\helpers\Url::to(['suggestion/vonevaldata']).'";
    $.ajax({
        url: call_url,
        data: {ref_number:$(this).val()},
        dataType: "html",
        success: function(data) {
            data = JSON.parse(data);
            console.log(data)
            $("#vonevaluations-client_reference").val(data.data.client_reference);     
            $("#vonevaluations-valuation_id").val(data.data.id);  
            $("#vonevaluations-reason").val(data.data.revised_reason).trigger("change");  
        },
        error: bbAlert
    });
});


');
?>

<section class="vone-valuations-form card my-2 card-outline card-info">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card-body">
        <div class="row">
            <?= $form->field($model, 'valuation_id')->hiddenInput()->label(false) ?>
            <div class="col-3">
                <div class="form-group">
                    <?php
                    echo $form->field($model, 'wm_reference')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->getVoneValuations(),
                        'options' => ['placeholder' => 'Select ...', 'class' => 'wmreference'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('WM Reference');
                    ?>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <?= $form->field($model, 'client_reference')->textInput(['readonly'=>true]) ?>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <?php
                    echo $form->field($model, 'reason')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->getRevisedReasonsVone(),
                        'options' => ['placeholder' => 'Select ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Reason');
                    ?>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <?php
                    echo $form->field($model, 'error_status')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->getErrorStatusVone(),
                        // 'options' => ['placeholder' => 'Select ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Error Status');
                    ?>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <?= $form->field($model, 'client_requirements')->textArea(['rows'=>'3']) ?>
                </div>
            </div>
        </div>


        <?php
        if(Yii::$app->menuHelperFunction->checkActionAllowed('verification')){
            echo StatusVerified::widget([ 'model' => $model, 'form' => $form ]);
        }
        ?>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php 
            if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                ]);
            }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
