<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VoneValuations */

$this->title = 'Update : ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vone Valuations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vone-valuations-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
