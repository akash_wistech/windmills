

<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

$this->title = $title;
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;



$createBtn=false;
// if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
//     $createBtn=true;
// }
// if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
//     $actionBtns.='{view}';
// }
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
// if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
//     $actionBtns.='{delete}';
// }
$actionBtns.='{get-history}';



?>
<div class="sla-master-file-index">


    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
            
            [
                'attribute' => 'client_reference',
                'label' => Yii::t('app', 'Client Reference'),
                'value' => function ($model) {
                    return $model->client_reference;
                },
            ],   

            [
                'attribute' => 'wm_reference',
                'label' => Yii::t('app', 'WM Reference'),
                'value' => function ($model) {
                    return $model->wm_reference;
                },
            ],   

            [
                'attribute' => 'client_customer_name',
                'label' => Yii::t('app', 'Client Customer Name'),
                'value' => function ($model) {
                    return $model->valuation->client_name_passport;
                },
            ],   

            [
                'attribute' => 'property',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->valuation->property->title;
                },
            ],   

            [
                'attribute' => 'community',
                'label' => Yii::t('app', 'Community'),
                'value' => function ($model) {
                    return $model->valuation->building->communities->title;
                },
            ],   

            [
                'attribute' => 'building',
                'label' => Yii::t('app', 'Building/Project'),
                'value' => function ($model) {
                    return $model->valuation->building->title;
                },
            ],   

            [
                'attribute' => 'type',
                'label' => Yii::t('app', 'Inspection Type'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->inspectionTypeArr[$model->valuation->inspection_type];
                },
            ],   
            
            [
                'attribute' => 'reason',
                'label' => Yii::t('app', 'Reason'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->getRevisedReasonsVone()[$model->reason];
                },
            ],   
            
            // 'client_requirements',

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:12%'],
            //     'attribute' => 'status_verified',
            //     'label' => Yii::t('app', 'Status Verification'),
            //     'value' => function ($model) {
            //             return Yii::$app->appHelperFunctions->statusVerifyLabel[$model->status_verified];
            //     },
            //     'filter' => Yii::$app->appHelperFunctions->statusVerifyArr
            // ], 
            
            
            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'valuation_status',
                'label' => Yii::t('app', 'Valuation Status'),
                'value' => function ($model) {
                    return Yii::$app->helperFunctions->valuationStatusListArrLabel[$model->valuation->valuation_status];
                },
                // 'filter' => Yii::$app->helperFunctions->valuationStatusListArrLabel,
            ],             
            
            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'error_status',
                'label' => Yii::t('app', 'Error Status'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->getErrorStatusVoneLable()[$model->error_status];
                },
                'filter' => Yii::$app->appHelperFunctions->getErrorStatusVone(),
            ],             
            

            
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                    'get-history' => function ($url, $model) {
                        // Yii::$app->session->set('model_name', Yii::$app->appHelperFunctions->getModelName());
                        return Html::a('<i class="fa fa-history"></i> '.Yii::t('app', 'History'), 'javascript:;', [
                            'title' => Yii::t('app', 'History'),
                            'class'=>'dropdown-item text-1',
                            'target' => '_blank',
                            'onclick'=> "window.open('".$url."', '_blank')",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>


</div>



