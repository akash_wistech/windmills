<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\ActivityLogWidget;
use app\widgets\CustomFieldsDetailWidget;
use app\widgets\AttachmentsWidget;
use app\assets\ProspectDetailAsset;
ProspectDetailAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Prospect */

$this->title = Yii::t('app', 'Prospects');
$cardTitle = Yii::t('app','View Prospect:  {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');
?>

<div class="prospect-view">
  <section class="card card-outline card-primary">
    <header class="card-header">
      <h2 class="card-title"><?= $model->name?></h2>
      <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){?>
      <div class="card-tools">
        <a href="<?= Url::to(['update','id'=>$model['id']])?>" class="btn btn-tool">
          <i class="fas fa-edit"></i>
        </a>
      </div>
      <?php }?>
    </header>
    <div class="card-body multi-cards">
      <div class="row">
        <div class="col-sm-8">
          <section class="card card-outline card-info mb-3">
            <header class="card-header">
              <h2 class="card-title"><?= Yii::t('app','Contact Info')?></h2>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </header>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Created:').'</strong> '.Yii::$app->formatter->asDate($model->created_at);?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Full Name:').'</strong> '.$model->name;?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Email:').'</strong> '.$model->email;?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Contact No:').'</strong> '.$model->mobile;?>
                </div>
              </div>
              <?= $this->render('/shared/_multi_email_phone_detail',['model'=>$model])?>
              <?= '';//$this->render('/shared/_multi_company_detail',['model'=>$model])?>
              <div>
                <?php
                echo '<strong>'.Yii::t('app','Tags:').'</strong><br />';
                if($model->tagsListArray!=null){
                  foreach($model->tagsListArray as $key=>$val){
                    echo '<span class="badge badge-primary">'.$val.'</span>&nbsp;';
                  }
                }
                ?>
              </div>
            </div>
          </section>
          <?= CustomFieldsDetailWidget::widget(['type'=>$model->moduleTypeId,'model'=>$model])?>
          <?= '';//$this->render('/shared/_address_detail',['model'=>$model])?>
          <?= AttachmentsWidget::widget(['type'=>$model->moduleTypeId,'model'=>$model])?>
        </div>
        <div class="col-sm-4">
          <?= ActivityLogWidget::widget(['type'=>$model->moduleTypeId,'model'=>$model])?>
        </div>
      </div>
    </div>
  </section>
</div>
