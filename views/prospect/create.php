<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Prospect */

$this->title = Yii::t('app', 'Prospects');
$cardTitle = Yii::t('app','New Prospect');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="prospect-create">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
