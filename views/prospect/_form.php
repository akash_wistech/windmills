<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\widgets\CustomFieldsWidget;
use app\components\widgets\CustomPjax;
use app\assets\ProspectFormAsset;
ProspectFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Prospect */
/* @var $form yii\widgets\ActiveForm */


$model->tags=implode(",",$model->tagsListArray);
$model->manager_id = ArrayHelper::map($model->managerIdz,"staff_id","staff_id");

$this->registerJs('
$(".numeral-input").each(function(index){
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("body").on("blur", ".numeral-input", function () {
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("#prospect-manager_id").select2({
	allowClear: true,
	width: "100%",
});
$("#prospect-tags").tagsInput({
	"width":"100%",
	"defaultText":"Add tags",
});
');
$emlABtn = '<button type="button" class="btn btn-success btn-flat" onclick="addEmail()"><i class="fa fa-plus"></i></button>';
$nbrABtn = '<button type="button" class="btn btn-success btn-flat" onclick="addPhone()"><i class="fa fa-plus"></i></button>';
?>
<section class="prospect-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(); ?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-12">
        <?= $form->field($model, 'company_name')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'email',['template'=>'
        {label}
        <div class="input-group input-group-md">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
          </div>
          {input}
          <div class="input-group-append">
            '.$emlABtn.'
          </div>
        </div>
        {error}{hint}
        '])->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'mobile',['template'=>'
        {label}
        <div class="input-group input-group-md">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-phone fa-flip-horizontal"></i></span>
          </div>
          {input}
          <div class="input-group-append">
            '.$nbrABtn.'
          </div>
        </div>
        {error}{hint}
        '])->textInput(['maxlength' => true])?>
      </div>
    </div>
    <?= $this->render('/shared/_multi_email_phone_form',['form'=>$form,'model'=>$model])?>
    <?= '';//$this->render('/shared/_multi_company_form',['form'=>$form,'model'=>$model])?>
    <?= $form->field($model, 'tags')->textInput(['placeholder'=>'Add tags','maxlength' => true])?>
    <?= $form->field($model, 'manager_id')->dropDownList(Yii::$app->appHelperFunctions->staffMemberListArr,['multiple'=>'multiple'])?>
    <?= CustomFieldsWidget::widget(['form'=>$form,'type'=>$model->moduleTypeId,'model'=>$model])?>
    <?= '';//$this->render('/shared/_address_form',['form'=>$form,'model'=>$model])?>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
<?= $this->render('js/form_scripts',['model'=>$model]);?>
