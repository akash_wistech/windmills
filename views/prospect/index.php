<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\DataTableGridView;
use app\components\widgets\CustomPjax;
use app\assets\ProspectListAsset;
ProspectListAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProspectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Prospects');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
  $actionBtns.='{view}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
  $actionBtns.='{status}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('convert')){
  $actionBtns.='{convert}';
}
$this->registerJs('
initScripts();
$(document).on("pjax:success", function() {
  initScripts();
});
$("body").on("beforeSubmit", "form#company-form", function () {
	var _targetContainer="#company-form";
	var form = $(this);
	// return false if form still have some validation errors
	if (form.find(".has-error").length) {
		return false;
	}
	App.blockUI({
		message: "'.Yii::t('app','Please wait...').'",
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});
	// submit form
	$.ajax({
		url: form.attr("action"),
		type: "post",
		data: form.serialize(),
		dataType: "json",
		success: function (response) {
			if(response["success"]){
				toastr.success(response["success"]["msg"], response["success"]["heading"], {timeOut: 5000});
        form.trigger("reset");
				closeModal();
        if($("#grid-container").length){
          $.pjax.reload({container: "#grid-container", timeout: 2000});
        }
			}else{
				Swal.fire({title: response["error"]["heading"], html: response["error"]["msg"], icon: "error", timer: 5000});
			}
			App.unblockUI($(_targetContainer));
		}
	});
	return false;
});
');
//$columns[]=['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']];
$columns[]=['format'=>'raw','attribute'=>'cp_name','value'=>function($model){
  if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
    return Html::a($model['cp_name'],['view','id'=>$model['id']],['data-pjax'=>'0']);
  }else{
    return $model['cp_name'];
  }
}];
$columns[]='company_name';
$gridViewColumns=Yii::$app->inputHelperFunctions->getGridViewColumns($searchModel->moduleTypeId);
if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $colLabel=($gridViewColumn['short_name']!=null && $gridViewColumn['short_name']!='' ? $gridViewColumn['short_name'] : $gridViewColumn['title']);
    $columns[]=['format'=>'html','label'=>$colLabel,'value'=>function($model) use ($gridViewColumn){
      return Yii::$app->inputHelperFunctions->getGridValue('prospect',$model,$gridViewColumn);
    },'filter'=>Yii::$app->inputHelperFunctions->getGridViewFilters($gridViewColumn)];
  }
}
$columns[]=['attribute'=>'created_at','label'=>Yii::t('app', 'Created'),'format'=>'datetime','filterInputOptions'=>['class'=>'form-control dtrpicker','autocomplete'=>'off']];
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>Yii::t('app','Action'),
  'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
  'contentOptions'=>['class'=>'noprint actions'],
  'template' => '
    <div class="btn-group flex-wrap">
      <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
      </button>
      <div class="dropdown-menu" role="menu">
        '.$actionBtns.'
      </div>
    </div>',
  'buttons' => [
      'view' => function ($url, $model) {
        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
          'title' => Yii::t('app', 'View'),
          'class'=>'dropdown-item text-1',
          'data-pjax'=>"0",
        ]);
      },
      'update' => function ($url, $model) {
        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
          'title' => Yii::t('app', 'Edit'),
          'class'=>'dropdown-item text-1',
          'data-pjax'=>"0",
        ]);
      },
      'convert' => function ($url, $model) {
        if(!$model['company_id']>0){
          return Html::a('<i class="fas fa-arrow-right"></i> '.Yii::t('app', 'Convert'), 'javascript:;', [
            'title' => Yii::t('app', 'Convert'),
            'class'=>'dropdown-item text-1 load-modal',
            'data-heading'=>Yii::t('app','Convert to Client - {name}',['name'=>$model['cp_name']]),
            'data-url'=>Url::to(['prospect/convert','id'=>$model['id']]),
            'data-pjax'=>"0",
          ]);
        }
      },
      'delete' => function ($url, $model) {
        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
          'title' => Yii::t('app', 'Delete'),
          'class'=>'dropdown-item text-1',
          'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
          'data-method'=>"post",
          'data-pjax'=>"0",
        ]);
      },
  ],
];
?>
<style>
.filters{display: none;}
</style>
<div class="lead-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= DataTableGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'cardTitle' => $cardTitle,
    'createBtn' => $createBtn,
    'import' => true,
    'columns' => $columns,
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<script>
function initScripts(){
  if($(".dtrpicker").length>0){
    $(".dtrpicker").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
    $(".dtrpicker").on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
      $(this).trigger("change");
    });
    $(".dtrpicker").on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });
  }
}
</script>
