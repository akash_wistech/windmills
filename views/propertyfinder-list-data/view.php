<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyfinderListData */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Propertyfinder List Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="propertyfinder-list-data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'listings_reference',
            'source',
            'listing_website_link',
            'listing_date',
            'building_info',
            'city_id',
            'property_type',
            'community',
            'sub_community',
            'property_category',
            'no_of_bedrooms',
            'built_up_area',
            'land_size',
            'listings_price',
            'listings_rent',
            'final_price',
            'status',
            'purpose',
            'move_to_listing',
        ],
    ]) ?>

</div>
