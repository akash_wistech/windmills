<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ListingsTransactionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Property Finder List Data');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;
$actionBtns .= '{view}';
$actionBtns .= '{update}';
$actionBtns .= '{delete}';


?>
<style>
.btn-success {
    margin-left: 5px;
}
</style>
<div class="list-data-index">


<?php CustomPjax::begin(['id' => 'grid-container']); ?>
<?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'cardTitle' => $cardTitle,
    'createBtn' => $createBtn,
    'columns' => [
        
        [
            'class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']
        ],
        
        'listings_reference',
        
        [
            'attribute' => 'source',
            'label' => Yii::t('app', 'Source'),
            'value' => function ($model) {
                return Yii::$app->appHelperFunctions->getPropertySourceListArr()[$model->source];
            },
            'filter' =>Yii::$app->appHelperFunctions->getPropertySourceListArr()
        ],
        
        // 'listing_website_link',
        'listing_date',
        'building_info',
        
        ['attribute' => 'city_id',
        'label' => Yii::t('app', 'City'),
        'value' => function ($model) {
            return Yii::$app->appHelperFunctions->emiratedListArr[$model['city_id']];
        },
        'filter' => Yii::$app->appHelperFunctions->emiratedListArr
    ],
    ['attribute' => 'property_type',
    'label' => Yii::t('app', 'Property type'),
    'value' => function ($model) {
        return Yii::$app->appHelperFunctions->propertyTypeArr[$model['property_type']];
    },
    'filter' => Yii::$app->appHelperFunctions->propertyTypeArr
],


// 'property_category',
'no_of_bedrooms',
'built_up_area',
'land_size',
'listings_price',
'listings_rent',
'final_price',

['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
    return Yii::$app->helperFunctions->arrStatusIconList[$model['status']];
},'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>Yii::$app->helperFunctions->arrFilterStatusList],

['format'=>'raw','attribute'=>'purpose','label'=>Yii::t('app','Purpose'),'value'=>function($model){
    return Yii::$app->appHelperFunctions->purposeListLableArr[$model['purpose']];
},'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>Yii::$app->appHelperFunctions->purposeListArr],


['format'=>'raw','attribute'=>'move_to_listing','label'=>Yii::t('app','Migration Status'),'value'=>function($model){
    return Yii::$app->appHelperFunctions->moveToListingArrLable[$model['move_to_listing']];
},'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>Yii::$app->appHelperFunctions->moveToListingArr],





// [
    //     'class' => 'yii\grid\ActionColumn',
    //     'header' => '',
    //     'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
    //     'contentOptions' => ['class' => 'noprint actions'],
    //     'template' => '
    //     <div class="btn-group flex-wrap">
    //     <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
    //     <span class="caret"></span>
    //     </button>
    //     <div class="dropdown-menu" role="menu">
    //     ' . $actionBtns . '
    //     </div>
    //     </div>',
    //     'buttons' => [
        //         'view' => function ($url, $model) {
            //             return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
                //                 'title' => Yii::t('app', 'View'),
                //                 'class' => 'dropdown-item text-1',
                //                 'data-pjax' => "0",
                //             ]);
                //         },
                //         'update' => function ($url, $model) {
                    //             // print_r($url); die;
                    //             return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Edit'), $url, [
                        //                 'title' => Yii::t('app', 'Edit'),
                        //                 'class' => 'dropdown-item text-1',
                        //                 'data-pjax' => "0",
                        //             ]);
                        //         },
                        //         'delete' => function ($url, $model) {
                            //             return Html::a('<i class="fas fa-trash"></i> ' . Yii::t('app', 'Delete'), $url, [
                                //                 'title' => Yii::t('app', 'Delete'),
                                //                 'class' => 'dropdown-item text-1',
                                //                 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                //                 'data-method' => "post",
                                //                 'data-pjax' => "0",
                                //             ]);
                                //         },
                                //     ],
                                // ],
                            ],
                        ]); ?>
                        <?php CustomPjax::end(); ?>
                        
                        
                        </div>
                        