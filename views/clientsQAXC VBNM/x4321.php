<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Developers */
/* @var $form yii\widgets\ActiveForm */
?>


<section class="developers-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'segment_type')->dropDownList(Yii::$app->appHelperFunctions->ClientSegments)->label('Type')?>
            </div>
        </div>




        <div class="row">
            <div class="col-sm-12">
                <section class="valuation-form card card-outline card-primary">

                    <header class="card-header">
                        <h2 class="card-title">Finance</h2>
                    </header>
                    <div class="card-body">

                        <table id="attachment-auto" class="table table-striped table-bordered table-hover images-table">
                            <thead>
                            <tr>
                                <td class="text-left">Name</td>
                                <td class="text-left">Email</td>
                                <td class="text-left">status</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $row_auto = 0; ?>
                            <?php



                            if(isset($model->customEmailsVals) && ($model->customEmailsVals <> null)) {
                                foreach ($model->customEmailsVals as $attachment) { ?>
                                    <tr id="image-row-attachment-auto-<?php echo $row_auto; ?>">

                                        <td>
                                            <input type="text" class="form-control"
                                                   name="Company[customEmailsAuto][<?= $row_auto ?>][name]"
                                                   value="<?= $attachment->name ?>" placeholder="Name" required/>
                                        </td>

                                        <td>
                                            <input type="email" class="form-control"09nb876c
                                                   name="Company[customEmailsAuto][<?= $row_auto ?>][email]"
                                                   value="<?= $attachment->email ?>" placeholder="Email" required/>
                                        </td>
                                        <input type="hidden" class="form-control"
                                               name="Company[customEmailsAuto][<?= $row_auto ?>][id]"
                                               value="<?= $attachment->id ?>" placeholder="Name" required/>
                                        <input type="hidden" class="form-control"
                                               name="Company[customEmailsAuto][<?= $row_auto ?>][type]"
                                               value="<?= $attachment->type ?>" placeholder="Name" required/>

                                        <td class="text-left">
                                            <button type="button"
                                                    onclick="deleteRowauto('-attachment-auto-<?= $row_auto ?>', '<?= $attachment->id; ?>', 'attachment')"
                                                    data-toggle="tooltip" title="You want to delete Attachment"
                                                    class="btn btn-danger"><i
                                                        class="fa fa-minus-circle"></i></button>
                                        </td>
                                    </tr>
                                    <?php $row_auto++; ?>
                                <?php }
                            }?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="2"></td>
                                <td class="text-left">
                                    <button type="button" onclick="addAttachmentauto();" data-toggle="tooltip" title="Add"
                                            class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                                </td>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </section>
            </div>
        </div>
        <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php 
            if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                ]);
            }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
