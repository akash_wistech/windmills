<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Industry */

$this->title = 'Create Industry';
$this->params['breadcrumbs'][] = ['label' => 'Industry', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sales-and-marketing-purpose-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
