<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;

/* @var $this yii\web\View */
/* @var $model app\models\AssetCategory */
/* @var $form yii\widgets\ActiveForm */
?>



<section class="sales-and-marketing-purpose-form card card-outline card-success">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card-body">

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder' => 'Enter the title...']) ?>
            </div>
        </div>

        <?php
        if (Yii::$app->menuHelperFunction->checkActionAllowed('Verified')) {
            echo StatusVerified::widget(['model' => $model, 'form' => $form]);
        }
        ?>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i> Save'), ['class' => 'btn btn-success sav-btn1']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if ($model <> null && $model->id <> null) {
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => get_class($model),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>