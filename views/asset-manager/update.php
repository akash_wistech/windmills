<?php

use yii\helpers\Html;

$this->title = 'Update Asset: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asset-update">

    <?= $this->render('_form', [
        'model' => $model,
        'page_title' => $this->title,
    ]) ?>

</div>
