<?php

use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\User;
use app\models\CategoryManager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GeneralAsumptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Asset Manager";
$this->params['breadcrumbs'][] = $this->title;

$cardTitle = Yii::t('app', 'Asset List');

$createBtn=true;
// if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
//     $createBtn=true;
// }
// if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
//     $actionBtns.='{view}';
// }
$actionBtns.='{update}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
$actionBtns.='{delete}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
    $actionBtns.='{delete}';
}

$actionBtns.='{download-register}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('download-register')){
    $actionBtns.='{download-register}';
}
// $actionBtns.='{get-history}';


?>
<div class="task-index">

    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:1px;']],


            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:20%'],
                'attribute' => 'transaction_type',
                'label' => Yii::t('app', 'Type'),
                'value' => function ($model) {
                    return ucfirst($model->transaction_type);
                },
            ],


            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:20%'],
                'attribute' => 'asset_name',
                'label' => Yii::t('app', 'Asset Name'),
                'value' => function ($model) {
                    return $model->asset_name;
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:20%'],
                'attribute' => 'category_id',
                'label' => Yii::t('app', 'Category'),
                'value' => function ($model) {
                    return $model->category_name;
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:20%'],
                'attribute' => 'depreciation',
                'label' => Yii::t('app', 'Depreciation'),
                'value' => function ($model) {
                    if(isset($model->depreciation))
                    {
                        return $model->depreciation.'%';
                    }else{
                        return "";
                    }

                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:20%'],
                'attribute' => 'status',
                'label' => Yii::t('app', 'Status'),
                'value' => function ($model) {
                    if($model->status == "active")
                    {
                        return "<span style=\"color:green\">".ucfirst($model->status)."</span>";
                    }elseif($model->status == "inactive"){
                        return "<span style=\"color:orange\">".ucfirst($model->status)."</span>";
                    } 
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
                    <div class="dropdown-menu" role="menu">
              '.$actionBtns.'
                    </div>
                </div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },

                    'get-history' => function ($url, $model) {
                        // Yii::$app->session->set('model_name', Yii::$app->appHelperFunctions->getModelName());
                        return Html::a('<i class="fa fa-history"></i> '.Yii::t('app', 'History'), $url, [
                            'title' => Yii::t('app', 'History'),
                            'class'=>'dropdown-item text-1',
                            'target' => '_blank',
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>

</div>
