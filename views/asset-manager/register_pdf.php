<?php

use app\models\ExpenseManager;
use app\models\AssetManager;
use app\models\User;

$expenses = ExpenseManager::find()->where(['transaction_type'=>'asset'])->all();

?>

<br>

<style>
.box1{
  background-color: #E3F2FD;
  width: 90px;
  border-bottom: 0.5px solid #1E88E5;
}
.box2{
  background-color: #19B65F;
  width: 150px;
  border-bottom: 0.5px solid #1E88E5;
  height: 75px;
}
.box3{
  background-color: #E3F2FD;
  width: 90px;
  border-bottom: 0.5px solid #1E88E5;
}
</style>


<table cellspacing="1" cellpadding="3" width="466">
<br><span class="airal-family" style="font-size:14px ; text-align:center">Asset Register</span><br>

  <thead>
    <hr style="height: 0.2px;">
    <tr style="background-color: ;">
      <td class="airal-family" style="width: 30px; font-size:9px; font-weight: bold; border-bottom: 0.2px solid black; ">S.No</td>
      <td class="airal-family" style="width: 60px; font-size:9px; font-weight: bold; border-bottom: 0.2px solid black;">Asset <br>Name </td>
      <td class="airal-family" style="width: 50px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">Location</td>
      <td class="airal-family" style="width: 90px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">Owner</td>
      <td class="airal-family" style="width: 70px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">Cost</td>
      <td class="airal-family" style="width: 45px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">Asset Life<br>(Months)</td>
      <td class="airal-family" style="width: 55px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">Remaining Life<br>(Months)</td>
      <td class="airal-family" style="width: 70px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">Depreciation <br> (Till today)</td>
      <td class="airal-family" style="width: 60px; font-size:9px; font-weight: bold; text-align: center; border-bottom: 0.2px solid black;">Book Value</td>
    </tr>
  </thead>

  <tbody>
    <?php
    $i=1;
    $total_cost = 0;
    $total_dep = 0;
    foreach($expenses as $key => $expense) {

      $asset = AssetManager::find()->where(['id' => $expense->asset_id])->one();
      $name = $asset->asset_name;
      $location = $expense->asset_location;

      $owner_details = User::find()->where(['id' => $expense->asset_owner])->one();
      $owner = $owner_details->firstname.' '.$owner_details->lastname;

      $cost = $expense->amount;
      $asset_life = $expense->asset_life;
  
        $specificDate = $expense->collection_date;
  
        $startDate = new \DateTime($specificDate);
        $endDate = new \DateTime();
  
        $interval = $startDate->diff($endDate);

        $years = $interval->format('%y');
        $years = $years*12;

        $months = $interval->format('%m');
        $monthsPassed = $years + $months;
  
      $remaining_life = $asset_life-$monthsPassed;

      $depreciation = ($cost/$asset_life)*$monthsPassed;
      $book_value = $cost-$depreciation;
      
      if($remaining_life < 0)
      {
        $remaining_life = 0;
        $book_value = 0;
        $depreciation = $cost;
      }

      $total_cost += $cost;
      $total_dep += $depreciation;
      $total_val += $book_value;
  

        ?>
        <tr style="background-color: ;">
          <td class="airal-family" style="width: 30px; font-size:9px; "><?php echo $i ?></td>
          <td class="airal-family" style="width: 60px; font-size:9px; "><?= $name ?></td>
          <td class="airal-family" style="width: 50px; font-size:9px; text-align: center;"><?= $location ?></td>
          <td class="airal-family" style="width: 90px; font-size:9px; text-align: center;"><?= $owner ?></td>
          <td class="airal-family" style="width: 70px; font-size:9px; text-align: center;"><?= number_format($cost,0) ?></td>
          <td class="airal-family" style="width: 45px; font-size:9px; text-align: center;"><?= $asset_life ?></td>
          <td class="airal-family" style="width: 55px; font-size:9px; text-align: center;"><?= $remaining_life ?></td>
          <td class="airal-family" style="width: 70px; font-size:9px; text-align: center;"><?= number_format($depreciation , 0) ?></td>
          <td class="airal-family" style="width: 60px; font-size:9px; text-align: center;"><?= number_format($book_value , 0) ?></td>
        </tr>

        <?php
        $i++;
        if ($i<=count($expenses)) {?>
          <hr style="height: 0.2px;">
          <?php
        }

    }
    
    ?>
  </tbody>

  <tfoot>
    <hr style="height: 0.2px;">
    <tr style="background-color: ;">
      <td class="airal-family" style="width: 30px; font-size:9px; "></td>
      <td class="airal-family" style="width: 60px; font-size:9px; font-weight: bold;"> Total </td>
      <td class="airal-family" style="width: 50px; font-size:9px; font-weight: bold; text-align: center; "></td>
      <td class="airal-family" style="width: 90px; font-size:9px; font-weight: bold; text-align: center; "></td>
      <td class="airal-family" style="width: 70px; font-size:9px; font-weight: bold; text-align: center; "><?php echo number_format($total_cost,0) ?></td>
      <td class="airal-family" style="width: 45px; font-size:9px; font-weight: bold; text-align: center; "></td>
      <td class="airal-family" style="width: 55px; font-size:9px; font-weight: bold; text-align: center; "></td>
      <td class="airal-family" style="width: 70px; font-size:9px; font-weight: bold; text-align: center; "><?php echo number_format($total_dep,0) ?></td>
      <td class="airal-family" style="width: 60px; font-size:9px; font-weight: bold; text-align: center; "><?php echo number_format($total_val,0) ?></td>
    </tr>

    <hr style="height: 0.2px;">
    <tr style="background-color: ;">
      <td class="airal-family" style="width: 30px; font-size:9px; "></td>
      <td class="airal-family" style="width: 60px; font-size:9px; font-weight: bold;"> Average </td>
      <td class="airal-family" style="width: 50px; font-size:9px; font-weight: bold; text-align: center; "></td>
      <td class="airal-family" style="width: 90px; font-size:9px; font-weight: bold; text-align: center; "></td>
      <td class="airal-family" style="width: 70px; font-size:9px; font-weight: bold; text-align: center; "><?php echo number_format($total_cost/count($expenses),2) ?></td>
      <td class="airal-family" style="width: 45px; font-size:9px; font-weight: bold; text-align: center; "></td>
      <td class="airal-family" style="width: 55px; font-size:9px; font-weight: bold; text-align: center; "></td>
      <td class="airal-family" style="width: 70px; font-size:9px; font-weight: bold; text-align: center; "><?php echo number_format($total_dep/count($expenses),2) ?></td>
      <td class="airal-family" style="width: 60px; font-size:9px; font-weight: bold; text-align: center; "><?php echo number_format($total_val/count($expenses),2) ?></td>
    </tr>
  </tfoot>

</table>
<hr style="height: 0.3px;">

<br><br>
<!-- <table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <tr style="background-color: ;">
      <td class="airal-family" style="font-size:10px; text-align:left;">Accepted By</td>
      <td class="airal-family" style="width: 90px; font-size:10px; text-align:left;">Accepted Date</td>
    </tr>
  </thead>
</table> -->


<style>
  .airal-family{
    font-family: Arial, Helvetica, sans-serif;
  }
</style>


<style>

  .text-dec{
    font-size: 10px;
    text-align: center;
  }

/*td.detailtext{*/
/*background-color:#BDBDBD; */
/*font-size:9px;*/
/*border-top: 0.4px solid grey;*/
/*}*/

td.detailtexttext{
/* background-color:#EEEEEE; */
font-size:11px;
}
th.detailtext{
/* background-color:#BDBDBD; */
font-size:9px;
border-top: 1px solid black;
}
/*.border {
border: 1px solid black;
}*/

th.amount-heading{
  color: #039BE5;
}
td.amount-heading{
  color: #039BE5;
}
th.total-due{
  font-size: 16px;
}
td.total-due{
  font-size: 16px;
}
span.size-8{
  font-size: 10px;
  font-weight: bold;
}
td.first-section{
  padding-left: 10px;
/* background-color: black; */
}
</style>

