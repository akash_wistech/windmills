<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use yii\helpers\ArrayHelper;

$categories = ArrayHelper::map(\app\models\CategoryManager::find()
->select([
  'id', 'category_name',
])
  ->where(['status' => 'active'])
  ->orderBy(['id' => SORT_ASC,])
  ->all(), 'id', 'category_name');

  $categories = ['' => 'select'] + $categories;

      //zoho get chart of accounts
      Yii::$app->appHelperFunctions->ZohoRefereshToken();
      $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');
      $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');
  
  
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://www.zohoapis.com/books/v3/chartofaccounts?filter_by=AccountType.All&organization_id='.$OrganizationID,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
          'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
      ),
      ));
  
      $response = curl_exec($curl);
      curl_close($curl);
      $chart_accounts = json_decode($response);


?>

<section class="contact-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <header class="card-header">
        <h2 class="card-title"><?php if(isset($page_title)) {echo $page_title;}else{echo "Add Asset";} ?></h2>
    </header>
    <div class="card-body">

        <div class="row"> 

            <div class="col-4">
                <label for="transaction_type">Transaction Type</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='transaction_type' id="transaction_type" class="form-control">
                            <option value="asset"
                                <?php if(isset($model->transaction_type) && ($model->transaction_type == 'asset')){ echo 'selected';} ?>>
                                Asset</option>
                            <option value="consumable"
                                <?php if(isset($model->transaction_type) && ($model->transaction_type == 'consumable')){ echo 'selected';} ?>>
                                Consumable</option>
                            <option value="service"
                                <?php if(isset($model->transaction_type) && ($model->transaction_type == 'service')){ echo 'selected';} ?>>
                                Service</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-4" id="asset_div">
                <label for="asset_name">Asset Name</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" name="asset_name" id="asset_name" value="<?php if(isset($model->asset_name)) echo $model->asset_name; ?>" >
                    </div>
                </div>
            </div>

            <div class="col-4" id="service_div">
                <label for="expense_name">Consumable/Service Name</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" name="expense_name" id="expense_name" value="<?php if(isset($model->asset_name)) echo $model->asset_name; ?>" >
                    </div>
                </div>
            </div>

            <div class="col-4" id="asset_category">
                <label for="asset_category_id"> Asset Category </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name="asset_category_id" id="asset_category_id" class="form-control chartOfAccounts">
                            <?php
                                    foreach($chart_accounts->chartofaccounts as $key => $data){
                                        if( $data->account_type == "fixed_asset")
                                        {
                                            if($model->category_id == $data->account_id)
                                            {
                                                echo "<option value='$data->account_id".","."$data->account_name' selected>$data->account_name</option>";
                                            }else{
                                                echo "<option value='$data->account_id".","."$data->account_name'> $data->account_name</option>";
                                            }
                                        }

                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-4" id="expense_category">
                <label for="expense_category_id"> Consumable/Service Category </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name="expense_category_id" id="expense_category_id" class="form-control chartOfAccounts">
                            <?php
                                    foreach($chart_accounts->chartofaccounts as $key => $data){
                                        if( $data->account_type == "expense")
                                        {
                                            if($model->category_id == $data->account_id)
                                            {
                                                echo "<option value='$data->account_id".","."$data->account_name' selected>$data->account_name</option>";
                                            }else{
                                                echo "<option value='$data->account_id".","."$data->account_name'> $data->account_name</option>";
                                            }
                                        }

                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

        </div>
        
        <div class="row">
            <div class="col-4" id="dep_div">
                <label for="depreciation"> Depriciation <span style="font-size:9px"> (%)</span> </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" name="depreciation" id="depreciation" value="<?php if(isset($model->depreciation)) echo $model->depreciation; ?>" >
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="brand">Brand </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="text" id="brand" name="brand" value="<?php if(isset($model->brand)) echo $model->brand; ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="status">Status</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='status' id="status" class="form-control">
                            <option value="active"
                                <?php if(isset($model->status) && ($model->status == 'active')){ echo 'selected';} ?>>
                                Active</option>
                            <option value="inactive"
                                <?php if(isset($model->status) && ($model->status == 'inactive')){ echo 'selected';} ?>>
                                Inactive</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if($model<>null && $model->id<>null){
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => get_class($model),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>

<?php
$this->registerJs('

var value = $("#transaction_type").val();

if(value == "asset")
{
    $("#asset_div").css("display", "block");
    $("#service_div").css("display", "none");
    $("#dep_div").css("display", "block");
    $("#expense_category").css("display", "none");
    $("#asset_category").css("display", "block");
}

if(value == "consumable")
{
    $("#asset_div").css("display", "none");
    $("#service_div").css("display", "block");
    $("#dep_div").css("display", "none");
    $("#expense_category").css("display", "block");
    $("#asset_category").css("display", "none");
}

if(value == "service")
{
    $("#asset_div").css("display", "none");
    $("#service_div").css("display", "block");
    $("#dep_div").css("display", "none");
    $("#expense_category").css("display", "block");
    $("#asset_category").css("display", "none");
}

$("#transaction_type").change(function() {
    var value = $("#transaction_type").val();
      if(value == "asset")
      {
        $("#asset_div").css("display", "block");
        $("#service_div").css("display", "none");
        $("#dep_div").css("display", "block");
        $("#expense_category").css("display", "none");
        $("#asset_category").css("display", "block");
      }
      if(value == "service"){
        $("#asset_div").css("display", "none");
        $("#service_div").css("display", "block");
        $("#dep_div").css("display", "none");
        $("#expense_category").css("display", "block");
        $("#asset_category").css("display", "none");
      }
      if(value == "consumable"){
        $("#asset_div").css("display", "none");
        $("#service_div").css("display", "block");
        $("#dep_div").css("display", "none");
        $("#expense_category").css("display", "block");
        $("#asset_category").css("display", "none");
      }
});

    
');
