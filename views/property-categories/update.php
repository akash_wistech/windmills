<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ListingSubTypes */


$this->title = Yii::t('app', 'Property Categories');
$cardTitle = Yii::t('app','Update Property Categories:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="developers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>