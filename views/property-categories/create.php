<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ListingSubTypes */

$this->title = Yii::t('app', 'Create Property Categories');
$cardTitle = Yii::t('app','New Property Categories');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="developers-create">



    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
