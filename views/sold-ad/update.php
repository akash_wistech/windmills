<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SoldAd */

$this->title = 'Update Sold Ad: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sold Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sold-ad-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
