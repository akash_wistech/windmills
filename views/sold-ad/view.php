<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SoldAd */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sold Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sold-ad-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'reidin_ref',
            'sales_type',
            'sales_sequence',
            'transaction_date',
            't_date',
            'municipality',
            'district',
            'community',
            'projectsub_comm',
            'building',
            'property_subtype',
            'floor',
            'bedroom',
            'sale_percent',
            'unit_area_sqm',
            'plot_area_sqm',
            'price',
            'mortgage_value',
            'average_price_sqm',
            'valuation_amount',
            'developer',
            'status',
        ],
    ]) ?>

</div>
