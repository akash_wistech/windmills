<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SoldAd */

$this->title = 'Create Sold Ad';
$this->params['breadcrumbs'][] = ['label' => 'Sold Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sold-ad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
