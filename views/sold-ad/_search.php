<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SoldAdSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sold-ad-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'reidin_ref') ?>

    <?= $form->field($model, 'sales_type') ?>

    <?= $form->field($model, 'sales_sequence') ?>

    <?= $form->field($model, 'transaction_date') ?>

    <?php // echo $form->field($model, 't_date') ?>

    <?php // echo $form->field($model, 'municipality') ?>

    <?php // echo $form->field($model, 'district') ?>

    <?php // echo $form->field($model, 'community') ?>

    <?php // echo $form->field($model, 'projectsub_comm') ?>

    <?php // echo $form->field($model, 'building') ?>

    <?php // echo $form->field($model, 'property_subtype') ?>

    <?php // echo $form->field($model, 'floor') ?>

    <?php // echo $form->field($model, 'bedroom') ?>

    <?php // echo $form->field($model, 'sale_percent') ?>

    <?php // echo $form->field($model, 'unit_area_sqm') ?>

    <?php // echo $form->field($model, 'plot_area_sqm') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'mortgage_value') ?>

    <?php // echo $form->field($model, 'average_price_sqm') ?>

    <?php // echo $form->field($model, 'valuation_amount') ?>

    <?php // echo $form->field($model, 'developer') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
