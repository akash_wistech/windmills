<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SoldAd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sold-ad-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reidin_ref')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sales_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sales_sequence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transaction_date')->textInput() ?>

    <?= $form->field($model, 't_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'municipality')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'district')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'community')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'projectsub_comm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'building')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'property_subtype')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'floor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bedroom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sale_percent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unit_area_sqm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plot_area_sqm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mortgage_value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'average_price_sqm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'valuation_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'developer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
