<?php


use app\assets\DateRangePickerAsset2;
use app\models\FeeDifference;

DateRangePickerAsset2::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BranchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Fee Differences');
$this->params['breadcrumbs'][] = $this->title;



//number count for client, buildings, valuation steps.
$totalProposalFeedDiff =(int)FeeDifference::find()->where(['type'=>1])->count('id');
$totalValuationFeedDiff =(int)FeeDifference::find()->where(['type'=>2])->count('id');


?>
<style>
    .yfstar { color: #f39c12;}
    .gnfstar { color: #95a5a6;}
    h4 {
        font-size: 16px !important;
        font-weight: bold;
        font-family: sans-serif !important;
        width: fit-content;
    }

    .card-header {
        padding: 10px !important;
        padding-bottom: 20px !important;
    }

    .card-footer {
        padding: 0px !important;
    }

    .card-footer span {
        font-size: 13.5px !important;
    }

    .col-7 {
        max-width: fit-content !important;
    }

    .pending-text-info {
        color: orange !important;
    }
</style>

<div class="feedback-index">

<section class="valuation-form card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title">Fee Difference</h2>
        </header>
        <div class="card-body" style="background: #E5E4E2">
            <div class="row px-4">

                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: orange !important;">
                                    <i class="fas fa-money-bill-alt pt-1 pl-2" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Proposals Fee Difference</span><br>
                                        <?= number_format($totalProposalFeedDiff) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px; color:orange;"></i>
                                        <a class="position-absolute footer-increas" style="left:53px;top:5px; color:orange; font-weight:bold" href="<?= yii\helpers\Url::toRoute(['fee-difference/proposals']); ?>"> <span style="font-size:18px;">Go to Proposals Fee Difference</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: orange !important;">
                                    <i class="fas fa-money-bill-alt pt-1 pl-2" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Valuations Fee Difference</span><br>
                                        <?= number_format($totalValuationFeedDiff) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px; color:orange;"></i>
                                        <a class="position-absolute" style="left:53px;top:5px; color:orange; font-weight:bold" href="<?= yii\helpers\Url::toRoute(['fee-difference/valuations']); ?>"> <span style="font-size:18px;">Go to Valuations Fee Difference</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                
                
                    
                
            </div>
        </div>

    </section>


    
</div>



