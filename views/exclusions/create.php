<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Exclusions */

$this->title = Yii::t('app', 'Exclusions');
$cardTitle = Yii::t('app', 'New Exclusions');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle
?>
<div class="exclusions-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
