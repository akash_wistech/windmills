<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Exclusions */

$this->title = 'Update Exclusions: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Exclusions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$this->title = Yii::t('app', 'Exclusions');
$cardTitle = Yii::t('app', 'Update Exclusions:  {nameAttribute}');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="exclusions-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
