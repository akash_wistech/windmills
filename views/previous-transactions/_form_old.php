<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PreviousTransactions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="previous-transactions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'instruction_date')->textInput() ?>

    <?= $form->field($model, 'inspection_date')->textInput() ?>

    <?= $form->field($model, 'approver')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fee')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'target_date')->textInput() ?>

    <?= $form->field($model, 'due_date')->textInput() ?>

    <?= $form->field($model, 'date_submitted')->textInput() ?>



    <?= $form->field($model, 'client_reference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'client_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'client_customer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'property_category')->textInput() ?>

    <?= $form->field($model, 'property_id')->textInput() ?>

    <?= $form->field($model, 'tenure')->textInput() ?>

    <?= $form->field($model, 'plot_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unit_number')->textInput() ?>

    <?= $form->field($model, 'building_info')->textInput() ?>

    <?= $form->field($model, 'valuation_type')->textInput() ?>

    <?= $form->field($model, 'valuation_method')->textInput() ?>

    <?= $form->field($model, 'valuer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'taqyimee_number')->textInput() ?>

    <?= $form->field($model, 'location')->textInput() ?>

    <?= $form->field($model, 'view')->textInput() ?>

    <?= $form->field($model, 'property_condition')->textInput() ?>

    <?= $form->field($model, 'property_placement')->textInput() ?>

    <?= $form->field($model, 'finished_status')->dropDownList([ 'Shell & Core' => 'Shell & Core', 'Fitted' => 'Fitted', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'property_exposure')->textInput() ?>

    <?= $form->field($model, 'floor_number')->textInput() ?>

    <?= $form->field($model, 'no_of_bedrooms')->textInput() ?>

    <?= $form->field($model, 'land_size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'market_value_of_land')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estimated_age')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transaction_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'market_value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'psf')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'purpose_of_valuation')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'trashed')->textInput() ?>

    <?= $form->field($model, 'trashed_at')->textInput() ?>

    <?= $form->field($model, 'trashed_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
