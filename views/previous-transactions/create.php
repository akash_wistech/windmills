<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PreviousTransactions */

$this->title = Yii::t('app', 'Previous Transactions');
$cardTitle = Yii::t('app', 'New Previous Transaction');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;

?>
<div class="previous-transactions-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
