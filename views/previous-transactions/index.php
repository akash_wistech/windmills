<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PreviousTransactionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Previous Transactions');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;
if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('view')) {
    $actionBtns .= '{view}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('status')) {
    $actionBtns .= '{status}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('import')) {
    $import = true;
}


?>
<style>
    .btn-success{
        margin-left: 5px;
    }
</style>
<div class="Previous-transactions-index">


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'import' => $import,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'instruction_date',
                'label' => Yii::t('app', 'Instruction Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->instruction_date));
                },
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->instruction_date));
                },
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Building'),
                'value' => function ($model) {
                    return $model->building->title;
                },
                'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'community',
                'label' => Yii::t('app', 'Community'),
                'value' => function ($model) {
                    return $model->communities->title;
                },
                'filter' => ArrayHelper::map(\app\models\Communities::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'sub_community',
                'label' => Yii::t('app', 'Sub Community'),
                'value' => function ($model) {
                    return $model->subCommunities->title;
                },
                'filter' => ArrayHelper::map(\app\models\SubCommunities::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'no_of_bedrooms', 'label' => Yii::t('app', 'No. of Rooms')],
            ['attribute' => 'land_size', 'label' => Yii::t('app', 'Land Size')],
            ['attribute' => 'built_up_area', 'label' => Yii::t('app', 'BUA')],
            ['attribute' => 'psf', 'label' => Yii::t('app', 'Price / sf')],
            [
                'attribute' => 'valuer',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->valuation->service_officer_name) && ($model->valuation->service_officer_name <> null))? ($model->valuation->approver->firstname.' '.$model->valuation->approver->lastname): '';
                },
                /* 'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
                     'firstname' => SORT_ASC,
                 ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];})*/
            ],
            /*['attribute' => 'valuer', 'label' => Yii::t('app', 'Valuer')],*/
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
                'contentOptions' => ['class' => 'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              ' . $actionBtns . '
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'status' => function ($url, $model) {
                        if ($model['status'] == 1) {
                            return Html::a('<i class="fas fa-eye-slash"></i> ' . Yii::t('app', 'Disable'), $url, [
                                'title' => Yii::t('app', 'Disable'),
                                'class' => 'dropdown-item text-1',
                                'data-confirm' => Yii::t('app', 'Are you sure you want to disable this item?'),
                                'data-method' => "post",
                            ]);
                        } else {
                            return Html::a('<i class="fas fa-eye"></i> ' . Yii::t('app', 'Enable'), $url, [
                                'title' => Yii::t('app', 'Enable'),
                                'class' => 'dropdown-item text-1',
                                'data-confirm' => Yii::t('app', 'Are you sure you want to enable this item?'),
                                'data-method' => "post",
                            ]);
                        }
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> ' . Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'dropdown-item text-1',
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'data-method' => "post",
                            'data-pjax' => "0",
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>

