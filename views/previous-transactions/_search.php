<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PreviousTransactionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="previous-transactions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'instruction_date') ?>

    <?= $form->field($model, 'inspection_date') ?>

    <?= $form->field($model, 'approver') ?>

    <?= $form->field($model, 'fee') ?>

    <?php // echo $form->field($model, 'target_date') ?>

    <?php // echo $form->field($model, 'due_date') ?>

    <?php // echo $form->field($model, 'date_submitted') ?>

    <?php // echo $form->field($model, 'reference_number') ?>

    <?php // echo $form->field($model, 'client_reference') ?>

    <?php // echo $form->field($model, 'client_name') ?>

    <?php // echo $form->field($model, 'client_type') ?>

    <?php // echo $form->field($model, 'client_customer_name') ?>

    <?php // echo $form->field($model, 'contact_person') ?>

    <?php // echo $form->field($model, 'designation') ?>

    <?php // echo $form->field($model, 'contact_number') ?>

    <?php // echo $form->field($model, 'property_category') ?>

    <?php // echo $form->field($model, 'property_id') ?>

    <?php // echo $form->field($model, 'tenure') ?>

    <?php // echo $form->field($model, 'plot_number') ?>

    <?php // echo $form->field($model, 'unit_number') ?>

    <?php // echo $form->field($model, 'building_info') ?>

    <?php // echo $form->field($model, 'valuation_type') ?>

    <?php // echo $form->field($model, 'valuation_method') ?>

    <?php // echo $form->field($model, 'valuer') ?>

    <?php // echo $form->field($model, 'taqyimee_number') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'view') ?>

    <?php // echo $form->field($model, 'property_condition') ?>

    <?php // echo $form->field($model, 'property_placement') ?>

    <?php // echo $form->field($model, 'finished_status') ?>

    <?php // echo $form->field($model, 'property_exposure') ?>

    <?php // echo $form->field($model, 'floor_number') ?>

    <?php // echo $form->field($model, 'no_of_bedrooms') ?>

    <?php // echo $form->field($model, 'land_size') ?>

    <?php // echo $form->field($model, 'market_value_of_land') ?>

    <?php // echo $form->field($model, 'built_up_area') ?>

    <?php // echo $form->field($model, 'estimated_age') ?>

    <?php // echo $form->field($model, 'transaction_price') ?>

    <?php // echo $form->field($model, 'market_value') ?>

    <?php // echo $form->field($model, 'psf') ?>

    <?php // echo $form->field($model, 'purpose_of_valuation') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'trashed') ?>

    <?php // echo $form->field($model, 'trashed_at') ?>

    <?php // echo $form->field($model, 'trashed_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
