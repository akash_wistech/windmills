<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\PreviousTransactions */

$this->title = Yii::t('app', 'Previous Transactions');
$cardTitle = Yii::t('app', 'View Previous Transaction:  {nameAttribute}', [
    'nameAttribute' => $model->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>

<div class="previous-transactions-view">
    <section class="card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title"><?= $model->reference_number ?></h2>
            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) { ?>
                <div class="card-tools">
                    <a href="<?= Url::to(['update', 'id' => $model['id']]) ?>" class="btn btn-tool">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            <?php } ?>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Reference No:') . '</strong> ' . $model->reference_number; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Instruction Date:') . '</strong> ' . ($model->instruction_date <> null) ? date('m-d-Y', strtotime($model->instruction_date )) : ''; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Inspection Date:') . '</strong> ' . ($model->inspection_date <> null) ? date('m-d-Y', strtotime($model->inspection_date )) : ''; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Inspector:') . '</strong> ' . $model->inspector; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Approver:') . '</strong> ' . $model->approver; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'fee:') . '</strong> ' . $model->fee; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Target Date:') . '</strong> ' . ($model->target_date <> null) ? date('m-d-Y', strtotime($model->target_date )) : ''; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Due Date:') . '</strong> ' . ($model->due_date <> null) ? date('m-d-Y', strtotime($model->due_date )) : ''; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Submitted Date:') . '</strong> ' . ($model->date_submitted <> null) ? date('m-d-Y', strtotime($model->date_submitted )) : ''; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Purpose Of Valuation:') . '</strong> ' . Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation]; ?>
                                </div>

                            </div>
                            <section class="card card-outline card-info mt-5">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Client Information') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Client Reference:') . '</strong> ' . $model->client_reference; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Client Name:') . '</strong> ' . $model->client_name; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Client Type:') . '</strong> ' . $model->client_type; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Client Customer Name:') . '</strong> ' . $model->client_customer_name; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Contact Person:') . '</strong> ' . $model->contact_person; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Designation:') . '</strong> ' . $model->designation; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Contact Number:') . '</strong> ' . $model->contact_number; ?>
                                        </div>

                                    </div>
                                </div>
                            </section>
                            <section class="card card-outline card-info mt-5">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'General Information') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Building Info:') . '</strong> ' . $model->building->title; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Property:') . '</strong> ' .$model->property->title; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Tenure:') . '</strong> ' . Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure]; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Plot Number:') . '</strong> ' . $model->plot_number; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Unit Number:') . '</strong> ' . $model->unit_number; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Valuation Type:') . '</strong> ' . Yii::$app->appHelperFunctions->typesOfValuationArr[$model->valuation_type]; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Valuation Method:') . '</strong> ' . Yii::$app->appHelperFunctions->methodsOfValuationArr[$model->valuation_method]; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Valuer:') . '</strong> ' .(isset($model->valuation->service_officer_name) && ($model->valuation->service_officer_name <> null))? ($model->valuation->approver->firstname.' '.$model->valuation->approver->lastname): ''; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Taqyimee Number:') . '</strong> ' . $model->taqyimee_number; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'property_condition:') . '</strong> ' . Yii::$app->appHelperFunctions->propertyConditionListArrMaster[$model->property_condition]; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Upgrade:') . '</strong> ' . $model->valuation->valuationConfiguration->over_all_upgrade; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'property_placement:') . '</strong> ' . Yii::$app->appHelperFunctions->propertyPlacementListArr[$model->property_placement]; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'property_exposure:') . '</strong> ' . Yii::$app->appHelperFunctions->propertyExposureListArr[$model->property_exposure]; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Finished Status:') . '</strong> ' . $model->finished_status; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Floor Number:') . '</strong> ' . $model->floor_number; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'No of Bedrooms:') . '</strong> ' . $model->no_of_bedrooms; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Land Size:') . '</strong> ' . $model->land_size; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Market Value Of Land:') . '</strong> ' . $model->market_value_of_land; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Built Up Area:') . '</strong> ' . $model->built_up_area; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Estimated Age:') . '</strong> ' . $model->estimated_age; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Transaction Price:') . '</strong> ' . $model->estimated_age; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Market Value:') . '</strong> ' . $model->market_value; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'PSF:') . '</strong> ' . $model->psf; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'No. of levels:') . '</strong> ' . Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->number_of_levels]; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Parking Space:') . '</strong> ' . $model->parking_space; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Pool:') . '</strong> ' . $model->pool; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'White Goods:') . '</strong> ' . $model->white_goods; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Utilities Connected:') . '</strong> ' . $model->utilities_connected; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Developer Margin:') . '</strong> ' . $model->developer_margin; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Balcony Size:') . '</strong> ' . $model->balcony_size; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= '<strong>' . Yii::t('app', 'Landscaping:') . '</strong> ' . $model->landscaping; ?>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="card card-outline card-success">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Location Information') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Drive to Highway/Main Road and metro:') . '</strong> ' . Yii::$app->appHelperFunctions->locationAttributesValue[$model->location_highway_drive]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Drive to school:') . '</strong> ' . Yii::$app->appHelperFunctions->locationAttributesValue[$model->location_school_drive]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Drive to commercial area/Mall:') . '</strong> ' . Yii::$app->appHelperFunctions->locationAttributesValue[$model->location_mall_drive]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Drive to special landmark, sea, marina:') . '</strong> ' . Yii::$app->appHelperFunctions->locationAttributesValue[$model->location_sea_drive]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Drive to pool and park:') . '</strong> ' . Yii::$app->appHelperFunctions->locationAttributesValue[$model->location_park_drive]; ?>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="card card-outline card-success">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'View Information') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Community:') . '</strong> ' . Yii::$app->appHelperFunctions->viewCommunityAttributesValue[$model->view_community]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Pool:') . '</strong> ' . Yii::$app->appHelperFunctions->viewAttributesValue[$model->view_pool]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Burj:') . '</strong> ' . Yii::$app->appHelperFunctions->viewAttributesValue[$model->view_burj]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Sea:') . '</strong> ' . Yii::$app->appHelperFunctions->viewAttributesValue[$model->view_sea]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Marina:') . '</strong> ' . Yii::$app->appHelperFunctions->viewAttributesValue[$model->view_marina]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Mountains:') . '</strong> ' . Yii::$app->appHelperFunctions->viewAttributesValue[$model->view_mountains]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Lake:') . '</strong> ' . Yii::$app->appHelperFunctions->viewAttributesValue[$model->view_lake]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Golf Course:') . '</strong> ' . Yii::$app->appHelperFunctions->viewAttributesValue[$model->view_golf_course]; ?>
                                        </div>



                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Small Park:') . '</strong> ' . Yii::$app->appHelperFunctions->viewAttributesValue[$model->view_park]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Special view:') . '</strong> ' . Yii::$app->appHelperFunctions->viewAttributesValue[$model->view_special]; ?>
                                        </div>
                                    </div>
                                </div>
                            </section>


                        </div>
                </div>
    </section>
</div>

