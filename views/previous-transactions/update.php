<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PreviousTransactions */


$this->title = Yii::t('app', 'Previous Transactions');
$cardTitle = Yii::t('app', 'Update Previous Transaction:  {nameAttribute}', [
    'nameAttribute' => $model->building->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="previous-transactions-update">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
