<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\PreviousFormAsset;

PreviousFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\previoustransactions */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('

$("#previoustransactions-instruction_date,#previoustransactions-inspection_date,#previoustransactions-target_date,#previoustransactions-due_date,#previoustransactions-date_submitted").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');

?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
</style>

<section class="listings-transactions-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle ?></h2>
    </header>
    <div class="card-body">


        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'reference_number')->textInput(['maxlength' => true]) ?>
            </div>
            

            <div class="col-sm-4">
                <?= $form->field($model, 'instruction_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="previoustransactions-instruction_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#previoustransactions-instruction_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'inspection_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="previoustransactions-inspection_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#previoustransactions-inspection_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>





            <div class="col-sm-4">
                <?= $form->field($model, 'inspector')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'approver')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'fee')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'target_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="previoustransactions-target_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#previoustransactions-target_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'due_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="previoustransactions-due_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#previoustransactions-due_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'date_submitted', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="previoustransactions-date_submitted" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#previoustransactions-date_submitted" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'purpose_of_valuation')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->purposeOfValuationArr,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>



        </div>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Client Information') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_reference')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_type')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_customer_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'contact_number')->textInput(['maxlength' => true]) ?>
                    </div>


                </div>
            </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'General Information') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'building_info')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select a Building ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select a Property Type ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'plot_number')->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'valuation_type')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->typesOfValuationArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'valuation_method')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->methodsOfValuationArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                   <!-- <div class="col-sm-4">
                        <?/*= $form->field($model, 'valuer')->textInput(['maxlength' => true]) */?>
                    </div>-->
                    <div class="col-sm-4">
                        <?= $form->field($model, 'taqyimee_number')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_condition')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertyConditionListArrMaster,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_placement')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertyPlacementListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_exposure')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertyExposureListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'finished_status')->widget(Select2::classname(), [
                            'data' => array('Shell & Core' => 'Shell & Core', 'Fitted' => 'Fitted'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'floor_number')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'no_of_bedrooms')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'land_size')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'market_value_of_land')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'estimated_age')->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'transaction_price')->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'market_value')->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'psf')->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'number_of_levels')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->listingLevelsListArr,
                            'options' => ['placeholder' => 'Select a Level ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'parking_space')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'pool')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'white_goods')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'utilities_connected')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>

                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'developer_margin')->widget(Select2::classname(), [
                        'data' => array('Yes' => 'Yes', 'No' => 'No'),

                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'balcony_size')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'landscaping')->widget(Select2::classname(), [
                        'data' => array('Yes' => 'Yes', 'No' => 'No', 'Semi-Landscape' => 'Semi-Landscape'),
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
                </div>
            </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Location Attributes') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_highway_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_school_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_mall_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_sea_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_park_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>


                </div>

            </div>

        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'View Attributes') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_community')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewCommunityAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_pool')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_burj')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_sea')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_marina')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_mountains')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_lake')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_golf_course')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_park')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_special')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                </div>

            </div>

        </section>








    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>

