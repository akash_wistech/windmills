<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\ClientFormAsset;
ClientFormAsset::register($this);
use  app\components\widgets\StatusVerified;

$this->registerJs('

$("#holidays-date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

');


?>

<section class="contact-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title">Create</h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col">
                <?= $form->field($model, 'name')->textInput([]) ?>
            </div>

            <div class="col">
            <?= $form->field($model, 'date',['template'=>'
                {label}
                <div class="input-group date" id="holidays-date" data-target-input="nearest">
                {input}
                <div class="input-group-append" data-target="#holidays-date" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
                </div>
                {hint}{error}
                '])->textInput(['maxlength' => true])?>
            </div>
        </div>
	        <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>