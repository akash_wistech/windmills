<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Developers */

$this->title = Yii::t('app', 'Developers');
$cardTitle = Yii::t('app','New Developer');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="developers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
