<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Developers */


$this->title = Yii::t('app', 'Developers');
$cardTitle = Yii::t('app','Update Developer:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="developers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
