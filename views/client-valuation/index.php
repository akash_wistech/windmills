<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;

use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);



$this->title = 'Verify Valuation Instruction';
$this->params['breadcrumbs'][] = $this->title;
$cardTitle = Yii::t('app', 'Verify Valuation Instruction');

// dd($dataProvider);s

// if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    // $createBtn = true;
// }
// else { $createBtn = false; }
// if (Yii::$app->menuHelperFunction->checkActionAllowed('view')) {
//     $actionBtns .= '{view}';
// }
// else if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
//     $actionBtns .= '{update}';
// }
// if (Yii::$app->menuHelperFunction->checkActionAllowed('delete')) {
//     $actionBtns .= '{delete}';
// }
$actionBtns .= '{get-history}';

$actionBtns .= '{view}';    
$actionBtns .= '{update}';

?>

<style>
    .card-title {
        font-weight: bold;
    }
    .card-header {
        display: none;
    }
</style>
        

<div class="valuation-index">

    <div class="valuation-search card-body p-0">

      <?php $form = ActiveForm::begin([
          'action' => ['index'],
          'method' => 'get',
      ]); ?>
     
      
      <div class="row">
        
        
        <div class="col-sm-4 text-left">
          <?php
          echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
              'data' => Yii:: $app->appHelperFunctions->reportPeriod,
              'options' => ['placeholder' => 'Time Frame ...'],
              'pluginOptions' => [
                'allowClear' => true
              ],
              ])->label(false);
              ?>
        </div>
        <div class="col-sm-4" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
          <div class="" id="end_date_id">
          
            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>

          </div>
        </div>
        <div class="col-sm-4">
          <div class="text-left">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            </div>
          </div>
        </div>
      </div>
      
        <?php ActiveForm::end(); ?>
    </div>

    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'cardTitle' => false,
        'showFooter' => true,
        'options' => [
            'id' => 'sortable-table',
        ],
        // 'createBtn' => $createBtn,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
           [
                'attribute' => 'created_at',
                'label' => Yii::t('app', 'Created Date'),
                'value' => function ($model) {
                    return date('d-M-Y', strtotime($model->created_at)); // Use correct attribute name
                },
                'filter' => '<input type="date" class="form-control" name="ClientValuationSearch[created_at]" placeholder="Date">',
                'enableSorting' => true, 
            ],
            [
                'attribute' => 'client_reference', 
                'label' => Yii::t('app', 'Reference'),
                'value' => function ($model) {
                    return ($model->client_reference <> null) ? $model->client_reference : null;
                },
                'filter' => '<input type="text" class="form-control" name="ClientValuationSearch[client_reference]" >',
                'enableSorting' => true, 
            ],
            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title;
                },
                'filter' => '<input type="text" class="form-control" name="ClientValuationSearch[property_id]" >',
                // 'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                //     'title' => SORT_ASC,
                // ])->all(), 'id', 'title'),
                'enableSorting' => true, 
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Building'),
                'value' => function ($model) {
                    return $model->building->title;
                },
                'filter' => '<input type="text" class="form-control" name="ClientValuationSearch[building_info]" >',
                // 'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                //     'title' => SORT_ASC,
                // ])->all(), 'id', 'title'),
                'enableSorting' => true, 
            ],
            ['attribute' => 'community',
                'label' => Yii::t('app', 'Community'),
                'value' => function ($model) {
                    return ($model->building_info <> null) ? $model->building->communities->title : $model->communities->title;
                },
                'filter' => '<input type="text" class="form-control" name="ClientValuationSearch[community]" >',   
                'enableSorting' => true, 
                // 'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                //     'title' => SORT_ASC,
                // ])->all(), 'id', 'community')
            ],
            ['attribute' => 'sub_community',
                'label' => Yii::t('app', 'Sub Community'),
                'value' => function ($model) {
                    return ($model->building_info <> null) ? $model->building->subCommunities->title : $model->subCommunities->title;
                },
                'filter' => '<input type="text" class="form-control" name="ClientValuationSearch[community]" >',   
                'enableSorting' => true, 
                // 'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                //     'title' => SORT_ASC,
                // ])->all(), 'id', 'community')
            ],
            ['attribute' => 'city',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    return ($model->building_info <> null) ? Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] : $model->city;
                },
                'filter' => Yii::$app->appHelperFunctions->emiratedListArr,
                'enableSorting' => true, 
            ],
            // ['attribute' => 'contact_person_fname',
            //     'label' => Yii::t('app', 'Contact Person'),
            //     'value' => function ($model) {
            //         return $model->contact_person_fname.' '.$model->contact_person_lname;
            //     },
            //     'enableSorting' => true, 
            // ],
            // ['attribute' => 'contact_person_phone',
            //     'label' => Yii::t('app', 'Contact Phone'),
            //     'value' => function ($model) {
            //         return $model->contact_person_phone_code.$model->contact_person_phone;
            //     },
            //     'enableSorting' => true, 
            // ],
            // ['attribute' => 'contact_person_landline',
            //     'label' => Yii::t('app', 'Contact Landline'),
            //     'value' => function ($model) {
            //         return $model->contact_person_landline_code.$model->contact_person_landline;
            //     },
            //     'enableSorting' => true, 
            // ],
            // ['attribute' => 'valuation_date',
            //     'label' => Yii::t('app', 'Valuation Date'),
            //     'value' => function ($model) {
            //         return ($model->valuation_date <> null) ? date('d-M-Y', strtotime($model->valuation_date)) : null;
            //     },
            //     'filter' => '<input type="date" class="form-control" name="ClientValuationSearch[valuation_date]" placeholder="Select Valuation Date">',
            //     'enableSorting' => true, 
            // ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'status_verified',
                'label' => Yii::t('app', 'Verification'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->statusVerifyLabel[$model->status_verified];
                },
                'filter' => Yii::$app->appHelperFunctions->statusVerifyArr,
                'enableSorting' => true, 
            ],

 

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
                'contentOptions' => ['class' => 'noprint actions'],
                'template' => '
                    <div class="btn-group flex-wrap">
                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">
                        ' . $actionBtns . '
                        </div>
  				    </div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        if ($model->status_verified == 1) {
                        return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]); }
                    },

                    'update' => function ($url, $model) {
                        if ($model->status_verified != 1) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);}
                    },
                ],
            ],

            // [
            //     'class' => 'yii\grid\ActionColumn',
            //     'header' => 'Action',
            //     'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
            //     'contentOptions' => ['class' => 'noprint actions'],
            //     'template' => '
            //         <div class="btn-group flex-wrap">
            //             <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
            //                 <span class="caret"></span>
            //             </button>
            //             <div class="dropdown-menu" role="menu">
            //                 ' . $actionBtns . '
            //             </div>
            //         </div>',
            //         'buttons' => [
            //             'view' => function ($url, $model) {
            //                 if ($model->status_verified == 1) {
            //                     return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
            //                         'title' => Yii::t('app', 'View'),
            //                         'class' => 'dropdown-item text-1',
            //                         'data-pjax' => "0",
            //                     ]);
            //                 } else {
            //                     $updateUrl = Url::to(['update', 'id' => $model->id]); // Modify the URL here
            //                     return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Update'), $updateUrl, [
            //                         'title' => Yii::t('app', 'Update'),
            //                         'class' => 'dropdown-item text-1',
            //                         'data-pjax' => "0",
            //                     ]);
            //                 }
            //             },
            //         ],
            // ],
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>

        


<?php
$this->registerJs('
  




    // $(".sortable").click(function () {
    //     var index = $(this).index();
    //     var rows = $("#sortable-table tbody tr").get();
    //     rows.sort(function (a, b) {
    //         var aValue = $(a).children("td").eq(index).text();
    //         var bValue = $(b).children("td").eq(index).text();

    //     //   return $.isNumeric(aValue) && $.isNumeric(bValue) ?
    //     //   aValue - bValue :
    //     //   aValue.localeCompare(bValue);

    //         return $.isNumeric(bValue) && $.isNumeric(aValue) ?
    //         bValue - aValue :
    //         bValue.localeCompare(aValue);
    //     });

    //     $.each(rows, function (index, row) {
    //         $("#sortable-table tbody").append(row);
    //     });
    // });

    // $( document ).ready(function() {
    //     var index = $(this).index();
    //     var rows = $("#sortable-table tbody tr").get();
    //     rows.sort(function (a, b) {
    //         var aValue = $(a).children("td").eq(index).text();
    //         var bValue = $(b).children("td").eq(index).text();

    //     //   return $.isNumeric(aValue) && $.isNumeric(bValue) ?
    //     //   aValue - bValue :
    //     //   aValue.localeCompare(bValue);

    //         return $.isNumeric(bValue) && $.isNumeric(aValue) ?
    //         bValue - aValue :
    //         bValue.localeCompare(aValue);
    //     });

    //     $.each(rows, function (index, row) {
    //         $("#sortable-table tbody").append(row);
    //     });
    // });


      $(".div1").daterangepicker({
        autoUpdateInput: false,
          locale: {
            format: "YYYY-MM-DD"
          }
      });
     
      $(".div1").on("apply.daterangepicker", function(ev, picker) {
         $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
      });
      $(document).ready(function() {
          // Code to execute when the document is ready
          var period = $("#clientvaluationsearch-time_period").val();
          
          if (period == 9) {
              $(".div1").prop("required", true);
              $("#date_range_array").show();
          } else {
              $(".div1").prop("required", false);
              $("#date_range_array").hide();
          }
          
          // Change event handler for select input
          $("#clientvaluationsearch-time_period").on("change", function() {
              var period = $(this).val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $(".div1").val("");
                  $("#date_range_array").hide();
              }
          });
      });




');
?>

