<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ValuationFormAsset;
use app\components\widgets\StatusVerified;

ValuationFormAsset::register($this);
use kartik\depdrop\DepDrop;


$client_type = Yii::$app->user->identity->company->client_type;
// dd($client_type);


$show_number_of_units = array_merge(array(3,10,42,81));
$show_unit_number = array_merge(array(1,2,6,17,28));
$show_floor_number = array_merge(array(1,17,28));
$show_extended = array_merge(array(1,2,6,10,17,28));


$this->registerJs('

    $("#clientvaluation_valuation_date").datetimepicker({
        allowInputToggle: true,
        // viewMode: "months",
        format: "DD-MMM-YYYY",
        useCurrent: false,
    });

    $("#clientvaluation_inspection_date").datetimepicker({
        allowInputToggle: true,
        format: "DD-MMM-YYYY", // Use "hh:mm A" for 12-hour time format
        useCurrent: false, // Do not default to the current date/time
        showClear: true, // Show a clear button
        showTodayButton: true, // Show a "Today" button
        minDate: moment().startOf("day"), // Set the minimum date to today
        icons: {
            time: "fas fa-clock", // Use Font Awesome icons for time
            date: "fas fa-calendar", // Use Font Awesome icons for date
            up: "fas fa-chevron-up", // Use Font Awesome icons for up arrow
            down: "fas fa-chevron-down", // Use Font Awesome icons for down arrow
            previous: "fas fa-chevron-left", // Use Font Awesome icons for previous button
            next: "fas fa-chevron-right", // Use Font Awesome icons for next button
            today: "fas fa-calendar-day", // Use Font Awesome icons for "Today" button
            clear: "fas fa-trash-alt", // Use Font Awesome icons for "Clear" button
            close: "fas fa-times" // Use Font Awesome icons for close button
        } 
    });

    $("#clientvaluation-inspection_time").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "HH:mm",
        showClear: true, // Show a clear button
        showTodayButton: true, // Show a "Today" button
    });
    

    $(document).on("click", function (e) {
        if ($(e.target).closest("#clientvaluation_valuation_date,#clientvaluation_inspection_date").length === 0) {
            $("#clientvaluation_valuation_date,#clientvaluation_inspection_date").datetimepicker("hide");
        }
    });

    $("body").on("click", ".delete-file", function (e) {

        id = $(this).attr("id")
        data = {id:id}
    
        swal({
            title: "' . Yii::t('app', 'Are you sure you want to delete file from bucket ?') . '",
            html: "' . Yii::t('app', '') . '",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#47a447",
            confirmButtonText: "' . Yii::t('app', 'Yes, Delete it!') . '",
            cancelButtonText: "' . Yii::t('app', 'Cancel') . '",
        },function(result) {
            if (result) {
                $.ajax({
                    data : data,
                    url: "' . Url::to(['valuation/delete-file']) . '",
                    dataType: "html",
                    type: "POST",
                    success: function(data) {
                        data = JSON.parse(data)
                        if(data.msg == "success"){
    
                            deleted = "#deleted-"+id;
                            $(deleted). attr("src", "' . Yii::$app->params['uploadIcon'] . '");
    
                            removed = ".removed-"+id;
                            $(removed).remove()
    
                            removedDelBtn = "#del-btn-"+id;
                            $(removedDelBtn).remove()
    
                            swal({
                                title: "' . Yii::t('app', 'Successfully Deleted') . '",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#47a447",
                            });
                        }
                    },
                    error: bbAlert
                });
            }
        });
    });


    $(document).ready(function(){

        var key = $("#clientvaluation-key").val(); 
        var propertyId = $("#clientvaluation-property_id").val(); 
        var extended = $("#clientvaluation-extended").val();
        var completionStatus = $("#clientvaluation-completion_status").val();
        var city = $("#clientvaluation-city").val();
        var numberOfUnits = $("#clientvaluation-number_of_units").val();

        $.ajax({
            // url: "' . Yii::$app->urlManager->createUrl('client-valuation/clientvaluation-documents?id=') .$model->id. '",
            url: "' . Yii::$app->urlManager->createUrl('client-valuation/clientvaluation-documents') . '",
            method: "POST",
            data: {propertyId: propertyId, key: key, extended: extended, completionStatus: completionStatus, city: city, numberOfUnits: numberOfUnits}, 
            success: function(response) {
                $("#property-documents-container").html(response);
            }
        });

        $("#clientvaluation-property_id").change(function(){
            var propertyId = $(this).val(); 
            var extended = $("#clientvaluation-extended").val();
            var key = $("#clientvaluation-key").val(); 
            var completionStatus = $("#clientvaluation-completion_status").val();
            var city = $("#clientvaluation-city").val();
            var numberOfUnits = $("#clientvaluation-number_of_units").val();
            
            $.ajax({
                // url: "client-valuation/update-variable", 
                // url: "' . Yii::$app->urlManager->createUrl('client-valuation/clientvaluation-documents?id=') .$model->id. '",
                url: "' . Yii::$app->urlManager->createUrl('client-valuation/clientvaluation-documents') . '",
                method: "POST",
                data: {propertyId: propertyId, key: key, extended: extended, completionStatus: completionStatus, city: city, numberOfUnits: numberOfUnits}, 
                success: function(response) {
                    $("#property-documents-container").html(response);
                }
            });
        });

        $("#clientvaluation-extended").change(function(){
            var extended = $(this).val(); 
            var propertyId = $("#clientvaluation-property_id").val(); 
            var key = $("#clientvaluation-key").val(); 
            var completionStatus = $("#clientvaluation-completion_status").val();
            var city = $("#clientvaluation-city").val();
            var numberOfUnits = $("#clientvaluation-number_of_units").val();
            
            $.ajax({
                // url: "client-valuation/update-variable", 
                // url: "' . Yii::$app->urlManager->createUrl('client-valuation/clientvaluation-documents?id=') .$model->id. '",
                url: "' . Yii::$app->urlManager->createUrl('client-valuation/clientvaluation-documents') . '",
                method: "POST",
                data: {propertyId: propertyId, key: key, extended: extended, completionStatus: completionStatus, city: city, numberOfUnits: numberOfUnits}, 
                success: function(response) {
                    $("#property-documents-container").html(response);
                }
            });
        });

        $("#clientvaluation-completion_status").change(function(){
            var completionStatus = $(this).val(); 
            var propertyId = $("#clientvaluation-property_id").val(); 
            var extended = $("#clientvaluation-extended").val();
            var key = $("#clientvaluation-key").val(); 
            var city = $("#clientvaluation-city").val();
            var numberOfUnits = $("#clientvaluation-number_of_units").val();
            
            $.ajax({
                // url: "client-valuation/update-variable", 
                // url: "' . Yii::$app->urlManager->createUrl('client-valuation/clientvaluation-documents?id=') .$model->id. '",
                url: "' . Yii::$app->urlManager->createUrl('client-valuation/clientvaluation-documents') . '",
                method: "POST",
                data: {propertyId: propertyId, key: key, extended: extended, completionStatus: completionStatus, city: city, numberOfUnits: numberOfUnits}, 
                success: function(response) {
                    $("#property-documents-container").html(response);
                }
            });
        });

        $("#clientvaluation-number_of_units").change(function(){
            var numberOfUnits = $(this).val();
            var completionStatus = $("#clientvaluation-completion_status").val();
            var propertyId = $("#clientvaluation-property_id").val(); 
            var extended = $("#clientvaluation-extended").val();
            var key = $("#clientvaluation-key").val(); 
            var city = $("#clientvaluation-city").val();
            
            $.ajax({
                // url: "client-valuation/update-variable", 
                // url: "' . Yii::$app->urlManager->createUrl('client-valuation/clientvaluation-documents?id=') .$model->id. '",
                url: "' . Yii::$app->urlManager->createUrl('client-valuation/clientvaluation-documents') . '",
                method: "POST",
                data: {propertyId: propertyId, key: key, extended: extended, completionStatus: completionStatus, city: city, numberOfUnits: numberOfUnits}, 
                success: function(response) {
                    $("#property-documents-container").html(response);
                }
            });
        });

    });


    $(document).ready(function(){
        var $bua_value = $("#clientvaluation-built_up_area_value");
        var $bua_unit = $("#clientvaluation-built_up_area_unit");
        var $bua_auto = $("#clientvaluation-built_up_area");
        var $nla_value = $("#clientvaluation-net_leasable_area_value");
        var $nla_unit = $("#clientvaluation-net_leasable_area_unit");
        var $nla_auto = $("#clientvaluation-net_leasable_area");
        var $plot_value = $("#clientvaluation-plot_area_value");
        var $plot_unit = $("#clientvaluation-plot_area_unit");
        var $plot_auto = $("#clientvaluation-plot_area");

        convertSqMtrToSqFt($bua_value,$bua_unit,$bua_auto);
        convertSqMtrToSqFt($nla_value,$nla_unit,$nla_auto);
        convertSqMtrToSqFt($plot_value,$plot_unit,$plot_auto);

        function convertSqMtrToSqFt($field_value,$field_unit,$field_auto){

            $field_value.on("input", function () {
                var field_value = $field_value.val().replace(",", ".");
                if (field_value !== "") {
                    if ($field_unit.val() == 2) {
                        var sq_meter = parseFloat(field_value);
                        if (!isNaN(sq_meter)) {
                            var sq_feet = (sq_meter * 10.763915).toFixed(2); 
                            $field_auto.val(sq_feet);
                        }
                    } else {
                        $field_auto.val(field_value);
                    }
                } else {
                    $field_auto.val("");
                }
            });
        
            $field_unit.on("change", function () {
                var field_value = $field_value.val().replace(",", ".");
                if (field_value !== "") {
                    if ($field_unit.val() == 1) {
                        $field_auto.val(field_value);
                    } else {
                        var sq_meter = parseFloat(field_value);
                        if (!isNaN(sq_meter)) {
                            var sq_feet = (sq_meter * 10.763915).toFixed(2); 
                            $field_auto.val(sq_feet);
                        }
                    }
                } else {
                    $field_auto.val("");
                }
            });
        }

    });

    $(document).ready(function(){

        var community_id = $("#clientvaluation-community").val();
        var community_url = "communities/communitydetail/"+community_id;

        // communitySelection(community_id,community_url);

        $("#clientvaluation-community").on("change", function () {

            var community_id = $(this).val();
            var community_url = "communities/communitydetail/"+community_id;
             
            communitySelection(community_id,community_url);            
        });

        function communitySelection(community_id,community_url){
            heading=$(this).data("heading");
            if (community_id) {
                $.ajax({
                    url: community_url, 
                    success: function(data){
                        var response = JSON.parse(data);
                        // Populate fields with received data
                        
                        $("#clientvaluation-building_info").html(response.buildingOptions);
                        $("#clientvaluation-sub_community").html(response.subCommunityOptions);
                        $("#clientvaluation-city").val(response.city);
                        $("#clientvaluation-country").val(response.country);
                    },
                    error: function(xhr, status, error) {
                        console.error("AJAX error:", error);
                    }
                });
            } else {
                // Clear fields if no community selected
                $("#clientvaluation-building_info").html("");
                $("#clientvaluation-sub_community").html("");
                $("#clientvaluation-city").val("");
                $("#clientvaluation-country").val("");
            }
        }

    });


    


    


    $(document).ready(function () {
        var $building_info = $("#clientvaluation-building_info");
        var $property_id = $("#clientvaluation-property_id");
    
        function toggleFieldsVisibilityByPropertyType(propertyTypeArray, className) {
            if (propertyTypeArray.includes(parseInt($property_id.val()))) {
                $("." + className).show();
            } else {
                $("." + className).hide();
            }
        }
    
        var showFieldUnitNumber= ' . json_encode($show_unit_number) . ';    
        var showFieldFloorNumber= ' . json_encode($show_floor_number) . ';    
        var showFieldNumberOfUnits= ' . json_encode($show_number_of_units) . ';
        var showFieldExtended= ' . json_encode($show_extended) . ';

        function toggleFunctions(){   
            toggleFieldsVisibilityByPropertyType(showFieldUnitNumber, "unit_number");
            toggleFieldsVisibilityByPropertyType(showFieldFloorNumber, "floor_number");
            toggleFieldsVisibilityByPropertyType(showFieldNumberOfUnits, "number_of_units");
            toggleFieldsVisibilityByPropertyType(showFieldExtended, "extended");
        }
    
        $building_info.on("change", function () {
            toggleFunctions();
        });
    
        $property_id.on("change", function () {
            toggleFunctions();
        });
    
        if($building_info.val() != ""){ 
            toggleFunctions();
        }

    
    }); 

    
    // $("#w0").on("submit", function (e) {
    //     e.preventDefault();
        
    //     var flag = 0;
    //     var mandatory_doc = $(".mandatory-doc");
    //     for(var i = 0; i < mandatory_doc.length; i++){
    //         var id = mandatory_doc.eq(i).attr("id");
    //         if($("#"+id).val() == ""){
    //             $("#image-row" + i + " .help-block2").html("This document cannot be blank").show();
    //             flag += 0;
    //         }else{
    //             $("#image-row" + i + " .help-block2").html("").hide();
    //             flag += 1;
    //         }
    //     }
    //     if(flag == mandatory_doc.length){
    //         this.submit(); // Directly submit the form
    //     }
    // });
    


');
$image_row = 0;

?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css"
      integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg=="
      crossorigin="anonymous" referrerpolicy="no-referrer"/>


<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }

    
    
    .phone-code {
        width:74px !important;
        border-radius: 4px !important;
    }
    .phone-number {
        border-radius: 4px !important;
    }
    .phone .phone-parent { display: flex;}
    .phone .input-group-prepend {
        flex: 1; 
        
    }
    .phone .form-group  {
        flex: 5; 
        
    }
    .help-block2 {
        color: #a94442;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -6px;
        font-family: inherit;
    }
    .navbar { margin-bottom: 0;}
    .navbar-nav > li > a { padding-top: 8px; }
    .client-valuation .has-success .control-label{ color: inherit;}
    .client-valuation .has-success .form-control { border: 1px solid #ccc;}   
    .client-valuation .has-success.select2-container--krajee-bs3 .select2-dropdown, .client-valuation .has-success .select2-container--krajee-bs3 .select2-selection{ border: 1px solid #ccc;}   
    .bootstrap-datetimepicker-widget.dropdown-menu { width: 76%;}
    .bootstrap-datetimepicker-widget table td.active, .bootstrap-datetimepicker-widget table td.active:hover {background-color: #007bff !important;}
    .content-header h1 {
        font-size: 1.8rem;
        margin: 0;
        
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
    }
    body {font-family: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";}
</style>

<section class="sales-and-marketing-purpose-form card card-outline card-primary client-valuation">
    <!-- <header class="card-header">
        <h2 class="card-title"><strong><?= $this->title; ?></strong></h2>
    </header> -->
    <?php $form = ActiveForm::begin(); ?>

    <?php 
    $key = '';
    for ($i = 0; $i < 10; $i++) {
        $key .= rand(0, 9);
    }
    
    ?>
    <?php echo $form->field($model, 'key')->hiddenInput(['maxlength' => true, 'value' => ($model->key <> null) ? $model->key : $key ])->label(false) ?>
   

   
    <div class="card-body">

    
    <div class="card card-outline card-primary">
        <header class="card-header">
                <h2 class="card-title">General Details</h2>
        </header>
        <div class="card-body">

            <div class="row">
                <div class="col-sm-4">
                    <?php 
                    if(Yii::$app->user->identity->company_id == 119846){
                        echo $form->field($model, 'client_reference')->textInput(['maxlength' => true,'placeholder' => 'Enter client reference']);
                    }else{
                        echo $form->field($model, 'client_reference')->textInput(['maxlength' => true,'placeholder' => 'Client Reference','readonly' => true , 'value' => ($model->client_reference <> null) ? $model->client_reference : Yii::$app->appHelperFunctions->getUniqueClientValuationReference(Yii::$app->user->identity->company_id)]);
                    }
                    ?>
                    
                </div>

                <div class="col-sm-4">
                    <?php
                    if($model->inspection_type <> null){
                        
                    }else{
                        $model->inspection_type = 2;
                    }
                    echo $form->field($model, 'inspection_type')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->inspectionTypeArr,
                        'options' => ['placeholder' => 'Select a Inspection Type'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'urgency')->widget(Select2::classname(), [
                        'data' => array('0' => 'Normal - 2 working days', '1' => 'Urgent - 1 working day'),
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-sm-4">
                    <?php
                     if($model->purpose_of_valuation <> null){
                        
                    }else{
                        $model->purpose_of_valuation = 6;
                    }
                    echo $form->field($model, 'purpose_of_valuation')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->purposeOfValuationArr,
                        'options' => ['placeholder' => 'Select a Purpose ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-sm-4 valuation_date" id="valuation_date_id">
                    <?php $valuation_date = ($model->valuation_date <> null) ? date('d-M-Y', strtotime($model->valuation_date)) : '' ?>
                    <?= $form->field($model, 'valuation_date', [
                        'template' => '
                    {label}
                    <div class="input-group date" style="display: flex" id="clientvaluation_valuation_date" data-target-input="nearest">
                    {input}
                    <div class="input-group-append" data-target="#clientvaluation_valuation_date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    </div>
                    {hint}{error}
                    '
                    ])->textInput(['maxlength' => true, 'value' => $valuation_date])     ?>
                </div>

                <div class="col-sm-4 inspection_date" id="inspection_date_id">
                    <?php $inspection_date = ($model->inspection_date <> null) ? date('d-M-Y', strtotime($model->inspection_date)) : '' ?>
                    <?= $form->field($model, 'inspection_date', [
                        'template' => '
                    {label}
                    <div class="input-group date" style="display: flex" id="clientvaluation_inspection_date" data-target-input="nearest">
                    {input}
                    <div class="input-group-append" data-target="#clientvaluation_inspection_date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    </div>
                    {hint}{error}
                    '
                    ])->textInput(['maxlength' => true, 'value' => $inspection_date]) ?>
                </div>

                <div class="col-sm-4" id="inspection_time_id">
                    <?php $inspection_time = ($model->inspection_time <> null) ? date('HH:mm', strtotime($model->inspection_time)) : '' ?>
                    <?= $form->field($model, 'inspection_time', ['template' => '
                    {label}
                    <div class="input-group date" style="display: flex" id="clientvaluation_inspection_time" data-target-input="nearest">
                    {input}
                    <div class="input-group-append" data-target="#clientvaluation-inspection_time" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                    </div>
                    </div>
                    {hint}{error}
                    '])->textInput(['maxlength' => true, 'value' => $inspection_time]) ?>
                </div>


            </div>

           

        </div>
        
    </div>

    <div class="card card-outline card-primary">
        <header class="card-header">
                <h2 class="card-title">Customer Details</h2>
        </header>
        <div class="card-body">

            <div class="row">

                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'client_customer_prefix')->widget(\kartik\select2\Select2::classname(), [
                        'data' => Yii::$app->smHelper->getPrefixArr(),
                        'options' => ['placeholder' => 'Select...','value' => "Mr."],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'initialize' => true,
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'client_customer_fname')->textInput(['maxlength' => true,'placeholder' => 'Enter first name']); ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'client_customer_lname')->textInput(['maxlength' => true,'placeholder' => 'Enter last name']); ?>
                </div>

                <div class="col-sm-4 phone">
                    <label class="control-label" for="clientvaluation-client_customer_phone">Client Customer Phone <span class="text-danger"></span></label>
                    <div class="input-group phone-parent">
                        <div class="input-group-prepend">
                            <select name='ClientValuation[client_customer_phone_code]' class="form-control phone-code">
                                <option value="050" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '050')) {
                                    echo 'selected';
                                } ?>>050</option>
                                <option value="052" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '052')) {
                                    echo 'selected';
                                } ?>>052</option>
                                <option value="054" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '054')) {
                                    echo 'selected';
                                } ?>>054</option>
                                <option value="055" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '055')) {
                                    echo 'selected';
                                } ?>>055</option>
                                <option value="056" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '056')) {
                                    echo 'selected';
                                } ?>>056</option>
                                <option value="058" <?php if (isset($model->client_customer_phone_code) && ($model->client_customer_phone_code == '058')) {
                                    echo 'selected';
                                } ?>>058</option>
                            </select>
                        </div>
                        <?= $form->field($model, 'client_customer_phone')->textInput(['maxlength' => true, 'class' => 'form-control phone-number', 'placeholder' => 'Enter phone number'])->label(false) ?>
                    </div>
                </div>
                <div class="col-sm-4 phone">
                    <label class="control-label" for="clientvaluation-client_customer_landline">Client Customer Landline <span class="text-danger"> </span></label>
                    <div class="input-group phone-parent">
                        <div class="input-group-prepend">
                            <select name="ClientValuation[client_customer_landline_code]" class="form-control phone-code">
                                <option value="04" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '04')) {
                                    echo 'selected';
                                } ?>>04</option>
                                <option value="02" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '02')) {
                                    echo 'selected';
                                } ?>>02</option>
                                <option value="03" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '03')) {
                                    echo 'selected';
                                } ?>>03</option>
                                <option value="06" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '06')) {
                                    echo 'selected';
                                } ?>>06</option>
                                <option value="07" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '07')) {
                                    echo 'selected';
                                } ?>>07</option>
                                <option value="09" <?php if (isset($model->client_customer_landline_code) && ($model->client_customer_landline_code == '09')) {
                                    echo 'selected';
                                } ?>>09</option>
                            </select>
                        </div>
                        <?= $form->field($model, 'client_customer_landline')->textInput(['maxlength' => true, 'class' => 'form-control phone-number', 'placeholder' => 'Enter landline number'])->label(false) ?>
                    </div>
                </div>
                <div class="col-sm-4 ">
                        <?= $form->field($model, 'client_customer_email')->textInput(['maxlength' => true,'placeholder' => 'Enter email']); ?>
                </div>

                
            </div>

        </div>
    </div>

    <?php /* ?>
    <div class="card card-outline card-primary">
        <header class="card-header">
                <h2 class="card-title">Instructing Party Details</h2>
        </header>
        <div class="card-body">

            <div class="row">

                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'instructing_person_prefix')->widget(\kartik\select2\Select2::classname(), [
                        'data' => Yii::$app->smHelper->getPrefixArr(),
                        'options' => ['placeholder' => 'Select...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'initialize' => true,
                        ],
                    ])->label('Instructing Person Prefix');
                    ?>
                </div>

                <div class="col-sm-4 ">
                        <?= $form->field($model, 'instructing_person_fname')->textInput(['maxlength' => true, 'placeholder' => 'Enter first name']); ?>
                </div>

                <div class="col-sm-4 ">
                        <?= $form->field($model, 'instructing_person_lname')->textInput(['maxlength' => true, 'placeholder' => 'Enter last name']); ?>
                </div>
                
                <div class="col-sm-4 phone">
                    <label class="control-label" for="clientvaluation-instructing_person_phone">Instructing Person Phone <span class="text-danger"></span></label>
                    <div class="input-group phone-parent">
                        <div class="input-group-prepend">
                            <select name='ClientValuation[instructing_person_phone_code]' class="form-control phone-code">
                                <option value="050" <?php if (isset($model->instructing_person_phone_code) && ($model->instructing_person_phone_code == '050')) {
                                    echo 'selected';
                                } ?>>050</option>
                                <option value="052" <?php if (isset($model->instructing_person_phone_code) && ($model->instructing_person_phone_code == '052')) {
                                    echo 'selected';
                                } ?>>052</option>
                                <option value="054" <?php if (isset($model->instructing_person_phone_code) && ($model->instructing_person_phone_code == '054')) {
                                    echo 'selected';
                                } ?>>054</option>
                                <option value="055" <?php if (isset($model->instructing_person_phone_code) && ($model->instructing_person_phone_code == '055')) {
                                    echo 'selected';
                                } ?>>055</option>
                                <option value="056" <?php if (isset($model->instructing_person_phone_code) && ($model->instructing_person_phone_code == '056')) {
                                    echo 'selected';
                                } ?>>056</option>
                                <option value="058" <?php if (isset($model->instructing_person_phone_code) && ($model->instructing_person_phone_code == '058')) {
                                    echo 'selected';
                                } ?>>058</option>
                            </select>
                        </div>
                        <?= $form->field($model, 'instructing_person_phone')->textInput(['maxlength' => true, 'class' => 'form-control phone-number', 'placeholder' => 'Enter phone number'])->label(false) ?>
                    </div>
                </div>
                <div class="col-sm-4 phone">
                    <label class="control-label" for="clientvaluation-instructing_person_landline">Client Customer Landline <span class="text-danger"> </span></label>
                    <div class="input-group phone-parent">
                        <div class="input-group-prepend">
                            <select name="ClientValuation[instructing_person_landline_code]"  class="form-control phone-code">
                                <option value="04" <?php if (isset($model->instructing_person_landline_code) && ($model->instructing_person_landline_code == '04')) {
                                    echo 'selected';
                                } ?>>04</option>
                                <option value="02" <?php if (isset($model->instructing_person_landline_code) && ($model->instructing_person_landline_code == '02')) {
                                    echo 'selected';
                                } ?>>02</option>
                                <option value="03" <?php if (isset($model->instructing_person_landline_code) && ($model->instructing_person_landline_code == '03')) {
                                    echo 'selected';
                                } ?>>03</option>
                                <option value="06" <?php if (isset($model->instructing_person_landline_code) && ($model->instructing_person_landline_code == '06')) {
                                    echo 'selected';
                                } ?>>06</option>
                                <option value="07" <?php if (isset($model->instructing_person_landline_code) && ($model->instructing_person_landline_code == '07')) {
                                    echo 'selected';
                                } ?>>07</option>
                                <option value="09" <?php if (isset($model->instructing_person_landline_code) && ($model->instructing_person_landline_code == '09')) {
                                    echo 'selected';
                                } ?>>09</option>
                            </select>
                        </div>
                        <?= $form->field($model, 'instructing_person_landline')->textInput(['maxlength' => true, 'class' => 'form-control phone-number', 'placeholder' => 'Enter landline number'])->label(false) ?>
                    </div>
                </div>
                <div class="col-sm-4 ">
                        <?= $form->field($model, 'instructing_person_email')->textInput(['maxlength' => true, 'placeholder' => 'Enter email']); ?>
                </div>
            </div>

        </div>
    </div>
    <?php */ ?>

    <div class="card card-outline card-primary">
        <header class="card-header">
                <h2 class="card-title">Contact Person Details</h2>
        </header>
        <div class="card-body">

            <div class="row">

                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'contact_person_prefix')->widget(\kartik\select2\Select2::classname(), [
                        'data' => Yii::$app->smHelper->getPrefixArr(),
                        'options' => ['placeholder' => 'Select...','value' => "Mr."],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'initialize' => true,
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-sm-4 ">
                        <?= $form->field($model, 'contact_person_fname')->textInput(['maxlength' => true, 'placeholder' => 'Enter first name']); ?>
                </div>

                <div class="col-sm-4 ">
                        <?= $form->field($model, 'contact_person_lname')->textInput(['maxlength' => true, 'placeholder' => 'Enter last name']); ?>
                </div>
                
                <div class="col-sm-4 phone">
                    <label class="control-label" for="clientvaluation-contact_person_phone">Contact Person Phone <span class="text-danger">*</span></label>
                    <div class="input-group phone-parent">
                        <div class="input-group-prepend">
                            <select name='ClientValuation[contact_person_phone_code]'  class="form-control phone-code">
                                <option value="050" <?php if (isset($model->contact_person_phone_code) && ($model->contact_person_phone_code == '050')) {
                                    echo 'selected';
                                } ?>>050</option>
                                <option value="052" <?php if (isset($model->contact_person_phone_code) && ($model->contact_person_phone_code == '052')) {
                                    echo 'selected';
                                } ?>>052</option>
                                <option value="054" <?php if (isset($model->contact_person_phone_code) && ($model->contact_person_phone_code == '054')) {
                                    echo 'selected';
                                } ?>>054</option>
                                <option value="055" <?php if (isset($model->contact_person_phone_code) && ($model->contact_person_phone_code == '055')) {
                                    echo 'selected';
                                } ?>>055</option>
                                <option value="056" <?php if (isset($model->contact_person_phone_code) && ($model->contact_person_phone_code == '056')) {
                                    echo 'selected';
                                } ?>>056</option>
                                <option value="058" <?php if (isset($model->contact_person_phone_code) && ($model->contact_person_phone_code == '058')) {
                                    echo 'selected';
                                } ?>>058</option>
                            </select>
                        </div>
                        <?= $form->field($model, 'contact_person_phone')->textInput(['maxlength' => true, 'class' => 'form-control phone-number', 'placeholder' => 'Enter phone number'])->label(false) ?>
                    </div>
                </div>
                <div class="col-sm-4 phone">
                    <label class="control-label" for="clientvaluation-contact_person_landline">Contact Person Landline <span class="text-danger"> </span></label>
                    <div class="input-group phone-parent">
                        <div class="input-group-prepend">
                            <select name="ClientValuation[contact_person_landline_code]" class="form-control phone-code">
                                <option value="04" <?php if (isset($model->contact_person_landline_code) && ($model->contact_person_landline_code == '04')) {
                                    echo 'selected';
                                } ?>>04</option>
                                <option value="02" <?php if (isset($model->contact_person_landline_code) && ($model->contact_person_landline_code == '02')) {
                                    echo 'selected';
                                } ?>>02</option>
                                <option value="03" <?php if (isset($model->contact_person_landline_code) && ($model->contact_person_landline_code == '03')) {
                                    echo 'selected';
                                } ?>>03</option>
                                <option value="06" <?php if (isset($model->contact_person_landline_code) && ($model->contact_person_landline_code == '06')) {
                                    echo 'selected';
                                } ?>>06</option>
                                <option value="07" <?php if (isset($model->contact_person_landline_code) && ($model->contact_person_landline_code == '07')) {
                                    echo 'selected';
                                } ?>>07</option>
                                <option value="09" <?php if (isset($model->contact_person_landline_code) && ($model->contact_person_landline_code == '09')) {
                                    echo 'selected';
                                } ?>>09</option>
                            </select>
                        </div>
                        <?= $form->field($model, 'contact_person_landline')->textInput(['maxlength' => true, 'class' => 'form-control phone-number', 'placeholder' => 'Enter landline number'])->label(false) ?>
                    </div>
                </div>
                <div class="col-sm-4 ">
                        <?= $form->field($model, 'contact_person_email')->textInput(['maxlength' => true, 'placeholder' => 'Enter email']); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="card card-outline card-primary">
        <header class="card-header">
                <h2 class="card-title">Property Address Details</h2>
        </header>
        <div class="card-body">

            <div class="row">
                <div class="col-sm-4">
                    <?= $form->field($model, 'community')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\Communities::find()
                        ->where(['city' => 3510])
                        ->andWhere(['trashed' => 0])
                        ->orderBy(['title' => SORT_ASC])->all(), 'id', 'title'),
                        'options' => ['placeholder' => 'Select a Community ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Community') ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'building_info')->widget(Select2::classname(), [
                        'data' => ($model->building_info ? [$model->building_info => $model->building->title] : []),
                        'options' => [
                            'placeholder' => 'Select a Building ...',
                            'value' => $model->building_info ? $model->building_info : '',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Building/Project Name') ?>
                </div>
                <div class="col-sm-4">
                    <?php 
                        echo $form->field($model, 'sub_community')->widget(Select2::classname(), [
                            'data' => ($model->sub_community ? [$model->sub_community => $model->subCommunities->title] : []),
                            'options' => [
                                'placeholder' => 'Select a Sub Community ...',
                                'value' => $model->sub_community ? $model->sub_community : '',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Sub Community'); 
                    ?>
                </div>
                <!-- <div class="col-sm-4">
                    <?php //echo $form->field($model, 'sub_community')->textInput(['maxlength' => true, 'readonly' => true,'placeholder' => 'Sub community']) ?>
                </div> -->
                <div class="col-sm-4">
                    <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readonly' => true, 'placeholder' => 'City']) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'country')->textInput(['maxlength' => true, 'readonly' => true, 'placeholder' => 'Country']) ?>
                </div>
                
            
                <div class="col-sm-4">
                    <?= $form->field($model, 'building_number')->textInput(['maxlength' => true,'placeholder' => 'Building number']) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'plot_number')->textInput(['maxlength' => true, 'placeholder' => 'Plot number']) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'street')->textInput(['maxlength' => true,'placeholder' => 'Street']) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'location_pin')->textInput(['maxlength' => true,'placeholder' => 'Location Pin']) ?>
                </div>

                
                

            </div>
            

        </div>
    </div>

    <div class="card card-outline card-primary">
        <header class="card-header">
                <h2 class="card-title">Property Details</h2>
        </header>
        <div class="card-body">

            <div class="row">
                <div class="col-sm-4">
                    <?php
                    $properties = \app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all();
                    $propertyData = ArrayHelper::map($properties, 'id', 'title');
                    // $propertyData = $propertyData + ['21' => 'Land'];
                    // asort($propertyData);
    
                    echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                        'data' => $propertyData,
                        'options' => ['placeholder' => 'Select a Property Type ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Property Type');
                    ?>

                </div>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>

                </div>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 unit_number">
                    <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true,'placeholder' => 'Enter unit number']) ?>
                    
                </div>
                <div class="col-sm-4 floor_number">
                    <?php
                    echo $form->field($model, 'floor_number')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->getCrmOptionZeroToNumber(200),
                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                    ]);
                    ?>
                </div>
                <div class="col-sm-4 number_of_units">
                    <?php
                    echo $form->field($model, 'number_of_units')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->getCrmOptionZeroToNumber(1000),
                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                    ]);
                    ?>
                </div>
            </div>
            <div class="row">    
                <div class="col-sm-4">
                    <?= $form->field($model, 'built_up_area_value')->textInput(['maxlength' => true,'placeholder' => 'Enter built up area', 'type' => 'number', 'step' => 'any', 'min' => 0])->label('Built Up Area') ?>
                </div>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'built_up_area_unit')->widget(\kartik\select2\Select2::classname(), [
                        'data' => ['1' => 'Sq.Ft.', '2' => 'Sq.M.'],
                        // 'options' => ['placeholder' => 'Select...'],
                        'pluginOptions' => [
                            // 'allowClear' => true,
                            'initialize' => true,
                        ],
                    ])->label('Built Up Area Unit (SQM/SQFT)');
                    ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true, 'type' => 'number', 'min' => 0, 'readonly' => true])->label('Built Up Area Auto Sq.Ft.') ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'net_leasable_area_value')->textInput(['maxlength' => true,'placeholder' => 'Enter net leasable area', 'type' => 'number', 'step' => 'any', 'min' => 0])->label('Net Leasable Area') ?>
                </div>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'net_leasable_area_unit')->widget(\kartik\select2\Select2::classname(), [
                        'data' => ['1' => 'Sq.Ft.', '2' => 'Sq.M.'],
                        'pluginOptions' => [
                            'initialize' => true,
                        ],
                    ])->label('Net Leasable Area Unit (SQM/SQFT)');
                    ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'net_leasable_area')->textInput(['maxlength' => true, 'type' => 'number', 'min' => 0, 'readonly' => true])->label('Net Leasable Area Auto Sq.Ft.') ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'plot_area_value')->textInput(['maxlength' => true,'placeholder' => 'Enter plot area', 'type' => 'number', 'step' => 'any', 'min' => 0])->label('Plot Area') ?>
                </div>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'plot_area_unit')->widget(\kartik\select2\Select2::classname(), [
                        'data' => ['1' => 'Sq.Ft.', '2' => 'Sq.M.'],
                        'pluginOptions' => [
                            'initialize' => true,
                        ],
                    ])->label('Plot Area Unit (SQM/SQFT)');
                    ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'plot_area')->textInput(['maxlength' => true, 'type' => 'number', 'min' => 0, 'readonly' => true])->label('Plot Auto Sq.Ft.') ?>
                </div>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'development_type')->widget(\kartik\select2\Select2::classname(), [
                        'data' => ['Standard' => 'Standard / By Developer', 'Non-Standard' => 'Non-Standard / By Client'],
                        'options' => ['placeholder' => 'Select...','value' => "Standard"],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'initialize' => true,
                        ],
                    ]);
                    ?>
                </div>
                
                <div class="col-sm-4 extended">
                    <?php 
                    echo $form->field($model, 'extended')->widget(\kartik\select2\Select2::classname(), [
                        'data' => ['No' => 'No', 'Yes' => 'Yes' ],
                        // 'options' => ['placeholder' => 'Select...','value' => "No"],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'initialize' => true,
                        ],
                    ]); 
                    ?>
                </div>

                <div class="col-sm-4">
                    <?php 
                    echo $form->field($model, 'completion_status')->widget(\kartik\select2\Select2::classname(), [
                        'data' => ['1' => 'Ready', '2' => 'Under Construction'],
                        // 'options' => ['placeholder' => 'Select...','value' => "1"],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'initialize' => true,
                        ],
                    ]); 
                    ?>
                </div>
            </div>

        </div>
    </div>

    <div class="card card-outline card-primary">
        <header class="card-header">
                <h2 class="card-title">Documents Details</h2>
        </header>
        <?php $unit_row = 0; ?>
        <div class="card-body" id="property-documents-container">
            <!-- document will be dynamically loaded here -->
        </div>

            
    </div>

    

   
   
    </div>
    <div class="card-footer">

        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i> Save'), ['name' => 'save-btn', 'class' => 'btn btn-success sav-btn1', 'value' => 'save']) ?>

        <?php /* <?= Html::submitButton(Yii::t('app', '<i class="fa fa-thumbs-up"></i> Confirm'), ['name' => 'confirm-btn', 'class' => 'btn btn-info', 'value' => 'confirm']) ?> */ ?>
        <?php if(Yii::$app->user->identity->client_approver == 1){ ?>
        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check-square"></i> Send'), ['name' => 'verify-btn', 'class' => 'btn btn-primary', 'value' => 'verify']) ?>
        <?php } ?>

        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>

       
        <?php
        if ($model <> null && $model->id <> null) {
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => get_class($model),
            ]);
        }
        ?>
    </div>
    

        
            
    <?php ActiveForm::end(); ?>
</section>

<script type="text/javascript">
    var unit_row = <?= $unit_row ?>;


    var uploadAttachment = function (attachmentId) {


        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function (json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);


                            console.log(json.file.href);
                            var str1 = json['file'].href;
                            var str2 = ".pdf";
                            var str3 = ".doc";
                            var str4 = ".docx";
                            var str5 = ".xlsx";
                            var str6 = ".xls";
                            if (str1.indexOf(str2) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                            } else if (str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                            } else {
                                $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                            }
                            $('#input-attachment' + attachmentId).val(json['file'].href);


                            /*  $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                             $('#input-attachment' + attachmentId).val(json['file'].href);*/
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }

</script>

<?php

$this->registerJs('


    var atch_count = "'.$unit_row.'";

    $("body").on("click", ".add-km-image", function (e) {
        
        call_url = "'.\yii\helpers\Url::to(['suggestion/getclientvalimagehtml']).'";
        $.ajax({
            url: call_url,
            data: {atch_count:atch_count},
            dataType: "html",
            success: function(data) {
                data = JSON.parse(data);
                console.log(data)
                $("#km-table").append(data.col);
                atch_count++;                    
            },
            error: bbAlert
        });
    });


    
    $("body").on("click", ".open-img-window", function (e) {
        target_id = $(this).attr("data-uploadid");
        uploadAttachment(target_id)
    });



    
    // window.addEventListener("load", initialize);


    
        
');
?>