<?php

use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\User;
use app\models\CategoryManager;
use app\models\AssetManager;
use app\models\Department;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GeneralAsumptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Expense Manager";
$this->params['breadcrumbs'][] = $this->title;

$cardTitle = Yii::t('app', 'List');


// echo Html::tag('div',
//     Html::a(
//         'Asset Register',
//         ['asset-manager/register'],
//         [
//             'class' => 'btn btn-success',
//             'style' => 'margin:10px 10px;',
//             'target' => '_blank', // Add this line to set the target
//         ]
//     ),
//     ['class' => 'text-right']
// );

// $createBtn=true;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    $createBtn=true;
}
// $actionBtns.='{view}';
// if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
//     $actionBtns.='{view}';
// }
$actionBtns.='{update}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
$actionBtns.='{delete}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
    $actionBtns.='{delete}';
}
// $actionBtns.='{get-history}';

?>
<div class="task-index">

    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        // 'createBtn' => $createBtn,

        'columns' => [


            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:1px;']],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:1%'],
            //     'attribute' => 'priority',
            //     'label' => Yii::t('app', 'Lead'),
            //     'value' => function ($model) {
            //         return ucfirst($model->priority);
            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:1%'],
            //     'attribute' => 'modification',
            //     'label' => Yii::t('app', 'Change'),
            //     'value' => function ($model) {
            //         return ucfirst($model->modification);
            //     },
            // ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'asset_id',
                'label' => Yii::t('app', 'Name'),
                'value' => function ($model) {
                    $get_name = AssetManager::find()->where(['id' => $model->asset_id])->one();
                    if(isset($get_name->asset_name))
                    {
                        return $get_name->asset_name;
                    }else{
                        return "";
                    }

                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'category_name',
                'label' => Yii::t('app', 'Category'),
                'value' => function ($model) {
                    return $model->category_name;
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'asset_owner',
                'label' => Yii::t('app', 'Asset Owner'),
                'value' => function ($model) {
                    $get_name = User::find()->where(['id' => $model->asset_owner])->one();
                    if(isset($get_name->lastname))
                    {
                        return $get_name->lastname;
                    }else{
                        return "";
                    }

                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:5%'],
                'attribute' => 'amount',
                'label' => Yii::t('app', 'Amount'),
                'value' => function ($model) {
                    return number_format($model->amount,0);
                },
            ],
            

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:7%'],
                'attribute' => 'first_supplier',
                'label' => Yii::t('app', 'Supplier 1 Amount'),
                'value' => function ($model) {
                    return ucfirst($model->first_supplier);
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:7%'],
                'attribute' => 'second_supplier',
                'label' => Yii::t('app', 'Supplier 2 Amount'),
                'value' => function ($model) {
                    return ucfirst($model->second_supplier);
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:7%'],
                'attribute' => 'third_supplier',
                'label' => Yii::t('app', 'Supplier 3 Amount'),
                'value' => function ($model) {
                    return ucfirst($model->third_supplier);
                },
            ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:1%'],
            //     'attribute' => 'module',
            //     'label' => Yii::t('app', 'Module'),
            //     'value' => function ($model) {

            //         if($model->module == "task_management")
            //         {
            //             return"Task Management";
            //         }elseif ( $model->module == "none" ){
            //             return " ";
            //         }else{
            //             return ucfirst($model->module);
            //         }

            //     },
            // ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:1%'],
                'attribute' => 'department',
                'label' => Yii::t('app', 'Department'),
                'value' => function ($model) {
                    $department = Department::find()->where(['id' => $model->department])->one();
                    if(isset($department->title))
                    {
                        return $department->title;
                    }else{
                        return "";
                    }

                },
            ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:1%'],
            //     'attribute' => 'frequency',
            //     'label' => Yii::t('app', 'Frequency'),
            //     'value' => function ($model) {
            //         return ucfirst($model->frequency);
            //     },
            // ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'entered_date',
                'label' => Yii::t('app', 'Entered Date'),
                'value' => function ($model) {
                    if(isset($model->entered_date))
                    {
                        return date("d-M-Y", strtotime($model->entered_date)) . "<br>" . date('h:i A', strtotime($model->entered_date));
                    }else{
                        return "";
                    }
                },
            ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:16%'],
            //     'attribute' => 'start_date',
            //     'label' => Yii::t('app', 'Warranty Start Date'),
            //     'value' => function ($model) {
            //         return  date("d-M-Y", strtotime($model->start_date)) . "<br>" . date('h:i A', strtotime($model->start_date));
            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:13%'],
            //     'attribute' => 'expiry_date',
            //     'label' => Yii::t('app', 'Warranty Expiry Date'),
            //     'value' => function ($model) {
            //         if(isset($model->expiry_date))
            //         {
            //             return  date("d-M-Y", strtotime($model->expiry_date)) . "<br>" . date('h:i A', strtotime($model->expiry_date));
            //         }else{
            //             return "";
            //         }

            //     },
            // ],



            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:16%'],
            //     'attribute' => 'end_date',
            //     'label' => Yii::t('app', 'End Date'),
            //     'value' => function ($model) {
            //         return  date("d-M-Y", strtotime($model->end_date)) . "<br>" . date('h:i A', strtotime($model->end_date));
            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:7% ; color:#017BFE;'],
            //     'attribute' => 'days_hours',
            //     'label' => Yii::t('app', 'Days - Hours'),
            //     'value' => function ($model) {
            //         $startDate = $model->start_date;
            //         $endDate = $model->end_date;

            //         $startDateTime = new DateTime($startDate); // Start date and time
            //         $endDateTime = new DateTime($endDate);

            //         $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
            //         return number_format($workingHours_1/9,2).'</b>'.'<br>'.$workingHours_1.' Hour';
            //     },
            // ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'collection_date',
                'label' => Yii::t('app', 'Collection Date'),
                'value' => function ($model) {
                    if(isset($model->collection_date))
                    {
                        return  date("d-M-Y", strtotime($model->collection_date)) . "<br>" . date('h:i A', strtotime($model->collection_date));
                    }else{
                        return "";
                    }

                },
            ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:1%'],
            //     'attribute' => 'responsibility',
            //     'label' => Yii::t('app', 'Assigned To'),
            //     'value' => function ($model) {
            //         $get_name = User::find()->where(['id' => $model->assigned_to])->one();
            //         return $get_name->firstname.'<br>'.$get_name->lastname;
            //     },
            // ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:1% ; color:#017BFE;'],
                'attribute' => 'progress',
                'label' => Yii::t('app', 'Status'),
                'value' => function ($model) {
                    return ucfirst($model->status);
                },
            ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:7%'],
            //     'attribute' => 'completed_date',
            //     'label' => Yii::t('app', 'Completion Date'),
            //     'value' => function ($model) {
            //         if(isset($model->completed_date))
            //         {
            //             return date("d-M-Y", strtotime($model->completed_date)) . "<br>" . date('h:i A', strtotime($model->completed_date));
            //         }else{
            //             return "";
            //         }
            //     },
            // ],



            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:4%'],
            //     'attribute' => 'status',
            //     'label' => Yii::t('app', 'Progress'),
            //     'value' => function ($model) {
            //         if($model->status == "overdue")
            //         {
            //             return "<span style=\"color:red\">".ucfirst($model->status)."</span>";
            //         }elseif($model->status == "pending"){
            //             return "<span style=\"color:#017BFE\">".ucfirst($model->status)."</span>";
            //         }elseif($model->status == "completed"){
            //             return "<span style=\"color:#EC9706\">".ucfirst($model->status)."</span>";
            //         }elseif($model->status == "cancelled"){
            //             return "<span style=\"color:grey\">".ucfirst($model->status)."</span>";
            //         }elseif($model->status == "completed_ahead"){
            //             return "<span style=\"color:green\">".'Completed'."</span>";
            //         }


            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:4% ; color:#017BFE;'],
            //     'attribute' => 'tat',
            //     'label' => Yii::t('app', 'Perfor mance'),
            //     'value' => function ($model) {

                    // if(isset($model->completed_date))
                    // {
                    //     $endDateTime = new \DateTime($model->end_date);
                    //     $formatEndDate = $endDateTime->format('Y-m-d');

                    //     $completeDateTime = new \DateTime($model->completed_date);
                    //     $formatCompleteDate = $completeDateTime->format('Y-m-d');
    
                    //     $startDateTime = new DateTime($formatEndDate); // Start date and time
                    //     $endDateTime = new DateTime($formatCompleteDate);

                    //     $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                    //     return number_format($workingHours_1/8.5,2).'</b>'.' Days';
                    // }else{
                    //     $currentDate = new \DateTime(date("Y/m/d H:i:s"));
                    //     $formattedDateTimeToday = strtotime($currentDate->format('Y-m-d'));

                    //     $endDate = new \DateTime($model->end_date);
                    //     $formattedEndDate = strtotime($endDate->format('Y-m-d'));

                    //     if($formattedEndDate <= $formattedDateTimeToday)
                    //     {
                    //         $diff = $formattedDateTimeToday - $formattedEndDate;
                    //         $days = floor($diff / (60 * 60 * 24));
                    //         $days = '+ '.$days;
                    //     }elseif($formattedEndDate >= $formattedDateTimeToday)
                    //     {
                    //         $diff = $formattedEndDate - $formattedDateTimeToday;
                    //         $days = floor($diff / (60 * 60 * 24));
                    //         $days = '- '.$days;
                    //     }
                    //     return $days.' Days';
                    // }
                    
                    // if($model->status != "pending")
                    // {
                    //     if( $model->completed_date <> '' || $model->completed_date <> null )
                    //     {
                    //         $currentDate = new \DateTime($model->completed_date);
                    //         $startDate = new \DateTime($model->start_date);
                    //         $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDate, $currentDate);
                            
                    //         return number_format($workingHours_1/9,2).'</b>'.' Days';
                    //     }else{
                    //         $currentDate = new \DateTime(date("Y/m/d H:i:s"));
                    //         $startDate = new \DateTime($model->start_date);
                    //         $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDate, $currentDate);
                            
                    //         return number_format($workingHours_1/9,2).'</b>'.' Days';
                    //     }

                    // }else{
                    //     $currentDate = new \DateTime(date("Y/m/d H:i:s"));
                    //     $formattedDateTimeToday = strtotime($currentDate->format('Y-m-d'));

                    //     $endDate = new \DateTime($model->expiry_date);
                    //     $formattedEndDate = strtotime($endDate->format('Y-m-d'));

                    //     if($formattedEndDate <= $formattedDateTimeToday)
                    //     {
                    //         $diff = $formattedDateTimeToday - $formattedEndDate;
                    //         $days = floor($diff / (60 * 60 * 24));
                    //         $days = '+ '.$days;
                    //     }elseif($formattedEndDate >= $formattedDateTimeToday)
                    //     {
                    //         $diff = $formattedEndDate - $formattedDateTimeToday;
                    //         $days = floor($diff / (60 * 60 * 24));
                    //         $days = '- '.$days;
                    //     }
                    //     return $days.' Days';
                    // }
            //     },
            // ],



            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:5%'],
            //     'attribute' => 'department',
            //     'label' => Yii::t('app', 'Department'),
            //     'value' => function ($model) {
            //         if($model->department == "it")
            //         {
            //             return "IT";
            //         }elseif($model->department == "hr")
            //         {
            //             return "HR";
            //         }elseif($model->department == "service")
            //         {
            //             return "New Service/Geography";
            //         }else{
            //             return ucfirst($model->department);
            //         }

            //     },
            // ],


            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:10%'],
            //     'attribute' => 'target_date',
            //     'label' => Yii::t('app', 'Target Date'),
            //     'value' => function ($model) {
            //         return  date("d-M-Y", strtotime($model->target_date)) . "<br>" . date('h:i A', strtotime($model->target_date));
            //     },
            // ],


            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:5%'],
            //     'attribute' => 'verified_by',
            //     'label' => Yii::t('app', 'Verified By'),
            //     'value' => function ($model) {
            //         $get_name = User::find()->where(['id' => $model->verified_by])->one();
            //         return $get_name->lastname;
            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:5%'],
            //     'attribute' => 'approved_by',
            //     'label' => Yii::t('app', 'Approved By'),
            //     'value' => function ($model) {
            //         $get_name = User::find()->where(['id' => $model->approved_by])->one();
            //         return $get_name->lastname;
            //     },
            // ],




            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
                    <div class="dropdown-menu" role="menu">
              '.$actionBtns.'
                    </div>
                </div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>

</div>
