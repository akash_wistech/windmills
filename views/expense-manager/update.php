<?php

use yii\helpers\Html;

$this->title = 'Update Expense: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Expense Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="expense-update">

    <?= $this->render('_form', [
        'model' => $model,
        'page_title' => $this->title,
    ]) ?>

</div>
