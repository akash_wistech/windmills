<?php

use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\User;
use app\models\CategoryManager;
use app\models\AssetManager;
use app\models\Department;
use kartik\select2\Select2;

use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GeneralAsumptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Expense Manager";
$this->params['breadcrumbs'][] = $this->title;

$cardTitle = Yii::t('app', 'Overtime Expenses');


// echo Html::tag('div',
//     Html::a(
//         'Asset Register',
//         ['asset-manager/register'],
//         [
//             'class' => 'btn btn-success',
//             'style' => 'margin:10px 10px;',
//             'target' => '_blank', // Add this line to set the target
//         ]
//     ),
//     ['class' => 'text-right']
// );

// $createBtn=true;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create-ot')){
    $createBtn=true;
}
// $actionBtns.='{view}';
// if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
//     $actionBtns.='{view}';
// }
$actionBtns.='{update-ot}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update-ot')){
    $actionBtns.='{update-ot}';
}
$actionBtns.='{delete-ot}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete-ot')){
    $actionBtns.='{delete-ot}';
}



// $actionBtns.='{get-history}';

?>
<div class="task-index">


<div class="valuation-search card-body mb-0 pb-0">

    <?php $form = ActiveForm::begin([
        'action' => ['index-ot'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-sm-4 text-left">
            <?php
            echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                'options' => ['placeholder' => 'Time Frame ...'],
                'pluginOptions' => [
                'allowClear' => true
                ],
                ])->label(false);
                ?>
        </div>
        <div class="col-sm-4" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
            <div class="" id="end_date_id">
            
            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>

            </div>
        </div>
        <div class="col-sm-4">
            <div class="text-left">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            </div>
            </div>
        </div>
    </div>

    

  <?php ActiveForm::end(); ?>
</div>

    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        // 'createBtn' => $createBtn,
        'showFooter' => true,
        'columns' => [

            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:1px;']],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:20%'],
                'attribute' => 'purpose',
                'label' => Yii::t('app', 'Purpose'),
                'value' => function ($model) {
                    return $model->purpose;
                },
                'filter' => false,
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:13%'],
                'attribute' => 'start_date',
                'label' => Yii::t('app', 'Start Date'),
                'value' => function ($model) {
                    $timestamp = strtotime($model->start_date);

                    $formattedDate = Yii::$app->formatter->asDate($timestamp, 'php:Y-m-d');
                    return date("d-M-Y", strtotime($formattedDate));
                },
                'filter' => false,
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:13%'],
                'attribute' => 'start_time',
                'label' => Yii::t('app', 'Start Time'),
                'value' => function ($model) {
                    $date = new DateTime($model->start_time);
                    $formattedTime = $date->format('g:i a');
                    return $formattedTime;
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'end_time',
                'label' => Yii::t('app', 'End Time'),
                'value' => function ($model) {
                    $date = new DateTime($model->end_time);
                    $formattedTime = $date->format('g:i a');
                    return $formattedTime;
                },
            ],
            

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'total_time',
                'label' => Yii::t('app', 'Total Time'),
                'value' => function ($model) {
                    return $model->total_time . '<b> Hours </b>' ;
                },
                'footer' => $totalTime . '<b> Hours </b>',
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'val_amount',
                'label' => Yii::t('app', 'Total Amount'),
                'value' => function ($model) {
                    return $model->val_amount . ' ' . 'AED';
                },
                'footer' => $totalAmt . ' ' . 'AED',
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'applicant',
                'label' => Yii::t('app', 'Applicant'),
                'value' => function ($model) {
                    $user_detail = \app\models\User::findOne(['id' => $model->applicant]);
                    return $user_detail->firstname.' '.$user_detail->lastname;
                },

                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'applicant',
                    'data' => ArrayHelper::map(\app\models\User::find()
                                ->where([ 'IN', 'user_type' , [10,20]])
                                ->orderBy(['firstname' => SORT_ASC,])
                                ->all(), 'id',
                                function ($user) {
                                    return $user->firstname . ' ' . $user->lastname;
                                }
                            ),
                    'options' => ['placeholder' => 'Select...'],

                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),

            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:15%'],
                'attribute' => 'entered_date',
                'label' => Yii::t('app', 'Entered Date'),
                'value' => function ($model) {
                    if(isset($model->entered_date))
                    {
                        return date("d-M-Y", strtotime($model->entered_date)) . "<br>" . date('h:i A', strtotime($model->entered_date));
                    }else{
                        return "";
                    }
                },
                'filter' => false,
            ],


            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10% ; color:#017BFE;'],
                'attribute' => 'progress',
                'label' => Yii::t('app', 'Status'),
                'value' => function ($model) {
                    return ucfirst($model->status);
                },
            ],


            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
                    <div class="dropdown-menu" role="menu">
              '.$actionBtns.'
                    </div>
                </div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update-ot' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'delete-ot' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>

</div>




<?php
$this->registerJs('

      $(".div1").daterangepicker({
        autoUpdateInput: false,
          locale: {
            format: "YYYY-MM-DD"
          }
      });
     
      $(".div1").on("apply.daterangepicker", function(ev, picker) {
         $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
      });
      $(document).ready(function() {
          // Code to execute when the document is ready
          var period = $("#feedbacksearch-time_period").val();

          console.log(period)
          
          if (period == 9) {
              $(".div1").prop("required", true);
              $("#date_range_array").show();
          } else {
              $(".div1").prop("required", false);
              $("#date_range_array").hide();
          }
          
          // Change event handler for select input
          $("#feedbacksearch-time_period").on("change", function() {
              var period = $(this).val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $(".div1").val("");
                  $("#date_range_array").hide();
              }
          });
      });




');
?>
