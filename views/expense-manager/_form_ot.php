<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use yii\helpers\ArrayHelper;


    $clientsArr = ArrayHelper::map(\app\models\User::find()
    ->select([
    'id', 'CONCAT(firstname, " ", lastname) AS lastname',
    ])
        ->where(['status' => 1])
        ->andWhere(['user_type' => [10,20]])
        ->orderBy(['lastname' => SORT_ASC,])
        ->all(), 'id', 'lastname');
    $clientsArr = ['' => 'select'] + $clientsArr;

    if(isset($model))
    {
        $user_detail = \app\models\User::findOne(['id' => $model->applicant]);
    }else{
        $user_id = Yii::$app->user->identity->id;
        $user_detail = \app\models\User::findOne(['id' => $user_id]);
    }

    $user_profile_detail = \app\models\UserProfileInfo::findOne(['id' => $user_detail->id]);
    $department = \app\models\Department::findOne(['id' => $user_profile_detail->department_id]);
    $full_name = $user_detail->firstname.' '.$user_detail->lastname;


    // Banking Channels

    // Yii::$app->appHelperFunctions->ZohoRefereshToken();
    // $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');
    // $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');

    // $curl = curl_init();
    // curl_setopt_array($curl, array(
    // CURLOPT_URL => 'https://www.zohoapis.com/books/v3/bankaccounts?organization_id='.$OrganizationID.'&filter_by=Status.Active',
    // CURLOPT_RETURNTRANSFER => true,
    // CURLOPT_ENCODING => '',
    // CURLOPT_MAXREDIRS => 10,
    // CURLOPT_TIMEOUT => 0,
    // CURLOPT_FOLLOWLOCATION => true,
    // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    // CURLOPT_CUSTOMREQUEST => 'GET',
    // CURLOPT_HTTPHEADER => array(
    //     'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
    // ),
    // ));

    // $response = curl_exec($curl);
    // curl_close($curl);
    // $bank_accounts = json_decode($response);
?>

<input type="hidden" id="user_id" value="<?= Yii::$app->user->identity->id; ?>">
<section class="contact-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <header class="card-header">
        <h2 class="card-title"><?php if(isset($page_title)) {echo $page_title;}else{echo "Add Over-Time Expense";} ?></h2>
    </header>
    <div class="card-body">
        <div class="row">

            <div class="col-4">
                <label for="purpose"> Purpose </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" name="purpose" id="purpose" value="<?php if(isset($model->purpose)) echo $model->purpose; ?>" >
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="start_date">Start Date</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="date" id ="start_date" name="start_date" class="form-control" value="<?php if(isset($model->start_date)) echo date("Y-m-d", strtotime($model->start_date)); ?>" >
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="end_date">End Date</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="date" id ="end_date" name="end_date" class="form-control" value="<?php if(isset($model->end_date)) echo date("Y-m-d", strtotime($model->end_date)); ?>" >
                    </div>
                </div>
            </div>


        </div>

        <hr style="border-color:#017BFE">

        <div class="row">

            <div class="col-4">
                <label for="start_time">Start Time</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                    <input type="time" id="start_time" name="start_time" class="form-control" value="<?php if(isset($model->start_time)) echo $model->start_time; ?>" >
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="end_time">End Time</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                    <input type="time" id="end_time" name="end_time" class="form-control" value="<?php if(isset($model->end_time)) echo $model->end_time; ?>" >
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="total_time">Total Time (in hours)</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                    <input readonly type="text" id="total_time" name="total_time" class="form-control" value="<?php if(isset($model->total_time)) echo $model->total_time; ?>" >
                    </div>
                </div>
            </div>

        </div>

    <hr style="border-color:#017BFE">
        <div class="row"> 

            <div class="col-4">
                <label for="applicant">Applicant</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" value="<?= $full_name ?>" readonly>
                        <input type="hidden" class="form-control" id="applicant" name="applicant" value="<?= $user_detail->id; ?>">
                        <!-- <select name="applicant" id="applicant" class="form-control">
                            <?php
                                foreach($clientsArr as $key => $users){

                                    if($model->applicant == $key)
                                    {
                                        echo "<option value='$model->applicant' selected>$users</option>";
                                    }else{
                                        echo "<option value='$key'> $users</option>";
                                    }

                                }
                            ?>
                        </select> -->
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="department">Department</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" value="<?= $department->title ?>" readonly>
                        <input type="hidden" class="form-control" id="department" name="department" value="<?= $department->id; ?>">
                    </div>
                </div>
            </div>

        </div>

        <hr style="border-color:#017BFE">

        <br>

        <div class="row">
            <div class="col-4">
                <label for="status">Status</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='status' id="status" class="form-control">
                            <option value="entered"
                                <?php if(isset($model->status) && ($model->status == 'entered')){ echo 'selected';} ?>>
                                Entered</option>
                            <option id="app_div" value="approved"
                                <?php if(isset($model->status) && ($model->status == 'approved')){ echo 'selected';} ?>>
                                Approved</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

            <!-- <div class="col-4">
                <label for="type">Type of Task</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='type' id="type" class="form-control">
                            <option value="project"
                                <?php if(isset($model->type) && ($model->type == 'project')){ echo 'selected';} ?>>
                                Project</option>
                            <option value="kpi"
                                <?php if(isset($model->type) && ($model->type == 'kpi')){ echo 'selected';} ?>>
                                KPI</option>
                        </select>
                    </div>
                </div>
            </div> -->

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index-ot'], ['class' => 'btn btn-default']) ?>
        <?php
        if($model<>null && $model->id<>null){
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => get_class($model),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
    
</section>


<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<?php
$this->registerJs('

$("#asset_id").change(function() {
    var val = $(this).val(); 
    $.ajax({
        url: "'.yii\helpers\Url::to(['suggestion/get_depreciation']).'/"+val,
        method: "post",
        // dataType: "html",
        success: function(data) {

            var parts = data.split(",");
            var category_id = parts[0];
            var category_name = parts[1];
            var depreciation = parts[2];

            $("#category_id").val(category_id);
            $("#category_name").val(category_name);
            $("#depriciation").val(depreciation);
        },
        error: bbAlert
    });
});

$("#expense_id").change(function() {
    var val = $(this).val(); 
    $.ajax({
        url: "'.yii\helpers\Url::to(['suggestion/get_depreciation']).'/"+val,
        method: "post",
        // dataType: "html",
        success: function(data) {

            var parts = data.split(",");
            var category_id = parts[0];
            var category_name = parts[1];
            var depreciation = parts[2];


            $("#category_id").val(category_id);
            $("#category_name").val(category_name);
            $("#depriciation").val(depreciation);

        },
        error: bbAlert
    });
});

var value = $("#accounting_entry").val();
if(value == "old")
{
    $("#payment_div").css("display", "none");
    $("#vendor_div").css("display", "none");
    $("#pay_bank_div").css("display", "none");
}

var value = $("#payment_mode").val();
if(value == "cash")
{
    $("#vendor_div").css("display", "none");
    $("#pay_bank_div").css("display", "block");
}
if(value == "credit")
{
    $("#vendor_div").css("display", "block");
    $("#pay_bank_div").css("display", "block");
}

$("#accounting_entry").change(function() {
    var val = $(this).val(); 
      if(val == "old")
      {
        $("#payment_div").css("display", "none");
        $("#vendor_div").css("display", "none");
        $("#pay_bank_div").css("display", "none");
      }
      if(val == "new"){
        $("#payment_div").css("display", "block");
        $("#vendor_div").css("display", "none");
        $("#pay_bank_div").css("display", "block");
      }
});


$("#payment_mode").change(function() {
    var val = $(this).val(); 
      if(val == "cash")
      {
        $("#pay_bank_div").css("display", "block");
        $("#vendor_div").css("display", "none");
      }
      if(val == "credit"){
        $("#vendor_div").css("display", "block");
        $("#pay_bank_div").css("display", "block");
      }
      if(val == "none"){
        $("#vendor_div").css("display", "none");
        $("#pay_bank_div").css("display", "block");
      }
});


$("#app_div").prop("disabled", true);

var value = $("#user_id").val();
if( value == "1" || value == "14" || value == "86" || value == "33" || value == "21" || value == "53" || value == "110465" )
{
    $("#app_div").prop("disabled", false);
}

$("#end_time").on("change", function() {
    var end = this.value;
    var start = $("#start_time").val();

    var start = new Date("1970-01-01T" + start + "Z");
    var end = new Date("1970-01-01T" + end + "Z");

    if (end < start) {
        end.setDate(end.getDate() + 1);
    }
    var diff = end - start;

    var hours = diff / 1000 / 60 / 60;

    $("#total_time").val(hours);
});

// $("#reference_number").select2({
//     placeholder: "Select Reference Number",
//     allowClear: true
// });


    
');
