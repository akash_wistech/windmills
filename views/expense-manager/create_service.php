<?php

use yii\helpers\Html;

$this->title = 'Add Service';
$this->params['breadcrumbs'][] = ['label' => 'Expense Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-create">

    <?= $this->render('_form_service', [
        'model' => $model,
    ]) ?>

</div>
