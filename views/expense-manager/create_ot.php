<?php

use yii\helpers\Html;

$this->title = 'Add Expense';
$this->params['breadcrumbs'][] = ['label' => 'Expense Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expense-create">

    <?= $this->render('_form_ot', [
        'model' => $model,
    ]) ?>

</div>
