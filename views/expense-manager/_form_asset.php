<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use yii\helpers\ArrayHelper;

    $assets = ArrayHelper::map(\app\models\AssetManager::find()
    ->select([
    'id', 'asset_name',
    ])
        ->where(['status' => 'active'])
        ->where(['transaction_type' => 'asset'])
        ->orderBy(['id' => SORT_ASC,])
        ->all(), 'id', 'asset_name');
    $assets = ['' => 'select'] + $assets;

    $expenses = ArrayHelper::map(\app\models\AssetManager::find()
    ->select([
    'id', 'asset_name',
    ])
        ->where(['status' => 'active'])
        ->where(['transaction_type' => 'consumable'])
        ->orWhere(['transaction_type' => 'service'])
        ->orderBy(['id' => SORT_ASC,])
        ->all(), 'id', 'asset_name');
    $expenses = ['' => 'select'] + $expenses;

    $clientsArr = ArrayHelper::map(\app\models\User::find()
    ->select([
    'id', 'CONCAT(firstname, " ", lastname) AS lastname',
    ])
        ->where(['status' => 1])
        ->andWhere(['user_type' => [10,20]])
        ->orderBy(['lastname' => SORT_ASC,])
        ->all(), 'id', 'lastname');
    $clientsArr = ['' => 'select'] + $clientsArr;

    $user_id = Yii::$app->user->identity->id;
    $user_detail = \app\models\User::findOne(['id' => $user_id]);
    $user_profile_detail = \app\models\UserProfileInfo::findOne(['id' => $user_detail->id]);
    $department = \app\models\Department::findOne(['id' => $user_profile_detail->department_id]);
    $full_name = $user_detail->firstname.' '.$user_detail->lastname;


    // Banking Channels

    Yii::$app->appHelperFunctions->ZohoRefereshToken();
    $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');
    $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');

    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://www.zohoapis.com/books/v3/bankaccounts?organization_id='.$OrganizationID.'&filter_by=Status.Active',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_HTTPHEADER => array(
        'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
    ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $bank_accounts = json_decode($response);

    // Vendors

    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://www.zohoapis.com/books/v3/vendors?organization_id='.$OrganizationID,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_HTTPHEADER => array(
        'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
    ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $vendors = json_decode($response);
?>
<input type="hidden" id="user_id" value="<?= $user_id ?>">
<section class="contact-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <header class="card-header">
        <h2 class="card-title"><?php if(isset($page_title)) {echo $page_title;}else{echo "Add Asset";} ?></h2>
    </header>
    <div class="card-body">
        <div class="row">

            <div class="col-4">
                <label for="action">Action</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='action' id="action" class="form-control">
                            <option value="purchase"
                                <?php if(isset($model->action) && ($model->action == 'purchase')){ echo 'selected';} ?>>
                                Purchase</option>
                            <option value="sale"
                                <?php if(isset($model->action) && ($model->action == 'sale')){ echo 'selected';} ?>>
                                Sale</option>
                            <option value="throw"
                                <?php if(isset($model->action) && ($model->action == 'throw')){ echo 'selected';} ?>>
                                Throw Away</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-4" id="asset_dropdown">
                <label for="asset_id">Asset</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name="asset_id" id="asset_id" class="form-control">
                            <?php
                                foreach($assets as $key => $asset){

                                    if($model->asset_id == $key)
                                    {
                                        echo "<option value='$model->asset_id' selected>$asset</option>";
                                    }else{
                                        echo "<option value='$key'> $asset</option>";
                                    }

                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" class="form-control" id="category_id" name="category_id" value="<?= $category_id;; ?>">
            <input type="hidden" class="form-control" id="category_name" name="category_name" value="<?= $category_name;; ?>">

            <div class="col-4">
                <label for="quantity"> Quantity </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="number" id="quantity" name="quantity" value="<?php if(isset($model->quantity)) echo $model->quantity; ?>">
                    </div>
                </div>
            </div>

        </div>

        <div class="row"> 


            <div class="col-4">
                <label for="brand">Brand </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input readonly class="form-control" type="text" id="brand" name="brand" value="<?php if(isset($model->brand)) echo $model->brand; ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="asset_location">Asset Location</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='asset_location' id="asset_location" class="form-control">
                            <option value="305"
                                <?php if(isset($model->priority) && ($model->priority == '305')){ echo 'selected';} ?>>
                                305</option>
                            <option value="604"
                                <?php if(isset($model->priority) && ($model->priority == '604')){ echo 'selected';} ?>>
                                604</option>
                            <option value="605"
                                <?php if(isset($model->priority) && ($model->priority == '605')){ echo 'selected';} ?>>
                                605</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="amount"> Amount <span style="font-size:9px"> (AED) </span> </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" name="amount" id="amount" value="<?php if(isset($model->amount)) echo $model->amount; ?>" >
                    </div>
                </div>
            </div>

        </div>

        <div class="row"> 



            <div class="col-4" id="life_div">
                <label for="asset_life">Asset Life <span style="font-size:9px"> (Months)</span> </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select class="form-control" name="asset_life" id="asset_life">
                            <?php for ($i = 0; $i <= 120; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if(isset($model->asset_life) && $model->asset_life == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="accounting_entry">New/Old Purchase</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='accounting_entry' id="accounting_entry" class="form-control">
                            <option value="old"
                                <?php if(isset($model->accounting_entry) && ($model->accounting_entry == 'old')){ echo 'selected';} ?>>
                                OLD</option>
                            <option value="new"
                                <?php if(isset($model->accounting_entry) && ($model->accounting_entry == 'new')){ echo 'selected';} ?>>
                                NEW</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-4" id="payment_div">
                <label for="payment_mode">Payment Mode</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='payment_mode' id="payment_mode" class="form-control">
                            <option value="none"
                                <?php if(isset($model->payment_mode) && ($model->payment_mode == 'none')){ echo 'selected';} ?>>
                                Select</option>
                            <option value="cash"
                                <?php if(isset($model->payment_mode) && ($model->payment_mode == 'cash')){ echo 'selected';} ?>>
                                Cash</option>
                            <option value="credit"
                                <?php if(isset($model->payment_mode) && ($model->payment_mode == 'credit')){ echo 'selected';} ?>>
                                Credit</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>

        <div class="row"> 



            <div class="col-4" id="pay_bank_div">
                <label for="pay_bank"> Pay Through </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='pay_bank' id="pay_bank" class="form-control">
                            <?php
                                    foreach($bank_accounts->bankaccounts as $key => $data){
                                        // dd($model->pay_bank);
                                    if($model->pay_bank == $data->account_id)
                                    {
                                        echo "<option value='$data->account_id' selected>$data->account_name</option>";
                                    }else{
                                        echo "<option value='$data->account_id'> $data->account_name</option>";
                                    }

                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-4" id="vendor_div">
                <label for="pay_bank">Vendor </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='vendor' id="vendor" class="form-control">
                            <?php
                                    foreach($vendors->contacts as $key => $data){
                                    if($model->vendor_id == $data->contact_id)
                                    {
                                        echo "<option value='$data->contact_id".","."$data->vendor_name' selected>$data->vendor_name</option>";
                                    }else{
                                        echo "<option value='$data->contact_id".","."$data->vendor_name'> $data->vendor_name</option>";
                                    }

                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

        </div>

        <hr style="border-color:#017BFE">

        <div class="row"> 

            <div class="col-4">
                <label for="first_supplier">Supplier 1 Amount </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="text" name="first_supplier" id="first_supplier" value="<?php if(isset($model->first_supplier)) echo $model->first_supplier; ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="second_supplier">Supplier 2 Amount </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="text" name="second_supplier" id="second_supplier" value="<?php if(isset($model->second_supplier)) echo $model->second_supplier; ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="third_supplier">Supplier 3 Amount </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="text" name="third_supplier" id="third_supplier" value="<?php if(isset($model->third_supplier)) echo $model->third_supplier; ?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="row"> 
            <div class="col-4">
                <label for="first_quotation">Supplier 1 Quotation </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="file" name="image_1" id="image_1" accept="image/*">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="second_quotation">Supplier 2 Quotation </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="file" name="image_2" id="image_2" accept="image/*">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="third_quotation">Supplier 3 Quotation </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="file" name="image_3" id="image_3" accept="image/*">
                    </div>
                </div>
            </div>

        </div>

        <div class="row"> 
            <div class="col-4">
                <!-- Display the image -->
                <?php if (isset($model->image_1)) : ?>
                    <img src="<?= $model->image_1 ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                    <br>
                    <center> <a href="<?= $model->image_1 ?>" target="_blank">View</a> <center>
                    <?php endif; ?>
            </div>

            <div class="col-4">
                <!-- Display the image -->
                <?php if (isset($model->image_2)) : ?>
                    <img src="<?= $model->image_2 ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                    <br>
                    <center> <a href="<?= $model->image_2 ?>" target="_blank">View</a> <center>
                    <?php endif; ?>
            </div>

            <div class="col-4">
                <!-- Display the image -->
                <?php if (isset($model->image_3)) : ?>
                    <img src="<?= $model->image_3 ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                    <br>
                    <center> <a href="<?= $model->image_3 ?>" target="_blank">View</a> <center>
                    <?php endif; ?>
            </div>
        </div>

        <hr style="border-color:#017BFE">

        <div class="row"> 

            <div class="col-4">
                <label for="applicant">Applicant</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" value="<?= $full_name ?>" readonly>
                        <input type="hidden" class="form-control" id="applicant" name="applicant" value="<?= $user_id; ?>">
                        <!-- <select name="applicant" id="applicant" class="form-control">
                            <?php
                                foreach($clientsArr as $key => $users){

                                    if($model->applicant == $key)
                                    {
                                        echo "<option value='$model->applicant' selected>$users</option>";
                                    }else{
                                        echo "<option value='$key'> $users</option>";
                                    }

                                }
                            ?>
                        </select> -->
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="department">Department</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" value="<?= $department->title ?>" readonly>
                        <input type="hidden" class="form-control" id="department" name="department" value="<?= $department->id; ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="approved_by">Approver</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name="approved_by" id="approved_by" class="form-control">
                            <?php
                                foreach($clientsArr as $key => $users){

                                    if($model->approved_by == $key)
                                    {
                                        echo "<option value='$model->approved_by' selected>$users</option>";
                                    }else{
                                        echo "<option value='$key'> $users</option>";
                                    }

                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

        </div>

        <div class="row"> 
            <div class="col-4">
                <label for="approvers_dept">Department of Approver</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='approvers_dept' id="approvers_dept" class="form-control">
                            <option value="select"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'select')){ echo 'selected';} ?>>
                                Select</option>
                            <option value="operations"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'operations')){ echo 'selected';} ?>>
                                Operations</option>
                            <option value="finance"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'finance')){ echo 'selected';} ?>>
                                Finance</option>
                            <option value="hr"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'hr')){ echo 'selected';} ?>>
                                HR</option>
                            <option value="maxima"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'maxima')){ echo 'selected';} ?>>
                                Maxima</option>
                            <option value="administration"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'administration')){ echo 'selected';} ?>>
                                Administration</option>
                            <option value="it"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'it')){ echo 'selected';} ?>>
                                IT</option>
                            <option value="service"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'service')){ echo 'selected';} ?>>
                                New Service/Geography</option>
                            <option value="valuations"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'valuations')){ echo 'selected';} ?>>
                                Valuations</option>
                            <option value="support"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'support')){ echo 'selected';} ?>>
                                Valuations Support</option>
                            <option value="data_management"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'data_management')){ echo 'selected';} ?>>
                                Data Management</option>
                            <option value="sales"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'sales')){ echo 'selected';} ?>>
                                Sales</option>
                            <option value="marketing"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'marketing')){ echo 'selected';} ?>>
                                Marketing</option>
                            <option value="empanelment"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'empanelment')){ echo 'selected';} ?>>
                                Empanelment</option>
                            <option value="business_development"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'business_development')){ echo 'selected';} ?>>
                                Business Development</option>
                            <option value="bcs"
                                <?php if(isset($model->approvers_dept) && ($model->approvers_dept == 'bcs')){ echo 'selected';} ?>>
                                BCS</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="asset_owner">Owner of Asset</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name="asset_owner" id="asset_owner" class="form-control">
                            <?php
                                foreach($clientsArr as $key => $users){

                                    if($model->asset_owner == $key)
                                    {
                                        echo "<option value='$model->asset_owner' selected>$users</option>";
                                    }else{
                                        echo "<option value='$key'> $users</option>";
                                    }

                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <hr style="border-color:#017BFE">

        <div class="row"> 

            <div class="col-4">
                <label for="approved_date">Approved Date</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="datetime-local" id="approved_date" name="approved_date" value="<?php $dateTime = new DateTime($model->approved_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->approved_date)) {echo $formattedDateTime;} ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="purchasing_date">Date of Purchasing Order</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="datetime-local" id="purchasing_date" name="purchasing_date" value="<?php $dateTime = new DateTime($model->purchasing_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->purchasing_date)) {echo $formattedDateTime;} ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="target_date">Delivery Target Date</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="datetime-local" id="target_date" name="target_date" value="<?php $dateTime = new DateTime($model->target_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->target_date)) {echo $formattedDateTime;} ?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-4">
                <label for="collection_date">Owner Collection Date</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="datetime-local" id="collection_date" name="collection_date" value="<?php $dateTime = new DateTime($model->collection_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->collection_date)) {echo $formattedDateTime;} ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="meeting_date"> Meeting Date</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="datetime-local" id="meeting_date" name="meeting_date" value="<?php $dateTime = new DateTime($model->meeting_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->meeting_date)) {echo $formattedDateTime;} ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="start_date">Warranty Start Date</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="datetime-local" id="start_date" name="start_date" value="<?php $dateTime = new DateTime($model->start_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->start_date)) {echo $formattedDateTime;} ?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-4">
                <label for="expiry_date">Warranty Expiry Date</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="datetime-local" id="expiry_date" name="expiry_date" value="<?php $dateTime = new DateTime($model->expiry_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->expiry_date)) {echo $formattedDateTime;} ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="frequency_start">Frequency Start Date</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="datetime-local" id="frequency_start" name="frequency_start" value="<?php $dateTime = new DateTime($model->frequency_start); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->frequency_start)) {echo $formattedDateTime;} ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="frequency_end">Frequency End Date</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="datetime-local" id="frequency_end" name="frequency_end" value="<?php $dateTime = new DateTime($model->frequency_end); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->frequency_end)) {echo $formattedDateTime;} ?>">
                    </div>
                </div>
            </div>
        </div>

        <br>

        <div class="row"> 

            <div class="col-4">
                <label for="status">Status</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='status' id="status" class="form-control">
                            <option value="entered"
                                <?php if(isset($model->status) && ($model->status == 'entered')){ echo 'selected';} ?>>
                                Entered</option>
                            <option id="app_div" value="approved"
                                <?php if(isset($model->status) && ($model->status == 'approved')){ echo 'selected';} ?>>
                                Approved</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

            <!-- <div class="col-4">
                <label for="type">Type of Task</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='type' id="type" class="form-control">
                            <option value="project"
                                <?php if(isset($model->type) && ($model->type == 'project')){ echo 'selected';} ?>>
                                Project</option>
                            <option value="kpi"
                                <?php if(isset($model->type) && ($model->type == 'kpi')){ echo 'selected';} ?>>
                                KPI</option>
                        </select>
                    </div>
                </div>
            </div> -->

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if($model<>null && $model->id<>null){
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => get_class($model),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>

<?php
$this->registerJs('

$("#asset_id").change(function() {
    var val = $(this).val(); 
    $.ajax({
        url: "'.yii\helpers\Url::to(['suggestion/get_depreciation']).'/"+val,
        method: "post",
        // dataType: "html",
        success: function(data) {

            console.log(data)

            var parts = data.split(",");
            var category_id = parts[0];
            var category_name = parts[1];
            var depreciation = parts[2];
            var brand = parts[3];


            $("#category_id").val(category_id);
            $("#category_name").val(category_name);
            $("#depriciation").val(depreciation);
            $("#brand").val(brand);

        },
        error: bbAlert
    });
});

$("#expense_id").change(function() {
    var val = $(this).val(); 
    $.ajax({
        url: "'.yii\helpers\Url::to(['suggestion/get_depreciation']).'/"+val,
        method: "post",
        // dataType: "html",
        success: function(data) {

            console.log(data)

            var parts = data.split(",");
            var category_id = parts[0];
            var category_name = parts[1];
            var depreciation = parts[2];


            $("#category_id").val(category_id);
            $("#category_name").val(category_name);
            $("#depriciation").val(depreciation);

        },
        error: bbAlert
    });
});

var value = $("#accounting_entry").val();
if(value == "old")
{
    $("#payment_div").css("display", "none");
    $("#vendor_div").css("display", "none");
    $("#pay_bank_div").css("display", "none");
}

var value = $("#payment_mode").val();
if(value == "cash")
{
    $("#vendor_div").css("display", "none");
    $("#pay_bank_div").css("display", "block");
}
if(value == "credit")
{
    $("#vendor_div").css("display", "block");
    $("#pay_bank_div").css("display", "block");
}

$("#accounting_entry").change(function() {
    var val = $(this).val(); 
      if(val == "old")
      {
        $("#payment_div").css("display", "none");
        $("#vendor_div").css("display", "none");
        $("#pay_bank_div").css("display", "none");
      }
      if(val == "new"){
        $("#payment_div").css("display", "block");
        $("#vendor_div").css("display", "none");
        $("#pay_bank_div").css("display", "block");
      }
});


$("#payment_mode").change(function() {
    var val = $(this).val(); 
      if(val == "cash")
      {
        $("#pay_bank_div").css("display", "block");
        $("#vendor_div").css("display", "none");
      }
      if(val == "credit"){
        $("#vendor_div").css("display", "block");
        $("#pay_bank_div").css("display", "block");
      }
      if(val == "none"){
        $("#vendor_div").css("display", "none");
        $("#pay_bank_div").css("display", "block");
      }
});


$("#app_div").prop("disabled", true);

var value = $("#user_id").val();
if( value == "1" || value == "14" || value == "86" || value == "33" )
{
    $("#app_div").prop("disabled", false);
}

    
');
