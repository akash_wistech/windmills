<?php

use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\User;
use app\models\CategoryManager;
use app\models\AssetManager;
use app\models\Department;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GeneralAsumptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Expense Manager";
$this->params['breadcrumbs'][] = $this->title;

$cardTitle = Yii::t('app', 'Travel Expenses');


// echo Html::tag('div',
//     Html::a(
//         'Asset Register',
//         ['asset-manager/register'],
//         [
//             'class' => 'btn btn-success',
//             'style' => 'margin:10px 10px;',
//             'target' => '_blank', // Add this line to set the target
//         ]
//     ),
//     ['class' => 'text-right']
// );

// $createBtn=true;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    $createBtn=true;
}
// $actionBtns.='{view}';
// if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
//     $actionBtns.='{view}';
// }
$actionBtns.='{update}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
$actionBtns.='{delete}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
    $actionBtns.='{delete}';
}
// $actionBtns.='{get-history}';

?>
<div class="task-index">

    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        // 'createBtn' => $createBtn,

        'columns' => [

            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:1px;']],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:13%'],
                'attribute' => 'reference_number',
                'label' => Yii::t('app', 'Ref. Number'),
                'value' => function ($model) {
                    return $model->reference_number;
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:13%'],
                'attribute' => 'start_reading',
                'label' => Yii::t('app', 'Start reading'),
                'value' => function ($model) {
                    return $model->start_reading;
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:13%'],
                'attribute' => 'end_reading',
                'label' => Yii::t('app', 'End Reading'),
                'value' => function ($model) {
                    return $model->end_reading;
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'total_km',
                'label' => Yii::t('app', 'Total KM'),
                'value' => function ($model) {
                    return $model->total_km;
                },
            ],
            

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'attribute' => 'val_amount',
                'label' => Yii::t('app', 'Amount'),
                'value' => function ($model) {
                    return $model->val_amount;
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:20%'],
                'attribute' => 'transport_mode',
                'label' => Yii::t('app', 'Mode of Transport'),
                'value' => function ($model) {
                    return $model->transport_mode;
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:15%'],
                'attribute' => 'entered_date',
                'label' => Yii::t('app', 'Entered Date'),
                'value' => function ($model) {
                    if(isset($model->entered_date))
                    {
                        return date("d-M-Y", strtotime($model->entered_date)) . "<br>" . date('h:i A', strtotime($model->entered_date));
                    }else{
                        return "";
                    }
                },
            ],


            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10% ; color:#017BFE;'],
                'attribute' => 'progress',
                'label' => Yii::t('app', 'Status'),
                'value' => function ($model) {
                    return ucfirst($model->status);
                },
            ],


            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
                    <div class="dropdown-menu" role="menu">
              '.$actionBtns.'
                    </div>
                </div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>

</div>
