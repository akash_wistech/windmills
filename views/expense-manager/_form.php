<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use yii\helpers\ArrayHelper;


    $clientsArr = ArrayHelper::map(\app\models\User::find()
    ->select([
    'id', 'CONCAT(firstname, " ", lastname) AS lastname',
    ])
        ->where(['status' => 1])
        ->andWhere(['user_type' => [10,20]])
        ->orderBy(['lastname' => SORT_ASC,])
        ->all(), 'id', 'lastname');
    $clientsArr = ['' => 'select'] + $clientsArr;

    if(isset($model))
    {
        $user_detail = \app\models\User::findOne(['id' => $model->applicant]);
    }else{
        $user_id = Yii::$app->user->identity->id;
        $user_detail = \app\models\User::findOne(['id' => $user_id]);
    }

    $user_profile_detail = \app\models\UserProfileInfo::findOne(['id' => $user_detail->id]);
    $department = \app\models\Department::findOne(['id' => $user_profile_detail->department_id]);
    $full_name = $user_detail->firstname.' '.$user_detail->lastname;


    // Banking Channels

    // Yii::$app->appHelperFunctions->ZohoRefereshToken();
    // $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');
    // $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');

    // $curl = curl_init();
    // curl_setopt_array($curl, array(
    // CURLOPT_URL => 'https://www.zohoapis.com/books/v3/bankaccounts?organization_id='.$OrganizationID.'&filter_by=Status.Active',
    // CURLOPT_RETURNTRANSFER => true,
    // CURLOPT_ENCODING => '',
    // CURLOPT_MAXREDIRS => 10,
    // CURLOPT_TIMEOUT => 0,
    // CURLOPT_FOLLOWLOCATION => true,
    // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    // CURLOPT_CUSTOMREQUEST => 'GET',
    // CURLOPT_HTTPHEADER => array(
    //     'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
    // ),
    // ));

    // $response = curl_exec($curl);
    // curl_close($curl);
    // $bank_accounts = json_decode($response);

?>

<input type="hidden" id="user_id" value="<?= $user_id ?>">
<section class="contact-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <header class="card-header">
        <h2 class="card-title"><?php if(isset($page_title)) {echo $page_title;}else{echo "Add Travel Expense";} ?></h2>
    </header>
    <div class="card-body">
        <div class="row">

            <div class="col-4">
                <label for="department">Department</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" value="<?= $department->title ?>" readonly>
                        <input type="hidden" class="form-control" id="department" name="department" value="<?= $department->id; ?>">
                    </div>
                </div>
            </div>

            <?php if($department->title == "Valuations") { ?>
                <div class="col-4">
                    <label for="reference_number">Reference Number</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                        <select id="reference_number" name="reference_number" class="form-control select2">
                            <option value="">Select Reference Number</option>
                            <?php $referenceListArr = Yii::$app->appHelperFunctions->referenceListArr; ?>
                            <?php foreach ($referenceListArr as $value => $label): ?>
                                <option value="<?= htmlspecialchars($value, ENT_QUOTES, 'UTF-8') ?>" 
                                    <?= $value == $model->reference_number ? 'selected' : '' ?>>
                                    <?= htmlspecialchars($label, ENT_QUOTES, 'UTF-8') ?>
                                </option>
                            <?php endforeach; ?>
                        </select>

                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="start_reading">Start Reading</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input type="number" step="0.01" id ="start_reading" name="start_reading" class="form-control" value="<?php if(isset($model->start_reading)) echo $model->start_reading; ?>" >
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="end_reading">End Reading</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input type="number" step="0.01" id ="end_reading" name="end_reading" class="form-control" value="<?php if(isset($model->end_reading)) echo $model->end_reading; ?>" >
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="start_date">Start Date Time</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input type="datetime-local" id ="start_date" name="start_date" class="form-control" value="<?php if(isset($model->start_date)) echo $model->start_date; ?>" >
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="end_date">End Date Time</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input type="datetime-local" id ="end_date" name="end_date" class="form-control" value="<?php if(isset($model->end_date)) echo $model->end_date; ?>" >
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="total_km">Total KM's Travelled</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input readonly type="number" id ="total_km" name="total_km" class="form-control" value="<?php if(isset($model->total_km)) echo $model->total_km; ?>" >
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="val_amount"> Amount <span style="font-size:9px"> (AED) </span> </label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input readonly type="text" class="form-control" name="val_amount" id="val_amount" value="<?php if(isset($model->val_amount)) echo $model->val_amount; ?>" >
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="transport_mode">Mode of Transport</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name='transport_mode' id="transport_mode" class="form-control">
                            <option value=""
                                <?php if(isset($model->transport_mode) && ($model->transport_mode == '')){ echo 'selected';} ?>>
                                Select</option>    
                            <option value="company"
                                <?php if(isset($model->transport_mode) && ($model->transport_mode == 'company')){ echo 'selected';} ?>>
                                Company Transport</option>
                            <option value="personal"
                                <?php if(isset($model->transport_mode) && ($model->transport_mode == 'personal')){ echo 'selected';} ?>>
                                Personal Transport</option>
                            <option value="external"
                                <?php if(isset($model->transport_mode) && ($model->transport_mode == 'external')){ echo 'selected';} ?>>
                                External Transport</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4" id="car_type_div">
                    <label for="car_type">Car Type</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name='car_type' id="car_type" class="form-control">
                            <option value=""
                                    <?php if(isset($model->car_type) && ($model->car_type == '')){ echo 'selected';} ?>>
                                    Select</option>    
                            <option value="sunny"
                                    <?php if(isset($model->car_type) && ($model->car_type == 'sunny')){ echo 'selected';} ?>>
                                    Nissan Sunny</option>
                                <option value="lancer"
                                    <?php if(isset($model->car_type) && ($model->car_type == 'lancer')){ echo 'selected';} ?>>
                                    mitsubishi lancer</option>
                            </select>
                        </div>
                    </div>
                </div>

            <?php } ?>

            <div class="col-4">
                <label for="asset_location">Location</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='asset_location' id="asset_location" class="form-control">
                            <option value=""
                                <?php if(isset($model->asset_location) && ($model->asset_location == '')){ echo 'selected';} ?>>
                                Select</option>
                            <option value="dubai"
                                <?php if(isset($model->asset_location) && ($model->asset_location == 'dubai')){ echo 'selected';} ?>>
                                Dubai</option>
                            <option value="ajman"
                                <?php if(isset($model->asset_location) && ($model->asset_location == 'ajman')){ echo 'selected';} ?>>
                                Ajman</option>
                            <option value="abudhabi"
                                <?php if(isset($model->asset_location) && ($model->asset_location == 'abudhabi')){ echo 'selected';} ?>>
                                Abu Dhabi</option>
                        </select>
                    </div>
                </div>
            </div>

            <?php if($department->title != "Valuations")  {?> 
            <div class="col-4">
                <label for="amount"> Amount <span style="font-size:9px"> (AED) </span> </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" name="amount" id="amount" value="<?php if(isset($model->amount)) echo $model->amount; ?>" >
                    </div>
                </div>
            </div>
            <?php } ?>

            <div class="col-4">
                <label for="purpose"> Purpose <span style="font-size:9px"> (AED) </span> </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" name="purpose" id="purpose" value="<?php if(isset($model->purpose)) echo $model->purpose; ?>" >
                    </div>
                </div>
            </div>

            <div class="col-4" id="pay_bank_div">
                <label for="pay_bank"> Pay Through </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='pay_bank' id="pay_bank" class="form-control">
                            <?php
                                    foreach($bank_accounts->bankaccounts as $key => $data){
                                    if($model->pay_bank == $data->account_id)
                                    {
                                        echo "<option value='$data->account_id' selected>$data->account_name</option>";
                                    }else{
                                        echo "<option value='$data->account_id'> $data->account_name</option>";
                                    }

                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <?php if($department->title == "Valuations") { ?>
        <hr style="border-color:#017BFE">

        <div class="row"> 
            <div class="col-4">
                <label for="car_image">Car Image </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="file" name="car_image" id="car_image" accept="image/*">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="start_image">Journey Start Image </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="file" name="start_image" id="start_image" accept="image/*">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="end_image">Journey End Image </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="file" name="end_image" id="end_image" accept="image/*">
                    </div>
                </div>
            </div>

        </div>

        <div class="row"> 
            <div class="col-4">
                <!-- Display the image -->
                <?php if (isset($model->image_1)) : ?>
                    <img src="<?= $model->image_1 ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                    <br>
                    <center> <a href="<?= $model->image_1 ?>" target="_blank">View</a> <center>
                    <?php endif; ?>
            </div>

            <div class="col-4">
                <!-- Display the image -->
                <?php if (isset($model->image_2)) : ?>
                    <img src="<?= $model->image_2 ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                    <br>
                    <center> <a href="<?= $model->image_2 ?>" target="_blank">View</a> <center>
                    <?php endif; ?>
            </div>

            <div class="col-4">
                <!-- Display the image -->
                <?php if (isset($model->image_3)) : ?>
                    <img src="<?= $model->image_3 ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                    <br>
                    <center> <a href="<?= $model->image_3 ?>" target="_blank">View</a> <center>
                    <?php endif; ?>
            </div>
        </div>
        <?php } ?>

        <hr style="border-color:#017BFE">

        <div class="row"> 

            <div class="col-4">
                <label for="applicant">Applicant</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" value="<?= $full_name ?>" readonly>
                        <input type="hidden" class="form-control" id="applicant" name="applicant" value="<?= $user_detail->id; ?>">
                        <!-- <select name="applicant" id="applicant" class="form-control">
                            <?php
                                foreach($clientsArr as $key => $users){

                                    if($model->applicant == $key)
                                    {
                                        echo "<option value='$model->applicant' selected>$users</option>";
                                    }else{
                                        echo "<option value='$key'> $users</option>";
                                    }
                                }
                            ?>
                        </select> -->
                    </div>
                </div>
            </div>

        </div>

        <hr style="border-color:#017BFE">

        <br>

        <div class="row">
            <div class="col-4">
                <label for="status">Status</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='status' id="status" class="form-control">
                            <option value="entered"
                                <?php if(isset($model->status) && ($model->status == 'entered')){ echo 'selected';} ?>>
                                Entered</option>
                            <option id="app_div" value="approved"
                                <?php if(isset($model->status) && ($model->status == 'approved')){ echo 'selected';} ?>>
                                Approved</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

            <!-- <div class="col-4">
                <label for="type">Type of Task</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='type' id="type" class="form-control">
                            <option value="project"
                                <?php if(isset($model->type) && ($model->type == 'project')){ echo 'selected';} ?>>
                                Project</option>
                            <option value="kpi"
                                <?php if(isset($model->type) && ($model->type == 'kpi')){ echo 'selected';} ?>>
                                KPI</option>
                        </select>
                    </div>
                </div>
            </div> -->

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index-travel'], ['class' => 'btn btn-default']) ?>
        <?php
        if($model<>null && $model->id<>null){
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => get_class($model),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
    
</section>


<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<?php
$this->registerJs('

$("#asset_id").change(function() {
    var val = $(this).val(); 
    $.ajax({
        url: "'.yii\helpers\Url::to(['suggestion/get_depreciation']).'/"+val,
        method: "post",
        // dataType: "html",
        success: function(data) {

            var parts = data.split(",");
            var category_id = parts[0];
            var category_name = parts[1];
            var depreciation = parts[2];

            $("#category_id").val(category_id);
            $("#category_name").val(category_name);
            $("#depriciation").val(depreciation);
        },
        error: bbAlert
    });
});

$("#expense_id").change(function() {
    var val = $(this).val(); 
    $.ajax({
        url: "'.yii\helpers\Url::to(['suggestion/get_depreciation']).'/"+val,
        method: "post",
        // dataType: "html",
        success: function(data) {

            var parts = data.split(",");
            var category_id = parts[0];
            var category_name = parts[1];
            var depreciation = parts[2];


            $("#category_id").val(category_id);
            $("#category_name").val(category_name);
            $("#depriciation").val(depreciation);

        },
        error: bbAlert
    });
});

var value = $("#accounting_entry").val();
if(value == "old")
{
    $("#payment_div").css("display", "none");
    $("#vendor_div").css("display", "none");
    $("#pay_bank_div").css("display", "none");
}

var value = $("#payment_mode").val();
if(value == "cash")
{
    $("#vendor_div").css("display", "none");
    $("#pay_bank_div").css("display", "block");
}
if(value == "credit")
{
    $("#vendor_div").css("display", "block");
    $("#pay_bank_div").css("display", "block");
}

$("#accounting_entry").change(function() {
    var val = $(this).val(); 
      if(val == "old")
      {
        $("#payment_div").css("display", "none");
        $("#vendor_div").css("display", "none");
        $("#pay_bank_div").css("display", "none");
      }
      if(val == "new"){
        $("#payment_div").css("display", "block");
        $("#vendor_div").css("display", "none");
        $("#pay_bank_div").css("display", "block");
      }
});


$("#payment_mode").change(function() {
    var val = $(this).val(); 
      if(val == "cash")
      {
        $("#pay_bank_div").css("display", "block");
        $("#vendor_div").css("display", "none");
      }
      if(val == "credit"){
        $("#vendor_div").css("display", "block");
        $("#pay_bank_div").css("display", "block");
      }
      if(val == "none"){
        $("#vendor_div").css("display", "none");
        $("#pay_bank_div").css("display", "block");
      }
});


$("#app_div").prop("disabled", true);

var value = $("#user_id").val();
if( value == "1" || value == "14" || value == "86" || value == "33" || value == "21" )
{
    $("#app_div").prop("disabled", false);
}

$("#start_reading").on("keyup", function() {
    var start = this.value;
    var end = $("#end_reading").val();
    var total = (end-start).toFixed(2);
    $("#total_km").val(total);
    $("#val_amount").val((total*1.1).toFixed(2));
});

$("#end_reading").on("keyup", function() {
    var end = this.value;
    var start = $("#start_reading").val();
    var total = (end-start).toFixed(2);
    $("#total_km").val(total);
    $("#val_amount").val((total*1.1).toFixed(2));
});


$("#car_type_div").hide();
$("#transport_mode").change(function() {
    var current = $("#transport_mode").val();
    if(current == "company")
    {
        $("#car_type_div").show();
    }else{
        $("#car_type_div").hide(); 
    }
});

// $("#reference_number").select2({
//     placeholder: "Select Reference Number",
//     allowClear: true
// });


    
');
