<?php

use yii\helpers\Html;

$this->title = 'Add Asset';
$this->params['breadcrumbs'][] = ['label' => 'Expense Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-create">

    <?= $this->render('_form_asset', [
        'model' => $model,
    ]) ?>

</div>
