<?php

use yii\helpers\Html;

$this->title = 'Add Consumable';
$this->params['breadcrumbs'][] = ['label' => 'Expense Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consumable-create">

    <?= $this->render('_form_consumable', [
        'model' => $model,
    ]) ?>

</div>
