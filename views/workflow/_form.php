<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomPjax;
use app\assets\WorkflowFormAsset;
WorkflowFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Workflow */
/* @var $form yii\widgets\ActiveForm */

$stageRank=[];
$servicesListId = Yii::$app->appHelperFunctions->getSetting('service_list_id');
?>
<style>
#workflow-assigned_modules label{width: 20%;}
#stages-container .card-body{padding: 0.75rem;}
#stages-container .card-body .form-group{margin-bottom:0;}
#stages-container .card-body .act-btn .btn{margin:15px 0 10px;padding: 0.125rem .5rem;}
</style>
<section class="workflows-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(); ?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-8">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'completion_day',['template'=>'
        {label}
        <div class="input-group input-group-md">
          {input}
          <div class="input-group-append">
            <span class="input-group-text">'.Yii::t('app','Day(s)').'</span>
          </div>
        </div>
        {error}{hint}
        '])->textInput(['maxlength' => true])?>
      </div>
    </div>
    <?= $form->field($model, 'assigned_modules')->checkboxList(Yii::$app->appHelperFunctions->workFlowModulesListArr)?>
    <?= $form->field($model, 'assigned_services')->checkboxList(Yii::$app->appHelperFunctions->getPredefinedListOptionsArr($servicesListId))?>
    <section class="card card-outline card-info">
      <header class="card-header">
        <h2 class="card-title"><?= Yii::t('app','Stages')?></h2>
        <div class="card-tools">
          <button type="button" class="btn btn-success btn-xs" onclick="addStage();">
            <i class="fas fa-plus"></i>
          </button>
        </div>
      </header>
      <?php CustomPjax::begin(['id'=>'gc-2']); ?>
      <div class="card-body">
        <ul id="stages-container">
        <?php
        $n=1;
        if(Yii::$app->request->post()){
        ?>
        <?php
        }else{
        ?>
          <?php if($model->stages!=null){?>
          <?php
            foreach($model->stages as $stage){
              $stageRank[]=$stage->id;
              $model->s_id[$n]=$stage->id;
              $model->s_name[$n]=$stage->title;
              $model->s_color[$n]=$stage->color_code;
              $model->s_descp[$n]=$stage->descp;
          ?>
          <li id="<?= $stage->id?>">
            <section id="stage-<?= $n?>" class="card card-outline card-success">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="hidden">
                      <?= $form->field($model, 's_id['.$n.']')->textInput(['maxlength' => true])->label(false)?>
                    </div>
                    <?= $form->field($model, 's_name['.$n.']')->textInput(['placeholder'=>$model->getAttributeLabel('s_name'),'maxlength' => true])?>
                  </div>
                  <div class="col-sm-1">
                    <?= $form->field($model, 's_color['.$n.']')->textInput(['placeholder'=>$model->getAttributeLabel('s_color'),'maxlength' => true, 'class'=>'form-control colorpicker'])?>
                  </div>
                  <div class="col-sm-6">
                    <?= $form->field($model, 's_descp['.$n.']')->textInput(['placeholder'=>$model->getAttributeLabel('s_descp'),'maxlength' => true])?>
                  </div>
                  <div class="col-sm-1 act-btn text-center">
                    <a href="javascript:;" class="btn btn-md btn-danger aa-confirm-action" data-cmsg="<?= Yii::t('app','Are you sure you want to delete this?')?>" data-url="<?= Url::to(['delete-stage','id'=>$stage->id])?>" data-container="gc-2">
                      <i class="fa fa-times"></i>
                    </a>
                  </div>
                </div>
              </div>
            </section>
          </li>
          <?php
            $n++;
            }
          ?>
          <?php
          }else{?>
          <li id="<?= $n?>">
            <section class="card card-outline card-success">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-4">
                    <?= $form->field($model, 's_name['.$n.']')->textInput(['placeholder'=>$model->getAttributeLabel('s_name'),'maxlength' => true])?>
                  </div>
                  <div class="col-sm-1">
                    <?= $form->field($model, 's_color['.$n.']')->textInput(['placeholder'=>$model->getAttributeLabel('s_color'),'maxlength' => true, 'class'=>'form-control colorpicker'])?>
                  </div>
                  <div class="col-sm-6">
                    <?= $form->field($model, 's_descp['.$n.']')->textInput(['placeholder'=>$model->getAttributeLabel('s_descp'),'maxlength' => true])?>
                  </div>
                  <div class="col-sm-1 act-btn">

                  </div>
                </div>
              </div>
            </section>
          </li>
        <?php
          }
        }
        ?>
        </ul>
      </div>
      <?php CustomPjax::end(); ?>
    </section>
  </div>
  <input id="stageRank" name="Workflow[s_sort_order]" type="hidden" value="<?= implode(",",$stageRank)?>" />
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
<?= $this->render('js/form_scripts',['model'=>$model,'n'=>$n]);?>
