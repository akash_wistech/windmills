<?php
$this->registerJs('
$("#stages-container").sortable({
  revert: 100,
  placeholder: "placeholder",
	update: function (event, ui) {
    var data = $(this).sortable("toArray");
		$("#stageRank").val(data.join(","));
  }
});
$("#sortable").sortable({
  revert: 100,
});
initColorPicker();
');
?>
<style>
ul#stages-container{
    width: 100%;
    margin: 0;
    padding: 0;
    list-style: none;
    position: relative !important;
}

ul#stages-container li {
    cursor: move;
}

ul#stages-container li.ui-sortable-helper {
    border-color: #3498db;
}

ul#stages-container li.placeholder {
    height: 50px;
    background: #eee;
    border: 2px dashed #bbb;
    display: block;
    opacity: 0.6;
    border-radius: 2px;
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
}
</style>
<script>
var sRows=<?= $n?>;
function addStage()
{
  sRows++;
  html ='';
  html+='<li>';
  html+='<section id="stage-'+sRows+'" class="card card-outline card-success">';
  html+=' <div class="card-body">';
  html+='   <div class="row">';
  html+='     <div class="col-sm-4">';
  html+='       <div class="form-group field-workflow-s_name-'+sRows+'">';
  html+='         <label class="control-label" for="workflow-s_name-'+sRows+'"><?= $model->getAttributeLabel('s_name')?></label>';
  html+='         <input type="text" id="workflow-s_name-'+sRows+'" class="form-control" name="Workflow[s_name]['+sRows+']" placeholder="<?= $model->getAttributeLabel('s_name')?>">';
  html+='       </div>';
  html+='     </div>';
  html+='     <div class="col-sm-1">';
  html+='       <div class="form-group field-workflow-s_color-'+sRows+'">';
  html+='         <label class="control-label" for="workflow-s_name-'+sRows+'"><?= $model->getAttributeLabel('s_color')?></label>';
  html+='         <input type="text" id="workflow-s_color'+sRows+'" class="form-control colorpicker" name="Workflow[s_color]['+sRows+']" placeholder="<?= $model->getAttributeLabel('s_color')?>">';
  html+='       </div>';
  html+='     </div>';
  html+='     <div class="col-sm-6">';
  html+='       <div class="form-group field-workflow-s_descp-'+sRows+'">';
  html+='         <label class="control-label" for="workflow-s_name-'+sRows+'"><?= $model->getAttributeLabel('s_descp')?></label>';
  html+='         <input type="text" id="workflow-s_descp'+sRows+'" class="form-control" name="Workflow[s_descp]['+sRows+']" placeholder="<?= $model->getAttributeLabel('s_descp')?>">';
  html+='       </div>';
  html+='     </div>';
  html+='     <div class="col-sm-1 act-btn text-center">';
  html+='       <a href="javascript:;" onclick="removeStage('+sRows+')" class="btn btn-md btn-danger">';
  html+='         <i class="fa fa-times"></i>';
  html+='       </a>';
  html+='     </div>';
  html+='   </div>';
  html+=' </div>';
  html+='</section>';
  html+='</li>';
  $("#stages-container").append(html);
  initColorPicker();
}
function removeStage(r)
{
  $("#stage-"+r).parent("li").remove();
}
function initColorPicker()
{
  $(".colorpicker").spectrum({preferredFormat: "hex",});
}
</script>
