<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Workflow */

$this->title = Yii::t('app', 'Process');
$cardTitle = Yii::t('app','View Process:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');
?>

<div class="workflow-view">
  <section class="card card-outline card-primary">
    <header class="card-header">
      <h2 class="card-title"><?= $model->title?></h2>
      <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){?>
      <div class="card-tools">
        <a href="<?= Url::to(['update','id'=>$model['id']])?>" class="btn btn-tool">
          <i class="fas fa-edit"></i>
        </a>
      </div>
      <?php }?>
    </header>
    <div class="card-body">
      <?= $this->render('/shared/pending')?>
    </div>
  </section>
</div>
