<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\WorkflowListAsset;
WorkflowListAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkflowSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Process');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$modulesList=Yii::$app->appHelperFunctions->moduleTypeListArr;

$servicesListId = Yii::$app->appHelperFunctions->getSetting('service_list_id');
$servicesList=Yii::$app->appHelperFunctions->getPredefinedListOptionsArr($servicesListId);

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
  $actionBtns.='{view}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
  $actionBtns.='{status}';
}
$this->registerJs('
initScripts();
$(document).on("pjax:success", function() {
  initScripts();
});
');
?>
<div class="lead-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'cardTitle' => $cardTitle,
    'createBtn' => $createBtn,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
      ['format'=>'raw','attribute'=>'title','value'=>function($model){
        if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
          return Html::a($model['title'],['view','id'=>$model['id']],['data-pjax'=>'0']);
        }else{
          return $model['title'];
        }
      }],
      ['attribute'=>'module_id','label'=>Yii::t('app','Module'),'value'=>function($model) use ($modulesList){
        return implode(", ",Yii::$app->appHelperFunctions->getWorkflowModuleNames($model['id']));
      },'filter'=>$modulesList],
      ['attribute'=>'service_type_id','label'=>Yii::t('app','Services'),'value'=>function($model) use ($modulesList){
        return implode(", ",Yii::$app->appHelperFunctions->getWorkflowServiceTypeNames($model['id']));
      },'filter'=>$servicesList],
      ['attribute'=>'stage_name','label'=>Yii::t('app','Stages'),'value'=>function($model) use ($modulesList){
        return implode(", ",Yii::$app->appHelperFunctions->getWorkflowStageNames($model['id']));
      }],
      'completion_day',
      ['attribute'=>'created_at','label'=>Yii::t('app', 'Created'),'format'=>'datetime','filterInputOptions'=>['class'=>'form-control dtrpicker','autocomplete'=>'off']],
      [
        'class' => 'yii\grid\ActionColumn',
        'header'=>'',
        'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
        'contentOptions'=>['class'=>'noprint actions'],
        'template' => '
          <div class="btn-group flex-wrap">
            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
            <div class="dropdown-menu" role="menu">
              '.$actionBtns.'
            </div>
          </div>',
        'buttons' => [
            'view' => function ($url, $model) {
              return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                'title' => Yii::t('app', 'View'),
                'class'=>'dropdown-item text-1',
                'data-pjax'=>"0",
              ]);
            },
            'update' => function ($url, $model) {
              return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                'title' => Yii::t('app', 'Edit'),
                'class'=>'dropdown-item text-1',
                'data-pjax'=>"0",
              ]);
            },
            'delete' => function ($url, $model) {
              return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                'title' => Yii::t('app', 'Delete'),
                'class'=>'dropdown-item text-1',
                'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                'data-method'=>"post",
                'data-pjax'=>"0",
              ]);
            },
        ],
      ],
    ],
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<script>
function initScripts(){
  if($(".dtrpicker").length>0){
    $(".dtrpicker").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
    $(".dtrpicker").on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
      $(this).trigger("change");
    });
    $(".dtrpicker").on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });
  }
}
</script>
