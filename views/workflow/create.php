<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Workflow */

$this->title = Yii::t('app', 'Process');
$cardTitle = Yii::t('app','New Process');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="process-create">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
