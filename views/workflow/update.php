<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Workflow */

$this->title = Yii::t('app', 'Process');
$cardTitle = Yii::t('app','Update Process:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="process-update">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
