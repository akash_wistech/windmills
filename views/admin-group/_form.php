<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdminGroup */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('
$("body").on("click", ".cbl1", function () {
  thisid=$(this).attr("value");
  $(".cbl1_"+thisid).prop("checked",$(this).prop("checked"));
});
$("body").on("click", ".cbl2", function () {
  thisid=$(this).attr("value");
  $(".cbl2_"+thisid).prop("checked",$(this).prop("checked"));
});
$("body").on("click", ".cbl3", function () {
  thisid=$(this).attr("value");
  $(".cbl3_"+thisid).prop("checked",$(this).prop("checked"));
});
$("body").on("click", ".selAll", function () {
  thisid=$(this).data("id");
  $(".cbl1_"+thisid).prop("checked",true);
});
$("body").on("click", ".unselAll", function () {
  thisid=$(this).data("id");
  $(".cbl1_"+thisid).prop("checked",false);
});

$(document).ready(function(){
    $(".card-header").click(function(){
      // Toggle the collapse class for the corresponding collapsible content
      $(this).next(".collapse").toggleClass("show");
      $(this).toggleClass("collapsed");
    });
  });
  
');

$menuLinks=Yii::$app->menuHelperFunction->adminMenuList;
$arrListType=Yii::$app->helperFunctions->permissionListingType;
?>
<style>
.admin-group-form .card-header label{margin-bottom: 0px;}
.admin-group-form .card .card-header .card-title{padding-top: 7px;}
.admin-group-form .card-header input{margin-right: 5px;}
.admin-group-form .card .card-header .card-tools .form-group{margin-bottom: 0px;}
.admin-group-form .card-body{padding: .75rem .75rem .35rem;}
.admin-group-form .card .card-body label{margin-right: 20px;}

.card-header .card-tools {
    float: right;
    margin-right: 1.375rem;
}

.arrow-icon {
  float: right;
  transition: transform 0.3s ease;
}

.collapsed .arrow-icon {
  transform: rotate(-90deg);
}


</style>


<section class="admin-group-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(); ?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
    <div class="card-body">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
        <div class="row">
            <?php
            $n=1;
                foreach($menuLinks as $menuLink) {
                    // if ($menuLink['id'] != 208) {
                        $boxSelected = Yii::$app->menuHelperFunction->isChecked($model->id, $menuLink['id']);
                        $model->list_type[$menuLink['id']] = Yii::$app->menuHelperFunction->getListingType($model->id, $menuLink['id']);
                        ?>
                        <div class="col-12">
                            <div class="card card-info" style="border: 1px solid #17a2b8;">
                                <div class="card-header" id="headingOne">
                                    <h3 class="card-title">
                                        <label><input type="checkbox"<?= ($boxSelected == true ? ' checked="checked"' : '') ?>
                                                        class="cbl1" name="AdminGroup[actions][]"
                                                        value="<?= $menuLink['id'] ?>"/> <?= $menuLink['title'] ?></label>
                                    </h3>

                                    <h5 class="mb-0">
                                    <span class="arrow-icon">&#9660;</span>
                                    </h5>

                                    <?php if ($menuLink['ask_list_type'] == 1 && $menuLink['action_id'] == 'index') { ?>
                                        <div class="card-tools">
                                            <?= $form->field($model, 'list_type[' . $menuLink['id'] . ']')->dropDownList($arrListType, ['class' => 'form-control form-control-sm'])->label(false) ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                    <div id="collapseOne" class="collapse hide" aria-labelledby="headingOne">
                                        <div class="card-body">
                                            <?php
                                            $secondLevelOptions = Yii::$app->menuHelperFunction->getSubOptions($menuLink['id']);
                                            if ($secondLevelOptions != null) {
                                                foreach ($secondLevelOptions as $secondLevelOption) {
                                                    $boxSelected = Yii::$app->menuHelperFunction->isChecked($model->id, $secondLevelOption['id']);
                                                    $thirdLevelOptions = Yii::$app->menuHelperFunction->getSubOptions($secondLevelOption['id']);
                                                    if ($thirdLevelOptions != null) {
                                                        $model->list_type[$secondLevelOption['id']] = Yii::$app->menuHelperFunction->getListingType($model->id, $secondLevelOption['id']);
                                                        ?>
                                                            <div class="card card-success" style="border: 1px solid #28a745;">
                                                                <div class="card-header" id="headingTwo">
                                                                    <h3 class="card-title">
                                                                        <label><input
                                                                                    type="checkbox"<?= ($boxSelected == true ? ' checked="checked"' : '') ?>
                                                                                    class="cbl2 cbl1_<?= $menuLink['id'] ?>"
                                                                                    name="AdminGroup[actions][]"
                                                                                    value="<?= $secondLevelOption['id'] ?>"/> <?= $secondLevelOption['title'] ?>
                                                                        </label>
                                                                    </h3>

                                                                    <h5 class="mb-0">
                                                                    <span class="arrow-icon">&#9660;</span>
                                                                    </h5>


                                                                    <?php if ($secondLevelOption['ask_list_type'] == 1 && $secondLevelOption['action_id'] == 'index') { ?>
                                                                        <div class="card-tools">
                                                                            <?= $form->field($model, 'list_type[' . $secondLevelOption['id'] . ']')->dropDownList($arrListType, ['class' => 'form-control form-control-sm'])->label(false) ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                                    <div id="collapseOne" class="collapse hide" aria-labelledby="headingTwo">
                                                                        <div class="card-body">
                                                                            <?php
                                                                            foreach ($thirdLevelOptions as $thirdLevelOption) {
                                                                                $boxSelected = Yii::$app->menuHelperFunction->isChecked($model->id, $thirdLevelOption['id']);
                                                                                $fourthLevelOptions = Yii::$app->menuHelperFunction->getSubOptions($thirdLevelOption['id']);
                                                                                if ($fourthLevelOptions != null) {
                                                                                    $model->list_type[$thirdLevelOption['id']] = Yii::$app->menuHelperFunction->getListingType($model->id, $thirdLevelOption['id']);
                                                                                    ?>
                                                                                    <div class="card card-warning" style="border: 1px solid #ffc107;">
                                                                                        <div class="card-header" id="headingThree">
                                                                                            <h3 class="card-title">
                                                                                                <label>
                                                                                                    <input type="checkbox"<?= ($boxSelected == true ? ' checked="checked"' : '') ?>
                                                                                                            class="cbl3 cbl1_<?= $menuLink['id'] ?> cbl2_<?= $secondLevelOption['id'] ?>"
                                                                                                            name="AdminGroup[actions][]"
                                                                                                            value="<?= $thirdLevelOption['id'] ?>"/>
                                                                                                    <?= $thirdLevelOption['title'] ?>
                                                                                                </label>
                                                                                            </h3>

                                                                                            <h5 class="mb-0">
                                                                                            <span class="arrow-icon">&#9660;</span>
                                                                                            </h5>

                                                                                            <?php if ($thirdLevelOption['ask_list_type'] == 1 && $thirdLevelOption['action_id'] == 'index') { ?>
                                                                                                <div class="card-tools">
                                                                                                    <?= $form->field($model, 'list_type[' . $thirdLevelOption['id'] . ']')->dropDownList($arrListType, ['class' => 'form-control form-control-sm'])->label(false) ?>
                                                                                                </div>
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                            <div id="collapseOne" class="collapse hide" aria-labelledby="headingThree">
                                                                                                <div class="card-body">
                                                                                                    <?php
                                                                                                    foreach ($fourthLevelOptions as $fourthLevelOption) {
                                                                                                        $boxSelected = Yii::$app->menuHelperFunction->isChecked($model->id, $fourthLevelOption['id']);


                                                                                                        // NEW CODE

                                                                                                            $fifthLevelOptions = Yii::$app->menuHelperFunction->getSubOptions($fourthLevelOption['id']);
                                                                                                            if ($fifthLevelOptions != null) {
                                                                                                                $model->list_type[$fourthLevelOption['id']] = Yii::$app->menuHelperFunction->getListingType($model->id, $fourthLevelOption['id']);
                                                                                                                ?>
                                                                                                                <div class="card card-warning" style="border: 1px solid #ffc107;">
                                                                                                                    <div class="card-header">
                                                                                                                        <h3 class="card-title">
                                                                                                                            <label>
                                                                                                                                <input type="checkbox"<?= ($boxSelected == true ? ' checked="checked"' : '') ?>
                                                                                                                                        class="cbl3 cbl1_<?= $menuLink['id'] ?> cbl2_<?= $thirdLevelOption['id'] ?>"
                                                                                                                                        name="AdminGroup[actions][]"
                                                                                                                                        value="<?= $fourthLevelOption['id'] ?>"/>
                                                                                                                                <?= $fourthLevelOption['title'] ?>
                                                                                                                            </label>
                                                                                                                        </h3>

                                                                                                                        <h5 class="mb-0">
                                                                                                                        <span class="arrow-icon">&#9660;</span>
                                                                                                                        </h5>
                                                                                                                        
                                                                                                                        <?php if ($fourthLevelOption['ask_list_type'] == 1 && $fourthLevelOption['action_id'] == 'index') { ?>
                                                                                                                            <div class="card-tools">
                                                                                                                                <?= $form->field($model, 'list_type[' . $fourthLevelOption['id'] . ']')->dropDownList($arrListType, ['class' => 'form-control form-control-sm'])->label(false) ?>
                                                                                                                            </div>
                                                                                                                        <?php } ?>
                                                                                                                    </div>
                                                                                                                    <div class="card-body">
                                                                                                                        <?php
                                                                                                                        foreach ($fifthLevelOptions as $fifthLevelOption) {
                                                                                                                            $boxSelected = Yii::$app->menuHelperFunction->isChecked($model->id, $fifthLevelOption['id']);
                                                                                                                            ?>
                                                                                                                            <label>
                                                                                                                                <input type="checkbox"<?= ($boxSelected == true ? ' checked="checked"' : '') ?>
                                                                                                                                        class="cbl1_<?= $menuLink['id'] ?> cbl2_<?= $fourthLevelOption['id'] ?> cbl3_<?= $fourthLevelOption['id'] ?>"
                                                                                                                                        data-parent="" name="AdminGroup[actions][]"
                                                                                                                                        value="<?= $fifthLevelOption['id'] ?>"/>
                                                                                                                                <?= $fifthLevelOption['title'] ?>
                                                                                                                            </label>
                                                                                                                            <?php
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <?php
                                                                                                            } else {
                                                                                                                ?>
                                                                                                                <label>
                                                                                                                    <input type="checkbox"<?= ($boxSelected == true ? ' checked="checked"' : '') ?>
                                                                                                                            class="cbl1_<?= $menuLink['id'] ?> cbl2_<?= $thirdLevelOption['id'] ?>"
                                                                                                                            data-parent="" name="AdminGroup[actions][]"
                                                                                                                            value="<?= $fourthLevelOption['id'] ?>"/>
                                                                                                                    <?= $fourthLevelOption['title'] ?>
                                                                                                                </label>
                                                                                                                <?php
                                                                                                            }

                                                                                                        // END NEW CODE

                                                                                                        ?>

                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </div>
                                                                                            </div>
                                                                                    </div>
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <label>
                                                                                        <input type="checkbox"<?= ($boxSelected == true ? ' checked="checked"' : '') ?>
                                                                                                class="cbl1_<?= $menuLink['id'] ?> cbl2_<?= $secondLevelOption['id'] ?>"
                                                                                                data-parent="" name="AdminGroup[actions][]"
                                                                                                value="<?= $thirdLevelOption['id'] ?>"/>
                                                                                        <?= $thirdLevelOption['title'] ?>
                                                                                    </label>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <label><input
                                                                    type="checkbox"<?= ($boxSelected == true ? ' checked="checked"' : '') ?>
                                                                    class="cbl2 cbl1_<?= $menuLink['id'] ?>" name="AdminGroup[actions][]"
                                                                    value="<?= $secondLevelOption['id'] ?>"/> <?= $secondLevelOption['title'] ?>
                                                        </label>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <?php
                    //  }
                }
            ?>
        </div>
    </div>

  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
