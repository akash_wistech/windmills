<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdminGroup */

$this->title = Yii::t('app', 'User Permission');
$cardTitle = Yii::t('app','Update User:  {nameAttribute}', [
    'nameAttribute' => $userID->firstname.' '.$userID->lastname,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update User Permission');
?>
<div class="admin-group-update">
    <?= $this->render('user-form', [
      'adminGroups' => $adminGroups,
      'userID' => $userID,
    ]) ?>
</div>
