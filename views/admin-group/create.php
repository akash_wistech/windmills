<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdminGroup */

$this->title = Yii::t('app', 'Permission Groups');
$cardTitle = Yii::t('app','New Group');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="admin-group-create">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
