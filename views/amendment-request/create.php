<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AmendmentRequest */

$this->title = Yii::t('app', 'Create Amendment Request');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Amendment Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amendment-request-create">


    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $this->title,
    ]) ?>

</div>
