<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AmendmentRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Amendment Requests');
$this->params['breadcrumbs'][] = $this->title;


$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    $createBtn=true;
}
// if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
//     $actionBtns.='{view}';
// }
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
    $actionBtns.='{delete}';
}
?>

<div class="amendment-request-index">
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>

    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
          'reference_no',
          [
              'format'=>'raw','attribute'=>'date','label'=>Yii::t('app','Date'),'value'=>function($model){
                  return $model->getDateLable($model->date);
              },
          ],
          [
              'format'=>'raw','attribute'=>'deadline','label'=>Yii::t('app','Deadline'),'value'=>function($model){
                  return $model->getDateLable($model->deadline);
              },
          ],
          [
              'format'=>'raw','attribute'=>'number_of_hours_expected','label'=>Yii::t('app','No Of Hours'),'value'=>function($model){
                  return $model->number_of_hours_expected;
              },
          ],
          [
              'format'=>'raw','attribute'=>'priority','label'=>Yii::t('app','Priority'),'value'=>function($model){
                  return $model->getPriorityLable()[$model->priority];
              },
          ],
          [
              'format'=>'raw','attribute'=>'applicant','label'=>Yii::t('app','Applicant'),'value'=>function($model){
                  return $model->getUserName($model->applicant);
              },
          ],
          [
              'format'=>'raw','attribute'=>'start_date','label'=>Yii::t('app','Verify Start Date'),'value'=>function($model){
                  return $model->getDateLable($model->start_date);
              },
          ],
          [
              'format'=>'raw','attribute'=>'completion_date','label'=>Yii::t('app','Verify Completion Date'),'value'=>function($model){
                  return $model->getDateLable($model->completion_date);
              },
          ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
                <div class="btn-group flex-wrap">
                <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                </button>
                <div class="dropdown-menu" role="menu">
                '.$actionBtns.'
                </div>
                </div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>
</div>
