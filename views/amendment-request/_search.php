<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AmendmentRequestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="amendment-request-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'deadline') ?>

    <?= $form->field($model, 'completion_date') ?>

    <?= $form->field($model, 'applicant') ?>

    <?php // echo $form->field($model, 'change_explanation') ?>

    <?php // echo $form->field($model, 'vat') ?>

    <?php // echo $form->field($model, 'under_scope_of_agreement') ?>

    <?php // echo $form->field($model, 'number_of_hours_expected') ?>

    <?php // echo $form->field($model, 'priority:') ?>

    <?php // echo $form->field($model, 'reason_of_change') ?>

    <?php // echo $form->field($model, 'verify') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
