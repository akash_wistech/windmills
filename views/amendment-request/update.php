<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AmendmentRequest */

$this->title = Yii::t('app', 'Update Amendment Request: {ref}', [
    'ref' => $model->reference_no,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Amendment Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="amendment-request-update">


    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $this->title,
    ]) ?>

</div>
