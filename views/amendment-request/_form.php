<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\MyDtPickerAsset;
MyDtPickerAsset::register($this);

$this->registerJs('
  $("#amendment-date,#amendment-completion-date,#amendment-deadline,#amendment-start-date").datetimepicker({
      allowInputToggle: true,
      viewMode: "months",
      format: "YYYY-MM-DD"
  });
');
?>


<section class="amendment-form card">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <?= $form->field($model, 'reference_no')->textInput() ?>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <?= $form->field($model, 'applicant')->dropDownList(Yii::$app->appHelperFunctions->staffMemberListArr,['prompt'=>Yii::t('app','Select')]) ?>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <?= $form->field($model, 'date',['template'=>'
                      {label}
                      <div class="input-group date" id="amendment-date" data-target-input="nearest">
                        {input}
                        <div class="input-group-append" data-target="#amendment-date" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                      </div>
                      {hint}{error}
                      '])->textInput(['maxlength' => true])?>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                  <?= $form->field($model, 'deadline',['template'=>'
                    {label}
                    <div class="input-group date" id="amendment-deadline" data-target-input="nearest">
                      {input}
                      <div class="input-group-append" data-target="#amendment-deadline" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                    {hint}{error}
                    '])->textInput(['maxlength' => true])?>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <?= $form->field($model, 'number_of_hours_expected')->textInput(['type'=>'number']) ?>

                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <?= $form->field($model, 'priority')->dropDownList($model->getPriorityArr(),['prompt'=>Yii::t('app','Select')]) ?>
                </div>
            </div>


            <div class="col-3">
                <div class="form-group">
                    <?= $form->field($model, 'under_scope_of_agreement')->dropDownList($model->getYesNoArr(),['prompt'=>Yii::t('app','Select')]) ?>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                  <?= $form->field($model, 'start_date',['template'=>'
                    {label}
                    <div class="input-group date" id="amendment-start-date" data-target-input="nearest">
                      {input}
                      <div class="input-group-append" data-target="#amendment-start-date" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                    {hint}{error}
                    '])->textInput(['maxlength' => true])?>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                  <?= $form->field($model, 'completion_date',['template'=>'
                    {label}
                    <div class="input-group date" id="amendment-completion-date" data-target-input="nearest">
                      {input}
                      <div class="input-group-append" data-target="#amendment-completion-date" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                    {hint}{error}
                    '])->textInput(['maxlength' => true])?>
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <?= $form->field($model, 'change_explanation')->textarea(['rows' => 6]) ?>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <?= $form->field($model, 'reason_of_change')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</section>
