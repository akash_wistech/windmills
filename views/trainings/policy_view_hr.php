<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Developments */

$this->title = Yii::t('app', $title);
$cardTitle = Yii::t('app', 'View Training: '.$title, [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>


<div class="developments-view">
    <section class="card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title"><?php
                $title_policy = \app\models\PoliciesTitles::findOne($model->id);
                echo $title_policy->title; ?></h2>
            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) { ?>
                <div class="card-tools">
                    <a href="<?= Url::to(['update', 'id' => $model['id']]) ?>" class="btn btn-tool">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            <?php } ?>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/general_success_indicators.mov" type="video/mp4">

                                </video>
                            </div>
                            <p>General Success Indicators</p>
                        </div>
                </div>
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/business/executive_assistant_training_video.mp4" type="video/mp4">

                                </video>
                                <p>Executive Assistant Training</p>
                            </div>

                        </div>
                </div>

            </div>

        </div>
    </section>


    <?php
    if($model<>null && $model->id<>null){
        echo Yii::$app->appHelperFunctions->getLastActionHitory([
            'model_id' => $model->id,
            'model_name' =>'app\models\Policies',
        ]);
    }
    ?>
</div>
