<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Developments */

$this->title = Yii::t('app', $title);
$cardTitle = Yii::t('app', 'View Training: '.$title, [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>


<div class="developments-view">
    <section class="card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title"><?php
                $title_policy = \app\models\PoliciesTitles::findOne($model->id);
                echo $title_policy->title; ?></h2>
            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) { ?>
                <div class="card-tools">
                    <a href="<?= Url::to(['update', 'id' => $model['id']]) ?>" class="btn btn-tool">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            <?php } ?>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/how_to_enter_receive_valuation_instruction_in_maxima_and_arrange_inspections.mov" type="video/mp4">

                                </video>
                            </div>
                            <p>1. How to enter receive valuation instruction in maxima and arrange inspections</p>
                        </div>
                </div>
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/how_to_input_property_document_and_valuation_details_in_maxima.mov" type="video/mp4">

                                </video>
                                <p>2. How to input property ,document and valuation details in maxima</p>
                            </div>

                        </div>
                </div>
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/how_to_verify_location_and_project_name_for_villa.mov" type="video/mp4">

                                </video>
                                <p>3. How to verify location and project name for VILLA</p>
                            </div>

                        </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/verifying_the_valuation_instruction.mov" type="video/mp4">

                                </video>
                            </div>
                            <p>4. Verifying the  valuation instruction</p>
                        </div>
                </div>
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/how_to_inspect_the_property.mov" type="video/mp4">

                                </video>
                                <p>5. How to inspect the property</p>
                            </div>

                        </div>
                </div>
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/how_to_do_valuation_of_a_villa_in_maxima.mov" type="video/mp4">

                                </video>
                                <p>6.1. How to do valuation of a villa in maxima</p>
                            </div>
                        </div>
                  </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/valuation_of_an_apartment_in_maxima_003.mov" type="video/mp4">

                                </video>
                            </div>
                            <p>6.2 Valuation Of an Apartment In Maxima</p>
                        </div>
                </div>
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/how_to_approve_valuation_of_an_apartment_in_maxima.mov" type="video/mp4">

                                </video>
                                <p>7. How to approve valuation of an apartment in maxima</p>
                            </div>

                        </div>
                </div>
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/dib_submission_in_official_portal.mov" type="video/mp4">

                                </video>
                                <p>8. DIB Submission in Official Portal</p>
                            </div>
                        </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/taqyimee_tegistration_and_issuance_of_certificate.mov" type="video/mp4">

                                </video>
                            </div>
                            <p>9. Taqyimee Registration and Issuance of Certificate</p>
                        </div>
                </div>
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/ajman_rera_approval_in_maxima.mov" type="video/mp4">

                                </video>
                                <p>10. AJMAN RERA approval in maxima</p>
                            </div>

                        </div>
                </div>
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/how_to_record_client_inquiries_in_maxima.mov" type="video/mp4">

                                </video>
                                <p>11. How to record client inquiries in maxima</p>
                            </div>
                        </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/how_to_upload_the_scan_copy_of_the_valuation_report_in_maxima.mov" type="video/mp4">

                                </video>
                            </div>
                            <p>12. How to upload the scan copy of the valuation report in maxima</p>
                        </div>
                </div>
                <div class="col-sm-4">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <video width="360" height="240" controls>
                                    <source src="https://maxima.windmillsgroup.com/trainings/valuation/rules_of_conducts.mov" type="video/mp4">

                                </video>
                                <p>Rules of conducts</p>
                            </div>

                        </div>
                </div>

            </div>
        </div>
    </section>


    <?php
    if($model<>null && $model->id<>null){
        echo Yii::$app->appHelperFunctions->getLastActionHitory([
            'model_id' => $model->id,
            'model_name' =>'app\models\Policies',
        ]);
    }
    ?>
</div>
