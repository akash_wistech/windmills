<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BuildingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Merge Clients');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;
if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
$createBtn = true;
if (Yii::$app->menuHelperFunction->checkActionAllowed('view')) {
    $actionBtns .= '{view}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
    $actionBtns.='{status}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
    $actionBtns.='{delete}';
}
?>
<div class="buildings-index">


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'import' => $import,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],

            ['attribute' => 'client_id',
                'label' => Yii::t('app', 'Main Client'),
                'value' => function ($model) {
                    return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Clients::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],

            ['attribute' => 'client_project',
                'label' => Yii::t('app', 'Combine Clients'),
                'value' => function ($model) {
        $combine_names='';
                    if($model->client_project <>  null){
                        $each_combine = explode(',',$model->client_project);
                        $projects = app\models\Company::find()->select('title')->where(['id'=> $each_combine])->orderBy([
                            'title' => SORT_ASC,
                        ])->all();

                        if(!empty($projects)){
                            foreach ($projects as $project){
                                $combine_names .=$project->title.', ';
                            }
                        }
                    }
                    return $combine_names;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
                'contentOptions' => ['class' => 'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              ' . $actionBtns . '
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'status' => function ($url, $model) {
                        /* if ($model['status'] == 1) {
                             return Html::a('<i class="fas fa-eye-slash"></i> ' . Yii::t('app', 'Disable'), $url, [
                                 'title' => Yii::t('app', 'Disable'),
                                 'class' => 'dropdown-item text-1',
                                 'data-confirm' => Yii::t('app', 'Are you sure you want to disable this item?'),
                                 'data-method' => "post",
                             ]);
                         } else {
                             return Html::a('<i class="fas fa-eye"></i> ' . Yii::t('app', 'Enable'), $url, [
                                 'title' => Yii::t('app', 'Enable'),
                                 'class' => 'dropdown-item text-1',
                                 'data-confirm' => Yii::t('app', 'Are you sure you want to enable this item?'),
                                 'data-method' => "post",
                             ]);
                         }*/
                    },
                ],
            ],
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>
