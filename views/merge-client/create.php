<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MergeProject */

$this->title = 'Create Merge Client';
$this->params['breadcrumbs'][] = ['label' => 'Merge Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="merge-project-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
