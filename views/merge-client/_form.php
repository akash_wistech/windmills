<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Buildings */
/* @var $form yii\widgets\ActiveForm */
?>



<section class="buildings-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title">
            <?= $cardTitle ?>
        </h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'client_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Clients::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select Parent Client ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Parent Client');
                ?>

            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'client_department')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\ClientDepartments::find()
                        ->where(['status' => 1])
                        ->orderBy(['title' => SORT_ASC,])
                        ->all(), 'id', 'title'),

                    'options' => ['placeholder' => 'Select Department ...', 'class'=> 'client-cls'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Department');
                ?>

            </div>


            <div class="col-sm-12">
                <?php

                   $model->client_project = explode(',', ($model->client_project <> null) ? $model->client_project : "");
                   echo $form->field($model, 'client_project')->widget(Select2::classname(), [
                       'data' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                           'title' => SORT_ASC,
                       ])->all(), 'id', 'title'),
                       'options' => ['placeholder' => 'Select Child Client ...'],
                       'pluginOptions' => [
                           'placeholder' => 'Select a Child Client',
                           'multiple' => true,
                           'allowClear' => true
                       ],
                   ])->label('Select to Merge Client');;
                ?>
            </div>


        </div>



        </div>

        <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('status')) { ?>
            <section class="card card-outline card-info">
                <header class="card-header">
                    <h2 class="card-title">
                        <?= Yii::t('app', 'Verification') ?>
                    </h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="col-sm-4">
                                <?php
                                echo $form->field($model, 'status')->widget(Select2::classname(), [
                                    'data' => array('2' => 'Unverified', '1' => 'Verified'),
                                ]);
                                ?>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
        <?php } ?>

    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if ($model <> null && $model->id <> null) {
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>