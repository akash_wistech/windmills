<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SoldTransactions */


$this->title = Yii::t('app', 'Sold Transactions Abu Dhabi');
$cardTitle = Yii::t('app', 'New Sold Transaction Abu Dhabi');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle
?>
<div class="sold-transactions-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
