<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SoldTransactions */

$this->title = Yii::t('app', 'Sold Transactions Abu Dhabi');
$cardTitle = Yii::t('app', 'Update Sold Transaction Abu Dhabi:  {nameAttribute}', [
    'nameAttribute' => ($model->building!=null ? $model->building->title : '-'),
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sold-transactions-update">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
