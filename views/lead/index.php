<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\LeadListAsset;
LeadListAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Leads');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-index">
  <?= $this->render('/opportunity/_gridview', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'cardTitle' => $cardTitle,
  ]) ?>
</div>
