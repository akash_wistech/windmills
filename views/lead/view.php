<?php

/* @var $this yii\web\View */
/* @var $model app\models\Lead */

$this->title = Yii::t('app', 'Leads');
$cardTitle = Yii::t('app','View Lead:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');
?>
<?= $this->render('/opportunity/_view_details',['model'=>$model])?>
