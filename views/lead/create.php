<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Lead */

$this->title = Yii::t('app', 'Leads');
$cardTitle = Yii::t('app','New Lead');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="lead-create">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
