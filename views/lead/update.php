<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lead */

$this->title = Yii::t('app', 'Leads');
$cardTitle = Yii::t('app','Update Lead:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="lead-update">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
