<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\widgets\CustomFieldsWidget;
use app\widgets\ProcessWidget;
use app\assets\OpportunityFormAsset;
OpportunityFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Lead */
/* @var $form yii\widgets\ActiveForm */

$model->tags=implode(",",$model->tagsListArray);

$this->registerJs('
$("#opportunity-d-expected_close_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
$(".numeral-input").each(function(index){
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("body").on("blur", ".numeral-input", function () {
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("#opportunity-manager_id").select2({
	allowClear: true,
	width: "100%",
});
$("#opportunity-tags").tagsInput({
	"width":"100%",
	"defaultText":"Add tags",
});
');
$servicesListId = Yii::$app->appHelperFunctions->getSetting('service_list_id');
?>
<section class="contact-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(); ?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'company_name')->textInput(['maxlength' => true])?>
      </div>
      <?= CustomFieldsWidget::widget(['form'=>$form,'type'=>'prospect','model'=>$model->prospect,'mixed'=>true])?>
    </div>
    <div class="row">
      <div class="col-sm-8">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'service_type')->dropDownList(Yii::$app->appHelperFunctions->getPredefinedListOptionsArr($servicesListId),['id'=>'moduleServiceType','prompt'=>Yii::t('app','Select')])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'source')->dropDownList(Yii::$app->helperFunctions->opportunitySourceListArr,['prompt'=>Yii::t('app','Select')])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'expected_close_date',['template'=>'
        {label}
        <div class="input-group date" id="opportunity-d-expected_close_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#opportunity-d-expected_close_date" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'quote_amount',['template'=>'
        {label}
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">'.Yii::$app->appHelperFunctions->getSetting('currency_sign').'</div>
          </div>
          {input}
        </div>
        {hint}{error}
        '])->textInput(['class'=>'form-control numeral-input', 'maxlength' => true])?>
      </div>
    </div>
    <?= $form->field($model, 'descp')->textArea(['rows' => 4])?>
    <?= $form->field($model, 'tags')->textInput(['placeholder'=>'Add tags','maxlength' => true])?>
    <?= $form->field($model, 'manager_id')->dropDownList(Yii::$app->appHelperFunctions->staffMemberListArr,['multiple'=>'multiple'])?>
    <?= CustomFieldsWidget::widget(['form'=>$form,'type'=>$model->moduleTypeId,'model'=>$model])?>
    <?= ProcessWidget::widget(['form'=>$form,'type'=>$model->moduleTypeId,'model'=>$model])?>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
<?= $this->render('/client/js/create_company_script')?>
