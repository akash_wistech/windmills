<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StandardReport */
/* @var $form yii\widgets\ActiveForm */
?>



<section class="properties-form card card-outline card-primary">




    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>

    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">

    <?= $form->field($model, 'client_intended_users')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'subject_property')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'valuation_instructions')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'purpose_valuation')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'terms_engagement_agreed')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'value_duties_supervision')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'internal_external_status_valuer')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'declaration_independence_objectivity')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'assum_extent_inesti_limit_scope_work')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'structural_tecnical_survey')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'conditions_state_repair_matenence')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'contamination_ground_enviormental')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'natural_disasters')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'national_scenaiors_majeure_situation')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'statutory_regulatory_requirements')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'title_tenancies_property_document')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'planning_highway_enquires')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'plant_equipment')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'development_property')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'disposal_cost_libility')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'documentation_provided_clients')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'documentation_not_provided_clients')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'nature_sources_info_doc')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'rics_valuation')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'basis_value')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'market_value')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'statutory_definition_marketvalue')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'market_rent')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'investment_value')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'fair_value')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'valuation_date')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'comparable_analyses')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'comparable_properties')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'market_commentary')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'summary_key_inputs')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'valuation')->textarea(['rows' =>8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'general_uncertainties')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'cronavirus_related_contingencies')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'confidentinality')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'restrictions_use_destribution_publication')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'property_valuation_overview')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'limitation_attaching_document')->textarea(['rows' => 8]) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'limitation_of_liability')->textarea(['rows' => 8]) ?>
  </div>

    <div class="card-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</section>
