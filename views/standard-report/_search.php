<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StandardReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="standard-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'client_intended_users') ?>

    <?= $form->field($model, 'subject_property') ?>

    <?= $form->field($model, 'valuation_instructions') ?>

    <?= $form->field($model, 'purpose_valuation') ?>

    <?php // echo $form->field($model, 'terms_engagement_agreed') ?>

    <?php // echo $form->field($model, 'value_duties_supervision') ?>

    <?php // echo $form->field($model, 'internal_external_status_valuer') ?>

    <?php // echo $form->field($model, 'declaration_independence_objectivity') ?>

    <?php // echo $form->field($model, 'assum_extent_inesti_limit_scope_work') ?>

    <?php // echo $form->field($model, 'structural_tecnical_survey') ?>

    <?php // echo $form->field($model, 'conditions_state_repair_matenence') ?>

    <?php // echo $form->field($model, 'contamination_ground_enviormental') ?>

    <?php // echo $form->field($model, 'natural_disasters') ?>

    <?php // echo $form->field($model, 'national_scenaiors_majeure_situation') ?>

    <?php // echo $form->field($model, 'statutory_regulatory_requirements') ?>

    <?php // echo $form->field($model, 'title_tenancies_property_document') ?>

    <?php // echo $form->field($model, 'planning_highway_enquires') ?>

    <?php // echo $form->field($model, 'plant_equipment') ?>

    <?php // echo $form->field($model, 'development_property') ?>

    <?php // echo $form->field($model, 'disposal_cost_libility') ?>

    <?php // echo $form->field($model, 'documentation_provided_clients') ?>

    <?php // echo $form->field($model, 'documentation_not_provided_clients') ?>

    <?php // echo $form->field($model, 'nature_sources_info_doc') ?>

    <?php // echo $form->field($model, 'rics_valuation') ?>

    <?php // echo $form->field($model, 'basis_value') ?>

    <?php // echo $form->field($model, 'market_value') ?>

    <?php // echo $form->field($model, 'statutory_definition_marketvalue') ?>

    <?php // echo $form->field($model, 'market_rent') ?>

    <?php // echo $form->field($model, 'investment_value') ?>

    <?php // echo $form->field($model, 'fair_value') ?>

    <?php // echo $form->field($model, 'valuation_date') ?>

    <?php // echo $form->field($model, 'comparable_analyses') ?>

    <?php // echo $form->field($model, 'comparable_properties') ?>

    <?php // echo $form->field($model, 'market_commentary') ?>

    <?php // echo $form->field($model, 'summary_key_inputs') ?>

    <?php // echo $form->field($model, 'valuation') ?>

    <?php // echo $form->field($model, 'general_uncertainties') ?>

    <?php // echo $form->field($model, 'cronavirus_related_contingencies') ?>

    <?php // echo $form->field($model, 'confidentinality') ?>

    <?php // echo $form->field($model, 'restrictions_use_destribution_publication') ?>

    <?php // echo $form->field($model, 'property_valuation_overview') ?>

    <?php // echo $form->field($model, 'limitation_attaching_document') ?>

    <?php // echo $form->field($model, 'limitation_of_liability') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
