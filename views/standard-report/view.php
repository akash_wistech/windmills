<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StandardReport */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Standard Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="standard-report-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'client_intended_users:ntext',
            'subject_property:ntext',
            'valuation_instructions:ntext',
            'purpose_valuation:ntext',
            'terms_engagement_agreed:ntext',
            'value_duties_supervision:ntext',
            'internal_external_status_valuer:ntext',
            'declaration_independence_objectivity:ntext',
            'assum_extent_inesti_limit_scope_work:ntext',
            'structural_tecnical_survey:ntext',
            'conditions_state_repair_matenence:ntext',
            'contamination_ground_enviormental:ntext',
            'natural_disasters:ntext',
            'national_scenaiors_majeure_situation:ntext',
            'statutory_regulatory_requirements:ntext',
            'title_tenancies_property_document:ntext',
            'planning_highway_enquires:ntext',
            'plant_equipment:ntext',
            'development_property:ntext',
            'disposal_cost_libility:ntext',
            'documentation_provided_clients:ntext',
            'documentation_not_provided_clients:ntext',
            'nature_sources_info_doc:ntext',
            'rics_valuation:ntext',
            'basis_value:ntext',
            'market_value:ntext',
            'statutory_definition_marketvalue:ntext',
            'market_rent:ntext',
            'investment_value:ntext',
            'fair_value:ntext',
            'valuation_date:ntext',
            'comparable_analyses:ntext',
            'comparable_properties:ntext',
            'market_commentary:ntext',
            'summary_key_inputs:ntext',
            'valuation:ntext',
            'general_uncertainties:ntext',
            'cronavirus_related_contingencies:ntext',
            'confidentinality:ntext',
            'restrictions_use_destribution_publication:ntext',
            'property_valuation_overview:ntext',
            'limitation_attaching_document:ntext',
            'limitation_of_liability:ntext',
        ],
    ]) ?>

</div>
