<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StandardReport */

$this->title = 'Update Standard Report: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Standard Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="standard-report-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
