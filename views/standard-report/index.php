<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StandardReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Standard Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="standard-report-index">



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'layout'=>"

      

        <div class=\"card card-widget\">
                              <div class=\"card-header\">
                                        List
                                <div class=\"card-tools\">
                                  ".Html::a(Yii::t('app', '+'), ['create'], ['class' => ['btn btn-primary','py-0 px-2','font-weight-bold']])."
                                </div>
                              </div>

                              <div class=\"card-body\">
                                  {items}\n
                              </div>

                              <div class=\"card-footer\">

                              <div class=\"row\">

                            <div class=\"col-sm-3 col-md-6\" style=\"background-color:white;\">
                                  {summary}
                            </div>

                            <div class=\"col-sm-9 col-md-6\"  style=\"background-color:white;\">
                                \n{pager}
                            </div>

                          </div>
                        </div>

                              </div>
                            </div>


        ",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'client_intended_users:ntext',
            'subject_property:ntext',
            'valuation_instructions:ntext',
            'purpose_valuation:ntext',
            //'terms_engagement_agreed:ntext',
            //'value_duties_supervision:ntext',
            //'internal_external_status_valuer:ntext',
            //'declaration_independence_objectivity:ntext',
            //'assum_extent_inesti_limit_scope_work:ntext',
            //'structural_tecnical_survey:ntext',
            //'conditions_state_repair_matenence:ntext',
            //'contamination_ground_enviormental:ntext',
            //'natural_disasters:ntext',
            //'national_scenaiors_majeure_situation:ntext',
            //'statutory_regulatory_requirements:ntext',
            //'title_tenancies_property_document:ntext',
            //'planning_highway_enquires:ntext',
            //'plant_equipment:ntext',
            //'development_property:ntext',
            //'disposal_cost_libility:ntext',
            //'documentation_provided_clients:ntext',
            //'documentation_not_provided_clients:ntext',
            //'nature_sources_info_doc:ntext',
            //'rics_valuation:ntext',
            //'basis_value:ntext',
            //'market_value:ntext',
            //'statutory_definition_marketvalue:ntext',
            //'market_rent:ntext',
            //'investment_value:ntext',
            //'fair_value:ntext',
            //'valuation_date:ntext',
            //'comparable_analyses:ntext',
            //'comparable_properties:ntext',
            //'market_commentary:ntext',
            //'summary_key_inputs:ntext',
            //'valuation:ntext',
            //'general_uncertainties:ntext',
            //'cronavirus_related_contingencies:ntext',
            //'confidentinality:ntext',
            //'restrictions_use_destribution_publication:ntext',
            //'property_valuation_overview:ntext',
            //'limitation_attaching_document:ntext',
            //'limitation_of_liability:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
