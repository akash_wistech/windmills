<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StandardReport */

$this->title = 'Standard Report';
$this->params['breadcrumbs'][] = ['label' => 'Standard Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="standard-report-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
