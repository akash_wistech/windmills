<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CustomField */

$this->title = Yii::t('app', 'Custom Fields');
$cardTitle = Yii::t('app','Update Custom Field:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="custom-field-update">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
