<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomField */

$this->title = Yii::t('app', 'Custom Fields');
$cardTitle = Yii::t('app','New Custom Field');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="custom-field-create">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
