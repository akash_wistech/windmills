<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BuildingForSave */

$this->title = 'Update Building For Save: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Building For Saves', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="building-for-save-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
