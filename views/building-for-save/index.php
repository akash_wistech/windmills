
<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;


$this->title = Yii::t('app', 'Building For Save');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;
$actionBtns .= '{view}';
$actionBtns .= '{update}';
$actionBtns .= '{delete}';


?>
<style>
.btn-success {
    margin-left: 5px;
}
</style>


<div class="list-data-index my-2 clearfix">
<?php CustomPjax::begin(['id' => 'grid-container']); ?>
<?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'cardTitle' => $cardTitle,
    'createBtn' => $createBtn,
    'columns' => [
        
        [
            'class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']
        ],
        
        'building_name',
        
        ['attribute' => 'city',
        
        'label' => Yii::t('app', 'City'),
        'value' => function ($model) {
            return Yii::$app->appHelperFunctions->emiratedListArr[$model['city']];
        },
        'filter' => Yii::$app->appHelperFunctions->emiratedListArr
    ],
    
    
    // [
    //     'class' => 'yii\grid\ActionColumn',
    //     'header' => '',
    //     'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
    //     'contentOptions' => ['class' => 'noprint actions'],
    //     'template' => '
    //     <div class="btn-group flex-wrap">
    //     <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
    //     <span class="caret"></span>
    //     </button>
    //     <div class="dropdown-menu" role="menu">
    //     ' . $actionBtns . '
    //     </div>
    //     </div>',
    //     'buttons' => [
    //         'view' => function ($url, $model) {
    //             return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
    //                 'title' => Yii::t('app', 'View'),
    //                 'class' => 'dropdown-item text-1',
    //                 'data-pjax' => "0",
    //             ]);
    //         },
    //         'update' => function ($url, $model) {
    //             // print_r($url); die;
    //             return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Edit'), $url, [
    //                 'title' => Yii::t('app', 'Edit'),
    //                 'class' => 'dropdown-item text-1',
    //                 'data-pjax' => "0",
    //             ]);
    //         },
    //         'delete' => function ($url, $model) {
    //             return Html::a('<i class="fas fa-trash"></i> ' . Yii::t('app', 'Delete'), $url, [
    //                 'title' => Yii::t('app', 'Delete'),
    //                 'class' => 'dropdown-item text-1',
    //                 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
    //                 'data-method' => "post",
    //                 'data-pjax' => "0",
    //             ]);
    //         },
    //     ],
    // ],
],
]); ?>
<?php CustomPjax::end(); ?>


</div>



























