<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BuildingForSave */

$this->title = 'Create Building For Save';
$this->params['breadcrumbs'][] = ['label' => 'Building For Saves', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="building-for-save-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
