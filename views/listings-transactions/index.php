<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ListingsTransactionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Listings Transactions');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;
$import = false;
if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('view')) {
    $actionBtns .= '{view}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('status')) {
    $actionBtns .= '{status}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('import')) {
    $import = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('delete')) {
    $actionBtns .= '{delete}';
}
$actionBtns.='{get-history}';


?>
<style>
    .btn-success {
        margin-left: 5px;
    }
</style>
<div class="listings-transactions-index">


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'import' => $import,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'listings_reference', 'label' => Yii::t('app', 'Reference')],
            ['attribute' => 'listing_date',
                'label' => Yii::t('app', 'Listing Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->listing_date));
                },
            ],
            ['attribute' => 'source',
                'label' => Yii::t('app', 'Source'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->getPropertySourceListArr()[$model->source];
                },
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Building'),
                'value' => function ($model) {
                    return $model->building->title;
                },
                'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'community',
                'label' => Yii::t('app', 'Community'),
                'value' => function ($model) {
                    return $model->communities->title;
                },
                'filter' => ArrayHelper::map(\app\models\Communities::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'sub_community',
                'label' => Yii::t('app', 'Sub Community'),
                'value' => function ($model) {
                    return $model->subCommunities->title;
                },
                'filter' => ArrayHelper::map(\app\models\SubCommunities::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'no_of_bedrooms', 'label' => Yii::t('app', 'No. of Rooms')],
            ['attribute' => 'land_size', 'label' => Yii::t('app', 'Land Size')],
            ['attribute' => 'built_up_area', 'label' => Yii::t('app', 'BUA')],
            ['attribute' => 'listings_price', 'label' => Yii::t('app', 'Price')],
            ['attribute' => 'price_per_sqt', 'label' => Yii::t('app', 'Price / sf')],
            ['attribute' => 'created_at',
                'label' => Yii::t('app', 'Created Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->created_at));
                },
            ],
            [
                'attribute' => 'created_by',
                'label' => Yii::t('app', 'Created By'),
                'value' => function ($model) {
                    return (isset($model->created_by) && ($model->created_by <> null))? ($model->userCreatedBy->firstname.' '.$model->userCreatedBy->lastname): '';
                },
                'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
                    'firstname' => SORT_ASC,
                ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];})
            ],
            ['attribute' => 'updated_at',
                'label' => Yii::t('app', 'Updated Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->updated_at));
                },
            ],
            [
                'attribute' => 'updated_by',
                'label' => Yii::t('app', 'Updated By'),
                'value' => function ($model) {
                    return (isset($model->updated_by) && ($model->updated_by <> null))? ($model->userUpdatedBy->firstname.' '.$model->userUpdatedBy->lastname): '';
                },
                'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
                    'firstname' => SORT_ASC,
                ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];})
            ],
            ['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
                return Yii::$app->helperFunctions->arrStatusIconList[$model['status']];
            },'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>Yii::$app->helperFunctions->arrFilterStatusList],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
                'contentOptions' => ['class' => 'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              ' . $actionBtns . '
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'status' => function ($url, $model) {
                       /* if ($model['status'] == 1) {
                            return Html::a('<i class="fas fa-eye-slash"></i> ' . Yii::t('app', 'Unverified'), $url, [
                                'title' => Yii::t('app', 'Unverified'),
                                'class' => 'dropdown-item text-1',
                                'data-confirm' => Yii::t('app', 'Are you sure you want to Unverified this item?'),
                                'data-method' => "post",
                            ]);
                        } else {
                            return Html::a('<i class="fas fa-eye"></i> ' . Yii::t('app', 'Verified'), $url, [
                                'title' => Yii::t('app', 'Verified'),
                                'class' => 'dropdown-item text-1',
                                'data-confirm' => Yii::t('app', 'Are you sure you want to Verified this item?'),
                                'data-method' => "post",
                            ]);
                        }*/
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> ' . Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'dropdown-item text-1',
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'data-method' => "post",
                            'data-pjax' => "0",
                        ]);
                    },
                    'get-history' => function ($url, $model) {
                        // Yii::$app->session->set('model_name', Yii::$app->appHelperFunctions->getModelName());
                        return Html::a('<i class="fa fa-history"></i> '.Yii::t('app', 'History'), 'javascript:;', [
                            'title' => Yii::t('app', 'History'),
                            'class'=>'dropdown-item text-1',
                            'target' => '_blank',
                            'onclick'=> "window.open('".$url."', '_blank')",
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>

