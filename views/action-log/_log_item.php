<?php
use yii\helpers\Html;
use yii\helpers\Url;
$actionInfo=$model->actionInfo;
$timeInfo=$model->timeInfo;
$calendarInfo=$model->calendarEventInfo;
$attachmentInfo=$model->attachmentInfo;
?>
<img class="img-circle img-sm" src="images/icons/<?= $model->rec_type?>.png" alt="<?= $model->rec_type?>">
<div class="comment-text">
  <span class="username">
    <?= $model->createdByUser->name?>
    <span class="text-muted"><i class="fa fa-clock"></i> <?= Yii::$app->helperFunctions->convertTime($model->created_at)?>&nbsp;</span>
    <span class="float-right">
      <?php if($model->rec_type=='action' && $actionInfo!=null && $actionInfo['due_date']>=date("Y-m-d H:i:s") && $actionInfo['status']==0){?>
      <a href="javascript:;" class="btn btn-xs aa-confirm-action" data-cheading="<?= Yii::t('app','Confirmation')?>" data-cmsg="<?= Yii::t('app','Are you sure you want to mark this action done?')?>" data-url="<?= Url::to(['action-log/completed','id'=>$model->id])?>" data-container="al-lv-container"><i class="fa fa-check"></i></a>
      <?php }?>
      <?php if($model->rec_type!='attachment'){?>
      <a href="javascript:;" class="btn btn-xs load-modal" data-heading="<?= Yii::t('app','Update {type}',['type'=>Yii::$app->activityHelper->actionLogTypesArr[$model->rec_type]])?>" data-url="<?= Url::to(['action-log/update','id'=>$model->id])?>"><i class="fa fa-edit"></i></a>
      <?php }?>
      <a href="javascript:;" class="btn btn-xs aa-confirm-action" data-cheading="<?= Yii::t('app','Confirmation')?>" data-cmsg="<?= Yii::t('app','Are you sure you want to delete this item?')?>" data-url="<?= Url::to(['action-log/delete','id'=>$model->id])?>" data-container="al-lv-container"><i class="fa fa-trash"></i></a>
    </span>
  </span>
  <?php if($actionInfo!=null){?>
  <strong><?= $actionInfo['subject']?></strong>
  <?php if($actionInfo['due_date']!=null && $actionInfo['due_date']!=''){?>
    <span class="badge badge-danger"><i class="fa fa-bell"></i> <?= Yii::$app->formatter->asDateTime($actionInfo['due_date'])?></span>
  <?php }?>
  <br />
  <?php }?>
  <?php
  if($model->rec_type=='call' && $timeInfo!=null){
    $duration=$timeInfo['hours'].'.'.$timeInfo['minutes'];
  ?>
    <?= Yii::t('app','Call: {date}; {duration} hours',['date'=>Yii::$app->formatter->asDateTime($timeInfo['start_dt']),'duration'=>$duration])?>
    <br />
  <?php }?>
  <?php
  if($model->rec_type=='timer' && $timeInfo!=null){
    $duration=$timeInfo['hours'].'.'.$timeInfo['minutes'];
  ?>
    <?= Yii::t('app','{duration} hours, starting {date}',['duration'=>$duration,'date'=>Yii::$app->formatter->asDateTime($timeInfo['start_dt'])])?>
    <br />
  <?php }?>
  <?php
  if($model->rec_type=='calendar' && $calendarInfo!=null){
  ?>
    <?= Yii::t('app','Event: {startDate} - {endDate}',['startDate'=>Yii::$app->formatter->asDateTime($calendarInfo['start_date']),'endDate'=>Yii::$app->formatter->asDateTime($calendarInfo['end_date'])])?>
    <br />
  <?php }?>
  <?php
  if($model->rec_type=='attachment' && $attachmentInfo!=null){
    foreach($attachmentInfo as $attachment){
      echo '<div class="att-img-block"><a href="'.$attachment['link'].'" target="_blank" title="'.$attachment['title'].'" data-toggle="tooltip"><img src="'.$attachment['icon'].'" /></a></div>';
    }
  ?>
    <?= '';//Yii::t('app','Event: {startDate} - {endDate}',['startDate'=>Yii::$app->formatter->asDateTime($calendarInfo['start_date']),'endDate'=>Yii::$app->formatter->asDateTime($calendarInfo['end_date'])])?>
    <br />
  <?php }?>
  <?= $model->comments?>
</div>
<style>
.att-img-block{
margin: 1px 2px;
display: inline-block;
padding: 1px;
border:1px solid #ccc;
}
</style>
