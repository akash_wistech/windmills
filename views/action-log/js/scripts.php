<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\ActionLogWidgetAsset;
ActionLogWidgetAsset::register($this);

$this->registerJs('
reloadAlHistory();
$(document).on("pjax:success", function() {
  reloadAlHistory();
});
_alTargetContainer=$(".activity-logger-card")
$("body").on("beforeSubmit", "form#al-comments-form,form#al-action-form,form#al-call-form,form#al-timer-form,form#al-calendar-form", function () {
	App.blockUI({
		iconOnly: true,
		target: _alTargetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});

	 var form = $(this);
	 // return false if form still have some validation errors
	 if (form.find(".has-error").length) {
		  return false;
	 }
	 // submit form
	 $.ajax({
		  url: form.attr("action"),
		  type: "post",
		  data: form.serialize(),
      dataType: "json",
		  success: function (response) {
			  if(response["success"]){
          toastr.success(response["success"]["msg"]);
					form.trigger("reset");
			  }else{
				  Swal.fire({title: response["error"]["heading"], html: response["error"]["msg"], icon: "error"});
			  }
        if($("#al-lv-container").length){
          $.pjax.reload({container: "#al-lv-container", timeout: 2000});
        }
			  App.unblockUI($(_alTargetContainer));
		  }
	 });
	 return false;
});
$("body").on("beforeSubmit", "form#al-update-form", function () {
  _targetContainer="#al-update-form";
	App.blockUI({
		iconOnly: true,
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});

	 var form = $(this);
	 // return false if form still have some validation errors
	 if (form.find(".has-error").length) {
		  return false;
	 }
	 // submit form
	 $.ajax({
		  url: form.attr("action"),
		  type: "post",
		  data: form.serialize(),
      dataType: "json",
		  success: function (response) {
			  if(response["success"]){
          toastr.success(response["success"]["msg"]);
          window.closeModal();
			  }else{
				  Swal.fire({title: response["error"]["heading"], html: response["error"]["msg"], icon: "error"});
			  }
        if($("#al-lv-container").length){
          $.pjax.reload({container: "#al-lv-container", timeout: 2000});
        }
			  App.unblockUI($(_targetContainer));
		  }
	 });
	 return false;
});
')
?>
<script>
function reloadAlHistory(){
	$.ias('destroy');
  var ias = $.ias({
    container:  "#alh-list-view",
    item:       ".alh-item",
    pagination: ".pagination",
    next:       ".next a",
    delay:      1200,
    negativeMargin:0
  });
  <?= Yii::$app->jsFunctions->iasSpinnerHtml?>
  ias.extension(new IASSpinnerExtension({
  	html: iasSpinnerHtml,
  }));
}
</script>
