<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
if(Yii::$app->controller->action->id=='update'){
  $frmOpts=[
    'id'=>'al-update-form',
    'action'=>['action-log/update','id'=>$model->id]
  ];
}else{
  $frmOpts=[
    'id'=>'al-comments-form',
    'action'=>['action-log/save','ltype'=>$model->rec_type,'mtype'=>$model->module_type,'mid'=>$model->module_id]
  ];
}
?>
<?php $form = ActiveForm::begin($frmOpts); ?>
<?= $form->field($model, 'comments')->textArea(['rows'=>3,'placeholder'=>$model->getAttributeLabel('comments'),'class'=>$inputClass])->label(false)?>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'visibility',['template'=>$inputGroupTemplate])->dropDownList(Yii::$app->helperFunctions->visibilityListArr,['class'=>$inputClass])?>
  </div>
  <div class="col-sm-6 text-right">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn'.$btnClass.' btn-success']) ?>
  </div>
</div>
<?php ActiveForm::end(); ?>
