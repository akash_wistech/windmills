<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
if(Yii::$app->controller->action->id=='update'){
  $frmOpts=[
    'id'=>'al-update-form',
    'action'=>['action-log/update','id'=>$model->id]
  ];
  $this->registerJs('
  $("body").on("click", "#update-log #alcalendareventform-reminder", function () {
    console.log("Here?");
    if($(this).prop("checked")){
  		$("#calendarReminderInfo'.$model->id.'").show();
  	}else{
  		$("#calendarReminderInfo'.$model->id.'").hide();
  	}
  });
  ');
}else{
  $frmOpts=[
    'id'=>'al-calendar-form',
    'action'=>['action-log/save','ltype'=>$model->rec_type,'mtype'=>$model->module_type,'mid'=>$model->module_id]
  ];
  $this->registerJs('
  $("body").on("click", ".activity-logger-card #alcalendareventform-reminder", function () {
    if($(this).prop("checked")){
  		$("#calendarReminderInfo'.$model->id.'").show();
  	}else{
  		$("#calendarReminderInfo'.$model->id.'").hide();
  	}
  });
  ');
}
$this->registerJs('
$("#alcalendareventform-event_start-input'.$model->id.',#alcalendareventform-event_end-input'.$model->id.'").datetimepicker({
  allowInputToggle: true,
  viewMode: "days",
  format: "YYYY-MM-DD hh:mm:ss",
  icons: {
    time: "fa fa-clock",
  },
});
$("#alcalendareventform-event_start-input'.$model->id.'").on("change.datetimepicker", function (e) {
  $("#alcalendareventform-event_end-input'.$model->id.'").datetimepicker("minDate", e.date);
});
$("#alcalendareventform-event_end-input'.$model->id.'").on("change.datetimepicker", function (e) {
  $("#alcalendareventform-event_start-input'.$model->id.'").datetimepicker("maxDate", e.date);
});
$("#alcalendareventform-assign_to'.$model->id.'").select2({
	allowClear: true,
	width: "100%",
});
');
?>
<?php $form = ActiveForm::begin($frmOpts); ?>
<?= $form->field($model, 'comments')->textArea(['rows'=>3,'placeholder'=>$model->getAttributeLabel('comments'),'class'=>$inputClass])->label(false)?>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'event_start',['template'=>'
    {label}
    <div class="input-group date" id="alcalendareventform-event_start-input'.$model->id.'" data-target-input="nearest">
      {input}
      <div class="input-group-append" data-target="#alcalendareventform-event_start-input'.$model->id.'" data-toggle="datetimepicker">
          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
      </div>
    </div>
    {hint}{error}
    '])->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('event_start'),'class'=>$dtInputClass,'autocomplete'=>'off'])->label(false)?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'event_end',['template'=>'
    {label}
    <div class="input-group date" id="alcalendareventform-event_end-input'.$model->id.'" data-target-input="nearest">
      {input}
      <div class="input-group-append" data-target="#alcalendareventform-event_end-input'.$model->id.'" data-toggle="datetimepicker">
          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
      </div>
    </div>
    {hint}{error}
    '])->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('event_end'),'class'=>$dtInputClass,'autocomplete'=>'off'])->label(false)?>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'priority',['template'=>$inputGroupTemplate])->dropDownList(Yii::$app->helperFunctions->priorityListArr,['class'=>$inputClass])?>
  </div>
  <div class="col-sm-6 text-right">
    <?= $form->field($model, 'color',['template'=>$inputGroupTemplate])->dropDownList(Yii::$app->helperFunctions->calendarColorListArr,['class'=>$inputClass])?>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'sub_type',['template'=>$inputGroupTemplate])->dropDownList(Yii::$app->helperFunctions->eventSubTypesListArr,['prompt'=>Yii::t('app','Select'),'class'=>$inputClass])?>
  </div>
  <div class="col-sm-6 text-right">
    <?= $form->field($model, 'status',['template'=>$inputGroupTemplate])->dropDownList(Yii::$app->helperFunctions->eventStatusListArr,['prompt'=>Yii::t('app','Select'),'class'=>$inputClass])?>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'calendar_id',['template'=>$inputGroupTemplate])->dropDownList(Yii::$app->appHelperFunctions->myCalendarListArr,['prompt'=>Yii::t('app','None'),'class'=>$inputClass])?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'allday',['options'=>['class'=>'form-group mb-0 lbl-mb-0']])->checkbox(); ?>
  </div>
</div>
<?= $form->field($model, 'assign_to')->dropDownList(Yii::$app->appHelperFunctions->staffMemberListArr,['id'=>'alcalendareventform-assign_to'.$model->id,'multiple'=>'multiple','class'=>$inputClass])?>
<?= $form->field($model, 'reminder',['options'=>['class'=>'form-group mb-0 lbl-mb-0']])->checkbox(); ?>
<div id="calendarReminderInfo<?= $model->id?>" class="<?= $model->reminder==1 ? '' : 'hidden'?>">
  <?= Yii::T('app','Create a notification reminder for')?>
  <div class="row">
    <div class="col-sm-6">
      <?= $form->field($model, 'reminder_to',['options'=>['class'=>'form-group mb-0']])->dropDownList(Yii::$app->helperFunctions->actionLogActionRemindToListArr,['class'=>$inputClass])->label(false)?>
    </div>
    <div class="col-sm-6 text-right">
      <?= $form->field($model, 'remind_time',['options'=>['class'=>'form-group mb-0']])->dropDownList(Yii::$app->helperFunctions->actionLogActionRemindTimeListArr,['class'=>$inputClass])->label(false)?>
    </div>
  </div>
  <?= Yii::t('app','before this action is due')?>
</div>
<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn'.$btnClass.' btn-success']) ?>
<?php ActiveForm::end(); ?>
