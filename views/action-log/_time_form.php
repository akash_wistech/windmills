<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
if(Yii::$app->controller->action->id=='update'){
  $frmOpts=[
    'id'=>'al-update-form',
    'action'=>['action-log/update','id'=>$model->id]
  ];
}else{
  $frmOpts=[
    'id'=>'al-timer-form',
    'action'=>['action-log/save','ltype'=>$model->rec_type,'mtype'=>$model->module_type,'mid'=>$model->module_id]
  ];
}
$this->registerJs('
$("#altimerform-start_dt-input'.$model->id.',#altimerform-end_dt-input'.$model->id.'").datetimepicker({
  allowInputToggle: true,
  viewMode: "days",
  format: "YYYY-MM-DD hh:mm:ss",
  icons: {
    time: "fa fa-clock",
  },
});
$("#altimerform-start_dt-input'.$model->id.'").on("change.datetimepicker", function (e) {
  $("#altimerform-end_dt-input'.$model->id.'").datetimepicker("minDate", e.date);
});
$("#altimerform-end_dt-input'.$model->id.'").on("change.datetimepicker", function (e) {
  $("#altimerform-start_dt-input'.$model->id.'").datetimepicker("maxDate", e.date);
});
');
?>
<?php $form = ActiveForm::begin($frmOpts); ?>
<?= $form->field($model, 'comments')->textArea(['rows'=>3,'placeholder'=>$model->getAttributeLabel('comments'),'class'=>$inputClass])->label(false)?>
<label><?= Yii::t('app','Duration')?></label>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'hours',['template'=>'
    <div class="input-group'.$inputGroupClass.'">
      {input}
      <div class="input-group-append">
        <span class="input-group-text"><label class="control-label">'.Yii::t('app','Hours').'</label></span>
      </div>
    </div>
    {error}{hint}
    '])->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('hours'),'class'=>$inputClass])?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'minutes',['template'=>'
    <div class="input-group'.$inputGroupClass.'">
      {input}
      <div class="input-group-append">
        <span class="input-group-text"><label class="control-label">'.Yii::t('app','Minutes').'</label></span>
      </div>
    </div>
    {error}{hint}
    '])->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('minutes'),'class'=>$inputClass])?>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'start_dt',['template'=>'
    {label}
    <div class="input-group date" id="altimerform-start_dt-input'.$model->id.'" data-target-input="nearest">
      {input}
      <div class="input-group-append" data-target="#altimerform-start_dt-input'.$model->id.'" data-toggle="datetimepicker">
          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
      </div>
    </div>
    {hint}{error}
    '])->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('start_dt'),'class'=>$dtInputClass])?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'end_dt',['template'=>'
    {label}
    <div class="input-group date" id="altimerform-end_dt-input'.$model->id.'" data-target-input="nearest">
      {input}
      <div class="input-group-append" data-target="#altimerform-end_dt-input'.$model->id.'" data-toggle="datetimepicker">
          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
      </div>
    </div>
    {hint}{error}
    '])->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('end_dt'),'class'=>$dtInputClass])?>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'visibility',['template'=>$inputGroupTemplate])->dropDownList(Yii::$app->helperFunctions->visibilityListArr,['class'=>$inputClass])?>
  </div>
  <div class="col-sm-6 text-right">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn'.$btnClass.' btn-success']) ?>
  </div>
</div>
<?php ActiveForm::end(); ?>
