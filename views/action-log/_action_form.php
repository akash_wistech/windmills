<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
if(Yii::$app->controller->action->id=='update'){
  $frmOpts=[
    'id'=>'al-update-form',
    'action'=>['action-log/update','id'=>$model->id]
  ];
  $this->registerJs('
  $("body").on("click", "#update-log #alactionform-reminder", function () {
    if($(this).prop("checked")){
  		$("#actionReminderInfo'.$model->id.'").show();
  	}else{
  		$("#actionReminderInfo'.$model->id.'").hide();
  	}
  });
  ');
}else{
  $frmOpts=[
    'id'=>'al-action-form',
    'action'=>['action-log/save','ltype'=>$model->rec_type,'mtype'=>$model->module_type,'mid'=>$model->module_id]
  ];
  $this->registerJs('
  $("body").on("click", ".activity-logger-card #alactionform-reminder", function () {
    if($(this).prop("checked")){
  		$("#actionReminderInfo").show();
  	}else{
  		$("#actionReminderInfo").hide();
  	}
  });
  ');
}
$this->registerJs('
$("#alactionform-due_date-input'.$model->id.'").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
$("#alactionform-assign_to'.$model->id.'").select2({
  allowClear: true,
  width: "100%",
});
');
?>
<?php $form = ActiveForm::begin($frmOpts); ?>
<div class="row">
  <div class="col-sm-7">
    <?= $form->field($model, 'subject')->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('subject'),'class'=>$inputClass])->label(false)?>
  </div>
  <div class="col-sm-5 text-right">
    <?= $form->field($model, 'due_date',['template'=>'
    {label}
    <div class="input-group date" id="alactionform-due_date-input'.$model->id.'" data-target-input="nearest">
      {input}
      <div class="input-group-append" data-target="#alactionform-due_date-input'.$model->id.'" data-toggle="datetimepicker">
          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
      </div>
    </div>
    {hint}{error}
    '])->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('due_date'),'class'=>$inputClass,'autocomplete'=>'off'])->label(false)?>
  </div>
</div>
<?= $form->field($model, 'comments')->textArea(['rows'=>3,'placeholder'=>$model->getAttributeLabel('comments'),'class'=>$inputClass])->label(false)?>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'priority',['template'=>$inputGroupTemplate])->dropDownList(Yii::$app->helperFunctions->priorityListArr,['class'=>$inputClass])?>
  </div>
  <div class="col-sm-6 text-right">
    <?= $form->field($model, 'visibility',['template'=>$inputGroupTemplate])->dropDownList(Yii::$app->helperFunctions->visibilityListArr,['class'=>$inputClass])?>
  </div>
</div>
<?= $form->field($model, 'assign_to')->dropDownList(Yii::$app->appHelperFunctions->staffMemberListArr,['id'=>'alactionform-assign_to'.$model->id,'multiple'=>'multiple','class'=>$inputClass])?>
<?= $form->field($model, 'reminder',['options'=>['class'=>'form-group mb-0 lbl-mb-0']])->checkbox(); ?>
<div id="actionReminderInfo<?= $model->id?>" class="<?= $model->reminder==1 ? '' : 'hidden'?>">
  <?= Yii::T('app','Create a notification reminder for')?>
  <div class="row">
    <div class="col-sm-6">
      <?= $form->field($model, 'reminder_to',['options'=>['class'=>'form-group mb-0']])->dropDownList(Yii::$app->helperFunctions->actionLogActionRemindToListArr,['class'=>$inputClass])->label(false)?>
    </div>
    <div class="col-sm-6 text-right">
      <?= $form->field($model, 'remind_time',['options'=>['class'=>'form-group mb-0']])->dropDownList(Yii::$app->helperFunctions->actionLogActionRemindTimeListArr,['class'=>$inputClass])->label(false)?>
    </div>
  </div>
  <?= Yii::t('app','before this action is due')?>
</div>
<div class="mt-2">
  <?= $form->field($model, 'calendar_id')->dropDownList(Yii::$app->appHelperFunctions->myCalendarListArr,['prompt'=>Yii::t('app','None'),'class'=>$inputClass])?>
</div>
<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn'.$btnClass.' btn-success']) ?>
<?php ActiveForm::end(); ?>
