<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
if(Yii::$app->controller->action->id=='update'){
  $frmOpts=[
    'id'=>'al-update-form',
    'action'=>['action-log/update','id'=>$model->id]
  ];
}else{
  $frmOpts=[
    'id'=>'al-call-form',
    'action'=>['action-log/save','ltype'=>$model->rec_type,'mtype'=>$model->module_type,'mid'=>$model->module_id]
  ];
}
$this->registerJs('
$("#alcallform-start_dt-input'.$model->id.',#alcallform-end_dt-input'.$model->id.'").datetimepicker({
  allowInputToggle: true,
  viewMode: "days",
  format: "YYYY-MM-DD hh:mm:ss",
  icons: {
    time: "fa fa-clock",
  },
});
$("#alcallform-start_dt-input'.$model->id.'").on("change.datetimepicker", function (e) {
  $("#alcallform-end_dt-input'.$model->id.'").datetimepicker("minDate", e.date);
});
$("#alcallform-end_dt-input'.$model->id.'").on("change.datetimepicker", function (e) {
  $("#alcallform-start_dt-input'.$model->id.'").datetimepicker("maxDate", e.date);
});
'.Yii::$app->jsFunctions->logCallActionResponseList.'
$("body").on("change", "#alcallform-qnoteaction'.$model->id.'", function () {
  console.log("Here");
  html="<option value=\"\">-</option>";
  resOptions=alLogCallActionResponses[$(this).val()];
  jQuery.each(resOptions, function(i, val){
    html+="<option value=\""+i+"\">"+val+"</option>";
  });
  $("#alcallform-qnoteresponse'.$model->id.'").html(html);
});
$("body").on("change", "#alcallform-qnoteresponse'.$model->id.'", function () {
  selValue=resOptions=alLogCallActionResponses[$("#alcallform-qnoteaction'.$model->id.'").val()][$(this).val()];
  $("#alcallform-comments'.$model->id.'").val(selValue);
});
');
?>
<?php $form = ActiveForm::begin($frmOpts); ?>
<?= $form->field($model, 'qnoteAction',['template'=>'
<div class="input-group'.$inputGroupClass.'">
  <div class="input-group-prepend">
    <span class="input-group-text">{label}</span>
  </div>
  {input}
  &nbsp;:&nbsp;
  '.$form->field($model, 'qnoteResponse',['options'=>['class'=>'form-group mb-0']])->dropDownList([],['prompt'=>'-','id'=>'alcallform-qnoteresponse'.$model->id,'class'=>$inputClass])->label(false).'
</div>
{error}{hint}
'])->dropDownList(Yii::$app->helperFunctions->actionLogCallQuickNoteAction,['prompt'=>'','id'=>'alcallform-qnoteaction'.$model->id,'class'=>$inputClass])?>
<?= $form->field($model, 'comments')->textArea(['id'=>'alcallform-comments'.$model->id,'rows'=>3,'placeholder'=>$model->getAttributeLabel('comments'),'class'=>$inputClass])->label(false)?>
<label><?= Yii::t('app','Duration')?></label>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'hours',['template'=>'
    <div class="input-group'.$inputGroupClass.'">
      {input}
      <div class="input-group-append">
        <span class="input-group-text"><label class="control-label">'.Yii::t('app','Hours').'</label></span>
      </div>
    </div>
    {error}{hint}
    '])->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('hours'),'class'=>$inputClass])?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'minutes',['template'=>'
    <div class="input-group'.$inputGroupClass.'">
      {input}
      <div class="input-group-append">
        <span class="input-group-text"><label class="control-label">'.Yii::t('app','Minutes').'</label></span>
      </div>
    </div>
    {error}{hint}
    '])->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('minutes'),'class'=>$inputClass])?>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'start_dt',['template'=>'
    {label}
    <div class="input-group date" id="alcallform-start_dt-input'.$model->id.'" data-target-input="nearest">
      {input}
      <div class="input-group-append" data-target="#alcallform-start_dt-input'.$model->id.'" data-toggle="datetimepicker">
          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
      </div>
    </div>
    {hint}{error}
    '])->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('start_dt'),'class'=>$dtInputClass,'autocomplete'=>'off'])?>
  </div>
  <div class="col-sm-6">
    <?= $form->field($model, 'end_dt',['template'=>'
    {label}
    <div class="input-group date" id="alcallform-end_dt-input'.$model->id.'" data-target-input="nearest">
      {input}
      <div class="input-group-append" data-target="#alcallform-end_dt-input'.$model->id.'" data-toggle="datetimepicker">
          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
      </div>
    </div>
    {hint}{error}
    '])->textInput(['maxlength'=>true,'placeholder'=>$model->getAttributeLabel('end_dt'),'class'=>$dtInputClass,'autocomplete'=>'off'])?>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <?= $form->field($model, 'visibility',['template'=>$inputGroupTemplate])->dropDownList(Yii::$app->helperFunctions->visibilityListArr,['class'=>$inputClass])?>
  </div>
  <div class="col-sm-6 text-right">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn'.$btnClass.' btn-success']) ?>
  </div>
</div>
<?php ActiveForm::end(); ?>
