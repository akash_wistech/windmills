<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$inputClass = Yii::$app->activityHelper->actionLogMdInputClass;
$dtInputClass = Yii::$app->activityHelper->actionLogDtMdInputClass;
$inputGroupTemplate = Yii::$app->activityHelper->actionLogMdInputGroupTemplate;
$inputGroupClass = ' input-group-md';
$btnClass = '';
?>
<div id="update-log">
<?php
if($modelLog->rec_type=='comment'){
  $model->comments=$modelLog->comments;
  $model->visibility=$modelLog->visibility;
  echo $this->render('/action-log/_comment_form',[
    'model'=>$model,
    'inputClass'=>$inputClass,
    'dtInputClass'=>$dtInputClass,
    'inputGroupTemplate'=>$inputGroupTemplate,
    'inputGroupClass'=>$inputGroupClass,
    'btnClass'=>$btnClass,
  ]);
}
if($modelLog->rec_type=='action'){
  if($modelLog->actionInfo!=null){
    $model->subject=$modelLog->actionInfo['subject'];
    $model->due_date=$modelLog->actionInfo['due_date'];
  }
  $model->priority=$modelLog->priority;
  $model->comments=$modelLog->comments;
  $model->visibility=$modelLog->visibility;
  $model->assign_to=ArrayHelper::map($modelLog->managerIdz,"staff_id","staff_id");
  if($modelLog->reminderInfo!=null){
    $model->reminder=1;
    $model->reminder_to=$modelLog->reminderInfo['notify_to'];
    $model->remind_time=$modelLog->reminderInfo['notify_time'];
  }
  if($modelLog->calendarInfo!=null){
    $model->calendar_id=$modelLog->calendarInfo['calendar_id'];
  }
  echo $this->render('/action-log/_action_form',[
    'model'=>$model,
    'inputClass'=>$inputClass,
    'dtInputClass'=>$dtInputClass,
    'inputGroupTemplate'=>$inputGroupTemplate,
    'inputGroupClass'=>$inputGroupClass,
    'btnClass'=>$btnClass,
  ]);
}
if($modelLog->rec_type=='call'){
  $model->comments=$modelLog->comments;
  if($modelLog->timeInfo!=null){
    $model->hours=$modelLog->timeInfo['hours'];
    $model->minutes=$modelLog->timeInfo['minutes'];
    $model->start_dt=$modelLog->timeInfo['start_dt'];
    $model->end_dt=$modelLog->timeInfo['end_dt'];
  }
  $model->visibility=$modelLog->visibility;
  echo $this->render('/action-log/_call_form',[
    'model'=>$model,
    'inputClass'=>$inputClass,
    'dtInputClass'=>$dtInputClass,
    'inputGroupTemplate'=>$inputGroupTemplate,
    'inputGroupClass'=>$inputGroupClass,
    'btnClass'=>$btnClass,
  ]);
}
if($modelLog->rec_type=='timer'){
  $model->comments=$modelLog->comments;
  $model->hours=$modelLog->timeInfo['hours'];
  $model->minutes=$modelLog->timeInfo['minutes'];
  $model->start_dt=$modelLog->timeInfo['start_dt'];
  $model->end_dt=$modelLog->timeInfo['end_dt'];
  $model->visibility=$modelLog->visibility;
  echo $this->render('/action-log/_time_form',[
    'model'=>$model,
    'inputClass'=>$inputClass,
    'dtInputClass'=>$dtInputClass,
    'inputGroupTemplate'=>$inputGroupTemplate,
    'inputGroupClass'=>$inputGroupClass,
    'btnClass'=>$btnClass,
  ]);
}
if($modelLog->rec_type=='calendar'){
  $model->priority=$modelLog->priority;
  $model->comments=$modelLog->comments;
  $model->assign_to=ArrayHelper::map($modelLog->managerIdz,"staff_id","staff_id");
  if($modelLog->calendarEventInfo!=null){
    $model->event_start=$modelLog->calendarEventInfo['start_date'];
    $model->event_end=$modelLog->calendarEventInfo['end_date'];
    $model->color=$modelLog->calendarEventInfo['color_code'];
    $model->allday=$modelLog->calendarEventInfo['all_day'];
    $model->sub_type=$modelLog->calendarEventInfo['sub_type'];
    $model->status=$modelLog->calendarEventInfo['status'];
  }
  if($modelLog->reminderInfo!=null){
    $model->reminder=1;
    $model->reminder_to=$modelLog->reminderInfo['notify_to'];
    $model->remind_time=$modelLog->reminderInfo['notify_time'];
  }
  if($modelLog->calendarInfo!=null){
    $model->calendar_id=$modelLog->calendarInfo['calendar_id'];
  }
  echo $this->render('/action-log/_calendar_form',[
    'model'=>$model,
    'inputClass'=>$inputClass,
    'dtInputClass'=>$dtInputClass,
    'inputGroupTemplate'=>$inputGroupTemplate,
    'inputGroupClass'=>$inputGroupClass,
    'btnClass'=>$btnClass,
  ]);
}
?>
</div>
