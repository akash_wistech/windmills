<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AutoListingsMaxSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auto-listings-max-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'too_less_records_limit_from') ?>

    <?= $form->field($model, 'too_less_records_limit_to') ?>

    <?= $form->field($model, 'too_less_bua_percentage') ?>

    <?= $form->field($model, 'too_less_price_percentage') ?>

    <?php // echo $form->field($model, 'too_less_date') ?>

    <?php // echo $form->field($model, 'less_records_limit_from') ?>

    <?php // echo $form->field($model, 'less_records_limit_to') ?>

    <?php // echo $form->field($model, 'less_bua_percentage') ?>

    <?php // echo $form->field($model, 'less_price_percentage') ?>

    <?php // echo $form->field($model, 'less_date') ?>

    <?php // echo $form->field($model, 'moderate_records_limit_from') ?>

    <?php // echo $form->field($model, 'moderate_records_limit_to') ?>

    <?php // echo $form->field($model, 'moderate_bua_percentage') ?>

    <?php // echo $form->field($model, 'moderate_price_percentage') ?>

    <?php // echo $form->field($model, 'moderate_date') ?>

    <?php // echo $form->field($model, 'many_records_limit_from') ?>

    <?php // echo $form->field($model, 'many_records_limit_to') ?>

    <?php // echo $form->field($model, 'many_bua_percentage') ?>

    <?php // echo $form->field($model, 'many_price_percentage') ?>

    <?php // echo $form->field($model, 'many_date') ?>

    <?php // echo $form->field($model, 'too_many_records_limit_from') ?>

    <?php // echo $form->field($model, 'too_many_records_limit_to') ?>

    <?php // echo $form->field($model, 'too_many_bua_percentage') ?>

    <?php // echo $form->field($model, 'too_many_price_percentage') ?>

    <?php // echo $form->field($model, 'too_many_date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'data_type') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'search_limit') ?>

    <?php // echo $form->field($model, 'status_verified_at') ?>

    <?php // echo $form->field($model, 'status_verified_by') ?>

    <?php // echo $form->field($model, 'city_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
