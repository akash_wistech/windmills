<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AutoListingsMax */

$this->title = ucwords($model->data_type).' - '.$model->cityName->title;
$this->params['breadcrumbs'][] = ['label' => 'Auto Listings Maxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="auto-listings-max-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
