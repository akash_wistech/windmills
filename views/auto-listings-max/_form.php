<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\AutoLististings */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="auto-lististings-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" >Data Type</label>
                    <input type="text"  class="form-control"  maxlength="255" value="<?= ucfirst($model->data_type) ?> - <?= $model->cityName->title ?>" readonly>

                </div>
            </div>

            <div class="col-sm-4">

                    <?= $form->field($model, 'search_limit')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Limit From') ?>

            </div>
            <div class="col-sm-4">

                    <?= $form->field($model, 'search_limit_sc')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Limit To Check Sub Community') ?>

            </div>


        </div>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Too Less Records Mode') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'too_less_records_limit_from')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Limit From') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'too_less_records_limit_to')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Limit To') ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'too_less_date')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->dateArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Date Limit');
                        ?>

                    </div>
                    <!--  <div class="col-sm-4">
                        <?/*= $form->field($model, 'relax_date')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Date Limit(%)') */?>
                    </div>-->
                    <div class="col-sm-4">
                        <?= $form->field($model, 'too_less_bua_price_percentage_1')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 1 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'too_less_bua_price_percentage_2')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 2 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'too_less_bua_price_percentage_3')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 3 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                </div>

            </div>

        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Records Less Mode') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_records_limit_from')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Limit From') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_records_limit_to')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Limit To') ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'less_date')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->dateArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Date Limit');
                        ?>

                    </div>
                    <!--  <div class="col-sm-4">
                        <?/*= $form->field($model, 'relax_date')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Date Limit(%)') */?>
                    </div>-->
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_bua_price_percentage_1')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 1 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_bua_price_percentage_2')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 2 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_bua_price_percentage_3')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 3 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                </div>

            </div>

        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Moderate Records Mode') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'moderate_records_limit_from')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Limit From') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'moderate_records_limit_to')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Limit To') ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'moderate_date')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->dateArr,
                            /* 'options' => ['placeholder' => 'Relax Date'],*/
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Date Limit');
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'moderate_bua_price_percentage_1')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 1 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'moderate_bua_price_percentage_2')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 2 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'moderate_bua_price_percentage_3')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 3 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                </div>

            </div>

        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Many Records Mode') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'many_records_limit_from')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Limit From') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'many_records_limit_to')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Limit To') ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'many_date')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->dateArr,
                            /* 'options' => ['placeholder' => 'Relax Date'],*/
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Date Limit');
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'many_bua_price_percentage_1')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 1 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'many_bua_price_percentage_2')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 2 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'many_bua_price_percentage_3')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 3 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                </div>

            </div>

        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Too Many Records Mode') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'too_many_records_limit_from')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Greater Than') ?>
                    </div>
                   <!-- <div class="col-sm-4">
                        <?/*= $form->field($model, 'too_many_records_limit_to')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Records Limit To') */?>
                    </div>-->
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'too_many_date')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->dateArr,
                            /* 'options' => ['placeholder' => 'Relax Date'],*/
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Date Limit');
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'too_many_bua_price_percentage_1')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 1 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'too_many_bua_price_percentage_2')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 2 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'too_many_bua_price_percentage_3')->textInput(['maxlength' => true,'type'=> 'number', 'step'=>'.01'])->label('Level 3 : BUA & Price/sqt (%) +/-') ?>
                    </div>
                </div>

            </div>

        </section>


    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
