<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AutoListingsMax */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Auto Listings Maxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="auto-listings-max-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'too_less_records_limit_from',
            'too_less_records_limit_to',
            'too_less_bua_percentage',
            'too_less_price_percentage',
            'too_less_date',
            'less_records_limit_from',
            'less_records_limit_to',
            'less_bua_percentage',
            'less_price_percentage',
            'less_date',
            'moderate_records_limit_from',
            'moderate_records_limit_to',
            'moderate_bua_percentage',
            'moderate_price_percentage',
            'moderate_date',
            'many_records_limit_from',
            'many_records_limit_to',
            'many_bua_percentage',
            'many_price_percentage',
            'many_date',
            'too_many_records_limit_from',
            'too_many_records_limit_to',
            'too_many_bua_percentage',
            'too_many_price_percentage',
            'too_many_date',
            'status',
            'data_type',
            'updated_at',
            'updated_by',
            'search_limit',
            'status_verified_at',
            'status_verified_by',
            'city_id',
        ],
    ]) ?>

</div>
