<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AutoListingsMax */

$this->title = 'Create Auto Listings Max';
$this->params['breadcrumbs'][] = ['label' => 'Auto Listings Maxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auto-listings-max-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
