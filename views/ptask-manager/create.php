<?php

use yii\helpers\Html;

$this->title = 'Create Periodic Task';
$this->params['breadcrumbs'][] = ['label' => 'Periodic Task Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
