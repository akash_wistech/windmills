<?php

use yii\helpers\Html;

$this->title = 'Update Periodic Task: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Periodic Task Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="task-update">

    <?= $this->render('_form', [
        'model' => $model,
        'page_title' => $this->title,
    ]) ?>

</div>
