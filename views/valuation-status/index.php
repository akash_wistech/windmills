<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\ValuationReportsSearch;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);



$this->title = Yii::t('app', 'Valuation Status Lists');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;


$yearArr = \app\models\valuation::find()->select(['yearcol'=>'YEAR(created_at)'])->distinct('created_at')->asArray()->all();
$yearArr = ArrayHelper::map($yearArr, "yearcol", "yearcol");
$yearsArrHelper = $yearArr;
$yearArr['all-time'] = 'All Time';


$monthsArr = [
    'monthly'   => 'Monthly',
    '1-quarter' => '1st Quarter',
    '2-quarter' => '2nd Quarter',
    '3-quarter' => '3rd Quarter',
    '4-quarter' => '4th Quarter',
    'all-quarter' => 'All Quarter',
];

$monthsArrForLoop = [
    '01' => 'January',
    '02' => 'February',
    '03' => 'March',
    '04' => 'April',
    '05' => 'May',
    '06' => 'June',
    '07' => 'July ',
    '08' => 'August',
    '09' => 'September',
    '10' => 'October',
    '11' => 'November',
    '12' => 'December',
];

$model = new \app\models\Valuation();

if($data['valuation_month']<>null){
    $model->valuation_month = $data['valuation_month'];
}else{
    $data['valuation_month'] = 'monthly';
    $model->valuation_month = $data['valuation_month'];
}

if($data['valuation_year']<>null){
    $model->valuation_year = $data['valuation_year'];
}else{
    $data['valuation_year'] = 2022;
    $model->valuation_year = $data['valuation_year'];
}
if($data['valuation_year']=='all-time'){
    $model->valuation_month = null;
    $data['valuation_month'] = null;
}

// echo"<pre>"; print_r($data); echo"</pre>"; 
// echo"<pre>"; print_r($yearsArrHelper); echo"</pre>"; 

?>

<div class="card">
    <div class="card-body">
        <?php $form = ActiveForm::begin([
    'action' => ['valuation-status-all'],
    'method' => 'get',
]); ?>
        <div class="row">
            <div class="col-5">
                <?php
echo $form->field($model, 'valuation_month')->widget(Select2::classname(), [
    'data' => $monthsArr,
    'options' => ['placeholder' => 'Select Month ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
    ])->label(false);
    ?>
            </div>
            <div class="col-5">
                <?php
    echo $form->field($model, 'valuation_year')->widget(Select2::classname(), [
        'data' => $yearArr,
        'options' => ['placeholder' => 'Select Year ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
        ])->label(false);
        ?>
            </div>
            <div class="col-2">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary ']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
        if($data['valuation_month'] == 'monthly' && $data['valuation_year'] != 'all-time'){
            echo $this->render('../valuation-status/by-month', [
                'data' => $data,
                'monthsArrForLoop' => $monthsArrForLoop,
            ]);
        }
        else if($data['valuation_year'] == 'all-time'){
            echo $this->render('../valuation-status/by-year', [
                'data' => $data,
                'monthsArrForLoop' => $monthsArrForLoop,
                'yearsArrHelper' => $yearsArrHelper,
            ]);
        }
        else if($data['valuation_month']=='1-quarter' || $data['valuation_month']=='2-quarter' || $data['valuation_month']=='3-quarter' || $data['valuation_month']=='4-quarter'  || $data['valuation_month']='all-quarter' ){
                echo $this->render('../valuation-status/by-quarter', [
                        'data' => $data,
                        'monthsArrForLoop' => $monthsArrForLoop,
                    ]);
                }
                