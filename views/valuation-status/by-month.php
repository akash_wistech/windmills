<?php 
use app\models\Valuation;
use app\models\ReceivedDocsFiles;
use app\models\ScheduleInspection;
use yii\db\Expression;



function GetResult($month_key, $year){
    $total_val_recieved=$total_doc_requested=$total_inspect_requested=$total_approved=$total_rejected='';
    $per_total_val_recieved=$per_total_doc_requested=$per_total_inspect_requested=$per_total_approved=$per_total_rejected=0;
    
    if($year!='all-times' && $month_key<>null){
        $total_valuations = Valuation::find()->count("id");
        
        //total_val_recieved
        $total_val_recieved = Valuation::find()
        ->where(new Expression(" YEAR(instruction_date) = ".$year." AND MONTH(instruction_date) = ".$month_key." "))
        ->count("id");
        $per_total_val_recieved = $total_val_recieved/$total_valuations*100;
        
        //total_doc_requested
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("select count(valuation.id) as total_doc_requested from valuation where valuation.id in (select valuation_id from received_docs_files where YEAR(received_docs_files.created_at) = ".$year." AND MONTH(received_docs_files.created_at) = ".$month_key.") ");
        $command = $command->queryAll();
        $total_doc_requested = $command[0]['total_doc_requested'];
        $per_total_doc_requested = $total_doc_requested/$total_valuations*100;
        
        //total_inspect_requested
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(" select count(valuation.id) as total_inspect_requested from valuation where valuation.id in (select valuation_id from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$year." AND MONTH(schedule_inspection.inspection_date) =  ".$month_key.") ");
        $command = $command->queryAll();
        $total_inspect_requested = $command[0]['total_inspect_requested'];
        $per_total_inspect_requested = $total_inspect_requested/$total_valuations*100;
        
        //total_approved
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(" 
        select count(valuation.id) as total_approved from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Approve' AND YEAR(valuation_approvers_data.created_at) = ".$year." AND MONTH(valuation_approvers_data.created_at) = ".$month_key.")
        ");
        $command = $command->queryAll();
        $total_approved = $command[0]['total_approved'];
        $per_total_approved = $total_approved/$total_valuations*100;
        
        
        
        //total_rejected
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(" 
        select count(valuation.id) as total_rejected from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Reject' AND YEAR(valuation_approvers_data.created_at) = ".$year." AND MONTH(valuation_approvers_data.created_at) = ".$month_key.")
        ");
        $command = $command->queryAll();
        $total_rejected = $command[0]['total_rejected'];
        $per_total_rejected = $total_rejected/$total_valuations*100;
        
        
        
    }
    
    
    
    return [
        'total_val_recieved'          => $total_val_recieved,
        'total_doc_requested'         => $total_doc_requested,
        'total_inspect_requested'     => $total_inspect_requested,
        'total_approved'              => $total_approved,
        'total_rejected'              => $total_rejected,
        'per_total_val_recieved'      => $per_total_val_recieved,
        'per_total_doc_requested'     => $per_total_doc_requested,
        'per_total_inspect_requested' => $per_total_inspect_requested,
        'per_total_approved'          => $per_total_approved,
        'per_total_rejected'          => $per_total_rejected,
    ];
}

?>

<div class="row">
    <?php 
foreach ($monthsArrForLoop as $month_key => $month) {
    if($data['valuation_year']<>null){
        $result = GetResult($month_key, $data['valuation_year']);
        // echo "<pre>"; print_r($result); echo "</pre>"; die;
    }else{
        $result = GetResult($month_key, 2022);
        // echo "<pre>"; print_r($result); echo "</pre>"; die;
    }
    
    ?>


    <div class="col-4">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">List - <?=$month?></h3>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap table-striped">
                    <thead>
                        <tr>
                            <th>Valuation Status</th>
                            <th>Total Valuation</th>
                            <th>Percentage (%)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-primary">Valuation Received</td>
                            <td><?=$result['total_val_recieved'] ?></td>
                            <td><?= round($result['per_total_val_recieved'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Documents Requested</td>
                            <td><?=$result['total_doc_requested'] ?></td>
                            <td><?= round($result['per_total_doc_requested'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Inspection Requested</td>
                            <td><?=$result['total_inspect_requested'] ?></td>
                            <td><?= round($result['per_total_inspect_requested'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Approved</td>
                            <td><?=$result['total_approved'] ?></td>
                            <td><?= round($result['per_total_approved'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Rejected</td>
                            <td><?=$result['total_rejected'] ?></td>
                            <td><?= round($result['per_total_rejected'], 2) ?> %</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php
}
?>
</div>
<?php
$this->registerJs('');
?>