<?php 
use app\models\Valuation;
use app\models\ReceivedDocsFiles;
use app\models\ScheduleInspection;
use yii\db\Expression;


function result($data)
{
    $total_val_recieved=$total_doc_requested=$total_inspect_requested=$total_approved=$total_rejected=$total_valuations=0;
    $per_total_val_recieved=$per_total_doc_requested=$per_total_inspect_requested=$per_total_approved=$per_total_rejected=0;
    // echo "<pre>"; print_r($data); echo "</pre>"; die;
    
    $date_array = Yii::$app->appHelperFunctions->getThisQuarterNew($data['valuation_month'], $data['valuation_year'], );
    $date_from = date('Y-m-d', $date_array['start_date']);
    $date_to = date('Y-m-d', $date_array['end_date']);
    
    
    // echo "<pre>"; print_r($date_from); echo "</pre>";
    // echo "<pre>"; print_r($date_to); echo "</pre>"; 
    // die();      
    $total_valuations = Valuation::find()->count("id");
    
    $total_val_recieved = Valuation::find()
    ->where(new Expression(" YEAR(instruction_date) = ".$data['valuation_year']." "))
    ->andWhere([ 'between', 'instruction_date', $date_from, $date_to ])
    ->count("id");
    $per_total_val_recieved = $total_val_recieved/$total_valuations*100;
    
    $connection = Yii::$app->getDb();
    $command = $connection->createCommand(" select count(valuation.id) as total_doc_requested from valuation where valuation.id in (select valuation_id from received_docs_files where YEAR(received_docs_files.created_at) = ".$data['valuation_year']." AND received_docs_files.created_at BETWEEN '".$date_from."' AND '".$date_to."') ");
    // $command = $connection->createCommand(" select count(valuation.id) as total_doc_requested from valuation where valuation.id in (select valuation_id from received_docs_files where YEAR(valuation.created_at) = ".$data['valuation_year']." AND valuation.created_at BETWEEN '".$date_from."' AND '".$date_to."') ");
    // $command = $connection->createCommand("SELECT COUNT( DISTINCT valuation_id ) as total_doc_requested FROM `received_docs_files` WHERE YEAR(created_at) = ".$data['valuation_year']." AND created_at BETWEEN '".$date_from."' AND '".$date_to."' ");
    $command = $command->queryAll();
    $total_doc_requested = $command[0]['total_doc_requested'];
    $per_total_doc_requested = $total_doc_requested/$total_valuations*100;
    
    
    $connection = Yii::$app->getDb();
    $command = $connection->createCommand(" select count(valuation.id) as total_inspect_requested from valuation where valuation.id in (select valuation_id from schedule_inspection where YEAR(schedule_inspection.inspection_date) = ".$data['valuation_year']." AND schedule_inspection.inspection_date BETWEEN '".$date_from."' AND '".$date_to."') ");
    // $command = $connection->createCommand(" select count(valuation.id) as total_doc_requested from valuation where valuation.id in (select valuation_id from received_docs_files where YEAR(valuation.created_at) = ".$data['valuation_year']." AND valuation.created_at BETWEEN '".$date_from."' AND '".$date_to."') ");
    // $command = $connection->createCommand("SELECT COUNT( DISTINCT valuation_id ) as total_doc_requested FROM `received_docs_files` WHERE YEAR(created_at) = ".$data['valuation_year']." AND created_at BETWEEN '".$date_from."' AND '".$date_to."' ");
    $command = $command->queryAll();
    $total_inspect_requested = $command[0]['total_inspect_requested'];
    $per_total_inspect_requested = $total_inspect_requested/$total_valuations*100;
    // $total_inspect_requested = ScheduleInspection::find()
    // ->where(new Expression(" YEAR(inspection_date) = ".$data['valuation_year']." "))
    // ->andWhere([ 'between', 'inspection_date', $date_from, $date_to ])
    // ->count("id");
    
    $connection = Yii::$app->getDb();
    // $command = $connection->createCommand(" 
    // SELECT COUNT( DISTINCT valuation_id ) as total_approved FROM `valuation_approvers_data` 
    // WHERE approver_type = 'approver' AND status = 'Approve' 
    // AND YEAR(created_at) = ".$data['valuation_year']." 
    // AND created_at BETWEEN '".$date_from."' AND '".$date_to."' 
    // ");
    $command = $connection->createCommand(" 
    select count(valuation.id) as total_approved from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Approve' AND YEAR(valuation_approvers_data.created_at) = ".$data['valuation_year']." AND valuation_approvers_data.created_at BETWEEN '".$date_from."' AND '".$date_to."') 
    ");
    $command = $command->queryAll();
    $total_approved = $command[0]['total_approved'];
    $per_total_approved = $total_approved/$total_valuations*100;
    
    $connection = Yii::$app->getDb();
    // $command = $connection->createCommand("
    // SELECT COUNT( DISTINCT valuation_id ) as total_rejected FROM `valuation_approvers_data` 
    // WHERE approver_type = 'approver' AND status = 'Reject' 
    // AND YEAR(created_at) = ".$data['valuation_year']." 
    // AND created_at BETWEEN '".$date_from."' AND '".$date_to."' 
    // ");
    $command = $connection->createCommand(" 
    select count(valuation.id) as total_rejected from valuation where valuation.id in (select valuation_approvers_data.valuation_id from valuation_approvers_data where approver_type = 'approver' AND status = 'Reject' AND YEAR(valuation_approvers_data.created_at) = ".$data['valuation_year']." AND valuation_approvers_data.created_at BETWEEN '".$date_from."' AND '".$date_to."') 
    ");
    $command = $command->queryAll();
    $total_rejected = $command[0]['total_rejected'];
    $per_total_rejected = $total_rejected/$total_valuations*100;
    
    
    return[
        'total_val_recieved'          => $total_val_recieved,
        'total_doc_requested'         => $total_doc_requested,
        'total_inspect_requested'     => $total_inspect_requested,
        'total_approved'              => $total_approved,
        'total_rejected'              => $total_rejected,
        'per_total_val_recieved'      => $per_total_val_recieved,
        'per_total_doc_requested'     => $per_total_doc_requested,
        'per_total_inspect_requested' => $per_total_inspect_requested,
        'per_total_approved'          => $per_total_approved,
        'per_total_rejected'          => $per_total_rejected,
    ];
}



// $date_array = Yii::$app->appHelperFunctions->getThisQuarterNew($data['valuation_month'], $data['valuation_year']);
// $date_from = date('Y-m-d', $date_array['start_date']);
// $date_to = date('Y-m-d', $date_array['end_date']);
?>

<div class="row">
    <?php
if($data['valuation_month']!='all-quarter') {
    $result = result($data);
    ?>

    <div class="col-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">List - <?=$year?></h3>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap table-striped">
                    <thead>
                        <tr>
                            <th>Valuation Status</th>
                            <th>Total Valuation</th>
                            <th>Percentage (%)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-primary">Valuation Received</td>
                            <td><?=$result['total_val_recieved'] ?></td>
                            <td><?= round($result['per_total_val_recieved'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Documents Requested</td>
                            <td><?=$result['total_doc_requested'] ?></td>
                            <td><?= round($result['per_total_doc_requested'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Inspection Requested</td>
                            <td><?=$result['total_inspect_requested'] ?></td>
                            <td><?= round($result['per_total_inspect_requested'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Approved</td>
                            <td><?=$result['total_approved'] ?></td>
                            <td><?= round($result['per_total_approved'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Rejected</td>
                            <td><?=$result['total_rejected'] ?></td>
                            <td><?= round($result['per_total_rejected'], 2) ?> %</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}else{
    for ($i=1; $i <=4 ; $i++) {
        if($i==1){$num='1st'; $data['valuation_month'] = '1-quarter';}
        if($i==2){$num='2nd'; $data['valuation_month'] = '2-quarter';}
        if($i==3){$num='3rd'; $data['valuation_month'] = '3-quarter';}
        if($i==4){$num='4th'; $data['valuation_month'] = '4-quarter';}
        
        
        $result = result($data);
        // echo "<pre>"; print_r($result); echo"</pre>"; //die();
        
        ?>

    <div class="col-4">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title"><?=$num?> Quarter - <?= $data['valuation_year'] ?></h3>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap table-striped">
                    <thead>
                        <tr>
                            <th>Valuation Status</th>
                            <th>Total Valuation</th>
                            <th>Percentage (%)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-primary">Valuation Received</td>
                            <td><?=$result['total_val_recieved'] ?></td>
                            <td><?= round($result['per_total_val_recieved'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Documents Requested</td>
                            <td><?=$result['total_doc_requested'] ?></td>
                            <td><?= round($result['per_total_doc_requested'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Inspection Requested</td>
                            <td><?=$result['total_inspect_requested'] ?></td>
                            <td><?= round($result['per_total_inspect_requested'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Approved</td>
                            <td><?=$result['total_approved'] ?></td>
                            <td><?= round($result['per_total_approved'], 2) ?> %</td>
                        </tr>
                        <tr>
                            <td class="text-primary">Rejected</td>
                            <td><?=$result['total_rejected'] ?></td>
                            <td><?= round($result['per_total_rejected'], 2) ?> %</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
    }
}
?>
</div>