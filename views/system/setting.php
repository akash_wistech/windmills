<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\SettingsAssets;
SettingsAssets::register($this);

/* @var $this yii\web\View */

$countryListArr=Yii::$app->appHelperFunctions->countryListArr;
$predefinedListArr=Yii::$app->appHelperFunctions->predefinedListArr;

$this->title = 'Settings';
?>
<div class="system-setting">
	<section class="update-profile-form card card-outline card-primary">
		<header class="card-header">
			<h2 class="card-title"><?= $this->title?></h2>
		</header>
		<?php $form = ActiveForm::begin(); ?>
		<div class="card-body">
			<div class="row">
                <?php

                $percentage= array();
                for($i=1;$i<=100;$i++){
                    $percentage[$i]=$i;
                }
                ?>

                <div class="col-sm-4">
                    <?= $form->field($model, 'sold_percentage_calculation')
                        ->dropDownList($percentage,['prompt'=>'Select'])->label('Sold Percentage Calculation (%)')
                    ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'comparables_percentage_calculation')
                        ->dropDownList($percentage,['prompt'=>'Select'])->label('Comparables Percentage Calculation (%)') ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'previous_percentage_calculation')
                        ->dropDownList($percentage,['prompt'=>'Select'])->label('Previous Percentage Calculation (%)') ?>
                </div>

			</div>
		</div>
		<div class="card-footer">
			<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a('Cancel', ['site/index'], ['class' => 'btn btn-default']) ?>
		</div>
		<?php ActiveForm::end(); ?>
	</section>
</div>
