<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\SettingsAssets;
SettingsAssets::register($this);

/* @var $this yii\web\View */

$countryListArr=Yii::$app->appHelperFunctions->countryListArr;
$predefinedListArr=Yii::$app->appHelperFunctions->predefinedListArr;

$this->title = 'Settings';
?>
<div class="system-setting">
	<section class="update-profile-form card card-outline card-primary">
		<header class="card-header">
			<h2 class="">Valuation Settings</h2>
		</header>
		<?php $form = ActiveForm::begin(); ?>
		<div class="card-body">

            <section class="update-profile-form card card-outline card-primary">
                    <header class="card-header">
                        <h2 class="card-title">If ALL (Sold Transactions, Market Comparables, Previous Transactions) Are Available </h2>
                    </header>
                    <div class="card-body">
                        <div class="row">
                            <?php

                            $percentage= array();
                            for($i=1;$i<=100;$i++){
                                $percentage[$i]=$i;
                            }
                            ?>

                            <div class="col-sm-4">
                                <?= $form->field($model, 'sold_percentage_calculation')
                                    ->dropDownList($percentage)->label('Sold Percentage Calculation (%)')
                                ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($model, 'comparables_percentage_calculation')
                                    ->dropDownList($percentage)->label('Comparables Percentage Calculation (%)') ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($model, 'previous_percentage_calculation')
                                    ->dropDownList($percentage)->label('Previous Percentage Calculation (%)') ?>
                            </div>

                        </div>
                    </div>
                </section>

            <section class="update-profile-form card card-outline card-primary">
                <header class="card-header">
                    <h2 class="card-title">If Only (Sold Transactions, Market Comparables) Are Available </h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <?php

                        $percentage= array();
                        for($i=1;$i<=100;$i++){
                            $percentage[$i]=$i;
                        }
                        ?>

                        <div class="col-sm-4">
                            <?= $form->field($model, 'sold_percentage_calculation_2')
                                ->dropDownList($percentage)->label('Sold Percentage Calculation (%)')
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'comparables_percentage_calculation_2')
                                ->dropDownList($percentage)->label('Comparables Percentage Calculation (%)') ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'previous_percentage_calculation_2')
                                    ->dropDownList(array(0),['disabled' => true])->label('Previous Percentage Calculation (%)') ?>
                        </div>

                    </div>
                </div>
            </section>

            <section class="update-profile-form card card-outline card-primary">
                <header class="card-header">
                    <h2 class="card-title">If Only (Sold Transactions, Previous Transactions) Are Available </h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <?php

                        $percentage= array();
                        for($i=1;$i<=100;$i++){
                            $percentage[$i]=$i;
                        }
                        ?>

                        <div class="col-sm-4">
                            <?= $form->field($model, 'sold_percentage_calculation_3')
                                ->dropDownList($percentage)->label('Sold Percentage Calculation (%)')
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'comparables_percentage_calculation_3')
                                ->dropDownList(array(0),['disabled' => true])->label('Comparables Percentage Calculation (%)') ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'previous_percentage_calculation_3')
                                ->dropDownList($percentage)->label('Previous Percentage Calculation (%)') ?>
                        </div>

                    </div>
                </div>
            </section>

            <section class="update-profile-form card card-outline card-primary">
                <header class="card-header">
                    <h2 class="card-title">If Only (Market Comparables, Previous Transactions) Are Available </h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <?php

                        $percentage= array();
                        for($i=1;$i<=100;$i++){
                            $percentage[$i]=$i;
                        }
                        ?>


                        <div class="col-sm-4">
                            <?= $form->field($model, 'sold_percentage_calculation_4')
                                ->dropDownList(array(0),['disabled' => true])->label('Sold Percentage Calculation (%)') ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'comparables_percentage_calculation_4')
                                ->dropDownList($percentage)->label('Comparables Percentage Calculation (%)') ?>
                        </div>

                        <div class="col-sm-4">
                            <?= $form->field($model, 'previous_percentage_calculation_4')
                                ->dropDownList($percentage)->label('Previous Percentage Calculation (%)') ?>
                        </div>

                    </div>
                </div>
            </section>
            <section class="update-profile-form card card-outline card-primary">
                <header class="card-header">
                    <h2 class="card-title">Ajman Rera Follow up Email Frequency (Days)</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <?php

                        $percentage= array();
                        for($i=1;$i<=30;$i++){
                            $percentage[$i]=$i;
                        }
                        ?>

                        <div class="col-sm-4">
                            <?= $form->field($model, 'ajman_rera_followup_email_frequency')
                                ->dropDownList($percentage)->label('Ajman Rera Followup Email Frequency') ?>
                        </div>

                    </div>
                </div>
            </section>



        </div>
		<div class="card-footer">
			<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a('Cancel', ['site/index'], ['class' => 'btn btn-default']) ?>
		</div>
		<?php ActiveForm::end(); ?>
	</section>
</div>
