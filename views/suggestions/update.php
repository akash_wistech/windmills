<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Developments */

$this->title = Yii::t('app', 'Suggestions');
$cardTitle = Yii::t('app', 'Update Suggestion:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="developments-update">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
