<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Developments */

$this->title = Yii::t('app', $title);
$cardTitle = Yii::t('app', 'View Policies: '.$title, [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>


<div class="developments-view">
    <section class="card card-outline card-primary">

        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                   <?= $model->description ?>
                                </div>
                            </div>

                        </div>
                </div>
    </section>

    <?php
    if($model<>null && $model->id<>null){
        echo Yii::$app->appHelperFunctions->getLastActionHitory([
            'model_id' => $model->id,
            'model_name' =>'app\models\Policies',
        ]);
    }
    ?>
</div>
