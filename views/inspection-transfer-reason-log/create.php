<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InspectionTransferReasonLog */

$this->title = Yii::t('app', 'Inspection Cancel Reasons');
$cardTitle = Yii::t('app','New Inspection Cancel Reasons');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="inspection-transfer-reason-log-create">


    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
