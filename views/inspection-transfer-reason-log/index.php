<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InspectionTransferReasonLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inspection Transfer Reason Logs';
$this->params['breadcrumbs'][] = $this->title;
$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
    $actionBtns.='{view}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){

    $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
    $actionBtns.='{status}';
}
?>



<div class="inspection-transfer-reason-log-index">


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'reference_number', 'label' => Yii::t('app', 'Reference')],
            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->valuation->property->title;
                },
                'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'city',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->emiratedListArr[$model->city];

                },
                'filter' => Yii::$app->appHelperFunctions->emiratedListArr
            ],
            ['attribute' => 'inspection_date',
                'label' => Yii::t('app', 'Inspection Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->inspection_date));
                },
            ],
            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->valuation->service_officer_name) && ($model->valuation->service_officer_name <> null))? ($model->valuation->approver->firstname.' '.$model->valuation->approver->lastname): '';
                },
                'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
                    'firstname' => SORT_ASC,
                ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];})
            ],
            ['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
                return Yii::$app->helperFunctions->arrStatusIconList[$model['status']];
            },'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>Yii::$app->helperFunctions->arrFilterStatusList],



            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>

