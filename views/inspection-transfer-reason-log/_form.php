<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\CommunitiesAssets;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\InspectionTransferReasonLog */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="communities-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>
    <fieldset disabled="disabled">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'reference_number')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'service_officer_name')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->staffMemberListArr,
                    'options' => ['placeholder' => 'Select a Officer ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'disabled' => true
                    ],
                ]);
                ?>

            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select a Property Type ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'disabled' => true
                    ],
                ]);
                ?>

            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'reason_id')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\app\models\InspectionTransferReasons::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'pluginOptions' => [
                        'allowClear' => true,
                        'disabled' => true
                    ],
                ]);
                ?>

            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'inspection_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-instruction_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-instruction_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4" id="inspection_officer_id">
                <?php
                echo $form->field($model, 'inspection_officer')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->staffMemberListArr,
                    'options' => ['placeholder' => 'Select a Person ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'disabled' => true
                    ],
                ]);
                ?>

            </div>

        </div>
    </fieldset>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Verification') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'status')->widget(Select2::classname(), [
                                'data' => array( '2' => 'Unverified','1' => 'Verified'),
                            ]);
                            ?>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
