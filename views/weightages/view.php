<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Weightages */


$this->title = Yii::t('app', 'Weightages');
$cardTitle = Yii::t('app', 'View Weightage:  {nameAttribute}', [
    'nameAttribute' => $model->property->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>


<div class="weightages-view">
    <section class="card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title"><?= $model->property->title ?></h2>
            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) { ?>
                <div class="card-tools">
                    <a href="<?= Url::to(['update', 'id' => $model['id']]) ?>" class="btn btn-tool">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            <?php } ?>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Property Type:') . '</strong> ' . $model->property->title; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Emirates:') . '</strong> ' . Yii::$app->appHelperFunctions->emiratedListArr[$model->emirates]; ?>
                                </div>

                            </div>


                            <section class="card card-outline card-info mt-5">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Star Rating Info') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Location (%):') . '</strong> ' . $model->location; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Age (%):') . '</strong> ' . $model->age; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Exposure (%):') . '</strong> ' . $model->property_exposure; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Placement (%):') . '</strong> ' . $model->property_placement; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Finishing Status (%):') . '</strong> ' . $model->finishing_status; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Bedrooom (%):') . '</strong> ' . $model->bedrooom; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'View (%):') . '</strong> ' . $model->view; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Quality (%):') . '</strong> ' . $model->quality; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Floor (%):') . '</strong> ' . $model->floor; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Land Size (%):') . '</strong> ' . $model->land_size; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'BUA (%):') . '</strong> ' . $model->bua; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Balcony Size (%):') . '</strong> ' . $model->balcony_size; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Furnished (%):') . '</strong> ' . $model->furnished; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Upgrades (%):') . '</strong> ' . $model->upgrades; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Parking (%):') . '</strong> ' . $model->parking; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Pool (%):') . '</strong> ' . $model->pool; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Landscape (%):') . '</strong> ' . $model->landscape; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'White Goods (%):') . '</strong> ' . $model->white_goods; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Utilities Connected (%):') . '</strong> ' . $model->utilities_connected; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Tenure (%):') . '</strong> ' . $model->tenure; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Number Of Levels (%):') . '</strong> ' . $model->number_of_levels; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Visibility (%):') . '</strong> ' . $model->property_visibility; ?>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="card card-outline card-warning">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Average Sold Transactions Attributes') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Location:') . '</strong> ' . $model->location_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Tenure:') . '</strong> ' . $model->tenure_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'View:') . '</strong> ' . $model->view_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Finished Status:') . '</strong> ' . $model->finished_status_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Condition:') . '</strong> ' . $model->property_condition_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Exposure:') . '</strong> ' . $model->property_exposure_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Property Placement:') . '</strong> ' . $model->property_placement_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Upgrades:') . '</strong> ' . $model->upgrades_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'No.of Levels:') . '</strong> ' . $model->number_of_levels_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Parking Space:') . '</strong> ' . $model->parking_space_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Pool:') . '</strong> ' . $model->pool_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Landscaping:') . '</strong> ' . $model->landscaping_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'White Goods:') . '</strong> ' . $model->white_goods_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Utilities Connected:') . '</strong> ' . $model->utilities_connected_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Land Size') . '</strong> ' . $model->land_size_avg; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Balcony Size:') . '</strong> ' . $model->balcony_size_avg; ?>
                                        </div>





                                    </div>
                                </div>
                            </section>
                        </div>
                </div>
    </section>
</div>
