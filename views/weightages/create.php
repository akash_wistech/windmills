<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Weightages */


$this->title = Yii::t('app', 'Weightages');
$cardTitle = Yii::t('app', 'New Weightage');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle
?>
<div class="weightages-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
