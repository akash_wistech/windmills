<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Weightages */

$this->title = 'Update Weightages: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Weightages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$this->title = Yii::t('app', 'Weightages');
$cardTitle = Yii::t('app', 'Update Weightage:  {nameAttribute}', [
    'nameAttribute' => $model->property->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="weightages-update">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
