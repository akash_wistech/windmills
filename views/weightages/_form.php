<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\WeightagesAssets;

WeightagesAssets::register($this);
use  app\components\widgets\StatusVerified;

/* @var $this yii\web\View */
/* @var $model app\models\Weightages */
/* @var $form yii\widgets\ActiveForm */
?>


<section class="weightages-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle ?></h2>
    </header>
    <div class="card-body">

        <div class="row">
            <div class="col-sm-6">
                <?php
                echo $form->field($model, 'property_type')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select a Type ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>
            <div class="col-sm-6">
                <?php
                echo $form->field($model, 'emirates')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->emiratedListArr,
                    'options' => ['placeholder' => 'Select an Emirate ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="clearfix"></div>
        </div>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'General Weightages') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'location')->textInput(['maxlength' => true])->label('Location (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'age')->textInput(['maxlength' => true])->label('Age (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'property_exposure')->textInput(['maxlength' => true])->label('Property Exposure (%)') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'property_placement')->textInput(['maxlength' => true])->label('Property Placement (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'finishing_status')->textInput(['maxlength' => true])->label('Finishing Status (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'bedrooom')->textInput(['maxlength' => true])->label('Bedrooom (%)') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'view')->textInput(['maxlength' => true])->label('View (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'quality')->textInput(['maxlength' => true])->label('Quality (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'floor')->textInput(['maxlength' => true])->label('Floor (%)') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'land_size')->textInput(['maxlength' => true])->label('Land Size (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'bua')->textInput(['maxlength' => true])->label('BUA (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'balcony_size')->textInput(['maxlength' => true])->label('Balcony Size (%)') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'furnished')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'upgrades')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'parking')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'pool')->textInput(['maxlength' => true])->label('Pool (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'landscape')->textInput(['maxlength' => true])->label('Landscape (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'white_goods')->textInput(['maxlength' => true])->label('White Goods (%)') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'utilities_connected')->textInput(['maxlength' => true])->label('Utilities Connected (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'tenure')->textInput(['maxlength' => true])->label('Tenure (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'number_of_levels')->textInput(['maxlength' => true])->label('Number Of Levels (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'property_visibility')->textInput(['maxlength' => true])->label('Property Visibility (%)') ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Market Change Weightages') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_1_month')->textInput(['maxlength' => true])->label('Less Than 1 Month (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_2_month')->textInput(['maxlength' => true])->label('Less Than 2 Month (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_3_month')->textInput(['maxlength' => true])->label('Less Than 3 Month (%)') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_4_month')->textInput(['maxlength' => true])->label('Less Than 4 Month (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_5_month')->textInput(['maxlength' => true])->label('Less Than 5 Month (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_6_month')->textInput(['maxlength' => true])->label('Less Than 6 Month (%)') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_7_month')->textInput(['maxlength' => true])->label('Less Than 7 Month (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_8_month')->textInput(['maxlength' => true])->label('Less Than 8 Month (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_9_month')->textInput(['maxlength' => true])->label('Less Than 9 Month (%)') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_10_month')->textInput(['maxlength' => true])->label('Less Than 10 Month (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_11_month')->textInput(['maxlength' => true])->label('Less Than 11 Month (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_12_month')->textInput(['maxlength' => true])->label('Less Than 12 Month (%)') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_2_year')->textInput(['maxlength' => true])->label('Less Than 2 Year (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_3_year')->textInput(['maxlength' => true])->label('Less Than 3 Year (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_4_year')->textInput(['maxlength' => true])->label('Less Than 4 Year (%)') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_5_year')->textInput(['maxlength' => true])->label('Less Than 5 Year (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_6_year')->textInput(['maxlength' => true])->label('Less Than 6 Year (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_7_year')->textInput(['maxlength' => true])->label('Less Than 7 Year (%)') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_8_year')->textInput(['maxlength' => true])->label('Less Than 8 Year (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_9_year')->textInput(['maxlength' => true])->label('Less Than 9 Year (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'less_than_10_year')->textInput(['maxlength' => true])->label('Less Than 10 Year (%)') ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Average Sold Transactions Attributes') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'location_avg')->textInput(['maxlength' => true])->label('Location'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'tenure_avg')->textInput(['maxlength' => true])->label('Tenure'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'view_avg')->textInput(['maxlength' => true])->label('View'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'finished_status_avg')->textInput(['maxlength' => true])->label('Finished Status'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'property_condition_avg')->textInput(['maxlength' => true])->label('Property Condition'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'property_exposure_avg')->textInput(['maxlength' => true])->label('Property Exposure'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'property_placement_avg')->textInput(['maxlength' => true])->label('Property Placement'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'upgrades_avg')->textInput(['maxlength' => true])->label('Upgrades'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'number_of_levels_avg')->textInput(['maxlength' => true])->label('No.of Levels'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'parking_space_avg')->textInput(['maxlength' => true])->label('Parking Space'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'pool_avg')->textInput(['maxlength' => true])->label('Pool'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'landscaping_avg')->textInput(['maxlength' => true])->label('Landscaping'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'white_goods_avg')->textInput(['maxlength' => true])->label('White Goods'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'utilities_connected_avg')->textInput(['maxlength' => true])->label('Utilities Connected'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'developer_margin_avg')->textInput(['maxlength' => true])->label('Developer Margin'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'land_size_avg')->textInput(['maxlength' => true])->label('Land Size'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'balcony_size_avg')->textInput(['maxlength' => true])->label('Balcony Size'); ?>
                    </div>

                </div>
            </div>
        </section>
        <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?> 
        <?php 
            if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                ]);
            }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>


