<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WeightagesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="weightages-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'property_type') ?>

    <?= $form->field($model, 'emirates') ?>

    <?= $form->field($model, 'location') ?>

    <?= $form->field($model, 'age') ?>

    <?php // echo $form->field($model, 'property_exposure') ?>

    <?php // echo $form->field($model, 'property_placement') ?>

    <?php // echo $form->field($model, 'finishing_status') ?>

    <?php // echo $form->field($model, 'bedrooom') ?>

    <?php // echo $form->field($model, 'view') ?>

    <?php // echo $form->field($model, 'quality') ?>

    <?php // echo $form->field($model, 'floor') ?>

    <?php // echo $form->field($model, 'land_size') ?>

    <?php // echo $form->field($model, 'bua') ?>

    <?php // echo $form->field($model, 'balcony_size') ?>

    <?php // echo $form->field($model, 'furnished') ?>

    <?php // echo $form->field($model, 'upgrades') ?>

    <?php // echo $form->field($model, 'parking') ?>

    <?php // echo $form->field($model, 'pool') ?>

    <?php // echo $form->field($model, 'landscape') ?>

    <?php // echo $form->field($model, 'white_goods') ?>

    <?php // echo $form->field($model, 'utilities_connected') ?>

    <?php // echo $form->field($model, 'tenure') ?>

    <?php // echo $form->field($model, 'number_of_levels') ?>

    <?php // echo $form->field($model, 'property_visibility') ?>

    <?php // echo $form->field($model, 'less_than_1_month') ?>

    <?php // echo $form->field($model, 'less_than_2_month') ?>

    <?php // echo $form->field($model, 'less_than_3_month') ?>

    <?php // echo $form->field($model, 'less_than_4_month') ?>

    <?php // echo $form->field($model, 'less_than_5_month') ?>

    <?php // echo $form->field($model, 'less_than_6_month') ?>

    <?php // echo $form->field($model, 'less_than_7_month') ?>

    <?php // echo $form->field($model, 'less_than_8_month') ?>

    <?php // echo $form->field($model, 'less_than_9_month') ?>

    <?php // echo $form->field($model, 'less_than_10_month') ?>

    <?php // echo $form->field($model, 'less_than_11_month') ?>

    <?php // echo $form->field($model, 'less_than_12_month') ?>

    <?php // echo $form->field($model, 'less_than_2_year') ?>

    <?php // echo $form->field($model, 'less_than_3_year') ?>

    <?php // echo $form->field($model, 'less_than_4_year') ?>

    <?php // echo $form->field($model, 'less_than_5_year') ?>

    <?php // echo $form->field($model, 'less_than_6_year') ?>

    <?php // echo $form->field($model, 'less_than_7_year') ?>

    <?php // echo $form->field($model, 'less_than_8_year') ?>

    <?php // echo $form->field($model, 'less_than_9_year') ?>

    <?php // echo $form->field($model, 'less_than_10_year') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'trashed_at') ?>

    <?php // echo $form->field($model, 'trashed') ?>

    <?php // echo $form->field($model, 'trashed_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
