<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IpList */

$this->title = 'Update IP List: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ip Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$iPaddress= app\models\IpAddresses::find()->select(['ip'])->where(['list_id'=>$model->id])->asArray()->all();
?>
<div class="ip-list-update">


    <?= $this->render('_form', [
        'model' => $model,
        'iPaddress'=>$iPaddress,
    ]) ?>

</div>
