<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IpList */
/* @var $form yii\widgets\ActiveForm */


$this->registerJs('

    var  Html="<div class=\"row m-2 clone-input\">";
    Html+="<div class=\"col-4\">";
    Html+="<input type=\"text\" name=\"IpList[ips][]\" id=\"iplist-ips[]\" class=\"form-control my-1\">";
    Html+="</div>";
    Html+="<div class=\"col-2\">";
    Html+="<p class=\" btn btn-danger px-3 delete-btn\" style=\"margin-top:5.7px; padding-top:4px; padding-bottom:4px;\">-";
    Html+="</p>";
    Html+="</div></div>";

    $("body").on("click",".clone-btn" ,function(){
      $(".copy-here").append(Html);
    });

    $("body").on("click",".delete-btn" ,function(){
       $(this).parents(".clone-input").remove();
    
        });');



?>


<div class="ip-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="copy-here">

<div class="row m-2 clone-input">
    <div class="col-4">
    <?= $form->field($model, 'list_name')->textInput(['maxlength' => true]) ?>
</div></div>



<?php

if($iPaddress!=null) {
    $n=0;?>
    <div class=" m-3">
    <label class="mx-1">IP Address</label>

    <?php
    foreach ($iPaddress as $key => $value) {


         $model->ips[$n]=$value['ip'];

        // echo "<pre>";
        // print_r();
        // echo "</pre>";
        // die();

     ?>
        <div class="row clone-input">
        <div class="col-4 my-2"> 
        <?= $form->field($model, 'ips['.$n.']')->textInput(['maxlength' => true])->label(false) ?>
         </div>
         <?php if ($n==0){ ?>
            <div class="col-2">
            <p class=" btn btn-success px-3 clone-btn" style="margin-top:9px; padding-top:4px; padding-bottom:4px;">+</p>
            </div>
         <?php }else{ ?>
         <div class="col-2">
        <p class=" btn btn-danger px-3 delete-btn" style="margin-top:9px; padding-top:4px; padding-bottom:4px;">-</p>
         </div>
     <?php }?>
         </div>
        <?php $n++; }  ?>

   </div>

   <?php }else{ ?>

<div class="row m-2 clone-input">
    <div class="col-4">
    <?= $form->field($model, 'ips[]')->textInput(['maxlength' => true,'id'=>'iplist-ips[]']) ?>
    </div>
    <div class="col-2">
    <p class=" btn btn-success px-3 clone-btn" style="margin-top:30px; padding-top:4px; padding-bottom:4px;">+
    </p>
    </div></div>



  <?php } ?>



    
</div>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success ml-3']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
