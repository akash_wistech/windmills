<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ListingsTransactions */

$this->title = Yii::t('app', 'Listings Transactions');
$cardTitle = Yii::t('app', 'Update Listing Transaction:  {nameAttribute}', [
    'nameAttribute' => $model->building->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="listings-transactions-update">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
