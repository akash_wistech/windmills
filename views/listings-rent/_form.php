<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\ListingsTransactions */
/* @var $form yii\widgets\ActiveForm */
$current_date = date('Y/m/d');$this->registerJs('


$("#listingstransactions-listing_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD",

});
');

?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
</style>

<section class="listings-transactions-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle ?></h2>
    </header>
    <div class="card-body">


        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'listings_reference')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'source')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->propertySourceListArr,
                    'options' => ['placeholder' => 'Select a Source ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'listing_website_link')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'listing_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-listing_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-listing_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true]) ?>
            </div>

            <?php if($model->listing_website_link_image <> null){ ?>
                <div class="col-sm-4">
                    <?= '<strong>' . Yii::t('app', ' Listing Website Link Image:') . '</strong> ' ?>
                    <a href="<?= $model->listing_website_link_image; ?>"
                       target="_blank">
                        <span class="glyphicon glyphicon-eye-open">View Image</span>
                    </a>
                </div>
            <?php } ?>

        </div>


        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Building Information') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'building_info')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select a Building ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>


                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'auto_list_type')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->incomeRentsPropertyTypesArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                </div>




                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_placement')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertyPlacementListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_visibility')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertyVisibilityListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_exposure')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertyExposureListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_condition')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertyConditionListArrMaster,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'development_type')->widget(Select2::classname(), [
                            'data' => array('Standard' => 'Standard', 'Non-Standard' => 'Non-Standard'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'utilities_connected')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>

                </div>



                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'finished_status')->widget(Select2::classname(), [
                            'data' => array('Shell & Core' => 'Shell & Core', 'Fitted' => 'Fitted'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'pool')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'gym')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'play_area')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php

                        $model->other_facilities = explode(',', ($model->other_facilities <> null) ? $model->other_facilities : "");
                        echo $form->field($model, 'other_facilities')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\OtherFacilities::find()->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select a Facilities ...'],
                            'pluginOptions' => [
                                'placeholder' => 'Select a Facilities',
                                'multiple' => true,
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'completion_status')->textInput(['maxlength' => true])?>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'white_goods')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'furnished')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No', 'Semi-Furnished' => 'Semi-Furnished'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'landscaping')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No', 'Semi-Landscape' => 'Semi-Landscape'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>




                </div>




        </section>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Property Other Information') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'no_of_bedrooms')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'upgrades')->widget(Select2::classname(), [
                            'data' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'full_building_floors')->textInput(['maxlength' => true]) ?>
                    </div>


                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <?= $form->field($model, 'parking_space')->textInput(['maxlength' => true]) ?>
                    </div>


                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'listing_property_type')->widget(Select2::classname(), [
                            //'data' => Yii::$app->appHelperFunctions->listingsPropertyTypeListArr,
                            'data' => ArrayHelper::map(\app\models\ListingSubTypes::find()->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'title', 'title'),
                            'options' => ['placeholder' => 'Select a Type ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'estimated_age')->textInput(['maxlength' => true]) ?>
                    </div>

                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <?= $form->field($model, 'floor_number')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'number_of_levels')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->listingLevelsListArr,
                            'options' => ['placeholder' => 'Select a Level ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'land_size')->textInput(['maxlength' => true]) ?>
                    </div>


                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'balcony_size')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Location Attributes') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_highway_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_school_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_mall_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_sea_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_park_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>


                </div>

            </div>

        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'View Attributes') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_community')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewCommunityAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_pool')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_burj')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_sea')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_marina')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_mountains')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_lake')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_golf_course')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_park')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'view_special')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                        ]);
                        ?>
                    </div>
                </div>

            </div>

        </section>



        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Price Information') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'listings_rent')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'price_per_sqt')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'final_price')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'gross_yield')->textInput(['maxlength' => true,'readonly'=>true])->label('Gross Yield(%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'developer_margin')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>


                </div>

            </div>

        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Agent Information') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'agent_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'agent_phone_no')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'agent_company')->textInput(['maxlength' => true]) ?>
                    </div>


                </div>

            </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Listing Verification') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('verify')) { ?>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'status')->widget(Select2::classname(), [
                            'data' => array( '2' => 'Unverified','1' => 'Verified'),
                        ]);
                        ?>
                    </div>
                        <?php } ?>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'changed')->widget(Select2::classname(), [
                            'data' => array( '0' => 'No','1' => 'Yes'),
                        ])->label('Changed?');
                        ?>
                    </div>
                    </div>
                    <div id="change" class="col-sm-12">

                        <?php if($model->changed == 1){ ?>

                    <div class="col-sm-12">
                        <?= $form->field($model, 'change_from')->textarea(['maxlength' => true, 'rows' => 2])->label('Change From') ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $form->field($model, 'change_to')->textarea(['maxlength' => true, 'rows' => 2])->label('Change To') ?>
                    </div>

                    <div class="col-sm-12">
                        <?= $form->field($model, 'change_reason')->textarea(['maxlength' => true, 'rows' => 2])->label('Change Reason') ?>
                    </div>
                        <?php } ?>
                    </div>



                </div>
            </div>
        </section>




    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
<script>





</script>
<?php
$this->registerJs('
 var html = \'\';

    html+=\'<div class="col-sm-12">\';
    html+=\'<div class="form-group field-listingstransactions-change_from required has-success">\';
    html+=\'<label class="control-label" for="listingstransactions-change_from">Change From</label>\';
    html+=\'<textarea id="listingstransactions-change_from" class="form-control" name="ListingsTransactions[change_from]" rows="2" aria-required="true" aria-invalid="false"> </textarea>\';

    html+=\'<div class="help-block"></div>\';
    html+=\'</div>                    </div>\';
    html+=\'<div class="col-sm-12">\';
    html+=\'<div class="form-group field-listingstransactions-change_to required">\';
    html+=\'<label class="control-label" for="listingstransactions-change_to">Change To</label>\';
    html+=\'<textarea id="listingstransactions-change_to" class="form-control" name="ListingsTransactions[change_to]" rows="2" aria-required="true"> </textarea>\';

    html+=\'<div class="help-block"></div>\';
    html+=\'</div>                    </div>\';

    html+=\'<div class="col-sm-12">\';
    html+=\'<div class="form-group field-listingstransactions-change_reason required">\';
    html+=\'<label class="control-label" for="listingstransactions-change_reason">Change Reason</label>\';
    html+=\'<textarea id="listingstransactions-change_reason" class="form-control" name="ListingsTransactions[change_reason]" rows="2" aria-required="true"> </textarea>\';

    html+=\'<div class="help-block"></div>\';
    html+=\'</div>                    </div>\';

    $(\'#listingstransactions-changed\').on(\'change\',function () {
        var change = $(this).val();
        if(change == "1"){
            $(\'#change\').html(html);
        }else{
         $(\'#change\').html("");
        }
    });
');
?>
