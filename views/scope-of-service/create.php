<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ScopeOfService */

$this->title = 'Create Scope Of Service';
$this->params['breadcrumbs'][] = ['label' => 'Scope Of Service', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sales-and-marketing-purpose-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
