<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use kartik\select2\Select2;



$this->title = 'Scope Of Service';
$this->params['breadcrumbs'][] = $this->title;
$cardTitle = Yii::t('app', 'List');


if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('view')) {
    $actionBtns .= '{view}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('delete')) {
    $actionBtns .= '{delete}';
}
$actionBtns .= '{get-history}';
$createBtn = false;



?>



<section class="sales-and-marketing-purpose-form card card-outline card-success">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card-body">

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder' => 'Enter the title...']) ?>
            </div>
            <div class="col-6">
                <?php
                echo $form->field($model, 'status_verified')->widget(Select2::classname(), [
                    'data' => array('1' => 'Verified', '2' => 'Unverified'),
                    'options' => ['placeholder' => 'Select'],
                ]);
                ?>
            </div>
        </div>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i> Save'), ['class' => 'btn btn-success sav-btn1']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if($model<>null && $model->id<>null){
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => get_class($model),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>

<div class="sales-and-marketing-purpose-index">


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],

            'title',


            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'status_verified',
                'label' => Yii::t('app', 'Verification'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->statusVerifyLabel[$model->status_verified];
                },
                'filter' => Yii::$app->appHelperFunctions->statusVerifyArr
            ],



            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
                'contentOptions' => ['class' => 'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              ' . $actionBtns . '
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Edit'), Url::toRoute(['index', 'id' => $model['id']]), [
                            'title' => Yii::t('app', 'Edit'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                    'get-history' => function ($url, $model) {
                        // Yii::$app->session->set('model_name', Yii::$app->appHelperFunctions->getModelName());
                        return Html::a('<i class="fa fa-history"></i> ' . Yii::t('app', 'History'), 'javascript:;', [
                            'title' => Yii::t('app', 'History'),
                            'class' => 'dropdown-item text-1',
                            'target' => '_blank',
                            'onclick' => "window.open('" . $url . "', '_blank')",
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>