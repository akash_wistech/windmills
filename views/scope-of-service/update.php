<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ScopeOfService */

$this->title = 'Update Scope Of Service: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Scope Of Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sales-and-marketing-purpose-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
