<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use yii\helpers\ArrayHelper;


?>

<section class="contact-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <header class="card-header">
        <h2 class="card-title"><?php if(isset($page_title)) {echo $page_title;}else{echo "Add Category";} ?></h2>
    </header>
    <div class="card-body">

        <div class="row"> 
            <div class="col-6">
                <label for="category_name">Category Name</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input type="text" class="form-control" name="category_name" id="category_name" value="<?php if(isset($model->category_name)) echo $model->category_name; ?>" >
                    </div>
                </div>
            </div>

            <div class="col-6">
                <label for="status">Status</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='status' id="status" class="form-control">
                            <option value="active"
                                <?php if(isset($model->status) && ($model->status == 'active')){ echo 'selected';} ?>>
                                Active</option>
                            <option value="inactive"
                                <?php if(isset($model->status) && ($model->status == 'inactive')){ echo 'selected';} ?>>
                                Inactive</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if($model<>null && $model->id<>null){
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => get_class($model),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>

<?php
$this->registerJs('

    
');
