<?php

use yii\helpers\Html;

$this->title = 'Add Category';
$this->params['breadcrumbs'][] = ['label' => 'Category Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
