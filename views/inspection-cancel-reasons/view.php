<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\InspectionCancelReasons */



$this->title = Yii::t('app', 'Inspection Cancel Reason');
$cardTitle = Yii::t('app','View Inspection Cancel Reason:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>
<div class="inspection-cancel-reasons-view">
    <section class="card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title"><?= $model->title?></h2>
            <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){?>
                <div class="card-tools">
                    <a href="<?= Url::to(['update','id'=>$model['id']])?>" class="btn btn-tool">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            <?php }?>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <section class="card card-outline card-info">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= '<strong>'.Yii::t('app','Title:').'</strong> '.$model->title;?>
                                </div>
                            </div>

                        </div>
                    </section>
                </div>

            </div>
        </div>
    </section>
</div>

