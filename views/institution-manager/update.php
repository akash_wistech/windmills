<?php

use yii\helpers\Html;

$this->title = 'Update Institution: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Institutions Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="institution-update">

    <?= $this->render('_form', [
        'model' => $model,
        'page_title' => $this->title,
    ]) ?>

</div>
