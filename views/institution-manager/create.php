<?php

use yii\helpers\Html;

$this->title = 'Create Institution';
$this->params['breadcrumbs'][] = ['label' => 'Institutions Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institutions-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
