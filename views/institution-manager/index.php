<?php

use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\User;
use app\models\Country;

use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GeneralAsumptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Institution Manager";
$this->params['breadcrumbs'][] = $this->title;

$cardTitle = Yii::t('app', 'Institutions List');

// $createBtn=true;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    $createBtn=true;
}
// if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
//     $actionBtns.='{view}';
// }
// $actionBtns.='{update}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
// $actionBtns.='{delete}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
    $actionBtns.='{delete}';
}
// $actionBtns.='{get-history}';


?>
<div class="institution-index">

    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        // 'createBtn' => $createBtn,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:0px;']],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:50%'],
                'attribute' => 'institute_name',
                'label' => Yii::t('app', 'Institution name'),
                'value' => function ($model) {
                    return ucfirst($model->institute_name);
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:25%'],
                'attribute' => 'country_id',
                'label' => Yii::t('app', 'Country'),
                'value' => function ($model) {
                    $get_name = Country::find()->where(['id' => $model->country_id])->one();
                    return $get_name->title;
                },
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:25%'],
                'attribute' => 'status',
                'label' => Yii::t('app', 'Status'),
                'value' => function ($model) {
                    return ucfirst($model->status);
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:0px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
                    <div class="dropdown-menu" role="menu">
              '.$actionBtns.'
                    </div>
                </div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },

                    'get-history' => function ($url, $model) {
                        // Yii::$app->session->set('model_name', Yii::$app->appHelperFunctions->getModelName());
                        return Html::a('<i class="fa fa-history"></i> '.Yii::t('app', 'History'), $url, [
                            'title' => Yii::t('app', 'History'),
                            'class'=>'dropdown-item text-1',
                            'target' => '_blank',
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>

</div>
