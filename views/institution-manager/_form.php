<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use yii\helpers\ArrayHelper;

$countries = ArrayHelper::map(\app\models\Country::find()->where(['status' => 1])->all(), 'id', 'title');

?>

<section class="contact-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?php if(isset($page_title)) {echo $page_title;}else{echo "Create Institution";} ?></h2>
    </header>
    <div class="card-body">

        <div class="row"> 

            <div class="col-4">
                <label for="institute_name"> Name of Institution </label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <input class="form-control" type="text" name="institute_name" id="institute_name" maxlength="40" value="<?php if(isset($model->institute_name)) echo $model->institute_name; ?>">
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="country_id">Country</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name="country_id" id="country_id" class="form-control">
                            <?php
                                foreach($countries as $key => $country){

                                    if($model->country_id == $key)
                                    {
                                        echo "<option value='$model->country_id' selected>$country</option>";
                                    }else{
                                        echo "<option value='$key'> $country</option>";
                                    }

                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <label for="status">Status</label>
                <div class="input-group-md mb-3">
                    <div class="input-group-prepend">
                        <select name='status' id="status" class="form-control">
                            <option value="active"
                                <?php if(isset($model->status) && ($model->status == 'active')){ echo 'selected';} ?>>
                                Active</option>
                            <option value="inactive"
                                <?php if(isset($model->status) && ($model->status == 'inactive')){ echo 'selected';} ?>>
                                Inactive</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if($model<>null && $model->id<>null){
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => get_class($model),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>

<?php
$this->registerJs('

var value = $("#department").val();
if(value == "maxima")
{
  $("#module_div").css("display", "block");
}else{
    $("#module_div").css("display", "none");
}

$("#department").change(function() {
    var val = $(this).val(); 
      if(val == "maxima")
      {
        $("#module_div").css("display", "block");
      }else{
        $("#module_div").val("");
        $("#module_div").css("display", "none");
      }
});

var dropdown1 = document.getElementById("responsibility");
var dropdown2 = document.getElementById("assigned_to");

dropdown1.addEventListener("change", function () {
    dropdown2.value = dropdown1.value;
});


var freq_val = $("#frequency").val();
if(freq_val == "one-time")
{
    document.getElementById("frequency_start").disabled=true;
    document.getElementById("frequency_end").disabled=true;
}else{
    document.getElementById("frequency_start").disabled=false;
    document.getElementById("frequency_end").disabled=false;
}

$("#frequency").change(function() {
    var val = $(this).val(); 
      if(val == "one-time")
      {
        document.getElementById("frequency_start").disabled=true;
        document.getElementById("frequency_end").disabled=true;
      }else{
        document.getElementById("frequency_start").disabled=false;
        document.getElementById("frequency_end").disabled=false;
      }
});
    
');
