<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\AutoLististings */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="auto-lististings-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" >Data Type</label>
                    <input type="text"  class="form-control"  maxlength="255" value="<?= ucfirst($model->data_type) ?>" readonly>

                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-4">
                    <?= $form->field($model, 'search_limit')->textInput(['maxlength' => true,'type'=> 'number'])->label('Records Limit From') ?>
                </div>
            </div>

        </div>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Relax Mode') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'relax_records_limit_from')->textInput(['maxlength' => true,'type'=> 'number'])->label('Records Limit From') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'relax_records_limit_to')->textInput(['maxlength' => true,'type'=> 'number'])->label('Records Limit To') ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'relax_date')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->dateArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Relax Date Limit');
                        ?>

                    </div>
                  <!--  <div class="col-sm-4">
                        <?/*= $form->field($model, 'relax_date')->textInput(['maxlength' => true,'type'=> 'number'])->label('Date Limit(%)') */?>
                    </div>-->
                    <div class="col-sm-4">
                        <?= $form->field($model, 'relax_bua_percentage')->textInput(['maxlength' => true,'type'=> 'number'])->label('BUA(%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'relax_price_percentage')->textInput(['maxlength' => true,'type'=> 'number'])->label('Price/sqt (%) +/-') ?>
                    </div>
                </div>

            </div>

        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Moderate Mode') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'moderate_records_limit_from')->textInput(['maxlength' => true,'type'=> 'number'])->label('Records Limit From') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'moderate_records_limit_to')->textInput(['maxlength' => true,'type'=> 'number'])->label('Records Limit To') ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'moderate_date')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->dateArr,
                            /* 'options' => ['placeholder' => 'Relax Date'],*/
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Moderate Date Limit');
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'moderate_bua_percentage')->textInput(['maxlength' => true,'type'=> 'number'])->label('BUA (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'moderate_price_percentage')->textInput(['maxlength' => true,'type'=> 'number'])->label('Price/sqft (%) +/-') ?>
                    </div>
                </div>

            </div>

        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Strict Mode') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'strict_records_limit_from')->textInput(['maxlength' => true,'type'=> 'number'])->label('Records Limit > X') ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'strict_date')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->dateArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Strict Date Limit');
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'strict_bua_percentage')->textInput(['maxlength' => true,'type'=> 'number'])->label('BUA (%) +/-') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'strict_price_percentage')->textInput(['maxlength' => true,'type'=> 'number'])->label('Price (%) +/-') ?>
                    </div>

                </div>

            </div>

        </section>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
