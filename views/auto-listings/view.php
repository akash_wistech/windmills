<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AutoLististings */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Auto Lististings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="auto-lististings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'relax_records_limit',
            'relax_bua_from',
            'relax_bua_to',
            'relax_price_from',
            'relax_price_to',
            'relax_date',
            'strict_records_limit',
            'strict_bua_from',
            'strict_bua_to',
            'strict_price_from',
            'strict_price_to',
            'strict_date',
            'moderate_records_limit',
            'moderate_bua_from',
            'moderate_bua_to',
            'moderate_price_from',
            'moderate_price_to',
            'moderate_date',
            'mode',
            'status',
            'data_type',
        ],
    ]) ?>

</div>
