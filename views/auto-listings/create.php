<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AutoLististings */


$this->title = Yii::t('app', 'Create Auto Listings');
$cardTitle = Yii::t('app','New Auto Listing');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="developers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>