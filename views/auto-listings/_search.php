<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AutoLististingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auto-lististings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'relax_records_limit') ?>

    <?= $form->field($model, 'relax_bua_from') ?>

    <?= $form->field($model, 'relax_bua_to') ?>

    <?= $form->field($model, 'relax_price_from') ?>

    <?php // echo $form->field($model, 'relax_price_to') ?>

    <?php // echo $form->field($model, 'relax_date') ?>

    <?php // echo $form->field($model, 'strict_records_limit') ?>

    <?php // echo $form->field($model, 'strict_bua_from') ?>

    <?php // echo $form->field($model, 'strict_bua_to') ?>

    <?php // echo $form->field($model, 'strict_price_from') ?>

    <?php // echo $form->field($model, 'strict_price_to') ?>

    <?php // echo $form->field($model, 'strict_date') ?>

    <?php // echo $form->field($model, 'moderate_records_limit') ?>

    <?php // echo $form->field($model, 'moderate_bua_from') ?>

    <?php // echo $form->field($model, 'moderate_bua_to') ?>

    <?php // echo $form->field($model, 'moderate_price_from') ?>

    <?php // echo $form->field($model, 'moderate_price_to') ?>

    <?php // echo $form->field($model, 'moderate_date') ?>

    <?php // echo $form->field($model, 'mode') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'data_type') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
