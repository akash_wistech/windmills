<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AutoLististings */

$this->title = Yii::t('app', 'Update Auto Listings');
$cardTitle = Yii::t('app','Update Auto Listings:  {nameAttribute}', [
    'nameAttribute' => ucwords($model->data_type),
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="developers-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
