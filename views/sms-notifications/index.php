<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SmsNotificationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sms Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-notifications-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sms Notifications', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'sms_body:ntext',
            'type',
            'module',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
