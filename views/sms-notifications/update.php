<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmsNotifications */

$this->title = 'Update Sms Notifications: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sms Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sms-notifications-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
