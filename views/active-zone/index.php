<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

$this->title = Yii::t('app', 'Active Zone');
$cardTitle = Yii::t('app','New');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['active-zone']];
$this->params['breadcrumbs'][] = $cardTitle;

$zonesList=[];
if($model->country_id>0){
  $zonesList=Yii::$app->appHelperFunctions->getCountryZoneListArr($model->country_id);
}

$this->registerJs('
  $("body").on("change", "#acivezone-country_id", function () {
    $.get("'.Url::to(['suggestion/zone-options','country_id'=>'']).'"+$(this).val(),function(data){
      $("select#acivezone-zone_id").html(data);
    });
  });
');

$actionBtns.='{update}';
?>

<section class="active-zone-form card">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card-body">
        <div class="row">
            <div class="col-5">
                <div class="form-group">
                    <?= $form->field($model, 'country_id')->dropDownList(Yii::$app->appHelperFunctions->getCountryListArr(),['prompt'=>Yii::t('app','Select')]) ?>
                </div>
            </div>
            <div class="col-5">
                <div class="form-group">
                    <?= $form->field($model, 'zone_id')->dropDownList($zonesList,['prompt'=>Yii::t('app','Select')]) ?>
                </div>
            </div>
            <div class="col-2">
                    <?= Html::submitButton(Yii::t('app', 'Add'), ['class' => 'btn btn-success', 'style' => 'margin: 26px;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</section>





<div class="active-zone-index">
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],

            ['attribute'=>'country','label'=>Yii::t('app', 'Country')],

            ['attribute'=>'city','label'=>Yii::t('app', 'City')],


            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), Url::toRoute(['index', 'id' => $model['id']]), [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    // 'delete' => function ($url, $model) {
                    //     return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                    //         'title' => Yii::t('app', 'Delete'),
                    //         'class'=>'dropdown-item text-1',
                    //         'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                    //         'data-method'=>"post",
                    //         'data-pjax'=>"0",
                    //     ]);
                    // },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>
</div>
