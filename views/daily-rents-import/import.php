<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SoldTransactionImportForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Import Transactions');
$cardTitle = $this->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rent Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="listings-transactions-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
  <header class="card-header">
      <h2 class="card-title"><?= $cardTitle ?></h2>
  </header>
  <div class="card-body">
    <?= $form->field($model, 'importfile',[
      'template' => '
        {label}
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="fa fa-file-csv"></i>
            </span>
          </div>
          <div class="custom-file">
            {input}
            <label class="custom-file-label" for="customFile">'.Yii::t('app','Upload CSV File').'</label>
          </div>
        </div>
      {error}'
      ])->fileInput(['id'=>'customFile', 'class'=>'custom-file-input', 'data-img'=>'csv-file', 'accept' => '.csv']);?>
  </div>
  <div class="card-footer">
      <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
      <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
