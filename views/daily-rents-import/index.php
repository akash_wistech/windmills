<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LandRentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daily Rents Transactions';
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;
$import = true;

$createBtn = true;


//$actionBtns .= '{view}';



?>
<div class="row">
    <div class="col-sm-12">


<a style="float: right; margin-bottom: 10px; clear: both" href="<?= Url::to(['daily-rents-import/import'])?>" target="_blank" class="btn btn-success">
    Import
</a>
    </div>
</div>
<div class="rent-ad-index">

    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'createBtn' => $createBtn,
        'import' => $import,
        'columns' => [
          //  'id',
            'reidin_ref',
            'transaction_type',
            'transaction_date',
            'community',
            'property',
            'property_type',
            'size_sqf',
            'land_size',
            'bedrooms',
            'amount',
            'sqf',


            ['attribute' => 'new_building_id',
                'label' => Yii::t('app', 'Maxima Building'),
                'value' => function ($model) {
                    if($model->newBuilding <> null){
                        return $model->newBuilding->building->title;
                    }
                },
            ],
            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'status',
                'label' => Yii::t('app', 'Status Verification'),
                'value' => function ($model) {
                    if($model->status == 0){
                        return '<span class="badge grid-badge badge-danger"> Pending</span>';
                    }else if($model->status == 1){
                        return '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> Converted</span>';
                    }else if($model->status == 2){
                        return '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> Duplicate</span>';
                    }else if($model->status == 3){
                        return '<span class="badge grid-badge badge-danger"><i class="fa fa-check"></i> Not Converted</span>';
                    }
                    // return Yii::$app->appHelperFunctions->statusVerifyLabel[$model->status];
                },
                'filter' => array(0=>'Pending', 1 => 'Converted', 2 => 'Duplicate',3=>'Not Found')
            ],


        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>

