<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DailySoldsImport */

$this->title = 'Create Daily Rents Import';
$this->params['breadcrumbs'][] = ['label' => 'Daily Solds Imports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-solds-import-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
