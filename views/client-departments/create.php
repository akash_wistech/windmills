<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientDepartments */

$this->title = 'Create Client Departments';
$cardTitle = Yii::t('app','Create Client Departments');
$this->params['breadcrumbs'][] = ['label' => 'Client Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-departments-create">


    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
