<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use  app\components\widgets\StatusVerified;

/* @var $this yii\web\View */
/* @var $model app\models\ClientDepartments */
/* @var $form yii\widgets\ActiveForm */
?>



<section class="developers-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'group_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\ClientGroups::find()
                        ->where(['status' => 1])
                        ->orderBy(['title' => SORT_ASC,])
                        ->all(), 'id', 'title'),

                    'options' => ['placeholder' => 'Select a Group ...', 'class'=> 'client-cls'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Group Name');
                ?>

            </div>
        </div>
        <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
    </div>



    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if($model<>null && $model->id<>null){
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
