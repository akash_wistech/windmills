<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientDepartments */

$this->title = 'Update Client Departments: ' . $model->title;
$cardTitle = Yii::t('app','Update Client Departments:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => 'Client Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="client-departments-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
