<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ListData */

$this->title = Yii::t('app', 'Create List Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="list-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
