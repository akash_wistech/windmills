<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ListingsTransactionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'List Data');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;
$actionBtns .= '{view}';
$actionBtns .= '{update}';
$actionBtns .= '{delete}';


?>
<style>
.btn-success {
    margin-left: 5px;
}
</style>
<div class="list-data-index">


<?php CustomPjax::begin(['id' => 'grid-container']); ?>
<?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'cardTitle' => $cardTitle,
    'createBtn' => $createBtn,
    'columns' => [
        
        [
            'class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']
        ],

        'listings_reference',
       // 'source',

        
        'listing_website_link',
        'listing_date',
        'building_info',
        
        ['attribute' => 'city_id',
        'label' => Yii::t('app', 'City'),
        'value' => function ($model) {
            return Yii::$app->appHelperFunctions->emiratedListArr[$model['city_id']];
        },
        'filter' => Yii::$app->appHelperFunctions->emiratedListArr
    ],
    ['attribute' => 'property_type',
    'label' => Yii::t('app', 'Property type'),
    'value' => function ($model) {
        return Yii::$app->appHelperFunctions->propertyTypeArr[$model['property_type']];
    },
    'filter' => Yii::$app->appHelperFunctions->propertyTypeArr
],
        
        
        // 'property_category',
        'no_of_bedrooms',
        'built_up_area',
        // 'land_size',
        'listings_price',
        'listings_rent',
        'final_price',
        'list_data_date',

        [
            'format' => 'raw',
            'headerOptions' => ['style' => 'width:12%'],
            'attribute' => 'move_to_listing',
            'label' => Yii::t('app', 'Status Verification'),
            'value' => function ($model) {


                if($model->move_to_listing == 0){
                    return '<span class="badge grid-badge badge-danger"> Pending</span>';
                }
                else if($model->move_to_listing == 1){
                    return '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> Converted</span>';
                }else if($model->move_to_listing == 2){
                    return '<span class="badge grid-badge badge-danger"><i class="fa fa-check"></i> Not Found</span>';
                }
                else if($model->move_to_listing == 3){
                    return '<span class="badge grid-badge badge-danger"><i class="fa fa-check"></i> Duplicate</span>';
                }
                else if($model->move_to_listing == 5){
                    return '<span class="badge grid-badge badge-danger"><i class="fa fa-check"></i> Old</span>';
                }
            },
            'filter' => array(0=>'Pending', 1 => 'Converted',2=>'Not Found',3=>'Duplicate')
        ],
        

    ],
]); ?>
<?php CustomPjax::end(); ?>


</div>
