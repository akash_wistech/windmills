<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ListDataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="list-data-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'listings_reference') ?>

    <?= $form->field($model, 'source') ?>

    <?= $form->field($model, 'listing_website_link') ?>

    <?= $form->field($model, 'listing_date') ?>

    <?php // echo $form->field($model, 'building_info') ?>

    <?php // echo $form->field($model, 'city_id') ?>

    <?php // echo $form->field($model, 'property_category') ?>

    <?php // echo $form->field($model, 'no_of_bedrooms') ?>

    <?php // echo $form->field($model, 'built_up_area') ?>

    <?php // echo $form->field($model, 'land_size') ?>

    <?php // echo $form->field($model, 'listings_price') ?>

    <?php // echo $form->field($model, 'listings_rent') ?>

    <?php // echo $form->field($model, 'final_price') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
