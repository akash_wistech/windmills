<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BilalContacts */

$this->title = 'Update Bilal Contacts: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bilal Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bilal-contacts-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
