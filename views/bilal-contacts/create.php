<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BilalContacts */

$this->title = 'Create Bilal Contacts';
$this->params['breadcrumbs'][] = ['label' => 'Bilal Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bilal-contacts-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
