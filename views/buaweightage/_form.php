<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Buaweightage */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="buaweightage-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'difference')->textInput(['maxlength' => true])->label('Difference (%)')?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'bigger_sp')->textInput(['maxlength' => true])->label('Bigger SP (%)')?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'smaller_sp')->textInput(['maxlength' => true])->label('Smaller SP (%)')?>
            </div>

        </div>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
