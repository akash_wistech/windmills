<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use app\assets\SortBootstrapAsset;
SortBootstrapAsset::register($this);

use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);

$this->title = Yii::t('app', $title); 
$this->params['breadcrumbs'][] = $this->title;

?>


<style>
/* .dataTable th {
    color: #0056b3;
    font-size: 14px;
    text-align: left !important;
    padding-right: 30px !important;
}

.dataTable td {
    font-size: 16px;
    text-align: right;
    padding-right: 50px;
}

.content-header h1 {
    font-size: 16px !important;

}

.content-header .row {
    margin-bottom: 0px !important;
}

.content-header {
    padding: 4px !important;
} */


/* table {
  border-collapse: collapse;
}
td, th {
  height: 3px !important;
} */

.dataTables_wrapper tbody td {
  height: 5px;
  padding: 5px;
}

.dataTable th {
    color: #0056b3;
    font-size: 15px;
    text-align: left !important;
    padding-right: 30px !important;
}
.dataTable td {
    font-size: 15px;
    text-align: left;
    /* padding-top: 3px; */
}
/* 
.content-header h1 {
    font-size: 16px !important;
} */

/* .content-header .row {
    margin-bottom: 0px !important;
} */

/* .content-header {
    padding: 0px !important;
} */

</style>

    <?php
    if($time == 'today'){
        echo $this->render('today_inspection_scheduled',[
            'inspections' => $inspections,
        ]);
    }
    if($time == 'pending'){
        echo $this->render('pending_inspection_scheduled',[
            'inspections' => $inspections,
        ]);
    }
   
    if($time == 'overall'){
        echo $this->render('total_inspection_scheduled',[
            'inspections' => $inspections,
        ]);
    }
    ?>
    
<div class="reason-modal-append">

</div>




<?php
$this->registerJs('
        
    var dataTable = $("#bank-revenue").DataTable({
        autoWidth: false,
        order: [
            [0, "desc"], 
            // [1, "asc"],
        ],
        pageLength: 25,
        searching: true,
        dom: "lrtip", // Specify the desired layout
        columnDefs: [
            {
                targets: -1,  // The last column index
                orderable: false  // Disable sorting on the last column
            },  
        ]
    });
    
    
    $("#bank-revenue_filter").css({
        "display":"none",
    });

    $(".custom-search-input-text").on("keyup", function () {
        dataTable.search(this.value).draw();
      });

      $(".custom-search-input").on("change", function () {
        $("#intruction_date").val("");
        $("#inspection_date").val("");
        dataTable.search(this.value).draw();
      });

      $(".custom-search-input").on("keyup", function () {
        $("#intruction_date").val("");
        $("#inspection_date").val("");
        dataTable.search(this.value).draw();
      });

      //for date search
      $("#intruction_date").on("change", function () {
        $(".custom-search-input").val("");
        var format1 = moment($("#intruction_date").val()).format("DD-MMM-YYYY"); 
          dataTable.search(format1).draw();
        });

        //for inspection date search
        $("#inspection_date").on("change", function () {
          $(".custom-search-input").val("");
          var format1 = moment($("#inspection_date").val()).format("DD-MMM-YYYY"); 
            dataTable.search(format1).draw();
          });
    
    $(".custom-search-input").on("click", function (event) {
        event.stopPropagation();
    });
    
    // Move the length menu to the table footer
    // var lengthMenu = $(".dataTables_length");
    // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");
    
    // Create a custom footer element
    var customFooter = $("<div class=\"custom-footer\"></div>");
    
    // Move the length menu to the custom footer
    var lengthMenu = $(".dataTables_length");
    lengthMenu.detach().appendTo(customFooter);
    
    // Append the custom footer to the DataTable wrapper
    $("#bank-revenue_wrapper").append(customFooter);
        
        $(".custom-search-input-client").on("click", function (event) {
          event.stopPropagation();
        });

    $(".view-reasons-btn").on("click", function (event) {
        $.ajax({
            url: "'.yii\helpers\Url::to(['suggestion/get_inspection_reasons']).'/"+$(this).attr("data-valuation-id"),
            method: "post",
            dataType: "html",
            success: function(data) {
            //   console.log(data);
                $(".reason-modal-append").html(data);
                $("#inspection-reasons-modal").modal("show");
            },
            error: bbAlert
        });
    });

    $(".custom-search-input-client").on("change", function () {
        $.ajax({
          url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
          method: "post",
          dataType: "html",
          success: function(data) {
            console.log(data);
            dataTable.search(data).draw();
          },
          error: bbAlert
      });
    });
    
        
        
');
?>

