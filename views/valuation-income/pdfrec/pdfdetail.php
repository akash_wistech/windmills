
<?php
$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();

$standard_report = \app\models\StandardReport::find()->one();

$ValuerName= \app\models\User::find()->select(['firstname', 'lastname'])->where(['id'=>$approver_data->created_by])->one();

$valuerOther= \app\models\UserProfileInfo::find()->select(['valuer_qualifications','valuer_status','valuer_experience_expertise','signature_img_name'])->where(['user_id'=>$approver_data->created_by])->one();

// print_r(Yii::$app->get('s3bucket')->getUrl('images/'.$valuerOther['signature_img_name'])); die;

$special_assumptionreport = \app\models\SpecialAssumptionreport::find()->one();

$conflict= \app\models\ValuationConflict::find()->where(['valuation_id'=>$model->id])->one();
$exclusions= \app\models\Exclusions::find()->where(['status'=>1])->all();


//18.	Nature and Sources of Information and Documents Relied Upon
$ReceivedDocs= Yii::$app->PdfHelper->getReceiveDocs($model);

//	Market Commentary
$model_data = \app\models\MarketCommentary::find()->orderBy('id DESC')->one();
$city_lower=strtolower(Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city]);
$replaceSpace=str_replace(" ","_",$city_lower);
$replaceSpace.='_commentary';


//25 Property Description – Internal
$select_calculations = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $model->id, 'type' => 'list'])->one();



if($model->client->id == 183 || $model->id >12254){
    $appendices_number = '2';
    $valuer_number = '3';
    $assumptions_number = '4';
    $market_number = '5';
}else {
    $valuer_number = '2';
    $assumptions_number = '3';
    $market_number = '4';
    $appendices_number = '5';
}
?>
<br pagebreak="true"/>
<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
          0<?= $valuer_number; ?>: Valuer
</div>
<br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.01: Valuer Name</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $ValuerName['firstname'].' '.$ValuerName['lastname'] ?></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.02: Valuer Qualifications</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $valuerOther['valuer_qualifications'] ?></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.03: Valuer Status</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $valuerOther['valuer_status'] ?></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.04: Valuer Experience and Expertise</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $valuerOther['valuer_experience_expertise'] ?></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.05: Valuer Duties and Supervision</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->value_duties_supervision ?></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.06: Internal/External Status of the Valuer</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->internal_external_status_valuer ?></td></tr>
</table>
<br pagebreak="true"/>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.07: Previous involvement and Conflict of Interest</h4></td></tr>
<tr><td class="detailtext" colspan="2">We are not aware of any conflict of interest on the level of seller, buyer, client, third party engaged by the Client and/or the subject property,
either on the part of the Firm or Valuer or any individual member of our team assigned to this valuation,
which prevent us from providing an independent and objective opinion of the value of the Property.</td></tr>
<?php
if($conflict['related_to_buyer']== 'Yes'){
?>
<tr><td class="detailtext" colspan="2"><b style="color:#0277BD">BUYER</b></td></tr>
<tr><td class="detailtext" colspan="2"><?= $conflict['related_to_buyer'] ?></td></tr>
<?php
}
if($conflict['related_to_seller']== 'Yes'){
?>
<tr><td class="detailtext" colspan="2"><b style="color:#0277BD">SELLER</b></td></tr>
<tr><td class="detailtext" colspan="2"><?= $conflict['related_to_seller'] ?></td></tr>
<?php
}

if($conflict['related_to_property']== 'Yes'){
?>
<tr><td class="detailtext" colspan="2"><b style="color:#0277BD">PROPERTY</b></td></tr>
<tr><td class="detailtext" colspan="2"><?= $conflict['related_to_property'] ?></td></tr>
<?php }
$special_assumptions='';
//special Assumptions Occupancy, Tananted, Vacant, Acquisition
//  $special_assumptions.= '<br><b>Occupancy Status</b><br>';
if($model->inspectProperty->occupancy_status == "Owner Occupied"){
$special_assumptions.=$special_assumptionreport->owner_occupied;

}
if($model->inspectProperty->occupancy_status == "Tenanted"){
$special_assumptions.=$special_assumptionreport->tenanted;
}

if($model->inspectProperty->occupancy_status == "Vacant"){
$special_assumptions.=$special_assumptionreport->vacant;
}
if($model->inspectProperty->acquisition_method == 1 || $model->inspectProperty->acquisition_method == 2 ){
$special_assumptions.= '<br>'.$special_assumptionreport->gifted_granted;
}else{
if(trim($special_assumptionreport->purchased) != 'None' && trim($special_assumptionreport->purchased) != '') {
$special_assumptions .= '<br>' .$special_assumptionreport->purchased.'<br>' ;
}


}
if ($model->scheduleInspection->valuation_date > $model->scheduleInspection->inspection_date  ) {
$valuationdate_assumption= \app\models\GeneralAsumption::find()->where(['id'=>5])->one();
$special_assumptions .= '<br>' .$valuationdate_assumption->general_asumption.'<br>';

}
if($model->special_assumption !== 'None' &&  ($model->special_assumption <> null)) {
$special_assumptions.= trim($model->special_assumption);
}

?>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.08: Declaration of Independence and Objectivity</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->declaration_independence_objectivity ?></td></tr>
</table>
<!-- <br><br>
        <table cellspacing="1" cellpadding="8" class="main-class">
          <tr><td class="detailheading" colspan="2"><h4>1. Subject Property (Interest) to be Valued</h4></td></tr>
          <tr><td class="detailtext" colspan="2"><?= ''; //$standard_report->subject_property ?></td></tr>
        </table>
<br><br>
        <table cellspacing="1" cellpadding="8" class="main-class">
          <tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>. Valuation Instructions</h4></td></tr>
          <tr><td class="detailtext" colspan="2"><?= '';//$standard_report->valuation_instructions ?></td></tr>
        </table> -->
<br pagebreak="true"/>
<div  class="col-12"
      style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
        0<?= $assumptions_number; ?>: Assumptions and Considerations:
</div>
<br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.01:Purpose of this Reinstatement Cost Assessment Report </h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>This report has been developed on behalf of the Client, who require a detailed report on the estimated reinstatement cost of the subject property. The Client requires a reinstatement cost for Insurance Purpose to comply with local legislation and international best practice. </p>
            <p>The Reinstatement Cost Assessment has been prepared having regard to the advice given by the RICS and insurance companies for building insurance purposes and is not appropriate for any purpose other than setting the insurance rebuilding estimated cost.</p>
        </td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.02: RICS Professional Standards (and Departures from those Standards)</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>This assessment follows the guidance outlined in the Royal Institution of Chartered Surveyors (RICS) Practice Standard - Reinstatement Cost Assessments of Buildings, 3rd Edition February 2018. The Report have been prepared in accordance with the International Valuation Standards and the RICS Valuation – Global Standards effective 31 January 2022 (the Red Book). In accordance with your instructions in preparing our reinstatement cost assessment report, we depart from the mandatory requirements of Red Book in the following regards: None. Our Company has established a Complaints Handling Procedure (CHP) and Complaints Logbook, complying with the requirements of the RICS. The procedure is available to the Client on request.
        </p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.03: Compliance on the Regulation in the United Arab Emirates</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>The Reinstatement Cost Assessment Report have been prepared in accordance with:
                Law No. (27) of 2007 Concerning Ownership of Jointly Owned Real Property in the Emirate of Dubai, Part 9 – Insurances,
                Article 72.

            </p>
        <p>The Owners Association must insure in its own name.</p>
            <p>(a) the building, under a comprehensive insurance policy against damage or destruction by explosion, fire, lightning, storm, tempest and water for:</p>
            <p> &nbsp; &nbsp;(i). its full replacement value (as ascertained at least once every 3 years by a professional valuer); and</p>
            <p> &nbsp; &nbsp;(ii). the costs incidental to its replacement or reinstatement, including the cost of removal of debris and professional fees on re-building, so that the building is reinstated to the condition it was in when new;</p>

        </td></tr>
</table>
<br><br>
<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.04: Basis of Value:</h4></td></tr>

<tr><td class="detailtext" colspan="2"><p>The basis of value of this assignment is Reinstatement Cost.
            The assessment must not be used for market valuation or lending purposes. The reinstatement cost of a building is unlikely to have a direct relationship to its market value.
        </p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.05: Reinstatement Cost:</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>A Reinstatement Cost Assessment (RCA) is defined as the cost of reinstating the existing structures at the address noted in the report, together with the building services, to the same design, in new materials using modern construction techniques, to a standard and size equal to, but no greater or better than, the existing structures, in accordance with current Building Regulations and other statutory requirements.</p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.06: Terms of Engagement.</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>This Reinstatement Cost Assessment is governed by the Terms of Engagement signed between the Client and Windmills Real Estate Valuation Services (referred to in this report as “Firm”) by way of “Terms of Engagement” agreed and signed by the Client. The agreed Terms of Engagement and the Form and Contents of this Reinstatement Cost Assessment report apply together with each other and must always be read together.</p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.07:Scope of Investigation:</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>The Reinstatement Cost Assessment has been prepared having regard to the advice given by the RICS and insurance companies for building insurance purposes and is not appropriate for any purpose other than setting the insurance rebuilding value. The report provides a reinstatement cost assessment based upon an overview and approximate estimating methods. A more accurate method of recalculating a reinstatement cost would be to commission quantity surveyors to prepare a detailed tender document for pricing by contractors. The detailed measurement and documentation required would render this an extremely involved and costly exercise, which is beyond the scope of this instruction. The assessment does not contain advice concerning the condition of the building or possible defects, nor has any structural survey or other building inspection been undertaken. The assessment must not be used for market valuation or lending purposes. The reinstatement cost of a building in unlikely to have a direct relationship to its market value.
            Considering that the Firm is to provide a Reinstatement Cost Assessment report within a reasonable timeframe and at an economic cost agreed, the following limitations in the scope of inspections and due diligence in enquiries are agreed together with the necessary assumptions which are adopted in this Reinstatement Cost Assessment report to cover uncertainties.
        </p>
        <b>Exclusion</b>
        <?php
        foreach ($exclusions as $key => $exclusion){ ?>
            <p>&nbsp;&nbsp; <?=($key + 1) ?>)	<?= $exclusion->title; ?></p>
        <?php
        }
        ?>


    </td></tr>
</table>
<!--<br pagebreak="true"/>-->
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
  <tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.08: Physical Inspection</h4></td></tr>
  <tr><td class="detailtext" colspan="2"><p>The inspection of the Property is carried out internally and externally by the Inspecting Officer highlighted “Inspecting Officer” in the “Subject Property Overview” under the supervision of the Valuer. The detailed site inspection report has been attached in the Appendices section of this Reinstatement Cost Assessment report. There were no special circumstances or restrictions encountered during the inspection. We have taken a sufficient number of photographs of the Property both from the inside and outside, some of which are attached in the appendices. The Inspecting Officer has measured the property during the inspection to verify the built-up area of the property. We assume that the built-up areas provided to us by the Client or his/her representative are correct and are in accordance with the latest RICS Code of Measuring Practice and International Property Measurement Standards. Should the adopted areas differ from the said code, this may affect the Reinstatement Cost Assessment, and in such case, we reserve the right to review our Reinstatement Cost Assessment accordingly without any responsibility or liability.</p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.09: Assumptions  </h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>Our assessment is based on the assumption that the buildings are totally destroyed or damaged to such an extent that total reconstruction is required. We have made no allowance for temporarily supporting or protecting the damaged structure.
                The assessment excludes any allowances for upgrading or improvements that may be incorporated in the redesign of the building.
                We have allowed in our assessment for the buildings to be rebuilt in a manner to comply with current statutory building regulations.
            </p></td></tr>
</table>
<br pagebreak="true" />
<!--<br pagebreak="true" />-->
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.10: Exclusions</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>We attach within Appendix 6 to this report a Schedule of Elements typically both included and excluded from the Landlords/Owners reinstatement cost assessment.
            No allowance has been included in our figures for loss of rental income or for temporary payment of rental that you may incur on temporary accommodation during the period of reinstatement following either partial or complete destruction of the property. Separate provision should be made to cover this risk by means of a loss of notional rent if required. We would recommend that the appropriate period to be adopted is that which will facilitate the complete rebuilding of the premises.

        </p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.11: Structural and Technical Survey</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>We are not instructed to undertake any structural or technical or engineering or conditional surveys at the subject property. Since we are not the experts/professionals on structural and technical developments of the subject property, The Firm and the Valuer have not carried out a building structural and technical survey or tests of any sort (i.e., including but not limited to the air-conditioning, civil, mechanical, electrical or plumbing). Therefore, we are not able to provide any opinion or assurance that the property is free from defects or not.</p>
    <p>We advise the Client to take experts/professional opinions in these regards. For the sake of this Reinstatement Cost Assessment, we assume that the property is facilitated with all standards services and amenities, they are in a proper order and working condition, and no material construction/building problems that require replacement, maintenance and/or repair work is needed.</p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.12:Conditions and State of Repair and Maintenance of the Property</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>Our Reinstatement Cost Assessment assumes that except for any material defects notified to us by the Client or generally observed during our inspection (or not if it is agreed to be a desktop Reinstatement Cost Assessment) and noted in our report, the property and all the fitted installations, machinery and equipment therein are in a good condition and state of repair and maintenance overall.
        </p></td></tr>
</table>

<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.13: Natural Disaster</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>Since we are neither the nature/geological experts/professionals nor are we mandated to carry out a geological survey under this Reinstatement Cost Assessment, the Firm and the Valuer has not carried out an such survey or test with regard to any existing or potential risks of any national calamities/disasters including storms, flooding, earthquakes, pandemics etc. Therefore, we are not aware of any such risks, and are unable to provide any opinion on the site whether it is free from such risks or not.
                The Client is suggested to take expert’s opinion on the above-mentioned risk subjects. Our Reinstatement Cost Assessment assumes that none of such risks exists. We take no responsibility/liability in this regard.
            </p></td></tr>
</table>
<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.14: Contamination, Ground and Environmental Considerations</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>Since we are neither the environmental experts/professionals nor are we mandated to carry out an environmental survey under this Reinstatement Cost Assessment, the Firm and the Valuer has not carried out an environmental, or archaeological, or ecological survey or test of any sort in or around the property or in the surrounding neighborhoods. We recommended that the Client secures an independent and professional advice about the environmental risk and conditions associated with the property.</p>
    <p>We are not aware of any such risks, and our Reinstatement Cost Assessment assumes that the property is free from all and any risks associated with the factors including but not limited to the Deleterious or Radon Gas or Hazardous Material or Asbestos, Adverse Ground Conditions. Pollution and Contamination, Flooding, Mining, Fire, Medical Conditions/Diseases, Fire, Disabled access, Coastal Erosion, Brine Extraction, Incapacity of Development or Redevelopment and any other matters, which may affect the sustainability of the subject property and/or this Reinstatement Cost Assessment.</p>
    </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.15: National Scenarios or Force Majeure Situations</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>We have not considered, developed, forecasted or incorporated any potential national or regional or force majeure situation analysis and/or scenarios in this Reinstatement Cost Assessment report, including but not limited to political, economic, legal, regulatory, social and the real estate sector related scenario in this Reinstatement Cost Assessment report. The client is recommended to consult relevant professionals/experts. Our Reinstatement Cost Assessment report assumes that there are no associated risks with regard to any national or regional or force majeure situation and/or scenarios existing in the foreseeable period. We take no responsibility/liability in this regard.
        </p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.16: Statutory and Regulatory Requirements</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>Since we are not the statutory and regulatory experts/professionals, the Firm and the Valuer has not carried out any compliance related survey of any sort in or around the property. We recommended that the Client secures an independent and professional advice about the Statutory and regulatory requirements. Our Reinstatement Cost Assessment assumes that all relevant statutory. legal and regulatory requirements relating to the subject property (including but not limited to planning, building, fire regulations, disability access, and health and safety) have been complied with fully. All necessary licenses, permissions, certificates, and documents are obtained, complete, updated and valid. We take no responsibility/liability in this regard.
        </p></td></tr>
</table>
<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.17: Planning</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>The assessment is intended to reflect the costs of the provision of a replacement building, compliant with building regulations, to incorporate current usage, replicating the design as closely as is practicable.
        </p></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.18: Plant and Equipment:</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>We include in our Reinstatement Cost Assessments those items of the equipment that are normally considered to be a fitted, integral and/or immovable part of the service installations to a building and which would normally pass with the property on a sale or letting. We exclude all items of process equipment, together with their special foundations and supports, furniture and furnishings, vehicles, stock and loose tools, and tenants’ fixtures and fittings.
            We have not checked and verified the condition and quality of such plant and equipment considered to be a part of service installation. We recommend that the Client secures an independent and professional advice about the plant, machinery and equipment. As we are not technical experts/professionals on the plant, machinery and equipment, we assume that they are in good and working condition. Unless otherwise specified no allowance is made for the cost of repairing any damage caused by the removal from the premises of items of plant, machinery, fixtures and fittings.

        </p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.19: Documentation Provided by the Client:</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>We have stated the details of the documents provided to the Firm by the Client or his/her representatives in the section 1.80 “Documents Provided by the Client”. This Reinstatement Cost Assessment report is primarily based on the copies of the documents, data and information provided by the Client and/or the Owner of the Property and/or their representative(s). We have not had the opportunity to receive, view or inspect any of the original documents. We have not made any independent inquiries to verify the contents of the documents and information provided to the Firm. We assume and have relied upon all documents and the information provided to us by any one of them as being relevant, complete, accurate and valid. us. In case the required documents and information required as per the terms of engagement and/or proposal and/or by email are not arranged or provided by the client and/or 3rd party engaged by the Client timely, and/or they are not complete, correct, well-represented and/or valid, the Firm and the Valuer carry no responsibility or liability associated with the documents and information provided and any consequential impact on the Reinstatement Cost Assessment(s) reported herein. In such case, the Firm and the Valuer however assumes the information based on his best judgement possible and will assume no liability or responsibility for doing so.</p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.20:Documentation not Provided by the Client:</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>We have stated the details of the documents not provided to the Firm by the Client or his/her representatives in the section 1.81 “Documents Not Provided by the Client”. We take no responsibility or liability or obligation for the documents, data and/or information not provided or arranged for by the Client, and any consequential impact on the Reinstatement Cost Assessment(s) reported herein.
        </p>

    <p>•	Documents provided by the Client (as stated in the section on the Subject Property Overview).</p>
    <p>•	Inspection observations and measurements, if applicable.</p>
    <p>•	Similar Assignments done by us in the past, if applicable.</p>
    <p>•	Any other public and private source that is deemed appropriate, relevant, and/or applicable.</p>
        <p>
            We have used and relied upon the above-mentioned sources of information and assumed them to be correct, valid, and relevant to the Property and the Reinstatement Cost Assessment. We have not verified the authenticity of the documents or data or information available in the various sources mentioned above. We assume no liability or responsibility in case we received misleading or incomplete or incorrect information from the above-mentioned market to arrive at the estimate of reinstatement cost of the Property.
        </p>

    </td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.21: Restriction on Use, Distribution and Publication of the Report</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>No part of this report or the whole report may be copied, extracted from, re-produced, published and/or distributed and/or without our prior consent from the Valuer in writing. We strongly recommend not to share this Reinstatement Cost Assessment report with any one party than the relevant members of the Client with confidentiality. Where the purpose of the report requires a published reference to it, the valuer must agree to provide a draft statement for inclusion in the publication before its publication.</p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.22: General Uncertainties</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>This Reinstatement Cost Assessment is a professional opinion on a particular Reinstatement Cost Assessment date based on our best professional judgement possible that is derived from the information sourced from the documents, information, inspection, market analysis available, and the assumptions, approach and the methodologies adopted. Our Reinstatement Cost Assessment report is not a recommendation to the Client for making decisions related to the lending/credit or investment in any form and manner.</p>
    <p>We would like to notify the Client and the intended users that there are certain known and/or unknown existing and potential risks, which may not be reflected/controlled in the report. They may associate with the political, economic, social, legal and/or regulatory developments. They risks may also related to the changes in the sector with regard to processes, technological advancement, level of availability and transparency of the market and the real estate sector data, volatility of real estate market, misleading information received from various sources deployed, and changes in demand, supply and fee tariff of the sector, among others. It is beyond the scope of our services to ensure the perfect accuracy and consistency in values due to changing scenarios.</p>
        <p>Accordingly, we advise you that an uncertainty must be considered by the Client when any financial decision is made based on this Reinstatement Cost Assessment report, and adequate protective arrangements (including insurance cover) are made. We also recommend the Client to arrange for frequent Reinstatement Cost Assessments of the Property to monitor its reinstatement cost.</p>
    </td></tr>
</table>
<br pagebreak="true" />
<!--<br><br>-->
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.23: Limitation of Liability</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>We trust that this report and Reinstatement Cost Assessment fulfills the requirement and objectives of the Client’s Reinstatement Cost Assessment instruction. The statements of fact presented in the report are to the best of our knowledge. In undertaking and executing this Reinstatement Cost Assessment assignment, a reasonable level of care has been exercised. We assume no responsibility or liability for the misrepresentation that is beyond normality, practice, and scope of our responsibilities or our control.
        </p>
    <p>We advise the Client that before entering into any financial transaction based on this Reinstatement Cost Assessment, kindly obtain verification of all information contained in this Reinstatement Cost Assessment report, and the validity of the assumptions we have adopted across the report from other reliable expert sources. We have made various assumptions in this Reinstatement Cost Assessment, as highlighted throughout the report above, in order to arrive at our reinstatement cost estimate. In the event any of above-mentioned assumptions or information adopted in this Reinstatement Cost Assessment report proves to be incorrect or inappropriate or irrelevant thus impact the reinstatement cost of the subject property, we reserve a right to review and amend our Reinstatement Cost Assessments without any responsibility of liability or obligation on our part. There shall be no personal liability whatsoever by the individual valuer or the management member involved in the assignment in any circumstances. In any situation, the Firm does not assume any liability or responsibility whatsoever unless a proceeding is commenced after two months of the date of this Reinstatement Cost Assessment report.
        The amount of any claim in any circumstance is limited to an amount that does not exceed the fees received by the company from the client for this Reinstatement Cost Assessment assignment.
    </p></td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.24: Additional Recommendation of Policy Clauses</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>Whilst you will no doubt receive expert advice from your insurance brokers on the range of perils to be covered together with the precise content and format of the policy to be taken, we would recommend that you ensure that the following specific clauses are incorporated: </p>
    <h3>Debris Clearance Clause </h3>
    <p>This clause insures against the cost of removing debris in order to allow for a detailed assessment of the extent of damage caused. </p>
        <h3>Local Authority Clause  </h3>
        <p>This clause insures against increased costs resulting from the Local Authority requiring a superior standard of construction on the rebuilding when compared to the standard of the previously existing building. </p>

    <h3>Reinstatement Clause </h3>
        <p>This clause insures against a reduced level of compensation being paid on a depreciated building which has been damaged. Compensation is therefore paid on the basis of reinstatement and no account is made for possible betterment which may result from the destruction and subsequent rebuilding. </p>
    </td></tr>
</table>
<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.25. Confidentiality</h4></td></tr>
<tr><td class="detailtext" colspan="2"><p>This Reinstatement Cost Assessment report is strictly confidential between the Client, the intended user(s) and the Valuer. It should be noted that this Reinstatement Cost Assessment report and any subsequent amendments or changes in any form will only be shared with the Client and the intended user(s) authorized by the Client. The Client intended user(s) and the Valuer are to strictly ensure that this Reinstatement Cost Assessment report or any subsequent amendments or changes in any form will not be formally or informally/indirectly shared with any other non-authorized person with the formal consent of the Valuer and the Firm. The Firm or Valuer or any of its employees or representatives assume no responsibility or liability to the Client or intended users or any third party, who may breach confidentiality of this Reinstatement Cost Assessment report and use or rely upon the whole or any part of the contents of this report without a formal consent from the undersigned Valuer. The contents, formats, methodology and criteria adopted in this report are copyrighted by the Firm.</p></td></tr>
</table>
<br><br>


<br pagebreak="true"/>
<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
      0<?= $market_number; ?>: Methodology:
</div>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.1. Parameter of Assessment</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>The Reinstatement Cost assignment is the assessment of the cost of reconstructing the property on the date of the assessment and will take into consideration demolition, debris removal, temporary shoring and professional fees likely to be incurred in its reconstruction. This figure may be used to make your own insurance arrangements or for you to negotiate a claim with the assistance of your broker and your insurers. The figure is calculated based on estimated building costs and may not, in all circumstances, reflect the lowest tender price available.</p>
            <p>The figure is calculated using the Gross Internal Area (GIA) of the property, as defined in the RICS’ Code of Measuring Practice. The GIA has been calculated from site measurements recorded from provided drawings and our sample inspection. </p>
            <p>In estimating the cost of reinstatement, it has assumed that the building and its use will be similar to those existing, and the rebuilding will be to the original design in modern materials and using modern techniques to a standard equal to the existing property, whilst complying in all aspects with current legislation and statutory requirements. We have not made investigations into local or structure plans. We have made allowances in our assessment for the reinstatement of, for example, external paving, services and the like, which are assumed will be damaged as a result of a fire or similar loss. </p>
            <p>We have not included allowances for tenant’s fitting-out works, fixtures, fittings or furnishings. However, in assessing the extent of the building structure, services and fittings, we have made reasonable assumptions in respect of the inclusion of items that may have been installed by tenants but which, with reference to the lease and their degree of permanence and annexation to the structure, are now deemed to be of benefit to the owner. No allowance has been made for any remediation works that may be required under legislation to contaminated land, which may arise in the event of reinstatement of the property, since the extent and costs of such cannot be reasonably determined without separate detailed and costly investigations. You may wish to bring this to your insurer’s attention.</p>
            <p>Allowances for providing alternative accommodation, from the date of damage to the date of reoccupation, or the loss of rent or other pecuniary loss that may arise from the destruction of the building.</p>
            <p>The assessment does not include allowances for cover in respect to other property insurances, such as additional plant and machinery added by the third party, occupiers’ fitting outworks, contents, plate, glass and Third Party and Public Liability matters.
                Please note that this reinstatement cost assessment has no direct relationship to the market value of the property. The assessment has been prepared in accordance with the advice given by the Royal Institution of Chartered Surveyors and insurance companies for building insurance purposes and is not appropriate for any purpose other than insurance. This assessment is not including a value for secondhand values of materials, fixtures and fittings.
            </p>
            <p>Our assessment of the reinstatement value has been calculated in the light of current prices and conditions but in assessing the actual sum to be insured consideration must also be given to the problem of inflation.
                We have not shown any provisions or allowances for VAT on rebuilding costs, fees, etc. which in certain countries would attract VAT. It is essential that you consult with your accountants and insurance brokers to advise you of your particular liability in this respect and whether any tax will be payable.
            </p>
            <p>No allowance has been included in our figures for loss of rental income or for temporary payment of rental that you may incur on temporary accommodation during the period of reinstatement following either partial or complete destruction of the property. Separate provision should be made to cover this risk by means of a loss of notional rent if required. We would recommend that the appropriate period to be adopted is that which will facilitate the complete rebuilding of the premises.
            </p>

        </td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.2.	Basis of Assessment</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>Generally, all assessments are made on the basis of total loss or of such substantial damage that the entire building will require demolition and rebuilding. </p>
       <p>The usual requirement for a reinstatement cost assessment is that it is prepared on the basis of a ‘day one reinstatement’ figure known as the declared value, though it is prudent for this to be confirmed in the instructions and referred to in the final report. In calculating the declared value, RICS members and RICS-regulated firms should ignore the effects of inflation during the period of the insurance policy as this is allowed for in the inflation provision.</p>
            <p>The declared value figure should be the equivalent of a fixed price, lump sum, competitive tender submitted by a suitable and competent contractor for works to commence on site on the first day of the period of insurance (net rebuilding cost), together with appropriate allowances for demolition and other costs and all associated professional and other statutory fees. </p>
<p>If the insurance is not to be written on a day one reinstatement basis, but on some other basis giving the same ‘new for old’ cover, RICS members and RICS- regulated firms should establish exactly how the sum insured is to be calculated. It may have to be based on the assumption that a total loss occurs on the last day of insurance and that on that date it should be sufficient to pay for reinstatement allowing for all the time that might take and the delays that could occur. Some policies in the insurance market include allowances for index-linking of sums insured. This is not a problem if it is the sum insured at each renewal that is increased automatically in accordance with an index, that will not affect the current risk assessment. </p>
       <p>
           However, if index linking of a sum insured is different to this, care is needed. If in doubt, the assessment should make it clear how much allowance has been made for inflation and during what periods.
       </p>
            <p>
                The day one reinstatement basis is the most popular method in the reinstatement cost assessment, particularly for buildings insured by a landlord and leased. If there is no intention to repair or reinstate a damaged building, then some other basis of settlement may be more suitable.
            </p>
            <p>
                It is prudent to incorporate recommendations within the report to the effect that the client needs to reassess the sum insured on a regular basis, with an annual adjustment to reflect inflationary effects, and a major review and reassessment every three years, or earlier should significant alterations be made to the insured property.
            </p>
            <p>

            </p>
        </td></tr>
</table>
<br><br>
<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.3.	Building Inspection and Collecting Information</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>The quality of information initially gathered, and later collated, is pivotal to the accurate assessment of the reinstatement cost. An inspection of the property will be required in order to gather the information needed to complete the reinstatement cost assessment. During the course of the inspection the RICS member or RICS-regulated firm is advised to: </p>
        <p>1)	Make a general inspection of the property and its environs before beginning the detailed inspection and confirm that the extent of the property being inspected conforms to the description given by the client.</p>
        <p>2)	Assess the age of the building. If an assessment cannot be given without a degree of certainty, the RICS member or RICS-regulated firm should give an opinion of the building’s era and evolution. </p>
        <p>3)	Consider whether there are any restrictions to demolition and rebuilding that will result from the surrounding environment. Are there buildings, the occupation of which may affect working hours? Are any special protection works likely to be necessary?
            Is the building detached, semi-detached or terraced, therefore do party walls, temporary protection, insurers, etc. need to be considered? Consider proximity to public transport infrastructure and location within the city congestion zones.
        </p>
        <p>4)	Consider whether there are likely to be any deleterious materials that will increase the cost of demolition and debris removal due to higher disposal costs, i.e., review the asbestos register. </p>
        <p>
            5)	Take sufficient notes, photographs and measurements and produce sketches to record:
        </p>
            <p>&nbsp;a)	The layout of the building </p>
            <p>&nbsp;b)	Measurements sufficient to calculate the IPMS 2/ GIFA for each different type of construction; these should be in accordance with the current edition of the RICS professional statement RICS property measurement.  </p>
            <p>&nbsp;c)	A schedule of the accommodational use  </p>
            <p>&nbsp;d)	A summary of the building’s structure, and the different types of construction, eaves, ceiling heights and finishes   </p>
            <p>&nbsp;e)	A summary of the building’s services (for example, sprinkler provision, lifts and air conditioning) </p>
            <p>&nbsp;f)	A summary of the external areas, boundaries, outbuildings, etc.  </p>
            <p>&nbsp;g)	A summary of specialist features (these may be internal or external features considered as extra over the standard building finishes or that influence the basis of assessment, for example, marble cladding, ornate stonework or timber paneling)   </p>
            <p>&nbsp;h)	A summary of general topography.   </p>
        </td></tr>
</table>
<br><br>
<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.4.	Types of Assessment</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>To understand the bases on which buildings are, or need to be, valued it is prudent to understand indemnity, which is one of the three basic principles of insurance.
              The objective of indemnity is to place the insured, as nearly as possible, in the same financial position after a loss as that they were in immediately before the loss event. All insurance policies are contracts of indemnity but the basis on which indemnity is measured will vary to suit different circumstances.
          </p>
            <p>Some measures of indemnity, such as day one reinstatement, may leave an insured better off. This can give rise to a belief that ‘reinstatement’ and ‘indemnity’ are two different measurements. They are not. Reinstatement is just one way of assessing indemnity. If a building is not going to be repaired following damage or will not be repaired if the cost exceeds an economic figure, some other basis of indemnity may be more appropriate. </p>
        <p>According to the RICS Reinstatement Cost Assessment of Buildings – Guidance notes 3rd Edition, the most common variants of assessment are as follow: </p>
        <p>1. Day one reinstatement </p>
        <p>2. Reinstatement including inflation provision.  </p>
        <p>3. Reinstatement less wear and tear (‘indemnity’ basis)  </p>
        <p>4. Obsolete buildings   </p>
        <p>5. Site clearance, debris removal and ‘making safe’ costs only and    </p>
        <p>6. Second-hand value of building materials.    </p>

            <p>
                Additionally, or alternatively, a client may request a current assessment of market value. If such a request is received, the RICS member or RICS-regulated firm must refer to the extant edition of RICS Valuation – Global Standards (the RICS Red Book) and UK supplement, which set out mandatory requirements for members undertaking this type of work. The reference to ‘alternatively’ is because market value may in some cases be used as a means of arranging policy cover. If repairs to a building insured on an ‘indemnity by reinstatement’ basis are not carried out, then the insured is not entitled to an equivalent payment in cash. Instead, any claim will be based on the diminution in market value as a result of the damage. What was the value of the buildings (and site) the minute before the loss and what is the value of the remains of the buildings (and site) post loss? (This would be the measure of loss, provided that this does not exceed the cost of repairs.) If an insured decides not to effect repairs, it is usually for a sound financial reason. Perhaps the building has no tenant and there is no prospect of finding one. Perhaps it is just ‘in the wrong place’ now. In that event the diminution in market value may be far less than the cost of reinstatement. Thus, if it is known in advance of damage that repairs will not be carried out for any reason, the insured could opt for this basis of indemnity under the policy from the outset.
            </p>
            <p>This will also carry a lower sum insured and, therefore, a lower premium. This basis is usually only suitable if there is no possibility of any reinstatement being necessary or obligatory following either partial or total loss. In some cases, for example where there is redevelopment potential for a ‘better and higher’ use, a building may not suffer any loss of market value following damage, so there is a possibility of paying a premium but not being able to prove a loss. </p>
        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.5.	Day One Reinstatement</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>Most buildings in the UK are insured on what is commonly known as the ‘day one reinstatement’ basis. Insurance cover arranged under such policies allows for claims to be settled on a ‘new for old’ basis if repairs are actually carried out. In theory, it does not matter how old a building is or what its state of repair. If insurers have agreed indemnity by reinstatement cover, the damage to the building will be repaired to a condition substantially the same as, but not better than or more extensive than, its condition when new and the insured will not have to contribute to any betterment. ‘Day one reinstatement’ is derived from the fact that the sum insured is made up of two elements:</p>
<p>1. A ‘declared value’ and </p>
<p>2. An ‘inflation provision’,  </p>
<p>2. An ‘inflation provision’,  </p>
            <p>
                As explained below. The insurer’s limit of liability for any claim is the sum insured on any particular building unless the policy provides otherwise.
            </p>
            <h3>Declared value. </h3>
            <p>This is the cost of rebuilding and associated on costs as detailed in section 5, at the level of costs applying at the commencement of the insurance period without any provision for inflation. </p>
        <h3>Inflation provision </h3>
            <p>This is a percentage uplift selected by the insured to cover inflation during the insurance year and during the subsequent period required for designing, planning, tendering and actual reconstruction, however long that might take. The percentage selected can only be an estimate based on anticipated building costs in the future and the worst-case scenario for the length of time it could take to rebuild. </p>
       <p>RICS members and RICS-regulated firms may be asked to comment on a suitable uplift percentage for inflation for the operative period of the policy. Commonly, percentages uplifts vary between 15 and 50 per cent, sometimes depending on how much extra premium the insured is prepared to pay for inflation protection or how much can be obtained from the insurer at no extra cost. </p>
        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.6.	Reinstatement Including Inflation Provision</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>The basis of the RCA is as in sections 5 and 6. However, unlike the day one basis, the sum insured does not have two constituent parts  rganize d separately in the policy. There will be one sum insured inclusive of an allowance for inflation and it is this sum insured, rather than the ‘declared value’ under the day one reinstatement basis, that will be used for premium calculation and average purposes. </p>
<p>RICS members and RICS-regulated firms should make an allowance for inflation, for the period of the policy and the period of procurement of reinstatement work, bearing in mind that a loss might occur on the last day of a policy period. Recognizing the difficulty in predicting the rate of inflation, insurers have often accepted a margin of error of 15 per cent at the time of a loss before they would impose an average and hence cover may include an ’85 per cent average condition’. </p>
 <p>Subject to the policy limit, claims will be paid in full provided that the sum insured is at least 85 per cent of the true value at risk calculated at the time of loss. This is helpful for partial losses but not if there is a total loss and the sum insured is insufficient. </p>
        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.7.	Reinstatement Less Wear and Tear (Indemnity Basis)</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>This basis of indemnity is a feature of most policy wordings but, in practice, few buildings are insured on this basis of indemnity. It is a rarely used method of settling claims but there are circumstances in which it may be imposed by insurers or selected by an insured. When a building is very old or not in a good state of repair, the insurer may be unwilling to grant full reinstatement cover, or the insured may wish to save premium by having a lower sum insured while accepting that there would be significant and inevitable betterment contribution to be made in the event of repairs being necessary. </p>
        <p>The RICS member’s or RICS-regulated firm’s role in this type of survey is to value on a reinstatement cost basis, but to depreciate elements to reflect their current condition. In the case of brick walls, the difference between reinstatement cost and the value less wear and tear may be slight, but in the case of services and decorations there may be little, or no value included, as they have no useful life left. These elements of construction would have no value at the time of assessment and similarly no value at the time of any future claim. </p>

        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.8.	Obsolete Buildings</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>The RICS member’s or RICS-regulated firm’s role in this type of survey is to make an assessment on a reinstatement cost basis, but to depreciate elements to reflect their current condition. In the case of brick walls, the difference between reinstatement cost and the value less wear and tear may be slight, but in the case of services and decorations there may be little, or no value included, as they have no useful life left. These elements of construction would have no value at the time of assessment and similarly no value at the time of any future claim. </p>
       <p>This rare basis of assessment could be suitable if a building would be demolished and rebuilt in a different and cheaper form if it was destroyed or seriously damaged. Partial damage would be repaired up to an agreed amount but beyond that amount the building would be knocked down and replaced by a modern building providing the same function. The premium would be calculated on the full cost of reinstatement of the existing building with a reduction to reflect that the insurers’ limit of liability is a lesser amount based on the demolition of what remains of the original building and the cost of the modern replacement. The saving in premium is relatively small as most losses are partial and the insurers would only save on larger claims. </p>
       <p>This basis is particularly useful for both insured and insurer where the ‘obsolete’ building is a poor risk carrying a high premium. The insured saves on premium and the insurer has a lower exposure. RICS members and RICS-regulated firms may be asked to assess the full reinstatement cost, as existing, and the comparable cost of the provision of
           a replacement with the same functionality to the client, which could be a different shape, or a more efficient or cost-effective design, e.g., clear span portal frames in lieu of a cumbersome and costly north light roof with many intermediate columns.
       </p>

        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.9.	Site Clearance, Debris Removal and “Making Safe” Costs Only</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>If a building is due for demolition no material cover may be necessary if, when damage occurs, the date of demolition can be brought forward.
                However, the cost of demolition may be higher as a consequence of the damage or, if demolition cannot be brought forward, additional costs may be incurred in making the building safe in the meantime.
            </p>
            <p>Such extra costs, arising as a result of the damage, may be assessed and insured on their own. Compliance with stringent waste control regulation and professional fees associated with supervision will need to be reflected in such an assessment. </p>

        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.10.	Second-Hand Value of Building Materials</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>This form of assessment of value is rarely requested. A building due for demolition or even redevelopment may have no value except that arising from items that have
       a second-hand value, such as fireplaces, architectural features and roofing tiles. If that second-hand value could be lost as a result of damage, then that value can be assessed.

   </p>
        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.11.	Net Rebuilding Cost</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>With few exceptions, the final assessment figure will comprise the total cost of completely rebuilding the property to be insured (net rebuilding cost), together with allowances in respect of other matters, including: </p>
        <p>1) Demolition and debris removal. </p>
        <p>2) professional and statutory fees.  </p>
        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.11.	Net Rebuilding Cost</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>With few exceptions, the final assessment figure will comprise the total cost of completely rebuilding the property to be insured (net rebuilding cost), together with allowances in respect of other matters, including: </p>
            <p>1) Demolition and debris removal. </p>
            <p>2) professional and statutory fees.  </p>
            <p>
                To carry out the reinstatement cost assessment, the RICS member or RICS-regulated firm will need to determine the IPMS 2/GIFA measurements of the building according to RICS property measurement.
                The net rebuilding cost is normally calculated by multiplying the gross internal area of the building by a suitable rate for its reconstruction.

            </p>
            <p>It is advisable for the net rebuilding cost of the whole property to include: </p>
            <p>1) The cost of rebuilding the whole of the building in its present design and materials, to its existing shape and size, including basements, foundations and retaining walls </p>
            <p>2) Any tenants’ alterations for which the landlord has a responsibility to insure.</p>
            <p>3) Allowance for modifications to the design and specification required to comply with current Building Regulations and statutory requirements</p>
            <p>4) All external works and services such as drainage, manholes, water supply, electricity supply, boundary structures and outbuildings (if required by the policy). </p>
       <p>Evidence for current building costs can be obtained from various sources including BCIS, price books and in-house costings for similar projects.
           Average prices for reinstatement costs are usually generated from a wide range of building costs that reflect differing methods of construction, scopes of work and standards of finishes.
       </p>
            <p>Care should be taken to ensure that the building rate is suitable for the building being assessed. An elemental breakdown of the reinstatement cost may assist in the accurate calculation of a suitable rebuilding rate for the property. If this approach is used it is recommended that building elements and definitions applied are those as defined in the standard industry practice where the subject property is located and/or the assessment being conducted. </p>

      <p>The RICS member or RICS-regulated firm should consider the location of the property in considering the rebuilding rate, as it is advisable to take local variations in construction costs into account. Approximate quantities may also be used to assist in the appropriate allocation of costs in the elemental breakdown. RICS members and RICS-regulated firms are advised to confirm with the appropriate authority: </p>
            <p>1) Whether the property is listed </p>
            <p>2) If so, the category of listing  </p>
            <p>3) Whether this applies to the whole or part of the building</p>
            <p>
                Any listing may have an impact on the reinstatement cost. The RICS member or RICS-regulated firm should take these effects into account. In a conservation area, restrictions will apply that may affect reinstatement cost levels and timescales for rebuilding. With listed or specialist buildings, the RICS member or RICS-regulated firm should have regard to abnormal costs relating to:
            </p>
            <p>1) Programmed implications</p>
            <p>2) Replacement using vernacular materials and uncommon traditional techniques in replicating items such as ornamentation, lead work and stonework</p>
<p>3)	The involvement of heritage bodies</p>
            <p>
                RICS members and RICS-regulated firms should consider other adjustments that might be necessary to account for additional costs of reconstruction. For instance, the location of the property may be such that there are particular access restrictions, such as river frontage or pedestrianized areas.
            </p>
            <p>It is advisable for the reinstatement cost assessment to include certain additions to take account of further costs that will be incurred in the reconstruction of the building. </p>

        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.12.	Debris removal, demolition and shoring Up Party walls</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>The assessment should incorporate an allowance for demolition of any remaining sections of the building and associated site clearance prior to reinstatement. The method of calculation is typically undertaken by way of a percentage proportion over the construction cost, or as a lump sum based on published rates. </p>
            <p>It is worth considering additional demolition costs, where applicable, due to factors such as site access difficulties, demolition of reinforced concrete and high-level working. </p>
            <p>It is also worth considering the presence (or potential presence) of deleterious materials such as those containing asbestos. These will present demolition and removal difficulties and will result in increased costs.</p>

            <p>Insurance cover for removal of building debris is usually restricted to the site and an area immediately adjacent to the site. RICS members and RICS-regulated firms should check there is cover for clearing, cleaning and repairing services to the site. Cover for cleaning up pollution and contamination from buildings and the site on which the buildings stand may be limited to a sum stated within the policy. </p>
       <p>Market wordings for debris removal cover do vary. It is advisable to ask for a sight of the wording so that the reinstatement cost assessment is drawn up on the basis of the exact cover being given. Alternatively, and especially when there are particular potential problems at the site, the assessment should make it clear how the figures have been arrived at so they can be checked against the cover. </p>

        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.13.	Professional Fees</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>It is advisable to ensure that the reinstatement cost assessment (RCA) makes allowance for fees necessarily and reasonably incurred in the reinstatement or repair of a building. This should relate to the charges of architects, engineers, surveyors, and anyone else whose services are required if the work is to be done satisfactorily. </p>
        <p>Professional fees incurred in the preparation and negotiation of claims settlements, as opposed to reinstatement of damage, are not covered under this heading. </p>
       <p>Professional fees for dealing with reinstatement of damage will often be higher than those for procuring new buildings, and it is advisable to make do allowance for this factor. </p>
        <p>The assessment should allow for local authority planning and Building Regulations costs and also include an allowance for all fees of professionals necessarily incurred in the reinstatement of the building. Depending on the complexity of the building, the list may include surveyors, architects, structural engineers and, in the case of attached buildings, party wall surveyors. </p>
       <p>It is worth carefully considering the level of fees pertaining at the time. It is advisable to regularly update the fee allowance as the economic climate changes. </p>

        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.14.	Public Authorities’ Stipulation</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>Since the purpose of the RCA is to establish the cost of reconstructing a building following total destruction, in as close a form to the original as is permitted by current regulations, then this compliance will form an increasingly significant part of the RCA. RICS members and RICS- regulated firms should incorporate sufficient allowance for compliance with current legislation. It is therefore advisable for RCAs to be constantly reviewed, as inflation indexation is not the only variable. </p>
        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.15.	Value Added Tax</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>We have not shown any provisions or allowances for VAT on rebuilding costs, fees, etc. which in certain countries would attract VAT. It is essential that you consult with your accountants and insurance brokers to advise you of your particular liability in this respect and whether any tax will be payable. </p>

        </td></tr>
</table>
<br><br>
<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>4.16.	Inflation</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>Our assessment of the reinstatement value has been calculated in the light of current prices and conditions but in assessing the actual sum to be insured consideration must also be given to the problem of inflation. Three distinct aspects of inflation must be considered: </p>
        <p style="font-weight: bold; line-height: 0.5px">Policy Year </p>
        <p>In order to avoid unduly frequent reassessments of the current reinstatement value, an allowance must be made for any inflation which is likely to occur during the course of the insured period. This allowance is to provide for the possibility that the building could be damaged, either partially or wholly, by fire on the last day of the insured period, i.e., approximately one year from the date of the Reinstatement Cost Assessment. </p>
        <h4 style="padding: 0">Pre-Contract Period </h4>
        <p>When damage is caused a period of time must be allowed prior to the contractors commencing work on site. This period is required in order to enable the development team to prepare plans and detailed specifications, discuss the rebuilding project with the Local Authorities and to obtain consents and competitive tenders. The repairs must be agreed with the insurers’ loss adjusters and finally the contractors must organize the commencement of the work on receipt of formal instructions to proceed. The period of time which will be required to complete these responsibilities will be dependent upon the extent of damage to the property. For the purpose of this Report, we have assumed the worst scenario and have allowed for a period which would cover the event of total destruction. </p>
            <h4 style="padding: 0">Rebuilding Period </h4><p>In the event of the property being substantially damaged by fire, it is likely that increases in building costs will occur during the actual time taken by the contractor to complete the contract. To avoid a shortfall in the sum insured, allowance should be made for the anticipated inflation during the contract period or alternatively the extra amount, which the contractor would charge for entering into a fixed price contract. We would recommend that the period, which should be provided, be sufficient to allow the complete rebuilding of the property.
            </p>

        </td></tr>
</table>
<br><br>

<br pagebreak="true" />
<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
    05.	Construction Cost Outlook
</div>
<br><br>
<?php
$market_construction_cost =\app\models\WindmillsDocs::find()->where(['file' => 'market_construction_cost'])->orderBy(['created_at' => SORT_DESC])->one();
?>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>5.1.	Market Construction Cost</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>We have taken the construction cost benchmark from 6 (six) quantity surveyor and real estate consultancy reports including:
            </p>
            <p>1.	AECOM</p>
            <p>2.	Linesight</p>
            <p>3.	Turner and Townsend</p>
            <p>4.	Colliers</p>
            <p>5.	Jones Lang LaSalle (JLL)</p>
            <p>6.	Rider Levett Bucknall (RLB)</p>
            <?php if($market_construction_cost<>null){ ?>
            <img src="<?= $market_construction_cost->url ?>"  >
            <?php } ?>

            <p>The construction cost benchmark data has been collected based on their latest official publication related to the projects in the UAE. </p>
        </td></tr>
</table>
<br><br>
<?php
$construction_cost_index =\app\models\WindmillsDocs::find()->where(['file' => 'construction_cost_index'])->orderBy(['created_at' => SORT_DESC])->one();
$construction_cost_index_dubai =\app\models\WindmillsDocs::find()->where(['file' => 'construction_cost_index_dubai'])->orderBy(['created_at' => SORT_DESC])->one();
?>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>5.2.	Construction Cost Index</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>The UAE Tender Price Index (TPI) is AECOM’s assessment of construction tender prices in the UAE. It is compiled by AECOM’s Middle East Business Intelligence team and is based on actual returns of projects. The Index is a measure of average price increases across differing project types and locations across the UAE. It should be regarded as a guide only when looking at any specific project, as the pricing of individual projects will vary depending on factors such as their complexity, location, timescale, etc.</p>
            <?php if($construction_cost_index<>null){ ?>
                <img src="<?= $construction_cost_index->url ?>"  >
            <?php } ?>

            <p>Source: AECOM </p>
            <p>AECOM’s UAE TPI was anticipating a y-o-y change from 2020-2021 of between 1.0 - 1.5 per cent. This slight upward growth considers that although the market is experiencing considerable commodity price hikes and pricing pressures, this is being offset by prudent business costs revisions implemented following the mitigations of the coronavirus pandemic. Generally, construction organizations are operating at lower margins and have been seen to be hedging that markets will subside once pandemic economic restrictions ease.</p>
            <p>Overall increased competitiveness has driven prices to remain subdued, and rate increases are typically sluggish to be reflected in tender returns, as tenderers look to keep historic norms in a bid to be more competitive. As the pandemic restrictions continue globally, tender prices are increasingly under pressure to adjust upwards and is expected to become more prevalent in the first half of 2022. In consideration, AECOM forecasts that the UAE TPI will increase to 1.5 - 3.5 per cent in 2022, as pandemic induced prices hikes continue to be sustained.</p>
       <p>We have researched the trend of construction cost index from Dubai Statistic Centre to obtain the price movement of the construction cost in Dubai over the years. </p>
       <p>Annual percentage change in Construction Cost Index by Building stages By Building stages.</p>
            <p style="text-align: center">Emirate of Dubai 2013 - 2023</p>
            <?php if($construction_cost_index_dubai<>null){ ?>
                <img src="<?= $construction_cost_index_dubai->url ?>"  >
            <?php } ?>
            <p>Source: Dubai Statistics Centre</p>
        </td></tr>
</table>
<br><br>
<br pagebreak="true" />
<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
    06. Subject Property Description
</div>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>6.1.	Location</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><?= $model->inspectProperty->location_remarks; ?>

        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>6.2.	Building</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><?= $model->inspectProperty->building_remarks; ?>

        </td></tr>
</table>
<br><br>

<br pagebreak="true" />
<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
    07. Reistatement Cost Assessment
</div>
<br><br>
<?php
$main_construction_cost_basis =\app\models\WindmillsDocs::find()->where(['file' => 'main_construction_cost_basis'])->orderBy(['created_at' => SORT_DESC])->one();
$main_construction_cost_analysis =\app\models\WindmillsDocs::find()->where(['file' => 'main_construction_cost_analysis'])->orderBy(['created_at' => SORT_DESC])->one();
?>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>7.1.	Main Construction Cost Basis</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>All assessments are made on the appropriate basis as agreed with the client in accordance with the provisions and definitions of the Reinstatement Cost Assessment Standards unless otherwise specifically agreed and stated.</p>
    <p>We have taken development cost benchmarks from 6 (six) published report of construction cost as follow. We have applied the adjustment of construction cost increment based on the latest data from Dubai Statistic Centre (please refer to the appendix 6). </p>
            <?php if($main_construction_cost_basis<>null){ ?>
                <img src="<?= $main_construction_cost_basis->url ?>"  >
            <?php } ?>
            <p>Based on the six-construction cost comparable above, we have analyzed the cost into three different specifications, including standard specification, medium specification and high specification.  </p>
            <?php if($main_construction_cost_analysis<>null){ ?>
                <img src="<?= $main_construction_cost_analysis->url ?>"  >
            <?php } ?>
        </td></tr>
</table>
<br><br>
<br pagebreak="true" />
<?php
$interest_rate_and_financing_schedule =\app\models\WindmillsDocs::find()->where(['file' => 'interest_rate_and_financing_schedule'])->orderBy(['created_at' => SORT_DESC])->one();
?>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>7.2.	Soft Cost</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>We have made some research and comparison for the soft cost component with the following assumptions:</p>
            <?php if($interest_rate_and_financing_schedule<>null){ ?>
                <img src="<?= $interest_rate_and_financing_schedule->url ?>"  >
            <?php } ?>

        </td></tr>
</table>
<br><br>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>7.3.	Interest/Finance Charges</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>We have implemented the interest/finance charges throughout the construction period by using the current interest rate from UAE Central Bank. Details of the finance charges calculation is in the appendix 5</p>

        </td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>7.4.	Reinstatement Cost Summary</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>We have calculated the reinstatement cost based on the building area configuration data provided by the client. We have analyzed each configuration and applied the estimated construction cost within the price range and based on its specification. We have added the soft cost proportion based on the specific calculation as describe in the section 7.2 above. We have implemented the interest/finance charges during the construction period. Details of the reinstatement cost calculation of the subject property are as follow: </p>
        <table border="1" cellpadding="2" >
            <tr>
                <td style="font-size: 13.5px">Project:</td>
                <td><?= $model->building->title; ?></td>
                <td>&nbsp;&nbsp;</td>
                <td>Land Area:</td>
                <td><?= $model->land_size; ?></td>

            </tr>
            <tr>
                <td style="font-size: 13.5px">Address:</td>
                <td><?= $model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?></td>
                <td>&nbsp;&nbsp;</td>
                <td>Building Foot Print Area (BFA):</td>
                <td><?= $model->inspectProperty->building_floor_point_area ?></td>

            </tr>
            <tr>
                <td style="font-size: 13.5px">Inspection Date:</td>
                <td><?= date('l, jS \of F, Y', strtotime($model->scheduleInspection->inspection_date)) ?></td>
                <td>&nbsp;&nbsp;</td>
                <td>Built Up Area (BUA):</td>
                <td><?= $model->inspectProperty->net_built_up_area ?></td>

            </tr>
            <tr>
                <td style="font-size: 13.5px">Assessment Date:</td>
                <td><?= trim( date('l, jS \of F, Y', strtotime($model->scheduleInspection->valuation_date))) ?></td>
                <td>&nbsp;&nbsp;</td>
                <td>Gross Floor Area (GFA):</td>
                <td><?= $model->inspectProperty->gfa ?></td>

            </tr>
            <tr>
                <td style="font-size: 13.5px">Configuration:</td>
                <td>As per configuration details</td>
                <td>&nbsp;&nbsp;</td>
                <td>Saleable Area (GFA):</td>
                <td><?= $model->inspectProperty->sealable_area ?></td>

            </tr>
            <tr>
                <td style="font-size: 13.5px">Typology:</td>
                <td><?= Yii::$app->appHelperFunctions->recostQuality[$model->inspectProperty->typology] ?></td>
                <td>&nbsp;&nbsp;</td>
                <td>Unit Saleable Area + Balcony:</td>
                <td>><?= $model->inspectProperty->unit_sealable_balcony_area ?> </td>

            </tr>
            <tr>
                <td style="font-size: 13.5px">Construction Start Date:</td>
                <td><?= $model->building->year_of_construction ?></td>
                <td>&nbsp;&nbsp;</td>
                <td>Ratail Area:</td>
                <td><?= $model->inspectProperty->retail_area ?></td>

            </tr>
            <tr>
                <td style="font-size: 13.5px">Completion Date:</td>
                <td><?= ($model->building->year_of_construction + $model->inspectProperty->project_development_period) ?></td>
                <td>&nbsp;&nbsp;</td>
                <td>Landscape Area:</td>
                <td><?= $model->inspectProperty->landscape_area ?></td>

            </tr>
            <tr>
                <td style="font-size: 13.5px">Contruction Period(Years):</td>
                <td><?= $model->inspectProperty->project_development_period ?></td>
                <td>&nbsp;&nbsp;</td>
                <td>Uncovered Area:</td>
                <td><?= $model->inspectProperty->covered_area ?></td>

            </tr>


        </table>
            <br pagebreak="true" />

            <table border="1" cellpadding="2">
                <thead>
                <tr style="border: #0e6bff 1px solid;background-color: darkgrey">
                    <td style="padding: 10px !important;" class="toeheading">
                        <strong>Cost Subjects</strong>
                    </td>

                    <td style="padding: 10px !important;" class=" width_25">
                        <strong> Base Factor</strong>
                    </td>
                    <td style="padding: 10px !important;" class=" width_15">
                        <strong>Fee/ Rate</strong>
                    </td>

                    <td style="padding: 10px !important;" class="width_25">
                        <strong>Reinstatment Cost</strong>
                    </td>
                </tr>
                </thead>
                <tbody>


                <tr style="background-color: #D8D8D8">
                    <td colspan="5" style="text-align: left">
                        <strong> Demolition Costs </strong>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Demolition Cost
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['demolition_costs']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['demolition_costs']['fee_rate']; ?> (cost per sm)
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->demolition_costs_values;  ?>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Debris Removal
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['debris_removal']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['debris_removal']['fee_rate']; ?> (cost per sm)
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->debris_removal_values; ?>
                    </td>
                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Total Demolition Cost
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>
                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['total_demolition_costs_values_saved']; ?>
                    </td>

                </tr>


                <tr style="background-color: #D8D8D8">
                    <td colspan="5" style="text-align: left">
                        <strong> Contracting Costs </strong>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Construction Cost
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['contracting_cost']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['contracting_cost']['fee_rate']; ?> (cost per sm)
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->contracting_cost_values; ?>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Basement Parking
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['basement_parking']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['basement_parking']['fee_rate']; ?> (cost per sm)
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->basement_parking_values; ?>
                    </td>
                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Multi Story Parking
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['multi_story_parking']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['multi_story_parking']['fee_rate']; ?> (cost per sm)
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->multi_story_parking_values; ?>
                    </td>
                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Boundry Wall Length
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['boundry_wall']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['boundry_wall']['fee_rate']; ?> (cost per lm)
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->boundry_wall_length_values; ?>
                    </td>
                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Total
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>
                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['total_contracting_cost_values_saved']; ?>
                    </td>

                </tr>




                <tr style="background-color: #D8D8D8">
                    <td colspan="5" style="text-align: left">
                        <strong> Landscapiing/Facilities Costs  </strong>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Site Improvements
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['site_improvements']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['site_improvements']['fee_rate']; ?> (cost per sm)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->site_improvements_values; ?>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Pool
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['pool_costs']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['pool_costs']['fee_rate']; ?> (cost per cm)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->pool_price_values; ?>
                    </td>
                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Facilities
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['facilities_costs']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['facilities_costs']['fee_rate']; ?> (cost per sm)
                    </td>
                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?=  $cost->whitegoods_price_values; ?>
                    </td>
                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Landscaping
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['landscaping_costs']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['landscaping_costs']['fee_rate']; ?> (cost per sm)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->landscape_price_values; ?>
                    </td>
                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Total
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>
                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['total_landscapiing_facilities_values_saved']; ?>
                    </td>

                </tr>






                <tr style="background-color: #D8D8D8">
                    <td colspan="5" style="text-align: left">
                        <strong>  Government Fees</strong>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Land Department/Municipality Fee
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['land_department_municipality_cost']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['land_department_municipality_cost']['fee_rate']; ?> (cost per SF)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->land_department_municipality_fee_values;  ?>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Master Developer Charges
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['master_developer_charges_costs']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['master_developer_charges_costs']['fee_rate']; ?> (%)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->developer_margin_values; ?>
                    </td>
                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Green Building Certification Fee
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['green_building_certification_fee']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['green_building_certification_fee']['fee_rate']; ?> (%)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->green_building_certification_fee_values; ?>
                    </td>
                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Demolition Fee
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['demolition_fee_costs']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['demolition_fee_costs']['fee_rate']; ?> (per unit)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->demolition_fee_values; ?>
                    </td>
                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Total
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>
                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['total_government_fees_values_saved']; ?>
                    </td>

                </tr>



                <tr style="background-color: #D8D8D8">
                    <td colspan="5" style="text-align: left">
                        <strong>   Professional fee</strong>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Consulting/Engineering/Arhitecture Costs
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['consulting_engineering_arhitecture_cost']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['consulting_engineering_arhitecture_cost']['fee_rate']; ?> (%)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->professional_charges_values; ?>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Designing and Architecture Costs
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['designing_and_architecture_costs']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['designing_and_architecture_costs']['fee_rate']; ?> (%)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->designing_and_architecture_costs_values; ?>
                    </td>
                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Project Management Costs
                    </td>



                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['project_management_costs']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['project_management_costs']['fee_rate']; ?> (%)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->project_management_costs_values;  ?>
                    </td>
                </tr>

                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Total
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>
                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['total_professional_fee_values_saved']; ?>
                    </td>

                </tr>
                <tr style="background-color: #D8D8D8">
                    <td colspan="5" style="text-align: left">
                        <strong>  Finance charges</strong>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Loan Amount (<?= $cost_approach['loan_amount'] ?>)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['finance_cost']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['finance_cost']['fee_rate']; ?> (%)
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->interest_rate_values;  ?>
                    </td>

                </tr>
               <!-- <tr style="border: none">
                    <td colspan="5" style="text-align: left">
                        <strong>  &nbsp;</strong>
                    </td>

                </tr >
                <tr style="border: none>
                    <td colspan="5" style="text-align: left">
                        <strong>  &nbsp;</strong>
                    </td>

                </tr>
                <tr style="border: none>
                    <td colspan="5" style="text-align: left">
                        <strong>  &nbsp;</strong>
                    </td>

                </tr>-->
                <tr style="background-color: #D8D8D8">
                    <td colspan="5" style="text-align: left">
                        <strong>  Contingencies</strong>
                    </td>

                </tr>
                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Contingencies
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['contingencies_cost']['factor']; ?>
                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['contingencies_cost']['fee_rate']; ?> (%)
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost->contingency_margin_values; ?>
                    </td>

                </tr>




                <tr id="owner_data_row">
                    <td style="padding-top: 30px!important;">
                        Grand Total Cost
                    </td>


                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>
                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                    </td>

                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                        <?= $cost_approach['grand_total']; ?>
                    </td>

                </tr>


                </tbody>
                <tfoot>
                </tfoot>
            </table>

        </td></tr>
</table>
<br><br>
<br pagebreak="true" />
<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
    08.	Conclusion
</div>
<br><br>
<?php
$signature_image =\app\models\WindmillsDocs::find()->where(['file' => 'signature_image'])->orderBy(['created_at' => SORT_DESC])->one();
?>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td colspan="2" class="detailtext" colspan="2"><p>We have not taken into account any view on the potential risks and/or returns related to the sustainability, maintainability and growth/changes of the sales prices, values and rental income associated with the Property over the life of the loan and/or the Property, other than what is specified in the Reinstatement Cost Assessment report. We have not made any allowance for vendor’s sale costs, and any tax liabilities that may arise upon the disposal of the property or any parts thereof. Our Reinstatement Cost Assessment is expressed as exclusive of VAT that may become chargeable.
                Under the analyses explained above, this conclude the Reinstatement Cost Assessment for the subject properties carried out by Windmills, subject to the assumptions, limitations, exclusions, that has been stipulated in the report.
                This concludes the Reinstatement Cost Assessment for the subject property carried out by Windmills Real Estate Valuation Services LLC. If there is any matter contained within this report that you would like to discuss further, please do not hesitate to contact us.

            </p>
            <br>
            <p>Yours Sincerely,</p>
            <?php if($signature_image<>null){ ?>
                <img src="<?= $signature_image->url ?>" width="500"  >
            <?php } ?>


        </td></tr>
    <tr >

                <td style="border-right: 0.5px solid black; border-top: 0.5px solid black;">
                    <p>Laxmi Narayana Kumar MRICS </p>
                    <p>RICS and RERA Registered Valuer  </p>
                    <p>Member of Royal Institution of Chartered Surveyors  </p>
                    <p>RICS Membership No. 6794407  </p>
                    <p>RERA Registered No. 42744  </p>
                    <p>(Civil Engineering). Master of Business Administration  </p>
                    <p>Windmills Real Estate Valuation Services, L.L.C.  </p>
                </td>
                <td style=" border-top: 0.5px solid black;" >
                    <p>Febrian Rosyadi MRICS </p>
                    <p>RICS and RERA Registered Valuer  </p>
                    <p>Member of Royal Institution of Chartered  </p>
                    <p>Surveyors (MRICS), BASc (Asset
                        Management), MSc. (Real Estate)
                    </p>
                    <p>RICS Membership No. 6538314  </p>
                    <p>RERA Registered No. 42744 </p>
                    <p>Windmills Real Estate Valuation Services, L.L.C.  </p>

                </td>

    </tr>
</table>
<br pagebreak="true" />