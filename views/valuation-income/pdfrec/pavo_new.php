
<?php

// This is for 1.05 Valuation Instructions From.
$instructorname=Yii::$app->PdfHelper->getUserInformation($model);



//1.14  Total Building Floor(s)
if($model->property->title == 'Villa') {
    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1 ) {
        $total_building_floors = "Basement + " .Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
    }else if($model->inspectProperty->number_of_basement > 1){
        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }else{
        $total_building_floors = Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
    }

}else {
    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1) {
        $total_building_floors = "Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    } else if($model->inspectProperty->number_of_basement > 1){
        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }else {
        $total_building_floors = "Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }
}
$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();

$conflict= \app\models\ValuationConflict::find()->where(['valuation_id'=>$model->id])->one();

$owners_in_valuation = \yii\helpers\ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $model->id])->all(), 'id', 'name');
$woners_name = "";
if(!empty($owners_in_valuation)) {
    $woners_name = implode(", ", $owners_in_valuation);
}

$tower_data = \app\models\ReTowersData::find()->where(['valuation_id' => $model->id])->all();

//  1.36  View Type
$ViewType = Yii::$app->PdfHelper->getViewType($model);



// 1.39 Building/Community Facilities
$model->inspectProperty->other_facilities = explode(',', ($model->inspectProperty->other_facilities <> null) ? $model->inspectProperty->other_facilities : "");
foreach ($model->inspectProperty->other_facilities as $key => $value) {
    $other_facilitie= \app\models\OtherFacilities::find()->where(['id'=>$value])->one();
    if ($other_facilities!=null) {  $other_facilities.=', '.$other_facilitie->title;  }
    else { $other_facilities.=$other_facilitie->title;   }
}



// 1.66 Documents Provided by Client , 1.67 Documents not Provided
$DocumentByClient = Yii::$app->PdfHelper->getDocumentByClient($model);


$DocumentByClient['documentAvail'] = rtrim($DocumentByClient['documentAvail'], "<br>");
$DocumentByClient['documentNotavail'] = rtrim($DocumentByClient['documentNotavail'], "<br>");


$plot_number = ($model->plot_number <> null && $model->plot_number != '0')? $model->plot_number: 'Not Applicable';
$source_bua =($model->inspectProperty->bua_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->bua_source] . ')': '';
$source_plot_size =($model->inspectProperty->plot_area_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->plot_area_source]. ')': '';
$source_extension =($model->inspectProperty->extension_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->extension_source]. ')': '';

$plot_size = ($model->land_size > 0)? $model->land_size.' square feet'.$source_plot_size: 'Not Applicable';


$otherInstructingPerson='';
$result = \app\models\User::find()->where(['id'=>$model->other_instructing_person])->one();
if ($result<>null) {
    $otherInstructingPerson = $result->firstname.' '.$result->lastname;
}
// print_r($otherInstructingPerson); die();
?>



    <table cellspacing="1" cellpadding="2" style="border: 1px dashed #64B5F6;" class="col-12">
        <tr>
            <td colspan="7" class="tableSecondHeading" style="color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;"><h5>EXECUTIVE VALUATION REPORT SUMMARY</h5></td>
        </tr>

        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> A. VALUATION OVERVIEW</h5></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Client’s Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->client->title ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Client’s Customer Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->client_name_passport ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Intended User(s)</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= ( $model->other_intended_users <> null) ? $model->other_intended_users : 'None' ?></td>
        </tr>
       <!-- <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Valuation Date</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php /*echo trim( date('l, jS \of F, Y', strtotime($model->scheduleInspection->valuation_date))) */?></td>
        </tr>-->
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;">Scope of Work</td><td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td><?php if($model->purpose_of_valuation == 3){ ?><td  colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray">Estimated Fair Value of the Subject Property</td>
            <?php } else{ ?>
                <td  colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?=  Yii::$app->appHelperFunctions->valuationScopeArr[$model->valuation_scope] ?></td>
            <?php } ?>

        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;">Purpose of Assessment</td><td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td><td  colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray">Reinstatement cost for Insurance Purpose to comply with local legislation and international best practice. </td>



        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"></td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray"></td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"></td>
        </tr>

        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> B. Property Description - Location</h5></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Property Valued</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].' '.$model->property->title ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Project/Building Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->building->title ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Plot Number</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->plot_number ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Sub-Community Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->building->subCommunities->title ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Community Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->building->communities->title ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> City and Country</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>, United Arab Emirates</td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"></td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray"></td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"></td>
        </tr>


        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> C. Property Description -External</h5></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Tenure</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Plot Size</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $plot_size ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Built-Up Area (BUA)</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->inspectProperty->net_built_up_area ?> square feet <?= $source_bua ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Gross Floor Area (GFA)</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->inspectProperty->gfa ?> square feet <?= $source_bua ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Number of Towers</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->no_of_towers ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Number of Units</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->unit_number ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Building Age</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php if($model->client->id == 1 && $model->inspectProperty->estimated_remaining_life > 0){ ?><?= ($model->inspectProperty->estimated_remaining_life - 1) . ' - '. ($model->inspectProperty->estimated_remaining_life + 1) ?> Years<?php  }else{ ?><?= round($model->inspectProperty->estimated_remaining_life) ?> Years<?php } ?></td>
        </tr>

        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"></td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray"></td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"></td>
        </tr>


        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> D. Valuation Details</h5></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Inspection Type</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type] ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Inspection Date</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php if($model->inspection_type == 3){
                    ?>
                    <?='Not Applicable as desktop valuation '; ?>
                <?php }
                else{?><?php echo date('l, jS \of F, Y', strtotime($model->scheduleInspection->inspection_date)) ?>
                    <?php //echo Yii::$app->formatter->asDate($model->scheduleInspection->inspection_date,Yii::$app->params['fulldaydate']) ?>
                <?php } ?>
            </td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Date of Assessment</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php echo trim( date('l, jS \of F, Y', strtotime($model->scheduleInspection->valuation_date))) ?></td>
        </tr>


        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> E.Reinstatement Cost</h5></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Reinstatement Cost</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:10; width:293px;"  class="bggray"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Reinstatement Cost Rate</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot': "Not Applicable" ?></td>
        </tr>
    </table>
    <br><br>





<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">

    1.Valuation Overview

</div>
<br>
<table class="mx-auto col-12" cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;">

    <tr><td class="tableSecondHeading" colspan="2"><h3>General Details</h3></td></tr>


    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.01 Windmills Reference</td>
        <td style="color:#212121; font-size:12;"><?= $model->reference_number ?></td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.02 Client Reference </td>
        <td  style="color:#212121; font-size:12;"><?= $model->client_reference ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.03 Client’s Full Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->client->title ?></td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.04 Intended User(s) Name</td>
        <td  style="color:#212121; font-size:12;"><?= ( $model->other_intended_users <> null) ? $model->other_intended_users : 'None' ?></td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.05 Valuation Instructions From</td>
        <td  style="color:#212121; font-size:12;"><?= ($otherInstructingPerson <> null) ? $otherInstructingPerson:$instructorname ; //$instructorname ?></td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.06 Valuation Instructions Date</td>
        <td  style="color:#212121; font-size:12;"><?=  date('l, jS \of F, Y', strtotime($model->instruction_date)) ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.07 Valuation Scope</td>

        <?php if($model->purpose_of_valuation == 3){ ?>
            <td  style="color:#212121; font-size:12;">Estimated Fair Value of the Subject Property</td>
        <?php } else{ ?>
            <td  style="color:#212121; font-size:12;"><?=  Yii::$app->appHelperFunctions->valuationScopeArr[$model->valuation_scope] ?></td>
        <?php } ?>


        <!--<td  style="color:#212121; font-size:12;"><?/*=  Yii::$app->appHelperFunctions->valuationScopeArr[$model->valuation_scope] */?></td>-->
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.08 Owner’s Full Name</td>
        <td  style="color:#212121; font-size:12;"><?= $woners_name ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.09 Client’s Customer Full Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->client_name_passport ?></td>
    </tr >
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.10.	Purpose of the Assessment</td>
        <td  style="color:#212121; font-size:12;">Reinstatement cost for Insurance Purpose to comply with local legislation and international best practice</td>
    </tr>

    <?php if($model->costDetails->original_purchase_price <> null && $model->costDetails->original_purchase_price != "0.00") { ?>

        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.15 Original Purchase Price Rate</td>
            <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->original_purchase_price <> null && $model->costDetails->original_purchase_price != "0.00") ? "AED ".number_format((float)($model->costDetails->original_purchase_price/$model->inspectProperty->net_built_up_area), 2, '.', '')." per square foot" : "Not Applicable" ?>  </td>
        </tr>
        <tr >
            <td  style="color:#0277BD; font-size:13;">1.16 Source of Original Purchase Price</td>
            <td  style="color:#212121; font-size:12;"><?=  ($model->costDetails->source_of_original_date_price <> null) ? Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->costDetails->source_of_original_date_price] : "Not Applicable" ?></td>
        </tr>
        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.17 Date of Original Purchase Price</td>
            <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->date_of_original_date_price <> null) ? date('l, jS \of F, Y', strtotime($model->costDetails->date_of_original_date_price)) : "Not Applicable"?> </td>
        </tr>
    <?php  }else{ ?>
        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.15 Original Purchase Price Rate</td>
            <td  style="color:#212121; font-size:12;">Not Applicable </td>
        </tr>
        <tr >
            <td  style="color:#0277BD; font-size:13;">1.16 Source of Original Purchase Price</td>
            <td  style="color:#212121; font-size:12;">Not Applicable</td>
        </tr>
        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.17 Date of Original Purchase Price</td>
            <td  style="color:#212121; font-size:12;">Not Applicable </td>
        </tr>
    <?php  } ?>
</table>

<br pagebreak="true" />

<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;" class="col-12">

    <tr><td class="tableSecondHeading" colspan="2"><h3>Property Description - Location</h3></td></tr>

    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.18 Property Studied</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].' '.$model->property->title ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.19 Property Use</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category] ?>
        </td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.20 Total Building Floor(s)</td>
        <td  style="color:#212121; font-size:12;">
            <?php
                foreach ($tower_data as $tower_datum){
                    echo $tower_datum->title.'<br>';
                }

                ?>
        </td>
    </tr>


    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.22 Property Unit Number</td>
        <td  style="color:#212121; font-size:12;"><?= $model->unit_number ?></td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.23 Building No.</td>
        <td  style="color:#212121; font-size:12;"><?= $model->building_number ?></td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.24 Project Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->building->title ?></td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.25 Street Number/Name</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not Applicable as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->street ?></td>
    </tr>
    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.26 Plot Number</td>
        <td style="color:#212121; font-size:12;"><?= $plot_number ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.27  Makani Number</td>
        <td  style="color:#212121; font-size:12;">
<?php
            foreach ($tower_data as $tower_datum){
            echo $tower_datum->makani_number.'<br>';
            }
            ?>



        </td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.28 Sub Community Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->building->subCommunities->title ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.29 Community Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->building->communities->title ?></td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.30 City and Country Name</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>, United Arab Emirates</td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.31 Location Characteristics</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><b>Good location</b>
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_highway_drive) ?> drive to highway
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_school_drive) ?> to school
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_mall_drive) ?> to commercial mall
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_sea_drive) ?> to special landmark
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_park_drive) ?> drive to pool/park
        </td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.32 Location Coordinates</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not Applicable as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->latitude.','.$model->inspectProperty->longitude ?> (as per Google Maps)</td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.33 Placement (Middle / Corner) </td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= Yii::$app->appHelperFunctions->propertyPlacementListArr[$model->inspectProperty->property_placement] ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.34 Exposure (Single Row / Back)</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= Yii::$app->appHelperFunctions->propertyExposureListArr[$model->inspectProperty->property_exposure] ?>
        </td>
    </tr>
</table>



<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;" class="col-12">

    <tr><td class="tableSecondHeading" colspan="2"><h3>Property Description - External</h3></td></tr>

    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.35  Property Type (1E, 2M etc.)</td>
        <td style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->listing_property_type ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.36 Development (Standard/Non)</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->development_type ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.37 Tenure (FH/NFH/Leasehold)</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
    </tr>

    <tr>
        <td style="color:#0277BD; font-size:13;">1.38 Completion Status</td>
        <td style="color:#212121; font-size:12;"><?= ($model->inspectProperty->completion_status)==100 ? 'Yes' : 'No'; ?>
        </td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.39 Completion Percentage</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->completion_status ?></td>
    </tr>

    <tr >
        <td style="color:#0277BD; font-size:13;">1.40 Property Completion Year</td>
        <td style="color:#212121; font-size:12;"><?= $model->building->year_of_construction ?> </td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.41 Estimate Age (in years)</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= round($model->inspectProperty->estimated_age).' Years' ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.42 Estimated Remaining Life (in years)</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->client->id == 1 && $model->inspectProperty->estimated_remaining_life > 0){ ?><?= ($model->inspectProperty->estimated_remaining_life - 1) . ' - '. ($model->inspectProperty->estimated_remaining_life + 1) ?> Years<?php  }else{ ?><?= round($model->inspectProperty->estimated_remaining_life) ?> Years<?php } ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.43  Plot Size (in square feet)</td>
        <td  style="color:#212121; font-size:12;"><?= $plot_size ?></td>
    </tr>


    <tr>
        <td style="color:#0277BD; font-size:13;">1.44 Built Up Area (in square feet)</td>
        <td style="color:#212121; font-size:12;"><?= $model->inspectProperty->net_built_up_area ?> square feet <?= $source_bua ?></td>
    </tr>


    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.45 View Type</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?= $ViewType['viewCommunity'].$ViewType['viewPool'].$ViewType['viewBurj'].$ViewType['viewSea'].$ViewType['viewMarina'].$ViewType['h'].$ViewType['viewLake'].$ViewType['viewGolfCourse'].$ViewType['viewPark'].$ViewType['viewSpecial'] ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.46 Landscaping Details</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->landscaping ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.47 Parking Spaces</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->parking_floors ?></td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.48  Building/Community Facilities</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $other_facilities ?></td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.49 Developer’s Full Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->developer->title ?></td>
    </tr>

</table>


<br pagebreak="true" />


<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;" class="col-12">
    <tr><td class="tableSecondHeading" colspan="2"><h3><?= trim('Green Efficient Certification') ?></h3></td></tr>

    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.50 Green Efficient Certification</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->greenEfficientCertificationArr[$model->inspectProperty->green_efficient_certification] ?></td>
    </tr>
    <tr>
        <td style="color:#0277BD; font-size:13;">1.51 Certifier Name</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->certifierNameArr[$model->inspectProperty->certifier_name] ?></td>
    </tr>
    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.52 Certification Level</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->certificationLevelArr[$model->inspectProperty->certification_level] ?></td>
    </tr>
    <tr>
        <td style="color:#0277BD; font-size:13;">1.53 Source of Green Certificate Information</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->sGCInformationArr[$model->inspectProperty->source_of_green_certificate_information] ?></td>
    </tr>

</table>
<br><br>


<table cellspacing="1" cellpadding="6" style="border: 1px dashed #64B5F6;" class="col-12">

    <tr><td class="tableSecondHeading" colspan="2"><h3>Property Description – Internal</h3></td></tr>


    <tr>
        <td style="color:#0277BD; font-size:13;">1.54 Accommodation</td>
        <td style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->no_of_bedrooms ?> Bedrooms</td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.55 Basement Floor Configuration</td>

        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= ($floorConfig['basementTitle'] <> null) ? rtrim($floorConfig['basementTitle'],"<br>") : "Not Applicable"; ?> </td>
    </tr>


        <tr>
            <td  style="color:#0277BD; font-size:13;">1.56  Floor Configuration</td>
            <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?>

                <table>
                    <thead>
                    <th>Towers</th>
                    <th>Floor Configuration</th>
                    <th>Number of Units</th>
                    </thead>
                    <tbody>
                    <?php foreach ($tower_data as $tower_datum){ ?>
                        <tr><?= $tower_datum->title ?></tr>
                        <tr><?= $tower_datum->floor_details ?></tr>
                        <tr><?= $tower_datum->unit_number ?></tr>

                    <?php } ?>

                    </tbody>

                </table>



            </td>
        </tr>





    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.59 Upgrade Details</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?= ($floorConfig['upgrade_text'] <> null) ? $floorConfig['upgrade_text'] : "Not Applicable" ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.60 Extensions Details</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= ($model->inspectProperty->extension <> null)? $model->inspectProperty->extension.$source_extension:"None" ?></td>
    </tr>


    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.61 Property Condition/Quality</td>
        <td style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?= Yii::$app->appHelperFunctions->propertyConditionListArr[$model->inspectProperty->property_condition] ?></td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.62 Property Defect/s</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->getPropertyDefectsArr()[$model->inspectProperty->property_defect] ?></td>
    </tr>

    <tr  class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.63 Furnished</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?=  Yii::$app->appHelperFunctions->reportAttributesValuefurnished[$model->inspectProperty->furnished]   ?></td>
    </tr>




    <tr>
        <td  style="color:#0277BD; font-size:13;">1.64 Swimming Pool </td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->pool ?></td>
    </tr>
    <tr  class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.65 Cooker Installed</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?= Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->cooker] ?></td>
    </tr>

    <tr >
        <td  style="color:#0277BD; font-size:13;">1.66 Oven Installed</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?=  Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->oven] ?></td>
    </tr>
    <tr  class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.67 Fridge Installed</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?= Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->fridge]  ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.68 Washing Machine Installed</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?=  Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->washing_machine] ?></td>
    </tr>
    <?php
    if(!empty($model->inspectProperty->ac_type)){
        $actypes = explode(',', ($model->inspectProperty->ac_type <> null) ? $model->inspectProperty->ac_type : "");
    }
    ?>
    <tr  class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.69 Central Air-conditioning</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php }

            if($actypes <> null && !empty($actypes)){
                if (in_array(3, $actypes))
                {
                    echo "Yes";
                }else{
                    echo "No";
                }
            }else{
                echo "No";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.70 Split Air-conditioning Units</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php }
            if($actypes <> null && !empty($actypes)){
                if (in_array(1, $actypes))
                {
                    echo "Yes";
                }else{
                    echo "No";
                }
            }else{
                echo "No";
            }


            ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.71 Window Air-conditioning Unit</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php }

            if($actypes <> null && !empty($actypes)){
                if (in_array(2, $actypes))
                {
                    echo "Yes";
                }else{
                    echo "No";
                }
            }else{
                echo "No";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.72 Utilities connected</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->utilities_connected ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.73 Tenancies / Occupancy Status</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->occupancy_status ?></td>
    </tr>
</table>


<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;" class="col-12">
    <tr><td class="tableSecondHeading" colspan="2"><h3>Valuation Details</h3></td></tr>


    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.74 Inspection Type Instructed</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type] ?></td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.75 Purpose of the Valuation</td>
        <td  style="color:#212121; font-size:12;">Reinstatement cost for Insurance Purpose to comply with local legislation and international best practice.</td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.76 Inspecting Officer and Surveying Team</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){ ?> <?='Not Applicable as desktop valuation '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation '; ?>
            <?php }else{ ?><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->scheduleInspection->inspection_officer] ?>
                – Civil Engineer & Valuer
            <?php } ?>
        </td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.77 Service Officer</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->service_officer_name] ?>

        </td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.78 Inspection Date</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not Applicable as desktop valuation '; ?>
            <?php }else{?><?= Yii::$app->formatter->asDate($model->scheduleInspection->inspection_date,Yii::$app->params['fulldaydate']) ?>
            <?php } ?>
        </td>
    </tr>
    <tr >

        <td  style="color:#0277BD; font-size:13;">1.79 Valuation Date</td>
        <td  style="color:#212121; font-size:12;"><?= trim(Yii::$app->formatter->asDate($model->scheduleInspection->valuation_date,Yii::$app->params['fulldaydate'])) ?></td>
    </tr>

    <tr  class="bggray">
        <td style="color:#0277BD; font-size:13;">1.80 Documents Provided </td>
        <td style="color:#212121; font-size:12;"><?= $DocumentByClient['documentAvail'] ?></td>
    </tr>
    <tr >
        <td style="color:#0277BD; font-size:13;">1.81 Documents not Provided </td>
        <td style="color:#212121; font-size:12;"><?= $DocumentByClient['documentNotavail'] ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.82 Basis of Value</td>
        <?php if($model->purpose_of_valuation == 3){ ?>
            <td  style="color:#212121; font-size:12;">Fair Value</td>
        <?php } else{ ?>
            <td  style="color:#212121; font-size:12;"><?= $model->property->basis_of_value ?></td>
        <?php } ?>

        <!--<td  style="color:#212121; font-size:12;"><?/*= $model->property->basis_of_value */?></td>-->
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.83 Valuation Approach</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach] ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.84 Approach Reasoning</td>
        <td  style="color:#212121; font-size:12;"><?= $model->property->approach_reason ?></td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.85 Valuation Adjustments</td>
        <td  style="color:#212121; font-size:12;">Size, Floor, Quality, Row, View, Placement, Listing Date and Sales Negotiation Discount.</td>
    </tr>
</table>
<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;" class="col-12">
    <tr><td class="tableSecondHeading" colspan="2"><h3><?= $market ?> Value</h3></td></tr>




    <tr>
        <td style="color:#0277BD; font-size:13;">1.86 Current Replacement Cost</td>


        <td style="color:#212121; font-size:12;"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
    </tr>

</table>