

<?php
//Google Map
$lon=  $model->inspectProperty->longitude;
$lan = $model->inspectProperty->latitude;
//$map_image_src = "https://maps.googleapis.com/maps/api/staticmap?zoom=11&size=900x300&maptype=hybrid&markers=color:red%7Clabel:B%7C".$lan.",".$lon."&key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs";
//$Sateliteimage_src="https://maps.googleapis.com/maps/api/staticmap?zoom=11&size=900x300&maptype=roadmap&markers=color:red%7Clabel:B%7C".$lan.",".$lon."&key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs";
$img_map = 'map/'.$model->id.'_m.png';
$img_satelite = 'map/'.$model->id.'_s.png';
if(!file_exists($img_map)){
    $map_image_src_url = "https://maps.googleapis.com/maps/api/staticmap?zoom=11&size=900x300&maptype=hybrid&markers=color:red%7Clabel:B%7C".trim($lan).",".trim($lon)."&key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs";
    file_put_contents($img_map, file_get_contents($map_image_src_url));
    $map_image_src = $img_map;
}else{
    $map_image_src = $img_map;
}
if(!file_exists($img_satelite)){
    $Sateliteimage_src_url="https://maps.googleapis.com/maps/api/staticmap?zoom=11&size=900x300&maptype=roadmap&markers=color:red%7Clabel:B%7C".trim($lan).",".trim($lon)."&key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs";
    file_put_contents($img_satelite, file_get_contents($Sateliteimage_src_url));
    $Sateliteimage_src = $img_satelite;
}else{
    $Sateliteimage_src = $img_satelite;
}
if($model->client->id == 183 || $model->id >12254){
    $appendices_number = '2';
}else {
    $appendices_number = '5';
}
?>
<br pagebreak="true" />
<div  style=" color:#E65100;
       text-align: left;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
    0<?=$appendices_number?>: Appendices
</div>

</div>
<div class="col mx-auto">
    <table class="mx-auto">
        <br>
        <tr>
            <td>
                <img src="<?= $map_image_src ?>" style=" display: block;
    margin: 0 auto;">
            </td>
        </tr>
        <br>
        <tr>
            <td>
                <img src="<?= $Sateliteimage_src ?>" style=" display: block;
    margin: 0 auto;">
            </td>
        </tr>
    </table>
    <?php if($model->inspection_type != 3){?>
        <br pagebreak="true" />
        <table class="mx-auto">
            <tr>
                <td class="table_of_content">
                    <h3><?= $appendices_number; ?>.02: Property Photos</h3>
                </td>
            </tr>
        </table>
        <table class="mx-auto" cellspacing="1" cellpadding="1" class="main-class" style="font-size:12px;">
            <tr>

                <?php


                $configuration = \app\models\ConfigurationFiles::find()->select(['type','attachment','custom_field_id'])->where(['valuation_id' => $model->id, "checked_image"=>'on'])->asArray()->all();

                $n=1; $j=1;
                // echo "<pre>";
                // print_r($configuration);
                // echo "</pre>";
                //
                // die();
                foreach ($configuration as $key => $value) {
                    $attachment_type = $value['type'];
                if($value['type'] == 'custom_fields'){
                    $custom_attachment= \app\models\CustomAttachements::find()->where(['id' => $value['custom_field_id']])->one();
                    $value['type'] = $custom_attachment->name;
                }
                if ($value['attachment']!=null && $value['type'] !='') {

                $title = str_replace("_", " ", str_replace("config"," ",$value['type']));
                $title=ucfirst(trim($title));
                if($attachment_type != 'custom_fields') {
                    $title = rtrim($title, "s ");
                }

                ?>

                <td
                        style="color:#0D47A1;
            font-size:16px;
            font-weight:bold;
            text-align:center;">
                   <!-- <h4><?/*= ($title.' No '.($i+1)) */?></h4>-->
                    <h4><?= ($title); ?></h4>
                    <img src="<?= $value['attachment'] ?>"  width="240px" height="220px" style=" display: block; margin: 0 auto;"></td>


                <?php
                $n++;
                if ($n==3) {
                $n=1;
                ?>
            </tr><tr collapse='2'><td></td></tr><tr>
                <!-- </table>  -->

                <?php
                }
                //      $j++;
                // if ($j==3) {
                //     $j=1;
                ?>
                <!-- <br pagebreak="true" /> -->
                <?php  //}   ?>

                <!-- <br><br>
                  <table cellspacing="1" cellpadding="5" class="main-class mx-auto" style="font-size:12px;">
                  <tr> -->

                <?php   //}
                }
                }


                ?>




            </tr>
        </table>
        <br><br><br>

    <?php } ?>

</div>
