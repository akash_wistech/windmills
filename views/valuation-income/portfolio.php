<?php

use app\models\CrmReceivedProperties;


//same as TOE
$discount = 0;
$discount_p = 0;
$VAT =0;
$discount_quotations = 0;


?>




<style>
    .box1{
        background-color: #e6e6e6;
        width: 100px;
        padding-top: 20px !important;
      /*  border-right: 5px white;*/
       /* border-left: 5px white;*/
    }

    .border {
        width: 100%;
      /*  border-top-width: 0.05px;
        border-bottom-width: 0.05px;
        border-left-width: 0.05px;
        border-right-width: 0.05px;*/
        padding: 5px;
    }
    .font_color{
        color: #3763ae;
    }

</style>


<table class="" style="padding-bottom: 2px;" >

    <tr>
        <td colspan="8" class="detailtexttext first-section" >
            <h2>Summary of Values</h2>
            <p>Below is the summarized valuation for each individual property. These figures represent a concise synthesis of our detailed assessment contained in the individual Valuation Report of each property.
                Please  note that these summarized values are part of a broader analysis, and for comprehensive understanding, they should be considered in the context of the of the full individual reports which include in-depth evaluations and justifications for each valuation:
            </p>
        </td>
    </tr>

</table>


<table style="border: solid 1px black;"   cellspacing="1" cellpadding="3" width="800">


    <tr  style="color:#3763ae ">
        <td class="airal-family" style=" border-right: solid 1px black;font-size:9px; font-weight: bold;width:10%">WM Reference</td>
        <td class="airal-family" style=" border-right: solid 1px black; font-size:9px; font-weight: bold; width:10%;">Property Description</td>
        <td class="airal-family" style=" border-right: solid 1px black;font-size:9px; font-weight: bold; width:10% ">Floor No.</td>
        <td class="airal-family" style=" border-right: solid 1px black;font-size:9px; font-weight: bold; ;width:10%">Unit No.</td>
        <td class="airal-family" style=" border-right: solid 1px black;font-size:9px; font-weight: bold; width:10% ">City</td>
        <td class="airal-family" style=" border-right: solid 1px black;font-size:9px; font-weight: bold; width:10% ">Land Size in Sqft</td>
        <td class="airal-family" style=" border-right: solid 1px black;font-size:9px; font-weight: bold; width:10% ">Unit Size in Sqft(BUA)</td>
        <td class="airal-family" style=" border-right: solid 1px black;font-size:9px; font-weight: bold; width:10% ">Market Value (AED)</td>
        <td class="airal-family" style=" border-right: solid 1px black;font-size:9px; font-weight: bold; width:10% ">Market Value Per Sqft (AED)</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:10% ">Market Rent (AED)</td>
    </tr>
</table>
<table   cellpadding="3" width="800" style=" padding: 5px;border: solid 1px black;">
    <tbody>
    <?php
    $i=1;
    if($valuations <> null && !empty($valuations)) {
    $market_value = 0;
    $market_rent = 0;
    foreach($valuations as $key => $valuation){
        $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $valuation->id])->one();
        $approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $valuation->id, 'approver_type' => 'approver'])->one();
       if(in_array($valuation->property_id, [1,12,37,28,17])){
            $floor_text = "Ground floor";
        }else{
            $floor_text = "Not Applicable";
        }
        $ViewType = Yii::$app->PdfHelper->getViewType($model);

        ?>
        <tr style="background-color: ;">
            <td  style="border: solid 1px black;font-size:8.5px;width:10%">&nbsp;<?= $valuation->reference_number; ?></td>
            <td  style="border: solid 1px black;font-size:8.5px;width:10%"><?= $valuation->building->title; ?></td>
            <td  style=" border: solid 1px black;font-size:9px; width:10%"><?= ($valuation->floor_number > 0)?  $valuation->floor_number: $floor_text?></td>
            <td  style=" border: solid 1px black;font-size:9px; width:10%"><?= $valuation->unit_number ?></td>
            <td  style="border: solid 1px black;font-size:9px;width:10% "><?= Yii::$app->appHelperFunctions->emiratedListArr[$valuation->building->city]; ?></td>
            <td class="tbl-body" style="border: solid 1px black;font-size:9px; text-align:right;width: 10%"><?= $valuation->land_size ?></td>
            <td class="tbl-body" style="border: solid 1px black;font-size:9px; text-align:right;width: 10%"><?= Yii::$app->appHelperFunctions->wmFormate($valuation->inspectProperty->net_built_up_area) ?></td>
            <td class="tbl-body" style="border: solid 1px black;font-size:9px; text-align:right;width: 10%"><?= Yii::$app->appHelperFunctions->wmFormate($approver_data->estimated_market_value) ?></td>
            <td class="tbl-body" style="border: solid 1px black;font-size:9px; text-align:right;width: 10%"><?= Yii::$app->appHelperFunctions->wmFormate($approver_data->estimated_market_value_sqf) ?></td>
            <td class="tbl-body" style="border: solid 1px black;font-size:9px; text-align:right;width: 10%"><?= Yii::$app->appHelperFunctions->wmFormate($approver_data->estimated_market_rent) ?></td>

        </tr>



        <?php
        $market_value = $market_value + $approver_data->estimated_market_value;
        $market_rent = $market_rent + $approver_data->estimated_market_rent;
      }
    }
    ?>
    <tr style="background-color: ;">
        <td  style="font-size:8.5px;width:10%"></td>
        <td  style="font-size:8.5px;width:10%"></td>
        <td  style=" font-size:9px; width:10%"></td>
        <td  style=" font-size:9px; width:10%"></td>
        <td  style="font-size:9px;width:10% "></td>
        <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"></td>
        <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"></td>
        <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"></td>
        <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"></td>
        <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"></td>

    </tr>
    <tr style="background-color: ;">
        <td  style="font-size:8.5px;width:10%"></td>
        <td  style="font-size:8.5px;width:10%"></td>
        <td  style=" font-size:9px; width:10%"></td>
        <td  style=" font-size:9px; width:10%"></td>
        <td  style="font-size:9px;width:10% "></td>
        <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"><b>Total</b></td>
        <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"></td>
        <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"><?= Yii::$app->appHelperFunctions->wmFormate($market_value) ?></td>
        <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"></td>
        <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"><?= Yii::$app->appHelperFunctions->wmFormate($market_rent) ?></td>

    </tr>
    </tbody>
</table>
<br><br>




<style>
    .airal-family{
        font-family: Arial, Helvetica, sans-serif;
    }
</style>


<style>

    .text-dec{
        font-size: 8px;
        text-align: center;
        padding-right: 10px;
    }

    /*td.detailtext{*/
    /*background-color:#BDBDBD; */
    /*font-size:9px;*/
    /*border-top: 0.4px solid grey;*/
    /*}*/

    td.detailtexttext{
        /* background-color:#EEEEEE; */
        font-size:10px;
        font-weight: 100;

    }
    th.detailtext{
        /* background-color:#BDBDBD; */
        font-size:9px;
       /*// border-top: 1px solid black;*/
    }
    /*.border {
    border: 1px solid black;
    }*/

    th.amount-heading{
        color: #039BE5;
    }
    td.amount-heading{
        color: #039BE5;
    }
    th.total-due{
        font-size: 16px;
    }
    td.total-due{
        font-size: 16px;
    }
    span.size-8{
        font-size: 10px;
        font-weight: bold;
    }
    td.first-section{
        padding-left: 10px;
        /* background-color: black; */
    }
</style>

