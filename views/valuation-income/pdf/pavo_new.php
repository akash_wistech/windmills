
<?php

// This is for 1.05 Valuation Instructions From.
$instructorname=Yii::$app->PdfHelper->getUserInformation($model);


//1.14  Total Building Floor(s)
if($model->property->title == 'Villa') {
    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1 ) {
        $total_building_floors = "Basement + " .Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
    }else if($model->inspectProperty->number_of_basement > 1){
        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }else{
        $total_building_floors = Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
    }

}else {
    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1) {
        $total_building_floors = "Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    } else if($model->inspectProperty->number_of_basement > 1){
        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }else {
        $total_building_floors = "Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }
}
$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();

$conflict= \app\models\ValuationConflict::find()->where(['valuation_id'=>$model->id])->one();
$owners= \app\models\ValuationOwners::find()->where(['valuation_id'=>$model->id])->all();
$woners_name = "";
foreach ($owners as $record){
    $woners_name .= $record->name." ".$record->lastname.", ";
}
/*
$owners_in_valuation = \yii\helpers\ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $model->id])->all(), 'id', 'name');
//$owners_in_valuation = \yii\helpers\ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $model->id])->all(), 'id', 'CONCAT(name," ",lastname) as name');
$woners_name = "";
if(!empty($owners_in_valuation)) {
    $woners_name = implode(", ", $owners_in_valuation);
}*/



//  1.36  View Type
$ViewType = Yii::$app->PdfHelper->getViewType($model);

// echo "<pre>";
// print_r($ViewType);
// echo "<pre>";
// die();


// print_r($model->inspectProperty->other_facilities );
// die();

// 1.39 Building/Community Facilities
$model->inspectProperty->other_facilities = explode(',', ($model->inspectProperty->other_facilities <> null) ? $model->inspectProperty->other_facilities : "");
foreach ($model->inspectProperty->other_facilities as $key => $value) {
    $other_facilitie= \app\models\OtherFacilities::find()->where(['id'=>$value])->one();
    if ($other_facilities!=null) {  $other_facilities.=', '.$other_facilitie->title;  }
    else { $other_facilities.=$other_facilitie->title;   }
}


// 1.43, 1.44 , 1.45, 1.46  Floor Configuration
$floorConfig=Yii::$app->PdfHelper->getFloorConfig($model);
$floorConfigCustom=Yii::$app->PdfHelper->getFloorConfigCustom($model);


$floorConfig['groundFloortitle'] = rtrim($floorConfig['groundFloortitle'], "<br>");

if($floorConfig['groundFloortitle'] == 'None' && $floorConfigCustom['groundFloortitle'] !=''){
    $floorConfig['groundFloortitle'] = "<br>" . rtrim($floorConfigCustom['groundFloortitle'], "<br>");
}else {
    $floorConfig['groundFloortitle'] .= "<br>" . rtrim($floorConfigCustom['groundFloortitle'], "<br>");
}
$floorConfig['firstFloortitle'] = rtrim($floorConfig['firstFloortitle'], "<br>");
if($floorConfig['firstFloortitle'] == 'None' && $floorConfigCustom['firstFloortitle'] !=''){
    $floorConfig['firstFloortitle'] =  $floorConfigCustom['firstFloortitle']. "<br>";
}else {
    if(rtrim($floorConfigCustom['firstFloortitle'] <> null)) {
        $floorConfig['firstFloortitle'] .= "<br>" . $floorConfigCustom['firstFloortitle']. "<br>";
    }
}

// $floorConfig['firstFloortitle'] .= "<br>".rtrim($floorConfigCustom['firstFloortitle'], "<br>");

$floorConfig['secondFloortitle'] = rtrim($floorConfig['secondFloortitle'], "<br>");
if($floorConfig['secondFloortitle'] == 'None' && $floorConfigCustom['secondFloortitle'] !=''){
    $floorConfig['secondFloortitle'] =  rtrim($floorConfigCustom['secondFloortitle'], "<br>");
}else {
    if(rtrim($floorConfigCustom['secondFloortitle'] <> null)) {
        $floorConfig['secondFloortitle'] .= "<br>" . rtrim($floorConfigCustom['secondFloortitle'], "<br>");
    }
}
//   $floorConfig['secondFloortitle'] .= "<br>".rtrim($floorConfigCustom['secondFloortitle'], "<br>");
$floorConfig['basementTitle'] = rtrim($floorConfig['basementTitle'], "<br>");
if($floorConfig['basementTitle'] == 'None' && $floorConfigCustom['basementTitle'] !=''){
    $floorConfig['basementTitle'] =  rtrim($floorConfigCustom['basementTitle'], "<br>");
}else {
    if(rtrim($floorConfigCustom['basementTitle'] <> null)) {
        $floorConfig['basementTitle'] .= "<br>" . rtrim($floorConfigCustom['basementTitle'], "<br>");
    }
}


//$floorConfig['basementTitle'] .= "<br>".rtrim($floorConfigCustom['basementTitle'], "<br>");

// 1.66 Documents Provided by Client , 1.67 Documents not Provided
$DocumentByClient = Yii::$app->PdfHelper->getDocumentByClient($model);
$DocumentByClientpdf = Yii::$app->PdfHelper->getDocumentByClientpdf($model);


$DocumentByClient['documentAvail'] = rtrim($DocumentByClient['documentAvail'], "<br>");
$DocumentByClient['documentNotavail'] = rtrim($DocumentByClient['documentNotavail'], "<br>");


$makani_number = ($model->inspectProperty->makani_number > 0)? $model->inspectProperty->makani_number: 'Not Applicable';
$plot_number = ($model->plot_number <> null && $model->plot_number != '0')? $model->plot_number: 'Not Applicable';
$source_bua =($model->inspectProperty->bua_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->bua_source] . ')': '';
$source_plot_size =($model->inspectProperty->plot_area_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->plot_area_source]. ')': '';
$source_extension =($model->inspectProperty->extension_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->extension_source]. ')': '';

$plot_size = ($model->land_size > 0)? $model->land_size.' square feet'.$source_plot_size: 'Not Applicable';

$market = 'Market';
if($model->purpose_of_valuation == 3) {
    $market = 'Fair';
}


$otherInstructingPerson='';
$result = \app\models\User::find()->where(['id'=>$model->other_instructing_person])->one();
if ($result<>null) {
    $otherInstructingPerson = $result->firstname.' '.$result->lastname;
}
// print_r($otherInstructingPerson); die();
?>


<!--  <br pagebreak="true" />-->
<?php if($model->client->id == 183 || $model->id >12566){ ?>
    <style>
        td.topbarleft{
            font-weight: bold;
            font-size:12px;
            color:#000000;
            text-align: left
        }
        td.topbarright{
            font-weight: bold;
            font-size:12px;
            color:#000000;
            text-align: right
        }
    </style>

    <table class="col-12">
        <tr>
            <td class="topbarleft"><p>Valuation Summary</p></td>
            <td class="topbarright"><p>Report Date: <?= ($model->scheduleInspection->valuation_report_date <> null) ? date('l, jS \of F, Y', strtotime($model->scheduleInspection->valuation_report_date)) : '' ?> </p></td>
        </tr>
    </table>

    <br><br>
    <table cellspacing="1" cellpadding="4" style="border: 1px dashed #64B5F6;" class="col-12">
        <tr><td class="tableSecondHeading" colspan="4"><h3>General Information</h3></td></tr>
        <tr  class="bggray">
            <td colspan="1" style="color:#0277BD; font-size:12;">Client’s Name:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?= $model->client->title ?></td>
        </tr>
        <tr>
            <td colspan="1" style="color:#0277BD; font-size:13;">Applicant’s Name:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?= $model->client_name_passport ?>  <?=  $model->client_lastname_passport ?></td>
        </tr>
        <tr  class="bggray">
            <td colspan="1" style="color:#0277BD; font-size:13;">Reference No:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?= $model->reference_number ?></td>
        </tr>
        <tr>
            <td colspan="1" style="color:#0277BD; font-size:13;">Valuation Date:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?=  date('l, jS \of F, Y', strtotime($model->scheduleInspection->valuation_date)) ?></td>
        </tr>
        <tr  class="bggray">
            <td colspan="1" style="color:#0277BD; font-size:13;">Purpose of Valuation:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation] ?></td>
        </tr>


        <tr>
            <td colspan="1" style="color:#0277BD; font-size:13;">Inspection Date:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){
                    ?>
                    <?='Not Applicable as desktop valuation '; ?>
                <?php }
                else{
                    echo date('l, jS \of F, Y', strtotime($model->scheduleInspection->inspection_date));
                } ?>
            </td>
        </tr>
        <tr class="bggray">
            <td colspan="1" style="color:#0277BD; font-size:13;">Inspected By:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){ ?>
                    <?='Not Applicable as desktop valuation '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation '; ?>
                <?php }else{ ?><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->scheduleInspection->inspection_officer] ?>
                <?php } ?>
            </td>
        </tr>


        <tr >
            <td colspan="1" style="color:#0277BD; font-size:13;">Method of Valuation:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach] ?></td>
        </tr>
      <!--  <tr class="bggray">
            <td colspan="1"  style="color:#0277BD; font-size:13;">Status of Valuer:</td>
            <td colspan="3"  style="color:#212121; font-size:12;">External Valuer</td>
        </tr>-->
      <!--  <tr >
            <td colspan="1" style="color:#0277BD; font-size:13;">Previous Involvement:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?php
/*                $conflict_text = '';
                if($conflict['related_to_buyer']== 'Yes' || $conflict['related_to_seller']== 'Yes' || $conflict['related_to_property']== 'Yes'){

                    if($conflict['related_to_property']== 'Yes'){
                        $conflict_text .= 'Subject Property, ';
                    }
                    if($conflict['related_to_seller']== 'Yes'){
                        $conflict_text .= 'Seller, ';
                    }

                    if($conflict['related_to_buyer']== 'Yes'){
                        $conflict_text .= 'Buyer';
                    }


                    */?>We have had previous involvement with the <?/*= $conflict_text; */?> and there might be potential conflicts of interest exist in accepting this instruction.
                <?php /*}else{ */?>We confirm we have had no previous involvement with the Subject Property and that no conflicts of interest exist in accepting this instruction.
                <?php /*} */?>

            </td>
        </tr>-->
    </table>
    <table cellspacing="1" cellpadding="4" style="border: 1px dashed #64B5F6;" class="col-12">
        <tr>
            <td colspan="4" class="tableSecondHeading"><h3>Property Information</h3></td>
        </tr>
       <!-- <tr  class="bggray">
            <td colspan="1"  style="color:#0277BD; font-size:13;">Property / Site Address</td>
            <td colspan="3"  style="color:#212121; font-size:12;">Unit Number <?/*= $model->unit_number.', '.$model->building->title */?>
                Plot Number <?/*= $model->plot_number.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] */?>
            </td>
        </tr>-->
        <tr>
            <td  colspan="1" style="color:#0277BD; font-size:13;">Property Address :</td>
            <td  colspan="3" style="color:#212121; font-size:12;">Unit Number <?= $model->unit_number.', '.$model->building->title ?>
                Plot Number <?= $model->plot_number.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
            </td>
        </tr>
        <tr class="bggray">
            <td  colspan="1" style="color:#0277BD; font-size:13;">Owner's Name:</td>
            <td  colspan="3" style="color:#212121; font-size:12;"><?= $woners_name ?></td>
        </tr>
        <tr>
            <td  colspan="1" style="color:#0277BD; font-size:12;">Property Type/Description:</td>
            <td  colspan="3" style="color:#212121; font-size:12;"><?= $model->inspectProperty->no_of_bedrooms ?> Bedrooms <?= $model->property->title ?></td>
        </tr>
        <tr class="bggray">
            <td  colspan="1" style="color:#0277BD; font-size:13;">Property Category:</td>
            <td   colspan="3" style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category] ?></td>
        </tr>
        <tr>
            <td  colspan="1" style="color:#0277BD; font-size:13;">Total Built Up Area :</td>
            <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->net_built_up_area ?> square feet </td>
        </tr>
        <tr class="bggray">
            <td  colspan="1" style="color:#0277BD; font-size:13;">Tenure:</td>
            <td   colspan="3" style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
        </tr>
        <tr>
            <td  colspan="1" style="color:#0277BD; font-size:13;">Occupancy Status:</td>
            <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->occupancy_status ?></td>
        </tr>
        <tr class="bggray">
            <td  colspan="1" style="color:#0277BD; font-size:13;">Developer’s name:</td>
            <td   colspan="3" style="color:#212121; font-size:12;"><?= $model->inspectProperty->developer->title ?></td>
        </tr>
        <tr>
            <td  colspan="1" style="color:#0277BD; font-size:13;">Market Value:</td>
            <td   colspan="3" style="color:#212121; font-size:12;"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
        </tr>
        <tr class="bggray">
            <td  colspan="1" style="color:#0277BD; font-size:10;">Insurance Reinstatement Cost:</td>
            <td   colspan="3" style="color:#212121; font-size:12;">Not Applicable</td>
        </tr>
        <tr>
            <td  colspan="1" style="color:#0277BD; font-size:12;">Property Condition/Quality</td>
            <td  colspan="3" style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><br><?= Yii::$app->appHelperFunctions->propertyConditionListArr[$model->inspectProperty->property_condition] ?>
            </td>
        </tr>
        <tr class="bggray">
            <td  colspan="1" style="color:#0277BD; font-size:13;">Property Defect(s):</td>
            <td   colspan="3" style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->getPropertyDefectsArr()[$model->inspectProperty->property_defect] ?></td>
        </tr>
        <tr>
            <td  colspan="1" style="color:#0277BD; font-size:13;">Market Value:</td>
            <td  colspan="3"  colspan="3" style="color:#212121; font-size:12;"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
        </tr>
        <tr class="bggray">
            <td  colspan="1" style="color:#0277BD; font-size:11;"> Green Efficient Certification:</td>
            <td  colspan="3" style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->greenEfficientCertificationArr[$model->inspectProperty->green_efficient_certification] ?></td>
        </tr>
        <tr>
            <td  colspan="1" style="color:#0277BD; font-size:13;">Inspection Date:</td>
            <td  colspan="3"  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not Applicable as desktop valuation '; ?>
                <?php }else{?><?= Yii::$app->formatter->asDate($model->scheduleInspection->inspection_date,Yii::$app->params['fulldaydate']) ?>
                <?php } ?>
            </td>
        </tr>
        <tr class="bggray">
            <td  colspan="1" style="color:#0277BD; font-size:13;"> Documents Provided:</td>
            <td  colspan="3" style="color:#212121; font-size:12;"><?= $DocumentByClientpdf['documentAvail'] ?></td>
        </tr>
    </table>
  <!--  <br><br>
    <table cellspacing="1" cellpadding="4" style="border: 1px dashed #64B5F6;" class="col-12">-->
       <!-- <tr  class="bggray">
            <td  style="color:#0277BD; font-size:13;">Property Type/Description:</td>
            <td  style="color:#212121; font-size:12;"><?/*= $model->inspectProperty->no_of_bedrooms */?> Bedrooms <?/*= $model->property->title */?></td>
            <td  style="color:#0277BD; font-size:13;">Property Category:</td>
            <td  style="color:#212121; font-size:12;"><?/*= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category] */?></td>
        </tr>-->
      <!--  <tr>
            <td  style="color:#0277BD; font-size:13;">Total Built Up Area :</td>
            <td  style="color:#212121; font-size:12;"><?/*= $model->inspectProperty->net_built_up_area */?> square feet </td>
            <td  style="color:#0277BD; font-size:13;">Tenure:</td>
            <td  style="color:#212121; font-size:12;"><?/*= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] */?></td>
        </tr>-->
      <!--  <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">Occupancy Status:</td>
            <td  style="color:#212121; font-size:12;"><?/*= $model->inspectProperty->occupancy_status */?></td>
            <td  style="color:#0277BD; font-size:13;">Developer’s name:</td>
            <td  style="color:#212121; font-size:12;"><?/*= $model->inspectProperty->developer->title */?></td>
        </tr>-->
       <!-- <tr>
            <td  style="color:#0277BD; font-size:13;">Market Value: </td>
            <td  style="color:#212121; font-size:12;"><?/*= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" */?></td>
            <td  style="color:#0277BD; font-size:13;">Insurance Reinstatement Cost:</td>
            <td  style="color:#212121; font-size:12;">Not Applicable</td>
        </tr>-->
   <!-- </table>-->


    <br pagebreak="true" />
    <?php
}else{
    ?>
    <table cellspacing="1" cellpadding="2" style="border: 1px dashed #64B5F6;" class="col-12">
        <tr>
            <td colspan="7" class="tableSecondHeading" style="color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;"><h5>Executive Valuation Report Summary</h5></td>
        </tr>

        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> A. Valuation Overview</h5></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Client’s Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->client->title ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Client’s Customer Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->client_name_passport ?> <?=  $model->client_lastname_passport ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Intended User(s)</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= ( $model->other_intended_users <> null) ? $model->other_intended_users : 'None' ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Valuation Date</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php echo trim( date('l, jS \of F, Y', strtotime($model->scheduleInspection->valuation_date))) ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Valuation Scope</td><td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td><?php if($model->purpose_of_valuation == 3){ ?><td  colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray">Estimated Fair Value of the Subject Property</td>
            <?php } else{ ?>
                <td  colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?=  Yii::$app->appHelperFunctions->valuationScopeArr[$model->valuation_scope] ?></td>
            <?php } ?>

        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"></td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray"></td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"></td>
        </tr>

        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> B. Property Description - Location</h5></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Property Valued</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].' '.$model->property->title ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Project/Building Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->building->title ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Unit Number</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->unit_number ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Sub-Community Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->building->subCommunities->title ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Community Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->building->communities->title ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> City and Country</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>, United Arab Emirates</td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"></td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray"></td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"></td>
        </tr>


        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> C. Property Description -External</h5></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Tenure</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <?php 
                if($model->client_id == 54514 && $model->tenure == 1){ 
            ?>
                    <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray">Non-Freehold</td>
            <?php
                }
            else{ 
            ?>
                <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
            <?php
            } 
            ?>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Plot Size</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $plot_size ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Built-Up Area (BUA)</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->inspectProperty->net_built_up_area ?> square feet <?= $source_bua ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Accommodation</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= $model->inspectProperty->no_of_bedrooms ?> Bedrooms
            </td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"></td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray"></td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"></td>
        </tr>


        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> D. Valuation Details</h5></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Inspection Type</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type] ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Inspection Date</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php if($model->inspection_type == 3){
                    ?>
                    <?='Not Applicable as desktop valuation '; ?>
                <?php }
                else{?><?php echo date('l, jS \of F, Y', strtotime($model->scheduleInspection->inspection_date)) ?>
                    <?php //echo Yii::$app->formatter->asDate($model->scheduleInspection->inspection_date,Yii::$app->params['fulldaydate']) ?>
                <?php } ?>
            </td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Valuation Date</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php echo trim( date('l, jS \of F, Y', strtotime($model->scheduleInspection->valuation_date))) ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Valuation Approach</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach] ?></td>
        </tr>

        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> E. Market Detail</h5></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Market Value</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:10; width:293px;"  class="bggray"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Market Value Rate</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot': "Not Applicable" ?></td>
        </tr>
        <tr class="bggray" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Market Rent</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?>
                <?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?>
                <?php } ?>
            </td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="3" style="color:#0277BD; font-size:13;"> Estimated Price Under the Restricted Marketing Period of 1 Month (Forced Sale)</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?=($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9)  : "" ?></td>
        </tr>
    </table>
    <br><br>

    <?php
}
?>





<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">

    1.Valuation Overview

</div>
<br>
<table class="mx-auto col-12" cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;">

    <tr><td class="tableSecondHeading" colspan="2"><h3>General Details</h3></td></tr>


    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.01 Windmills Reference</td>
        <td style="color:#212121; font-size:12;"><?= $model->reference_number ?></td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.02 Client Reference </td>
        <td  style="color:#212121; font-size:12;"><?= $model->client_reference ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.03 Client’s Full Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->client->title ?></td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.04 Intended User(s) Name</td>
        <td  style="color:#212121; font-size:12;"><?= ( $model->other_intended_users <> null) ? $model->other_intended_users : 'None' ?></td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.05 Valuation Instructions From</td>
        <td  style="color:#212121; font-size:12;"><?= ($otherInstructingPerson <> null) ? $otherInstructingPerson:$instructorname ; //$instructorname ?></td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.06 Valuation Instructions Date</td>
        <td  style="color:#212121; font-size:12;"><?=  date('l, jS \of F, Y', strtotime($model->instruction_date)) ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.07 Valuation Scope</td>

        <?php if($model->purpose_of_valuation == 3){ ?>
            <td  style="color:#212121; font-size:12;">Estimated Fair Value of the Subject Property</td>
        <?php } else{ ?>
            <td  style="color:#212121; font-size:12;"><?=  Yii::$app->appHelperFunctions->valuationScopeArr[$model->valuation_scope] ?></td>
        <?php } ?>


        <!--<td  style="color:#212121; font-size:12;"><?/*=  Yii::$app->appHelperFunctions->valuationScopeArr[$model->valuation_scope] */?></td>-->
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.08 Owner’s Full Name</td>
        <td  style="color:#212121; font-size:12;"><?= $woners_name ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.09 Client’s Customer Full Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->client_name_passport ?> <?=  $model->client_lastname_passport ?></td>
    </tr >
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.10 Transaction Price</td>
        <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->transaction_price <> null && $model->costDetails->transaction_price != "0.00")? "AED ".number_format($model->costDetails->transaction_price) : "Not Applicable" ?></td>
    </tr>
    <?php if($model->costDetails->transaction_price <> null && $model->costDetails->transaction_price != "0.00") { ?>
        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.11 Transaction Price Rate</td>
            <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->transaction_price <> null && $model->costDetails->transaction_price != "0.00" )? "AED ".number_format((float)($model->costDetails->transaction_price/$model->inspectProperty->net_built_up_area), 2, '.', '') ." per square foot" : "Not Applicable"?></td>
        </tr>

        <tr >
            <td  style="color:#0277BD; font-size:13;">1.12 Transaction Price Date</td>
            <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->transaction_price_date <> null)? date('l, jS \of F, Y', strtotime($model->costDetails->transaction_price_date)): "Not Applicable" ?></td>
        </tr>

        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.13 Source of Transaction Price</td>
            <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->transaction_source <> null) ? Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->costDetails->transaction_source] : "Not Applicable"  ?> </td>
        </tr>
    <?php  }else{ ?>

        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.11 Transaction Price Rate</td>
            <td  style="color:#212121; font-size:12;">Not Applicable</td>
        </tr>
        <tr >
            <td  style="color:#0277BD; font-size:13;">1.12 Transaction Price Date</td>
            <td  style="color:#212121; font-size:12;">Not Applicable</td>
        </tr>


        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.13 Source of Transaction Price</td>
            <td  style="color:#212121; font-size:12;">Not Applicable </td>
        </tr>
    <?php } ?>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.14 Original Purchase Price</td>
        <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->original_purchase_price <> null && $model->costDetails->original_purchase_price != "0.00")? "AED ".number_format($model->costDetails->original_purchase_price) : "Not Applicable" ?></td>
    </tr>

    <?php if($model->costDetails->original_purchase_price <> null && $model->costDetails->original_purchase_price != "0.00") { ?>

        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.15 Original Purchase Price Rate</td>
            <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->original_purchase_price <> null && $model->costDetails->original_purchase_price != "0.00") ? "AED ".number_format((float)($model->costDetails->original_purchase_price/$model->inspectProperty->net_built_up_area), 2, '.', '')." per square foot" : "Not Applicable" ?>  </td>
        </tr>
        <tr >
            <td  style="color:#0277BD; font-size:13;">1.16 Source of Original Purchase Price</td>
            <td  style="color:#212121; font-size:12;"><?=  ($model->costDetails->source_of_original_date_price <> null) ? Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->costDetails->source_of_original_date_price] : "Not Applicable" ?></td>
        </tr>
        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.17 Date of Original Purchase Price</td>
            <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->date_of_original_date_price <> null) ? date('l, jS \of F, Y', strtotime($model->costDetails->date_of_original_date_price)) : "Not Applicable"?> </td>
        </tr>
    <?php  }else{ ?>
        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.15 Original Purchase Price Rate</td>
            <td  style="color:#212121; font-size:12;">Not Applicable </td>
        </tr>
        <tr >
            <td  style="color:#0277BD; font-size:13;">1.16 Source of Original Purchase Price</td>
            <td  style="color:#212121; font-size:12;">Not Applicable</td>
        </tr>
        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.17 Date of Original Purchase Price</td>
            <td  style="color:#212121; font-size:12;">Not Applicable </td>
        </tr>
    <?php  } ?>
</table>

<br pagebreak="true" />

<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;" class="col-12">

    <tr><td class="tableSecondHeading" colspan="2"><h3>Property Description - Location</h3></td></tr>

    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.18 Property (Interest) Valued</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].' '.$model->property->title ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.19 Property Use</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category] ?>
        </td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.20 Total Building Floor(s)</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $total_building_floors ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.21 Floor Number(s)</td>
        <?php if(in_array($model->property_id, [1,12,37,28,17])){
            $floor_text = "Ground floor";
        }else{
            $floor_text = "Not Applicable";
        } ?>
        <td  style="color:#212121; font-size:12;"><?= ($model->floor_number > 0)?  $model->floor_number: $floor_text?></td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.22 Property Unit Number</td>
        <td  style="color:#212121; font-size:12;"><?= $model->unit_number ?></td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.23 Building No.</td>
        <td  style="color:#212121; font-size:12;"><?= $model->building_number ?></td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.24 Project Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->building->title ?></td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.25 Street Number/Name</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not Applicable as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->street ?></td>
    </tr>
    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.26 Plot Number</td>
        <td style="color:#212121; font-size:12;"><?= $plot_number ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.27 Municipality’ Makani Number</td>
        <td  style="color:#212121; font-size:12;"><?= $makani_number?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.28 Sub Community Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->building->subCommunities->title ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.29 Community Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->building->communities->title ?></td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.30 City and Country Name</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>, United Arab Emirates</td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.31 Location Characteristics</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><b>Good location</b>
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_highway_drive) ?> drive to highway
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_school_drive) ?> to school
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_mall_drive) ?> to commercial mall
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_sea_drive) ?> to special landmark
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_park_drive) ?> drive to pool/park
        </td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.32 Location Coordinates</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not Applicable as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->latitude.','.$model->inspectProperty->longitude ?> (as per Google Maps)</td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.33 Placement (Middle / Corner) </td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= Yii::$app->appHelperFunctions->propertyPlacementListArr[$model->inspectProperty->property_placement] ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.34 Exposure (Single Row / Back)</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= Yii::$app->appHelperFunctions->propertyExposureListArr[$model->inspectProperty->property_exposure] ?>
        </td>
    </tr>
</table>



<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;" class="col-12">

    <tr><td class="tableSecondHeading" colspan="2"><h3>Property Description - External</h3></td></tr>

    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.35  Property Type (1E, 2M etc.)</td>
        <td style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->listing_property_type ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.36 Development (Standard/Non)</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->development_type ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.37 Tenure (FH/NFH/Leasehold)</td>
                 <?php 
                if($model->client_id == 54514 && $model->tenure == 1){ 
            ?>
                <td  style="color:#212121; font-size:12;">Non-Freehold</td>
            <?php
                }
            else{ 
            ?>
                <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
            <?php
            } 
            ?>
    </tr>

    <tr>
        <td style="color:#0277BD; font-size:13;">1.38 Completion Status</td>
        <td style="color:#212121; font-size:12;"><?= ($model->inspectProperty->completion_status)==100 ? 'Yes' : 'No'; ?>
        </td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.39 Completion Percentage</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->completion_status ?></td>
    </tr>

    <tr >
        <td style="color:#0277BD; font-size:13;">1.40 Property Completion Year</td>
        <td style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= ((int)date('Y')) - round($model->inspectProperty->estimated_age).' ' ?>

        </td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.41 Estimate Age (in years)</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= round($model->inspectProperty->estimated_age).' Years' ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.42 Estimated Remaining Life (in years)</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->client->id == 1 && $model->inspectProperty->estimated_remaining_life > 0){ ?><?= ($model->inspectProperty->estimated_remaining_life - 1) . ' - '. ($model->inspectProperty->estimated_remaining_life + 1) ?> Years<?php  }else{ ?><?= round($model->inspectProperty->estimated_remaining_life) ?> Years<?php } ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.43  Plot Size (in square feet)</td>
        <td  style="color:#212121; font-size:12;"><?= $plot_size ?></td>
    </tr>


    <tr>
        <td style="color:#0277BD; font-size:13;">1.44 Built Up Area (in square feet)</td>
        <td style="color:#212121; font-size:12;"><?= $model->inspectProperty->net_built_up_area ?> square feet <?= $source_bua ?></td>
    </tr>


    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.45 View Type</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?= $ViewType['viewCommunity'].$ViewType['viewPool'].$ViewType['viewBurj'].$ViewType['viewSea'].$ViewType['viewMarina'].$ViewType['h'].$ViewType['viewLake'].$ViewType['viewGolfCourse'].$ViewType['viewPark'].$ViewType['viewSpecial'] ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.46 Landscaping Details</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->landscaping ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.47 Parking Spaces</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->parking_floors ?></td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.48  Building/Community Facilities</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $other_facilities ?></td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.49 Developer’s Full Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->developer->title ?></td>
    </tr>

</table>


<br pagebreak="true" />


<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;" class="col-12">
    <tr><td class="tableSecondHeading" colspan="2"><h3><?= trim('Green Efficient Certification') ?></h3></td></tr>

    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.50 Green Efficient Certification</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->greenEfficientCertificationArr[$model->inspectProperty->green_efficient_certification] ?></td>
    </tr>
    <tr>
        <td style="color:#0277BD; font-size:13;">1.51 Certifier Name</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->certifierNameArr[$model->inspectProperty->certifier_name] ?></td>
    </tr>
    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.52 Certification Level</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->certificationLevelArr[$model->inspectProperty->certification_level] ?></td>
    </tr>
    <tr>
        <td style="color:#0277BD; font-size:13;">1.53 Source of Green Certificate Information</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->sGCInformationArr[$model->inspectProperty->source_of_green_certificate_information] ?></td>
    </tr>

</table>
<br><br>


<table cellspacing="1" cellpadding="6" style="border: 1px dashed #64B5F6;" class="col-12">

    <tr><td class="tableSecondHeading" colspan="2"><h3>Property Description – Internal</h3></td></tr>


    <tr>
        <td style="color:#0277BD; font-size:13;">1.54 Accommodation</td>
        <td style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->no_of_bedrooms ?> Bedrooms</td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.55 Basement Floor Configuration</td>

        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= ($floorConfig['basementTitle'] <> null) ? rtrim($floorConfig['basementTitle'],"<br>") : "Not Applicable"; ?> </td>
    </tr>

    <?php
    $property='Ground';

    if($model->property_id==1 || $model->property_id == 17 || $model->property_id == 12 || $model->property_id == 14 || $model->property_id == 19 || $model->property_id == 28 || $model->property_id == 37)
    {
        $property='Unit';
    }

    if ($model->property_id != 1 && $model->property_id != 17 && $model->property_id != 12 && $model->property_id != 14 && $model->property_id != 19 && $model->property_id != 28 && $model->property_id != 37) {
        ?>

        <!-- <tr>
        <td  style="color:#0277BD; font-size:13;">1.49 <?/*= $property */?> Floor Configuration</td>
        <td  style="color:#212121; font-size:12;"><?php /*if($model->inspection_type == 3){*/?><?/*='Not known as desktop valuation. Assumed '; */?>
            <?php /*}else if($model->inspection_type == 1){ */?><?/*='Not known as drive-by valuation. Assumed '; */?>
            <?php /*} */?><br><?/*= ($floorConfig['groundFloor'] <> null) ? rtrim($floorConfig['groundFloor'],"<br>") : "Not Applicable"; */?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.50 First Floor Configuration</td>
      <td  style="color:#212121; font-size:12;"><?php /*if($model->inspection_type == 3){*/?><?/*='Not known as desktop valuation. Assumed '; */?>
          <?php /*}else if($model->inspection_type == 1){ */?><?/*='Not known as drive-by valuation. Assumed '; */?>
          <?php /*} */?><br><?/*= ($floorConfig['firstFloor'] <> null)? rtrim($floorConfig['firstFloor']) : "Not Applicable" */?></td>
      </tr>
      <tr>
        <td  style="color:#0277BD; font-size:13;">1.51 Second Floor Configuration</td>
        <td  style="color:#212121; font-size:12;"><?php /*if($model->inspection_type == 3){*/?><?/*='Not known as desktop valuation. Assumed '; */?>
            <?php /*}else if($model->inspection_type == 1){ */?><?/*='Not known as drive-by valuation. Assumed '; */?>
            <?php /*} */?><br><?/*= ($floorConfig['secondFloor'] <> null)?rtrim($floorConfig['secondFloor']): "Not Applicable" */?></td>
      </tr>-->

        <tr>
            <td  style="color:#0277BD; font-size:13;">1.56 <?= $property ?> Floor Configuration</td>
            <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= ($floorConfig['groundFloortitle'] <> null) ? rtrim($floorConfig['groundFloortitle'],"<br>") : "Not Applicable"; ?></td>
        </tr>
        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.57 First Floor Configuration</td>
            <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= ($floorConfig['firstFloortitle'] <> null)? rtrim($floorConfig['firstFloortitle']) : "Not Applicable" ?></td>
        </tr>
        <tr>
            <td  style="color:#0277BD; font-size:13;">1.58 Second Floor Configuration</td>
            <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= ($floorConfig['secondFloortitle'] <> null)?rtrim($floorConfig['secondFloortitle']): "Not Applicable" ?></td>
        </tr>


    <?php  }
    if ($model->valuationConfiguration->over_all_upgrade>=3 && ($model->property_id == 1 || $model->property_id == 17 || $model->property_id == 12 || $model->property_id == 14 || $model->property_id == 19 || $model->property_id == 28 || $model->property_id == 37)) {
        ?>

        <tr>
            <td  style="color:#0277BD; font-size:13;">1.56 <?= $property ?> Floor Configuration</td>
            <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= ($floorConfig['groundFloortitle'] <> null) ? $floorConfig['groundFloortitle'] : "Not Applicable" ?></td>
        </tr>
        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.57 First Floor Configuration</td>
            <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= ($floorConfig['firstFloortitle'] <> null) ? $floorConfig['firstFloortitle'] : "Not Applicable" ?></td>
        </tr>
        <tr>
            <td  style="color:#0277BD; font-size:13;">1.58 Second Floor Configuration</td>
            <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= ($floorConfig['secondFloortitle']) ? $floorConfig['secondFloortitle'] : "Not Applicable" ?></td>
        </tr>

    <?php } ?>





    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.59 Upgrade Details</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?= ($floorConfig['upgrade_text'] <> null) ? $floorConfig['upgrade_text'] : "Not Applicable" ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.60 Extensions Details</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= ($model->inspectProperty->extension <> null)? $model->inspectProperty->extension.$source_extension:"None" ?></td>
    </tr>


    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.61 Property Condition/Quality</td>
        <td style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?= Yii::$app->appHelperFunctions->propertyConditionListArr[$model->inspectProperty->property_condition] ?></td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.62 Property Defect/s</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->getPropertyDefectsArr()[$model->inspectProperty->property_defect] ?></td>
    </tr>

    <tr  class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.63 Furnished</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?=  Yii::$app->appHelperFunctions->reportAttributesValuefurnished[$model->inspectProperty->furnished]   ?></td>
    </tr>




    <tr>
        <td  style="color:#0277BD; font-size:13;">1.64 Swimming Pool </td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->pool ?></td>
    </tr>
    <tr  class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.65 Cooker Installed</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?= Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->cooker] ?></td>
    </tr>

    <tr >
        <td  style="color:#0277BD; font-size:13;">1.66 Oven Installed</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?=  Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->oven] ?></td>
    </tr>
    <tr  class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.67 Fridge Installed</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?= Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->fridge]  ?></td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.68 Washing Machine Installed</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?=  Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->washing_machine] ?></td>
    </tr>
    <?php
    if(!empty($model->inspectProperty->ac_type)){
        $actypes = explode(',', ($model->inspectProperty->ac_type <> null) ? $model->inspectProperty->ac_type : "");
    }
    ?>
    <tr  class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.69 Central Air-conditioning</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php }

            if($actypes <> null && !empty($actypes)){
                if (in_array(3, $actypes))
                {
                    echo "Yes";
                }else{
                    echo "No";
                }
            }else{
                echo "No";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.70 Split Air-conditioning Units</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php }
            if($actypes <> null && !empty($actypes)){
                if (in_array(1, $actypes))
                {
                    echo "Yes";
                }else{
                    echo "No";
                }
            }else{
                echo "No";
            }


            ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.71 Window Air-conditioning Unit</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php }

            if($actypes <> null && !empty($actypes)){
                if (in_array(2, $actypes))
                {
                    echo "Yes";
                }else{
                    echo "No";
                }
            }else{
                echo "No";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.72 Utilities connected</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->utilities_connected ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.73 Tenancies / Occupancy Status</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->occupancy_status ?></td>
    </tr>
</table>


<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;" class="col-12">
    <tr><td class="tableSecondHeading" colspan="2"><h3>Valuation Details</h3></td></tr>


    <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.74 Inspection Type Instructed</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type] ?></td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.75 Purpose of the Valuation</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation] ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.76 Inspecting Officer</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){ ?> <?='Not Applicable as desktop valuation '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation '; ?>
            <?php }else{ ?><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->scheduleInspection->inspection_officer] ?>
            <?php } ?>
        </td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.77 Service Officer</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->service_officer_name] ?>

        </td>
    </tr>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.78 Inspection Date</td>
        <td  style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){?><?='Not Applicable as desktop valuation '; ?>
            <?php }else{?><?= Yii::$app->formatter->asDate($model->scheduleInspection->inspection_date,Yii::$app->params['fulldaydate']) ?>
            <?php } ?>
        </td>
    </tr>
    <tr >

        <td  style="color:#0277BD; font-size:13;">1.79 Valuation Date</td>
        <td  style="color:#212121; font-size:12;"><?= trim(Yii::$app->formatter->asDate($model->scheduleInspection->valuation_date,Yii::$app->params['fulldaydate'])) ?></td>
    </tr>

    <!-- <tr  class="bggray">
            <td style="color:#0277BD; font-size:13;">1.72 Documents not Provided </td>
            <td style="color:#212121; font-size:12;"><?/*= $DocumentByClient['documentAvail'] */?></td>
        </tr>-->
    <tr  class="bggray">
        <td style="color:#0277BD; font-size:13;">1.80 Documents Provided </td>
        <td style="color:#212121; font-size:12;"><?= $DocumentByClient['documentAvail'] ?></td>
    </tr>
    <tr >
        <td style="color:#0277BD; font-size:13;">1.81 Documents not Provided </td>
        <td style="color:#212121; font-size:12;"><?= $DocumentByClient['documentNotavail'] ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.82 Basis of Value</td>
        <?php if($model->purpose_of_valuation == 3){ ?>
            <td  style="color:#212121; font-size:12;">Fair Value</td>
        <?php } else{ ?>
            <td  style="color:#212121; font-size:12;"><?= $model->property->basis_of_value ?></td>
        <?php } ?>

        <!--<td  style="color:#212121; font-size:12;"><?/*= $model->property->basis_of_value */?></td>-->
    </tr>
    <tr>
        <td  style="color:#0277BD; font-size:13;">1.83 Valuation Approach</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach] ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.84 Approach Reasoning</td>
        <td  style="color:#212121; font-size:12;"><?= $model->property->approach_reason ?></td>
    </tr>

    <tr>
        <td  style="color:#0277BD; font-size:13;">1.85 Valuation Adjustments</td>
        <td  style="color:#212121; font-size:12;">Size, Floor, Quality, Row, View, Placement, Listing Date and Sales Negotiation Discount.</td>
    </tr>
</table>
<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;" class="col-12">
    <tr><td class="tableSecondHeading" colspan="2"><h3><?= $market ?> Value</h3></td></tr>




    <tr>
        <td style="color:#0277BD; font-size:13;">1.86 <?= $market ?> Value</td>


        <td style="color:#212121; font-size:12;"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
    </tr>
    <?php if(in_array($model->property_id, array(4,5,11,23,26,29,39))){ ?>
        <tr class="bggray">

            <td  style="color:#0277BD; font-size:13;">1.87 <?= $market ?> Value Rate (BUA)</td>
            <td  style="color:#212121; font-size:12;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot': "Not Applicable" ?> </td>
        </tr>
        <tr>
            <td style="color:#0277BD; font-size:13;">1.88 Market Value Rate (Plot Area)</td>
            <td style="color:#212121; font-size:12;"><?= ($approver_data->market_value_sqf_pa <> null) ? "AED " . number_format($approver_data->market_value_sqf_pa) . ' per square foot' : "Not Applicable" ?> </td>
        </tr>
        <tr >
            <td style="color:#0277BD; font-size:13;">1.89 Market Rent</td>
            <td style="color:#212121; font-size:12;">
                <?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?><?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?><?php } ?></td>
        </tr>
        <tr class="bggray">

            <td style="color:#0277BD; font-size:13;">1.83 Estimated Price Under the Restricted Marketing Period of 1 Month (Forced Sale)</td>
            <td style="color:#212121; font-size:12;"><?= ($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9)  : "" ?> </td>
        </tr>
        <?php
        $number= '1.90';
    }else if(($model->client->land_valutaion == 1) && (round($model->inspectProperty->estimated_age) >= $model->client->land_age) && ($model->tenure == 1) && ($model->property_id == 6 || $model->property_id == 22 || $model->property_id == 10 || $model->property_id == 2) ){ ?>
        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.87 <?= $market ?> Value Rate</td>
            <td  style="color:#212121; font-size:12;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot': "Not Applicable" ?> </td>
        </tr>
        <tr>

            <td style="color:#0277BD; font-size:13;">1.88 Market Rent</td>
            <td style="color:#212121; font-size:12;"><?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?><?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?><?php } ?></td>
        </tr>
        <tr class="bggray">
            <td style="color:#0277BD; font-size:13;">1.89 Estimated Price Under the Restricted Marketing Period of 1 Month (Forced Sale)</td>
            <td style="color:#212121; font-size:12;"><?= ($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9)  : "" ?> </td>
        </tr>
        <tr>
            <td style="color:#0277BD; font-size:13;">1.90 <?= $market ?> Value of Land</td>


            <td style="color:#212121; font-size:12;"><?= ($estimate_price_land_byapprover <> null)? "AED ".$estimate_price_land_byapprover: "Not Applicable" ?></td>
        </tr>


        <?php
        $number= '1.91';}else { ?>
        <tr class="bggray">

            <td  style="color:#0277BD; font-size:13;">1.87 <?= $market ?> Value Rate</td>
            <td  style="color:#212121; font-size:12;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot': "Not Applicable" ?> </td>
        </tr>

        <tr>

            <td style="color:#0277BD; font-size:13;">1.88 Market Rent</td>
            <td style="color:#212121; font-size:12;">
                <?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?><?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?><?php } ?></td>
        </tr>
        <tr class="bggray">

            <td style="color:#0277BD; font-size:13;">1.89 Estimated Price Under the Restricted Marketing Period of 1 Month (Forced Sale)</td>
            <td style="color:#212121; font-size:12;"><?= ($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9)  : "" ?> </td>
        </tr>

        <?php
        $number= '1.90';
    }
    $special_assumptions='';
    //special Assumptions Occupancy, Tananted, Vacant, Acquisition
    //  $special_assumptions.= '<br><b>Occupancy Status</b><br>';
    $special_assumptionreport = \app\models\SpecialAssumptionreport::find()->one();



    if($model->inspectProperty->occupancy_status == "Vacant"){
        $special_assumptions.=$special_assumptionreport->vacant;
    }

    if($model->inspectProperty->occupancy_status == "Owner Occupied"){
        $special_assumptions.=$special_assumptionreport->owner_occupied;
    }

    if($model->inspectProperty->occupancy_status == "Tenanted"){
        $special_assumptions.=$special_assumptionreport->tenanted;
    }


    if($model->inspectProperty->acquisition_method == 1 || $model->inspectProperty->acquisition_method == 2 ){
        $special_assumptions.= '<br>'.$special_assumptionreport->gifted_granted;
    }else if($model->inspectProperty->acquisition_method == 4){
        $special_assumptions.= '<br>'.$special_assumptionreport->inherited;

    }else{
        if(trim($special_assumptionreport->purchased) != 'None' && trim($special_assumptionreport->purchased) != '') {
            $special_assumptions .= '<br>' .$special_assumptionreport->purchased.'<br>';
        }


    }

    if ($model->scheduleInspection->valuation_date > $model->scheduleInspection->inspection_date  ) {
        $valuationdate_assumption= \app\models\GeneralAsumption::find()->where(['id'=>5])->one();
        $special_assumptions .= '<br>' .$valuationdate_assumption->general_asumption.'<br>';

    }
    if($model->special_assumption !== 'None' &&  ($model->special_assumption <> null)) {
        $special_assumptions.= trim($model->special_assumption);
    }
    ?>
    <tr>
        <td  style="color:#0277BD; font-size:13;"><?= $number ?> Special Assumptions & Concerns</td>
        <td  style="color:#212121; font-size:12;"><?php if($special_assumptions <> null ){echo $special_assumptions;}else{echo 'None';}?></td>
    </tr>
</table>