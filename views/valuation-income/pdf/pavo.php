
<?php

// This is for 1.05 Valuation Instructions From.
$instructorname=Yii::$app->PdfHelper->getUserInformation($model);
$detail= \app\models\ValuationDetailData::find()->where(['valuation_id' => $model->id])->one();
$detail_main= \app\models\ValuationDetail::find()->where(['valuation_id' => $model->id])->one();
$land = array(4,5,20,23,26,29,39,46,47,48,49,50,53);

function convertNumberToWord($num = false)
    {
        $num = str_replace(array(',', ' '), '', trim($num));
        if (!$num) {
            return false;
        }
        $num = (int) $num;
        $words = array();
        $list1 = array(
            '',
            'One',
            'Two',
            'Three',
            'Four',
            'Five',
            'Six',
            'Seven',
            'Eight',
            'Nine',
            'Ten',
            'Eleven',
            'Twelve',
            'Thirteen',
            'Fourteen',
            'Fifteen',
            'Sixteen',
            'Seventeen',
            'Eighteen',
            'Nineteen'
        );
        $list2 = array('', 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety', 'Hundred');
        $list3 = array(
            '',
            'Thousand',
            'Million',
            'Billion',
            'Trillion',
            'Quadrillion',
            'Quintillion',
            'Sextillion',
            'Septillion',
            'Octillion',
            'Nonillion',
            'Decillion',
            'Undecillion',
            'Duodecillion',
            'Tredecillion',
            'Quattuordecillion',
            'Quindecillion',
            'Sexdecillion',
            'Septendecillion',
            'Octodecillion',
            'Novemdecillion',
            'Vigintillion'
        );
        $num_length = strlen($num);
        $levels = (int) (($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);
        for ($i = 0; $i < count($num_levels); $i++) {
            $levels--;
            $hundreds = (int) ($num_levels[$i] / 100);
            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ' ' : '');
            $tens = (int) ($num_levels[$i] % 100);
            $singles = '';
            if ($tens < 20) {
                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
            } else {
                $tens = (int) ($tens / 10);
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int) ($num_levels[$i] % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . (($levels && (int) ($num_levels[$i])) ? ' ' . $list3[$levels] . ' ' : '');
        } //end for loop
        $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }
        return implode(' ', $words);
    }


if (in_array($model->property_id, $land))
{
    $land_val = 1;
}
else
{
    $land_val = 0;
}
$bedroom_html= $model->inspectProperty->no_of_bedrooms.' Bedrooms';
if($model->inspectProperty->no_of_bedrooms == 0 && in_array($model->property_id, [1,12,37,53,59,70,84])){
    $bedroom_html = 'Studio';

}
if($model->inspectProperty->no_of_bedrooms == 0 && $land_val == 1){
    $bedroom_html = 'Not Applicable';
}
//1.14  Total Building Floor(s)
if($model->property->title == 'Villa') {
    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1 ) {
        $total_building_floors = "Basement + " .Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
    }else if($model->inspectProperty->number_of_basement > 1){
        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }else{
        $total_building_floors = Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
    }

}else {
    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1) {
        $total_building_floors = "Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    } else if($model->inspectProperty->number_of_basement > 1){
        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }else {
        $total_building_floors = "Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }
}
$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();

$conflict= \app\models\ValuationConflict::find()->where(['valuation_id'=>$model->id])->one();
$owners= \app\models\ValuationOwners::find()->where(['valuation_id'=>$model->id])->all();
$woners_name = "";
foreach ($owners as $record){
    $woners_name .= $record->name." ".$record->lastname.", ";
}
/*
$owners_in_valuation = \yii\helpers\ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $model->id])->all(), 'id', 'name');
//$owners_in_valuation = \yii\helpers\ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $model->id])->all(), 'id', 'CONCAT(name," ",lastname) as name');
$woners_name = "";
if(!empty($owners_in_valuation)) {
    $woners_name = implode(", ", $owners_in_valuation);
}*/



//  1.36  View Type
$ViewType = Yii::$app->PdfHelper->getViewType($model);

// echo "<pre>";
// print_r($ViewType);
// echo "<pre>";
// die();


// print_r($model->inspectProperty->other_facilities );
// die();

// 1.39 Building/Community Facilities
$model->inspectProperty->other_facilities = explode(',', ($model->inspectProperty->other_facilities <> null) ? $model->inspectProperty->other_facilities : "");
foreach ($model->inspectProperty->other_facilities as $key => $value) {
    $other_facilitie= \app\models\OtherFacilities::find()->where(['id'=>$value])->one();
    if ($other_facilities!=null) {  $other_facilities.=', '.$other_facilitie->title;  }
    else { $other_facilities.=$other_facilitie->title;   }
}


// 1.43, 1.44 , 1.45, 1.46  Floor Configuration
$floorConfig=Yii::$app->PdfHelper->getFloorConfig($model);
$floorConfigCustom=Yii::$app->PdfHelper->getFloorConfigCustom($model);


$floorConfig['groundFloortitle'] = rtrim($floorConfig['groundFloortitle'], "<br>");

if($floorConfig['groundFloortitle'] == 'None' && $floorConfigCustom['groundFloortitle'] !=''){
    $floorConfig['groundFloortitle'] = "<br>" . rtrim($floorConfigCustom['groundFloortitle'], "<br>");
}else {
    $floorConfig['groundFloortitle'] .= "<br>" . rtrim($floorConfigCustom['groundFloortitle'], "<br>");
}
$floorConfig['firstFloortitle'] = rtrim($floorConfig['firstFloortitle'], "<br>");
if($floorConfig['firstFloortitle'] == 'None' && $floorConfigCustom['firstFloortitle'] !=''){
    $floorConfig['firstFloortitle'] =  $floorConfigCustom['firstFloortitle']. "<br>";
}else {
    if(rtrim($floorConfigCustom['firstFloortitle'] <> null)) {
        $floorConfig['firstFloortitle'] .= "<br>" . $floorConfigCustom['firstFloortitle']. "<br>";
    }
}

// $floorConfig['firstFloortitle'] .= "<br>".rtrim($floorConfigCustom['firstFloortitle'], "<br>");

$floorConfig['secondFloortitle'] = rtrim($floorConfig['secondFloortitle'], "<br>");
if($floorConfig['secondFloortitle'] == 'None' && $floorConfigCustom['secondFloortitle'] !=''){
    $floorConfig['secondFloortitle'] =  rtrim($floorConfigCustom['secondFloortitle'], "<br>");
}else {
    if(rtrim($floorConfigCustom['secondFloortitle'] <> null)) {
        $floorConfig['secondFloortitle'] .= "<br>" . rtrim($floorConfigCustom['secondFloortitle'], "<br>");
    }
}
//   $floorConfig['secondFloortitle'] .= "<br>".rtrim($floorConfigCustom['secondFloortitle'], "<br>");
$floorConfig['basementTitle'] = rtrim($floorConfig['basementTitle'], "<br>");
if($floorConfig['basementTitle'] == 'None' && $floorConfigCustom['basementTitle'] !=''){
    $floorConfig['basementTitle'] =  rtrim($floorConfigCustom['basementTitle'], "<br>");
}else {
    if(rtrim($floorConfigCustom['basementTitle'] <> null)) {
        $floorConfig['basementTitle'] .= "<br>" . rtrim($floorConfigCustom['basementTitle'], "<br>");
    }
}


//$floorConfig['basementTitle'] .= "<br>".rtrim($floorConfigCustom['basementTitle'], "<br>");

// 1.66 Documents Provided by Client , 1.67 Documents not Provided
$DocumentByClient = Yii::$app->PdfHelper->getDocumentByClient($model);
$DocumentByClientpdf = Yii::$app->PdfHelper->getDocumentByClientpdf($model);


$DocumentByClient['documentAvail'] = rtrim($DocumentByClient['documentAvail'], "<br>");
$DocumentByClient['documentNotavail'] = rtrim($DocumentByClient['documentNotavail'], "<br>");


$makani_number = ($model->inspectProperty->makani_number > 0)? $model->inspectProperty->makani_number: 'Not Applicable';
$municipality_number = ($model->inspectProperty->municipality_number > 0)? $model->inspectProperty->municipality_number: 'Not Known';
$plot_number = ($model->plot_number <> null && $model->plot_number != '0')? $model->plot_number: 'Not Applicable';
$source_bua =($detail_main->bua_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArrBuaSource[$detail_main->bua_source] . ')': '';
$source_plot_size =($detail_main->plot_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArrPlotSource[$detail_main->plot_source]. ')': '';
$source_extension =($model->inspectProperty->extension_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->extension_source]. ')': '';

$plot_size = ($model->land_size > 0)? Yii::$app->appHelperFunctions->wmFormate($model->land_size).' square feet'.$source_plot_size: 'Not Applicable';

$market = 'Estimated Market';
if($model->purpose_of_valuation == 3) {
    $market = 'Estimated Fair';
}

$market_ajman = 'Market';
if($model->purpose_of_valuation == 3) {
    $market_ajman = 'Fair';
}

$otherInstructingPerson='';
$result = \app\models\User::find()->where(['id'=>$model->other_instructing_person])->one();
if ($result<>null) {
    $otherInstructingPerson = $result->firstname.' '.$result->lastname;
}
// print_r($otherInstructingPerson); die();
?>


<!--  <br pagebreak="true" />-->
<?php if($model->client->id == 183 ){ ?>
    <style>
        td.topbarleft{
            font-weight: bold;
            font-size:12px;
            color:#000000;
            text-align: left
        }
        td.topbarright{
            font-weight: bold;
            font-size:12px;
            color:#000000;
            text-align: right
        }
    </style>

    <table cellspacing="1" cellpadding="4" class="main-class col-12">
        <tr>
            <td class="topbarleft" style="color:#E65100;"><p>Valuation Summary</p></td>
            <td class="topbarright"><p>Report Date: <?= ($model->scheduleInspection->valuation_report_date <> null) ? date('l, jS \of F, Y', strtotime($model->scheduleInspection->valuation_report_date)) : '' ?> </p></td>
        </tr>
    </table>

    <br>
    <table cellspacing="1" cellpadding="4" style="" class="main-class col-12">
        <tr><td class="tableSecondHeading" colspan="4"><h4>General Information</h4></td></tr>
        <tr  class="bggray">
            <td colspan="1" style="font-size:11;">Client’s Name:</td>
            <td colspan="3" style="color:#212121; font-size:11;"><?= $model->client->title ?></td>
        </tr>
        <tr>
            <td colspan="1" style="font-size:11;">Applicant’s Name:</td>
            <td colspan="3" style="color:#212121; font-size:11;"><?= $model->client_name_passport ?>  <?=  $model->client_lastname_passport ?></td>
        </tr>
        <tr  class="bggray">
            <td colspan="1" style="font-size:11;">Reference No:</td>
            <td colspan="3" style="color:#212121; font-size:11;"><?= $model->reference_number ?></td>
        </tr>
        <tr>
            <td colspan="1" style="font-size:11;">Valuation Date:</td>
            <td colspan="3" style="color:#212121; font-size:11;"><?=  date('l, jS \of F, Y', strtotime($detail->valuation_date)) ?></td>
        </tr>
        <tr  class="bggray">
            <td colspan="1" style="font-size:11;">Purpose of Valuation:</td>
            <td colspan="3" style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation] ?></td>
        </tr>


        <tr>
            <td colspan="1" style="font-size:11;">Inspection Date:</td>
            <td colspan="3" style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){
                    ?>
                    <?='Not Applicable as desktop valuation '; ?>
                <?php }
                else if($model->inspection_type == 1){
                    echo date('l, jS \of F, Y', strtotime($detail->valuation_date));
                }
                else{
                    echo date('l, jS \of F, Y', strtotime($model->scheduleInspection->inspection_date));
                } ?>
            </td>
        </tr>
        <!--<tr class="bggray">
            <td colspan="1" style="font-size:11;">Inspected By:</td>
            <td colspan="3" style="color:#212121; font-size:11;"><?php /*if($model->inspection_type == 3){ */?>
                    <?/*='Not Applicable as desktop valuation '; */?>
                <?php /*}else if($model->inspection_type == 1){ */?><?/*='Not known as drive-by valuation '; */?>
                <?php /*}else{ */?><?/*= Yii::$app->appHelperFunctions->staffMemberListArr[$model->scheduleInspection->inspection_officer] */?>
                <?php /*} */?>
            </td>
        </tr>-->


        <tr class="bggray">
            <td colspan="1" style="font-size:11;">Method of Valuation:</td>
            <td colspan="3" style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach] ?></td>
        </tr>
        <tr>
            <td colspan="1"  style="font-size:11;">Status of Valuer:</td>
            <td colspan="3"  style="color:#212121; font-size:11;">External Valuer</td>
        </tr>
        <tr class="bggray">
            <td colspan="1" style="font-size:11;">Previous Involvement:</td>
            <td colspan="3" style="color:#212121; font-size:11;"><?php
                $conflict_text = '';
                if($conflict['related_to_buyer']== 'Yes' || $conflict['related_to_seller']== 'Yes' || $conflict['related_to_property']== 'Yes'){

                    if($conflict['related_to_property']== 'Yes'){
                        $conflict_text .= 'Subject Property, ';
                    }
                    if($conflict['related_to_seller']== 'Yes'){
                        $conflict_text .= 'Seller, ';
                    }

                    if($conflict['related_to_buyer']== 'Yes'){
                        $conflict_text .= 'Buyer';
                    }


                    ?>We have had previous involvement with the <?= $conflict_text; ?> and there might be potential conflicts of interest exist in accepting this instruction.
                <?php }else{ ?>We confirm we have had no previous involvement with the Subject Property and that no conflicts of interest exist in accepting this instruction.
                <?php } ?>

            </td>
        </tr>
    </table>
    <br><br>



    <table cellspacing="1" cellpadding="4" style="" class="main-class col-12">
        <tr>
            <td colspan="4" class="tableSecondHeading"><h4>Property Information</h4></td>
        </tr>
       <!-- <tr  class="bggray">
            <td colspan="1"  style="font-size:11;">Property / Site Address</td>
            <td colspan="3"  style="color:#212121; font-size:11;">Unit Number <?/*= $model->unit_number.', '.$model->building->title */?>
                Plot Number <?/*= $model->plot_number.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] */?>
            </td>
        </tr>-->
        <tr class="bggray">
            <td  colspan="1" style="font-size:11;">Property Address:</td>
            <td  colspan="3" style="color:#212121; font-size:11;">Unit Number <?= $model->unit_number.', '.$model->building->title ?>
                Plot Number <?= $model->plot_number.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
            </td>
        </tr>
        <tr >
            <td  colspan="1" style="font-size:11;">Owner's Name:</td>
            <td  colspan="3" style="color:#212121; font-size:11;"><?= $woners_name ?></td>
        </tr>
    </table>
    <!--<br><br>-->
    <table cellspacing="1" cellpadding="4" style="" class="main-class col-12">
        <tr  class="bggray">
            <td  style="font-size:11;">Property Type:</td>
            <td  style="color:#212121; font-size:11;"><?= $bedroom_html ?> <?= $model->property->title ?></td>
            <td  style="font-size:11;">Property Category:</td>
            <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category] ?></td>
        </tr>
        <tr>
            <td  style="font-size:11;">Total Built Up Area:</td>
            <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->wmFormate($model->inspectProperty->built_up_area) ?> square feet </td>
            <td  style="font-size:11;">Tenure:</td>
            <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
        </tr>
        <tr class="bggray">
            <td  style="font-size:11;">Occupancy Status:</td>
            <td  style="color:#212121; font-size:11;"><?= $model->inspectProperty->occupancy_status ?></td>
            <td  style="font-size:11;">Property Developer’s name:</td>
            <td  style="color:#212121; font-size:11;"><?= $model->inspectProperty->propertyDeveloper->title ?></td>
        </tr>
        <tr>
            <td  style="font-size:11;">Market Value: </td>
            <td  style="color:#212121; font-size:11;"><?= ($estimate_price_byapproverpdf <> null)? "AED ".$estimate_price_byapproverpdf: "Not Applicable" ?></td>
            <td  style="font-size:11;">Insurance Reinstatement Cost:</td>
            <td  style="color:#212121; font-size:11;">Not Applicable</td>
        </tr>
    </table>
   <!-- <br> <br>-->
    <table cellspacing="1" cellpadding="4" style="" class="main-class col-12">
        <tr  class="bggray">
            <td  style="font-size:11;">Property Condition:</td>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><br><?= Yii::$app->appHelperFunctions->propertyConditionListArr[$model->inspectProperty->property_condition] ?>
            </td>
            <td  style="font-size:11;">Property Defect(s):</td>
            <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->getPropertyDefectsArr()[$model->inspectProperty->property_defect] ?></td>
        </tr>
        <tr>
            <td  style="font-size:11;">Green Efficient Certification:</td>
            <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->greenEfficientCertificationArr[$model->inspectProperty->green_efficient_certification] ?></td>
            <td  style="font-size:11;">Documents Not Provided:</td>
            <td  style="color:#212121; font-size:11;"><?= $DocumentByClientpdf['documentNotavail'] ?></td>
        </tr>


        <tr  class="bggray">
            <td  style="font-size:11;">Utilities connected:</td>
            <?php if($land_val == 1){
                ?>
                <td style="color:#212121; font-size:11;">Not Applicable</td>
                <?php
            }else{
                ?>
                <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                    <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                    <?php } ?><?= $model->inspectProperty->utilities_connected ?></td>
                <?php
            } ?>
            <td  style="font-size:11;"></td>
            <td  style="color:#212121; font-size:11;"></td>

        </tr>


    </table>


    <?php
}else{
    ?>
    
    <table cellspacing="1" cellpadding="2" style="" class="main-class col-12" >
        <tr>
            <td colspan="7" class="tableFirstHeading" style="color:#E65100;
       text-align: center;
       font-size:17px; line-height: 20px;
       font-weight:bold; padding-bottom: 5px;"><h4>Executive Valuation Report Summary</h4></td>
        </tr>
        <tr><td colspan="7" ></td></tr>

        <tr>
            <td colspan="7" class="tableSecondHeading"><h4> A. Valuation Overview</h4></td>
        </tr>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Client’s Name</td>
            <td colspan="4" style="color:#212121; font-size:11;  width:287px;"  class="bggray"><?= $model->client->title ?></td>
        </tr>
        <tr class="" >
            <td colspan="3" style="font-size:11;"> Client’s Customer Name</td>
            <td colspan="4" style="color:#212121; font-size:11; width:287px;"  ><?= $model->client_name_passport ?> <?=  $model->client_lastname_passport ?></td>
        </tr>
       <!-- <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Other Intended User(s)</td>
            <td colspan="1" style="font-size:11; width:10px; text-align:center;" class="bggray">:</td>
            <td colspan="3" style="color:#212121; font-size:11;"  class="bggray"><?/*= ( $model->other_intended_users <> null) ? $model->other_intended_users : 'None' */?></td>
        </tr>-->
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Instruction Date</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?php //echo trim( date('l, jS \of F, Y', strtotime($detail->valuation_date))) ?><?=  date('l, jS \of F, Y', strtotime($model->instruction_date)) ?>
        </td>
        </tr>
        <?php if($model->client->id == 5){ ?>
            <tr class="" >
                <td colspan="3" style="font-size:11;"> Valuation Scope</td><?php if($model->purpose_of_valuation == 3){ ?><td  colspan="3" style="color:#212121; font-size:11; width:287px;"  >Fair Value of the Subject Property</td>
                <?php } else{ ?>
                    <td  colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?=  Yii::$app->appHelperFunctions->valuationScopeAjmanArr[$model->valuation_scope] ?></td>
                <?php } ?>

            </tr>
        <?php }else{ ?>
        <tr class="" >
            <td colspan="3" style="font-size:11;"> Valuation Scope</td><?php if($model->purpose_of_valuation == 10){ ?><td  colspan="3" style="color:#212121; font-size:11; width:287px;"  >Estimated Fair Value of the Subject Property</td>
            <?php } else{ ?>
                <td  colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?=  Yii::$app->appHelperFunctions->ScopeOfWorkList[$model->valuation_scope] ?></td>
            <?php } ?>

        </tr>
        <?php } ?>
       <!-- <tr class="bggray" >
            <td colspan="3" style="color:#0277BD; font-size:11;"></td>
            <td colspan="1" style="color:#0277BD; font-size:11; width:10px; text-align:center;" class="bggray"></td>
            <td colspan="3" style="color:#212121; font-size:11;"  class="bggray"></td>
        </tr>-->
        <br/>
        <!-- <div style="padding:0px; margin: 0px; line-height:2px; height:2px;"></div> -->
        
        <tr>
            <td colspan="6" class="tableSecondHeading"><h4> B. Property Description - Location</h4></td>
        </tr>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Property Valued</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].' '.$model->property->title ?></td>
        </tr>
        <tr class="" >
            <td colspan="3" style="font-size:11;"> Project/Building Name</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?= $model->building->title ?></td>
        </tr>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Plot Number</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= $model->plot_number ?></td>
        </tr>
        <tr class="" >
            <td colspan="3" style="font-size:11;"> Unit Number</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;" ><?= $model->unit_number ?></td>
        </tr>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Sub-Community Name</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= $model->building->subCommunities->title ?></td>
        </tr>
        <tr class="" >
            <td colspan="3" style="font-size:11;"> Community Name</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;" ><?= $model->building->communities->title ?></td>
        </tr>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> City and Country</td>
            <?php if($model->quotation_id == 2323){ ?>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>, Saudi Arabia</td>
            <?php }else{ ?>
                <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>, United Arab Emirates</td>
            <?php } ?>
        </tr>
       <!-- <tr class="bggray" >
            <td colspan="3" style="font-size:11;"></td>
            <td colspan="1" style="font-size:11; width:10px; text-align:center;" class="bggray"></td>
            <td colspan="3" style="color:#212121; font-size:11;"  class="bggray"></td>
        </tr>-->
        <br/>

        <tr>
            <td colspan="6" class="tableSecondHeading" ><h4> C. Property Description - External</h4></td>
        </tr>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Tenure</td>
            <?php 
                if($model->client_id == 54514 && $model->tenure == 1){ 
            ?>
                    <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray">Non-Freehold</td>
            <?php
                }
            else{ 
            ?>
                <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
            <?php
            } 
            ?>
        </tr>
        <tr class="" >
            <td colspan="3" style="font-size:11;"> Plot Size</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?= $plot_size ?></td>
        </tr>
        <?php if($model->client->id == 230){ ?>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Built-Up Area</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= number_format($model->inspectProperty->built_up_area,2) ?> square feet <?= $source_bua ?></td>
        </tr>
        <?php if($land_val == 1){ ?>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Permitted Built-Up Area For Land</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= number_format($model->inspectProperty->permitted_bua,2) ?> square feet</td>
        </tr>
        <?php } ?>
        <?php }else{ ?>
            <tr class="bggray" >
                <td colspan="3" style="font-size:11;"> Built-Up Area</td>
                <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= Yii::$app->appHelperFunctions->wmFormate($model->inspectProperty->built_up_area) ?> square feet <?= $source_bua ?></td>
            </tr>
            <?php if($land_val == 1){ ?>
                <tr class="bggray" >
                    <td colspan="3" style="font-size:11;"> Permitted Built-Up Area For Land</td>
                    <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= Yii::$app->appHelperFunctions->wmFormate($model->inspectProperty->permitted_bua) ?> square feet</td>
                </tr>
            <?php } ?>
        <?php } ?>
        <tr class="" >
            <td colspan="3" style="font-size:11;"> Accommodation</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= $bedroom_html ?> 
            </td>
        </tr>
     <!--   <tr class="bggray" >
            <td colspan="3" style="font-size:11;"></td>
            <td colspan="1" style="font-size:11; width:10px; text-align:center;" class="bggray"></td>
            <td colspan="3" style="color:#212121; font-size:11;"  class="bggray"></td>
        </tr>-->
        <br/>

        <tr>
            <td colspan="6" class="tableSecondHeading" ><h4> D. Valuation Details</h4></td>
        </tr>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Inspection Type</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type] ?></td>
        </tr>
        <tr class="" >
            <td colspan="3" style="font-size:11;"> Inspection Date</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?php if($model->inspection_type == 3){?><?='Not Applicable as desktop valuation'; ?><?php } else if($model->inspection_type == 1){ ?><?php if($model->id==9911) { ?><?php echo trim(date('l, jS \of F, Y', strtotime($model->scheduleInspection->inspection_date))); ?><?php } else { ?> <?php echo trim(date('l, jS \of F, Y', strtotime($detail->valuation_date))); ?><?php } ?><?php } else { ?><?php echo trim(date('l, jS \of F, Y', strtotime($model->scheduleInspection->inspection_date))); ?><?php //echo Yii::$app->formatter->asDate($model->scheduleInspection->inspection_date,Yii::$app->params['fulldaydate']) ?><?php } ?>
            </td>
        </tr>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Valuation Date</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?php echo trim( date('l, jS \of F, Y', strtotime($detail->valuation_date))) ?></td>
        </tr>
        <tr class="" >
            <td colspan="3" style="font-size:11;"> Valuation Approach</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?= Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach] ?></td>
        </tr>
        <br/>
        <?php if($model->client->id == 5){ ?>
        <tr>
            <td colspan="6" class="tableSecondHeading"><h4> E. Opinion of Value</h4></td>
        </tr>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> <?= $market_ajman ?> Value</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
        </tr>
        <tr class="" >
            <td colspan="3" style="font-size:11;"> <?= $market_ajman ?> Value Rate</td>
            <?php if($model->client->id == 230){ ?>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf,2).' per square foot built up area': "Not Applicable" ?></td>
            <?php }else{ ?>
                <td colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot built up area': "Not Applicable" ?></td>
            <?php } ?>

        </tr>
        <tr class="bggray" >
            <td colspan="3" style="font-size:11;"> Market Rent</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?>
                <?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?>
                <?php } ?>
            </td>
        </tr>
        <?php if($market != 'Estimated Fair'){ ?>
        <tr class="" >
            <td colspan="3" style="font-size:11;"> Price Under the Restricted Marketing Period of 1 Month</td>
            <td colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?=($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9).'<br/>'.convertNumberToWord($approver_data->estimated_market_value * 0.9). ' Dirhams Only'  : "" ?></td>
        </tr>
        <?php } ?>
        <?php }else{ ?>



            <tr>
                <td colspan="6" class="tableSecondHeading"><h4> E. Estimate of Value</h4></td>
            </tr>
            <tr class="bggray" >
                <td colspan="3" style="font-size:11;"> <?= $market ?> Value</td>
                <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
            </tr>
            <tr class="" >
                <td colspan="3" style="font-size:11;"> <?= $market ?> Value Rate</td>
                <?php if($model->client->id == 230){ ?>
                    <td colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf,2).' per square foot built up area': "Not Applicable" ?></td>
                <?php }else{ ?>
                    <td colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot built up area': "Not Applicable" ?></td>
                <?php } ?>

            </tr>
            <tr class="bggray" >
                <td colspan="3" style="font-size:11;"> Estimated Market Rent</td>
                <td colspan="3" style="color:#212121; font-size:11; width:287px;"  class="bggray"><?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?>
                    <?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?>
                    <?php } ?>
                </td>
            </tr>
            <?php if($market != 'Estimated Fair' && $model->client->id != 110 && $model->client->id != 119087 && $model->client->id != 119951){ ?>
                <tr class="" >
                    <td colspan="3" style="font-size:11;"> Estimated Price Under the Restricted Marketing Period of 1 Month</td>
                    <td colspan="3" style="color:#212121; font-size:11; width:287px;"  ><?=($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9).'<br/>'.convertNumberToWord($approver_data->estimated_market_value * 0.9). ' Dirhams Only'  : "" ?></td>
                </tr>
            <?php } ?>



        <?php } ?>
    </table>
  <!--  <br><br>-->
    
    <?php

}
?>

<br pagebreak="true" />



<div  style=" color:#E65100;
       text-align: center;
       font-size:17px;
       font-weight:bold; margin-top: 10px;
       border: 1px solid #64B5F6;">01. Valuation Overview

</div>
<br>
<table class="main-class col-12 " cellspacing="1" cellpadding="4" style="">

    <tr><td class="tableSecondHeading" colspan="2"><h4>General Details</h4></td></tr>


    <tr class="bggray">
        <td style="font-size:11;">1.01 Windmills Reference</td>
        <td style="color:#212121; font-size:11;"><?= $model->reference_number ?></td>
    </tr>
    <tr >
        <td  style="font-size:11;">1.02 Client Reference </td>
        <td  style="color:#212121; font-size:11;"><?= $model->client_reference ?></td>
    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.03 Client’s Full Name</td>
        <td  style="color:#212121; font-size:11;"><?= $model->client->title ?></td>
    </tr>
    <tr >
        <td  style="font-size:11;">1.04 Other Intended User(s) Name</td>
        <td  style="color:#212121; font-size:11;"><?= ( $model->other_intended_users <> null) ? $model->other_intended_users : 'None' ?></td>
    </tr>

    <tr class="bggray">
        <td  style="font-size:11;">1.05 Valuation Instructions From</td>
        <td  style="color:#212121; font-size:11;"><?= ($otherInstructingPerson <> null) ? $otherInstructingPerson:$instructorname ; //$instructorname ?></td>
    </tr>
    <tr >
        <td  style="font-size:11;">1.06 Valuation Instructions Date</td>
        <td  style="color:#212121; font-size:11;"><?=  date('l, jS \of F, Y', strtotime($model->instruction_date)) ?></td>
    </tr>
    <tr class="bggray">

        <td  style="font-size:11;">1.07 Valuation Scope</td>
        <?php if($model->client->id == 5){ ?>
        <?php if($model->purpose_of_valuation == 3){ ?>

            <td  style="color:#212121; font-size:11;">Fair Value of the Subject Property</td>

        <?php } else{ ?>
            <td  style="color:#212121; font-size:11;"><?=  Yii::$app->appHelperFunctions->valuationScopeAjmanArr[$model->valuation_scope] ?></td>
        <?php } ?>

        <?php } else{ ?>
            <?php if($model->purpose_of_valuation == 3){ ?>

                <td  style="color:#212121; font-size:11;">Estimated Fair Value of the Subject Property</td>

            <?php } else{ ?>
                <td  style="color:#212121; font-size:11;"><?=  Yii::$app->appHelperFunctions->valuationScopeArr[$model->valuation_scope] ?></td>
            <?php } ?>
        <?php } ?>


        <!--<td  style="color:#212121; font-size:11;"><?/*=  Yii::$app->appHelperFunctions->valuationScopeArr[$model->valuation_scope] */?></td>-->
    </tr>
    <tr >
        <td  style="font-size:11;">1.08 Owner’s Full Name</td>
        <td  style="color:#212121; font-size:11;"><?= $woners_name ?></td>
    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.09 Client’s Customer Full Name</td>
        <td  style="color:#212121; font-size:11;"><?= $model->client_name_passport ?> <?=  $model->client_lastname_passport ?></td>
    </tr >
    <tr >
        <td  style="font-size:11;">1.10 Transaction Price</td>
        <td  style="color:#212121; font-size:11;"><?= ($model->costDetails->transaction_price <> null && $model->costDetails->transaction_price != "0.00")? "AED ".number_format($model->costDetails->transaction_price) : "Not Known" ?></td>
    </tr>
    <?php if($model->costDetails->current_transaction_price_available <> null && $model->costDetails->current_transaction_price_available != "No") { ?>
        <tr class="bggray">
            <td  style="font-size:11;">1.11 Transaction Price Rate</td>
            <td  style="color:#212121; font-size:11;"><?= ($model->costDetails->transaction_price <> null && $model->costDetails->transaction_price != "0.00" )? "AED ".number_format((float)($model->costDetails->transaction_price/$model->inspectProperty->built_up_area), 2, '.', '') ." per square foot" : "Not Known"?></td>
        </tr>

        <tr >
            <td  style="font-size:11;">1.12 Transaction Price Date</td>
            <td  style="color:#212121; font-size:11;"><?= ($model->costDetails->current_transaction_price_date <> null)? date('l, jS \of F, Y', strtotime($model->costDetails->current_transaction_price_date)): "Not Known" ?></td>
        </tr>

        <tr class="bggray">
            <td  style="font-size:11;">1.13 Source of Transaction Price</td>
            <td  style="color:#212121; font-size:11;"><?=  ($model->costDetails->source_of_transaction_details <> null) ? Yii::$app->appHelperFunctions->costSource[$model->costDetails->source_of_transaction_details] : "Not Known" ?></td>
        </tr>
    <?php  }else{ ?>

        <tr class="bggray">
            <td  style="font-size:11;">1.11 Transaction Price Rate</td>
            <td  style="color:#212121; font-size:11;">Not Known</td>
        </tr>
        <tr >
            <td  style="font-size:11;">1.12 Transaction Price Date</td>
            <td  style="color:#212121; font-size:11;">Not Known</td>
        </tr>


        <tr class="bggray">
            <td  style="font-size:11;">1.13 Source of Transaction Price</td>
            <td  style="color:#212121; font-size:11;">Not Known</td>
        </tr>
    <?php } ?>
    <tr >
        <td  style="font-size:11;">1.14 Original Purchase Price</td>
        <td  style="color:#212121; font-size:11;"><?= ($model->costDetails->original_purchase_price <> null && $model->costDetails->original_purchase_price != "0.00")? "AED ".number_format($model->costDetails->original_purchase_price) : "Not Known" ?></td>
    </tr>

    <?php if($model->costDetails->original_purchase_price_available <> null && $model->costDetails->original_purchase_price_available != "No") { ?>

        <tr class="bggray">
            <td  style="font-size:11;">1.15 Original Purchase Price Rate</td>
            <td  style="color:#212121; font-size:11;"><?= ($model->costDetails->original_purchase_price <> null && $model->costDetails->original_purchase_price != "0.00") ? "AED ".number_format((float)($model->costDetails->original_purchase_price/$model->inspectProperty->built_up_area), 2, '.', '')." per square foot" : "Not Known" ?>  </td>
        </tr>
        <tr >
            <td  style="font-size:11;">1.16 Source of Original Purchase Price</td>
            <td  style="color:#212121; font-size:11;"><?=  ($model->costDetails->source_of_original_purchase_details <> null) ? Yii::$app->appHelperFunctions->costSource[$model->costDetails->source_of_original_purchase_details] : "Not Known" ?></td>
        </tr>
        <tr class="bggray">
            <td  style="font-size:11;">1.17 Date of Original Purchase Price</td>
            <td  style="color:#212121; font-size:11;"><?= ($model->costDetails->original_purchase_date <> null) ? date('l, jS \of F, Y', strtotime($model->costDetails->original_purchase_date)) : "Not Known"?> </td>
        </tr>
    <?php  }else{ ?>
        <tr class="bggray">
            <td  style="font-size:11;">1.15 Original Purchase Price Rate</td>
            <td  style="color:#212121; font-size:11;">Not Known </td>
        </tr>
        <tr >
            <td  style="font-size:11;">1.16 Source of Original Purchase Price</td>
            <td  style="color:#212121; font-size:11;">Not Known</td>
        </tr>
        <tr class="bggray">
            <td  style="font-size:11;">1.17 Date of Original Purchase Price</td>
            <td  style="color:#212121; font-size:11;">Not Known </td>
        </tr>
    <?php  } ?>
</table>

<br pagebreak="true" />



<table cellspacing="1" cellpadding="4" style="" class="main-class col-12 ">

    <tr><td class="tableSecondHeading" colspan="2"><h4>Property Description - Location Details</h4></td></tr>

    <tr class="bggray">
        <td style="font-size:11;">1.18 Property (Interest) Valued</td>
        <td style="color:#212121; font-size:11;"><?= $model->property->title ?></td>
    </tr>
    <tr>
        <td  style="font-size:11;">1.19 Property Category</td>
        <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category] ?>
        </td>
    </tr>
    <tr class="bggray">
        <?php
        if($land_val == 1){
            $affection_plan_text = ' (Permitted as per Affection plan)';
            $floor_heading = 'Total Building Floor(s) (Permitted Height)';
        }else{
            $floor_heading = 'Total Building Floor(s)';
            $affection_plan_text = '';
        }
        ?>
        <td  style="font-size:11;">1.20 <?= $floor_heading ?></td>
        <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $total_building_floors ?><?= $affection_plan_text ?></td>
    </tr>
    <tr>
        <td  style="font-size:11;">1.21 Floor Number(s)</td>
        <?php if(in_array($model->property_id, [1,12,37,28,17])){
            $floor_text = "Ground floor";
        }else{
            $floor_text = "Not Applicable";
        } ?>
        <td  style="color:#212121; font-size:11;"><?= ($model->floor_number > 0)?  $model->floor_number: $floor_text?></td>
    </tr>

    <tr class="bggray">
        <td  style="font-size:11;">1.22 Property Unit Number</td>
        <td  style="color:#212121; font-size:11;"><?= $model->unit_number ?></td>
    </tr>

    <tr>

        <?php if($land_val == 1){
            $building_text = "Not Applicable";
        }else{
            $building_text = $model->building_number;
        } ?>
        <td  style="font-size:11;">1.23 Building Number</td>
        <td  style="color:#212121; font-size:11;"><?= $building_text ?></td>
    </tr>

    <tr class="bggray">
        <td  style="font-size:11;">1.24 Project Name</td>
        <td  style="color:#212121; font-size:11;"><?= $model->building->title ?></td>
    </tr>

    <!--<tr>
        <td  style="font-size:11;">1.25 Street Number/Name</td>
        <td  style="color:#212121; font-size:11;"><?php /*if($model->inspection_type == 3){*/?><?/*='Not Applicable as desktop valuation. Assumed '; */?>
            <?php /*}else if($model->inspection_type == 1){ */?><?/*='Not known as drive-by valuation. Assumed '; */?>
            <?php /*} */?><?/*= $model->street */?></td>
    </tr>-->
    <tr>
        <td  style="font-size:11;">1.25 Street Number/Name</td>
        <td  style="color:#212121; font-size:11;"><?= $model->street ?></td>
    </tr>
    <tr class="bggray">
        <td style="font-size:11;">1.26 Plot Number</td>
        <td style="color:#212121; font-size:11;"><?= $plot_number ?></td>
    </tr>
    <tr>
        <td  style="font-size:11;">1.27 Makani Number</td>
        <td  style="color:#212121; font-size:11;"><?= $makani_number?></td>
    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.28 Municipality Number</td>
        <td  style="color:#212121; font-size:11;"><?= $municipality_number?></td>
    </tr>
    <tr>
        <td  style="font-size:11;">1.29 Sub Community Name</td>
        <td  style="color:#212121; font-size:11;"><?= $model->building->subCommunities->title ?></td>
    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.30 Community Name</td>
        <td  style="color:#212121; font-size:11;"><?= $model->building->communities->title ?></td>
    </tr>

    <tr>
        <td  style="font-size:11;">1.31 City and Country Name</td>
        <?php if($model->quotation_id == 2323){ ?>
        <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>, Saudi Arabia</td>
        <?php }else{ ?>
        <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>, United Arab Emirates</td>
        <?php } ?>
    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.32 Location Characteristics</td>
        <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><b>Good location</b>
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_highway_drive) ?> drive to highway
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_school_drive) ?> to school
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_mall_drive) ?> to commercial mall
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_sea_drive) ?> to special landmark
            <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_park_drive) ?> drive to pool/park
            <?php if($model->id == 12492){ ?>
            <br>5 minutes drive to metro station
            <?php } ?>
        </td>
    </tr>
    <tr >
        <td  style="font-size:11;">1.33 Location Coordinates</td>
        <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->latitude.','.$model->inspectProperty->longitude ?> (as per Google Maps)</td>
    </tr>

    <tr class="bggray">
        <td  style="font-size:11;">1.34 Placement (Middle / Corner) </td>
        <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Assumed '; ?>
            <?php } ?><?= Yii::$app->appHelperFunctions->propertyPlacementListArr[$model->inspectProperty->property_placement] ?></td>
    </tr>
    <tr>
        <td  style="font-size:11;">1.35 Exposure (Single Row / Back)</td>
        <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Assumed '; ?>
            <?php } ?><?= Yii::$app->appHelperFunctions->propertyExposureListArr[$model->inspectProperty->property_exposure] ?>
        </td>
    </tr>
</table>

<br pagebreak="true" />



<table cellspacing="1" cellpadding="4" style="" class="main-class col-12 ">

    <tr><td class="tableSecondHeading" colspan="2"><h4>Property Description - External Details</h4></td></tr>

    <tr class="bggray">
        <td style="font-size:11;">1.36 Property Type (1E, 2M etc.)</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
        <?php
        }else{
            ?>
            <td style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= $model->inspectProperty->listing_property_type ?></td>
        <?php
        } ?>

    </tr>
    <tr>
        <td  style="font-size:11;">1.37 Development (Standard/Non)</td>
        <?php if($land_val == 1){
        ?>
            <td style="color:#212121; font-size:11;">Not Available</td>
        <?php
        }else{
        ?>
        <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $model->inspectProperty->development_type ?></td>
        <?php
        } ?>


    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.38 Tenure (FH/NFH/Leasehold)</td>
                 <?php 
                if($model->client_id == 54514 && $model->tenure == 1){ 
            ?>
                <td  style="color:#212121; font-size:11;">Non-Freehold</td>
            <?php
                }
            else{ 
            ?>
                <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
            <?php
            } 
            ?>
    </tr>

    <tr>
        <td style="font-size:11;">1.39 Completion Status</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td style="color:#212121; font-size:11;"><?= ($model->inspectProperty->completion_status)==100 ? 'Yes' : 'No'; ?></td>
            <?php
        } ?>
    </tr>

    <tr class="bggray">
        <td  style="font-size:11;">1.40 Completion Percentage</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= $model->inspectProperty->completion_status ?></td>
            <?php
        } ?>

    </tr>

    <tr >
        <td style="font-size:11;">1.41 Property Completion Year</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= ((int)date('Y')) - round($model->inspectProperty->estimated_age).' ' ?>

            </td>
            <?php
        } ?>

    </tr>

    <tr class="bggray">
        <td  style="font-size:11;">1.42 Estimate Age (in years)</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= round($model->inspectProperty->estimated_age).' Years' ?></td>
            <?php
        } ?>

    </tr>
    <tr>
        <td  style="font-size:11;">1.43 Estimated Remaining Life (in years)</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->client->id == 1 && $model->inspectProperty->estimated_remaining_life > 0){ ?><?= ($model->inspectProperty->estimated_remaining_life - 1) . ' - '. ($model->inspectProperty->estimated_remaining_life + 1) ?> Years<?php  }else{ ?><?= round($model->inspectProperty->estimated_remaining_life) ?> Years<?php } ?></td>
            <?php
        } ?>

    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.44  Plot Size (in square feet)</td>
        <td  style="color:#212121; font-size:11;"><?= $plot_size ?></td>
    </tr>

    <?php if($model->client->id == 230){ ?>
     <?php if($land_val == 1){
    ?>

    <tr>
        <td style="font-size:11;">1.45 Permitted Built Up Area (in square feet)</td>
        <td style="color:#212121; font-size:11;"><?= number_format($model->inspectProperty->permitted_bua,2) ?> square feet</td>
    </tr>
    <?php
    }else{
    ?>
    <tr>
        <td style="font-size:11;">1.45 Built Up Area (in square feet)</td>
        <td style="color:#212121; font-size:11;"><?= number_format($model->inspectProperty->built_up_area,2) ?> square feet <?= $source_bua ?></td>
    </tr>
    <?php
    } ?>
    <?php }else{ ?>
      <?php if($land_val == 1){
    ?>

    <tr>
        <td style="font-size:11;">1.45 Permitted Built Up Area (in square feet)</td>
        <td style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->wmFormate($model->inspectProperty->permitted_bua) ?> square feet</td>
    </tr>
    <?php
    }else{
    ?>
    <tr>
        <td style="font-size:11;">1.45 Built Up Area (in square feet)</td>
        <td style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->wmFormate($model->inspectProperty->built_up_area) ?> square feet <?= $source_bua ?></td>
    </tr>
    <?php
    } ?>
    <?php } ?>





    <tr class="bggray">
        <td  style="font-size:11;">1.46 View Type</td>
        <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><br><?= $ViewType['viewCommunity'].$ViewType['viewPool'].$ViewType['viewBurj'].$ViewType['viewSea'].$ViewType['viewMarina'].$ViewType['h'].$ViewType['viewLake'].$ViewType['viewGolfCourse'].$ViewType['viewPark'].$ViewType['viewSpecial'] ?></td>
    </tr>
    <tr>
        <td  style="font-size:11;">1.47 Landscaping Details</td>
        <?php if($land_val == 1){
        ?>
            <td style="color:#212121; font-size:11;">Not Available</td>
        <?php
        }else{
        ?>
        <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= ($model->inspectProperty->landscaping == "No") ? "Not Applicable" :  $model->inspectProperty->landscaping ?></td>
        <?php
        } ?>

    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.48 Parking Spaces</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Available</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= $model->inspectProperty->parking_floors ?></td>
            <?php
        } ?>

    </tr>

    <tr>
        <td  style="font-size:11;">1.49  Building/Community Facilities</td>
        <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $other_facilities ?></td>
    </tr>

    <tr class="bggray">
        <td  style="font-size:11;">1.50 Master Developer’s Full Name</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?= $model->inspectProperty->developer->title ?></td>
            <?php
        } ?>

    </tr>

    <tr >
        <td  style="font-size:11;">1.51 Property Developer’s Full Name</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?= $model->inspectProperty->propertyDeveloper->title ?></td>
            <?php
        } ?>

    </tr>

</table>

<br>
<br>


<table cellspacing="1" cellpadding="4" style="" class="main-class col-12 ">
    <tr><td class="tableSecondHeading" colspan="2"><h4><?= trim('Green Efficient Certification') ?></h4></td></tr>

    <tr class="bggray">
        <td style="font-size:11;">1.52 Green Efficient Certification</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->greenEfficientCertificationArr[$model->inspectProperty->green_efficient_certification] ?></td>
            <?php
        } ?>

    </tr>
    <tr>
        <td style="font-size:11;">1.53 Certifier Name</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->certifierNameArr[$model->inspectProperty->certifier_name] ?></td>
            <?php
        } ?>

    </tr>
    <tr class="bggray">
        <td style="font-size:11;">1.54 Certification Level</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->certificationLevelArr[$model->inspectProperty->certification_level] ?></td>
            <?php
        } ?>

    </tr>
    <tr>
        <td style="font-size:11;">1.55 Source of Green Certificate Information</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->sGCInformationArr[$model->inspectProperty->source_of_green_certificate_information] ?></td>
            <?php
        } ?>

    </tr>

</table>

<br pagebreak="true" />


<table cellspacing="1" cellpadding="4" style="" class="main-class col-12 ">
    <?php if($land_val == 1){
    ?>

    <tr><td class="tableSecondHeading" colspan="2"><h4>Property Description - Internal Details - As Per Building Permission </h4></td></tr>
        <?php
    }else{ ?>

        <tr><td class="tableSecondHeading" colspan="2"><h4>Property Description - Internal Details</h4></td></tr>

        <?php
    } ?>


    <tr>
        <?php if($land_val == 1){
        ?>

        <td style="font-size:11;">1.56 Accommodation</td>
        <td style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not Available as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not Available as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $bedroom_html ?> </td>
            <?php
        }else{ ?>

        <td style="font-size:11;">1.56 Accommodation</td>
        <td style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
            <?php } ?><?= $bedroom_html ?> </td>
        <?php } ?>
    </tr>
    <tr class="bggray">

        <?php if($land_val == 1){
            ?>
             <td  style="font-size:11;">1.57 Number of Floors </td>
           <td  style="color:#212121; font-size:11;"><?= $model->inspectProperty->full_building_floors ?></td>
            <?php
        }else{
            ?>
        <td  style="font-size:11;">1.57 Basement Floor Configuration</td>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= ($floorConfig['basementTitle'] <> null) ? rtrim($floorConfig['basementTitle'],"<br>") : "Not Applicable"; ?> </td>
            <?php
        } ?>



    </tr>

    <?php
    $property='Ground';

    if($model->property_id==1 || $model->property_id == 17 || $model->property_id == 12 || $model->property_id == 14 || $model->property_id == 19 || $model->property_id == 28 || $model->property_id == 37)
    {
        $property='Unit';
    }

    if ($model->property_id != 1 && $model->property_id != 17 && $model->property_id != 12 && $model->property_id != 14 && $model->property_id != 19 && $model->property_id != 28 && $model->property_id != 37) {
        ?>

        <!-- <tr>
        <td  style="font-size:11;">1.49 <?/*= $property */?> Floor Configuration</td>
        <td  style="color:#212121; font-size:11;"><?php /*if($model->inspection_type == 3){*/?><?/*='Not known as desktop valuation. Assumed '; */?>
            <?php /*}else if($model->inspection_type == 1){ */?><?/*='Not known as drive-by valuation. Assumed '; */?>
            <?php /*} */?><br><?/*= ($floorConfig['groundFloor'] <> null) ? rtrim($floorConfig['groundFloor'],"<br>") : "Not Applicable"; */?></td>
      </tr>
      <tr class="bggray">
      <td  style="font-size:11;">1.50 First Floor Configuration</td>
      <td  style="color:#212121; font-size:11;"><?php /*if($model->inspection_type == 3){*/?><?/*='Not known as desktop valuation. Assumed '; */?>
          <?php /*}else if($model->inspection_type == 1){ */?><?/*='Not known as drive-by valuation. Assumed '; */?>
          <?php /*} */?><br><?/*= ($floorConfig['firstFloor'] <> null)? rtrim($floorConfig['firstFloor']) : "Not Applicable" */?></td>
      </tr>
      <tr>
        <td  style="font-size:11;">1.51 Second Floor Configuration</td>
        <td  style="color:#212121; font-size:11;"><?php /*if($model->inspection_type == 3){*/?><?/*='Not known as desktop valuation. Assumed '; */?>
            <?php /*}else if($model->inspection_type == 1){ */?><?/*='Not known as drive-by valuation. Assumed '; */?>
            <?php /*} */?><br><?/*= ($floorConfig['secondFloor'] <> null)?rtrim($floorConfig['secondFloor']): "Not Applicable" */?></td>
      </tr>-->

        <tr>
            <td  style="font-size:11;">1.58 <?= $property ?> Floor Configuration</td>
            <?php if($land_val == 1){
                ?>
                <td style="color:#212121; font-size:11;">Not Applicable</td>
                <?php
            }else{
                ?>
                <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                    <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                    <?php } ?><?= ($floorConfig['groundFloortitle'] <> null) ? rtrim($floorConfig['groundFloortitle'],"<br>") : "Not Applicable"; ?></td>
                <?php
            } ?>

        </tr>
        <tr class="bggray">
            <td  style="font-size:11;">1.59 First Floor Configuration</td>
            <?php if($land_val == 1){
                ?>
                <td style="color:#212121; font-size:11;">Not Applicable</td>
                <?php
            }else{
                ?>
                <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                    <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                    <?php } ?><?= ($floorConfig['firstFloortitle'] <> null)? rtrim($floorConfig['firstFloortitle']) : "Not Applicable" ?></td>
                <?php
            } ?>

        </tr>
        <tr>
            <td  style="font-size:11;">1.60 Second Floor Configuration</td>
            <?php if($land_val == 1){
                ?>
                <td style="color:#212121; font-size:11;">Not Applicable</td>
                <?php
            }else{
                ?>
                <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                    <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                    <?php } ?><?= ($floorConfig['secondFloortitle'] <> null)?rtrim($floorConfig['secondFloortitle']): "Not Applicable" ?></td>
                <?php
            } ?>

        </tr>


    <?php  }
    if ($model->valuationConfiguration->over_all_upgrade>=3 && ($model->property_id == 1 || $model->property_id == 17 || $model->property_id == 12 || $model->property_id == 14 || $model->property_id == 19 || $model->property_id == 28 || $model->property_id == 37)) {
        ?>

        <tr>
            <td  style="font-size:11;">1.58 <?= $property ?> Floor Configuration</td>
            <?php if($land_val == 1){
                ?>
                <td style="color:#212121; font-size:11;">Not Applicable</td>
                <?php
            }else{
                ?>
                <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                    <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                    <?php } ?><?= ($floorConfig['groundFloortitle'] <> null) ?  rtrim($floorConfig['groundFloortitle'],"<br>") : "Not Applicable" ?></td>
                <?php
            } ?>

        </tr>
        <tr class="bggray">
            <td  style="font-size:11;">1.59 First Floor Configuration</td>
            <?php if($land_val == 1){
                ?>
                <td style="color:#212121; font-size:11;">Not Applicable</td>
                <?php
            }else{
                ?>
                <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                    <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                    <?php } ?><?= ($floorConfig['firstFloortitle'] <> null) ? $floorConfig['firstFloortitle'] : "Not Applicable" ?></td>
                <?php
            } ?>

        </tr>
        <tr>
            <td  style="font-size:11;">1.60 Second Floor Configuration</td>
            <?php if($land_val == 1){
                ?>
                <td style="color:#212121; font-size:11;">Not Applicable</td>
                <?php
            }else{
                ?>
                <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                    <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                    <?php } ?><?= ($floorConfig['secondFloortitle']) ? $floorConfig['secondFloortitle'] : "Not Applicable" ?></td>
                <?php
            } ?>

        </tr>

    <?php } ?>

    <tr class="bggray">
        <td  style="font-size:11;">1.61 Upgrade Details</td>

        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><br><?= ($floorConfig['upgrade_text'] <> null) ? $floorConfig['upgrade_text'] : "Not Applicable" ?></td>
            <?php
        } ?>

    </tr>
    <tr>
        <td  style="font-size:11;">1.62 Extensions Details</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= ($model->inspectProperty->extension <> null)? $model->inspectProperty->extension.$source_extension:"None" ?></td>
            <?php
        } ?>

    </tr>


    <tr class="bggray">
        <td style="font-size:11;">1.63 Property Condition/Quality</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><br><?= Yii::$app->appHelperFunctions->propertyConditionListArr[$model->inspectProperty->property_condition] ?></td>
            <?php
        } ?>

    </tr>

    <tr>
        <td  style="font-size:11;">1.64 Property Defect/s</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->getPropertyDefectsArr()[$model->inspectProperty->property_defect] ?></td>
            <?php
        } ?>

    </tr>

    <tr  class="bggray">
        <td  style="font-size:11;">1.65 Furnished</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?=  Yii::$app->appHelperFunctions->reportAttributesValuefurnished[$model->inspectProperty->furnished]   ?></td>
            <?php
        } ?>

    </tr>




    <tr>
        <td  style="font-size:11;">1.66 Swimming Pool </td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= $model->inspectProperty->pool ?></td>
            <?php
        } ?>

    </tr>
    <tr  class="bggray">
        <td  style="font-size:11;">1.67 Cooker Installed</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><br><?= Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->cooker] ?></td>
            <?php
        } ?>

    </tr>

    <tr >
        <td  style="font-size:11;">1.68 Oven Installed</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><br><?=  Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->oven] ?></td>
            <?php
        } ?>

    </tr>
    <tr  class="bggray">
        <td  style="font-size:11;">1.69 Fridge Installed</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><br><?= Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->fridge]  ?></td>
            <?php
        } ?>

    </tr>
    <tr>
        <td  style="font-size:11;">1.70 Washing Machine Installed</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><br><?=  Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->washing_machine] ?></td>
            <?php
        } ?>

    </tr>
    <?php
    if(!empty($model->inspectProperty->ac_type)){
        $actypes = explode(',', ($model->inspectProperty->ac_type <> null) ? $model->inspectProperty->ac_type : "");
    }
    ?>
    <tr  class="bggray">
        <td  style="font-size:11;">1.71 Central Air-conditioning</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php }

                if($actypes <> null && !empty($actypes)){
                    if (in_array(3, $actypes))
                    {
                        echo "Yes";
                    }else{
                        echo "No";
                    }
                }else{
                    echo "No";
                }
                ?>
            </td>
            <?php
        } ?>

    </tr>
    <tr>
        <td  style="font-size:11;">1.72 Split Air-conditioning Units</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php }
                if($actypes <> null && !empty($actypes)){
                    if (in_array(1, $actypes))
                    {
                        echo "Yes";
                    }else{
                        echo "No";
                    }
                }else{
                    echo "No";
                }


                ?></td>
            <?php
        } ?>

    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.73 Window Air-conditioning Unit</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php }

                if($actypes <> null && !empty($actypes)){
                    if (in_array(2, $actypes))
                    {
                        echo "Yes";
                    }else{
                        echo "No";
                    }
                }else{
                    echo "No";
                }
                ?>
            </td>
            <?php
        } ?>

    </tr>
    <tr>
        <td  style="font-size:11;">1.74 Utilities connected</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= $model->inspectProperty->utilities_connected ?></td>
            <?php
        } ?>

    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.75 Tenancies / Occupancy Status</td>
        <?php if($land_val == 1){
            ?>
            <td style="color:#212121; font-size:11;">Not Applicable</td>
            <?php
        }else{
            ?>
            <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= $model->inspectProperty->occupancy_status ?></td>
            <?php
        } ?>

    </tr>
</table>



<br pagebreak="true" />


<table cellspacing="1" cellpadding="4" style="" class="main-class col-12 ">
    <tr><td class="tableSecondHeading" colspan="2"><h4>Valuation Details</h4></td></tr>


    <tr class="bggray">
        <td style="font-size:11;">1.76 Inspection Type Instructed</td>
        <td style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type] ?></td>
    </tr>
    <tr >
        <td  style="font-size:11;">1.77 Purpose of the Valuation</td>
        <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation] ?></td>
    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.78 Inspecting Officer</td>
        <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){ ?> <?='Not Applicable as desktop valuation '; ?>
            <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation '; ?>
            <?php }else{ ?><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->scheduleInspection->inspection_officer] ?>
            <?php } ?>
        </td>
    </tr>
    <tr >
        <td  style="font-size:11;">1.79 Service Officer</td>
        <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->service_officer_name] ?>

        </td>
    </tr>

    <tr class="bggray">
        <td  style="font-size:11;">1.80 Inspection Date</td>
        <td  style="color:#212121; font-size:11;"><?php if($model->inspection_type == 3){?><?='Not Applicable as desktop valuation '; ?>
            <?php }else if($model->inspection_type == 1){?><?php if($model->id==9911) { ?><?= Yii::$app->formatter->asDate($model->scheduleInspection->inspection_date,Yii::$app->params['fulldaydate']) ?><?php } else { ?> <?= Yii::$app->formatter->asDate($detail->valuation_date,Yii::$app->params['fulldaydate']) ?><?php } ?>
            <?php }else{?><?= Yii::$app->formatter->asDate($model->scheduleInspection->inspection_date,Yii::$app->params['fulldaydate']) ?>
            <?php } ?>
        </td>
    </tr>
    <tr >

        <td  style="font-size:11;">1.81 Valuation Date</td>
        <td  style="color:#212121; font-size:11;"><?= trim(Yii::$app->formatter->asDate($detail->valuation_date,Yii::$app->params['fulldaydate'])) ?></td>
    </tr>

    <!-- <tr  class="bggray">
            <td style="font-size:11;">1.72 Documents not Provided </td>
            <td style="color:#212121; font-size:11;"><?/*= $DocumentByClient['documentAvail'] */?></td>
        </tr>-->
    <tr  class="bggray">
        <td style="font-size:11;">1.82 Documents Provided </td>
        <td style="color:#212121; font-size:11;"><?= $DocumentByClient['documentAvail'] ?></td>
    </tr>
    <tr >
        <td style="font-size:11;">1.83 Documents not Provided </td>
        <td style="color:#212121; font-size:11;"><?= $DocumentByClient['documentNotavail'] ?></td>
    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.84 Basis of Value</td>
        <?php if($model->purpose_of_valuation == 3){ ?>
            <td  style="color:#212121; font-size:11;">Fair Value</td>
        <?php } else{ ?>
            <td  style="color:#212121; font-size:11;"><?= $model->property->basis_of_value ?></td>
        <?php } ?>

        <!--<td  style="color:#212121; font-size:11;"><?/*= $model->property->basis_of_value */?></td>-->
    </tr>
    <tr>
        <td  style="font-size:11;">1.85 Valuation Approach</td>
        <td  style="color:#212121; font-size:11;"><?= Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach] ?></td>
    </tr>
    <tr class="bggray">
        <td  style="font-size:11;">1.86 Approach Reasoning</td>
        <td  style="color:#212121; font-size:11;"><?= $model->property->approach_reason ?></td>
    </tr>


    <?php


    $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $model->id, 'type' => 'sold','search_type'=>0])->all(), 'id', 'selected_id');
    $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $model->id, 'type' => 'list','search_type'=>0])->all(), 'id', 'selected_id');
    $selected_data_prvious = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $model->id, 'type' => 'previous','search_type'=>0])->all(), 'id', 'selected_id');
    $sold = 0;
    $list = 0;
    $previous = 0;
    if($selected_data_sold <> null && count($selected_data_sold) > 0) {
    $sold = 1;
    $curl_handle=curl_init();
    curl_setopt($curl_handle, CURLOPT_URL,\yii\helpers\Url::toRoute(['valuation/step_12_report','id'=>$model->id]));
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

    $val = curl_exec($curl_handle);
    curl_close($curl_handle);

    $response_sold = json_decode($val);
    /*echo "<pre>";
    print_r($response_sold->adjustments);
    die('dd');*/
    }


    if($selected_data_list <> null && count($selected_data_list) > 0) {
    $list = 1;

    $curl_handle=curl_init();
    curl_setopt($curl_handle, CURLOPT_URL,\yii\helpers\Url::toRoute(['valuation/step_15_report','id'=>$model->id]));
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

    $val = curl_exec($curl_handle);
    curl_close($curl_handle);

    $response_list = json_decode($val);

    /* echo "<pre>";
     print_r($response_list);
     die;*/
    }

    ?>

    <tr>
        <td  style="font-size:11;">1.87 Valuation Adjustments</td>
        <td  style="color:#212121; font-size:11;">Sold Transactions:<br/><span style="font-size: 11px;"><?= (isset($response_sold->adjustments) && $response_sold->adjustments <> null) ? $response_sold->adjustments : '' ?></span>
            <br/>
            <br/>
            Market Comparable Listings:<br/><span style="font-size: 11px;"><?= (isset($response_list->adjustments) && $response_list->adjustments <> null) ? $response_list->adjustments : '' ?> </span>
        </td>
    </tr>
</table>

<br />
<br />
<?php if($model->client->id == 5){ ?>
<table cellspacing="1" cellpadding="4" style="" class="main-class col-12 ">
    <tr><?php if($land_val == 1){ ?><td class="tableSecondHeading" colspan="2"><h4>Opinion Of Value</h4></td>
        <?php } else { ?>
            <td class="tableSecondHeading" colspan="2"><h4>Opinion of Value Details</h4></td>
        <?php
        } ?>
    </tr>

    <tr>

        <td style="font-size:11;">1.88 <?= $market_ajman ?> Value</td>

        <td style="color:#212121; font-size:11;"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
    </tr>
    <?php if(in_array($model->property_id, array(4,5,11,23,26,29,39))){ ?>
    <tr class="bggray">

        <td  style="font-size:11;">1.89 <?= $market_ajman ?> Value Rate (BUA)</td>
        <?php if($model->client->id == 230){ ?>
        <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf,2).' per square foot built up area': "Not Applicable" ?> </td>
        <?php }else{ ?>
        <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot built up area': "Not Applicable" ?> </td>
        <?php } ?>
    </tr>
    <tr>
        <td style="font-size:11;">1.90 Market Value Rate (Plot Area)</td>
        <?php if($model->client->id == 230){ ?>
        <td style="color:#212121; font-size:11;"><?= ($approver_data->market_value_sqf_pa <> null) ? "AED " . number_format($approver_data->market_value_sqf_pa,2) . ' per square foot' : "Not Applicable" ?> </td>
        <?php }else{ ?>
        <td style="color:#212121; font-size:11;"><?= ($approver_data->market_value_sqf_pa <> null) ? "AED " . number_format($approver_data->market_value_sqf_pa) . ' per square foot' : "Not Applicable" ?> </td>
        <?php } ?>
    </tr>
    <tr >
        <td style="font-size:11;">1.91 Market Rent</td>
        <td style="color:#212121; font-size:11;">
            <?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?><?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?><?php } ?></td>
    </tr>
    <tr >
        <td style="font-size:11;">1.92 Parking Market Value</td>
        <td style="color:#212121; font-size:11;">
            <?= ($approver_data->parking_market_value <> null)? "AED ".number_format($approver_data->parking_market_value,2): "Not Applicable" ?>
        </td>

    </tr>
    <?php if($model->id != 8430){ ?>
    <tr class="bggray">

        <td style="font-size:11;">1.91 Price Under the Restricted Marketing Period of 1 Month </td>
        <td style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9).'<br/>'.convertNumberToWord($approver_data->estimated_market_value * 0.9). ' Dirhams Only'   : "" ?> </td>
    </tr>
    <?php } ?>
        <?php
    $number= '1.93';
    }else if(($model->client->land_valutaion == 1) && (round($model->inspectProperty->estimated_age) >= $model->client->land_age) && ($model->tenure == 1) && ($model->property_id == 6 || $model->property_id == 22 || $model->property_id == 10 || $model->property_id == 2) ){ ?>
    <tr class="bggray">
        <td  style="font-size:11;">1.89 <?= $market_ajman ?> Value Rate</td>
        <?php if($model->client->id == 230){ ?>
        <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf,2).' per square foot built up area': "Not Applicable" ?> </td>
        <?php }else{ ?>
        <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot built up area': "Not Applicable" ?> </td>
        <?php } ?>
    </tr>
    <tr>

        <td style="font-size:11;">1.90 Market Rent</td>
        <td style="color:#212121; font-size:11;"><?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?><?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?><?php } ?></td>
    </tr>
    <?php if($model->id != 8430){ ?>
    <tr class="bggray">
        <td style="font-size:11;">1.91 Price Under the Restricted Marketing Period of 1 Month</td>
        <td style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9).'<br/>'.convertNumberToWord($approver_data->estimated_market_value * 0.9). ' Dirhams Only'   : "" ?> </td>
    </tr>
    <?php } ?>

    <tr>
        <td style="font-size:11;">1.92 Parking Market Value</td>
        <td style="color:#212121; font-size:11;">
            <?= ($approver_data->parking_market_value <> null)? "AED ".number_format($approver_data->parking_market_value,2): "Not Applicable" ?>
        </td>

        <td style="font-size:11;">1.93 <?= $market_ajman ?> Value of Land</td>


        <td style="color:#212121; font-size:11;"><?= ($estimate_price_land_byapprover <> null)? "AED ".$estimate_price_land_byapprover: "Not Applicable" ?></td>
    </tr>


    <?php
    $number= '1.94';}else { ?>
    <tr class="bggray">

        <td  style="font-size:11;">1.89 <?= $market_ajman ?> Value Rate</td>
        <?php if($model->client->id == 230){ ?>
        <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf,2).' per square foot built up area': "Not Applicable" ?> </td>
        <?php }else{ ?>
        <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot built up area': "Not Applicable" ?> </td>
        <?php } ?>
    </tr>

    <tr>

        <td style="font-size:11;">1.90 Market Rent</td>
        <td style="color:#212121; font-size:11;"><?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?><?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?><?php } ?></td>
    </tr>
    <?php if($model->id != 8430){ ?>
    <tr class="bggray">

        <td style="font-size:11;">1.91 Price Under the Restricted Marketing Period of 1 Month</td>
        <td style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9).'<br/>'.convertNumberToWord($approver_data->estimated_market_value * 0.9). ' Dirhams Only'   : "" ?> </td>
    </tr>
    <?php } ?>
    <tr>
        <td style="font-size:11;">1.92 Parking Market Value</td>
        <td style="color:#212121; font-size:11;">
            <?= ($approver_data->parking_market_value <> null)? "AED ".number_format($approver_data->parking_market_value): "Not Applicable" ?>
        </td>
    </tr>

    <?php
    $number= '1.93';
    }
    $special_assumptions='';
    //special Assumptions Occupancy, Tananted, Vacant, Acquisition
    //  $special_assumptions.= '<br><b>Occupancy Status</b><br>';
    $special_assumptionreport = \app\models\SpecialAssumptionreport::find()->one();



    if($model->inspectProperty->occupancy_status == "Vacant"){
    $special_assumptions.=$special_assumptionreport->vacant;
    }

    if($model->inspectProperty->occupancy_status == "Owner Occupied"){
    $special_assumptions.=$special_assumptionreport->owner_occupied;
    }

    if($model->inspectProperty->occupancy_status == "Tenanted"){
    $special_assumptions.=$special_assumptionreport->tenanted;
    }


    if($model->inspectProperty->acquisition_method == 1 || $model->inspectProperty->acquisition_method == 2 ){
    $special_assumptions.= '<br>'.$special_assumptionreport->gifted_granted;
    }else if($model->inspectProperty->acquisition_method == 4){
    $special_assumptions.= '<br>'.$special_assumptionreport->inherited;

    }else{
    if(trim($special_assumptionreport->purchased) != 'None' && trim($special_assumptionreport->purchased) != '') {
    $special_assumptions .= '<br>' .$special_assumptionreport->purchased.'<br>';
    }


    }

    if (($detail->valuation_date > $model->scheduleInspection->inspection_date) && ($model->inspection_type != 3)) {
    $valuationdate_assumption= \app\models\GeneralAsumption::find()->where(['id'=>5])->one();
    $special_assumptions .= '<br>' .$valuationdate_assumption->general_asumption.'<br>';

    }
    if (($detail->valuation_date < $model->scheduleInspection->inspection_date) && ($model->inspection_type != 3)) {
    $special_assumptions .= "<br>For purposes of this valuation and based on your instruction to conduct a retrospective property valuation, our valuation report is based on a special assumption that any changes or developments occurring between the valuation date and the inspection date are considered to have had no material impact on the property's value as of the specified valuation date. In obtaining our opinion of value, we have relied on market data as of the specified valuation date. Additionally, we have assumed that physical attributes of the property, observed during the inspection date accurately reflect the conditions that existed on the valuation date.<br>";

    }
    if($model->special_assumption !== 'None' &&  ($model->special_assumption <> null)) {
    $special_assumptions.= trim($model->special_assumption);
    }
    ?>
    <tr>
        <td  style="font-size:11;"><?= $number ?> Special Assumptions & Concerns</td>
        <td  style="color:#212121; font-size:11;"><?php if($special_assumptions <> null ){echo $special_assumptions;}else{echo 'None';}?></td>
    </tr>
</table>
<?php }else{ ?>

<table cellspacing="1" cellpadding="4" style="" class="main-class col-12 ">
    <tr><?php if($land_val == 1){ ?><td class="tableSecondHeading" colspan="2"><h4>Opinion Of Value</h4></td>
        <?php } else { ?>
            <td class="tableSecondHeading" colspan="2"><h4>Estimate of Value Details</h4></td>
        <?php
        } ?>
    </tr>

    <tr>

        <td style="font-size:11;">1.88 <?= $market ?> Value</td>

        <td style="color:#212121; font-size:11;"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
    </tr>
    <?php if(in_array($model->property_id, array(4,5,11,23,26,29,39))){ ?>
        <tr class="bggray">

            <td  style="font-size:11;">1.89 <?= $market ?> Value Rate (BUA)</td>
            <?php if($model->client->id == 230){ ?>
            <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf,2).' per square foot built up area': "Not Applicable" ?> </td>
            <?php }else{ ?>
            <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot built up area': "Not Applicable" ?> </td>
             <?php } ?>
        </tr>
        <tr>
            <td style="font-size:11;">1.90 Market Value Rate (Plot Area)</td>
            <?php if($model->client->id == 230){ ?>
            <td style="color:#212121; font-size:11;"><?= ($approver_data->market_value_sqf_pa <> null) ? "AED " . number_format($approver_data->market_value_sqf_pa,2) . ' per square foot' : "Not Applicable" ?> </td>
            <?php }else{ ?>
            <td style="color:#212121; font-size:11;"><?= ($approver_data->market_value_sqf_pa <> null) ? "AED " . number_format($approver_data->market_value_sqf_pa) . ' per square foot' : "Not Applicable" ?> </td>
            <?php } ?>
        </tr>
        <tr >
            <td style="font-size:11;">1.91 Estimated Market Rent</td>
            <td style="color:#212121; font-size:11;">
                <?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?><?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?><?php } ?></td>
        </tr>
    <tr >
        <td style="font-size:11;">1.92 Estimated Parking Market Value</td>
        <td style="color:#212121; font-size:11;">
            <?= ($approver_data->parking_market_value <> null)? "AED ".number_format($approver_data->parking_market_value,2): "Not Applicable" ?>
        </td>

    </tr>
    <?php if($model->id != 8430 && $model->client->id != 110 && $model->client->id != 119087 && $model->client->id != 119951){ ?>
        <tr class="bggray">

            <td style="font-size:11;">1.91 Estimated Price Under the Restricted Marketing Period of 1 Month </td>
            <td style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9).'<br/>'.convertNumberToWord($approver_data->estimated_market_value * 0.9). ' Dirhams Only'   : "" ?> </td>
        </tr>
    <?php } ?>
        <?php
        $number= '1.93';
    }else if(($model->client->land_valutaion == 1) && (round($model->inspectProperty->estimated_age) >= $model->client->land_age) && ($model->tenure == 1) && ($model->property_id == 6 || $model->property_id == 22 || $model->property_id == 10 || $model->property_id == 2) ){ ?>
        <tr class="bggray">
            <td  style="font-size:11;">1.89 <?= $market ?> Value Rate</td>
            <?php if($model->client->id == 230){ ?>
            <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf,2).' per square foot built up area': "Not Applicable" ?> </td>
            <?php }else{ ?>
            <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot built up area': "Not Applicable" ?> </td>
            <?php } ?>
        </tr>
        <tr>

            <td style="font-size:11;">1.90 Market Rent</td>
            <td style="color:#212121; font-size:11;"><?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?><?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?><?php } ?></td>
        </tr>
    <?php if($model->id != 8430 && $model->client->id != 110 && $model->client->id != 119087 && $model->client->id != 119951){ ?>
        <tr class="bggray">
            <td style="font-size:11;">1.91 Estimated Price Under the Restricted Marketing Period of 1 Month</td>
            <td style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9).'<br/>'.convertNumberToWord($approver_data->estimated_market_value * 0.9). ' Dirhams Only'   : "" ?> </td>
        </tr>
    <?php } ?>
    <?php if($model->client->id == 110 ||  $model->client->id == 119087){ ?>
        <tr>
            <td style="font-size:11;">1.91 Estimated Parking Market Value</td>
            <td style="color:#212121; font-size:11;">
                <?= ($approver_data->parking_market_value <> null)? "AED ".number_format($approver_data->parking_market_value,2): "Not Applicable" ?>
            </td>

            <td style="font-size:11;">1.92 <?= $market ?> Value of Land</td>


            <td style="color:#212121; font-size:11;"><?= ($estimate_price_land_byapprover <> null)? "AED ".$estimate_price_land_byapprover: "Not Applicable" ?></td>
        </tr>
    <?php
    $number= '1.93';
    }else{ ?>
    <tr>
        <td style="font-size:11;">1.92 Estimated Parking Market Value</td>
        <td style="color:#212121; font-size:11;">
            <?= ($approver_data->parking_market_value <> null)? "AED ".number_format($approver_data->parking_market_value,2): "Not Applicable" ?>
        </td>

        <td style="font-size:11;">1.93 <?= $market ?> Value of Land</td>


        <td style="color:#212121; font-size:11;"><?= ($estimate_price_land_byapprover <> null)? "AED ".$estimate_price_land_byapprover: "Not Applicable" ?></td>
    </tr>
     <?php
     $number= '1.94';
    } ?>


        <?php
       // $number= '1.94';

            }else { ?>
        <tr class="bggray">

            <td  style="font-size:11;">1.89 <?= $market ?> Value Rate</td>
            <?php if($model->client->id == 230){ ?>
            <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf,2).' per square foot built up area': "Not Applicable" ?> </td>
            <?php }else{ ?>
            <td  style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot built up area': "Not Applicable" ?> </td>
            <?php } ?>
        </tr>

        <tr>

            <td style="font-size:11;">1.90 Market Rent</td>
            <td style="color:#212121; font-size:11;"><?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?><?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?><?php } ?></td>
        </tr>
    <?php if($model->id != 8430 && $model->client->id != 110 && $model->client->id != 119087 && $model->client->id != 119951){ ?>
        <tr class="bggray">

            <td style="font-size:11;">1.91 Estimated Price Under the Restricted Marketing Period of 1 Month</td>
            <td style="color:#212121; font-size:11;"><?= ($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9).'<br/>'.convertNumberToWord($approver_data->estimated_market_value * 0.9). ' Dirhams Only'   : "" ?> </td>
        </tr>
    <?php } ?>
     <?php if($model->client->id == 110 ||  $model->client->id == 119087){ ?>
    <tr>
        <td style="font-size:11;">1.91 Estimated Parking Market Value</td>
        <td style="color:#212121; font-size:11;">
            <?= ($approver_data->parking_market_value <> null)? "AED ".number_format($approver_data->parking_market_value): "Not Applicable" ?>
        </td>
    </tr>
    <?php
    $number= '1.92';
     }else{ ?>
    <tr>
        <td style="font-size:11;">1.92 Estimated Parking Market Value</td>
        <td style="color:#212121; font-size:11;">
            <?= ($approver_data->parking_market_value <> null)? "AED ".number_format($approver_data->parking_market_value): "Not Applicable" ?>
        </td>
    </tr>
    <?php
    $number= '1.93';
     } ?>


        <?php
        //$number= '1.93';
    }
    $special_assumptions='';
    //special Assumptions Occupancy, Tananted, Vacant, Acquisition
    //  $special_assumptions.= '<br><b>Occupancy Status</b><br>';
    $special_assumptionreport = \app\models\SpecialAssumptionreport::find()->one();



    if($model->inspectProperty->occupancy_status == "Vacant"){
        $special_assumptions.=$special_assumptionreport->vacant;
    }

    if($model->inspectProperty->occupancy_status == "Owner Occupied"){
        $special_assumptions.=$special_assumptionreport->owner_occupied;
    }

    if($model->inspectProperty->occupancy_status == "Tenanted"){
        $special_assumptions.=$special_assumptionreport->tenanted;
    }


    if($model->inspectProperty->acquisition_method == 1 || $model->inspectProperty->acquisition_method == 2 ){
        $special_assumptions.= '<br>'.$special_assumptionreport->gifted_granted;
    }else if($model->inspectProperty->acquisition_method == 4){
        $special_assumptions.= '<br>'.$special_assumptionreport->inherited;

    }else{
        if(trim($special_assumptionreport->purchased) != 'None' && trim($special_assumptionreport->purchased) != '') {
            $special_assumptions .= '<br>' .$special_assumptionreport->purchased.'<br>';
        }


    }

    if (($detail->valuation_date > $model->scheduleInspection->inspection_date) && ($model->inspection_type != 3)) {
        $valuationdate_assumption= \app\models\GeneralAsumption::find()->where(['id'=>5])->one();
        $special_assumptions .= '<br>' .$valuationdate_assumption->general_asumption.'<br>';

    }
    if (($detail->valuation_date < $model->scheduleInspection->inspection_date) && ($model->inspection_type != 3)) {
    $special_assumptions .= "<br>For purposes of this valuation and based on your instruction to conduct a retrospective property valuation, our valuation report is based on a special assumption that any changes or developments occurring between the valuation date and the inspection date are considered to have had no material impact on the property's value as of the specified valuation date. In obtaining our opinion of value, we have relied on market data as of the specified valuation date. Additionally, we have assumed that physical attributes of the property, observed during the inspection date accurately reflect the conditions that existed on the valuation date.<br>";

    }
    if($model->special_assumption !== 'None' &&  ($model->special_assumption <> null)) {
        $special_assumptions.= trim($model->special_assumption);
    }
    ?>
    <tr>
        <td  style="font-size:11;"><?= $number ?> Special Assumptions & Concerns</td>
        <td  style="color:#212121; font-size:11;"><?php if($special_assumptions <> null ){echo $special_assumptions;}else{echo 'None';}?></td>
    </tr>
</table>
<?php } ?>

