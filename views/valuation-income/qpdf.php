<?php


//same as TOE

$VAT =0;

$VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);

$finalFeePayable = $netValuationFee+$VAT;
//End same as TOE
$clientAddress = \app\models\Company::find()->select(['address'])->where(['id'=>$model->client_id])->one();
$fee_to_words     = yii::$app->quotationHelperFunctions->numberTowords($finalFeePayable);
?>




<style>
    .box1{
        background-color: #e6e6e6;
        width: 100px;
        padding-top: 20px !important;
        border-right: 5px white;
        border-left: 5px white;
    }

    .border {
        width: 100%;
        border-top-width: 0.05px;
        border-bottom-width: 0.05px;
        border-left-width: 0.05px;
        border-right-width: 0.05px;
        padding: 5px;
    }
    .font_color{
        color: #3763ae;
    }

</style>


<table class="border" width="547">

    <tr>
        <td colspan="5" class="detailtexttext first-section" >
            <br><span class="font_color" ><?= $branch_address->company; ?> </span>
            <br><span><?= $branch_address->address; ?></span>
            <br><span><?= $branch_address->office_phone; ?></span>
            <br><span>finance@windmillsgroup.com, serviceteam@windmillsgroup.com </span>
            <br><span><?= $branch_address->website; ?></span>
            <br><span>TRN: <?= $branch_address->trn_number; ?></span>
        </td>


        <td colspan="3" class="detailtexttext first-section">
            <br><span class="font_color">Bank Details: </span>
            <br><span>Emirates NBD </span>
            <br><span>Al Quoz Branch  </span>
            <br><span>Account No: 1015286631901 </span>
            <br><span>IBAN: <?= $branch_address->iban_number; ?> </span>
            <br><span>Swift code: EBILAEAD</span>
        </td>
    </tr>

</table><br><br>

<table>
    <thead>
    <tr>
        <td colspan="1" class="detailtexttext first-section" style="width: 30%">
            <br><span class="size-8 airal-family font_color">Client</span>

            <br><span><?php

                if(	$model->client_invoice_type == 1){
                ?><?= $model->client_name_passport;?></span>
            <?php }else{ ?><?= $model->client->title ?></span>
            <?php } ?>



            <br><span><?= $clientAddress->address ?></span>
            <?php
            if ($model->client->id !=9167) {?>
                <br><span>UAE.</span>
                <?php
            }
            ?>
            <?php if($model->client->trn_number <> null ){ ?>
                <br>TRN: <span><?= $model->client->trn_number ?></span>
                <?php
            }
            ?>

        </td>

        <td class="box1 text-dec airal-family" style="margin-top: 20px; padding-top: 20px!important;top:50px; width: 18%">
            <span style="color: #000000;"><p><span style="font-weight: bold; font-size: 10px">Invoice Number</span>
                    <br>
                    <span style="padding-top: 10px;font-size: 9px; margin-top: 10px; line-height: 20px"><?= Yii::$app->appHelperFunctions->getInvoiceNumber($model);
                        ?><?= $postfix_invoice_number ?></span></p>
            </span>
        </td>
        <td class="box1 text-dec airal-family" style="margin-top: 20px; padding-top: 20px!important;top:50px;width: 18%">
            <span style="color: #000000;"><p><span style="font-weight: bold; font-size: 10px">Issue Date</span>
                    <br>
                    <span style="padding-top: 10px;font-size: 9px; margin-top: 10px; line-height: 20px"><?= date('d-M-Y', strtotime($model->submission_approver_date)) ?></span></p>
            </span>
        </td>
        <td class="box1 text-dec airal-family" style="margin-top: 20px; padding-top: 20px!important;top:50px;width: 18%">
            <span style="color: #000000;"><p><span style="font-weight: bold; font-size: 10px">Payable Fee</span>
                    <br>
                    <span style="padding-top: 10px;font-size: 9px; margin-top: 10px; line-height: 20px"><?= Yii::$app->appHelperFunctions->wmFormateno($finalFeePayable) ?></span></p>
            </span>
        </td>
        <td class="box1 text-dec airal-family" style="margin-top: 20px; padding-top: 20px!important;top:50px;width: 18%">
            <span style="color: #000000;"><p><span style="font-weight: bold; font-size: 10px">Due Date</span>
                    <br>
                    <span style="padding-top: 10px;font-size: 9px; margin-top: 10px; line-height: 20px">


                    </span></p>
            </span>
        </td>

    </tr>
    </thead>
</table><br><br>
<br>






<br>

<table class="border" cellspacing="1" cellpadding="3" width="466">



    <tr  style="color:#3763ae ; border: 1px solid !important;">
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:18%;">&nbsp;&nbsp;Instruction Date</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold;width:15%">WM Reference</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:17% ">Client Reference</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; ;width:15%">Customer</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:10% ">Property-</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:17% ">Community</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:12% ">City</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:14% ">Fee AED</td>
    </tr>
</table>
<table  cellspacing="1" cellpadding="3" width="466" style=" padding: 5px;">
  <tbody>
  <?php
  $i=1;
  if($valuations <> null && !empty($valuations)) {
      foreach($valuations as $key => $valuation){

          ?>
          <tr style="background-color: ;">
              <td  style="font-size:8.5px;width:18%">&nbsp;&nbsp;<?= date('d-M-Y',strtotime($valuation->instruction_date))  ?></td>
              <td  style="font-size:8.5px;width:15%"><?= $valuation->reference_number; ?></td>
              <td  style=" font-size:9px; width:17%"><?= $valuation->client_reference; ?></td>
              <td  style=" font-size:9px; width:15%"><?= $valuation->client_name_passport; ?></td>
              <td  style="font-size:9px;width:10% "><?= $valuation->property->title; ?></td>
              <td  style="font-size:9px;width:17% "><?= $valuation->building->subCommunities->title ?></td>
              <td  style="font-size:9px;width:12% "><?= Yii::$app->appHelperFunctions->emiratedListArr[$valuation->building->city]; ?></td>
              <td  style="font-size:9px;width:14% "><?= Yii::$app->appHelperFunctions->wmFormate($netValuationFee); ?></td>

          </tr>




          <?php

      }
  }
  ?>

  </tbody>
</table>
<br><br>
<table cellspacing="1" cellpadding="3" width="466">
    <thead>
    <tr style="background-color: ;">
        <td class="airal-family" style="font-size:10px; text-align:left;">Total Fee </td>
    </tr>
    </thead>
</table>
<table class="border airal-family" cellspacing="1" cellpadding="5" width="547">
    <tbody>
    <?php
    if($model <> null && !empty($model)) {

        ?>
        <tr style="line-height: 7px">
            <td  style="font-size:9px;text-align:left;font-weight:bold">Gross Fee</td>
            <td colspan="5"  style=" font-size:9px; text-align: center;"></td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px;font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($netValuationFee); ?></td>


        </tr>
        <tr style="line-height: 7px">
            <td  style="font-size:9px;text-align:left;font-weight:bold">Discount</td>
            <td colspan="5"  style=" font-size:9px; text-align: center;font-weight:bold"></td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($discount); ?></td>

        </tr>
        <tr style="line-height: 7px">
            <td  style="font-size:9px;text-align:left;font-weight:bold">Net Fee</td>
            <td colspan="5"  style=" font-size:9px; text-align: center;"></td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px;font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($netValuationFee); ?></td>


        </tr>
        <tr style="line-height: 7px">
            <td  style="font-size:9px;text-align:left;font-weight:bold">VAT (5%)</td>
            <td colspan="5"  style=" font-size:9px; text-align: center;"></td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($VAT); ?></td>


        </tr>
        <tr style="line-height: 7px">
            <td  style="font-size:9px;text-align:left;font-weight:bold" >Fee + VAT</td>

            <td colspan="5"  style=" font-size:9px; text-align: center;font-weight:bold">&nbsp;<?= $fee_to_words." only." ?></td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormateno($finalFeePayable); ?></td>
        </tr>


    <?php }
    ?>

    </tbody>
</table>







<br><br>
<table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <tr style="background-color: ;">
      <td class="airal-family" style="font-size:10px; text-align:left;">We sincerely thank you for giving us this privilege to work for you. </td>

    </tr>
  </thead>
</table>


<br>
<br>
<br>
<br>
<br>


<style>
  .airal-family{
    font-family: Arial, Helvetica, sans-serif;
  }
</style>


<style>

  .text-dec{
    font-size: 10px;
    text-align: center;
      padding-right: 10px;
  }

/*td.detailtext{*/
/*background-color:#BDBDBD; */
/*font-size:9px;*/
/*border-top: 0.4px solid grey;*/
/*}*/

td.detailtexttext{
/* background-color:#EEEEEE; */
font-size:10px;
    font-weight: 100;

}
th.detailtext{
/* background-color:#BDBDBD; */
font-size:9px;
border-top: 1px solid black;
}
/*.border {
border: 1px solid black;
}*/

th.amount-heading{
  color: #039BE5;
}
td.amount-heading{
  color: #039BE5;
}
th.total-due{
  font-size: 16px;
}
td.total-due{
  font-size: 16px;
}
span.size-8{
  font-size: 10px;
  font-weight: bold;
}
td.first-section{
  padding-left: 10px;
/* background-color: black; */
}
</style>

<?php


function convertNumberToWord($num = false)
{
    $num = str_replace(array(',', ' '), '', trim($num));
    if (!$num) {
        return false;
    }
    $num = (int) $num;
    $words = array();
    $list1 = array(
        '',
        'One',
        'Two',
        'Three',
        'Four',
        'Five',
        'Six',
        'Seven',
        'Eight',
        'Nine',
        'Ten',
        'Eleven',
        'Twelve',
        'Thirteen',
        'Fourteen',
        'Fifteen',
        'Sixteen',
        'Seventeen',
        'Eighteen',
        'Nineteen'
    );
    $list2 = array('', 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety', 'Hundred');
    $list3 = array(
        '',
        'Thousand',
        'Million',
        'Billion',
        'Trillion',
        'Quadrillion',
        'Quintillion',
        'Sextillion',
        'Septillion',
        'Octillion',
        'Nonillion',
        'Decillion',
        'Undecillion',
        'Duodecillion',
        'Tredecillion',
        'Quattuordecillion',
        'Quindecillion',
        'Sexdecillion',
        'Septendecillion',
        'Octodecillion',
        'Novemdecillion',
        'Vigintillion'
    );
    $num_length = strlen($num);
    $levels = (int) (($num_length + 2) / 3);
    $max_length = $levels * 3;
    $num = substr('00' . $num, -$max_length);
    $num_levels = str_split($num, 3);
    for ($i = 0; $i < count($num_levels); $i++) {
        $levels--;
        $hundreds = (int) ($num_levels[$i] / 100);
        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ' ' : '');
        $tens = (int) ($num_levels[$i] % 100);
        $singles = '';
        if ($tens < 20) {
            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
        } else {
            $tens = (int) ($tens / 10);
            $tens = ' ' . $list2[$tens] . ' ';
            $singles = (int) ($num_levels[$i] % 10);
            $singles = ' ' . $list1[$singles] . ' ';
        }
        $words[] = $hundreds . $tens . $singles . (($levels && (int) ($num_levels[$i])) ? ' ' . $list3[$levels] . ' ' : '');
    } //end for loop
    $commas = count($words);
    if ($commas > 1) {
        $commas = $commas - 1;
    }
    return implode(' ', $words);
}
?>