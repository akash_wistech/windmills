<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */

$this->title = Yii::t('app', 'Valuation');
$cardTitle = Yii::t('app', 'Update Valuation:  {nameAttribute}', [
    'nameAttribute' => $model->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="valuation-update">


    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
