<?php if($model->valuation_approach == 1 && $model->id >  13860){?>

<style>
    .fade {
        opacity: 1;
    }

    .nav-tabs.flex-column .nav-link.active {
        background-color: #007bff;
        color: white;
        font-weight: bold;
    }
    .disabled-link {
        pointer-events: none;
        cursor:pointer;
        cursor: not-allowed !important;
    }
    .parent_cat_income{
        background-color: #d1d1e0;
        font-weight: 600;
        color: #007bff !important;
        text-decoration: none;
        cursor: pointer;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .child_cat_income{
        padding-left: 40px;
        display: none;
    }
</style>
<?php

$stype = 0;
if(isset($_GET['stype'])){
    $stype = $_GET['stype'];
}
?>
<style>
    .st_c{
        display: none;
    }
    .one_bedroom_c{
        display: none;
    }
    .two_bedroom_c{
        display: none;
    }
    .three_bedroom_c{
        display: none;
    }
    .four_bedrooms_c{
        display: none;
    }
    .penthouse_c{
        display: none;
    }
    .shops_c{
        display: none;
    }
    .offices_c{
        display: none;
    }
    .warehouse_c{
        display: none;
    }
</style>
<?php
if($stype == 1){
    ?>
    <style>
        .st_c{
            display: block;
        }

    </style>
<?php
}?>

<?php
if($stype == 2){
    ?>
    <style>
        .one_bedroom_c{
            display: block;
        }
    </style>
<?php
}?>
<?php
if($stype == 3){
    ?>
    <style>
        .two_bedroom_c{
            display: block;
        }
    </style>
<?php
}?>
<?php
if($stype == 4){
    ?>
    <style>
        .three_bedroom_c{
            display: block;
        }
    </style>
<?php
}?>

<?php
if($stype == 5){
    ?>
    <style>
        .four_bedrooms_c{
            display: block;
        }
    </style>
<?php
}?>
<?php
if($stype == 6){
    ?>
    <style>
        .penthouse_c{
            display: block;
        }
    </style>
<?php
}?>

<?php
if($stype == 7){
    ?>
    <style>
        .shops_c{
            display: block;
        }
    </style>
<?php
}?>
<?php
if($stype == 8){
    ?>
    <style>
        .offices_c{
            display: block;
        }
    </style>
<?php
}?>
    <?php
    if($stype == 9){
        ?>
        <style>
            .warehouse_c{
                display: block;
            }
        </style>
        <?php
    }?>

<div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
     aria-orientation="vertical">
    <?php if(Yii::$app->user->identity->location_status == 0){ ?>
        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) { ?>
            <a class="nav-link <?= ($step == 1) ? 'active' : '' ?>" href="valuation/step_1/<?= $model->id ?>"
               aria-selected="false">Enter Valuation Instruction Details</a>
        <?php } ?>
        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) { ?>
            <a class="nav-link <?= ($step == 101) ? 'active' : '' ?>" href="valuation/step_101/<?= $model->id ?>"
               aria-selected="false">Enter Property Details</a>
        <?php } ?>


        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_2')) { ?>
            <a class="nav-link <?= ($step == 2) ? 'active' : '' ?>" href="valuation/step_2/<?= $model->id ?>"
               aria-selected="false">Enter Documents Details</a>
        <?php } ?>

        <a class="nav-link <?= ($step == 401) ? 'active' : '' ?>" href="valuation/step_401/<?= $model->id ?>"
           aria-selected="false">Enter Valuation Details</a>

        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_4')) { ?>
            <a class="nav-link <?= ($step == 4) ? 'active' : '' ?>" href="valuation/step_4/<?= $model->id ?>"
               aria-selected="false"> Enter Inspection Schedule</a>
        <?php } ?>
            <!-- <?php /*if(Yii::$app->menuHelperFunction->checkActionAllowed('step_4')) { */?>
        <a class="nav-link <?/*= ($step == 401) ? 'active' : '' */?>" href="valuation/step_401/<?/*= $model->id */?>"
           aria-selected="false">Enter Valuation Details</a>
    --><?php /*} */?>

        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_5')) { ?>
            <a class="nav-link <?= ($step == 5) ? 'active' : '' ?>" href="valuation/step_5/<?= $model->id ?>"
               aria-selected="false">Enter Inspected Property Details</a>
            <a class="nav-link <?= ($step == 502) ? 'active' : '' ?>" href="valuation/step_502/<?= $model->id ?>"
               aria-selected="false">Enter Green Certification</a>
            <a class="nav-link <?= ($step == 503) ? 'active' : '' ?>" href="valuation/step_503/<?= $model->id ?>"
               aria-selected="false">Travel Details</a>

        <?php } ?>
        <?php if($model->valuation_approach == 1){ ?>
            <a class="nav-link <?= ($step == 504) ? 'active' : '' ?>" href="valuation/step_504/<?= $model->id ?>"
               aria-selected="false">Comparable Properties Details</a>
        <?php } ?>
        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_5')) { ?>
            <a class="nav-link <?= ($step == 501) ? 'active' : '' ?>" href="valuation/step_501/<?= $model->id ?>"
               aria-selected="false">Enter Public Data</a>
        <?php } ?>

        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_6')) { ?>
            <a class="nav-link  <?= ($step == 6) ? 'active' : '' ?>" href="valuation/step_6/<?= $model->id ?>"
               aria-selected="false">Enter Property Configuration Details</a>
        <?php } ?>

        <?php if($model->purpose_of_valuation == 14){ ?>
            <a class="nav-link  <?= ($step == '5_0') ? 'active' : '' ?>" href="valuation/step_5_0/<?= $model->id ?>"
               aria-selected="false">Major Assets/Equipment</a>
        <?php } ?>
        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_7')) { ?>
            <a class="nav-link  <?= ($step == 7) ? 'active' : '' ?>" href="valuation/step_7/<?= $model->id ?>"
               aria-selected="false">Enter Cost
                Details</a>
        <?php } ?>
            <!--  <?php /*if(Yii::$app->menuHelperFunction->checkActionAllowed('step_8')) { */?>
    <a class="nav-link  <?/*= ($step == 8) ? 'active' : '' */?>" href="valuation/step_8/<?/*= $model->id */?>"
       aria-selected="false">Developer Margin</a>
    --><?php /*} */?>
        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_9')) { ?>
            <a class="nav-link  <?= ($step == 9) ? 'active' : '' ?>" href="valuation/step_9/<?= $model->id ?>"
               aria-selected="false">Assumption/Summary</a>
        <?php } ?>
        <a class="nav-link  <?= ($step == 701) ? 'active' : '' ?>" href="valuation/client-provided-rents?id=<?= $model->id ?>"
           aria-selected="false">Client Provided Rents</a>

        <?php
        if($model->valuation_approach == 1) { ?>

            <?php if ($model->inspectProperty->no_studios > 0) { ?>

                <a class="nav-link parent_cat_income st_p"
                   aria-selected="false">Calculations For Studio </a>
                <a class="nav-link active st_c" style="background-color:#4f5962;" aria-selected="false">Maxima Auto Calculations
                </a>

                <a class="nav-link child_cat_income st_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '10_v' && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_10_v?id=<?= $model->id ?>&stype=1"
                   aria-selected="false">Auto Ejari-Listings</a>
                <a class="nav-link child_cat_income st_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '12_v' && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_12_v?id=<?= $model->id ?>&stype=1"
                   aria-selected="false">Auto Ejari-Val-Calculations</a>

                <a class="nav-link child_cat_income st_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '13_v' && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_13_v?id=<?= $model->id ?>&stype=1"
                   aria-selected="false">Auto Market Rent Comparables</a>
                <a class="nav-link child_cat_income st_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '15_v' && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_15_v?id=<?= $model->id ?>&stype=1"
                   aria-selected="false">Auto Market Rent Comparables-Val-Calculations</a>

                <a class="nav-link active st_c" style="background-color:#4f5962;" aria-selected="false">Reviewer Calculations
                </a>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_10') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income st_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '10' && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_10?id=<?= $model->id ?>&stype=1"
                       aria-selected="false">Enquiry Ejari-Listings</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income st_c <?= ($step == 110 && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_110?id=<?= $model->id ?>&stype=1"
                       aria-selected="false">Ejari-Listings (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income st_c <?= ($step == 120 && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_120?id=<?= $model->id ?>&stype=1"
                       aria-selected="false">Derive Ejari Listings (R)</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_13') && $model->valuation_approach != 2  ) { ?>
                    <a class="nav-link child_cat_income st_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '13' && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_13?id=<?= $model->id ?>&stype=1"
                       aria-selected="false">Enquiry Market Rent Comparables</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income st_c <?= ($step == 140 && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_140?id=<?= $model->id ?>&stype=1"
                       aria-selected="false">Market Rent Comparables (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income st_c  <?= ($step == 150 && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_150?id=<?= $model->id ?>&stype=1"
                       aria-selected="false">Derive Market Rent Comparables (R)</a>
                <?php } ?>
                <a class="nav-link active st_c" style="background-color:#4f5962;" aria-selected="false">Approver Calculations
                </a>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income st_c <?= ($step == 11 && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_11?id=<?= $model->id ?>&stype=1"
                       aria-selected="false">Ejari-Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income st_c <?= ($step == 12 && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_12?id=<?= $model->id ?>&stype=1"
                       aria-selected="false">Derive Ejari Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income st_c <?= ($step == 14 && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_14?id=<?= $model->id ?>&stype=1"
                       aria-selected="false">Market Rent Comparables (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income st_c <?= ($step == 15 && $stype == '1') ? 'active' : '' ?>" href="valuation-income/step_15?id=<?= $model->id ?>&stype=1"
                       aria-selected="false">Derive Market Rent Comparables (A)</a>
                <?php } ?>

            <?php } ?>

            <?php if ($model->inspectProperty->no_one_bedrooms > 0) { ?>
              
                <a class="nav-link parent_cat_income one_bedroom_p"
                   aria-selected="false">Auto Calculations For One Bedroom </a>
                <a class="nav-link active one_bedroom_c" style="background-color:#4f5962;" aria-selected="false">Maxima Auto Calculations
                </a>

                <a class="nav-link child_cat_income one_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '10_v' && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_10_v?id=<?= $model->id ?>&stype=2"
                   aria-selected="false">Auto Ejari-Listings</a>
                <a class="nav-link child_cat_income one_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '12_v' && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_12_v?id=<?= $model->id ?>&stype=2"
                   aria-selected="false">Auto Ejari-Val-Calculations</a>

                <a class="nav-link child_cat_income one_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '13_v' && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_13_v?id=<?= $model->id ?>&stype=2"
                   aria-selected="false">Auto Market Rent Comparables</a>
                <a class="nav-link child_cat_income one_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '15_v' && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_15_v?id=<?= $model->id ?>&stype=2"
                   aria-selected="false">Auto Market Rent Comparables-Val-Calculations</a>


                <a class="nav-link active one_bedroom_c" style="background-color:#4f5962;" aria-selected="false">Reviewer Calculations
                </a>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_10') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income one_bedroom_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '10' && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_10?id=<?= $model->id ?>&stype=2"
                       aria-selected="false">Enquiry Ejari-Listings</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income one_bedroom_c <?= ($step == 110 && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_110?id=<?= $model->id ?>&stype=2"
                       aria-selected="false">Ejari-Listings (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income one_bedroom_c <?= ($step == 120 && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_120?id=<?= $model->id ?>&stype=2"
                       aria-selected="false">Derive Ejari Listings (R)</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_13') && $model->valuation_approach != 2  ) { ?>
                    <a class="nav-link child_cat_income one_bedroom_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '13' && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_13?id=<?= $model->id ?>&stype=2"
                       aria-selected="false">Enquiry Market Rent Comparables</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income one_bedroom_c <?= ($step == 140 && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_140?id=<?= $model->id ?>&stype=2"
                       aria-selected="false">Market Rent Comparables (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income one_bedroom_c  <?= ($step == 150 && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_150?id=<?= $model->id ?>&stype=2"
                       aria-selected="false">Derive Market Rent Comparables (R)</a>
                <?php } ?>
                <a class="nav-link active one_bedroom_c" style="background-color:#4f5962;" aria-selected="false">Approver Calculations
                </a>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income one_bedroom_c <?= ($step == 11 && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_11?id=<?= $model->id ?>&stype=2"
                       aria-selected="false">Ejari-Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income one_bedroom_c <?= ($step == 12 && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_12?id=<?= $model->id ?>&stype=2"
                       aria-selected="false">Derive Ejari Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income one_bedroom_c <?= ($step == 14 && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_14?id=<?= $model->id ?>&stype=2"
                       aria-selected="false">Market Rent Comparables (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income one_bedroom_c <?= ($step == 15 && $stype == '2') ? 'active' : '' ?>" href="valuation-income/step_15?id=<?= $model->id ?>&stype=2"
                       aria-selected="false">Derive Market Rent Comparables (A)</a>
                <?php } ?>






            <?php } ?>

            <?php if ($model->inspectProperty->no_two_bedrooms > 0) { ?>
                <a class="nav-link parent_cat_income two_bedroom_p"
                   aria-selected="false">Auto Calculations For Two Bedroom </a>
                <a class="nav-link active two_bedroom_c" style="background-color:#4f5962;" aria-selected="false">Maxima Auto Calculations
                </a>
       <a class="nav-link child_cat_income two_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '10_v' && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_10_v?id=<?= $model->id ?>&stype=3"
                   aria-selected="false">Auto Ejari-Listings</a>
                <a class="nav-link child_cat_income two_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '12_v' && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_12_v?id=<?= $model->id ?>&stype=3"
                   aria-selected="false">Auto Ejari-Val-Calculations</a>

                <a class="nav-link child_cat_income two_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '13_v' && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_13_v?id=<?= $model->id ?>&stype=3"
                   aria-selected="false">Auto Market Rent Comparables</a>
                <a class="nav-link child_cat_income two_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '15_v' && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_15_v?id=<?= $model->id ?>&stype=3"
                   aria-selected="false">Auto Market Rent Comparables-Val-Calculations</a>
                <a class="nav-link active two_bedroom_c" style="background-color:#4f5962;" aria-selected="false">Reviewer Calculations
                </a>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_10') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income two_bedroom_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '10' && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_10?id=<?= $model->id ?>&stype=3"
                       aria-selected="false">Enquiry Ejari-Listings</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income two_bedroom_c <?= ($step == 110 && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_110?id=<?= $model->id ?>&stype=3"
                       aria-selected="false">Ejari-Listings (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income two_bedroom_c <?= ($step == 120 && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_120?id=<?= $model->id ?>&stype=3"
                       aria-selected="false">Derive Ejari Listings (R)</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_13') && $model->valuation_approach != 2  ) { ?>
                    <a class="nav-link child_cat_income two_bedroom_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '13' && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_13?id=<?= $model->id ?>&stype=3"
                       aria-selected="false">Enquiry Market Rent Comparables</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income two_bedroom_c <?= ($step == 140 && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_140?id=<?= $model->id ?>&stype=3"
                       aria-selected="false">Market Rent Comparables (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income two_bedroom_c  <?= ($step == 150 && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_150?id=<?= $model->id ?>&stype=3"
                       aria-selected="false">Derive Market Rent Comparables (R)</a>
                <?php } ?>
                <a class="nav-link active two_bedroom_c" style="background-color:#4f5962;" aria-selected="false">Approver Calculations
                </a>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income two_bedroom_c <?= ($step == 11 && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_11?id=<?= $model->id ?>&stype=3"
                       aria-selected="false">Ejari-Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income two_bedroom_c <?= ($step == 12 && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_12?id=<?= $model->id ?>&stype=3"
                       aria-selected="false">Derive Ejari Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income two_bedroom_c <?= ($step == 14 && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_14?id=<?= $model->id ?>&stype=3"
                       aria-selected="false">Market Rent Comparables (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income two_bedroom_c <?= ($step == 15 && $stype == '3') ? 'active' : '' ?>" href="valuation-income/step_15?id=<?= $model->id ?>&stype=3"
                       aria-selected="false">Derive Market Rent Comparables (A)</a>
                <?php } ?>


            <?php } ?>

            <?php if ($model->inspectProperty->no_three_bedrooms > 0) { ?>
                <a class="nav-link parent_cat_income three_bedroom_p"
                   aria-selected="false">Auto Calculations For Three Bedroom </a>

                <a class="nav-link active three_bedroom_c" style="background-color:#4f5962;" aria-selected="false">Maxima Auto Calculations
                </a>
                <a class="nav-link child_cat_income three_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '10_v' && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_10_v?id=<?= $model->id ?>&stype=4"
                   aria-selected="false">Auto Ejari-Listings</a>
                <a class="nav-link child_cat_income three_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '12_v' && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_12_v?id=<?= $model->id ?>&stype=4"
                   aria-selected="false">Auto Ejari-Val-Calculations</a>

                <a class="nav-link child_cat_income three_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '13_v' && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_13_v?id=<?= $model->id ?>&stype=4"
                   aria-selected="false">Auto Market Rent Comparables</a>
                <a class="nav-link child_cat_income three_bedroom_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '15_v' && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_15_v?id=<?= $model->id ?>&stype=4"
                   aria-selected="false">Auto Market Rent Comparables-Val-Calculations</a>

                <a class="nav-link active three_bedroom_c" style="background-color:#4f5962;" aria-selected="false">Reviewer Calculations
                </a>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_10') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income three_bedroom_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '10' && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_10?id=<?= $model->id ?>&stype=4"
                       aria-selected="false">Enquiry Ejari-Listings</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income three_bedroom_c <?= ($step == 110 && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_110?id=<?= $model->id ?>&stype=4"
                       aria-selected="false">Ejari-Listings (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income three_bedroom_c <?= ($step == 120 && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_120?id=<?= $model->id ?>&stype=4"
                       aria-selected="false">Derive Ejari Listings (R)</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_13') && $model->valuation_approach != 2  ) { ?>
                    <a class="nav-link child_cat_income three_bedroom_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '13' && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_13?id=<?= $model->id ?>&stype=4"
                       aria-selected="false">Enquiry Market Rent Comparables</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income three_bedroom_c <?= ($step == 140 && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_140?id=<?= $model->id ?>&stype=4"
                       aria-selected="false">Market Rent Comparables (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income three_bedroom_c  <?= ($step == 150 && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_150?id=<?= $model->id ?>&stype=4"
                       aria-selected="false">Derive Market Rent Comparables (R)</a>
                <?php } ?>
                <a class="nav-link active three_bedroom_c" style="background-color:#4f5962;" aria-selected="false">Approver Calculations
                </a>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income three_bedroom_c <?= ($step == 11 && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_11?id=<?= $model->id ?>&stype=4"
                       aria-selected="false">Ejari-Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income three_bedroom_c <?= ($step == 12 && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_12?id=<?= $model->id ?>&stype=4"
                       aria-selected="false">Derive Ejari Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income three_bedroom_c <?= ($step == 14 && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_14?id=<?= $model->id ?>&stype=4"
                       aria-selected="false">Market Rent Comparables (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income three_bedroom_c <?= ($step == 15 && $stype == '4') ? 'active' : '' ?>" href="valuation-income/step_15?id=<?= $model->id ?>&stype=4"
                       aria-selected="false">Derive Market Rent Comparables (A)</a>
                <?php } ?>

            <?php } ?>

            <?php if ($model->inspectProperty->no_four_bedrooms > 0) { ?>
                <a class="nav-link parent_cat_income four_bedrooms_p"
                   aria-selected="false">Auto Calculations For Four Bedroom </a>
                <a class="nav-link active four_bedrooms_c" style="background-color:#4f5962;" aria-selected="false">Maxima Auto Calculations
                </a>

                <a class="nav-link child_cat_income four_bedrooms_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '10_v' && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_10_v?id=<?= $model->id ?>&stype=5"
                   aria-selected="false">Auto Ejari-Listings</a>
                <a class="nav-link child_cat_income four_bedrooms_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '12_v' && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_12_v?id=<?= $model->id ?>&stype=5"
                   aria-selected="false">Auto Ejari-Val-Calculations</a>

                <a class="nav-link child_cat_income four_bedrooms_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '13_v' && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_13_v?id=<?= $model->id ?>&stype=5"
                   aria-selected="false">Auto Market Rent Comparables</a>
                <a class="nav-link child_cat_income four_bedrooms_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '15_v' && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_15_v?id=<?= $model->id ?>&stype=5"
                   aria-selected="false">Auto Market Rent Comparables-Val-Calculations</a>
                <a class="nav-link active four_bedrooms_c" style="background-color:#4f5962;" aria-selected="false">Reviewer Calculations
                </a>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_10') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income four_bedrooms_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '10' && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_10?id=<?= $model->id ?>&stype=5"
                       aria-selected="false">Enquiry Ejari-Listings</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income four_bedrooms_c <?= ($step == 110 && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_110?id=<?= $model->id ?>&stype=5"
                       aria-selected="false">Ejari-Listings (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income four_bedrooms_c <?= ($step == 120 && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_120?id=<?= $model->id ?>&stype=5"
                       aria-selected="false">Derive Ejari Listings (R)</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_13') && $model->valuation_approach != 2  ) { ?>
                    <a class="nav-link child_cat_income four_bedrooms_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '13' && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_13?id=<?= $model->id ?>&stype=5"
                       aria-selected="false">Enquiry Market Rent Comparables</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income four_bedrooms_c <?= ($step == 140 && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_140?id=<?= $model->id ?>&stype=5"
                       aria-selected="false">Market Rent Comparables (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income four_bedrooms_c  <?= ($step == 150 && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_150?id=<?= $model->id ?>&stype=5"
                       aria-selected="false">Derive Market Rent Comparables (R)</a>
                <?php } ?>
                <a class="nav-link active four_bedrooms_c" style="background-color:#4f5962;" aria-selected="false">Approver Calculations
                </a>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income four_bedrooms_c <?= ($step == 11 && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_11?id=<?= $model->id ?>&stype=5"
                       aria-selected="false">Ejari-Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income four_bedrooms_c <?= ($step == 12 && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_12?id=<?= $model->id ?>&stype=5"
                       aria-selected="false">Derive Ejari Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income four_bedrooms_c <?= ($step == 14 && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_14?id=<?= $model->id ?>&stype=5"
                       aria-selected="false">Market Rent Comparables (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income four_bedrooms_c <?= ($step == 15 && $stype == '5') ? 'active' : '' ?>" href="valuation-income/step_15?id=<?= $model->id ?>&stype=5"
                       aria-selected="false">Derive Market Rent Comparables (A)</a>
                <?php } ?>

            <?php } ?>

            <?php if ($model->inspectProperty->no_penthouse > 0) { ?>
                <a class="nav-link parent_cat_income penthouse_p"
                   aria-selected="false">Auto Calculations For Penthouse </a>

                <a class="nav-link active penthouse_c" style="background-color:#4f5962;" aria-selected="false">Maxima Auto Calculations
                </a>
                <a class="nav-link child_cat_income penthouse_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '10_v' && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_10_v?id=<?= $model->id ?>&stype=6"
                   aria-selected="false">Auto Ejari-Listings</a>
                <a class="nav-link child_cat_income penthouse_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '12_v' && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_12_v?id=<?= $model->id ?>&stype=6"
                   aria-selected="false">Auto Ejari-Val-Calculations</a>

                <a class="nav-link child_cat_income penthouse_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '13_v' && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_13_v?id=<?= $model->id ?>&stype=6"
                   aria-selected="false">Auto Market Rent Comparables</a>
                <a class="nav-link child_cat_income penthouse_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '15_v' && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_15_v?id=<?= $model->id ?>&stype=6"
                   aria-selected="false">Auto Market Rent Comparables-Val-Calculations</a>
                <a class="nav-link active penthouse_c" style="background-color:#4f5962;" aria-selected="false">Reviewer Calculations
                </a>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_10') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income penthouse_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '10' && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_10?id=<?= $model->id ?>&stype=6"
                       aria-selected="false">Enquiry Ejari-Listings</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income penthouse_c <?= ($step == 110 && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_110?id=<?= $model->id ?>&stype=6"
                       aria-selected="false">Ejari-Listings (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income penthouse_c <?= ($step == 120 && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_120?id=<?= $model->id ?>&stype=6"
                       aria-selected="false">Derive Ejari Listings (R)</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_13') && $model->valuation_approach != 2  ) { ?>
                    <a class="nav-link child_cat_income penthouse_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '13' && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_13?id=<?= $model->id ?>&stype=6"
                       aria-selected="false">Enquiry Market Rent Comparables</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income penthouse_c <?= ($step == 140 && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_140?id=<?= $model->id ?>&stype=6"
                       aria-selected="false">Market Rent Comparables (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income penthouse_c  <?= ($step == 150 && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_150?id=<?= $model->id ?>&stype=6"
                       aria-selected="false">Derive Market Rent Comparables (R)</a>
                <?php } ?>
                <a class="nav-link active penthouse_c" style="background-color:#4f5962;" aria-selected="false">Approver Calculations
                </a>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income penthouse_c <?= ($step == 11 && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_11?id=<?= $model->id ?>&stype=6"
                       aria-selected="false">Ejari-Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income penthouse_c <?= ($step == 12 && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_12?id=<?= $model->id ?>&stype=6"
                       aria-selected="false">Derive Ejari Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income penthouse_c <?= ($step == 14 && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_14?id=<?= $model->id ?>&stype=6"
                       aria-selected="false">Market Rent Comparables (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income penthouse_c <?= ($step == 15 && $stype == '6') ? 'active' : '' ?>" href="valuation-income/step_15?id=<?= $model->id ?>&stype=6"
                       aria-selected="false">Derive Market Rent Comparables (A)</a>
                <?php } ?>

            <?php } ?>

            <?php if ($model->inspectProperty->no_of_shops > 0) { ?>
                <a class="nav-link parent_cat_income shops_p"
                   aria-selected="false">Auto Calculations For Shop </a>
                <a class="nav-link active shops_c" style="background-color:#4f5962;" aria-selected="false">Maxima Auto Calculations
                </a>

               <a class="nav-link child_cat_income shops_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '10_v' && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_10_v?id=<?= $model->id ?>&stype=7"
                   aria-selected="false">Auto Ejari-Listings</a>
                <a class="nav-link child_cat_income shops_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '12_v' && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_12_v?id=<?= $model->id ?>&stype=7"
                   aria-selected="false">Auto Ejari-Val-Calculations</a>

                <a class="nav-link child_cat_income shops_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '13_v' && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_13_v?id=<?= $model->id ?>&stype=7"
                   aria-selected="false">Auto Market Rent Comparables</a>
                <a class="nav-link child_cat_income shops_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '15_v' && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_15_v?id=<?= $model->id ?>&stype=7"
                   aria-selected="false">Auto Market Rent Comparables-Val-Calculations</a>

                <a class="nav-link active shops_c" style="background-color:#4f5962;" aria-selected="false">Reviewer Calculations
                </a>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_10') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income shops_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '10' && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_10?id=<?= $model->id ?>&stype=7"
                       aria-selected="false">Enquiry Ejari-Listings</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income shops_c <?= ($step == 110 && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_110?id=<?= $model->id ?>&stype=7"
                       aria-selected="false">Ejari-Listings (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income shops_c <?= ($step == 120 && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_120?id=<?= $model->id ?>&stype=7"
                       aria-selected="false">Derive Ejari Listings (R)</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_13') && $model->valuation_approach != 2  ) { ?>
                    <a class="nav-link child_cat_income shops_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '13' && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_13?id=<?= $model->id ?>&stype=7"
                       aria-selected="false">Enquiry Market Rent Comparables</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income shops_c <?= ($step == 140 && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_140?id=<?= $model->id ?>&stype=7"
                       aria-selected="false">Market Rent Comparables (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income shops_c  <?= ($step == 150 && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_150?id=<?= $model->id ?>&stype=7"
                       aria-selected="false">Derive Market Rent Comparables (R)</a>
                <?php } ?>
                <a class="nav-link active shops_c" style="background-color:#4f5962;" aria-selected="false">Approver Calculations
                </a>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income shops_c <?= ($step == 11 && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_11?id=<?= $model->id ?>&stype=7"
                       aria-selected="false">Ejari-Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income shops_c <?= ($step == 12 && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_12?id=<?= $model->id ?>&stype=7"
                       aria-selected="false">Derive Ejari Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income shops_c <?= ($step == 14 && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_14?id=<?= $model->id ?>&stype=7"
                       aria-selected="false">Market Rent Comparables (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income shops_c <?= ($step == 15 && $stype == '7') ? 'active' : '' ?>" href="valuation-income/step_15?id=<?= $model->id ?>&stype=7"
                       aria-selected="false">Derive Market Rent Comparables (A)</a>
                <?php } ?>

            <?php } ?>

            <?php if ($model->inspectProperty->no_of_offices > 0) { ?>
                <a class="nav-link parent_cat_income offices_p"
                   aria-selected="false">Auto Calculations For Office </a>
                <a class="nav-link active offices_c" style="background-color:#4f5962;" aria-selected="false">Maxima Auto Calculations
                </a>

                    <a class="nav-link child_cat_income offices_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '10_v' && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_10_v?id=<?= $model->id ?>&stype=8"
                   aria-selected="false">Auto Ejari-Listings</a>
                <a class="nav-link child_cat_income offices_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '12_v' && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_12_v?id=<?= $model->id ?>&stype=8"
                   aria-selected="false">Auto Ejari-Val-Calculations</a>

                <a class="nav-link child_cat_income offices_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '13_v' && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_13_v?id=<?= $model->id ?>&stype=8"
                   aria-selected="false">Auto Market Rent Comparables</a>
                <a class="nav-link child_cat_income offices_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '15_v' && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_15_v?id=<?= $model->id ?>&stype=8"
                   aria-selected="false">Auto Market Rent Comparables-Val-Calculations</a>
                <a class="nav-link active offices_c" style="background-color:#4f5962;" aria-selected="false">Reviewer Calculations
                </a>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_10') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income offices_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '10' && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_10?id=<?= $model->id ?>&stype=8"
                       aria-selected="false">Enquiry Ejari-Listings</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income offices_c <?= ($step == 110 && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_110?id=<?= $model->id ?>&stype=8"
                       aria-selected="false">Ejari-Listings (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income offices_c <?= ($step == 120 && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_120?id=<?= $model->id ?>&stype=8"
                       aria-selected="false">Derive Ejari Listings (R)</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_13') && $model->valuation_approach != 2  ) { ?>
                    <a class="nav-link child_cat_income offices_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '13' && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_13?id=<?= $model->id ?>&stype=8"
                       aria-selected="false">Enquiry Market Rent Comparables</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income offices_c <?= ($step == 140 && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_140?id=<?= $model->id ?>&stype=8"
                       aria-selected="false">Market Rent Comparables (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income offices_c  <?= ($step == 150 && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_150?id=<?= $model->id ?>&stype=8"
                       aria-selected="false">Derive Market Rent Comparables (R)</a>
                <?php } ?>
                <a class="nav-link active offices_c" style="background-color:#4f5962;" aria-selected="false">Approver Calculations
                </a>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income offices_c <?= ($step == 11 && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_11?id=<?= $model->id ?>&stype=8"
                       aria-selected="false">Ejari-Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income offices_c <?= ($step == 12 && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_12?id=<?= $model->id ?>&stype=8"
                       aria-selected="false">Derive Ejari Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income offices_c <?= ($step == 14 && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_14?id=<?= $model->id ?>&stype=8"
                       aria-selected="false">Market Rent Comparables (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income offices_c <?= ($step == 15 && $stype == '8') ? 'active' : '' ?>" href="valuation-income/step_15?id=<?= $model->id ?>&stype=8"
                       aria-selected="false">Derive Market Rent Comparables (A)</a>
                <?php } ?>

            <?php } ?>
            <?php if ($model->inspectProperty->no_warehouses > 0) { ?>
                <a class="nav-link parent_cat_income warehouse_p"
                   aria-selected="false">Auto Calculations For Warehouses </a>
                <a class="nav-link active warehouse_c" style="background-color:#4f5962;" aria-selected="false">Maxima Auto Calculations
                </a>

                <a class="nav-link child_cat_income warehouse_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '10_v' && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_10_v?id=<?= $model->id ?>&stype=9"
                   aria-selected="false">Auto Ejari-Listings</a>
                <a class="nav-link child_cat_income warehouse_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '12_v' && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_12_v?id=<?= $model->id ?>&stype=9"
                   aria-selected="false">Auto Ejari-Val-Calculations</a>

                <a class="nav-link child_cat_income warehouse_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '13_v' && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_13_v?id=<?= $model->id ?>&stype=9"
                   aria-selected="false">Auto Market Rent Comparables</a>
                <a class="nav-link child_cat_income warehouse_c <?= ($model->parent_id <> null) ? 'disabled-link' : '' ?> <?= ($step == '15_v' && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_15_v?id=<?= $model->id ?>&stype=9"
                   aria-selected="false">Auto Market Rent Comparables-Val-Calculations</a>
                <a class="nav-link active warehouse_c" style="background-color:#4f5962;" aria-selected="false">Reviewer Calculations
                </a>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_10') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income warehouse_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '10' && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_10?id=<?= $model->id ?>&stype=9"
                       aria-selected="false">Enquiry Ejari-Listings</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income warehouse_c <?= ($step == 110 && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_110?id=<?= $model->id ?>&stype=9"
                       aria-selected="false">Ejari-Listings (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income warehouse_c <?= ($step == 120 && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_120?id=<?= $model->id ?>&stype=9"
                       aria-selected="false">Derive Ejari Listings (R)</a>
                <?php } ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_13') && $model->valuation_approach != 2  ) { ?>
                    <a class="nav-link child_cat_income warehouse_c <?= ($model->parent_id <> null && $model->id != 12846) ? 'disabled-link' : '' ?> <?= ($step == '13' && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_13?id=<?= $model->id ?>&stype=9"
                       aria-selected="false">Enquiry Market Rent Comparables</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income warehouse_c <?= ($step == 140 && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_140?id=<?= $model->id ?>&stype=9"
                       aria-selected="false">Market Rent Comparables (R)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2  && $model->id > 11184) { ?>
                    <a class="nav-link child_cat_income warehouse_c  <?= ($step == 150 && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_150?id=<?= $model->id ?>&stype=9"
                       aria-selected="false">Derive Market Rent Comparables (R)</a>
                <?php } ?>
                <a class="nav-link active warehouse_c" style="background-color:#4f5962;" aria-selected="false">Approver Calculations
                </a>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_11') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income warehouse_c <?= ($step == 11 && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_11?id=<?= $model->id ?>&stype=9"
                       aria-selected="false">Ejari-Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_12') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income warehouse_c <?= ($step == 12 && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_12?id=<?= $model->id ?>&stype=9"
                       aria-selected="false">Derive Ejari Listings (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_14') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income warehouse_c <?= ($step == 14 && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_14?id=<?= $model->id ?>&stype=9"
                       aria-selected="false">Market Rent Comparables (A)</a>
                <?php } ?>
                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_15') && $model->valuation_approach != 2) { ?>
                    <a class="nav-link child_cat_income warehouse_c <?= ($step == 15 && $stype == '9') ? 'active' : '' ?>" href="valuation-income/step_15?id=<?= $model->id ?>&stype=9"
                       aria-selected="false">Derive Market Rent Comparables (A)</a>
                <?php } ?>

            <?php } ?>

        <?php  } ?>







        <?php if(in_array(Yii::$app->user->identity->permission_group_id, [40,3,9,43,1]) && Yii::$app->user->identity->location_status == 0){
            ?>


            <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_20')) { ?>
                <a class="nav-link  <?= ($step == 20) ? 'active' : '' ?>" href="valuation-income/step_20?id=<?= $model->id ?>"
                   aria-selected="false">Derive MV Income Approach</a>
            <?php } ?>
            <a class="nav-link  <?= ($step == 210) ? 'active' : '' ?>" href="valuation-income/step_21?id=<?= $model->id ?>"
               aria-selected="false">Summary (R)</a>



            <?php
            if($model->parent_id <> null){ ?>

                <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_21')) { ?>

                    <a class="nav-link  <?= ($step == 210) ? 'active' : '' ?>" href="valuation-income/step_21?id=<?= $model->id ?>"
                       aria-selected="false">Summary (R)</a>

                  <!--  <a class="nav-link  <?/*= ($step == '21') ? 'active' : '' */?>" href="valuation/step_21/<?/*= $model->id */?>"
                       aria-selected="false">Summary (A)</a>-->
                <?php } ?>

                <?php
            }
            ?>

            <?php
        }else{ ?>



            <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_20')) { ?>
                <a class="nav-link  <?= ($step == 20) ? 'active' : '' ?>" href="valuation-income/step_20?id=<?= $model->id ?>"
                   aria-selected="false">Derive MV Income Approach</a>
            <?php } ?>
            <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_21') && $model->id > 11184) { ?>
                <a class="nav-link  <?= ($step == 210) ? 'active' : '' ?>" href="valuation-income/step_21?id=<?= $model->id ?>"
                   aria-selected="false">Summary (R)</a>

            <?php } ?>

            <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_23')) { ?>
                <a class="nav-link  <?= ($step == 23) ? 'active' : '' ?>" href="valuation/step_23/<?= $model->id ?>"
                   aria-selected="false">Approval</a>
            <?php } ?>
            <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_24')) { ?>
                <a class="nav-link  <?= ($step == 24) ? 'active' : '' ?>" href="valuation/step_24/<?= $model->id ?>"
                   aria-selected="false">Add Signature Copy</a>
            <?php } ?>
        <?php } ?>
        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_23')) { ?>
            <a class="nav-link  <?= ($step == 23) ? 'active' : '' ?>" href="valuation/step_23/<?= $model->id ?>"
               aria-selected="false">Approval</a>
        <?php } ?>
        <?php /*if(Yii::$app->menuHelperFunction->checkActionAllowed('pdf') && $model->client->print_report == 1 ) { */?><!--
    <a class="nav-link " href="valuation/pdf/<?/*= $model->id */?>" target="_blank"
       aria-selected="false">Generate PDF</a>
    --><?php /*} */?>

            <!-- <?php /*if (Yii::$app->menuHelperFunction->checkActionAllowed('pdf') && $model->client->print_report == 1) { */?>
        <a class="nav-link " href="valuation/pdf/<?/*= $model->id */?>" target="_blank"
           aria-selected="false">Generate PDF</a>
    --><?php /*} */?>



        <?php

        $allowed_properties = array(1,2,4,5,6,11,12,17,20,23,24,26,28,29,37,39,41,44,46,47,48,49,50,51,52,53,54,55,56,77);

        if (!in_array($model->property_id, $allowed_properties)) {
            //echo $model->property_id;
            ?>
            <?php if($model->purpose_of_valuation == 14){ ?>
                <!-- <a class="nav-link " href="valuation/pdfrec/<?/*= $model->id */?>" target="_blank"
           aria-selected="false">Generate PDF</a>-->
            <?}else{ ?>
                <!-- <a class="nav-link " href="valuation/pdf/<?/*= $model->id */?>" target="_blank"
               aria-selected="false">Generate PDF</a>-->
            <?php } ?>
        <?php }else if (Yii::$app->menuHelperFunction->checkActionAllowed('pdf')) { ?>
            <?php if($model->purpose_of_valuation == 14){ ?>
                <!--  <a class="nav-link " href="valuation/pdfrec/<?/*= $model->id */?>" target="_blank"
       aria-selected="false">Generate PDF</a>-->
            <?php }else{ ?>
                <!--  <a class="nav-link " href="valuation/pdf/<?/*= $model->id */?>" target="_blank"
           aria-selected="false">Generate PDF</a>-->
            <?php } ?>
        <?php } ?>

        <?php
        if (in_array(Yii::$app->user->identity->id, [110465])) { ?>

            <a class="nav-link " href="valuation/pdf/<?= $model->id ?>" target="_blank"
               aria-selected="false">Generate PDF</a>

        <?php } ?>

            <!--
/*
    // $allowed_properties = array(1,2,5,6,12,4,20,24,28,37,39,17);
    $allowed_properties = array(1,2,4,5,6,11,12,17,20,23,24,25,26,28,29,37,39,41,44,46,47,48,49,50,51,52,53,54,55,56,77);
    if(Yii::$app->user->identity->permission_group_id==3) {
        if ($model->client->id == 1) {


            */?>
            <a class="nav-link " href="valuation/pdf/<?/*= $model->id */?>" target="_blank"
               aria-selected="false">Generate PDF</a>
        <?php /*} else if (!in_array($model->property_id, $allowed_properties)) {
            echo $model->property_id;
            */?>
            <a class="nav-link " href="valuation/pdf/<?/*= $model->id */?>" target="_blank"
               aria-selected="false">Generate PDF</a>
        <?php /*} else if (Yii::$app->menuHelperFunction->checkActionAllowed('pdf') && $model->client->print_report == 1) { */?>
            <a class="nav-link " href="valuation/pdf/<?/*= $model->id */?>" target="_blank"
               aria-selected="false">Generate PDF</a>
        <?php /*}
    }else {
        */?>
        <?php /*if (Yii::$app->menuHelperFunction->checkActionAllowed('pdf') && $model->client->print_report == 1) { */?>
            <a class="nav-link " href="valuation/pdf/<?/*= $model->id */?>" target="_blank"
               aria-selected="false">Generate PDF</a>
        --><?php /*}
    }*/?>
        <?php if($model->client->client_invoice_customer ==1 && Yii::$app->menuHelperFunction->checkActionAllowed('invoice_toe')){ ?>
            <a class="nav-link " href="valuation/invoice/<?= $model->id ?>" target="_blank"
               aria-selected="false">Generate Invoice</a>
        <?php } ?>

    <?php }else{ ?>
        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) { ?>
            <a class="nav-link <?= ($step == 1) ? 'active' : '' ?>" href="valuation/step_1/<?= $model->id ?>"
               aria-selected="false">Enter Valuation Instruction Details</a>
        <?php } ?>
        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) { ?>
            <a class="nav-link <?= ($step == 101) ? 'active' : '' ?>" href="valuation/step_101/<?= $model->id ?>"
               aria-selected="false">Enter Property Details</a>
        <?php } ?>


        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_2')) { ?>
            <a class="nav-link <?= ($step == 2) ? 'active' : '' ?>" href="valuation/step_2/<?= $model->id ?>"
               aria-selected="false">Enter Documents Details</a>
        <?php } ?>

        <a class="nav-link <?= ($step == 401) ? 'active' : '' ?>" href="valuation/step_401/<?= $model->id ?>"
           aria-selected="false">Enter Valuation Details</a>

        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_4')) { ?>
            <a class="nav-link <?= ($step == 4) ? 'active' : '' ?>" href="valuation/step_4/<?= $model->id ?>"
               aria-selected="false"> Enter Inspection Schedule</a>
        <?php } ?>
        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_5')) { ?>
            <a class="nav-link <?= ($step == 5) ? 'active' : '' ?>" href="valuation/step_5/<?= $model->id ?>"
               aria-selected="false">Enter Inspected Property Details</a>
        <?php } ?>
        <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('step_6')) { ?>
            <a class="nav-link  <?= ($step == 6) ? 'active' : '' ?>" href="valuation/step_6/<?= $model->id ?>"
               aria-selected="false">Enter Property Configuration Details</a>
        <?php } ?>

    <?php } ?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(".st_p").click(function(){
            $(".st_c").toggle(1000);
        });

        $(".one_bedroom_p").click(function(){
            $(".one_bedroom_c").toggle(1000);
        });

        $(".two_bedroom_p").click(function(){
            $(".two_bedroom_c").toggle(1000);
        });

        $(".three_bedroom_p").click(function(){
            $(".three_bedroom_c").toggle(1000);
        });

        $(".four_bedrooms_p").click(function(){
            $(".four_bedrooms_c").toggle(1000);
        });

        $(".penthouse_p").click(function(){
            $(".penthouse_c").toggle(1000);
        });

        $(".shops_p").click(function(){
            $(".shops_c").toggle(1000);
        });

        $(".offices_p").click(function(){
            $(".offices_c").toggle(1000);
        });
        $(".warehouse_p").click(function(){
            $(".warehouse_c").toggle(1000);
        });
    });


</script>
<?php  } ?>


