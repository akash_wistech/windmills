<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$clientsArr = ArrayHelper::map(\app\models\Company::find()
->select([
  'id','title',
  'cname_with_nick' => 'CONCAT(title," ",nick_name)',
])
  ->where(['status' => 1])
  ->andWhere([
      'or',
      ['data_type' => 0],
      ['data_type' => null],
  ])
  ->orderBy(['title' => SORT_ASC,])
  ->all(), 'id', 'title');
  $clientsArr = ['' => 'select'] + $clientsArr;

  $propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
  $propertiesArr = ['' => 'select'] + $propertiesArr;

  $communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
  $communitiesArr = ['' => 'select'] + $communitiesArr;

  $citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
  $citiesArr = ['' => 'select'] + $citiesArr;
  
?>

<style>
/*.dataTable th {*/
/*    color: #0056b3;*/
/*    font-size: 14px;*/
/*    text-align: right !important;*/
/*    padding-right: 30px !important;*/
/*}*/
/*.dataTable td {*/
/*    font-size: 16px;*/
/*    text-align: right;*/
/*    padding-right: 50px;*/
/*}*/
/*.content-header h1 {*/
/*       font-size: 16px !important;*/

/*    }*/
/*.content-header .row {*/
/*    margin-bottom: 0px !important;*/
/*}*/
/*.content-header {*/
/*    padding: 4px !important;*/
/*}*/



.dataTable th {
    color: #0056b3;
    font-size: 15px;
    text-align: left !important;
    padding-right: 30px !important;
}
.dataTable td {
    font-size: 16px;
    text-align: left;
    /*padding-right: 50px;*/
    padding-top: 3px;
}
.content-header h1 {
       font-size: 16px !important;

    }
.content-header .row {
    margin-bottom: 0px !important;
}
.content-header {
    padding: 0px !important;
}




<?php 
  if($request['ValuationSearch']['page_title'] =='Today Received'){?>

    th:nth-child(1) {
      min-width: 80px;
      max-width: 80px;
    }
    th:nth-child(2) {
      min-width: 150px;
      max-width: 150px;
    }
    th:nth-child(3) {
      min-width: 120px;
      max-width: 120px;
    }
    th:nth-child(4) {
      min-width: 130px;
      max-width: 130px;
    }
    th:nth-child(5) {
      min-width: 180px;
      max-width: 180px;
    }
    th:nth-child(6) {
      min-width: 70px;
      max-width: 70px;
    }
    th:nth-child(7) {
      min-width: 40px;
      max-width: 40px;
    }
  th:nth-child(8) {
    min-width: 70px;
    max-width: 70px;
  }

  th:nth-child(9) {
    min-width: 150px;
    max-width: 150px;
  }

  <?php
    }
  ?>


/*th:nth-child(1) {*/
/*  min-width: 80px;*/
/*  max-width: 80px;*/
/*}*/
/*th:nth-child(2) {*/
/*  min-width: 90px;*/
/*  max-width: 90px;*/
/*}*/
/*th:nth-child(3) {*/
/*  min-width: 120px;*/
/*  max-width: 120px;*/
/*}*/
/*th:nth-child(4) {*/
/*  min-width: 100px;*/
/*  max-width: 100px;*/
/*}*/
/*th:nth-child(5) {*/
/*  min-width: 160px;*/
/*  max-width: 160px;*/
/*}*/
/*th:nth-child(6) {*/
/*  min-width: 100px;*/
/*  max-width: 100px;*/
/*}*/
/*th:nth-child(7) {*/
/*  min-width: 40px;*/
/*  max-width: 40px;*/
/*}*/
/*th:nth-child(8) {*/
/*  min-width: 70px;*/
/*  max-width: 70px;*/
/*}*/
/*th:nth-child(9) {*/
/*  min-width: 50px;*/
/*  max-width: 50px;*/
/*}*/
/*th:nth-child(10) {*/
/*  min-width: 150px;*/
/*  max-width: 150px;*/
/*}*/


</style>


<div class="bank-revenue-index col-12">
    <div class="card card-outline card-info">
        <div class="card-body">
            <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                <thead>
                    <tr>
                        <th class="">Instruction Date<input type="text" class="custom-search-input form-control" placeholder="date"></th>
                        <!-- <th width="6%" class="">Client<input type="text" class="custom-search-input form-control" placeholder="client"></th> -->


                        <th class="">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder'=>'client']); ?>
                        </th>
                        
                        <th class="">WM Reference<input type="text" class="custom-search-input form-control" placeholder="ref#"></th>
                        
                        <th  class="">Property
                          <?php echo Html::dropDownList('property', null, $propertiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'Property']); ?>
                        </th>
                        
                        <th  class="">Community
                          <?php echo Html::dropDownList('community', null, $communitiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'commmunity']); ?>
                        </th>
                        <th  class="">City
                          <?php echo Html::dropDownList('city', null, $citiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'city']); ?>
                        </th>
                        <th  class="">Valuer<input type="text" class="custom-search-input form-control" placeholder="valuer"></th>
                        
                        <?php 
                            if(! $request['ValuationSearch']['page_title'] =='Today Received'){
                          ?>
                            <th  class="">Inspector<input type="text" class="custom-search-input form-control" placeholder="inspector"></th>
                        <?php
                            }
                        ?>
                        
                        <th class="">Fee<input type="text" class="custom-search-input form-control" placeholder="fee"></th>
                        <th  class="">Valuation Status<input type="text" class="custom-search-input form-control" placeholder="status"></th>
             

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($dataProvider->getModels() as $model){
                    ?>
                    <tr class="active">
                        <td><?=  date('d-M-Y', strtotime($model->instruction_date)) ?></td>
                        <td><?= ($model->client->nick_name <> null) ?$model->client->nick_name : $model->client->title ?></td>
                        <td><?= $model->reference_number ?></td>
                        <td><?= $model->property->title ?></td>
                        <td><?= $model->building->communities->title ?></td>
                        <td><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?></td>
                        <td><?= (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '' ?>
                       
                        <?php 
                            if(! $request['ValuationSearch']['page_title'] =='Today Received'){
                          ?>
                          <td><?= (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null))? (Yii::$app->appHelperFunctions->staffMemberListArrLastName[$model->scheduleInspection->inspection_officer]): '' ?>
                        <?php
                            }
                        ?>
                        
                        <td><?= Yii::$app->appHelperFunctions->wmFormate($model->fee) ?></td>

                        <td>
                          <?= Yii::$app->helperFunctions->valuationStatusListArrWords[$model->valuation_status] ?>
                            <!--<div class="btn-group flex-wrap">-->
                            <!--    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">-->
                            <!--        <span class="caret"></span>-->
                            <!--    </button>-->
                            <!--    <div class="dropdown-menu" role="menu">-->
                            <!--        <a class="dropdown-item text-1" href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>" title="Step 1" data-pjax="0">-->
                            <!--            <i class="fas fa-edit"></i> Step 1-->
                            <!--        </a>-->
                            <!--    </div>-->
                            <!--</div>-->
                        </td>


                    </tr>
                    <?php
                }
            }
        ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <?php 
                            if(! $request['ValuationSearch']['page_title'] =='Today Received'){
                          ?>
                          <th></th>
                        <?php
                            }
                        ?>
                        <th colspan="2" style="padding-right: 10px!important;  text-align:left!important;"><strong>Total Fee:</strong></th>
                        <th  style="padding-right: 10px!important;  text-align:left!important;"> <?= Yii::$app->appHelperFunctions->wmFormate(\app\models\Valuation::getTotal($dataProvider->models, 'fee')) ?></th>
                        <th></th>

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>




<?php
    $this->registerJs('
    
    
        // $("#bank-revenue").DataTable({
        //     order: [[0, "asc"]],
        //     pageLength: 50,
        //     searching: false,
        // });
        
          var dataTable = $("#bank-revenue").DataTable({
            order: [[0, "asc"]],
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
          });
        
          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });
          
        $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input-client").on("change", function () {
              $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                  console.log(data);
                  dataTable.search(data).draw();
                },
                error: bbAlert
            });

          });
                
          $("#bank-revenue_filter").css({
            "display":"none",
          });
        
        $(".custom-search-input").on("click", function (event) {
          event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
          event.stopPropagation();
        });
        

        // Move the length menu to the table footer
        // var lengthMenu = $(".dataTables_length");
        // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");

        // Create a custom footer element
        var customFooter = $("<div class=\"custom-footer\"></div>");
      
        // Move the length menu to the custom footer
        var lengthMenu = $(".dataTables_length");
        lengthMenu.detach().appendTo(customFooter);
      
        // Append the custom footer to the DataTable wrapper
        $("#bank-revenue_wrapper").append(customFooter);
        
        

    ');
?>