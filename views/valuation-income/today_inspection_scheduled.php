<?php 
use app\models\Company;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Valuation;
use app\models\ValuationApproversData;

use app\assets\DateRangePickerAsset2;
use app\models\User;

DateRangePickerAsset2::register($this);

$clientsArr = ArrayHelper::map(\app\models\Company::find()
->select([
  'id','title',
  'cname_with_nick' => 'CONCAT(title," ",nick_name)',
])
  ->where(['status' => 1])
  ->andWhere([
      'or',
      ['data_type' => 0],
      ['data_type' => null],
  ])
  ->orderBy(['title' => SORT_ASC,])
  ->all(), 'id', 'title');
  $clientsArr = ['' => 'select'] + $clientsArr;


$citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
$citiesArr = ['' => 'select'] + $citiesArr;

$propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$propertiesArr = ['' => 'select'] + $propertiesArr;


?>
<style>
th:nth-child(1) {
    min-width: 75px;
    max-width: 75px;
}
th:nth-child(2) {
    min-width: 70px;
    max-width: 70px;
}
th:nth-child(3) {
    min-width: 70px;
    max-width: 70px;
}
th:nth-child(4) {
    min-width: 70px;
    max-width: 70px;
}
th:nth-child(5) {
    min-width: 50px;
    max-width: 50px;
}
th:nth-child(6) {
    min-width: 70px;
    max-width: 70px;
}

th:nth-child(7) {
    min-width: 65px;
    max-width: 65px;
}
th:nth-child(8) {
    min-width: 85px;
    max-width: 85px;
}
th:nth-child(9) {
    min-width: 50px;
    max-width: 50px;
}
th:nth-child(10) {
    min-width: 70px;
    max-width: 70px;
}
th:nth-child(11) {
    min-width: 45px;
    max-width: 45px;
}
</style>
<?php
if($_REQUEST['time']  == "today")
{
    $page_title = "Today Inspection Scheduled";
}
?>

<?php 
$form = ActiveForm::begin([
    'action' => [$callback_url],
    'method' => 'get',
]);
?>

<div class="card card-outline card-warning">
    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label> General Search : </label>
                    <input type="search" id="custom-search-input-text" placeholder="Search from datatable" class="form-control custom-search-input-text">
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<div class="bank-revenue-index col-12">
    <div class="card card-outline card-info">
        <div class="card-body">
        <span><strong><?= $page_title; ?></strong></span><br /><br />
            <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                <thead>
                    <tr>

                        <th class="">Instruction Date
                            <input id="intruction_date" type="date" class="form-control" placeholder="date">
                        </th>
                        <th class="">Inspection Schedule
                            <input id="inspection_date" type="date" class="form-control" placeholder="date">
                        </th>
                        <th class="">WM Refrence
                            <input type="text" class="custom-search-input form-control" placeholder="Reference#">
                        </th>

                        <th class="">Client Refrence
                            <input type="text" class="custom-search-input form-control" placeholder="Reference#">
                        </th>

                        <th class="">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder'=>'client']); ?>
                        </th>

                        <th class="">Property
                            <?php echo Html::dropDownList('property', null, $propertiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'Property']); ?>
                        </th>

                        <th class="">Building
                            <input type="text" class="custom-search-input form-control" placeholder="Project/Building">
                        </th>

                        <th class="">Community
                            <input type="text" class="custom-search-input form-control" placeholder="Community">
                        </th>

                        <th class="">City
                                <?php echo Html::dropDownList('city', null, $citiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'city']); ?>
                        </th>

                        <th class="">Inspector
                            <input type="text" class="custom-search-input form-control" placeholder="Inspector">
                        </th>

                        <?php 
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14 || Yii::$app->user->id==110465 ||  Yii::$app->user->id==6148){
                        ?>
                        <th>Action</th>
                        <?php
                          }
                        ?>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($inspections) > 0){
                        foreach($inspections as $inspection){
                           //  dd($inspection->valuation->instruction_date);
                            $contact_number = '';
                            if($inspection->valuation->id >7418){

                                if ($inspection->valuation->contact_phone_no <> null) $contact_number = $inspection->valuation->phone_code . '' . $inspection->valuation->contact_phone_no;
                                if ($inspection->valuation->land_line_no <> null) $contact_number = $inspection->valuation->land_line_code . '' . $inspection->valuation->land_line_no;
                            }else {

                                if ($inspection->contact_phone_no <> null) $contact_number = $inspection->phone_code . '' . $inspection->contact_phone_no;
                                if ($inspection->land_line_no <> null) $contact_number = $inspection->land_line_code . '' . $inspection->land_line_no;
                            }
                    ?>

                    <tr class="active">
                        <!-- Instruction date -->
                        <td>
                            <?=  date('d-M-Y', strtotime($inspection->valuation->instruction_date)) ?>
                            <br>
                            <?= Yii::$app->appHelperFunctions->formatTimeAmPm($inspection->valuation->instruction_time) ?>
                        </td>

                        <!-- Inspection Schedule time -->
                        <td>
                            <?=  date('d-M-Y', strtotime($inspection->inspection_date)) ?>
                            <br>
                            <?= Yii::$app->appHelperFunctions->formatTimeAmPm($inspection->inspection_time) ?>
                        </td>

                        <!-- WM Reference -->
                        <td><?= $inspection->valuation->reference_number ?></td>

                        <!-- Client ref -->
                        <td><?php echo chunk_split($inspection->valuation->client_reference, 15, ' ') ?></td>

                        <!-- Client -->
                        <td><?php             
                            $client =  Company::find()->where(['id' => $inspection->valuation->client_id])->one(); 
                            echo ($client->nick_name <> null) ? $client->nick_name : $client->title;
                            ?>  
                        </td>

                        <!-- Property -->
                        <td><?= $inspection->valuation->property->title ?></td>

                        <!-- Building -->
                        <td><?= $inspection->valuation->building->title ?></td>

                        <!-- Community -->
                        <td><?php echo $inspection->valuation->building->communities->title ?></td>
                        
                        <!-- City -->
                        <td><?php echo Yii::$app->appHelperFunctions->emiratedListArr[$inspection->valuation->building->city] ?></td>
                        
                        <!-- Inspector -->
                        <td><?= (isset($inspection->inspection_officer) && ($inspection->inspection_officer <> null))? ($inspection->inspectorData->lastname) : '' ?></td>
                    
                        <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14 || Yii::$app->user->id==110465 ||  Yii::$app->user->id==6148){
                        ?>
                        <td class="noprint actions">
                            <div class="btn-group flex-wrap">
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                    data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a class="dropdown-item text-1"
                                        href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$inspection->valuation->id]) ?>"
                                        title="Step 1" data-pjax="0">
                                        <i class="fas fa-edit"></i> View 
                                    </a>
                                </div>
                            </div>
                        </td>
                        <?php
                          }
                        ?>
                        
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
