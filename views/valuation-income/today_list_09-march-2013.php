<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Valuations');
$cardTitle = Yii::t('app', 'Today Approved List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;
if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) {
    $actionBtns .= '{step_1}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('revise')) {

    $actionBtns .= '{revise}';
}
?>

<div class="valuation-index">


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
       /* 'createBtn' => $createBtn,*/
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'reference_number', 'label' => Yii::t('app', 'Reference')],
            ['attribute' => 'client_id',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                    return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title;
                },
                'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Building'),
                'value' => function ($model) {
                    return $model->building->title;
                },
                'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Community'),
                'value' => function ($model) {
                    return $model->building->communities->title;
                },
                // 'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                //     'title' => SORT_ASC,
                // ])->all(), 'id', 'community')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];

                },
                'filter' => Yii::$app->appHelperFunctions->emiratedListArr
            ],
            ['attribute' => 'instruction_date',
                'label' => Yii::t('app', 'Instruction Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->instruction_date));
                },
            ],
            ['attribute' => 'target_date',
                'label' => Yii::t('app', 'Target Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->target_date));
                },
            ],
            ['attribute' => 'id',
                'label' => Yii::t('app', 'Valuation Report Date'),
                'value' => function ($model) {
                    // return date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
                    if($model->scheduleInspection->valuation_report_date <> null) {
                        return date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
                    }else{

                    }
                },
            ],

            /* ['attribute' => 'target_date',
                 'label' => Yii::t('app', 'Target Date'),
                 'value' => function ($model) {
                     return date('d-m-Y', strtotime($model->target_date));
                 },
             ],*/
            ['attribute' => 'id',
                'label' => Yii::t('app', 'Fee'),
                'value' => function ($model) {
                    /*if (Yii::$app->appHelperFunctions->getClientRevenue($model->id)!=null) {
                      $total+=(int)Yii::$app->appHelperFunctions->getClientRevenue($model->id);
                    }*/
                    if($model->quotation_id <> null) {
                        return $model->fee;
                    }else {
                        return Yii::$app->appHelperFunctions->getClientRevenue($model->id);
                    }
                },
                // 'footer' => '<b>Fee Total</b> '.\app\models\Valuation::getTotal($dataProvider->models, 'id'),
            ],
            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
                },
                'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
                    'firstname' => SORT_ASC,
                ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];})
            ],

            [
                'attribute' => 'id',
                'label' => Yii::t('app', 'Market Value'),
                'value' => function ($model) {
                    return (isset($model->approverData) && ($model->approverData <> null))? (number_format($model->approverData->estimated_market_value)): '';
                },
            ],



        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>
