
<?php

use app\models\Company;
use app\models\InspectProperty;
use app\models\User;
use app\models\Valuation;
use app\models\ValuationApproversData;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use app\assets\SortBootstrapAsset;
SortBootstrapAsset::register($this);
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);

use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = Yii::t('app', $page_title);
$cardTitle = Yii::t('app', $page_title);
$this->params['breadcrumbs'][] = $this->title;

$propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$propertiesArr = ['' => 'select'] + $propertiesArr;

$communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$communitiesArr = ['' => 'select'] + $communitiesArr;

$citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
$citiesArr = ['' => 'select'] + $citiesArr;

if(Yii::$app->request->queryParams['ValuationSearch']['valuation_status']['0'] == "9"){
    $page_title = "Total Cancelled";
}

if (Yii::$app->request->queryParams['ValuationSearch']['revised_reason']['0'] == "1" ||
    Yii::$app->request->queryParams['ValuationSearch']['revised_reason']['1'] == "2" )
    {
        $page_title = "Total Challenged";
    }

if (Yii::$app->request->queryParams['ValuationSearch']['revised_reason']['0'] == "3")
{
    $page_title = "Total Mistakes";
}

$allowedId = [1,14,33,110465];

?>

<style>


.dataTable th {
      color: #0056b3;
      font-size: 15px;
      text-align: left !important;
      /* padding-right: 30px !important; */

  }
  .dataTable td {
      font-size: 16px;
      text-align: left;
      /* padding-right: 50px; */
      padding-top: 3px;
      max-width: 100px;
  }

.content-header h1 {
    font-size: 16px !important;

}

.content-header .row {
    margin-bottom: 0px !important;
}

.content-header {
    padding: 0px !important;
}


<?php if($page_title == "Total Received") { ?> 
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(3) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(4) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(5) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(6) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(7) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(8) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(9) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(10) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(11) {
        min-width:  70px;
        max-width:  70px;
    }
<?php } else if($page_title == "Total Cancelled") { ?>
    .table.dataTable thead > tr > th.sorting{
    padding-right: 10px !important;
    }
    th:nth-child(1) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(2) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(3) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(4) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(5) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(6) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(7) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(8) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(9) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(10) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(11) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(12) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(13) {
        min-width:  45px;
        max-width:  45px;
    }
<?php } else if($page_title == "Total Approved") { ?>
    .table.dataTable thead > tr > th.sorting{
    padding-right: 10px !important;
    }
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(3) {
        min-width:  70px;
        max-width:  70px;
    }

    th:nth-child(4) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(5) {
        min-width:  40px;
        max-width:  40px;
    }
    th:nth-child(6) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(7) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(8) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(9) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(10) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(11) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(12) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(13) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(14) {
        min-width:  45px;
        max-width:  45px;
    }
    th:nth-child(15) {
        min-width:  45px;
        max-width:  45px;
    }
<?php } else if($page_title == "Total Inspected") { ?>
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(3) {
        min-width:  70px;
        max-width:  70px;
    }

    th:nth-child(4) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(5) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(6) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(7) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(8) {
        min-width:  80px;
        max-width:  80px;
    }
    th:nth-child(9) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(10) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(11) {
        min-width:  45px;
        max-width:  45px;
    }
<?php } else if($page_title == "Total Challenged") { ?>
    .table.dataTable thead > tr > th.sorting{
    padding-right: 10px !important;
    }
    th:nth-child(1) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(2) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(3) {
        min-width:  70px;
        max-width:  70px;
    }

    th:nth-child(4) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(5) {
        min-width:  40px;
        max-width:  40px;
    }
    th:nth-child(6) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(7) {
        min-width:  66px;
        max-width:  66px;
    }
    th:nth-child(8) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(9) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(10) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(11) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(12) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(13) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(14) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(15) {
        min-width:  45px;
        max-width:  45px;
    }
<?php } else if($page_title == "Total Mistakes") {  ?>
    /* .table.dataTable thead > tr > th.sorting{
    padding-right: 10px !important;
    } */
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(3) {
        min-width:  75px;
        max-width:  75px;
    }

    th:nth-child(4) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(5) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(6) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(7) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(8) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(9) {
        min-width:  45px;
        max-width:  45px;
    }
    th:nth-child(10) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(11) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(12) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(13) {
        min-width:  45px;
        max-width:  45px;
    }
    th:nth-child(14) {
        min-width:  45px;
        max-width:  45px;
    }
    th:nth-child(15) {
        min-width:  45px;
        max-width:  45px;
    }
<?php } else if($page_title == "Tat Reports") { ?>
    th:nth-child(1) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(2) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(3) {
        min-width:  85px;
        max-width:  85px;
    }

    th:nth-child(4) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(5) {
        min-width:  90px;
        max-width:  90px;
    }
    th:nth-child(6) {
        min-width:  90px;
        max-width:  90px;
    }
    th:nth-child(7) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(8) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(9) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(10) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(11) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(12) {
        min-width:  80px;
        max-width:  80px;
    }
    th:nth-child(13) {
        min-width:  80px;
        max-width:  80px;
    }
<?php } else if($page_title == "Total Ahead of Time Reports" || $page_title == "Total On Time" || $page_title == "Total Delay Reports" ) { ?>
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(3) {
        min-width:  70px;
        max-width:  70px;
    }

    th:nth-child(4) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(5) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(6) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(7) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(8) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(9) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(10) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(11) {
        min-width:  70px;
        max-width:  70px;
    }
<?php } else if($page_title == "Total Reminders") { ?>
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(3) {
        min-width:  70px;
        max-width:  70px;
    }

    th:nth-child(4) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(5) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(6) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(7) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(8) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(9) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(10) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(11) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(12) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(13) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(14) {
        min-width:  70px;
        max-width:  70px;
    }
<?php } ?>


</style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.css" />
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.js"></script>

<input type="hidden" id="comm-id" value='<?php echo $page_title; ?>'>

<?php if($page_title == "Total Received") { ?> 

    <div class="valuation-search">

        <?php $form = ActiveForm::begin([
            'action' => ['dashboard-report-all'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
            <span><strong><?= $page_title; ?></strong></span><br /><br /><br />
                <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                    <thead>
                        <tr>
                    
                            <th class="">Instruction Date
                                <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Reference Number
                                <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                            </th>

                            <th class="">Client Reference
                                <input type="text" class="custom-search-input form-control" placeholder="Ref#">
                            </th>

                            <th class="">Client
                                <input type="text" class="custom-search-input form-control" placeholder="Client">
                            </th>

                            <th class="">Property
                                <input type="text" class="custom-search-input form-control" placeholder="Property">
                            </th>

                            <th class="">City
                                <input type="text" class="custom-search-input form-control" placeholder="text">
                            </th>

                            <th class="">Inspector
                                <input type="text" class="custom-search-input form-control" placeholder="I/O">
                            </th>

                            <th class="">Valuer
                                <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                            </th>

                            <?php if(in_array( Yii::$app->user->identity->id, $allowedId)) { ?>
                                <th class="">Fee
                                    <input type="text" class="custom-search-input form-control" placeholder="Fee">
                                </th>
                            <?php } ?>
                            
                            <th class="">Status
                                <input type="text" class="custom-search-input form-control" placeholder="status">
                            </th>



                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    if(count($dataProvider->getModels())>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($dataProvider->getModels() as $model){
                            // dd($model);
                        ?>
                        <tr class="active">

                            <!-- Instruction -->
                            <td>
                                <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                            </td>

                            <!-- WM Reference Number -->
                            <td>
                                <?= $model->reference_number ?>
                            </td>

                            <!-- Client Reference -->
                            <td>
                                <?php echo $model->client_reference; ?>
                            </td>

                            <!-- Client -->
                            <td>
                                <?php 
                                    echo $model->client->nick_name; 
                                ?>
                            </td>

                            <!-- Property -->
                            <td>
                                <?php
                                    echo $model->property->title;
                                ?>
                            </td>
                                    
                            <!-- City -->
                            <td>
                                <?php 
                                    echo Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];
                                ?>
                            </td>

                            <!-- Inspector -->
                            <td>
                                <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                            </td>

                            <!-- Valuer -->
                            <td>
                                <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                ?>
                            </td>

                            <?php if(in_array( Yii::$app->user->identity->id, $allowedId)) { ?>
                                <!-- Fee  -->
                                <td>
                                    <?php
                                        echo $total_fee = Yii::$app->appHelperFunctions->wmFormate($model->total_fee);
                                    ?>
                                </td>
                            <?php } ?>

                            <!-- Status -->
                            <td>
                                <?= Yii::$app->helperFunctions->valuationStatusListArrWords[$model->valuation_status] ?>
                            </td>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                            href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                            title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> View 
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <?php
                            }
                            ?>

                        </tr>
                        <?php
                    }
                }
            ?>
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <?php if(in_array( Yii::$app->user->identity->id, $allowedId)) { ?>
                            <th>Total Fee : </th>
                            <th><?= Yii::$app->appHelperFunctions->wmFormate(\app\models\Valuation::getTotal($dataProvider->models, 'total_fee')) ?></th>
                        <?php } ?>
                            <th></th>
                        <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php
        $this->registerJs('

        let total_table = new DataTable("#bank-revenue", {
            responsive: true,
            order: [[0, "desc"]],
            dom: "lrtip",

            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]

        });

        $(".div1").daterangepicker({
            autoUpdateInput: false,
            locale: {
            format: "YYYY-MM-DD"
            }
        
        
        });
        
        $(".div1").on("apply.daterangepicker", function(ev, picker) {
            $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
        });

        $(\'#valuationsearch-time_period\').on(\'change\',function(){
            var period = $(this).val();
        
            if(period == 9){
                $(\'#date_range_array\').show();
            }else{
                $(\'#date_range_array\').hide();
            }
        });

        $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
        });

        //for date search instruction_date
        $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
            var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
            total_table.column(0).search(format1).draw();
            }else{
                total_table.search("").columns().search("").draw();
            }
        });

        $(".custom-search-input").on("keyup", function () {
            total_table.search(this.value).draw();
        });
        
        $(".custom-search-input-text").on("keyup", function () {
            total_table.search(this.value).draw();
        });
        $(".custom-search-input").on("change", function () {
            total_table.search(this.value).draw();
        });

        ');
    ?>

<?php } ?>

<?php if($page_title == "Total Cancelled") { ?> 

    <div class="valuation-search">
        <?php $form = ActiveForm::begin([
            'action' => ['dashboard-report-all'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
            <span><strong><?= "Total Cancelled"; ?></strong></span><br /><br /><br />
                <table id="insp_scheduled" class="table table-striped dataTable table-responsive">
                    <thead>
                        <tr>
                            <th class="">Instruction Date
                                <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Inspection Done
                                <input id="done_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Recommend Date
                                <input id="recommended_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Review Date
                                <input id="review_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Approval Date
                                <input id="approval_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Cancelled Date
                                <input id="cancelled_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Ref No.
                                <input id="reference_number" type="text" class="custom-search-input form-control" placeholder="Ref. No.">
                            </th>

                            <th class="">Client Ref
                                <input type="text" class="custom-search-input form-control" placeholder="Ref#">
                            </th>

                            <th class="">Inspector
                                <input type="text" class="custom-search-input form-control" placeholder="I/O">
                            </th>

                            <th class="">Valuer
                                <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                            </th>

                            <th class="">Reviewer
                                <input type="text" class="custom-search-input form-control" placeholder="Rev">
                            </th>

                            <th class="">Approver
                                <input type="text" class="custom-search-input form-control" placeholder="Appr">
                            </th>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if(count($dataProvider->getModels())>0){
                            // dd($dataProvider->getModels());
                            foreach($dataProvider->getModels() as $model){
                            // dd($model->scheduleInspection->created_at);
                        ?>
                        <tr class="active">

                            <!-- Instruction -->
                            <td>
                                <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                            </td>

                            <!-- Inspection Done -->
                            <td>
                                <?php 
                                    if(isset($model->inspectProperty->inspection_done_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->inspectProperty->inspection_done_date));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Recomm Date -->
                            <td>
                                <?php 
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "valuer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    if(isset($get_valuation_data->updated_at))
                                    {
                                        echo date('d-M-Y', strtotime($get_valuation_data->updated_at));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Review Date -->
                            <td>
                                <?php 
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();

                                    if(isset($get_valuation_data->created_at))
                                    {
                                        echo date('d-M-Y', strtotime($get_valuation_data->created_at));
                                    }else{
                                        echo "";
                                    }


                                ?>
                            </td>

                            <!-- Approval Date -->
                            <td>
                                <?php
                                    if(isset($model->submission_approver_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->submission_approver_date));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Cancel Date -->
                            <td>
                                <?php
                                    if($model->valuation_status == "9")
                                    {
                                        echo date('d-M-Y', strtotime($model->updated_at));
                                    }
                                ?>
                            </td>

                            <!-- Reference Number -->
                            <td>
                                <?= $model->reference_number ?>
                            </td>

                            <!-- Client ref -->
                            <td>
                                <?= $model->client_reference ?>
                            </td>

                            <!-- Inspector -->
                            <td>
                                <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                            </td>

                            <!-- Valuer -->
                            <td>
                                <?php echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): ''; ?>
                            </td>

                            <!-- reviewer -->
                            <td>
                                <?php 
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();

                                    $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                    echo  $get_name->lastname;
                                ?>
                            </td>

                            <!-- approver -->
                            <td>
                                <?php 
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "approver"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();

                                    $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                    echo  $get_name->lastname;
                                ?>
                            </td>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                            href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                            title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> View 
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <?php
                            }
                            ?>

                        </tr>
                        <?php
                    }
                }
            ?>
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php
        $this->registerJs('

        let cancelled_table = new DataTable("#insp_scheduled", {
            responsive: true,
            order: [[0, "desc"]],
            dom: "lrtip",

            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
        });

        $(".div1").daterangepicker({
            autoUpdateInput: false,
            locale: {
            format: "YYYY-MM-DD"
            }
        
        
        });
        
        $(".div1").on("apply.daterangepicker", function(ev, picker) {
            $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
        });

        $(\'#valuationsearch-time_period\').on(\'change\',function(){
            var period = $(this).val();
        
            if(period == 9){
                $(\'#date_range_array\').show();
            }else{
                $(\'#date_range_array\').hide();
            }
        });

        $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
        });

        //for date search instruction_date
        $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
            var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
            cancelled_table.column(0).search(format1).draw();
            }else{
                cancelled_table.search("").columns().search("").draw();
            }
        });

        //for date search done_date
        $("#done_date").on("change", function () {
            if($("#done_date").val() != ""){
            var format1 = moment($("#done_date").val()).format("DD-MMM-YYYY"); 
            cancelled_table.column(1).search(format1).draw();
            }else{
                cancelled_table.search("").columns().search("").draw();
            }
        });

        //for date search recommended_date
        $("#recommended_date").on("change", function () {
            if($("#recommended_date").val() != ""){
            var format1 = moment($("#recommended_date").val()).format("DD-MMM-YYYY"); 
            cancelled_table.column(2).search(format1).draw();
            }else{
                cancelled_table.search("").columns().search("").draw();
            }
        });

        //for date search review_date
        $("#review_date").on("change", function () {
            if($("#review_date").val() != ""){
            var format1 = moment($("#review_date").val()).format("DD-MMM-YYYY"); 
            cancelled_table.column(3).search(format1).draw();
            }else{
                cancelled_table.search("").columns().search("").draw();
            }
        });

        //for date search approval_date
        $("#approval_date").on("change", function () {
            if($("#approval_date").val() != ""){
            var format1 = moment($("#approval_date").val()).format("DD-MMM-YYYY"); 
            cancelled_table.column(4).search(format1).draw();
            }else{
                cancelled_table.search("").columns().search("").draw();
            }
        });

        //for date search cancelled_date
        $("#cancelled_date").on("change", function () {
            if($("#cancelled_date").val() != ""){
            var format1 = moment($("#cancelled_date").val()).format("DD-MMM-YYYY"); 
            cancelled_table.column(5).search(format1).draw();
            }else{
                cancelled_table.search("").columns().search("").draw();
            }
        });

        $(".custom-search-input").on("keyup", function () {
            cancelled_table.search(this.value).draw();
        });
        
        $(".custom-search-input-text").on("keyup", function () {
            cancelled_table.search(this.value).draw();
        });
        $(".custom-search-input").on("change", function () {
            cancelled_table.search(this.value).draw();
        });

        ');
    ?>

<?php } ?>

<?php if($page_title == "Total Approved") { ?> 

    <div class="valuation-search">
        <?php $form = ActiveForm::begin([
            'action' => ['dashboard-report-all'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
            <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
                <table id="doc_table" class="table table-striped dataTable table-responsive">
                    <thead>
                        <tr>
                            <th class="">Instruction Date
                                <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Review Date
                                <input id="review_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Ref No.
                                <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                            </th>

                            <th class="">Client Ref
                                <input type="text" class="custom-search-input form-control" placeholder="Ref">
                            </th>

                            <th class="">Client
                                <input type="text" class="custom-search-input form-control" placeholder="Client">
                            </th>

                            <th class="">Property
                                <input type="text" class="custom-search-input form-control" placeholder="Property">
                            </th>

                            <th class="">Review
                                <input type="text" class="custom-search-input form-control" placeholder="Rev">
                            </th>

                            <th class="">Approver
                                <input type="text" class="custom-search-input form-control" placeholder="App">
                            </th>

                            <th class="">Value Recommended
                                <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                            </th>

                            <th class="">Value Review
                                <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                            </th>

                            <th class="">Value Approve
                                <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                            </th>

                            <th class="">Rev Value Recom
                                <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                            </th>

                            <th class="">Rev Value Review
                                <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                            </th>

                            <th class="">Rev Value Approve
                                <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                            </th>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($dataProvider->getModels())>0){
                            // dd(count($dataProvider->getModels()));
                            foreach($dataProvider->getModels() as $model){
                                // dd($model->scheduleInspection->created_at);
                        ?>
                        <tr class="active">

                            <!-- Instruction -->
                            <td>
                                <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                            </td>

                            <!-- Review Date -->
                            <td>
                                <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();

                                    if(isset($get_valuation_data->created_at))
                                    {
                                        echo date('d-M-Y', strtotime($get_valuation_data->created_at));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Reference Number -->
                            <td>
                                <?= $model->reference_number ?>
                            </td>

                            <!-- Client Ref -->
                            <td>
                                <?= $model->client_reference ?>
                            </td>

                            <!-- Client -->
                            <td>
                                <?= $model->client->nick_name ?>
                            </td>

                            <!--  Property -->
                            <td>
                                <?= $model->property->title ?>
                            </td>

                            <!-- Reviwer -->
                            <td>
                                <?php  
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();

                                    $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                    echo  $get_name->lastname;
                                ?>
                            </td>

                            <!-- Approver -->
                            <td>
                                <?php
                                    $type = $model->approverData->approver_type;
                                    if($type == "approver")
                                    {
                                        $get_name = User::find()->where(['id' => $model->approverData->created_by])->one();
                                        echo $get_name->lastname ;
                                    }else{
                                        echo '';
                                    }
                                ?>
                            </td>

                            <!-- Market Val recomm -->
                            <td>
                                <?php 
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "valuer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    if(isset($get_valuation_data->estimated_market_value))
                                    {
                                        echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Market Val Reviewed -->
                            <td>
                                <?php 
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    if(isset($get_valuation_data->estimated_market_value))
                                    {
                                        echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Market Val Approve -->
                            <td>
                                <?php 
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "approver"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    if(isset($get_valuation_data->estimated_market_value))
                                    {
                                        echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Revised Market Val recomm -->
                            <td>
                                <?php 
                                    $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                                
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                                    ->andWhere(['approver_type' => "valuer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    if(isset($get_valuation_data->estimated_market_value))
                                    {
                                        echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Revised Market val reviewed -->
                            <td>
                                <?php 
                                    $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                                
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    if(isset($get_valuation_data->estimated_market_value))
                                    {
                                        echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Revised Market val Approve -->
                            <td>
                                <?php 
                                    $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                                
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                                    ->andWhere(['approver_type' => "approver"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    if(isset($get_valuation_data->estimated_market_value))
                                    {
                                        echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                            href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                            title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> View 
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <?php
                            }
                            ?>

                        </tr>
                        <?php
                    }
                }
            ?>
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php
        $this->registerJs('

        let approved_table = new DataTable("#doc_table", {
            responsive: true,
            order: [[0, "desc"]],
            dom: "lrtip",

            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
        });

        $(".div1").daterangepicker({
            autoUpdateInput: false,
            locale: {
            format: "YYYY-MM-DD"
            }
        
        
        });
        
        $(".div1").on("apply.daterangepicker", function(ev, picker) {
            $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
        });

        $(\'#valuationsearch-time_period\').on(\'change\',function(){
            var period = $(this).val();
        
            if(period == 9){
                $(\'#date_range_array\').show();
            }else{
                $(\'#date_range_array\').hide();
            }
        });

        $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
        });

        //for date search instruction_date
        $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
            var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
            approved_table.column(0).search(format1).draw();
            }else{
                approved_table.search("").columns().search("").draw();
            }
        });

        //for date search review_date
        $("#review_date").on("change", function () {
            if($("#review_date").val() != ""){
            var format1 = moment($("#review_date").val()).format("DD-MMM-YYYY"); 
            approved_table.column(1).search(format1).draw();
            }else{
                approved_table.search("").columns().search("").draw();
            }
        });

        $(".custom-search-input").on("keyup", function () {
            approved_table.search(this.value).draw();
        });
        
        $(".custom-search-input-text").on("keyup", function () {
            approved_table.search(this.value).draw();
        });
        $(".custom-search-input").on("change", function () {
            approved_table.search(this.value).draw();
        });

        ');
    ?>

<?php } ?>

<?php if($page_title == "Total Inspected") { ?> 

    <div class="valuation-search">

        <?php $form = ActiveForm::begin([
            'action' => ['dashboard-report-all'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
            <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
                <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                    <thead>
                        <tr>
                            <th class="">Instruction Date
                                <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Inspection Schedule
                                <input id="schedule_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Inspection Done
                                <input id="done_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Reference Number
                                <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                            </th>

                            <th class="">Client Reference
                                <input type="text" class="custom-search-input form-control" placeholder="Ref">
                            </th>

                            <th class="">Client
                                <input type="text" class="custom-search-input form-control" placeholder="Client">
                            </th>

                            <th class="">Property
                                <input type="text" class="custom-search-input form-control" placeholder="Property">
                            </th>
                            
                            <th class="">Community
                                <input type="text" class="custom-search-input form-control" placeholder="Community">
                            </th>

                            <th class="">City
                                <input type="text" class="custom-search-input form-control" placeholder="City">
                            </th>

                            <th class="">Inspector
                                <input type="text" class="custom-search-input form-control" placeholder="Inspector">
                            </th>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($dataProvider->getModels())>0){
                            // dd($dataProvider->getModels());
                            foreach($dataProvider->getModels() as $model){
                                // dd($model->inspectProperty->inspection_done_date);
                        ?>
                        <tr class="active">

                            <!-- Instruction -->
                            <td>
                                <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                            </td>

                            <!-- Inspection Schedule -->
                            <td>
                                <?php 
                                    if(isset($model->scheduleInspection->inspection_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->scheduleInspection->inspection_date));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Inspection Done -->
                            <td>
                                <?php 
                                    if(isset($model->inspectProperty->inspection_done_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->inspectProperty->inspection_done_date));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Reference Number -->
                            <td>
                                <?= $model->reference_number ?>
                            </td>

                            <!-- Client Ref -->
                            <td>
                                <?= $model->client_reference ?>
                            </td>
                                    
                            <!-- Client -->
                            <td>
                                <?= $model->client->nick_name ?>
                            </td>

                            <!--  Property -->
                            <td>
                                <?= $model->property->title ?>
                            </td>

                            <!-- Community -->
                            <td>
                                <?= $model->building->communities->title ?>
                            </td>

                            <!-- City -->
                            <td>
                                <?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
                            </td>

                            <!-- Inspection Officer -->
                            <td><?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null))? (Yii::$app->appHelperFunctions->staffMemberListArrLastName[$model->scheduleInspection->inspection_officer]): '' ?>


                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                            href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                            title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> View 
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <?php
                            }
                            ?>

                        </tr>
                        <?php
                    }
                }
            ?>
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php
        $this->registerJs('

        let inspected_table = new DataTable("#bank-revenue", {
            responsive: true,
            order: [[0, "desc"]],
            dom: "lrtip",

            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
        });

        $(".div1").daterangepicker({
            autoUpdateInput: false,
            locale: {
            format: "YYYY-MM-DD"
            }
        
        
        });
        
        $(".div1").on("apply.daterangepicker", function(ev, picker) {
            $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
        });

        $(\'#valuationsearch-time_period\').on(\'change\',function(){
            var period = $(this).val();
        
            if(period == 9){
                $(\'#date_range_array\').show();
            }else{
                $(\'#date_range_array\').hide();
            }
        });

        $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
        });

        //for date search instruction_date
        $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
            var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
            inspected_table.column(0).search(format1).draw();
            }else{
                inspected_table.search("").columns().search("").draw();
            }
        });

        //for date search schedule_date
        $("#schedule_date").on("change", function () {
            if($("#schedule_date").val() != ""){
            var format1 = moment($("#schedule_date").val()).format("DD-MMM-YYYY"); 
            inspected_table.column(1).search(format1).draw();
            }else{
                inspected_table.search("").columns().search("").draw();
            }
        });

        //for date search done_date
        $("#done_date").on("change", function () {
            if($("#done_date").val() != ""){
            var format1 = moment($("#done_date").val()).format("DD-MMM-YYYY"); 
            inspected_table.column(2).search(format1).draw();
            }else{
                inspected_table.search("").columns().search("").draw();
            }
        });

        $(".custom-search-input").on("keyup", function () {
            inspected_table.search(this.value).draw();
        });
        
        $(".custom-search-input-text").on("keyup", function () {
            inspected_table.search(this.value).draw();
        });
        $(".custom-search-input").on("change", function () {
            inspected_table.search(this.value).draw();
        });

        ');
    ?>

<?php } ?>

<?php if($page_title == "Total Challenged") { ?> 

    <div class="valuation-search">
        <?php $form = ActiveForm::begin([
            'action' => ['dashboard-report-all'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
            <span><strong><?= $page_title; ?></strong></span><br /><br /><br />
                <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                    <thead>
                        <tr>
                            <th class="">Instruction Date
                                <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Challenge Date
                                <input id="challenged_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">WM New Ref No.
                                <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                            </th>

                            <th class="">Client Ref
                                <input type="text" class="custom-search-input form-control" placeholder="Ref">
                            </th>

                            <th class="">Client 
                                <input type="text" class="custom-search-input form-control" placeholder="Client">
                            </th>

                            <th class="">Property
                                <input type="text" class="custom-search-input form-control" placeholder="Property">
                            </th>
                            
                            <th class="">Inspector
                                <input type="text" class="custom-search-input form-control" placeholder="I/O">
                            </th>

                            <th class="">Valuer
                                <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                            </th>

                            <th class="">Reviewer
                                <input type="text" class="custom-search-input form-control" placeholder="Reviewer">
                            </th>

                            <th class="">Approver
                                <input type="text" class="custom-search-input form-control" placeholder="Approver">
                            </th>

                            <th class="">Market Val Approved
                                <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                            </th>

                            <th class="">Rev Market Val Recomm
                                <input type="text" class="custom-search-input form-control" placeholder="Rev. Market Val">
                            </th>

                            <th class="">Rev Market Val Review
                                <input type="text" class="custom-search-input form-control" placeholder="Rev. Market Val">
                            </th>

                            <th class="">Rev Market Val Approved
                                <input type="text" class="custom-search-input form-control" placeholder="Rev. Market Val">
                            </th>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($dataProvider->getModels())>0){
                            // dd($dataProvider->getModels());
                            foreach($dataProvider->getModels() as $model){
                                // dd($model->inspectProperty);
                        ?>
                        <tr class="active">

                            <!-- Instruction -->
                            <td>
                                <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                            </td>

                            <!-- Challenge Date -->
                            <td>
                                <?php 
                                    if($model->revised_reason == "1" || $model->revised_reason == "2")
                                    {
                                        echo date('d-M-Y', strtotime($model->updated_at));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- New Reference Number -->
                            <td>
                                <?php 
                                    if($model->revised_reason != NULL || $model->revised_reason != "" || $model->revised_reason > 0)
                                    {
                                        echo $model->reference_number;
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Client Ref -->
                            <td>
                                <?= $model->client_reference ?>
                            </td>

                            <!-- Client -->
                            <td>
                                <?= $model->client->nick_name ?>
                            </td>

                            <!-- Property -->
                            <td>
                                <?= $model->property->title ?>
                            </td>
                                    
                            <!-- Inspector -->
                            <td>
                                <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                            </td>

                            <!-- Valuer -->
                            <td>
                                <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                ?>
                            </td>

                            <!-- Reviewer -->
                            <td>
                                <?php 
                                    // $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                                
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    $get_name = User::find()->where(['id' => $get_valuation_data->updated_by])->one();
                                    if(isset($get_name->lastname))
                                    {
                                        echo $get_name->lastname;
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Approver -->
                            <td>
                                <?php 
                                    // $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                                
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "approver"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    $get_name = User::find()->where(['id' => $get_valuation_data->updated_by])->one();
                                    if(isset($get_name->lastname))
                                    {
                                        echo $get_name->lastname;
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Market Value approved -->
                            <td>
                                <?php 
                                    // $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => 'approver'])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    
                                    echo (isset($get_valuation_data->id) && ($get_valuation_data->id <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                                ?>
                            </td>

                            <!-- Rev Market Value recomm -->
                            <td>
                                <?php
                                $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();

                                $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                                ->andWhere(['approver_type' => 'valuer'])
                                ->orderBy(['id' => SORT_DESC])
                                ->one();

                                echo (isset($get_valuation_data->estimated_market_value) && ($get_valuation_data->estimated_market_value <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                                ?>
                            </td>

                            <!-- Rev Market Value reviewer -->
                            <td>
                                <?php
                                $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();

                                $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                                ->andWhere(['approver_type' => 'reviewer'])
                                ->orderBy(['id' => SORT_DESC])
                                ->one();

                                echo (isset($get_valuation_data->estimated_market_value) && ($get_valuation_data->estimated_market_value <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                                ?>
                            </td>

                            <!-- Rev Market Value Approved -->
                            <td>
                                <?php
                                $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();

                                $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                                ->andWhere(['approver_type' => 'approver'])
                                ->orderBy(['id' => SORT_DESC])
                                ->one();

                                echo (isset($get_valuation_data->estimated_market_value) && ($get_valuation_data->estimated_market_value <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                                ?>
                            </td>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                            href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                            title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> View 
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <?php
                            }
                            ?>

                        </tr>
                        <?php
                    }
                }
            ?>
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php
        $this->registerJs('

        let challenged_table = new DataTable("#bank-revenue", {
            responsive: true,
            order: [[0, "desc"]],
            dom: "lrtip",

            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
        });

        $(".div1").daterangepicker({
            autoUpdateInput: false,
            locale: {
            format: "YYYY-MM-DD"
            }
        
        
        });
        
        $(".div1").on("apply.daterangepicker", function(ev, picker) {
            $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
        });

        $(\'#valuationsearch-time_period\').on(\'change\',function(){
            var period = $(this).val();
        
            if(period == 9){
                $(\'#date_range_array\').show();
            }else{
                $(\'#date_range_array\').hide();
            }
        });

        $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
        });

        //for date search instruction_date
        $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
            var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
            challenged_table.column(0).search(format1).draw();
            }else{
                challenged_table.search("").columns().search("").draw();
            }
        });

        //for date search challenged_date
        $("#challenged_date").on("change", function () {
            if($("#challenged_date").val() != ""){
            var format1 = moment($("#challenged_date").val()).format("DD-MMM-YYYY"); 
            challenged_table.column(1).search(format1).draw();
            }else{
                challenged_table.search("").columns().search("").draw();
            }
        });

        $(".custom-search-input").on("keyup", function () {
            challenged_table.search(this.value).draw();
        });
        
        $(".custom-search-input-text").on("keyup", function () {
            challenged_table.search(this.value).draw();
        });
        $(".custom-search-input").on("change", function () {
            challenged_table.search(this.value).draw();
        });

        ');
    ?>
<?php } ?>

<?php if($page_title == "Total Mistakes") { ?> 

    <div class="valuation-search">
        <?php $form = ActiveForm::begin([
            'action' => ['dashboard-report-all'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
            <span><strong><?= $page_title; ?></strong></span><br /><br /><br />
                <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                    <thead>
                        <tr>
                            <th class="">Instruction Date
                                <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Challenge Date
                                <input id="challenged_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Error Date
                                <input id="error_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">WM New Ref No.
                                <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                            </th>

                            <th class="">Client Ref
                                <input type="text" class="custom-search-input form-control" placeholder="Ref">
                            </th>

                            <th class="">Client 
                                <input type="text" class="custom-search-input form-control" placeholder="Client">
                            </th>

                            <th class="">Property
                                <input type="text" class="custom-search-input form-control" placeholder="Property">
                            </th>
                            
                            <th class="">Inspect
                                <input type="text" class="custom-search-input form-control" placeholder="I/O">
                            </th>

                            <th class="">Valuer
                                <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                            </th>

                            <th class="">Review
                                <input type="text" class="custom-search-input form-control" placeholder="Reviewer">
                            </th>

                            <th class="">Approv
                                <input type="text" class="custom-search-input form-control" placeholder="Approver">
                            </th>

                            <th class="">Market Val Appr
                                <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                            </th>

                            <th class="">Original Data
                                <input type="text" class="custom-search-input form-control" placeholder="Rev. Market Val">
                            </th>

                            <th class="">Revised Data
                                <input type="text" class="custom-search-input form-control" placeholder="Rev. Market Val">
                            </th>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($dataProvider->getModels())>0){
                            // dd($dataProvider->getModels());
                            foreach($dataProvider->getModels() as $model){
                                // dd($model->inspectProperty);
                        ?>
                        <tr class="active">

                            <!-- Instruction -->
                            <td>
                                <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                            </td>

                            <!-- Challenge Date -->
                            <td>
                                <?php 
                                    if($model->revised_reason == "3")
                                    {
                                        echo date('d-M-Y', strtotime($model->updated_at));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Client Error Date -->
                            <td>
                                <?php 

                                ?>
                            </td>

                            <!-- New Reference Number -->
                            <td>
                                <?php 
                                    if($model->revised_reason != NULL || $model->revised_reason != "" || $model->revised_reason > 0)
                                    {
                                        echo $model->reference_number;
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Client Ref -->
                            <td>
                                <?= $model->client_reference ?>
                            </td>

                            <!-- Client -->
                            <td>
                                <?= $model->client->nick_name ?>
                            </td>

                            <!-- Property -->
                            <td>
                                <?= $model->property->title ?>
                            </td>
                                    
                            <!-- Inspector -->
                            <td>
                                <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                            </td>

                            <!-- Valuer -->
                            <td>
                                <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                ?>
                            </td>

                            <!-- Reviewer -->
                            <td>
                                <?php 
                                    // $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                                
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    $get_name = User::find()->where(['id' => $get_valuation_data->updated_by])->one();
                                    if(isset($get_name->lastname))
                                    {
                                        echo $get_name->lastname;
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Approver -->
                            <td>
                                <?php 
                                    // $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                                
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "approver"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    $get_name = User::find()->where(['id' => $get_valuation_data->updated_by])->one();
                                    if(isset($get_name->lastname))
                                    {
                                        echo $get_name->lastname;
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Market Value approved -->
                            <td>
                                <?php 
                                    // $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => 'approver'])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    
                                    echo (isset($get_valuation_data->id) && ($get_valuation_data->id <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                                ?>
                            </td>

                            <!-- Original data -->
                            <td>
                                <?php
                                ?>
                            </td>

                            <!-- Revised data -->
                            <td>
                                <?php
                                ?>
                            </td>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                            href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                            title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> View 
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <?php
                            }
                            ?>

                        </tr>
                        <?php
                    }
                }
            ?>
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php
        $this->registerJs('

        let mistakes_table = new DataTable("#bank-revenue", {
            responsive: true,
            order: [[0, "desc"]],
            dom: "lrtip",

            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
        });

        $(".div1").daterangepicker({
            autoUpdateInput: false,
            locale: {
            format: "YYYY-MM-DD"
            }
        
        
        });
        
        $(".div1").on("apply.daterangepicker", function(ev, picker) {
            $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
        });

        $(\'#valuationsearch-time_period\').on(\'change\',function(){
            var period = $(this).val();
        
            if(period == 9){
                $(\'#date_range_array\').show();
            }else{
                $(\'#date_range_array\').hide();
            }
        });

        $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
        });

        //for date search instruction_date
        $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
            var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
            mistakes_table.column(0).search(format1).draw();
            }else{
                mistakes_table.search("").columns().search("").draw();
            }
        });

        //for date search challenged_date
        $("#challenged_date").on("change", function () {
            if($("#challenged_date").val() != ""){
            var format1 = moment($("#challenged_date").val()).format("DD-MMM-YYYY"); 
            mistakes_table.column(1).search(format1).draw();
            }else{
                mistakes_table.search("").columns().search("").draw();
            }
        });

        //for date search error_date
        $("#error_date").on("change", function () {
            if($("#error_date").val() != ""){
            var format1 = moment($("#error_date").val()).format("DD-MMM-YYYY"); 
            mistakes_table.column(2).search(format1).draw();
            }else{
                mistakes_table.search("").columns().search("").draw();
            }
        });

        $(".custom-search-input").on("keyup", function () {
            mistakes_table.search(this.value).draw();
        });
        
        $(".custom-search-input-text").on("keyup", function () {
            mistakes_table.search(this.value).draw();
        });
        $(".custom-search-input").on("change", function () {
            mistakes_table.search(this.value).draw();
        });

        ');
    ?>
<?php } ?>

<?php if($page_title == "Tat Reports") { ?> 
    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
            <span><strong><?= "Total TAT Reports" ?></strong></span><br /><br /><br />
                <table id="tat_table" class="table table-striped dataTable table-responsive">
                    <thead>
                        <tr>

                            <th class="">Instruction Date
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <th class="">Inspection Created
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <th class="">Inspection Schedule
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <th class="">Inspection Done
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <th class="">Recommended
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <th class="">Approval Date
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <th class="">Ref. No
                                <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                            </th>
                    
                            <th class="">Inspect
                                <input type="text" class="custom-search-input form-control" placeholder="I/O">
                            </th>

                            <th class="">Valuer
                                <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                            </th>

                            <th class="">Approver
                                <input type="text" class="custom-search-input form-control" placeholder="Approver">
                            </th>

                            <th class="">Reviewer
                                <input type="text" class="custom-search-input form-control" placeholder="Review">
                            </th>
                            
                            <th class="">Total TAT
                                <input type="text" class="custom-search-input form-control" placeholder="TAT">
                            </th>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($dataProvider->getModels())>0){
                            // dd($dataProvider->getModels());
                            foreach($dataProvider->getModels() as $model){
                        ?>
                        <tr class="active">

                            <!-- Instruction -->
                            <td>
                                <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                                <br>
                                <?php
                                    if(isset($model->instruction_time))
                                    {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time);
                                    }else{
                                        echo "";
                                    }  
                                ?>
                            </td>

                            <!-- Inspection Created -->
                            <td>
                                <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        if($model->scheduleInspection->created_at != '' && $model->created_at !='') {
                
                                            $startDate = $model->created_at;
                                            $endDate = $model->scheduleInspection->created_at;
                
                
                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);

                                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                            if($model->reference_number == 'REV-2023-10737'){
                                                $workingHours = $workingHours - 8.5;
                                            }
                                        }else{
                                            $workingHours = 0;
                                        }

                                        echo date('d-M-Y', strtotime($model->scheduleInspection->created_at)) . '<br>  ' . date('h:i A', strtotime($model->scheduleInspection->created_at)).'<br>'.'<strong>'.number_format($workingHours,1).' Hours'.'</strong>';
                
                                    }else{
                                        echo 'Desktop Valuation';
                                    }
                                ?>
                            </td>

                            <!-- Inspection Schedule -->
                            <td>
                                <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        $inspection_time = $model->inspection_time . ":00";
                                        if ($model->inspection_time == null) {
                                            $inspection_time = '00:00:00';
                                        }
                                        if($model->scheduleInspection->created_at != '' && $model->scheduleInspection->inspection_date !='') {
                
                                            $minus = 0;
                
                                            $startDate = $model->scheduleInspection->created_at;
                                            $endDate = $model->scheduleInspection->inspection_date.' '.$inspection_time;
                
                
                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);
                
                                            $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                            if($model->reference_number == 'REV-2023-10772'){
                                                $workingHours_1 = $workingHours_1 - 8.5;
                                            }
                
                
                                        }else{
                                            $workingHours_1 = 0;
                                        }
                                        echo date('d-M-Y', strtotime($model->scheduleInspection->inspection_date)) . '<br>  ' . Yii::$app->appHelperFunctions->formatTimeAmPm($model->scheduleInspection->inspection_time).'<br>'.'<strong>'.number_format($workingHours_1,1).' Hours'.'</strong>';

                                    }else{
                                        echo 'Desktop Valuation';
                                    }
                                ?>
                            </td>

                            <!-- Inspection Done -->
                            <td>
                                <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        
                                        $inspection_time = $model->scheduleInspection->inspection_time . ":00";
                                        if ($model->scheduleInspection->inspection_time == null) {
                                            $inspection_time = '00:00:00';
                                        }
                                        if($model->inspection_done_date_time != '' && $model->scheduleInspection->inspection_date !='') {
                                            $minus = 0;
                
                                            //  $startDate = $model->scheduleInspection->created_at;
                                            $startDate = $model->scheduleInspection->inspection_date.' '.$inspection_time;
                                            $endDate = $model->inspection_done_date_time;
                
                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);
                
                                            $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                            /* if($model->reference_number == 'REV-2023-10772'){
                                                $workingHours = $workingHours - 8.5;
                                            }*/
                                        }else{
                                            $workingHours_2 = 0;
                                        }
                                        echo date('d-M-Y', strtotime($model->inspection_done_date_time)) . '<br>  ' . date('h:i A', strtotime($model->inspection_done_date_time)).'<br>'.'<strong>'.number_format($workingHours_2,1).' Hours'.'</strong>';
                                    }else{
                                        echo 'Desktop Valuation';
                                    }
                                ?>
                            </td>

                            <!-- Recommended -->
                            <td>
                                <?php
                                    if($model->inspection_done_date_time != '' && $model->valuerData->created_at!='') {

                                        $minus = 0;

                                        $startDate = $model->inspection_done_date_time;
                                        $endDate = $model->valuerData->created_at;

                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);

                                        $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        if($model->reference_number == 'REV-2023-10756'){
                                            $workingHours_2 = $workingHours_2 - 8.5;
                                        }
                                    }else{
                                        $workingHours_2 = 0;
                                    }
                                    echo date('d-M-Y', strtotime($model->inspection_done_date_time)) . '<br>  ' . date('h:i A', strtotime($model->inspection_done_date_time)).'<br>'.'<strong>'.number_format($workingHours_2,1).' Hours'.'</strong>';
                                ?>
                            </td>

                            <!-- Approval date -->
                            <td>
                                <?php
                                    if(isset($model->submission_approver_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->submission_approver_date));
                                    }else{
                                        echo "";
                                    }
                                ?>
                                <br>
                                <?php
                                    if(isset($model->submission_approver_date))
                                    {
                                        echo date('h:i A', strtotime($model->submission_approver_date));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Reference Number -->
                            <td>
                                <?= $model->reference_number ?>
                            </td>

                            <!-- Inspection Officer -->
                            <td>
                                <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        echo (isset($model->inspection_officer_name) && ($model->inspection_officer_name <> null))? (Yii::$app->appHelperFunctions->staffMemberListArrLastName[$model->scheduleInspection->inspection_officer]): '';
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Valuer -->
                            <td>
                                <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                ?>
                            </td>

                            <!-- Approver -->
                            <td>
                                <?php
                                    $type = $model->approverData->approver_type;
                                    if($type == "approver")
                                    {
                                        $get_name = User::find()->where(['id' => $model->approverData->created_by])->one();
                                        echo $get_name->lastname ;
                                    }else{
                                        echo '';
                                    }
                                ?>
                            </td>

                            <!-- Reviewer -->
                            <td>
                                <?php 
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();

                                    $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                    echo  $get_name->lastname;
                                ?>
                            </td>

                            <!-- Total TAT -->
                            <td>
                                <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        $inspection_time = $model->inspection_time . ":00";
                                        if ($model->inspection_time == null) {
                                            $inspection_time = '00:00:00';
                                        }
                                        if($model->instruction_date !='' && $model->inspection_date !='') {

                                            $startDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                            $endDate = $model->inspection_date . ' ' . $inspection_time;
                
                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);
                                            $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        }else{
                                            $workingHours_1 =0;
                                        }
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //  $time_1 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1));
                                        //  $time_1_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));
                                        if($model->inspection_date !='' && $model->submission_approver_date !='') {
                                            $startDate_2 = $model->inspection_date . ' ' . $inspection_time;
                                            $endDate_2 = $model->submission_approver_date;
                                            $startDateTime = new DateTime($startDate_2); // Start date and time
                                            $endDateTime = new DateTime($endDate_2);
                                            /*echo $startDate_2.' '.$endDate_2;
                                            die;*/
                                            $workingHours_2 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        }else{
                                            $workingHours_2=0;
                                        }
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //  $time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                        // $time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));
                
                
                
                                        // print_r( abs( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate ), 2) ) ); echo "<br>";
                                        // die;
                                        $workingHours = $workingHours_1 + $workingHours_2;
                                        if($model->reference_number == 'REV-2023-10737' || $model->reference_number == 'REV-2023-10756'){
                                            $workingHours = $workingHours - 8.5;
                                        }
                                        $test = abs(number_format(($workingHours/8.5),2)).'<br>'.'<br>'.'<strong>'.number_format($workingHours,1).' Hours'.'</strong>';
                                        echo $test;
                
                                    }else{
                                        $startDate_2 = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                        $endDate_2 = $model->submission_approver_date;
                                        $startDateTime = new DateTime($startDate_2); // Start date and time
                                        $endDateTime = new DateTime($endDate_2);
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //$time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                        //$time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));
                                        if($model->instruction_date !='' && $model->submission_approver_date !='') {
                                            $startDateTime = new DateTime($startDate_2); // Start date and time
                                            $endDateTime = new DateTime($endDate_2);
                                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                            if($model->reference_number == 'REV-2023-10772' || $model->reference_number == 'REV-2023-10756'){
                                                $workingHours = $workingHours - 8.5;
                                            }
                                            $test = abs(number_format(($workingHours/8.5),1)).'<br>'.'<br>'.'<strong>'.number_format($workingHours).' Hours'.'</strong>';
                                            echo $test;
                                        }else{
                                            echo '';
                                        }
                
                                    }
                                ?>
                            </td>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                            href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                            title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> View 
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <?php
                            }
                            ?>

                        </tr>
                        <?php
                    }
                }
            ?>
                    </tbody>
                    <tfoot>

                    <th>Average:</th>
                    <th><?= number_format(($diff_1/$total_rows),1).' Hours' ?></th>
                    <th><?= number_format(($diff_3/$total_rows),1).' Hours' ?></th>
                    <th><?= number_format(($diff_4/$total_rows),1).' Hours' ?></th>
                    <th><?= number_format(($diff_5/$total_rows),1).' Hours' ?></th>
                    <!-- <th><?= number_format(($diff_6/$total_rows),1).' Hours' ?></th> -->
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    <th><?= number_format(($diff_9/$total_rows),1).' Hours' ?></th>
                        <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
<?php } ?>

<?php if($page_title == "Total Ahead of Time Reports"  || $page_title == "Total On Time" || $page_title == "Total Delay Reports" ) { ?> 
    
    <div class="card card-outline card-warning mx-2">
        <div class="card-body search_filter">
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label> General Search : </label>
                        <input type="search" id="custom-search-input-text" placeholder="Search from datatable" class="form-control custom-search-input-text">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if($page_title == "Total Ahead of Time Reports") { ?>
        <div class="valuation-search">
            <?php $form = ActiveForm::begin([
                'action' => ['tat-ahead-reports'],
                'method' => 'get',
            ]); ?>
                <div class="row">
                    <div class="col-sm-3"></div>

                    <div class="col-sm-6 text-center">
                        <?php
                        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                            'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                            'options' => ['placeholder' => 'Time Frame ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>
                    </div>
                </div>
                <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" id="end_date_id">

                        <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                        <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="text-center">
                    <div class="form-group">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    <?php } ?>

    <?php if($page_title == "Total On Time") { ?>
        <div class="valuation-search">
            <?php $form = ActiveForm::begin([
                'action' => ['tat-on-time-reports'],
                'method' => 'get',
            ]); ?>
                <div class="row">
                    <div class="col-sm-3"></div>

                    <div class="col-sm-6 text-center">
                        <?php
                        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                            'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                            'options' => ['placeholder' => 'Time Frame ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>
                    </div>
                </div>
                <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" id="end_date_id">

                        <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                        <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="text-center">
                    <div class="form-group">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    <?php } ?>

    <?php if($page_title == "Total Delay Reports") { ?>
        <div class="valuation-search">
            <?php $form = ActiveForm::begin([
                'action' => ['tat-delay-reports'],
                'method' => 'get',
            ]); ?>
                <div class="row">
                    <div class="col-sm-3"></div>

                    <div class="col-sm-6 text-center">
                        <?php
                        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                            'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                            'options' => ['placeholder' => 'Time Frame ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>
                    </div>
                </div>
                <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" id="end_date_id">

                        <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                        <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="text-center">
                    <div class="form-group">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    <?php } ?>

    
    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
            <span><strong><?= $page_title; ?></strong></span><br /><br /><br />
                <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                    <thead>
                        <tr>
                            <th class="">Instruction Date
                                <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Inspection Schedule
                                <input id="schedule_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Approval Date
                                <input id="approval_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Reference Number
                                <input id="reference_number" type="text" class="custom-search-input form-control" placeholder="Ref. No.">
                            </th>

                            <th class="">Client Reference
                                <input id="Client Ref" type="text" class="custom-search-input form-control" placeholder="Ref. No.">
                            </th>

                            <th class="">Valuer
                                <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                            </th>

                            <th class="">Reviewer
                                <input type="text" class="custom-search-input form-control" placeholder="Reviewer">
                            </th>

                            <th class="">Approver
                                <input type="text" class="custom-search-input form-control" placeholder="Approver">
                            </th>

                            <?php if(in_array( Yii::$app->user->identity->id, $allowedId)) { ?>
                                <th class="">Fee
                                    <input type="text" class="custom-search-input form-control" placeholder="Fee">
                                </th>
                            <?php } ?>

                            <th class="">Total TAT
                                <input type="text" class="custom-search-input form-control" placeholder="VAT">
                            </th>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($dataProvider->getModels())>0){
                            // dd($dataProvider->getModels());
                            foreach($dataProvider->getModels() as $model){
                        ?>
                        <tr class="active">

                            <!-- Instruction -->
                            <td>
                                <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                                <br>
                                
                                <?php
                                    if(isset($model->instruction_time))
                                    {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time);
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Inspection schedule date  -->
                            <td>
                                <?=  date('d-M-Y', strtotime($model->scheduleInspection->inspection_date)) ?>
                                <!-- <br>
                                <?= Yii::$app->appHelperFunctions->formatTimeAmPm($model->scheduleInspection->inspection_time) ?> -->
                            </td>

                            <!-- Approval date  -->
                            <td>
                            <?=  date('d-M-Y', strtotime($model->approverData->created_at)) ?>
                            </td>

                            <!-- Reference Number -->
                            <td>
                                <?= $model->reference_number ?>
                            </td>

                            <!-- Client Ref Number -->
                            <td>
                            <?= $model->client_reference ?>
                            </td>

                            <!-- Valuer -->
                            <td>
                                <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                ?>
                            </td>

                            <!-- Reviewer -->
                            <td>
                                <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();

                                    $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                    echo $get_name->lastname ;
                                ?>
                            </td>

                            <!-- Approver -->
                            <td>
                                <?php
                                echo $model->approverData->user->lastname ;
                                ?>
                            </td>

                            <?php if(in_array( Yii::$app->user->identity->id, $allowedId)) { ?>
                                <!-- Fee -->
                                <td>
                                    <?= Yii::$app->appHelperFunctions->wmFormate($model->total_fee) ?>
                                </td>
                            <?php } ?>

                            <!-- Total TAT -->
                            <td>
                                <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        $inspection_time = $model->inspection_time . ":00";
                                        if ($model->inspection_time == null) {
                                            $inspection_time = '00:00:00';
                                        }

                                        if($model->instruction_date !='' && $model->submission_approver_date !='') {
                                            $startDate_2 = $model->instruction_date . ' ' . $inspection_time;
                                            $endDate_2 = $model->submission_approver_date;
                                            $startDateTime = new DateTime($startDate_2); // Start date and time
                                            $endDateTime = new DateTime($endDate_2);

                                            $workingHours_2 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        }else{
                                            $workingHours_2=0;
                                        }
                                        $test = abs(number_format(($workingHours_2/8.5),2)).'<br>'.'<br>'.'<strong>'.number_format($workingHours_2,1).' Hours'.'</strong>';
                                        echo $test;
                
                                    }else{
                                        echo "Desktop Valuation";
                                    }
                                ?>
                            </td>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                            href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                            title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> View 
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <?php
                            }
                            ?>

                        </tr>
                        <?php
                    }
                }
            ?>
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php
        $this->registerJs('

        let delay_table = new DataTable("#bank-revenue", {
            responsive: true,
            order: [[0, "desc"]],
            dom: "lrtip",

            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
        });

        $(".div1").daterangepicker({
            autoUpdateInput: false,
            locale: {
            format: "YYYY-MM-DD"
            }
        });
        
        $(".div1").on("apply.daterangepicker", function(ev, picker) {
            $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
        });

        $(\'#valuationreportssearch-time_period\').on(\'change\',function(){
            var period = $(this).val();
        
            if(period == 9){
                $(\'#date_range_array\').show();
            }else{
                $(\'#date_range_array\').hide();
            }
        });

        $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
        });

        //for date search instruction_date
        $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
            var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
            delay_table.column(0).search(format1).draw();
            }else{
                delay_table.search("").columns().search("").draw();
            }
        });

        //for date search schedule_date
        $("#schedule_date").on("change", function () {
            if($("#schedule_date").val() != ""){
            var format1 = moment($("#schedule_date").val()).format("DD-MMM-YYYY"); 
            delay_table.column(1).search(format1).draw();
            }else{
                delay_table.search("").columns().search("").draw();
            }
        });

        //for date search approval_date
        $("#approval_date").on("change", function () {
            if($("#approval_date").val() != ""){
            var format1 = moment($("#approval_date").val()).format("DD-MMM-YYYY"); 
            delay_table.column(2).search(format1).draw();
            }else{
                delay_table.search("").columns().search("").draw();
            }
        });

        $(".custom-search-input").on("keyup", function () {
            delay_table.search(this.value).draw();
        });
        
        $(".custom-search-input-text").on("keyup", function () {
            delay_table.search(this.value).draw();
        });
        $(".custom-search-input").on("change", function () {
            delay_table.search(this.value).draw();
        });

        ');
    ?>
<?php } ?>

<?php if($page_title == "Total Reminders") { ?> 

    <div class="valuation-search">
        <?php $form = ActiveForm::begin([
            'action' => ['dashboard-report-all'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
            <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
                <table id="reminders_table" class="table table-striped dataTable table-responsive">
                    <thead>
                        <tr>

                            <th class="">Instruction Date
                                <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Inspection Done
                                <input id="done_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Recommended
                                <input id="recommended_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Review Date
                                <input id="review_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Approval Date
                                <input id="approval_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Reminder Date
                                <input id="reminder_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">New Ref Number
                                <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                            </th>
                    
                            <th class="">Inspect
                                <input type="text" class="custom-search-input form-control" placeholder="I/O">
                            </th>

                            <th class="">Valuer
                                <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                            </th>

                            <th class="">Review
                                <input type="text" class="custom-search-input form-control" placeholder="Reviewer">
                            </th>

                            <th class="">Approver
                                <input type="text" class="custom-search-input form-control" placeholder="Approver">
                            </th>

                            <th class="">Status
                                <input type="text" class="custom-search-input form-control" placeholder="Status">
                            </th>
                            
                            <th class="">Total TAT
                                <input type="text" class="custom-search-input form-control" placeholder="TAT">
                            </th>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($dataProvider->getModels())>0){
                            // dd($dataProvider->getModels());
                            foreach($dataProvider->getModels() as $model){
                        ?>
                        <tr class="active">

                            <!-- Instruction -->
                            <td>
                                <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                                <br>
                                <?php
                                    if(isset($model->instruction_time))
                                    {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time);
                                    }else{
                                        echo "";
                                    }  
                                ?>
                            </td>

                            <!-- Inspection Done -->
                            <td>
                                <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        echo date('d-M-Y', strtotime($model->inspectProperty->inspection_done_date)) . '<br>  ' . date('h:i A', strtotime($model->inspection_done_date_time));
                                    }else{
                                        echo 'Desktop Valuation';
                                    }
                                ?>
                            </td>

                            <!-- Recommended -->
                            <td>
                                <?php 
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "valuer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    if(isset($get_valuation_data->updated_at))
                                    {
                                        echo date('d-M-Y', strtotime($get_valuation_data->updated_at));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Review date -->
                            <td>
                                <?php 
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    
                                    if(isset($get_valuation_data->created_at))
                                    {
                                        echo date('d-M-Y', strtotime($get_valuation_data->created_at));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Approval date -->
                            <td>
                                <?php
                                if(isset($model->submission_approver_date))
                                {
                                    echo date('d-M-Y', strtotime($model->submission_approver_date));
                                }else{
                                    echo "";
                                }

                            ?>
                            </td>

                            <!-- Reminder date -->
                            <td>
                                <?php ?>
                            </td>

                            <!-- New Ref Number -->
                            <td>
                                <?php ?>
                            </td>

                            <!-- Inspection Officer -->
                            <td>
                                <?php
                                    echo (isset($model->inspection_officer_name) && ($model->inspection_officer_name <> null))? (Yii::$app->appHelperFunctions->staffMemberListArrLastName[$model->scheduleInspection->inspection_officer]): '';
                                ?>
                            </td>

                            <!-- Valuer -->
                            <td>
                                <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                ?>
                            </td>

                            <!-- Reviewer -->
                            <td>
                                <?php 
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();

                                    $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                    echo  $get_name->lastname;
                                ?>
                            </td>

                            <!-- Approver -->
                            <td>
                                <?php
                                    $type = $model->approverData->approver_type;
                                    if($type == "approver")
                                    {
                                        $get_name = User::find()->where(['id' => $model->approverData->created_by])->one();
                                        echo $get_name->lastname ;
                                    }else{
                                        echo '';
                                    }
                                ?>
                            </td>

                            <!-- Status -->
                            <td>
                            <?= Yii::$app->helperFunctions->valuationStatusListArrWords[$model->valuation_status] ?>
                            </td>

                            <!-- Total TAT -->
                            <td>
                                <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        $inspection_time = $model->inspection_time . ":00";
                                        if ($model->inspection_time == null) {
                                            $inspection_time = '00:00:00';
                                        }
                                        if($model->instruction_date !='' && $model->inspection_date !='') {

                                            $startDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                            $endDate = $model->inspection_date . ' ' . $inspection_time;
                
                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);
                                            $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        }else{
                                            $workingHours_1 =0;
                                        }
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //  $time_1 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1));
                                        //  $time_1_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));
                                        if($model->inspection_date !='' && $model->submission_approver_date !='') {
                                            $startDate_2 = $model->inspection_date . ' ' . $inspection_time;
                                            $endDate_2 = $model->submission_approver_date;
                                            $startDateTime = new DateTime($startDate_2); // Start date and time
                                            $endDateTime = new DateTime($endDate_2);
                                            /*echo $startDate_2.' '.$endDate_2;
                                            die;*/
                                            $workingHours_2 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        }else{
                                            $workingHours_2=0;
                                        }
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //  $time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                        // $time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));
                
                
                
                                        // print_r( abs( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate ), 2) ) ); echo "<br>";
                                        // die;
                                        $workingHours = $workingHours_1 + $workingHours_2;
                                        if($model->reference_number == 'REV-2023-10737' || $model->reference_number == 'REV-2023-10756'){
                                            $workingHours = $workingHours - 8.5;
                                        }
                                        $test = abs(number_format(($workingHours/8.5),2)).'<br>'.'<br>'.'<strong>'.number_format($workingHours,1).' Hours'.'</strong>';
                                        echo $test;
                
                                    }else{
                                        $startDate_2 = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                        $endDate_2 = $model->submission_approver_date;
                                        $startDateTime = new DateTime($startDate_2); // Start date and time
                                        $endDateTime = new DateTime($endDate_2);
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //$time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                        //$time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));
                                        if($model->instruction_date !='' && $model->submission_approver_date !='') {
                                            $startDateTime = new DateTime($startDate_2); // Start date and time
                                            $endDateTime = new DateTime($endDate_2);
                                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                            if($model->reference_number == 'REV-2023-10772' || $model->reference_number == 'REV-2023-10756'){
                                                $workingHours = $workingHours - 8.5;
                                            }
                                            $test = abs(number_format(($workingHours/8.5),1)).'<br>'.'<br>'.'<strong>'.number_format($workingHours).' Hours'.'</strong>';
                                            echo $test;
                                        }else{
                                            echo '';
                                        }
                
                                    }
                                ?>
                            </td>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                            href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                            title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> View 
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <?php
                            }
                            ?>

                        </tr>
                        <?php
                    }
                }
            ?>
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php
        $this->registerJs('

        let reminders_table = new DataTable("#reminders_table", {
            responsive: true,
            order: [[0, "desc"]],
            dom: "lrtip",

            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
        });

        $(".div1").daterangepicker({
            autoUpdateInput: false,
            locale: {
            format: "YYYY-MM-DD"
            }
        
        
        });
        
        $(".div1").on("apply.daterangepicker", function(ev, picker) {
            $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
        });

        $(\'#valuationsearch-time_period\').on(\'change\',function(){
            var period = $(this).val();
        
            if(period == 9){
                $(\'#date_range_array\').show();
            }else{
                $(\'#date_range_array\').hide();
            }
        });

        $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
        });

        //for date search instruction_date
        $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
            var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
            reminders_table.column(0).search(format1).draw();
            }else{
                reminders_table.search("").columns().search("").draw();
            }
        });

        //for date search done_date
        $("#done_date").on("change", function () {
            if($("#done_date").val() != ""){
            var format1 = moment($("#done_date").val()).format("DD-MMM-YYYY"); 
            reminders_table.column(1).search(format1).draw();
            }else{
                reminders_table.search("").columns().search("").draw();
            }
        });

        //for date search recommended_date
        $("#recommended_date").on("change", function () {
            if($("#recommended_date").val() != ""){
            var format1 = moment($("#recommended_date").val()).format("DD-MMM-YYYY"); 
            reminders_table.column(2).search(format1).draw();
            }else{
                reminders_table.search("").columns().search("").draw();
            }
        });

        //for date search review_date
        $("#review_date").on("change", function () {
            if($("#review_date").val() != ""){
            var format1 = moment($("#review_date").val()).format("DD-MMM-YYYY"); 
            reminders_table.column(3).search(format1).draw();
            }else{
                reminders_table.search("").columns().search("").draw();
            }
        });

        //for date search approval_date
        $("#approval_date").on("change", function () {
            if($("#approval_date").val() != ""){
            var format1 = moment($("#approval_date").val()).format("DD-MMM-YYYY"); 
            reminders_table.column(4).search(format1).draw();
            }else{
                reminders_table.search("").columns().search("").draw();
            }
        });

        //for date search reminder_date
        $("#reminder_date").on("change", function () {
            if($("#reminder_date").val() != ""){
            var format1 = moment($("#reminder_date").val()).format("DD-MMM-YYYY"); 
            reminders_table.column(5).search(format1).draw();
            }else{
                reminders_table.search("").columns().search("").draw();
            }
        });

        $(".custom-search-input").on("keyup", function () {
            reminders_table.search(this.value).draw();
        });
        
        $(".custom-search-input-text").on("keyup", function () {
            reminders_table.search(this.value).draw();
        });
        $(".custom-search-input").on("change", function () {
            reminders_table.search(this.value).draw();
        });

        ');
    ?>

<?php } ?>