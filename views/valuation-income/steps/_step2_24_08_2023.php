<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Receive Info/Documents');
$cardTitle = Yii::t('app', 'Receive Info/Documents:  {nameAttribute}', [
    'nameAttribute' => $valuation->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_2/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
if ($model->email_status !=1) {
    $AlertText='Data and Valuation Status will be saved and Email will be sent once?';
}
else{
    $AlertText='Only Data will be saved?';
}



$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "'.Yii::t('app',$AlertText).'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
      console.log("Hello 123")
      $("#w0").unbind("submit").submit();
    }
});
});


var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});




$("body").on("click", ".delete-file", function (e) {

    id = $(this).attr("id")
    data = {id:id}

    swal({
    title: "'.Yii::t('app','Are you sure you want to delete file from bucket ?').'",
    html: "'.Yii::t('app','').'",
    type: "warning",
    showCancelButton: true,
        confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Yes, Delete it!').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
    },function(result) {
        if (result) {
            $.ajax({
            data : data,
            url: "'.Url::to(['valuation/delete-file']).'",
            dataType: "html",
            type: "POST",
            success: function(data) {
                data = JSON.parse(data)
                if(data.msg == "success"){

                    deleted = "#deleted-"+id;
                    $(deleted). attr("src", "'.Yii::$app->params['uploadIcon'].'");

                    removed = ".removed-"+id;
                    $(removed).remove()

                    removedDelBtn = "#del-btn-"+id;
                    $(removedDelBtn).remove()

                    swal({
                        title: "'.Yii::t('app','Successfully Deleted').'",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#47a447",
                    });
                }
            },
            error: bbAlert
        });
        }
    });
    });
























');

$image_row = 0;
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 2]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">
                                <?php if(Yii::$app->user->identity->permission_group_id != 15){ ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'dld_data_enquiry')->widget(Select2::classname(), [
                                            'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'kharetati_application_enquiry')->widget(Select2::classname(), [
                                            'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'dld_sold_transaction_enquiry')->widget(Select2::classname(), [
                                            'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'dld_valuations_enquir')->widget(Select2::classname(), [
                                            'data' => array('No' => 'No', 'Yes' => 'Yes',),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'discussions_real_estate')->widget(Select2::classname(), [
                                            'data' => array('No' => 'No', 'Yes' => 'Yes',),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>

                                <!-- Owners Information-->
                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Owners Information</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row" style="padding: 10px">
                                            <?php $owner_row = 0; ?>


                                            <table id="owner_container"
                                                   class="table table-striped table-bordered table-hover images-table ">
                                                <thead>
                                                <tr>
                                                    <td style="padding-left: 5px !important;"
                                                        class="text-left  width_40"><strong>Name</strong></td>
                                                    <td style="padding-left: 5px !important;"
                                                        class="text-left  width_40"><strong>Percentage</strong></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php

                                                if ($valuation->no_of_owners > 0) {
                                                    for ($o = 0; $o < $valuation->no_of_owners; $o++) {
                                                        $owner_Details = \app\models\ValuationOwners::find()->where(["valuation_id" => $valuation->id, "index_id" => $o])->one();
                                                        ?>
                                                        <tr id="owner_data_row<?php echo($o); ?>">


                                                            <td class="text-left">
                                                                <div class="form-group">
                                                                    <input type="text"
                                                                           name="ReceivedDocs[owners_data][<?= $o; ?>][name]"
                                                                           value="<?= ($owner_Details <> null) ? $owner_Details->name : "" ?>"
                                                                           placeholder="Name" class="form-control"
                                                                           required/>
                                                                </div>
                                                            </td>

                                                            <td class="text-left">
                                                                <div class="form-group">
                                                                    <input type="text"
                                                                           name="ReceivedDocs[owners_data][<?= $o; ?>][percentage]"
                                                                           value="<?= ($owner_Details <> null) ? $owner_Details->percentage : "" ?>"
                                                                           placeholder="Percentage" class="form-control"
                                                                           required/>
                                                                </div>
                                                            </td>
                                                            <input type="hidden"
                                                                   name="ReceivedDocs[owners_data][<?= $o; ?>][valuation_id]"
                                                                   value="<?= $valuation->id; ?>"
                                                            />
                                                            <input type="hidden"
                                                                   name="ReceivedDocs[owners_data][<?= $o; ?>][index_id]"
                                                                   value="<?= $o; ?>"
                                                            />
                                                        </tr>
                                                        <?php

                                                    }
                                                }
                                                ?>
                                                </tbody>
                                                <tfoot>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </section>
                                <!-- Media Information-->
                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Documents Information</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row" style="padding: 10px">

                                            <?php

                                            if (isset($valuation->property->required_documents) && ($valuation->property->required_documents <> null)) {
                                                $required_documents = explode(',', $valuation->property->required_documents);
                                            }
                                            ?>
                                            <?php

                                            if (isset($valuation->property->optional_documents) && ($valuation->property->optional_documents <> null)) {
                                                $optional_documents = explode(',', $valuation->property->optional_documents);
                                            }
                                            ?>

                                            <?php $unit_row = 0; ?>

                                            <?php if($required_documents <> null && !empty($required_documents)) { ?>
                                            <table id="requestTypes"
                                                   class="table table-striped table-bordered table-hover images-table">
                                                <thead>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>Attachment</td>
                                                    <td>Date</td>
                                                    <td></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php

                                                foreach ($required_documents as $required_document) {
                                                    $attachment_Details = \app\models\ReceivedDocsFiles::find()->where(["valuation_id" => $valuation->id, "document_id" => $required_document])->one();
                                                    // echo "<pre>"; print_r($attachment_Details); echo "</pre>"; die;
                                                    $doc_insert_date='';
                                                    if($attachment_Details->attachment<>null){
                                                        if($attachment_Details->doc_insert_date<>null){  
                                                            $doc_insert_date = date('d F Y', strtotime($attachment_Details->doc_insert_date));
                                                        }else{
                                                            $data = \app\models\ScheduleInspection::find()->where(['valuation_id' => $attachment_Details->valuation_id])->one();
                                                            $doc_insert_date = ($data->inspection_date<>null) ? date('d F Y', strtotime($data->inspection_date)) : '';
                                                        }
                                                    }
                                                    // dd($attachment_Details->id);
                                                    $attachment = $attachment_Details->attachment;
                                                    ?>

                                                    <tr id="image-row<?php echo $unit_row; ?>">

                                                        <td class="text-left">
                                                            <div class="required">


                                                                <label class="control-label">
                                                            <?= Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] ?>
                                                                </label>
                                                            </div>
                                                        </td>

                                                        <td class="text-left upload-docs">
                                                            <div class="form-group">
                                                                <a href="javascript:;"
                                                                   id="upload-document<?= $unit_row; ?>"
                                                                   onclick="uploadAttachment(<?= $unit_row; ?>)"
                                                                   data-toggle="tooltip"
                                                                   class="img-thumbnail"
                                                                   title="Upload Document">
                                                                    <?php

                                                                    if ($attachment <> null) {

                                                                        if (strpos($attachment, '.pdf') !== false) {
                                                                            ?>
                                                                            <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                                                 id="deleted-<?= $attachment_Details->id ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                            </a>

                                                                            <?php

                                                                        } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                                            ?>
                                                                            <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                                                id="deleted-<?= $attachment_Details->id ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                            </a>
                                                                            <?php

                                                                        } else {
                                                                            ?>
                                                                            <img src="<?php echo $attachment; ?>"
                                                                                id="deleted-<?= $attachment_Details->id ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                            </a>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                                             alt="" title=""
                                                                             data-placeholder="no_image.png"/>
                                                                        <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                                                           target="_blank">
                                                                            <span class="glyphicon glyphicon-eye-open"></span>
                                                                        </a>
                                                                        <?php
                                                                    }

                                                                    ?>
                                                                </a>
                                                                <input type="hidden"
                                                                       class="removed-<?= $attachment_Details->id; ?>"
                                                                       name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][attachment]"
                                                                       value="<?= $attachment; ?>"
                                                                       id="input-attachment<?php echo $unit_row; ?>"/>
                                                                <input type="hidden"
                                                                       class="removed-<?= $attachment_Details->id; ?>"
                                                                       name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][document_id]"
                                                                       value="<?= $required_document; ?>"
                                                                       id="input-attachment<?php echo $unit_row; ?>"/>

                                                                <input type="hidden"
                                                                       class="removed-<?= $attachment_Details->id; ?>"
                                                                       name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][valuation_id]"
                                                                       value="<?= $valuation->id; ?>"
                                                                       id="input-history_id<?php echo $unit_row; ?>"/>
                                                            </div>
                                                        </td>

                                                            <td>
                                                                <span class="badge grid-badge badge-info bg-info"><?= ($doc_insert_date<>null) ? $doc_insert_date : ''; ?></span>
                                                            </td>

                                                        </tr>
                                                        <?php $unit_row++; ?>

                                                    <?php }
                                                    foreach ($optional_documents as $optinal_document) {
                                                        $attachment_Details = \app\models\ReceivedDocsFiles::find()->where(["valuation_id" => $valuation->id, "document_id" => $optinal_document])->one();
                                                        // echo "<pre>"; print_r($attachment_Details); echo "</pre>"; die;
                                                        $doc_insert_date='';
                                                        if($attachment_Details->attachment<>null){
                                                            if($attachment_Details->doc_insert_date<>null){
                                                                $doc_insert_date = date('d F Y', strtotime($attachment_Details->doc_insert_date));
                                                            }else{
                                                                $data = \app\models\ScheduleInspection::find()->where(['valuation_id' => $attachment_Details->valuation_id])->one();
                                                                $doc_insert_date = ($data->inspection_date<>null) ? date('d F Y', strtotime($data->inspection_date)) : '';
                                                            }
                                                        }
                                                        // dd($attachment_Details->id);
                                                        $attachment = $attachment_Details->attachment;
                                                        ?>

                                                        <tr id="image-row<?php echo $unit_row; ?>">

                                                            <td class="text-left ">
                                                                <div class="required">


                                                                    <label>
                                                                        <?= Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$optinal_document] ?>
                                                                    </label>
                                                                </div>
                                                            </td>

                                                            <td class="text-left upload-docs">
                                                                <div class="form-group">
                                                                    <a href="javascript:;"
                                                                       id="upload-document<?= $unit_row; ?>"
                                                                       onclick="uploadAttachment(<?= $unit_row; ?>)"
                                                                       data-toggle="tooltip"
                                                                       class="img-thumbnail"
                                                                       title="Upload Document">
                                                                        <?php

                                                                        if ($attachment <> null) {

                                                                            if (strpos($attachment, '.pdf') !== false) {
                                                                                ?>
                                                                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                                                     id="deleted-<?= $attachment_Details->id ?>"
                                                                                     alt="" title=""
                                                                                     data-placeholder="no_image.png"/>
                                                                                <a href="<?= $attachment; ?>"
                                                                                   target="_blank">
                                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                                </a>
                                                                                <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                                </a>

                                                                                <?php

                                                                            } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                                                ?>
                                                                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                                                     id="deleted-<?= $attachment_Details->id ?>"
                                                                                     alt="" title=""
                                                                                     data-placeholder="no_image.png"/>
                                                                                <a href="<?= $attachment; ?>"
                                                                                   target="_blank">
                                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                                </a>
                                                                                <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                                </a>
                                                                                <?php

                                                                            } else {
                                                                                ?>
                                                                                <img src="<?php echo $attachment; ?>"
                                                                                     id="deleted-<?= $attachment_Details->id ?>"
                                                                                     alt="" title=""
                                                                                     data-placeholder="no_image.png"/>
                                                                                <a href="<?= $attachment; ?>"
                                                                                   target="_blank">
                                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                                </a>
                                                                                <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                                </a>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <?php
                                                                        }

                                                                        ?>
                                                                    </a>
                                                                    <input type="hidden"
                                                                           class="removed-<?= $attachment_Details->id; ?>"
                                                                           name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][attachment]"
                                                                           value="<?= $attachment; ?>"
                                                                           id="input-attachment<?php echo $unit_row; ?>"/>
                                                                    <input type="hidden"
                                                                           class="removed-<?= $attachment_Details->id; ?>"
                                                                           name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][document_id]"
                                                                           value="<?= $optinal_document; ?>"
                                                                           id="input-attachment<?php echo $unit_row; ?>"/>

                                                                    <input type="hidden"
                                                                           class="removed-<?= $attachment_Details->id; ?>"
                                                                           name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][valuation_id]"
                                                                           value="<?= $valuation->id; ?>"
                                                                           id="input-history_id<?php echo $unit_row; ?>"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <span class="badge grid-badge badge-info bg-info"><?= ($doc_insert_date<>null) ? $doc_insert_date : ''; ?></span>
                                                            </td>


                                                        </tr>
                                                    <?php $unit_row++; ?>

                                                <?php }

                                                ?>
                                                </tbody>
                                                <tfoot>

                                                </tfoot>
                                            </table>
                                            <?php } ?>


                                        </div>
                                    </div>

                                    <?php $model->step=1;  ?>
                                    <?php echo $form->field($model, 'step')->textInput(['maxlength' => true,
                                        'type'=>'hidden'])->label('') ?>
                                </section>


                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>


<script type="text/javascript">
    var owner_row = <?= $owner_row ?>;

    function addOwner() {
        html = '<tr id="owner_data_row' + owner_row + '" class="padding_5">';


        html += '  <td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="ReceivedDocs[owners_data][' + owner_row + '][name]" value="" placeholder="Name" class="form-control" required />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="ReceivedDocs[owners_data][' + owner_row + '][percentage]" value="" placeholder="Percentage" class="form-control" required />';
        html += '    </div>';
        html += '  </td>';
        html += ' <input type="hidden" name="ReceivedDocs[owners_data][' + owner_row + '][valuation_id]" value="<?= $valuation->id; ?>" id="input-valuation_id' + owner_row + '" />';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#owner_data_row' + owner_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        owner_row++;

        $('#owner_container tbody').append(html);
    }
</script>


<script type="text/javascript">
    var unit_row = <?= $unit_row ?>;

    function addUnit() {
        html = '<tr id="image-row' + unit_row + '">';


        html += '  <td class="text-left">';
        html += ' <div class="form-group">';
        html += ' <a href="javascript:;" id="upload-document' + unit_row + '" onclick="uploadAttachment(' + unit_row + ')" data-toggle="tooltip" class="img-thumbnail" title="Upload Document">';
        html += '<img src="<?= Yii::getAlias('@web') . '/images/upload-image.png' ?>" width="100" alt="" title="" data-placeholder="<?= Yii::getAlias('@web') . '/themes/wisdom/images/upload.png' ?>" />';
        html += '<i></i>';
        html += '</a>';
        html += ' <input type="hidden" name="Ticket[attachments][' + unit_row + '][attachment]" value="" id="input-attachment' + unit_row + '" />';
        html += ' <input type="hidden" name="Ticket[attachments][' + unit_row + '][history_id]" value="" id="input-history_id' + unit_row + '" />';
        html += ' <input type="hidden" name="Ticket[attachments][' + unit_row + '][user_id]" value="" id="input-user_id' + unit_row + '" />';
        html += '</div>';
        html += '  </td>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + unit_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger">-</button></td>';

        html += '</tr>';

        unit_row++;

        $('#requestTypes tbody').append(html);
    }

    var uploadAttachment = function (attachmentId) {


        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function (json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);


                            console.log(json.file.href);
                            var str1 = json['file'].href;
                            var str2 = ".pdf";
                            var str3 = ".doc";
                            var str4 = ".docx";
                            var str5 = ".xlsx";
                            var str6 = ".xls";
                            if (str1.indexOf(str2) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                            } else if (str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                            } else {
                                $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                            }
                            $('#input-attachment' + attachmentId).val(json['file'].href);


                            /*  $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                             $('#input-attachment' + attachmentId).val(json['file'].href);*/
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }

</script>



