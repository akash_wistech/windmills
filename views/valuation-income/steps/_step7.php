<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enter Cost Details');
$cardTitle = Yii::t('app', 'Enter Cost Details:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_7/'.$valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$this->registerJs('

$("#listingstransactions-original_purchase_date,#listingstransactions-current_transaction_price_date,#listingstransactions-land_lease_start_date,#listingstransactions-land_lease_expiry_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');
?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
</style>

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-edit"></i>
                <?= $valuation->reference_number ?>
            </h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-5 col-sm-3">
                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                        <?php  echo $this->render('../left-nav', ['model' => $valuation,'step' => 7]); ?>
                    </div>
                </div>
                <div class="col-7 col-sm-9">
                    <div class="tab-content" id="vert-tabs-tabContent">
                        <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                            <section class="valuation-form card card-outline card-primary">

                                <?php $form = ActiveForm::begin(); ?>
                                <header class="card-header">
                                    <h2 class="card-title"><?= $cardTitle ?></h2>
                                </header>
                                <div class="card-body">

                                    <section class="valuation-form card card-outline card-primary">


                                        <header class="card-header">
                                            <h2 class="card-title">Original Purchase Details</h2>
                                        </header>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <?php
                                                    echo $form->field($model, 'original_purchase_price_available')->widget(Select2::classname(), [
                                                        'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Original Purchase Price Available? See TD/SPA');
                                                    ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, 'original_purchase_price')->textInput(['maxlength' => true])
                                                        ->label('Original Purchase Price Value') ?>
                                                </div>

                                                <div class="col-sm-4" id="original_purchase_date_id">
                                                    <?= $form->field($model, 'original_purchase_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-original_purchase_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-original_purchase_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?php
                                                    echo $form->field($model, 'source_of_original_purchase_details')->widget(Select2::classname(), [
                                                        'data' => Yii::$app->appHelperFunctions->costSource,
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Source of Original Purchase Details');
                                                    ?>

                                                </div>

                                                <div class="col-sm-4">
                                                    <?php
                                                    echo $form->field($model, 'acquisition_method')->widget(Select2::classname(), [
                                                        'data' => Yii::$app->appHelperFunctions->acquisitionMethodsArr,
                                                        'options' => ['placeholder' => 'Select a Placement ...'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]);
                                                    ?>
                                                </div>
                                        </div>
                                    </section>

                                    <section class="valuation-form card card-outline card-primary">


                                        <header class="card-header">
                                            <h2 class="card-title">Transation Price Details</h2>
                                        </header>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <?php
                                                    echo $form->field($model, 'current_transaction_price_available')->widget(Select2::classname(), [
                                                        'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Current Transaction Price Available ? (See MOU/SPA)');
                                                    ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, 'transaction_price')->textInput(['maxlength' => true])
                                                        ->label('Transaction Price Value') ?>
                                                </div>


                                                <div class="col-sm-4" id="current_transaction_price_date_id">
                                                    <?= $form->field($model, 'current_transaction_price_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-current_transaction_price_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-current_transaction_price_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>

                                                </div>
                                                <div class="col-sm-4">
                                                    <?php
                                                    echo $form->field($model, 'source_of_transaction_details')->widget(Select2::classname(), [
                                                        'data' => Yii::$app->appHelperFunctions->costSource,
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Source of Transaction Details');
                                                    ?>

                                                </div>
                                            </div>
                                        </div>
                                    </section>

                                    <section class="valuation-form card card-outline card-primary">


                                        <header class="card-header">
                                            <h2 class="card-title">Land Lease Details</h2>
                                        </header>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <?php
                                                    echo $form->field($model, 'land_lease_cost_available')->widget(Select2::classname(), [
                                                        'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ])->label('Land Lease Cost available?');
                                                    ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, 'land_lease_costs')->textInput(['maxlength' => true])
                                                        ->label('Land Lease Cost') ?>
                                                </div>
                                                <div class="col-sm-4" id="land_lease_start_date_id">
                                                    <?= $form->field($model, 'land_lease_start_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-land_lease_start_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-land_lease_start_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>

                                                </div>
                                                <div class="col-sm-4" id="land_lease_expiry_date_id">
                                                    <?= $form->field($model, 'land_lease_expiry_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-land_lease_expiry_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-land_lease_expiry_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>

                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, 'total_number_of_lease_years')->textInput(['maxlength' => true])
                                                        ->label('Land Lease Cost') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </section>

                                    <section class="valuation-form card card-outline card-primary">


                                        <header class="card-header">
                                            <h2 class="card-title">Additional Costs Details</h2>
                                        </header>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <?php
                                                    if($model->lands_price <> null){

                                                    }else {
                                                        $model->lands_price = $valuation->building->communities->land_price;
                                                    }
                                                    ?>
                                                    <?= $form->field($model, 'lands_price')->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, 'parking_price')->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, 'pool_price')->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, 'landscape_price')->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, 'utilities_connected_price')->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, 'white_goods_price')->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, 'number_of_years')->textInput(['maxlength' => true]) ?>
                                                </div>

                                            </div>
                                        </div>
                                    </section>







                                </div>
                                <div class="card-footer">
                                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </section>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>



