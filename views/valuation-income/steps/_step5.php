<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use app\assets\ListingsFormAsset;
use app\components\widgets\StatusVerified;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Inspect Property');
$cardTitle = Yii::t('app', 'Inspect Property:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_5/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
if ($model->email_status != 1) {
    $AlertText = 'Data and Valuation Status will be saved and Email will be sent once?';
} else {
    $AlertText = 'Only Data will be saved?';
}
$AlertText = 'Only Data will be saved?';
$this->registerJs('

$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "' . Yii::t('app', $AlertText) . '",
html: "' . Yii::t('app', 'Are you sure you want to delete this? You will not be able to recover this!') . '",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "' . Yii::t('app', 'Yes, Do you want to save it!') . '",
cancelButtonText: "' . Yii::t('app', 'Cancel') . '",
},function(result) {
    if (result) {
      console.log("Hello 123")
      //   $("#w0").unbind("submit").submit();
      $("#w0").submit();
    }
});
});

$("body").on("click", ".sav-btn", function (e) {

swal({
title: "' . Yii::t('app', 'Email will be sent to Clients!') . '",
html: "' . Yii::t('app', '') . '",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "' . Yii::t('app', 'Email will be sent to Clients!') . '",
cancelButtonText: "' . Yii::t('app', 'Cancel') . '",
},function(result) {
    if (result) {
   
  $.ajax({

  url: "' . Url::to(['valuation/inspectpropert', 'id' => $model->id]) . '",
  dataType: "html",
  type: "POST",
  });
    }
});
});

var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});
  $("#inspectproperty-listing_property_type_options").on(\'change\',function(){
        $("#inspectproperty-listing_property_type").val($(this).val());
        });
        
        $("body").on("change", "#inspectproperty-permitted_bua_check", function () {
           console.log($(this).val());
           if($(this).val() == "yes"){
            $("#iuser_fixed_fee").show();
            $("#iuser_fixed_fee_gfa").show();
                   }else{
                    $("#inspectproperty-permitted_bua").val("");
                    $("#iuser_fixed_fee").hide();
                    $("#iuser_fixed_fee_gfa").hide();
                   }
       
    });
    
       $("#completion_certificate_date,#date_of_building_permit").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "YYYY-MM-DD"
    });

');

$i = 1;
?>
<style>
    .help-block2 {
        color: red;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -14px;
        font-family: inherit;
    }
</style>
<script>

</script>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 5]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <?php $form = ActiveForm::begin(); ?>

                        <section class="card card-outline card-info">
                            <header class="card-header">
                                <h2 class="card-title"><?= Yii::t('app', 'Address Details Verify') ?></h2>
                            </header>
                            <div class="card-body">
                                <div class="row">


                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'plot_number_verify')->textInput(['maxlength' => true])->label('Plot Number <span class="text-danger">*</span>') ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'street_verify')->textInput(['maxlength' => true])->label('Street <span class="text-danger">*</span>') ?>
                                    </div>
                                    <div class="col-sm-4" style="">
                                        <?= $form->field($model, 'unit_number_verify')->textInput(['maxlength' => true])->label('Unit Number <span class="text-danger">*</span>') ?>
                                    </div>


                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'city_verify')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->emiratedListArr,
                                            'options' => ['placeholder' => 'Select an City ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('City <span class="text-danger">*</span>');
                                        ?>

                                    </div>

                                    <div class="col-sm-4">
                                        <?php

                                        echo $form->field($model, 'community_verify')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(\app\models\Communities::find()->where(['status'=>1])->andWhere(['trashed'=>0])->orderBy([
                                                'title' => SORT_ASC,
                                            ])->all(), 'id', 'title'),
                                            'options' => ['placeholder' => 'Select a Community ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Community <span class="text-danger">*</span>');
                                        ?>

                                    </div>


                                    <div class="col-sm-4">
                                        <?= Html::hiddenInput('sub_community_value', $model->sub_community_verify, ['id' => 'sub_community_value']); ?>
                                        <?php

                                        echo $form->field($model, 'sub_community_verify')->widget(DepDrop::classname(), [
                                            'data' => ($model->sub_community_verify ? [$model->sub_community_verify => $model->subCommunities->title] : []),
                                            'options' => ['placeholder' => 'Select Sub Community'],
                                            'type' => DepDrop::TYPE_SELECT2,
                                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                            'pluginOptions' => [
                                                'initialize' => ($model->sub_community_verify ? true : false),
                                                'depends' => ['inspectproperty-community_verify'],
                                                'params' => ['sub_community_value'],
                                                'url' => Url::to(['/sub-communities/search-sub-commuinites']),
                                                'loadingText' => 'Loading Sub Communities ...',
                                            ]
                                        ])->label('Sub Community <span class="text-danger">*</span>');;
                                        ?>

                                    </div>


                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'country_verify')->dropDownList(Yii::$app->appHelperFunctions->countryListArr,['prompt'=>Yii::t('app','Select')])->label('Country <span class="text-danger">*</span>')?>
                                    </div>

                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'location_pin_verify')->textInput(['maxlength' => true])->label('Location Pin <span class="text-danger">*</span>') ?>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="valuation-form card card-outline card-primary">


                            <header class="card-header">
                                <h2 class="card-title">Add Property Address Details</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">









                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'makani_number')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'municipality_number')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'location_pin')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'listing_property_type')->widget(Select2::classname(), [
                                            // 'data' => Yii::$app->appHelperFunctions->listingsPropertyTypeListArr,
                                            'data' => ArrayHelper::map(\app\models\ListingSubTypes::find()->orderBy([
                                                'title' => SORT_ASC,
                                            ])->all(), 'title', 'title'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>
                                </div>
                            </div>



                        <section class="valuation-form card card-outline card-primary">


                            <header class="card-header">
                                <h2 class="card-title">Size Details (square feet)</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true])->label('Final Gross BUA/GFA Size') ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'built_up_area_m')->textInput(['maxlength' => true])->label('Measured Gross BUA/GFA Size') ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'balcony_size')->textInput(['maxlength' => true])->label('Measured Balcony Size') ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'service_area_size')->textInput(['maxlength' => true])->label('Measured Service Area BUA Size') ?>
                                    </div>

                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'net_built_up_area')->textInput(['maxlength' => true])->label('Measured Net BUA/GFA Size ') ?>
                                    </div>

                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'permitted_bua_check')->widget(Select2::classname(), [
                                            'data' => array('no' => 'Not Available', 'yes' => 'Available'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Permitted Built-Up Area For Land');
                                        ?>
                                    </div>
                                    <?php
                                    if($model->permitted_bua_check == 'yes') {
                                        $style = "block";
                                    }else{
                                        $style = "none";
                                    }

                                    ?>
                                    <div class="col-sm-4" id="iuser_fixed_fee" style="display: <?= $style ?>">
                                        <?= $form->field($model, 'permitted_bua')->textInput(['maxlength' => true])->label('Permitted BUA for Land'); ?>
                                    </div>
                                    <div class="col-sm-4" id="iuser_fixed_fee_gfa" style="display: <?= $style ?>">
                                        <?= $form->field($model, 'permitted_gfa')->textInput(['maxlength' => true])->label('Permitted GFA for Land'); ?>
                                    </div>

                                    <?php   if(isset($valuation->building->city) && ($valuation->building->city == 3507)){ ?>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'length_of_fence')->textInput(['maxlength' => true])->label('Length Of Fence') ?>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </section>

                        <section class="valuation-form card card-outline card-primary">


                            <header class="card-header">
                                <h2 class="card-title">Locations Details</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'property_placement')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertyPlacementListArr,
                                            'options' => ['placeholder' => 'Select a Placement ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'property_exposure')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertyExposureListArr,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'property_visibility')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertyVisibilityListArr,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'location_highway_drive')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'location_school_drive')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'location_mall_drive')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'location_sea_drive')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'location_park_drive')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                        ]);
                                        ?>
                                    </div>

                                </div>
                            </div>
                        </section>

                        <section class="valuation-form card card-outline card-primary">


                            <header class="card-header">
                                <h2 class="card-title">Condition/Status  Details</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'finished_status')->widget(Select2::classname(), [
                                            'data' => array('Shell & Core' => 'Shell & Core', 'Fitted' => 'Fitted'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'estimated_age')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        $months= array();
                                        for($i=0;$i<=12;$i++){
                                            $months[$i]=$i;
                                        }
                                        echo $form->field($model, 'estimated_age_month')->widget(Select2::classname(), [
                                            'data' => $months,
                                            'pluginOptions' => [
                                              //  'allowClear' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'age_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrAgeSource,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Age Source');
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'estimated_remaining_life')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'property_condition')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertyConditionListArr,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'property_defect')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertyDefectsArr,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'occupancy_status')->widget(Select2::classname(), [
                                            'data' => array('Owner Occupied' => 'Owner Occupied', 'Tenanted' => 'Tenanted', 'Vacant' => 'Vacant'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'acquisition_method')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->acquisitionMethodsArr,
                                            'options' => ['placeholder' => 'Select a Placement ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'completion_status')->textInput(['maxlength' => true])->label('Completion Status (%)') ?>
                                    </div>



<?php   if(isset($valuation->building->city) && ($valuation->building->city == 3507)){ ?>

                                    <div class="col-sm-4" id="inspection_date_id">

                                        <label for="html" id='inpect_date_label'>Date of Completion Certificate</label><br>
                                        <?= $form->field($model, 'date_of_completion_certificate', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="completion_certificate_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#completion_certificate_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])->label(false) ?>
                                    </div>

                                    <div class="col-sm-4" id="inspection_date_id">

                                        <label for="html" id='inpect_date_label'>Date of Building Permit</label><br>
                                        <?= $form->field($model, 'date_of_building_permit', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="date_of_building_permit" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#date_of_building_permit" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])->label(false) ?>
                                    </div>

                                    <?php } ?>



                                </div>
                            </div>
                        </section>

                        <section class="valuation-form card card-outline card-primary">


                            <header class="card-header">
                                <h2 class="card-title">Development Details</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'development_type')->widget(Select2::classname(), [
                                            'data' => array('Standard' => 'Standard', 'Non-Standard' => 'Non-Standard'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'developer_id')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(\app\models\Developers::find()->orderBy([
                                                'title' => SORT_ASC,
                                            ])->all(), 'id', 'title'),
                                            'options' => ['placeholder' => 'Select Master Developer ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Master Developer');
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'developer_id_property')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(\app\models\Developers::find()->orderBy([
                                                'title' => SORT_ASC,
                                            ])->all(), 'id', 'title'),
                                            'options' => ['placeholder' => 'Select Property Developer ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Property Developer');;
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'full_building_floors')->textInput(['maxlength' => true])->label('Number Of Floors'); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'number_of_basement')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'number_of_mezannines')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'parking_floors')->textInput(['maxlength' => true])->label("Parking Space") ?>
                                    </div>
                                    <div class="col-sm-4">

                                        <?php

                                        $model->ac_type = explode(',', ($model->ac_type <> null) ? $model->ac_type : "");
                                        echo $form->field($model, 'ac_type')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->acTypesArr,
                                            'options' => ['placeholder' => 'Select a AC Type ...'],
                                            'pluginOptions' => [
                                                'placeholder' => 'Select a AC Type',
                                                'multiple' => true,
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'utilities_connected')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>


                                   <!-- <div class="col-sm-4">
                                        <?/*= $form->field($model, 'parking_floors')->textInput(['maxlength' => true])->label("Parking Space") */?>
                                    </div>-->
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'number_of_levels')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->listingLevelsListArr,
                                            'options' => ['placeholder' => 'Select a Level ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'pool')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes', 'No' => 'No'),

                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'landscaping')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes', 'No' => 'No', 'Semi-Landscape' => 'Semi-Landscape'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>

                                </div>
                            </div>
                        </section>

                        <section class="valuation-form card card-outline card-primary">


                            <header class="card-header">
                                <h2 class="card-title">Kitchen Equipment Details</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'fridge')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'oven')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'dish_washer')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'cooker')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'washing_machine')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'furnished')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes – we are valuing the unit as unfurnished','standard'=> 'Yes – we are valuing the unit as standard furnished', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'white_goods')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>

                                </div>
                            </div>
                        </section>

                        <section class="valuation-form card card-outline card-primary">


                            <header class="card-header">
                                <h2 class="card-title">Unit/Building Internal Facilites Details</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_bedrooms')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_bathrooms')->textInput(['maxlength' => true]) ?>
                                    </div>


                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_kitchen')->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_living_area')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_dining_area')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_maid_rooms')->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_laundry_area')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_store')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_service_block')->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_garage')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_balcony')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_family_room')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_powder_room')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_study_room')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'no_of_gym_room')->textInput(['maxlength' => true]) ?>
                                    </div>

                                </div>
                            </div>
                        </section>

                        <section class="valuation-form card card-outline card-primary">


                            <header class="card-header">
                                <h2 class="card-title">Unit/Building External Facilities Details</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php
                                        /*  echo "<pre>";
                                          print_r($model->other_facilities);
                                          die;
                                          echo $model->other_facilities;
                                          die;
                                          if($model->other_facilities <> null) {
                                              $model->other_facilities = explode(',', ($model->other_facilities <> null) ? $model->other_facilities : "");
                                          }*/
                                        $model->other_facilities = explode(',', ($model->other_facilities <> null) ? $model->other_facilities : "");
                                        echo $form->field($model, 'other_facilities')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(\app\models\OtherFacilities::find()->orderBy([
                                                'title' => SORT_ASC,
                                            ])->all(), 'id', 'title'),
                                            'options' => ['placeholder' => 'Select a Facilities ...'],
                                            'pluginOptions' => [
                                                'placeholder' => 'Select a Facilities',
                                                'multiple' => true,
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>


                                </div>
                            </div>
                        </section>

                        <section class="valuation-form card card-outline card-primary">


                            <header class="card-header">
                                <h2 class="card-title">Extension Details</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'balcony_covered_with_roof')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'extension_permision_document')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'extension_completion_document')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>


                                </div>
                            </div>
                        </section>



                        <section class="card card-outline card-info">
                            <header class="card-header">
                                <h2 class="card-title"><?= Yii::t('app', 'View Attributes') ?></h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'view_community')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->viewCommunityAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'view_pool')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'view_burj')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                        ]);
                                        ?>
                                    </div>

                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'view_sea')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'view_marina')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'view_mountains')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'view_lake')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'view_golf_course')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'view_park')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'view_special')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                        ]);
                                        ?>
                                    </div>
                                </div>

                            </div>

                        </section>


                        <section class="valuation-form card card-outline card-primary">

                            <header class="card-header">
                                <h2 class="card-title">Custom Configration Information</h2>
                            </header>
                            <div class="card-body">

                                <table id="attachment" class="table table-striped table-bordered table-hover images-table">
                                    <thead>
                                    <tr>
                                        <td class="text-left">Name</td>
                                        <td class="text-left">Quantity</td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $row = 0; ?>
                                    <?php foreach ($model->customAttachements as $attachment) { ?>
                                        <tr id="image-row-attachment-<?php echo $row; ?>">

                                            <td>
                                                <input type="text" class="form-control"
                                                       name="InspectProperty[customAttachments][<?= $row ?>][name]"
                                                       value="<?= $attachment->name ?>" placeholder="Name" required />
                                            </td>

                                            <td>
                                                <input type="number" class="form-control"
                                                       name="InspectProperty[customAttachments][<?= $row ?>][quantity]"
                                                       value="<?= $attachment->quantity ?>" placeholder="Quantity" required />
                                            </td>
                                            <input type="hidden" class="form-control"
                                                   name="InspectProperty[customAttachments][<?= $row ?>][id]"
                                                   value="<?= $attachment->id ?>" placeholder="Name" required />

                                            <td class="text-left">
                                                <button type="button"
                                                        onclick="deleteRow('-attachment-<?= $row ?>', '<?= $attachment->id; ?>', 'attachment')"
                                                        data-toggle="tooltip" title="You want to delete Attachment"
                                                        class="btn btn-danger"><i
                                                            class="fa fa-minus-circle"></i></button>
                                            </td>
                                        </tr>
                                        <?php $row++; ?>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="text-left">
                                            <button type="button" onclick="addAttachment();" data-toggle="tooltip" title="Add"
                                                    class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </section>
                        </section>

                    </div>


                        <?php
                        $allow_array = array(1, 21, 38);
                        if (isset($model->created_by) && ($model->created_by <> null)) {
                            $check_current_id = $model->created_by;
                        } else {
                            $check_current_id = Yii::$app->user->identity->id;
                        }
                        if (($key = array_search($check_current_id, $allow_array)) !== false) {
                            unset($allow_array[$key]);
                        }
                        if (in_array(Yii::$app->user->identity->id, $allow_array) || Yii::$app->user->identity->id == 1) {
                            echo StatusVerified::widget(['model' => $model, 'form' => $form]);
                        }
                        ?>

                    </div>
                    <div class="card-footer">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                        <?php if ($model->email_status <> 1) { ?>
                            <button type="button" name="button" class="sav-btn btn btn-primary">Send Email</button>
                        <?php } ?>

                        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                        <?php
                        if ($model <> null && $model->id <> null) {
                            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                                'model_id' => $model->id,
                                'model_name' => 'app\models\InspectProperty',
                            ]);
                        }
                        ?>
                    </div>
                    <!-- <div class="card-footer">
                                <? /*= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) */ ?>
                                <? /*= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) */ ?>
                            </div>-->
                    <?php ActiveForm::end(); ?>

                </div>

            </div>
        </div>
    </div>
</div>
<!-- /.card -->
</div>




<script type="text/javascript">
    var row = <?= $row ?>;
    function addAttachment() {

        html = '<tr id="image-row' + row + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="InspectProperty[customAttachments][' + row + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="number" class="form-control"  name="InspectProperty[customAttachments][' + row + '][quantity]" value="" placeholder="Quantity" required />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment tbody').append(html);

        row++;
    }

    function deleteRow(rowId, docID, type) {


        var url = '<?= \yii\helpers\Url::to('/valuation/remove-attachment'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if (response.status == 'exist') {
                            swal("Warning!", response.message, "warning");
                        } else {
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
/*    $('#inspectproperty-community_verify')
        .trigger('change');*/
</script>