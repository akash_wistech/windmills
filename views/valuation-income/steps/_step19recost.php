<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Derive Reinstatement Cost Approach');
$cardTitle = Yii::t('app', 'Derive Reinstatement Approach:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_19rec/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-4 col-sm-2">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 19]); ?>
                </div>
            </div>
            <div class="col-8 col-sm-10">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">

                        <section class="valuation-form card card-outline card-primary">
                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title">Reinstatement Approach - Final</h2>
                            </header>
                            <div class="card-body">
                                <div class="row" style="padding: 10px">

                                    <table id="owner_container"
                                           class="table table-striped table-bordered table-hover text-center images-table ">
                                        <thead>
                                        <tr>
                                            <td style="padding: 10px !important;" class=" width_5">
                                                <strong>Cost Subjects</strong>
                                            </td>

                                            <td style="padding: 10px !important;" class=" width_25">
                                                <strong> Base Factor</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class=" width_15">
                                                <strong>Fee/ Rate</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_10">
                                                <strong>Reinstatment Cost</strong>
                                            </td>

                                            <td style="padding: 10px !important;" class="width_25">
                                                <strong>Adjustments</strong>
                                            </td>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        <tr>
                                            <td colspan="5" style="text-align: left">
                                                <strong> Demolition Costs </strong>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Demolition Cost
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['demolition_costs']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['demolition_costs']['fee_rate']; ?> (cost per sm)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['demolition_costs']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'demolition_costs_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Debris Removal
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['debris_removal']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['debris_removal']['fee_rate']; ?> (cost per sm)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['debris_removal']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'debris_removal_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Total Demolition Cost
                                            </td>


                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['total_demolition_costs_values']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['total_demolition_costs_values_saved']; ?>
                                            </td>

                                        </tr>


                                        <tr>
                                            <td colspan="5" style="text-align: left">
                                                <strong> Contracting Costs </strong>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Construction Cost
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['contracting_cost']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['contracting_cost']['fee_rate']; ?> (cost per sm)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['contracting_cost']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'contracting_cost_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Basement Parking
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['basement_parking']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['basement_parking']['fee_rate']; ?> (cost per sm)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['basement_parking']['calculations']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'basement_parking_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Multi Story Parking
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['multi_story_parking']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['multi_story_parking']['fee_rate']; ?> (cost per sm)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['multi_story_parking']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'multi_story_parking_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Boundry Wall Length
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['boundry_wall']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['boundry_wall']['fee_rate']; ?> (cost per lm)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['boundry_wall']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'boundry_wall_length_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Total Contracting Cost
                                            </td>


                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['total_contracting_cost_values']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['total_contracting_cost_values_saved']; ?>
                                            </td>

                                        </tr>




                                        <tr>
                                            <td colspan="5" style="text-align: left">
                                                <strong> Landscapiing/Facilities Costs  </strong>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Site Improvements
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['site_improvements']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['site_improvements']['fee_rate']; ?> (cost per sm)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['site_improvements']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'site_improvements_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Pool
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['pool_costs']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['pool_costs']['fee_rate']; ?> (cost per cm)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['pool_costs']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'pool_price_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Facilities
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['facilities_costs']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['facilities_costs']['fee_rate']; ?> (cost per sm)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['facilities_costs']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'whitegoods_price_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Landscaping
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['landscaping_costs']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['landscaping_costs']['fee_rate']; ?> (cost per sm)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['landscaping_costs']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'landscape_price_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Total Landscapiing/Facilities Cost
                                            </td>


                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['total_landscapiing_facilities_values']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['total_landscapiing_facilities_values_saved']; ?>
                                            </td>

                                        </tr>






                                        <tr>
                                            <td colspan="5" style="text-align: left">
                                                <strong>  Government Fees</strong>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Land Department/Municipality Fee
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['land_department_municipality_cost']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['land_department_municipality_cost']['fee_rate']; ?> (cost per SF)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['land_department_municipality_cost']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'land_department_municipality_fee_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Master Developer Charges
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['master_developer_charges_costs']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['master_developer_charges_costs']['fee_rate']; ?> (%)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['master_developer_charges_costs']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'developer_margin_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Green Building Certification Fee
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['green_building_certification_fee']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['green_building_certification_fee']['fee_rate']; ?> (%)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['green_building_certification_fee']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'green_building_certification_fee_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Demolition Fee
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['demolition_fee_costs']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['demolition_fee_costs']['fee_rate']; ?> (per unit)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['demolition_fee_costs']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'demolition_fee_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Total Government Fees Cost
                                            </td>


                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['total_government_fees_values']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['total_government_fees_values_saved']; ?>
                                            </td>

                                        </tr>



                                        <tr>
                                            <td colspan="5" style="text-align: left">
                                                <strong>   Professional fee</strong>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Consulting/Engineering/Arhitecture Costs
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['consulting_engineering_arhitecture_cost']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['consulting_engineering_arhitecture_cost']['fee_rate']; ?> (%)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['consulting_engineering_arhitecture_cost']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'professional_charges_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Designing and Architecture Costs
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['designing_and_architecture_costs']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['designing_and_architecture_costs']['fee_rate']; ?> (%)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['designing_and_architecture_costs']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'designing_and_architecture_costs_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Project Management Costs
                                            </td>



                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['project_management_costs']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['project_management_costs']['fee_rate']; ?> (%)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['project_management_costs']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'project_management_costs_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Total finance charges
                                            </td>


                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['total_professional_fee_values']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['total_professional_fee_values_saved']; ?>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="5" style="text-align: left">
                                                <strong>  Finance charges</strong>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Loan Amount (<?= $cost_approach['loan_amount'] ?>)
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['finance_cost']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['finance_cost']['fee_rate']; ?> (%)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['finance_cost']['calculations']; ?>
                                            </td>
                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'interest_rate_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td colspan="5" style="text-align: left">
                                                <strong>  Contingencies</strong>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Contingencies
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['contingencies_cost']['factor']; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['contingencies_cost']['fee_rate']; ?> (%)
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['contingencies_cost']['calculations']; ?>
                                            </td>

                                             <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $form->field($cost, 'contingency_margin_values')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>

                                        </tr>




                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                              Grand Total Cost
                                            </td>


                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">

                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['total_cost_values']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $cost_approach['grand_total']; ?>
                                            </td>

                                        </tr>


                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>



