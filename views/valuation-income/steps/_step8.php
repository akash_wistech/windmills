<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DeveloperMarginFormAsset;

DeveloperMarginFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('

$("#initial_deposit_contract_date,#last_payment_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');


?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }

    .kv-file-content, .upload-docs img {
        width: 50px !important;
        height: 50px !important;
    }

    .width_30 {
        width: 30%;
    }

    .width_25 {
        width: 25%;
    }

    .width_20 {
        width: 20%;
    }

    .width_5 {
        width: 5%;
    }

    .width_10 {
        width: 10%;
    }

    .width_15 {
        width: 15%;
    }

    .padding10 {
        padding: 10px !important;
    }

    .pb10 {
        padding-bottom: 10px;
    }
</style>
<?php
$this->title = Yii::t('app', 'Derive Developer Margin');
$cardTitle = Yii::t('app', 'Derive Developer Margin:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_8/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 8]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'funding_cost')->textInput(['maxlength' => true])->label('Interest Rate') ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'admin_charges')->textInput(['maxlength' => true]) ?>
                                    </div>
                                   <!-- <div class="col-sm-4">
                                        <?/*= $form->field($model, 'total_rate')->textInput(['maxlength' => true, 'readonly' => true,'value']) */?>
                                    </div>-->


                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'initial_deposit_contract_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="initial_deposit_contract_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#initial_deposit_contract_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'last_payment_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="last_payment_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#last_payment_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                    </div>

                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'initial_deposit')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'last_payment_amount')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'frequency_of_payments')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'decline_in_prices')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-12 text-right pb10">
                                        <?= Html::submitButton(Yii::t('app', 'Payments Process Table'), ['class' => 'btn btn-info', 'id' => 'payments_process_table']) ?>
                                    </div>

                                    <?php if ($show_payment_table == 1) { ?>
                                        <section class="valuation-form card card-outline card-primary">

                                            <header class="card-header">
                                                <h2 class="card-title">Payments Information</h2>
                                            </header>
                                            <div class="card-body">
                                                <div class="row" style="padding: 10px">

                                                    <table id="owner_container"
                                                           class="table table-striped table-bordered table-hover text-center images-table ">
                                                        <thead>
                                                        <tr>
                                                            <td style="padding: 10px !important;" class=" width_5">
                                                                <strong>No#</strong>
                                                            </td>

                                                            <td style="padding: 10px !important;" class=" width_25">
                                                                <strong>&nbsp;Installement Date</strong>
                                                            </td>
                                                            <td style="padding: 10px !important;" class=" width_15">
                                                                <strong>No of days</strong>
                                                            </td>
                                                            <td style="padding: 10px !important;" class="width_10">
                                                                <strong>Percentage</strong>
                                                            </td>
                                                            <td style="padding: 10px !important;" class="width_25">
                                                                <strong>Amount</strong>
                                                            </td>
                                                            <td style="padding: 10px !important;" class="width_25">
                                                                <strong>Present Value</strong>
                                                            </td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $total_days = 0;
                                                        $total_percentage = 0;
                                                        $total_amount = 0;
                                                        $total_present_value = 0;
                                                        $d_m_percentage = 0;
                                                        if (!empty($payments_table) && $payments_table <> null) {

                                                            foreach ($payments_table as $key => $item) {
                                                                ?>

                                                                <tr id="owner_data_row<?php echo($key); ?>">
                                                                    <td style="padding-top: 30px!important;">
                                                                        <?= ($key + 1) ?>
                                                                    </td>


                                                                    <td class="text-center"
                                                                        style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                                        <?php if ($key == 0) { ?>
                                                                            <input type="text"
                                                                                   name="ValuationDeveloperDriveMargin[payment_info][<?= $key; ?>][installment_date]"
                                                                                   value="<?= $model->initial_deposit_contract_date ?>"
                                                                                   readonly
                                                                                   placeholder="Days"
                                                                                   class="form-control"
                                                                            />

                                                                        <?php } else if (($key + 1) == $model->frequency_of_payments) { ?>
                                                                            <input type="text"
                                                                                   name="ValuationDeveloperDriveMargin[payment_info][<?= $key; ?>][installment_date]"
                                                                                   value="<?= $model->last_payment_date ?>"
                                                                                   readonly
                                                                                   placeholder="Days"
                                                                                   class="form-control"
                                                                            />


                                                                        <?php } else { ?>
                                                                            <div class="form-group">
                                                                                <div class="input-group date"
                                                                                     style="display: flex"
                                                                                     id="payment_date_calendar<?= $key; ?>"
                                                                                     data-target-input="nearest">
                                                                                    <input type="text"
                                                                                           id="payment_date<?= $key; ?>"
                                                                                           class="form-control"
                                                                                           name="ValuationDeveloperDriveMargin[payment_info][<?= $key; ?>][installment_date]"
                                                                                           value="<?= ($item->installment_date) ? $item->installment_date : '' ?>"
                                                                                           aria-required="true"
                                                                                           aria-invalid="false"
                                                                                           required>
                                                                                    <div class="input-group-append"
                                                                                         data-target="#payment_date_calendar<?= $key; ?>"
                                                                                         data-toggle="datetimepicker">
                                                                                        <div class="input-group-text"><i
                                                                                                    class="fa fa-calendar"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        <?php } ?>
                                                                    </td>
                                                                    <td class="text-center"
                                                                        style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                                        <div class="form-group">
                                                                            <input type="text"
                                                                                   name="ValuationDeveloperDriveMargin[payment_info][<?= $key; ?>][no_of_days]"
                                                                                   value="<?= ($item->no_of_days >= 0) ? $item->no_of_days : '' ?>"
                                                                                   readonly
                                                                                   placeholder="Days"
                                                                                   class="form-control"
                                                                            />
                                                                        </div>
                                                                    </td>

                                                                    <td class="text-center"
                                                                        style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                                        <div class="form-group">
                                                                            <input type="text"
                                                                                   name="ValuationDeveloperDriveMargin[payment_info][<?= $key; ?>][percentage]"
                                                                                   value="<?= ($item->percentage <> null) ? $item->percentage : '' ?>"
                                                                                   placeholder="%" class="form-control"
                                                                                   required
                                                                            />
                                                                        </div>
                                                                    </td>
                                                                    <td class="text-center"
                                                                        style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                                        <div class="form-group">
                                                                            <input type="text"
                                                                                   name="ValuationDeveloperDriveMargin[payment_info][<?= $key; ?>][amount]"
                                                                                   value="<?= ($item->amount <> null) ? $item->amount : '' ?>"
                                                                                   readonly
                                                                                   placeholder="Amount"
                                                                                   class="form-control"
                                                                            />
                                                                        </div>
                                                                    </td>
                                                                    <td class="text-center"
                                                                        style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                                        <div class="form-group">
                                                                            <input type="text"
                                                                                   name="ValuationDeveloperDriveMargin[payment_info][<?= $key; ?>][present_value]"
                                                                                   value="<?= ($item->present_value <> null) ? $item->present_value : '' ?>"
                                                                                   readonly
                                                                                   placeholder="P.V"
                                                                                   class="form-control"
                                                                            />
                                                                        </div>
                                                                    </td>
                                                                </tr>


                                                                <?php


                                                                $total_days = $total_days + $item->no_of_days;
                                                                $total_percentage = $total_percentage + $item->percentage;
                                                                $total_amount = $total_amount + $item->amount;
                                                                $total_present_value = $total_present_value + $item->present_value;

                                                                echo $this->registerJs('

$("#payment_date_calendar' . $key . '").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');
                                                            }
                                                            if ($total_amount > 0) {

                                                                $d_m_percentage = round((($model->developer_margin / $total_amount) * 100), 2);


                                                                ?>
                                                                <tr id="owner_data_row">


                                                                    <td class="text-center" colspan="2"
                                                                        style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                                        <h3>Total Values: </h3>
                                                                    </td>
                                                                    <td class="text-center"
                                                                        style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                                        <div class="form-group">
                                                                            <input type="text"
                                                                                   value="<?= ($total_days <> null) ? $total_days : '' ?>"
                                                                                   readonly
                                                                                   placeholder="Days"
                                                                                   class="form-control"
                                                                            />
                                                                        </div>
                                                                    </td>

                                                                    <td class="text-center"
                                                                        style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                                        <div class="form-group">
                                                                            <input type="text"
                                                                                   value="<?= ($total_percentage <> null) ? $total_percentage : '' ?>"
                                                                                   placeholder="%" class="form-control"
                                                                                   readonly
                                                                            />
                                                                        </div>
                                                                    </td>
                                                                    <td class="text-center"
                                                                        style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                                        <div class="form-group">
                                                                            <input type="text"
                                                                                   value="<?= ($total_amount <> null) ? $total_amount : '' ?>"
                                                                                   readonly
                                                                                   placeholder="Amount"
                                                                                   class="form-control"
                                                                            />
                                                                        </div>
                                                                    </td>
                                                                    <td class="text-center"
                                                                        style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                                        <div class="form-group">
                                                                            <input type="text"
                                                                                   value="<?= ($total_present_value <> null) ? $total_present_value : '' ?>"
                                                                                   readonly
                                                                                   placeholder="P.V"
                                                                                   class="form-control"
                                                                            />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php


                                                            }


                                                        }
                                                        ?>
                                                        </tbody>
                                                        <tfoot>
                                                        </tfoot>
                                                    </table>
                                                    <input type="hidden"
                                                           name="ValuationDeveloperDriveMargin[calculation_check]"
                                                           value="1">

                                                    <div class="col-sm-12 text-right pb10">
                                                        <?= Html::submitButton(Yii::t('app', 'Calculate Payments'), ['class' => 'btn btn-info', 'id' => 'payments_process']) ?>
                                                    </div>

                                                    <?php if ($total_amount > 0) { ?>


                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Developer
                                                                    Margin:</label>
                                                                <input type="text" class="form-control"
                                                                       value="<?= ($model->developer_margin <> null) ? $model->developer_margin : '' ?>"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Developer Margin
                                                                    %:</label>
                                                                <input type="text" class="form-control"
                                                                       value="<?= ($d_m_percentage <> null) ? $d_m_percentage : '' ?>"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Cash Equivalent
                                                                    Price:</label>
                                                                <input type="text" class="form-control"
                                                                       value="<?= ($model->cash_equivalent_price <> null) ? $model->cash_equivalent_price : '' ?>"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Estimated Market Value as per Elevation Analysis:</label>
                                                                <input type="text" class="form-control"
                                                                       value="<?= ($model->estimated_price_per_valuation <> null) ? $model->estimated_price_per_valuation : '' ?>"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                    <?php } ?>


                                                </div>
                                            </div>
                                        </section>
                                    <?php } ?>


                                </div>

                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>



