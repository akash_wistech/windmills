<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
ListingsFormAsset::register($this);


$this->registerJs('

$("#date_from,#date_to").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enquiry Sold-Transactions');
$cardTitle = Yii::t('app', 'Enquiry Sold-Transactions:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_10/'.$valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }

</style>

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-edit"></i>
                <?= $valuation->reference_number ?>
            </h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-5 col-sm-3">
                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                        <?php  echo $this->render('../left-nav', ['model' => $valuation,'step' => 10]); ?>
                    </div>
                </div>
                <div class="col-7 col-sm-9">
                    <div class="tab-content" id="vert-tabs-tabContent">
                        <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                            <section class="valuation-form card card-outline card-primary">

                                <?php $form = ActiveForm::begin(); ?>
                                <header class="card-header">
                                    <h2 class="card-title"><?= $cardTitle ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <?= $form->field($model, 'date_from', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="date_from" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#date_from" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($model, 'date_to', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="date_to" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#date_to" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                        </div>



                                        <div class="col-sm-12">
                                            <?php
                                            if ($valuation->parent_id <> null) {

                                                $model->building_info = explode(',', $model->building_info);
                                                echo $form->field($model, 'building_info')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(\app\models\Buildings::find()->where(['status' => [1,2]])->orderBy([
                                                        'title' => SORT_ASC,
                                                    ])->all(), 'id', 'title'),
                                                    'options' => ['placeholder' => 'Select Building ...'],
                                                    'pluginOptions' => [
                                                        'placeholder' => 'Select Building',
                                                        'multiple' => true,
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                            }else{
                                                $model->building_info = explode(',', $model->building_info);
                                                echo $form->field($model, 'building_info')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                                                        'title' => SORT_ASC,
                                                    ])->all(), 'id', 'title'),
                                                    'options' => ['placeholder' => 'Select Building ...'],
                                                    'pluginOptions' => [
                                                        'placeholder' => 'Select Building',
                                                        'multiple' => true,
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                            }
                                            ?>
                                        </div>

                                        <div class="col-sm-6">
                                            <?php
                                            echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                                                    'title' => SORT_ASC,
                                                ])->all(), 'id', 'title'),
                                                'options' => ['placeholder' => 'Select a Property Type ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>

                                        </div>
                                        <div class="col-sm-6">
                                            <?php
                                            echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                                                'options' => ['placeholder' => 'Select a Property Cat ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>

                                        </div>

                                      <!--  <div class="col-sm-6">
                                            <?/*= $form->field($model, 'bedroom_from')->textInput(['maxlength' => true]) */?>
                                        </div>

                                        <div class="col-sm-6">
                                            <?/*= $form->field($model, 'bedroom_to')->textInput(['maxlength' => true]) */?>
                                        </div>-->
                                        <div class="col-sm-6">
                                            <?= $form->field($model, 'landsize_from')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($model, 'landsize_to')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($model, 'bua_from')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($model, 'bua_to')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($model, 'price_from')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($model, 'price_to')->textInput(['maxlength' => true]) ?>
                                        </div>
                                    
                                        <div class="col-sm-6">
                                            <?= $form->field($model, 'price_psf_from')->textInput(['maxlength' => true]) ?>
                                        </div>
                                  
                                        <div class="col-sm-6">
                                            <?= $form->field($model, 'price_psf_to')->textInput(['maxlength' => true]) ?>
                                        </div>
                                      




                                    </div>


                                </div>
                                <div class="card-footer">
                                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </section>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>



