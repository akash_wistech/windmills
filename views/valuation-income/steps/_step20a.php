<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Derive MV Income Approach');
$cardTitle = Yii::t('app', 'Derive MV Income Approach:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_20/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-4 col-sm-2">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 20]); ?>
                </div>
            </div>
            <div class="col-8 col-sm-10">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <?php

                        if($model->valuation_approach == 1) {


                            ?>
                            <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title">Income Approach - Proposed Calculations  - Proposed (By Maxima)</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row" style="padding: 10px">

                                        <table id="owner_container"
                                               class="table table-striped table-bordered table-hover text-center images-table ">
                                            <thead>
                                            <tr>
                                                <td style="padding: 10px !important;" class=" width_5">
                                                    <strong>Unit Type </strong>
                                                </td>

                                                <td style="padding: 10px !important;" class=" width_25">
                                                    <strong>Quantity</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class=" width_25">
                                                    <strong>NLA</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class=" width_15">
                                                    <strong>Rent per square feet</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_10">
                                                    <strong>Income per Unit (Market rents)</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_10">
                                                    <strong>Income per Unit (Ejari)</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_25">
                                                    <strong>Total Gross / Per Unit (Market rents (25%) + Ejari(75%)) </strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_25">
                                                    <strong>Total Gross * (Quantity) </strong>
                                                </td>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if(!empty($all_units_maxima)){

                                                foreach ($all_units_maxima as $key => $unit){
                                                    ?>


                                                    <tr id="owner_data_row">
                                                        <td style="padding-top: 30px!important;">
                                                            <?php echo  Yii::$app->appHelperFunctions->incomeTypesArr[$key] ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= $unit['qty'] ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= $unit['nla'] ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= ($unit['rsf']<> null) ?  number_format($unit['rsf'],2):''; ?>
                                                        </td>

                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= ($unit['list']->mv_total_price <> null) ?  number_format($unit['list']->mv_total_price,2):''; ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= ($unit['sold']->mv_total_price<> null) ?  number_format($unit['sold']->mv_total_price,2):''; ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= number_format($unit['total_per_unit'],2); ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= number_format($unit['total'],2); ?>
                                                        </td>
                                                    </tr>
                                                <?php } } ?>


                                            </tbody>
                                            <tfoot>

                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </section>

                            <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title">Income Approach - Proposed Calculations  - Proposed (By Valuer)</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row" style="padding: 10px">

                                        <table id="owner_container"
                                               class="table table-striped table-bordered table-hover text-center images-table ">
                                            <thead>
                                            <tr>
                                                <td style="padding: 10px !important;" class=" width_5">
                                                    <strong>Unit Type </strong>
                                                </td>

                                                <td style="padding: 10px !important;" class=" width_25">
                                                    <strong>Quantity</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class=" width_25">
                                                    <strong>NLA</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class=" width_15">
                                                    <strong>Rent per square feet</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_10">
                                                    <strong>Income per Unit (Market rents)</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_10">
                                                    <strong>Income per Unit (Ejari)</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_25">
                                                    <strong>Total Gross (Market rents (25%) + Ejari(75%)) </strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_25">
                                                    <strong>Total Gross * (Quantity) </strong>
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if(!empty($all_units_valuer)){

                                                foreach ($all_units_valuer as $key => $unit){
                                                    ?>


                                                    <tr id="owner_data_row">
                                                        <td style="padding-top: 30px!important;">
                                                            <?php echo  Yii::$app->appHelperFunctions->incomeTypesArr[$key] ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= $unit['qty'] ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= $unit['nla'] ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= ($unit['rsf']<> null) ?  number_format($unit['rsf'],2):''; ?>
                                                        </td>

                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= ($unit['list']->mv_total_price <> null) ?  number_format($unit['list']->mv_total_price,2):''; ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= ($unit['sold']->mv_total_price<> null) ?  number_format($unit['sold']->mv_total_price,2):''; ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= number_format($unit['total_per_unit'],2); ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= number_format($unit['total'],2); ?>
                                                        </td>
                                                    </tr>
                                                <?php } } ?>


                                            </tbody>
                                            <tfoot>

                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </section>

                            <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title">Income Approach - Proposed Calculations  - Proposed (By Approver)</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row" style="padding: 10px">

                                        <table id="owner_container"
                                               class="table table-striped table-bordered table-hover text-center images-table ">
                                            <thead>
                                            <tr>
                                                <td style="padding: 10px !important;" class=" width_5">
                                                    <strong>Unit Type </strong>
                                                </td>

                                                <td style="padding: 10px !important;" class=" width_25">
                                                    <strong>Quantity</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class=" width_25">
                                                    <strong>NLA</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class=" width_15">
                                                    <strong>Rent per square feet</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_10">
                                                    <strong>Income per Unit (Market rents)</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_10">
                                                    <strong>Income per Unit (Ejari)</strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_25">
                                                    <strong>Total Gross / Per Unit (Market rents (25%) + Ejari(75%)) </strong>
                                                </td>
                                                <td style="padding: 10px !important;" class="width_25">
                                                    <strong>Total Gross * (Quantity)) </strong>
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if(!empty($all_units)){

                                                foreach ($all_units as $key => $unit){
                                                    ?>


                                                    <tr id="owner_data_row">
                                                        <td style="padding-top: 30px!important;">
                                                            <?php echo  Yii::$app->appHelperFunctions->incomeTypesArr[$key] ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= $unit['qty'] ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= $unit['nla'] ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= ($unit['rsf']<> null) ?  number_format($unit['rsf'],2):''; ?>
                                                        </td>

                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= ($unit['list']->mv_total_price <> null) ?  number_format($unit['list']->mv_total_price,2):''; ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= ($unit['sold']->mv_total_price<> null) ?  number_format($unit['sold']->mv_total_price,2):''; ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= number_format($unit['total_per_unit'],2); ?>
                                                        </td>
                                                        <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                            <?= number_format($unit['total'],2); ?>
                                                        </td>

                                                    </tr>
                                                <?php } } ?>


                                            </tbody>
                                            <tfoot>

                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </section>
                        <?php } ?>



                        <section class="valuation-form card card-outline card-primary">

                            <header class="card-header">
                                <h2 class="card-title">Income Approach - Proposed (By Maxima)</h2>
                            </header>
                            <div class="card-body">
                                <div class="row" style="padding: 10px">

                                    <table id="owner_container"
                                           class="table table-striped table-bordered table-hover text-center images-table ">
                                        <thead>
                                        <tr>
                                            <td style="padding: 10px !important;" class=" width_5">
                                                <strong>Income Subjects</strong>
                                            </td>

                                            <td style="padding: 10px !important;" class=" width_25">
                                                <strong>Factor</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class=" width_15">
                                                <strong>Property Financial Source</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_10">
                                                <strong>Value per square feet</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_25">
                                                <strong>Gross Yields (%)</strong>
                                            </td>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Gross Rental Income
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['gross_rental_income']['factor']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['gross_rental_income']['source'] <> null) ?  number_format($income_approach_orignal['gross_rental_income']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?=  ($income_approach_orignal['gross_rental_income']['psf'] <> null) ?  number_format($income_approach_orignal['gross_rental_income']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= number_format($income_approach_orignal['gross_rental_income']['yield'],2); ?>
                                            </td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Operations and Maintainance Costs
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['operations_and_maintainance_costs']['factor']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['operations_and_maintainance_costs']['source'] <> null) ?  number_format($income_approach_orignal['operations_and_maintainance_costs']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['operations_and_maintainance_costs']['psf'] <> null) ? number_format($income_approach_orignal['operations_and_maintainance_costs']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['operations_and_maintainance_costs']['yield']; ?>
                                            </td>

                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Vacancy
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['vacancy']['factor']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['vacancy']['source'] <> null) ? number_format($income_approach_orignal['vacancy']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['vacancy']['psf'] <> null) ? number_format($income_approach_orignal['vacancy']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['vacancy']['yield']; ?>
                                            </td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Land Lease Costs
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['land_lease_costs']['factor']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['land_lease_costs']['source'] <> null) ? number_format($income_approach_orignal['land_lease_costs']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['land_lease_costs']['psf'] <> null) ? number_format($income_approach_orignal['land_lease_costs']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['land_lease_costs']['yield']; ?>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Utilities Cost
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['insurance']['factor']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['insurance']['source'] <> null) ? number_format($income_approach_orignal['insurance']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['insurance']['psf'] <> null) ? number_format($income_approach_orignal['insurance']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['insurance']['yield']; ?>
                                            </td>

                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Net Income
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['net_income']['factor']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['net_income']['source'] <> null) ? number_format($income_approach_orignal['net_income']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['net_income']['psf'] <> null) ? number_format($income_approach_orignal['net_income']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['net_income']['yield']; ?>
                                            </td>

                                        </tr>


                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Net Yield Applied
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['net_yield_applied']['factor']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['net_yield_applied']['source'] <> null) ? number_format($income_approach_orignal['net_yield_applied']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['net_yield_applied']['psf'] <> null) ?  number_format($income_approach_orignal['net_yield_applied']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['net_yield_applied']['yield']; ?>
                                            </td>

                                        </tr>


                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Estimated Market Value <br> as per Income Approach
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['estimated_market_value']['factor']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['estimated_market_value']['source'] <> null) ? number_format($income_approach_orignal['estimated_market_value']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach_orignal['estimated_market_value']['psf'] <> null) ? number_format($income_approach_orignal['estimated_market_value']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach_orignal['estimated_market_value']['yield']; ?>
                                            </td>


                                        </tr>




                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </section>

                        <section class="valuation-form card card-outline card-primary">
                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title">Income Approach - Final</h2>
                            </header>
                            <div class="card-body">

                                <div class="row" style="padding: 10px">

                                    <table id="owner_container"
                                           class="table table-striped table-bordered table-hover text-center images-table ">
                                        <thead>
                                        <tr>
                                            <td style="padding: 10px !important;" class=" width_5">
                                                <strong>Income Subjects</strong>
                                            </td>

                                            <td style="padding: 10px !important;" class=" width_25">
                                                <strong>Factor</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class=" width_15">
                                                <strong>Property Financial Source</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_10">
                                                <strong>Value per square feet</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_25">
                                                <strong>Gross Yields (%)</strong>
                                            </td>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Gross Rental Income
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach['gross_rental_income']['factor']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?/*= $income_approach['gross_rental_income']['source']; */?>
                                                <?= $form->field($income, 'gross_source')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['gross_rental_income']['psf'] <> null) ? number_format($income_approach['gross_rental_income']['psf'],2) : ''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['gross_rental_income']['yield'] <> null) ? number_format($income_approach['gross_rental_income']['yield'],2):''; ?>

                                            </td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Operations and Maintainance Costs
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach['operations_and_maintainance_costs']['factor']; ?>

                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['operations_and_maintainance_costs']['source'] <> null) ? number_format($income_approach['operations_and_maintainance_costs']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['operations_and_maintainance_costs']['psf'] <> null) ? number_format($income_approach['operations_and_maintainance_costs']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach['operations_and_maintainance_costs']['yield']; ?>
                                            </td>

                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Vacancy
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <!--  --><?/*= $income_approach['vacancy']['factor']; */?>
                                                <?= $form->field($income, 'vacancy')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['vacancy']['source'] <> null) ? number_format($income_approach['vacancy']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['vacancy']['psf'] <> null) ? number_format($income_approach['vacancy']['psf'],2): ''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach['vacancy']['yield']; ?>
                                            </td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Land Lease Costs
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <!--  --><?/*= $income_approach['land_lease_costs']['factor']; */?>
                                                <?= $form->field($income, 'land_lease_costs')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['land_lease_costs']['source'] <> null) ? number_format($income_approach['land_lease_costs']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['land_lease_costs']['psf'] <> null) ? number_format($income_approach['land_lease_costs']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach['land_lease_costs']['yield']; ?>
                                            </td>

                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Utilities Cost
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <!--  --><?/*= $income_approach['insurance']['factor']; */?>
                                                <?= $form->field($income, 'utilities_cost')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['insurance']['source'] <> null) ? number_format($income_approach['insurance']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?=($income_approach['insurance']['psf'] <> null) ? number_format($income_approach['insurance']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach['insurance']['yield']; ?>
                                            </td>

                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Net Income
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach['net_income']['factor']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['net_income']['source'] <> null) ? number_format($income_approach['net_income']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['net_income']['psf'] <> null) ? number_format($income_approach['net_income']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach['net_income']['yield']; ?>
                                            </td>

                                        </tr>


                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Net Yield Applied
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <!-- --><?/*= $income_approach['net_yield_applied']['factor']; */?>
                                                <?= $form->field($income, 'net_yield_applied')->textInput(['maxlength' => true])->label(false) ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['net_yield_applied']['source'] <> null) ? number_format($income_approach['net_yield_applied']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['net_yield_applied']['psf'] <> null) ? number_format($income_approach['net_yield_applied']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach['net_yield_applied']['yield']; ?>
                                            </td>

                                        </tr>


                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Estimated Market Value <br> as per Income Approach
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach['estimated_market_value']['factor']; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['estimated_market_value']['source'] <> null) ? number_format($income_approach['estimated_market_value']['source'],2):''; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($income_approach['estimated_market_value']['psf'] <> null) ? number_format($income_approach['estimated_market_value']['psf'],2):''; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $income_approach['estimated_market_value']['yield']; ?>
                                            </td>


                                        </tr>




                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>






                    </div>
                </div>
            </div>
        </div>
    </div>











    <!-- /.card -->
</div>
<div class="clearfix"></div>



