<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Derive MV Cost Approach');
$cardTitle = Yii::t('app', 'Derive MV Cost Approach:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_19/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-4 col-sm-2">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 19]); ?>
                </div>
            </div>
            <div class="col-8 col-sm-10">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <header class="card-header">
                                <h2 class="card-title">Cost Approach</h2>
                            </header>
                            <div class="card-body">
                                <div class="row" style="padding: 10px">

                                    <table id="owner_container"
                                           class="table table-striped table-bordered table-hover text-center images-table ">
                                        <thead>
                                        <tr>
                                            <td style="padding: 10px !important;" class=" width_5">
                                                <strong>Cost Subjects</strong>
                                            </td>

                                            <td style="padding: 10px !important;" class=" width_25">
                                                <strong>Price/Factor</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class=" width_15">
                                                <strong>Size</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_10">
                                                <strong>Status</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_25">
                                                <strong>Calculations</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_25">
                                                <strong>Total Value/Costs</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_25">
                                                <strong>Price/square feet BUA</strong>
                                            </td>
                                        </tr>
                                        </thead>
                                        <tbody>


                                                <tr>
                                                    <td colspan="7" style="text-align: left">
                                                       <strong> Land Market Value Calculations </strong>
                                                    </td>

                                                </tr>
                                                <tr id="owner_data_row">
                                                    <td style="padding-top: 30px!important;">
                                                        Land Market Value
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_market_value']['factor']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_market_value']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_market_value']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_market_value']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_market_value']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_market_value']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr id="owner_data_row">
                                                    <td style="padding-top: 30px!important;">
                                                        Financing Charges for Land
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['financing_charges_for_land']['factor']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['financing_charges_for_land']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['financing_charges_for_land']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['financing_charges_for_land']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['financing_charges_for_land']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['financing_charges_for_land']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr id="owner_data_row">
                                                    <td style="padding-top: 30px!important;">
                                                       Total Cost
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_price_total']['factor']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_price_total']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_price_total']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_price_total']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_price_total']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_price_total']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="7" style="text-align: left">
                                                        <strong> Construction Costs Calculations</strong>
                                                    </td>

                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Construction Costs
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['construction_costs']['factor']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['construction_costs']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['construction_costs']['status']; ?> %
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['construction_costs']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['construction_costs']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['construction_costs']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Professional Charges
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['professional_charges']['factor']; ?> %
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['professional_charges']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['professional_charges']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['professional_charges']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['professional_charges']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['professional_charges']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Contingency Margin
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['contingency_margin']['factor']; ?>  %
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['contingency_margin']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['contingency_margin']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['contingency_margin']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['contingency_margin']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['contingency_margin']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Final Construction Cost
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['final_construction_cost']['factor']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['final_construction_cost']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['final_construction_cost']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['final_construction_cost']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['final_construction_cost']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['final_construction_cost']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td colspan="7" style="text-align: left">
                                                        <strong>Financing Charges</strong>
                                                    </td>

                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Number of years to <br> complete the building
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['number_of_years']['factor']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['number_of_years']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['number_of_years']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['number_of_years']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['number_of_years']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['number_of_years']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Interest rate
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['interest_rate']['factor']; ?>  %
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['interest_rate']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['interest_rate']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['interest_rate']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['interest_rate']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['interest_rate']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Effective interest rate
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['effective_interest_rate']['factor']; ?>  %
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['effective_interest_rate']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['effective_interest_rate']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['effective_interest_rate']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['effective_interest_rate']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['effective_interest_rate']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                     Total Costs including <br> Financing Charges
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['total_financing_charges']['factor']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['total_financing_charges']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['total_financing_charges']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['total_financing_charges']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['total_financing_charges']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['total_financing_charges']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>



                                                <tr>
                                                    <td colspan="7" style="text-align: left">
                                                        <strong>Development Costs Deductions</strong>
                                                    </td>

                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Depreciation
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['depreciation']['factor']; ?>  %
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['depreciation']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['depreciation']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['depreciation']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['depreciation']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['depreciation']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Obscolence
                                                    </td>



                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['obscolence']['factor']; ?>  %
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['obscolence']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['obscolence']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['obscolence']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['obscolence']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['obscolence']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="7"></td>
                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Net Development Cost
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['net_development_cost']['factor']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['net_development_cost']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['net_development_cost']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['net_development_cost']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['net_development_cost']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['net_development_cost']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Land Value Plus <br> Net Development Cost
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_value_plus_net_development_cost']['factor']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_value_plus_net_development_cost']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_value_plus_net_development_cost']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_value_plus_net_development_cost']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_value_plus_net_development_cost']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['land_value_plus_net_development_cost']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Developer Profit (%)
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['developer_profit']['factor']; ?>  %
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['developer_profit']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['developer_profit']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['developer_profit']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['developer_profit']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['developer_profit']['price_per_sf_bua']; ?>
                                                    </td>

                                                </tr>
                                                <tr >
                                                    <td style="padding-top: 30px!important;">
                                                        Estimated Total Cost
                                                    </td>


                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['estimated_total_cost']['factor']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['estimated_total_cost']['size']; ?>
                                                    </td>

                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['estimated_total_cost']['status']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['estimated_total_cost']['calculations']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['estimated_total_cost']['total_value_by_cost']; ?>
                                                    </td>
                                                    <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                        <?= $cost_approach['estimated_total_cost']['price_per_sf_bua']; ?>
                                                    </td>
                                                </tr>



                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>



