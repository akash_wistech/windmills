<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enter Cost Details');
$cardTitle = Yii::t('app', 'Enter Cost Details:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_7/'.$valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$this->registerJs('

$("#transaction_price_date_id,#date_of_original_date_price_id").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');
?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
</style>

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-edit"></i>
                <?= $valuation->reference_number ?>
            </h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-5 col-sm-3">
                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                        <?php  echo $this->render('../left-nav', ['model' => $valuation,'step' => 7]); ?>
                    </div>
                </div>
                <div class="col-7 col-sm-9">
                    <div class="tab-content" id="vert-tabs-tabContent">
                        <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                            <section class="valuation-form card card-outline card-primary">

                                <?php $form = ActiveForm::begin(); ?>
                                <header class="card-header">
                                    <h2 class="card-title"><?= $cardTitle ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'original_purchase_price')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'transaction_price')->textInput(['maxlength' => true]) ?>
                                        </div>

                                        <div class="col-sm-4">
                                            <?php
                                            echo $form->field($model, 'transaction_source')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArr,
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>

                                        </div>
                                        <div class="col-sm-4">
                                            <?php
                                            if($model->lands_price <> null){

                                            }else {
                                                $model->lands_price = $valuation->building->communities->land_price;
                                            }
                                            ?>
                                            <?= $form->field($model, 'lands_price')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'parking_price')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'pool_price')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'landscape_price')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'utilities_connected_price')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'white_goods_price')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'number_of_years')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'land_lease_costs')->textInput(['maxlength' => true])->label('Land Lease Cost(%)') ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?php
                                            echo $form->field($model, 'source_of_original_date_price')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArr,
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>

                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'transaction_price_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="transaction_price_date_id" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#transaction_price_date_id" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'date_of_original_date_price', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="date_of_original_date_price_id" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#date_of_original_date_price_id" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>


                                </div>
                                <div class="card-footer">
                                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </section>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>



