<?php

if ($config_type == 'config_bedrooms') {
    $current_data = $configuration->no_of_bedrooms;
} else if ($config_type == 'config_bathrooms') {
    $current_data = $configuration->no_of_bathrooms;
} else if ($config_type == 'config_kitchen') {
    $current_data = $configuration->no_of_kitchen;
}else if ($config_type == 'config_living_area') {
    $current_data = $configuration->no_of_living_area;
}else if ($config_type == 'config_dining_area') {
    $current_data = $configuration->no_of_dining_area;
}else if ($config_type == 'config_maid_rooms') {
    $current_data = $configuration->no_of_maid_rooms;
}else if ($config_type == 'config_laundry_area') {
    $current_data = $configuration->no_of_laundry_area;
}else if ($config_type == 'config_store') {
    $current_data = $configuration->no_of_store;
}else if ($config_type == 'config_service_block') {
    $current_data = $configuration->no_of_service_block;
}else if ($config_type == 'config_garage') {
    $current_data = $configuration->no_of_garage;
}else if ($config_type == 'config_balcony') {
    $current_data = $configuration->no_of_balcony;
}else if ($config_type == 'config_family_room') {
    $current_data = $configuration->no_of_family_room;
}else if ($config_type == 'config_powder_room') {
    $current_data = $configuration->no_of_powder_room;
}else if ($config_type == 'config_study_room') {
    $current_data = $configuration->no_of_study_room;
}else if ($config_type == 'config_flooring') {
    $current_data = 1;
}else if ($config_type == 'config_ceiling') {
    $current_data = 1;
}else if ($config_type == 'config_view') {
    $current_data = 1;
}else if ($config_type == 'config_general_elevation') {
    $current_data = 1;
}else if ($config_type == 'config_unit_tag') {
    $current_data = 1;
}else if ($config_type == 'config_electricity_board') {
    $current_data = 1;
}else if ($config_type == 'custom_fields') {
    $current_data = $quantity;
}
?>

<section class="valuation-form card card-outline card-primary">

    <header class="card-header">
        <?php
        if(isset($custom_field_id) && ($custom_field_id <> null)) {
            $title = str_replace("_", " ", str_replace("config", " ", $name));
        }else {
            $title = str_replace("_", " ", str_replace("config", " ", $config_type));
        }
        ?>
        <h2 class="card-title"><?= ucfirst(trim($title)); ?> Information</h2>
    </header>
    <div class="card-body">
        <div class="row" style="padding: 10px">

            <table id="owner_container"
                   class="table table-striped table-bordered table-hover text-center images-table ">
                <thead>
                <tr>
                    <td style="padding: 10px !important;"
                        class=" width_10"><strong>No#</strong></td>
                    <?php if($attributes == 0){ ?>
                    <td style="padding: 10px !important;"
                        class=" width_30"><strong>&nbsp;</strong></td>
                    <?php }else{ ?>
                        <td style="padding: 10px !important;"
                            class=" width_30"><strong>Floor</strong></td>
                    <?php } ?>
                    <?php if($attributes == 0){ ?>
                        <td style="padding: 10px !important;"
                            class="width_30"><strong>&nbsp;</strong></td>
                    <?php }else{ ?>

                        <td style="padding: 10px !important;"
                            class="width_30"><strong>Flooring</strong></td>

                        <td style="padding: 10px !important;"
                            class="width_30"><strong>Ceilings</strong></td>

                        <td style="padding: 10px !important;"
                            class="width_30"><strong>Speciality</strong></td>


                   <!-- <td style="padding: 10px !important;"
                        class="width_30"><strong>Upgrade</strong></td>-->
                    <?php } ?>
                    <td style="padding: 10px !important;"
                        class="width_30"><strong>Attachment</strong></td>
                    <td style="padding: 10px !important;"
                        class="width_30"><strong>Check for Email</strong></td>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($current_data > 0) {
                    $custom_counter = 0;
                    for ($i1 = 0; $i1 < $current_data; $i1++) {
                       if(isset($custom_field_id) && ($custom_field_id <> null)) {
                           $bed_Details = \app\models\ConfigurationFiles::find()->where(["valuation_id" => $valuation->id, "index_id" => $i1, 'custom_field_id' => $custom_field_id])->one();
                       }else {
                           $bed_Details = \app\models\ConfigurationFiles::find()->where(["valuation_id" => $valuation->id, "index_id" => $i1, 'type' => $config_type])->one();
                       }
                        $attachment = ($bed_Details <> null) ? $bed_Details->attachment : '';
                        $floor_value = ($bed_Details <> null) ? $bed_Details->floor : '';
                        $field_name = ($bed_Details <> null) ? $bed_Details->field_name : '';
                        $flooring_value = ($bed_Details <> null) ? $bed_Details->flooring : '';
                        $ceiling_value = ($bed_Details <> null) ? $bed_Details->ceilings : '';
                        $speciality_value = ($bed_Details <> null) ? $bed_Details->speciality : '';
                        $CheckedEmail = ($bed_Details <> null) ? $bed_Details->checked_image : '';
                        $upgrade_value = ($bed_Details <> null) ? $bed_Details->upgrade : '';
                        ?>
                        <tr id="owner_data_row<?php echo($i1); ?>">
                            <td style="padding-top: 30px!important;"><?= ($i1 + 1) ?></td>

                            <?php if($attributes == 0){ ?>
                            <!--<td class="text-center"
                                style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                            </td>-->
                                <?php if(isset($custom_field_id) && ($custom_field_id <> null)){ ?>
                                <td class="text-center"
                                    style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                    <div class="form-group">
                                        <input type="text" class="form-control"
                                               name="ValuationConfiguration[<?= $config_type ?>][<?php echo $custom_field_id.'_'.$custom_counter; ?>][field_name]"
                                               value="<?= $field_name; ?>"
                                               />
                                    </div>
                                </td>
                            <?php }else{ ?>
                                <td class="text-center"
                                    style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                </td>
                            <?php } ?>
                        <?php }else{ ?>
                            <td class="text-center"
                                style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                <div class="form-group">
                                    <select class="form-control"
                                            name="ValuationConfiguration[<?= $config_type ?>][<?= $i1 ?>][floor]">
                                        <?php foreach (Yii::$app->appHelperFunctions->configrationFloorListArr as $key => $floors) {

                                            if (Yii::$app->appHelperFunctions->configrationFloorListArr[$floor_value] == $floors) {
                                                ?>
                                                <option value="<?= $key ?>" selected> <?= $floors ?> </option>
                                            <?php } else { ?>
                                                <option value="<?= $key ?>"> <?= $floors ?> </option>
                                            <?php }

                                        } ?>
                                    </select>
                                </div>
                            </td>
                            <?php } ?>

                        <?php if($attributes == 0){ ?>
                            <td class="text-center"
                                style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                            </td>
                            <?php }else{ ?>
                            <td class="text-center"
                                style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                <div class="form-group">

                                    <select class="form-control"
                                            name="ValuationConfiguration[<?= $config_type ?>][<?= $i1 ?>][flooring]">
                                        <option value="0" <?= ($flooring_value == 0)? 'selected': '' ?> > No </option>
                                        <option value="1" <?= ($flooring_value == 1)? 'selected': '' ?>> Yes </option>
                                    </select>


                                  <!--  <select class="form-control"
                                            name="ValuationConfiguration[<?/*= $config_type */?>][<?/*= $i1 */?>][upgrade]">
                                        <?php /*foreach (Yii::$app->appHelperFunctions->configrationUpgradesListArr as $key => $upgrade) {

                                            if (Yii::$app->appHelperFunctions->configrationUpgradesListArr[$upgrade_value] == $upgrade) {
                                                */?>
                                                <option value="<?/*= $key */?>" selected> <?/*= $upgrade */?> </option>
                                            <?php /*} else { */?>
                                                <option value="<?/*= $key */?>"> <?/*= $upgrade */?> </option>
                                            <?php /*}

                                        } */?>
                                    </select>-->

                                </div>
                            </td>
                            <td class="text-center"
                                style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                <div class="form-group">

                                    <select class="form-control"
                                            name="ValuationConfiguration[<?= $config_type ?>][<?= $i1 ?>][ceilings]">
                                        <option value="0" <?= ($ceiling_value == 0)? 'selected': '' ?> > No </option>
                                        <option value="1" <?= ($ceiling_value == 1)? 'selected': '' ?>> Yes </option>
                                    </select>
                                </div>
                            </td>
                            <td class="text-center"
                                style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                <div class="form-group">

                                    <select class="form-control"
                                            name="ValuationConfiguration[<?= $config_type ?>][<?= $i1 ?>][speciality]">
                                        <option value="0" <?= ($speciality_value == 0)? 'selected': '' ?> > No </option>
                                        <option value="1" <?= ($speciality_value == 1)? 'selected': '' ?>> Yes </option>
                                    </select>
                                </div>
                            </td>
                            <?php } ?>
                            <td class="text-center upload-docs"
                                style="padding-top: 10px!important;">
                                <div class="form-group">
                                    <a href="javascript:;"
                                       id="upload-document<?= $unit_row; ?>"
                                       onclick="uploadAttachment(<?= $unit_row; ?>)"
                                       data-toggle="tooltip"
                                       class="img-thumbnail"
                                       title="Upload Document">
                                        <?php

                                        if ($attachment <> null) {

                                            if (strpos($attachment, '.pdf') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>

                                                <?php

                                            } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php

                                            } else {
                                                ?>
                                                <img src="<?php echo $attachment; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                 alt="" title=""
                                                 data-placeholder="no_image.png"/>
                                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                               target="_blank">
                                                <span class="glyphicon glyphicon-eye-open"></span>
                                            </a>
                                            <?php
                                        }

                                        ?>
                                    </a>

                                    <?php if(isset($custom_field_id) && ($custom_field_id <> null)){ ?>
                            <input type="hidden"
                                   name="ValuationConfiguration[<?= $config_type ?>][<?= $custom_field_id.'_'.$custom_counter; ?>][custom_field_id]"
                                   value="<?= $custom_field_id; ?>"/>

                                        <input type="hidden"
                                               name="ValuationConfiguration[<?= $config_type ?>][<?php echo $custom_field_id.'_'.$custom_counter; ?>][attachment]"
                                               value="<?= $attachment; ?>"
                                               id="input-attachment<?php echo $unit_row; ?>"/>

                                     <?php }else{ ?>

                                    <input type="hidden"
                                           name="ValuationConfiguration[<?= $config_type ?>][<?php echo $i1; ?>][attachment]"
                                           value="<?= $attachment; ?>"
                                           id="input-attachment<?php echo $unit_row; ?>"/>
                                <?php } ?>

                                </div>
                            </td>
                        <?php if(isset($custom_field_id) && ($custom_field_id <> null)){ ?>
                            <input type="hidden"
                                   name="ValuationConfiguration[<?= $config_type ?>][<?= $custom_field_id.'_'.$custom_counter; ?>][valuation_id]"
                                   value="<?= $valuation->id; ?>"/>
                            <input type="hidden"
                                   name="ValuationConfiguration[<?= $config_type ?>][<?= $custom_field_id.'_'.$custom_counter; ?>][index_id]"
                                   value="<?= $i1; ?>"
                            />
                        <?php

                        }else{ ?>
                            <input type="hidden"
                                   name="ValuationConfiguration[<?= $config_type ?>][<?= $i1; ?>][valuation_id]"
                                   value="<?= $valuation->id; ?>"/>
                            <input type="hidden"
                                   name="ValuationConfiguration[<?= $config_type ?>][<?= $i1; ?>][index_id]"
                                   value="<?= $i1; ?>">
                        <?php } ?>
                            <td>
                        <?php if(isset($custom_field_id) && ($custom_field_id <> null)){ ?>
                                <input class="mt-4 pt-2 check_image_checked <?= ($CheckedEmail=='on') ? '' : 'checked_img' ?> " type="checkbox"  name="ValuationConfiguration[<?= $config_type ?>][<?= $custom_field_id.'_'.$custom_counter; ?>][image_checked]" <?= ($CheckedEmail=='on') ? 'checked' : '' ?>>
                            <?php
                            $custom_counter++;
                        }else{ ?>
                            <input class="mt-4 pt-2 check_image_checked <?= ($CheckedEmail=='on') ? '' : 'checked_img' ?> " type="checkbox"  name="ValuationConfiguration[<?= $config_type ?>][<?= $i1; ?>][image_checked]" <?= ($CheckedEmail=='on') ? 'checked' : '' ?>>
                           <?php } ?>
                            </td>
                        </tr>
                        <?php
                        $unit_row++;
                    }
                }
                ?>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
</section>