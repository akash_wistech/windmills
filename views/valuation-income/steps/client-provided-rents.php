<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Client Provided Rents');
$cardTitle = Yii::t('app', 'Client Provided Rents:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/client-provided-rents?id='.$valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');





$this->registerJs('

    $("#clientprovidedrents-start_date,#clientprovidedrents-end_date").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "YYYY-MM-DD"
    });
   


   
');

?>
<style>
.datepicker-days .table>thead>tr>th,
.table>tbody>tr>th,
.table>tfoot>tr>th,
.table>thead>tr>td,
.table>tbody>tr>td,
.table>tfoot>tr>td {
    padding: inherit !important;
}
</style>




<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
</style>



<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                    <?php  echo $this->render('../left-nav', ['model' => $valuation,'step' => 4]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'income_type')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->getIncomeTypesArr(),
                                            'options' => ['placeholder' => 'Select a Income Type'],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4 ">
                                        <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true]) ?>
                                    </div>


                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'nla')->textInput(['type'=>'number','maxlength' => true])->label('Net Leasable Area ') ?>
                                    </div>


                                    <div class="col-sm-4" id="start_date_id">

                                        <label for="html" id='inpect_date_label'>Contract Start Date</label><br>
                                        <?= $form->field($model, 'contract_start_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="clientprovidedrents-start_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#clientprovidedrents-start_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])->label(false) ?>
                                    </div>


                                    <div class="col-sm-4" id="end_date_id">

                                        <label for="html" id='inpect_date_label'>Contract End Date</label><br>
                                        <?= $form->field($model, 'contract_end_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="clientprovidedrents-end_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#clientprovidedrents-end_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])->label(false) ?>
                                    </div>


                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'rent')->textInput(['type'=>'number','maxlength' => true]) ?>
                                    </div>

                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'status')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->getCprArr(),
                                            'options' => ['placeholder' => 'Select a Status'],
                                        ]);
                                        ?>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>


                        <section class="valuation-form card card-outline card-primary">

                            <div class="card-body">
                                <?php
                                $total = 0;
                                $avg = 0;


                                if(isset($previous_records) && ($previous_records <> null)){ ?>
                                    <div class="row">

                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Sr#</th>
                                                <th>Unit Number</th>
                                                <th>Type</th>
                                                <th>Contract Start Date</th>
                                                <th>Contract End Date</th>
                                                <th>NLA</th>
                                                <th>Rent</th>
                                                <th>Rent/NLA</th>
                                                <th>Occupancy Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($previous_records as $key => $record){

                                                $total =  $total + $record->rent_sqf;
                                                $avg =  count($previous_records);
                                                echo $avg;
                                                die;

                                                ?>
                                                <tr>
                                                    <td>&nbsp;<?= ($key + 1) ?></td>
                                                    <td>&nbsp;<?= $record->unit_number ?></td>
                                                    <td>&nbsp;<?= Yii::$app->appHelperFunctions->getIncomeTypesArr()[$record->income_type] ?></td>
                                                    <td>&nbsp;<?= date('d-m-Y', strtotime($record->contract_start_date)); ?></td>
                                                    <td>&nbsp;<?= date('d-m-Y', strtotime($record->contract_end_date)); ?></td>
                                                    <td>&nbsp;<?= Yii::$app->appHelperFunctions->wmFormate($record->nla); ?></td>
                                                    <td>&nbsp;<?= Yii::$app->appHelperFunctions->wmFormate($record->rent); ?></td>
                                                    <td>&nbsp;<?= Yii::$app->appHelperFunctions->wmFormate($record->rent_sqf); ?></td>
                                                    <td>&nbsp;<?= Yii::$app->appHelperFunctions->getCprArr()[$record->status]; ?></td>

                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } ?>

                            </div>

                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



