<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Derive MV Income Approach');
$cardTitle = Yii::t('app', 'Derive MV Income Approach:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_21/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

if ($model->email_status !=1) {
    $AlertText='Data and Valuation Status will be saved and Email will be sent once?';
}
else{
    $AlertText='Only Data will be saved?';
}

$this->registerJs('

    $("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "'.Yii::t('app',$AlertText).'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
      console.log("Hello 123")
      $("#w0").unbind("submit").submit();
    }
});
});



$("body").on("click", ".sav-btn", function (e) {

swal({
title: "'.Yii::t('app','Valuation status will be saved and Email will be sent to Clients ?').'",
html: "'.Yii::t('app','').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({

  url: "'.Url::to(['valuation/send-email','id'=>$model->id,'step'=>21]).'",
  dataType: "html",
  type: "POST",
  });
    }
});
});


$("body").on("click",".load-model",function(){
modelurl=$(this).data("url")
$.ajax({
  url:modelurl,
  dataType: "html",
  type: "POST",
  success: function(html) {
  $(".modal-body").html(html);
  },
  error: function(xhr,ajaxoptions,thownError){

    alert(xhr.responseText);
  }

});

});

var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});



');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number  ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-4 col-sm-2">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 21]); ?>
                </div>
            </div>
            <div class="col-8 col-sm-10">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">


                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title">Final Review</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <input type="hidden" id="landsize" value="<?= $valuation->land_size ?>">

                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_value')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_value_sqf')->textInput(['maxlength' => true, 'readonly' => true])->label('Estimated Market Value per Sqft of BUA') ?>
                                    </div>
                                    <?php

                                    if(in_array($valuation->property_id, array(4,5,11,23,26,29,39))){ ?>
                                        <div class="col-sm-6">
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($model, 'market_value_sqf_pa')->textInput(['maxlength' => true, 'readonly' => true])->label('Estimated Market Value per Sqft of Plot Area') ?>
                                        </div>


                                    <?php } ?>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_rent')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_rent_sqf')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'parking_market_value')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'parking_market_value_sqf')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                    </div>

                                    <?php if(($valuation->client->land_valutaion == 1) && (round($valuation->inspectProperty->estimated_age) >= $valuation->client->land_age) && ($valuation->tenure == 1) &&  ($valuation->property_id == 6 || $valuation->property_id == 22 || $valuation->property_id == 2 || $valuation->property_id == 22 || $valuation->property_id == 2) ){ ?>
                                        <div class="col-sm-12">
                                            <section class="valuation-form card card-outline card-primary">
                                                <header class="card-header">
                                                    <h2 class="card-title">Final Review</h2>
                                                </header>
                                                <div class="card-body">
                                                    <div class="row">


                                                        <div class="col-sm-6">
                                                            <?= $form->field($model, 'estimated_market_value_land')->textInput(['maxlength' => true])->label('Land Market Value') ?>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <?= $form->field($model, 'land_market_value_sqf_bua')->textInput(['maxlength' => true, 'readonly' => true])->label('BUA Sqf') ?>
                                                        </div>
                                                        <div class="col-sm-6">
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <?= $form->field($model, 'land_market_value_sqf_pa')->textInput(['maxlength' => true, 'readonly' => true])->label('Plot Area Sqf') ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    <?php } ?>


                                    <div class="col-sm-12 text-right pb10">
                                        <button type="button" id="bua_calculation" class="btn btn-info">BUA Calculate</button>
                                        <input type="hidden" id="bua" value="<?= $bua ?>">
                                    </div>


                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'status')->widget(Select2::classname(), [
                                            'data' => array('Approve' => 'Recommended For Approval', 'Reject' => 'Reject'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6"></div>

                                    <div class="col-sm-12" id="reason_text" style="<?= ($model->status == 'Reject')? "": 'display:none;' ?>" >

                                        <?= $form->field($model, 'reason')->textarea(['rows' => '6']) ?>
                                    </div>


                                    <div class="col-sm-12" id="reason_text" style="<?= ($valuation->client_id == 4)? "": 'display:none;' ?>" >

                                        <?= $form->field($model, 'additional_notes')->textarea(['rows' => '6']) ?>
                                    </div>



                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label class="control-label">Valuer</label>
                                            <input type="text" id="valuationapproversdata-parking_market_value_sqf"
                                                   class="form-control"
                                                   value="<?= (isset($model->created_by) && ($model->created_by <> null))? ($model->user->firstname): Yii::$app->user->identity->firstname  ?>"
                                                   readonly="">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label class="control-label">Date</label>
                                            <input type="text" id="valuationapproversdata-parking_market_value_sqf"
                                                   class="form-control"
                                                   value="<?= (isset($model->created_at) && ($model->created_at <> null))? (date('Y-m-d',strtotime($model->created_at))) : date('Y-m-d')  ?>"
                                                   readonly="">
                                        </div>
                                    </div>

                                    <?php if($valuation->parent_id <> null && $valuation->parent_id > 0){ ?>
                                        <div class="col-sm-12"  >
                                            <?= $form->field($model, 'revise_reason')->textarea(['rows' => '6']) ?>
                                        </div>
                                    <?php } ?>

                                </div>


                            </div>

                        </section>






                                <?php $model->step=21;  ?>
                                <?php echo $form->field($model, 'step')->textInput(['maxlength' => true,
                                    'type'=>'hidden'])->label('') ?>


                               <!-- <?php /*if($valuation->parent_id <> null && $valuation->parent_id > 0){ */?>

                                    <div class="col-sm-12"  >

                                        <?/*= $form->field($model, 'revise_reason')->textarea(['rows' => '6']) */?>
                                    </div>
                                --><?php /*} */?>
                                <input type="hidden" name="ValuationApproversData[created_by]" value="<?= (isset($model->created_by) && ($model->created_by <> null))? $model->created_by : Yii::$app->user->identity->id  ?>">
                                <!-- <div class="col-sm-6">
                                        <div class="form-group ">

                                            <label class="control-label">Valuer</label>
                                            <input type="text" id="valuationapproversdata-parking_market_value_sqf"
                                                   class="form-control"
                                                   value="<?/*= (isset($model->created_by) && ($model->created_by <> null))? ($model->user->firstname.''.$model->user->lastname): (Yii::$app->user->identity->firstname.' '.Yii::$app->user->identity->lastname)  */?>"
                                                   readonly="">
                                        </div>
                                    </div>
-->



                    </div>


                    <!-- Modal -->
                    <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header ">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1 mx-2']) ?>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <!-- Button trigger modal -->


                        <button type="button" class="btn btn-success load-model mr-2" data-url="<?= Url::toRoute(['valuation/pdf-html', 'id' =>$valuation->id]);  ?>" data-toggle="modal" data-target="#exampleModalLong">
                            Save
                        </button>
                        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                        <?php  if(isset($valuation->building->city) && ($valuation->building->city == 3507)){ ?>
                          <a href="https://maxima.windmillsgroup.com/client/export-ajman-download?id=<?= $valuation->id ?>" style="margin-left: 10px;" class="btn btn-info"> Verify Excel report</a>
                   <?php } ?>
                        <?php


                        $lastRecordId = \app\models\Valuation::find()
                            ->where(['portfolio' => 1])
                            ->andWhere(['ref_portfolio' => $valuation->ref_portfolio])
                            //->andWhere(['valuation_status' => 5])
                            ->orderBy(['id' => SORT_DESC])
                            ->limit(1)
                            ->one()->id;

                        if(isset($lastRecordId) && $lastRecordId ==  $valuation->id) {
                            ?>
                            <a href="https://maxima.windmillsgroup.com/valuation/invoice_pt?ref_portfolio=<?= $valuation->ref_portfolio ?>" style="margin-left: 10px;" class="btn btn-info" target="_blank"> Verify Portfolio Report</a>
                            <?php
                        }
                        ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.card -->
</div>

<script type="text/javascript">






</script>
