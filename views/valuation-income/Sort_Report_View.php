<?php
// die($page_title);

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$clientsArr = ArrayHelper::map(\app\models\Company::find()
    ->select([
        'id','title',
        'cname_with_nick' => 'CONCAT(title," ",nick_name)',
    ])
    ->where(['status' => 1])
    ->andWhere([
        'or',
        ['data_type' => 0],
        ['data_type' => null],
    ])
    ->orderBy(['title' => SORT_ASC,])
    ->all(), 'id', 'title');
$clientsArr = ['' => 'select'] + $clientsArr;

$propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$propertiesArr = ['' => 'select'] + $propertiesArr;

$communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$communitiesArr = ['' => 'select'] + $communitiesArr;

$citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
$citiesArr = ['' => 'select'] + $citiesArr;


$instruction_date_width = '80px';
$client_width = '70px';
$client_ref_width_mistake = '7px';
$wm_ref_width = '70px';
$property_width = '70px';
$property_width_mistake = '80px';
$community_width = '70px';
$city_width = '80px';
$valuer_width = '80px';
$valuer_width_mistake = '80px';
$fee_width = '90px';
$inspector_width = '15px';
$val_status_width = '150px';
$client_ref_width = '150px';
?>

    <style>
        /* .dataTable th {*/
        /*    color: #0056b3;*/
        /*    font-size: 14px;*/
        /*    text-align: right !important;*/
        /*    padding-right: 30px !important;*/
        /*}*/
        /*.dataTable td {*/
        /*    font-size: 16px;*/
        /*    text-align: right;*/
        /*    padding-right: 50px;*/
        /*}*/
        /*.content-header h1 {*/
        /*       font-size: 16px !important;*/

        /*    }*/
        /*.content-header .row {*/
        /*    margin-bottom: 0px !important;*/
        /*}*/
        /*.content-header {*/
        /*    padding: 4px !important;*/
        /*} */



        .dataTable th {
            color: #0056b3;
            font-size: 15px;
            text-align: left !important;
            padding-right: 20px !important;
        }
        .dataTable td {
            font-size: 16px;
            text-align: left;
            /* padding-right: 50px; */
            padding-top: 3px;
            max-width: 100px;
        }

        .content-header h1 {
            font-size: 16px !important;

        }

        .content-header .row {
            margin-bottom: 0px !important;
        }

        .content-header {
            padding: 0px !important;
        }
        <?php if($page_title == 'Today Received') {
           ?>

        th:nth-child(1) {
            min-width: <?= $instruction_date_width ?> ;
            max-width: <?= $instruction_date_width ?> ;
        }
        th:nth-child(2) {
            min-width: <?= $client_width ?>;
            max-width: <?= $client_width ?>;
        }
        th:nth-child(3) {
            min-width: <?= $wm_ref_width ?>;
            max-width: <?= $wm_ref_width ?>;
        }
        th:nth-child(4) {
            min-width: <?= $property_width ?>;
            max-width: <?= $property_width ?>;
        }
        th:nth-child(5) {
            min-width: <?= $community_width ?>;
            max-width: <?= $community_width ?>;
        }
        th:nth-child(6) {
            min-width: <?= $city_width ?>;
            max-width: <?= $city_width ?>;
        }
        th:nth-child(7) {
            min-width: <?= $valuer_width ?>;
            max-width: <?= $valuer_width ?>;
        }
        th:nth-child(8) {
            min-width: <?= $fee_width ?>;
            max-width: <?= $fee_width ?>;
            text-align: right;
        }
        th:nth-child(9) {
            min-width: <?= $val_status_width ?>;
            max-width: <?= $val_status_width ?>;
        }


        <?php
    }

    else if($page_title == 'Today Inspected') {
        ?>
        th:nth-child(1) {
            min-width: <?= $instruction_date_width ?>;
            max-width: <?= $instruction_date_width ?>;
        }
        th:nth-child(2) {
            min-width: <?= $client_width ?>;
            max-width: <?= $client_width ?>;
        }
        th:nth-child(3) {
            min-width: <?= $wm_ref_width ?>;
            max-width: <?= $wm_ref_width ?>;
        }
        th:nth-child(4) {
            min-width: <?= $property_width ?>;
            max-width: <?= $property_width ?>;
        }
        th:nth-child(5) {
            min-width: <?= $community_width ?>;
            max-width: <?= $community_width ?>;
        }
        th:nth-child(6) {
            min-width: <?= $city_width ?>;
            max-width: <?= $city_width ?>;
        }
        th:nth-child(7) {
            min-width: <?= $inspector_width ?>;
            max-width: <?= $inspector_width ?>;
        }
        th:nth-child(8) {
            min-width: <?= $fee_width ?>;
            max-width: <?= $fee_width ?>;
            text-align: right;
        }
        th:nth-child(9) {
            min-width: <?= $val_status_width ?>;
            max-width: <?= $val_status_width ?>;
        }
        <?php
    }
    else if($page_title == 'Today Mistakes'){

      ?>
        th:nth-child(1) {
            min-width: <?= $instruction_date_width ?> ;
            max-width: <?= $instruction_date_width ?> ;
        }
        th:nth-child(2) {
            min-width: <?= $client_width ?>;
            max-width: <?= $client_width ?>;
        }
        th:nth-child(3) {
            min-width: <?= $client_ref_width_mistake ?>;
            max-width: <?= $client_ref_width_mistake ?>;
        }
        th:nth-child(4) {
            min-width: <?= $wm_ref_width ?>;
            max-width: <?= $wm_ref_width ?>;
        }
        th:nth-child(5) {
            min-width: <?= $property_width_mistake ?>;
            max-width: <?= $property_width_mistake ?>;
        }
        th:nth-child(6) {
            min-width: <?= $valuer_width_mistake ?>;
            max-width: <?= $valuer_width_mistake ?>;
        }
        th:nth-child(7) {
            min-width: <?= $fee_width ?>;
            max-width: <?= $fee_width ?>;
            text-align: right;
        }
        th:nth-child(8) {
            min-width: '90px';
            max-width: '90px';
        }


        <?php } else if($page_title == 'Today Performance Overview'){ ?>

        th:nth-child(1) {
            min-width: <?= $instruction_date_width ?> ;
            max-width: <?= $instruction_date_width ?> ;
        }
        th:nth-child(2) {
            min-width: <?= $wm_ref_width ?>;
            max-width: <?= $wm_ref_width ?>;
        }
        th:nth-child(3) {
            min-width: <?= $client_width ?>;
            max-width: <?= $client_width ?>;
        }
        th:nth-child(4) {
            min-width: <?= $client_width ?>;
            max-width: <?= $client_width ?>;
        }

        th:nth-child(5) {
            min-width: <?= $property_width ?>;
            max-width: <?= $property_width ?>;
        }
        th:nth-child(6) {
            min-width: <?= $city_width ?>;
            max-width: <?= $city_width ?>;
        }
        th:nth-child(7) {
            min-width: <?= $valuer_width ?>;
            max-width: <?= $valuer_width ?>;
        }
        th:nth-child(8) {
            min-width: <?= $inspector_width ?>;
            max-width: <?= $inspector_width ?>;
        }
        th:nth-child(9) {
            min-width: 70px;
            max-width: 70px;
        }
        th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }
        th:nth-child(11) {
            min-width: 10px;
            max-width: 10px;
        }
        <?php } elseif ($page_title == "Today Reminders") { ?>
        th:nth-child(1) {
            min-width: 75px ;
            max-width: 75px ;
        }
        th:nth-child(2) {
            min-width: 75px ;
            max-width: 75px ;
        }
        th:nth-child(3) {
            min-width: 60px ;
            max-width: 60px ;
        }
        th:nth-child(4) {
            min-width: 60px ;
            max-width: 60px ;
        }

        th:nth-child(5) {
            min-width: 60px ;
            max-width: 60px ;
        }
        th:nth-child(6) {
            min-width: 70px ;
            max-width: 70px ;
        }
        th:nth-child(7) {
            min-width: 45px ;
            max-width: 45px ;
        }
        th:nth-child(8) {
            min-width: 45px ;
            max-width: 45px ;
        }
        th:nth-child(9) {
            min-width: 40px ;
            max-width: 40px ;
        }
        th:nth-child(10) {
            min-width: 45px ;
            max-width: 45px ;
        }
        th:nth-child(11) {
            min-width: 45px;
            max-width: 45px;
        }
        th:nth-child(12) {
            min-width: 40px;
            max-width: 40px;
        }
        th:nth-child(13) {
            min-width: 45px;
            max-width: 45px;
        }
        th:nth-child(14) {
            min-width: 45px;
            max-width: 45px;
        }

        <?php } elseif ($page_title == "Today Cancelled") { ?>

        <?php } ?>

    </style>

<?php
// echo "<pre>";
// print_r($page_title);
// echo "<pre>";die;
?>
    <input type="hidden" id="comm-id" value='<?php echo $page_title; ?>'>
    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <span><strong><?= $page_title; ?></strong></span><br /><br />
                <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>

                        <th class="">Instruction Date
                            <input id="intruction_date" type="date" class="form-control" placeholder="date">
                        </th>

                        <?php if($page_title == 'Today Reminders'){ ?>
                            <th class="">Inspection Done Date
                                <input type="text" class="custom-search-input form-control" placeholder="date">
                            </th>

                            <th class="">Recomm Date
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <th class="">Review Date
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <th class="">Approve Date
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <th class="">Client Reminder Date
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <th class="">WM New Ref
                                <input type="text" class="custom-search-input form-control" placeholder="New ref#">
                            </th>
                        <?php } ?>

                        <?php if($page_title == 'Today Mistakes' || $page_title == 'Today Performance Overview'){ ?>
                            <th class="">Client Ref
                                <input type="text" class="custom-search-input form-control" placeholder="client ref">
                            </th>
                        <?php } ?>

                        <?php if($page_title != 'Today Reminders'){   ?>

                            <th class="">WM Reference
                                <input type="text" class="custom-search-input form-control" placeholder="ref#">
                            </th>

                            <th class="">Client
                                <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder'=>'client']); ?>
                            </th>

                            <th class="">Property
                                <?php echo Html::dropDownList('property', null, $propertiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'Property']); ?>
                            </th>
                        <?php } ?>

                        <?php if($page_title != 'Today Mistakes'){ ?>

                            <?php if($page_title == 'Today Reminders'){ ?>
                                <th style="display: none;" id="community-id" class="">Community
                                    <?php echo Html::dropDownList('community', null, $communitiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'commmunity']); ?>
                                </th>
                            <?php } else if($page_title == "Today Performance Overview") { ?>
                                <th style="display: none;" id="community-id" class="">Community
                                    <?php echo Html::dropDownList('community', null, $communitiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'commmunity']); ?>
                                </th>
                            <?php } ?>

                            <?php if($page_title == 'Today Reminders'){ ?>
                                <th style="display: none;" class="">City
                                    <?php echo Html::dropDownList('city', null, $citiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'city']); ?>
                                </th>
                            <?php } else { ?>
                                <th class="">City
                                    <?php echo Html::dropDownList('city', null, $citiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'city']); ?>
                                </th>
                            <?php } ?>
                        <?php } ?>

                        <?php if($page_title == 'Today Inspected' || $page_title == 'Today Performance Overview' || $page_title == 'Today Reminders'){ ?>
                            <th class="">Inspector
                                <input type="text" class="custom-search-input form-control" placeholder="inspector">
                            </th>
                        <?php } ?>


                        <?php if($page_title != 'Today Inspected'){ ?>
                            <th class="">Valuer
                                <input type="text" class="custom-search-input form-control" placeholder="valuer">
                            </th>
                        <?php } ?>


                        <?php if($page_title == 'Today Approved' || $page_title == 'Today Reminders'){ ?>
                            <th class="">Approver<input type="text" class="custom-search-input form-control" placeholder="approver"></th>
                        <?php } ?>

                        <?php if($page_title == 'Today Reminders'){ ?>
                            <th class="">Reviewer<input type="text" class="custom-search-input form-control" placeholder="Reviewer"></th>
                        <?php } ?>

                        <?php if($page_title != 'Today Reminders'){ ?>
                            <th class="">Fee
                                <input type="text" class="custom-search-input form-control" placeholder="Fee">
                            </th>
                        <?php } ?>

                        <th class="">Status
                            <input type="text" class="custom-search-input form-control" placeholder="Status">
                        </th>

                        <?php if($page_title == 'Today Reminders'){ ?>
                            <th class="">Total TAT Taken
                                <input type="text" class="custom-search-input form-control" placeholder="Total TAT">
                            </th>
                        <?php } ?>

                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($dataProvider->getModels() as $model){
                            ?>
                            <tr class="active">

                                <!-- Instruction Date -->
                                <?php if($page_title == 'Today Reminders' || $page_title == 'Today Performance Overview'){ ?>
                                    <td>
                                        <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                                        <br>
                                        <?= Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time) ?>
                                    </td>
                                <?php } ?>

                                <!-- Inspection Done Date -->
                                <?php if($page_title == 'Today Reminders') { ?>
                                    <td>
                                        <?= date('d-M-Y',strtotime($model->inspectProperty->inspection_done_date )) ?>
                                        <br>
                                        <?= date('h:i A',strtotime($model->inspectProperty->inspection_done_date )) ?>
                                    </td>

                                    <!-- Recom date -->
                                    <td>

                                    </td>

                                    <!-- Review Date -->
                                    <td>

                                    </td>

                                    <!-- Approve Date -->
                                    <td>

                                    </td>

                                    <!-- Client Reminder Date -->
                                    <td>

                                    </td>

                                    <!-- WM New Refernce -->
                                    <td>

                                    </td>
                                <?php } ?>

                                <!-- Client ref -->
                                <?php if($page_title == 'Today Mistakes' || $page_title == 'Today Performance Overview'){ ?>
                                    <td><?= $model->client_reference ?></td>
                                <?php } ?>

                                <!-- Ref Number -->
                                <?php if($page_title != 'Today Reminders'){ ?>
                                    <td>
                                        <?= $model->reference_number ?>
                                    </td>

                                    <!-- Client -->
                                    <td>
                                        <?= ($model->client->nick_name <> null) ?$model->client->nick_name : $model->client->title ?>
                                    </td>

                                    <!-- Property -->
                                    <td>
                                        <?= $model->property->title ?>
                                    </td>
                                <?php } ?>

                                <!-- Communitys -->
                                <?php if($page_title != 'Today Mistakes'){
                                    ?>
                                    <?php if($page_title == 'Today Performance Overview' || $page_title == 'Today Reminders'){ ?>
                                        <td id="community-id" style="display: none;">
                                            <?= $model->building->communities->title ?>
                                        </td>
                                        <?php
                                    } else { ?>
                                        <td>
                                            <?= $model->building->communities->title ?>
                                        </td>
                                    <?php }
                                    ?>

                                    <?php if($page_title == 'Today Reminders'){ ?>
                                        <td style="display: none;">
                                            <?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
                                        </td>
                                    <?php } else{ ?>
                                        <td>
                                            <?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
                                        </td>
                                    <?php } ?>
                                    <?php
                                }
                                ?>

                                <?php if($page_title == 'Today Reminders'){ ?>
                                    <td>
                                        <?php  ?>
                                    </td>
                                <?php } ?>

                                <?php if($page_title == 'Today Inspected' || $page_title == 'Today Performance Overview' || $page_title == 'Today Reminders'){ ?>
                                    <td>
                                        <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null))? (Yii::$app->appHelperFunctions->staffMemberListArrLastName[$model->scheduleInspection->inspection_officer]): '' ?>
                                    </td>
                                <?php } ?>

                                <?php if($page_title != 'Today Inspected'){ ?>
                                    <td>
                                        <?= (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '' ?>
                                    </td>
                                <?php } ?>

                                <!-- Approver -->
                                <?php if($page_title == 'Today Approved' || $page_title == 'Today Reminders'){ ?>
                                <td><?= (isset($model->approverData->user->firstname) && ($model->approverData->user->firstname <> null))? ($model->approverData->user->lastname): '' ?>
                                    <?php } ?>

                                    <!-- Reviewer -->
                                    <?php if($page_title == 'Today Reminders'){ ?>
                                <td> </td>
                            <?php } ?>

                                <?php if($page_title != 'Today Reminders'){ ?>
                                    <td>
                                        <?= Yii::$app->appHelperFunctions->wmFormate($model->fee) ?>
                                    </td>
                                <?php } ?>

                                <!-- Status -->
                                <td>
                                    <?= Yii::$app->helperFunctions->valuationStatusListArrWords[$model->valuation_status] ?>
                                </td>

                                <!-- Total TAT -->
                                <?php if($page_title == 'Today Reminders'){ ?>
                                    <td>
                                    </td>
                                <?php } ?>

                                <?php
                                if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                                    ?>
                                    <td class="noprint actions">
                                        <div class="btn-group flex-wrap">
                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <a class="dropdown-item text-1"
                                                   href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                                   title="Step 1" data-pjax="0">
                                                    <i class="fas fa-edit"></i> Step 1
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>

                    <?php if($page_title == 'Today Received'){ ?>
                        <th></th><th></th><th></th><th></th><th></th>
                        <th><strong>Total Fee:</strong></th>
                        <th></th>
                        <th  style="padding-right: 10px!important; text-align:right!important;"> <?= Yii::$app->appHelperFunctions->wmFormate(\app\models\Valuation::getTotal($dataProvider->models, 'fee')) ?></th>
                        <th></th>
                        <?php
                    }else if($page_title == 'Today Inspected'){
                        ?>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><strong>Total Fee:</strong></th>
                        <th></th>
                        <th  style="padding-right: 10px!important; text-align:right!important;"> <?= Yii::$app->appHelperFunctions->wmFormate(\app\models\Valuation::getTotal($dataProvider->models, 'fee')) ?></th>
                        <th></th>
                    <?php }else if ($page_title == 'Today Mistakes'){
                        ?>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><strong>Total Fee:</strong></th>
                        <th></th>
                        <th  style="padding-right: 10px!important; text-align:right!important;"> <?= Yii::$app->appHelperFunctions->wmFormate(\app\models\Valuation::getTotal($dataProvider->models, 'fee')) ?></th>
                        <th></th>
                        <?php
                    }else if ($page_title == 'Today Performance Overview') { ?>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><strong>Total Fee:</strong></th>
                        <th  style="padding-right: 10px!important;"> <?= Yii::$app->appHelperFunctions->wmFormate(\app\models\Valuation::getTotal($dataProvider->models, 'fee')) ?></th>
                        <th></th>
                    <?php } else {?>

                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>

                        <!-- <th><strong>Total Fee:</strong></th> -->
                        <th></th>
                        <th></th>
                        <!-- <th  style="padding-right: 10px!important; "> <?= Yii::$app->appHelperFunctions->wmFormate(\app\models\Valuation::getTotal($dataProvider->models, 'fee')) ?></th> -->
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <?php
                    }
                    ?>


                    <?php
                    if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <th></th>
                        <?php
                    }
                    ?>

                    </tfoot>
                </table>
            </div>
        </div>
    </div>




<?php
$this->registerJs('
        // $("#bank-revenue").DataTable({
        //     order: [[0, "asc"]],
        //     pageLength: 50,
        //     searching: false,
        // });
        
          var dataTable = $("#bank-revenue").DataTable({
            order: [[0, "asc"]],
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            columnDefs: [{
              targets: -1,  // The last column index
              orderable: false  // Disable sorting on the last column
            }]
          });
        
          $(".custom-search-input").on("change", function () {
            $("#intruction_date").val("");
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input").on("keyup", function () {
            $("#intruction_date").val("");
            dataTable.search(this.value).draw();
          });

          //for date search
          $("#intruction_date").on("change", function () {
            $(".custom-search-input").val("");
            var format1 = moment($("#intruction_date").val()).format("DD-MMM-YYYY"); 
              dataTable.search(format1).draw();
            });

          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input-client").on("change", function () {
              $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                  console.log(data);
                  dataTable.search(data).draw();
                },
                error: bbAlert
            });

          });
                
          $("#bank-revenue_filter").css({
            "display":"none",
          });
        
        $(".custom-search-input").on("click", function (event) {
          event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
          event.stopPropagation();
        });
        

        // Move the length menu to the table footer
        // var lengthMenu = $(".dataTables_length");
        // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");

        // Create a custom footer element
        var customFooter = $("<div class=\"custom-footer\"></div>");
      
        // Move the length menu to the custom footer
        var lengthMenu = $(".dataTables_length");
        lengthMenu.detach().appendTo(customFooter);
      
        // Append the custom footer to the DataTable wrapper
        $("#bank-revenue_wrapper").append(customFooter);

        if($("#comm-id").val() == "Today Performance Overview")
        {
          $("#community-id").css({
            "display":"none",
          });
        }

    ');
?>