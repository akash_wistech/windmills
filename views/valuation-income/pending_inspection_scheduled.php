
<style>
    th:nth-child(1) {
        min-width: 70px;
        max-width: 70px;
    }
    th:nth-child(2) {
        min-width: 70px;
        max-width: 70px;
    }
    th:nth-child(3) {
        min-width: 70px;
        max-width: 70px;
    }
    th:nth-child(4) {
        min-width: 60px;
        max-width: 60px;
    }
    th:nth-child(5) {
        min-width: 90px;
        max-width: 90px;
    }
    th:nth-child(6) {
        min-width: 220px;
        max-width: 220px;
    }
    th:nth-child(7) {
        min-width: 65px;
        max-width: 65px;
    }
    th:nth-child(8) {
        min-width: 70px;
        max-width: 70px;
    }
    th:nth-child(9) {
        min-width: 70px;
        max-width: 70px;
    }
    th:nth-child(10) {
        min-width: 70px;
        max-width: 70px;
    }
    th:nth-child(11) {
        min-width: 50px;
        max-width: 50px;
    }

</style>


<div class="bank-revenue-index col-12">
    <div class="card card-outline card-info">
        <div class="card-body">
            <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                <thead>
                    <tr>
                        
                        <th class="">Inspection Date
                            <input type="text" class="custom-search-input form-control" placeholder="date">
                        </th>

                        <th class="">Inspection Time
                            <input type="text" class="custom-search-input form-control" placeholder="time">
                        </th>

                        <th class="">WM Refrence
                            <input type="text" class="custom-search-input form-control" placeholder="Ref#">
                        </th>

                        <th class="">Client Refrence
                            <input type="text" class="custom-search-input form-control" placeholder="client Ref#">
                        </th>

                        <th class="">Property
                            <input type="text" class="custom-search-input form-control" placeholder="Property">
                        </th>

                        <th class="">Project/Building
                            <input type="text" class="custom-search-input form-control" placeholder="Project/Building">
                        </th>

                        <!-- <th class="">Community
                            <input type="text" class="custom-search-input form-control" placeholder="Community">
                        </th> -->

                        <th class="">City
                            <input type="text" class="custom-search-input form-control" placeholder="City">
                        </th>

                        <th class="">Inspector
                            <input type="text" class="custom-search-input form-control" placeholder="Inspector">
                        </th>

                        <!-- <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th> -->

                        <th class="">Contact Person
                            <input type="text" class="custom-search-input form-control" placeholder="contact email">
                        </th>

                        <th class="">Contact Phone
                            <input type="text" class="custom-search-input form-control" placeholder="contact phone">
                        </th>
                        <th>Reason</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($inspections) > 0){
                        foreach($inspections as $inspection){
                            // dd($inspection->valuation->property->title);
                            $contact_number = '';
                            if($inspection->valuation->id >7418){

                                if ($inspection->valuation->contact_phone_no <> null) $contact_number = $inspection->valuation->phone_code . '' . $inspection->valuation->contact_phone_no;
                                if ($inspection->valuation->land_line_no <> null) $contact_number = $inspection->valuation->land_line_code . '' . $inspection->valuation->land_line_no;
                            }else {

                                if ($inspection->contact_phone_no <> null) $contact_number = $inspection->phone_code . '' . $inspection->contact_phone_no;
                                if ($inspection->land_line_no <> null) $contact_number = $inspection->land_line_code . '' . $inspection->land_line_no;
                            }
                    ?>

                    <tr class="active">

                        <td><?php echo $inspection->inspection_date ?></td>

                        <td><?= Yii::$app->appHelperFunctions->formatTimeAmPm($inspection->inspection_time) ?></td>
                        <td><?= $inspection->valuation->reference_number ?></td>
                        <td><?php echo chunk_split($inspection->valuation->client_reference, 15, ' ') ?></td>
                        <td><?= $inspection->valuation->property->title ?></td>
                        <td><?= $inspection->valuation->building->title ?></td>
                        <!-- <td><?php //echo $inspection->valuation->building->communities->title ?></td> -->
                        <td><?php echo Yii::$app->appHelperFunctions->emiratedListArr[$inspection->valuation->building->city] ?></td>
                        <td><?= (isset($inspection->inspection_officer) && ($inspection->inspection_officer <> null))? ($inspection->inspectorData->lastname) : '' ?></td>
                        <!-- <td><?php //echo (isset($inspection->valuation->service_officer_name) && ($inspection->valuation->service_officer_name <> null))? ($inspection->valuation->approver->lastname): '' ?></td> -->
                      <!--  <td><?/*= strtolower($inspection->contact_person_name); */?></td>-->
                        <td>
                            <?php if($inspection->valuation->id >7418){ ?>
                                <?= strtolower($inspection->valuation->contact_person_name); ?>
                            <?php }else{ ?>

                                <?= strtolower($inspection->contact_person_name); ?>
                            <?php } ?>
                        </td>
                        <td><?= $contact_number ?></td>

                        <td style="text-align:center;"> <a href="javascript:;" class="view-reasons-btn" data-valuation-id="<?= $inspection->valuation_id ?>"><i class="fas fa-eye text-success"></i></a> </td>



                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

