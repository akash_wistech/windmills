<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

use app\assets\SortBootstrapAsset;
SortBootstrapAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Valuations');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
$createIncomeBtn = false;

if (($searchModel->valuation_approach == 0) && Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (($searchModel->valuation_approach == 1) && Yii::$app->menuHelperFunction->checkActionAllowed('income-create')) {
    $createIncomeBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) {
    $actionBtns .= '{step_1}';
}
$cancelBtns = '{cancel_valuation}';


if (Yii::$app->menuHelperFunction->checkActionAllowed('cancel_permission')) {
    $updateToCancel = '{update_to_cancel}';
}
$scanOfficer=User::find()->where(['id'=>Yii::$app->user->identity->id, 'permission_group_id'=>2])->select(['id'])->one();
if ($scanOfficer['id'] !=null) {
    $step_24 = '{step_24}';
}

$this->registerJs('

$("body").on("click", ".sav-btn1", function (e) {

_this=$(this);
var url=_this.attr("data-url");
var alerttext=_this.attr("data-text");
e.preventDefault();
swal({
title: alerttext,
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
     if (result) {
  $.ajax({
  url: url,
  dataType: "html",
  type: "POST",
  });
    }
});
});


    ');
?>


<?php  
if(in_array(Yii::$app->user->id, $allowedId )){ 
?>
<div class="valuation-index">
    <div class="card card-outline card-primary">
        <header class="card-header">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h2 class="card-title">List</h2>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="clearfix">
                        <div class="float-right">
                            <?php if($searchModel->valuation_approach == 0) { ?>
                            &nbsp;<a class="btn btn-xs btn-primary" href="<?=  Url::to(['valuation/create']) ?>" data-pjax="0" data-toggle="tooltip" data-title="Add New"><i class="fas fa-plus"></i></a>
                            <?php } else if($searchModel->valuation_approach == 1){ ?>
                            &nbsp;<a class="btn btn-xs btn-primary" href="<?= Url::to(['valuation/income-create']) ?>" data-pjax="0" data-toggle="tooltip" data-title="Add New"><i class="fas fa-plus"></i></a>
                            <?php } ?>
                        </div>
                        <div class="float-right" style="margin-right:10px;">
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="card-body">
            <table id="valuation-table" class="table table-bordered table-hover table-responsive cloud-data-table" style="width:100%">
                <thead>
                    <tr>
                        <th class="">Reference</th>
                        <th class="">Client</th>
                        <th class="">Property</th>
                        <th class="">Building</th>
                        <th class="">Community</th>
                        <th class="">City</th>
                        <th class="">Instruction Date</th>
                        <th class="">Valuation Report Date</th>
                        <th class="">Fee</th>
                        <th class="">Valuer</th>
                        <th class="">Market Value</th>
                        <th class="">Valuation Status</th>
                        <th class="">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    if(count($dataProvider->getModels())>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($dataProvider->getModels() as $model){
                    ?>
                        <tr>
                            <td><?= $model->reference_number ?></td>
                            <td><?= $model->client->title ?></td>
                            <td><?= $model->property->title ?></td>
                            <td><?= $model->building->title ?></td>
                            <td><?= $model->building->communities->title ?></td>
                            <td><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?></td>
                            <td><?= date('d-m-Y', strtotime($model->instruction_date)) ?></td>
                            <td>
                                <?php
                                if($model->scheduleInspection->valuation_report_date <> null) {
                                    echo date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
                                }else{
            
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if($model->quotation_id <> null) {
                                    echo Yii::$app->appHelperFunctions->wmFormate($model->fee);
                                }else {
                                    echo Yii::$app->appHelperFunctions->wmFormate( Yii::$app->appHelperFunctions->getClientRevenue($model->id) );
                                }
                                ?>
                            </td>
                            <td><?= (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): ''; ?></td>
                            <td><?= (isset($model->approverData) && ($model->approverData <> null))? (number_format($model->approverData->estimated_market_value)): '' ?></td>
                            <td><?= Yii::$app->helperFunctions->valuationStatusListArrLabel[$model['valuation_status']] ?></td>

                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1" href="<?= Url::to(['valuation/step_1','id'=>$model->id]) ?>" title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> Update
                                        </a>
                                    </div>
                                </div>
                            </td>
                            
                        </tr>
                    <?php
                        }
                    }else{
                        
                    }
                    ?>
                </tbody>
                <tfoot>
                    
                </tfoot>
            </table>
        </div>
    </div>
</div>


<?php
    }
    else{ 
?>
        
<div class="valuation-index">


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'createIncomeBtn' => $createIncomeBtn,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'reference_number', 'label' => Yii::t('app', 'Reference')],
            ['attribute' => 'client_id',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                    return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title;
                },
                'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Building'),
                'value' => function ($model) {
                    return $model->building->title;
                },
                'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'community',
                'label' => Yii::t('app', 'Community'),
                'value' => function ($model) {
                    return $model->building->communities->title;
                },
                'filter' => ArrayHelper::map(\app\models\Communities::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'city',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];
                },
                'filter' => Yii::$app->appHelperFunctions->emiratedListArr
            ],
            ['attribute' => 'instruction_date',
                'label' => Yii::t('app', 'Instruction Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->instruction_date));
                },
            ],

           /* ['attribute' => 'target_date',
                'label' => Yii::t('app', 'Target Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->target_date));
                },
            ],*/
            ['attribute' => 'submission_approver_date',
                'label' => Yii::t('app', 'Valuation Report Date'),
                'value' => function ($model) {
                    // return date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
                    if($model->scheduleInspection->valuation_report_date <> null) {
                        return date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
                    }else{

                    }
                },
            ],
            ['attribute' => 'total_fee',
                'label' => Yii::t('app', 'Fee'),
                'value' => function ($model) {
                    /*if (Yii::$app->appHelperFunctions->getClientRevenue($model->id)!=null) {
                      $total+=(int)Yii::$app->appHelperFunctions->getClientRevenue($model->id);
                    }*/
                    if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33])) 
                    {
                        if($model->quotation_id <> null) {
                            return $model->fee;
                        }else {
                            return Yii::$app->appHelperFunctions->getClientRevenue($model->id);
                        }
                    }else{
                        return "";
                    }
                   // return Yii::$app->appHelperFunctions->getClientRevenue($model->id);
                },
               // 'footer' => '<b>Fee Total</b> '.\app\models\Valuation::getTotal($dataProvider->models, 'id'),
            ],
            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
                },
                'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
                    'firstname' => SORT_ASC,
                ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];})
            ],
            [
                'attribute' => 'total_fee',
                'label' => Yii::t('app', 'Market Value'),
                'value' => function ($model) {
                    return (isset($model->approverData) && ($model->approverData <> null))? (number_format($model->approverData->estimated_market_value)): '';
                },
            ],

            ['format' => 'raw','attribute' => 'valuation_status',
                'label' => Yii::t('app', 'Valuation Status'),
                'value' => function ($model) {
                    $value=Yii::$app->helperFunctions->valuationStatusListArrLabel[$model['valuation_status']];

// target="_blank"
                    return Html::a($value, Url::to(['valuation/step_1','id'=>$model->id]), [
                        'title' => $value,
                        // 'class' => 'dropdown-item text-1',
                        // 'data-pjax' => "0",
                    ]);
                },
                'contentOptions' => ['class' => ' pt-3 pl-2'],
                'filter' => Yii::$app->helperFunctions->valuationStatusListArr
            ],

            ['format' => 'raw','attribute' => 'listing_done',
                'label' => Yii::t('app', ''),
                'value' => function ($model) {
                    if (Yii::$app->menuHelperFunction->checkActionAllowed('listing_done')) {
                        $query = \app\models\ValuationDetail::find()->where(['valuation_id'=>$model->id])->one();
                        if ($query->listing_done == "1" || $query->listing_done_number > 0) {
                            return '<br><i class="fa fa-check"></i> '.$query->listing_done_number;
                        } else {
                            return "";
                        }
                    }else{
                        return "";
                    }
                },
            ],


            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
                'contentOptions' => ['class' => 'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              ' . $actionBtns . '
              '.$cancelBtns.'
              '.$updateToCancel.'
              '.$step_24.'
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },

                    'step_1' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Update'), $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'status' => function ($url, $model) {
                        if ($model['status'] == 1) {
                            return Html::a('<i class="fas fa-eye-slash"></i> ' . Yii::t('app', 'Disable'), $url, [
                                'title' => Yii::t('app', 'Disable'),
                                'class' => 'dropdown-item text-1',
                                'data-confirm' => Yii::t('app', 'Are you sure you want to disable this item?'),
                                'data-method' => "post",
                            ]);
                        } else {
                            return Html::a('<i class="fas fa-eye"></i> ' . Yii::t('app', 'Enable'), $url, [
                                'title' => Yii::t('app', 'Enable'),
                                'class' => 'dropdown-item text-1',
                                'data-confirm' => Yii::t('app', 'Are you sure you want to enable this item?'),
                                'data-method' => "post",
                            ]);
                        }
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> ' . Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'dropdown-item text-1',
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'data-method' => "post",
                            'data-pjax' => "0",
                        ]);
                    },
                   /* 'cancel_valuation' => function ($url, $model) {
                        return Html::a('<i class="far fa-window-close"></i> ' . Yii::t('app', 'Cancel'),$url, [
                            'title' => Yii::t('app', 'Cancel'),
                            'class' => 'dropdown-item text-1 sav-btn1',
                            'data-url' => $url,
                            'data-text' => 'Email will be sent to Reviewer to Cancel the Valuation.',


                        ]);
                    },*/
                    'update_to_cancel' => function ($url, $model) {
                        return Html::a('<i class="far fa-window-close"></i> ' . Yii::t('app', 'Update to Cancel'), $url, [
                            'title' => Yii::t('app', 'Update To cancel'),
                            'class' => 'dropdown-item text-1 sav-btn1',
                            'data-url' => $url,
                            'data-text' => 'Valuation will be Canceled.',

                        ]);
                    },
                    'step_24' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Step 24'), $url, [
                            'title' => Yii::t('app', 'Step 24'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>

        
<?php
    }
?>



<?php 
    $this->registerJs('
        $("#valuation-table").DataTable({
            order: [[0, "asc"]],
            pageLength: 25,
        });
    ');
?>


