<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use app\assets\ListingsFormAsset;
use app\components\widgets\StatusVerified;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Inspect Property');
$cardTitle = Yii::t('app', 'Inspect Property:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_5/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
if ($model->email_status != 1) {
    $AlertText = 'Data and Valuation Status will be saved and Email will be sent once?';
} else {
    $AlertText = 'Only Data will be saved?';
}
$AlertText = 'Only Data will be saved?';


$property_id = $valuation->property_id;

// to display fields based on property type
$lands = Yii::$app->appHelperFunctions->landIdArr;
$apartments = array(1);
$villas = Yii::$app->appHelperFunctions->villaIdArr;
$townhouse = array(6, 11);
$office = array(28);
$shop = array(17);
$warehouse = array(24, 78);

$landIds = [];
foreach ($lands as $land) {
    $landIds[] = $land->id;
}

$villaIds = [];
foreach ($villas as $villa) {
    $villaIds[] = $villa->id;
}



$display_makani_number = (in_array($property_id, array_merge($landIds, $apartments, $shop))) ? "none" : "block";
$display_municipality_number = (in_array($property_id, array_merge($landIds, $apartments, $shop))) ? "none" : "block";

$display_built_up_area = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_balcony_size = (in_array($property_id, array_merge($landIds, $warehouse))) ? "none" : "block";
$display_service_area_size = (in_array($property_id, array_merge($landIds, $apartments, $office, $shop, $warehouse))) ? "none" : "block";
$display_net_built_up_area = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_permitted_bua = (in_array($property_id, array_merge($villaIds, $townhouse, $apartments, $office, $shop, $warehouse))) ? "none" : "block";
$display_permitted_gfa = (in_array($property_id, array_merge($villaIds, $townhouse, $apartments, $office, $shop, $warehouse))) ? "none" : "block";

$display_property_placement = (in_array($property_id, array_merge($apartments))) ? "none" : "block";
$display_property_exposure = (in_array($property_id, array_merge($apartments))) ? "none" : "block";
$display_property_visibility = (in_array($property_id, array_merge($apartments))) ? "none" : "block";
$display_location_school_drive = (in_array($property_id, array_merge($warehouse))) ? "none" : "block";
$display_location_park_drive = (in_array($property_id, array_merge($warehouse))) ? "none" : "block";

$display_finished_status = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_estimated_age = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_age_source = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_estimated_remaining_life = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_property_condition = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_property_defect = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_occupancy_status = (in_array($property_id, array_merge($landIds))) ? "none" : "block";

$display_development_type = (in_array($property_id, array_merge($landIds, $apartments))) ? "none" : "block";
$display_developer_id_property = (in_array($property_id, array_merge($landIds))) ? "none" : "block";

$display_number_of_building_on_land = (in_array($property_id, array_merge($landIds, $apartments, $villaIds, $townhouse, $office))) ? "none" : "block";
$display_number_of_floors = (in_array($property_id, array_merge($landIds, $villaIds, $townhouse, $office, $shop, $warehouse))) ? "none" : "block";
$display_total_units = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_residential_units = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_commercial_units = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_retail_units = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_number_of_basement = (in_array($property_id, array_merge($landIds, $apartments))) ? "none" : "block";
$display_number_of_mezannines = (in_array($property_id, array_merge($landIds, $apartments, $villaIds))) ? "none" : "block";
$display_parking_floors = (in_array($property_id, array_merge($landIds, $apartments))) ? "none" : "block";
$display_gated_community = (in_array($property_id, array_merge($landIds, $apartments))) ? "none" : "block";
$display_no_party_hall = (in_array($property_id, array_merge($landIds, $apartments))) ? "none" : "block";
$display_no_health_club_spa = (in_array($property_id, array_merge($landIds, $apartments))) ? "none" : "block";
$display_pool = (in_array($property_id, array_merge($landIds, $apartments))) ? "none" : "block";

$display_no_elevators = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_ac_type = (in_array($property_id, array_merge($landIds))) ? "none" : "block";
$display_utilities_connected = (in_array($property_id, array_merge($landIds))) ? "none" : "block";

$display_fridge = (in_array($property_id, array_merge($landIds, $shop, $warehouse))) ? "none" : "block";
$display_oven = (in_array($property_id, array_merge($landIds, $shop, $warehouse))) ? "none" : "block";
$display_washing_machine = (in_array($property_id, array_merge($landIds, $shop, $office, $warehouse))) ? "none" : "block";
$display_dish_washer = (in_array($property_id, array_merge($landIds, $shop, $office, $warehouse))) ? "none" : "block";

$display_no_studios = (in_array($property_id, array_merge($landIds, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_one_bedrooms = (in_array($property_id, array_merge($landIds, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_two_bedrooms = (in_array($property_id, array_merge($landIds, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_three_bedrooms = (in_array($property_id, array_merge($landIds, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_four_bedrooms = (in_array($property_id, array_merge($landIds, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_penthouse = (in_array($property_id, array_merge($landIds, $shop, $office, $warehouse))) ? "none" : "block";

$display_number_of_bbq_areas = (in_array($property_id, array_merge($landIds, $apartments, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_childrens_play_area = (in_array($property_id, array_merge($landIds, $apartments, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_of_swimming_pool = (in_array($property_id, array_merge($landIds, $apartments, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_of_jacuzzi = (in_array($property_id, array_merge($landIds, $apartments, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_of_gym_room = (in_array($property_id, array_merge($landIds, $apartments, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_of_school = (in_array($property_id, array_merge($landIds, $apartments, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_of_clinic = (in_array($property_id, array_merge($landIds, $apartments, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_of_sports_court = (in_array($property_id, array_merge($landIds, $apartments, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_of_masjid = (in_array($property_id, array_merge($landIds, $apartments, $shop, $office, $warehouse))) ? "none" : "block";
$display_no_of_community_center = (in_array($property_id, array_merge($landIds, $apartments, $shop, $office, $warehouse))) ? "none" : "block";

$display_view_community = (in_array($property_id, array_merge($shop, $warehouse))) ? "none" : "block";
$display_view_pool = (in_array($property_id, array_merge($shop, $warehouse))) ? "none" : "block";
$display_view_burj = (in_array($property_id, array_merge($shop, $warehouse))) ? "none" : "block";
$display_view_sea = (in_array($property_id, array_merge($shop, $warehouse))) ? "none" : "block";
$display_view_marina = (in_array($property_id, array_merge($shop, $warehouse))) ? "none" : "block";
$display_view_mountains = (in_array($property_id, array_merge($shop, $warehouse))) ? "none" : "block";
$display_view_lake = (in_array($property_id, array_merge($shop, $warehouse))) ? "none" : "block";
$display_view_golf_course = (in_array($property_id, array_merge($shop, $warehouse))) ? "none" : "block";
$display_view_park = (in_array($property_id, array_merge($shop, $warehouse))) ? "none" : "block";
$display_view_special = (in_array($property_id, array_merge($shop, $warehouse))) ? "none" : "block";

$this->registerJs('

$("body").on("click", ".sav-btn1", function (e) {
    e.preventDefault();
    _this=$(this);
    swal({
        title: "' . Yii::t('app', $AlertText) . '",
        html: "' . Yii::t('app', 'Are you sure you want to delete this? You will not be able to recover this!') . '",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#47a447",
        confirmButtonText: "' . Yii::t('app', 'Yes, Do you want to save it!') . '",
        cancelButtonText: "' . Yii::t('app', 'Cancel') . '",
    },function(result) {
        if (result) {
            console.log("Hello 123");
            // $("#w0").unbind("submit").submit();
            $("#w0").submit();
        }
    });
});

$("body").on("click", ".sav-btn", function (e) {

    swal({
        title: "' . Yii::t('app', 'Email will be sent to Clients!') . '",
        html: "' . Yii::t('app', '') . '",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#47a447",
        confirmButtonText: "' . Yii::t('app', 'Email will be sent to Clients!') . '",
        cancelButtonText: "' . Yii::t('app', 'Cancel') . '",
    },function(result) {
        if (result) {
            $.ajax({
                url: "' . Url::to(['valuation/inspectpropert', 'id' => $model->id]) . '",
                dataType: "html",
                type: "POST",
            });
        }
    });
});

var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});
  $("#inspectproperty-listing_property_type_options").on(\'change\',function(){
        $("#inspectproperty-listing_property_type").val($(this).val());
        });
        
    $("body").on("change", "#inspectproperty-permitted_bua_check", function () {
           console.log($(this).val());
           if($(this).val() == "yes"){
            $("#iuser_fixed_fee").show();
            $("#iuser_fixed_fee_gfa").show();
                   }else{
                    $("#inspectproperty-permitted_bua").val("");
                    $("#iuser_fixed_fee").hide();
                    $("#iuser_fixed_fee_gfa").hide();
                   }
       
    });
    
    $("#completion_certificate_date,#date_of_building_permit").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "YYYY-MM-DD"
    });


    



 

');





$i = 1;

if ($valuation->valuation_approach == 1) {
    $valuationApproach = "incomeApproach";
}
?>




<style>
    .help-block2 {
        color: red;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -14px;
        font-family: inherit;
    }
</style>

<div class="card card-primary card-outline" id="<?= $valuationApproach ?>">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 5]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form">
                            <?php $form = ActiveForm::begin(); ?>
                            <section class="card card-outline card-info">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Address Details Verify') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">


                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'plot_number_verify')->textInput(['maxlength' => true])->label('Plot Number <span class="text-danger">*</span>') ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'street_verify')->textInput(['maxlength' => true])->label('Street <span class="text-danger">*</span>') ?>
                                        </div>
                                        <div class="col-sm-4" style="">
                                            <?= $form->field($model, 'unit_number_verify')->textInput(['maxlength' => true])->label('Unit Number <span class="text-danger">*</span>') ?>
                                        </div>


                                        <div class="col-sm-4">
                                            <?php
                                            echo $form->field($model, 'city_verify')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->emiratedListArr,
                                                'options' => ['placeholder' => 'Select an City ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('City <span class="text-danger">*</span>');
                                            ?>

                                        </div>

                                        <div class="col-sm-4">
                                            <?php

                                            echo $form->field($model, 'community_verify')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(\app\models\Communities::find()->orderBy([
                                                    'title' => SORT_ASC,
                                                ])->all(), 'id', 'title'),
                                                'options' => ['placeholder' => 'Select a Community ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Community <span class="text-danger">*</span>');
                                            ?>

                                        </div>


                                        <div class="col-sm-4">
                                            <?= Html::hiddenInput('sub_community_value', $model->sub_community_verify, ['id' => 'sub_community_value']); ?>
                                            <?php

                                            echo $form->field($model, 'sub_community_verify')->widget(DepDrop::classname(), [
                                                'data' => ($model->sub_community_verify ? [$model->sub_community_verify => $model->subCommunities->title] : []),
                                                'options' => ['placeholder' => 'Select Sub Community'],
                                                'type' => DepDrop::TYPE_SELECT2,
                                                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                                'pluginOptions' => [
                                                    'initialize' => ($model->sub_community_verify ? true : false),
                                                    'depends' => ['inspectproperty-community_verify'],
                                                    'params' => ['sub_community_value'],
                                                    'url' => Url::to(['/sub-communities/search-sub-commuinites']),
                                                    'loadingText' => 'Loading Sub Communities ...',
                                                ]
                                            ])->label('Sub Community <span class="text-danger">*</span>');;
                                            ?>

                                        </div>


                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'country_verify')->dropDownList(Yii::$app->appHelperFunctions->countryListArr,['prompt'=>Yii::t('app','Select')])->label('Country <span class="text-danger">*</span>')?>
                                        </div>

                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'location_pin_verify')->textInput(['maxlength' => true])->label('Location Pin <span class="text-danger">*</span>') ?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title">Add Property Address Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 makani_number" style="display: <?= $display_makani_number ?>">
                                            <?= $form->field($model, 'makani_number')->textInput(['maxlength' => true])->label('Makani Number <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>
                                        <div class="col-sm-4 municipality_number" style="display: <?= $display_municipality_number ?>">
                                            <?= $form->field($model, 'municipality_number')->textInput(['maxlength' => true])->label('Municipality Number <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>
                                        <div class="col-sm-4 location_pin">
                                            <?= $form->field($model, 'location_pin')->textInput(['maxlength' => true])->label('Location Pin <span class="text-danger">*</span>') ?>
                                        </div>
                                        <!-- <div class="col-sm-4">
                                            <?php
                                        echo $form->field($model, 'listing_property_type')->widget(Select2::classname(), [
                                            // 'data' => Yii::$app->appHelperFunctions->listingsPropertyTypeListArr,
                                            'data' => ArrayHelper::map(\app\models\ListingSubTypes::find()->orderBy([
                                                'title' => SORT_ASC,
                                            ])->all(), 'title', 'title'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>

                                        </div> -->
                                    </div>
                                </div>

                            </section>

                            <section class="valuation-form card card-outline card-primary">


                                <header class="card-header">
                                    <h2 class="card-title">Size Details (square feet)</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 built_up_area mb-3"  style="display: <?= $display_built_up_area ?>">
                                            <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true])->label('Final Gross BUA/GFA Size (sft) <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($model, 'built_up_area_m')->textInput(['maxlength' => true])->label('Measured Gross BUA/GFA Size') ?>
                                        </div>
                                        <div class="col-sm-4 balcony_size mb-3"  style="display: <?= $display_balcony_size ?>">
                                            <?= $form->field($model, 'balcony_size')->textInput(['maxlength' => true])->label('Measured Balcony Size (sft) <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>
                                        <div class="col-sm-4 service_area_size mb-3" style="display: <?= $display_service_area_size ?>">
                                            <?= $form->field($model, 'service_area_size')->textInput(['maxlength' => true])->label('Measured Service Block Area BUA Size (sft) <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>

                                        </div>

                                        <div class="col-sm-4 net_built_up_area mb-3" style="display: <?= $display_net_built_up_area ?>">
                                            <?= $form->field($model, 'net_built_up_area')->textInput(['maxlength' => true])->label('Measured Net BUA/GFA Size  (sft) <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>

                                        </div>

                                        <!-- <div class="col-sm-4 permitted_bua_check">
                                            <?php
                                        // echo $form->field($model, 'permitted_bua_check')->widget(Select2::classname(), [
                                        //     'data' => array('no' => 'Not Available', 'yes' => 'Available'),
                                        //     'pluginOptions' => [
                                        //         'allowClear' => true
                                        //     ],
                                        // ])->label('Permitted BUA For Land <span class="text-danger">*</span>');
                                        // ?>
                                        </div> -->
                                        <?php
                                        // if ($model->permitted_bua_check == 'yes') {
                                        //     $style = "block";
                                        // } else {
                                        //     $style = "none";
                                        // }

                                        ?>
                                        <div class="col-sm-4 permitted_bua" id="iuser_fixed_fee"
                                             style="display: <?= $display_permitted_bua ?>">
                                            <?= $form->field($model, 'permitted_bua')->textInput(['maxlength' => true])->label('Permitted BUA for Land (sft) <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>
                                        <div class="col-sm-4 permitted_gfa" id="iuser_fixed_fee_gfa"
                                             style="display: <?= $display_permitted_gfa ?>">
                                            <?= $form->field($model, 'permitted_gfa')->textInput(['maxlength' => true])->label('Permitted GFA for Land (sft) <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>
                                        <div class="col-sm-4" >
                                            <?= $form->field($model, 'nla')->textInput(['maxlength' => true])->label('Net Leasable Area'); ?>
                                        </div>

                                        <?php if (isset($valuation->building->city) && ($valuation->building->city == 3507)) { ?>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'length_of_fence')->textInput(['maxlength' => true])->label('Length Of Fence') ?>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </section>

                            <section class="valuation-form card card-outline card-primary">


                                <header class="card-header">
                                    <h2 class="card-title">Locations Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 property_placement" style="display: <?= $display_property_placement ?>">
                                            <?php
                                            echo $form->field($model, 'property_placement')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->propertyPlacementListArr,
                                                'options' => ['placeholder' => 'Select a Placement ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Property Placement <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 property_exposure" style="display: <?= $display_property_exposure ?>">
                                            <?php
                                            echo $form->field($model, 'property_exposure')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->propertyExposureListArr,
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Property Exposure <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 property_visibility" style="display: <?= $display_property_visibility ?>">
                                            <?php
                                            echo $form->field($model, 'property_visibility')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->propertyVisibilityListArr,
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Property Visibility <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 location_highway_drive" style="display: <?= $display_location_highway_drive ?>">
                                            <?php
                                            echo $form->field($model, 'location_highway_drive')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                            ])->label('Drive to Highway/Main Road <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 location_school_drive" style="display: <?= $display_location_school_drive ?>">
                                            <?php
                                            echo $form->field($model, 'location_school_drive')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                            ])->label('Drive to School/University <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 location_mall_drive" style="display: <?= $location_mall_drive ?>">
                                            <?php
                                            echo $form->field($model, 'location_mall_drive')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                            ])->label('Drive to Mall/Commercial Centrel <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 location_sea_drive" style="display: <?= $location_sea_drive ?>">
                                            <?php
                                            echo $form->field($model, 'location_sea_drive')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                            ])->label('Drive to Special Landmark like Sea, Beach etc. <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 location_park_drive" style="display: <?= $display_location_park_drive ?>">
                                            <?php
                                            echo $form->field($model, 'location_park_drive')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                            ])->label('Drive to Pool and Park <span class="text-danger">*</span>');
                                            ?>
                                        </div>

                                    </div>
                                </div>
                            </section>

                            <section class="valuation-form card card-outline card-primary">


                                <header class="card-header">
                                    <h2 class="card-title">Condition/Status Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 finished_status" style="display: <?= $display_finished_status ?>">
                                            <?php
                                            echo $form->field($model, 'finished_status')->widget(Select2::classname(), [
                                                'data' => array('Shell & Core' => 'Shell & Core', 'Fitted' => 'Fitted'),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Finished Status <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 estimated_age" style="display: <?= $display_estimated_age ?>">
                                            <?= $form->field($model, 'estimated_age')->textInput(['maxlength' => true])->label('Estimated Age <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <?php
                                            $months= array();
                                            for($i=0;$i<=12;$i++){
                                                $months[$i]=$i;
                                            }
                                            echo $form->field($model, 'estimated_age_month')->widget(Select2::classname(), [
                                                'data' => $months,
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>

                                        </div>
                                        <div class="col-sm-4 age_source" style="display: <?= $display_age_source ?>">
                                            <?php
                                            echo $form->field($model, 'age_source')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrAgeSource,
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Age Source');
                                            ?>

                                        </div>
                                        <div class="col-sm-4 estimated_remaining_life" style="display: <?= $display_estimated_remaining_life ?>">
                                            <?= $form->field($model, 'estimated_remaining_life')->textInput(['maxlength' => true, 'readonly' => true])->label('Estimated Remaining Life <span class="text-danger">*</span>') ?>
                                        </div>
                                        <div class="col-sm-4 property_condition" style="display: <?= $display_property_condition ?>">
                                            <?php
                                            echo $form->field($model, 'property_condition')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->propertyConditionListArr,
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Property Condition <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 property_defect" style="display: <?= $display_property_defect ?>">
                                            <?php
                                            echo $form->field($model, 'property_defect')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->propertyDefectsArr,
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Property Defect <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 occupancy_status" style="display: <?= $display_occupancy_status ?>">
                                            <?php
                                            echo $form->field($model, 'occupancy_status')->widget(Select2::classname(), [
                                                'data' => array('Owner Occupied' => 'Owner Occupied', 'Tenanted' => 'Tenanted', 'Vacant' => 'Vacant'),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Occupancy Status <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 acquisition_method">
                                            <?php
                                            echo $form->field($model, 'acquisition_method')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->acquisitionMethodsArr,
                                                'options' => ['placeholder' => 'Select a Placement ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Acquisition Method <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 completion_status">
                                            <?= $form->field($model, 'completion_status')->textInput(['maxlength' => true])->label('Completion Status (%)<span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>



                                        <?php if (isset($valuation->building->city) && ($valuation->building->city == 3507)) { ?>

                                            <div class="col-sm-4 inpect_date_label" id="inspection_date_id">

                                                <label for="html" id='inpect_date_label'>Date of Completion
                                                    Certificate</label><br>
                                                <?= $form->field($model, 'date_of_completion_certificate', [
                                                    'template' => '
                                                    {label}
                                                    <div class="input-group date" style="display: flex" id="completion_certificate_date" data-target-input="nearest">
                                                    {input}
                                                    <div class="input-group-append" data-target="#completion_certificate_date" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    </div>
                                                    {hint}{error}
                                                    '
                                                ])->textInput(['maxlength' => true])->label(false) ?>
                                            </div>

                                            <div class="col-sm-4 inpect_date_label" id="inspection_date_id">

                                                <label for="html" id='inpect_date_label'>Date of Building Permit</label><br>
                                                <?= $form->field($model, 'date_of_building_permit', [
                                                    'template' => '
                                                    {label}
                                                    <div class="input-group date" style="display: flex" id="date_of_building_permit" data-target-input="nearest">
                                                    {input}
                                                    <div class="input-group-append" data-target="#date_of_building_permit" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    </div>
                                                    {hint}{error}
                                                    '
                                                ])->textInput(['maxlength' => true])->label(false) ?>
                                            </div>

                                        <?php } ?>



                                    </div>
                                </div>
                            </section>

                            <section class="valuation-form card card-outline card-primary">


                                <header class="card-header">
                                    <h2 class="card-title">Development Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 development_type" style="display: <?= $display_development_type ?>">
                                            <?php
                                            echo $form->field($model, 'development_type')->widget(Select2::classname(), [
                                                'data' => array('Standard' => 'Standard', 'Non-Standard' => 'Non-Standard'),
                                                // 'options' => ['placeholder' => 'Select Development Type ...'],

                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Development Type <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 developer_id">
                                            <?php
                                            echo $form->field($model, 'developer_id')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(\app\models\Developers::find()->orderBy([
                                                    'title' => SORT_ASC,
                                                ])->all(), 'id', 'title'),
                                                // 'options' => ['placeholder' => 'Select Master Developer ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Master Developer <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 developer_id_property" style="display: <?= $display_developer_id_property ?>">
                                            <?php
                                            echo $form->field($model, 'developer_id_property')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(\app\models\Developers::find()->orderBy([
                                                    'title' => SORT_ASC,
                                                ])->all(), 'id', 'title'),
                                                'options' => ['placeholder' => 'Select Property Developer ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Property Developer  <span class="text-danger">*</span>');
                                            ;
                                            ?>

                                        </div>


                                    </div>
                                </div>
                            </section>

                            <section class="valuation-form card card-outline card-primary">


                                <header class="card-header">
                                    <h2 class="card-title">Building Configuration Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-4 number_of_building_on_land"  style="display: <?= $display_number_of_building_on_land ?>">
                                            <?php
                                            echo $form->field($model, 'number_of_building_on_land')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number Of Building On Land <span class="text-danger">*</span>');
                                            ?>

                                        </div>
                                        <div class="col-sm-4 number_of_floors" style="display: <?= $display_number_of_floors ?>">
                                            <?php
                                            echo $form->field($model, 'number_of_floors')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number Of Floors');
                                            ?>

                                        </div>
                                        <div class="col-sm-4 total_units" style="display: <?= $display_total_units ?>">
                                            <?= $form->field($model, 'total_units')->textInput(['type' => 'number', 'min' => 0, 'maxlength' => true])->label('Total Number of Units <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>
                                        <div class="col-sm-4 residential_units mb-3" style="display: <?= $display_residential_units ?>">
                                            <?= $form->field($model, 'residential_units')->textInput(['type' => 'number', 'min' => 0, 'maxlength' => true])->label('Total Number of Residential Units <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>
                                        <div class="col-sm-4 commercial_units" style="display: <?= $display_commercial_units ?>">
                                            <?= $form->field($model, 'commercial_units')->textInput(['type' => 'number', 'min' => 0, 'maxlength' => true])->label('Total Number of Commercial Units <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>
                                        <div class="col-sm-4 retail_units" style="display: <?= $display_retail_units ?>">
                                            <?= $form->field($model, 'retail_units')->textInput(['type' => 'number', 'min' => 0, 'maxlength' => true])->label('Total Number of Retail Units <span class="text-danger">*</span>') ?>
                                            <div class="help-block2"></div>
                                        </div>


                                        <!-- <div class="col-sm-4">
                                            <?= $form->field($model, 'full_building_floors')->textInput(['maxlength' => true]) ?>
                                        </div> -->
                                        <!--<div class="col-sm-4 full_building_floors" style="display: <?/*= $display_full_building_floors */?>">
                                            <?php
/*                                            echo $form->field($model, 'full_building_floors')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Full Building Floors');
                                            */?>

                                        </div>-->
                                        <div class="col-sm-4 number_of_floors" style="display: <?= $display_number_of_floors ?>">
                                            <?php
                                            echo $form->field($model, 'number_of_parking_floors')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number Of Parking Floors');
                                            ?>

                                        </div>
                                        <div class="col-sm-4 number_of_basement" style="display: <?= $display_number_of_basement ?>">
                                            <?php
                                            echo $form->field($model, 'number_of_basement')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Basements <span class="text-danger">*</span>');
                                            ?>

                                        </div>
                                        <div class="col-sm-4 number_of_mezannines" style="display: <?= $display_number_of_mezannines ?>">
                                            <?php
                                            echo $form->field($model, 'number_of_mezannines')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Mezannines <span class="text-danger">*</span>');
                                            ?>

                                        </div>
                                        <div class="col-sm-4 parking_floors" style="display: <?= $display_parking_floors ?>">
                                            <?php
                                            echo $form->field($model, 'parking_floors')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number Parking Spaces <span class="text-danger">*</span>');
                                            ?>
                                            <div class="help-block2"></div>
                                        </div>


                                        <!-- <div class="col-sm-4">
                                        <? /*= $form->field($model, 'parking_floors')->textInput(['maxlength' => true])->label("Parking Space") */?>
                                    </div>-->

                                        <div class="col-sm-4 gated_community" style="display: <?= $display_gated_community ?>">
                                            <?php
                                            echo $form->field($model, 'gated_community')->widget(Select2::classname(), [
                                                'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                // 'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Gated Community');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_party_hall" style="display: <?= $display_no_party_hall ?>">
                                            <?= $form->field($model, 'no_party_hall')->textInput(['type' => 'number', 'min' => 0, 'maxlength' => true])->label('Number of Party Halls') ?>
                                        </div>
                                        <div class="col-sm-4 no_health_club_spa" style="display: <?= $display_no_health_club_spa ?>">
                                            <?= $form->field($model, 'no_health_club_spa')->textInput(['type' => 'number', 'min' => 0, 'maxlength' => true])->label('Number of Health Clubs/Spa') ?>
                                        </div>


                                        <!-- <div class="col-sm-4 number_of_levels">
                                            <?php /*
                             echo $form->field($model, 'number_of_levels')->widget(Select2::classname(), [
                                 'data' => Yii::$app->appHelperFunctions->listingLevelsListArr,
                                 'options' => ['placeholder' => 'Select a Level ...'],
                                 'pluginOptions' => [
                                     'allowClear' => true
                                 ],
                             ]); */
                                        ?>

                                        </div> -->
                                        <div class="col-sm-4 pool" style="display: <?= $display_pool ?>">
                                            <?php
                                            echo $form->field($model, 'pool')->widget(Select2::classname(), [
                                                'data' => array('Yes' => 'Yes', 'No' => 'No'),

                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                        <!-- <div class="col-sm-4 landscaping">
                                            <?php /*
                             echo $form->field($model, 'landscaping')->widget(Select2::classname(), [
                                 'data' => array('Yes' => 'Yes', 'No' => 'No', 'Semi-Landscape' => 'Semi-Landscape'),
                                 'pluginOptions' => [
                                     'allowClear' => true
                                 ],
                             ]); */
                                        ?>
                                        </div> -->

                                    </div>
                                </div>
                            </section>

                            <section class="valuation-form card card-outline card-primary">


                                <header class="card-header">
                                    <h2 class="card-title">MEP and HVAC Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-4 no_elevators" style="display: <?= $display_no_elevators ?>">
                                            <?= $form->field($model, 'no_elevators')->textInput(['type' => 'number', 'min' => 0, 'maxlength' => true])->label('Number of Elevators') ?>
                                        </div>

                                        <div class="col-sm-4 ac_type" style="display: <?= $display_ac_type ?>">

                                            <?php

                                            $model->ac_type = explode(',', ($model->ac_type <> null) ? $model->ac_type : "");
                                            echo $form->field($model, 'ac_type')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->acTypesArr,
                                                'options' => ['placeholder' => 'Select a AC Type ...'],
                                                'pluginOptions' => [
                                                    'placeholder' => 'Select a AC Type',
                                                    'multiple' => true,
                                                    'allowClear' => true
                                                ],
                                            ])->label('AC Type <span class="text-danger">*</span>');
                                            ?>
                                            <div class="help-block2"></div>


                                        </div>
                                        <div class="col-sm-4 utilities_connected" style="display: <?= $display_utilities_connected ?>">
                                            <?php
                                            echo $form->field($model, 'utilities_connected')->widget(Select2::classname(), [
                                                'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Utilities Connected <span class="text-danger">*</span>');
                                            ?>
                                        </div>




                                    </div>
                                </div>
                            </section>

                            <section class="valuation-form card card-outline card-primary">


                                <header class="card-header">
                                    <h2 class="card-title">Kitchen Equipment Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 fridge" style="display: <?= $display_fridge ?>">
                                            <?php
                                            echo $form->field($model, 'fridge')->widget(Select2::classname(), [
                                                'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped', 'standard' => 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Fridges <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 oven" style="display: <?= $display_oven ?>">
                                            <?php
                                            echo $form->field($model, 'oven')->widget(Select2::classname(), [
                                                'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped', 'standard' => 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Ovens <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 washing_machine" style="display: <?= $display_washing_machine ?>">
                                            <?php
                                            echo $form->field($model, 'washing_machine')->widget(Select2::classname(), [
                                                'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped', 'standard' => 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Washing Machine <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 dish_washer" style="display: <?= $display_dish_washer ?>">
                                            <?php
                                            echo $form->field($model, 'dish_washer')->widget(Select2::classname(), [
                                                'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Dish Washer <span class="text-danger">*</span>');
                                            ?>
                                        </div>

                                        <!-- <div class="col-sm-4 cooker" style="display: <?= $display_cooker ?>">
                                            <?php
                                        echo $form->field($model, 'cooker')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped', 'standard' => 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                        </div>
                                        
                                        <div class="col-sm-4 furnished" style="display: <?= $display_furnished ?>">
                                            <?php
                                        echo $form->field($model, 'furnished')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes – we are valuing the unit as unfurnished', 'standard' => 'Yes – we are valuing the unit as standard furnished', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                        </div>
                                        <div class="col-sm-4 white_goods"  style="display: <?= $display_white_goods ?>">
                                            <?php
                                        echo $form->field($model, 'white_goods')->widget(Select2::classname(), [
                                            'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped', 'standard' => 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                        </div> -->

                                    </div>
                                </div>
                            </section>

                            <section class="valuation-form card card-outline card-primary">


                                <header class="card-header">
                                    <h2 class="card-title">Units Types Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-6 no_studios mb-3" style="display: <?= $display_no_studios ?>">
                                            <?php
                                            echo $form->field($model, 'no_studios')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Studio <span class="text-danger">*</span>');
                                            ?>
                                            <div class="help-block2"></div>

                                        </div>
                                        <div class="col-sm-6" style="display: <?= $display_no_studios ?>">
                                            <?= $form->field($model, 'no_studios_nla')->textInput(['maxlength' => true])->label('Net Leasable Area'); ?>
                                        </div>
                                        <div class="col-sm-6 no_one_bedrooms" style="display: <?= $display_no_one_bedrooms ?>">
                                            <?php
                                            echo $form->field($model, 'no_one_bedrooms')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of 1 Bedroom <span class="text-danger">*</span>');
                                            ?>
                                            <div class="help-block2"></div>

                                        </div>
                                        <div class="col-sm-6" style="display: <?= $display_no_one_bedrooms ?>">
                                            <?= $form->field($model, 'no_one_bedrooms_nla')->textInput(['maxlength' => true])->label('Net Leasable Area'); ?>
                                        </div>
                                        <div class="col-sm-6 no_two_bedrooms" style="display: <?= $display_no_two_bedrooms ?>">
                                            <?php
                                            echo $form->field($model, 'no_two_bedrooms')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of 2 Bedrooms <span class="text-danger">*</span>');
                                            ?>
                                            <div class="help-block2"></div>

                                        </div>
                                        <div class="col-sm-6" style="display: <?= $display_no_two_bedrooms ?>">
                                            <?= $form->field($model, 'no_two_bedrooms_nla')->textInput(['maxlength' => true])->label('Net Leasable Area'); ?>
                                        </div>
                                        <div class="col-sm-6 no_three_bedrooms" style="display: <?= $display_no_three_bedrooms ?>">
                                            <?php
                                            echo $form->field($model, 'no_three_bedrooms')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of 3 Bedrooms <span class="text-danger">*</span>');
                                            ?>
                                            <div class="help-block2"></div>

                                        </div>
                                        <div class="col-sm-6" style="display: <?= $display_no_three_bedrooms ?>">
                                            <?= $form->field($model, 'no_three_bedrooms_nla')->textInput(['maxlength' => true])->label('Net Leasable Area'); ?>
                                        </div>
                                        <div class="col-sm-6 no_four_bedrooms" style="display: <?= $display_no_four_bedrooms ?>">
                                            <?php
                                            echo $form->field($model, 'no_four_bedrooms')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of 4 Bedrooms <span class="text-danger">*</span>');
                                            ?>
                                            <div class="help-block2"></div>

                                        </div>
                                        <div class="col-sm-6" style="display: <?= $display_no_four_bedrooms ?>">
                                            <?= $form->field($model, 'no_four_bedrooms_nla')->textInput(['maxlength' => true])->label('Net Leasable Area'); ?>
                                        </div>
                                        <div class="col-sm-6 no_penthouse" style="display: <?= $display_no_penthouse ?>">
                                            <?php
                                            echo $form->field($model, 'no_penthouse')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Penthouse <span class="text-danger">*</span>');
                                            ?>
                                            <div class="help-block2"></div>

                                        </div>
                                        <div class="col-sm-6" style="display: <?= $display_no_penthouse ?>">
                                            <?= $form->field($model, 'no_penthouse_nla')->textInput(['maxlength' => true])->label('Net Leasable Area'); ?>
                                        </div>

                                        <div class="col-sm-6 no_of_shops" style="display: <?= $display_no_penthouse ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_shops')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Shops <span class="text-danger">*</span>');
                                            ?>
                                            <div class="help-block2"></div>

                                        </div>
                                        <div class="col-sm-6" style="display: <?= $display_no_penthouse ?>">
                                            <?= $form->field($model, 'no_of_shops_nla')->textInput(['maxlength' => true])->label('Net Leasable Area'); ?>
                                        </div>

                                        <div class="col-sm-6 no_of_offices" style="display: <?= $display_no_penthouse ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_offices')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoThousandNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Offices <span class="text-danger">*</span>');
                                            ?>
                                            <div class="help-block2"></div>

                                        </div>
                                        <div class="col-sm-6" style="display: <?= $display_no_penthouse ?>">
                                            <?= $form->field($model, 'no_of_offices_nla')->textInput(['maxlength' => true])->label('Net Leasable Area'); ?>
                                        </div>



                                    </div>
                                </div>
                            </section>

                            <section class="valuation-form card card-outline card-primary">


                                <header class="card-header">
                                    <h2 class="card-title">Unit/Building External Facilites Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 number_of_bbq_areas" style="display: <?= $display_number_of_bbq_areas ?>">
                                            <?php
                                            echo $form->field($model, 'number_of_bbq_areas')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of BBQ Areas <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_childrens_play_area" style="display: <?= $display_no_childrens_play_area ?>">
                                            <?php
                                            echo $form->field($model, 'no_childrens_play_area')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Children Play Area <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_swimming_pool" style="display: <?= $display_no_of_swimming_pool ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_swimming_pool')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Swimming Pool <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_jacuzzi" style="display: <?= $display_no_of_jacuzzi ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_jacuzzi')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Jacuzzi <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_gym_room" style="display: <?= $display_no_of_gym_room ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_gym_room')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Gym Rooms');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_school" style="display: <?= $display_no_of_school ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_school')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Schools <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_clinic" style="display: <?= $display_no_of_clinic ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_clinic')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Clinics <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_sports_court" style="display: <?= $display_no_of_sports_court ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_sports_court')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Sports Courts <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_masjid" style="display: <?= $display_no_of_masjid ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_masjid')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Masjids <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_community_center" style="display: <?= $display_no_of_community_center ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_community_center')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                                'options' => ['placeholder' => 'Select ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ])->label('Number of Community Centers <span class="text-danger">*</span>');
                                            ?>
                                        </div>



                                    </div>

                                </div>
                            </section>


                            <?php /* ?>
                       <section class="valuation-form card card-outline card-primary">


                           <header class="card-header">
                               <h2 class="card-title">Unit/Building External Facilities Details</h2>
                           </header>
                           <div class="card-body">
                               <div class="row">
                                   <div class="col-sm-12">
                                       <?php
                                       /*  echo "<pre>";
                                         print_r($model->other_facilities);
                                         die;
                                         echo $model->other_facilities;
                                         die;
                                         if($model->other_facilities <> null) {
                                             $model->other_facilities = explode(',', ($model->other_facilities <> null) ? $model->other_facilities : "");
                                         }*//*
                                       $model->other_facilities = explode(',', ($model->other_facilities <> null) ? $model->other_facilities : "");
                                       echo $form->field($model, 'other_facilities')->widget(Select2::classname(), [
                                           'data' => ArrayHelper::map(\app\models\OtherFacilities::find()->orderBy([
                                               'title' => SORT_ASC,
                                           ])->all(), 'id', 'title'),
                                           'options' => ['placeholder' => 'Select a Facilities ...'],
                                           'pluginOptions' => [
                                               'placeholder' => 'Select a Facilities',
                                               'multiple' => true,
                                               'allowClear' => true
                                           ],
                                       ]);
                                       ?>
                                   </div>


                               </div>
                           </div>
                       </section>
                       <?php */?>

                            <section class="valuation-form card card-outline card-primary">
                                <header class="card-header">
                                    <h2 class="card-title">Extension Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?php
                                            echo $form->field($model, 'balcony_covered_with_roof')->widget(Select2::classname(), [
                                                'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?php
                                            echo $form->field($model, 'extension_permision_document')->widget(Select2::classname(), [
                                                'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?php
                                            echo $form->field($model, 'extension_completion_document')->widget(Select2::classname(), [
                                                'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>
                                        </div>

                                    </div>
                                </div>
                            </section>



                            <section class="card card-outline card-info">
                                <header class="card-header">
                                    <h2 class="card-title">View  Details</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 view_community" style="display: <?= $display_view_community ?>">
                                            <?php
                                            echo $form->field($model, 'view_community')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->viewCommunityAttributesValue,
                                            ])->label('Community View  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 view_pool" style="display: <?= $display_view_pool ?>">
                                            <?php
                                            echo $form->field($model, 'view_pool')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                            ])->label('Pool / Fountain View  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 view_burj" style="display: <?= $display_view_burj ?>">
                                            <?php
                                            echo $form->field($model, 'view_burj')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                            ])->label('Burj View  <span class="text-danger">*</span>');
                                            ?>
                                        </div>

                                        <div class="col-sm-4 view_sea" style="display: <?= $display_view_sea ?>">
                                            <?php
                                            echo $form->field($model, 'view_sea')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                            ])->label('Sea View  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 view_marina" style="display: <?= $display_view_marina ?>">
                                            <?php
                                            echo $form->field($model, 'view_marina')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                            ])->label('Marina View  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 view_mountains" style="display: <?= $display_view_mountains ?>">
                                            <?php
                                            echo $form->field($model, 'view_mountains')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                            ])->label('Mountains View  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 view_lake" style="display: <?= $display_view_lake ?>">
                                            <?php
                                            echo $form->field($model, 'view_lake')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                            ])->label('Lake View  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 view_golf_course " style="display: <?= $display_view_golf_course ?>">
                                            <?php
                                            echo $form->field($model, 'view_golf_course')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                            ])->label('Golf Course View  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 view_park" style="display: <?= $display_view_park ?>">
                                            <?php
                                            echo $form->field($model, 'view_park')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                            ])->label('Park View  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 view_special" style="display: <?= $display_view_special ?>">
                                            <?php
                                            echo $form->field($model, 'view_special')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                            ])->label('Special View  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                    </div>

                                </div>

                            </section>

                            <section class="card card-outline card-info">
                                <header class="card-header">
                                    <h2 class="card-title">Other Income Areas</h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 no_of_restaurants" style="display: <?= $display_no_of_restaurants ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_restaurants')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                            ])->label('Number of Restaurants  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_atm_machines" style="display: <?= $display_no_of_atm_machines ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_atm_machines')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                            ])->label('Number of ATM Machines  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_coffee_shops" style="display: <?= $display_no_of_coffee_shops ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_coffee_shops')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                            ])->label('Number of Coffee Shops  <span class="text-danger">*</span>');
                                            ?>
                                        </div>

                                        <div class="col-sm-4 no_of_signboards" style="display: <?= $display_no_of_signboards ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_signboards')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                            ])->label('Number of Signboards  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_bars" style="display: <?= $display_no_of_bars ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_bars')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                            ])->label('Number of Bars  <span class="text-danger">*</span>');
                                            ?>
                                        </div>
                                        <div class="col-sm-4 no_of_night_clubs" style="display: <?= $display_no_of_night_clubs ?>">
                                            <?php
                                            echo $form->field($model, 'no_of_night_clubs')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTwoHundredNumber,
                                            ])->label('Number of Night Clubs  <span class="text-danger">*</span>');
                                            ?>
                                        </div>

                                    </div>

                                </div>

                            </section>


                            <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title">Custom Configration Information</h2>
                                </header>
                                <div class="card-body">

                                    <table id="attachment"
                                           class="table table-striped table-bordered table-hover images-table">
                                        <thead>
                                        <tr>
                                            <td class="text-left">Name</td>
                                            <td class="text-left">Quantity</td>
                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $row = 0; ?>
                                        <?php foreach ($model->customAttachements as $attachment) { ?>
                                            <tr id="image-row-attachment-<?php echo $row; ?>">

                                                <td>
                                                    <input type="text" class="form-control"
                                                           name="InspectProperty[customAttachments][<?= $row ?>][name]"
                                                           value="<?= $attachment->name ?>" placeholder="Name" required />
                                                </td>

                                                <td>
                                                    <input type="number" class="form-control"
                                                           name="InspectProperty[customAttachments][<?= $row ?>][quantity]"
                                                           value="<?= $attachment->quantity ?>" placeholder="Quantity"
                                                           required />
                                                </td>
                                                <input type="hidden" class="form-control"
                                                       name="InspectProperty[customAttachments][<?= $row ?>][id]"
                                                       value="<?= $attachment->id ?>" placeholder="Name" required />

                                                <td class="text-left">
                                                    <button type="button"
                                                            onclick="deleteRow('-attachment-<?= $row ?>', '<?= $attachment->id; ?>', 'attachment')"
                                                            data-toggle="tooltip" title="You want to delete Attachment"
                                                            class="btn btn-danger"><i
                                                                class="fa fa-minus-circle"></i></button>
                                                </td>
                                            </tr>
                                            <?php $row++; ?>
                                        <?php } ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td class="text-left">
                                                <button type="button" onclick="addAttachment();"
                                                        data-toggle="tooltip" title="Add" class="btn btn-primary"><i
                                                            class="fa fa-plus-circle"></i></button>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </section>
                        </section>

                    </div>


                    <?php
                    $allow_array = array(1, 21, 38);
                    if (isset($model->created_by) && ($model->created_by <> null)) {
                        $check_current_id = $model->created_by;
                    } else {
                        $check_current_id = Yii::$app->user->identity->id;
                    }
                    if (($key = array_search($check_current_id, $allow_array)) !== false) {
                        unset($allow_array[$key]);
                    }
                    if (in_array(Yii::$app->user->identity->id, $allow_array) || Yii::$app->user->identity->id == 1) {
                        echo StatusVerified::widget(['model' => $model, 'form' => $form]);
                    }
                    ?>

                </div>
                <div class="card-footer">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                    <?php if ($model->email_status <> 1) { ?>
                        <button type="button" name="button" class="sav-btn btn btn-primary">Send Email</button>
                    <?php } ?>

                    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                    <?php
                    if ($model <> null && $model->id <> null) {
                        echo Yii::$app->appHelperFunctions->getLastActionHitory([
                            'model_id' => $model->id,
                            'model_name' => 'app\models\InspectProperty',
                        ]);
                    }
                    ?>
                </div>
                <!-- <div class="card-footer">
                                <? /*= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) */?>
                                <? /*= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) */?>
                            </div>-->
                <?php ActiveForm::end(); ?>

            </div>

        </div>
    </div>
</div>
</div>
<!-- /.card -->
</div>




<script type="text/javascript">
    var row = <?= $row ?>;
    function addAttachment() {

        html = '<tr id="image-row' + row + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="InspectProperty[customAttachments][' + row + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="number" class="form-control"  name="InspectProperty[customAttachments][' + row + '][quantity]" value="" placeholder="Quantity" required />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment tbody').append(html);

        row++;
    }

    function deleteRow(rowId, docID, type) {


        var url = '<?= \yii\helpers\Url::to('valuation/remove-attachment'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if (response.status == 'exist') {
                            swal("Warning!", response.message, "warning");
                        } else {
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
</script>