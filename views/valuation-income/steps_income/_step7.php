<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enter Cost Details');
$cardTitle = Yii::t('app', 'Enter Cost Details:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_7/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$this->registerJs('

$("#listingstransactions-original_purchase_date,#listingstransactions-original_land_purchase_date,#listingstransactions-current_transaction_price_date,#listingstransactions-land_lease_start_date,#listingstransactions-land_lease_expiry_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

$(document).on("click", function (e) {
    if ($(e.target).closest("#listingstransactions-original_purchase_date,#listingstransactions-original_land_purchase_date,#listingstransactions-current_transaction_price_date,#listingstransactions-land_lease_start_date,#listingstransactions-land_lease_expiry_date").length === 0) {
        $("#listingstransactions-original_purchase_date,#listingstransactions-original_land_purchase_date,#listingstransactions-current_transaction_price_date,#listingstransactions-land_lease_start_date,#listingstransactions-land_lease_expiry_date").datetimepicker("hide");
    }
});


$(document).ready(function () {
    
    
    makeMandatoryByYesNo("costdetails-original_purchase_price_available", "oppdm");
    makeMandatoryByYesNo("costdetails-original_land_purchase_price_available", "olppdm");
    makeMandatoryByYesNo("costdetails-land_lease_cost_available", "llcm");
    makeMandatoryByYesNo("costdetails-land_lease_cost_available", "lledm");
    makeMandatoryByYesNo("costdetails-land_lease_cost_available", "tnlym");
    makeMandatoryByYesNo("costdetails-current_transaction_price_available", "ctpm");
    makeMandatoryByYesNo("costdetails-current_transaction_price_available", "ctpdm");

    function makeMandatoryByYesNo(fieldId, fieldStarClass) {
        $("#" + fieldId).on("change", function () {
            if ($(this).val() === "Yes") {
                $("." + fieldStarClass).show();
            } else {
                $("." + fieldStarClass).hide();
            }
        });
    
        if ($("#" + fieldId).val() === "Yes") {
            $("." + fieldStarClass).show();
        } else {
            $("." + fieldStarClass).hide();
        }
    }

});


');



$property_id = $valuation->property_id;


// to display fields based on property type
$lands = Yii::$app->appHelperFunctions->landIdArr;
$apartments = array(1);
$villas = Yii::$app->appHelperFunctions->villaIdArr;
$townhouse = array(6, 11);
$office = array(28);
$shop = array(17);
$warehouse = array(24, 78);

$landIds = [];
foreach ($lands as $land) { $landIds[] = $land->id; }

$villaIds = [];
foreach ($villas as $villa) {  $villaIds[] = $villa->id; }

$display_acquisition_method =  (in_array($property_id, array_merge($apartments, $office, $shop, $warehouse))) ? "none" : "block";
$display_land_acquisition_method =  (in_array($property_id, array_merge($apartments, $office, $shop, $warehouse))) ? "none" : "block";
$display_land_lease_cost_available =  (in_array($property_id, array_merge($apartments, $office, $shop))) ? "none" : "block";
$display_land_lease_costs =  (in_array($property_id, array_merge($apartments, $office, $shop))) ? "none" : "block";
$display_land_lease_expiry_date =  (in_array($property_id, array_merge($apartments, $office, $shop))) ? "none" : "block";
$display_total_no_of_lease_years =  (in_array($property_id, array_merge($apartments, $office, $shop))) ? "none" : "block";




?>




<style>
    .datepicker-days .table>thead>tr>th,
    .table>tbody>tr>th,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>tbody>tr>td,
    .table>tfoot>tr>td {
        padding: inherit !important;
    }
    .help-block2 {
        color: red;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -14px;
        font-family: inherit;
    }
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                    aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 7]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                        aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title">
                                    <?= $cardTitle ?>
                                </h2>
                            </header>
                            <div class="card-body">

                                <section class="valuation-form card card-outline card-primary">


                                    <header class="card-header">
                                        <h2 class="card-title">Original Property Purchase Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'original_purchase_price_available')->widget(Select2::classname(), [
                                                    'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Original Purchase Price Available? See TD/SPA');
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'original_purchase_price')->textInput(['maxlength' => true])
                                                    ->label('Original Purchase Price ') ?>
                                            </div>

                                            <div class="col-sm-4 original_purchase_date" id="original_purchase_date_id">
                                                <?= $form->field($model, 'original_purchase_date', [
                                                    'template' => '
                                                {label}
                                                <div class="input-group date" style="display: flex" id="listingstransactions-original_purchase_date" data-target-input="nearest">
                                                {input}
                                                <div class="input-group-append" data-target="#listingstransactions-original_purchase_date" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                                </div>
                                                {hint}{error}
                                                '
                                                ])->textInput(['maxlength' => true])->label('Original Purchase Date  <span class="text-danger oppdm">*</span>') ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'source_of_original_purchase_details')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->costSource,
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Source of Original Purchase Details');
                                                ?>

                                            </div>

                                            <div class="col-sm-4 acquisition_method"  style="display: <?= $display_acquisition_method ?>">
                                                <?php
                                                echo $form->field($model, 'acquisition_method')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->acquisitionMethodsArr,
                                                    'options' => ['placeholder' => 'Select a Placement ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Acquisition Method <span class="text-danger">*</span>');
                                                ?>
                                            </div>
                                        </div>
                                </section>

                                <section class="valuation-form card card-outline card-primary">


                                    <header class="card-header">
                                        <h2 class="card-title">Original Land Purchase Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'original_land_purchase_price_available')->widget(Select2::classname(), [
                                                    'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Original Land Purchase Price Available? See TD/SPA');
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'original_land_purchase_price')->textInput(['maxlength' => true])
                                                    ->label('Original Land Purchase Price ') ?>
                                            </div>

                                            <div class="col-sm-4 original_land_purchase_date" id="original_land_purchase_date_id">
                                                <?= $form->field($model, 'original_land_purchase_date', [
                                                    'template' => '
                                                {label}
                                                <div class="input-group date" style="display: flex" id="listingstransactions-original_land_purchase_date" data-target-input="nearest">
                                                {input}
                                                <div class="input-group-append" data-target="#listingstransactions-original_land_purchase_date" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                                </div>
                                                {hint}{error}
                                                '
                                                ])->textInput(['maxlength' => true])->label('Original Land Purchase Date <span class="text-danger olppdm">*</span>') ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'source_of_original_land_purchase_details')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->costSource,
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Source of Original Land Purchase Details');
                                                ?>

                                            </div>

                                            <div class="col-sm-4 land_acquisition_method" style="display: <?= $display_land_acquisition_method ?>">
                                                <?php
                                                echo $form->field($model, 'land_acquisition_method')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->acquisitionMethodsArr,
                                                    'options' => ['placeholder' => 'Select a Placement ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Land Acquisition Method <span class="text-danger">*</span>');
                                                ?>
                                            </div>
                                        </div>
                                </section>

                                <section class="valuation-form card card-outline card-primary">


                                    <header class="card-header">
                                        <h2 class="card-title">Land Lease Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4 land_lease_cost_available" style="display: <?= $display_land_lease_cost_available ?>">
                                                <?php
                                                echo $form->field($model, 'land_lease_cost_available')->widget(Select2::classname(), [
                                                    'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Land Lease Cost available?');
                                                ?>
                                            </div>
                                            <div class="col-sm-4 land_lease_costs" style="display: <?= $display_land_lease_costs ?>">
                                                <?= $form->field($model, 'land_lease_costs')->textInput(['maxlength' => true])
                                                    ->label('Land Lease Cost <span class="text-danger llcm">*</span>') ?>
                                                    <div class="help-block2"></div>

                                            </div>
                                            <!-- <div class="col-sm-4" id="land_lease_start_date_id">
                                                <?= $form->field($model, 'land_lease_start_date', [
                                                    'template' => '
                                                    {label}
                                                    <div class="input-group date" style="display: flex" id="listingstransactions-land_lease_start_date" data-target-input="nearest">
                                                    {input}
                                                    <div class="input-group-append" data-target="#listingstransactions-land_lease_start_date" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    </div>
                                                    {hint}{error}
                                                    '
                                                ])->textInput(['maxlength' => true]) ?>

                                            </div> -->
                                            <div class="col-sm-4 land_lease_expiry_date" id="land_lease_expiry_date_id" style="display: <?= $display_land_lease_expiry_date ?>">
                                                <?= $form->field($model, 'land_lease_expiry_date', [
                                                    'template' => '
                                                    {label}
                                                    <div class="input-group date" style="display: flex" id="listingstransactions-land_lease_expiry_date" data-target-input="nearest">
                                                    {input}
                                                    <div class="input-group-append" data-target="#listingstransactions-land_lease_expiry_date" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    </div>
                                                    {hint}{error}
                                                    '
                                                ])->textInput(['maxlength' => true])->label('Land Lease Expiry Date  <span class="text-danger lledm">*</span>') ?>

                                            </div>
                                            <div class="col-sm-4 total_no_of_lease_years" style="display: <?= $display_total_no_of_lease_years ?>">
                                                <?= $form->field($model, 'total_no_of_lease_years')->textInput(['type' => 'number', 'min' => 0, 'maxlength' => true])
                                                    ->label('Total Number of Lease Years  <span class="text-danger tnlym">*</span>') ?>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section class="valuation-form card card-outline card-primary">


                                    <header class="card-header">
                                        <h2 class="card-title">Going Transation Price Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'current_transaction_price_available')->widget(Select2::classname(), [
                                                    'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Current Transaction Price Available ? (See MOU/SPA)');
                                                ?>
                                            </div>
                                            <div class="col-sm-4 transaction_price">
                                                <?= $form->field($model, 'transaction_price')->textInput(['maxlength' => true])
                                                    ->label('Current Transaction Price (MOU/SPA)  <span class="text-danger ctpm">*</span>'); ?>
                                                    <div class="help-block2"></div>
                                            </div>


                                            <div class="col-sm-4 current_transaction_price_date" id="current_transaction_price_date_id">
                                                <?= $form->field($model, 'current_transaction_price_date', [
                                                    'template' => '
                                                        {label}
                                                        <div class="input-group date" style="display: flex" id="listingstransactions-current_transaction_price_date" data-target-input="nearest">
                                                        {input}
                                                        <div class="input-group-append" data-target="#listingstransactions-current_transaction_price_date" data-toggle="datetimepicker">
                                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                        </div>
                                                        {hint}{error}
                                                        '
                                                ])->textInput(['maxlength' => true])->label('Current Transaction Price Date  <span class="text-danger ctpdm">*</span>'); ?>

                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'source_of_transaction_details')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->costSource,
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Source of Transaction Details');
                                                ?>

                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section class="valuation-form card card-outline card-primary">


                                    <header class="card-header">
                                        <h2 class="card-title">Property Buidling Costs</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'consultants_agreement_costs')->textInput(['maxlength' => true])->label('Consultants Agreement Costs') ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'contracting_costs')->textInput(['maxlength' => true])->label('Contracting Costs') ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'governmnet_authority_costs')->textInput(['maxlength' => true])->label('Governmnet Authority Costs') ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'dewa_sewa_fewa_costs')->textInput(['maxlength' => true])->label('DEWA/SEWA/FEWA Costs') ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'developers_master_costs')->textInput(['maxlength' => true])->label('Developers Master Costs') ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'pool_price')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'landscape_price')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'white_goods_price')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                            </div>

                                             <div class="col-sm-4">
                                                <?php
                                                if ($model->lands_price <> null) {

                                                } else {
                                                    $model->lands_price = $valuation->building->communities->land_price;
                                                }
                                                ?>
                                                <?= $form->field($model, 'lands_price')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'parking_price')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <!-- <div class="col-sm-4">
                                                <?= $form->field($model, 'utilities_connected_price')->textInput(['maxlength' => true]) ?>
                                            </div> -->
                                            <!-- <div class="col-sm-4">
                                                <?= $form->field($model, 'number_of_years')->textInput(['maxlength' => true]) ?>
                                            </div> -->

                                        </div>
                                    </div>
                                </section>

                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>