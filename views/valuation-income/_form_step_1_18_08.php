<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ValuationFormAsset;

ValuationFormAsset::register($this);
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */

if ($model->email_status !=1) {
    $AlertText='Data and Valuation Status will be saved and Email will be sent once?';
}
else{
    $AlertText='Only Data will be saved?';
}

$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "'.Yii::t('app',$AlertText).'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
      console.log("Hello 123")
      $("#w0").unbind("submit").submit();
    }
});
});


var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});

$("body").on("click", ".sav-btn", function (e) {
swal({
title: "'.Yii::t('app','Valuation status will be saved and Email will be sent to Clients ?').'",
html: "'.Yii::t('app','').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({

  url: "'.Url::to(['valuation/receive-valuation-email','id'=>$model->id,'step'=>1]).'",
  dataType: "html",
  type: "POST",
  });
    }
});
});




    $("body").on("change", ".client-cls", function () {
           console.log($(this).val());
           if($(this).val() == 1){
            $("#client_fixed_fee").show();
                   }else{
                    $("#client_fixed_fee").hide();
                   }
        $(".appentWali-row").remove();
        _this = $(this);
        GetOtherInstructingPersons(_this);
    });


    var other_instructing_person_id =  "'.$model->other_instructing_person.'";

    function GetOtherInstructingPersons(_this){
        var keyword = "Valuation";
        var client_id = _this.val();      
        if (client_id!= "") {
            var data = {client_id:client_id, other_instructing_person_id:other_instructing_person_id, keyword:keyword};
            $.ajax({
                url: "'.Url::to(['client/other-instructing-persons']).'",
                data: data,
                method: "post",
                dataType: "html",
                success: function(data) {
//                    console.log(data);
                    $(".parent-row-ff").append(data);
                },
                error: bbAlert
            });
        }
        else{
            $(".appentWali-row").remove();
        }
         if (client_id!= "") {
         var data = {client_id:client_id};
            $.ajax({
                url: "'.Url::to(['client/clientinvoice']).'",
                data: data,
                method: "post",
                dataType: "html",
                success: function(response) {
//                    console.log(response);
                   if(response == "1"){
                   $("#client_invoice_type").show();
                   }else{
                    $("#client_invoice_type").hide();
                   }
                },
                error: bbAlert
            });
        }
        else{
            $(".appentWali-row").remove();
        }

    }
   
    $(".client-cls").trigger("change");



');

?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }

    .fade {
        opacity: 1;
    }

    .nav-tabs.flex-column .nav-link.active {
        background-color: #007bff;
        color: white;
        font-weight: bold;
    }
</style>

<section class="valuation-form card card-outline card-primary">

    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle ?></h2>
    </header>
    <div class="card-body">


        <div class="row parent-row-ff">
            <div class="col-sm-4">
                <?= $form->field($model, 'reference_number')->textInput(['maxlength' => true, 'value' => ($model->reference_number <> null) ? $model->reference_number : Yii::$app->appHelperFunctions->uniqueReference]) ?>
            </div>

            <div class="col-sm-4">


                <?php
                echo $form->field($model, 'client_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Company::find()
                        ->where(['status' => 1])
                        ->andWhere(['allow_for_valuation'=>1])
                        // ->andWhere([
                        //     'or',
                        //     ['data_type' => 0],
                        //     ['data_type' => null],
                        // ])
                        ->orderBy(['title' => SORT_ASC,])
                        ->all(), 'id', 'title'),

                    'options' => ['placeholder' => 'Select a Client ...', 'class'=> 'client-cls'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Client Name');
                ?>



                <?php
               /* echo $form->field($model, 'client_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Company::find()->where(['status' => 1])->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select a Client ...', 'class'=> 'client-cls'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Client Name');*/
                ?>

            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'client_reference')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'other_intended_users')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'client_name_passport')->textInput(['maxlength' => true])->label('Customer Name') ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'no_of_owners')->textInput(['maxlength' => true]) ?>
            </div>
            <?php

            if (in_array(Yii::$app->user->identity->permission_group_id, [12,4,14,1,5,7,10,22]))
            {
                ?>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'service_officer_name')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->staffMemberListArr,
                        'options' => ['placeholder' => 'Select a Officer ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>

                </div>
                <?php

            }
            ?>
        </div>


        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Inspection Information') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">


                    <div class="col-sm-4">
                        <?= $form->field($model, 'instruction_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-instruction_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-instruction_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'target_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-target_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-target_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'building_info')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select a Building ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select a Property Type ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'sub_community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'building_number')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'plot_number')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
                    </div>
                    <!--<div class="col-sm-4">
                        <?php
/*                        echo $form->field($model, 'payment_plan')->widget(Select2::classname(), [
                            'data' => array('Yes' => 'Yes', 'No' => 'No'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        */?>
                    </div>-->
                    <div class="col-sm-4">
                        <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'floor_number')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'no_of_towers')->textInput(['type'=>'number','maxlength' => true]) ?>
                    </div>

                  <!--  <div class="col-sm-4">
                        <?php
/*                        echo $form->field($model, 'instruction_person')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->staffMemberListArr,
                            'options' => ['placeholder' => 'Select a Person ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        */?>

                    </div>-->
                    <div class="col-sm-4">
                        <?= $form->field($model, 'land_size')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'purpose_of_valuation')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->purposeOfValuationArr,
                            'options' => ['placeholder' => 'Select a Purpose ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'valuation_scope')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->valuationScopeArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'email_subject')->textInput(['required'=>true]);
                        ?>

                    </div>

                    <?php if(($model->parent_id <> null) && $model->parent_id > 0){ ?>

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'revised_reason')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->revisedReasons,
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label('Reason for Revision');
                            ?>

                        </div>
                    <?php } ?>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'inspection_type')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->inspectionTypeArr,
                            'options' => ['placeholder' => 'Select a Inspection Type'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>

                    <?php
                    if($model->client->client_invoice_customer == 1) {
                        $style = "block";
                    }else{
                        $style = "none";
                    }

                    if($model->client->id == 1) {
                        $style_fee = "block";
                    }else{
                        $style_fee = "none";
                    }


                     ?>
                        <div class="col-sm-4" id="client_invoice_type" style="display: <?= $style ?>">
                            <?php
                            echo $form->field($model, 'client_invoice_type')->widget(\kartik\select2\Select2::classname(), [
                                'data' => array('0' => 'Client Name', '1' => 'Customer Name'),

                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label('Client Invoice Name');
                            ?>
                        </div>

                    <div class="col-sm-4" id="client_fixed_fee" style="display: <?= $style_fee ?>">
                        <?php


                        echo $form->field($model, 'client_fixed_fee_check')->widget(Select2::classname(), [
                            'data' => array('1' => 'Normal', '2' => 'Shaikh Zayed case fixed fee'),
                            'options' => ['placeholder' => 'Select a Placement ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Fee Selection');
                        ?>
                    </div>



                    <div class="col-sm-4">
                        <?= $form->field($model, 'title_deed')->textInput(['maxlength' => true])->label('Title Deed Certificate Number') ?>
                    </div>

                    <!--<div class="col-sm-4">
                        <?php /*echo $form->field($model, 'link_to_scrape')->textInput(); */?>
                    </div>-->

                   <!-- <div class="col-sm-4">
                        <?php /*echo $form->field($model, 'b_to_rent_link_to_scrape')->textInput(); */?>
                    </div>

                    <div class="col-sm-4">
                        <?php /*echo $form->field($model, 'pf_for_sale_scrape_url')->textInput(); */?>
                    </div>

                    <div class="col-sm-4">
                        <?php /*echo $form->field($model, 'pf_to_rent_scrape_url')->textInput(); */?>
                    </div>-->

                </div>

                <?php
                $model->step=1;
                ?>

                <?php echo $form->field($model, 'step')->textInput(['maxlength' => true,

                    'type'=>'hidden'])->label('') ?>
        </section>


    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>



