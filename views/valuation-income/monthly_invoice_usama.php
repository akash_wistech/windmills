<?php


//same as TOE

$VAT =0;

$VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);

$finalFeePayable = $netValuationFee+$VAT;
//End same as TOE
$clientAddress = \app\models\Company::find()->select(['address'])->where(['id'=>$model->id])->one();
$fee_to_words     = yii::$app->quotationHelperFunctions->numberTowords($finalFeePayable);

?>




<style>
.box1 {
    background-color: #e6e6e6;
    width: 110px;


    border-right: 5px white;
    border-left: 5px white;


}

.box2 {
    background-color: #cccccc;
    width: 90px;
    margin-right: 2px;
    border-right: 5px white;

}

.box3 {
    background-color: #cccccc;
    width: 90px;

    /* margin-right: 100px; */
}

.border {
    width: 100%;
    border-top-width: 0.05px;
    border-bottom-width: 0.05px;
    border-left-width: 0.05px;
    border-right-width: 0.05px;
    padding: 5px;
}

.font_color {
    color: #4472C4;
}

td.tbl-body {
    border-bottom: 0.2px solid grey;
}
</style>


<table class="border" width="547">

    <tr>
        <td colspan="5" class="detailtexttext first-section">
            <br><span><?= $branch_address->company; ?> </span>
            <br><span><?= $branch_address->address; ?></span>
            <br><span><?= $branch_address->office_phone; ?></span>
            <br><span>finance@windmillsgroup.com, services@windmillsgroup.com </span>
            <br><span><?= $branch_address->website; ?></span>
        </td>


        <td colspan="3" class="detailtexttext first-section">
            <br><span>Bank Details: </span>
            <br><span>Emirates NBD </span>
            <br><span>Account No: <?= $branch_address->website; ?> </span>
            <br><span>IBAN: <?= $branch_address->iban_number; ?> </span>
            <br><span>TRN: <?= $branch_address->trn_number; ?></span>
        </td>
    </tr>

</table><br><br>

<table>
    <thead>
        <tr>
            <td colspan="2" class="detailtexttext first-section" style="background-color: ;">
                <br><span class="size-8 airal-family font_color">Client</span>
                <br><span><?= $model->title ?></span>
                <br><span><?= $clientAddress->address ?></span>
                <?php
if ($model->id !=9167) {?>
                <br><span>UAE.</span>
                <?php
}
?>
                <?php if($model->trn_number <> null ){ ?>
                <br>TRN: <span><?= $model->trn_number ?></span>
                <?php
}
?>

            </td>

            <td class="box1 text-dec airal-family" style="margin-top: 20px">
                <span style="color: #4472C4;font-size: 11.5px;font-weight: bold"><br>Transaction<br>
                    Invoice
                    <br>
                </span>
            </td>
            <td class="box1 text-dec airal-family" style="margin-top: 20px">
                <span style="color: #4472C4;font-size: 11.5px;font-weight: bold"><br>Issue Date<br>30th June 2024
                    <br>
                </span>
            </td>
            <td class="box1 text-dec airal-family" style="margin-top: 20px">
                <span style="color: #4472C4;font-size: 11.5px;font-weight: bold"><br>Total
                    Amount<br><?= number_format($finalFeePayable,2) ?>
                    <br>
                </span>
            </td>





        </tr>
    </thead>
</table><br><br>

<br>

<?php 
$tbl_header = '
<table class="border" cellspacing="1" cellpadding="3" width="466">
<tr  style="color:#4472C4 ; border: 1px solid !important;">
<td class="airal-family" style="font-size:9px; font-weight: bold; text-align:left; ">DATE</td>
<td class="airal-family" style="font-size:9px; font-weight: bold; width: 100px">WM REFERENCE</td>
<td class="airal-family" style="font-size:9px; font-weight: bold; text-align: center; ">Client Ref</td>
<td class="airal-family" style="font-size:9px; font-weight: bold; text-align: center; width: 95px">Customer</td>
<td class="airal-family" style="font-size:9px; font-weight: bold; text-align: center; ">Property</td>
<td class="airal-family" style="font-size:9px; font-weight: bold; text-align:right; ">Community</td>
<td class="airal-family" style="font-size:9px; font-weight: bold; text-align:center; ">City</td>
<td class="airal-family" style="font-size:9px; font-weight: bold; text-align:right; ">Fee AED</td>
</tr>
</table>
';
?>





<?php
if(is_array($valuatonsArr['dubai_valuations']) && count($valuatonsArr['dubai_valuations']) > 0){
  $vatInDubaiVals = yii::$app->quotationHelperFunctions->getVatTotal($valuatonsFeeArr['dubai_valuations_fee']);
  $dubaiFinalFeePayable = $valuatonsFeeArr['dubai_valuations_fee']+$vatInDubaiVals;
  $dubaiFinalFeePayableToWords = yii::$app->quotationHelperFunctions->numberTowords($dubaiFinalFeePayable);
  ?>
<table class="border" width="547" style="">
    <tr>
        <td
            style="font-size:10px; font-weight: bold; text-align:center; background-color:#E3F2FD; color:#0D47A1; border-right:0.2px solid black; border-left:0.2px solid black">
            Dubai valuations
        </td>
    </tr>
</table>

<?= $tbl_header ?>

<table cellspacing="1" cellpadding="3" width="466" style=" padding: 5px;">
    <tbody>
        <?php 
  foreach($valuatonsArr['dubai_valuations'] as $key => $valuation)
  {
    echo Yii::$app->revenueHelper->getTblBody($valuation);
  }
  ?>
    </tbody>
</table>
<table class="border" width="547" style="">
    <tr>
        <td
            style="font-size:10px; font-weight: bold; text-align:center; background-color:#E3F2FD; color:#0D47A1; border-right:0.2px solid black; border-left:0.2px solid black">
            Dubai Fee
        </td>
    </tr>
</table>
<?php echo Yii::$app->revenueHelper->getFeeTable($valuatonsFeeArr['dubai_valuations_fee'], $discount , $vatInDubaiVals, $dubaiFinalFeePayable, $dubaiFinalFeePayableToWords); ?>

<?php
}
?>


<?php
if(is_array($valuatonsArr['abudhabi_and_alain_valuations']) && count($valuatonsArr['abudhabi_and_alain_valuations']) > 0){
  $vatIn_ab_alin_Vals = yii::$app->quotationHelperFunctions->getVatTotal($valuatonsFeeArr['abudhabi_and_alain_valuations_fee']);
  $ab_alin_FinalFeePayable = $valuatonsFeeArr['abudhabi_and_alain_valuations_fee']+$vatIn_ab_alin_Vals;
  $ab_alin_FinalFeePayableToWords = yii::$app->quotationHelperFunctions->numberTowords($ab_alin_FinalFeePayable);
  ?>
<br><br>
<table class="border" width="547" style="">
    <tr>
        <td
            style="font-size:10px; font-weight: bold; text-align:center; background-color:#E3F2FD; color:#0D47A1; border-right:0.2px solid black; border-left:0.2px solid black">
            Abu Dhabi valuations
        </td>
    </tr>
</table>

<?= $tbl_header ?>

<table cellspacing="1" cellpadding="3" width="466" style=" padding: 5px;">
    <tbody>
        <?php 
  foreach($valuatonsArr['abudhabi_and_alain_valuations'] as $key => $valuation)
  {
    echo Yii::$app->revenueHelper->getTblBody($valuation);
  }
  ?>
    </tbody>
</table>
<table class="border" width="547" style="">
    <tr>
        <td
            style="font-size:10px; font-weight: bold; text-align:center; background-color:#E3F2FD; color:#0D47A1; border-right:0.2px solid black; border-left:0.2px solid black">
            Abu Dhabi Fee
        </td>
    </tr>
</table>
<?php echo Yii::$app->revenueHelper->getFeeTable($valuatonsFeeArr['abudhabi_and_alain_valuations_fee'], $discount , $vatIn_ab_alin_Vals, $ab_alin_FinalFeePayable, $ab_alin_FinalFeePayableToWords); ?>

<?php
}
?>




<?php

if(is_array($valuatonsArr['ajman_valuation']) && count($valuatonsArr['ajman_valuation']) > 0){
  $vatIn_ajman_Vals = yii::$app->quotationHelperFunctions->getVatTotal($valuatonsFeeArr['ajman_valuation_fee']);
  $ajman_FinalFeePayable = $valuatonsFeeArr['ajman_valuation_fee']+$vatIn_ajman_Vals;
  $ajman_FinalFeePayableToWords = yii::$app->quotationHelperFunctions->numberTowords($ajman_FinalFeePayable);
  ?>
<br><br>
<table class="border" width="547" style="">
    <tr>
        <td
            style="font-size:10px; font-weight: bold; text-align:center; background-color:#E3F2FD; color:#0D47A1; border-right:0.2px solid black; border-left:0.2px solid black">
            Ajman valuations
        </td>
    </tr>
</table>

<?= $tbl_header ?>

<table cellspacing="1" cellpadding="3" width="466" style=" padding: 5px;">
    <tbody>
        <?php 
  foreach($valuatonsArr['ajman_valuation'] as $key => $valuation)
  {
    echo Yii::$app->revenueHelper->getTblBody($valuation);
  }
  ?>
    </tbody>
</table>
<table class="border" width="547" style="">
    <tr>
        <td
            style="font-size:10px; font-weight: bold; text-align:center; background-color:#E3F2FD; color:#0D47A1; border-right:0.2px solid black; border-left:0.2px solid black">
            Ajman Fee
        </td>
    </tr>
</table>
<?php echo Yii::$app->revenueHelper->getFeeTable($valuatonsFeeArr['ajman_valuation_fee'], $discount , $vatIn_ajman_Vals, $ajman_FinalFeePayable, $ajman_FinalFeePayableToWords); ?>

<?php
}
?>



<br><br>
<table class="border" width="547" style="">
    <tr>
        <td
            style="font-size:10px; font-weight: bold; text-align:center; background-color:#E3F2FD; color:#0D47A1; border-right:0.2px solid black; border-left:0.2px solid black">
            FINAL FEE
        </td>
    </tr>
</table>


<?php
if($model <> null && !empty($model)) {
  echo Yii::$app->revenueHelper->getFeeTable($netValuationFee, $discount , $VAT, $finalFeePayable, $fee_to_words);
}
?>










<br><br>
<table cellspacing="1" cellpadding="3" width="466">
    <thead>
        <tr style="background-color: ;">
            <td class="airal-family" style="font-size:10px; text-align:left;">We sincerely thank you for giving us this
                privilege to work for you. </td>

        </tr>
    </thead>
</table>


<br>
<br>
<br>
<br>
<br>
<hr style="height: 0.2px; margin-top: 100px;">
<br>
<table cellspacing="1" cellpadding="3" width="466">
    <thead>
        <tr style="background-color: ;">
            <td class="airal-family" style="font-size:10px; text-align:left;">Ali Raza </td>
        </tr>
        <tr style="background-color: ;">
            <td class="airal-family" style="font-size:10px; text-align:left;">Finance Manager </td>
        </tr>
    </thead>
</table>

<style>
.airal-family {
    font-family: Arial, Helvetica, sans-serif;
}
</style>


<style>
.text-dec {
    font-size: 10px;
    text-align: center;
    padding-right: 10px;
}

/*td.detailtext{*/
/*background-color:#BDBDBD; */
/*font-size:9px;*/
/*border-top: 0.4px solid grey;*/
/*}*/

td.detailtexttext {
    /* background-color:#EEEEEE; */
    font-size: 10px;
    font-weight: 100;

}

th.detailtext {
    /* background-color:#BDBDBD; */
    font-size: 9px;
    border-top: 1px solid black;
}

/*.border {
      border: 1px solid black;
    }*/

th.amount-heading {
    color: #039BE5;
}

td.amount-heading {
    color: #039BE5;
}

th.total-due {
    font-size: 16px;
}

td.total-due {
    font-size: 16px;
}

span.size-8 {
    font-size: 10px;
    font-weight: bold;
}

td.first-section {
    padding-left: 10px;
    /* background-color: black; */
}
</style>