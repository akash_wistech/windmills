<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Add Configuration');
$cardTitle = Yii::t('app', 'Add Configuration:  {nameAttribute}', [
    'nameAttribute' => $valuation->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_6/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
');


?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }

    .kv-file-content, .upload-docs img {
        width: 50px !important;
        height: 50px !important;
    }

    .width_30 {
        width: 20%;
    }

    .width_10 {
        width: 10%;
    }

    .padding10 {
        padding: 10px !important;
    }
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <fieldset disabled="disabled">
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 6]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <!-- BedRooms-->

                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">

                                <?php $unit_row = 0; ?>
                                <!--Bedroom-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_bedrooms','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_bedrooms;
                                ?>
                                <!--Bathroom-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_bathrooms','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_bathrooms;
                                ?>

                                <!--no_of_kitchen-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_kitchen','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_kitchen;
                                ?>

                                <!--	no_of_living_area-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_living_area','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_living_area;
                                ?>

                               <!-- no_of_dining_area-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_dining_area','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_dining_area;
                                ?>

                                <!-- no_of_maid_rooms-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_maid_rooms','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_maid_rooms;
                                ?>

                                <!-- no_of_laundry_area-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_laundry_area','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_laundry_area;
                                ?>

                                <!-- no_of_store-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_store','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_store;
                                ?>

                                <!-- no_of_service_block-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_service_block','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_service_block;
                                ?>

                                <!-- no_of_garage-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_garage','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_garage;
                                ?>


                                <!-- no_of_balcony-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_balcony','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_balcony;
                                ?>

                                <!-- no_of_family_room-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_family_room','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_family_room;
                                ?>

                                <!-- no_of_powder_room-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_powder_room','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_powder_room;
                                ?>
                                <!-- no_of_powder_room-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_study_room','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + $configuration->no_of_study_room;
                                ?>

                                <!-- config_flooring-->
                                <!--
                                <?php
/*                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_flooring','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + 1;
                                */?>
                                <!-- config_ceiling-->
                                <?php
/*                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_ceiling','unit_row'=> $unit_row,'attributes'=>1]);
                                $unit_row =  $unit_row + 1;
                                */?>

                                <!-- config_ceiling-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_view','unit_row'=> $unit_row,'attributes'=>0]);
                                $unit_row =  $unit_row + 1;
                                ?>

                                <!-- config_general_elevation-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_general_elevation','unit_row'=> $unit_row,'attributes'=>0]);
                                $unit_row =  $unit_row + 1;
                                ?>

                                <!-- config_Door Tag / Unit Tag-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_unit_tag','unit_row'=> $unit_row,'attributes'=>0]);
                                $unit_row =  $unit_row + 1;
                                ?>

                                <!-- Electricity Board / Panel Picture-->
                                <?php
                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' => 'config_electricity_board','unit_row'=> $unit_row,'attributes'=>0]);
                                $unit_row =  $unit_row + 1;
                                ?>

                                <?php
                                if(!empty($configuration->customAttachements) && $configuration->customAttachements <> null) {

                                    foreach ($configuration->customAttachements as $i => $attachement) {
                                        if ($attachement->quantity > 0) {
                                           // for ($i = 0; $i < $attachement->quantity; $i++) {
                                                $name = str_replace(' ','_',$attachement->name);
                                                echo $this->render('_step6_section', ['model' => $model,'valuation' => $valuation, 'configuration'=> $configuration,'config_type' =>'custom_fields','unit_row'=> $unit_row,'attributes'=>0, 'quantity'=> $attachement->quantity,'name'=> $name,'custom_field_id'=> $attachement->id, 'custom_key'=>$attachement->id.'_'.$i,'index_id' => $i]);
                                                $unit_row =  $unit_row + $attachement->quantity;


                                          //  }
                                        }


                                    }
                                }




                                ?>




                                <div class="row">
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'over_all_upgrade')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                                    </div>
                                </div>


                            </div>

                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </fieldset>
    <!-- /.card -->
</div>





<?php

$CheckedEmail = \app\models\ConfigurationFiles::find()->where(["valuation_id" => $valuation->id, "checked_image"=>'on'])->count();


?>

