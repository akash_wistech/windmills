<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use app\components\widgets\StatusVerified;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enter Public Data');
$cardTitle = Yii::t('app', ' Enter Public Data:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_501/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

$(\'#listingstransactions-instruction_date > .form-control\').prop(\'disabled\', true);
$(\'#listingstransactions-target_date > .form-control\').prop(\'disabled\', true);


$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "'.Yii::t('app',$AlertText).'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
      console.log("Hello 123")
      $("#w0").unbind("submit").submit();
    }
});
});


var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});




$("body").on("click", ".delete-file", function (e) {

    id = $(this).attr("id")
    data = {id:id}

    swal({
    title: "'.Yii::t('app','Are you sure you want to delete file from bucket ?').'",
    html: "'.Yii::t('app','').'",
    type: "warning",
    showCancelButton: true,
        confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Yes, Delete it!').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
    },function(result) {
        if (result) {
            $.ajax({
            data : data,
            url: "'.Url::to(['valuation/delete-file-public']).'",
            dataType: "html",
            type: "POST",
            success: function(data) {
                data = JSON.parse(data)
                if(data.msg == "success"){

                    deleted = "#deleted-"+id;
                    $(deleted). attr("src", "'.Yii::$app->params['uploadIcon'].'");

                    removed = ".removed-"+id;
                    $(removed).remove()

                    removedDelBtn = "#del-btn-"+id;
                    $(removedDelBtn).remove()

                    swal({
                        title: "'.Yii::t('app','Successfully Deleted').'",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#47a447",
                    });
                }
            },
            error: bbAlert
        });
        }
    });
    });
























');

$image_row = 0;
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 501]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <fieldset disabled="disabled">
                            <header class="card-header">
                                <h2 class="card-title">Valuation Details</h2>
                            </header>
                            <div class="card-body">

                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Enter Details of Agents spoken with</h2>
                                    </header>
                                    <div class="card-body">

                                        <table id="attachment"
                                               class="table table-striped table-bordered table-hover images-table">
                                            <thead>
                                            <tr>
                                                <td class="text-left">Prefix</td>
                                                <td class="text-left">First Name</td>
                                                <td class="text-left">Last Name</td>
                                                <td class="text-left">Company Name</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $row = 0; ?>
                                            <?php foreach ($model->valuationagents as $attachment) { ?>
                                                <tr id="image-row-attachment-<?php echo $row; ?>">
                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="ValuationPublicData[agents_data][<?= $row ?>][name]"
                                                               value="<?= $attachment->prefix ?>" placeholder="Mr./Mrs."
                                                               required/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="ValuationPublicData[agents_data][<?= $row ?>][name]"
                                                               value="<?= $attachment->name ?>" placeholder="First Name"
                                                               required/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="ValuationPublicData[agents_data][<?= $row ?>][lastname]"
                                                               value="<?= $attachment->lastname ?>" placeholder="Last Name"
                                                               required/>
                                                    </td>

                                                    <td>
                                                        <input type="number" class="form-control"
                                                               name="ValuationPublicData[agents_data][<?= $row ?>][percentage]"
                                                               value="<?= $attachment->percentage ?>"
                                                               placeholder="Company" required/>
                                                    </td>
                                                    <input type="hidden" class="form-control"
                                                           name="ValuationDetailData[customAttachments][<?= $row ?>][id]"
                                                           value="<?= $attachment->id ?>" placeholder="Name" required/>
                                                    <input type="hidden" class="form-control"
                                                           name="ValuationPublicData[customAttachments][<?= $row ?>][index_id]"
                                                           value="<?= $row ?>" placeholder="Name" required/>

                                                    <td class="text-left">
                                                        <button type="button"
                                                                onclick="deleteRow('-attachment-<?= $row ?>', '<?= $attachment->id; ?>', 'attachment')"
                                                                data-toggle="tooltip"
                                                                title="You want to delete Attachment"
                                                                class="btn btn-danger"><i
                                                                    class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $row++; ?>
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="4"></td>
                                                <td class="text-left">
                                                    <button type="button" onclick="addAttachment();"
                                                            data-toggle="tooltip" title="Add"
                                                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>


                                </section>


                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Attach Public Data Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row" style="padding: 10px">

                                            <?php
                                            $required_documents = array(1=>' DLD Property Status',2=>'Ajman Municipality',3=>' Abu Dhabi Municipality',4=>'Al Ain Municipality');
                                            $optional_documents = array(5=>' Sharjah Municipality',6=>'Umm Quwain  Municipality',7 =>' RAK Municipality',8 =>'Fujairah Municipality');

                                            ?>


                                            <?php $unit_row = 0; ?>

                                            <?php if($required_documents <> null && !empty($required_documents)) { ?>
                                                <table id="requestTypes"
                                                       class="table table-striped table-bordered table-hover images-table" style="pointer-events: none;">
                                                    <thead>
                                                    <tr>
                                                        <td>Description</td>
                                                        <td>Attachment</td>


                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php

                                                    foreach ($required_documents as $key =>  $required_document) {
                                                        $attachment_Details = \app\models\PublicDocsFiles::find()->where(["valuation_id" => $valuation->id, "document_id" => $key])->one();
                                                        // echo "<pre>"; print_r($attachment_Details); echo "</pre>"; die;

                                                        // dd($attachment_Details->id);
                                                        $attachment = $attachment_Details->attachment;
                                                        ?>

                                                        <tr id="image-row<?php echo $unit_row; ?>">

                                                            <td class="text-left">
                                                                <div class="required">


                                                                    <label class="control-label">
                                                                        <?= $required_document ?>
                                                                    </label>
                                                                </div>
                                                            </td>

                                                            <td class="text-left upload-docs">
                                                                <div class="form-group">
                                                                    <a href="javascript:;"
                                                                       id="upload-document<?= $unit_row; ?>"
                                                                       onclick="uploadAttachment(<?= $unit_row; ?>)"
                                                                       data-toggle="tooltip"
                                                                       class="img-thumbnail"
                                                                       title="Upload Document">
                                                                        <?php

                                                                        if ($attachment <> null) {

                                                                            if (strpos($attachment, '.pdf') !== false) {
                                                                                ?>
                                                                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                                                     id="deleted-<?= $attachment_Details->id ?>"
                                                                                     alt="" title=""
                                                                                     data-placeholder="no_image.png"/>
                                                                                <a href="<?= $attachment; ?>"
                                                                                   target="_blank">
                                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                                </a>
                                                                                <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                                </a>

                                                                                <?php

                                                                            } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                                                ?>
                                                                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                                                     id="deleted-<?= $attachment_Details->id ?>"
                                                                                     alt="" title=""
                                                                                     data-placeholder="no_image.png"/>
                                                                                <a href="<?= $attachment; ?>"
                                                                                   target="_blank">
                                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                                </a>
                                                                                <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                                </a>
                                                                                <?php

                                                                            } else {
                                                                                ?>
                                                                                <img src="<?php echo $attachment; ?>"
                                                                                     id="deleted-<?= $attachment_Details->id ?>"
                                                                                     alt="" title=""
                                                                                     data-placeholder="no_image.png"/>
                                                                                <a href="<?= $attachment; ?>"
                                                                                   target="_blank">
                                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                                </a>
                                                                                <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                                </a>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <?php
                                                                        }

                                                                        ?>
                                                                    </a>
                                                                    <input type="hidden"
                                                                           class="removed-<?= $attachment_Details->id; ?>"
                                                                           name="ValuationPublicData[received_docs][<?php echo $unit_row; ?>][attachment]"
                                                                           value="<?= $attachment; ?>"
                                                                           id="input-attachment<?php echo $unit_row; ?>"/>
                                                                    <input type="hidden"
                                                                           class="removed-<?= $attachment_Details->id; ?>"
                                                                           name="ValuationPublicData[received_docs][<?php echo $unit_row; ?>][document_id]"
                                                                           value="<?= $key; ?>"
                                                                           id="input-attachment<?php echo $unit_row; ?>"/>

                                                                    <input type="hidden"
                                                                           class="removed-<?= $attachment_Details->id; ?>"
                                                                           name="ValuationPublicData[received_docs][<?php echo $unit_row; ?>][valuation_id]"
                                                                           value="<?= $valuation->id; ?>"
                                                                           id="input-history_id<?php echo $unit_row; ?>"/>
                                                                </div>
                                                            </td>


                                                        </tr>
                                                        <?php $unit_row++; ?>

                                                    <?php }
                                                    foreach ($optional_documents as $okey => $optinal_document) {
                                                        $attachment_Details = \app\models\PublicDocsFiles::find()->where(["valuation_id" => $valuation->id, "document_id" => $okey])->one();
                                                        // echo "<pre>"; print_r($attachment_Details); echo "</pre>"; die;
                                                        // dd($attachment_Details->id);
                                                        $attachment = $attachment_Details->attachment;
                                                        ?>

                                                        <tr id="image-row<?php echo $unit_row; ?>">

                                                            <td class="text-left ">
                                                                <div class="required">


                                                                    <label>
                                                                        <?= $optinal_document ?>
                                                                    </label>
                                                                </div>
                                                            </td>

                                                            <td class="text-left upload-docs">
                                                                <div class="form-group">
                                                                    <a href="javascript:;"
                                                                       id="upload-document<?= $unit_row; ?>"
                                                                       onclick="uploadAttachment(<?= $unit_row; ?>)"
                                                                       data-toggle="tooltip"
                                                                       class="img-thumbnail"
                                                                       title="Upload Document">
                                                                        <?php

                                                                        if ($attachment <> null) {

                                                                            if (strpos($attachment, '.pdf') !== false) {
                                                                                ?>
                                                                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                                                     id="deleted-<?= $attachment_Details->id ?>"
                                                                                     alt="" title=""
                                                                                     data-placeholder="no_image.png"/>
                                                                                <a href="<?= $attachment; ?>"
                                                                                   target="_blank">
                                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                                </a>
                                                                                <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                                </a>

                                                                                <?php

                                                                            } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                                                ?>
                                                                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                                                     id="deleted-<?= $attachment_Details->id ?>"
                                                                                     alt="" title=""
                                                                                     data-placeholder="no_image.png"/>
                                                                                <a href="<?= $attachment; ?>"
                                                                                   target="_blank">
                                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                                </a>
                                                                                <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                                </a>
                                                                                <?php

                                                                            } else {
                                                                                ?>
                                                                                <img src="<?php echo $attachment; ?>"
                                                                                     id="deleted-<?= $attachment_Details->id ?>"
                                                                                     alt="" title=""
                                                                                     data-placeholder="no_image.png"/>
                                                                                <a href="<?= $attachment; ?>"
                                                                                   target="_blank">
                                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                                </a>
                                                                                <a href="javascript:;" id="del-btn-<?= $attachment_Details->id ?>">
                                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"  id ="<?= $attachment_Details->id; ?>"></span>
                                                                                </a>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <?php
                                                                        }

                                                                        ?>
                                                                    </a>
                                                                    <input type="hidden"
                                                                           class="removed-<?= $attachment_Details->id; ?>"
                                                                           name="ValuationPublicData[received_docs][<?php echo $unit_row; ?>][attachment]"
                                                                           value="<?= $attachment; ?>"
                                                                           id="input-attachment<?php echo $unit_row; ?>"/>
                                                                    <input type="hidden"
                                                                           class="removed-<?= $attachment_Details->id; ?>"
                                                                           name="ValuationPublicData[received_docs][<?php echo $unit_row; ?>][document_id]"
                                                                           value="<?= $okey; ?>"
                                                                           id="input-attachment<?php echo $unit_row; ?>"/>

                                                                    <input type="hidden"
                                                                           class="removed-<?= $attachment_Details->id; ?>"
                                                                           name="ValuationPublicData[received_docs][<?php echo $unit_row; ?>][valuation_id]"
                                                                           value="<?= $valuation->id; ?>"
                                                                           id="input-history_id<?php echo $unit_row; ?>"/>
                                                                </div>
                                                            </td>


                                                        </tr>
                                                        <?php $unit_row++; ?>

                                                    <?php }

                                                    ?>
                                                    </tbody>
                                                    <tfoot>

                                                    </tfoot>
                                                </table>
                                            <?php } ?>


                                        </div>
                                    </div>


                                </section>



                                


                            </div>
                            
                            </fieldset>
                            <?php ActiveForm::end(); ?>
                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    var row = <?= $row ?>;
    function addAttachment() {

        html = '<tr id="image-row' + row + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ValuationPublicData[agents_data][' + row + '][prefix]" value="" placeholder="Prefix" required/>';
        html += '    </div>';
        html += '  </td>';
        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ValuationPublicData[agents_data][' + row + '][name]" value="" placeholder="First Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ValuationPublicData[agents_data][' + row + '][lastname]" value="" placeholder="Last Name" required />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ValuationPublicData[agents_data][' + row + '][percentage]" value="" placeholder="Company" required />';
        html += '    </div>';
        html += '  </td>';


        html += '       <input type="hidden" class="form-control"  name="ValuationPublicData[agents_data][' + row + '][index_id]" value="' + row + '" placeholder="Quantity" required />';



        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment tbody').append(html);

        row++;
    }

    function deleteRow(rowId, docID, type) {



        var url = '<?= \yii\helpers\Url::to('/valuation/remove-agent'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if(response.status == 'exist'){
                            swal("Warning!", response.message, "warning");
                        }else{
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
</script>


<script type="text/javascript">
    var unit_row = <?= $unit_row ?>;

    function addUnit() {
        html = '<tr id="image-row' + unit_row + '">';


        html += '  <td class="text-left">';
        html += ' <div class="form-group">';
        html += ' <a href="javascript:;" id="upload-document' + unit_row + '" onclick="uploadAttachment(' + unit_row + ')" data-toggle="tooltip" class="img-thumbnail" title="Upload Document">';
        html += '<img src="<?= Yii::getAlias('@web') . '/images/upload-image.png' ?>" width="100" alt="" title="" data-placeholder="<?= Yii::getAlias('@web') . '/themes/wisdom/images/upload.png' ?>" />';
        html += '<i></i>';
        html += '</a>';
        html += ' <input type="hidden" name="Ticket[attachments][' + unit_row + '][attachment]" value="" id="input-attachment' + unit_row + '" />';
        html += ' <input type="hidden" name="Ticket[attachments][' + unit_row + '][history_id]" value="" id="input-history_id' + unit_row + '" />';
        html += ' <input type="hidden" name="Ticket[attachments][' + unit_row + '][user_id]" value="" id="input-user_id' + unit_row + '" />';
        html += '</div>';
        html += '  </td>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + unit_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger">-</button></td>';

        html += '</tr>';

        unit_row++;

        $('#requestTypes tbody').append(html);
    }

    var uploadAttachment = function (attachmentId) {


        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function (json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);


                            console.log(json.file.href);
                            var str1 = json['file'].href;
                            var str2 = ".pdf";
                            var str3 = ".doc";
                            var str4 = ".docx";
                            var str5 = ".xlsx";
                            var str6 = ".xls";
                            if (str1.indexOf(str2) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                            } else if (str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                            } else {
                                $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                            }
                            $('#input-attachment' + attachmentId).val(json['file'].href);


                            /*  $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                             $('#input-attachment' + attachmentId).val(json['file'].href);*/
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }

</script>

