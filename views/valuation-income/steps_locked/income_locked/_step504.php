<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use app\components\widgets\StatusVerified;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Comparable Properties Price Details');
$cardTitle = Yii::t('app', ' Comparable Properties:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_504/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                    aria-orientation="vertical">
                    <?php echo $this->render('../../left-nav', ['model' => $valuation, 'step' => 504]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                        aria-labelledby="vert-tabs-home-tab">
                        <?php $form = ActiveForm::begin(); ?>
                        <fieldset disabled="disabled">

                        <section class="valuation-form card card-outline card-primary">

                            <header class="card-header">
                                <h2 class="card-title">Recent and Comparable Sold Transaction Prices Analysis:</h2>
                            </header>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-sm-4 avg_price_recent_sold mb-3">
                                        <?= $form->field($model, 'avg_price_recent_sold')->textInput(['maxlength' => true])
                                            ->label('Average Transaction Price of the Recent Sold') ?>
                                        <div class="help-block2"></div>
                                    </div>
                                    <div class="col-sm-4 avg_price_sqft_sold mb-3">
                                        <?= $form->field($model, 'avg_price_sqft_sold')->textInput(['maxlength' => true])
                                            ->label('Average Transaction Price/Sqft of the Sold') ?>
                                        <div class="help-block2"></div>
                                    </div>
                                    <div class="col-sm-4 avg_price_sold_adjusted mb-3">
                                        <?= $form->field($model, 'avg_price_sold_adjusted')->textInput(['maxlength' => true])
                                            ->label('Adjusted Average Transaction Price Recent Sold') ?>
                                        <div class="help-block2"></div>
                                    </div>
                                    <div class="col-sm-4 avg_price_sqft_sold_adjusted mb-3">
                                        <?= $form->field($model, 'avg_price_sqft_sold_adjusted')->textInput(['maxlength' => true])
                                            ->label('Adjusted Average Transaction Price/Sqft Sold') ?>
                                        <div class="help-block2"></div>
                                    </div>
                                </div>

                            </div>

                        </section>

                        <section class="valuation-form card card-outline card-primary">

                            <header class="card-header">
                                <h2 class="card-title">Recent and Comparable Market Listings Analysis:</h2>
                            </header>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-sm-4 avg_price_recent_market mb-3">
                                        <?= $form->field($model, 'avg_price_recent_market')->textInput(['maxlength' => true])
                                            ->label('Average Transaction Price of the Recent Market') ?>
                                        <div class="help-block2"></div>
                                    </div>
                                    <div class="col-sm-4 avg_price_sqft_market mb-3">
                                        <?= $form->field($model, 'avg_price_sqft_market')->textInput(['maxlength' => true])
                                            ->label('Average Transaction Price/Sqft of the Market') ?>
                                        <div class="help-block2"></div>
                                    </div>
                                    <div class="col-sm-4 avg_price_market_adjusted mb-3">
                                        <?= $form->field($model, 'avg_price_market_adjusted')->textInput(['maxlength' => true])
                                            ->label('Adjusted Average Transaction Price Market') ?>
                                        <div class="help-block2"></div>
                                    </div>
                                    <div class="col-sm-4 avg_price_sqft_market_adjusted mb-3">
                                        <?= $form->field($model, 'avg_price_sqft_market_adjusted')->textInput(['maxlength' => true])
                                            ->label('Adjusted Average Transaction Price/Sqft Market') ?>
                                        <div class="help-block2"></div>
                                    </div>
                                </div>

                            </div>

                        </section>


                        </fieldset>
                        <?php ActiveForm::end(); ?>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>

</div>