<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use app\components\widgets\StatusVerified;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enter Inspection Schedule');
$cardTitle = Yii::t('app', ' Enter Inspection Schedule:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_4/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
if ($model->email_status != 1) {
    $AlertText = 'Data and Valuation Status will be saved and Email will be sent once?';
} else {
    $AlertText = 'Only Data will be saved?';
}

$this->registerJs('

    

    $("#listingstransactions-valuation_report_date,#listingstransactions-valuation_date,#listingstransactions-inspection_date,#listingstransactions-client_deadline").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "DD-MMM-YYYY"
    });
    
    
    $("#listingstransactions-inspection_time").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "HH:mm",
        // defaultDate: moment(),
        // focusOnShow: false,
    });
    
    $(document).on("click", function (e) {
        if ($(e.target).closest("#listingstransactions-valuation_report_date,#listingstransactions-valuation_date,#listingstransactions-inspection_date,#listingstransactions-client_deadline").length === 0) {
            $("#listingstransactions-valuation_report_date,#listingstransactions-valuation_date,#listingstransactions-inspection_date,#listingstransactions-client_deadline").datetimepicker("hide");
        }
    });
    
    $(document).on("click", function (e) {
        if ($(e.target).closest("#listingstransactions-inspection_time").length === 0) {
            $("#listingstransactions-inspection_time").datetimepicker("hide");
        }
    });

    $(\'#listingstransactions-inspection_date > .form-control\').prop(\'disabled\', true);
    $(\'#listingstransactions-inspection_time > .form-control\').prop(\'disabled\', true);

    
    $("#listingstransactions-valuation_report_date").on("change.datetimepicker", ({date, oldDate}) => {              
        // console.log(date);
        // console.log(oldDate);
        var valuationDate         = $("#scheduleinspection-valuation_date").val();
        var valuationReportDate   = $("#scheduleinspection-valuation_report_date").val();
       
        var g1 = new Date(valuationDate);
        var g2 = new Date(valuationReportDate);
        // console.log(g1); console.log(g2);   
                 
        if (g2.getTime() >= g1.getTime()){
        // console.log("done");
        }
        else{
         $("#scheduleinspection-valuation_report_date").val("");
          swal({
            title: "' . Yii::t('app', 'Valuation Report Date Must be Greater then or Equal to Valuation Date') . '",
            type: "warning",
            showCancelButton: false,
                confirmButtonColor: "#47a447",
            });
        }
    })    
    
   
    
    

    var preventClick = false;
    $("#ThisLink").click(function(e) {
        $(this)
           .css("cursor", "default")
           .css("text-decoration", "none")
        if (!preventClick) {
            $(this).html($(this).html() );
        }
        preventClick = true;
        return false;
    });
    
    $("body").on("click", ".sav-btn1", function (e) {
    _this=$(this);
    e.preventDefault();
    swal({
    title: "' . Yii::t('app', $AlertText) . '",
    html: "' . Yii::t('app', 'Are you sure you want to delete this? You will not be able to recover this!') . '",
    type: "warning",
    showCancelButton: true,
        confirmButtonColor: "#47a447",
    confirmButtonText: "' . Yii::t('app', 'Yes, Do you want to save it!') . '",
    cancelButtonText: "' . Yii::t('app', 'Cancel') . '",
    },function(result) {
        if (result) {
          console.log("Hello 123")
          $("#w0").submit().unbind("submit");
        }
    });
    });
    
    $("body").on("click", ".sav-btn2", function (e) {
    _this=$(this);
    e.preventDefault();
    swal({
    title: "' . Yii::t('app', 'Are you sure you want to send Email!') . '",
    html: "' . Yii::t('app', 'Are you sure you want to send Email!') . '",
    type: "warning",
    showCancelButton: true,
        confirmButtonColor: "#47a447",
    confirmButtonText: "' . Yii::t('app', 'Yes, Do you want to send it!') . '",
    cancelButtonText: "' . Yii::t('app', 'Cancel') . '",
    },function(result) {
        if (result) {
        
         $("#w1").submit().unbind("submit");
         
        }
    });
    });


    $(".only-char").keypress(function(event) {
        var inputValue = String.fromCharCode(event.which);
        // Regular expression to allow only letters and exclude the "+" sign
        var regex = /^[a-zA-Z]*$/;
        // Check if the input value matches the regular expression
        if (!regex.test(inputValue)) {
          event.preventDefault(); // Prevent input of non-letter characters
        }
      });
');
if ($valuation->purpose_of_valuation == 14) {
    $valuation_lable = 'Assessment Date';
} else {
    $valuation_lable = 'Valuation Date';
}
?>
<style>
    .datepicker-days .table>thead>tr>th,
    .table>tbody>tr>th,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>tbody>tr>td,
    .table>tfoot>tr>td {
        padding: inherit !important;
    }

    .field-scheduleinspection-contact_phone_no,
    .field-scheduleinspection-land_line_no {
        width: 73% !important;
    }
</style>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                    aria-orientation="vertical">
                    <?php echo $this->render('../../left-nav', ['model' => $valuation, 'step' => 4]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                        aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-secondary mb-3">

                            <?php $form = ActiveForm::begin(); ?>
                            <fieldset disabled="disabled">
                            <header class="card-header">
                                <h2 class="card-title">

                                </h2>
                            </header>

                            <div class="card-body">
                                <div class="card card-outline card-primary mb-3">
                                    <header class="card-header">
                                        <h2 class="card-title">Inspection Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <input type="hidden" id="inspection_type_id"
                                                value="<?= $valuation->inspection_type ?>">



                                            <!--     <div class="col-sm-4" id="valuation_date_id">
                                                <? /*= $form->field($model, 'valuation_date', ['template' => '
                                              {label}
                                              <div class="input-group date" style="display: flex" id="listingstransactions-valuation_date" data-target-input="nearest">
                                                  {input}
                                                  <div class="input-group-append" data-target="#listingstransactions-valuation_date" data-toggle="datetimepicker">
                                                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                  </div>
                                              </div>
                                              {hint}{error}
                                              '])->textInput(['maxlength' => true])->label($valuation_lable) */?>
                                            </div>-->


                                            <div class="col-sm-4" id="inspection_date_id">
                                                <?php $inspection_date = ($model->inspection_date <> null) ? date('d-M-Y', strtotime($model->inspection_date)) : date('d-M-Y') ?>
                                                <label for="html" id='inpect_date_label'>Inspection Date <span
                                                        class="text-danger">*</span></label><br>
                                                <?= $form->field($model, 'inspection_date', [
                                                    'template' => '
                                                    {label}
                                                    <div class="input-group date" style="display: flex" id="listingstransactions-inspection_date" data-target-input="nearest">
                                                    {input}
                                                    <div class="input-group-append" data-target="#listingstransactions-inspection_date" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    </div>
                                                    {hint}{error}
                                                    '
                                                ])->textInput(['maxlength' => true, 'value' => $inspection_date])->label(false) ?>
                                            </div>

                                            <div class="col-sm-4" id="inspection_time_id">
                                                <?= $form->field($model, 'inspection_time', [
                                                    'template' => '
                                                    {label}
                                                    <div class="input-group date" style="display: flex" id="listingstransactions-inspection_time" data-target-input="nearest">
                                                    {input}
                                                    <div class="input-group-append" data-target="#listingstransactions-inspection_time" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                                    </div>
                                                    </div>
                                                    {hint}{error}
                                                    '
                                                ])->textInput(['maxlength' => true]) ?>
                                            </div>






                                            <div class="col-sm-4" id="inspection_officer_id">
                                                <?php
                                                echo $form->field($model, 'inspection_officer')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->staffMemberListArr,
                                                    'options' => ['placeholder' => 'Select a Person ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'disabled' => true
                                                    ],
                                                ]);
                                                ?>

                                            </div>






                                        </div>
                                    </div>
                                </div>

                                <div class="card card-outline card-primary mb-3">
                                    <header class="card-header">
                                        <h2 class="card-title">Inspector Change Details</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4 changed_inspection_officer"
                                                id="changed_inspection_officer_id">
                                                <?php
                                                echo $form->field($model, 'changed_inspection_officer')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->staffMemberListArr,
                                                    'options' => ['placeholder' => 'Select a Person ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'disabled' => true
                                                    ],
                                                ])->label('Changed Inspection Officer <span class="text-danger">*</span>');
                                                ?>

                                            </div>

                                            <div class="col-sm-4 transfer_reason">
                                                <?php
                                                echo $form->field($model, 'transfer_reason')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(\app\models\InspectionTransferReasons::find()->where(['status' => 1])->andWhere(['trashed' => 0])->orderBy([
                                                        'title' => SORT_ASC,
                                                    ])->all(), 'id', 'title'),
                                                    'options' => ['placeholder' => 'Select a Transfer Reason ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'disabled' => true
                                                    ],
                                                ])->label('Transfer Reason <span class="text-danger">*</span>');
                                                ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                

                                


                            </div>
                            
                            </fieldset>
                            <?php ActiveForm::end(); ?>
                        </section>

                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <div class="card-body">
                                <?php if (isset($model->valuation->cancelReasons) && ($model->valuation->cancelReasons <> null)) { ?>
                                    <div class="row">

                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Reason</th>
                                                    <th>Email Sent Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($model->valuation->cancelReasons as $reason) { ?>
                                                    <tr>

                                                        <td>&nbsp;&nbsp;&nbsp;
                                                            <?= $reason->reason->title ?>
                                                        </td>
                                                        <td>&nbsp;&nbsp;&nbsp;
                                                            <?= date('d-m-Y', strtotime($reason->created_at)); ?>
                                                        </td>

                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } ?>


                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model_reason, 'reason_id')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(\app\models\InspectionCancelReasons::find()->where(['status' => 1])->andWhere(['trashed' => 0])->orderBy([
                                            'title' => SORT_ASC,
                                        ])->all(), 'id', 'title'),
                                        'options' => ['placeholder' => 'Select a Inspection Reason ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label('Select reason for Email');
                                    ?>

                                </div>
                            </div>
                            <?php echo $form->field($model_reason, 'valuation_id')->textInput([
                                'maxlength' => true,
                                'type' => 'hidden',
                                'value' => $valuation->id
                            ])->label('') ?>

                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Send Email'), ['class' => 'btn btn-success sav-btn2']) ?>

                            </div>
                            <?php ActiveForm::end(); ?>





                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<script>

    /* document.getElementById("ajaxSubmitBtn").onclick = function(){
     var calltime =document.getElementById("requests-call_time").value;
     var visittime  =document.getElementById("requests-visit_time").value;
     var descp  =document.getElementById("requests-description").value;
     if (calltime !== '' && visittime  !== '' && descp !== '' ) {
     setInterval(function(){
     document.getElementById("ajaxSubmitBtn").disabled = true;
     },100);
     }
     } */

    var inspectiontype = document.getElementById("inspection_type_id").value;

    if (inspectiontype == 2) {
        document.getElementById("client_deadline_id").classList.add("d-none");
        document.getElementById("inspection_date_id").classList.remove("d-none");
        document.getElementById("inspection_time_id").classList.remove("d-none");
        document.getElementById("inspection_officer_id").classList.remove("d-none");
        document.getElementById("contact_person_name_id").classList.remove("d-none");
        document.getElementById("contact_email_id").classList.remove("d-none");
        document.getElementById("contact_phone_no_id").classList.remove("d-none");

        document.getElementById("inpect_date_label").innerHTML = "Physical Inspection";
    }
    if (inspectiontype == 1) {

        document.getElementById("inspection_date_id").classList.remove("d-none");
        document.getElementById("inspection_time_id").classList.remove("d-none");
        document.getElementById("inspection_officer_id").classList.remove("d-none");
        document.getElementById("client_deadline_id").classList.add("d-none");
        document.getElementById("contact_person_name_id").classList.add("d-none");
        document.getElementById("contact_email_id").classList.add("d-none");
        document.getElementById("contact_phone_no_id").classList.add("d-none");

        document.getElementById("inpect_date_label").innerHTML = "Drive By";
    }
    if (inspectiontype == 3) {
        document.getElementById("inspection_date_id").classList.add("d-none");
        document.getElementById("inspection_time_id").classList.add("d-none");
        document.getElementById("client_deadline_id").classList.add("d-none");
        document.getElementById("inspection_officer_id").classList.add("d-none");
        document.getElementById("contact_person_name_id").classList.add("d-none");
        document.getElementById("contact_email_id").classList.add("d-none");
        document.getElementById("contact_phone_no_id").classList.add("d-none");
        document.getElementById("client_deadline_id").classList.add("d-none");

        document.getElementById("inpect_date_label").innerHTML = "Desktop";
    }


    document.getElementById("inspection_type_id").onchange = function () {
        var inspectiontype = document.getElementById("inspection_type_id").value;

        if (inspectiontype == 2) {
            document.getElementById("client_deadline_id").classList.add("d-none");
            document.getElementById("inspection_date_id").classList.remove("d-none");
            document.getElementById("inspection_time_id").classList.remove("d-none");
            document.getElementById("inspection_officer_id").classList.remove("d-none");
            document.getElementById("contact_person_name_id").classList.remove("d-none");
            document.getElementById("contact_email_id").classList.remove("d-none");
            document.getElementById("contact_phone_no_id").classList.remove("d-none");

            document.getElementById("inpect_date_label").innerHTML = "Physical Inspection";
        }
        if (inspectiontype == 1) {

            document.getElementById("inspection_date_id").classList.remove("d-none");
            document.getElementById("inspection_time_id").classList.remove("d-none");
            document.getElementById("inspection_officer_id").classList.remove("d-none");
            document.getElementById("client_deadline_id").classList.add("d-none");
            document.getElementById("contact_person_name_id").classList.add("d-none");
            document.getElementById("contact_email_id").classList.add("d-none");
            document.getElementById("contact_phone_no_id").classList.add("d-none");

            document.getElementById("inpect_date_label").innerHTML = "Drive By";
        }
        if (inspectiontype == 3) {
            document.getElementById("inspection_date_id").classList.add("d-none");
            document.getElementById("inspection_time_id").classList.add("d-none");
            document.getElementById("client_deadline_id").classList.add("d-none");
            document.getElementById("inspection_officer_id").classList.add("d-none");
            document.getElementById("contact_person_name_id").classList.add("d-none");
            document.getElementById("contact_email_id").classList.add("d-none");
            document.getElementById("contact_phone_no_id").classList.add("d-none");
            document.getElementById("client_deadline_id").classList.add("d-none");

            document.getElementById("inpect_date_label").innerHTML = "Desktop";
        }


        console.log(Driveby);
    }





</script>