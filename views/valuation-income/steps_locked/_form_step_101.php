<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ValuationFormAsset;
use app\components\widgets\StatusVerified;

ValuationFormAsset::register($this);
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */



$land = array(4,5,20,23,26,29,39,46,47,48,49,50,53);


if (in_array($model->property_id, $land))
{
    $style_land = "none";
}
else
{
    $style_land = "block";
}
?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }

    .fade {
        opacity: 1;
    }

    .nav-tabs.flex-column .nav-link.active {
        background-color: #007bff;
        color: white;
        font-weight: bold;
    }
</style>

<section class="valuation-form card card-outline card-primary">

    <?php $form = ActiveForm::begin(); ?>
    <fieldset disabled="disabled">
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle ?></h2>
    </header>
    <div class="card-body">


      <!--  <div class="row parent-row-ff">-->


            <section class="card card-outline card-info">
                <header class="card-header">
                    <h2 class="card-title"><?= Yii::t('app', 'Address Details') ?></h2>
                </header>
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'building_info')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                                    'title' => SORT_ASC,
                                ])->all(), 'id', 'title'),
                                'options' => ['placeholder' => 'Select a Building ...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ])->label('Building/Project Name');
                            ?>

                        </div>
                        <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>">
                            <?= $form->field($model, 'building_number')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'plot_number')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
                        </div>


                        <div class="col-sm-4">
                            <?= $form->field($model, 'community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'sub_community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'country')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                        </div>
                        <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>">
                            <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>">
                            <?= $form->field($model, 'floor_number')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'location_pin')->textInput(['maxlength' => true])->label('Location Pin <span class="text-danger">*</span>') ?>
                        </div>
                    </div>
                </div>
            </section>
            <div class="clearfix"></div>

            <section class="card card-outline card-info" >
                <header class="card-header">
                    <h2 class="card-title"><?= Yii::t('app', 'Address Details') ?></h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy([
                                    'title' => SORT_ASC,
                                ])->all(), 'id', 'title'),
                                'options' => ['placeholder' => 'Select a Property Type ...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ])->label('Property Type');
                            ?>

                        </div>
                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ]);
                            ?>

                        </div>
                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'property_purpose')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(\app\models\PropertyPurposes::find()->where(['status' => 1])->orderBy([
                                    'title' => SORT_ASC,
                                ])->all(), 'id', 'title'),
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ]);
                            ?>

                        </div>

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ]);
                            ?>

                        </div>
                        <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>" >
                            <?= $form->field($model, 'no_of_towers')->textInput(['type'=>'number','maxlength' => true])->label('Number of Buildings/Units on Land') ?>
                        </div>
                        <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>" >
                            <?= $form->field($model, 'bedrooms')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-sm-4">
                            <?= $form->field($model, 'land_size')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>" >
                            <?php
                            echo $form->field($model, 'plot_source')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrPlotSource,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ])->label(' Source Document of Land Size');
                            ?>

                        </div>
                        <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>" >
                            <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true])->label('BUA / GFA') ?>
                        </div>
                        <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>" >
                            <?php
                            echo $form->field($model, 'bua_source')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrBuaSource,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ])->label(' Source Document of BUA/GFA');
                            ?>

                        </div>
                        <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>">
                            <?= $form->field($model, 'parking_floors')->textInput(['maxlength' => true])->label("Parking Space") ?>
                        </div>


                        <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>" >
                            <?php
                            echo $form->field($model, 'parking_source')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrParkingSource,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ])->label('Source of Number of Parkings');
                            ?>

                        </div>
                        <?php   if(isset($model->building->city) && ($model->building->city == 3507)){ ?>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'title_deed')->textInput(['maxlength' => true,'value'=>$valuation->title_deed])->label('Title Deed Certificate Number') ?>
                        </div>
                        <?php } ?>
                    </div>

                </div>
            </section>

     <!--   </div>
-->
       




    </div>
    
    </fieldset>
    <?php ActiveForm::end(); ?>
</section>



