<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Receive Info/Documents');
$cardTitle = Yii::t('app', 'Receive Info/Documents:  {nameAttribute}', [
    'nameAttribute' => $valuation->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_2/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
if ($model->email_status !=1) {
    $AlertText='Data and Valuation Status will be saved and Email will be sent once?';
}
else{
    $AlertText='Only Data will be saved?';
}

$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "'.Yii::t('app',$AlertText).'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
      console.log("Hello 123")
      $("#w0").unbind("submit").submit();
    }
});
});


var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});
');

$image_row = 0;
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 2]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <fieldset disabled="disabled">
                            <div class="card-body">
                                <?php if(Yii::$app->user->identity->permission_group_id != 15){ ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'dld_data_enquiry')->widget(Select2::classname(), [
                                            'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'kharetati_application_enquiry')->widget(Select2::classname(), [
                                            'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'dld_sold_transaction_enquiry')->widget(Select2::classname(), [
                                            'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'dld_valuations_enquir')->widget(Select2::classname(), [
                                            'data' => array('No' => 'No', 'Yes' => 'Yes',),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'discussions_real_estate')->widget(Select2::classname(), [
                                            'data' => array('No' => 'No', 'Yes' => 'Yes',),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>

                                <!-- Owners Information-->
                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Owners Information</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row" style="padding: 10px">
                                            <?php $owner_row = 0; ?>


                                            <table id="owner_container"
                                                   class="table table-striped table-bordered table-hover images-table ">
                                                <thead>
                                                <tr>
                                                    <td style="padding-left: 5px !important;"
                                                        class="text-left  width_40"><strong>Name</strong></td>
                                                    <td style="padding-left: 5px !important;"
                                                        class="text-left  width_40"><strong>Percentage</strong></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php

                                                if ($valuation->no_of_owners > 0) {
                                                    for ($o = 0; $o < $valuation->no_of_owners; $o++) {
                                                        $owner_Details = \app\models\ValuationOwners::find()->where(["valuation_id" => $valuation->id, "index_id" => $o])->one();
                                                        ?>
                                                        <tr id="owner_data_row<?php echo($o); ?>">


                                                            <td class="text-left">
                                                                <div class="form-group">
                                                                    <input type="text"
                                                                           name="ReceivedDocs[owners_data][<?= $o; ?>][name]"
                                                                           value="<?= ($owner_Details <> null) ? $owner_Details->name : "" ?>"
                                                                           placeholder="Name" class="form-control"
                                                                           required/>
                                                                </div>
                                                            </td>

                                                            <td class="text-left">
                                                                <div class="form-group">
                                                                    <input type="text"
                                                                           name="ReceivedDocs[owners_data][<?= $o; ?>][percentage]"
                                                                           value="<?= ($owner_Details <> null) ? $owner_Details->percentage : "" ?>"
                                                                           placeholder="Percentage" class="form-control"
                                                                           required/>
                                                                </div>
                                                            </td>
                                                            <input type="hidden"
                                                                   name="ReceivedDocs[owners_data][<?= $o; ?>][valuation_id]"
                                                                   value="<?= $valuation->id; ?>"
                                                            />
                                                            <input type="hidden"
                                                                   name="ReceivedDocs[owners_data][<?= $o; ?>][index_id]"
                                                                   value="<?= $o; ?>"
                                                            />
                                                        </tr>
                                                        <?php

                                                    }
                                                }
                                                ?>
                                                </tbody>
                                                <tfoot>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </section>
                                <!-- Media Information-->
                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Documents Information</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row" style="padding: 10px">

                                            <?php

                                            if (isset($valuation->property->required_documents) && ($valuation->property->required_documents <> null)) {
                                                $required_documents = explode(',', $valuation->property->required_documents);
                                            }
                                            if (isset($valuation->property->optional_documents) && ($valuation->property->optional_documents <> null)) {
                                                $optional_documents = explode(',', $valuation->property->optional_documents);
                                            }
                                            ?>


                                            <?php $unit_row = 0; ?>

                                            <?php if($required_documents <> null && !empty($required_documents)) { ?>
                                            <table id="requestTypes"
                                                   class="table table-striped table-bordered table-hover images-table">
                                                <thead>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>Attachment</td>
                                                    <td></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php

                                                foreach ($required_documents as $required_document) {

                                                    if($required_document == 60){
                                                        if ($valuation->client->client_type !== "corporate" && $valuation->client->client_type !== "individual") {
                                                            continue;
                                                        }
                                                    }
                                                    if($required_document == 73){
                                                        if ($valuation->client->client_type !== "bank" ) {
                                                            continue;
                                                        }
                                                    }
                                                    if($required_document == 58 || $required_document == 59 ){
                                                        if ($valuationDetail->extended == "No") {
                                                            continue;
                                                        }
                                                    }
                                                    if($required_document == 61 ){
                                                        if ($valuationDetail->completion_status == 1) {
                                                            continue;
                                                        }
                                                    }
                                                    if($required_document == 50 ){
                                                        if ($valuationDetail->building->cityName->title != "Dubai") {
                                                            continue;
                                                        }
                                                    }
                                                    if($required_document == 11 ){
                                                        if (in_array($valuationDetail->property_id, [20,46]) && $valuationDetail->completion_status == 1) {
                                                            continue;
                                                        }
                                                    }

                                                    $attachment_Details = \app\models\ReceivedDocsFiles::find()->where(["valuation_id" => $valuation->id, "document_id" => $required_document])->one();
                                                    $attachment = $attachment_Details->attachment;
                                                    ?>

                                                    <tr id="image-row<?php echo $unit_row; ?>">

                                                        <td class="text-left">
                                                            <?= Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] ?>

                                                        </td>

                                                        <td class="text-left upload-docs">
                                                            <div class="form-group">
                                                                <a href="javascript:;"
                                                                   id="upload-document<?= $unit_row; ?>"
                                                                   onclick="uploadAttachment(<?= $unit_row; ?>)"
                                                                   data-toggle="tooltip"
                                                                   class="img-thumbnail"
                                                                   title="Upload Document">
                                                                    <?php

                                                                    if ($attachment <> null) {

                                                                        $attachment_link= $attachment;
                                                                        $explode_attach_doc = explode('received-info/',$attachment);

                                                                        if(isset($explode_attach_doc[1])) {
                                                                            $attachment_src = 'https://maxima.windmillsgroup.com/cache/'.$explode_attach_doc[1];
                                                                        }else{
                                                                            $attachment_src = Yii::$app->params['uploadDocsIcon'];
                                                                        }
                                                                        $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/',$attachment);
                                                                        if(isset($explode_attach[1])) {
                                                                            $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                        }else {
                                                                            $explode_attach = explode('https://newcliudfrontaclmax.s3.ap-southeast-1.amazonaws.com/', $attachment);
                                                                            if (isset($explode_attach[1])) {


                                                                                $attachment_link = Yii::$app->get('s3bucketwe')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');

                                                                                /*  echo $explode_attach[1];
                                                                                  die;*/

                                                                            } else {
                                                                                $explode_attach = explode('https://maxima-media.s3.eu-central-1.amazonaws.com/', $attachment);
                                                                                if (isset($explode_attach[1])) {
                                                                                    $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                                } else {

                                                                                    $explode_attach = explode('https://winsmills-wiz-prod.s3.eu-central-1.amazonaws.com/', $attachment);
                                                                                    if (isset($explode_attach[1])) {
                                                                                        $attachment_link = Yii::$app->get('olds3bucketwz')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                                    }else{
                                                                                        $explode_attach = explode('https://maxclientmodule.s3.ap-southeast-1.amazonaws.com/', $attachment);
                                                                                        $attachment_link = Yii::$app->get('olds3bucketcm')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                        if (strpos($attachment, '.pdf') !== false) {
                                                                            ?>
                                                                            <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment_link; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>

                                                                            <?php

                                                                        } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                                            ?>
                                                                            <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment_link; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <?php

                                                                        } else {
                                                                            ?>
                                                                            <img src="<?php echo $attachment_src; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment_link; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                                             alt="" title=""
                                                                             data-placeholder="no_image.png"/>
                                                                        <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                                                           target="_blank">
                                                                            <span class="glyphicon glyphicon-eye-open"></span>
                                                                        </a>
                                                                        <?php
                                                                    }

                                                                    ?>
                                                                </a>
                                                                <input type="hidden"
                                                                       name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][attachment]"
                                                                       value="<?= $attachment; ?>"
                                                                       id="input-attachment<?php echo $unit_row; ?>"/>
                                                                <input type="hidden"
                                                                       name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][document_id]"
                                                                       value="<?= $required_document; ?>"
                                                                       id="input-attachment<?php echo $unit_row; ?>"/>

                                                                <input type="hidden"
                                                                       name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][valuation_id]"
                                                                       value="<?= $valuation->id; ?>"
                                                                       id="input-history_id<?php echo $unit_row; ?>"/>


                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php $unit_row++; ?>

                                                <?php }

                                                ?>
                                                </tbody>
                                                <tfoot>

                                                </tfoot>
                                            </table>
                                            <?php } ?>
                                            
                                            
                                            
                                            
                                            <!--//optional documents working by usama-->
                                            
                                            <?php if($optional_documents <> null && !empty($optional_documents)) { ?>
                                            <table id="requestTypes"
                                                   class="table table-striped table-bordered table-hover images-table">
                                                <thead>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>Attachment</td>
                                                    <td></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php

                                                foreach ($optional_documents as $optional_document) {

                                                    if($optional_document == 9 ){
                                                        if (in_array($valuationDetail->property_id, [4,5,39,48,49,50,53,20,46]) && $valuationDetail->completion_status == 1) {
                                                            continue;
                                                        }
                                                    }
                                                    if($optional_document == 73){
                                                        if ($valuation->client->client_type !== "bank" ) {
                                                            continue;
                                                        }
                                                    }
        
                                                    if($optional_document == 1){
                                                        if ($valuation->client->client_type == "bank" ) {
                                                            continue;
                                                        }
                                                    }

                                                    if($optional_document == 66 || $optional_document == 67){
                                                        if ($valuationConfig->over_all_upgrade <= 3 ) {
                                                            continue;
                                                        }
                                                    }

                                                    $attachment_Details = \app\models\ReceivedDocsFiles::find()->where(["valuation_id" => $valuation->id, "document_id" => $optional_document])->one();
                                                    $attachment = $attachment_Details->attachment;
                                                    ?>

                                                    <tr id="image-row<?php echo $unit_row; ?>">

                                                        <td class="text-left">
                                                            <?= Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$optional_document] ?>

                                                        </td>

                                                        <td class="text-left upload-docs">
                                                            <div class="form-group">
                                                                <a href="javascript:;"
                                                                   id="upload-document<?= $unit_row; ?>"
                                                                   onclick="uploadAttachment(<?= $unit_row; ?>)"
                                                                   data-toggle="tooltip"
                                                                   class="img-thumbnail"
                                                                   title="Upload Document">
                                                                    <?php

                                                                    if ($attachment <> null) {

                                                                        $attachment_link= $attachment;
                                                                        $explode_attach_doc = explode('received-info/',$attachment);

                                                                        if(isset($explode_attach_doc[1])) {
                                                                            $attachment_src = 'https://maxima.windmillsgroup.com/cache/'.$explode_attach_doc[1];
                                                                        }else{
                                                                            $attachment_src = Yii::$app->params['uploadDocsIcon'];
                                                                        }
                                                                        $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/',$attachment);
                                                                        if(isset($explode_attach[1])) {
                                                                            $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                        }else {
                                                                            $explode_attach = explode('https://newcliudfrontaclmax.s3.ap-southeast-1.amazonaws.com/', $attachment);
                                                                            if (isset($explode_attach[1])) {


                                                                                $attachment_link = Yii::$app->get('s3bucketwe')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');

                                                                                /*  echo $explode_attach[1];
                                                                                  die;*/

                                                                            } else {
                                                                                $explode_attach = explode('https://maxima-media.s3.eu-central-1.amazonaws.com/', $attachment);
                                                                                if (isset($explode_attach[1])) {
                                                                                    $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                                } else {

                                                                                    $explode_attach = explode('https://winsmills-wiz-prod.s3.eu-central-1.amazonaws.com/', $attachment);
                                                                                    if (isset($explode_attach[1])) {
                                                                                        $attachment_link = Yii::$app->get('olds3bucketwz')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                                    }else{
                                                                                        $explode_attach = explode('https://maxclientmodule.s3.ap-southeast-1.amazonaws.com/', $attachment);
                                                                                        $attachment_link = Yii::$app->get('olds3bucketcm')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                        if (strpos($attachment, '.pdf') !== false) {
                                                                            ?>
                                                                            <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment_link; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>

                                                                            <?php

                                                                        } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                                            ?>
                                                                            <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment_link; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <?php

                                                                        } else {
                                                                            ?>
                                                                            <img src="<?php echo $attachment_src; ?>"
                                                                                 alt="" title=""
                                                                                 data-placeholder="no_image.png"/>
                                                                            <a href="<?= $attachment_link; ?>"
                                                                               target="_blank">
                                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                                            </a>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                                             alt="" title=""
                                                                             data-placeholder="no_image.png"/>
                                                                        <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                                                           target="_blank">
                                                                            <span class="glyphicon glyphicon-eye-open"></span>
                                                                        </a>
                                                                        <?php
                                                                    }

                                                                    ?>
                                                                </a>
                                                                <input type="hidden"
                                                                       name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][attachment]"
                                                                       value="<?= $attachment; ?>"
                                                                       id="input-attachment<?php echo $unit_row; ?>"/>
                                                                <input type="hidden"
                                                                       name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][document_id]"
                                                                       value="<?= $optional_document; ?>"
                                                                       id="input-attachment<?php echo $unit_row; ?>"/>

                                                                <input type="hidden"
                                                                       name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][valuation_id]"
                                                                       value="<?= $valuation->id; ?>"
                                                                       id="input-history_id<?php echo $unit_row; ?>"/>


                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php $unit_row++; ?>

                                                <?php }

                                                ?>
                                                </tbody>
                                                <tfoot>

                                                </tfoot>
                                            </table>
                                            <?php } ?>
                                            
                                            


                                        </div>
                                    </div>

                                    <?php $model->step=1;  ?>
                                    <?php echo $form->field($model, 'step')->textInput(['maxlength' => true,
                                        'type'=>'hidden'])->label('') ?>
                                </section>

                                <section class="card mx-4" style="border-top:2px solid #FFA500;">
                                    <header class="card-header">
                                        <h2 class="card-title"><strong>Additional Documents Provided by Client</strong></h2>

                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool add-km-image btn-warning text-dark"
                                                    title="Additional Document Image">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                    </header>
                                    <div class="card-body">
                                        <div class="row" id="km-table">
                                            <?php
                                            if($valuation->kmImages<>null){
                                                foreach($valuation->kmImages as $key => $image){
                                                   $attachment = $image->attachment;

                                                    $attachment_link= $image->attachment;
                                                    $explode_attach_doc = explode('received-info/',$image->attachment);

                                                    if(isset($explode_attach_doc[1])) {
                                                        $attachment_src = 'https://maxima.windmillsgroup.com/cache/'.$explode_attach_doc[1];
                                                    }else{
                                                        $attachment_src = Yii::$app->params['uploadDocsIcon'];
                                                    }
                                                    $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/',$image->attachment);
                                                    if(isset($explode_attach[1])) {
                                                        $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                    }else {
                                                        $explode_attach = explode('https://newcliudfrontaclmax.s3.ap-southeast-1.amazonaws.com/', $image->attachment);
                                                        if (isset($explode_attach[1])) {


                                                            $attachment_link = Yii::$app->get('s3bucketwe')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');

                                                            /*  echo $explode_attach[1];
                                                              die;*/

                                                        } else {
                                                            $explode_attach = explode('https://maxima-media.s3.eu-central-1.amazonaws.com/', $image->attachment);
                                                            $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                        }
                                                    }






                                                    if (strpos($attachment, '.pdf') !== false) {
                                                        $attachment_src = Yii::$app->params['uploadPdfIcon'];
                                                    } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                        $attachment_src =Yii::$app->params['uploadDocsIcon'];
                                                    } else {
                                                        $attachment_icon = $image->attachment;
                                                    }

                                                    ?>
                                                    <div class="col-2 my-2 upload-docs" id="image-row<?=$atch_count?>">
                                                        <div class="form-group">
                                                            <a href="javascript:;" id="upload-document<?= $atch_count ?>"
                                                               data-uploadid=<?= $atch_count ?>  data-toggle="tooltip"
                                                               class="img-thumbnail open-img-window" title="Upload Document">

                                                                <img src="<?= $attachment_src ?>" alt=""
                                                                     title="" data-placeholder="no_image.png" />
                                                            </a>
                                                            <a href="<?= $attachment_link ?>" class="mx-2" target="_blank">
                                                                <i class="fa fa-eye text-primary"></i>
                                                            </a>
                                                            <input type="hidden"
                                                                   name="ReceivedDocs[km_images][<?= $atch_count ?>][attachment]"
                                                                   id="input-attachment<?=$atch_count?>"
                                                                   value="<?= $image->attachment ?>" />
                                                            <input type="hidden"
                                                                   name="ReceivedDocs[km_images][<?= $atch_count ?>][db_id]"
                                                                   value="<?= $image->id ?>" />
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $atch_count++;
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </section>


                            </div>
                            </fieldset>

                            <?php ActiveForm::end(); ?>
                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>





