
<?php

use app\models\Company;
use app\models\InspectProperty;
use app\models\User;
use app\models\Valuation;
use app\models\ValuationApproversData;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\assets\SortBootstrapAsset;
SortBootstrapAsset::register($this);
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);

use yii\widgets\ActiveForm;
use kartik\select2\Select2;



$this->title = Yii::t('app', $page_title);
$cardTitle = Yii::t('app', $page_title);
$this->params['breadcrumbs'][] = $this->title;

$propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$propertiesArr = ['' => 'select'] + $propertiesArr;

$communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$communitiesArr = ['' => 'select'] + $communitiesArr;

$citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
$citiesArr = ['' => 'select'] + $citiesArr;

$allowedId = [1,14,33,110465];
?>

<style>


    .dataTable th {
        color: #0056b3;
        font-size: 15px;
        text-align: left !important;
        /* padding-right: 30px !important; */
    }
    .dataTable td {
        font-size: 16px;
        text-align: left;
        /* padding-right: 50px; */
        padding-top: 3px;
        max-width: 100px;
    }

    .content-header h1 {
        font-size: 16px !important;

    }

    .content-header .row {
        margin-bottom: 0px !important;
    }

    .content-header {
        padding: 0px !important;
    }

<?php if($page_title == "Pending Valuation Performance" || $page_title == "Pending Valuation Received") { ?>

    .custom-footer .dataTables_length{
        clear: both !important;
    }
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(3) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(4) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(5) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(6) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(7) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(8) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(9) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(10) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(11) {
        min-width:  70px;
        max-width:  70px;
    }
<?php } else if( $page_title == "Pending Inspection Scheduled") { ?>
    .custom-footer .dataTables_length{
        clear: both !important;
    }
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(3) {
        min-width:  55px;
        max-width:  55px;
    }

    th:nth-child(4) {
        min-width:  95px;
        max-width:  95px;
    }
    <?php if(!in_array(Yii::$app->user->identity->permission_group_id, [40,3,9]))
    { ?>
    
    th:nth-child(5) {
        min-width:  40px;
        max-width:  40px;
    }

    <?php } ?>
    th:nth-child(6) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(7) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(8) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(9) {
        min-width:  30px;
        max-width:  30px;
    }
    th:nth-child(10) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(11) {
        min-width:  55px;
        max-width:  55px;
    }
    th:nth-child(12) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(13) {
        min-width:  0px;
        max-width:  0px;
    }
    th:nth-child(14) {
        min-width:  45px;
        max-width:  45px;
    }
<?php } else if($page_title == "Pending Documents Requested") { ?>
    .custom-footer .dataTables_length{
        clear: both !important;
    }
    .table.dataTable thead > tr > th.sorting{
        padding-right: 0px !important;
        padding-left: 10px !important;
    }
    th:nth-child(1) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(2) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(3) {
        min-width:  85px;
        max-width:  85px;
    }

    th:nth-child(4) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(5) {
        min-width:  40px;
        max-width:  40px;
    }
    th:nth-child(6) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(7) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(8) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(9) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(10) {
        min-width:  55px;
        max-width:  55px;
    }
    th:nth-child(11) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(12) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(13) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(14) {
        min-width:  45px;
        max-width:  45px;
    }
<?php } else if($page_title == "Pending Inspected") { ?>
    .custom-footer .dataTables_length{
        clear: both !important;
    }
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(3) {
        min-width:  70px;
        max-width:  70px;
    }

    th:nth-child(4) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(5) {
        min-width:  95px;
        max-width:  95px;
    }
    th:nth-child(6) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(7) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(8) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(9) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(10) {
        min-width:  30px;
        max-width:  30px;
    }
    th:nth-child(11) {
        min-width:  40px;
        max-width:  40px;
    }
    th:nth-child(12) {
        min-width:  45px;
        max-width:  45px;
    }
<?php } else if($page_title == "Pending Recommended") { ?>
    .custom-footer .dataTables_length{
        clear: both !important;
    }
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(3) {
        min-width:  70px;
        max-width:  70px;
    }

    th:nth-child(4) {
        min-width:  95px;
        max-width:  95px;
    }
    th:nth-child(5) {
        min-width:  40px;
        max-width:  40px;
    }
    th:nth-child(6) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(7) {
        min-width:  63px;
        max-width:  63px;
    }
    th:nth-child(8) {
        min-width:  40px;
        max-width:  40px;
    }
    th:nth-child(9) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(10) {
        min-width:  55px;
        max-width:  55px;
    }
    th:nth-child(11) {
        min-width:  55px;
        max-width:  55px;
    }
    th:nth-child(12) {
        min-width:  45px;
        max-width:  45px;
    }
<?php } else if($page_title == "Pending Review") {  ?>
    .custom-footer .dataTables_length{
        clear: both !important;
    }
    .table.dataTable thead > tr > th.sorting{
        padding-right: 21px !important;
    }
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(3) {
        min-width:  60px;
        max-width:  60px;
    }

    th:nth-child(4) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(5) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(6) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(7) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(8) {
        min-width:  65px;
        max-width:  65px;
    }
    th:nth-child(9) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(10) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(11) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(12) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(13) {
        min-width:  45px;
        max-width:  45px;
    }
<?php } else if($page_title == "TAT Reports Pending") { ?>
    .custom-footer .dataTables_length{
        clear: both !important;
    }
    .table.dataTable thead > tr > th.sorting{
        padding-right: 0px !important;
    }
    th:nth-child(1) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(2) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(3) {
        min-width:  85px;
        max-width:  85px;
    }

    th:nth-child(4) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(5) {
        min-width:  90px;
        max-width:  90px;
    }
    th:nth-child(6) {
        min-width:  90px;
        max-width:  90px;
    }
    th:nth-child(7) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(8) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(9) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(10) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(11) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(12) {
        min-width:  90px;
        max-width:  90px;
    }
    th:nth-child(13) {
        min-width:  80px;
        max-width:  80px;
    }
<?php } else if($page_title == "Pending Delayed") { ?>
    .custom-footer .dataTables_length{
        clear: both !important;
    }
    th:nth-child(1) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(2) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(3) {
        min-width:  70px;
        max-width:  70px;
    }

    th:nth-child(4) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(5) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(6) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(7) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(8) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(9) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(10) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(11) {
        min-width:  70px;
        max-width:  70px;
    }
<?php } else if($page_title == "Pending Reminders") { ?>
    .custom-footer .dataTables_length{
        clear: both !important;
    }
    .table.dataTable thead > tr > th.sorting{
        padding-right: 0px !important;
        padding-left: 6px !important;
    }
    th:nth-child(1) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(2) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(3) {
        min-width:  85px;
        max-width:  85px;
    }

    th:nth-child(4) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(5) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(6) {
        min-width:  85px;
        max-width:  85px;
    }
    th:nth-child(7) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(8) {
        min-width:  45px;
        max-width:  45px;
    }
    th:nth-child(9) {
        min-width:  45px;
        max-width:  45px;
    }
    th:nth-child(10) {
        min-width:  40px;
        max-width:  40px;
    }
    th:nth-child(11) {
        min-width:  55px;
        max-width:  55px;
    }
    th:nth-child(12) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(13) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(14) {
        min-width:  70px;
        max-width:  70px;
    }
    <?php } ?>


</style>

<?php
// echo "<pre>";
// print_r($model);
// echo "<pre>";die;
?>
<input type="hidden" id="comm-id" value='<?php echo $page_title; ?>'>

<?php if($page_title == "Pending Valuation Performance" || $page_title == "Pending Valuation Received") { ?>
    
    <div class="card card-outline card-warning mx-2">
        <div class="card-body search_filter">
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label> General Search : </label>
                        <input type="search" id="custom-search-input-text" placeholder="Search from datatable" class="form-control custom-search-input-text">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="valuation-search">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="pending_received-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <span><strong><?= $page_title ?></strong></span><br /><br /><br />
                <table id="pending_received" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>
                        <th class="">Instruction Date
                            <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                        </th>

                        <th class="">Reference Number
                            <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                        </th>

                        <th class="">Client Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Ref#">
                        </th>

                        <th class="">Client
                            <input type="text" class="custom-search-input form-control" placeholder="Client">
                        </th>

                        <th class="">Property
                            <input type="text" class="custom-search-input form-control" placeholder="Property">
                        </th>

                        <th class="">City
                            <input type="text" class="custom-search-input form-control" placeholder="text">
                        </th>

                        <th class="">Inspector
                            <input type="text" class="custom-search-input form-control" placeholder="I/O">
                        </th>

                        <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th>

                        <?php if(in_array( Yii::$app->user->identity->id, $allowedId)) { ?>
                        <th class="">Fee
                            <input type="text" class="custom-search-input form-control" placeholder="Fee">
                        </th>
                        <?php } ?>

                        <th class="">Status
                            <input type="text" class="custom-search-input form-control" placeholder="status">
                        </th>

                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($dataProvider->getModels() as $model){
                            // dd($model);
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?php
                                    echo date('d-M-Y', strtotime($model->instruction_date));
                                    echo "<br>";
                                    if(isset($model->instruction_time))
                                    {
                                        echo date('h:i A', strtotime($model->instruction_time));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- WM Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Reference -->
                                <td>
                                    <?php echo $model->client_reference; ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?php
                                    echo $model->client->nick_name;
                                    ?>
                                </td>

                                <!-- Property -->
                                <td>
                                    <?php
                                    echo $model->property->title;
                                    ?>
                                </td>

                                <!-- City -->
                                <td>
                                    <?php
                                    echo Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];
                                    ?>
                                </td>

                                <!-- Inspector -->
                                <td>
                                    <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                                </td>

                                <!-- Valuer -->
                                <td>
                                    <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                    ?>
                                </td>

                                <?php if(in_array( Yii::$app->user->identity->id, $allowedId)) { ?>
                                    <!-- Fee  -->
                                    <td>
                                        <?php
                                        echo $total_fee = Yii::$app->appHelperFunctions->wmFormate($model->total_fee);
                                        ?>
                                    </td>
                                <?php } ?>

                                <!-- Status -->
                                <td>
                                    <?= Yii::$app->helperFunctions->valuationStatusListArrWords[$model->valuation_status] ?>
                                </td>

                                <?php
                                if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                                    ?>
                                    <td class="noprint actions">
                                        <div class="btn-group flex-wrap">
                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <a class="dropdown-item text-1"
                                                   href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                                   title="Step 1" data-pjax="0">
                                                    <i class="fas fa-edit"></i> View
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <?php if(in_array( Yii::$app->user->identity->id, $allowedId)) { ?>
                        <th>Total Fee : </th>
                        <th><?= Yii::$app->appHelperFunctions->wmFormate(\app\models\Valuation::getTotal($dataProvider->models, 'total_fee')) ?></th>
                    <?php } ?>
                        <th></th>
                    <?php
                    if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <th></th>
                    <?php } ?>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php
    $this->registerJs('

    $(".div1").daterangepicker({
        autoUpdateInput: false,
          locale: {
          format: "YYYY-MM-DD"
        }
     
     
      });
     
      $(".div1").on("apply.daterangepicker", function(ev, picker) {
         $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
     });

     $(\'#valuationsearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
     
                
        var dataTable = $("#pending_received").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    dataTable.search(data).draw();
                },
                error: bbAlert
                });
            });
                
          $("#pending_received_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });

          //for date search instruction_date
          $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
              var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });

          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_received_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_received_wrapper").append(customFooter);
          
          
    ');
    ?>
<?php } ?>

<?php  if($page_title == "Pending Inspection Scheduled") { ?>

    <div class="card card-outline card-warning mx-2">
        <div class="card-body search_filter">
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label> General Search : </label>
                        <input type="search" id="custom-search-input-text" placeholder="Search from datatable" class="form-control custom-search-input-text">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="valuation-search">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>


    <div class="insp_scheduled-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
            <span><strong><?= "Pending Inspection Scheduled"; ?></strong></span><br /><br /><br />
                <table id="insp_scheduled" class="table table-striped dataTable table-responsive">
                    <thead>
                        <tr>
                            <th class="">Instruction Date
                                <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <th class="">Inspection Schedule
                                <input id="schedule_date" type="date" class="form-control" placeholder="Date">
                            </th>

                            <!-- <th class="">Inspection Done
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th> -->

                            <th class="">Ref. Number
                                <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                            </th>

                            <th class="">Client Ref. No
                                <input type="text" class="custom-search-input form-control" placeholder="Ref#">
                            </th>

                            <?php if(!in_array(Yii::$app->user->identity->permission_group_id, [40,3,9]))
                            { ?>
                            <th class="">Client
                                <input type="text" class="custom-search-input form-control" placeholder="Client">
                            </th>
                            <?php } ?>


                            <th class="">Property
                                <input type="text" class="custom-search-input form-control" placeholder="Property">
                            </th>
                            
                            <th style="padding: 0px 0px 10px 0px;" class="">Community
                                <input type="text" class="custom-search-input form-control" placeholder="Community">
                            </th>

                            <th class="">Project / Building
                                <input type="text" class="custom-search-input form-control" placeholder="Building">
                            </th>

                            <th class="">City
                                <input type="text" class="custom-search-input form-control" placeholder="City">
                            </th>

                            <th class="">Inspector
                                <input type="text" class="custom-search-input form-control" placeholder="I/O">
                            </th>

                            <th class="">Contact Person
                                <input type="text" class="custom-search-input form-control" placeholder="Person">
                            </th>

                            <th class="">Contact Phone
                                <input type="text" class="custom-search-input form-control" placeholder="Phone">
                            </th>

                            <th></th>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($dataProvider->getModels())>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($dataProvider->getModels() as $model){
                        ?>
                        <tr class="active">

                            <!-- Instruction -->
                            <td>
                                <?php  
                                    echo date('d-M-Y', strtotime($model->instruction_date));
                                    echo "<br>";
                                    if(isset($model->instruction_time))
                                    {
                                        echo date('h:i A', strtotime($model->instruction_time));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Inspection Schedule -->
                            <td>
                                <?php 
                                    if(isset($model->scheduleInspection->inspection_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->scheduleInspection->inspection_date));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($model->scheduleInspection->inspection_time));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Inspection Done -->
                            <!-- <td>
                                <?php   
                                    $data = InspectProperty::find()->where(['valuation_id' => $model->id])->one();
                                    if(isset($data->inspection_done_date))
                                    {
                                        echo   date('d-M-Y', strtotime($data->inspection_done_date));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td> -->

                            <!-- Reference Number -->
                            <td>
                                <?= $model->reference_number ?>
                            </td>

                            <!-- Client ref -->
                            <td>
                                <?= $model->client_reference ?>
                            </td>
                               
                            <?php if(!in_array(Yii::$app->user->identity->permission_group_id, [40,3,9]))
                            { ?>
                            <!-- Client -->
                            <td>
                                <?= $model->client->nick_name ?>
                            </td>
                            <?php } ?>

                            <!-- Property -->
                            <td>
                                <?= $model->property->title ?>
                            </td>

                            <!-- Community -->
                            <td>
                                <?= $model->building->communities->title ?>
                            </td>

                            <!-- Project / Building -->
                            <td>
                                <?= $model->building->title ?>
                            </td>

                            <!-- City -->
                            <td>
                                <?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
                            </td>

                            <!-- Inspection Officer -->
                            <td>
                                <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                            </td>

                            <!-- Contact Person -->
                            <td>
                                <?php
                                    if(isset($model->contact_person_name))
                                    {
                                        echo $model->contact_person_name;
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Contact Phone -->
                            <td>
                                <?php
                                    if ($model->contact_phone_no <> null) $contact_number = $model->phone_code . '' . $model->contact_phone_no;
                                    if ($model->land_line_no <> null) $contact_number = $model->land_line_code . '' . $model->land_line_no;
                                    echo $contact_number;
                                ?>
                            </td>

                            <td style="text-align:center;"> <a href="javascript:;" class="showModalButton" data-valuation-ref="<?= $model->reference_number ?>" data-valuation-id="<?= $model->id ?>"><i class="fas fa-eye text-success"></i></a> </td>

                            <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                            href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                            title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> View 
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <?php
                            }
                            ?>

                        </tr>
                        <?php
                    }
                }
            ?>
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <?php if(!in_array(Yii::$app->user->identity->permission_group_id, [40,3,9]))
                        { ?>
                        <th></th>
                        <?php } ?>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <?php 
                            if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                        <th></th>
                        <?php } ?>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?php
    $this->registerJs('

    $(".div1").daterangepicker({
        autoUpdateInput: false,
          locale: {
          format: "YYYY-MM-DD"
        }
     
     
      });
     
      $(".div1").on("apply.daterangepicker", function(ev, picker) {
         $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
     });

     $(\'#valuationsearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
      
                
        var dataTable = $("#insp_scheduled").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    dataTable.search(data).draw();
                },
                error: bbAlert
                });
            });
                
          $("#insp_scheduled_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });

          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });

          //for date search instruction_date
          $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
              var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });

          //for date search schedule_date
          $("#schedule_date").on("change", function () {
            if($("#schedule_date").val() != ""){
              var format1 = moment($("#schedule_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#insp_scheduled_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#insp_scheduled_wrapper").append(customFooter);

          $(".showModalButton").on("click", function (event) {
            // console.log(event.delegateTarget.dataset.valuationRef)
            // console.log(event.delegateTarget)
            // $id = $(".showModalButton").attr("data-valuation-id");
    
            // model_id = event.delegateTarget.dataset.valuationId;
            // model_ref = event.delegateTarget.dataset.valuationRef;
    
            // $(".modal-title").text(model_ref)
    
            // document.getElementById("id1").value=model_id ; 
    
            // $("#pending_modal").modal("show");
    
            $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/get_pending_inspection_reasons']).'/"+$(this).attr("data-valuation-id"),
                method: "post",
                dataType: "html",
                success: function(data) {
                //   console.log(data);
                $("#modalContainer").html("");
                  $("#modalContainer").append(data);
                  $("#modalContainer").modal("show");
                },
                error: bbAlert
            });
        });
    
        $("#modalContainer").on("hidden.bs.modal", function() {
            $(this).find(".modal-dialog").empty();
        });
          
          
    ');
    ?>
<?php } ?>

<?php if($page_title == "Pending Documents Requested") { ?>

    <br>

    <div class="valuation-search">
        <?php $form = ActiveForm::begin([
            'action' => ['dashboard-report-all'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="doc_table-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
                <table id="doc_table" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>
                        <th class="">Instruction Date
                            <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Created
                            <input id="created_date" type="date" class="form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Done
                            <input id="done_date" type="date" class="form-control" placeholder="Date">
                        </th>

                        <th class="">Reference Number
                            <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                        </th>

                        <th class="">Client Ref
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>

                        <th class="">Client
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Property
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Community
                            <input type="text" class="custom-search-input form-control" placeholder="TAT">
                        </th>

                        <th class="">City
                            <input type="text" class="custom-search-input form-control" placeholder="City">
                        </th>

                        <th class="">Inspector
                            <input type="text" class="custom-search-input form-control" placeholder="I/O">
                        </th>

                        <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th>

                        <th class="">Total TAT Taken
                            <input type="text" class="custom-search-input form-control" placeholder="TAT">
                        </th>

                        <th class="">Reason
                        </th>

                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd($dataProvider->getModels());
                        foreach($dataProvider->getModels() as $model){
                            // dd($model->scheduleInspection->created_at);
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                                    <br>
                                    <?php
                                    if(isset($model->instruction_time))
                                    {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time);
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Inspection Created -->
                                <td>
                                    <?php
                                    if(isset($model->scheduleInspection->created_at))
                                    {
                                        echo date('d-M-Y', strtotime($model->scheduleInspection->created_at));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($model->scheduleInspection->created_at));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Inspection Done -->
                                <td>
                                    <?php
                                    $data = InspectProperty::find()->where(['valuation_id' => $model->id])->one();
                                    if(isset($data->inspection_done_date))
                                    {
                                        echo   date('d-M-Y', strtotime($data->inspection_done_date));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($data->inspection_done_date));
                                    }else{
                                        echo "";
                                    }

                                    ?>
                                </td>

                                <!-- Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Ref -->
                                <td>
                                    <?= $model->client_reference ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?= $model->client->nick_name ?>
                                </td>

                                <!--  Property -->
                                <td>
                                    <?= $model->property->title ?>
                                </td>

                                <!-- Community -->
                                <td>
                                    <?= $model->building->communities->title ?>
                                </td>

                                <!-- City -->
                                <td>
                                    <?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
                                </td>

                                <!-- Inspection Officer -->
                                <td>
                                    <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                                </td>

                                <!-- Valuer -->
                                <td>
                                    <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                    ?>
                                </td>

                                <!-- TAT Taken -->
                                <td>
                                    <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        $inspection_time = $model->scheduleInspection->inspection_time . ":00";
                                        if ($inspection_time == null) {
                                            $inspection_time = '00:00:00';
                                        }

                                        if(!empty($model->scheduleInspection->inspection_date) && !empty($model->submission_approver_date))
                                        {
                                            $startDate = $model->scheduleInspection->inspection_date . ' ' .$inspection_time;
                                            $endDate = $model->submission_approver_date;

                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);

                                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                                            echo $test = abs(number_format(($workingHours/8.5),1)).'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';
                                        }
                                    }else{
                                        echo "Desktop Valuation";
                                    }
                                    ?>
                                </td>

                                <!-- Reason -->
                                <td style="text-align:center;">
                                    <a href="javascript:;" class="view-reasons-btn" data-valuation-id="<?= $model->id ?>">
                                        <i class="fas fa-eye text-success"></i>
                                    </a>
                                </td>

                                <?php
                                if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                                    ?>
                                    <td class="noprint actions">
                                        <div class="btn-group flex-wrap">
                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <a class="dropdown-item text-1"
                                                   href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                                   title="Step 1" data-pjax="0">
                                                    <i class="fas fa-edit"></i> View
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?php
    $this->registerJs('

    $(".div1").daterangepicker({
        autoUpdateInput: false,
          locale: {
          format: "YYYY-MM-DD"
        }
     
     
      });
     
      $(".div1").on("apply.daterangepicker", function(ev, picker) {
         $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
     });

     $(\'#valuationsearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
       
                
        var dataTable = $("#doc_table").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                   if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
          });

          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });

          //for date search instruction_date
          $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
              var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });

          //for date search created_date
          $("#created_date").on("change", function () {
            if($("#created_date").val() != ""){
              var format1 = moment($("#created_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });

          //for date search done_date
          $("#done_date").on("change", function () {
            if($("#done_date").val() != ""){
              var format1 = moment($("#done_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(2).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    dataTable.search(data).draw();
                },
                error: bbAlert
                });
            });
                
          $("#doc_table_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#doc_table_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#doc_table_wrapper").append(customFooter);
          
          
    ');
    ?>
<?php } ?>

<?php if($page_title == "Pending Inspected") { ?>

    <br>

    <div class="valuation-search">
        <?php $form = ActiveForm::begin([
            'action' => ['dashboard-report-all'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="pending_inspected-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
                <table id="pending_inspected" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>
                        <th class="">Instruction Date
                            <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Schedule
                            <input id="schedule_date" type="date" class="form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Done
                            <input id="done_date" type="date" class="form-control" placeholder="Date">
                        </th>

                        <th class="">Reference Number
                            <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                        </th>

                        <th class="">Client Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>

                        <th class="">Client
                            <input type="text" class="custom-search-input form-control" placeholder="Client">
                        </th>

                        <th class="">Property
                            <input type="text" class="custom-search-input form-control" placeholder="Property">
                        </th>

                        <th class="">Project/Building
                            <input type="text" class="custom-search-input form-control" placeholder="Building">
                        </th>

                        <th class="">Comm unity
                            <input type="text" class="custom-search-input form-control" placeholder="Community">
                        </th>

                        <th class="">City
                            <input type="text" class="custom-search-input form-control" placeholder="City">
                        </th>

                        <th class="">Inspector
                            <input type="text" class="custom-search-input form-control" placeholder="I/O">
                        </th>

                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd($dataProvider->getModels());
                        foreach($dataProvider->getModels() as $model){
                            // dd($model->inspectProperty->inspection_done_date);
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?php
                                    echo date('d-M-Y', strtotime($model->instruction_date));
                                    echo "<br>";
                                    if(isset($model->instruction_time))
                                    {
                                        echo date('h:i A', strtotime($model->instruction_time));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Inspection Schedule -->
                                <td>
                                    <?php
                                    if(isset($model->scheduleInspection->inspection_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->scheduleInspection->inspection_date));
                                    }else{
                                        echo "";
                                    }
                                    echo "<br>";
                                    if(isset($model->inspection_time))
                                    {
                                        echo date('h:i A', strtotime($model->inspection_time));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Inspection Done -->
                                <td>
                                    <?php
                                    if(isset($model->inspectProperty->inspection_done_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->inspectProperty->inspection_done_date));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($model->inspectProperty->inspection_done_date));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Ref -->
                                <td>
                                    <?= $model->client_reference ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?= $model->client->nick_name ?>
                                </td>

                                <!--  Property -->
                                <td>
                                    <?= $model->property->title ?>
                                </td>

                                <!-- Project / Building -->
                                <td>
                                    <?= $model->building->title ?>
                                </td>

                                <!-- Community -->
                                <td>
                                    <?= $model->building->communities->title ?>
                                </td>

                                <!-- City -->
                                <td>
                                    <?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
                                </td>

                                <!-- Inspection Officer -->
                                <td>
                                    <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                                </td>

                                <?php
                                if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                                    ?>
                                    <td class="noprint actions">
                                        <div class="btn-group flex-wrap">
                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <a class="dropdown-item text-1"
                                                   href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                                   title="Step 1" data-pjax="0">
                                                    <i class="fas fa-edit"></i> View
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?php
    $this->registerJs('

    $(".div1").daterangepicker({
        autoUpdateInput: false,
          locale: {
          format: "YYYY-MM-DD"
        }
     
     
      });
     
      $(".div1").on("apply.daterangepicker", function(ev, picker) {
         $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
     });

     $(\'#valuationsearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
       
                
        var dataTable = $("#pending_inspected").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    dataTable.search(data).draw();
                },
                error: bbAlert
                });
            });
                
          $("#pending_inspected_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });

          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });

          //for date search instruction_date
          $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
              var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });

          //for date search schedule_date
          $("#schedule_date").on("change", function () {
            if($("#schedule_date").val() != ""){
              var format1 = moment($("#schedule_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });

          //for date search done_date
          $("#done_date").on("change", function () {
            if($("#done_date").val() != ""){
              var format1 = moment($("#done_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(2).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_inspected_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_inspected_wrapper").append(customFooter);
          
          
    ');
    ?>
<?php } ?>

<?php if($page_title == "Pending Recommended") { ?>

    <div class="card card-outline card-warning mx-2">
        <div class="card-body search_filter">
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label> General Search : </label>
                        <input type="search" id="custom-search-input-text" placeholder="Search from datatable" class="form-control custom-search-input-text">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="valuation-search">
        <?php $form = ActiveForm::begin([
            'action' => ['recommended-pending-list'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="pending_recom-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
                <table id="pending_recom" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>
                        <th class="">Instruction Date
                            <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Done
                            <input id="inspection_date" type="date" class="form-control" placeholder="Date">
                        </th>

                        <th class="">Reference Number
                            <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                        </th>

                        <th class="">Client Reference
                            <input type="text" class="custom-search-input form-control" placeholder="Ref">
                        </th>

                        <th class="">Client
                            <input type="text" class="custom-search-input form-control" placeholder="Client">
                        </th>

                        <th class="">Property
                            <input type="text" class="custom-search-input form-control" placeholder="Property">
                        </th>

                        <th class="">Inspector
                            <input type="text" class="custom-search-input form-control" placeholder="I/O">
                        </th>

                        <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th>

                        <th class="">Reviewer
                            <input type="text" class="custom-search-input form-control" placeholder="Reviewer">
                        </th>

                        <th class="">Market Value Recom
                            <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                        </th>

                        <th class="">Revised Market Value Recom
                            <input type="text" class="custom-search-input form-control" placeholder="Rev. Market Val">
                        </th>

                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd($dataProvider->getModels());
                        foreach($dataProvider->getModels() as $model){
                            // dd($model->inspectProperty);
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?php
                                    echo date('d-M-Y', strtotime($model->instruction_date));
                                    echo "<br>";
                                    if(isset($model->instruction_time))
                                    {
                                        echo date('h:i A', strtotime($model->instruction_time));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Inspection Done -->
                                <td>
                                    <?php
                                    if(isset($model->inspectProperty->inspection_done_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->inspectProperty->inspection_done_date));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($model->inspectProperty->inspection_done_date));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Ref -->
                                <td>
                                    <?= $model->client_reference ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?= $model->client->nick_name ?>
                                </td>

                                <!-- Property -->
                                <td>
                                    <?= $model->property->title ?>
                                </td>

                                <!-- Inspector -->
                                <td>
                                    <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                                </td>

                                <!-- Valuer -->
                                <td>
                                    <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                    ?>
                                </td>

                                <!-- Reviewer -->
                                <td>
                                    <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                        ->andWhere(['approver_type' => "reviewer"])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->one();

                                    $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                    echo  $get_name->lastname;
                                    ?>
                                </td>

                                <!-- Market Value recomm -->
                                <td>
                                    <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                        ->andWhere(['approver_type' => 'valuer'])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->one();

                                    echo (isset($get_valuation_data->id) && ($get_valuation_data->id <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                                    ?>
                                </td>

                                <!-- Rev Market Value recomm -->
                                <td>
                                    <?php
                                    $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();

                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                                        ->andWhere(['approver_type' => 'valuer'])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->one();

                                    echo (isset($get_valuation_data->estimated_market_value) && ($get_valuation_data->estimated_market_value <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                                    ?>
                                </td>

                                <?php
                                if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                                    ?>
                                    <td class="noprint actions">
                                        <div class="btn-group flex-wrap">
                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <a class="dropdown-item text-1"
                                                   href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                                   title="Step 1" data-pjax="0">
                                                    <i class="fas fa-edit"></i> View
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?php
    $this->registerJs('
      

    $(".div1").daterangepicker({
        autoUpdateInput: false,
          locale: {
          format: "YYYY-MM-DD"
        }
     
     
      });
     
      $(".div1").on("apply.daterangepicker", function(ev, picker) {
         $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
     });
        
     $(\'#valuationsearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
                
        var dataTable = $("#pending_recom").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                  if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
          });
        
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });

          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });

          //for date search instruction_date
          $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
              var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });

          //for date search inspection_date
          $("#inspection_date").on("change", function () {
            if($("#inspection_date").val() != ""){
              var format1 = moment($("#inspection_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
          
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    dataTable.search(data).draw();
                },
                error: bbAlert
                });
            });
                
          $("#pending_recom_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_recom_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_recom_wrapper").append(customFooter);
          
          
    ');
    ?>
<?php } ?>

<?php if($page_title == "Pending Review") { ?>

    <div class="card card-outline card-warning mx-2">
        <div class="card-body search_filter">
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label> General Search : </label>
                        <input type="search" id="custom-search-input-text" placeholder="Search from datatable" class="form-control custom-search-input-text">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="valuation-search">
        <?php $form = ActiveForm::begin([
            'action' => ['reviewed-pending-list'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div7'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
    
    <div class="pending_review-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
                <table id="pending_review" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>

                        <th class="">Instruction Date
                            <input id="instruction_date" type="date" class="form-control" placeholder="Date">
                        </th>

                        <th class="">Recomm. Date
                            <input id="recom_date" type="date" class="form-control" placeholder="Date">
                        </th>

                        <th class="">WM Ref No
                            <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                        </th>

                        <th class="">Client Ref
                            <input type="text" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Client
                            <input type="text" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Property
                            <input type="text" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th>

                        <th class="">Reviewer
                            <input type="text" class="custom-search-input form-control" placeholder="Reviewer">
                        </th>

                        <th class="">Market Value Recom
                            <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                        </th>

                        <th class="">Market Value Review
                            <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                        </th>

                        <th class="">Revise Market Value Recom
                            <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                        </th>

                        <th class="">Revise Market Value Review
                            <input type="text" class="custom-search-input form-control" placeholder="Market Val">
                        </th>

                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd($dataProvider->getModels());
                        foreach($dataProvider->getModels() as $model){
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                <?php
                                    echo date('d-M-Y', strtotime($model->instruction_date));
                                    echo "<br>";
                                    if(isset($model->instruction_time))
                                    {
                                        echo date('h:i A', strtotime($model->instruction_time));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Recommended -->
                                <td>
                                    <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                        ->andWhere(['approver_type' => "valuer"])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->one();
                                    if(isset($get_valuation_data->updated_at))
                                    {
                                        echo date('d-M-Y', strtotime($get_valuation_data->updated_at));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($get_valuation_data->updated_at));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Ref -->
                                <td>
                                    <?= $model->client_reference ?>
                                </td>

                                <!-- Client -->
                                <td>
                                    <?= $model->client->nick_name ?>
                                </td>

                                <!-- Property -->
                                <td>
                                    <?= $model->property->title ?>
                                </td>

                                <!-- Valuer -->
                                <td>
                                    <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                    ?>
                                </td>


                                <!-- Reviewer -->
                                <td>
                                    <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                        ->andWhere(['approver_type' => "reviewer"])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->one();

                                    $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                    echo  $get_name->lastname;
                                    ?>
                                </td>

                                <!-- Market Val recomm -->
                                <td>
                                    <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                        ->andWhere(['approver_type' => "valuer"])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->one();
                                    if(isset($get_valuation_data->estimated_market_value))
                                    {
                                        echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Market Val Reviewed -->
                                <td>
                                    <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                        ->andWhere(['approver_type' => "reviewer"])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->one();
                                    if(isset($get_valuation_data->estimated_market_value))
                                    {
                                        echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Revised Market recomm -->
                                <td>
                                    <?php
                                    $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();

                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                                        ->andWhere(['approver_type' => "valuer"])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->one();
                                    if(isset($get_valuation_data->estimated_market_value))
                                    {
                                        echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Revised Market val reviewed -->
                                <td>
                                    <?php
                                    $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();

                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                                        ->andWhere(['approver_type' => "reviewer"])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->one();
                                    if(isset($get_valuation_data->estimated_market_value))
                                    {
                                        echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>


                                <?php
                                if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                                    ?>
                                    <td class="noprint actions">
                                        <div class="btn-group flex-wrap">
                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <a class="dropdown-item text-1"
                                                   href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                                   title="Step 1" data-pjax="0">
                                                    <i class="fas fa-edit"></i> View
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?php
    $this->registerJs('
     
    $(".div7").daterangepicker({
        autoUpdateInput: false,
          locale: {
          format: "YYYY-MM-DD"
        }
     
     
      });
     
      $(".div7").on("apply.daterangepicker", function(ev, picker) {
         $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
     });

     $(\'#valuationsearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
                
        var dataTable = $("#pending_review").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    dataTable.search(data).draw();
                },
                error: bbAlert
                });
            });
                
          $("#pending_review_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });

          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });

          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });

          //for date search instruction_date
          $("#instruction_date").on("change", function () {
            if($("#instruction_date").val() != ""){
              var format1 = moment($("#instruction_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(0).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });

          //for date search recom_date
          $("#recom_date").on("change", function () {
            if($("#recom_date").val() != ""){
              var format1 = moment($("#recom_date").val()).format("DD-MMM-YYYY"); 
              dataTable.column(1).search(format1).draw();
            }else{
              dataTable.search("").columns().search("").draw();
            }
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_review_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_review_wrapper").append(customFooter);
          
          
    ');
    ?>
<?php } ?>

<?php if($page_title == "TAT Reports Pending") { ?>
    <div class="tat_table-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
                <table id="tat_table" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>

                        <th class="">Instruction Date
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Created
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Schedule
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Done
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Recommended
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Approval Date
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Ref. No
                            <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                        </th>

                        <th class="">Inspect
                            <input type="text" class="custom-search-input form-control" placeholder="I/O">
                        </th>

                        <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th>

                        <th class="">Approver
                            <input type="text" class="custom-search-input form-control" placeholder="Approver">
                        </th>

                        <th class="">Reviewer
                            <input type="text" class="custom-search-input form-control" placeholder="Review">
                        </th>

                        <th class="">Total TAT
                            <input type="text" class="custom-search-input form-control" placeholder="TAT">
                        </th>

                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd($dataProvider->getModels());
                        foreach($dataProvider->getModels() as $model){
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                                    <br>
                                    <?php
                                    if(isset($model->instruction_time))
                                    {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time);
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Inspection Created -->
                                <td>
                                    <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        if($model->scheduleInspection->created_at != '' && $model->created_at !='') {

                                            $startDate = $model->created_at;
                                            $endDate = $model->scheduleInspection->created_at;


                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);

                                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                            if($model->reference_number == 'REV-2023-10737'){
                                                $workingHours = $workingHours - 8.5;
                                            }
                                        }else{
                                            $workingHours = 0;
                                        }

                                        echo date('d-M-Y', strtotime($model->scheduleInspection->created_at)) . '<br>  ' . date('h:i A', strtotime($model->scheduleInspection->created_at)).'<br>'.'<strong>'.number_format($workingHours,1).' Hours'.'</strong>';

                                    }else{
                                        echo 'Desktop Valuation';
                                    }
                                    ?>
                                </td>

                                <!-- Inspection Schedule -->
                                <td>
                                    <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        $inspection_time = $model->inspection_time . ":00";
                                        if ($model->inspection_time == null) {
                                            $inspection_time = '00:00:00';
                                        }
                                        if($model->scheduleInspection->created_at != '' && $model->scheduleInspection->inspection_date !='') {

                                            $minus = 0;

                                            $startDate = $model->scheduleInspection->created_at;
                                            $endDate = $model->scheduleInspection->inspection_date.' '.$inspection_time;


                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);

                                            $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                            if($model->reference_number == 'REV-2023-10772'){
                                                $workingHours_1 = $workingHours_1 - 8.5;
                                            }


                                        }else{
                                            $workingHours_1 = 0;
                                        }
                                        echo date('d-M-Y', strtotime($model->scheduleInspection->inspection_date)) . '<br>  ' . Yii::$app->appHelperFunctions->formatTimeAmPm($model->scheduleInspection->inspection_time).'<br>'.'<strong>'.number_format($workingHours_1,1).' Hours'.'</strong>';

                                    }else{
                                        echo 'Desktop Valuation';
                                    }
                                    ?>
                                </td>

                                <!-- Inspection Done -->
                                <td>
                                    <?php
                                    if($model->scheduleInspection->inspection_type != 3) {

                                        $inspection_time = $model->scheduleInspection->inspection_time . ":00";
                                        if ($model->scheduleInspection->inspection_time == null) {
                                            $inspection_time = '00:00:00';
                                        }
                                        if($model->inspection_done_date_time != '' && $model->scheduleInspection->inspection_date !='') {
                                            $minus = 0;

                                            //  $startDate = $model->scheduleInspection->created_at;
                                            $startDate = $model->scheduleInspection->inspection_date.' '.$inspection_time;
                                            $endDate = $model->inspection_done_date_time;

                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);

                                            $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                            /* if($model->reference_number == 'REV-2023-10772'){
                                                $workingHours = $workingHours - 8.5;
                                            }*/
                                        }else{
                                            $workingHours_2 = 0;
                                        }
                                        echo date('d-M-Y', strtotime($model->inspection_done_date_time)) . '<br>  ' . date('h:i A', strtotime($model->inspection_done_date_time)).'<br>'.'<strong>'.number_format($workingHours_2,1).' Hours'.'</strong>';
                                    }else{
                                        echo 'Desktop Valuation';
                                    }
                                    ?>
                                </td>

                                <!-- Recommended -->
                                <td>
                                    <?php
                                    if($model->inspection_done_date_time != '' && $model->valuerData->created_at!='') {

                                        $minus = 0;

                                        $startDate = $model->inspection_done_date_time;
                                        $endDate = $model->valuerData->created_at;

                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);

                                        $workingHours_2= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        if($model->reference_number == 'REV-2023-10756'){
                                            $workingHours_2 = $workingHours_2 - 8.5;
                                        }
                                    }else{
                                        $workingHours_2 = 0;
                                    }
                                    echo date('d-M-Y', strtotime($model->inspection_done_date_time)) . '<br>  ' . date('h:i A', strtotime($model->inspection_done_date_time)).'<br>'.'<strong>'.number_format($workingHours_2,1).' Hours'.'</strong>';
                                    ?>
                                </td>

                                <!-- Approval date -->
                                <td>
                                    <?php
                                    if(isset($model->submission_approver_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->submission_approver_date));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                    <br>
                                    <?php
                                    if(isset($model->submission_approver_date))
                                    {
                                        echo date('h:i A', strtotime($model->submission_approver_date));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Inspection Officer -->
                                <td>
                                    <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                                </td>

                                <!-- Valuer -->
                                <td>
                                    <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                    ?>
                                </td>

                                <!-- Approver -->
                                <td>
                                    <?php
                                    $type = $model->approverData->approver_type;
                                    if($type == "approver")
                                    {
                                        $get_name = User::find()->where(['id' => $model->approverData->created_by])->one();
                                        echo $get_name->lastname ;
                                    }else{
                                        echo '';
                                    }
                                    ?>
                                </td>

                                <!-- Reviewer -->
                                <td>
                                    <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                        ->andWhere(['approver_type' => "reviewer"])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->one();

                                    $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                    echo  $get_name->lastname;
                                    ?>
                                </td>

                                <!-- Total TAT -->
                                <td>
                                    <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        $inspection_time = $model->inspection_time . ":00";
                                        if ($model->inspection_time == null) {
                                            $inspection_time = '00:00:00';
                                        }
                                        if($model->instruction_date !='' && $model->inspection_date !='') {

                                            $startDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                            $endDate = $model->inspection_date . ' ' . $inspection_time;

                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);
                                            $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        }else{
                                            $workingHours_1 =0;
                                        }
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //  $time_1 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1));
                                        //  $time_1_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));
                                        if($model->inspection_date !='' && $model->submission_approver_date !='') {
                                            $startDate_2 = $model->inspection_date . ' ' . $inspection_time;
                                            $endDate_2 = $model->submission_approver_date;
                                            $startDateTime = new DateTime($startDate_2); // Start date and time
                                            $endDateTime = new DateTime($endDate_2);
                                            /*echo $startDate_2.' '.$endDate_2;
                                            die;*/
                                            $workingHours_2 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        }else{
                                            $workingHours_2=0;
                                        }
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //  $time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                        // $time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));



                                        // print_r( abs( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate ), 2) ) ); echo "<br>";
                                        // die;
                                        $workingHours = $workingHours_1 + $workingHours_2;
                                        if($model->reference_number == 'REV-2023-10737' || $model->reference_number == 'REV-2023-10756'){
                                            $workingHours = $workingHours - 8.5;
                                        }
                                        $test = abs(number_format(($workingHours/8.5),2)).'<br>'.'<br>'.'<strong>'.number_format($workingHours,1).' Hours'.'</strong>';
                                        echo $test;

                                    }else{
                                        $startDate_2 = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                        $endDate_2 = $model->submission_approver_date;
                                        $startDateTime = new DateTime($startDate_2); // Start date and time
                                        $endDateTime = new DateTime($endDate_2);
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //$time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                        //$time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));
                                        if($model->instruction_date !='' && $model->submission_approver_date !='') {
                                            $startDateTime = new DateTime($startDate_2); // Start date and time
                                            $endDateTime = new DateTime($endDate_2);
                                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                            if($model->reference_number == 'REV-2023-10772' || $model->reference_number == 'REV-2023-10756'){
                                                $workingHours = $workingHours - 8.5;
                                            }
                                            $test = abs(number_format(($workingHours/8.5),1)).'<br>'.'<br>'.'<strong>'.number_format($workingHours).' Hours'.'</strong>';
                                            echo $test;
                                        }else{
                                            echo '';
                                        }

                                    }
                                    ?>
                                </td>

                                <?php
                                if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                                    ?>
                                    <td class="noprint actions">
                                        <div class="btn-group flex-wrap">
                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <a class="dropdown-item text-1"
                                                   href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                                   title="Step 1" data-pjax="0">
                                                    <i class="fas fa-edit"></i> View
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>

                    <th>Average:</th>
                    <th><?= number_format(($diff_1/$total_rows),1).' Hours' ?></th>
                    <th><?= number_format(($diff_3/$total_rows),1).' Hours' ?></th>
                    <th><?= number_format(($diff_4/$total_rows),1).' Hours' ?></th>
                    <th><?= number_format(($diff_5/$total_rows),1).' Hours' ?></th>
                    <!-- <th><?= number_format(($diff_6/$total_rows),1).' Hours' ?></th> -->
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th><?= number_format(($diff_9/$total_rows),1).' Hours' ?></th>
                    <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?php
    $this->registerJs('
                   
        var dataTable = $("#tat_table").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, // Assuming the first column contains dates
                "type": "date-eu", // "date-eu" is a custom sorting type for European-style dates
                "render": function(data, type, row) {
                    // Use Moment.js to parse and format the date
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    dataTable.search(data).draw();
                },
                error: bbAlert
                });
            });
                
          $("#tat_table_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#tat_table_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#tat_table_wrapper").append(customFooter);
          
          
    ');
    ?>
<?php } ?>

<?php if($page_title == "Pending Delayed") { ?>

    <div class="pending_delay-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
                <table id="pending_delay" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>

                        <th class="">Instruction Date
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Schedule
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Approval Date
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Reference Number
                            <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                        </th>

                        <th class="">Client Reference
                            <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                        </th>

                        <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th>

                        <th class="">Reviewer
                            <input type="text" class="custom-search-input form-control" placeholder="Reviewer">
                        </th>

                        <th class="">Approver
                            <input type="text" class="custom-search-input form-control" placeholder="Approver">
                        </th>

                        <?php if(in_array( Yii::$app->user->identity->id, $allowedId)) { ?>
                            <th class="">Fee
                                <input type="text" class="custom-search-input form-control" placeholder="Fee">
                            </th>
                        <?php } ?>

                        <th class="">Total TAT
                            <input type="text" class="custom-search-input form-control" placeholder="TAT">
                        </th>

                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd($dataProvider->getModels());
                        foreach($dataProvider->getModels() as $model){
                            // dd($model);
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                                </td>

                                <!-- Inspection Schedule -->
                                <td>
                                    <?php
                                    if(isset($model->scheduleInspection->inspection_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->scheduleInspection->inspection_date));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Approval date -->
                                <td>
                                    <?php
                                    if(isset($model->submission_approver_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->submission_approver_date));
                                    }else{
                                        echo "";
                                    }

                                    ?>
                                </td>

                                <!-- Reference Number -->
                                <td>
                                    <?= $model->reference_number ?>
                                </td>

                                <!-- Client Reference  -->
                                <td>
                                    <?= $model->client_reference ?>
                                </td>

                                <!-- Valuer -->
                                <td>
                                    <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                    ?>
                                </td>

                                <!-- Reviewer -->
                                <td>
                                    <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                        ->andWhere(['approver_type' => "reviewer"])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->one();

                                    $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                    echo  $get_name->lastname;
                                    ?>
                                </td>

                                <!-- Approver -->
                                <td>
                                    <?php
                                    $type = $model->approverData->approver_type;
                                    if($type == "approver")
                                    {
                                        $get_name = User::find()->where(['id' => $model->approverData->created_by])->one();
                                        echo $get_name->lastname ;
                                    }else{
                                        echo '';
                                    }
                                    ?>
                                </td>

                                <?php if(in_array( Yii::$app->user->identity->id, $allowedId)) { ?>
                                    <!-- fee -->
                                    <td>
                                        <?php
                                        echo $total_fee = Yii::$app->appHelperFunctions->wmFormate($model->total_fee);
                                        ?>
                                    </td>
                                <?php } ?>

                                <!-- Total TAT -->
                                <td>
                                    <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        $inspection_time = $model->inspection_time . ":00";
                                        if ($model->inspection_time == null) {
                                            $inspection_time = '00:00:00';
                                        }
                                        if($model->instruction_date !='' && $model->inspection_date !='') {

                                            $startDate = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                            $endDate = $model->inspection_date . ' ' . $inspection_time;

                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);
                                            $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        }else{
                                            $workingHours_1 =0;
                                        }
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //  $time_1 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1));
                                        //  $time_1_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));
                                        if($model->inspection_date !='' && $model->submission_approver_date !='') {
                                            $startDate_2 = $model->inspection_date . ' ' . $inspection_time;
                                            $endDate_2 = $model->submission_approver_date;
                                            $startDateTime = new DateTime($startDate_2); // Start date and time
                                            $endDateTime = new DateTime($endDate_2);
                                            /*echo $startDate_2.' '.$endDate_2;
                                            die;*/
                                            $workingHours_2 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                        }else{
                                            $workingHours_2=0;
                                        }
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //  $time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                        // $time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));



                                        // print_r( abs( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate ), 2) ) ); echo "<br>";
                                        // die;
                                        $workingHours = $workingHours_1 + $workingHours_2;
                                        if($model->reference_number == 'REV-2023-10737' || $model->reference_number == 'REV-2023-10756'){
                                            $workingHours = $workingHours - 8.5;
                                        }
                                        $test = abs(number_format(($workingHours/8.5),2)).'<br>'.'<br>'.'<strong>'.number_format($workingHours,1).' Hours'.'</strong>';
                                        echo $test;

                                    }else{
                                        $startDate_2 = $model->instruction_date . ' ' . date('H:i:s', strtotime($model->instruction_time));
                                        $endDate_2 = $model->submission_approver_date;
                                        $startDateTime = new DateTime($startDate_2); // Start date and time
                                        $endDateTime = new DateTime($endDate_2);
                                        // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                                        //$time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                                        //$time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));
                                        if($model->instruction_date !='' && $model->submission_approver_date !='') {
                                            $startDateTime = new DateTime($startDate_2); // Start date and time
                                            $endDateTime = new DateTime($endDate_2);
                                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                                            if($model->reference_number == 'REV-2023-10772' || $model->reference_number == 'REV-2023-10756'){
                                                $workingHours = $workingHours - 8.5;
                                            }
                                            $test = abs(number_format(($workingHours/8.5),1)).'<br>'.'<br>'.'<strong>'.number_format($workingHours).' Hours'.'</strong>';
                                            echo $test;
                                        }else{
                                            echo '';
                                        }

                                    }
                                    ?>
                                </td>


                                <?php
                                if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                                    ?>
                                    <td class="noprint actions">
                                        <div class="btn-group flex-wrap">
                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <a class="dropdown-item text-1"
                                                   href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                                   title="Step 1" data-pjax="0">
                                                    <i class="fas fa-edit"></i> View
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?php
    $this->registerJs('
                
        var dataTable = $("#pending_delay").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    dataTable.search(data).draw();
                },
                error: bbAlert
                });
            });
                
          $("#pending_delay_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#pending_delay_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#pending_delay_wrapper").append(customFooter);
          
          
    ');
    ?>
<?php } ?>

<?php if($page_title == "Pending Reminders") { ?>

    <div class="valuation-search">
        <?php $form = ActiveForm::begin([
            'action' => ['reviewed-pending-list'],
            'method' => 'get',
        ]); ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-6 text-center">
                    <?php
                    echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                        'data' => Yii:: $app->appHelperFunctions->reportPeriod,
                        'options' => ['placeholder' => 'Time Frame ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="date_range_array" <?php if($custom == 0){echo 'style="display:none;"';} ?>>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="end_date_id">

                    <?= $form->field($searchModel, 'page_title')->hiddenInput(['value' => $page_title])->label(false); ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div10'])->label('Select Custom Date Range'); ?>
                </div>
                <div class="col-sm-4"></div>
            </div>

            <div class="text-center">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
    
    <div class="reminders_table-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <span><strong><?= $cardTitle; ?></strong></span><br /><br /><br />
                <table id="reminders_table" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>

                        <th class="">Instruction Date
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Inspection Done
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Recommended
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Review Date
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Approval Date
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">Reminder Date
                            <input type="date" class="custom-search-input form-control" placeholder="Date">
                        </th>

                        <th class="">New Ref Number
                            <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                        </th>

                        <th class="">Inspect
                            <input type="text" class="custom-search-input form-control" placeholder="I/O">
                        </th>

                        <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th>

                        <th class="">Review
                            <input type="text" class="custom-search-input form-control" placeholder="Reviewer">
                        </th>

                        <th class="">Approver
                            <input type="text" class="custom-search-input form-control" placeholder="Approver">
                        </th>

                        <th class="">Status
                            <input type="text" class="custom-search-input form-control" placeholder="Status">
                        </th>

                        <th class="">Total TAT
                            <input type="text" class="custom-search-input form-control" placeholder="TAT">
                        </th>

                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd($dataProvider->getModels());
                        foreach($dataProvider->getModels() as $model){
                            ?>
                            <tr class="active">

                                <!-- Instruction -->
                                <td>
                                    <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                                    <br>
                                    <?php
                                    if(isset($model->instruction_time))
                                    {
                                        echo Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time);
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Inspection Done -->
                                <td>
                                    <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        if(isset($model->inspectProperty->inspection_done_date))
                                        {
                                            echo date('d-M-Y', strtotime($model->inspectProperty->inspection_done_date));
                                            echo "<br>";
                                            echo date('h:i A', strtotime($model->inspectProperty->inspection_done_date));
                                        }else{
                                            echo "";
                                        }
                                    }else{
                                        echo 'Desktop Valuation';
                                    }
                                    ?>
                                </td>

                                <!-- Recommended -->
                                <td>
                                    <?php
                                    if($model->valuerData->approver_type == "valuer")
                                    {
                                        echo date('d-M-Y', strtotime($model->valuerData->created_at));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($model->valuerData->created_at));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Review date -->
                                <td>
                                    <?php
                                    if($model->valuerData->approver_type == "reviewer")
                                    {
                                        echo date('d-M-Y', strtotime($model->valuerData->updated_at));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($model->valuerData->updated_at));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Approval date -->
                                <td>
                                    <?php
                                    if(isset($model->submission_approver_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->submission_approver_date));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($model->submission_approver_date));
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Reminder date -->
                                <td>
                                    <?php 
                                        echo date('d-M-Y', strtotime($model->updated_at));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($model->updated_at));
                                    ?>
                                </td>

                                <!-- New Ref Number -->
                                <td>
                                    <?php echo $model->reference_number ?>
                                </td>

                                <!-- Inspection Officer -->
                                <td>
                                    <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                                </td>

                                <!-- Valuer -->
                                <td>
                                    <?php
                                    echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                                    ?>
                                </td>

                                <!-- Reviewer -->
                                <td>
                                    <?php
                                    if($model->valuerData->approver_type == "reviewer")
                                    {
                                        $get_name = User::find()->where(['id' => $model->valuerData->updated_by])->one();
                                        echo  $get_name->lastname;
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <!-- Approver -->
                                <td>
                                    <?php
                                    $type = $model->approverData->approver_type;
                                    if($type == "approver")
                                    {
                                        $get_name = User::find()->where(['id' => $model->approverData->created_by])->one();
                                        echo $get_name->lastname ;
                                    }else{
                                        echo '';
                                    }
                                    ?>
                                </td>

                                <!-- Status -->
                                <td>
                                    <?= Yii::$app->helperFunctions->valuationStatusListArrWords[$model->valuation_status] ?>
                                </td>

                                <!-- Total TAT -->
                                <td>
                                    <?php
                                    if($model->scheduleInspection->inspection_type != 3) {
                                        $inspection_time = $model->scheduleInspection->inspection_time . ":00";
                                        if ($inspection_time == null) {
                                            $inspection_time = '00:00:00';
                                        }

                                        // dd($model->submission_approver_date);

                                        if(!empty($model->scheduleInspection->inspection_date) && !empty($model->submission_approver_date))
                                        {
                                            $startDate = $model->scheduleInspection->inspection_date . ' ' .$inspection_time;
                                            $endDate = $model->submission_approver_date;

                                            $startDateTime = new DateTime($startDate); // Start date and time
                                            $endDateTime = new DateTime($endDate);

                                            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                                            echo $test = abs(number_format(($workingHours/8.5),1)).'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';
                                        }
                                    }else{
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <?php
                                if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                                    ?>
                                    <td class="noprint actions">
                                        <div class="btn-group flex-wrap">
                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <a class="dropdown-item text-1"
                                                   href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                                   title="Step 1" data-pjax="0">
                                                    <i class="fas fa-edit"></i> View
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?php
    $this->registerJs('
        
    $(".div10").daterangepicker({
        autoUpdateInput: false,
          locale: {
          format: "YYYY-MM-DD"
        }
     
     
      });
     
      $(".div10").on("apply.daterangepicker", function(ev, picker) {
         $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
     });

     $(\'#valuationsearch-time_period\').on(\'change\',function(){
        var period = $(this).val();
      
        if(period == 9){
            $(\'#date_range_array\').show();
        }else{
            $(\'#date_range_array\').hide();
        }
    });
                
        var dataTable = $("#reminders_table").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu",
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    dataTable.search(data).draw();
                },
                error: bbAlert
                });
            });
                
          $("#reminders_table_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#reminders_table_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#reminders_table_wrapper").append(customFooter);
          
          
    ');
    ?>
<?php } ?>



<div class="modal fade" id="inspection-reasons-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="border-top:3px solid orange">
                <h4 class="modal-title text-primary"><?= $model->reference_number ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">



                <?php if(isset($model->cancelReasons) && ($model->cancelReasons <> null)){ ?>
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th style="width: 75%;">Reason</th>
                            <th>Email Sent Date/Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($model->cancelReasons as $reason){
                            $date = new DateTime($reason->created_at);
                            $date_time = $date->format('d F Y / h:i A');
                            ?>
                            <tr>
                                <td><?= $reason->reason->title ?></td>
                                <td><?= $date_time ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                <?php }else{ ?>
                    <div class="row text-center text-danger text-bold">
                        <div class="col">
                            No Reasons found in table.
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalContainer"></div>

