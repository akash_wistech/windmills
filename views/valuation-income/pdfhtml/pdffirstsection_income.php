<!-- Bootstrap CSS -->
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>



<div class="col mx-auto mt-5">
  <style media="screen">
  .bggray {
  background-color: #ECEFF1;
  }
  </style>


    <?php
    $unitNumber = ($model->unit_number <> "" || $model->unit_number == 0) ? $model->unit_number : "Not Applicable";

    $approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();
    $ValuerName= \app\models\User::find()->select(['firstname', 'lastname'])->where(['id'=>$approver_data->created_by])->one();

    ?>
<table cellspacing="2" cellpadding="5"  style="border: 1px dashed #64B5F6;" class="mx-auto col-12">


      <tr class="bggray">
        <td colspan="2" style="color:#0277BD;"><h5> Property</h4></td>
        <td colspan="3" style="color:#212121;"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].' '.$model->property->title ?></td>
      </tr>
      <tr >
      <td colspan="2" style="color:#0277BD;" ><h5> Property Address</h4></td>
      <td colspan="3" style="color:#212121;"><?php if($model->unit_number > 0) { echo 'Unit Number '. $unitNumber.', '; }?><?= $model->building->title; ?></td>
      </tr>
      <tr class="bggray">
        <td colspan="2" style="color:#0277BD;" ></td>
         <td colspan="3" style="color:#212121;">Plot Number <?= $model->plot_number.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?></td>
         </tr>
      <tr>
        <td colspan="2"  style="color:#0277BD;"><h5> Client Reference No</h4></td>
        <td colspan="3"  style="color:#212121;"><?= $model->client_reference ?></td>
      </tr>
      <tr class="bggray">
      <td colspan="2" style="color:#0277BD;" ><h5> Windmills Reference</h4></td>
      <td colspan="3"  style="color:#212121;"><?= $model->reference_number ?></td>
      </tr>
      <tr>
        <td colspan="2" style="color:#0277BD;"><h5> Client and Intended Users</h4></td>
        <td colspan="3" style="color:#212121;"><?= $model->client->title ?></td>
      </tr>

    <tr class="bggray">
        <td colspan="2" style="color:#0277BD;" ><h5> Valuer Name</h4></td>
        <td colspan="3"  style="color:#212121;"><?= $ValuerName['firstname'].' '.$ValuerName['lastname']?></td>
    </tr>
    <tr>
        <td colspan="2" style="color:#0277BD;"><h5> Service Officer Name</h4></td>
        <td colspan="3"  style="color:#212121;"><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->service_officer_name] ?></td>
    </tr>
    <tr>
        <td colspan="2"  class="tdbold "><h4> Valuation Report Date</h4></td>
        <!--<td colspan="3"><?/*= Yii::$app->formatter->asDate($model->scheduleInspection->valuation_date) */?></td>-->
        <?php if($model->submission_approver_date <> null){ ?>
            <td colspan="3"><?= ($model->submission_approver_date  <> null) ? date('l, jS \of F, Y', strtotime($model->submission_approver_date )) : '' ?></td>
        <?php }else{
            $date = date('Y-m-d');
            ?>
            <td colspan="3"><?= ($date <> null) ? date('l, jS \of F, Y', strtotime($date)) : '' ?></td>
        <?php } ?>
    </tr>
</table>
</div>

<div class="shadow col mx-auto">
