<?php


//same as TOE

$VAT =0;

$VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);

$finalFeePayable = $netValuationFee+$VAT;
//End same as TOE
$clientAddress = \app\models\Company::find()->select(['address'])->where(['id'=>$model->id])->one();
$fee_to_words     = yii::$app->quotationHelperFunctions->numberTowords($finalFeePayable);
?>




<style>
  .box1{
    background-color: #e6e6e6;
    width: 110px;


      border-right: 5px white;
      border-left: 5px white;


}
.box2{
  background-color: #cccccc;
  width: 90px;
    margin-right: 2px;
    border-right: 5px white;

}
.box3{
  background-color: #cccccc;
  width: 90px;

/* margin-right: 100px; */
}
  .border {
      width: 100%;
      border-top-width: 0.05px;
      border-bottom-width: 0.05px;
      border-left-width: 0.05px;
      border-right-width: 0.05px;
      padding: 5px;
  }
.font_color{
    color: #4472C4;
}

td.tbl-body{
  border-bottom: 0.2px solid grey;
}
</style>


<table class="border" width="547">

    <tr>
        <td colspan="5" class="detailtexttext first-section" >
            <br><span ><?= $branch_address->company; ?> </span>
            <br><span><?= $branch_address->address; ?></span>
            <br><span><?= $branch_address->office_phone; ?></span>
            <br><span>finance@windmillsgroup.com, services@windmillsgroup.com </span>
            <br><span><?= $branch_address->website; ?></span>
        </td>


         <td colspan="3" class="detailtexttext first-section">
             <br><span>Bank Details: </span>
             <br><span>Emirates NBD </span>
             <br><span>Account No: <?= $branch_address->website; ?> </span>
             <br><span>IBAN: <?= $branch_address->iban_number; ?> </span>
             <br><span>TRN: <?= $branch_address->trn_number; ?></span>
        </td>
    </tr>

</table><br><br>

<table>
  <thead>
    <tr>
      <td colspan="2" class="detailtexttext first-section" style="background-color: ;">
        <br><span class="size-8 airal-family font_color">Client</span>
        <br><span><?= $model->title ?></span>
        <br><span><?= $clientAddress->address ?></span>
          <?php
          if ($model->id !=9167) {?>
        <br><span>UAE.</span>
              <?php
          }
          ?>
          <?php if($model->trn_number <> null ){ ?>
          <br>TRN: <span><?= $model->trn_number ?></span>
              <?php
          }
          ?>

      </td>

      <td class="box1 text-dec airal-family" style="margin-top: 20px">
        <span style="color: #4472C4;font-size: 11.5px;font-weight: bold"><br>Transaction<br>
          Invoice
          <br>
        </span>
      </td>
        <td class="box1 text-dec airal-family" style="margin-top: 20px">
        <span style="color: #4472C4;font-size: 11.5px;font-weight: bold"><br>Issue Date<br><?= date('d-M-Y') ?>
          <br>
        </span>
        </td>
        <td class="box1 text-dec airal-family" style="margin-top: 20px">
        <span style="color: #4472C4;font-size: 11.5px;font-weight: bold"><br>Total Amount<br><?= number_format($finalFeePayable,2) ?>
            <br>
        </span>
        </td>





    </tr>
  </thead>
</table><br><br>






<br>

<table class="border" cellspacing="1" cellpadding="3" width="466">


    <tr  style="color:#4472C4 ; border: 1px solid !important;">
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align:left; ">DATE</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; width: 100px">WM REFERENCE</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align: center; ">Client Ref</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align: center; width: 95px">Customer</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align: center; ">Property</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align:right; ">Community</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align:center; ">City</td>
      <td class="airal-family" style="font-size:9px; font-weight: bold; text-align:right; ">Fee AED</td>
    </tr>
</table>
<table  cellspacing="1" cellpadding="3" width="466" style=" padding: 5px;">
  <tbody>
    <?php
    if($valuations <> null && !empty($valuations)) {
      foreach($valuations as $key => $valuation)
      {
        ?>
        <tr>
          <td class="tbl-body" style="font-size:9px;text-align:left;"><?= $valuation->instruction_date; ?></td>
          <td class="tbl-body" style="font-size:9px;width: 100px"><?= $valuation->reference_number; ?></td>
          <td class="tbl-body" style=" font-size:9px; text-align: center;"><?= $valuation->client_reference; ?></td>
          <td class="tbl-body" style=" font-size:9px; text-align: center;width: 95px"><?= $valuation->client_name_passport; ?></td>
          <td class="tbl-body" style="font-size:9px; text-align: center;"><?= $valuation->building->title; ?></td>
          <td class="tbl-body" style="font-size:9px; text-align:right;"><?= $valuation->building->subCommunities->title ?></td>
          <td class="tbl-body" style="font-size:9px; text-align:right;"><?= Yii::$app->appHelperFunctions->emiratedListArr[$valuation->building->city]; ?></td>
          <td class="tbl-body" style="font-size:9px; text-align:right;"><?= number_format($valuation->fee,2); ?></td>
        </tr>
      <?php
      }
    }
    ?>
  </tbody>
</table>
<br><br>
<table class="border airal-family" cellspacing="1" cellpadding="5" width="547">
    <tbody>
    <?php
    if($model <> null && !empty($model)) {

        ?>
        <tr style="background-color: ;">
            <td  style="font-size:9px;text-align:left;font-weight:bold">Gross Fee</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($netValuationFee,2); ?></td>
            <td colspan="5"  style=" font-size:9px; text-align: center;"></td>

        </tr>
        <tr style="background-color: ;">
            <td  style="font-size:9px;text-align:left;font-weight:bold">Discount</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($discount,2); ?></td>
            <td colspan="5"  style=" font-size:9px; text-align: center;font-weight:bold"></td>

        </tr>
        <tr style="background-color: ;">
            <td  style="font-size:9px;text-align:left;font-weight:bold">Fee</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($netValuationFee,2); ?></td>
            <td colspan="5"  style=" font-size:9px; text-align: center;"></td>

        </tr>
        <tr style="background-color: ;">
            <td  style="font-size:9px;text-align:left;font-weight:bold">VAT (5%)</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($VAT,2); ?></td>
            <td colspan="5"  style=" font-size:9px; text-align: center;"></td>

        </tr>
        <tr style="background-color: ;">
            <td  style="font-size:9px;text-align:left;font-weight:bold">Net Fee</td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; text-align: center;font-weight:bold"><?= number_format($finalFeePayable,2); ?></td>
            <td colspan="5"  style=" font-size:9px; text-align: left;font-weight:bold"><?= $fee_to_words." only." ?></td>

        </tr>


    <?php }
    ?>

    </tbody>
</table>









<br><br>
<table cellspacing="1" cellpadding="3" width="466">
  <thead>
    <tr style="background-color: ;">
      <td class="airal-family" style="font-size:10px; text-align:left;">We sincerely thank you for giving us this privilege to work for you. </td>

    </tr>
  </thead>
</table>


<br>
<br>
<br>
<br>
<br>
<hr style="height: 0.2px; margin-top: 100px;">
<br>
<table cellspacing="1" cellpadding="3" width="466">
    <thead>
    <tr style="background-color: ;">
        <td class="airal-family" style="font-size:10px; text-align:left;">Ali Raza </td>
    </tr>
    <tr style="background-color: ;">
        <td class="airal-family" style="font-size:10px; text-align:left;">Finance Manager </td>
    </tr>
    </thead>
</table>

<style>
  .airal-family{
    font-family: Arial, Helvetica, sans-serif;
  }
</style>


<style>

  .text-dec{
    font-size: 10px;
    text-align: center;
      padding-right: 10px;
  }

/*td.detailtext{*/
/*background-color:#BDBDBD; */
/*font-size:9px;*/
/*border-top: 0.4px solid grey;*/
/*}*/

td.detailtexttext{
/* background-color:#EEEEEE; */
font-size:10px;
    font-weight: 100;

}
th.detailtext{
/* background-color:#BDBDBD; */
font-size:9px;
border-top: 1px solid black;
}
/*.border {
border: 1px solid black;
}*/

th.amount-heading{
  color: #039BE5;
}
td.amount-heading{
  color: #039BE5;
}
th.total-due{
  font-size: 16px;
}
td.total-due{
  font-size: 16px;
}
span.size-8{
  font-size: 10px;
  font-weight: bold;
}
td.first-section{
  padding-left: 10px;
/* background-color: black; */
}
</style>

