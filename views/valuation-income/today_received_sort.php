<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$clientsArr = ArrayHelper::map(\app\models\Company::find()
->select([
  'id','title',
  'cname_with_nick' => 'CONCAT(title," ",nick_name)',
])
  ->where(['status' => 1])
  ->andWhere([
      'or',
      ['data_type' => 0],
      ['data_type' => null],
  ])
  ->orderBy(['title' => SORT_ASC,])
  ->all(), 'id', 'title');
  $clientsArr = ['' => 'select'] + $clientsArr;

  $propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
  $propertiesArr = ['' => 'select'] + $propertiesArr;

  $communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
  $communitiesArr = ['' => 'select'] + $communitiesArr;

  $citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
  $citiesArr = ['' => 'select'] + $citiesArr;
  

  $instruction_date_width = '80px';
  $client_width = '180px'; 
  $wm_ref_width = '110px';
  $property_width = '130px'; 
  $community_width = '180px';
  $city_width = '70px';
  $valuer_width = '50px'; 
  $fee_width = '60px';
  $inspector_width = '75px';
  $val_status_width = '120px';
  $client_ref_width = '120px';
?>

<style>
/* .dataTable th {*/
/*    color: #0056b3;*/
/*    font-size: 14px;*/
/*    text-align: right !important;*/
/*    padding-right: 30px !important;*/
/*}*/
/*.dataTable td {*/
/*    font-size: 16px;*/
/*    text-align: right;*/
/*    padding-right: 50px;*/
/*}*/
/*.content-header h1 {*/
/*       font-size: 16px !important;*/

/*    }*/
/*.content-header .row {*/
/*    margin-bottom: 0px !important;*/
/*}*/
/*.content-header {*/
/*    padding: 4px !important;*/
/*} */



.dataTable th {
      color: #0056b3;
      font-size: 15px;
      text-align: left !important;
      padding-right: 30px !important;
  }
  .dataTable td {
      font-size: 16px;
      text-align: left;
      /* padding-right: 50px; */
      padding-top: 3px;
  }

.content-header h1 {
    font-size: 16px !important;
}

.content-header .row {
    margin-bottom: 0px !important;
}

.content-header {
    padding: 0px !important;
}


<style>
    th:nth-child(1) {
      min-width: <?= $instruction_date_width ?> ;
      max-width: <?= $instruction_date_width ?> ;
    }
    th:nth-child(2) {
      min-width: <?= $client_width ?>;
      max-width: <?= $client_width ?>;
    }
    th:nth-child(3) {
      min-width: <?= $wm_ref_width ?>;
      max-width: <?= $wm_ref_width ?>;
    }
    th:nth-child(4) {
      min-width: <?= $property_width ?>;
      max-width: <?= $property_width ?>;
    }
    th:nth-child(5) {
      min-width: <?= $community_width ?>;
      max-width: <?= $community_width ?>;
    }
    th:nth-child(6) {
      min-width: <?= $city_width ?>;
      max-width: <?= $city_width ?>;
    }
    th:nth-child(7) {
      min-width: <?= $valuer_width ?>;
      max-width: <?= $valuer_width ?>;
    }
    th:nth-child(8) {
      min-width: <?= $fee_width ?>;
      max-width: <?= $fee_width ?>;
      text-align: right;
    }
    th:nth-child(9) {
      min-width: <?= $val_status_width ?>;
      max-width: <?= $val_status_width ?>;
    }
</style>

<div class="bank-revenue-index col-12">
    <div class="card card-outline card-info">
        <div class="card-body">
            <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                <thead>
                    <tr>

                        <th class="">Instruction Date
                            <input type="text" class="custom-search-input form-control" placeholder="date">
                        </th>

                        <th class="">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder'=>'client']); ?>
                        </th>


                        <th class="">WM Reference
                            <input type="text" class="custom-search-input form-control" placeholder="ref#">
                        </th>

                        <th class="">Property
                            <?php echo Html::dropDownList('property', null, $propertiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'Property']); ?>
                        </th>

                        
                        <th class="">Community
                            <?php echo Html::dropDownList('community', null, $communitiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'commmunity']); ?>
                        </th>
                        <th class="">City
                            <?php echo Html::dropDownList('city', null, $citiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'city']); ?>
                        </th>
                        
                        <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="valuer">
                        </th>

                        


                        <th class="">Fee
                            <input type="text" class="custom-search-input form-control" placeholder="fee">
                        </th>

                        <?php if($page_title != 'Today Received'){ ?>
                        <th class="">Valuation Status
                            <input type="text" class="custom-search-input form-control" placeholder="status">
                        </th>
                        <?php } ?>

                        <?php 
                          if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <th></th>
                        <?php
                          }
                        ?>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($dataProvider->getModels() as $model){
                    ?>
                    <tr class="active">

                        <td>
                            <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                        </td>

                        <td>
                            <?= ($model->client->nick_name <> null) ?$model->client->nick_name : $model->client->title ?>
                        </td>

                        <td>
                            <?= $model->reference_number ?>
                        </td>

                        <td>
                            <?= $model->property->title ?>
                        </td>

                          <td>
                              <?= $model->building->communities->title ?>
                          </td>
                          <td>
                              <?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
                          </td>

                          <td>
                            <?= (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '' ?>
                          </td>
                        

                        

                        <td style="text-align:right!important;">
                            <?= Yii::$app->appHelperFunctions->wmFormate($model->fee) ?>
                        </td>

                        <?php if($page_title != 'Today Received'){ ?>
                        <td>
                            <?= Yii::$app->helperFunctions->valuationStatusListArrWords[$model->valuation_status] ?>
                        </td>
                        <?php } ?>



                        
                        <?php 
                          if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <td class="noprint actions">
                            <div class="btn-group flex-wrap">
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                    data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a class="dropdown-item text-1"
                                        href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                        title="Step 1" data-pjax="0">
                                        <i class="fas fa-edit"></i> Step 1
                                    </a>
                                </div>
                            </div>
                        </td>
                        <?php
                          }
                        ?>

                    </tr>
                    <?php
                }
            }
        ?>
                </tbody>
                <tfoot>
                  
                
                  <th></th><th></th><th></th><th></th><th></th>
                  <th><strong>Total Fee:</strong></th>
                  <th></th>
                  <th  style="padding-right: 10px!important; text-align:right!important;"> <?= Yii::$app->appHelperFunctions->wmFormate(\app\models\Valuation::getTotal($dataProvider->models, 'fee')) ?></th>
                  
                  <?php if($page_title != 'Today Received'){ ?>
                  <th></th>
                  <?php } ?>
                  

        
                <?php 
                  if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                ?>
                <th></th>
                <?php
                  }
                ?>
                  
                </tfoot>
            </table>
        </div>
    </div>
</div>




<?php
    $this->registerJs('
    
    
        // $("#bank-revenue").DataTable({
        //     order: [[0, "asc"]],
        //     pageLength: 50,
        //     searching: false,
        // });
        
          var dataTable = $("#bank-revenue").DataTable({
            order: [[0, "asc"]],
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            columnDefs: [{
              targets: -1,  // The last column index
              orderable: false  // Disable sorting on the last column
            }]
          });
        
          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input-client").on("change", function () {
              $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                  console.log(data);
                  dataTable.search(data).draw();
                },
                error: bbAlert
            });

          });
                
          $("#bank-revenue_filter").css({
            "display":"none",
          });
        
        $(".custom-search-input").on("click", function (event) {
          event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
          event.stopPropagation();
        });
        

        // Move the length menu to the table footer
        // var lengthMenu = $(".dataTables_length");
        // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");

        // Create a custom footer element
        var customFooter = $("<div class=\"custom-footer\"></div>");
      
        // Move the length menu to the custom footer
        var lengthMenu = $(".dataTables_length");
        lengthMenu.detach().appendTo(customFooter);
      
        // Append the custom footer to the DataTable wrapper
        $("#bank-revenue_wrapper").append(customFooter);
        
        

    ');
?>