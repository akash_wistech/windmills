<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\PropertiesAssets;
PropertiesAssets::register($this);
use  app\components\widgets\StatusVerified;

/* @var $this yii\web\View */
/* @var $model app\models\Properties */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="properties-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'category')->dropDownList(Yii::$app->appHelperFunctions->propertiesCategoriesListArr,['prompt'=>Yii::t('app','Select')])?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'valuation_approach')->dropDownList(Yii::$app->appHelperFunctions->valuationApproachListArr,['prompt'=>Yii::t('app','Select')])?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'approach_reason')->textInput(['maxlength' => true])?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'age')->textInput(['maxlength' => true])?>
            </div>
          <!--  <div class="col-sm-4">
                <?/*= $form->field($model, 'complete_years')->textInput(['maxlength' => true])*/?>
            </div>-->
            <div class="col-sm-4">
                <?= $form->field($model, 'basis_of_value')->textInput(['maxlength' => true])?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'complexity')->widget(Select2::classname(), [
    'data' => yii::$app->quotationHelperFunctions->getComplexity(),
    'options' => ['class'=>'complexity'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'tat')->textInput(['maxlength' => true])?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'prefix')->dropDownList(Yii::$app->appHelperFunctions->crmInvoiceArr,['prompt'=>Yii::t('app','Select')])->label('CRM Invoice Prefix')?>
            </div>

            <div class="col-sm-12">
                <?php
$model->required_documents =  explode(',',$model->required_documents );
echo $form->field($model, 'required_documents')->widget(Select2::classname(), [
    'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArr,
    'options' => ['placeholder' => 'Required Documents ...'],
    'pluginOptions' => [
        'placeholder' => 'Required Documents',
        'multiple' => true,
        'allowClear' => true
    ],
]);
?>
            </div>
            <div class="col-sm-12">
                <?php
                $model->optional_documents =  explode(',',$model->optional_documents );
                echo $form->field($model, 'optional_documents')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArr,
                    'options' => ['placeholder' => 'Optional Documents ...'],
                    'pluginOptions' => [
                        'placeholder' => 'Optional Documents',
                        'multiple' => true,
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <!-- <div class="col-sm-12">
<?php
/*                $model->required_documents =  explode(',',$model->required_documents );
echo $form->field($model, 'required_documents')->widget(Select2::classname(), [
    'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArr,
    'options' => ['placeholder' => 'Required Documents ...'],
    'pluginOptions' => [
        'placeholder' => 'Required Documents',
        'multiple' => true,
        'allowClear' => true
    ],
]);
*/?>
</div>-->
        </div>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Fee Master file Criteria') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'bua_fee')->widget(Select2::classname(), [
                            'data' => array(1 => 'Yes', 0 => 'No'),
                        ])->label('BUA Fee Include');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'upgrade_fee')->widget(Select2::classname(), [
                            'data' => array(1 => 'Yes', 0 => 'No'),
                        ])->label('Upgardes Fee Include');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'land_size_fee')->widget(Select2::classname(), [
                            'data' => array(1 => 'Yes', 0 => 'No'),
                        ])->label('Land Size Fee Include');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'no_of_units_fee')->widget(Select2::classname(), [
                            'data' => array(1 => 'Yes', 0 => 'No'),
                        ])->label('No. of Units Fee Include');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'commentary')->widget(Select2::classname(), [
                            'data' => array(1 => 'Yes', 0 => 'No'),
                        ])->label('Market Commentary');
                        ?>
                    </div>
                </div>
            </div>
        </section>





        <div class="row">
            <div class="col">
                <div class="card card-outline card-info">
                    <div class="card-body p-0">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Completion</th>
                                <th>Zero Comparables Available</th>
                                <th>Scope Of Work</th>
                                <th>Basis Of Value</th>
                                <th>Valuaiton Approach</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach (yii::$app->appHelperFunctions->getReadyArr() as $key => $value) {
                                $zeroCom = '';
                                $row = \app\models\PropertiesFee::find()
                                    ->where(['property_id'=>$model->id, 'ready' => $key])
                                    ->asArray()->one();
                                // echo "<pre>"; print_r($key); echo "</pre>";
                                if($row <> null){

                                    if($row['ready_value']<>null){
                                        $model->FeeStructure[$i]['ready_value'] = $row['ready_value'];
                                    }

                                    if($row['zero_comparables_available']>0){
                                        $model->FeeStructure[$i]['zero_comparables_available'] = $row['zero_comparables_available'];
                                    }

                                    if($row['scope_of_work']<>null){
                                        $model->FeeStructure[$i]['scope_of_work']  = explode(',', $row['scope_of_work'] );
                                    }

                                    if($row['basis_of_values_fee']<>null){
                                        $model->FeeStructure[$i]['basis_of_values_fee'] = explode(',', $row['basis_of_values_fee'] );
                                    }

                                    if($row['valuaiton_approach']<>null){
                                        $model->FeeStructure[$i]['valuaiton_approach'] = explode(',', $row['valuaiton_approach'] );
                                    }
                                }
                                ?>
                                <tr>
                                    <td style="width:20%">
                                        <div class="d-none">
                                            <?= $form->field($model, 'FeeStructure['.$i.'][ready_value]')->textInput(['value' => $key])->label(false) ?>
                                        </div>
                                        <?= $form->field($model, 'FeeStructure['.$i.'][ready]')->textInput(['readonly' => true, 'value' => $value])->label(false) ?>
                                    </td>

                                    <td style="width:20%">
                                        <?= $form->field($model, 'FeeStructure['.$i.'][zero_comparables_available]')->dropDownList(yii::$app->appHelperFunctions->getZeroComparablesAvailableArr(),['prompt'=>Yii::t('app','Select')])->label(false) ?>
                                    </td>

                                    <td style="width:20%">
                                        <?php
                                        echo $form->field($model, 'FeeStructure['.$i.'][scope_of_work]')->label(false)->widget(Select2::classname(), [
                                            'data' => yii::$app->appHelperFunctions->getScopeOfWorkArr(),
                                            'options' => ['placeholder' => 'Scope Of Work...', 'id' => 'properties-scope_of_work_'.$i],
                                            'pluginOptions' => [
                                                'placeholder' => 'Scope Of Work...',
                                                'multiple' => true,
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </td>

                                    <td style="width:20%">
                                        <?php
                                        echo $form->field($model, 'FeeStructure['.$i.'][basis_of_values_fee]')->label(false)->widget(Select2::classname(), [
                                            'data' => yii::$app->appHelperFunctions->getBasisOfValueArr(),
                                            'options' => ['placeholder' => 'Basis Of Value...', 'id' => 'properties-basis_of_value_'.$i],
                                            'pluginOptions' => [
                                                'placeholder' => 'Basis Of Value...',
                                                'multiple' => true,
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </td>

                                    <td style="width:20%">
                                        <?php
                                        echo $form->field($model, 'FeeStructure['.$i.'][valuaiton_approach]')->label(false)->widget(Select2::classname(), [
                                            'data' => yii::$app->appHelperFunctions->getValuaitonApproachArr(),
                                            'options' => ['placeholder' => 'Valuaiton Approach...', 'id' => 'properties-valuaiton_approach_'.$i],
                                            'pluginOptions' => [
                                                'placeholder' => 'Valuaiton Approach...',
                                                'multiple' => true,
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </td>


                                </tr>
                                <?php
                                $i++;
                            }
                            ?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


























































        <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php 
            if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                ]);
            }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>