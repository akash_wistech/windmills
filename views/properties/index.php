<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PropertiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = Yii::t('app', 'Property Types');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
    $actionBtns.='{view}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
    $actionBtns.='{status}';
}


?>
<div class="properties-index">


    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
            ['attribute'=>'title','label'=>Yii::t('app', 'Title')],
            //['attribute'=>'category','label'=>Yii::t('app', 'Category')],
            ['attribute'=>'category','label'=>Yii::t('app','Category'),'value'=>function($model){
        return $model->id;
                return Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model['category']];
            },'filter'=>Yii::$app->appHelperFunctions->propertiesCategoriesListArr],
         /*   ['attribute'=>'gross_category','label'=>Yii::t('app','Gross Category'),'value'=>function($model){
                return Yii::$app->appHelperFunctions->propertiesCategoriesList[$model['gross_category']];
            },'filter'=>Yii::$app->appHelperFunctions->propertiesCategoriesList],*/
            ['attribute'=>'valuation_approach','label'=>Yii::t('app','Approach'),'value'=>function($model){
                return Yii::$app->appHelperFunctions->valuationApproachListArr[$model['valuation_approach']];
            },'filter'=>Yii::$app->appHelperFunctions->valuationApproachListArr],
            ['attribute'=>'age','label'=>Yii::t('app', 'Age')],

            ['format'=>'raw','attribute'=>'bua_fee','label'=>Yii::t('app','BUA Fee Include'),'value'=>function($model){

               if($model['bua_fee'] == 1){
                   return 'Yes';
               }else{
                   return 'No';
               }

            },'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>[0=>'No',1=>'Yes']],

            ['format'=>'raw','attribute'=>'upgrade_fee','label'=>Yii::t('app','Upgrades Fee Include'),'value'=>function($model){

                if($model['upgrade_fee'] == 1){
                    return 'Yes';
                }else{
                    return 'No';
                }

            },'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>[0=>'No',1=>'Yes']],

            ['format'=>'raw','attribute'=>'land_size_fee','label'=>Yii::t('app','Land Size Fee Include'),'value'=>function($model){

                if($model['land_size_fee'] == 1){
                    return 'Yes';
                }else{
                    return 'No';
                }

            },'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>[0=>'No',1=>'Yes']],

            ['format'=>'raw','attribute'=>'no_of_units_fee','label'=>Yii::t('app','No. of Units Fee Include'),'value'=>function($model){

                if($model['no_of_units_fee'] == 1){
                    return 'Yes';
                }else{
                    return 'No';
                }

            },'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>[0=>'No',1=>'Yes']],
            ['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
                return Yii::$app->helperFunctions->arrStatusIcon[$model['status']];
            },'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>Yii::$app->helperFunctions->arrFilterStatus],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'status' => function ($url, $model) {
                        if($model['status']==1){
                            return Html::a('<i class="fas fa-eye-slash"></i> '.Yii::t('app', 'Disable'), $url, [
                                'title' => Yii::t('app', 'Disable'),
                                'class'=>'dropdown-item text-1',
                                'data-confirm'=>Yii::t('app','Are you sure you want to disable this item?'),
                                'data-method'=>"post",
                            ]);
                        }else{
                            return Html::a('<i class="fas fa-eye"></i> '.Yii::t('app', 'Enable'), $url, [
                                'title' => Yii::t('app', 'Enable'),
                                'class'=>'dropdown-item text-1',
                                'data-confirm'=>Yii::t('app','Are you sure you want to enable this item?'),
                                'data-method'=>"post",
                            ]);
                        }
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>


</div>
