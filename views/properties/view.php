<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Properties */

$this->title = Yii::t('app', 'Properties');
$cardTitle = Yii::t('app', 'View Property:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');
?>


<div class="properties-view">
    <section class="card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title"><?= $model->title ?></h2>
            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) { ?>
                <div class="card-tools">
                    <a href="<?= Url::to(['update', 'id' => $model['id']]) ?>" class="btn btn-tool">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            <?php } ?>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <section class="card card-outline card-info">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Title:') . '</strong> ' . $model->title; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Category:') . '</strong> ' . Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->category]; ?>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Valuation Approach:') . '</strong> ' . Yii::$app->appHelperFunctions->valuationApproachListArr[$model->valuation_approach]; ?>
                                </div>

                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Complete Years:') . '</strong> ' . $model->complete_years; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Basis Of Value:') . '</strong> ' . $model->basis_of_value; ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <?= '<strong>' . Yii::t('app', 'Approach Reason:') . '</strong> ' . $model->approach_reason; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= '<strong>' . Yii::t('app', 'required_documents:') . '</strong> ' . $model->savedDocuments; ?>
                                </div>
                            </div>
                        </div>
                </div>
    </section>
</div>

</div>
</div>
</section>
</div>
