<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DevelopmentsAssets;

DevelopmentsAssets::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Developments */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="developments-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle ?></h2>
    </header>
    <div class="card-body">

        <div class="row">

            <div class="col-sm-6">
                <?php
                echo $form->field($model, 'property_type')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select a Type ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>

            <div class="col-sm-6">
                <?php
                echo $form->field($model, 'emirates')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->emiratedListArr,
                    'options' => ['placeholder' => 'Select an Emirate ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="clearfix"></div>
        </div>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Star Rating Info') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'star1')->textInput(['maxlength' => true])->label('1 Star') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'star2')->textInput(['maxlength' => true])->label('2 Star') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'star3')->textInput(['maxlength' => true])->label('3 Star') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'star4')->textInput(['maxlength' => true])->label('4 Star') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'star5')->textInput(['maxlength' => true])->label('5 Star') ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Margin and Rate Info') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'professional_charges')->textInput(['maxlength' => true])->label('Professional Charges (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'contingency_margin')->textInput(['maxlength' => true])->label('Contingency Margin (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'obsolescence')->textInput(['maxlength' => true])->label('Obsolescence') ?>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <?= $form->field($model, 'developer_profit')->textInput(['maxlength' => true])->label('Developer Profit (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'developer_margin')->textInput(['maxlength' => true])->label('Developer Margin (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'interest_rate')->textInput(['maxlength' => true])->label('Interest Rate (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'utilities_cost')->textInput(['maxlength' => true])->label('Utilities Cost (%)') ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="card card-outline card-success">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Prices Info') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'parking_price')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'pool_price')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'landscape_price')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'whitegoods_price')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'utilities_connected_price')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>
            <?php   if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){ ?>
            <section class="card card-outline card-info">
                <header class="card-header">
                    <h2 class="card-title"><?= Yii::t('app', 'Verification') ?></h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="col-sm-4">
                                <?php
                                echo $form->field($model, 'status')->widget(Select2::classname(), [
                                    'data' => array( '2' => 'Unverified','1' => 'Verified'),
                                ]);
                                ?>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
            <?php } ?>
        </section>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
