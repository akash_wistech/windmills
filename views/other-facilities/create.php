<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Developers */

$this->title = Yii::t('app', 'Other Facilities');
$cardTitle = Yii::t('app','New Other Facility');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="developers-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
