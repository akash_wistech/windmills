<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;
use  app\components\widgets\StatusVerified;
use app\assets\ValuationFormAsset;

ValuationFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\BrokerageInquires */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('

$("#listingstransactions-inquiry_date,#listingstransactions-vacancy_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

');
?>

<section class="brokerage-inquires-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle ?></h2>
    </header>
    <div class="card-body">

        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Inquirty Details') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'reference_number')->textInput(['maxlength' => true,'readonly'=>true ,'value' => ($model->reference_number <> null) ? $model->reference_number : Yii::$app->appHelperFunctions->uniqueReferencebr]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'inquiry_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-inquiry_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-inquiry_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'inquiry_type')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->brokerageIquiryTypes,
                            'options' => ['placeholder' => 'Select a Type ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'inquiry_importance')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->BrokerageIquiryImportance,
                            'options' => ['placeholder' => 'Select Importance ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'purpose_of_brokerage_service')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->brokeragepurposes,
                            'options' => ['placeholder' => 'Select a purpose ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                </div>
            </div>
        </section>
        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Parties Details') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'client_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Company::find()
                                ->where(['status' => 1])
                                //->andWhere(['allow_for_valuation'=>1])
                                ->orderBy(['title' => SORT_ASC,])
                                ->all(), 'id', 'title'),

                            'options' => ['placeholder' => 'Select a Client ...', 'class'=> 'client-cls'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Client Name');
                        ?>
                    </div>
                    <div class="col-sm-4"">
                        <?= $form->field($model, 'client_segment')->dropDownList(yii::$app->quotationHelperFunctions->clienttype)?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'referred_by')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Company::find()
                                ->where(['status' => 1])
                              //  ->andWhere(['allow_for_valuation'=>1])
                                ->orderBy(['title' => SORT_ASC,])
                                ->all(), 'id', 'title'),

                            'options' => ['placeholder' => 'Select a Referred ...', 'class'=> 'client-cls'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Referred By');
                        ?>
                    </div>

                </div>
            </div>
        </section>
        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Property Details') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'building_info')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select a Building ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Building');
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select a Property Type ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Property');
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'sub_community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'number_bed_rooms')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->bedrooms,
                            'options' => ['placeholder' => 'Select Number of Bedrooms ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>


                    <div class="col-sm-4">
                        <?= $form->field($model, 'service_charges')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'parking_spaces')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->crmFloorNumber,
                            'options' => ['placeholder' => 'Select Parking Spaces ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'land_size')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'current_rent_amount')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'desired_rice_rent_amount')->textInput(['maxlength' => true])->label('Desired Price/Rent Amount') ?>
                    </div>


                    <div class="col-sm-4">
                        <?= $form->field($model, 'listing_permitted')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'eviction_notice_served')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'Yes', '1' => 'No'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Eviction Notice Served?');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'vacancy_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-vacancy_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-vacancy_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'vacant')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'Yes', '1' => 'No'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Vacant');
                        ?>
                    </div>



                </div>
            </div>
        </section>

        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'INVESTMENT PREFERENCE DETAILS') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-4">
                        <?= $form->field($model, 'target_investment')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'preferred_return_of_investment')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'cash_mortgage_buyer')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'Cash', '1' => 'Mortgage'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Vacant');
                        ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'expected_time_of_buying')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->crmFloorNumber,
                            'options' => ['placeholder' => 'Select No. Of weeks ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'expected_exit_period')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->crmFloorNumber,
                            'options' => ['placeholder' => 'Select No. Of weeks ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                </div>
            </div>
        </section>

        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Documents') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">

                </div>
            </div>
        </section>

        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Other Services Required') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'leasing_service_required')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'Yes', '1' => 'No'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Leasing Service Required');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'facility_management_service_required')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'Yes', '1' => 'No'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Facility Management Service');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_management_service_required')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'Yes', '1' => 'No'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(' Property Management Service Required ');
                        ?>
                    </div>

                </div>
            </div>
        </section>
        <div class="row">


            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'property_status')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->brokeragePropertyStatus,
                    'options' => ['placeholder' => 'Select Status ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'deal_possibility')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->brokerageDealPossibility,
                    'options' => ['placeholder' => 'Deal Possibility ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>

        </div>
<?php
if(Yii::$app->menuHelperFunction->checkActionAllowed('Verified')){
    echo StatusVerified::widget([ 'model' => $model, 'form' => $form ]);
}
?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>