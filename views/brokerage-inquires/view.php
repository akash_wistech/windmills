<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BrokerageInquires */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Brokerage Inquires', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$cardTitle = Yii::t('app', 'View Brokerage Inquiry');
\yii\web\YiiAsset::register($this);
?>
<div class="brokerage-inquires-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'reference_number',
            'client_id',
            'client_segment',
            'referred_by',
            'inquiry_date',
            'inquiry_type',
            'inquiry_importance',
            'purpose_of_brokerage_service',
            'building_info',
            'property_id',
            'property_category',
            'community',
            'sub_community',
            'tenure',
            'number_bed_rooms',
            'city',
            'vacant',
            'vacancy_date',
            'eviction_notice_served',
            'current_rent_amount',
            'service_charges',
            'parking_spaces',
            'listing_permitted',
            'land_size',
            'target_investment',
            'preferred_return_of_investment',
            'cash_mortgage_buyer',
            'expected_time_of_buying',
            'expected_exit_period',
            'leasing_service_required',
            'facility_management_service_required',
            'property_management_service_required',
            'property_status',
        ],
    ]) ?>

</div>
