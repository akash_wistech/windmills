<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BrokerageInquiresSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brokerage-inquires-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'reference_number') ?>

    <?= $form->field($model, 'client_id') ?>

    <?= $form->field($model, 'client_segment') ?>

    <?= $form->field($model, 'reffered_by') ?>

    <?php // echo $form->field($model, 'inquiry_date') ?>

    <?php // echo $form->field($model, 'inquiry_type') ?>

    <?php // echo $form->field($model, 'inquiry_importance') ?>

    <?php // echo $form->field($model, 'purpose_of_brokerage_service') ?>

    <?php // echo $form->field($model, 'building_info') ?>

    <?php // echo $form->field($model, 'property_id') ?>

    <?php // echo $form->field($model, 'property_category') ?>

    <?php // echo $form->field($model, 'community') ?>

    <?php // echo $form->field($model, 'sub_community') ?>

    <?php // echo $form->field($model, 'tenure') ?>

    <?php // echo $form->field($model, 'number_bed_rooms') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'vacant') ?>

    <?php // echo $form->field($model, 'vacancy_date') ?>

    <?php // echo $form->field($model, 'eviction_notice_served') ?>

    <?php // echo $form->field($model, 'current_rent_amount') ?>

    <?php // echo $form->field($model, 'service_charges') ?>

    <?php // echo $form->field($model, 'parking_spaces') ?>

    <?php // echo $form->field($model, 'listing_permitted') ?>

    <?php // echo $form->field($model, 'land_size') ?>

    <?php // echo $form->field($model, 'target_investment') ?>

    <?php // echo $form->field($model, 'preferred_return_of_investment') ?>

    <?php // echo $form->field($model, 'cash_mortgage_buyer') ?>

    <?php // echo $form->field($model, 'expected_time_of_buying') ?>

    <?php // echo $form->field($model, 'expected_exit_period') ?>

    <?php // echo $form->field($model, 'leasing_service_required') ?>

    <?php // echo $form->field($model, 'facility_management_service_required') ?>

    <?php // echo $form->field($model, 'property_management_service_required') ?>

    <?php // echo $form->field($model, 'property_status') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'trashed') ?>

    <?php // echo $form->field($model, 'trashed_at') ?>

    <?php // echo $form->field($model, 'trashed_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
