<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BrokerageInquires */

$this->title = 'Update Brokerage Inquires: ' . $model->id;
$cardTitle = Yii::t('app', 'Update Brokerage Inquiry');
$this->params['breadcrumbs'][] = ['label' => 'Brokerage Inquires', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
?>
<div class="brokerage-inquires-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
