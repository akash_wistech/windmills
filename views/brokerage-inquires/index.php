<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BrokerageInquiresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $page_title;
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;
$actionBtns = '';
$createBtn = false;
if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('view')) {
    $actionBtns .= '{view}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
?>
<div class="brokerage-inquires-index">


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'reference_number', 'label' => Yii::t('app', 'Reference')],
            ['attribute' => 'inquiry_date',
                'label' => Yii::t('app', 'Inquiry Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->inquiry_date));
                },
            ],
            ['attribute' => 'client_id',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                    return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Building'),
                'value' => function ($model) {
                    return $model->building->title;
                },
                'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title;
                },
                'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['format' => 'raw','attribute' => 'number_bed_rooms',
                'label' => Yii::t('app', 'Bedrooms'),
                'value' => function ($model) {
                    $value=Yii::$app->appHelperFunctions->bedrooms[$model['number_bed_rooms']];
                    return $value;
                },
                'contentOptions' => ['class' => ' pt-3 pl-2'],
                'filter' => Yii::$app->appHelperFunctions->bedrooms
            ],
            ['attribute' => 'desired_rice_rent_amount',
                'label' => Yii::t('app', 'Desired Rent Amount'),
                'value' => function ($model) {
                    return $model->desired_rice_rent_amount;
                }
            ],
            ['format' => 'raw','attribute' => 'inquiry_type',
                'label' => Yii::t('app', 'Inquiry Status'),
                'value' => function ($model) {
                    $value=Yii::$app->appHelperFunctions->brokerageIquiryTypes[$model['inquiry_type']];
                   return $value;
                },
                'contentOptions' => ['class' => ' pt-3 pl-2'],
                'filter' => Yii::$app->appHelperFunctions->brokerageIquiryTypes
            ],
            ['format' => 'raw','attribute' => 'deal_possibility',
                'label' => Yii::t('app', 'Deal Possibility'),
                'value' => function ($model) {
                    $value=Yii::$app->appHelperFunctions->brokerageDealPossibility[$model['deal_possibility']];
                    return $value;
                },
                'contentOptions' => ['class' => ' pt-3 pl-2'],
                'filter' => Yii::$app->appHelperFunctions->brokerageDealPossibility
            ],
            ['format' => 'raw','attribute' => 'inquiry_type',
                'label' => Yii::t('app', 'Mobile'),
                'value' => function ($model) {


                  $profile =  \app\models\UserProfileInfo::find()
                        ->where(['user_id'=>$model->client->primaryContact['id']])
                        ->one();
  
                    return $profile->mobile;

                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
                'contentOptions' => ['class' => 'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              ' . $actionBtns . '
              '.$cancelBtns.'
              '.$updateToCancel.'
              '.$step_24.'
  					</div>
  				</div>',
                'buttons' => [
                   /* 'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },*/

                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>

