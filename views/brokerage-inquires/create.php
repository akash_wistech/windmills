<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BrokerageInquires */

$this->title = 'Create Brokerage Inquires';
$cardTitle = Yii::t('app', 'New Brokerage Inquiry');
$this->params['breadcrumbs'][] = ['label' => 'Brokerage Inquires', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brokerage-inquires-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
