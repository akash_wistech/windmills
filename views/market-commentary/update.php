<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MarketCommentary */

$this->title = 'Update Market Commentary: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Market Commentaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$this->title = Yii::t('app', 'Update Market Commentary');
$cardTitle = Yii::t('app','Update Market Commentary:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="market-commentary-update">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
