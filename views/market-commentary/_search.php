<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MarketCommentarySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="market-commentary-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'abu_bhabi_commentary') ?>

    <?= $form->field($model, 'ajman_commentary') ?>

    <?= $form->field($model, 'al_ain_commentary') ?>

    <?= $form->field($model, 'dubai_commentary') ?>

    <?php // echo $form->field($model, 'fujairah_commentary') ?>

    <?php // echo $form->field($model, 'ras_al_khaimah_commentary') ?>

    <?php // echo $form->field($model, 'sharjah_commentary') ?>

    <?php // echo $form->field($model, 'umm_al_quwain_commentary') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'trashed') ?>

    <?php // echo $form->field($model, 'trashed_at') ?>

    <?php // echo $form->field($model, 'trashed_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
