<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MarketCommentary */

$this->title = Yii::t('app', 'Update Market Commentary');
$cardTitle = Yii::t('app','Update Market Commentary');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="market-commentary-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
        'property_type' => $property_type,
    ]) ?>

</div>
