<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\assets\TinyMceAsset;
TinyMceAsset::register($this);
use  app\components\widgets\StatusVerified;

$this->registerJs('


tinymce.init({
  selector: ".editor",

  menubar: false,
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table paste code help wordcount"
  ],
  toolbar: "undo redo | formatselect | " +
  "bold italic backcolor | alignleft aligncenter " +
  "alignright alignjustify | bullist numlist outdent indent | " +
  "removeformat | help",
});





');



/* @var $this yii\web\View */
/* @var $model app\models\SpecialAssumptionreport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="market-commentary card card-outline card-primary">

    <?php $form = ActiveForm::begin(); ?>

    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>

    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'abu_dhabi_commentary')->textarea(['class'=>'editor']) ?>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'dubai_commentary')->textarea(['class'=>'editor']) ?>
            </div>

            <div class="col-sm-12">
                <?= $form->field($model, 'sharjah_commentary')->textarea(['class'=>'editor']) ?>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'ajman_commentary')->textarea(['class'=>'editor']) ?>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'al_ain_commentary')->textarea(['class'=>'editor']) ?>
            </div><div class="col-sm-12">
                <?= $form->field($model, 'fujairah_commentary')->textarea(['class'=>'editor']) ?>
            </div><div class="col-sm-12">
                <?= $form->field($model, 'ras_al_khaimah_commentary')->textarea(['class'=>'editor']) ?>
            </div>
        </div><div class="col-sm-12">
            <?= $form->field($model, 'umm_al_quwain_commentary')->textarea(['class'=>'editor']) ?>
        </div>

</div>



            <div class="card-footer">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
