<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MarketCommentary */

$this->title = Yii::t('app', 'Market Commentaries');
$cardTitle = Yii::t('app','View Market Commentaries:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>

<div class="market-commentary-view">
    <section class="card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title"><?= $model->id?></h2>
            <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){?>
                <div class="card-tools">
                    <a href="<?= Url::to(['update','id'=>$model['id']])?>" class="btn btn-tool">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            <?php }?>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <section class="card card-outline card-info">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= '<strong>'.Yii::t('app','Abu Dhabi Commentary:').'</strong> '.$model->abu_dhabi_commentary;?>
                                </div>

                                <div class="col-sm-12">
                                    <?= '<strong>'.Yii::t('app','Dubai Commentary:').'</strong> '.$model->dubai_commentary;?>
                                </div>
                                <div class="col-sm-12">
                                    <?= '<strong>'.Yii::t('app','Sharjah Commentary:').'</strong> '.$model->sharjah_commentary;?>
                                </div>
                                <div class="col-sm-12">
                                    <?= '<strong>'.Yii::t('app','Ajman Commentary:').'</strong> '.$model->ajman_commentary;?>
                                </div>
                                <div class="col-sm-12">
                                    <?= '<strong>'.Yii::t('app','Al Ain Commentary:').'</strong> '.$model->al_ain_commentary;?>
                                </div>
                                <div class="col-sm-12">
                                    <?= '<strong>'.Yii::t('app','Fujairah Commentary:').'</strong> '.$model->fujairah_commentary;?>
                                </div>
                                <div class="col-sm-12">
                                    <?= '<strong>'.Yii::t('app','Ras Al Khaimah Commentary:').'</strong> '.$model->ras_al_khaimah_commentary;?>
                                </div>
                                <div class="col-sm-12">
                                    <?= '<strong>'.Yii::t('app','Umm Al Quwain Commentary:').'</strong> '.$model->umm_al_quwain_commentary;?>
                                </div>

                            </div>

                        </div>
                    </section>
                </div>

            </div>
        </div>
    </section>
</div>
