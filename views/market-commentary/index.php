<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;


/* @var $this yii\web\View */
/* @var $searchModel app\models\CommunitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Market Commentaries');
$cardTitle = Yii::t('app', 'Market Commentaries - '.$list_title);


$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    $createBtn=false;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
    $actionBtns.='{view}';
}
?>
<div style="padding-top: 20px; padding-bottom: 20px">


<?= Html::a(Yii::t('app','Back'), ['/market-commentary/index-all-properties'], ['class' => 'btn btn-success']); ?>
    <a class="btn btn-success float-right" href="https://maxima.windmillsgroup.com/market-commentary/create?type=<?= $ptype ?>" >Update Market Commentary</a>

</div>
<div class="user-index">
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
            //['attribute'=>'abu_dhabi_commentary','label'=>Yii::t('app', 'Abu Dhabi')],
            [
                'attribute' => 'abu_dhabi_commentary',
                'value' => function($model) {
                    if (strlen($model->abu_dhabi_commentary) > 50)
                        $str = substr($model->abu_dhabi_commentary, 0, 50) . '...';
                    else
                        $str = $model->abu_dhabi_commentary;
                    return html_entity_decode(strip_tags($str));
                },
            ],
            [
                'attribute' => 'dubai_commentary',
                'value' => function($model) {
                    if (strlen($model->dubai_commentary) > 50)
                        $str = substr($model->dubai_commentary, 0, 50) . '...';
                    else
                        $str = $model->dubai_commentary;
                    return html_entity_decode(strip_tags($str));
                },
            ],
            [
                'attribute' => 'sharjah_commentary',
                'value' => function($model) {
                    if (strlen($model->sharjah_commentary) > 50)
                        $str = substr($model->sharjah_commentary, 0, 50) . '...';
                    else
                        $str = $model->sharjah_commentary;
                    return html_entity_decode(strip_tags($str));
                },
            ],
            [
                'attribute' => 'ajman_commentary',
                'value' => function($model) {
                    if (strlen($model->ajman_commentary) > 50)
                        $str = substr($model->ajman_commentary, 0, 50) . '...';
                    else
                        $str = $model->ajman_commentary;
                    return html_entity_decode(strip_tags($str));
                },
            ],
            [
                'attribute' => 'al_ain_commentary',
                'value' => function($model) {
                    if (strlen($model->al_ain_commentary) > 50)
                        $str = substr($model->al_ain_commentary, 0, 100) . '...';
                    else
                        $str = $model->al_ain_commentary;
                    return html_entity_decode(strip_tags($str));
                },
            ],
            [
                'attribute' => 'fujairah_commentary',
                'value' => function($model) {
                    if (strlen($model->fujairah_commentary) > 50)
                        $str = substr($model->fujairah_commentary, 0, 100) . '...';
                    else
                        $str = $model->fujairah_commentary;
                    return html_entity_decode(strip_tags($str));
                },
            ],
            [
                'attribute' => 'ras_al_khaimah_commentary',
                'value' => function($model) {
                    if (strlen($model->ras_al_khaimah_commentary) > 50)
                        $str = substr($model->ras_al_khaimah_commentary, 0, 100) . '...';
                    else
                        $str = $model->ras_al_khaimah_commentary;
                    return html_entity_decode(strip_tags($str));
                },
            ],
            [
                'attribute' => 'umm_al_quwain_commentary',
                'value' => function($model) {
                    if (strlen($model->umm_al_quwain_commentary) > 50)
                        $str = substr($model->umm_al_quwain_commentary, 0, 100) . '...';
                    else
                        $str = $model->umm_al_quwain_commentary;
                    return html_entity_decode(strip_tags($str));
                },
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>
</div>
