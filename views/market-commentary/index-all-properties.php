<?php
// die($page_title);

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>


<style>
    .table-responsive {
        display: table;
    }
</style>
<?php
// echo "<pre>";
// print_r($page_title);
// echo "<pre>";die;
?>
    <input type="hidden" id="comm-id" value='<?php echo $page_title; ?>'>

    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <span><strong><?= $page_title; ?></strong></span><br /><br />
                <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>

                        <th class="">Property Type</th>
                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>

                    <tr class="active">

                        <td>
                            Default Market Commentary
                        </td>


                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14 || Yii::$app->user->id==111711 || Yii::$app->user->id == 16){
                            ?>
                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                            data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                           href="<?= yii\helpers\Url::to(['/market-commentary/index','type'=>0]) ?>"
                                           title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i>Update
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <?php
                        }
                        ?>

                    </tr>
                    <?php
                    if(count($property_data)>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($property_data as $model){
                            ?>
                            <tr class="active">

                                    <td>
                                        <?= $model->title; ?>
                                    </td>


                                <?php
                                if(Yii::$app->user->id==1 || Yii::$app->user->id==14 || Yii::$app->user->id==111711 || Yii::$app->user->id == 16){
                                    ?>
                                    <td class="noprint actions">
                                        <div class="btn-group flex-wrap">
                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <a class="dropdown-item text-1"
                                                   href="<?= yii\helpers\Url::to(['/market-commentary/index','type'=>$model->id]) ?>"
                                                   title="Market Commentary" data-pjax="0">
                                                    <i class="fas fa-edit"></i>Update
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>




<?php
$this->registerJs('
        // $("#bank-revenue").DataTable({
        //     order: [[0, "asc"]],
        //     pageLength: 50,
        //     searching: false,
        // });
        
          var dataTable = $("#bank-revenue").DataTable({
            order: [[0, "asc"]],
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            columnDefs: [{
              targets: -1,  // The last column index
              orderable: false  // Disable sorting on the last column
            }]
          });
        
          $(".custom-search-input").on("change", function () {
            $("#intruction_date").val("");
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input").on("keyup", function () {
            $("#intruction_date").val("");
            dataTable.search(this.value).draw();
          });

          //for date search
          $("#intruction_date").on("change", function () {
            $(".custom-search-input").val("");
            var format1 = moment($("#intruction_date").val()).format("DD-MMM-YYYY"); 
              dataTable.search(format1).draw();
            });

          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input-client").on("change", function () {
              $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                  console.log(data);
                  dataTable.search(data).draw();
                },
                error: bbAlert
            });

          });
                
          $("#bank-revenue_filter").css({
            "display":"none",
          });
        
        $(".custom-search-input").on("click", function (event) {
          event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
          event.stopPropagation();
        });
        

        // Move the length menu to the table footer
        // var lengthMenu = $(".dataTables_length");
        // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");

        // Create a custom footer element
        var customFooter = $("<div class=\"custom-footer\"></div>");
      
        // Move the length menu to the custom footer
        var lengthMenu = $(".dataTables_length");
        lengthMenu.detach().appendTo(customFooter);
      
        // Append the custom footer to the DataTable wrapper
        $("#bank-revenue_wrapper").append(customFooter);

        if($("#comm-id").val() == "Today Performance Overview")
        {
          $("#community-id").css({
            "display":"none",
          });
        }

    ');
?>