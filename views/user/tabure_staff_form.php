<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\StaffFormAsset;
StaffFormAsset::register($this);
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */
/* @var $form yii\widgets\ActiveForm */

if($model->profileInfo!=null){
  $model->job_title_id=$model->profileInfo->job_title_id;
  $model->job_title=$model->profileInfo->jobTitle->title;
  $model->department_id=$model->profileInfo->department_id;
  $model->department_name=$model->profileInfo->department->title;
  $model->mobile=$model->profileInfo->mobile;
  $model->phone1=$model->profileInfo->phone1;
  $model->phone2=$model->profileInfo->phone2;
  $model->address=$model->profileInfo->address;
  $model->background_info=$model->profileInfo->background_info;



  $model->valuer_qualifications=$model->profileInfo->valuer_qualifications;
  $model->valuer_status=$model->profileInfo->valuer_status;
  $model->valuer_experience_expertise=$model->profileInfo->valuer_experience_expertise;

  $oldImage=$model->profileInfo->signature_img_name;

  // print_r($model->signature_file);
  // die();



}

$this->registerJs('
$("#user-job_title").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#user-job_title_id").val()!=suggestion.data){
      $("#user-job_title_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#user-job_title_id").val("0");
  }
});
$("#user-department_name").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/departments']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#user-department_id").val()!=suggestion.data){
      $("#user-department_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#user-department_id").val("0");
  }
});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#blah").attr("src", e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(".imgInps").change(function(){
    readURL(this);
});



$("body").on("click", ".remove_file", function () {
_this=$(this);
swal({
title: "'.Yii::t('app','Confirmation').'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({
  url: "'.Url::to(['site/logodelete','id'=>$model->id]).'",
  dataType: "html",
  type: "POST",
  success: function(html) {
  $("#blah").attr("src", "'.Yii::$app->params['unit_images'].'unnamed.png");
  },
  error: function(xhr,ajaxoptions,thownError){

    alert(xhr.responseText);
  }
  });
    }
});
});



');
?>
<section class="user-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
  <div class="hidden">
    <?= $form->field($model, 'job_title_id')->textInput()?>
    <?= $form->field($model, 'department_id')->textInput()?>
  </div>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'new_password')->passwordInput(['maxlength' => true])->label('Password ('. $model->password.')')?>
      </div>
    </div>
      <input type="hidden"  class="form-control" name="User[permission_group_id]" value="15" >
      <input type="hidden"  class="form-control" name="User[user_type]" value="0" >
      <input type="hidden"  class="form-control" name="User[company_id]" value="119900" >

    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'job_title')->textInput(['maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'department_name')->textInput(['maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
      
    </div>
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])?>
      </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'client_approver')->dropDownList(array_reverse(Yii::$app->helperFunctions->arrYesNo))?>
        </div>

    </div>



  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['tabure'], ['class' => 'btn btn-default']) ?>

  </div>
  <?php ActiveForm::end(); ?>
</section>
