<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\StaffFormAsset;
StaffFormAsset::register($this);
use kartik\select2\Select2;

use app\models\UserDegreeInfo;
use app\models\UserExperience;
use app\models\UserReference;


/* @var $this yii\web\View */
/* @var $model app\models\Staff */
/* @var $form yii\widgets\ActiveForm */

if($model->profileInfo!=null){
  $model->job_title_id=$model->profileInfo->job_title_id;
  $model->job_title=$model->profileInfo->jobTitle->title;
  $model->department_id=$model->profileInfo->department_id;
  $model->department_name=$model->profileInfo->department->title;
  $model->mobile=$model->profileInfo->mobile;
  $model->phone1=$model->profileInfo->phone1;
  $model->phone2=$model->profileInfo->phone2;
  $model->address=$model->profileInfo->address;
  $model->background_info=$model->profileInfo->background_info;

  $model->gender=$model->profileInfo->gender;
  $model->middlename=$model->profileInfo->middlename;
  $model->country_id=$model->profileInfo->country_id;
  $model->nationality=$model->profileInfo->nationality;
  $model->passport_number=$model->profileInfo->passport_number;
  $model->birth_date=$model->profileInfo->birth_date;
  $model->marriage_date=$model->profileInfo->marriage_date;
  $model->marital_status=$model->profileInfo->marital_status;
  $model->no_of_children=$model->profileInfo->no_of_children;
  $model->mother_name=$model->profileInfo->mother_name;
  $model->mother_profession=$model->profileInfo->mother_profession;
  $model->father_name=$model->profileInfo->father_name;
  $model->father_profession=$model->profileInfo->father_profession;
  $model->street=$model->profileInfo->street;
  $model->apartment=$model->profileInfo->apartment;
  $model->community=$model->profileInfo->community;
  $model->city=$model->profileInfo->city;
  $model->ijari=$model->profileInfo->ijari;
  $model->emergency_home=$model->profileInfo->emergency_home;
  $model->emergency_uae=$model->profileInfo->emergency_uae;

  $model->origin_apartment=$model->profileInfo->origin_apartment;
  $model->origin_street=$model->profileInfo->origin_street;
  $model->origin_country=$model->profileInfo->origin_country;
  $model->origin_city=$model->profileInfo->origin_city;
  $model->personal_email=$model->profileInfo->personal_email;

  $model->ncc=$model->profileInfo->ncc;
  $model->ncc_breakage_cost=$model->profileInfo->ncc_breakage_cost;
  $model->notice_period=$model->profileInfo->notice_period;
  $model->early_breakage_cost=$model->profileInfo->early_breakage_cost;
  $model->visa_type=$model->profileInfo->visa_type;
  $model->visa_emirate=$model->profileInfo->visa_emirate;
  $model->joining_date=$model->profileInfo->joining_date;
  $model->visa_start_date=$model->profileInfo->visa_start_date;
  $model->visa_end_date=$model->profileInfo->visa_end_date;
  $model->driving_license=$model->profileInfo->driving_license;
  
  
  $model->whatsapp_groups = json_decode($model->profileInfo->whatsapp_groups, true);
  $model->group_emails = json_decode($model->profileInfo->group_emails, true);
  $model->hobby = json_decode($model->profileInfo->hobby, true);
  $model->language = json_decode($model->profileInfo->language, true);
  $model->total_experience=$model->profileInfo->total_experience;
  $model->relevant_experience=$model->profileInfo->relevant_experience;


  $model->basic_salary=$model->profileInfo->basic_salary;
  $model->house_allowance=$model->profileInfo->house_allowance;
  $model->transport_allowance=$model->profileInfo->transport_allowance;
  $model->last_gross_salary=$model->profileInfo->last_gross_salary;
  $model->tax_rate=$model->profileInfo->tax_rate;
  $model->net_salary=$model->profileInfo->net_salary;
  $model->reports_to=$model->profileInfo->reports_to;
  $model->other_allowance=$model->profileInfo->other_allowance;


  $model->picture=$model->profileInfo->picture;
  $model->cv=$model->profileInfo->cv;
  $model->cover_letter=$model->profileInfo->cover_letter;
  $model->emirates_id=$model->profileInfo->emirates_id;
  $model->residence_visa=$model->profileInfo->residence_visa;
  $model->last_contract=$model->profileInfo->last_contract;
  $model->offer_letter=$model->profileInfo->offer_letter;
  $model->work_permit=$model->profileInfo->work_permit;
  $model->job_description=$model->profileInfo->job_description;
  $model->kpi=$model->profileInfo->kpi;


  $model->valuer_qualifications=$model->profileInfo->valuer_qualifications;
  $model->valuer_status=$model->profileInfo->valuer_status;
  $model->valuer_experience_expertise=$model->profileInfo->valuer_experience_expertise;

  $oldImage=$model->profileInfo->signature_img_name;

  // print_r($model->signature_file);
  // die();
  
}

$qualifications = UserDegreeInfo::find()->where(['user_id'=>$model->id])->all();
$experiences = UserExperience::find()->where(['user_id'=>$model->id])->all();
$references = UserReference::find()->where(['user_id'=>$model->id])->all();


$this->registerJs('


  $("#user-mother_profession").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#user-mother_profession").val()!=suggestion.value){
        $("#user-mother_profession").val(suggestion.value);
      }
    },
    onInvalidateSelection: function() {
      $("#user-mother_profession").val("0");
    }
  });

  $("#user-father_profession").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#user-father_profession").val()!=suggestion.value){
        $("#user-father_profession").val(suggestion.value);
      }
    },
    onInvalidateSelection: function() {
      $("#user-father_profession").val("0");
    }
  });

  $("#user-job_title").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#user-job_title").val()!=suggestion.value){
        $("#user-job_title").val(suggestion.value);
      }
    },
    onInvalidateSelection: function() {
      $("#user-job_title").val("0");
    }
  });

  $("#user-department_name").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/departments']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#user-department_id").val()!=suggestion.data){
        $("#user-department_id").val(suggestion.data);
      }
    },
    onInvalidateSelection: function() {
      $("#user-department_id").val("0");
    }
  });

    $("#user-department-0-department").autocomplete({
      serviceUrl: "'.Url::to(['suggestion/departments']).'",
      noCache: true,
      onSelect: function(suggestion) {
        if($("#user-department-0-department").val()!=suggestion.data){
          $("#user-department-0-department").val(suggestion.data);
        }
      },
      onInvalidateSelection: function() {
        $("#user-department-0-department").val("0");
      }
    });

  $("#user-ref_dept-0-ref_dept").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/departments']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#user-ref_dept-0-ref_dept").val()!=suggestion.data){
        $("#user-ref_dept-0-ref_dept").val(suggestion.data);
      }
    },
    onInvalidateSelection: function() {
      $("#user-ref_dept-0-ref_dept").val("0");
    }
  });

  $("#user-ref_position-0-ref_position").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#user-ref_position-0-ref_position").val()!=suggestion.value){
        $("#user-ref_position-0-ref_position").val(suggestion.value);
      }
    },
    onInvalidateSelection: function() {
      $("#user-ref_position-0-ref_position").val("0");
    }
  });

  $("#user-windmills_position").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#user-windmills_position").val()!=suggestion.value){
        $("#user-windmills_position").val(suggestion.value);
      }
    },
    onInvalidateSelection: function() {
      $("#user-windmills_position").val("0");
    }
  });

  $("#user-position-0-position").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#user-position-0-position").val()!=suggestion.value){
        $("#user-position-0-position").val(suggestion.value);
      }
    },
    onInvalidateSelection: function() {
      $("#user-position-0-position").val("0");
    }
  });



  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
              $("#blah").attr("src", e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
      }
  }
  $(".imgInps").change(function(){
      readURL(this);
  });

  $("body").on("click", ".remove_file", function () {
  _this=$(this);
  swal({
  title: "'.Yii::t('app','Confirmation').'",
  html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
  type: "warning",
  showCancelButton: true,
      confirmButtonColor: "#47a447",
  confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
  cancelButtonText: "'.Yii::t('app','Cancel').'",
  },function(result) {
      if (result) {
    $.ajax({
    url: "'.Url::to(['site/logodelete','id'=>$model->id]).'",
    dataType: "html",
    type: "POST",
    success: function(html) {
    $("#blah").attr("src", "'.Yii::$app->params['unit_images'].'unnamed.png");
    },
    error: function(xhr,ajaxoptions,thownError){

      alert(xhr.responseText);
    }
    });
      }
  });
  });

  $("body").on("change", "#user-community", function () {

    var id = $(this).val();
    var url = "buildings/community/"+id;

    heading=$(this).data("heading");
    $.ajax({
        url: url,
        dataType: "html",
        success: function(data) {
            var response = JSON.parse(data);
            if(response.community.city != ""){
                $("#user-city").val(response.community.city);
            }
        },
        error: bbAlert
    });
});

');

$countries = ArrayHelper::map(\app\models\Country::find()->where(['status' => 1])->all(), 'id', 'title');
$instituteNamesArray = ArrayHelper::map(\app\models\InstitutionManager::find()->where(['status' => 'active'])->all(), 'id', 'institute_name');

$whatsappGroups = \app\models\WhatsappGroups::find()
    ->where(['type' => 'whatsapp']) // Add condition to filter by type
    ->orderBy(['title' => SORT_ASC])
    ->all();
$selectedTitles = [];
$selectedIdsArray = [];
foreach ($whatsappGroups as $group) {
    if (in_array($group->id, $selectedIdsArray)) {
        $selectedTitles[] = $group->title;
    }
}

$groupEmails = \app\models\WhatsappGroups::find()
    ->where(['type' => 'email']) // Add condition to filter by type
    ->orderBy(['title' => SORT_ASC])
    ->all();
$selectedEmails = [];
foreach ($groupEmails as $email) {
    if (in_array($email->id, $selectedIdsArray)) {
        $selectedEmails[] = $email->title;
    }
}

$hobbiesList = \app\models\WhatsappGroups::find()
    ->where(['type' => 'hobby']) // Add condition to filter by type
    ->orderBy(['title' => SORT_ASC])
    ->all();
$selectedHobby = [];
foreach ($hobbiesList as $hobby) {
    if (in_array($hobby->id, $selectedIdsArray)) {
        $selectedHobby[] = $hobby->title;
    }
}

$languageList = \app\models\WhatsappGroups::find()
    ->where(['type' => 'language']) // Add condition to filter by type
    ->orderBy(['title' => SORT_ASC])
    ->all();
$selectedLanguage = [];
foreach ($languageList as $language) {
    if (in_array($language->id, $selectedIdsArray)) {
        $selectedLanguage[] = $language->title;
    }
}

$clientsArr = ArrayHelper::map(\app\models\User::find()
->select([
'id', 'CONCAT(firstname, " ", lastname) AS lastname',
])
    ->where(['status' => 1])
    ->andWhere(['user_type' => [10,20]])
    ->orderBy(['lastname' => SORT_ASC,])
    ->all(), 'id', 'lastname');
$clientsArr = ['' => 'select'] + $clientsArr;

?>

<section class="user-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
  <div class="hidden">
    <?= $form->field($model, 'job_title_id')->textInput()?>
    <?= $form->field($model, 'department_id')->textInput()?>
  </div>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <h6> <b> <u>  Introduction Details </u> </b> </h6> <br>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'gender')->dropDownList([
                'male' => 'Male',
                'female' => 'Female',
            ], ['prompt' => 'Select Gender']) ?>
        </div>
        <div class="col-sm-4">
          <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])?>
        </div>
        <div class="col-sm-4">
          <?= $form->field($model, 'middlename')->textInput(['maxlength' => true])?>
        </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
          <?= $form->field($model, 'country_id', [
              'inputOptions' => ['id' => 'country_id' , 'class' => 'form-control'],
          ])->dropDownList($countries, ['prompt' => 'Select Country']) ?>
      </div>
      <div class="col-sm-4">
          <?= $form->field($model, 'nationality', [
              'inputOptions' => ['id' => 'nationality', 'class' => 'form-control'],
          ])->dropDownList($countries, ['prompt' => 'Select Country']) ?>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'passport_number')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
      <?= $form->field($model, 'birth_date')->textInput(['type' => 'datetime-local']) ?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'marital_status')->dropDownList([
            'single' => 'Single',
            'married' => 'Married',
            'divorced' => 'Divorced',
            'widowed' => 'Widowed',
            'separated' => 'Separated',
        ], ['prompt' => 'Select Marital Status']) ?>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
      <?= $form->field($model, 'marriage_date')->textInput(['type' => 'datetime-local']) ?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'no_of_children')->textInput(['type' => 'number']) ?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'mother_name')->textInput(['maxlength' => true])?>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'mother_profession')->textInput(['maxlength' => true])?>
      </div>
     <div class="col-sm-4">
        <?= $form->field($model, 'father_name')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'father_profession')->textInput(['maxlength' => true])?>
      </div>
    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u>  UAE Address </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'apartment')->textInput(['maxlength' => true])?>
      </div>

      <div class="col-sm-4">
        <?= $form->field($model, 'street')->textInput(['maxlength' => true])?>
      </div>

      <div class="col-sm-4">
          <?php
          echo $form->field($model, 'community')->widget(Select2::classname(), [
              'data' => ArrayHelper::map(\app\models\Communities::find()->orderBy([
                  'title' => SORT_ASC,
              ])->all(), 'id', 'title'),
              'options' => ['placeholder' => 'Select a Community ...'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
          ]);
          ?>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
          <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readonly' => true]) ?>
      </div>

      <div class="col-sm-4">
        <?= $form->field($model, 'ijari')->dropDownList([
            'yes' => 'Yes',
            'no' => 'No',
        ], ['prompt' => 'Select Option']) ?>
      </div>
    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u>  Home Country Address </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'origin_apartment')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'origin_street')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'origin_country')->dropDownList($countries, ['prompt' => 'Select Country']) ?>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <?php if($model->origin_city === null || $model->origin_city == "0" ) { ?>
          <?php echo $form->field($model, 'origin_city')->dropDownList([], ['prompt' => 'Select City', 'id' => 'origin-city']) ?>
        <?php } else {  ?>
          <?= $form->field($model, 'origin_city')->textInput(['maxlength' => true])?>
        <?php } ?>

        </div>
    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u>  Phone Numbers </u> </b> </h6> <br>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'phone1')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'phone2')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])?>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'emergency_uae')->textInput(['maxlength' => true])?>
      </div>

      <div class="col-sm-4">
        <?= $form->field($model, 'emergency_home')->textInput(['maxlength' => true])?>
      </div>

      <div class="col-sm-4">
          <?php
            echo $form->field($model, 'whatsapp_groups')->widget(Select2::classname(), [
              'data' => ArrayHelper::map($whatsappGroups, 'id', 'title'),
              'value' => $selectedTitles, // Preselect options based on titles
              'options' => ['placeholder' => 'Select a Group(s)'],
              'pluginOptions' => [
                  'placeholder' => 'Select a Group(s)',
                  'multiple' => true,
                  'allowClear' => true
              ],
            ]);
          ?>
      </div>
    </div>

    <hr style="border-color:#017BFE">

      <h6> <b> <u>  Email Addresses </u> </b> </h6> <br>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'personal_email')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
          <?php
            echo $form->field($model, 'group_emails')->widget(Select2::classname(), [
              'data' => ArrayHelper::map($groupEmails, 'id', 'title'),
              'value' => $selectedTitles, // Preselect options based on titles
              'options' => ['placeholder' => 'Select an Email(s)'],
              'pluginOptions' => [
                  'placeholder' => 'Select a Email(s)',
                  'multiple' => true,
                  'allowClear' => true
              ],
            ]);
          ?>
      </div>
    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u>  Qualification(s) </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-12">
      <table id="attachment" class="table table-striped table-bordered table-hover images-table">
          <thead>
            <tr>
                <td class="text-left">Degree Name</td>
                <td class="text-left">Institute Name</td>
                <td class="text-left">Passing Year</td>
                <td class="text-left">Percentage</td>
                <td class="text-left">Attested</td>
                <td class="text-left">Attachment</td>
                <td></td>
            </tr>
          </thead>
            
          <tbody>
            <?php $row = 0; ?>
            <?php foreach ($qualifications as $degree) { ?>
                <tr id="image-row<?php echo $row; ?>">

                    <td>
                        <input type="text" class="form-control"
                                name="User[qualification][<?= $row ?>][degree_name]"
                                value="<?= $degree->degree_name ?>" placeholder="Degree Name"
                                required/>
                    </td>
                    <td>
                        <input type="text" class="form-control"
                                name="User[qualification][<?= $row ?>][institute_name]"
                                value="<?= $degree->institute_name ?>" placeholder="Institute Name"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="User[qualification][<?= $row ?>][passing_year]"
                                value="<?= $degree->passing_year ?>" placeholder="Passing Year"
                                required/>
                    </td>

                    <td>
                        <input type="number" class="form-control"
                                name="User[qualification][<?= $row ?>][percentage]"
                                value="<?= $degree->percentage ?>"
                                placeholder="Percentage" required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="User[qualification][<?= $row ?>][degree_attested]"
                                value="<?= $degree->degree_attested ?>"
                                placeholder="Degree Attested" required/>
                    </td>

                    <td>
                        <?php if (!empty($degree->degree_attachment)) : ?>

                            <img src="<?= $degree->degree_attachment ?>" alt="PDF Attachment" style="width: 50px; height: 50px;">

                            <a href="<?= $degree->degree_attachment ?>" target="_blank" class="btn btn-primary">View</a>
                        <?php else : ?>
                            <input type="text" class="form-control"
                                    value="<?= $degree->degree_attachment ?>"
                                    placeholder="Degree Attachment" required/>
                        <?php endif; ?>
                    </td>

                    <td>
                        <input type="hidden" class="form-control"
                                name="User[qualification][<?= $row ?>][degree_attachment]"
                                value="<?= $degree->degree_attachment ?>"
                                required/>
                    </td>

                    <td>
                        <input type="hidden" class="form-control"
                                name="User[qualification][<?= $row ?>][user_id]"
                                value="<?= $model->id ?>"
                                required/>
                    </td>


                    <td class='text-left'>
                        <button type='button' onclick='$("#image-row<?= $row ?>").remove();' 
                                data-toggle='tooltip' title='Remove' 
                                class='btn btn-danger'>
                            <i class='fa fa-minus-circle'></i>
                        </button>
                    </td>


                </tr>
                <?php $row++; ?>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
                <td colspan="6"></td>
                <td class="text-left">
                    <button type="button" onclick="addAttachment();"
                            data-toggle="tooltip" title="Add"
                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                    </button>
                </td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u>  Experience(s) </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-12">
      <table id="experience" class="table table-striped table-bordered table-hover images-table">
          <thead>
            <tr>
              <th>Company</th>
              <th>Position</th>
              <th>Department</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Gross Salary</th>
              <th>Reason of Leaving</th>
              <th>Action</th>
            </tr>
          </thead>

          <tbody>
            <?php $exp_row = 0; ?>
            <?php foreach ($experiences as $experience) { ?>
                <tr id="exp-row<?php echo $exp_row; ?>">

                    <td>
                        <input type="text" class="form-control"
                                name="User[experience][<?= $exp_row ?>][company_name]"
                                value="<?= $experience->company_name ?>" placeholder="Company Name"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="User[experience][<?= $exp_row ?>][position]"
                                value="<?= $experience->position ?>" placeholder="Position"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="User[experience][<?= $exp_row ?>][department]"
                                value="<?= $experience->department ?>" placeholder="department"
                                required/>
                    </td>

                    <td>
                        <input type="date" class="form-control"
                                name="User[experience][<?= $exp_row ?>][start_date]"
                                value="<?= $experience->start_date ?>" placeholder="Start Date"
                                required/>
                    </td>

                    <td>
                        <input type="date" class="form-control"
                                name="User[experience][<?= $exp_row ?>][end_date]"
                                value="<?= $experience->end_date ?>" placeholder="End Date"
                                required/>
                    </td>

                    <td>
                        <input type="number" class="form-control"
                                name="User[experience][<?= $exp_row ?>][gross_salary]"
                                value="<?= $experience->gross_salary ?>" placeholder="Gross Salary"
                                required/>
                    </td>


                    <td>
                        <input type="text" class="form-control"
                                name="User[experience][<?= $exp_row ?>][leave_reason]"
                                value="<?= $experience->leave_reason ?>" placeholder="Leave Reason"
                                required/>
                    </td>

                    <td>
                        <input type="hidden" class="form-control"
                                name="User[experience][<?= $exp_row ?>][user_id]"
                                value="<?= $model->id ?>"
                                required/>
                    </td>


                    <td class='text-left'>
                        <button type='button' onclick='$("#exp-row<?= $exp_row ?>").remove();' 
                                data-toggle='tooltip' title='Remove' 
                                class='btn btn-danger'>
                            <i class='fa fa-minus-circle'></i>
                        </button>
                    </td>


                </tr>
                <?php $exp_row++; ?>
            <?php } ?>
          </tbody>

          <tfoot>
            <tr>
                <td colspan="7"></td>
                <td class="text-left">
                    <button type="button" onclick="addExperience();"
                            data-toggle="tooltip" title="Add"
                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                    </button>
                </td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u>  Experience (Years) </u> </b> </h6> <br>
    <div class="row">
      <div class="col-sm-4">
          <?= $form->field($model, 'total_experience')->dropDownList(
              range(0, 50),
              ['prompt' => 'Select Total Experience']
          ) ?>
      </div>

      <div class="col-sm-4">
          <?= $form->field($model, 'relevant_experience')->dropDownList(
            range(0, 50),
            ['prompt' => 'Select Relevant Experience']
          ) ?>
      </div>
    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u>  Latest Job Details (Additional) </u> </b> </h6> <br>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'ncc')->dropDownList([
            'yes' => 'Yes',
            'no' => 'No',
        ], ['prompt' => 'Select Option']) ?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'ncc_breakage_cost')->textInput(['maxlength' => true])?>
      </div>

      <div class="col-sm-4">
          <?= $form->field($model, 'notice_period')->dropDownList(
              array_combine(range(0, 60), array_map(function ($value) {
                  return $value . ' days';
              }, range(0, 60))),
              ['prompt' => 'Select Option', 'class' => 'form-control']
          ) ?>
      </div>

    </div>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'early_breakage_cost')->textInput(['maxlength' => true])?>
      </div>

      <div class="col-sm-4">
        <?= $form->field($model, 'visa_type')->dropDownList([
            'visit' => 'Visit',
            'cancelled' => 'Cancelled',
            'independent' => 'Independent',
            'residence' => 'Residence',
        ], ['prompt' => 'Select Option']) ?>
      </div>

      <div class="col-sm-4">
          <?= $form->field($model, 'visa_emirate')->dropDownList(
              Yii::$app->appHelperFunctions->EmiratedListArr,
              [
                  'prompt' => 'Select', // This sets the value of the prompt option to an empty string
              ]
          ) ?>
      </div>


    </div>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'joining_date')->textInput(['type' => 'datetime-local']) ?>
      </div>

      <div class="col-sm-4">
        <?= $form->field($model, 'visa_start_date')->textInput(['type' => 'datetime-local']) ?>
      </div>

      <div class="col-sm-4">
        <?= $form->field($model, 'visa_end_date')->textInput(['type' => 'datetime-local']) ?>
      </div>
    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u> Skills </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-4">
          <?= $form->field($model, 'driving_license')->dropDownList(
              ['yes' => 'Yes', 'no' => 'No'],
              ['prompt' => 'Select Option', 'class' => 'form-control']
          ) ?>
      </div>

      <div class="col-sm-4">
        <?php
          echo $form->field($model, 'hobby')->widget(Select2::classname(), [
            'data' => ArrayHelper::map($hobbiesList, 'id', 'title'),
            'value' => $selectedHobby, // Preselect options based on titles
            'options' => ['placeholder' => 'Select a Hobby(s)'],
            'pluginOptions' => [
                'placeholder' => 'Select a Hobby(s)',
                'multiple' => true,
                'allowClear' => true
            ],
          ]);
        ?>
      </div>
      <div class="col-sm-4">
        <?php
          echo $form->field($model, 'language')->widget(Select2::classname(), [
            'data' => ArrayHelper::map($languageList, 'id', 'title'),
            'value' => $selectedLanguage, // Preselect options based on titles
            'options' => ['placeholder' => 'Select Language(s)'],
            'pluginOptions' => [
                'placeholder' => 'Select Language(s)',
                'multiple' => true,
                'allowClear' => true
            ],
          ]);
        ?>
      </div>
    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u> Reference(s) </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-12">
      <table id="reference" class="table table-striped table-bordered table-hover images-table">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Nationality</th>
              <th>Company</th>
              <th>Department</th>
              <th>Position</th>
              <th>Office Phone</th>
              <th>Mobile Phone</th>
              <th>Attach Ref</th>
              <th>Action</th>
            </tr>
          </thead>
            
          <tbody>
            <?php $ref_row = 0; ?>
            <?php foreach ($references as $reference) { ?>
                <tr id="ref-row<?php echo $ref_row; ?>">

                    <td>
                        <input type="text" class="form-control"
                                name="User[reference][<?= $ref_row ?>][first_name]"
                                value="<?= $reference->first_name ?>" placeholder="First Name"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="User[reference][<?= $ref_row ?>][last_name]"
                                value="<?= $reference->last_name ?>" placeholder="Last Name"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="User[reference][<?= $ref_row ?>][nationality]"
                                value="<?= $reference->nationality ?>" placeholder="Nationality"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="User[reference][<?= $ref_row ?>][company]"
                                value="<?= $reference->company ?>" placeholder="Company"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="User[reference][<?= $ref_row ?>][department]"
                                value="<?= $reference->department ?>" placeholder="Department"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="User[reference][<?= $ref_row ?>][position]"
                                value="<?= $reference->position ?>" placeholder="Position"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="User[reference][<?= $ref_row ?>][office_phone]"
                                value="<?= $reference->office_phone ?>" placeholder="Office Phone"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="User[reference][<?= $ref_row ?>][mobile_phone]"
                                value="<?= $reference->mobile_phone ?>" placeholder="Mob. Phone"
                                required/>
                    </td>

                    <td>
                        <?php if (!empty($reference->ref_attachment)) : ?>

                            <img src="<?= $reference->ref_attachment ?>" alt="PDF Attachment" style="width: 50px; height: 50px;">

                            <a href="<?= $reference->ref_attachment ?>" target="_blank" class="btn btn-primary">View</a>
                        <?php else : ?>
                            <input type="file" class="form-control"
                                    name="User[reference][<?= $ref_row ?>][ref_attachment]"
                                    value="<?= $reference->ref_attachment ?>"
                                    placeholder="Ref. Attachment" required/>
                        <?php endif; ?>
                    </td>

                    <td>
                        <input type="hidden" class="form-control"
                                name="User[reference][<?= $ref_row ?>][ref_attachment]"
                                value="<?= $reference->ref_attachment ?>"
                                required/>
                    </td>

                    <td>
                        <input type="hidden" class="form-control"
                                name="User[reference][<?= $ref_row ?>][user_id]"
                                value="<?= $model->id ?>"
                                required/>
                    </td>


                    <td class='text-left'>
                        <button type='button' onclick='$("#ref-row<?= $ref_row ?>").remove();' 
                                data-toggle='tooltip' title='Remove' 
                                class='btn btn-danger'>
                            <i class='fa fa-minus-circle'></i>
                        </button>
                    </td>


                </tr>
                <?php $ref_row++; ?>
            <?php } ?>
          </tbody>

          <tfoot>
            <tr>
                <td colspan="9"></td>
                <td class="text-left">
                    <button type="button" onclick="addReference();"
                            data-toggle="tooltip" title="Add"
                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                    </button>
                </td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>

    <hr style="border-color:#017BFE">
    <h6> <b> <u> Current/Last Compensation </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'basic_salary')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'house_allowance')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'transport_allowance')->textInput(['maxlength' => true])?>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'last_gross_salary')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'tax_rate')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'net_salary')->textInput(['maxlength' => true])?>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'other_allowance')->textInput(['maxlength' => true])?>
      </div>
    </div>

    <hr style="border-color:#017BFE">
    <h6> <b> <u> Role at Windmills </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'job_title')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
      <?= $form->field($model, 'department_name')->textInput(['maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'permission_group_id')->dropDownList(Yii::$app->appHelperFunctions->permissionGroupListArr)?>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'reports_to')->dropDownList($clientsArr, ['prompt' => 'Select User']) ?>
      </div>
      <div class="col-sm-4">

      <div class="password-input-container">
          <?= $form->field($model, 'new_password')->passwordInput([
              'maxlength' => true,
              'id' => 'password-input',
              'value' => $model->password
          ]) ?>
          <span toggle="#password-input" class="fa fa-fw fa-eye field-icon toggle-password"></span> 
      </div>


    </div>
    </div>

    <hr style="border-color:#017BFE">

    <?= $form->field($model, 'background_info')->textArea(['rows' => 4])?>
    <?= $form->field($model, 'valuer_qualifications')->textArea(['rows' => 4])?>
    <?= $form->field($model, 'valuer_status')->textArea(['rows' => 4])?>
    <?= $form->field($model, 'valuer_experience_expertise')->textArea(['rows' => 4])?>

    <div class="col-lg border">
      <?= $form->field($model, 'signature_file', ['labelOptions'=>['style'=>' color:#E1F5FE; background-color:#0277BD;border-radius:3px;','class'=>'px-5 py-4']])->fileInput(['class'=>'imgInps']) ?>
      <?php
        $imgname ='unnamed.png';
        if ($oldImage != null) {
            $imgname = '/images/'.$oldImage;
        }
      ?>
      <div style='width:300px; margin-bottom:30px; position:relative;'>
        <img id="blah" src="<?= $imgname ?>" alt="No Image is selected." width="350px"/>
      </div>
    </div>

    <hr style="border-color:#017BFE">
    <h6> <b> <u> Other Attachments: </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-4">
      <h6> <b> Picture </b> </h6>
          <?= $form->field($model, 'picture')->fileInput()->label(false) ?>
      </div>
      <div class="col-sm-4">
      <h6> <b> CV </b> </h6>
          <?= $form->field($model, 'cv')->fileInput()->label(false) ?>
      </div>
      <div class="col-sm-4">
      <h6> <b> Cover Letter </b> </h6>
          <?= $form->field($model, 'cover_letter')->fileInput()->label(false) ?>
      </div>
    </div>

    <div class="row"> 
        <div class="col-4">
            <!-- Display the image -->
            <?php if (isset($model->picture)) : ?>
                <img src="<?= $model->picture ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                <br>
                <center> <a href="<?= $model->picture ?>" target="_blank">View</a> <center>
                <?php endif; ?>
        </div>

        <div class="col-4">
            <!-- Display the image -->
            <?php if (isset($model->cv)) : ?>
                <img src="<?= $model->cv ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                <br>
                <center> <a href="<?= $model->cv ?>" target="_blank">View</a> <center>
                <?php endif; ?>
        </div>

        <div class="col-4">
            <!-- Display the image -->
            <?php if (isset($model->cover_letter)) : ?>
                <img src="<?= $model->cover_letter ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                <br>
                <center> <a href="<?= $model->cover_letter ?>" target="_blank">View</a> <center>
                <?php endif; ?>
        </div>
    </div>

    <hr style="border-color:#017BFE">

    <div class="row">
      <div class="col-sm-4">
      <h6> <b> Emirates ID </b> </h6>
          <?= $form->field($model, 'emirates_id')->fileInput()->label(false) ?>
      </div>
      <div class="col-sm-4">
      <h6> <b> Residence Visa </b> </h6>
          <?= $form->field($model, 'residence_visa')->fileInput()->label(false) ?>
      </div>
      <div class="col-sm-4">
      <h6> <b> Work Permit </b> </h6>
          <?= $form->field($model, 'work_permit')->fileInput()->label(false) ?>
      </div>
    </div>

    <div class="row"> 
        <div class="col-4">
            <!-- Display the image -->
            <?php if (isset($model->emirates_id)) : ?>
                <img src="<?= $model->emirates_id ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                <br>
                <center> <a href="<?= $model->emirates_id ?>" target="_blank">View</a> <center>
                <?php endif; ?>
        </div>

        <div class="col-4">
            <!-- Display the image -->
            <?php if (isset($model->residence_visa)) : ?>
                <img src="<?= $model->residence_visa ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                <br>
                <center> <a href="<?= $model->residence_visa ?>" target="_blank">View</a> <center>
                <?php endif; ?>
        </div>

        <div class="col-4">
            <!-- Display the image -->
            <?php if (isset($model->work_permit)) : ?>
                <img src="<?= $model->work_permit ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                <br>
                <center> <a href="<?= $model->work_permit ?>" target="_blank">View</a> <center>
                <?php endif; ?>
        </div>
    </div>

    <hr style="border-color:#017BFE">

    <div class="row">
      <div class="col-sm-4">
      <h6> <b> Last Contract </b> </h6>
          <?= $form->field($model, 'last_contract')->fileInput()->label(false) ?>
      </div>
      <div class="col-sm-4">
      <h6> <b> Offer Letter </b> </h6>
          <?= $form->field($model, 'offer_letter')->fileInput()->label(false) ?>
      </div>
    </div>

    <div class="row">
          <div class="col-4">
              <!-- Display the image -->
              <?php if (isset($model->last_contract)) : ?>
              <img src="<?= $model->last_contract ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
              <br>
              <center> <a href="<?= $model->last_contract ?>" target="_blank">View</a> <center>
                      <?php endif; ?>
          </div>

          <div class="col-4">
              <!-- Display the image -->
              <?php if (isset($model->offer_letter)) : ?>
              <img src="<?= $model->offer_letter ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
              <br>
              <center> <a href="<?= $model->offer_letter ?>" target="_blank">View</a> <center>
                      <?php endif; ?>
          </div>
      </div>

      <hr style="border-color:#017BFE">

      <div class="row">
          <div class="col-sm-4">
              <h6> <b> Job Description (Please upload PDF File)</b> </h6>
              <?= $form->field($model, 'job_description')->fileInput()->label(false) ?>
          </div>
          <div class="col-sm-4">
              <h6> <b> Smart KPIs (Please upload PDF File)</b> </h6>
              <?= $form->field($model, 'kpi')->fileInput()->label(false) ?>
          </div>
      </div>

      <div class="row">
          <div class="col-4">
              <!-- Display the image -->
              <?php if (isset($model->job_description)) {

    if (strpos($model->job_description, '.pdf') !== false) {
        $src = Yii::$app->params['uploadPdfIcon'];
    } else if (strpos($model->job_description, '.doc') !== false || strpos($model->job_description, '.docx') !== false || strpos($model->job_description, '.xlsx') !== false || strpos($model->job_description, '.xls') !== false) {
        $src = Yii::$app->params['uploadDocsIcon'];
    }else{
        $src = $model->job_description;
    }

                  ?>
              <img src="<?= $src ?>" alt="Uploaded Image" style="width: 200px; height: 200px;">
              <br>
              <center> <a href="<?= $model->job_description ?>" target="_blank">View</a> <center>
                      <?php } ?>
          </div>

          <div class="col-4">
              <!-- Display the image -->
              <?php if (isset($model->kpi)) {
              if (strpos($model->kpi, '.pdf') !== false) {
                  $src = Yii::$app->params['uploadPdfIcon'];
              } else if (strpos($model->kpi, '.doc') !== false || strpos($model->kpi, '.docx') !== false || strpos($model->kpi, '.xlsx') !== false || strpos($model->kpi, '.xls') !== false) {
                  $src = Yii::$app->params['uploadDocsIcon'];
              }else{
                  $src = $model->kpi;
              }

              ?>
              <img src="<?= $src ?>" alt="Uploaded Image" style="width: 200px; height: 200px;">
              <br>
              <center> <a href="<?= $model->kpi ?>" target="_blank">View</a> <center>
                      <?php } ?>
          </div>
      </div>

<!-- <div class="">
  <img src="/images/<?=''; //$oldImage ?>" alt="" width="200px" height="200px">
</div> -->



  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>


<?php
$expjs2 = <<<JS
$(document).ready(function() {
    $(document).on('click', '.remove-degree', function() {
        $(this).closest('tr').remove();
    });
});
JS;

$this->registerJs($expjs2);
?>

<?php
$expjs = <<<JS
$(document).ready(function() {
    $(document).on('click', '.remove-experience', function() {
        $(this).closest('tr').remove();
    });
});
JS;

$this->registerJs($expjs);
?>

<?php
$referencejs = <<<JS
$(document).ready(function() {
    $(document).on('click', '.remove-reference', function() {
        $(this).closest('tr').remove();
    });
});
JS;

$this->registerJs($referencejs);
?>

<?php
$this->registerJs('

var dropdown1 = document.getElementById("country_id");
var dropdown2 = document.getElementById("nationality");

dropdown1.addEventListener("change", function () {
  dropdown2.value = dropdown1.value;
});



$(".toggle-password").on("click", function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

    
');
?>


<?php

$script = <<< JS
    $('#user-origin_country').change(function(){
      var id = $(this).val();
      var url = "buildings/city/"+id;

      heading=$(this).data("heading");
        $.ajax({
            url: url,
            dataType: "html",
            success: function(data) {
              var response = JSON.parse(data);
              console.log(response)
              var citiesArray = response.cities;
              var citySelect = $('#origin-city');
              citySelect.empty();
              citiesArray.forEach(function(city) {
                  citySelect.append('<option value="' + city + '">' + city + '</option>');
              });
              },
            error: function() {
                console.log('Error occurred while fetching cities.');
            }
        });
    });
JS;

$this->registerJs($script, \yii\web\View::POS_END);
?>

<style>
.password-input-container {
  position: relative;
}

.password-input-container .toggle-password {
  position: absolute;
  top: 72%;
  right: 10px; /* Adjust this value as needed */
  transform: translateY(-50%);
  cursor: pointer;
}
</style>

<!-- Degree -->
<?php
  if (isset($row)) {
      $row = $row;
  } else {
      $row = 0;
  }

  $instituteNamesArrayJson = json_encode($instituteNamesArray);
  $user_id = $model->id;

  $scriptDegree = <<<JS
      var row = $row; 
      var user_id = $user_id;
      var instituteNamesArray =  $instituteNamesArrayJson ;
      var instituteNamesArray = Object.values(instituteNamesArray);

      function addAttachment() {

        var html = "<tr id='image-row" + row + "'>";
        
          html += "<td><div class='form-group'><select class='form-control' name='User[qualification][" + row + "][degree_name]' required>";
            html += "<option value=''>Select Degree</option>";
            html += "<option value='High School Diploma'>High School Diploma</option>";
            html += "<option value='Associate Degree'>Associate Degree</option>";
            html += "<option value='Bachelors Degree'>Bachelor\'s Degree</option>";
            html += "<option value='Masters Degree'>Master\'s Degree</option>";
            html += "<option value='Doctorate Degree'>Doctorate Degree</option>";
          html += "</select></div></td>";

          html += "<td><div class='form-group'><select class='form-control' name='User[qualification][" + row + "][institute_name]' required>";
          html += "<option value=''>Select Institute Name</option>";

          for (var i = 0; i < instituteNamesArray.length; i++) {
            html += "<option value='" + instituteNamesArray[i] + "'>" + instituteNamesArray[i] + "</option>";
          }
          html += "</select></div></td>";

          var currentYear = new Date().getFullYear();
          var selectHtml = '<select class="form-control" name="User[qualification][' + row + '][passing_year]" required>';
          selectHtml += '<option value="">Select Year</option>';
          for (var year = currentYear - 30; year <= currentYear; year++) {
              selectHtml += '<option value="' + year + '">' + year + '</option>';
          }
          selectHtml += '</select>';
          html += '<td><div class="form-group">' + selectHtml + '</div></td>';

          var selectHtml = '<select class="form-control" name="User[qualification][' + row + '][percentage]" required>';
          selectHtml += '<option value="">Select</option>';
          for (var i = 1; i <= 100; i++) {
              selectHtml += '<option value="' + i + '">' + i + '</option>';
          }
          selectHtml += '</select>';
          html += '<td><div class="form-group">' + selectHtml + '</div></td>';

          var selectHtml = '<select class="form-control" name="User[qualification][' + row + '][degree_attested]" required>';
          selectHtml += '<option value="">Select</option>';
          selectHtml += '<option value="Yes">Yes</option>';
          selectHtml += '<option value="No">No</option>';
          selectHtml += '</select>';
          html += '<td><div class="form-group">' + selectHtml + '</div></td>';

          html += "<td><div class='form-group'><input type='file' class='form-control' name='User[qualification][" + row + "][degree_attachement]' value='' placeholder='Attachment' required /></div></td>";
          
          html += "<td><div class='form-group'><input type='hidden' class='form-control' name='User[qualification][" + row + "][user_id]' value=" + user_id +" /></div></td>";
          
          html += "<td class='text-left'><button type='button' onclick=\"$('#image-row" + row + "').remove();\" data-toggle='tooltip' title='Remove' class='btn btn-danger'><i class='fa fa-minus-circle'></i></button></td>";
          
          html += "</tr>";

          $("#attachment tbody").append(html);
          row++;
      }
  JS;

  $this->registerJs($scriptDegree, \yii\web\View::POS_END);
?>

<!-- Experiences -->
<?php
  if (isset($exp_row)) {
      $exp_row = $exp_row;
  } else {
      $exp_row = 0;
  }

  $user_id = $model->id;

  $scriptExp = <<<JS
      var exp_row = $exp_row; 
      var user_id = $user_id;

      function addExperience() {

        var html = "<tr id='exp-row" + exp_row + "'>";
          
        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[experience][" + exp_row + "][company_name]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[experience][" + exp_row + "][position]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[experience][" + exp_row + "][department]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='date' class='form-control' name='User[experience][" + exp_row + "][start_date]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='date' class='form-control' name='User[experience][" + exp_row + "][end_date]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='number' class='form-control' name='User[experience][" + exp_row + "][gross_salary]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[experience][" + exp_row + "][leave_reason]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='hidden' class='form-control' name='User[experience][" + exp_row + "][user_id]' value=" + user_id +" /></div></td>";
        
        html += "<td class='text-left'><button type='button' onclick=\"$('#exp-row" + exp_row + "').remove();\" data-toggle='tooltip' title='Remove' class='btn btn-danger'><i class='fa fa-minus-circle'></i></button></td>";
        
        html += "</tr>";

          $("#experience tbody").append(html);
          exp_row++;
      }
  JS;

  $this->registerJs($scriptExp, \yii\web\View::POS_END);
?>

<!-- References -->
<?php
  if (isset($ref_row)) {
      $ref_row = $ref_row;
  } else {
      $ref_row = 0;
  }

  $user_id = $model->id;

  $scriptRef = <<<JS
      var ref_row = $ref_row; 
      var user_id = $user_id;

      function addReference() {

        var html = "<tr id='ref-row" + ref_row + "'>";
          
        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[reference][" + ref_row + "][first_name]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[reference][" + ref_row + "][last_name]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[reference][" + ref_row + "][nationality]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[reference][" + ref_row + "][company]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[reference][" + ref_row + "][department]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[reference][" + ref_row + "][position]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[reference][" + ref_row + "][office_phone]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='User[reference][" + ref_row + "][mobile_phone]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='file' class='form-control' name='User[reference][" + row + "][ref_attachment]' value='' placeholder='Attachment' required /></div></td>";

        html += "<td><div class='form-group'><input type='hidden' class='form-control' name='User[reference][" + ref_row + "][user_id]' value=" + user_id +" /></div></td>";
        
        html += "<td class='text-left'><button type='button' onclick=\"$('#ref-row" + ref_row + "').remove();\" data-toggle='tooltip' title='Remove' class='btn btn-danger'><i class='fa fa-minus-circle'></i></button></td>";
        
        html += "</tr>";

          $("#reference tbody").append(html);
          ref_row++;
      }
  JS;

  $this->registerJs($scriptRef, \yii\web\View::POS_END);
?>


