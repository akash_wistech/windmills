<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;
$this->registerJs('
$("body").on("change", ".country-list", function () {
  zoneFld=$(this).data("zonefld");
  $.get("'.Url::to(['suggestion/zone-options','country_id'=>'']).'"+$(this).val(),function(data){
    $("select#"+zoneFld).html(data);
  });
});
$("body").on("click", ".remove-add-row", function () {
  $(this).parents(".new-add-row").remove();
});
');

$addresses=Yii::$app->appHelperFunctions->getModuleAddress($model);
?>
<section class="card card-info">
  <header class="card-header">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <h2 class="card-title"><?= Yii::t('app','Address')?></h2>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="clearfix">
          <div class="float-right">
            <a href="javascript:;" onclick="addAddress()" class="btn btn-xs btn-info" data-toggle="tooltip" data-title="Add New"><i class="fas fa-plus"></i></a>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div class="card-body">
  <div class="hidden">
    <?= Html::dropDownList('defCList',null,Yii::$app->appHelperFunctions->countryListArr,['id'=>'def-c-list'])?>
  </div>
    <?php CustomPjax::begin(['id'=>'accordion']); ?>
      <?php
      $addRows=1;
      if($addresses!=null){
        foreach($addresses as $address){
          $addZonesList=Yii::$app->appHelperFunctions->getCountryZoneListArr($address['country_id']);
          $model->add_id[$addRows]=$address['id'];
          //$model->add_is_primary=$addRows;
          $model->add_country_id[$addRows]=$address['country_id'];
          $model->add_zone_id[$addRows]=$address['zone_id'];
          $model->add_city[$addRows]=$address['city'];
          $model->add_phone[$addRows]=$address['phone'];
          $model->add_address[$addRows]=$address['address'];
      ?>
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAdd<?= $addRows?>" class="collapsed" aria-expanded="false">
              <?= Yii::t('app','Addres {number}',['number'=>$addRows])?>
            </a>
          </h4>
        </div>
        <div id="collapseAdd<?= $addRows?>" class="panel-collapse in collapse" style="">
          <div class="hidden">
            <?= $form->field($model, 'add_id['.$addRows.']')->textInput(['maxlength' => true])?>
          </div>
          <div class="card-body" style="position:relative;">
            <div class="crdbdy-btn">
              <a href="javascript:;" class="btn btn-danger btn-md aa-confirm-action" data-url="<?= Url::to(['module-action/delete-address','mt'=>$model->moduleTypeId,'mid'=>$model->id,'id'=>$address['id']])?>" data-cmsg="<?= Yii::$app->helperFunctions->delConfirmationMsg?>" data-container="accordion"><i class="fas fa-trash"></i></a>
            </div>
            <div class="form-group field-prospect-add_is_primary">
              <div id="prospect-add_is_primary" role="radiogroup">
                <label><input type="radio" name="<?= $model->baseClassName?>[add_is_primary]" value="<?= $addRows?>"<?= $address['is_primary']==1 ? ' checked=""' : ''?>> <?= $model->getAttributeLabel('add_is_primary')?></label>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <?= $form->field($model, 'add_country_id['.$addRows.']')->dropdownList(Yii::$app->appHelperFunctions->countryListArr,['prompt'=>Yii::t('app','Select'),'class'=>'form-control country-list','data-zonefld'=>'add_zone_id-'.$addRows])?>
              </div>
              <div class="col-sm-6">
                <?= $form->field($model, 'add_zone_id['.$addRows.']')->dropdownList($addZonesList,['prompt'=>Yii::t('app','Select'),'id'=>'add_zone_id-'.$addRows])?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <?= $form->field($model, 'add_city['.$addRows.']')->textInput(['maxlength' => true])?>
              </div>
              <div class="col-sm-6">
                <?= $form->field($model, 'add_phone['.$addRows.']')->textInput(['maxlength' => true])?>
              </div>
            </div>
            <?= $form->field($model, 'add_address['.$addRows.']')->textArea(['rows' => 4])?>
          </div>
        </div>
      </div>
      <?php
          $addRows++;
        }
      }
      ?>
    <?php CustomPjax::end(); ?>
  </div>
</section>
<script>
var addRows = <?= $addRows?>;
function addAddress(){
  countrylist=$("#def-c-list").html();
  html ='';
  html+='<div class="card new-add-row">';
  html+='  <div class="card-header">';
  html+='    <h4 class="card-title">';
  html+='      <a data-toggle="collapse" data-parent="#accordion" href="#collapseAdd'+addRows+'" class="collapsed" aria-expanded="false">';
  html+='        Address '+addRows;
  html+='      </a>';
  html+='    </h4>';
  html+='  </div>';
  html+='  <div id="collapseAdd'+addRows+'" class="panel-collapse in collapse" style="">';
  html+='    <div class="card-body add-cb-con">';
  html+='      <div class="crdbdy-btn">';
  html+='        <a href="javascript:;" class="btn btn-danger btn-md remove-add-row"><i class="fas fa-trash"></i></a>';
  html+='      </div>';
  html+='      <div class="form-group field-add_is_primary-'+addRows+'">';
  html+='        <label>';
  html+='          <input type="radio" id="add_is_primary-'+addRows+'" name="<?= $model->baseClassName?>[add_is_primary]" value="'+addRows+'"'+(addRows==1 ? ' checked' : '')+'> <?= $model->getAttributeLabel('add_is_primary')?>';
  html+='        </label>';
  html+='      </div>';
  html+='      <div class="row">';
  html+='        <div class="col-sm-6">';
  html+='          <div class="form-group field-add_country_id-'+addRows+'">';
  html+='            <label class="control-label" for="add_country_id-'+addRows+'"><?= $model->getAttributeLabel('add_country_id')?></label>';
  html+='            <select id="add_country_id-'+addRows+'" class="form-control country-list" data-zonefld="add_zone_id-'+addRows+'" name="<?= $model->baseClassName?>[add_country_id]['+addRows+']">';
  html+='              <option value=""><?= Yii::t('app','Select')?></option>'+countrylist;
  html+='            </select>';
  html+='          </div>';
  html+='        </div>';
  html+='        <div class="col-sm-6">';
  html+='          <div class="form-group field-add_zone_id-'+addRows+'">';
  html+='            <label class="control-label" for="add_zone_id-'+addRows+'"><?= $model->getAttributeLabel('add_zone_id')?></label>';
  html+='            <select id="add_zone_id-'+addRows+'" class="form-control" name="<?= $model->baseClassName?>[add_zone_id]['+addRows+']">';
  html+='              <option value=""><?= Yii::t('app','Select')?></option>';
  html+='            </select>';
  html+='          </div>';
  html+='        </div>';
  html+='      </div>';
  html+='      <div class="row">';
  html+='        <div class="col-sm-6">';
  html+='          <div class="form-group field-add_city-'+addRows+'">';
  html+='            <label class="control-label" for="add_city-'+addRows+'"><?= $model->getAttributeLabel('add_city')?></label>';
  html+='            <input type="text" id="add_city-'+addRows+'" class="form-control" name="<?= $model->baseClassName?>[add_city]['+addRows+']">';
  html+='          </div>';
  html+='        </div>';
  html+='        <div class="col-sm-6">';
  html+='          <div class="form-group field-add_phone-'+addRows+'">';
  html+='            <label class="control-label" for="add_phone-'+addRows+'"><?= $model->getAttributeLabel('add_phone')?></label>';
  html+='            <input type="text" id="add_phone-'+addRows+'" class="form-control" name="<?= $model->baseClassName?>[add_phone]['+addRows+']">';
  html+='          </div>';
  html+='        </div>';
  html+='      </div>';
  html+='      <div class="form-group field-add_address-'+addRows+'">';
  html+='        <label class="control-label" for="add_address-'+addRows+'"><?= $model->getAttributeLabel('add_address')?></label>';
  html+='        <textarea id="add_address-'+addRows+'" class="form-control" name="<?= $model->baseClassName?>[add_address]['+addRows+']" rows="4"></textarea>';
  html+='      </div>';
  html+='    </div>';
  html+='  </div>';
  html+='</div>';
  $("#accordion").append(html);
  addRows++;
}
</script>
