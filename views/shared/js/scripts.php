<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
$("body").on("click", ".aa-confirm-action", function () {
  containerId=$(this).data("container");
  confirmHeading=$(this).data("cheading");
  confirmMsg=$(this).data("cmsg");
  url=$(this).data("url");
  Swal.fire({
    title: confirmHeading,
    html: confirmMsg,
    icon: "info",
    focusConfirm: false,
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Confirm').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
  }).then((result) => {
    if (result.isConfirmed) {
      App.blockUI({
        message: "Please wait...",
        target: "#"+containerId,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });
      $.ajax({
        url: url,
        dataType: "json",
        success: function(response) {
          if(response["success"]){
            toastr.success(response["success"]["msg"]);
            window.closeModal();
          }else{
            Swal.fire({title: response["error"]["heading"], html: response["error"]["msg"], icon: "error", timer: 5000});
          }
          App.unblockUI($("#containerId"));
          if(containerId!=null && containerId!=""){
            $.pjax.reload({container: "#"+containerId, timeout: 2000});
          }
        },
        error: bbAlert
      });
    }
  });
});
if($(".autocomplete").length>0){
    $("body").on("keypress", ".autocomplete", function () {
      _t=$(this);
      $(this).autocomplete({
        serviceUrl: _t.data("ds"),
        noCache: true,
        onSelect: function(suggestion) {
          if($("#"+_t.data("fld")).val()!=suggestion.data){
            $("#"+_t.data("fld")).val(suggestion.data);
          }
        },
        onInvalidateSelection: function() {
          $("#"+_t.data("fld")).val("0");
        }
      });
    });
  }

');
?>

<script>
function makeAjaxRequest(url,grid){
  App.blockUI({
    message: "<?= Yii::t('app','Please wait...')?>",
    target: "#"+grid,
    overlayColor: "none",
    cenrerY: true,
    boxed: true
  });
  $.ajax({
    url: url,
    type: 'POST',
    dataType: "json",
    success: function(response) {
      App.unblockUI($("#"+grid));
      if(response['success']){
        toastr.success(response["success"]["msg"], response["success"]["heading"]);
        if(grid!=""){
          $.pjax.reload({container: "#"+grid, async:false, timeout: 5000});
        }else{
          window.location.reload();
        }
      }else{
        swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error"});
      }
    },
    error: bbAlert
  });
}
function makeSilentAjaxRequest(url,grid){
  App.blockUI({
    message: "<?= Yii::t('app','Please wait...')?>",
    target: "#"+grid,
    overlayColor: "none",
    cenrerY: true,
    boxed: true
  });
  $.ajax({
    url: url,
    type: 'POST',
    dataType: "json",
    success: function(response) {
      App.unblockUI($("#"+grid));
      if(response['success']){
        toastr.success(response["success"]["msg"], response["success"]["heading"]);
      }
    },
  });
}

function makeSilentAjaxRequestWithData(url,data,grid){
  if(grid!=''){
    App.blockUI({
      message: "<?= Yii::t('app','Please wait...')?>",
      target: "#"+grid,
      overlayColor: "none",
      cenrerY: true,
      boxed: true
    });
  }
  $.ajax({
    url: url,
    type: 'POST',
    data: data,
    dataType: "json",
    success: function(response) {
      if(grid!=''){
        App.unblockUI($("#"+grid));
      }
      if(response['success']){
        toastr.success(response["success"]["msg"], response["success"]["heading"]);
      }
    },
  });
}
</script>
