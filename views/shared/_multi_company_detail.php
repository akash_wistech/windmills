<?php
use yii\helpers\Html;
use yii\helpers\Url;

$companies=Yii::$app->appHelperFunctions->getModuleCompany($model);
?>
<table class="table table-bordered table-striped mb-2 mt-1">
  <thead>
    <tr>
      <th><?= Yii::t('app','Company')?></th>
      <th><?= Yii::t('app','Role')?></th>
      <th><?= Yii::t('app','Job Title')?></th>
    </tr>
  </thead>
  <tbody>
    <?php
    $n=1;
    if($companies!=null){
      foreach($companies as $company){
    ?>
    <tr>
      <td><?= $company['company_name']?></td>
      <td><?= $company['role_name']?></td>
      <td><?= $company['job_title']?></td>
    </tr>
    <?php
        $n++;
      }
    }
    ?>
  </tbody>
</table>
