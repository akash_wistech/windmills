<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DevelopmentsAssets;

DevelopmentsAssets::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Developments */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="developments-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle ?></h2>
    </header>
    <div class="card-body">

        <div class="row">

            <div class="col-sm-6">
                <?php
                echo $form->field($model, 'property_type')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select a Type ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>

            <div class="col-sm-6">
                <?php
                echo $form->field($model, 'emirates')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->emiratedListArr,
                    'options' => ['placeholder' => 'Select an Emirate ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="clearfix"></div>
        </div>


        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Government Fees') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'land_department_municipality_fee')->textInput(['maxlength' => true])->label('Land Department/Municipality Fee (Fee / SF of built up area)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'civil_defence_fee')->textInput(['maxlength' => true])->label('Civil Defence Fee (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'developer_margin')->textInput(['maxlength' => true])->label('Master Developer Charges (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'green_building_certification_fee')->textInput(['maxlength' => true])->label('Green Building Certification Fee(%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'demolition_fee')->textInput(['maxlength' => true])->label('Demolition Fee(Fee / unit)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'utilities_connected_price')->textInput(['maxlength' => true])->label('Utilities Connection Charges') ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Professional fee') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'professional_charges')->textInput(['maxlength' => true])->label('Consulting/Engineering/Arhitecture Costs (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'designing_and_architecture_costs')->textInput(['maxlength' => true])->label('Designing and Architecture Costs (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'project_management_costs')->textInput(['maxlength' => true])->label('Project Management Costs (%)') ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Demolition Costs') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'demolition_costs')->textInput(['maxlength' => true])->label('Demolition Costs (Cost Per SM)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'debris_removal')->textInput(['maxlength' => true])->label('Debris Removal (Cost Per SM)') ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Contracting Cost') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'contracting_cost')->textInput(['maxlength' => true])->label('Construction Costs (Cost Per SM)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'basement_parking')->textInput(['maxlength' => true])->label('Basement Parking (Cost Per SM)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'multi_story_parking')->textInput(['maxlength' => true])->label('Multi Story Parking (Cost Per SM)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'boundry_wall_length')->textInput(['maxlength' => true])->label('Boundry Wall Length (Cost Per SM)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'whitegoods_price')->textInput(['maxlength' => true])->label('Fixed White Goods Costs') ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Landscapiing/Facilities Costs') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'site_improvements')->textInput(['maxlength' => true])->label('Site Improvements (Cost Per SM)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'pool_price')->textInput(['maxlength' => true])->label('Pool (Cost Per SM)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'landscape_price')->textInput(['maxlength' => true])->label('Landscaping (Cost Per SM)') ?>
                    </div>

                </div>
            </div>
        </section>
        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Other Related Costs') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'staff_accomodation_costs')->textInput(['maxlength' => true])->label('Staff Accomodation Costs') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'pre_opening_expenses')->textInput(['maxlength' => true])->label('Pre Opening Expenses') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'mock_ups')->textInput(['maxlength' => true])->label('Mock Ups') ?>
                    </div>

                </div>
            </div>
        </section>

        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Contingencies') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'contingency_margin')->textInput(['maxlength' => true])->label('Contingencies (%)') ?>
                    </div>

                </div>
            </div>
        </section>

        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Accounting and Finance Charges') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'interest_rate')->textInput(['maxlength' => true])->label('Interest Rate (%)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'physical_depreciation')->textInput(['maxlength' => true])->label('Physical Depreciation (%/year)') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'obsolescence')->textInput(['maxlength' => true])->label('Technical Obsolecense (%/year)') ?>
                    </div>

                </div>
            </div>
        </section>
        <section class="card card-outline card-warning">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Developer\'s Profit') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'developer_profit')->textInput(['maxlength' => true])->label('Developer Profit (%)') ?>
                    </div>
                </div>
            </div>
        </section>



        <?php   if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){ ?>
            <section class="card card-outline card-info">
                <header class="card-header">
                    <h2 class="card-title"><?= Yii::t('app', 'Verification') ?></h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="col-sm-4">
                                <?php
                                echo $form->field($model, 'status')->widget(Select2::classname(), [
                                    'data' => array( '2' => 'Unverified','1' => 'Verified'),
                                ]);
                                ?>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
        <?php } ?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
