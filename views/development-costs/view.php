<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Developments */

$this->title = Yii::t('app', 'Development Costs');
$cardTitle = Yii::t('app', 'View Development Cost:  {nameAttribute}', [
    'nameAttribute' => $model->property->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>


<div class="developments-view">
    <section class="card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title"><?= $model->property->title ?></h2>
            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) { ?>
                <div class="card-tools">
                    <a href="<?= Url::to(['update', 'id' => $model['id']]) ?>" class="btn btn-tool">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            <?php } ?>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Property Type:') . '</strong> ' . $model->property->title; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Emirates:') . '</strong> ' . Yii::$app->appHelperFunctions->emiratedListArr[$model->emirates]; ?>
                                </div>

                            </div>


                            <section class="card card-outline card-info mt-5">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Star Rating Info') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', '1 Star:') . '</strong> ' . $model->star1; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', '2 Star:') . '</strong> ' . $model->star2; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', '3 Star:') . '</strong> ' . $model->star3; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', '4 Star:') . '</strong> ' . $model->star4; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', '5 Star:') . '</strong> ' . $model->star5; ?>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="card card-outline card-warning">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Margin and Rate Info') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Professional Charges (%):') . '</strong> ' . $model->professional_charges; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Contingency Margin (%):') . '</strong> ' . $model->contingency_margin; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Obsolescence (%):') . '</strong> ' . $model->obsolescence; ?>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Developer Profit (%):') . '</strong> ' . $model->developer_profit; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Developer Margin (%):') . '</strong> ' . $model->developer_margin; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Interest Rate (%):') . '</strong> ' . $model->interest_rate; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Utilities Cost (%):') . '</strong> ' . $model->utilities_cost; ?>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="card card-outline card-success">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Prices Info') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Parking Price:') . '</strong> ' . $model->parking_price; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Pool Price:') . '</strong> ' . $model->pool_price; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Landscape Price:') . '</strong> ' . $model->landscape_price; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Whitegoods Price:') . '</strong> ' . $model->whitegoods_price; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Utilities Connected Price:') . '</strong> ' . $model->utilities_connected_price; ?>
                                        </div>
                                    </div>
                                </div>
                            </section>

                        </div>
                </div>
    </section>
</div>
