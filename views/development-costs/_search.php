<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DevelopmentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="developments-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'emirates') ?>

    <?= $form->field($model, 'property_type') ?>

    <?= $form->field($model, 'star1') ?>

    <?= $form->field($model, 'star2') ?>

    <?php // echo $form->field($model, 'star3') ?>

    <?php // echo $form->field($model, 'star4') ?>

    <?php // echo $form->field($model, 'star5') ?>

    <?php // echo $form->field($model, 'professional_charges') ?>

    <?php // echo $form->field($model, 'contingency_margin') ?>

    <?php // echo $form->field($model, 'obsolescence') ?>

    <?php // echo $form->field($model, 'developer_profit') ?>

    <?php // echo $form->field($model, 'parking_price') ?>

    <?php // echo $form->field($model, 'pool_price') ?>

    <?php // echo $form->field($model, 'landscape_price') ?>

    <?php // echo $form->field($model, 'whitegoods_price') ?>

    <?php // echo $form->field($model, 'utilities_connected_price') ?>

    <?php // echo $form->field($model, 'developer_margin') ?>

    <?php // echo $form->field($model, 'interest_rate') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'trashed_at') ?>

    <?php // echo $form->field($model, 'trashed') ?>

    <?php // echo $form->field($model, 'trashed_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
