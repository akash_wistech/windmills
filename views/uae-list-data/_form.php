<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UaeListData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="uae-list-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'listings_reference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'listing_website_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'listing_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'building_info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'community')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sub_community')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'property_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'property_category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_of_bedrooms')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_of_bathrooms')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'land_size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'listings_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'listings_rent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'final_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
