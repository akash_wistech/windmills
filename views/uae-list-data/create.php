<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UaeListData */

$this->title = 'Create Uae List Data';
$this->params['breadcrumbs'][] = ['label' => 'Uae List Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="uae-list-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
