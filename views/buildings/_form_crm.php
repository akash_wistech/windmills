<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Buildings */
/* @var $form yii\widgets\ActiveForm */
?>



<section class="buildings-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title">
            <?= $cardTitle ?>
        </h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->where(['status' => 1])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select a Property Type ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>





        </div>
        <div class="row">
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'city')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->emiratedListArr,
                    'options' => ['placeholder' => 'Select an City ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>
            <div class="col-sm-4">
                <?= Html::hiddenInput('community_value', $model->community, ['id' => 'community_value']); ?>
                <?php

                echo $form->field($model, 'community')->widget(DepDrop::classname(), [
                    'data' => ($model->community ? [$model->community => $model->communities->title] : []),
                    'options' => ['placeholder' => 'Select Community'],
                    'type' => DepDrop::TYPE_SELECT2,
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'initialize' => ($model->community ? true : false),
                        'depends' => ['buildings-city'],
                        'params' => ['community_value'],
                        'url' => Url::to(['/communities/search-commuinites']),
                        'loadingText' => 'Loading Communities ...',
                    ]
                ]);
                ?>

            </div>

            <div class="col-sm-4">
                <?= Html::hiddenInput('sub_community_value', $model->sub_community, ['id' => 'sub_community_value']); ?>
                <?php

                echo $form->field($model, 'sub_community')->widget(DepDrop::classname(), [
                    'data' => ($model->sub_community ? [$model->sub_community => $model->subCommunities->title] : []),
                    'options' => ['placeholder' => 'Select Sub Community'],
                    'type' => DepDrop::TYPE_SELECT2,
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'initialize' => ($model->sub_community ? true : false),
                        'depends' => ['buildings-community'],
                        'params' => ['sub_community_value'],
                        'url' => Url::to(['/sub-communities/search-sub-commuinites']),
                        'loadingText' => 'Loading Sub Communities ...',
                    ]
                ]);
                ?>

            </div>


        </div>
        <div class="row">

            <div class="col-sm-4">
                <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'developer_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Developers::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select a Developer ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>



        </div>

        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'building_number')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'plot_number')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'makani_number')->textInput(['maxlength' => true]) ?>
            </div>



        </div>

        <div class="row">





            <div class="col-sm-4">
                <?= $form->field($model, 'estimated_age')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'year_of_construction')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?php
                if (!isset($model->id)) {
                    $model->vacancy = 10;
                }
                ?>
                <?= $form->field($model, 'vacancy')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'utilities_connected')->widget(Select2::classname(), [
                    'data' => array('Yes' => 'Yes', 'No' => 'No'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'development_type')->widget(Select2::classname(), [
                    'data' => array('Standard' => 'Standard', 'Non-Standard' => 'Non-Standard'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                if (!isset($model->id)) {
                    $model->property_condition = 3;
                }
                echo $form->field($model, 'property_condition')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->propertyConditionListArr,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>


        </div>
        <div class="row">
            <div class="col-sm-4">
                <?php

                if (!isset($model->id)) {
                    $model->property_placement = "1.00";
                }

                echo $form->field($model, 'property_placement')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->propertyPlacementListArr,
                    'options' => ['placeholder' => 'Select a Placement ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                if (!isset($model->id)) {
                    $model->property_visibility = 3;
                }
                echo $form->field($model, 'property_visibility')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->propertyVisibilityListArr,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'property_exposure')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->propertyExposureListArr,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4">
                <?php
                if (!isset($model->id)) {
                    $model->finished_status = 'Fitted';
                }
                echo $form->field($model, 'finished_status')->widget(Select2::classname(), [
                    'data' => array('Shell & Core' => 'Shell & Core', 'Fitted' => 'Fitted'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'pool')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'gym')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'play_area')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-4">
                <?php

                $model->other_facilities = explode(',', ($model->other_facilities <> null) ? $model->other_facilities : "");
                echo $form->field($model, 'other_facilities')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\OtherFacilities::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select a Facilities ...'],
                    'pluginOptions' => [
                        'placeholder' => 'Select a Facilities',
                        'multiple' => true,
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                if (!isset($model->id)) {
                    $model->completion_status = 100;
                }
                ?>
                <?= $form->field($model, 'completion_status')->textInput(['maxlength' => true]) ?>
            </div>

        </div>
        <div class="row">

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'white_goods')->widget(Select2::classname(), [
                    'data' => array('No' => 'No', 'Yes' => 'Yes'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'furnished')->widget(Select2::classname(), [
                    'data' => array('No' => 'No', 'Yes' => 'Yes', 'Semi-Furnished' => 'Semi-Furnished'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'landscaping')->widget(Select2::classname(), [
                    'data' => array('No' => 'No', 'Yes' => 'Yes', 'Semi-Landscape' => 'Semi-Landscape'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'upgrades')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->upgrades,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                ])->label('Upgrades Star Rating');
                ?>
            </div>




        </div>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title">
                    <?= Yii::t('app', 'Location Attributes') ?>
                </h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_highway_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_school_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_mall_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_sea_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'location_park_drive')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                        ]);
                        ?>
                    </div>


                </div>

            </div>

        </section>


        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'number_of_basement')->textInput(['maxlength' => true])->label('Number of Basement Floors') ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'parking_floors')->textInput(['maxlength' => true])->label('Number of Parking Floors') ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'typical_floors')->textInput(['maxlength' => true])->label('Number of Floors') ?>
            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'mezzanine_floors')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToFiftyNumber,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                ])->label('Number of Mezzanine Floors');
                ?>
            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'parking_space')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToFiftyNumber,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                ])->label('Number of Parking Spaces');
                ?>
            </div>
            
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'jacuzzi')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToFiftyNumber,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                ])->label('Number of Jacuzzi');
                ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'no_of_units')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'no_of_residential_units')->widget(Select2::classname(), [
                    'data' => yii::$app->quotationHelperFunctions->numberofUnitsInBuildingArr,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                ])->label('Number of Residential Units');
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'no_of_commercial_units')->widget(Select2::classname(), [
                    'data' => yii::$app->quotationHelperFunctions->numberofUnitsInBuildingArr,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                ])->label('Number of Commercial Units');
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'no_of_retail_units')->widget(Select2::classname(), [
                    'data' => yii::$app->quotationHelperFunctions->numberofUnitsInBuildingArr,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                ])->label('Number of Retail Units');
                ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'no_of_unit_types')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->typesOfUnitsArr,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Types of Unit'); ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'service_charges_psf')->textInput(['maxlength' => true]) ?>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'meeting_rooms')->widget(Select2::classname(), [
                    'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Number of Party Halls/Meeting Rooms'); ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'no_of_health_club_spa')->widget(Select2::classname(), [
                    'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Number of Health Clubs/Spa'); ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'no_of_bbq_area')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Number of BBQ Areas'); ?>
            </div>
            <div class="col-sm-4 ">
                <?= $form->field($model, 'no_of_schools')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Number of Schools'); ?>
            </div>
            <div class="col-sm-4 ">
                <?= $form->field($model, 'no_of_clinics')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Number of Clinics'); ?>
            </div>
            <div class="col-sm-4 ">
                <?= $form->field($model, 'no_of_sports_courts')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Number of Sports Courts'); ?>
            </div>
            <div class="col-sm-4 ">
                <?= $form->field($model, 'no_of_mosques')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                    'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Number of Mosque'); ?>
            </div>

            <div class="col-sm-4">
                <div class="income profit-income">
                    <?= $form->field($model, 'restaurant')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Number of Restaurants'); ?>
                </div>
            </div>
            <div class="col-sm-4 number_atms">
                <div class="income profit-income">
                    <?= $form->field($model, 'atms')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Number of ATM Machines'); ?>
                </div>
            </div>
            <div class="col-sm-4 number_coffee_shops">
                <div class="income profit-income">
                    <?= $form->field($model, 'no_of_coffee_shops')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Number of Coffee Shops'); ?>
                </div>
            </div>
            <div class="col-sm-4 number_sign_boards">
                <div class="income profit-income">
                    <?= $form->field($model, 'no_of_sign_boards')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->crmOptionZeroToTenNumber,
                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Number of Sign Boards'); ?>
                </div>
            </div>
            <div class="col-sm-4 number_night_clubs">
                <div class="income profit-income">
                    <?= $form->field($model, 'night_clubs')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->CrmOptionZeroToTenNumber,
                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Number of Night Clubs'); ?>
                </div>
            </div>
            <div class="col-sm-4 number_bars">
                <div class="income profit-income">
                    <?= $form->field($model, 'bars')->widget(Select2::classname(), [
                        'data' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                        'options' => ['placeholder' => 'Select', 'class' => 'form-control'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Number of Bars'); ?>
                </div>
            </div>

        </div>




    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>