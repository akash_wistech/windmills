<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Buildings */

$this->title = Yii::t('app', 'Buildings / Projects');
$cardTitle = Yii::t('app', 'Update Building:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="buildings-update">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
