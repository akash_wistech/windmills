<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Buildings */


$this->title = Yii::t('app', 'Buildings / Projects');
$cardTitle = Yii::t('app', 'New Building');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle
?>
<div class="buildings-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
