<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Buildings */

$this->title = Yii::t('app', 'Buildings / Projects');
$cardTitle = Yii::t('app', 'View Building:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');
$facilities = '';

$otherCommunityFacilitiesListArr=\yii\helpers\ArrayHelper::map(\app\models\OtherFacilities::find()->orderBy([
    'title' => SORT_ASC,
])->all(), 'id', 'title');
if ($model->other_facilities <> null) {
    $facility_array = explode(',', ($model->other_facilities));
    if (!empty($facility_array)) {
        foreach ($facility_array as $item) {
            $facilities .= $otherCommunityFacilitiesListArr[$item] . ', ';
        }
    }
}

?>

<div class="buildings-view">
    <section class="card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title"><?= $model->title ?></h2>
            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) { ?>
                <div class="card-tools">
                    <a href="<?= Url::to(['update', 'id' => $model['id']]) ?>" class="btn btn-tool">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            <?php } ?>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <section class="card card-outline card-info mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Title:') . '</strong> ' . $model->title; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Community:') . '</strong> ' . $model->communities->title; ?>
                                </div>
                            </div>

                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Sub Community:') . '</strong> ' . $model->subCommunities->title; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Property:') . '</strong> ' . $model->property->title ?>
                                </div>

                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Latitude:') . '</strong> ' . $model->latitude; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Longitude:') . '</strong> ' . $model->longitude; ?>
                                </div>

                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Estimated Age:') . '</strong> ' . $model->estimated_age; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Street Number/Name:') . '</strong> ' . $model->street; ?>
                                </div>

                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Developer:') . '</strong> ' . $model->developer->title; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'No. of Basement:') . '</strong> ' . $model->number_of_basement; ?>
                                </div>

                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Parking Floors:') . '</strong> ' . $model->parking_floors; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Typical Floors:') . '</strong> ' . $model->typical_floors; ?>
                                </div>

                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Building Number:') . '</strong> ' . $model->building_number; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Plot Number:') . '</strong> ' . $model->plot_number; ?>
                                </div>

                            </div>
                            <div class="row pt-2">

                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Makani Number:') . '</strong> ' . $model->makani_number; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'City:') . '</strong> ' . Yii::$app->appHelperFunctions->emiratedListArr[$model->city]; ?>
                                </div>
                            </div>

                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Property Category:') . '</strong> ' . Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category]; ?>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Tenure:') . '</strong> ' . Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->tenure];; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Utilities Connected:') . '</strong> ' . $model->utilities_connected; ?>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Property Placement:') . '</strong> ' . Yii::$app->appHelperFunctions->propertyPlacementListArr[$model->property_placement]; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Property Visibility:') . '</strong> ' . Yii::$app->appHelperFunctions->propertyVisibilityListArr[$model->property_visibility]; ?>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Furnished:') . '</strong> ' . $model->furnished; ?>
                                </div>

                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Property Condition:') . '</strong> ' . Yii::$app->appHelperFunctions->propertyConditionListArr[$model->property_condition]; ?>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Property Exposure:') . '</strong> ' . Yii::$app->appHelperFunctions->propertyExposureListArr[$model->property_exposure]; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Development Type:') . '</strong> ' . $model->development_type; ?>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Finished Status:') . '</strong> ' . $model->finished_status; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Pool:') . '</strong> ' . $model->pool; ?>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Gym:') . '</strong> ' . $model->gym; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Play Area:') . '</strong> ' . $model->play_area; ?>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Other Facilities:') . '</strong> ' . $facilities; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Completion Status:') . '</strong> ' . $model->completion_status; ?>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Landscaping:') . '</strong> ' . $model->landscaping; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'White Goods:') . '</strong> ' . $model->white_goods; ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= '<strong>' . Yii::t('app', 'Vacancy:') . '</strong> ' . $model->vacancy; ?>
                                </div>
                            </div>
                            <section class="card card-outline card-success">
                                <header class="card-header">
                                    <h2 class="card-title"><?= Yii::t('app', 'Location Information') ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Drive to Highway/Main Road and metro:') . '</strong> ' . Yii::$app->appHelperFunctions->locationAttributesValue[$model->location_highway_drive]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Drive to school:') . '</strong> ' . Yii::$app->appHelperFunctions->locationAttributesValue[$model->location_school_drive]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Drive to commercial area/Mall:') . '</strong> ' . Yii::$app->appHelperFunctions->locationAttributesValue[$model->location_mall_drive]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Drive to special landmark, sea, marina:') . '</strong> ' . Yii::$app->appHelperFunctions->locationAttributesValue[$model->location_sea_drive]; ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= '<strong>' . Yii::t('app', 'Drive to pool and park:') . '</strong> ' . Yii::$app->appHelperFunctions->locationAttributesValue[$model->location_park_drive]; ?>
                                        </div>
                                    </div>
                                </div>
                            </section>


                        </div>
                </div>
    </section>
</div>
