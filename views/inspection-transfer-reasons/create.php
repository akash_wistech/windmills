<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InspectionTransferReasons */

$this->title = 'Create Inspection Transfer Reasons';
$this->params['breadcrumbs'][] = ['label' => 'Inspection Transfer Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->title = Yii::t('app', 'Inspection Transfer Reasons');
$cardTitle = Yii::t('app','New Inspection Transfer Reasons');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="inspection-transfer-reasons-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
