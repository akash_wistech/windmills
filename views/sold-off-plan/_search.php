<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SoldOffPlanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sold-off-plan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'transaction_type') ?>

    <?= $form->field($model, 'subtype') ?>

    <?= $form->field($model, 'sales_sequence') ?>

    <?= $form->field($model, 'red_number') ?>

    <?php // echo $form->field($model, 'transanction_date') ?>

    <?php // echo $form->field($model, 'community') ?>

    <?php // echo $form->field($model, 'property') ?>

    <?php // echo $form->field($model, 'property_Type') ?>

    <?php // echo $form->field($model, 'unit_number') ?>

    <?php // echo $form->field($model, 'bedrooms') ?>

    <?php // echo $form->field($model, 'floor') ?>

    <?php // echo $form->field($model, 'parking') ?>

    <?php // echo $form->field($model, 'balcony_area') ?>

    <?php // echo $form->field($model, 'size_sqf') ?>

    <?php // echo $form->field($model, 'land_size') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'price_per_sqf') ?>

    <?php // echo $form->field($model, 'developer') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
