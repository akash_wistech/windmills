<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SoldOffPlan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sold Off Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sold-off-plan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'transaction_type',
            'subtype',
            'sales_sequence',
            'red_number',
            'transanction_date',
            'community',
            'property',
            'property_Type',
            'unit_number',
            'bedrooms',
            'floor',
            'parking',
            'balcony_area',
            'size_sqf',
            'land_size',
            'price',
            'price_per_sqf',
            'developer',
        ],
    ]) ?>

</div>
