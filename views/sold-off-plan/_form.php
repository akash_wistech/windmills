<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SoldOffPlan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sold-off-plan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'transaction_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subtype')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sales_sequence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'red_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transanction_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'community')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'property')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'property_Type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bedrooms')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'floor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parking')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'balcony_area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'size_sqf')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'land_size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_per_sqf')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'developer')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
