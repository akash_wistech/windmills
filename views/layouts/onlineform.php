<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppLoginAsset;
use app\components\widgets\ToasterAlert;
AppLoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title.' - '.Yii::$app->params['appName']) ?></title>
    <?= $this->render('blocks/favicon')?>
    <?php $this->head() ?>
</head>
<style>
    .heading_right{
        padding-top: 20px;
        float: right;
        text-align: right;
        padding-right: 35px !important;
    }
    .heading_right h3{
        font-family: math !important;
    }
</style>
<body class="hold-transition ">
<?php $this->beginBody() ?>

<section class="content my-4">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 "  >
                <div class="login-logo" style="text-align: left">
                    <img src="https://maxima.windmillsgroup.com/images/windmils-footer.png" width="300">
                </div>
            </div>
            <div class="col-sm-4 heading_right" style="padding-top: 20px;">
                <h3 ><?= date("F d, Y"); ?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 "  >
                <?= $content ?>
            </div>
        </div>
    </div>
</section>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
