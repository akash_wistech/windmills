<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\components\widgets\ToasterAlert;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\AdminLteBreadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title.' - '.Yii::$app->params['appName']) ?></title>
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <?= $this->render('blocks/favicon')?>
  <?php $this->head() ?>
  <base href="/">

</head>
<body class="hold-transition sidebar-mini">
  <?php $this->beginBody() ?>

  <div class="wrapper">
    <?= $this->render('blocks/topbar')?>
      <?php if(Yii::$app->user->identity->location_status == 0){ ?>
    <?= $this->render('blocks/left_menu')?>
      <?php } ?>

    <div class="content-wrapper">
      <section class="content-header d-none">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>
                <?= $this->title?>
                <?php
                if(isset($this->params['titleBtns'])){
                  foreach ($this->params['titleBtns'] as $btn) {
                    echo $btn;
                  }
                }
                if(isset($this->params['availableWorkFlows'])){
                  $processSelectionHtml='';
                  $wfActiveTitle='';
                  foreach ($this->params['availableWorkFlows'] as $workFlow) {
                    if($workFlow['active']==true)$wfActiveTitle=$workFlow['title'];
                    $processSelectionHtml.='<a class="dropdown-item" href="'.Url::to($workFlow['r']).'">'.$workFlow['title'].'</a>';
                  }
                  echo '<div class="btn-group">
                    <button type="button" class="btn btn-success">'.$wfActiveTitle.'</button>
                    <button type="button" class="btn btn-success dropdown-toggle dropdown-hover dropdown-icon" data-toggle="dropdo1wn">
                      <span class="sr-only">Toggle Dropdown</span>
                      <div class="dropdown-menu">
                        '.$processSelectionHtml.'
                      </div>
                    </button>
                  </div>';
                }
                ?>
              </h1>
            </div>
            <div class="col-sm-6 d-none">
              <?= AdminLteBreadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
            </div>
          </div>
        </div>
      </section>

      <section class="content my-2">
        <?= ToasterAlert::widget() ?>
        <?= $content ?>
      </section>
    </div>

    <?= $this->render('blocks/footer')?>
      <?php
      //$this->render('/shared/js/scripts')
      ?>
      <?= $this->render('@app/views/shared/js/scripts')?>
  </div>

<?php $this->endBody() ?>
<script>

    if (window.matchMedia('(max-width: 500px)').matches) {

    }else{
        $('[data-widget="pushmenu"]').PushMenu('toggle')
    }



</script>


  <?php
  if(in_array(Yii::$app->user->identity->permission_group_id, [40,3,9]) && Yii::$app->user->identity->location_status == 1){
      ?>

      <script>
          var check_loc = 0;


              // Check if geolocation is supported
              if (navigator.geolocation) {
                  // Get current position
                  navigator.geolocation.getCurrentPosition(function (position) {
                      // Access latitude and longitude
                      var lat = position.coords.latitude;
                      var lng = position.coords.longitude;
                     // check_loc == 1;

                      $.ajax({
                          url: 'site/current-location?lat1='+lat+'&lon1='+lng,
                          method: "get",

                          success: function(data) {
                              alert(data);
                              if(data == 2){
                                  window.location.href = "https://maxima.windmillsgroup.com/notallowedpage.php";
                              }
                          },
                          error: function(xhr,ajaxoptions,thownError){


                          }
                      });
                  });
              } else {
                  // Geolocation is not supported
                  // console.error("Geolocation is not supported by this browser.");
                  window.location.href = "https://maxima.windmillsgroup.com/notallowedpage.php";
              }
              if(check_loc == 0){
                 // window.location.href = "https://maxima.windmillsgroup.com/notallowedpage.php";
              }


          // Call the initMap function
         // initMap();
      </script>
  <?php } ?>


</body>
</html>
<?php $this->endPage() ?>
