<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

use app\assets\SortBootstrapAsset;
SortBootstrapAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', $title_view);
$cardTitle = Yii::t('app', $title_view);
$this->params['breadcrumbs'][] = $this->title;
?>


<style>
    th {
        color: #0056b3;
        font-size: 16px;
     /*  // text-align: right !important;*/
        padding-right: 30px !important;
    }
    td {
        font-size: 16px;
     /*   //text-align: right;*/
        padding-right: 50px;
    }
}
</style>

<div class="bank-revenue-index">
    <div class="card card-outline card-primary">
        <div class="card-body">
            <table id="bank-revenue" class="table table-striped dataTable">
                <thead>
                    <tr>

                        <th class="">Building/ Project Name</th>
                        <th class="">Room Number Estimated</th>
                        <th class=""> BUA </th>
                        <th class="">Plot Area </th>
                        <th class="">Price AED </th>
                        <th class=""> AED/sqf </th>
                        <th class=""> Date </th>
                        <th class=""> Unit Number </th>
                        <th class=""> Floor Number </th>
                        <th class=""> Parking Space </th>
                        <th class=""> Balcony Area </th>


                    </tr>
                </thead>
                <tbody>
                    <?php 
                    if(count($all_data)>0){
                        foreach($all_data as $model){
                    ?>
                        <tr>

                            <td><?= $model->building_name; ?></td>
                            <td><?= $model->no_of_rooms; ?></td>
                            <td><?= $model->bua; ?></td>
                            <td><?= $model->plot_area; ?></td>
                            <td><?= $model->price; ?></td>
                            <td><?= $model->price_sqf; ?></td>
                            <td><?=  date('d-M-Y', strtotime($model->	sold_date))  ?></td>
                            <td><?= $model->unit_number; ?></td>
                            <td><?= $model->floor_number; ?></td>
                            <td><?= $model->balcony_size; ?></td>
                            <td><?= $model->parking_space_number; ?></td>


                        </tr>
                    <?php
                        }
                    }else{
                        
                    }
                    ?>
                </tbody>

            </table>
        </div>
    </div>
</div>

<?php 
    $this->registerJs('
        $("#bank-revenue").DataTable({
            order: [[0, "desc"]],
            pageLength: 50,
        });
    ');
?>




