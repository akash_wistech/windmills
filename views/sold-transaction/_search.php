<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ListingsTransactionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="listings-transactions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'listings_reference') ?>

    <?= $form->field($model, 'source') ?>

    <?= $form->field($model, 'listing_website_link') ?>

    <?= $form->field($model, 'listing_date') ?>

    <?php // echo $form->field($model, 'property_listed') ?>

    <?php // echo $form->field($model, 'property_category') ?>

    <?php // echo $form->field($model, 'tenure') ?>

    <?php // echo $form->field($model, 'unit_number') ?>

    <?php // echo $form->field($model, 'floor_number') ?>

    <?php // echo $form->field($model, 'number_of_levels') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'land_size') ?>

    <?php // echo $form->field($model, 'built_up_area') ?>

    <?php // echo $form->field($model, 'balcony_size') ?>

    <?php // echo $form->field($model, 'property_placement') ?>

    <?php // echo $form->field($model, 'property_visibility') ?>

    <?php // echo $form->field($model, 'property_exposure') ?>

    <?php // echo $form->field($model, 'listing_property_type') ?>

    <?php // echo $form->field($model, 'property_condition') ?>

    <?php // echo $form->field($model, 'development_type') ?>

    <?php // echo $form->field($model, 'finished_status') ?>

    <?php // echo $form->field($model, 'pool') ?>

    <?php // echo $form->field($model, 'gym') ?>

    <?php // echo $form->field($model, 'play_area') ?>

    <?php // echo $form->field($model, 'other_facilities') ?>

    <?php // echo $form->field($model, 'completion_status') ?>

    <?php // echo $form->field($model, 'no_of_bedrooms') ?>

    <?php // echo $form->field($model, 'upgrades') ?>

    <?php // echo $form->field($model, 'full_building_floors') ?>

    <?php // echo $form->field($model, 'parking_space') ?>

    <?php // echo $form->field($model, 'view') ?>

    <?php // echo $form->field($model, 'landscaping') ?>

    <?php // echo $form->field($model, 'white_goods') ?>

    <?php // echo $form->field($model, 'furnished') ?>

    <?php // echo $form->field($model, 'utilities_connected') ?>

    <?php // echo $form->field($model, 'developer_margin') ?>

    <?php // echo $form->field($model, 'listings_price') ?>

    <?php // echo $form->field($model, 'listings_rent') ?>

    <?php // echo $form->field($model, 'price_per_sqt') ?>

    <?php // echo $form->field($model, 'final_price') ?>

    <?php // echo $form->field($model, 'agent_name') ?>

    <?php // echo $form->field($model, 'agent_phone_no') ?>

    <?php // echo $form->field($model, 'agent_company') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'trashed') ?>

    <?php // echo $form->field($model, 'trashed_at') ?>

    <?php // echo $form->field($model, 'trashed_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
