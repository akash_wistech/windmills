<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Staff Members');
$cardTitle = Yii::t('app','Update:  {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['ontime']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-update">
    <?= $this->render('/user/ontime_staff_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
