<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Staff Members');
$cardTitle = Yii::t('app','New Staff Member');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['ontime']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="user-create">
    <?= $this->render('/user/ontime_staff_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
