<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Staff Members');
$cardTitle = Yii::t('app','New Staff Member');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['approvedpr']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="user-create">
    <?= $this->render('/user/approvedpr_staff_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
