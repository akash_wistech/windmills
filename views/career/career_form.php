<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\StaffFormAsset;
StaffFormAsset::register($this);
use kartik\select2\Select2;

use app\models\UserDegreeInfo;
use app\models\UserExperience;
use app\models\UserReference;


/* @var $this yii\web\View */
/* @var $model app\models\Staff */
/* @var $form yii\widgets\ActiveForm */

if($model->profileInfo!=null){
  $model->job_title_id=$model->profileInfo->job_title_id;
  $model->job_title=$model->profileInfo->jobTitle->title;
  $model->department_id=$model->profileInfo->department_id;
  $model->department_name=$model->profileInfo->department->title;
  $model->mobile=$model->profileInfo->mobile;
  $model->phone1=$model->profileInfo->phone1;
  $model->phone2=$model->profileInfo->phone2;
  $model->address=$model->profileInfo->address;
  $model->background_info=$model->profileInfo->background_info;

  $model->gender=$model->profileInfo->gender;
  $model->middlename=$model->profileInfo->middlename;
  $model->country_id=$model->profileInfo->country_id;
  $model->nationality=$model->profileInfo->nationality;
  $model->passport_number=$model->profileInfo->passport_number;
  $model->birth_date=$model->profileInfo->birth_date;
  $model->marriage_date=$model->profileInfo->marriage_date;
  $model->marital_status=$model->profileInfo->marital_status;
  $model->no_of_children=$model->profileInfo->no_of_children;
  $model->mother_name=$model->profileInfo->mother_name;
  $model->mother_profession=$model->profileInfo->mother_profession;
  $model->father_name=$model->profileInfo->father_name;
  $model->father_profession=$model->profileInfo->father_profession;
  $model->street=$model->profileInfo->street;
  $model->apartment=$model->profileInfo->apartment;
  $model->community=$model->profileInfo->community;
  $model->city=$model->profileInfo->city;
  $model->ijari=$model->profileInfo->ijari;
  $model->emergency_home=$model->profileInfo->emergency_home;
  $model->emergency_uae=$model->profileInfo->emergency_uae;

  $model->origin_apartment=$model->profileInfo->origin_apartment;
  $model->origin_street=$model->profileInfo->origin_street;
  $model->origin_country=$model->profileInfo->origin_country;
  $model->origin_city=$model->profileInfo->origin_city;
  $model->personal_email=$model->profileInfo->personal_email;

  $model->ncc=$model->profileInfo->ncc;
  $model->ncc_breakage_cost=$model->profileInfo->ncc_breakage_cost;
  $model->notice_period=$model->profileInfo->notice_period;
  $model->early_breakage_cost=$model->profileInfo->early_breakage_cost;
  $model->visa_type=$model->profileInfo->visa_type;
  $model->visa_emirate=$model->profileInfo->visa_emirate;
  $model->joining_date=$model->profileInfo->joining_date;
  $model->visa_start_date=$model->profileInfo->visa_start_date;
  $model->visa_end_date=$model->profileInfo->visa_end_date;
  $model->driving_license=$model->profileInfo->driving_license;
  
  
  $model->whatsapp_groups = json_decode($model->profileInfo->whatsapp_groups, true);
  $model->group_emails = json_decode($model->profileInfo->group_emails, true);
  $model->hobby = json_decode($model->profileInfo->hobby, true);
  $model->language = json_decode($model->profileInfo->language, true);
  $model->total_experience=$model->profileInfo->total_experience;
  $model->relevant_experience=$model->profileInfo->relevant_experience;


  $model->basic_salary=$model->profileInfo->basic_salary;
  $model->house_allowance=$model->profileInfo->house_allowance;
  $model->transport_allowance=$model->profileInfo->transport_allowance;
  $model->last_gross_salary=$model->profileInfo->last_gross_salary;
  $model->tax_rate=$model->profileInfo->tax_rate;
  $model->net_salary=$model->profileInfo->net_salary;
  $model->reports_to=$model->profileInfo->reports_to;
  $model->other_allowance=$model->profileInfo->other_allowance;


  $model->picture=$model->profileInfo->picture;
  $model->cv=$model->profileInfo->cv;
  $model->cover_letter=$model->profileInfo->cover_letter;
  $model->emirates_id=$model->profileInfo->emirates_id;
  $model->residence_visa=$model->profileInfo->residence_visa;
  $model->last_contract=$model->profileInfo->last_contract;
  $model->offer_letter=$model->profileInfo->offer_letter;
  $model->work_permit=$model->profileInfo->work_permit;
  $model->job_description=$model->profileInfo->job_description;
  $model->kpi=$model->profileInfo->kpi;


  $model->valuer_qualifications=$model->profileInfo->valuer_qualifications;
  $model->valuer_status=$model->profileInfo->valuer_status;
  $model->valuer_experience_expertise=$model->profileInfo->valuer_experience_expertise;

  $oldImage=$model->profileInfo->signature_img_name;

  // print_r($model->signature_file);
  // die();
  
}

$qualifications = \app\models\UserDegreeInfoCareer::find()->where(['user_id'=>$model->id])->all();
$experiences = \app\models\UserExperienceCareer::find()->where(['user_id'=>$model->id])->all();
$references = \app\models\UserReferenceCareer::find()->where(['user_id'=>$model->id])->all();



$this->registerJs('






  $("#career-job_title").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#career-job_title").val()!=suggestion.value){
        $("#career-job_title").val(suggestion.value);
      }
    },
    onInvalidateSelection: function() {
      $("#career-job_title").val("0");
    }
  });

  $("#career-department_name").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/departments']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#career-department_id").val()!=suggestion.data){
        $("#career-department_id").val(suggestion.data);
      }
    },
    onInvalidateSelection: function() {
      $("#career-department_id").val("0");
    }
  });

    $("#career-department-0-department").autocomplete({
      serviceUrl: "'.Url::to(['suggestion/departments']).'",
      noCache: true,
      onSelect: function(suggestion) {
        if($("#career-department-0-department").val()!=suggestion.data){
          $("#career-department-0-department").val(suggestion.data);
        }
      },
      onInvalidateSelection: function() {
        $("#career-department-0-department").val("0");
      }
    });

  $("#career-ref_dept-0-ref_dept").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/departments']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#career-ref_dept-0-ref_dept").val()!=suggestion.data){
        $("#career-ref_dept-0-ref_dept").val(suggestion.data);
      }
    },
    onInvalidateSelection: function() {
      $("#career-ref_dept-0-ref_dept").val("0");
    }
  });

  $("#career-ref_position-0-ref_position").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#career-ref_position-0-ref_position").val()!=suggestion.value){
        $("#career-ref_position-0-ref_position").val(suggestion.value);
      }
    },
    onInvalidateSelection: function() {
      $("#career-ref_position-0-ref_position").val("0");
    }
  });

  $("#career-windmills_position").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#career-windmills_position").val()!=suggestion.value){
        $("#career-windmills_position").val(suggestion.value);
      }
    },
    onInvalidateSelection: function() {
      $("#career-windmills_position").val("0");
    }
  });

  $("#career-position-0-position").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
    noCache: true,
    onSelect: function(suggestion) {
      if($("#career-position-0-position").val()!=suggestion.value){
        $("#career-position-0-position").val(suggestion.value);
      }
    },
    onInvalidateSelection: function() {
      $("#career-position-0-position").val("0");
    }
  });



  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
              $("#blah").attr("src", e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
      }
  }
  $(".imgInps").change(function(){
      readURL(this);
  });

  $("body").on("click", ".remove_file", function () {
  _this=$(this);
  swal({
  title: "'.Yii::t('app','Confirmation').'",
  html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
  type: "warning",
  showCancelButton: true,
      confirmButtonColor: "#47a447",
  confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
  cancelButtonText: "'.Yii::t('app','Cancel').'",
  },function(result) {
      if (result) {
    $.ajax({
    url: "'.Url::to(['site/logodelete','id'=>$model->id]).'",
    dataType: "html",
    type: "POST",
    success: function(html) {
    $("#blah").attr("src", "'.Yii::$app->params['unit_images'].'unnamed.png");
    },
    error: function(xhr,ajaxoptions,thownError){

      alert(xhr.responseText);
    }
    });
      }
  });
  });

  $("body").on("change", "#career-community", function () {

    var id = $(this).val();
    var url = "buildings/community/"+id;

    heading=$(this).data("heading");
    $.ajax({
        url: url,
        dataType: "html",
        success: function(data) {
            var response = JSON.parse(data);
            if(response.community.city != ""){
                $("#career-city").val(response.community.city);
            }
        },
        error: bbAlert
    });
});

');

$countries = ArrayHelper::map(\app\models\Country::find()->where(['status' => 1])->all(), 'id', 'title');
$instituteNamesArray = ArrayHelper::map(\app\models\InstitutionManager::find()->where(['status' => 'active'])->all(), 'id', 'institute_name');


$languageList = \app\models\WhatsappGroups::find()
    ->where(['type' => 'language']) // Add condition to filter by type
    ->orderBy(['title' => SORT_ASC])
    ->all();
$selectedLanguage = [];
$selectedIdsArray = [];
foreach ($languageList as $language) {
    if (in_array($language->id, $selectedIdsArray)) {
        $selectedLanguage[] = $language->title;
    }
}

$clientsArr = ArrayHelper::map(\app\models\User::find()
->select([
'id', 'CONCAT(firstname, " ", lastname) AS lastname',
])
    ->where(['status' => 1])
    ->andWhere(['user_type' => [10,20]])
    ->orderBy(['lastname' => SORT_ASC,])
    ->all(), 'id', 'lastname');
$clientsArr = ['' => 'select'] + $clientsArr;
if(isset($_GET['submitted']) && $_GET['submitted'] == 1){
    $submitted =1;
}else{
    $submitted =0;
}
?>
<?php if($submitted == 1){ ?>
    <div class="alert alert-success">
        <h3 class="text-center">Thank You for Applying</h3>
        <p>Dear Candidate,<br><br>
            Thank you for taking the time to complete our job application form for the position at <b>Windmills Group</b>. We appreciate your interest in joining our team and are currently reviewing applications.
            <br>
            We will be in touch with you shortly to provide updates on the next steps in the recruitment process. In the meantime, if you have any questions, feel free to reach out.
            <br> <br>
            Thank you again for considering an opportunity with us.
            <br>
            <br>
            Best regards,
            <br>
            <b>Windmills Group</b>
           </p>

    </div>

    <?php }else{ ?>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .width_40 {
            width: 40% !important;

        }

        .width_20 {
            width: 20% !important;

        }

        .padding_5 {
            padding: 5px;
        }

        .kv-file-content, .upload-docs img {
            width: 50px !important;
            height: 50px !important;
        }
    </style>
<?php

    if((isset($_GET['job']) && ($_GET['job'] <> null)) && (isset($_GET['dep']) && $_GET['dep'] <> null)){

    }else{
        header("Location: https://windmillsgroup.com/careers/");
        die();
    }




    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="hidden">
        <?= $form->field($model, 'job_title_id')->textInput(['value'=> $_GET['job']])?>
        <?= $form->field($model, 'department_id')->textInput(['value'=> $_GET['dep']])?>
    </div>
<section class="user-form card card-outline card-primary">

  <header class="card-header">
    <h2 class="card-title">Role at Windmills - <?= Yii::$app->appHelperFunctions->jobtitles[$_GET['job']]; ?> (Department- <?= Yii::$app->appHelperFunctions->jobDepartment[$_GET['dep']]; ?>)</h2>
  </header>
  <div class="card-body">
    <h6> <b> <u>  General Details </u> </b> </h6> <br>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'gender')->dropDownList([
                'male' => 'Male',
                'female' => 'Female',
            ]) ?>
        </div>

        <div class="col-sm-4">
          <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])?>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-4">
            <?php if($model->id > 0){  }else{
                $model->nationality =221;
            } ?>
            <?= $form->field($model, 'nationality', [
                'inputOptions' => ['id' => 'nationality', 'class' => 'form-control'],
            ])->dropDownList($countries, ['prompt' => 'Select Country']) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'birth_date')->textInput(['type' => 'date']) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'marital_status')->dropDownList([
                'single' => 'Single',
                'married' => 'Married',
                'divorced' => 'Divorced',
                'widowed' => 'Widowed',
                'separated' => 'Separated',
            ], ['prompt' => 'Select Marital Status']) ?>
        </div>




    </div>

    <div class="row">


        <div class="col-sm-4">
            <?= $form->field($model, 'no_of_children')->textInput(['type' => 'number']) ?>
        </div>
    </div>





    <hr style="border-color:#017BFE">

    <h6> <b> <u>Address of UAE Residence</u> </b> </h6> <br>



    <div class="row">
        <div class="col-sm-4">
            <?php
            echo $form->field($model, 'community')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(\app\models\Communities::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title'),
                'options' => ['placeholder' => 'Select a Community ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
      <div class="col-sm-4">
          <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readonly' => true]) ?>
      </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'phone1')->textInput(['maxlength' => true])->label('Personal Mobile Phone')?>
        </div>
        <?php if($model->id > 0){  }else{
            $model->country_id =221;
        } ?>

      <!--  <div class="col-sm-4">
            <?/*= $form->field($model, 'country_id', [
                'inputOptions' => ['id' => 'country_id' , 'class' => 'form-control'],
            ])->dropDownList($countries, ['prompt' => 'Select Country'])->label('Country of Residence') */?>
        </div>-->
        <!--<div class="col-sm-4">
            <?/*= $form->field($model, 'ijari')->dropDownList([
                'yes' => 'Yes',
                'no' => 'No',
            ], ['prompt' => 'Select Option']) */?>
        </div>-->
    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u>  Home Country Address </u> </b> </h6> <br>

    <div class="row">

            <div class="col-sm-4">
                <?= $form->field($model, 'origin_country')->dropDownList($countries, ['prompt' => 'Select Country']) ?>
            </div>
            <div class="col-sm-4">
                <?php if($model->origin_city === null || $model->origin_city == "0" ) { ?>
                    <?php echo $form->field($model, 'origin_city')->dropDownList([], ['prompt' => 'Select City', 'id' => 'origin-city']) ?>
                <?php } else {  ?>
                    <?= $form->field($model, 'origin_city')->textInput(['maxlength' => true])?>
                <?php } ?>

            </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])->label('Home Country Phone')?>
        </div>


    </div>


    <hr style="border-color:#017BFE">

      <h6> <b> <u>  Email Addresses </u> </b> </h6> <br>
    <div class="row">

      <div class="col-sm-4">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label('Personal Email Address')?>
      </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'personal_email')->textInput(['maxlength' => true])->label('Backup Email Address')?>
        </div>

    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u>  Qualification(s) </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-12">
      <table id="attachment" class="table table-striped table-bordered table-hover images-table">
          <thead>
            <tr>
                <td class="text-left">Degree Name</td>
                <td class="text-left">Institute Name</td>
                <td class="text-left">Passing Year</td>
                <td class="text-left">Percentage</td>
                <td class="text-left">Attested</td>
                <td class="text-left">Attachment</td>
                <td></td>
            </tr>
          </thead>

          <tbody>
            <?php $row = 0; ?>
            <?php foreach ($qualifications as $degree) { ?>
                <tr id="image-row<?php echo $row; ?>">

                    <td>
                        <input type="text" class="form-control"
                                name="Career[qualification][<?= $row ?>][degree_name]"
                                value="<?= $degree->degree_name ?>" placeholder="Degree Name"
                                required/>
                    </td>
                    <td>
                        <input type="text" class="form-control"
                                name="Career[qualification][<?= $row ?>][institute_name]"
                                value="<?= $degree->institute_name ?>" placeholder="Institute Name"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="Career[qualification][<?= $row ?>][passing_year]"
                                value="<?= $degree->passing_year ?>" placeholder="Passing Year"
                                required/>
                    </td>

                    <td>
                        <input type="number" class="form-control"
                                name="Career[qualification][<?= $row ?>][percentage]"
                                value="<?= $degree->percentage ?>"
                                placeholder="Percentage" required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="Career[qualification][<?= $row ?>][degree_attested]"
                                value="<?= $degree->degree_attested ?>"
                                placeholder="Degree Attested" required/>
                    </td>

                    <td>
                        <?php if (!empty($degree->degree_attachment)) : ?>

                            <img src="<?= $degree->degree_attachment ?>" alt="PDF Attachment" style="width: 50px; height: 50px;">

                            <a href="<?= $degree->degree_attachment ?>" target="_blank" class="btn btn-primary">View</a>
                        <?php else : ?>
                            <input type="text" class="form-control"
                                    value="<?= $degree->degree_attachment ?>"
                                    placeholder="Degree Attachment" required/>
                        <?php endif; ?>
                    </td>

                    <td>
                        <input type="hidden" class="form-control"
                                name="Career[qualification][<?= $row ?>][degree_attachment]"
                                value="<?= $degree->degree_attachment ?>"
                                required/>
                    </td>

                    <td>
                        <input type="hidden" class="form-control"
                                name="Career[qualification][<?= $row ?>][user_id]"
                                value="<?= $model->id ?>"
                                required/>
                    </td>


                    <td class='text-left'>
                        <button type='button' onclick='$("#image-row<?= $row ?>").remove();'
                                data-toggle='tooltip' title='Remove'
                                class='btn btn-danger'>
                            <i class='fa fa-minus-circle'></i>
                        </button>
                    </td>


                </tr>
                <?php $row++; ?>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
                <td colspan="6"></td>
                <td class="text-left">
                    <button type="button" onclick="addAttachment();"
                            data-toggle="tooltip" title="Add"
                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                    </button>
                </td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u>  Experience(s) </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-12">
      <table id="experience" class="table table-striped table-bordered table-hover images-table">
          <thead>
            <tr>
              <th>Company</th>
              <th>Position</th>
              <th>Department</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Gross Salary</th>
              <th>Reason of Leaving</th>
              <th>Action</th>
            </tr>
          </thead>

          <tbody>
            <?php $exp_row = 0; ?>
            <?php foreach ($experiences as $experience) { ?>
                <tr id="exp-row<?php echo $exp_row; ?>">

                    <td>
                        <input type="text" class="form-control"
                                name="Career[experience][<?= $exp_row ?>][company_name]"
                                value="<?= $experience->company_name ?>" placeholder="Company Name"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="Career[experience][<?= $exp_row ?>][position]"
                                value="<?= $experience->position ?>" placeholder="Position"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="Career[experience][<?= $exp_row ?>][department]"
                                value="<?= $experience->department ?>" placeholder="department"
                                required/>
                    </td>

                    <td>
                        <input type="date" class="form-control"
                                name="Career[experience][<?= $exp_row ?>][start_date]"
                                value="<?= $experience->start_date ?>" placeholder="Start Date"
                                required/>
                    </td>

                    <td>
                        <input type="date" class="form-control"
                                name="Career[experience][<?= $exp_row ?>][end_date]"
                                value="<?= $experience->end_date ?>" placeholder="End Date"
                                required/>
                    </td>

                    <td>
                        <input type="number" class="form-control"
                                name="Career[experience][<?= $exp_row ?>][gross_salary]"
                                value="<?= $experience->gross_salary ?>" placeholder="Gross Salary"
                                required/>
                    </td>


                    <td>
                        <input type="text" class="form-control"
                                name="Career[experience][<?= $exp_row ?>][leave_reason]"
                                value="<?= $experience->leave_reason ?>" placeholder="Leave Reason"
                                required/>
                    </td>

                    <td>
                        <input type="hidden" class="form-control"
                                name="Career[experience][<?= $exp_row ?>][user_id]"
                                value="<?= $model->id ?>"
                                required/>
                    </td>


                    <td class='text-left'>
                        <button type='button' onclick='$("#exp-row<?= $exp_row ?>").remove();'
                                data-toggle='tooltip' title='Remove'
                                class='btn btn-danger'>
                            <i class='fa fa-minus-circle'></i>
                        </button>
                    </td>


                </tr>
                <?php $exp_row++; ?>
            <?php } ?>
          </tbody>

          <tfoot>
            <tr>
                <td colspan="7"></td>
                <td class="text-left">
                    <button type="button" onclick="addExperience();"
                            data-toggle="tooltip" title="Add"
                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                    </button>
                </td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>

   <!-- <hr style="border-color:#017BFE">-->

   <!-- <h6> <b> <u>  Experience (Years) </u> </b> </h6> <br>
    <div class="row">
      <div class="col-sm-4">
          <?/*= $form->field($model, 'total_experience')->dropDownList(
              range(0, 50),
              ['prompt' => 'Select Total Experience']
          ) */?>
      </div>

      <div class="col-sm-4">
          <?/*= $form->field($model, 'relevant_experience')->dropDownList(
            range(0, 50),
            ['prompt' => 'Select Relevant Experience']
          ) */?>
      </div>
    </div>-->

    <hr style="border-color:#017BFE">

    <h6> <b> <u>  Latest Job Details (Additional) </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'visa_type')->dropDownList([
            'visit' => 'Visit',
            'cancelled' => 'Cancelled',
            'independent' => 'Independent',
            'residence' => 'Residence',
        ], ['prompt' => 'Select Option']) ?>
      </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'visa_start_date')->textInput(['type' => 'date']) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'visa_end_date')->textInput(['type' => 'date']) ?>
        </div>

    </div>

    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'joining_date')->textInput(['type' => 'date'])->label('Expected Joining Date') ?>
      </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'notice_period')->dropDownList(
                array_combine(range(0, 60), array_map(function ($value) {
                    return $value . ' days';
                }, range(0, 60))),
                ['prompt' => 'Select Option', 'class' => 'form-control']
            ) ?>
        </div>

    </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u> Skills </u> </b> </h6> <br>

    <div class="row">

        <div class="col-sm-4">
            <?php
            echo $form->field($model, 'driving_license')->widget(Select2::classname(), [
                'data' => $countries,
                'value' => $selectedHobby, // Preselect options based on titles
                'options' => ['placeholder' => 'Select Countries of License'],
                'pluginOptions' => [
                    'placeholder' => 'Select Countries of License',
                    'multiple' => true,
                    'allowClear' => true
                ],
            ])->label('Countries Of Driving License You Have:');
            ?>
        </div>

      <div class="col-sm-4">
        <?php
          echo $form->field($model, 'language')->widget(Select2::classname(), [
            'data' => ArrayHelper::map($languageList, 'id', 'title'),
            'value' => $selectedLanguage, // Preselect options based on titles
            'options' => ['placeholder' => 'Select Language(s)'],
            'pluginOptions' => [
                'placeholder' => 'Select Language(s)',
                'multiple' => true,
                'allowClear' => true
            ],
          ])->label('Select Language(s)');
        ?>
      </div>
        <div class="col-sm-4">
            <?php
            echo $form->field($model, 'tech_skills')->widget(Select2::classname(), [
                'data' => array('1'=>'MS Office','2'=>'ERP','3'=>'CRM','4'=>'Digital Marketing Tools','5'=>'Google Workspace','6'=>'OneDrive '),
                'value' => [], // Preselect options based on titles
                'options' => ['placeholder' => 'Select Technology skills(s)'],
                'pluginOptions' => [
                    'placeholder' => 'Technology skill(s)',
                    'multiple' => true,
                    'allowClear' => true
                ],
            ])->label('Technology skills');
            ?>
        </div>
    </div>

      <hr style="border-color:#017BFE">

      <h6> <b> <u>  Individual Value Addition(s) </u> </b> </h6> <br>

      <div class="row">
          <div class="col-sm-12">
              <table id="attachmentva" class="table table-striped table-bordered table-hover images-table">
                  <thead>
                  <tr>
                      <td class="text-left">Subject</td>
                      <td class="text-left">Action Made</td>
                      <td class="text-left">Achievement Percentage</td>
                      <td class="text-left">Turn Aroud Time taken (Months)</td>
                      <td></td>
                  </tr>
                  </thead>

                  <tbody>
                  <?php $row_va = 0; ?>
                  <?php foreach ($value_additions as $value_addition) { ?>
                      <tr>

                          <td>
                              <input type="text" class="form-control"
                                     name="Career[achievement][<?= $row_va ?>][subject]"
                                     value="<?= $value_addition->subject ?>" placeholder="Subject"
                                     required/>
                          </td>
                          <td>
                              <input type="text" class="form-control"
                                     name="Career[achievement][<?= $row_va ?>][action_made]"
                                     value="<?= $value_addition->action_made ?>" placeholder="Action Made"
                                     required/>
                          </td>



                          <td>
                              <input type="number" class="form-control"
                                     name="Career[achievement][<?= $row_va ?>][percentage]"
                                     value="<?= $value_addition->percentage ?>"
                                     placeholder="Achievement Percentage" required/>
                          </td>
                          <td>
                              <input type="text" class="form-control"
                                     name="Career[achievement][<?= $row_va ?>][trt]"
                                     value="<?= $value_addition->trt ?>" placeholder="Turn Aroud Time taken"
                                     required/>
                              <input type="hidden" class="form-control"
                                     name="Career[achievement][<?= $row_va ?>][user_id]"
                                     value="<?= $model->id ?>"
                                     required/>
                          </td>

                          <!--<td>
                              <input type="hidden" class="form-control"
                                     name="Career[achievement][<?/*= $row_va */?>][user_id]"
                                     value="<?/*= $model->id */?>"
                                     required/>
                          </td>-->
                      </tr>
                      <?php $row_va++; ?>
                  <?php } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                      <td colspan="4"></td>
                      <td class="text-left">
                          <button type="button" onclick="addAttachmentva();"
                                  data-toggle="tooltip" title="Add"
                                  class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                          </button>
                      </td>
                  </tr>
                  </tfoot>
              </table>
          </div>
      </div>

      <hr style="border-color:#017BFE">

      <h6> <b> <u>  Individual Achivements/KPI(s) </u> </b> </h6> <br>

      <div class="row">
          <div class="col-sm-12">
              <table id="attachmentkpi" class="table table-striped table-bordered table-hover images-table">
                  <thead>
                  <tr>
                      <td class="text-left">Subject</td>
                      <td class="text-left">Action Made</td>
                      <td class="text-left">Achievement Percentage</td>
                      <td class="text-left">Turn Aroud Time taken (Months)</td>
                      <td></td>
                  </tr>
                  </thead>

                  <tbody>
                  <?php $row_kpi = 0; ?>
                  <?php foreach ($value_additions as $value_addition) { ?>
                      <tr>

                          <td>
                              <input type="text" class="form-control"
                                     name="Career[kpis][<?= $row_kpi ?>][subject]"
                                     value="<?= $value_addition->subject ?>" placeholder="Subject"
                                     required/>
                          </td>
                          <td>
                              <input type="text" class="form-control"
                                     name="Career[kpis][<?= $row_kpi ?>][action_made]"
                                     value="<?= $value_addition->action_made ?>" placeholder="Action Made"
                                     required/>
                          </td>



                          <td>
                              <input type="number" class="form-control"
                                     name="Career[kpis][<?= $row_kpi ?>][percentage]"
                                     value="<?= $value_addition->percentage ?>"
                                     placeholder="kpi Percentage" required/>
                          </td>
                          <td>
                              <input type="text" class="form-control"
                                     name="Career[kpis][<?= $row_kpi ?>][trt]"
                                     value="<?= $value_addition->trt ?>" placeholder="Turn Aroud Time taken"
                                     required/>
                              <input type="hidden" class="form-control"
                                     name="Career[kpis][<?= $row_kpi ?>][user_id]"
                                     value="<?= $model->id ?>"
                                     required/>
                          </td>

                          <!--<td>
                              <input type="hidden" class="form-control"
                                     name="Career[kpis][<?/*= $row_kpi */?>][user_id]"
                                     value="<?/*= $model->id */?>"
                                     required/>
                          </td>-->
                      </tr>
                      <?php $row_kpi++; ?>
                  <?php } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                      <td colspan="4"></td>
                      <td class="text-left">
                          <button type="button" onclick="addAttachmentkpi();"
                                  data-toggle="tooltip" title="Add"
                                  class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                          </button>
                      </td>
                  </tr>
                  </tfoot>
              </table>
          </div>
      </div>

    <hr style="border-color:#017BFE">

    <h6> <b> <u> Reference(s) </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-12">
      <table id="reference" class="table table-striped table-bordered table-hover images-table">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Nationality</th>
              <th>Company</th>
              <th>Department</th>
              <th>Position</th>
              <th>Office Phone</th>
              <th>Mobile Phone</th>
              <th>Action</th>
            </tr>
          </thead>

          <tbody>
            <?php $ref_row = 0; ?>
            <?php foreach ($references as $reference) { ?>
                <tr id="ref-row<?php echo $ref_row; ?>">

                    <td>
                        <input type="text" class="form-control"
                                name="Career[reference][<?= $ref_row ?>][first_name]"
                                value="<?= $reference->first_name ?>" placeholder="First Name"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="Career[reference][<?= $ref_row ?>][last_name]"
                                value="<?= $reference->last_name ?>" placeholder="Last Name"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="Career[reference][<?= $ref_row ?>][nationality]"
                                value="<?= $reference->nationality ?>" placeholder="Nationality"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="Career[reference][<?= $ref_row ?>][company]"
                                value="<?= $reference->company ?>" placeholder="Company"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="Career[reference][<?= $ref_row ?>][department]"
                                value="<?= $reference->department ?>" placeholder="Department"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="Career[reference][<?= $ref_row ?>][position]"
                                value="<?= $reference->position ?>" placeholder="Position"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="Career[reference][<?= $ref_row ?>][office_phone]"
                                value="<?= $reference->office_phone ?>" placeholder="Office Phone"
                                required/>
                    </td>

                    <td>
                        <input type="text" class="form-control"
                                name="Career[reference][<?= $ref_row ?>][mobile_phone]"
                                value="<?= $reference->mobile_phone ?>" placeholder="Mob. Phone"
                                required/>
                    </td>





                    <td>
                        <input type="hidden" class="form-control"
                                name="Career[reference][<?= $ref_row ?>][user_id]"
                                value="<?= $model->id ?>"
                                required/>
                    </td>


                    <td class='text-left'>
                        <button type='button' onclick='$("#ref-row<?= $ref_row ?>").remove();'
                                data-toggle='tooltip' title='Remove'
                                class='btn btn-danger'>
                            <i class='fa fa-minus-circle'></i>
                        </button>
                    </td>


                </tr>
                <?php $ref_row++; ?>
            <?php } ?>
          </tbody>

          <tfoot>
            <tr>
                <td colspan="9"></td>
                <td class="text-left">
                    <button type="button" onclick="addReference();"
                            data-toggle="tooltip" title="Add"
                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                    </button>
                </td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>




      <hr style="border-color:#017BFE">
      <h6> <b> <u> Cover Letter </u> </b> </h6> <br>
      <div class="row">
          <div class="col-sm-12">
              <?= $form->field($model, 'cover_letter')->textarea([
                  'rows' => 6,
                  'id' => 'limited-textarea',
                  'maxlength' => true, // optional to limit characters
                  'placeholder' => 'Write here (Maximum 500 words)...'
              ])->label(false) ?>

          </div>
      </div>

    <hr style="border-color:#017BFE">
    <h6> <b> <u> Attachments: </u> </b> </h6> <br>

    <div class="row">
      <div class="col-sm-4">
      <h6> <b> Picture </b> </h6>
          <?= $form->field($model, 'picture')->fileInput()->label(false) ?>
      </div>
     <!-- <div class="col-sm-4">
      <h6> <b> CV </b> </h6>
          <?/*= $form->field($model, 'cv')->fileInput()->label(false) */?>
      </div>
      <div class="col-sm-4">
      <h6> <b> Cover Letter </b> </h6>
          <?/*= $form->field($model, 'cover_letter')->fileInput()->label(false) */?>
      </div>-->
    </div>

    <div class="row">
        <div class="col-4">
            <!-- Display the image -->
            <?php if (isset($model->picture)) : ?>
                <img src="<?= $model->picture ?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                <br>
                <center> <a href="<?= $model->picture ?>" target="_blank">View</a> <center>
                <?php endif; ?>
        </div>

       <!-- <div class="col-4">

            <?php /*if (isset($model->cv)) : */?>
                <img src="<?/*= $model->cv */?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                <br>
                <center> <a href="<?/*= $model->cv */?>" target="_blank">View</a> <center>
                <?php /*endif; */?>
        </div>

        <div class="col-4">

            <?php /*if (isset($model->cover_letter)) : */?>
                <img src="<?/*= $model->cover_letter */?>" alt="Uploaded Image" style="width: 100%; height: 80%;">
                <br>
                <center> <a href="<?/*= $model->cover_letter */?>" target="_blank">View</a> <center>
                <?php /*endif; */?>
        </div>-->
    </div>



  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>

<?php } ?>
<?php
$expjs2 = <<<JS
$(document).ready(function() {
    $(document).on('click', '.remove-degree', function() {
        $(this).closest('tr').remove();
    });
});
JS;

$this->registerJs($expjs2);
?>

<?php
$expjs = <<<JS
$(document).ready(function() {
    $(document).on('click', '.remove-experience', function() {
        $(this).closest('tr').remove();
    });
});
JS;

$this->registerJs($expjs);
?>

<?php
$referencejs = <<<JS
$(document).ready(function() {
    $(document).on('click', '.remove-reference', function() {
        $(this).closest('tr').remove();
    });
});
JS;

$this->registerJs($referencejs);
?>

<?php
$this->registerJs('

var dropdown1 = document.getElementById("country_id");
var dropdown2 = document.getElementById("nationality");

dropdown1.addEventListener("change", function () {
  dropdown2.value = dropdown1.value;
});



$(".toggle-password").on("click", function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

    
');
?>


<?php

$script = <<< JS
    $('#career-origin_country').change(function(){
      var id = $(this).val();
      var url = "buildings/city/"+id;

      heading=$(this).data("heading");
        $.ajax({
            url: url,
            dataType: "html",
            success: function(data) {
              var response = JSON.parse(data);
              console.log(response)
              var citiesArray = response.cities;
              var citySelect = $('#origin-city');
              citySelect.empty();
              citiesArray.forEach(function(city) {
                  citySelect.append('<option value="' + city + '">' + city + '</option>');
              });
              },
            error: function() {
                console.log('Error occurred while fetching cities.');
            }
        });
    });
JS;

$this->registerJs($script, \yii\web\View::POS_END);
?>

<style>
.password-input-container {
  position: relative;
}

.password-input-container .toggle-password {
  position: absolute;
  top: 72%;
  right: 10px; /* Adjust this value as needed */
  transform: translateY(-50%);
  cursor: pointer;
}
</style>

<script type="text/javascript">

        var uploadAttachment = function (attachmentId) {

            $('#form-upload').remove();
            $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

            $('#form-upload input[name=\'file\']').trigger('click');

            if (typeof timer != 'undefined') {
                clearInterval(timer);
            }

            timer = setInterval(function() {
                if ($('#form-upload input[name=\'file\']').val() != '') {
                    clearInterval(timer);

                    $.ajax({
                        url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                        type: 'post',
                        dataType: 'json',
                        data: new FormData($('#form-upload')[0]),
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function() {
                            $('#thumb-image' + attachmentId + ' img').hide();
                            $('#thumb-image' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                            $('#thumb-image' + attachmentId).prop('disabled', true);
                        },
                        complete: function() {
                            $('#thumb-image' + attachmentId + ' i').replaceWith('<i></i>');
                            $('#thumb-image' + attachmentId).prop('disabled', false);
                            $('#thumb-image' + attachmentId + ' img').show();
                        },
                        success: function(json) {
                            if (json['error']) {
                                alert(json['error']);
                            }

                            if (json['success']) {
                                console.log(json.file.href);
                                var str1 = json['file'].href;
                                var str2 = ".pdf";
                                var str3 = ".doc";
                                var str4 = ".docx";
                                var str5 = ".xlsx";
                                var str6 = ".xls";
                                if(str1.indexOf(str2) != -1){
                                    $('#thumb-image' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                                }else if(str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1){
                                    $('#thumb-image' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                                }else {
                                    $('#thumb-image' + attachmentId + ' img').prop('src', json['file'].href);
                                }
                                $('#input-image' + attachmentId).val(json['file'].href);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            }, 500);
        }

    </script>

<script type="text/javascript">

        var uploadAttachmentref = function (attachmentId) {

            $('#form-upload-ref').remove();
            $('body').prepend('<form enctype="multipart/form-data" id="form-upload-ref" style="display: none;"><input type="file" name="file" value="" /></form>');

            $('#form-upload-ref input[name=\'file\']').trigger('click');

            if (typeof timer != 'undefined') {
                clearInterval(timer);
            }

            timer = setInterval(function() {
                if ($('#form-upload-ref input[name=\'file\']').val() != '') {
                    clearInterval(timer);

                    $.ajax({
                        url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                        type: 'post',
                        dataType: 'json',
                        data: new FormData($('#form-upload-ref')[0]),
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function() {
                            $('#thumb-image-ref' + attachmentId + ' img').hide();
                            $('#thumb-image-ref' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                            $('#thumb-image-ref' + attachmentId).prop('disabled', true);
                        },
                        complete: function() {
                            $('#thumb-image-ref' + attachmentId + ' i').replaceWith('<i></i>');
                            $('#thumb-image-ref' + attachmentId).prop('disabled', false);
                            $('#thumb-image-ref' + attachmentId + ' img').show();
                        },
                        success: function(json) {
                            if (json['error']) {
                                alert(json['error']);
                            }

                            if (json['success']) {
                                console.log(json.file.href);
                                var str1 = json['file'].href;
                                var str2 = ".pdf";
                                var str3 = ".doc";
                                var str4 = ".docx";
                                var str5 = ".xlsx";
                                var str6 = ".xls";
                                if(str1.indexOf(str2) != -1){
                                    $('#thumb-image-ref' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                                }else if(str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1){
                                    $('#thumb-image-ref' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                                }else {
                                    $('#thumb-image-ref' + attachmentId + ' img').prop('src', json['file'].href);
                                }
                                $('#input-image' + attachmentId).val(json['file'].href);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            }, 500);
        }

    </script>

<!-- Degree -->
<?php
  if (isset($row)) {
      $row = $row;
  } else {
      $row = 0;
  }

  $instituteNamesArrayJson = json_encode($instituteNamesArray);
  if(isset($model->id) && ($model->id <> null)){
      $user_id = $model->id;
  }else{
      $user_id = 0;
  }


$script = <<<JS
$(document).ready(function(){
    $('#limited-textarea').on('input', function() {
        var text = $(this).val();
        var words = text.trim().split(/\s+/); // Split text by spaces
        var wordCount = words.filter(function(word) {
            return word.length > 0; // Count non-empty words
        }).length;

        if (wordCount > 500) {
            var trimmedText = words.slice(0, 500).join(' ');
            $(this).val(trimmedText); // Trim the text to 500 words
            alert('You have reached the 500-word limit.');
        }

        $('#word-count').text('Words: ' + wordCount + '/500');
    });
});


JS;

$this->registerJs($script);


$upload_image =Yii::$app->params['uploadIcon'];

  $scriptDegree = <<<JS
      var row = $row; 
      var user_id = $user_id;
     

      function addAttachment() {

        var html = "<tr id='image-row" + row + "'>";
        
          html += "<td><div class='form-group'><select class='form-control' name='Career[qualification][" + row + "][degree_name]' required>";
            html += "<option value=''>Select Degree</option>";
            html += "<option value='secondary_schooling'>Secondary Schooling</option>";
            html += "<option value='high_school_diploma'>High School Diploma</option>";
          
            html += "<option value='bachelors_degree'>Bachelor\'s Degree</option>";
            html += "<option value='masters_degree'>Master\'s Degree</option>";
            html += "<option value='doctorate_degree'>Doctorate Degree</option>";
             html += "<option value='professional_degree'>Professional Degree</option>";
          html += "</select></div></td>";
          
           html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[qualification][" + row + "][institute_name]' value='' /></div></td>";

        

          var currentYear = new Date().getFullYear();
          var selectHtml = '<select class="form-control" name="Career[qualification][' + row + '][passing_year]" required>';
          selectHtml += '<option value="">Select Year</option>';
          for (var year = currentYear - 30; year <= currentYear; year++) {
              selectHtml += '<option value="' + year + '">' + year + '</option>';
          }
          selectHtml += '</select>';
          html += '<td><div class="form-group">' + selectHtml + '</div></td>';

          var selectHtml = '<select class="form-control" name="Career[qualification][' + row + '][percentage]" required>';
          selectHtml += '<option value="">Select</option>';
          for (var i = 1; i <= 100; i++) {
              selectHtml += '<option value="' + i + '">' + i + '</option>';
          }
          selectHtml += '</select>';
          html += '<td><div class="form-group">' + selectHtml + '</div></td>';

          var selectHtml = '<select class="form-control" name="Career[qualification][' + row + '][degree_attested]" required>';
          selectHtml += '<option value="">Select</option>';
          selectHtml += '<option value="Yes">Yes</option>';
          selectHtml += '<option value="No">No</option>';
          selectHtml += '</select>';
          html += '<td><div class="form-group">' + selectHtml + '</div></td>';

    html += '<td><div class="upload-docs" >';
    html += '<div class="form-group field-clientprovidedrents-status">';
        html += '  <a href="javascript:;" id="thumb-image' + row + '" data-toggle="image" onclick="uploadAttachment('+row+')" class="img-thumbnail">';
        html += ' <img src="https://maxima.windmillsgroup.com/images/upload-image.png" alt="" title="" data-placeholder="" />';
        html += '   </a>';
        html += ' <input type="hidden" name="Career[qualification][' + row + '][degree_attachment]" value="" id="input-image' + row + '" />';
       html += '<div class="help-block"></div>';
    html += '</div>';
    html += '</div></td>';
          
         // html += "<td><div class='form-group'><input type='hidden' class='form-control' name='Career[qualification][" + row + "][user_id]' value=" + user_id +" /></div></td>";
          
          html += "<td class='text-left'><button type='button' onclick=\"$('#image-row" + row + "').remove();\" data-toggle='tooltip' title='Remove' class='btn btn-danger'><i class='fa fa-minus-circle'></i></button></td>";
          
          html += "</tr>";

          $("#attachment tbody").append(html);
          row++;
      }
  JS;

  $this->registerJs($scriptDegree, \yii\web\View::POS_END);


 $scriptDegreeva = <<<JS
      var row_va = $row_va; 
      var user_id = $user_id;
    

      function addAttachmentva() {

        var html = "<tr id='va-row" + row_va + "'>";
        
          html += "<td><div class='form-group'><select class='form-control' name='Career[achievement][" + row_va + "][subject]' required>";
            html += "<option value=''>Select Subject</option>";
            html += "<option value='Increased revenue'>Increased revenue</option>";
            html += "<option value='Decreased Costs'>Decreased Costs</option>";
            html += "<option value='Managed Project'>Managed Project</option>";
            html += "<option value='Other'>Other</option>";
          html += "</select></div></td>";
          
           html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[achievement][" + row_va + "][action_made]' value='' /></div></td>";

        

   var selectHtml = '<select class="form-control" name="Career[achievement][' + row_va + '][percentage]" required>';
          selectHtml += '<option value="">Select</option>';
          for (var i = 1; i <= 100; i++) {
              selectHtml += '<option value="' + i + '">' + i + '</option>';
          }
          selectHtml += '</select>';
          html += '<td><div class="form-group">' + selectHtml + '</div></td>';

          var selectHtml = '<select class="form-control" name="Career[achievement][' + row_va + '][trt]" required>';
          selectHtml += '<option value="">Select</option>';
          for (var i = 1; i <= 100; i++) {
              selectHtml += '<option value="' + i + '">' + i + '</option>';
          }
          selectHtml += '</select>';
          html += '<td><div class="form-group">' + selectHtml + '</div></td>';

        html += "<td class='text-left'><button type='button' onclick=\"$('#va-row" + row_va + "').remove();\" data-toggle='tooltip' title='Remove' class='btn btn-danger'><i class='fa fa-minus-circle'></i></button></td>";
          
          html += "</tr>";

          $("#attachmentva tbody").append(html);
          row++;
      }
  JS;

  $this->registerJs($scriptDegreeva, \yii\web\View::POS_END);



$scriptDegreekpi = <<<JS
      var row_kpi = $row_kpi; 
      var user_id = $user_id;
    

      function addAttachmentkpi() {

        var html = "<tr id='kpi-row" + row_kpi + "'>";
        
          html += "<td><div class='form-group'><select class='form-control' name='Career[kpis][" + row_kpi + "][subject]' required>";
            html += "<option value=''>Select Subject</option>";
            html += "<option value='Increased revenue'>Increased revenue</option>";
            html += "<option value='Decreased Costs'>Decreased Costs</option>";
            html += "<option value='Managed Project'>Managed Project</option>";
            html += "<option value='Other'>Other</option>";
          html += "</select></div></td>";
          
           html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[kpis][" + row_kpi + "][action_made]' value='' /></div></td>";

        

   var selectHtml = '<select class="form-control" name="Career[kpis][' + row_kpi + '][percentage]" required>';
          selectHtml += '<option value="">Select</option>';
          for (var i = 1; i <= 100; i++) {
              selectHtml += '<option value="' + i + '">' + i + '</option>';
          }
          selectHtml += '</select>';
          html += '<td><div class="form-group">' + selectHtml + '</div></td>';

          var selectHtml = '<select class="form-control" name="Career[kpis][' + row_kpi + '][trt]" required>';
          selectHtml += '<option value="">Select</option>';
          for (var i = 1; i <= 100; i++) {
              selectHtml += '<option value="' + i + '">' + i + '</option>';
          }
          selectHtml += '</select>';
          html += '<td><div class="form-group">' + selectHtml + '</div></td>';

        html += "<td class='text-left'><button type='button' onclick=\"$('#kpi-row" + row_kpi + "').remove();\" data-toggle='tooltip' title='Remove' class='btn btn-danger'><i class='fa fa-minus-circle'></i></button></td>";
          
          html += "</tr>";

          $("#attachmentkpi tbody").append(html);
          row++;
      }
  JS;

  $this->registerJs($scriptDegreekpi, \yii\web\View::POS_END);
?>




<?php
  if (isset($exp_row)) {
      $exp_row = $exp_row;
  } else {
      $exp_row = 0;
  }

   if(isset($model->id) && ($model->id <> null)){
      $user_id = $model->id;
  }else{
      $user_id = 0;
  }

  $scriptExp = <<<JS
      var exp_row = $exp_row; 
      var user_id = $user_id;

      function addExperience() {

        var html = "<tr id='exp-row" + exp_row + "'>";
          
        html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[experience][" + exp_row + "][company_name]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[experience][" + exp_row + "][position]' value='' /></div></td>";

        //html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[experience][" + exp_row + "][department]' value='' /></div></td>";

       var selectHtml = '<select class="form-control" name="Career[experience][' + exp_row + '][department]" required>';
          selectHtml += '<option value="">Select</option>';
          selectHtml += '<option value="Valuation">Valuation</option>';
          selectHtml += '<option value="IT">IT</option>';
          selectHtml += '<option value="Finance">Finance</option>';
          selectHtml += '<option value="Sales">Sales</option>';
          selectHtml += '<option value="Business">Business</option>';
          selectHtml += '<option value="Human Resources">Human Resources</option>';
          selectHtml += '<option value="Admin">Admin</option>';
          selectHtml += '<option value="Other">Other</option>';
          selectHtml += '<option value="Real Estate Valuation Officer">Real Estate Valuation Officer</option>';
          selectHtml += '<option value="Data Management">Data Management</option>';
          selectHtml += '<option value="Legal Management">Legal Management</option>';
        
          selectHtml += '</select>';
          html += '<td><div class="form-group">' + selectHtml + '</div></td>';
        html += "<td><div class='form-group'><input type='date' class='form-control' name='Career[experience][" + exp_row + "][start_date]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='date' class='form-control' name='Career[experience][" + exp_row + "][end_date]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='number' class='form-control' name='Career[experience][" + exp_row + "][gross_salary]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[experience][" + exp_row + "][leave_reason]' value='' /></div></td>";
        
        html += "<td class='text-left'><button type='button' onclick=\"$('#exp-row" + exp_row + "').remove();\" data-toggle='tooltip' title='Remove' class='btn btn-danger'><i class='fa fa-minus-circle'></i></button></td>";
        
        html += "</tr>";

          $("#experience tbody").append(html);
          exp_row++;
      }
  JS;

  $this->registerJs($scriptExp, \yii\web\View::POS_END);
?>


<?php
  if (isset($ref_row)) {
      $ref_row = $ref_row;
  } else {
      $ref_row = 0;
  }

   if(isset($model->id) && ($model->id <> null)){
      $user_id = $model->id;
  }else{
      $user_id = 0;
  }

  $scriptRef = <<<JS
      var ref_row = $ref_row; 
      var user_id = $user_id;

      function addReference() {

        var html = "<tr id='ref-row" + ref_row + "'>";
          
        html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[reference][" + ref_row + "][first_name]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[reference][" + ref_row + "][last_name]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[reference][" + ref_row + "][nationality]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[reference][" + ref_row + "][company]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[reference][" + ref_row + "][department]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[reference][" + ref_row + "][position]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[reference][" + ref_row + "][office_phone]' value='' /></div></td>";

        html += "<td><div class='form-group'><input type='text' class='form-control' name='Career[reference][" + ref_row + "][mobile_phone]' value='' /></div></td>";

      
        html += "<td class='text-left'><button type='button' onclick=\"$('#ref-row" + ref_row + "').remove();\" data-toggle='tooltip' title='Remove' class='btn btn-danger'><i class='fa fa-minus-circle'></i></button></td>";
        
        html += "</tr>";

          $("#reference tbody").append(html);
          ref_row++;
      }
  JS;

  $this->registerJs($scriptRef, \yii\web\View::POS_END);
?>


