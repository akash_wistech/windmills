<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Windmillsgroup Career');
$cardTitle = Yii::t('app','Windmillsgroup Career');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['create']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="user-create">
    <?= $this->render('career_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
