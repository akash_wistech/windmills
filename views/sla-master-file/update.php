<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SlaMasterFile */

$this->title = 'Update Sla Master File: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sla Master Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sla-master-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
