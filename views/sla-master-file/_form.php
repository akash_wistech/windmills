<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use  app\components\widgets\StatusVerified;

/* @var $this yii\web\View */
/* @var $model app\models\SlaMasterFile */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('

    $("body").on("change", ".client-cls", function () {
        //    console.log($(this).val());
           if($(this).val() == 2 || $(this).val() == 10 || $(this).val() == 20 || $(this).val() == 41 || $(this).val() == 46){
            $("#sub_property_type").show();
                   }else{
                    $("#sub_property_type").hide();
                   }
     
    });


    // $(document).ready(function() {

    //     $("#bua_from").hide();
    //     $("#bua_to").hide();
    //     $("#plot_size_from").hide();
    //     $("#plot_size_to").hide();
    //     $("#no_units_from").hide();
    //     $("#no_units_to").hide();
    
    //     var propertyTypeField = $("#slamasterfile-property_type");
    //     console.log(propertyTypeField.val());

    //     if(propertyTypeField.val() == 2 || propertyTypeField.val() == 6 || propertyTypeField.val() == 1  || propertyTypeField.val() == 17 || propertyTypeField.val() == 28 || propertyTypeField.val() == 4 || propertyTypeField.val() == 50 || propertyTypeField.val() == 53 || propertyTypeField.val() == 48 || propertyTypeField.val() == 29 || propertyTypeField.val() == 39 || propertyTypeField.val() == 5 || propertyTypeField.val() == 49){
    //         $("#bua_from").show();
    //         $("#bua_to").show();
    //     }else{
    //         $("#bua_from").hide();
    //         $("#bua_to").hide();
    //     }

    //     if(propertyTypeField.val() == 20 || propertyTypeField.val() == 10){
    //         $("#plot_size_from").show();
    //         $("#plot_size_to").show();
    //     }else{
    //         $("#plot_size_from").hide();
    //         $("#plot_size_to").hide();
    //     }

    //     if(propertyTypeField.val() == 81 || propertyTypeField.val() == 59 || propertyTypeField.val() == 3 || propertyTypeField.val() == 42 || propertyTypeField.val() == 25 || propertyTypeField.val() == 9 ){
    //         $("#no_units_from").show();
    //         $("#plot_size_to").show();
    //     }else{
    //         $("#no_units_from").hide();
    //         $("#plot_size_to").hide();
    //     }

    //     $(propertyTypeField).on("change", function () {
            
    //         if(propertyTypeField.val() == 2 || propertyTypeField.val() == 6 || propertyTypeField.val() == 1  || propertyTypeField.val() == 17 || propertyTypeField.val() == 28 || propertyTypeField.val() == 4 || propertyTypeField.val() == 50 || propertyTypeField.val() == 53 || propertyTypeField.val() == 48 || propertyTypeField.val() == 29 || propertyTypeField.val() == 39 || propertyTypeField.val() == 5 || propertyTypeField.val() == 49 ){
    //             $("#bua_from").show();
    //             $("#bua_to").show();
    //         }else{
    //             $("#bua_from").hide();
    //             $("#bua_to").hide();
    //         }

    //         if(propertyTypeField.val() == 20 || propertyTypeField.val() == 10){
    //             $("#plot_size_from").show();
    //             $("#plot_size_to").show();
    //         }else{
    //             $("#plot_size_from").hide();
    //             $("#plot_size_to").hide();
    //         }
    //         if(propertyTypeField.val() == 81 || propertyTypeField.val() == 59 || propertyTypeField.val() == 3 || propertyTypeField.val() == 42 || propertyTypeField.val() == 25 || propertyTypeField.val() == 9){
    //             $("#no_units_from").show();
    //             $("#no_units_to").show();
    //         }else{
    //             $("#no_units_from").hide();
    //             $("#no_units_to").hide();
    //         }


    //     });
    // });


');
?>

<section class="active-zone-form card">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <?php
                        echo $form->field($model, 'client')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Company::find()
                            ->where(['status' => 1])
                            ->andWhere([
                                'or',
                                ['data_type' => 0],
                                ['data_type' => null],
                                ])
                                ->orderBy(['title' => SORT_ASC,])
                                ->all(), 'id', 'title'),
                                
                                'options' => ['placeholder' => 'Select a Client ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                ])->label('Client Name');
                                ?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <?php
                        echo $form->field($model, 'property_type')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy([
                        'title' => SORT_ASC,
                        ])->all(), 'id', 'title'),
                        'options' => ['placeholder' => 'All Property Types','class'=> 'client-cls'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
            </div>
                <?php
                if($model->property_type == 2 || $model->property_type == 10 || $model->property_type == 20 || $model->property_type == 41 || $model->property_type == 46) {
                    $style = "block";
                }else{
                    $style = "none";
                }


                ?>

            <div class="col-sm-4" id="sub_property_type" style="display: <?= $style ?>">
                <?php
                echo $form->field($model, 'sub_property_type')->widget(Select2::classname(), [
                    'data' => array('0'=>'Select Type', '1'=>'Independent Villa','2'=> 'Community Villa'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'options' => ['placeholder' => 'Select Property Sub Type'],
                ]);
                ?>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <?php
            echo $form->field($model, 'city')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->emiratedListArrSla,
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <?php
            echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->buildingTenureArrSla,
                'options' => ['placeholder' => 'Select a Tenure ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <?php
            echo $form->field($model, 'inspection_type')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->inspectionTypeArr,
                'options' => ['placeholder' => 'Select a Inspection Type ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <?php
                    echo $form->field($model, 'tat')->widget(Select2::classname(), [
                        'data' => Yii::$app->appHelperFunctions->tatNumberSla,
                        'options' => ['placeholder' => 'Select a TAT ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('TAT');
                    ?>
                </div>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'priority_type')->widget(Select2::classname(), [
                    'data' => array('0'=>'Normal', '1'=> 'Urgent'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <?= $form->field($model, 'fee')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <?= $form->field($model, 'revalidation_fee')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
           <!-- <div class="col-4">
                <div class="form-group">
                    <?/*= $form->field($model, 'independent_villa_fee')->textInput(['maxlength' => true]) */?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <?/*= $form->field($model, 'community_villa_fee')->textInput(['maxlength' => true]) */?>
                </div>
            </div>-->
            <div class="col-4">
                <div class="form-group">
                    <?= $form->field($model, 'staff_fee')->textInput(['maxlength' => true]) ?>
                </div>
            </div>


        </div>

        <section class="active-zone-form card">
            <div class="card-body">
                <div class="row">

                    <?php
                    
                    // if($model->property_type == 2 || $model->property_type == 6 || $model->property_type == 1 || $model->property_type == 17 || $model->property_type == 28 || $model->property_type == 4 || $model->property_type == 50 || $model->property_type == 53 || $model->property_type == 48 || $model->property_type == 29 || $model->property_type == 39 || $model->property_type == 5 || $model->property_type == 49 ) {
                    //     $displayBua = "block";
                    // }else{
                    //     $displayBua = "none";
                    // }
                    // if($model->property_type == 20 || $model->property_type == 10 ) {
                    //     $displayPlotSize = "block";
                    // }else{
                    //     $displayPlotSize = "none";
                    // }
                    
                    // if($model->property_type == 81 || $model->property_type == 59 || $model->property_type == 3 || $model->property_type == 42 || $model->property_type == 25 ) {
                    //     $displayNoUnit = "block";
                    // }else{
                    //     $displayNoUnit = "none";
                    // }


                    ?>

                    <div class="col-3" id="bua_from" style="display: <?= $displayBua ?>">
                        <div class="form-group">
                            <?= $form->field($model, 'bua_from')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-3" id="bua_to" style="display: <?= $displayBua ?>" >
                        <div class="form-group">
                            <?= $form->field($model, 'bua_to')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-3" id="plot_size_from" style="display: <?= $displayPlotSize ?>">
                        <div class="form-group">
                            <?= $form->field($model, 'plot_size_from')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-3" id="plot_size_to" style="display: <?= $displayPlotSize ?>">
                        <div class="form-group">
                            <?= $form->field($model, 'plot_size_to')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-3" id="no_units_from" style="display: <?= $displayNoUnit ?>">
                        <div class="form-group">
                            <?= $form->field($model, 'no_of_units_from')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-3 id="no_units_to" style="display: <?= $displayNoUnit ?>">
                        <div class="form-group">
                            <?= $form->field($model, 'no_of_units_to')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-3" id="no_units_from" style="display: <?= $displayNoUnit ?>">
                        <div class="form-group">
                            <?= $form->field($model, 'no_of_levels_from')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-3" id="no_units_to" style="display: <?= $displayNoUnit ?>">
                        <div class="form-group">
                            <?= $form->field($model, 'no_of_levels_to')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <?php
        if(Yii::$app->menuHelperFunction->checkActionAllowed('Verified')){
            echo StatusVerified::widget([ 'model' => $model, 'form' => $form ]);
        }
        ?>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>