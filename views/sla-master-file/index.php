

<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;


$this->title = Yii::t('app', 'Sla Master Files');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;


if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    $createBtn=true;
}
// if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
//     $actionBtns.='{view}';
// }
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
    $actionBtns.='{delete}';
}
$actionBtns.='{get-history}';



?>
<div class="sla-master-file-index">


    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],

            [
                'attribute' => 'client',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                    return $model->clientt->title;
                },
              //  'filterType' => GridView::FILTER_SELECT2,
                'filter' =>ArrayHelper::map(\app\models\Company::find()->where(['data_type'=>0])->orWhere(['data_type'=>null])->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],

            [
                'attribute' => 'property_type',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title;
                },
                'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            [
                'attribute' => 'inspection_type',
                'label' => Yii::t('app', 'Inspection Type'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type];
                },
                'filter' =>Yii::$app->appHelperFunctions->inspectionTypeArr
            ],
            [
                'attribute' => 'tat',
                'label' => Yii::t('app', 'TAT'),
                'value' => function ($model) {
                    return $model->tat;
                },

            ],
            [
                'attribute' => 'city',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {


                    return Yii::$app->appHelperFunctions->emiratedListArrSla[$model->city];
                },
                'filter' => Yii::$app->appHelperFunctions->emiratedListArrSla
            ],
            [
                'attribute' => 'fee',
                'label' => Yii::t('app', 'Fee'),
                'value' => function ($model) {
                    return number_format($model->fee,2);
                },

            ],
            [
                'attribute' => 'bua_from',
                'label' => Yii::t('app', 'BUA'),
                'value' => function ($model) {
                    if($model->bua_from !== null && $model->bua_to !== null){
                        $bua = $model->bua_from.' to '.$model->bua_to;
                    }else if($model->bua_from !== null && $model->bua_from > 5000){
                        $bua = '> '.$model->bua_from;
                    }

                    return $bua;
                    // return number_format($bua,2);
                },

            ],
            [
                'attribute' => 'plot_size_from',
                'label' => Yii::t('app', 'Plot'),
                'value' => function ($model) {
                    if($model->plot_size_from !== null && $model->plot_size_to !== null){
                        $bua = $model->plot_size_from.' to '.$model->plot_size_to;
                    }else if($model->plot_size_from !== null && $model->plot_size_from > 5000){
                        $bua = '> '.$model->plot_size_from;
                    }

                    return $bua;
                    // return number_format($bua,2);
                },

            ],
            [
                'attribute' => 'no_of_units_from',
                'label' => Yii::t('app', 'Units'),
                'value' => function ($model) {
                    if($model->no_of_units_from !== null && $model->no_of_units_to !== null){
                        $bua = $model->no_of_units_from.' to '.$model->no_of_units_to;
                    }else if($model->no_of_units_from !== null && $model->no_of_units_from > 5000){
                        $bua = '> '.$model->no_of_units_from;
                    }

                    return $bua;
                    // return number_format($bua,2);
                },

            ],

            [
                'attribute' => 'tenure',
                'label' => Yii::t('app', 'Tenure'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->buildingTenureArrSla[$model->tenure];
                },
                'filter' => Yii::$app->appHelperFunctions->buildingTenureArrSla
                ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'status_verified',
                'label' => Yii::t('app', 'Status Verification'),
                'value' => function ($model) {
                        return Yii::$app->appHelperFunctions->statusVerifyLabel[$model->status_verified];
                },
                'filter' => Yii::$app->appHelperFunctions->statusVerifyArr
            ],             
            

            
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                    'get-history' => function ($url, $model) {
                        // Yii::$app->session->set('model_name', Yii::$app->appHelperFunctions->getModelName());
                        return Html::a('<i class="fa fa-history"></i> '.Yii::t('app', 'History'), 'javascript:;', [
                            'title' => Yii::t('app', 'History'),
                            'class'=>'dropdown-item text-1',
                            'target' => '_blank',
                            'onclick'=> "window.open('".$url."', '_blank')",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>


</div>


