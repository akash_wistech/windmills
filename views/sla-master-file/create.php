<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SlaMasterFile */

$this->title = 'Create Sla Master File';
$this->params['breadcrumbs'][] = ['label' => 'Sla Master Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sla-master-file-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
