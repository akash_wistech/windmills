<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientSegmentFile */

$this->title = 'Create Client Segment File';
$this->params['breadcrumbs'][] = ['label' => 'Client Segment Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-segment-file-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
