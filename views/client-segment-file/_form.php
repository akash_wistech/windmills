<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use  app\components\widgets\StatusVerified;

$this->registerJs('
    $("body").on("change", "#clientsegmentfile-client_type", function (e) {
        
    if($(this).val() != ""){
        if($(this).val() == "corporate"){
            $(".hiddenfield").removeClass("d-none");
        }else{
            $(".hiddenfield").addClass("d-none");
        }

        $(".section-two-and-three").removeClass("d-none");
    }else{
        $(".section-two-and-three").addClass("d-none");
    }
        
    });
');
?>


<section class="client-segment-file-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title">Create</h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'client_type')->widget(Select2::classname(), [
                    'data' => yii::$app->quotationHelperFunctions->clienttype,
                    'options' => ['placeholder' => 'Select ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'disabled' => true
                    ],
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'fee_crm')->widget(Select2::classname(), [
                    'data' => array(0=>'Total Fee',1=>'Individual Fee'),
                    'options' => ['placeholder' => 'Select ...'],
                    'pluginOptions' => [
                    ],
                ])->label('Quotation Fee Format'); ?>
            </div>
        </div>

        <!--<section class="card card-outline card-info section-two-and-three  <?/*= ($model->id<>null) ? '' : 'd-none' */?>">
            <header class="card-header">
                <h2 class="card-title">Quotation Follow Up Period</h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <?/*= $form->field($model, 'quotation_followup_email_no')->widget(Select2::classname(), [
                            'data' => yii::$app->appHelperFunctions->getDaysArr(),
                            'options' => ['placeholder' => 'Select ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); */?>
                    </div>
                    <div class="col-6">
                        <?/*= $form->field($model, 'quotation_followup_period__days')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->getCrmOptionZeroToNumber(60),
                            'options' => ['placeholder' => 'Select ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); */?>
                    </div>
                    
                    <div class="col-6 hiddenfield  <?/*= ($model->id<>null && $model->client_type=='corporate') ? '' : 'd-none' */?>">
                        <?/*= $form->field($model, 'developer')->widget(Select2::classname(), [
                            'data' => yii::$app->appHelperFunctions->getDaysArr(),
                            'options' => ['placeholder' => 'Select ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); */?>
                    </div>
                    <div class="col-6 hiddenfield  <?/*= ($model->id<>null && $model->client_type=='corporate') ? '' : 'd-none' */?>">
                        <?/*= $form->field($model, 'family')->widget(Select2::classname(), [
                            'data' => yii::$app->appHelperFunctions->getDaysArr(),
                            'options' => ['placeholder' => 'Select ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); */?>
                    </div>
                    <div class="col-6 hiddenfield  <?/*= ($model->id<>null && $model->client_type=='corporate') ? '' : 'd-none' */?>">
                        <?/*= $form->field($model, 'publicly_listed')->widget(Select2::classname(), [
                            'data' => yii::$app->appHelperFunctions->getDaysArr(),
                            'options' => ['placeholder' => 'Select ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); */?>
                    </div>
                    <div class="col-6 hiddenfield  <?/*= ($model->id<>null && $model->client_type=='corporate') ? '' : 'd-none' */?>">
                        <?/*= $form->field($model, 'private_company')->widget(Select2::classname(), [
                            'data' => yii::$app->appHelperFunctions->getDaysArr(),
                            'options' => ['placeholder' => 'Select ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); */?>
                    </div>
                    <div class="col-6 hiddenfield  <?/*= ($model->id<>null && $model->client_type=='corporate') ? '' : 'd-none' */?>">
                        <?/*= $form->field($model, 'audit_firm')->widget(Select2::classname(), [
                            'data' => yii::$app->appHelperFunctions->getDaysArr(),
                            'options' => ['placeholder' => 'Select ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); */?>
                    </div>
                    <div class="col-6 hiddenfield  <?/*= ($model->id<>null && $model->client_type=='corporate') ? '' : 'd-none' */?>">
                        <?/*= $form->field($model, 'law_firm')->widget(Select2::classname(), [
                            'data' => yii::$app->appHelperFunctions->getDaysArr(),
                            'options' => ['placeholder' => 'Select ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); */?>
                    </div>
                </div>
            </div>
        </section>-->

        <div class="row section-two-and-three <?= ($model->id<>null) ? '' : 'd-none' ?>">
            <div class="col-6">
                <section class="client-segment-file-form card card-outline card-info">
                    <header class="card-header">
                        <h2 class="card-title">Report Follow Update</h2>
                    </header>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <?= $form->field($model, 'report_follow_update_days')->widget(Select2::classname(), [
                                    'data' => yii::$app->appHelperFunctions->getDaysArr(),
                                    'options' => ['placeholder' => 'Select ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-6">
                <section class="client-segment-file-form card card-outline card-info">
                    <header class="card-header">
                        <h2 class="card-title">Client Service Feedback</h2>
                    </header>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <?= $form->field($model, 'client_services_feedback_days')->widget(Select2::classname(), [
                                    'data' => yii::$app->appHelperFunctions->getDaysArr(),
                                    'options' => ['placeholder' => 'Select ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>





        <?php echo StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php 
            if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => get_class($model),
                ]);
            }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>