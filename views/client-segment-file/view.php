<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClientSegmentFile */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Client Segment Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-segment-file-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'client_type',
            'developer',
            'family',
            'publicly_listed',
            'private_company',
            'audit_firm',
            'law_firm',
            'quotation_followup_period__days',
            'report_follow_update_days',
            'client_services_feedback_days',
        ],
    ]) ?>

</div>
