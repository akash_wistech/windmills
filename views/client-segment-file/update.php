<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientSegmentFile */

$this->title = 'Update Client Segment File: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Client Segment Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="client-segment-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
