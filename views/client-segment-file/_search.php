<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientSegmentFileSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-segment-file-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'client_type') ?>

    <?= $form->field($model, 'developer') ?>

    <?= $form->field($model, 'family') ?>

    <?= $form->field($model, 'publicly_listed') ?>

    <?php // echo $form->field($model, 'private_company') ?>

    <?php // echo $form->field($model, 'audit_firm') ?>

    <?php // echo $form->field($model, 'law_firm') ?>

    <?php // echo $form->field($model, 'quotation_followup_period__days') ?>

    <?php // echo $form->field($model, 'report_follow_update_days') ?>

    <?php // echo $form->field($model, 'client_services_feedback_days') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
