<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Developers */
/* @var $form yii\widgets\ActiveForm */
?>


<section class="developers-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'segment_type')->dropDownList(Yii::$app->appHelperFunctions->ClientSegments)->label('Type')?>
            </div>
        </div>




        <?php

        foreach ($client_array as $key => $client_data) {
                    if($key != 0) {
                        $group_data = \app\models\ClientGroups::find()->where(['id' => $key])->one();
                        ?>
                        <div class="row">
                        <div class="col-sm-12">
                        <section class="valuation-form card card-outline card-primary">

                        <header class="card-header">
                            <h2 class="card-title"><?= $group_data->title ?></h2>
                        </header>
                        <div class="card-body">
                        <?php foreach ($client_data as $ky => $client_datum) {
                            $department_data = \app\models\ClientDepartments::find()->where(['id' => $ky])->one();

                            ?>
                            <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title"><?= $department_data->title ?></h2>
                                </header>
                                <div class="card-body">
                                    <table id="attachment-auto"
                                           class="table table-striped table-bordered table-hover images-table">
                                        <thead>
                                        <tr>
                                            <td style="width:45%" class="text-left">Name</td>
                                            <td style="width:45%" class="text-left">Email</td>
                                            <td style="width:45%" class="text-left">Status</td>
                                            <td style="width:20%" class="text-left">Detail</td>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php


                                        foreach ($client_datum as $kye => $client_edu) {



                                            $user_prof = \app\models\UserProfileInfo::findOne(['user_id' => $client_edu->id]);
                                            ?>
                                            <tr>
                                                <td><?= $client_edu->title ?></td>
                                                <td><?= $client_edu->primaryContact->email ?></td>

                                                <td>
                                                    <span class="badge badge-primary">Primary</span>
                                                </td>

                                                <td><a href="client/update/<?= $client_edu->id ?>">View</a></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        <?php }
                    }else{
                        ?>

                        <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title">Department (Unknown)</h2>
                                </header>
                                <div class="card-body">
                                    <table id="attachment-auto"
                                           class="table table-striped table-bordered table-hover images-table">
                                        <thead>
                                        <tr>
                                            <td style="width:45%" class="text-left">Name</td>
                                            <td style="width:45%" class="text-left">Email</td>
                                            <td style="width:45%" class="text-left">Status</td>
                                            <td style="width:20%" class="text-left">Detail</td>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php


                                        foreach ($client_data as $kye => $client_edu) {


                                            $user_prof = \app\models\UserProfileInfo::findOne(['user_id' => $client_edu->id]);
                                            ?>
                                            <tr>
                                                <td><?= $client_edu->title ?></td>
                                                <td><?= $client_edu->primaryContact->email ?></td>

                                                <td>
                                                    <span class="badge badge-primary">Primary</span>
                                                </td>

                                                <td><a href="client/update/<?= $client_edu->id ?>">View</a></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </section>

                 <?php   }
                                ?>


                            </div>
                        </section>
                    </div>
                </div>
                <?php
            }
        ?>





        <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php 
            if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                ]);
            }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
