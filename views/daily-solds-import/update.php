<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DailySoldsImport */

$this->title = 'Update Daily Solds Import: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Daily Solds Imports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daily-solds-import-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
