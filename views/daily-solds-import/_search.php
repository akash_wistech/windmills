<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DailySoldsImportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daily-solds-import-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'transaction_type') ?>

    <?= $form->field($model, 'subtype') ?>

    <?= $form->field($model, 'sales_sequence') ?>

    <?= $form->field($model, 'reidin_ref') ?>

    <?php // echo $form->field($model, 'transaction_date') ?>

    <?php // echo $form->field($model, 'community') ?>

    <?php // echo $form->field($model, 'property') ?>

    <?php // echo $form->field($model, 'property_type') ?>

    <?php // echo $form->field($model, 'unit') ?>

    <?php // echo $form->field($model, 'bedrooms') ?>

    <?php // echo $form->field($model, 'floor') ?>

    <?php // echo $form->field($model, 'parking') ?>

    <?php // echo $form->field($model, 'balcony_area') ?>

    <?php // echo $form->field($model, 'size_sqf') ?>

    <?php // echo $form->field($model, 'land_size') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'sqf') ?>

    <?php // echo $form->field($model, 'developer') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'new_building_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
