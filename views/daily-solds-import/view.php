<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DailySoldsImport */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Daily Solds Imports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="daily-solds-import-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'transaction_type',
            'subtype',
            'sales_sequence',
            'reidin_ref',
            'transaction_date',
            'community',
            'property',
            'property_type',
            'unit',
            'bedrooms',
            'floor',
            'parking',
            'balcony_area',
            'size_sqf',
            'land_size',
            'amount',
            'sqf',
            'developer',
            'status',
            'new_building_id',
        ],
    ]) ?>

</div>
