<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BranchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Branches');
$this->params['breadcrumbs'][] = $this->title;

$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
    $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
    $actionBtns.='{view}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
    $actionBtns.='{delete}';
}
?>

<div class="general-asumption-index">
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:25px;']],
            'company',
            'email:email',
            'mobile',
            // 'office_phone',
            'address:ntext',
            [
                'format'=>'raw','attribute'=>'zone_list','label'=>Yii::t('app','City'),'value'=>function($model){
                    $zoneName  = app\models\Zone::findOne($model['zone_list']);
                    return $zoneName->title;
                },
            ],
            [
                'format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
                    return Yii::$app->helperFunctions->arrStatusIcon[$model['status']];
                },
            ],
            // 'bank_name',
            // 'account_holder_name',
            // 'account_number',
            // 'branch_code',
            // 'swift_code',
            // 'iban_number',
            
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
                <div class="btn-group flex-wrap">
                <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                </button>
                <div class="dropdown-menu" role="menu">
                '.$actionBtns.'
                </div>
                </div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class'=>'dropdown-item text-1',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                            'data-method'=>"post",
                            'data-pjax'=>"0",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>
</div>
