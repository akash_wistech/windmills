<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Branch */
/* @var $form yii\widgets\ActiveForm */

$zonesList=Yii::$app->appHelperFunctions->getCountryZoneListArr(221);
?>


<section class="contact-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-4 d-none">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'class' => 'form-control ']) ?>
            </div>
            <div class="col-4">
                <?= $form->field($model, 'company')->textInput(['maxlength' => true, 'class' => 'form-control ']) ?>
            </div>
            <div class="col-4 d-none">
                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class' => 'form-control ']) ?>
            </div>
            <div class="col-4 d-none">
                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'class' => 'form-control ']) ?>
            </div>
            <div class="col-4">        
                <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'class' => 'form-control ']) ?>
            </div>
            <div class="col-4">        
                <?= $form->field($model, 'mobile')->textInput(['class' => 'form-control ']) ?>
            </div>  
            <div class="col-4">        
                <?= $form->field($model, 'office_phone')->textInput(['class' => 'form-control ']) ?>
            </div> 
            <div class="col-4">
                <?= $form->field($model, 'address')->textinput(['rows' => 6, 'class' => 'form-control ']) ?>
            </div> 
            <div class="col-4">
                <?= $form->field($model, 'website')->textinput(['rows' => 6, 'class' => 'form-control ']) ?>
            </div>            
            <div class="col-4">        
                <?= $form->field($model, 'zone_list')->dropDownList($zonesList ,['class' => 'form-control '])?>
            </div>           
            <div class="col-4">        
                <?= $form->field($model, 'status')->dropDownList([1=>'Active',2=>'In-Active'],['class' => 'form-control '])?>
            </div>
        </div>


        <section class="contact-form card card-outline card-primary">
            <header class="card-header">
                <h2 class="card-title">Bank details</h2>
            </header>
            <div class="card-body">

                <div class="row">          
                    <div class="col-4">        
                        <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true, 'class' => 'form-control ']) ?>
                    </div>            
                    <div class="col-4">        
                        <?= $form->field($model, 'account_holder_name')->textInput(['maxlength' => true, 'class' => 'form-control ']) ?>
                    </div>            
                    <div class="col-4">        
                        <?= $form->field($model, 'account_number')->textInput(['class' => 'form-control ']) ?>
                    </div>            
                    <div class="col-4">        
                        <?= $form->field($model, 'branch_code')->textInput(['class' => 'form-control ']) ?>
                    </div>            
                    <div class="col-4">        
                        <?= $form->field($model, 'swift_code')->textInput(['class' => 'form-control ']) ?>
                    </div>            
                    <div class="col-4">        
                        <?= $form->field($model, 'iban_number')->textInput(['class' => 'form-control ']) ?>
                    </div>
                    <div class="col-4">        
                        <?= $form->field($model, 'trn_number')->textInput(['class' => 'form-control ']) ?>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
