<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ValuationPurposes */

$this->title = 'Update Valuation Purposes: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Valuation Purposes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="valuation-purposes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
