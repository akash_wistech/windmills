<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubCommunities */


$this->title = Yii::t('app', 'Sub Communities');
$cardTitle = Yii::t('app','New Sub Community');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="sub-communities-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
