<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\CommunitiesAssets;
CommunitiesAssets::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\SubCommunities */
/* @var $form yii\widgets\ActiveForm */
?>



<section class="sub-communities-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'community')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Communities::find()->where(['status'=>1])->andWhere(['trashed'=>0])->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select a Community ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>

           <!-- <div class="col-sm-4">
                <?php
/*                echo $form->field($model, 'city')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->emiratedListArr,
                    'options' => ['placeholder' => 'Select an City ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                */?>
            </div>-->
        </div>
        <?php   if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){ ?>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Verification') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'status')->widget(Select2::classname(), [
                                'data' => array( '2' => 'Unverified','1' => 'Verified'),
                            ]);
                            ?>
                        </div>


                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>

