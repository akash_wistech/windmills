<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubCommunities */

$this->title = Yii::t('app', 'Sub Communities');
$cardTitle = Yii::t('app','Update Sub Community:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sub-communities-update">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
