<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Buaweightage */

$this->title = Yii::t('app', 'GFA weightage');
$cardTitle = Yii::t('app','New GFA weightage');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="buaweightage-create">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
