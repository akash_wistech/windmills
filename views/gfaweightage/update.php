<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Buaweightage */


$this->title = Yii::t('app', 'GFA weightage');
$cardTitle = Yii::t('app','Update GFA weightage:  {nameAttribute}', [
    'nameAttribute' => 'Buaweightage',
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="buaweightage-update">

    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
