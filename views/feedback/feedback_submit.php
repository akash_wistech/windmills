
<?php

    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use kartik\rating\StarRating;
    use yii\web\JsExpression;


    $this->title = Yii::t('app', 'Feedback');

$this->registerJs('

    $(document).ready(function() {

         $("#feedback-form").on("submit", function(e) {
                
                var isValid = true;

                if (!checkMandatory("#fee_rating", "#fee_rating_error")) {
                    isValid = false;
                }
                if (!checkMandatory("#tat_rating", "#tat_rating_error")) {
                    isValid = false;
                }
                if (!checkMandatory("#technical_capability_rating", "#technical_capability_rating_error")) {
                    isValid = false;
                }
                if (!checkMandatory("#quality_of_customer_service", "#quality_of_customer_service_error")) {
                    isValid = false;
                }
                if (!checkMandatory("#quality_of_proposal_service", "#quality_of_proposal_service_error")) {
                    isValid = false;
                }
                if (!checkMandatory("#quality_of_inspection", "#quality_of_inspection_error")) {
                    isValid = false;
                }
                if (!checkMandatory("#quality_of_valuation_service", "#quality_of_valuation_service_error")) {
                    isValid = false;
                }
                    
                if (!isValid) {
                    e.preventDefault(); 
                }
        });


        
    });

    function checkMandatory(field_id, error_id) {
        var rating = $(field_id).val();
        if (rating === "") {
            $(error_id).show(); 
            $(field_id).focus(); 
            return false; 
        } else {
            $(error_id).hide(); 
            return true; 
        }
    }




');
?>
<style>
    .feedback-form { 
        width: 1024px; 
        margin: 30px auto; 
        padding: 10px; 
        border: 1px solid #ccc;
        border-radius: 8px;
        font-size: 16px;
    }
    .feedback-form h1 { text-align: center; margin-bottom: 20px;}
    .feedback-form h2 { text-align: center; margin: 20px 0;}

   .feedback-message { width: 580px; margin: 30px auto;  }

   .clear-rating { display: none !important;} 
   .rating-container .caption-secondary { display: none !important;}

   .error-message {
        color: red; display: none; font-size: 11px;
   }

    @media only screen and (max-width: 770px) {
        .feedback-form{
            width: 100%;
        }
        .feedback-message { width: 100%; }

    }


</style>



<?php if(Yii::$app->session->hasFlash('success')) { ?>
<div  class="feedback-message">
    <div class="alert alert-success" role="alert">
        <?= Yii::$app->session->getFlash('success'); ?>
    </div>
</div>

<?php } ?>
<section class="feedback-form ">
    <div class="container">
        <div><h2>Tell us how happy you are.</h2></div>
        

        <?php
        $form = ActiveForm::begin(['id' => 'feedback-form']);

        // dd($model->type);

        // $randomKey = Yii::$app->security->generateRandomString(20, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        $options = 
                [
                    'min' => 0,
                    'max' => 100,
                    'step' => 20,
                    'theme' => 'krajee-uni',
                    'filledStar' => '★',
                    'emptyStar' => '☆',
                    'starCaptions' => [
                        20 => '20%',
                        40 => '40%',
                        60 => '60%',
                        80 => '80%',
                        100 => '100%',
                    ],
                    'starCaptionClasses' => [
                        20 => 'text-danger',
                        40 => 'text-warning',
                        60 => 'text-info',
                        80 => 'text-primary',
                        100 => 'text-success',
                    ],
                    'displayOnly' => ($model->status == 1) ? true : false
                ];

        ?>        

        <?php if($model->type == 0){ ?>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label">1. Inspection Quality Rating</label>
                    <div id="quality_of_inspection_error" class="error-message">Please provide a rating.</div>
                    <?php echo StarRating::widget(['model' => $model, 'attribute' => 'quality_of_inspection', 
                    'pluginOptions' => $options,
                    'options' => ['id' => 'quality_of_inspection']
                    ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <label class="control-label">2. Turn Around Time Rating</label>
                    <div id="tat_rating_error" class="error-message">Please provide a rating.</div>
                    <?php echo StarRating::widget(['model' => $model, 'attribute' => 'tat_rating', 
                        'pluginOptions' => $options,
                        'options' => ['id' => 'tat_rating']
                    ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <label class="control-label">3. Technical Capability Rating</label>
                    <div id="technical_capability_rating_error" class="error-message">Please provide a rating.</div>
                    <?php echo StarRating::widget(['model' => $model, 'attribute' => 'technical_capability_rating', 
                        'pluginOptions' => $options,
                        'options' => ['id' => 'technical_capability_rating']
                    ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <label class="control-label">4. Customer Service Rating</label>
                    <div id="quality_of_customer_service_error" class="error-message">Please provide a rating.</div>
                    <?php echo StarRating::widget(['model' => $model, 'attribute' => 'quality_of_customer_service', 
                        'pluginOptions' => $options,
                        'options' => ['id' => 'quality_of_customer_service']
                    ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <label class="control-label">5. Overall Valuation Quality Rating</label>
                    <div id="quality_of_valuation_service_error" class="error-message">Please provide a rating.</div>
                    <?php echo StarRating::widget(['model' => $model, 'attribute' => 'quality_of_valuation_service', 
                        'pluginOptions' => $options,
                        'options' => ['id' => 'quality_of_valuation_service']
                    ]);
                    ?>
                </div>
                
            </div>
        
           
           
            
            

        <?php }
        elseif($model->type == 1){ ?>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label">1. Fee Rating</label>
                    <div id="fee_rating_error" class="error-message">Please provide a rating.</div>
                    <?php echo StarRating::widget(['model' => $model, 'attribute' => 'fee_rating', 
                    'pluginOptions' => $options,
                    'options' => ['id' => 'fee_rating']
                    ]);
                    ?>
                    
                </div>
                <div class="col-sm-4">
                    <label class="control-label">2. Turn Around Time Rating</label>
                    <div id="tat_rating_error" class="error-message">Please provide a rating.</div>
                    <?php echo StarRating::widget(['model' => $model, 'attribute' => 'tat_rating', 
                        'pluginOptions' => $options,
                        'options' => ['id' => 'tat_rating']
                    ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <label class="control-label">3. Technical Capability Rating</label>
                    <div id="technical_capability_rating_error" class="error-message">Please provide a rating.</div>
                    <?php echo StarRating::widget(['model' => $model, 'attribute' => 'technical_capability_rating', 
                        'pluginOptions' => $options,
                        'options' => ['id' => 'technical_capability_rating']
                    ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <label class="control-label">4. Customer Service Rating</label>
                    <div id="quality_of_customer_service_error" class="error-message">Please provide a rating.</div>
                    <?php echo StarRating::widget(['model' => $model, 'attribute' => 'quality_of_customer_service', 
                        'pluginOptions' => $options,
                        'options' => ['id' => 'quality_of_customer_service']
                    ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <label class="control-label">5. Overall Proposal Quality Rating</label>
                    <div id="quality_of_proposal_service_error" class="error-message">Please provide a rating.</div>
                    <?php echo StarRating::widget(['model' => $model, 'attribute' => 'quality_of_proposal_service', 
                        'pluginOptions' => $options,
                        'options' => ['id' => 'quality_of_proposal_service']
                    ]);
                    ?>
                </div>
            </div>
            
            
            

        <?php } ?>

    
        <?= $form->field($model, 'appreciation_suggestion')->textarea(['rows' => 5, 'placeholder' => 'Appreciation And/Or Suggestion', 'readonly' => ($model->status == 1) ? true : false])->label(false) ?>


        
        <?php
        if($model->status <> 1) {
            echo Html::submitButton('Submit', ['class' => 'btn btn-primary mb-3']);
        }

        
        ActiveForm::end();
        ?>
        <div><p>Call our office on <a href="tel:+971524729083">+971 52 472 9083</a> if need be.</p></div>
       
    </div>
</section>


