<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use kartik\select2\Select2;
use kartik\rating\StarRating;
use yii\web\JsExpression;

use app\models\User;

use app\assets\DateRangePickerAsset2;

DateRangePickerAsset2::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BranchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Valuations Feedback');
$this->params['breadcrumbs'][] = $this->title;

$citiesArr = Yii::$app->appHelperFunctions->emiratedList;

$staffData = Yii::$app->appHelperFunctions->staffMemberListArrLastNameservice;

$total_quality_of_inspection = 0;
$total_tat_rating = 0;
$total_technical_capability_rating = 0;
$total_quality_of_customer_service = 0;
$total_quality_of_valuation_service = 0;

foreach ($dataProvider->getModels() as $model) {
    $total_quality_of_inspection += $model->quality_of_inspection;
    $total_tat_rating += $model->tat_rating;
    $total_technical_capability_rating += $model->technical_capability_rating;
    $total_quality_of_customer_service += $model->quality_of_customer_service;
    $total_quality_of_valuation_service += $model->quality_of_valuation_service;
}

$count = count($dataProvider->getModels());
$avg_quality_of_inspection = number_format($total_quality_of_inspection / $count, 0);
$avg_tat_rating = number_format($total_tat_rating / $count, 0);
$avg_technical_capability_rating = number_format($total_technical_capability_rating/$count, 0);
$avg_quality_of_customer_service = number_format($total_quality_of_customer_service/$count, 0);
$avg_quality_of_valuation_service = number_format($total_quality_of_valuation_service / $count, 0);

// dd($count,$avg_quality_of_inspection, $avg_quality_of_valuation, $avg_tat_rating, $avg_quality_of_valuation_service);

?>
<style>
    .yfstar {
        color: #f39c12;
    }

    .gnfstar {
        color: #95a5a6;
    }
</style>

<div class="feedback-index">

    <div class="valuation-search card-body mb-0 pb-0">

        <?php $form = ActiveForm::begin([
            'action' => ['valuations'],
            'method' => 'get',
        ]); ?>

        <div class="row">


            <div class="col-sm-4 text-left">
                <?php
                echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->reportPeriod,
                    'options' => ['placeholder' => 'Time Frame ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false);
                ?>
            </div>
            <div class="col-sm-4" id="date_range_array" <?php if ($custom == 0) {
                                                            echo 'style="display:none;"';
                                                        } ?>>
                <div class="" id="end_date_id">

                    <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); 
                    ?>
                    <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class' => 'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>

                </div>
            </div>
            <div class="col-sm-4">
                <div class="text-left">
                    <div class="form-group">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>



        <?php ActiveForm::end(); ?>
    </div>

    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'showFooter' => true,
        'options' => [
            'id' => 'sortable-table',
        ],
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:25px;']],

            [
                'attribute' => 'client_id',
                // 'label' => 'Client Name', 
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('client_id') . '">Client<br/>Name</a></div>',
                'value' => function ($model) {
                    return $model->client->title;
                    // return Yii::$app->crmQuotationHelperFunctions->getSubWords($model->client->title, 4);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'client_id',
                    'data' => ArrayHelper::map(\app\models\Company::find()
                        ->where(['status' => 1])
                        ->andWhere(['allow_for_valuation' => 1])
                        ->orderBy(['title' => SORT_ASC,])
                        ->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select Clent...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => 'width: 250px; vertical-align:middle'],

            ],
            [
                'attribute' => 'valuation_id',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('valuation_id') . '">Valuation<br/>Ref</a></div>',
                'value' => function ($model) {
                    return $model->valuation->reference_number;
                },
                'contentOptions' => ['style' => 'width: 200px; vertical-align:middle'],
                'footer' => ($count > 1) ? 'Average' : '',
            ],
            [
                'attribute' => 'quality_of_inspection',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('quality_of_inspection') . '">Inspection<br/>Quality</a></div>',
                'format' => 'html',
                'value' => function ($model) {
                    switch ($model->quality_of_inspection) {
                        case 20:
                            $stars = '<span class="yfstar">★</span><span class="gnfstar">☆☆☆☆</span> (' . $model->quality_of_inspection . '%)';
                            break;
                        case 40:
                            $stars = '<span class="yfstar">★★</span><span class="gnfstar">☆☆☆</span> (' . $model->quality_of_inspection . '%)';
                            break;
                        case 60:
                            $stars = '<span class="yfstar">★★★</span><span class="gnfstar">☆☆</span> (' . $model->quality_of_inspection . '%)';
                            break;
                        case 80:
                            $stars = '<span class="yfstar">★★★★</span><span class="gnfstar">☆</span> (' . $model->quality_of_inspection . '%)';
                            break;
                        case 100:
                            $stars = '<span class="yfstar">★★★★★</span> (' . $model->quality_of_inspection . '%)';
                            break;
                        default:
                            $stars = '<span class="gnfstar">☆☆☆☆☆</span>';
                            break;
                    }
                    return $stars;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'quality_of_inspection',
                    'data' => [
                        '20' => '★', '40' => '★★', '60' => '★★★', '80' => '★★★★', '100' => '★★★★★',
                    ],
                    'options' => ['placeholder' => 'Select ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => 'width: 100px; vertical-align:middle'],
                'footer' => ($count > 1) ? Yii::$app->appHelperFunctions->getAverageStarRating($avg_quality_of_inspection) : '',
            ],
            [
                'attribute' => 'tat_rating',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('tat_rating') . '">Turn Around<br/>Time Rating</a></div>',
                'format' => 'html',
                'value' => function ($model) {
                    switch ($model->tat_rating) {
                        case 20:
                            $stars = '<span class="yfstar">★</span><span class="gnfstar">☆☆☆☆</span> ('.$model->tat_rating.'%)';
                            break;
                        case 40:
                            $stars = '<span class="yfstar">★★</span><span class="gnfstar">☆☆☆</span> ('.$model->tat_rating.'%)';
                            break;
                        case 60:
                            $stars = '<span class="yfstar">★★★</span><span class="gnfstar">☆☆</span> ('.$model->tat_rating.'%)';
                            break;
                        case 80:
                            $stars = '<span class="yfstar">★★★★</span><span class="gnfstar">☆</span> ('.$model->tat_rating.'%)';
                            break;
                        case 100:
                            $stars = '<span class="yfstar">★★★★★</span> ('.$model->tat_rating.'%)';
                            break;
                        default:
                            $stars = '<span class="gnfstar">☆☆☆☆☆</span>';
                            break;
                    }
                    return $stars;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'tat_rating',
                    'data' => [
                        '20' => '★','40' => '★★','60' => '★★★','80' => '★★★★','100' => '★★★★★',
                    ],
                    'options' => ['placeholder' => 'Select ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => ' vertical-align:middle; width:50px'],
                'footer' => ($count > 1) ? Yii::$app->appHelperFunctions->getAverageStarRating($avg_tat_rating) : '',
            ],
            [
                'attribute' => 'technical_capability_rating',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('technical_capability_rating') . '">Technical<br/>Capability</a></div>',
                'format' => 'html',
                'value' => function ($model) {
                    switch ($model->technical_capability_rating) {
                        case 20:
                            $stars = '<span class="yfstar">★</span><span class="gnfstar">☆☆☆☆</span> ('.$model->technical_capability_rating.'%)';
                            break;
                        case 40:
                            $stars = '<span class="yfstar">★★</span><span class="gnfstar">☆☆☆</span> ('.$model->technical_capability_rating.'%)';
                            break;
                        case 60:
                            $stars = '<span class="yfstar">★★★</span><span class="gnfstar">☆☆</span> ('.$model->technical_capability_rating.'%)';
                            break;
                        case 80:
                            $stars = '<span class="yfstar">★★★★</span><span class="gnfstar">☆</span> ('.$model->technical_capability_rating.'%)';
                            break;
                        case 100:
                            $stars = '<span class="yfstar">★★★★★</span> ('.$model->technical_capability_rating.'%)';
                            break;
                        default:
                            $stars = '<span class="gnfstar">☆☆☆☆☆</span>';
                            break;
                    }
                    return $stars;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'technical_capability_rating',
                    'data' => [
                        '20' => '★','40' => '★★','60' => '★★★','80' => '★★★★','100' => '★★★★★',
                    ],
                    'options' => ['placeholder' => 'Select ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => ' vertical-align:middle; width:50px'],
                'footer' => ($count > 1) ? Yii::$app->appHelperFunctions->getAverageStarRating($avg_technical_capability_rating) : '',
            ],
            [
                'attribute' => 'quality_of_customer_service',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('quality_of_customer_service') . '">Customer<br/>Service</a></div>',
                'format' => 'html',
                'value' => function ($model) {
                    switch ($model->quality_of_customer_service) {
                        case 20:
                            $stars = '<span class="yfstar">★</span><span class="gnfstar">☆☆☆☆</span> ('.$model->quality_of_customer_service.'%)';
                            break;
                        case 40:
                            $stars = '<span class="yfstar">★★</span><span class="gnfstar">☆☆☆</span> ('.$model->quality_of_customer_service.'%)';
                            break;
                        case 60:
                            $stars = '<span class="yfstar">★★★</span><span class="gnfstar">☆☆</span> ('.$model->quality_of_customer_service.'%)';
                            break;
                        case 80:
                            $stars = '<span class="yfstar">★★★★</span><span class="gnfstar">☆</span> ('.$model->quality_of_customer_service.'%)';
                            break;
                        case 100:
                            $stars = '<span class="yfstar">★★★★★</span> ('.$model->quality_of_customer_service.'%)';
                            break;
                        default:
                            $stars = '<span class="gnfstar">☆☆☆☆☆</span>';
                            break;
                    }
                    return $stars;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'quality_of_customer_service',
                    'data' => [
                        '20' => '★','40' => '★★','60' => '★★★','80' => '★★★★','100' => '★★★★★',
                    ],
                    'options' => ['placeholder' => 'Select ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => ' vertical-align:middle; width:50px'],
                'footer' => ($count > 1) ? Yii::$app->appHelperFunctions->getAverageStarRating($avg_quality_of_customer_service) : '',
            ],
            [
                'attribute' => 'quality_of_valuation_service',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('quality_of_valuation_service') . '">Overall<br/>Valuation</a></div>',
                'format' => 'html',
                'value' => function ($model) {
                    switch ($model->quality_of_valuation_service) {
                        case 20:
                            $stars = '<span class="yfstar">★</span><span class="gnfstar">☆☆☆☆</span> (' . $model->quality_of_valuation_service . '%)';
                            break;
                        case 40:
                            $stars = '<span class="yfstar">★★</span><span class="gnfstar">☆☆☆</span> (' . $model->quality_of_valuation_service . '%)';
                            break;
                        case 60:
                            $stars = '<span class="yfstar">★★★</span><span class="gnfstar">☆☆</span> (' . $model->quality_of_valuation_service . '%)';
                            break;
                        case 80:
                            $stars = '<span class="yfstar">★★★★</span><span class="gnfstar">☆</span> (' . $model->quality_of_valuation_service . '%)';
                            break;
                        case 100:
                            $stars = '<span class="yfstar">★★★★★</span> (' . $model->quality_of_valuation_service . '%)';
                            break;
                        default:
                            $stars = '<span class="gnfstar">☆☆☆☆☆</span>';
                            break;
                    }
                    return $stars;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'quality_of_valuation_service',
                    'data' => [
                        '20' => '★', '40' => '★★', '60' => '★★★', '80' => '★★★★', '100' => '★★★★★',
                    ],
                    'options' => ['placeholder' => 'Select ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => 'width: 100px; vertical-align:middle'],
                'footer' => ($count > 1) ? Yii::$app->appHelperFunctions->getAverageStarRating($avg_quality_of_valuation_service) : '',

            ],
            [
                'attribute' => 'quality_average',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('quality_average') . '">Quality of <br>Average</a></div>',
                'format' => 'html',
                'value' => function ($model) {
                    $quality_avg = $model->qualityAverage;
                    return Yii::$app->appHelperFunctions->getAverageStarRating($quality_avg);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'quality_average',
                    'data' => [
                        '20' => '★', '40' => '★★', '60' => '★★★', '80' => '★★★★', '100' => '★★★★★',
                    ],
                    'options' => ['placeholder' => 'Select ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                // 'filter' => false,
                'contentOptions' => ['style' => 'width: 100px; vertical-align:middle'],
            ],
            [
                'attribute' => 'city',
                'label' => 'City',
                'value' => function ($model) {
                    return $model->cityName->title;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'city',
                    'data' => ArrayHelper::map($citiesArr, 'id', 'title'),

                    'options' => ['placeholder' => 'Select...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => 'width: 120px; vertical-align:middle'],
            ],
            [
                'attribute' => 'property_category',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('property_category') . '">Property <br>Segment</a></div>',
                'value' => function ($model) {
                    return $model->propertyCategory->title;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'property_category',
                    'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,

                    'options' => ['placeholder' => 'Select...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => 'width: 110px; vertical-align:middle'],
            ],
            [
                'attribute' => 'valuer',
                'label' => 'Valuer',
                'value' => function ($model) {
                    $get_name = User::find()->where(['id' => $model->valuer])->one();
                    return $get_name->lastname;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'valuer',
                    'data' => Yii::$app->appHelperFunctions->staffMemberListArr,

                    'options' => ['placeholder' => 'Select...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => 'width: 100px; vertical-align:middle; width:30px'],
            ],
            [
                'attribute' => 'inspector',
                'label' => 'Inspector',
                'value' => function ($model) {
                    // return $model->inspector;
                    $get_name = User::find()->where(['id' => $model->inspector])->one();
                    return $get_name->lastname;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'inspector',
                    'data' => Yii::$app->appHelperFunctions->staffMemberListArr,

                    'options' => ['placeholder' => 'Select...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => 'width: 100px; vertical-align:middle; width:30px'],
            ],
            [
                'attribute' => 'approver',
                'label' => 'Approver',
                'value' => function ($model) {
                    // return $model->approver;
                    $get_name = User::find()->where(['id' => $model->approver])->one();
                    return $get_name->lastname;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'approver',
                    'data' => Yii::$app->appHelperFunctions->staffMemberListArr,

                    'options' => ['placeholder' => 'Select...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => 'width: 100px; vertical-align:middle; width:30px'],
            ],
            [
                'attribute' => 'updated_at',
                'label' => 'Submitted',
                'value' => function ($model) {
                    $updated_date = explode(' ', $model->updated_at);
                    $updated_date = date('d-M-Y', strtotime($updated_date[0])) . '<br>' . date('h:i A', strtotime($updated_date[1]));
                    return $updated_date;
                },
                'format' => 'raw',
                // 'format' => ['date', 'php:d-M-Y H:i a'], 
                'contentOptions' => ['style' => 'width: 110px; vertical-align:middle;  width:100px'],
            ],
        ],
        'footerRowOptions' => ['style' => 'font-weight:bold'],
        'pager' => [
            'class' => \yii\widgets\LinkPager::class,
            'options' => ['class' => 'pagination justify-content-center'],
            'maxButtonCount' => 5, // Adjust as needed
            'prevPageLabel' => 'Previous',
            'nextPageLabel' => 'Next',
            'hideOnSinglePage' => true,
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>
</div>


<?php
$this->registerJs('
  




    // $(".sortable").click(function () {
    //     var index = $(this).index();
    //     var rows = $("#sortable-table tbody tr").get();
    //     rows.sort(function (a, b) {
    //         var aValue = $(a).children("td").eq(index).text();
    //         var bValue = $(b).children("td").eq(index).text();

    //     //   return $.isNumeric(aValue) && $.isNumeric(bValue) ?
    //     //   aValue - bValue :
    //     //   aValue.localeCompare(bValue);

    //         return $.isNumeric(bValue) && $.isNumeric(aValue) ?
    //         bValue - aValue :
    //         bValue.localeCompare(aValue);
    //     });

    //     $.each(rows, function (index, row) {
    //         $("#sortable-table tbody").append(row);
    //     });
    // });

    // $( document ).ready(function() {
    //     var index = $(this).index();
    //     var rows = $("#sortable-table tbody tr").get();
    //     rows.sort(function (a, b) {
    //         var aValue = $(a).children("td").eq(index).text();
    //         var bValue = $(b).children("td").eq(index).text();

    //     //   return $.isNumeric(aValue) && $.isNumeric(bValue) ?
    //     //   aValue - bValue :
    //     //   aValue.localeCompare(bValue);

    //         return $.isNumeric(bValue) && $.isNumeric(aValue) ?
    //         bValue - aValue :
    //         bValue.localeCompare(aValue);
    //     });

    //     $.each(rows, function (index, row) {
    //         $("#sortable-table tbody").append(row);
    //     });
    // });


      $(".div1").daterangepicker({
        autoUpdateInput: false,
          locale: {
            format: "YYYY-MM-DD"
          }
      });
     
      $(".div1").on("apply.daterangepicker", function(ev, picker) {
         $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
      });
      $(document).ready(function() {
          // Code to execute when the document is ready
          var period = $("#feedbacksearch-time_period").val();
          
          if (period == 9) {
              $(".div1").prop("required", true);
              $("#date_range_array").show();
          } else {
              $(".div1").prop("required", false);
              $("#date_range_array").hide();
          }
          
          // Change event handler for select input
          $("#feedbacksearch-time_period").on("change", function() {
              var period = $(this).val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $(".div1").val("");
                  $("#date_range_array").hide();
              }
          });
      });




');
?>