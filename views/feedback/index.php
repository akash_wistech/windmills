<?php

use app\models\Feedback;

use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BranchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Feedback');
$this->params['breadcrumbs'][] = $this->title;



//number count for client, buildings, valuation steps.
$totalValuationFeedback =(int)Feedback::find()->where(['type'=>0,'status'=>1])->count('id');
$totalProposalFeedback =(int)Feedback::find()->where(['type'=>1,'status'=>1])->count('id');


?>
<style>
    .yfstar { color: #f39c12;}
    .gnfstar { color: #95a5a6;}
    h4 {
        font-size: 16px !important;
        font-weight: bold;
        font-family: sans-serif !important;
        width: fit-content;
    }

    .card-header {
        padding: 10px !important;
        padding-bottom: 20px !important;
    }

    .card-footer {
        padding: 0px !important;
    }

    .card-footer span {
        font-size: 13.5px !important;
    }

    .col-7 {
        max-width: fit-content !important;
    }

    .pending-text-info {
        color: #08A86D !important;
    }
</style>

<div class="feedback-index">

<section class="valuation-form card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title">Feedback</h2>
        </header>
        <div class="card-body" style="background: #E5E4E2">
            <div class="row px-4">

                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-comments pt-1 pl-2" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Valuations Feedback</span><br>
                                        <?= number_format($totalValuationFeedback) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px; color:#08A86D;"></i>
                                        <a class="position-absolute footer-increas" style="left:53px;top:5px; color:#08A86D; font-weight:bold" href="<?= yii\helpers\Url::toRoute(['feedback/valuations']); ?>"> <span style="font-size:18px;">Go to Valuations Feedback</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-comments pt-1 pl-2" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Propposals Feedback</span><br>
                                        <?= number_format($totalProposalFeedback) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px; color:#08A86D;"></i>
                                        <a class="position-absolute" style="left:53px;top:5px; color:#08A86D; font-weight:bold" href="<?= yii\helpers\Url::toRoute(['feedback/proposals']); ?>"> <span style="font-size:18px;">Go to Proposals Feedback</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                
                
                    
                
            </div>
        </div>

    </section>


    
</div>



