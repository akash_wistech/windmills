<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$clientsArr = ArrayHelper::map(\app\models\User::find()
    ->select([
        'id', 'CONCAT(firstname, " ", lastname) AS lastname',
    ])
    ->where(['status' => 1])
    ->andWhere(['user_type' => [10,20]])
    ->orderBy(['lastname' => SORT_ASC,])
    ->all(), 'id', 'lastname');

$clientsArr = ['' => 'select'] + $clientsArr;

$user_id = Yii::$app->user->identity->id;
$percentages = [
'10%',
'20%',
'30%',
'40%',
'50%',
'60%',
'70%',
'80%',
'90%',
'100%',
];

$edit = isset($page_title) ? true : false; 


?>

    <section class="contact-form card card-outline card-primary">
        <?php $form = ActiveForm::begin(); ?>
        <header class="card-header">
            <h2 class="card-title"><?php if(isset($page_title)) {echo $page_title;}else{echo "Create KPI";} ?></h2>
        </header>
        <div class="card-body">
            <input type="hidden" class="model_id" value="<?php if(isset($model->id)){echo $model->id;} ?>" />

            <div class="row">
                <div class="col-12">
                    <label for="description">KPI Description <span style="font-size:9px"> (Max. 300 Characters Only)</span></label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <textarea class="form-control" rows="1" type="text" name="description" id="description" maxlength="300"><?php if(isset($model->description)) echo $model->description; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <br>

            <div class="row">
                <div class="col-4">
                    <label for="department">Department <span class="text-danger">*</span></label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select <?php echo $edit ? 'disabled' : ''; ?> name='department' id="department" class="form-control">
                                <option value=""
                                    <?php if(isset($model->department) && ($model->department == 'select')){ echo 'selected';} ?>>
                                    Select</option>
                                <option value="operations"
                                    <?php if(isset($model->department) && ($model->department == 'operations')){ echo 'selected';} ?>>
                                    Operations</option>
                                <option value="finance"
                                    <?php if(isset($model->department) && ($model->department == 'finance')){ echo 'selected';} ?>>
                                    Finance</option>
                                <option value="hr"
                                    <?php if(isset($model->department) && ($model->department == 'hr')){ echo 'selected';} ?>>
                                    HR</option>
                                <option value="maxima"
                                    <?php if(isset($model->department) && ($model->department == 'maxima')){ echo 'selected';} ?>>
                                    Maxima</option>
                                <option value="administration"
                                    <?php if(isset($model->department) && ($model->department == 'administration')){ echo 'selected';} ?>>
                                    Administration</option>
                                <option value="it"
                                    <?php if(isset($model->department) && ($model->department == 'it')){ echo 'selected';} ?>>
                                    IT</option>
                                <option value="service"
                                    <?php if(isset($model->department) && ($model->department == 'service')){ echo 'selected';} ?>>
                                    New Service/Geography</option>
                                <option value="valuations"
                                    <?php if(isset($model->department) && ($model->department == 'valuations')){ echo 'selected';} ?>>
                                    Valuations</option>
                                <option value="support"
                                    <?php if(isset($model->department) && ($model->department == 'support')){ echo 'selected';} ?>>
                                    Valuations Support</option>
                                <option value="data_management"
                                    <?php if(isset($model->department) && ($model->department == 'data_management')){ echo 'selected';} ?>>
                                    Data Management</option>
                                <option value="sales"
                                    <?php if(isset($model->department) && ($model->department == 'sales')){ echo 'selected';} ?>>
                                    Sales</option>
                                <option value="marketing"
                                    <?php if(isset($model->department) && ($model->department == 'marketing')){ echo 'selected';} ?>>
                                    Marketing</option>
                                <option value="empanelment"
                                    <?php if(isset($model->department) && ($model->department == 'empanelment')){ echo 'selected';} ?>>
                                    Empanelment</option>
                                <option value="business_development"
                                    <?php if(isset($model->department) && ($model->department == 'business_development')){ echo 'selected';} ?>>
                                    Business Development</option>
                                <option value="bcs"
                                    <?php if(isset($model->department) && ($model->department == 'bcs')){ echo 'selected';} ?>>
                                    BCS</option>
                                <option value="pak"
                                    <?php if(isset($model->department) && ($model->department == 'pak')){ echo 'selected';} ?>>
                                    Pakistan(Karachi)</option>
                                <option value="sa"
                                    <?php if(isset($model->department) && ($model->department == 'sa')){ echo 'selected';} ?>>
                                    Saudi Arabia(Riyadh)</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4" id="module_div">
                    <label for="module">Module</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select  name='module' id="module" class="form-control">
                                <option value="none"
                                    <?php if(isset($model->module) && ($model->module == 'none')){ echo 'selected';} ?>>
                                    Select</option>
                                <option value="valuations"
                                    <?php if(isset($model->module) && ($model->module == 'valuations')){ echo 'selected';} ?>>
                                    Valuations</option>
                                <option value="proposals"
                                    <?php if(isset($model->module) && ($model->module == 'proposals')){ echo 'selected';} ?>>
                                    Proposals</option>
                                <option value="dashboard"
                                    <?php if(isset($model->module) && ($model->module == 'dashboard')){ echo 'selected';} ?>>
                                    Dashboard</option>
                                <option value="bcs"
                                    <?php if(isset($model->module) && ($model->module == 'bcs')){ echo 'selected';} ?>>
                                    BCS</option>
                                <option value="brokerage"
                                    <?php if(isset($model->module) && ($model->module == 'brokerage')){ echo 'selected';} ?>>
                                    Brokerage</option>
                                <option value="task_management"
                                    <?php if(isset($model->module) && ($model->module == 'task_management')){ echo 'selected';} ?>>
                                    KPI Management</option>
                                <option value="reports"
                                    <?php if(isset($model->module) && ($model->module == 'reports')){ echo 'selected';} ?>>
                                    Reports</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="Reference Number">Reference Number <span class="text-danger">*</span></label>
                    <div class="form-group mb-3">
                        <input class="form-control" readonly id="reference_number" name="reference_number" 
                            value="<?php if(isset($model->reference_number)){ echo $model->reference_number; }?>" />
                    </div>
                </div>

                <div class="col-4">
                    <label for="Weightage">Weightage <span class="text-danger">*</span></label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select name='weightage' id="weightage" class="form-control">
                                <option value="0">Select Weightage</option>
                                <?php for ($i=1; $i <= 100 ; $i++) {  ?>
                                <option value="<?php echo$i;?>" <?php if(isset($model->weightage) && ($model->weightage == $i)){ echo 'selected';} ?>><?php echo $i.'%';?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="start_date">Start Date </label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input class="form-control" type="datetime-local" id="start_date" name="start_date" value="<?php $dateTime = new DateTime($model->start_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->start_date)) {echo $formattedDateTime;} ?>">
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="end_date">End Date</label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <input class="form-control" type="datetime-local" id="end_date" name="end_date" value="<?php $dateTime = new DateTime($model->end_date); $formattedDateTime = $dateTime->format('Y-m-d\TH:i'); if(isset($model->end_date)) {echo $formattedDateTime;} ?>">
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="employe">Employee <span class="text-danger">*</span></label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select <?php echo $edit ? 'disabled' : ''; ?> name="employe" id="employe" class="form-control">
                                <?php
                                foreach($clientsArr as $key => $users){
                                    if($model->employe == $key)
                                    {
                                        echo "<option value='$model->employe' selected>$users</option>";
                                    }else{
                                        echo "<option value='$key'> $users</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4" style="<?php echo $user_id==1 || $user_id==14 ? 'display: block;' : 'display: none;'; ?>">
                    <label for="approved_by">Approved By <span class="text-danger">*</span></label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select  name="approved_by" class="form-control">
                                <?php
                                foreach($clientsArr as $key => $users){
                                    if($model->approved_by == $key)
                                    {
                                        echo "<option value='$model->approved_by' selected>$clientsArr[$key]</option>";
                                    }else{
                                        if($key == 14)
                                        {
                                            echo "<option value='$key'>$users</option>";
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <label for="achievement_score">Achievement Score <span class="text-danger">*</span></label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select id="achievement_score" name='achievement_score' class="form-control">
                            <?php for ($i=1; $i <= 100 ; $i++) {  ?>
                                <option <?php if(isset($model->achievement_score) && ($model->achievement_score == $i)){ echo 'selected';} ?> value="<?php echo$i?>" ><?php echo $i.'%';?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group">
                        <label for="remaining_weightage">Remaining Weightage</label>
                        <input class="form-control" name="remaining_weightage" readonly type="text" />
                        <p class="error text-danger remaining_weightage_error"></p>
                    </div>
                </div>

            </div>
        </div>

        <div class="card-footer">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
            <?php
                if($model<>null && $model->id<>null){
                    echo Yii::$app->appHelperFunctions->getLastActionHitory([
                        'model_id' => $model->id,
                        'model_name' => get_class($model),
                    ]);
                }
            ?>
        </div>
        <?php ActiveForm::end(); ?>
    </section>

<?php

$fetchUrl = Url::to(['/kpi-manager/']);

$this->registerJs('

            let model_id = $(".model_id").val();
            
            function calculateWeightage() {

                let weightage = $("#weightage").val();
                let employe = $("#employe").val();

                if(!weightage || !employe){
                $("input[name=remaining_weightage]").val("");
                  return false;
                }

                fetch("'.$fetchUrl.'/weightage?emp_id="+employe+"&model_id="+model_id)
                .then(response => response.json())
                .then(data => {

                    let rem = parseFloat(data.data.total_weightage) - parseFloat(weightage);
                    if(rem < 0){
                    
                    }else{
                        // $(".remaining_weightage_error").text("");
                    }
                    $("input[name=remaining_weightage]").val(rem+"%");
                }).catch(error => {
                    console.error("Error:", error);
                });
            }

            $("#weightage, #employe").on("change input", calculateWeightage);
             calculateWeightage();


             
            function generatePrefix() {
                let department = $("#department").val();
                if(!department){
                   $("#reference_number").val("");
                   return false;
                }
                fetch("'.$fetchUrl.'/deptprefix?dept_id="+department)
                .then(response => response.json())
                .then(data => {
                    $("#reference_number").val(data.data.prefix_number);
                }).catch(error => {
                    console.error("Error:", error);
                });
            }
            $("#department").on("change input", generatePrefix);
        
            
            
            var value = $("#department").val();
            if(value == "maxima"){
                $("#module_div").css("display", "block");
            }else{
                $("#module_div").css("display", "none");
            }

            $("#department").change(function() {
                var val = $(this).val(); 
                if(val == "maxima")
                {
                    $("#module_div").css("display", "block");
                }else{
                    $("#module_div").val("");
                    $("#module_div").css("display", "none");
                }
            });
        
');
