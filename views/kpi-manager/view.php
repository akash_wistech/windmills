<?php
use yii\widgets\DetailView;
use yii\helpers\Html;
$this->title = $model->reference_number ?: 'No Reference';  
$this->params['breadcrumbs'][] = ['label' => 'Task Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ptask-manager-view">
    <section class="card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title"><?= Html::encode($this->title) ?></h2>
        </header>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>Reference Number:</strong></label>
                        <p><?= Html::encode($model->reference_number ?: 'Not provided') ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>Employee:</strong></label>
                        <p><?= Html::encode($selecteduser ?: 'Unknown') ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>Unit:</strong></label>
                        <p><?= Html::encode($department ?: 'Not assigned') ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>Department:</strong></label>
                        <?php  //dd($model->unit); ?>
                        <p><?= Html::encode($unit ?: 'Not assigned') ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>Periodic Task:</strong></label>
                        <p><?= Html::encode($model->description ?: 'No description available') ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>Start Date:</strong></label>
                        <p><?= $model->start_date ? Html::encode(date("j M Y", strtotime($model->start_date))) : 'Not set' ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>End Date:</strong></label>
                        <p><?= $model->end_date ? Html::encode(date("j M Y", strtotime($model->end_date))) : 'Not set' ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>Weightage:</strong></label>
                        <p><?= Html::encode($model->weightage ? $model->weightage . '%' : '0%') ?></p>
                    </div>
                </div>

            </div>

            <div class="text-right">
                <?= Html::a('Back to List', ['index'], ['class' => 'btn btn-secondary']) ?>
            </div>
        </div>

    </section>
</div>
