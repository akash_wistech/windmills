<?php

use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\User;
use app\models\UserProfileInfo;
use kartik\select2\Select2;

$this->title = 'KPI Manager';
$this->params['breadcrumbs'][] = $this->title;
// Calculate totals
$total_weightage = 0;
$total_performance = 0;
foreach ($dataProvider->models as $item) {
    $total_weightage += $item->weightage;
    $total_performance += ($item->weightage / 100) * ($item->achievement_score / 100) * 100;
}

// Register required assets
$this->registerCssFile('https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css');
$this->registerJsFile('https://code.jquery.com/jquery-3.6.0.min.js', ['position' => \yii\web\View::POS_HEAD]);
$this->registerJsFile('https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::class]]);
?>

<style>
    th{
        text-align: center;
    }
    .table {
    font-size: 14px;
}
.table th, .table td {
    /* text-align: center; */
    vertical-align: middle;
}
.table th {
    font-weight: bold;
    text-transform: uppercase;
}
.table input {
    font-size: 12px;
}
.d-flex {
    display: flex;
}
.gap-2 {
    gap: 5px;
}
th {
        text-align: center;
        vertical-align: middle !important;
    }
</style>

<div class="card">
    <div class="card-header">
        <h3>KPI Manager</h3>
    </div>
    <div class="card-body">
    <div class="table-responsive">
    <table id="kpi-table" class="table table-bordered table-striped table-hover" style="width:100%">
        <thead class="bg-primary text-white">
            <tr>
                <th style="width: 160px;">Ref No</th>
                <th style="width: 200px;">Employee</th>
                <th style="width: 100px;">Unit</th>
                <th style="width: 3 0px;">Department</th>
                <th style="width: 900px !important;">KPI</th>
                <th style="width: 150px;">Start Date</th>
                <th style="width: 150px;">End Date</th>
                <th style="width: 80px;">W</th>
                <th style="width: 80px;">R</th>
                <th style="width: 100px;">WAR</th>
                <th style="width: 150px;">Status</th>
                <th style="width: 200px;">Action</th>
            </tr>
            <tr>
                <th><input type="text" class="form-control form-control-sm" placeholder="Search..."></th>
                <th>
                <select name="employee_filter" class="form-control form-control-sm">
                    <option value="">Select Employee</option>
                    <?php foreach (User::find()->all() as $user): ?>
                        <option value="<?= Html::encode(ucfirst($user->firstname) . ' ' . ucfirst($user->lastname)) ?>">
                            <?= Html::encode(ucfirst($user->firstname) . ' ' . ucfirst($user->lastname)) ?>
                        </option>
                    <?php endforeach; ?>
                </select>
                </th>
                <th>
                    <select name="unit_filter" class="form-control form-control-sm">
                        <option value="">Select Unit</option>
                        <?php foreach ($units as $unit): ?>
                            <option value="<?= $unit['title'] ?>"><?= ucfirst($unit['title']) ?></option>
                        <?php endforeach; ?>
                    </select>
                </th>
                <th>
                    <select name="department_filter" class="form-control form-control-sm">
                        <option value="">Select Department</option>
                        <?php foreach ($departments as $department): ?>
                            <option value="<?= $department['title'] ?>"><?= ucfirst($department['title']) ?></option>
                        <?php endforeach; ?>
                    </select>
                </th>
                <th><input type="text" class="form-control form-control-sm" placeholder="Search..."></th>
                <th><input type="text" class="form-control form-control-sm date-picker" placeholder="dd-mm-yyyy"></th>
                <th><input type="text" class="form-control form-control-sm date-picker" placeholder="dd-mm-yyyy"></th>
                <th><input type="text" class="form-control form-control-sm" placeholder="Search..."></th>
                <th><input type="text" class="form-control form-control-sm" placeholder="Search..."></th>
                <th><input type="text" class="form-control form-control-sm" placeholder="Search..."></th>
                <th>
                    <?= Html::dropDownList(
                        'status_filter',
                        null,
                        ['submitted' => 'Submitted', 'verified' => 'Verified', 'approved' => 'Approved'],
                        ['class' => 'form-control form-control-sm', 'prompt' => 'Select Status']
                    ); ?>
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $currentUserId = Yii::$app->user->identity->id;
            foreach ($dataProvider->models as $model):
                $enterByUserInfo = UserProfileInfo::findOne(['user_id' => $model->employe]);

               /* if (
                    $model->employe == $currentUserId || 
                    ($enterByUserInfo && $enterByUserInfo->reports_to == $currentUserId) ||
                    ($currentUserId == 1 && $model->is_verified == 1)
                ):*/
            ?>
                <tr>
                    <td><?= Html::encode($model->reference_number) ?></td>
                    <td>
                        <?php
                        $user = User::findOne($model->employe);
                        echo $user ? Html::encode($user->firstname . ' ' . $user->lastname) : 'N/A';
                        ?>
                    </td>
                    <td><?= Html::encode(ucfirst($model->unitRelation->title ?? 'Unknown')) ?></td>
                    <td><?= Html::encode($model->departmentRelation->title ?? 'Unknown') ?></td>
                    <td><?= Html::encode(substr($model->description, 0, 60)) ?>...</td>
                    <td><?= date('j M Y', strtotime($model->start_date)) ?></td>
                    <td><?= date('j M Y', strtotime($model->end_date)) ?></td>
                    <td><?= Html::encode(number_format(round($model->weightage, 2), 2, '.', '')) ?>%</td>
                    <td><?= Html::encode(number_format(round($model->achievement_score, 2), 2, '.', '')) ?>%</td>
                    <td><?= $model->is_approved ? round(($model->weightage / 100) * ($model->achievement_score / 100) * 100, 2) . '%' : ' ' ?></td>
                    <td>
                        <?= $model->is_approved
                            ? '<span class="badge bg-success">Approved</span>'
                            : ($model->is_verified
                                ? '<span class="badge bg-info">Verified</span>'
                                : '<span class="badge bg-warning">Submitted</span>'); ?>
                    </td>
                   
                    <td class="d-flex justify-content-center gap-2">
                        <a href="<?= Url::to(['/kpi-manager/update', 'id' => $model->id]) ?>" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                        <?php if ($currentUserId == 1 || $currentUserId == 111811 ){ ?>
                            <a href="#" 
                                class="btn btn-danger btn-sm" 
                                data-toggle="modal" 
                                data-target="#deleteConfirmationModal"
                                data-href="<?= Url::to(['/kpi-manager/delete', 'id' => $model->id]) ?>">
                                    <i class="fa fa-trash"></i>
                            </a>
                        <?php } ?>
                        <a href="<?= Url::to(['/kpi-manager/view', 'id' => $model->id]) ?>" class="btn btn-warning btn-sm"><i class="fa fa-eye"></i></a>
                       
                    </td>
                   
                </tr>
            <?php 
       // endif;
        endforeach; ?>
        </tbody>
            <tfoot id="table-footer" style="display: none;">
                <tr>
                    <th colspan="7" style="text-align: right; font-weight: bold;">Totals:</th>
                    <th id="total-weightage">0%</th>
                    <th></th>
                    <th id="total-performance">0%</th>
                    <th colspan="2"></th>
                </tr>
            </tfoot>
    </table>
</div>

    </div>
</div>



<!-- Bootstrap Modal for Delete Confirmation -->
<div class="modal fade" id="deleteConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Confirm Deletion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete this KPI?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <a href="#" class="btn btn-danger" id="deleteConfirmButton">Delete</a>
      </div>
    </div>
  </div>
</div>


<?php
$this->registerJs('
$(document).ready(function () {
    var table = $("#kpi-table").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        autoWidth: false,
        responsive: true,
        orderCellsTop: true, // Use the top row of cells for sorting
        fixedHeader: true, 
        order: [], // Initial no order applied
        initComplete: function () {
            var api = this.api();
            // Only target the filter inputs in the second header row for each column
            $(\'thead tr:eq(1) th\').each(function (i) {
                $(\'input, select\', this).on(\'change keyup\', function () {
                    if (api.column(i).search() !== this.value) {
                        api.column(i).search(this.value).draw();
                    }
                });
            });
        },
        drawCallback: function () {
            calculateTotals(this.api());
        },
        columnDefs: [
            { orderable: true, targets: \'_all\' }, // Initially enable sorting on all columns
            { orderable: false, targets: \'no-sort\' } 
        ]
    });

    // Prevent sorting when clicking on inputs/selects
    $(\'thead tr:eq(1) input, thead tr:eq(1) select\').on(\'click\', function(e) {
        e.stopPropagation();
    });


    

    // Flatpickr initialization for date pickers
    $(".date-picker").flatpickr({
        dateFormat: "j M Y",
        allowInput: true,
    });

    // Function to calculate totals
    function calculateTotals(api) {
        var totalWeightage = 0;
        var totalPerformance = 0;
        var visibleRows = 0;

        // Check if any filter or search is applied
        var isFiltered = api.rows({ filter: "applied" }).data().length !== api.rows().data().length;

        // Iterate over visible rows
        api.rows({ filter: "applied" }).every(function () {
            visibleRows++; // Count visible rows
            var data = this.data();

            // Directly access raw data from the `data` array
            var weightage = parseFloat(data[7].replace("%", "")) || 0; 
            var performance = parseFloat(data[9].replace("%", "")) || 0; 

            totalWeightage += weightage;
            totalPerformance += performance;
        });

        // Update footer
        $("#total-weightage").text(totalWeightage.toFixed(2) + "%");
        $("#total-performance").text(totalPerformance.toFixed(2) + "%");

        // Show footer only if filtering or searching is applied and there are visible rows
        if (visibleRows > 0 && isFiltered) {
            $("#table-footer").fadeIn();
        } else {
            $("#table-footer").fadeOut();
        }
    }

     $(\'#deleteConfirmationModal\').on(\'show.bs.modal\', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var deleteUrl = button.data(\'href\'); // Extract info from data-* attributes
        var modal = $(this);
        modal.find(\'#deleteConfirmButton\').attr(\'href\', deleteUrl);
    });
});
');
?>
<?php
$this->registerCssFile('https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css');
$this->registerJsFile('https://cdn.jsdelivr.net/npm/flatpickr', ['depends' => [\yii\web\JqueryAsset::class]]);
?>
<style>
    
    #kpi-table thead th {
        background-color: #007bff;
        color: white;
        text-align: center;
        font-weight: bold;
        width: 130px;
    }

    /* Dropdown Styling */
    select.form-control {
        width: 100%;
        margin: 0 auto;
    }
</style>


