<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Update KPI ';
$this->params['breadcrumbs'][] = ['label' => 'KPI Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';


$clientsArr = ArrayHelper::map(\app\models\User::find()
    ->select([
        'id', 'CONCAT(firstname, " ", lastname) AS lastname',
    ])
    ->where(['status' => 1])
    ->andWhere(['user_type' => [10,20]])
    ->orderBy(['lastname' => SORT_ASC,])
    ->all(), 'id', 'lastname');

$clientsArr = ['' => 'select'] + $clientsArr;
$user_id = Yii::$app->user->identity->id;

$percentages = [
    '5',
    '10',
    '15',
    '20',
    '25',
    '30',
    '35',
    '40',
    '45',
    '50',
    '55',
    '60',
    '65',
    '70',
    '75',
    '80',
    '85',
    '90',
    '95',
    '100',
];

$departments = [
    "" => "Select",
    "operations" => "Operations",
    "finance" => "Finance",
    "hr" => "HR",
    "maxima" => "Maxima",
    "administration" => "Administration",
    "it" => "IT",
    "service" => "New Service/Geography",
    "valuations" => "Valuations",
    "support" => "Valuations Support",
    "data_management" => "Data Management",
    "sales" => "Sales",
    "marketing" => "Marketing",
    "empanelment" => "Empanelment",
    "business_development" => "Business Development",
    "bcs" => "BCS",
    "pak" => "Pakistan(Karachi)",
    "sa" => "Saudi Arabia(Riyadh)",
];


$old = Yii::$app->session->getFlash('old', []);
$errors = Yii::$app->session->getFlash('errors', []);

$currentYear = date('Y');

// Start date: January 1 of the current year
$nextYear = $currentYear + 1;
$nextYearDate = "31-12-$nextYear";
$startDate = "01-01-$nextYear";
$currentDate = $startDate;

?>

<style>

    .files img{
        height: 37px;
        width: 50px;
    }

    .files th,td{
        text-align: center;
    }

    .files td,th{
        border: 1px solid lightgray!important;
        border-top: 1px solid lightgray !important;
        /* text-align: center; */
        /* vertical-align: middle !important; */
        /* padding: 12px 4px !important; */
    }
</style>

<div class="task-update">
<section class="contact-form card card-outline card-primary">
     <form action="<?php echo Url::to(['/kpi-manager/updatedata/']);?>?id=<?=$model->id?>" method="post">

        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->csrfToken ?>">
        <input type="hidden" name="model_id" class="model_id" value="<?=$model->id?>" />

        <header class="card-header">
            <h2 class="card-title"><?=$this->title?></h2>
        </header>

        <div class="card-body">        
            <div class="row">

            <div class="col-md-4">
                    <div class="form-group">
                        <label for="employe">Employee <span class="text-danger">*</span></label>
                        <select disabled id="employe"  name="employe" class="form-control">
                            <?php foreach($clientsArr as $key => $users){ ?>
                                <option 
                                <?= $model->employe == $key ? 'selected' : '' ?>
                                value='<?=$key?>'><?=$users?></option>
                            <?php }?>
                        </select>
                        <?php if (isset($errors['employe'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['employe'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="department">Unit <span class="text-danger">*</span></label>
                        <input readonly class="form-control" value="<?=$departmentTitle?>" />     
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="department">Department <span class="text-danger">*</span></label>
                        <input readonly class="form-control" value="<?=$unitTitle?>" />     
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group">
                        <label for="description">KPI Description 
                            <span style="font-size:9px"> (Max. 300 Characters Only)</span>
                            <span class="text-danger">*</span>
                        </label>
                        <textarea class="form-control" rows="2" type="text" name="description" maxlength="300"><?=$model->description?></textarea>
                        <?php if (isset($errors['description'])): ?>
                            <p class="text-danger"><?= Html::encode($errors['description'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="Reference Number">Reference Number <span class="text-danger">*</span></label>
                        <input class="form-control" readonly value="<?=$model->reference_number?>" />
                    </div>
                </div>

                

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="start_date">Start Date <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <input class="form-control date-picker" type="text" name="start_date"
                                value="<?= $currentDate ?>" placeholder="dd-mm-yyyy" readonly />
                            <span class="input-group-text">
                                <i class="fas fa-calendar-alt"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="end_date">End Date <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <input class="form-control date-picker" type="text" name="end_date"
                                value="<?= $nextYearDate ?>" placeholder="dd-mm-yyyy"  readonly/>
                            <span class="input-group-text">
                                <i class="fas fa-calendar-alt"></i>
                            </span>
                        </div>
                    </div>
                </div>

            
                <?php if($model->is_approved){ ?>
                <div class="col-md-4">
                   <div class="form-group">
                       <label for="approved_by">Approved By <span class="text-danger">*</span></label>
                       <select disabled name="approved_by" class="form-control">
                            <?php foreach($clientsArr as $key => $users){ ?>
                                     <option <?= $model->approved_by == $key ? 'selected' : '' ?>
                                     value='<?=$key?>'><?=$users?></option>";
                            <?php } ?>
                        </select>
                        <?php if (isset($errors['approved_by'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['approved_by'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div>
                <?php } ?>

                <?php if($model->is_verified){ ?>
                <div class="col-md-4">
                   <div class="form-group">
                       <label for="approved_by">Verify By <span class="text-danger">*</span></label>
                       <select disabled name="verified_by" class="form-control">
                            <?php foreach($clientsArr as $key => $users){ ?>
                                     <option <?= $model->verified_by == $key ? 'selected' : '' ?>
                                     value='<?=$key?>'><?=$users?></option>";
                            <?php } ?>
                        </select>
                        <?php if (isset($errors['verified_by'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['verified_by'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div>
                <?php } ?>

                <?php  if (Yii::$app->user->identity->id == $reportedTo || $departmentDetails->id == 1){ ?>

                <div class="col-md-4">
                    <div class="form-group">
                            <label for="Weightage">Weightage <span class="text-danger">*</span></label>
                            <select name='weightage' id="weightage" class="form-control">
                                <option value="">Select Weightage</option>
                                <?php for ($i=1; $i <= 100 ; $i++) { ?>
                                    <option <?= $model->weightage == $i ? 'selected' : '' ?>
                                    value="<?php echo$i;?>"><?php echo $i.'%';?></option>
                                <?php } ?>
                            </select>
                            <?php if (isset($errors['weightage'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['weightage'][0]);?></p>
                            <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="achievement_score">Achievement Score <span class="text-danger">*</span>
                        </label>
                        <select id="achievement_score" name='achievement_score' class="form-control">
                        <option value="">Select </option>
                        <?php foreach ($percentages as $key => $p) { ?>
                            <option 
                            <?= $model->achievement_score == $p ? 'selected' : '' ?>
                            value="<?=$p?>" ><?=$p.'%'?></option>
                            <?php } ?>
                        </select>
                        <?php if (isset($errors['achievement_score'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['achievement_score'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="remaining_weightage">Remaining Weightage</label>
                        <input class="form-control" name="remaining_weightage" readonly type="text" />
                        <?php if (isset($errors['remaining_weightage'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['remaining_weightage'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        
            <?php } ?>
           

        </div>
      
        <div class="card-footer">
                <button type="submit" class="btn btn-success">Save</button>
                <?php 
                if(Yii::$app->user->identity->id ==1 && $model->is_approved == null){
                    echo '<a class="btn btn-primary" href="'.Url::to('/kpi-manager/approved?id='.$model->id).'">Approved</a>';
                }elseif(Yii::$app->user->identity->id == $reportedTo && $model->is_verified == null && $model->is_approved == 0){
                    echo '<a class="btn btn-primary" href="'.Url::to('/kpi-manager/verified?id='.$model->id).'">Verify</a>';
                }
                
                ?>
                <a class="btn btn-default" href="<?= Url::to('/kpi-manager/');?>">Cancel</a>
                <?php
                    if($model<>null && $model->id<>null){
                        echo Yii::$app->appHelperFunctions->getLastActionHitory([
                            'model_id' => $model->id,
                            'model_name' => get_class($model),
                        ]);
                    }
                ?>
        </div>

     </form>
    </section>


</div>

<?php 


$fetchUrl = Url::to(['/kpi-manager/']);

$this->registerJs('

            let model_id = $(".model_id").val();
            
            function calculateWeightage() {
            
                let weightage = $("#weightage").val();
                let employe = $("#employe").val();

                if(!employe){
                $("input[name=remaining_weightage]").val("");
                  return false;
                }

                fetch("'.$fetchUrl.'/weightage?emp_id="+employe+"&model_id="+model_id)
                .then(response => response.json())
                .then(data => {

                    weightage = parseFloat(weightage) || 0;
                    let rem = parseFloat(data.data.total_weightage) - parseFloat(weightage);
                    if(rem < 0){
                    
                    }else{
                        // $(".remaining_weightage_error").text("");
                    }
                    $("input[name=remaining_weightage]").val(rem+"%");
                }).catch(error => {
                    console.error("Error:", error);
                });
            }

            $("#weightage, #employe").on("change input", calculateWeightage);
             calculateWeightage();

');