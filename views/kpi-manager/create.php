<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


$this->title = 'Create KPI';
$this->params['breadcrumbs'][] = ['label' => 'KPI Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$clientsArr = ArrayHelper::map(\app\models\User::find()
    ->select([
        'id', 'CONCAT(firstname, " ", lastname) AS lastname',
    ])
    ->where(['status' => 1])
    ->andWhere(['user_type' => [10,20]])
    ->orderBy(['lastname' => SORT_ASC,])
    ->all(), 'id', 'lastname');

$clientsArr = ['' => 'select'] + $clientsArr;
$user_id = Yii::$app->user->identity->id;
$user = Yii::$app->user->identity;

$percentages = [
    '5',
    '10',
    '15',
    '20',
    '25',
    '30',
    '35',
    '40',
    '45',
    '50',
    '55',
    '60',
    '65',
    '70',
    '75',
    '80',
    '85',
    '90',
    '95',
    '100',
];




$old = Yii::$app->session->getFlash('old', []);
$errors = Yii::$app->session->getFlash('errors', []);
$is_hod = Yii::$app->menuHelperFunction->checkActionAllowed('hod_rights');


// Calculate the default dates in PHP
$currentYear = date('Y');

// Start date: January 1 of the current year
$nextYear = $currentYear + 1;
$nextYearDate = "31-12-$nextYear";
$startDate = "01-01-$nextYear";
$currentDate = $startDate;


$this->registerJs('

 
    $(document).ready(function(){

  $("#onlinevaluation-community").html("");
                    $("#department").val("");
                      $("#department").attr("data-department","");
                        $("#employee-display-name").html("");
                        $("#unit").val("");
                        $("#unit").attr("data-unit","");
                        $("#employee-display-name-unit").html("");  
              
 
        
        $("#employe").on("change", function () {

            var employe_id = $(this).val();
            var dep_url = "kpi-manager/getdata?employe_id="+employe_id;
             
            depSelection(employe_id,dep_url);            
        });
        
         function depSelection(city_id,dep_url){
            heading=$(this).data("heading");
            if (city_id) {
                $.ajax({
                    url: dep_url, 
                    success: function(data){
                        var response = JSON.parse(data);
                        $("#department").val(response.departmentDetails.id);
                        $("#department").attr("data-department",response.departmentDetails.title);
                        $("#employee-display-name").html(response.departmentDetails.title);
                        $("#unit").val(response.unittDetails.id);
                        $("#unit").attr("data-unit",response.unittDetails.title);
                        $("#employee-display-name-unit").html(response.unittDetails.title);
                        generatePrefix();
                        //console.log(response);
                       
               
                    },
                    error: function(xhr, status, error) {
                        console.error("AJAX error:", error);
                    }
                });
            } else {
                $("#department").val("");
                      $("#department").attr("data-department","");
                        $("#employee-display-name").html("");
                        $("#unit").val("");
                        $("#unit").attr("data-unit","");
                        $("#employee-display-name-unit").html("");  
              
            }
        }
        
        
        


    });


    


    


 
');



$this->registerCssFile('https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css');
$this->registerJsFile('https://cdn.jsdelivr.net/npm/flatpickr', ['depends' => [\yii\web\JqueryAsset::class]]);
?>

<style>
.input-group .form-control {
    border-right: 0;
}

.input-group .input-group-text {
    background: #fff;
    border-left: 0;
    cursor: pointer;
}

.input-group .input-group-text i {
    color: #007bff;
}
</style>

<div class="task-create">
  <section class="contact-form card card-outline card-primary">
     <form action="<?php echo Url::to(['/kpi-manager/store']);?>" method="post">

        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->csrfToken ?>">

        <header class="card-header">
            <h2 class="card-title">Create KPI</h2>
        </header>
        <div class="card-body"> 
            <div class="row">

                <div class="col-4">
                    <label for="employe">Employee d <span class="text-danger">*</span></label>
                    <div class="input-group-md mb-3">
                        <div class="input-group-prepend">
                            <select <?php echo $edit ? 'disabled' : ''; ?> name="employe" id="employe" class="form-control">
                                <?php
                                foreach($clientsArr as $key => $users){
                                    if($model->employe == $key)
                                    {
                                        echo "<option value='$model->employe' selected>$users</option>";
                                    }else{
                                        echo "<option value='$key'> $users</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <!--<input
                        class="form-control"
                        id="employe-data"
                        name="employe"
                        value="<?/*= Yii::$app->user->identity->id; */?>"
                        readonly
                        type="hidden"
                        data-user="<?/*= Yii::$app->user->identity->id; */?>"
                />-->
           <!-- <div class="col-md-4">
                   <div class="form-group">
                       <label for="Enter By">Employee<span class="text-danger">*</span></label>
                       <div class="form-group">
                        <input 
                            class="form-control" 
                            id="employe-data" 
                            name="employe" 
                            value="<?/*= Yii::$app->user->identity->id; */?>"
                            readonly 
                            type="hidden" 
                            data-user="<?/*= Yii::$app->user->identity->id; */?>"
                        />
                        <span id="employee-display-name"><?/*= Yii::$app->user->identity->name; */?></span>
                    </div>

                    </div>
                </div>-->

                <div class="col-md-4">
                   <div class="form-group">
                       <label for="Enter By">Unit<span class="text-danger">*</span></label>
                       <div class="form-group">
                        <input 
                            class="form-control" 
                            id="department" 
                            name="department" 
                            value="<?= $departmentDetails->id ?>" 
                            readonly 
                            type="hidden" 
                            data-department="<?= $departmentDetails->title ?>" 
                            
                        />
                        <span id="employee-display-name"><?= $departmentDetails->title ?></span>
                    </div>

                    </div>
                </div>

                <div class="col-md-4">
                   <div class="form-group">
                       <label for="Enter By">Department<span class="text-danger">*</span></label>
                       <div class="form-group">
                        <input 
                            class="form-control" 
                            id="unit" 
                            name="unit" 
                            value="<?= $unittDetails->id ?>" 
                            readonly 
                            type="hidden" 
                            data-unit="<?= $unittDetails->title ?>"
                            
                        />
                        <span id="employee-display-name-unit"><?= $unittDetails->title ?></span>
                    </div>

                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group">
                        <label for="description">KPI Description 
                            <span style="font-size:9px"> (Max. 300 Characters Only)</span>
                            <span class="text-danger">*</span>
                        </label>
                        <textarea class="form-control" rows="2" type="text" name="description" maxlength="300"><?=$old['description'] ?? ''?></textarea>
                        <?php if (isset($errors['description'])): ?>
                            <p class="text-danger"><?= Html::encode($errors['description'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="Reference Number">Reference Number 
                            <span class="text-danger">*</span>
                        </label>
                        <input class="form-control" readonly id="reference_number" name="reference_number" 
                            value="" />
                        <?php if (isset($errors['reference_number'])): ?>
                            <p class="text-danger"><?= Html::encode($errors['reference_number'][0]);?></p>
                         <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="start_date">Start Date <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <!-- Format date in PHP using date() and strtotime() -->
                            <input class="form-control" type="text" name="start_date"
                                value="<?= date('j M Y', strtotime($currentDate)); ?>" readonly placeholder="1 Dec 2024"/>
                            <span class="input-group-text">
                                <i class="fas fa-calendar-alt"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="end_date">End Date <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <!-- Ensure $nextYearDate is correctly formatted in your PHP code -->
                            <input class="form-control" type="text" name="end_date"
                                value="<?= date('j M Y', strtotime($nextYearDate)); ?>" readonly placeholder="1 Dec 2025"/>
                            <span class="input-group-text">
                                <i class="fas fa-calendar-alt"></i>
                            </span>
                        </div>
                    </div>
                </div>


                <?php 
                // 18 for managment
                 if ($departmentDetails->id == 1){ ?>

                <div class="col-md-4">
                    <div class="form-group">
                            <label for="Weightage">Weightage <span class="text-danger">*</span></label>
                            <select name='weightage' id="weightage" class="form-control">
                                <option value="">Select Weightage</option>
                                <?php for ($i=1; $i <= 100 ; $i++) {  ?>
                                
                                    <option 
                                    <?= isset($old['weightage']) && $old['weightage'] == $i ? 'selected' : '' ?>
                                    value="<?php echo$i;?>" <?php if(isset($model->weightage) && ($model->weightage == $i)){ echo 'selected';} ?>><?php echo $i.'%';?></option>

                                <?php } ?>
                            </select>
                            <?php if (isset($errors['weightage'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['weightage'][0]);?></p>
                            <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="achievement_score">Achievement Score <span class="text-danger">*</span>
                        </label>
                        <select id="achievement_score" name='achievement_score' class="form-control">
                        <option value="">Select </option>

                        <?php foreach ($percentages as $key => $p) { ?>
                            <option 
                            <?= isset($old['achievement_score']) && $old['achievement_score'] == $i ? 'selected' : '' ?>
                            value="<?=$p?>" ><?=$p.'%'?></option>
                            <?php } ?>
                        </select>
                        <?php if (isset($errors['achievement_score'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['achievement_score'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="Performance">Performance (Rating) <span class="text-danger">*</span></label>
                        <input class="form-control" name="performance" readonly />
                        <?php if (isset($errors['performance'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['performance'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="remaining_weightage">Remaining Weightage</label>
                        <input class="form-control" name="remaining_weightage" readonly type="text" />
                        <?php if (isset($errors['remaining_weightage'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['remaining_weightage'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div>
            <?php } ?>

                <div class="col-md-4">
                   <div class="form-group">
                       <label for="Enter By">Enter By <span class="text-danger">*</span></label>
                       <input class="form-control" name="enter_by" value="<?php echo Yii::$app->user->identity->name; ?>" readonly type="text" />
                       <?php if (isset($errors['enter_by'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['enter_by'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div>

                <!-- <div class="col-md-4">
                   <div class="form-group">
                       <label for="verify_by">Verify By</label>
                       
                        <?php  
                        if(Yii::$app->user->identity->id ==1111 ){ ?>
                       <select name="verify_by" class="form-control">
                            <?php foreach($clientsArr as $key => $users){ ?>
                                     <option <?= $user_id == $key ? 'selected' : '' ?> value='<?=$key?>'><?=$users?></option>
                            <?php } ?>
                        </select>
                        <?php } else {?>
                        <input class="form-control" name="verify_by" readonly type="text" />
                        <?php } ?>
                        <?php if (isset($errors['verify_by'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['verify_by'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div> -->

                <!-- <div class="col-md-4">
                   <div class="form-group">
                       <label for="approved_by">Approved By</label>
                       <?php  
                        if(Yii::$app->user->identity->id ==1){ ?>
                       <select name="approved_by" class="form-control">
                            <?php foreach($clientsArr as $key => $users){ ?>
                                     <option <?= $user_id == $key ? 'selected' : '' ?> value='<?=$key?>'><?=$users?></option>
                            <?php } ?>
                        </select>
                        <?php } else {?>
                        <input class="form-control" name="approved_by" readonly type="text" />
                        <?php } ?>
                        <?php if (isset($errors['approved_by'])): ?>
                             <p class="text-danger"><?= Html::encode($errors['approved_by'][0]);?></p>
                        <?php endif; ?>
                    </div>
                </div> -->


            </div>
        </div>

        <div class="card-footer">
                <button type="submit" class="btn btn-success">Submit</button>
                <a class="btn btn-default" href="<?= Url::to('/kpi-manager/');?>">Cancel</a>
                <?php
                    if($model<>null && $model->id<>null){
                        echo Yii::$app->appHelperFunctions->getLastActionHitory([
                            'model_id' => $model->id,
                            'model_name' => get_class($model),
                        ]);
                    }
                ?>
        </div>

     </form>
    </section>
</div>

<?php
$fetchUrl = Url::to(['/kpi-manager/']);
$deptPrefixUrl = Url::to(['/kpi-manager/deptprefix'], true);

// JavaScript with employee-dependant selections for department and business unit
$this->registerJs("
    
      function calculateWeightage() {
        let weightage = parseFloat($('#weightage').val()) || 0;
        let employee = $('#employe-data').data('user') || $('#employe').val(); // Get data-user or dropdown value

        if (!employee) {
            $('input[name=remaining_weightage]').val('');
            return false;
        }

        fetch('${fetchUrl}/weightage?emp_id=' + employee)
            .then(response => response.json())
            .then(data => {
                let totalWeightage = parseFloat(data.data.total_weightage) || 0;
                let remaining = totalWeightage - weightage;
                $('input[name=remaining_weightage]').val(remaining + '%');
            })
            .catch(error => {
                console.error('Error fetching weightage:', error);
            });
    }

    // Function to initialize weightage for the preselected employee
    function initializeDefaultWeightage() {
        let preselectedEmployee = $('#employe-data').data('user'); // Get employee ID from data-user
        if (preselectedEmployee) {
            calculateWeightage(); // Trigger weightage calculation for the default employee
        }
    }
                    
  function generatePrefix() {
    //var department = $('#department').data('department'); 
    var department = $('#department').attr(\"data-department\")
    //alert(department);

    console.log('Department:', department);

    if (!department) {
        $('#reference_number').val(''); // Clear reference number if no department
        return;
    }

    // Fetch the prefix from the server
   fetch('${deptPrefixUrl}?dept_id=' + encodeURIComponent(department), {
        method: 'GET',
        headers: {
            'X-CSRF-Token': '<?= Yii::$app->request->csrfToken ?>'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('HTTP error! status: ' + response.status);
        }
        return response.json();
    })
    .then(data => {
        if (data.status === 'success' && data.data.prefix_number) {
            $('#reference_number').val(data.data.prefix_number); // Update the reference number
        } else {
            console.error('Error in response:', data.message || 'Prefix number missing');
            $('#reference_number').val(''); // Clear reference number if prefix is missing
        }
    })
    .catch(error => {
        console.error('Error:', error);
        $('#reference_number').val(''); // Clear reference number on error
    });
}

// Trigger the prefix generation on page load and department change
$('#department').on('change input', generatePrefix);
generatePrefix();

    // Initialize flatpickr date pickers
    function initializeDatePickers() {
        // General date picker with custom format
        $('.date-picker').flatpickr({
            altInput: true,
            altFormat: 'j M Y', 
            dateFormat: 'd-m-Y',
            allowInput: true
        });

        // Specific date picker for 'start_date' with a default value
        $('.date-picker[name=\"start_date\"]').flatpickr({
            defaultDate: '" . $currentDate . "', 
            altInput: true,
            altFormat: 'j M Y', 
            dateFormat: 'd-m-Y', 
            allowInput: false,
                onReady: function(selectedDates, dateStr, instance) {
                instance.input.setAttribute('readonly', 'readonly');
            }
        });

        // Specific date picker for 'end_date' with a default value and minDate
        $('.date-picker[name=\"end_date\"]').flatpickr({
            defaultDate: '" . $nextYearDate . "', 
            altInput: true,
            altFormat: 'j M Y', 
            dateFormat: 'd-m-Y', 
            minDate: '" . date('d-m-Y') . "', 
            allowInput: false,
             onReady: function(selectedDates, dateStr, instance) {
            instance.input.setAttribute('readonly', 'readonly');
        }
        });
    }

    // Initialize the date pickers

    $(document).ready(function () {
        $('#weightage, #employe').on('change input', calculateWeightage);

        initializeDefaultWeightage(); 
        // generatePrefix();
        initializeDatePickers();
    });
", yii\web\View::POS_READY, 'kpi-behaviors');

?>
