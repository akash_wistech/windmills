<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use  app\components\widgets\StatusVerified;
use yii\helpers\Url;


use app\assets\SaleMeetingAsset;
SaleMeetingAsset::register($this);


$clientContactsArr = null;
if($model->id <> null){
    $clientContactsArr = Yii::$app->smHelper->getClientContactsArr($model->client_id);
}

$clientMainAttendee_db_id=null;
if($model->clientMainAttendee<>null){
    $model->client_attendee[0]['client_attendee_title'] = $model->clientMainAttendee->client_attendee_title;
    $model->client_attendee[0]['client_attendee_id'] = $model->clientMainAttendee->sales_and_marketing_attendees_id;
    $clientMainAttendee_db_id = $model->clientMainAttendee->id;
}

$wmMainAttendee_db_id=null;
if($model->wmMainAttendee<>null){
    $model->wm_attendee[0]['wm_attendee_title'] = $model->wmMainAttendee->client_attendee_title;
    $model->wm_attendee[0]['wm_attendee_id'] = $model->wmMainAttendee->sales_and_marketing_attendees_id;
    $wmMainAttendee_db_id = $model->wmMainAttendee->id;
}

?>


<style>
.width_40 {
    width: 40% !important;

}

.width_20 {
    width: 20% !important;

}

.padding_5 {
    padding: 5px;
}

.kv-file-content,
.upload-docs img {
    width: 80px !important;
    height: 80px !important;
}
/* .img-thumbnail{
    border: none;
    box-shadow: none;
} */
</style>

<section class="sales-and-marketing card">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card-body">

        <section class="card" style="border-top:2px solid purple;">
            <header class="card-header">
                <h2 class="card-title"><strong>Schedule Meeting/Zoom/Call</strong></h2>
            </header>
            <div class="card-body">

                <div class="row">
                    <div class="col-4">
                        <?php
                        echo $form->field($model, 'client_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Company::find()->where(['allow_for_valuation'=>1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title'),
                            'options' => [
                                'placeholder' => 'Select...',
                                'id'=>'client_id',        
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'disabled' => $disabled,
                            ],
                        ]);
                        ?>
                    </div>

                </div>



                <?php $client_counter = 0; ?>

                <section class="card" style="border-top:2px solid #00897B;">
                    <header class="card-header">
                        <h2 class="card-title"><strong>Client attendees</strong></h2>
                    </header>
                    <div class="card-body">


                        <section class="card" style="border-top:2px solid #00897B;">
                            <header class="card-header">
                                <h2 class="card-title"><strong>Client main attendee</strong></h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <?= $form->field($model, 'client_attendee['.$client_counter.'][client_attendee_type]')->hiddenInput(['maxlength' => true, 'value'=>1, 'disabled'=>$disabled])->label(false) ?>
                                    <?= $form->field($model, 'client_attendee['.$client_counter.'][attendee_db_id]')->hiddenInput(['maxlength' => true, 'disabled'=>$disabled, 'value'=>$clientMainAttendee_db_id])->label(false) ?>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'client_attendee['.$client_counter.'][client_attendee_title]')->widget(\kartik\select2\Select2::classname(), [
                                            'data' => Yii::$app->smHelper->getTitleArr(),
                                            'options' => ['placeholder' => 'Select...'],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'initialize' => true,
                                                'disabled' => $disabled,
                                            ],
                                        ])->label('Title');
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'client_attendee['.$client_counter.'][client_attendee_id]')->widget(\kartik\select2\Select2::classname(), [
                                            'data' => $clientContactsArr,
                                            'options' => ['id'=>'main-client-attendee','placeholder' => 'Select...', 'class'=>'client_main_attendee_cls'],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => $disabled,
                                            ],
                                        ])->label('Attendee Name');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <?php $client_counter++; ?>

                        <section class="card c_attendee_area" style="border-top:2px solid #00897B;">
                            <header class="card-header">
                                <h2 class="card-title"><strong>Client other attendees</strong></h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool add-client-attendee btn-info text-dark"
                                        title="Add client attendee">
                                        Add client other attendee
                                    </button>
                                </div>
                            </header>
                            <div class="card-body c_attendee_area_body">
                                <?php 
                                    if(isset($model->clientOtherAttendees) && $model->clientOtherAttendees<>null){
                                        foreach($model->clientOtherAttendees as $key => $otherAttendee){

                                        $model->client_attendee[$client_counter]['client_attendee_title'] = $otherAttendee->client_attendee_title;
                                        $model->client_attendee[$client_counter]['client_attendee_id'] = $otherAttendee->sales_and_marketing_attendees_id;
                                ?>

                                <section class="card" style="border-top:2px solid #00897B;">
                                    <div class="card-body">
                                        <div class="row">
                                            <?= $form->field($model, 'client_attendee['.$client_counter.'][client_attendee_type]')->hiddenInput(['maxlength' => true, 'disabled'=>$disabled, 'value'=>0])->label(false) ?>
                                            <?= $form->field($model, 'client_attendee['.$client_counter.'][attendee_db_id]')->hiddenInput(['maxlength' => true, 'disabled'=>$disabled, 'value'=>$otherAttendee->id])->label(false) ?>
                                            <div class="col-sm-6">
                                                <?php
                                                    echo $form->field($model, 'client_attendee['.$client_counter.'][client_attendee_title]')->widget(\kartik\select2\Select2::classname(), [
                                                        'data' => Yii::$app->smHelper->getTitleArr(),
                                                        'options' => ['placeholder' => 'Select...'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true,
                                                            'initialize' => true,
                                                            'disabled' => $disabled,
                                                        ],
                                                    ])->label('Title');
                                                    ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                    echo $form->field($model, 'client_attendee['.$client_counter.'][client_attendee_id]')->widget(\kartik\select2\Select2::classname(), [
                                                        'data' => $clientContactsArr,
                                                        'options' => ['placeholder' => 'Select...', 'class'=>'client_main_attendee_cls'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true,
                                                            'disabled' => $disabled,
                                                        ],
                                                    ])->label('Attendee Name');
                                                    ?>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <?php
                                    $client_counter++;
                                    }
                                }
                                ?>
                            </div>
                        </section>



                    </div>
                </section>



                <!-- start start windmills addendee -->
                <?php $wm_counter = 0; ?>

                <section class="card" style="border-top:2px solid #455A64;">
                    <header class="card-header">
                        <h2 class="card-title"><strong>Windmills attendees</strong></h2>
                    </header>
                    <div class="card-body">

                        <section class="card" style="border-top:2px solid #455A64;">
                            <header class="card-header">
                                <h2 class="card-title"><strong>Windmills main attendee</strong></h2>
                            </header>
                            <div class="card-body">

                                <div class="row">
                                    <?= $form->field($model, 'wm_attendee['.$wm_counter.'][wm_attendee_type]')->hiddenInput(['maxlength' => true, 'disabled'=>$disabled, 'value'=>1])->label(false) ?>
                                    <?= $form->field($model, 'wm_attendee['.$wm_counter.'][attendee_db_id]')->hiddenInput(['maxlength' => true, 'disabled'=>$disabled, 'value'=>$wmMainAttendee_db_id])->label(false) ?>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'wm_attendee['.$wm_counter.'][wm_attendee_title]')->widget(\kartik\select2\Select2::classname(), [
                                            'data' => Yii::$app->smHelper->getTitleArr(),
                                            'options' => ['placeholder' => 'Select ...',],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => $disabled,
                                            ],
                                        ])->label('Title');
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'wm_attendee['.$wm_counter.'][wm_attendee_id]')->widget(\kartik\select2\Select2::classname(), [
                                            'data' => Yii::$app->smHelper->getAttendeesArr(),
                                            'options' => ['placeholder' => 'Select ...',],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => $disabled,
                                            ],
                                        ])->label('Attendee Name');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </section>


                        <?php $wm_counter++; ?>

                        <section class="card wm_attendee_area" style="border-top:2px solid #455A64;">
                            <header class="card-header">
                                <h2 class="card-title"><strong>Windmills other attendees</strong></h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool add-wm-attendee btn-info text-dark"
                                        title="Add Windmills attendee">
                                        Add Windmills other attendee
                                    </button>
                                </div>
                            </header>
                            <div class="card-body wm_attendee_area_body">
                                <?php 
                                    if(isset($model->wmOtherAttendees) && $model->wmOtherAttendees<>null){
                                        foreach($model->wmOtherAttendees as $key => $otherAttendee){

                                        $model->wm_attendee[$wm_counter]['wm_attendee_title'] = $otherAttendee->client_attendee_title;
                                        $model->wm_attendee[$wm_counter]['wm_attendee_id'] = $otherAttendee->sales_and_marketing_attendees_id;
                                ?>


                                <section class="card" style="border-top:2px solid #455A64;">
                                    <div class="card-body">

                                        <div class="row">
                                            <?= $form->field($model, 'wm_attendee['.$wm_counter.'][wm_attendee_type]')->hiddenInput(['maxlength' => true, 'disabled'=>$disabled, 'value'=>0])->label(false) ?>
                                            <?= $form->field($model, 'wm_attendee['.$wm_counter.'][attendee_db_id]')->hiddenInput(['maxlength' => true, 'disabled'=>$disabled, 'value'=>$otherAttendee->id])->label(false) ?>
                                            <div class="col-sm-6">
                                                <?php
                                                        echo $form->field($model, 'wm_attendee['.$wm_counter.'][wm_attendee_title]')->widget(\kartik\select2\Select2::classname(), [
                                                            'data' => Yii::$app->smHelper->getTitleArr(),
                                                            'options' => ['placeholder' => 'Select ...',],
                                                            'pluginOptions' => [
                                                                'allowClear' => true,
                                                                'disabled' => $disabled,
                                                            ],
                                                        ])->label('Title');
                                                        ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                        echo $form->field($model, 'wm_attendee['.$wm_counter.'][wm_attendee_id]')->widget(\kartik\select2\Select2::classname(), [
                                                            'data' => Yii::$app->smHelper->getAttendeesArr(),
                                                            'options' => ['placeholder' => 'Select ...',],
                                                            'pluginOptions' => [
                                                                'allowClear' => true,
                                                                'disabled' => $disabled,
                                                            ],
                                                        ])->label('Attendee Name');
                                                        ?>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <?php
                                    $wm_counter++;
                                    }
                                }
                                ?>
                            </div>
                        </section>



                    </div>
                </section>





                <!-- end start windmills addendee -->





                <div class="row">
                    <div class="col-4">
                        <?= $form->field($model, 'date',['template'=>'
                        {label}
                        <div class="input-group date" id="salesandmarketing-date" data-target-input="nearest">
                        {input}
                        <div class="input-group-append" data-target="#salesandmarketing-date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                        </div>
                        {hint}{error}
                        '])->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'time', ['template' => '
                        {label}
                        <div class="input-group date" style="display: flex" id="salesandmarketing-time" data-target-input="nearest">
                        {input}
                        <div class="input-group-append" data-target="#salesandmarketing-time" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                        </div>
                        {hint}{error}
                        '])->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-4">
                        <?= $form->field($model, 'meeting_end_time', ['template' => '
                            {label}
                            <div class="input-group date" style="display: flex" id="salesandmarketing-meeting_end_time" data-target-input="nearest">
                            {input}
                            <div class="input-group-append" data-target="#salesandmarketing-meeting_end_time" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            </div>
                            {hint}{error}
                        '])->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?php
                        echo $form->field($model, 'meeting_interface')->widget(Select2::classname(), [
                            'data' => Yii::$app->smHelper->getMeetingInterfaceArr(),
                            'options' => ['placeholder' => 'Select...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'disabled' => $disabled,
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-4">
                        <?php
                        echo $form->field($model, 'purpose')->widget(Select2::classname(), [
                            'data' => Yii::$app->smHelper->getPurposeArr(),
                            'options' => ['placeholder' => 'Select...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'disabled' => $disabled,
                            ],
                        ]);
                        ?>
                    </div>

                    <div class="col-4">
                        <?php
                        echo $form->field($model, 'meeting_status')->widget(Select2::classname(), [
                            'data' => Yii::$app->smHelper->getMeetingStatusArr(),
                            'options' => ['id'=>'meeting-status'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'disabled' => $disabled,
                            ],
                        ]);
                        ?>
                    </div>
                </div>

                <div
                    class="row reason-row <?= ($model->meeting_status == 2 || $model->meeting_status == 3) ? '' : ' d-none' ?>">
                    <div class="col-12 postpone-reason <?= ($model->meeting_status == 2) ? '' : ' d-none' ?>">
                        <?= $form->field($model, 'postpone_reason')->textarea(['rows' => '2', 'disabled'=>$disabled, 'required'=>false]) ?>
                    </div>
                    <div class="col-12 rejected-reason  <?= ($model->meeting_status == 3) ? '' : ' d-none' ?>">
                        <?= $form->field($model, 'rejected_reason')->textarea(['rows' => '2', 'disabled'=>$disabled, 'required'=>false]) ?>
                    </div>
                </div>




                <div class="row">
                    <div class="col-6">
                        <?= $form->field($model, 'meeting_place')->textInput(['maxlength' => true, 'readonly'=>true, 'id'=>'meeting_place']) ?>
                    </div>
                </div>

                <div class="row">
                    <?= $form->field($model, 'searchInput')->textInput(['maxlength' => true, 'class'=>'my-1 form-control' , 'id'=>'searchInput', 'value'=>($model->meeting_place<>null) ? $model->meeting_place : ''])->label(false) ?>
                    <div class="col-6">
                        <div class="map" id="map" style="width: 100%; height: 400px;"></div>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'location')->hiddenInput(['maxlength' => true, 'readonly'=>true, 'id'=>'location', 'class'=>'my-2 form-control'])->label(false) ?>
                        <?= $form->field($model, 'location_url')->textInput(['maxlength' => true, 'readonly'=>true, 'id'=>'locationUrl', 'class'=>'my-2 form-control'])->label("Map Url") ?>

                        <?= $form->field($model, 'lat')->textInput(['maxlength' => true, 'readonly'=>true, 'id'=>'lat'])->label("Longitude") ?>

                        <?= $form->field($model, 'lng')->textInput(['maxlength' => true, 'readonly'=>true, 'id'=>'lng'])->label("Latitude") ?>
                    </div>
                </div>
            </div>
        </section>





        <section class="card" style="border-topp:2px solid #455A64;">
            <header class="card-header bg-secondary">
                <h2 class="card-title"><strong>Enter Client Strategic Details</strong></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'client_priority')->dropDownList(Yii::$app->smHelper->getClientPriorityArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'number_of_total_properties_client_owns')->textInput(['maxlength' => true,])->label() ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'senisitivity')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'good_bad_client')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'date_of_birth_incorporation',['template'=>'
                        {label}
                        <div class="input-group dob_incorporation" id="salesandmarketing-dob_incorporation" data-target-input="nearest">
                        {input}
                        <div class="input-group-append" data-target="#salesandmarketing-dob_incorporation" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                        </div>
                        {hint}{error}
                        '])->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'wedding_date',['template'=>'
                        {label}
                        <div class="input-group weding_date" id="salesandmarketing-weding_date" data-target-input="nearest">
                        {input}
                        <div class="input-group-append" data-target="#salesandmarketing-weding_date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                        </div>
                        {hint}{error}
                        '])->textInput(['maxlength' => true])?>
                    </div>
                </div>
            </div>
        </section>






        <?php if($model->meeting_status == 4){ ?>
        <section class="card" style="border-topp:2px solid #455A64;">
            <header class="card-header bg-info">
                <h2 class="card-title"><strong>Enter Client Documentary Requirements</strong></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'services_required')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'frequency_of_service_required')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'others_firms_client_have_worked_with')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'send_corporate_profile')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'send_fee_tariff')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'arrange_recent_market_research_presentation')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'send_market_research_reports_newsletter')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'arrange_nda')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'sign_client_nda')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'arrange_sla')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'sign_client_sla')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                </div>
            </div>
        </section>


        <section class="card" style="border-topp:2px solid #455A64;">
            <header class="card-header bg-warning">
                <h2 class="card-title"><strong>Enter Client Acceptance Status</strong></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'is_client_happy')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'was_our_last_valuation_amount_acceptable')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'is_fee_acceptable')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'is_tat_acceptable')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                </div>
            </div>
        </section>


        <section class="card" style="border-topp:2px solid #455A64;">
            <header class="card-header bg-dark">
                <h2 class="card-title"><strong>Actions</strong></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'arrange_meeting_for_ceo')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'arrange_meeting_for_chairman')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'deliver_business_cards_to_crm_team')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'procure_through_portal')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'schedule_next_follow_up_by_meeting')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'schedule_next_follow_up_by_call_required')->dropDownList(Yii::$app->smHelper->getDocAllowArr(),['prompt' => 'Select...']) ?>
                    </div>
                    <div class="col-3" style="margin-top: 33px;">
                        <!-- checked="checked" -->
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="feedbackEmailStatus" name="SalesAndMarketing[feedback_email_status]" <?= ($model->feedback_email_status=='on') ? 'checked' : '' ?>>
                          <input type="hidden" id="feedbackEmailValue" name="SalesAndMarketing[feedback_email_value]" value="<?= $model->feedback_email_value ?>">
                          <label for="feedbackEmailStatus" class="custom-control-label">Send Email</label>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="card" style="border-topp:2px solid #455A64;">
            <header class="card-header bg-success">
                <h2 class="card-title"><strong>Travel Details</strong></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-4">
                        <?= $form->field($model, 'starting_km')->textInput() ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'end_km')->textInput() ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'parking_fee')->textInput() ?>
                    </div>
                </div>
            </div>
            <?php $atch_count = 0; ?>

            <section class="card mx-4" style="border-top:2px solid #FFA500;">
                <header class="card-header">
                    <h2 class="card-title"><strong>Attach Km Photos</strong></h2>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool add-km-image btn-warning text-dark"
                            title="Add km Image">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                </header>
                <div class="card-body">
                    <div class="row" id="km-table">
                        <?php
                            if($model->kmImages<>null){
                                foreach($model->kmImages as $key => $image){
                            ?>
                            <div class="col-2 my-2 upload-docs" id="image-row<?=$atch_count?>">
                                <div class="form-group">
                                    <a href="javascript:;" id="upload-document<?= $atch_count ?>"
                                        data-uploadid=<?= $atch_count ?>  data-toggle="tooltip"
                                        class="img-thumbnail open-img-window" title="Upload Document">

                                        <img src="<?= $image->attachment ?>" alt=""
                                            title="" data-placeholder="no_image.png" />
                                    </a>
                                    <a href="<?= $image->attachment ?>" class="mx-2" target="_blank">
                                        <i class="fa fa-eye text-primary"></i>
                                    </a>
                                    <input type="hidden"
                                        name="SalesAndMarketing[km_images][<?= $atch_count ?>][attachment]"
                                        id="input-attachment<?=$atch_count?>" 
                                        value="<?= $image->attachment ?>" />
                                    <input type="hidden"
                                        name="SalesAndMarketing[km_images][<?= $atch_count ?>][db_id]" 
                                        value="<?= $image->id ?>" />
                                </div>
                            </div>
                            <?php
                                $atch_count++;
                                }
                            }  
                        ?>
                    </div>
                </div>
            </section>

            <section class="card mx-4" style="border-top:2px solid #12ae2a;">
                <header class="card-header">
                    <h2 class="card-title"><strong>Attach Parking Photos</strong></h2>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool add-parking-image btn-info text-dark"
                            title="Add Parking Image">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                </header>

                <div class="card-body">
                
                
                    <div class="row" id="parking-table">
                        <?php
                            if($model->parkingImages<>null){
                                foreach($model->parkingImages as $key => $image){
                            ?>
                            <div class="col-2 my-2 upload-docs" id="image-row<?=$atch_count?>">
                                <div class="form-group">
                                    <a href="javascript:;" id="upload-document<?= $atch_count ?>"
                                        data-uploadid=<?= $atch_count ?>  data-toggle="tooltip"
                                        class="img-thumbnail open-img-window" title="Upload Document">

                                        <img src="<?= $image->attachment ?>" alt=""
                                            title="" data-placeholder="no_image.png" />
                                    </a>
                                    <a href="<?= $image->attachment ?>" class="mx-2" target="_blank">
                                        <i class="fa fa-eye text-primary"></i>
                                    </a>
                                    <input type="hidden"
                                        name="SalesAndMarketing[parking_images][<?= $atch_count ?>][attachment]"
                                        id="input-attachment<?=$atch_count?>" 
                                        value="<?= $image->attachment ?>" />
                                    <input type="hidden"
                                        name="SalesAndMarketing[parking_images][<?= $atch_count ?>][db_id]" 
                                        value="<?= $image->id ?>" />
                                </div>
                            </div>
                            <?php
                                $atch_count++;
                                }
                            }  
                        ?>
                    </div>

                </div>
            </section>


        </section>
        <?php } ?>




        <?php
        if(Yii::$app->menuHelperFunction->checkActionAllowed('Verified')){
            echo StatusVerified::widget([ 'model' => $model, 'form' => $form ]);
        }
        ?>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php 
        if($model<>null && $model->id<>null){
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => get_class($model),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>



<!-- my account  -->
<!-- <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs&callback=initAutocomplete&libraries=places&v=weekly"
    async defer>
</> -->


<!-- new account -->
<!-- <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALpee00P4fIoS-D7OKN6Itq59jrj3_V9M&callback=initAutocomplete&libraries=places&v=weekly" async defer>
</script> -->


<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" /> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script> -->

<!-- windmills account  -->
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs&callback=initAutocomplete&libraries=places&v=weekly"
    async defer>
</script>



<?php

$this->registerJs('

$("#feedbackEmailStatus").click(function() {
    if ($(this).is(":checked")) {
      $("#feedbackEmailValue").val("1");
    } else {
      $("#feedbackEmailValue").val("0");
    }
});

var client_counter = "'.$client_counter.'";
var wm_counter = "'.$wm_counter.'";




    $("#salesandmarketing-date, #salesandmarketing-meeting_end_date, #salesandmarketing-weding_date, #salesandmarketing-dob_incorporation").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "YYYY-MM-DD"
    });
    
    $("#salesandmarketing-time,#salesandmarketing-meeting_end_time").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "HH:mm"
    });
  
    $("#salesandmarketing-time").on("change.datetimepicker", ({date, oldDate}) => {              
       
        let newDate = date.add(1, "hour");
        new_time = newDate.format("HH:mm");
        console.log(new_time)
        
        // $("#salesandmarketing-meeting_end_time").val(new_time);
        $("#salesandmarketing-meeting_end_time").datetimepicker("date", new_time);
        
        
       
    })  
    
    
    
    $("body").on("change", "#client_id", function (e) {
        client_id = $(this).val();
        if(client_id > 0){
            call_url = "'.\yii\helpers\Url::to(['suggestion/get-client-data']).'?id="+client_id;
            $.ajax({
                url: call_url,
                dataType: "html",
                success: function(data) {
                    data = JSON.parse(data);
                    
                    $("#main-client-attendee").html(data.client_contacts);
                    $("#salesandmarketing-client_priority").val(data.client.client_priority);
                    $("#salesandmarketing-number_of_total_properties_client_owns").val(data.client.number_of_total_properties);
                    $("#salesandmarketing-senisitivity").val(data.client.sensitivity);
                    $("#salesandmarketing-good_bad_client").val(data.client.good_bad_client);
                    $("#salesandmarketing-date_of_birth_incorporation").val(data.client.dob_date);
                    $("#salesandmarketing-wedding_date").val(data.client.wedding_date);

                    updateLocation(data.client.address, data.client.lat, data.client.lng, data.client.location_url)
                },
                error: bbAlert
            });
        }
    });
    
    $("body").on("click", ".add-client-attendee", function (e) {
        client_id = $("#client_id").val();
        if(client_id > 0){
            call_url = "'.\yii\helpers\Url::to(['suggestion/get-client-attendee']).'?id="+client_id;
            $.ajax({
                url: call_url,
                data: {client_counter:client_counter},
                dataType: "html",
                success: function(data) {
                    data = JSON.parse(data);
                    $(".c_attendee_area .c_attendee_area_body").append(data.client_attend_card);
                    client_counter++;                    
                },
                error: bbAlert
            });
        }else{
            Swal.fire({
                title: "Warning",
                html: "<b>Please Select Client!</b>",
                icon: "warning",
                timer: 2000,
                showConfirmButton: false,
                timerProgressBar: true
              }).then((result) => {
                // Do something after the swal is closed (if needed)
              })
        }
    });
    


    $("body").on("click", ".add-wm-attendee", function (e) {
        
            call_url = "'.\yii\helpers\Url::to(['suggestion/get-wm-attendee']).'";
            $.ajax({
                url: call_url,
                data: {wm_counter:wm_counter},
                dataType: "html",
                success: function(data) {
                    data = JSON.parse(data);
                    console.log(data)
                    $(".wm_attendee_area .wm_attendee_area_body").append(data.wm_attend_card);
                    wm_counter++;                    
                },
                error: bbAlert
            });
    });

        

    $("body").on("click", ".wm-delete-btn", function (e) {
        delete_section_id = $(this).attr("data-delid");
        $("#windmills-other-attendees-"+delete_section_id).remove();
    });

    $("body").on("click", ".client-delete-btn", function (e) {
        delete_section_id = $(this).attr("data-delid");
        $("#client-other-attendees-"+delete_section_id).remove();
    });
        



    $("body").on("change", "#meeting-status", function (e) {
        if( $(this).val() == 2 ){
            $(".reason-row").removeClass("d-none");
            $(".postpone-reason").removeClass("d-none");
            $(".rejected-reason").addClass("d-none");
        }
        else if( $(this).val() == 3 ){
            $(".reason-row").removeClass("d-none");
            $(".rejected-reason").removeClass("d-none");
            $(".postpone-reason").addClass("d-none");
        }
        else{
            $(".reason-row").addClass("d-none");
            $(".postpone-reason").addClass("d-none");
            $(".rejected-reason").addClass("d-none");
        }
    });




    var atch_count = "'.$atch_count.'";

    $("body").on("click", ".add-km-image", function (e) {
        
        call_url = "'.\yii\helpers\Url::to(['suggestion/getkmimagehtml']).'";
        $.ajax({
            url: call_url,
            data: {atch_count:atch_count},
            dataType: "html",
            success: function(data) {
                data = JSON.parse(data);
                console.log(data)
                $("#km-table").append(data.col);
                atch_count++;                    
            },
            error: bbAlert
        });
    });


    $("body").on("click", ".add-parking-image", function (e) {
        
        call_url = "'.\yii\helpers\Url::to(['suggestion/getparkingimagehtml']).'";
        $.ajax({
            url: call_url,
            data: {atch_count:atch_count},
            dataType: "html",
            success: function(data) {
                data = JSON.parse(data);
                console.log(data)
                $("#parking-table").append(data.col);
                atch_count++;                    
            },
            error: bbAlert
        });
    });

    $("body").on("click", ".open-img-window", function (e) {
        target_id = $(this).attr("data-uploadid");
        uploadAttachment(target_id)
    });



































        
    function initAutocomplete(){
        initialize();
    }
        
        
    var map, marker, geocoder, infowindow;
    
    function initialize() {
        
        var latdefault = 25.276987;
        var lngdefault = 55.296249;
        
        var client_lat = $("#lat").val();
        var client_lng = $("#lng").val();
        
        if(client_lat != ""){
            latdefault = parseFloat(client_lat);
        }
        if(client_lng != ""){
            lngdefault = parseFloat(client_lng);
        }
        
        var latlng = new google.maps.LatLng(latdefault,lngdefault);
        
        map = new google.maps.Map(document.getElementById("map"), {
            center: latlng,
            zoom: 13
        });
        marker = new google.maps.Marker({
            map: map,
            position: latlng,
            draggable: false,
            anchorPoint: new google.maps.Point(0, -29)
        });
        var input = document.getElementById("searchInput");
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        geocoder = new google.maps.Geocoder();
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo("bounds", map);
        infowindow = new google.maps.InfoWindow();
        
        autocomplete.addListener("place_changed", function() {            
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocompletes returned place contains no geometry");
                return;
            }
            
            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            const locationUrl = `https://www.google.com/maps/search/?api=1&query=${place.name},${place.geometry.location.lat()},${place.geometry.location.lng()}`;
            // console.log(locationUrl)
            bindDataToForm(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng(), locationUrl);
            infowindow.setContent(place.formatted_address);
            infowindow.open(map, marker);
            
        });
        
        // this function will work on marker move event into map
        google.maps.event.addListener(marker, "dragend", function() {

            geocoder.geocode({
                "latLng": marker.getPosition()
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        // var locationUrl = `https://www.google.com/maps/search/?api=1&query=${marker.getPosition().lat()},${marker.getPosition().lng()}`;
                        // console.log(locationUrl)
                        bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });
    }
    
    function bindDataToForm(address, lat, lng, locationUrl){
        document.getElementById("location").value = address;
        document.getElementById("lat").value = lat;
        document.getElementById("lng").value = lng;
        document.getElementById("meeting_place").value = address;
        document.getElementById("searchInput").value = address;
        document.getElementById("locationUrl").value = locationUrl;
    }
    
    function updateLocation(address, lat, lng, locationUrl) {
        
        var latlng = new google.maps.LatLng(lat, lng);
        marker.setPosition(latlng);
        map.setCenter(latlng);
        bindDataToForm(address, lat, lng, locationUrl);
        
    }
    
    window.addEventListener("load", initialize);
        
');
?>


<script>
var uploadAttachment = function(attachmentId) {

    $('#form-upload').remove();
    $('body').prepend(
        '<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>'
    );

    $('#form-upload input[name=\'file\']').trigger('click');

    if (typeof timer != 'undefined') {
        clearInterval(timer);
    }

    timer = setInterval(function() {
        if ($('#form-upload input[name=\'file\']').val() != '') {
            clearInterval(timer);

            $.ajax({
                url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                type: 'post',
                dataType: 'json',
                data: new FormData($('#form-upload')[0]),
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('#upload-document' + attachmentId + ' img').hide();
                    $('#upload-document' + attachmentId + ' i').replaceWith(
                        '<i class="fa fa-circle-o-notch fa-spin"></i>');
                    $('#upload-document' + attachmentId).prop('disabled', true);
                },
                complete: function() {
                    $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                    $('#upload-document' + attachmentId).prop('disabled', false);
                    $('#upload-document' + attachmentId + ' img').show();
                },
                success: function(json) {
                    if (json['error']) {
                        alert(json['error']);
                    }

                    if (json['success']) {
                        console.log(json);


                        console.log(json.file.href);
                        var str1 = json['file'].href;
                        var str2 = ".pdf";
                        var str3 = ".doc";
                        var str4 = ".docx";
                        var str5 = ".xlsx";
                        var str6 = ".xls";
                        if (str1.indexOf(str2) != -1) {
                            $('#upload-document' + attachmentId + ' img').prop('src',
                                "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                        } else if (str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1
                            .indexOf(str5) != -1 || str1.indexOf(str6) != -1) {
                            $('#upload-document' + attachmentId + ' img').prop('src',
                                "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                        } else {
                            $('#upload-document' + attachmentId + ' img').prop('src', json[
                                'file'].href);
                        }
                        $('#input-attachment' + attachmentId).val(json['file'].href);
                        $('#input-attachment-eye' + attachmentId).attr("href", json['file'].href);


                        /*  $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                        $('#input-attachment' + attachmentId).val(json['file'].href);*/
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    }, 500);
}
</script>