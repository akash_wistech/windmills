<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use app\assets\SortBootstrapAsset;
SortBootstrapAsset::register($this);

?>

<style>
/* .dataTable th {
    color: #0056b3;
    font-size: 14px;
    text-align: left !important;
    padding-right: 30px !important;
}

.dataTable td {
    font-size: 16px;
    text-align: right;
    padding-right: 50px;
}

.content-header h1 {
    font-size: 16px !important;

}

.content-header .row {
    margin-bottom: 0px !important;
}

.content-header {
    padding: 4px !important;
} */


/* table {
  border-collapse: collapse;
}
td, th {
  height: 3px !important;
} */

.dataTables_wrapper tbody td {
  height: 5px;
  padding: 5px;
}

.dataTable th {
    color: #0056b3;
    font-size: 15px;
    text-align: left !important;
    padding-right: 30px !important;
}
.dataTable td {
    font-size: 15px;
    text-align: left;
    /* padding-top: 3px; */
}
/* 
.content-header h1 {
    font-size: 16px !important;
} */

/* .content-header .row {
    margin-bottom: 0px !important;
} */

/* .content-header {
    padding: 0px !important;
} */


th:nth-child(1) {
    min-width: 90px;
    max-width: 90px;
}
th:nth-child(2) {
    min-width: 100px;
    max-width: 100px;
}
th:nth-child(3) {
    min-width: 75px;
    max-width: 75px;
}
th:nth-child(4) {
    min-width: 260px;
    max-width: 260px;
}
th:nth-child(5) {
    min-width: 260px;
    max-width: 260px;
}
th:nth-child(6) {
    min-width: 90px;
    max-width: 90px;
}
th:nth-child(7) {
    min-width: 75px;
    max-width: 75px;
}
th:nth-child(8) {
    min-width: 75px;
    max-width: 75px;
}
</style>

<div class="bank-revenue-index col-12">
    <div class="card card-outline card-info">
        <div class="card-body">
            <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                <thead>
                    <tr>

                        <th class="">WM Refrence
                            <input type="text" class="custom-search-input form-control" placeholder="Reference#">
                        </th>

                        <th class="">Inspection Date
                            <input type="text" class="custom-search-input form-control" placeholder="date">
                        </th>

                        <th class="">Inspection Time
                            <input type="text" class="custom-search-input form-control" placeholder="time">
                        </th>

                        <th class="">Project
                            <input type="text" class="custom-search-input form-control" placeholder="Project/Building">
                        </th>

                        <th class="">Area/Community
                            <input type="text" class="custom-search-input form-control" placeholder="Community">
                        </th>

                        <th class="">Inspector
                            <input type="text" class="custom-search-input form-control" placeholder="Inspector">
                        </th>

                        <th class="">Valuer
                            <input type="text" class="custom-search-input form-control" placeholder="Valuer">
                        </th>

                        <th>Email
                            <input type="text" class="custom-search-input form-control" placeholder="Yes/No">
                        </th>

                        <th></th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($inspections) > 0){
                        foreach($inspections as $inspection){
                    ?>
                    <tr class="active">

                    <td><?= $inspection->valuation->reference_number ?></td>
                    <td><?php echo $inspection->inspection_date ?></td>
                    <td><?= Yii::$app->appHelperFunctions->formatTimeAmPm($inspection->inspection_time) ?></td>
                    <td><?= $inspection->valuation->building->title ?></td>
                    <td><?= $inspection->valuation->building->communities->title ?></td>
                    <td><?= (isset($inspection->inspection_officer) && ($inspection->inspection_officer <> null))? ($inspection->inspectorData->lastname) : '' ?></td>
                    <td><?= (isset($inspection->valuation->service_officer_name) && ($inspection->valuation->service_officer_name <> null))? ($inspection->valuation->approver->lastname): '' ?></td>
                    <td style="text-align: center;"><?= Yii::$app->appHelperFunctions->getEmailStatusLable()[$inspection->email_reminder_status] ?></td>



                    <td class="noprint actions">
                        <?php if($inspection->email_reminder_status<>1){ ?>
                        <div class="btn-group flex-wrap">
                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                data-toggle="dropdown">
                                <span class="caret"></span>
                            </button>
                            <div class="dropdown-menu" role="menu">
                                    <a class="dropdown-item text-1"
                                        href="<?= yii\helpers\Url::to(['valuation/inspection-reminder-email','inspection_id'=>$inspection->id]) ?>"
                                        title="send email" data-pjax="0">
                                        <i class="fa fa-envelope text-primary mx-2 pb-0"></i>send email
                                    </a>
                            </div>
                        </div>
                        <?php } ?>
                    </td>


                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>




<?php
        $this->registerJs('
        
        var dataTable = $("#bank-revenue").DataTable({
            order: [[1, "desc"]],
            pageLength: 25,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            columnDefs: [{
                targets: -1,  // The last column index
                orderable: false  // Disable sorting on the last column
            }]
        });
        
        $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
        });
        
        
        $("#bank-revenue_filter").css({
            "display":"none",
        });
        
        $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
        });
        
        // Move the length menu to the table footer
        // var lengthMenu = $(".dataTables_length");
        // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");
        
        // Create a custom footer element
        var customFooter = $("<div class=\"custom-footer\"></div>");
        
        // Move the length menu to the custom footer
        var lengthMenu = $(".dataTables_length");
        lengthMenu.detach().appendTo(customFooter);
        
        // Append the custom footer to the DataTable wrapper
        $("#bank-revenue_wrapper").append(customFooter);
        
        
        
        ');
        ?>