<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SalesAndMarketing */

$this->title = 'Meeting Schedule: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sales And Marketings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="sales-and-marketing-update">
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>
</div>
<!-- 
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs&callback=initAutocomplete&libraries=places&v=weekly">
</script> -->

<?php
    
    $this->registerJs('
    
    
    
    // function initialize() {

    //     var latdefault = 25.276987;
    //     var lngdefault = 55.296249;

    //     var client_lat = $("#lat").val();
    //     var client_lng = $("#lng").val();

    //     if(client_lat != ""){
    //         latdefault = parseFloat(client_lat);
    //     }
    //     if(client_lng != ""){
    //         lngdefault = parseFloat(client_lng);
    //     }

    //     console.log(latdefault)
    //     console.log(lngdefault)
        
    //     var latlng = new google.maps.LatLng(latdefault,lngdefault);
    //     var map = new google.maps.Map(document.getElementById("map"), {
    //         center: latlng,
    //         zoom: 13
    //     });
    //     var marker = new google.maps.Marker({
    //         map: map,
    //         position: latlng,
    //         draggable: true,
    //         anchorPoint: new google.maps.Point(0, -29)
    //     });
        
    //     var input = document.getElementById("searchInput");
    //     map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    //     var geocoder = new google.maps.Geocoder();
    //     var autocomplete = new google.maps.places.Autocomplete(input);
    //     autocomplete.bindTo("bounds", map);
    //     var infowindow = new google.maps.InfoWindow();
        
    //     autocomplete.addListener("place_changed", function() {
    //         infowindow.close();
    //         marker.setVisible(false);
    //         var place = autocomplete.getPlace();
    //         if (!place.geometry) {
    //             window.alert("Autocompletes returned place contains no geometry");
    //             return;
    //         }
            
    //         // If the place has a geometry, then present it on a map.
    //         if (place.geometry.viewport) {
    //             map.fitBounds(place.geometry.viewport);
    //         } else {
    //             map.setCenter(place.geometry.location);
    //             map.setZoom(17);
    //         }
            
    //         marker.setPosition(place.geometry.location);
    //         marker.setVisible(true);
            
    //         bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
            
    //         infowindow.setContent(place.formatted_address);
    //         infowindow.open(map, marker);
            
    //     });

    //     // this function will work on marker move event into map
    //     google.maps.event.addListener(marker, "dragend", function() {
    //         geocoder.geocode({"latLng": marker.getPosition()}, function(results, status) {
    //             if (status == google.maps.GeocoderStatus.OK) {
    //                 if (results[0]) {
    //                     bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
    //                     infowindow.setContent(results[0].formatted_address);
    //                     infowindow.open(map, marker);
    //                 }
    //             }
    //         });
    //     });
        
    // }

    // function bindDataToForm(address,lat,lng){
    //     document.getElementById("location").value = address;
    //     document.getElementById("lat").value = lat;
    //     document.getElementById("lng").value = lng;
    //     document.getElementById("meeting_place").value = address;
    //     document.getElementById("searchInput").value = address;
    // }

    // // google.maps.event.addDomListener(window, "load", initialize);
    // window.addEventListener("load", initialize);



    
');