<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;



$this->title = 'Meeting Schedules';
$this->params['breadcrumbs'][] = $this->title;
$cardTitle = Yii::t('app', 'List');

// if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
//     $createBtn=true;
// }
// if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
//     $actionBtns.='{view}';
// }
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
    $actionBtns.='{update}';
}
// if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
//     $actionBtns.='{delete}';
// }
$actionBtns.='{reschedules-meeting}';
$actionBtns.='{get-history}';



?>
<div class="sales-and-marketing-index">


    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],

            [
                'headerOptions' => ['style' => 'width:20%'],
                'attribute' => 'client_id',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                        return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->where(['allow_for_valuation'=>1])->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],

            [
                'attribute' => 'date',
                'headerOptions' => ['style' => 'width:10%'],
                'label' => Yii::t('app', 'Meeting start Date'),
                'value' => function ($model) {
                        return date('d F Y', strtotime($model->date));
                },
            ],

            [
                'attribute' => 'time',
                'headerOptions' => ['style' => 'width:10%'],
                'label' => Yii::t('app', 'Meeting start Time'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->formatTimeAmPm($model->time);
                        // return date('H:m', strtotime($model->time));
                },
            ],


            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:12%'],
            //     'attribute' => 'date',
            //     'label' => Yii::t('app', 'Meeting end Date'),
            //     'value' => function ($model) {
            //             return date('d F Y', strtotime($model->meeting_end_date));
            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:12%'],
            //     'attribute' => 'time',
            //     'label' => Yii::t('app', 'Meeting end Time'),
            //     'value' => function ($model) {
            //             return date('H:m', strtotime($model->meeting_end_time));
            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:12%'],
            //     'attribute' => 'meeting_interface',
            //     'label' => Yii::t('app', 'Meeting Interface'),
            //     'value' => function ($model) {
            //             return Yii::$app->smHelper->getMeetingInterfaceArr()[$model->meeting_interface];
            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:12%'],
            //     'attribute' => 'purpose',
            //     'label' => Yii::t('app', 'Purpose'),
            //     'value' => function ($model) {
            //             return Yii::$app->smHelper->getPurposeArr()[$model->purpose];
            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:12%'],
            //     'attribute' => 'calendar_invite',
            //     'label' => Yii::t('app', 'Calendar Invite'),
            //     'value' => function ($model) {
            //             return Yii::$app->smHelper->getCalendarInviteArr()[$model->calendar_invite];
            //     },
            // ],


           

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:12%'],
            //     'attribute' => 'meeting_location_pin',
            //     'label' => Yii::t('app', 'Meeting Location'),
            //     'value' => function ($model) {
            //             return $model->meeting_location_pin;
            //     },
            // ],

            
            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:12%'],
            //     'attribute' => 'attendees',
            //     'label' => Yii::t('app', 'Attendees'),
            //     'value' => function ($model) {
            //             return count($model->smAttendees);
            //     },
            // ],

            // [
            //     'format' => 'raw',
            //     'headerOptions' => ['style' => 'width:200%'],
            //     'attribute' => 'meeting_place',
            //     'label' => Yii::t('app', 'Meeting Place'),
            //     'value' => function ($model) {
            //             return $model->meeting_place;
            //     },
            // ],


            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'meeting_status',
                'label' => Yii::t('app', 'Meeting Status'),
                'value' => function ($model) {
                    return Yii::$app->smHelper->getMeetingStatusLabelArr()[$model->meeting_status];
                },
                'filter' => Yii::$app->smHelper->getMeetingStatusArr()
            ],

            [
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:12%'],
                'attribute' => 'status_verified',
                'label' => Yii::t('app', 'Verification'),
                'value' => function ($model) {
                        return Yii::$app->appHelperFunctions->statusVerifyLabel[$model->status_verified];
                },
                'filter' => Yii::$app->appHelperFunctions->statusVerifyArr
            ],          

            [
                'format' => 'raw',
                'label' => Yii::t('app', 'Windmills Attendees'),
                'value' => function ($model) {
                    return Yii::$app->smHelper->getAttendeesLable($model->wmAttendees, 'primary');
                },
            ],

            [
                'format' => 'raw',
                'label' => Yii::t('app', 'Client Attendees'),
                'value' => function ($model) {
                    return Yii::$app->smHelper->getAttendeesLable($model->clientAllAttendees, 'info');
                },
            ],   
            

            
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
                'contentOptions'=>['class'=>'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
                'buttons' => [
                    // 'view' => function ($url, $model) {
                    //     return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                    //         'title' => Yii::t('app', 'View'),
                    //         'class'=>'dropdown-item text-1',
                    //         'data-pjax'=>"0",
                    //     ]);
                    // },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                            'title' => Yii::t('app', 'Edit'),
                            'class'=>'dropdown-item text-1',
                            'data-pjax'=>"0",
                        ]);
                    },
                    'reschedules-meeting' => function ($url, $model) {
                            return Html::a('<i class="fa fa-history"></i> '.Yii::t('app', 'Re-Schedule'), 'javascript:;', [
                            'title' => Yii::t('app', 'Re-Schedule'),
                            'class'=>'dropdown-item text-1',
                            'target' => '_blank',
                            'onclick'=> "window.open('".Url::toRoute(['sales-and-marketing/create-remeeting', 'parent_id' => $model->id])."', '_blank')",
                        ]);
                    },
                    // 'delete' => function ($url, $model) {
                    //     return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                    //         'title' => Yii::t('app', 'Delete'),
                    //         'class'=>'dropdown-item text-1',
                    //         'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                    //         'data-method'=>"post",
                    //         'data-pjax'=>"0",
                    //     ]);
                    // },
                    'get-history' => function ($url, $model) {
                        // Yii::$app->session->set('model_name', Yii::$app->appHelperFunctions->getModelName());
                        return Html::a('<i class="fa fa-history"></i> '.Yii::t('app', 'History'), 'javascript:;', [
                            'title' => Yii::t('app', 'History'),
                            'class'=>'dropdown-item text-1',
                            'target' => '_blank',
                            'onclick'=> "window.open('".$url."', '_blank')",
                        ]);
                    },
                ],
            ],
        ],
    ]);?>
    <?php CustomPjax::end(); ?>


</div>


