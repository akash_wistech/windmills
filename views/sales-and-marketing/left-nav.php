<?php 
use yii\helpers\Url;
?>


<style>
.fade {
    opacity: 1;
}

.nav-tabs.flex-column .nav-link.active {
    background-color: #007bff;
    color: white;
    font-weight: bold;
}
</style>


<div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">

    <a class="nav-link <?= ($step == 'step_2') ? 'active' : '' ?>"
        href="sales-and-marketing/step_2?id=<?= $model->id; ?>" aria-selected="false">Documents</a>

</div>