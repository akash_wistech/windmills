<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use  app\components\widgets\StatusVerified;
use yii\helpers\Url;


use app\assets\ClientFormAsset;
ClientFormAsset::register($this);

?>



<section class="sales-and-marketing card">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card-body">


        <section class="card" style="border-top:2px solid purple;">
            <header class="card-header">
                <h2 class="card-title"><strong>Schedule Meeting/Zoom/Call</strong></h2>
            </header>
            <div class="card-body">

                <div class="row">
                    <div class="col-4">
                        <?php
                        echo $form->field($model, 'client_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Company::find()->where(['allow_for_valuation'=>1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title'),
                            'options' => ['placeholder' => 'Select...','id'=>'client_id'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'date',['template'=>'
                        {label}
                        <div class="input-group date" id="salesandmarketing-date" data-target-input="nearest">
                        {input}
                        <div class="input-group-append" data-target="#salesandmarketing-date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                        </div>
                        {hint}{error}
                        '])->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'time', ['template' => '
                        {label}
                        <div class="input-group date" style="display: flex" id="salesandmarketing-time" data-target-input="nearest">
                        {input}
                        <div class="input-group-append" data-target="#salesandmarketing-time" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                        </div>
                        {hint}{error}
                        '])->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'meeting_end_date',['template'=>'
                            {label}
                            <div class="input-group date" id="salesandmarketing-meeting_end_date" data-target-input="nearest">
                            {input}
                            <div class="input-group-append" data-target="#salesandmarketing-meeting_end_date" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            </div>
                            {hint}{error}
                        '])->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'meeting_end_time', ['template' => '
                            {label}
                            <div class="input-group date" style="display: flex" id="salesandmarketing-meeting_end_time" data-target-input="nearest">
                            {input}
                            <div class="input-group-append" data-target="#salesandmarketing-meeting_end_time" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            </div>
                            {hint}{error}
                        '])->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?php
                        echo $form->field($model, 'meeting_interface')->widget(Select2::classname(), [
                            'data' => Yii::$app->smHelper->getMeetingInterfaceArr(),
                            'options' => ['placeholder' => 'Select...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-4">
                        <?php
                        echo $form->field($model, 'purpose')->widget(Select2::classname(), [
                            'data' => Yii::$app->smHelper->getPurposeArr(),
                            'options' => ['placeholder' => 'Select...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-4">
                        <?php
                        echo $form->field($model, 'attendees')->widget(Select2::classname(), [
                            'data' => Yii::$app->smHelper->getAttendeesArr(),
                            'options' => ['placeholder' => 'Select...'],
                            'pluginOptions' => [
                                'multiple' => true,
                                'allowClear' => true,
                            ],
                        ]);
                        ?>
                    </div>
                    
                    <!-- <div class="col-4">
                        <?= $form->field($model, 'description')->textarea(['rows' => '2']) ?>
                    </div> -->


                    <!-- <div class="col-4">
                    <?= $form->field($model, 'meeting_location_pin')->textInput(['maxlength' => true]) ?>
                    </div> -->

                    <div class="col-4">
                        <?php
                        // echo $form->field($model, 'calendar_invite')->widget(Select2::classname(), [
                        //         'data' => Yii::$app->smHelper->getCalendarInviteArr(),
                        //         'options' => ['placeholder' => 'Select...'],
                        //         'pluginOptions' => [
                        //         'allowClear' => true
                        //     ],
                        // ]);
                        ?>
                    </div>

                </div>

                <div class="row">
                    <div class="col-6">
                        <?= $form->field($model, 'meeting_place')->textInput(['maxlength' => true, 'readonly'=>true, 'id'=>'meeting_place']) ?>
                    </div>
                </div>

                <div class="row">
                    <?= $form->field($model, 'searchInput')->textInput(['maxlength' => true, 'class'=>'my-1 form-control' , 'id'=>'searchInput', 'value'=>($model->meeting_place<>null) ? $model->meeting_place : ''])->label(false) ?>
                    <div class="col-6">
                        <div class="map" id="map" style="width: 100%; height: 400px;"></div>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'location')->hiddenInput(['maxlength' => true, 'readonly'=>true, 'id'=>'location', 'class'=>'my-2 form-control'])->label(false) ?>
                        <?= $form->field($model, 'location_url')->textInput(['maxlength' => true, 'readonly'=>true, 'id'=>'locationUrl', 'class'=>'my-2 form-control'])->label("Map Url") ?>

                        <?= $form->field($model, 'lat')->textInput(['maxlength' => true, 'readonly'=>true, 'id'=>'lat'])->label("Longitude") ?>

                        <?= $form->field($model, 'lng')->textInput(['maxlength' => true, 'readonly'=>true, 'id'=>'lng'])->label("Latitude") ?>
                    </div>
                </div>

            </div>


        </section>




        <?php
        if(Yii::$app->menuHelperFunction->checkActionAllowed('Verified')){
            echo StatusVerified::widget([ 'model' => $model, 'form' => $form ]);
        }
        ?>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php 
        if($model<>null && $model->id<>null){
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => get_class($model),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>

<!-- my account  -->
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs&callback=initAutocomplete&libraries=places&v=weekly"  async defer>
</script>


<!-- new account -->
<!-- <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALpee00P4fIoS-D7OKN6Itq59jrj3_V9M&callback=initAutocomplete&libraries=places&v=weekly" async defer>
</script> -->



<?php
        
$this->registerJs('

    $("#salesandmarketing-date,#salesandmarketing-meeting_end_date").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "YYYY-MM-DD"
    });
    
    $("#salesandmarketing-time,#salesandmarketing-meeting_end_time").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "HH:mm"
    });
    
    
    
    
    $("body").on("change", "#client_id", function (e) {
        client_id = $(this).val();
        if(client_id > 0){
            call_url = "'.\yii\helpers\Url::to(['suggestion/get-client-data']).'?id="+client_id;
            $.ajax({
                url: call_url,
                dataType: "html",
                success: function(data) {
                    data = JSON.parse(data);
                    updateLocation(data.address, data.lat, data.lng, data.location_url)
                },
                error: bbAlert
            });
        }
    });
        
        
    function initAutocomplete(){
        initialize();
    }
        
        
    var map, marker, geocoder, infowindow;
    
    function initialize() {
        
        var latdefault = 25.276987;
        var lngdefault = 55.296249;
        
        var client_lat = $("#lat").val();
        var client_lng = $("#lng").val();
        
        if(client_lat != ""){
            latdefault = parseFloat(client_lat);
        }
        if(client_lng != ""){
            lngdefault = parseFloat(client_lng);
        }
        
        var latlng = new google.maps.LatLng(latdefault,lngdefault);
        
        map = new google.maps.Map(document.getElementById("map"), {
            center: latlng,
            zoom: 13
        });
        marker = new google.maps.Marker({
            map: map,
            position: latlng,
            draggable: true,
            anchorPoint: new google.maps.Point(0, -29)
        });
        var input = document.getElementById("searchInput");
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        geocoder = new google.maps.Geocoder();
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo("bounds", map);
        infowindow = new google.maps.InfoWindow();
        
        autocomplete.addListener("place_changed", function() {            
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocompletes returned place contains no geometry");
                return;
            }
            
            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            const locationUrl = `https://www.google.com/maps/search/?api=1&query=${place.name},${place.geometry.location.lat()},${place.geometry.location.lng()}`;
            // console.log(locationUrl)
            bindDataToForm(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng(), locationUrl);
            infowindow.setContent(place.formatted_address);
            infowindow.open(map, marker);
            
        });
        
        // this function will work on marker move event into map
        google.maps.event.addListener(marker, "dragend", function() {

            geocoder.geocode({
                "latLng": marker.getPosition()
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        // var locationUrl = `https://www.google.com/maps/search/?api=1&query=${marker.getPosition().lat()},${marker.getPosition().lng()}`;
                        // console.log(locationUrl)
                        bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });
    }
    
    function bindDataToForm(address, lat, lng, locationUrl){
        document.getElementById("location").value = address;
        document.getElementById("lat").value = lat;
        document.getElementById("lng").value = lng;
        document.getElementById("meeting_place").value = address;
        document.getElementById("searchInput").value = address;
        document.getElementById("locationUrl").value = locationUrl;
    }
    
    function updateLocation(address, lat, lng, locationUrl) {
        
        var latlng = new google.maps.LatLng(lat, lng);
        marker.setPosition(latlng);
        map.setCenter(latlng);
        bindDataToForm(address, lat, lng, locationUrl);
        
    }
    
    window.addEventListener("load", initialize);
        
');
?>