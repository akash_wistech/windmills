<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\ClientFormAsset;
ClientFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
$zonesList=[];
if($model->country_id>0){
    $zonesList=Yii::$app->appHelperFunctions->getCountryZoneListArr($model->country_id);
}
$emiratesListArr=Yii::$app->appHelperFunctions->emiratedListArr;
$feeStructureOptions=Yii::$app->appHelperFunctions->feeStructureOptionListArr;
//$feeStructureTypes=Yii::$app->helperFunctions->feeStructureTypeListArr;
$feeStructureTypes='';

// if($model->primaryContact!=null){
//     // echo "<pre>"; print_r($model->primaryContact); echo "</pre>"; die();

//   $model->firstname=$model->primaryContact->firstname;
//   $model->lastname=$model->primaryContact->lastname;
//   $model->job_title=$model->primaryContact->profileInfo!=null && $model->primaryContact->profileInfo->jobTitle!=null ? $model->primaryContact->profileInfo->jobTitle->title : '';
//   $model->job_title_id=$model->primaryContact->profileInfo->job_title_id;
//   $model->mobile=$model->primaryContact->profileInfo->mobile;
//   $model->email=$model->primaryContact->email;
// }

$actionBtns='Edit';

// die('here');



?>

<script>

</script>
<section class="company-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['id'=>'company-form']); ?>
    <div class="hidden">
        <?= $form->field($model, 'job_title_id')->textInput()?>
        <?= $form->field($model, 'lead_score')->textInput()?>
        <?= $form->field($model, 'confidence')->textInput()?>
    </div>
    <?=  $form->field($model, 'existing_valuation_contact_c')->hiddenInput(['value'=> $existing_valuation_contact])->label(false); ?>
    <?=  $form->field($model, 'bilal_contact_c')->hiddenInput(['value'=> $bilal_contact])->label(false); ?>
    <?=  $form->field($model, 'valuation_contact_c')->hiddenInput(['value'=> $valuation_contact])->label(false); ?>
    <?=  $form->field($model, 'prospect_contact_c')->hiddenInput(['value'=>$prospect_contact])->label(false); ?>
    <?=  $form->field($model, 'property_owner_contact_c')->hiddenInput(['value'=> $property_owner_contact])->label(false); ?>
    <?=  $form->field($model, 'inquiry_valuations_contact_c')->hiddenInput(['value'=> $inquiry_valuations_contact])->label(false); ?>
    <?=  $form->field($model, 'icai_contact_c')->hiddenInput(['value'=> $icai_contact])->label(false); ?>
    <?=  $form->field($model, 'broker_contact_c')->hiddenInput(['value'=> $broker_contact])->label(false); ?>
    <?=  $form->field($model, 'developer_contact_c')->hiddenInput(['value'=> $developer_contact])->label(false); ?>
    <?php if(isset($cardTitle)){?>
        <header class="card-header">
            <h2 class="card-title"><?= $cardTitle?></h2>
        </header>
    <?php }?>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'client_type')->dropDownList(yii::$app->quotationHelperFunctions->clienttype)?>
            </div>

        </div>

        <!-- </div>-->
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'start_date',['template'=>'
        {label}
        <div class="input-group date" id="company-d-start_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#company-d-start_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'website')->textInput(['maxlength' => true])?>
                <?=  $form->field($model, 'data_type')->hiddenInput(['value'=> 0])->label(false); ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'country_id')->dropDownList(Yii::$app->appHelperFunctions->countryListArr,['prompt'=>Yii::t('app','Select')])?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'client_reference')->textInput()?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'zone_id')->dropDownList($zonesList)?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'address')->textInput(['maxlength' => true])?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'print_report')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Hard Copy Reports');
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'land_valutaion')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Land Market Value');
                ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'land_age')->textInput(['maxlength' => true,'type' => 'number'])->label('Minimum Age for Land MV')?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'vat')->dropDownList(Yii::$app->appHelperFunctions->getvatArr())->label('Vat for Quotation')?>
            </div>
          <!--  <?php /*if(Yii::$app->user->identity->id == 33 || Yii::$app->user->identity->id == 1) { */?>
                <div class="col-sm-4">
                    <?/*= $form->field($model, 'quickbooks_registration_id')->textInput(['maxlength' => true,'type' => 'number'])->label('QuickBook ID')*/?>
                </div>
            --><?php /*} */?>
            <?php if(isset($model->id) && $model->id > 0) {
                if ($model->client_type == 'bank') {

                    ?>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'relative_discount')->widget(\kartik\select2\Select2::classname(), [
                            'data' => yii::$app->quotationHelperFunctions->relativediscount,
                            'options' => ['placeholder' => 'Select a Relative Discount ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                    </div>
                <?php }
            }?>
            <div class="col-sm-4">
                <?= $form->field($model, 'report_password')->textInput(['maxlength' => true])->label('Report Password')?>
            </div>
            <!--<div class="col-sm-4">
                <?php
/*                echo $form->field($model, 'fee_master_file_vat')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Fee Master file Vat');
                */?>
            </div>-->
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'client_invoice_customer')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Client Require Customer Invoice');
                ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'trn_number')->textInput(['maxlength' => true])->label('TRN')?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'send_monthly_report')->dropDownList([0=>'No', 1=>'Yes'])?>
            </div>

          <!--  <div class="col-sm-4">
                <?/*= $form->field($model, 'send_monthly_quotation_invoice')->dropDownList([0=>'No', 1=>'Yes'])*/?>
            </div>-->
            <?php if(isset($model->id) && $model->id == 1) {
            ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'client_fixed_fee')->textInput(['maxlength' => true,'type' => 'number'])->label('Shaikh Zayed case fixed fee')?>
            </div>
            <?php } ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'nick_name')->textInput(['maxlength' => true])->label('Nick Name')?>
            </div>

        </div>
        <!--
    <?/*= $form->field($model, 'manager_id')->dropDownList(Yii::$app->appHelperFunctions->StaffMemberListArrBusiness,['multiple'=>'multiple'])*/?>
     -->


        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app','<b>Instructing Person Info</b>')?></h2>
            </header>

            <div class="card-body instructing-person-append-section">
                <?php
                $i=0;
                if($model->allContacts!=null){
                    foreach ($model->allContacts as $key => $value) {
                       /* echo "<pre>";
                        print_r($model->allContacts);
                        die;*/
                        // echo "<pre>"; print_r($value); echo "</pre>"; echo "<br>"; die();
                        // echo "<pre>"; print_r($value->firstname); echo "</pre>"; echo "<br>"; die();
                        $jobTitle = $value->profileInfo!=null && $value->profileInfo->jobTitle!=null ? $value->profileInfo->jobTitle->title : '';
                        ?>

                        <section class="card card-outline card-info  append-card">
                            <?php
                            if ($value->profileInfo->primary_contact==1) {?>
                                <header class="card-header bg-success">
                                    <h2 class="card-title"><?= Yii::t('app','<b>Primary Contact</b>')?></h2>
                                </header>
                                <?php
                            }
                            ?>
                            <div class="row mx-2 my-2">
                                <input type="hidden" name="Company[InstructingPersonInfo][<?=$i?>][contant_id]" value="<?= $value->id ?>">
                                <div class="col-sm-4">
                                    <div class="form-group field-company-firstname">
                                        <label class="control-label" for="company-firstname">Firstname</label>
                                        <input type="text" id="company-firstname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][firstname]" value="<?= $value->firstname ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-lastname">
                                        <label class="control-label" for="company-lastname">Lastname</label>
                                        <input type="text" id="company-lastname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][lastname]" value="<?= $value->lastname ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-job_title">
                                        <label class="control-label" for="company-job_title">Job Title</label>
                                        <input type="text" id="company-job_title" class="form-control job-title-cls" name="Company[InstructingPersonInfo][<?=$i?>][job_title]" value="<?= $jobTitle ?>" autocomplete="off">
                                        <input type="hidden" class="job-title-id-field" name="Company[InstructingPersonInfo][<?=$i?>][job_title_id]" value="<?= $value->profileInfo->job_title_id ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-mobile">
                                        <label class="control-label" for="company-mobile">Mobile</label>
                                        <input type="text" id="company-mobile" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][mobile]" value="<?= $value->profileInfo->mobile ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-email">
                                        <label class="control-label" for="company-email">Email</label>
                                        <input type="text" id="company-email" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][email]" value="<?= $value->email ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <?php
                                if ($value->profileInfo->primary_contact!==1) {?>
                                    <div class="col-sm-4 my-2 py-4 text-right">
                                        <button type="button" data-toggle="tooltip" title="Remove" class="btn btn-danger remove-apnd-card"><i class="fa fa-minus-circle"></i></button>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                        </section>

                        <?php
                        $i++;
                    }
                }else{?>
                    <section class="card card-outline card-info  append-card">
                        <div class="row mx-2 my-2">
                            <div class="col-sm-4">
                                <div class="form-group field-company-firstname">
                                    <label class="control-label" for="company-firstname">Firstname</label>
                                    <input type="text" id="company-firstname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][firstname]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-lastname">
                                    <label class="control-label" for="company-lastname">Lastname</label>
                                    <input type="text" id="company-lastname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][lastname]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-job_title">
                                    <label class="control-label" for="company-job_title">Job Title</label>
                                    <input type="text" id="company-job_title" class="form-control job-title-cls" name="Company[InstructingPersonInfo][<?=$i?>][job_title]" autocomplete="off">
                                    <input type="hidden" class="job-title-id-field" name="Company[InstructingPersonInfo][<?=$i?>][job_title_id]" value="<?= $value->profileInfo->job_title_id ?>">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-mobile">
                                    <label class="control-label" for="company-mobile">Mobile</label>
                                    <input type="text" id="company-mobile" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][mobile]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-email">
                                    <label class="control-label" for="company-email">Email</label>
                                    <input type="text" id="company-email" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][email]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php
                    $i++;
                }
                ?>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-sm-10"></div>
                    <div class="col-sm-2 text-right">
                        <button type="button"  data-toggle="tooltip" title="Add" class="btn btn-primary create-instructing-person"><i class="fa fa-plus-circle"></i></button>
                    </div>
                </div>
            </div>
        </section>































      

        <?php
        if($feeStructureTypes!=null){
            foreach($feeStructureTypes as $fstKey=>$fstVal){
                ?>
                <section class="card card-outline card-warning">
                    <header class="card-header">
                        <h2 class="card-title"><?= Yii::t('app','Agreed Fees Structure For {type} Standard Property',['type'=>$fstVal])?></h2>
                    </header>
                    <div class="card-body fee-structure">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th><?= Yii::t('app','Emirate')?></th>
                                <?php foreach($feeStructureOptions as $key=>$val){?>
                                    <th width="15%" class="text-center text-danger"><?= $val?></th>
                                    <th width="10%" class="text-center text-success"><?= Yii::t('app','TAT')?></th>
                                <?php }?>
                            </tr>
                            <?php foreach($emiratesListArr as $key=>$val){?>
                                <tr>
                                    <th><?= $val?></th>
                                    <?php
                                    foreach($feeStructureOptions as $fskey=>$fsval){
                                        $feeStructure=Yii::$app->appHelperFunctions->getCompanyFeeStructureValues($model->id,$fstKey,$key,$fskey);
                                        $feeValue=$feeStructure['fee'];
                                        $tatValue=$feeStructure['tat'];
                                        $model->fee_structure[$fstKey][$key][$fskey]=$feeValue;
                                        $model->tat[$fstKey][$key][$fskey]=$tatValue;
                                        ?>
                                        <td><?= $form->field($model, 'fee_structure['.$fstKey.']['.$key.']['.$fskey.']')->textInput(['maxlength' => true])->label(false)?></td>
                                        <td><?= $form->field($model, 'tat['.$fstKey.']['.$key.']['.$fskey.']')->textInput(['maxlength' => true])->label(false)?></td>
                                    <?php }?>
                                </tr>
                            <?php }?>
                        </table>
                    </div>
                </section>
                <?php
            }
        }
        ?>

        <section class="valuation-form card card-outline card-primary">

            <header class="card-header">
                <h2 class="card-title">CC Emails for Daily Status Reports</h2>
            </header>
            <div class="card-body">

                <table id="attachment" class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td class="text-left">Name</td>
                        <td class="text-left">Email</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $row = 0; ?>
                    <?php



                    if(isset($model->customEmails) && ($model->customEmails <> null)) {
                        foreach ($model->customEmails as $attachment) { ?>
                            <tr id="image-row-attachment-<?php echo $row; ?>">

                                <td>
                                    <input type="text" class="form-control"
                                           name="Company[customEmailsStatus][<?= $row ?>][name]"
                                           value="<?= $attachment->name ?>" placeholder="Name" required/>
                                </td>

                                <td>
                                    <input type="email" class="form-control"
                                           name="Company[customEmailsStatus][<?= $row ?>][email]"
                                           value="<?= $attachment->email ?>" placeholder="Email" required/>
                                </td>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsStatus][<?= $row ?>][id]"
                                       value="<?= $attachment->id ?>" placeholder="Name" required/>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsStatus][<?= $row ?>][type]"
                                       value="<?= $attachment->type ?>" placeholder="Name" required/>

                                <td class="text-left">
                                    <button type="button"
                                            onclick="deleteRow('-attachment-<?= $row ?>', '<?= $attachment->id; ?>', 'attachment')"
                                            data-toggle="tooltip" title="You want to delete Attachment"
                                            class="btn btn-danger"><i
                                                class="fa fa-minus-circle"></i></button>
                                </td>
                            </tr>
                            <?php $row++; ?>
                        <?php }
                    }?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2"></td>
                        <td class="text-left">
                            <button type="button" onclick="addAttachment();" data-toggle="tooltip" title="Add"
                                    class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </section>
        <section class="valuation-form card card-outline card-primary">

            <header class="card-header">
                <h2 class="card-title">CC Emails for Auto Emails</h2>
            </header>
            <div class="card-body">

                <table id="attachment-auto" class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td class="text-left">Name</td>
                        <td class="text-left">Email</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $row_auto = 0; ?>
                    <?php



                    if(isset($model->customEmailsVals) && ($model->customEmailsVals <> null)) {
                        foreach ($model->customEmailsVals as $attachment) { ?>
                            <tr id="image-row-attachment-auto-<?php echo $row_auto; ?>">

                                <td>
                                    <input type="text" class="form-control"
                                           name="Company[customEmailsAuto][<?= $row_auto ?>][name]"
                                           value="<?= $attachment->name ?>" placeholder="Name" required/>
                                </td>

                                <td>
                                    <input type="email" class="form-control"
                                           name="Company[customEmailsAuto][<?= $row_auto ?>][email]"
                                           value="<?= $attachment->email ?>" placeholder="Email" required/>
                                </td>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsAuto][<?= $row_auto ?>][id]"
                                       value="<?= $attachment->id ?>" placeholder="Name" required/>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsAuto][<?= $row_auto ?>][type]"
                                       value="<?= $attachment->type ?>" placeholder="Name" required/>

                                <td class="text-left">
                                    <button type="button"
                                            onclick="deleteRowauto('-attachment-auto-<?= $row_auto ?>', '<?= $attachment->id; ?>', 'attachment')"
                                            data-toggle="tooltip" title="You want to delete Attachment"
                                            class="btn btn-danger"><i
                                                class="fa fa-minus-circle"></i></button>
                                </td>
                            </tr>
                            <?php $row_auto++; ?>
                        <?php }
                    }?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2"></td>
                        <td class="text-left">
                            <button type="button" onclick="addAttachmentauto();" data-toggle="tooltip" title="Add"
                                    class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </section>


        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app','Documents')?></h2>
            </header>
            <div class="card-body">
                <div class="col child-col border py-2">
                    <?= $form->field($model, 'signed_toe', ['labelOptions'=>['style'=>'padding:9px; color:#E1F5FE; background-color:#0277BD;border-radius:3px;']])->fileInput(['class'=>'imgInps d-none']) ?>

                    <p style="font-weight: bold; font-size:18px; color:red;">Upload Only Pdf</p>
                    <?php

                    $imgname = Yii::$app->params['dummy_image_address'];
                    if ($model->signed_toe != null) {
                        $imgname = Yii::$app->get('s3bucket')->getUrl('signed-toe/images/'.$model->signed_toe);
                        // echo $imgname; die();
                        $link=$imgname;
                    }
                    ?>
                    <div style='width:350px; margin-bottom:30px; position:relative;'>
                        <?php

                        if (strpos($imgname,'.pdf') == true) {
                            $imgname=Yii::$app->params['uploadPdfIcon'];
                        }

                        ?>
                        <img class="blah" src="<?= $imgname ?>" alt="No Image is selected." width="200px"/>

                        <?php
                        if ($model->signed_toe != null) {
                            ?>
                            <a style="color:#ffffff; font-weight: bold; padding-left:7px; padding-right :7px; font-size:22px; position:absolute;  bottom:10px; left:20px; background-color:#558B2F;";
                               href="<?= $link ?>" target="_blank"><i class="fas fa-eye"></i></a>
                        <?php } ?>

                    </div>
                    <div>
                        <?php

                        $scanOfficer=app\models\User::find()->where(['id'=>Yii::$app->user->identity->id, 'permission_group_id'=>11])->select(['id'])->one();
                        if ($scanOfficer['id'] !=null) {
                            echo $form->field($model, 'sig_verification_status')->checkbox(['class'=>'mt-4']);
                        } ?></div>
                </div>
            </div>
        </section>

        <?php   if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){ ?>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Verification') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'status')->widget(\kartik\select2\Select2::classname(), [
                                'data' => array( '2' => 'Unverified','1' => 'Verified'),
                            ]);
                            ?>
                        </div>


                    </div>
                </div>
            </div>
        </section>
        <?php } ?>




    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>



<script type="text/javascript">
    var row = <?= $row ?>;
    function addAttachment() {

        html = '<tr id="image-row' + row + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="Company[customEmailsStatus][' + row + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="email" class="form-control"  name="Company[customEmailsStatus][' + row + '][email]" value="" placeholder="Email" required />';
        html += '    </div>';
        html += '  </td>';

        html += '<input type="hidden" class="form-control" name="Company[customEmailsStatus][<?= $row ?>][type]" value="1" placeholder="Name" required/>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment tbody').append(html);

        row++;
    }


    var row_auto = <?= $row_auto ?>;
    function addAttachmentauto() {

        html = '<tr id="image-row-auto' + row_auto + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="Company[customEmailsAuto][' + row_auto + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="email" class="form-control"  name="Company[customEmailsAuto][' + row_auto + '][email]" value="" placeholder="Email" required />';
        html += '    </div>';
        html += '  </td>';

        html += '<input type="hidden" class="form-control" name="Company[customEmailsAuto][<?= $row_auto ?>][type]" value="1" placeholder="Name" required/>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row-auto' + row_auto + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment-auto tbody').append(html);

        row_auto++;
    }


    function deleteRow(rowId, docID, type) {



        var url = '<?= \yii\helpers\Url::to('/client/remove-attachment'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if(response.status == 'exist'){
                            swal("Warning!", response.message, "warning");
                        }else{
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
    function deleteRowauto(rowId, docID, type) {



        var url = '<?= \yii\helpers\Url::to('/client/remove-attachment'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if(response.status == 'exist'){
                            swal("Warning!", response.message, "warning");
                        }else{
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
</script>




<?php

$this->registerJs('

    function AutoCompleteFunction(){
        $(".job-title-cls").autocomplete({
            serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
            noCache: true,
            onSelect: function(suggestion) {
                // if($(".company-job_title_id").val()!=suggestion.data){
                    // $(".job-title-id-field").val(suggestion.data);
                    $(this).parents(".append-card").find(".job-title-id-field").val(suggestion.data);
                // }
            },
            onInvalidateSelection: function() {
                $(this).parents(".append-card").find(".job-title-id-field").val("0");
                // $(".job-title-id-field").val("0");
            }
        });
    }
AutoCompleteFunction();


  $("body").on("click", ".create-instructing-person", function() {
        addInstructingPerson()
  });


	var add_Rows = '.$i.';
	function addInstructingPerson(){
		html = "";
		html += "<section class=\"card card-outline card-info append-card\">";
		html += "<div class=\"row mx-2 my-2\">";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-firstname\">";
		html +=         "<label class=\"control-label\" for=\"company-firstname\">Firstname</label>";
		html +=         "<input type=\"text\" id=\"company-firstname\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][firstname]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-lastname\">";
		html +=         "<label class=\"control-label\" for=\"company-lastname\">Lastname</label>";
		html +=         "<input type=\"text\" id=\"company-lastname\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][lastname]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-job_title\">";
		html +=         "<label class=\"control-label\" for=\"company-job_title\">Job Title</label>";
		html +=         "<input type=\"text\" id=\"company-job_title\" class=\"form-control job-title-cls\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][job_title]\" autocomplete=\"off\">";
		html +=         "<input type=\"hidden\" class=\"job-title-id-field\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][job_title_id]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-mobile\">";
		html +=         "<label class=\"control-label\" for=\"company-mobile\">Mobile</label>";
		html +=         "<input type=\"text\" id=\"company-mobile\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][mobile]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-email\">";
		html +=         "<label class=\"control-label\" for=\"company-email\">Email</label>";
		html +=         "<input type=\"text\" id=\"company-email\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][email]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4 my-2 py-4 text-right\">";
		html +=     "<button type=\"button\" data-toggle=\"tooltip\" title=\"Remove\" class=\"btn btn-danger remove-apnd-card\"><i class=\"fa fa-minus-circle\"></i></button>";
		html += "</div>";

		html += "</div>";
		html += "</section>";

        // console.log(html);
        $(".instructing-person-append-section").append(html);
        add_Rows++;
        AutoCompleteFunction();	 
    }









$("#company-d-start_date,#company-d-lead_date,#company-d-lead_expected_close_date,#company-d-lead_close_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
$("body").on("change", "#company-country_id", function () {
  $.get("'.Url::to(['suggestion/zone-options','country_id'=>'']).'"+$(this).val(),function(data){
    $("select#company-zone_id").html(data);
  });
});
$("#company-d-lead_score_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->lead_score>0 ? 'initial_value: '.$model->lead_score : '').'
});
$("#company-d-lead_score_star").on("afterChange", function(ev, data){
  $("#company-lead_score").val(data.to);
});
$("#company-d-confidence_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->confidence>0 ? 'initial_value: '.$model->confidence : '').'
});
$("#company-d-confidence_star").on("afterChange", function(ev, data){
  $("#company-confidence").val(data.to);
});
$(".numeral-input").each(function(index){
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("body").on("blur", ".numeral-input", function () {
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("#company-manager_id").select2({
	allowClear: true,
	width: "100%",
});


    
    

  $("body").on("click", ".remove-apnd-card", function() {
    $(this).parents(".append-card").remove();
  });

');
?>
