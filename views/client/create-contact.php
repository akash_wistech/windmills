<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = Yii::t('app', $form_title);
$cardTitle = Yii::t('app','New Contact');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="company-create">

    <?= $this->render('_formcontact', [
        'model' => $model,
        'cardTitle' => $cardTitle,
        'existing_valuation_contact' => $existing_valuation_contact,
        'bilal_contact' => $bilal_contact,
        'valuation_contact' => $valuation_contact,
        'prospect_contact' => $prospect_contact,
        'property_owner_contact' => $property_owner_contact,
        'inquiry_valuations_contact' => $inquiry_valuations_contact,
        'icai_contact' => $icai_contact,
        'broker_contact' => $broker_contact,
        'developer_contact' => $developer_contact,
    ]) ?>
</div>
