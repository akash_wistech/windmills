<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
$("body").on("beforeSubmit", "form#company-form", function () {
	var _targetContainer="#company-form";
	var form = $(this);
	// return false if form still have some validation errors
	if (form.find(".has-error").length) {
		return false;
	}
	App.blockUI({
		message: "'.Yii::t('app','Please wait...').'",
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});
	// submit form
	$.ajax({
		url: form.attr("action"),
		type: "post",
		data: form.serialize(),
		dataType: "json",
		success: function (response) {
			if(response["success"]){
				toastr.success(response["success"]["msg"], response["success"]["heading"], {timeOut: 5000});
        form.trigger("reset");
				closeModal();
				$("#selected_company_id").val(response["success"]["id"]);
				$("#selected_company_title").val(response["success"]["title"]);
			}else{
				Swal.fire({title: response["error"]["heading"], html: response["error"]["msg"], icon: "error", timer: 5000});
			}
			App.unblockUI($(_targetContainer));
		}
	});
	return false;
});
');
?>
