<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\ClientFormAsset;
use kartik\select2\Select2;
ClientFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
$zonesList = [];
if ($model->country_id > 0) {
    $zonesList = Yii::$app->appHelperFunctions->getCountryZoneListArr($model->country_id);
}
$emiratesListArr = Yii::$app->appHelperFunctions->emiratedListArr;
$feeStructureOptions = Yii::$app->appHelperFunctions->feeStructureOptionListArr;
//$feeStructureTypes=Yii::$app->helperFunctions->feeStructureTypeListArr;
$feeStructureTypes = '';

// if($model->primaryContact!=null){
//     // echo "<pre>"; print_r($model->primaryContact); echo "</pre>"; die();

//   $model->firstname=$model->primaryContact->firstname;
//   $model->lastname=$model->primaryContact->lastname;
//   $model->job_title=$model->primaryContact->profileInfo!=null && $model->primaryContact->profileInfo->jobTitle!=null ? $model->primaryContact->profileInfo->jobTitle->title : '';
//   $model->job_title_id=$model->primaryContact->profileInfo->job_title_id;
//   $model->mobile=$model->primaryContact->profileInfo->mobile;
//   $model->email=$model->primaryContact->email;
// }

$actionBtns = 'Edit';

// die('here');


?>
<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }
</style>
<script>

</script>
<section class="company-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['id' => 'company-form']); ?>
    <div class="hidden">
        <?= $form->field($model, 'job_title_id')->textInput() ?>
        <?= $form->field($model, 'lead_score')->textInput() ?>
        <?= $form->field($model, 'confidence')->textInput() ?>
        <input type="hidden" name="Company[segment_type]" value="11" >
        <!-- --><? /*= $form->field($model, 'company_id')->textInput(['id'=>'selected_company_id'])*/ ?>
    </div>

    <div class="card-body">

        <section class="valuation-form card card-outline card-primary">

            <header class="card-header">
                <h2 class="card-title">Client Details </h2>
            </header>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-4">
                        <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Company Official Name') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'nick_name')->textInput(['maxlength' => true])->label('Company Nick Name') ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'main_client_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Clients::find()
                               // ->where(['status' => 1])
                                ->orderBy(['title' => SORT_ASC,])
                                ->all(), 'id', 'title'),

                            'options' => ['placeholder' => 'Select Client ...', 'class'=> 'client-cls'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Client Name');
                        ?>

                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'client_department')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\ClientDepartments::find()
                                ->where(['status' => 1])
                                ->orderBy(['title' => SORT_ASC,])
                                ->all(), 'id', 'title'),

                            'options' => ['placeholder' => 'Select Department ...', 'class'=> 'client-cls'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Department');
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'building_name')->textInput(['maxlength' => true])->label('Building Name') ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'office_number')->textInput(['maxlength' => true])->label('Office Number') ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'address')->textInput(['maxlength' => true])->label('Street') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'p_o_box')->textInput(['maxlength' => true])->label('P. O. Box') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'zohobooks_id')->textInput(['maxlength' => 255,'type' => 'varchar'])->label('ZohoBook ID')?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'zone_id')->dropDownList(Yii::$app->appHelperFunctions->emiratedListArr, ['prompt' => Yii::t('app', 'Select')]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php

                        if ($model->country_id > 0) {

                        } else {
                            $model->country_id = 221;
                        }

                        if ($model->country_id_nationality > 0) {

                        } else {
                            $model->country_id_nationality = 221;
                        }

                        ?>
                        <?= $form->field($model, 'country_id')->dropDownList(Yii::$app->appHelperFunctions->countryListArr, ['prompt' => Yii::t('app', 'Select')])->label('Country of Origin') ?>
                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'data_type')->hiddenInput(['value' => 0])->label(false); ?>
                    </div>

                </div>
        </section>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', '<b>Contacts Details</b>') ?></h2>
            </header>

            <div class="card-body">
                <div class="row">
                    <table id="new_contas_table" class="table table-bordered">
                        <thead>

                        <tr>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Email</th>
                            <th>Position</th>
                            <th>Mobile</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if ($model->allContacts != null) {
                            foreach ($model->allContacts as $key => $value) { ?>
                                <tr>
                                    <td><?= $value->firstname ?></td>
                                    <td><?= $value->lastname ?></td>
                                    <td><?= $value->email ?></td>
                                    <td><?= $value->profileInfo->job_title_id ?></td>
                                    <td> <?= $value->profileInfo->mobile ?></td>

                                    <td>
                                        <button type="button" data-toggle="tooltip" title="Add"
                                                class="btn btn-primary create-instructing-person"><i
                                                    class="fa fa-minus-square"></i></button>
                                        <button type="button" data-toggle="tooltip" title="Add"
                                                class="btn btn-primary create-instructing-person"><i
                                                    class="fa fa-pencil-square"></i></button>
                                    </td>
                                </tr>
                            <?php }
                        } ?>
                        <tr>
                            <td colspan="5"></td>
                            <td>
                                <button type="button" data-toggle="modal" data-target="#contactModal" title="Add"
                                        class="btn btn-primary create-instructing-person"><i
                                            class="fa fa-plus-square"></i></button>

                            </td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', '<b>Client Classification Details</b>') ?></h2>
            </header>

            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_type')->dropDownList(yii::$app->quotationHelperFunctions->clienttype)->label('Client Segment') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_industry')->dropDownList(yii::$app->appHelperFunctions->clientIndustry) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_priority')->dropDownList(Yii::$app->appHelperFunctions->clientPriority, ['prompt' => Yii::t('app', 'Select')])->label('Client Priority') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'number_of_total_properties')->dropDownList(Yii::$app->appHelperFunctions->numberOfTotalPropertiesClientOwns, ['prompt' => Yii::t('app', 'Select')])->label('Number of total properties client owns') ?>
                    </div>

                </div>


            </div>
        </section>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', '<b>Additonal Corporate Details</b>') ?></h2>
            </header>

            <div class="card-body ">
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'company_detail')->textInput(['maxlength' => true])->label('Company Additonal Detail') ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'linked_in')->textInput(['maxlength' => true])->label('Linked in Page') ?>
                    </div>
                </div>
        </section>



        <!--
    <? /*= $form->field($model, 'manager_id')->dropDownList(Yii::$app->appHelperFunctions->StaffMemberListArrBusiness,['multiple'=>'multiple'])*/ ?>
     -->


        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', '<b>Additional Relationship Details </b>') ?></h2>
            </header>

            <div class="card-body ">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'sensitivity')->dropDownList(Yii::$app->appHelperFunctions->clientSenisitivity,['prompt'=>Yii::t('app','Select')])->label('Sensitivity')?>
                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'good_bad_client')->dropDownList(array('0' => 'Good', '1' => 'Bad'),['prompt'=>Yii::t('app','Select')])->label('Good/Bad Client')?>
                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'relative_discount')->dropDownList(yii::$app->quotationHelperFunctions->relativediscount,['prompt'=>Yii::t('app','Select')])->label('Relative Discount')?>
                    </div>

                </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', '<b>Client Requirements Details </b>') ?></h2>
            </header>

            <div class="card-body ">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'print_report')->dropDownList(array('0' => 'No', '1' => 'Yes'),['prompt'=>Yii::t('app','Select')])->label('Hard Copy Reports')?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'land_valutaion')->dropDownList(array('0' => 'No', '1' => 'Yes'),['prompt'=>Yii::t('app','Select')])->label('Land Market Value')?>
                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'land_age')->textInput(['maxlength' => true,'type' => 'number'])->label('Minimum Age for Land MV')?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'vat')->dropDownList(Yii::$app->appHelperFunctions->getvatArr())->label('Vat for Quotation')?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'report_password')->textInput(['maxlength' => true])->label('Report Password')?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_invoice_customer')->dropDownList(array('0' => 'No', '1' => 'Yes'),['prompt'=>Yii::t('app','Select')])->label('Client Require Customer Invoice')?>
                    </div>
                </div>
        </section>




        <section class="valuation-form valuation-form card card-outline card-info">

            <header class="card-header">
                <h2 class="card-title">Documents Information</h2>
            </header>
            <div class="card-body">
                <div class="row" style="padding: 10px">


                    <table id="requestTypes"
                           class="table table-striped table-bordered table-hover images-table">
                        <thead>
                        <tr>
                            <td>Description</td>
                            <td>Attachment</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>


                        <tr id="image-row1">

                            <td class="text-left">
                                <div class="required">
                                    <label class="control-label">
                                        Terms of Engagement
                                    </label>
                                </div>

                            </td>

                            <td class="text-left upload-docs">
                                <div class="form-group">
                                    <a href="javascript:;"
                                       id="upload-document1"
                                       onclick="uploadAttachment(1)"
                                       data-toggle="tooltip"
                                       class="img-thumbnail"
                                       title="Upload Document">
                                        <?php
                                        $attachment = $model->signed_toe;
                                        if ($attachment <> null) {

                                            if (strpos($attachment, '.pdf') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>

                                                <?php

                                            } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php

                                            } else {
                                                ?>
                                                <img src="<?php echo $attachment; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                 alt="" title=""
                                                 data-placeholder="no_image.png"/>
                                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                               target="_blank">
                                                <span class="glyphicon glyphicon-eye-open"></span>
                                            </a>
                                            <?php
                                        }

                                        ?>
                                    </a>
                                    <input type="hidden"
                                           name="Company[signed_toe]"
                                           value="<?= $attachment; ?>"
                                           id="input-attachment1"/>


                                </div>
                            </td>

                        </tr>
                        <tr id="image-row2">

                            <td class="text-left">
                                <div class="required">
                                    <label class="control-label">
                                        SLA
                                    </label>
                                </div>

                            </td>

                            <td class="text-left upload-docs">
                                <div class="form-group">
                                    <a href="javascript:;"
                                       id="upload-document2"
                                       onclick="uploadAttachment(2)"
                                       data-toggle="tooltip"
                                       class="img-thumbnail"
                                       title="Upload Document">
                                        <?php
                                        $attachment_sla = $model->sla;
                                        if ($attachment_sla <> null) {

                                            if (strpos($attachment_sla, '.pdf') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_sla; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>

                                                <?php

                                            } else if (strpos($attachment_sla, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_sla; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php

                                            } else {
                                                ?>
                                                <img src="<?php echo $attachment_sla; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_sla; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                 alt="" title=""
                                                 data-placeholder="no_image.png"/>
                                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                               target="_blank">
                                                <span class="glyphicon glyphicon-eye-open"></span>
                                            </a>
                                            <?php
                                        }

                                        ?>
                                    </a>
                                    <input type="hidden"
                                           name="Company[sla]"
                                           value="<?= $attachment_sla; ?>"
                                           id="input-attachment2"/>


                                </div>
                            </td>

                        </tr>
                        <tr id="image-row3">
                        <tr id="image-row4">

                            <td class="text-left">
                                <div class="required">
                                    <label class="control-label">
                                        NDA
                                    </label>
                                </div>

                            </td>

                            <td class="text-left upload-docs">
                                <div class="form-group">
                                    <a href="javascript:;"
                                       id="upload-document2"
                                       onclick="uploadAttachment(2)"
                                       data-toggle="tooltip"
                                       class="img-thumbnail"
                                       title="Upload Document">
                                        <?php
                                        $attachment_nda = $model->nda;
                                        if ($attachment_nda <> null) {

                                            if (strpos($attachment_nda, '.pdf') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_nda; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>

                                                <?php

                                            } else if (strpos($attachment_nda, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_nda; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php

                                            } else {
                                                ?>
                                                <img src="<?php echo $attachment_nda; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_nda; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                 alt="" title=""
                                                 data-placeholder="no_image.png"/>
                                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                               target="_blank">
                                                <span class="glyphicon glyphicon-eye-open"></span>
                                            </a>
                                            <?php
                                        }

                                        ?>
                                    </a>
                                    <input type="hidden"
                                           name="Company[nda]"
                                           value="<?= $attachment_nda; ?>"
                                           id="input-attachment2"/>


                                </div>
                            </td>

                        </tr>


                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>


                </div>
            </div>

        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Relatonship Manager') ?></h2>
            </header>
            <div class="card-body">
                <?= $form->field($model, 'manager_id')->dropDownList(Yii::$app->appHelperFunctions->staffMemberListArr)->label('Relatonship Manager') ?>
                <?= $form->field($model, 'manager_id_back_up')->dropDownList(Yii::$app->appHelperFunctions->staffMemberListArr)->label('Back up Relationship Manager') ?>
            </div>
        </section>

        <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('status')) { ?>
            <section class="card card-outline card-info">
                <header class="card-header">
                    <h2 class="card-title"><?= Yii::t('app', 'Verification') ?></h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-4">
                                <?= $form->field($model, 'print_report')->dropDownList(array('2' => 'Unverified', '1' => 'Verified'),['prompt'=>Yii::t('app','Select')])->label('Status')?>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
        <?php } ?>


    </div>

    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>

<style>
    background-position: right calc(0.375em + 1.1875rem) center !important;
</style>
<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#contactModal">Open Contact Form</button>
-->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg"  style="max-width:1250px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contactModalLabel">Create Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="contactForm">
                    <section class="valuation-form card card-outline card-primary">

                        <header class="card-header">
                            <h2 class="card-title">Client Details </h2>
                        </header>
                        <div class="card-body">
<div class="row">

                        <div class="col-sm-4 form-group required">
                            <label for="firstName">First Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="firstName" name="firstName">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="col-sm-4 form-group required">
                            <label for="lastName">Last Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="lastName" name="lastName">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="col-sm-4 form-group required">
                            <label for="email">Email <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" name="email">
                            <div class="invalid-feedback"></div>
                        </div>


                        <div class="col-sm-4 form-group">
                            <label for="position" class=" form-label">Source of Contact</label>
                            <select class=" select2 form-control form-select" id="source_of_contact" name="source_of_contact">
                                <option value="">Select Source</option>
                                <?php
                                foreach (Yii::$app->appHelperFunctions->sourceOfContact as $skey => $source){ ?>
                                    <option value="<?= $skey ?>"><?= $source ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="col-sm-4 form-group">
                            <label for="position" class=" form-label">Country <span class="text-danger">*</span></label>
                            <select class=" select2 form-control form-select" id="country_id" name="country_id">
                                <option value="">Select Country</option>
                                <?php
                                foreach ($countries as $ckey => $country){ ?>
                                    <option value="<?= $ckey ?>"><?= $country ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="col-sm-4 form-group">
                            <label for="position" class=" form-label">City <span class="text-danger">*</span></label>
                            <select class=" select2 form-control form-select" id="city_id" name="city_id">
                                <option value="">Select City</option>

                            </select>
                            <div class="invalid-feedback"></div>
                        </div>




                        <div class="col-sm-4  form-group">

                            <div class="form-row">
                                <div class="col-md-3 mb-3" style="padding-right: 0px;">
                                    <label for="countryCode">Country Code </label>
                                    <input type="text" class="form-control country_code" id="countryCode" readonly placeholder="+00" >
                                </div>
                                <div class="col-md-9 mb-3">
                                    <label for="phoneNumber">Landline 1 <span class="text-danger">*</span></label>
                                    <input type="tel" class="form-control" id="landline" name="landline" placeholder="0000000">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4  form-group">

                            <div class="form-row">
                                <div class="col-md-3 mb-3" style="padding-right: 0px;">
                                    <label for="countryCode">Country Code</label>
                                    <input type="text"  class="form-control country_code" id="countryCode" readonly placeholder="+00" >
                                    <input type="hidden" name="country_code" class="form-control country_code" id="countryCode"  placeholder="+00" >
                                </div>
                                <div class="col-md-9 mb-3">
                                    <label for="phoneNumber">Landline 2</label>
                                    <input type="tel" class="form-control" id="landline2" name="landline2" placeholder="0000000">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-4 form-group ">

                            <div class="form-row">
                                <div class="col-md-3 mb-3" style="padding-right: 0px;">
                                    <label for="countryCode">Country Code</label>
                                    <input type="text" class="form-control country_code" id="countryCode" readonly placeholder="+00" >
                                </div>
                                <div class="col-md-9 mb-3">
                                    <label for="phoneNumber">Mobile 1 <span class="text-danger">*</span></label>
                                    <input type="tel" class="form-control" id="mobile1" name="mobile1" placeholder="0000000">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-4  form-group">
                            <div class="form-row">
                                <div class="col-md-3 mb-3" style="padding-right: 0px;">
                                    <label for="countryCode">Country Code</label>
                                    <input type="text" class="form-control country_code" id="countryCode" readonly placeholder="+00" >
                                </div>
                                <div class="col-md-9 mb-3">
                                    <label for="phoneNumber">Mobile 2</label>
                                    <input type="tel" class="form-control" id="mobile2" name="mobile2" placeholder="0000000">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>

                        </div>





                        <div class="col-sm-4 form-group">
                            <label for="position" class=" form-label">Position <span class="text-danger">*</span></label>
                            <select class=" select2 form-control form-select" id="position" name="position">
                                <option value="">Select Position</option>
                                <?php
                                foreach ($positions as $pkey => $position){ ?>
                                    <option value="<?= $pkey ?>"><?= $position ?></option>
                             <?php
                                }
                                ?>
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="col-sm-4 form-group">
                            <label for="position" class=" form-label">Department <span class="text-danger">*</span></label>
                            <select class=" select2 form-control form-select" id="department" name="department">
                                <option value="">Select Department</option>
                                <?php
                                foreach ($departments as $dkey => $department){ ?>
                                    <option value="<?= $dkey ?>"><?= $department ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>


            </div>
                        </div>
            </section>

                    <section class="valuation-form card card-outline card-primary">

                        <header class="card-header">
                            <h2 class="card-title">Communication Details</h2>
                        </header>
                        <div class="card-body">
                            <div class="row">
                            <div class="col-sm-3 form-group">
                                <label for="send_emails" class=" form-label">Send Emails </label>
                                <select class=" select2 form-control form-select" id="send_emails" name="send_emails">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="send_sms" class=" form-label">Send SMS </label>
                                <select class=" select2 form-control form-select" id="send_sms" name="send_sms">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="send_whatsapp" class=" form-label">Send WhatsApp </label>
                                <select class=" select2 form-control form-select" id="send_whatsapp" name="send_whatsapp">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="linkedin_posts" class=" form-label">Linkedin Posts </label>
                                <select class=" select2 form-control form-select" id="linkedin_posts" name="linkedin_posts">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>

                            <div class="col-sm-3 form-group">
                                <label for="facebooks_posts " class=" form-label">Facebooks Posts </label>
                                <select class=" select2 form-control form-select" id="facebooks_posts" name="facebooks_posts">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>

                            <div class="col-sm-3 form-group">
                                <label for="instagram_posts" class=" form-label">Instagram Posts </label>
                                <select class=" select2 form-control form-select" id="instagram_posts" name="instagram_posts">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>

                            <div class="col-sm-3 form-group">
                                <label for="twitter_posts" class=" form-label">Twitter Posts </label>
                                <select class=" select2 form-control form-select" id="twitter_posts" name="twitter_posts">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>

                            <div class="col-sm-3 form-group">
                                <label for="tiktok_posts" class=" form-label">Tiktok Posts </label>
                                <select class=" select2 form-control form-select" id="tiktok_posts" name="tiktok_posts">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>

                                <div class="col-sm-3 form-group">
                                    <label for="cc_daily_status " class=" form-label">Send CC Emails for Daily Status Reports </label>
                                    <select class=" select2 form-control form-select" id="cc_daily_status" name="cc_daily_status">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>

                                <div class="col-sm-3 form-group">
                                    <label for="cc_auto_emails" class=" form-label">CC Emails for Auto Emails </label>
                                    <select class=" select2 form-control form-select" id="cc_auto_emails" name="cc_auto_emails">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>

                                <div class="col-sm-3 form-group">
                                    <label for="monthly_status_email" class=" form-label">Monthly Status Report needed? </label>
                                    <select class=" select2 form-control form-select" id="monthly_status_email" name="monthly_status_email">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>

                                <div class="col-sm-3 form-group">
                                    <label for="daily_status_email" class=" form-label">Daily Status Report Nedeed </label>
                                    <select class=" select2 form-control form-select" id="daily_status_email" name="daily_status_email">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>




                            </div>

                        </div>
                    </section>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitForm">Submit</button>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $('#country_id').on('change', function() {
            var countryId = $(this).val();
            if (countryId) {
                $.ajax({
                    url: 'client/get-cities', // Replace with the URL to your AJAX action
                    data: {countryId: countryId},
                    dataType: 'json',
                    success: function(data) {
                        $('#city_id').html(data.cities);
                        var your_string=data.phone_code;
                        if (your_string.indexOf('+') > -1){
                            $('.country_code').val(data.phone_code);
                        }else {
                            $('.country_code').val("+" + data.phone_code);
                        }

                    }
                });
            } else {
                $('#city_id').html('');
            }
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#contactModal').on('hidden.bs.modal', function () {
            $('#contactForm')[0].reset();
            $('.invalid-feedback').text('');
            $('.is-invalid').removeClass('is-invalid');
        });


        $('#email').on('focusout', function () {
            validateEmail($(this));
        });

        $('#mobile1, #mobile2').on('focusout', function () {
            if($('#country_id').val() == 221) {
                validatePhoneuae($(this));
            }else{
                validatePhone($(this));
            }
        });

        $('#landline, #landline2').on('focusout', function () {
            if($('#country_id').val() == 221) {
                validatelanlineuae($(this));
            }else{
                validatelanline($(this));
            }
        });

        function validateFirstName(fnameField) {
            const fname = fnameField.val();
            if (fname ==='') {
                fnameField.addClass('is-invalid');
                fnameField.next('.invalid-feedback').text('Please enter a First Name.');
            } else {
                fnameField.removeClass('is-invalid');
                fnameField.next('.invalid-feedback').text('');
            }
        }
        function validateLastName(lnameField) {
            const lname = lnameField.val();
            if (lname ==='') {
                lnameField.addClass('is-invalid');
                lnameField.next('.invalid-feedback').text('Please enter a Last Name.');
            } else {
                lnameField.removeClass('is-invalid');
                lnameField.next('.invalid-feedback').text('');
            }
        }

        function validatePosition(positionField) {
            const position = positionField.val();
            if (position ==='') {
                positionField.addClass('is-invalid');
                positionField.next('.invalid-feedback').text('Please select Position');
            } else {
                positionField.removeClass('is-invalid');
                positionField.next('.invalid-feedback').text('');
            }
        }

        function validateDepartment(departmentField) {
            const department = departmentField.val();
            if (department ==='') {
                departmentField.addClass('is-invalid');
                departmentField.next('.invalid-feedback').text('Please select Department');
            } else {
                departmentField.removeClass('is-invalid');
                departmentField.next('.invalid-feedback').text('');
            }
        }
        function validateCountry_id(country_idField) {
            const country_id = country_idField.val();
            if (country_id ==='') {
                country_idField.addClass('is-invalid');
                country_idField.next('.invalid-feedback').text('Please select Country');
            } else {
                country_idField.removeClass('is-invalid');
                country_idField.next('.invalid-feedback').text('');
            }
        }

        function validateCity_id(city_idField) {
            const city_id = city_idField.val();
            if (city_id ==='') {
                city_idField.addClass('is-invalid');
                city_idField.next('.invalid-feedback').text('Please select City');
            } else {
                city_idField.removeClass('is-invalid');
                city_idField.next('.invalid-feedback').text('');
            }
        }


        function validateSource_of_contact(source_of_contactField) {
            const source_of_contact = source_of_contactField.val();
            if (source_of_contact ==='') {
                source_of_contactField.addClass('is-invalid');
                source_of_contactField.next('.invalid-feedback').text('Please select Source of contact');
            } else {
                source_of_contactField.removeClass('is-invalid');
                source_of_contactField.next('.invalid-feedback').text('');
            }
        }





        function validateEmail(emailField) {
            const email = emailField.val();
            const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            if (!emailPattern.test(email)) {
                emailField.addClass('is-invalid');
                emailField.next('.invalid-feedback').text('Please enter a valid email address.');
                isemail = false;
            } else {
                emailField.removeClass('is-invalid');
                emailField.next('.invalid-feedback').text('');
                isemail = true;
            }
        }

        function validatelanline(landlineField) {
            const phone = landlineField.val();

            const phonePattern = /^\d{7,14}$/;
            if (!phonePattern.test(phone)) {
                landlineField.addClass('is-invalid');
                landlineField.next('.invalid-feedback').text('Please enter a valid phone number.');
            } else {
                landlineField.removeClass('is-invalid');
                landlineField.next('.invalid-feedback').text('');
            }
        }
        function validatelanlineuae(landlineField) {
            const phone = landlineField.val();
             const phonePattern = /^[0-9]{8}$/;
            if (!phonePattern.test(phone)) {
                landlineField.addClass('is-invalid');
                landlineField.next('.invalid-feedback').text('Please enter a valid 8-digit phone number.');
            } else {
                landlineField.removeClass('is-invalid');
                landlineField.next('.invalid-feedback').text('');
            }
        }

        function validatePhone(phoneField) {
            const phone = phoneField.val();
            const phonePattern = /^\d{7,14}$/;
           // const phonePattern = ^\+[1-9]{1}[0-9]{3,14}$
            if (!phonePattern.test(phone)) {
                phoneField.addClass('is-invalid');
                phoneField.next('.invalid-feedback').text('Please enter a valid mobile number.');
            } else {
                phoneField.removeClass('is-invalid');
                phoneField.next('.invalid-feedback').text('');
            }
        }
        function validatePhoneuae(phoneField) {
            const phone = phoneField.val();
            const phonePattern = /^[0-9]{9}$/;
            if (!phonePattern.test(phone)) {
                phoneField.addClass('is-invalid');
                phoneField.next('.invalid-feedback').text('Please enter a valid 9-digit mobile number.');
            } else {
                phoneField.removeClass('is-invalid');
                phoneField.next('.invalid-feedback').text('');
            }
        }

        $('#submitForm').on('click', function () {
            var isValid = true;
            var isemail = true;
            $('.is-invalid').removeClass('is-invalid');
            $('.invalid-feedback').text('');

            if ($('#firstName').val() === '') {
                validateFirstName($('#firstName'));
            }

            if ($('#lastName').val() === '') {
                validateLastName($('#lastName'));
            }

            if ($('#position').val() === '') {
                validatePosition($('#position'));
                isValid = false;
            }

            if ($('#department').val() === '') {
                validateDepartment($('#department'));
                isValid = false;
            }

            if ($('#country_id').val() === '') {
                validateCountry_id($('#country_id'));
                isValid = false;
            }
            if ($('#city_id').val() === '') {
                validateCountry_id($('#city_id'));
                isValid = false;
            }
            if ($('#source_of_contact').val() === '') {
                validateSource_of_contact($('#source_of_contact'));
                isValid = false;
            }



            if ($('#email').val() !== '') {
                validateEmail($('#email'));
                if (!$('#email').hasClass('is-invalid') && isValid) {
                    isValid = true;
                    isemail =  true;
                } else {
                    isValid = false;
                    isemail =  false;
                }
            } else {

                validateEmail($('#email'));
                isValid = false;
                isemail = false
            }

            if ($('#mobile1').val() !== '') {
                if($('#country_id').val() == 221) {
                    validatePhoneuae($('#mobile1'));
                }else{
                    validatePhone($('#mobile1'));
                }
                if (!$('#mobile1').hasClass('is-invalid') && isValid) {
                    isValid = true;
                } else {
                    isValid = false;
                }
            } else {
                if($('#country_id').val() == 221) {
                    validatePhoneuae($('#mobile1'));
                }else{
                    validatePhone($('#mobile1'));
                }

                isValid = false;
            }

            if ($('#landline').val() !== '') {
                if($('#country_id').val() == 221) {
                    validatelanlineuae($('#landline'));
                }else{
                    validatelanline($('#landline'));
                }

                if (!$('#landline').hasClass('is-invalid') && isValid) {
                    isValid = true;
                } else {
                    isValid = false;
                }
            } else {
                if($('#country_id').val() == 221) {
                    validatelanlineuae($('#landline'));
                }else{
                    validatelanline($('#landline'));
                }
               // validatelanline($('#landline'));
                isValid = false;
            }

            if ($('#landline2').val() !== '') {
                if($('#country_id').val() == 221) {
                    validatelanlineuae($('#landline2'));
                }else{
                    validatelanline($('#landline2'));
                }

                if (!$('#landline2').hasClass('is-invalid') && isValid) {
                    isValid = true;
                } else {
                    isValid = false;
                }
            } else {

                // validatelanline($('#landline'));
                isValid = false;
            }


            if ($('#mobile2').val() !== '') {

                if($('#country_id').val() == 221) {
                    validatePhoneuae($('#mobile2'));
                }else{
                    validatePhone($('#mobile2'));
                }
                if (!$('#mobile2').hasClass('is-invalid') && isValid) {
                    isValid = true;
                } else {
                    isValid = false;
                }
            } else {
               // validatePhone($('#mobile2'));
                isValid = false;
            }


                $.ajax({
                    url: 'client/check-email-existence',
                    method: 'POST',
                    dataType: 'json',
                    async: false,
                    data: {email: $('#email').val()},
                    success: function (response) {

                        if (response.exists == 1) {
                            isValid = false;
                            $('#email').addClass('is-invalid');
                            $('#email').next('.invalid-feedback').text('Email already exists.');
                        }
                    }
                });



            if (isValid) {
                console.log(isValid);
                $.ajax({
                    url: 'client/addnewcontact',
                    type: 'post',
                    dataType: 'json',
                    data: $('#contactForm').serialize(),
                    success: function(data) {
                        console.log(data);
                        ///$("#contactModal .close").click();
                        $('#contactModal').modal('hide');
                      //  $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                       // $('#contactModal').modal('hide');
                       // $('#contactModal').modal('toggle');
                       $('#new_contas_table tbody').html(data.html);
                        // Handle the success response
                    },
                    error: function() {
                        // Handle the error
                    }
                });

               // $('#contactModal').modal('hide');
                // Process form submission or any other action here
            }
        });
    });
</script>
<script type="text/javascript">


</script>


<?php

$this->registerJs('

    function AutoCompleteFunction(){
        $(".job-title-cls").autocomplete({
            serviceUrl: "' . Url::to(['suggestion/job-titles']) . '",
            noCache: true,
            onSelect: function(suggestion) {
                // if($(".company-job_title_id").val()!=suggestion.data){
                    // $(".job-title-id-field").val(suggestion.data);
                    $(this).parents(".append-card").find(".job-title-id-field").val(suggestion.data);
                // }
            },
            onInvalidateSelection: function() {
                $(this).parents(".append-card").find(".job-title-id-field").val("0");
                // $(".job-title-id-field").val("0");
            }
        });
    }
AutoCompleteFunction();


 









$("#company-d-start_date,#company-d-lead_date,#company-d-lead_expected_close_date,#company-d-lead_close_date,#company-d-dob_date,#company-d-wedding_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});


$("#company-d-lead_score_star").rate({
    max_value: 5,
    step_size: 1,
    ' . ($model->lead_score > 0 ? 'initial_value: ' . $model->lead_score : '') . '
});
$("#company-d-lead_score_star").on("afterChange", function(ev, data){
  $("#company-lead_score").val(data.to);
});
$("#company-d-confidence_star").rate({
    max_value: 5,
    step_size: 1,
    ' . ($model->confidence > 0 ? 'initial_value: ' . $model->confidence : '') . '
});
$("#company-d-confidence_star").on("afterChange", function(ev, data){
  $("#company-confidence").val(data.to);
});
$(".numeral-input").each(function(index){
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("body").on("blur", ".numeral-input", function () {
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("#company-manager_id").select2({
	allowClear: true,
	width: "100%",
});


    
    

  $("body").on("click", ".remove-apnd-card", function() {
    $(this).parents(".append-card").remove();
  });

');
$this->registerJs('
$("#user-job_title").autocomplete({
  serviceUrl: "' . Url::to(['suggestion/job-titles']) . '",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#user-job_title_id").val()!=suggestion.data){
      $("#user-job_title_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#user-job_title_id").val("0");
  }
});
$("#selected_company_title").autocomplete({
  serviceUrl: "' . Url::to(['suggestion/company']) . '",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#selected_company_id").val()!=suggestion.data){
      $("#selected_company_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#selected_company_id").val("0");
  }
});




$("body").on("click",".btn-random", function(){


$.ajax({

  url: "' . Url::to(['contact/generate-random-string', 'id' => $model->id]) . '",
  dataType: "html",
  type: "POST",

  });
});

$("body").on("click" ,".btn-random", function(){
         AppBlockSave();
      });


');
?>

<script>

    var uploadAttachment = function (attachmentId) {


        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function (json) {
                        console.log(json);
                       // alert('ddd');
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);


                            console.log(json.file.href);
                            var str1 = json['file'].href;
                            var str2 = ".pdf";
                            var str3 = ".doc";
                            var str4 = ".docx";
                            var str5 = ".xlsx";
                            var str6 = ".xls";
                            if (str1.indexOf(str2) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                            } else if (str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                            } else {
                                $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                            }
                            $('#input-attachment' + attachmentId).val(json['file'].href);


                            /*  $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                             $('#input-attachment' + attachmentId).val(json['file'].href);*/
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }

</script>
<?= $this->render('/client/js/create_company_script') ?>

<script>
    // random-string

    function AppBlockSave() {
        App.blockUI({
            iconOnly: true,
            target: ".Main-class-block",
            overlayColor: "none",
            cenrerY: true,
            boxed: true
        });

    };


</script>
