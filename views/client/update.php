<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = Yii::t('app', 'Clients');
$cardTitle = Yii::t('app','Update Client:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="company-update">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
        'searchModel'=>$searchModel,
        'dataProvider'=>$dataProvider,
    ]) ?>
</div>
