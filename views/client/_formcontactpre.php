<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\ClientFormAsset;
use yii\web\JsExpression;

ClientFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
$zonesList = [];


$actionBtns = 'Edit';

// die('here');


?>
<style>
    .select2-results__option.select2-results__message {
        padding: 0px;
    }
    button#no-results-btn {
        width: 100%;
        height: 100%;
        padding: 6px;
    }

    /* Make button look like other li elements */
    button#no-results-btn {
        border: 0;
        background-color: white;
        text-align: left;
    }

    /* Give button same hover effect */
    .select2-results__option.select2-results__message:hover {
        color: white;
    }
    button#no-results-btn:hover {
        background-color: #5897fb;
    }
</style>
<script>

</script>


<section class="valuation-form card card-outline card-primary">
            <?php $form = ActiveForm::begin(['id' => 'company-form']); ?>
            <header class="card-header">
                <h2 class="card-title">Serach Company Official Name</h2>
            </header>
            <div class="card-body">

                <div class="row">

                    <div class="col-sm-6">
                        <?php

                        // Assuming $data is an array of options for the select box
                        echo $form->field($model, 'title')->widget(\kartik\select2\Select2::classname(), [
                            //'data' => array(),
                            'options' => ['placeholder' => 'Type Company Name..'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1, // Number of characters before search is triggered
                      /*          'language' => [
                                    'noResults' => function() {
        return '<button id=\"no-results-btn\" onclick=\"noResultsButtonClicked()\">No Result Found</a>';
      },
                                ],*/
                                'ajax' => [
                                    'url' => \yii\helpers\Url::to(['client/search']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                  
                        return {q:params.term};
                        }'),
                                ],
                            ],
                        ])->label(false);
                        ?>

                    </div>
                    <div class="col-sm-6" id="create_btn">
                        <a type="submit" href="client/create-client-contact?s_type=<?= $s_type; ?>" class="btn btn-success">New Company Contact</a>
                    </div>

                </div>


                <?php ActiveForm::end(); ?>
        </section>



<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        // Initialize Select2
        var newcontact = '<a type="submit" href="client/create-client-contact?s_type=<?=$s_type; ?>" class="btn btn-success">New Company Contact</a>';
        // Event listener for change event
        $('#company-title').on('change', function() {
            var selectedValue = $(this).val();
            if(selectedValue > 0){
              $('#create_btn').html('<a type="submit" href="client/update?id='+selectedValue+'" class="btn btn-success">Add Contact To The Current Company</a>');
            }else{
                $('#create_btn').html(newcontact);
            }
            //console.log('create_btn: ', selectedValue);

            // You can perform further actions here based on the selected value
        });
        $("#company-title").on("select2:open", function (event) {

            // Check if the search input has no results

        });
    });
</script>

<script>
    // random-string

    function AppBlockSave() {
        App.blockUI({
            iconOnly: true,
            target: ".Main-class-block",
            overlayColor: "none",
            cenrerY: true,
            boxed: true
        });

    };


</script>
