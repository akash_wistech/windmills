<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\ClientFormAsset;
ClientFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
$zonesList=[];
if($model->country_id>0){
  $zonesList=Yii::$app->appHelperFunctions->getCountryZoneListArr($model->country_id);
}
$emiratesListArr=Yii::$app->appHelperFunctions->emiratedListArr;
$feeStructureOptions=Yii::$app->appHelperFunctions->feeStructureOptionListArr;
$feeStructureTypes=Yii::$app->helperFunctions->feeStructureTypeListArr;

if($model->primaryContact!=null){
  $model->firstname=$model->primaryContact->firstname;
  $model->lastname=$model->primaryContact->lastname;
  $model->job_title=$model->primaryContact->profileInfo!=null && $model->primaryContact->profileInfo->jobTitle!=null ? $model->primaryContact->profileInfo->jobTitle->title : '';
  $model->job_title_id=$model->primaryContact->profileInfo->job_title_id;
  $model->mobile=$model->primaryContact->profileInfo->mobile;
  $model->email=$model->primaryContact->email;
}

$actionBtns='Edit';

$this->registerJs('
$("#company-job_title").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#company-job_title_id").val()!=suggestion.data){
      $("#company-job_title_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#company-job_title_id").val("0");
  }
});
$("#company-d-start_date,#company-d-lead_date,#company-d-lead_expected_close_date,#company-d-lead_close_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
$("body").on("change", "#company-country_id", function () {
  $.get("'.Url::to(['suggestion/zone-options','country_id'=>'']).'"+$(this).val(),function(data){
    $("select#company-zone_id").html(data);
  });
});
$("#company-d-lead_score_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->lead_score>0 ? 'initial_value: '.$model->lead_score : '').'
});
$("#company-d-lead_score_star").on("afterChange", function(ev, data){
  $("#company-lead_score").val(data.to);
});
$("#company-d-confidence_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->confidence>0 ? 'initial_value: '.$model->confidence : '').'
});
$("#company-d-confidence_star").on("afterChange", function(ev, data){
  $("#company-confidence").val(data.to);
});
$(".numeral-input").each(function(index){
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("body").on("blur", ".numeral-input", function () {
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("#company-manager_id").select2({
	allowClear: true,
	width: "100%",
});
');
?>
<section class="company-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(['id'=>'company-form']); ?>
  <div class="hidden">
    <?= $form->field($model, 'job_title_id')->textInput()?>
    <?= $form->field($model, 'lead_score')->textInput()?>
    <?= $form->field($model, 'confidence')->textInput()?>
  </div>
  <?php if(isset($cardTitle)){?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <?php }?>
  <div class="card-body">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'start_date',['template'=>'
        {label}
        <div class="input-group date" id="company-d-start_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#company-d-start_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'website')->textInput(['maxlength' => true])?>
        <?=  $form->field($model, 'data_type')->hiddenInput(['value'=> 1])->label(false); ?>
      </div>
      <div class="col-sm-4">
          <?= $form->field($model, 'country_id')->dropDownList(Yii::$app->appHelperFunctions->countryListArr,['prompt'=>Yii::t('app','Select')])?>
        </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'client_reference')->textInput()?>
      </div>
           <div class="col-sm-4">
        <?= $form->field($model, 'zone_id')->dropDownList($zonesList)?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'address')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <?= $form->field($model, 'manager_id')->dropDownList(Yii::$app->appHelperFunctions->StaffMemberListArrBusiness,['multiple'=>'multiple'])?>
    <section class="card card-outline card-info">
      <header class="card-header">
        <h2 class="card-title"><?= Yii::t('app','Instructing Person Info')?></h2>
      </header>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-4">
            <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])?>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])?>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'job_title')->textInput(['maxlength' => true])?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])?>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>
          </div>
        </div>
      </div>
    </section>

    <?php if ($model->id <> null) { ?>

    <div class="valuation-index">


        <?php CustomPjax::begin(['id' => 'grid-container']); ?>
        <?= CustomGridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'cardTitle' => $cardTitle,
            // 'createBtn' => $createBtn,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
                ['attribute' => 'reference_number', 'label' => Yii::t('app', 'Reference')],
                ['attribute' => 'client_id',
                'format'=>'raw',
                    'label' => Yii::t('app', 'Client'),
                    'value' => function ($model) {
                        //return $model->client->title;
                        return Html::a($model->client->title,['valuation/step_1','id'=>$model['id']],['data-pjax'=>'0']);
                    },
                    'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title')
                ],

                ['attribute' => 'building_info',
                    'label' => Yii::t('app', 'Building'),
                    'value' => function ($model) {
                        return $model->building->title;
                    },
                    'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title')
                ],
                ['attribute' => 'instruction_date',
                    'label' => Yii::t('app', 'Instruction Date'),
                    'value' => function ($model) {
                        return date('d-m-Y', strtotime($model->instruction_date));
                    },
                ],
                ['attribute' => 'target_date',
                    'label' => Yii::t('app', 'Target Date'),
                    'value' => function ($model) {
                        return date('d-m-Y', strtotime($model->target_date));
                    },
                ],


                ['attribute' => 'purpose_of_valuation',
                    'label' => Yii::t('app', 'Purpose'),
                    'value' => function ($model) {
                        return Yii::$app->appHelperFunctions->purposeOfValuationArr[$model['purpose_of_valuation']];
                    },
                    'filter' => Yii::$app->appHelperFunctions->purposeOfValuationArr
                ],

                // ['format' => 'raw', 'attribute' => 'status', 'label' => Yii::t('app', 'Status'), 'value' => function ($model) {
                //     return Yii::$app->helperFunctions->arrStatusIcon[$model['status']];
                // }, 'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'], 'filter' => Yii::$app->helperFunctions->arrFilterStatus],

                ['format' => 'raw', 'attribute' => 'valuation_status', 'label' => Yii::t('app', 'Valuation Status'), 'value' => function ($model) {
                    return Yii::$app->helperFunctions->valuationStatusListArrLabel[$model['valuation_status']];
                }, 'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'], 'filter' => Yii::$app->helperFunctions->valuationStatusListArrLabel],



                ['attribute' => 'client_id',
                'format'=>'raw',
                    'label' => Yii::t('app', 'Actions'),
                    'value' => function ($model) {
                        //return $model->client->title;
                        return Html::a('Edit',['valuation/step_1','id'=>$model['id']],['data-pjax'=>'0']);
                    },
                    'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title')
                ],
            ],
        ]); ?>
        <?php CustomPjax::end(); ?>
</div>

<?php } ?>


    <section class="card card-outline card-success">
      <header class="card-header">
        <h2 class="card-title"><?= Yii::t('app','Sales & Marketing')?></h2>
      </header>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-4">
            <?= $form->field($model, 'lead_type')->dropDownList(Yii::$app->appHelperFunctions->leadTypeListArr,['prompt'=>Yii::t('app','Select')])?>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'lead_source')->dropDownList(Yii::$app->appHelperFunctions->leadSourceListArr,['prompt'=>Yii::t('app','Select')])?>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'lead_status')->dropDownList(Yii::$app->appHelperFunctions->leadStatusListArr,['prompt'=>Yii::t('app','Select')])?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <?= $form->field($model, 'lead_date',['template'=>'
            {label}
            <div class="input-group date" id="company-d-lead_date" data-target-input="nearest">
              {input}
              <div class="input-group-append" data-target="#company-d-lead_date" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
            {hint}{error}
            '])->textInput(['maxlength' => true])?>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label"><?= $model->getAttributeLabel('lead_score')?></label>
              <div>
                <div id="company-d-lead_score_star" class="rating"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'deal_status')->dropDownList(Yii::$app->appHelperFunctions->dealStatusListArr,['prompt'=>Yii::t('app','Select')])?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <?= $form->field($model, 'interest')->textInput(['maxlength' => true])?>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'deal_value',['template'=>'
            {label}
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">'.Yii::$app->appHelperFunctions->getSetting('currency_sign').'</div>
              </div>
              {input}
            </div>
            {hint}{error}
            '])->textInput(['class'=>'form-control numeral-input', 'maxlength' => true])?>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'expected_close_date',['template'=>'
            {label}
            <div class="input-group date" id="company-d-lead_expected_close_date" data-target-input="nearest">
              {input}
              <div class="input-group-append" data-target="#company-d-lead_expected_close_date" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
            {hint}{error}
            '])->textInput(['maxlength' => true])?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <?= $form->field($model, 'close_date',['template'=>'
            {label}
            <div class="input-group date" id="company-d-lead_close_date" data-target-input="nearest">
              {input}
              <div class="input-group-append" data-target="#company-d-lead_close_date" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
            {hint}{error}
            '])->textInput(['maxlength' => true])?>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label"><?= $model->getAttributeLabel('confidence')?></label>
              <div>
                <div id="company-d-confidence_star" class="rating"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
