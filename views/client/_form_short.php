<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
$zonesList=[];
if($model->country_id>0){
  $zonesList=Yii::$app->appHelperFunctions->getCountryZoneListArr($model->country_id);
}
$emiratesListArr=Yii::$app->appHelperFunctions->emiratedListArr;
$feeStructureOptions=Yii::$app->appHelperFunctions->feeStructureOptionListArr;
$feeStructureTypes=Yii::$app->helperFunctions->feeStructureTypeListArr;

$this->registerJs('
$("#company-d-start_date,#company-d-lead_date,#company-d-lead_expected_close_date,#company-d-lead_close_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
$("body").on("change", "#company-country_id", function () {
  $.get("'.Url::to(['suggestion/zone-options','country_id'=>'']).'"+$(this).val(),function(data){
    $("select#company-zone_id").html(data);
  });
});
$("#company-d-lead_score_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->lead_score>0 ? 'initial_value: '.$model->lead_score : '').'
});
$("#company-d-lead_score_star").on("afterChange", function(ev, data){
  $("#company-lead_score").val(data.to);
});
$("#company-d-confidence_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->confidence>0 ? 'initial_value: '.$model->confidence : '').'
});
$("#confidence_star").on("afterChange", function(ev, data){
  $("#company-d-company-confidence").val(data.to);
});
$(".numeral-input").each(function(index){
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("body").on("blur", ".numeral-input", function () {
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("#company-manager_id").select2({
	allowClear: true,
	width: "100%",
});
');
?>
<?php $form = ActiveForm::begin(['id'=>'company-form']); ?>
<div class="hidden">
  <?= $form->field($model, 'lead_score')->textInput()?>
  <?= $form->field($model, 'confidence')->textInput()?>
</div>
<div class="row">
  <div class="col-sm-4">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
  </div>
  <div class="col-sm-4">
    <?= $form->field($model, 'start_date',['template'=>'
    {label}
    <div class="input-group date" id="company-d-start_date" data-target-input="nearest">
      {input}
      <div class="input-group-append" data-target="#company-d-start_date" data-toggle="datetimepicker">
          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
      </div>
    </div>
    {hint}{error}
    '])->textInput(['maxlength' => true])?>
  </div>
  <div class="col-sm-4">
    <?= $form->field($model, 'website')->textInput(['maxlength' => true])?>
  </div>
</div>
<div class="row">
  <div class="col-sm-4">
    <?= $form->field($model, 'country_id')->dropDownList(Yii::$app->appHelperFunctions->countryListArr,['prompt'=>Yii::t('app','Select')])?>
  </div>
  <div class="col-sm-4">
    <?= $form->field($model, 'zone_id')->dropDownList($zonesList)?>
  </div>
  <div class="col-sm-4">
    <?= $form->field($model, 'address')->textInput(['maxlength' => true])?>
  </div>
</div>
<?= $form->field($model, 'manager_id')->dropDownList(Yii::$app->appHelperFunctions->StaffMemberListArrBusiness,['multiple'=>'multiple'])?>
<?php
if($feeStructureTypes!=null){
  foreach($feeStructureTypes as $fstKey=>$fstVal){
?>
<section class="card card-outline card-warning collapsed-card">
  <header class="card-header">
    <h2 class="card-title"><?= Yii::t('app','Agreed Fees Structure For {type} Standard Property',['type'=>$fstVal])?></h2>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse">
        <i class="fas fa-plus"></i>
      </button>
    </div>
  </header>
  <div class="card-body fee-structure">
    <table class="table table-bordered table-striped">
      <tr>
        <th><?= Yii::t('app','Emirate')?></th>
        <?php foreach($feeStructureOptions as $key=>$val){?>
        <th width="15%" class="text-center text-danger"><?= $val?></th>
        <th width="10%" class="text-center text-success"><?= Yii::t('app','TAT')?></th>
        <?php }?>
      </tr>
      <?php foreach($emiratesListArr as $key=>$val){?>
      <tr>
        <th><?= $val?></th>
        <?php
          foreach($feeStructureOptions as $fskey=>$fsval){
            $feeStructure=Yii::$app->appHelperFunctions->getCompanyFeeStructureValues($model->id,$fstKey,$key,$fskey);
            $feeValue=$feeStructure['fee'];
            $tatValue=$feeStructure['tat'];
            $model->fee_structure[$fstKey][$key][$fskey]=$feeValue;
            $model->tat[$fstKey][$key][$fskey]=$tatValue;
        ?>
        <td><?= $form->field($model, 'fee_structure['.$fstKey.']['.$key.']['.$fskey.']')->textInput(['maxlength' => true])->label(false)?></td>
        <td><?= $form->field($model, 'tat['.$fstKey.']['.$key.']['.$fskey.']')->textInput(['maxlength' => true])->label(false)?></td>
        <?php }?>
      </tr>
      <?php }?>
    </table>
  </div>
</section>
<?php
  }
}
?>
<section class="card card-outline card-success collapsed-card">
  <header class="card-header">
    <h2 class="card-title"><?= Yii::t('app','Sales & Marketing')?></h2>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse">
        <i class="fas fa-plus"></i>
      </button>
    </div>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'lead_type')->dropDownList(Yii::$app->appHelperFunctions->leadTypeListArr,['prompt'=>Yii::t('app','Select')])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'lead_source')->dropDownList(Yii::$app->appHelperFunctions->leadSourceListArr,['prompt'=>Yii::t('app','Select')])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'lead_status')->dropDownList(Yii::$app->appHelperFunctions->leadStatusListArr,['prompt'=>Yii::t('app','Select')])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'lead_date',['template'=>'
        {label}
        <div class="input-group date" id="company-d-lead_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#company-d-lead_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label class="control-label"><?= $model->getAttributeLabel('lead_score')?></label>
          <div>
            <div id="company-d-lead_score_star" class="rating"></div>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'deal_status')->dropDownList(Yii::$app->appHelperFunctions->dealStatusListArr,['prompt'=>Yii::t('app','Select')])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'interest')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'deal_value',['template'=>'
        {label}
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">'.Yii::$app->appHelperFunctions->getSetting('currency_sign').'</div>
          </div>
          {input}
        </div>
        {hint}{error}
        '])->textInput(['class'=>'form-control numeral-input', 'maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'expected_close_date',['template'=>'
        {label}
        <div class="input-group date" id="company-d-lead_expected_close_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#company-d-lead_expected_close_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'close_date',['template'=>'
        {label}
        <div class="input-group date" id="company-d-lead_close_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#company-d-lead_close_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label class="control-label"><?= $model->getAttributeLabel('confidence')?></label>
          <div>
            <div id="company-d-confidence_star" class="rating"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="group">
  <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
  <?= Html::a(Yii::t('app', 'Cancel'), 'javascript:;', ['data-dismiss' => 'modal', 'class' => 'btn btn-default']) ?>
</div>
<?php ActiveForm::end(); ?>
