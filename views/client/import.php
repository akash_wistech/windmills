<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SoldTransactionImportForm */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Import Contacts';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Prospect'), 'url' => ['prospects']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('



//   $("body").on("click", ".alert-msg", function (e) {
//     // alert("hello");
//     e.preventDefault();
// swal({
//    // alert("yellow");
//     title: "'.Yii::t('app','Confirmation').'",
//     html: "'.Yii::t('app','Are you sure you want to Import Contacts').'",
//     type: "warning",
//     showCancelButton: true,
//     confirmButtonColor: "#47a447",
//     confirmButtonText: "'.Yii::t('app','Yes').'",
//     cancelButtonText: "'.Yii::t('app','Cancel').'",
//     }).then((result) => {
//       console.log(result);
//         if (result) {
//           $("#w0").unbind("submit").submit();
//         }
//         });
// });



');

?>



<section class="card card-outline card-info  append-card">
  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
      <div class="row mx-2 my-2">
            <label for="exampleInputFile">File input</label>
            <div class="input-group">
              <div class="custom-file">
                <input style="padding-bottom: 6px;" type="file" class="custom-file-input" id="company-importfile" name="Company[importfile]">
                <label class="custom-file-label">upload csv file</label>
              </div>
            </div>
      </div>
      <div class="row mx-2 my-2">
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success alert-msg'])?>
        <?=Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default mx-2'])?>
      </div>
    <?php ActiveForm::end();?>
</section>



