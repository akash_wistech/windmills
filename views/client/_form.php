<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\ClientFormAsset;
use kartik\select2\Select2;
ClientFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
$zonesList=[];
if($model->country_id>0){
    $zonesList=Yii::$app->appHelperFunctions->getCountryZoneListArr($model->country_id);
}
$emiratesListArr=Yii::$app->appHelperFunctions->emiratedListArr;
$feeStructureOptions=Yii::$app->appHelperFunctions->feeStructureOptionListArr;
$feeStructureTypes=Yii::$app->helperFunctions->feeStructureTypeListArr;

// if($model->primaryContact!=null){
//     // echo "<pre>"; print_r($model->primaryContact); echo "</pre>"; die();

//   $model->firstname=$model->primaryContact->firstname;
//   $model->lastname=$model->primaryContact->lastname;
//   $model->job_title=$model->primaryContact->profileInfo!=null && $model->primaryContact->profileInfo->jobTitle!=null ? $model->primaryContact->profileInfo->jobTitle->title : '';
//   $model->job_title_id=$model->primaryContact->profileInfo->job_title_id;
//   $model->mobile=$model->primaryContact->profileInfo->mobile;
//   $model->email=$model->primaryContact->email;
// }

$actionBtns='Edit';

// die('here');



?>

<script>

</script>
<section class="company-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['id'=>'company-form']); ?>
    <div class="hidden">
        <?= $form->field($model, 'job_title_id')->textInput()?>
        <?= $form->field($model, 'lead_score')->textInput()?>
        <?= $form->field($model, 'confidence')->textInput()?>
    </div>
    <?php if(isset($cardTitle)){?>
        <header class="card-header">
            <h2 class="card-title"><?= $cardTitle ?></h2>
        </header>
    <?php } ?>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <?php

                if($model->status == 1){
                    $readonly = true;
                }else{
                    $readonly = false;
                }

                ?>
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Title')?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'nick_name')->textInput(['maxlength' => true])->label('Nick Name')?>
            </div>
            <div class="col-sm-4">
            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'main_client_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Clients::find()
                       // ->where(['status' => 1])
                        ->orderBy(['title' => SORT_ASC,])
                        ->all(), 'id', 'title'),

                    'options' => ['placeholder' => 'Select Client ...', 'class'=> 'client-cls'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Client Name');
                ?>

            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'client_department')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\ClientDepartments::find()
                        ->where(['status' => 1])
                        ->orderBy(['title' => SORT_ASC,])
                        ->all(), 'id', 'title'),

                    'options' => ['placeholder' => 'Select Department ...', 'class'=> 'client-cls'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Department');
                ?>

            </div>


            <div class="col-4">
                <?= $form->field($model, 'client_type')->dropDownList(yii::$app->quotationHelperFunctions->clienttype)?>
            </div>

        </div>

        <!-- </div>-->
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'start_date',['template'=>'
        {label}
        <div class="input-group date" id="company-d-start_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#company-d-start_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'website')->textInput(['maxlength' => true])?>
                <?=  $form->field($model, 'data_type')->hiddenInput(['value'=> 0])->label(false); ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'country_id')->dropDownList(Yii::$app->appHelperFunctions->countryListArr,['prompt'=>Yii::t('app','Select')])?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'client_reference')->textInput()?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'zone_id')->dropDownList($zonesList)?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'readonly'=>false,])?>
            </div>
        </div>
        
        
        <div class="row d-none">
                <?= $form->field($model, 'searchInput')->textInput(['maxlength' => true, 'class'=>'my-1 form-control' , 'id'=>'searchInput', 'value'=>($model->address<>null) ? $model->address : ''])->label(false) ?>
            <div class="col-6">
                <div class="map" id="map" style="width: 100%; height: 250px;"></div>
            </div>


            <div class="col-6">
                <?= $form->field($model, 'location')->hiddenInput(['maxlength' => true, 'readonly'=>true, 'id'=>'location', 'class'=>'my-2 form-control'])->label(false) ?>
                
                
                <?= $form->field($model, 'location_url')->textInput(['maxlength' => true, 'readonly'=>true, 'id'=>'locationUrl', 'class'=>'my-2 form-control'])->label("Map Url") ?>

                <?= $form->field($model, 'lat')->textInput(['maxlength' => true, 'readonly'=>true, 'id'=>'lat'])->label("Latitude") ?>

                <?= $form->field($model, 'lng')->textInput(['maxlength' => true, 'readonly'=>true, 'id'=>'lng'])->label("Longitude") ?>
                
            </div>


        </div>
        
        
        
        <div class="row">
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'print_report')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Hard Copy Reports');
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'land_valutaion')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Land Market Value');
                ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'land_age')->textInput(['maxlength' => true,'type' => 'number'])->label('Minimum Age for Land MV')?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'vat')->dropDownList(Yii::$app->appHelperFunctions->getvatArr())->label('Vat for Quotation')?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'zohobooks_id')->textInput(['maxlength' => 255,'type' => 'varchar'])->label('ZohoBook ID')?>
            </div>
            <?php if(Yii::$app->user->identity->id == 33 || Yii::$app->user->identity->id == 1) { ?>
                <div class="col-sm-4">
                    <?= $form->field($model, 'quickbooks_registration_id')->textInput(['maxlength' => true,'type' => 'number'])->label('QuickBook ID')?>
                </div>
            <?php } ?>
            <?php if(isset($model->id) && $model->id > 0) {
                if ($model->client_type == 'bank') {

                    ?>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'relative_discount')->widget(\kartik\select2\Select2::classname(), [
                            'data' => yii::$app->quotationHelperFunctions->relativediscount,
                            'options' => ['placeholder' => 'Select a Relative Discount ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                    </div>
                <?php }
            }?>
            <div class="col-sm-4">
                <?= $form->field($model, 'report_password')->textInput(['maxlength' => true])->label('Report Password')?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'fee_master_file_vat')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Fee Master file Vat');
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'client_invoice_customer')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Client Require Customer Invoice');
                ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'trn_number')->textInput(['maxlength' => true])->label('TRN')?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'send_monthly_report')->dropDownList([0=>'No', 1=>'Yes'])?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'send_monthly_quotation_invoice')->dropDownList([0=>'No', 1=>'Yes'])?>
            </div>
            <?php if(isset($model->id) && $model->id == 1) {
            ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'client_fixed_fee')->textInput(['maxlength' => true,'type' => 'number'])->label('Shaikh Zayed case fixed fee')?>
            </div>
            <?php } ?>
            
            
            <div class="col-sm-4">
                <?= $form->field($model, 'allow_for_valuation')->dropDownList([0=>'No', 1=>'Yes'])?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'instruction_via_quotation')->dropDownList([0=>'No', 1=>'Yes'])?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'fee_with_bua_check')->dropDownList([0=>'No', 1=>'Yes'])?>
            </div>

<?php
if ($model->client_type == 'bank') {
?>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'p_status_1')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Daily Status Report (7:00 am)');
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'p_status_2')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Daily Status Report (01:00 pm)');
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'p_status_3')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Daily Status Report (05:00 pm)');
                ?>
            </div>
            <?php } ?>

        </div>
        <!--
    <?/*= $form->field($model, 'manager_id')->dropDownList(Yii::$app->appHelperFunctions->StaffMemberListArrBusiness,['multiple'=>'multiple'])*/?>
     -->


        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app','<b>Instructing Person Info</b>')?></h2>
            </header>

            <div class="card-body instructing-person-append-section">
                <?php
                $i=0;
                if($model->allContacts!=null){
                    foreach ($model->allContacts as $key => $value) {
                       /* echo "<pre>";
                        print_r($model->allContacts);
                        die;*/
                        // echo "<pre>"; print_r($value); echo "</pre>"; echo "<br>"; die();
                        // echo "<pre>"; print_r($value->firstname); echo "</pre>"; echo "<br>"; die();
                        $jobTitle = $value->profileInfo!=null && $value->profileInfo->jobTitle!=null ? $value->profileInfo->jobTitle->title : '';
                        ?>

                        <section class="card card-outline append-card <?= ($value->profileInfo->primary_contact==1) ? '' : 'card-info'?>">
                            <?php
                            if ($value->profileInfo->primary_contact==1) {?>
                                <header class="card-header bg-success">
                                    <h2 class="card-title"><?= Yii::t('app','<b>Primary Contact</b>')?></h2>
                                </header>
                                <?php
                            }
                            ?>
                            <div class="row mx-2 my-2">
                                <input type="hidden" name="Company[InstructingPersonInfo][<?=$i?>][contant_id]" value="<?= $value->id ?>">
                                <?php
                                    $allowed_users = array(1,110465);
                                    if(in_array(Yii::$app->user->identity->id, $allowed_users)) {
                                        if ($value->profileInfo->primary_contact !== 1) { ?>
                                            <div class="col-sm-12 text-right float-end ">
                                                <a href="client/makeprimarycontact/<?= $value->id ?>">Make Primary Contact</a>
                                            </div>
                                            <?php
                                        }
                                    }
                                ?>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-firstname">
                                        <label class="control-label" for="company-firstname">Firstname</label>
                                        <input type="text" id="company-firstname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][firstname]" value="<?= $value->firstname ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-lastname">
                                        <label class="control-label" for="company-lastname">Lastname</label>
                                        <input type="text" id="company-lastname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][lastname]" value="<?= $value->lastname ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-job_title">
                                        <label class="control-label" for="company-job_title">Job Title</label>
                                        <input type="text" id="company-job_title" class="form-control job-title-cls" name="Company[InstructingPersonInfo][<?=$i?>][job_title]" value="<?= $jobTitle ?>" autocomplete="off">
                                        <input type="hidden" class="job-title-id-field" name="Company[InstructingPersonInfo][<?=$i?>][job_title_id]" value="<?= $value->profileInfo->job_title_id ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-4 mobile">
                                    <label class="control-label" for="company-mobile">Mobile <span class="text-danger"></span></label>
                                    <div class="input-group phone-parent">
                                        <div class="input-group-prepend">
                                            <select name='Company[InstructingPersonInfo][<?=$i?>][mobile_code]' class="form-control phone-code">
                                                <option value="" <?php if (isset($model->mobile_code) && ($model->mobile_code == '')) {
                                                    echo 'selected';
                                                } ?>></option>
                                                <option value="050" <?php if (isset($model->mobile_code) && ($model->mobile_code == '050')) {
                                                    echo 'selected';
                                                } ?>>050</option>
                                                <option value="052" <?php if (isset($model->mobile_code) && ($model->mobile_code == '052')) {
                                                    echo 'selected';
                                                } ?>>052</option>
                                                <option value="054" <?php if (isset($model->mobile_code) && ($model->mobile_code == '054')) {
                                                    echo 'selected';
                                                } ?>>054</option>
                                                <option value="055" <?php if (isset($model->mobile_code) && ($model->mobile_code == '055')) {
                                                    echo 'selected';
                                                } ?>>055</option>
                                                <option value="056" <?php if (isset($model->mobile_code) && ($model->mobile_code == '056')) {
                                                    echo 'selected';
                                                } ?>>056</option>
                                                <option value="058" <?php if (isset($model->mobile_code) && ($model->mobile_code == '058')) {
                                                    echo 'selected';
                                                } ?>>058</option>
                                            </select>
                                        </div>
                                        <input type="text" id="company-mobile<?=$i?>" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][mobile]" value="<?= $value->profileInfo->mobile ?>">
                                    </div>
                                </div> -->



                                <div class="col-sm-4">
                                    <div class="form-group field-company-mobile">
                                        <label class="control-label" for="company-mobile">Mobile1</label>
                                        <input type="text" id="company-mobile" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][mobile]" value="<?= $value->profileInfo->mobile ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group field-company-email">
                                        <label class="control-label" for="company-email">Email</label>
                                        <input type="text" id="company-email" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][email]" value="<?= $value->email ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <?php
                                if ($value->profileInfo->primary_contact!==1) {?>
                                    <div class="col-sm-4 my-2 py-4 text-right">
                                        <button type="button" data-toggle="tooltip" title="Remove" class="btn btn-danger remove-apnd-card"><i class="fa fa-minus-circle"></i></button>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                        </section>

                        <?php
                        $i++;
                    }
                }else{?>
                    <section class="card card-outline card-info  append-card">
                        <div class="row mx-2 my-2">
                            <div class="col-sm-4">
                                <div class="form-group field-company-firstname">
                                    <label class="control-label" for="company-firstname">Firstname</label>
                                    <input type="text" id="company-firstname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][firstname]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-lastname">
                                    <label class="control-label" for="company-lastname">Lastname</label>
                                    <input type="text" id="company-lastname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][lastname]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-job_title">
                                    <label class="control-label" for="company-job_title">Job Title</label>
                                    <input type="text" id="company-job_title" class="form-control job-title-cls" name="Company[InstructingPersonInfo][<?=$i?>][job_title]" autocomplete="off">
                                    <input type="hidden" class="job-title-id-field" name="Company[InstructingPersonInfo][<?=$i?>][job_title_id]" value="<?= $value->profileInfo->job_title_id ?>">
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <!-- <div class="col-sm-4 mobile">
                                <label class="control-label" for="company-mobile">Mobile <span class="text-danger"></span></label>
                                <div class="input-group phone-parent">
                                    <div class="input-group-prepend">
                                        <select name='Company[InstructingPersonInfo][<?=$i?>][mobile_code]' class="form-control phone-code">
                                            <option value="050" <?php if (isset($model->mobile_code) && ($model->mobile_code == '050')) {
                                                echo 'selected';
                                            } ?>>050</option>
                                            <option value="052" <?php if (isset($model->mobile_code) && ($model->mobile_code == '052')) {
                                                echo 'selected';
                                            } ?>>052</option>
                                            <option value="054" <?php if (isset($model->mobile_code) && ($model->mobile_code == '054')) {
                                                echo 'selected';
                                            } ?>>054</option>
                                            <option value="055" <?php if (isset($model->mobile_code) && ($model->mobile_code == '055')) {
                                                echo 'selected';
                                            } ?>>055</option>
                                            <option value="056" <?php if (isset($model->mobile_code) && ($model->mobile_code == '056')) {
                                                echo 'selected';
                                            } ?>>056</option>
                                            <option value="058" <?php if (isset($model->mobile_code) && ($model->mobile_code == '058')) {
                                                echo 'selected';
                                            } ?>>058</option>
                                        </select>
                                    </div>
                                    <input type="text" id="company-mobile<?=$i?>" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][mobile]">
                                </div>
                            </div> -->

                            
                            <div class="col-sm-4">
                                <div class="form-group field-company-mobile">
                                    <label class="control-label" for="company-mobile">Mobile2</label>
                                    <input type="text" id="company-mobile" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][mobile]">
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group field-company-email">
                                    <label class="control-label" for="company-email">Email</label>
                                    <input type="text" id="company-email" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][email]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php
                    $i++;
                }
                ?>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-sm-10"></div>
                    <div class="col-sm-2 text-right">
                        <button type="button"  data-toggle="tooltip" title="Add" class="btn btn-primary create-instructing-person"><i class="fa fa-plus-circle"></i></button>
                    </div>
                </div>
            </div>
        </section>





























        <section class="valuation-form card card-outline card-primary">

            <header class="card-header">
                <h2 class="card-title">CC Emails for Daily Status Reports</h2>
            </header>
            <div class="card-body">

                <table id="attachment" class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td class="text-left">Name</td>
                        <td class="text-left">Email</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $row = 0; ?>
                    <?php



                    if(isset($model->customEmails) && ($model->customEmails <> null)) {
                        foreach ($model->customEmails as $attachment) { ?>
                            <tr id="image-row-attachment-<?php echo $row; ?>">

                                <td>
                                    <input type="text" class="form-control"
                                           name="Company[customEmailsStatus][<?= $row ?>][name]"
                                           value="<?= $attachment->name ?>" placeholder="Name" required/>
                                </td>

                                <td>
                                    <input type="email" class="form-control"
                                           name="Company[customEmailsStatus][<?= $row ?>][email]"
                                           value="<?= $attachment->email ?>" placeholder="Email" required/>
                                </td>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsStatus][<?= $row ?>][id]"
                                       value="<?= $attachment->id ?>" placeholder="Name" required/>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsStatus][<?= $row ?>][type]"
                                       value="<?= $attachment->type ?>" placeholder="Name" required/>

                                <td class="text-left">
                                    <button type="button"
                                            onclick="deleteRow('-attachment-<?= $row ?>', '<?= $attachment->id; ?>', 'attachment')"
                                            data-toggle="tooltip" title="You want to delete Attachment"
                                            class="btn btn-danger"><i
                                                class="fa fa-minus-circle"></i></button>
                                </td>
                            </tr>
                            <?php $row++; ?>
                        <?php }
                    }?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2"></td>
                        <td class="text-left">
                            <button type="button" onclick="addAttachment();" data-toggle="tooltip" title="Add"
                                    class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </section>
        <section class="valuation-form card card-outline card-primary">

            <header class="card-header">
                <h2 class="card-title">CC Emails for Auto Emails</h2>
            </header>
            <div class="card-body">

                <table id="attachment-auto" class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td class="text-left">Name</td>
                        <td class="text-left">Email</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $row_auto = 0; ?>
                    <?php



                    if(isset($model->customEmailsVals) && ($model->customEmailsVals <> null)) {
                        foreach ($model->customEmailsVals as $attachment) { ?>
                            <tr id="image-row-attachment-auto-<?php echo $row_auto; ?>">

                                <td>
                                    <input type="text" class="form-control"
                                           name="Company[customEmailsAuto][<?= $row_auto ?>][name]"
                                           value="<?= $attachment->name ?>" placeholder="Name" required/>
                                </td>

                                <td>
                                    <input type="email" class="form-control"
                                           name="Company[customEmailsAuto][<?= $row_auto ?>][email]"
                                           value="<?= $attachment->email ?>" placeholder="Email" required/>
                                </td>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsAuto][<?= $row_auto ?>][id]"
                                       value="<?= $attachment->id ?>" placeholder="Name" required/>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsAuto][<?= $row_auto ?>][type]"
                                       value="<?= $attachment->type ?>" placeholder="Name" required/>

                                <td class="text-left">
                                    <button type="button"
                                            onclick="deleteRowauto('-attachment-auto-<?= $row_auto ?>', '<?= $attachment->id; ?>', 'attachment')"
                                            data-toggle="tooltip" title="You want to delete Attachment"
                                            class="btn btn-danger"><i
                                                class="fa fa-minus-circle"></i></button>
                                </td>
                            </tr>
                            <?php $row_auto++; ?>
                        <?php }
                    }?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2"></td>
                        <td class="text-left">
                            <button type="button" onclick="addAttachmentauto();" data-toggle="tooltip" title="Add"
                                    class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </section>

        <section class="card card-outline card-success">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app','Sales & Marketing')?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'lead_type')->dropDownList(Yii::$app->appHelperFunctions->leadTypeListArr,['prompt'=>Yii::t('app','Select')])?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'lead_source')->dropDownList(Yii::$app->appHelperFunctions->leadSourceListArr,['prompt'=>Yii::t('app','Select')])?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'lead_status')->dropDownList(Yii::$app->appHelperFunctions->leadStatusListArr,['prompt'=>Yii::t('app','Select')])?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'lead_date',['template'=>'
            {label}
            <div class="input-group date" id="company-d-lead_date" data-target-input="nearest">
              {input}
              <div class="input-group-append" data-target="#company-d-lead_date" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
            {hint}{error}
            '])->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?= $model->getAttributeLabel('lead_score')?></label>
                            <div>
                                <div id="company-d-lead_score_star" class="rating"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'deal_status')->dropDownList(Yii::$app->appHelperFunctions->dealStatusListArr,['prompt'=>Yii::t('app','Select')])?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'interest')->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'deal_value',['template'=>'
            {label}
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">'.Yii::$app->appHelperFunctions->getSetting('currency_sign').'</div>
              </div>
              {input}
            </div>
            {hint}{error}
            '])->textInput(['class'=>'form-control numeral-input', 'maxlength' => true])?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'expected_close_date',['template'=>'
            {label}
            <div class="input-group date" id="company-d-lead_expected_close_date" data-target-input="nearest">
              {input}
              <div class="input-group-append" data-target="#company-d-lead_expected_close_date" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
            {hint}{error}
            '])->textInput(['maxlength' => true])?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'close_date',['template'=>'
            {label}
            <div class="input-group date" id="company-d-lead_close_date" data-target-input="nearest">
              {input}
              <div class="input-group-append" data-target="#company-d-lead_close_date" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
            {hint}{error}
            '])->textInput(['maxlength' => true])?>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?= $model->getAttributeLabel('confidence')?></label>
                            <div>
                                <div id="company-d-confidence_star" class="rating"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app','Documents')?></h2>
            </header>
            <div class="card-body">
                <div class="col child-col border py-2">
                    <?= $form->field($model, 'signed_toe', ['labelOptions'=>['style'=>'padding:9px; color:#E1F5FE; background-color:#0277BD;border-radius:3px;']])->fileInput(['class'=>'imgInps d-none']) ?>

                    <p style="font-weight: bold; font-size:18px; color:red;">Upload Only Pdf</p>
                    <?php

                    $imgname = Yii::$app->params['dummy_image_address'];
                    if ($model->signed_toe != null) {
                        if($model->segment_type == 11){
                            $imgname = $model->signed_toe;

                        }else {
                            $toe = 'signed-toe/images/' . $model->signed_toe;
                            $imgname = Yii::$app->get('olds3bucket')->getUrl('signed-toe/images/' . $model->signed_toe);
                            $imgname = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($toe), '+10 minutes');
                        }
                        // echo $imgname; die();
                        $link=$imgname;
                    }
                    ?>
                    <div style='width:350px; margin-bottom:30px; position:relative;'>
                        <?php

                        if (strpos($imgname,'.pdf') == true) {
                            $imgname=Yii::$app->params['uploadPdfIcon'];
                        }

                        ?>
                        <img class="blah" src="<?= $imgname ?>" alt="No Image is selected." width="200px"/>

                        <?php
                        if ($model->signed_toe != null) {
                            ?>
                            <a style="color:#ffffff; font-weight: bold; padding-left:7px; padding-right :7px; font-size:22px; position:absolute;  bottom:10px; left:20px; background-color:#558B2F;";
                               href="<?= $link ?>" target="_blank"><i class="fas fa-eye"></i></a>
                        <?php } ?>

                    </div>
                    <div>
                        <?php

                        $scanOfficer=app\models\User::find()->where(['id'=>Yii::$app->user->identity->id, 'permission_group_id'=>11])->select(['id'])->one();
                        if ($scanOfficer['id'] !=null) {
                            echo $form->field($model, 'sig_verification_status')->checkbox(['class'=>'mt-4']);
                        } ?></div>
                </div>
            </div>
        </section>

        <?php   if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){ ?>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Verification') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'status')->widget(\kartik\select2\Select2::classname(), [
                                'data' => array( '2' => 'Unverified','1' => 'Verified'),
                            ]);
                            ?>
                        </div>


                    </div>
                </div>
            </div>
        </section>
        <?php } ?>




    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>      
        <?php 
            if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                ]);
            }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>


<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs&callback=initAutocomplete&libraries=places&v=weekly"
    async defer>
</script>


<script type="text/javascript">
    var row = <?= $row ?>;
    function addAttachment() {

        html = '<tr id="image-row' + row + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="Company[customEmailsStatus][' + row + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="email" class="form-control"  name="Company[customEmailsStatus][' + row + '][email]" value="" placeholder="Email" required />';
        html += '    </div>';
        html += '  </td>';

        html += '<input type="hidden" class="form-control" name="Company[customEmailsStatus][<?= $row ?>][type]" value="1" placeholder="Name" required/>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment tbody').append(html);

        row++;
    }


    var row_auto = <?= $row_auto ?>;
    function addAttachmentauto() {

        html = '<tr id="image-row-auto' + row_auto + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="Company[customEmailsAuto][' + row_auto + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="email" class="form-control"  name="Company[customEmailsAuto][' + row_auto + '][email]" value="" placeholder="Email" required />';
        html += '    </div>';
        html += '  </td>';

        html += '<input type="hidden" class="form-control" name="Company[customEmailsAuto][<?= $row_auto ?>][type]" value="1" placeholder="Name" required/>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row-auto' + row_auto + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment-auto tbody').append(html);

        row_auto++;
    }


    function deleteRow(rowId, docID, type) {



        var url = '<?= \yii\helpers\Url::to('/client/remove-attachment'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if(response.status == 'exist'){
                            swal("Warning!", response.message, "warning");
                        }else{
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
    function deleteRowauto(rowId, docID, type) {



        var url = '<?= \yii\helpers\Url::to('/client/remove-attachment'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if(response.status == 'exist'){
                            swal("Warning!", response.message, "warning");
                        }else{
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
</script>




<?php

$this->registerJs('

    function AutoCompleteFunction(){
        $(".job-title-cls").autocomplete({
            serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
            noCache: true,
            onSelect: function(suggestion) {
                // if($(".company-job_title_id").val()!=suggestion.data){
                    // $(".job-title-id-field").val(suggestion.data);
                    $(this).parents(".append-card").find(".job-title-id-field").val(suggestion.data);
                // }
            },
            onInvalidateSelection: function() {
                $(this).parents(".append-card").find(".job-title-id-field").val("0");
                // $(".job-title-id-field").val("0");
            }
        });
    }
AutoCompleteFunction();


  $("body").on("click", ".create-instructing-person", function() {
        addInstructingPerson()
  });


	var add_Rows = '.$i.';
	function addInstructingPerson(){
		html = "";
		html += "<section class=\"card card-outline card-info append-card\">";
		html += "<div class=\"row mx-2 my-2\">";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-firstname\">";
		html +=         "<label class=\"control-label\" for=\"company-firstname\">Firstname</label>";
		html +=         "<input type=\"text\" id=\"company-firstname\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][firstname]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-lastname\">";
		html +=         "<label class=\"control-label\" for=\"company-lastname\">Lastname</label>";
		html +=         "<input type=\"text\" id=\"company-lastname\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][lastname]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-job_title\">";
		html +=         "<label class=\"control-label\" for=\"company-job_title\">Job Title</label>";
		html +=         "<input type=\"text\" id=\"company-job_title\" class=\"form-control job-title-cls\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][job_title]\" autocomplete=\"off\">";
		html +=         "<input type=\"hidden\" class=\"job-title-id-field\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][job_title_id]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";



        // html += "<div class=\"col-sm-4 mobile\">";
        // html +=     "<label class=\"control-label\" for=\"company-mobile\">Mobile <span class=\"text-danger\"></span></label>";
        // html +=     "<div class=\"input-group phone-parent\">";
        // html +=        "<div class=\"input-group-prepend\">";
        // html +=            "<select name=\"Company[InstructingPersonInfo]["+add_Rows+"][mobile_code]\" class=\"form-control phone-code\">";
        // html +=                "<option value=\"050\"  >050</option>";
        // html +=                "<option value=\"052\"  >052</option>";
        // html +=                "<option value=\"054\"  >054</option>";
        // html +=                "<option value=\"055\"  >055</option>";
        // html +=                "<option value=\"056\"  >056</option>";
        // html +=                "<option value=\"058\"  >058</option>";
        // html +=            "</select>";
        // html +=        "</div>";
        // html +=        "<input type=\"text\" id=\"company-mobile\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][mobile]\">";
        // html +=    "</div>";
        // html += "</div>";




		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-mobile\">";
		html +=         "<label class=\"control-label\" for=\"company-mobile\">Mobile</label>";
		html +=         "<input type=\"text\" id=\"company-mobile\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][mobile]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-email\">";
		html +=         "<label class=\"control-label\" for=\"company-email\">Email</label>";
		html +=         "<input type=\"text\" id=\"company-email\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][email]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4 my-2 py-4 text-right\">";
		html +=     "<button type=\"button\" data-toggle=\"tooltip\" title=\"Remove\" class=\"btn btn-danger remove-apnd-card\"><i class=\"fa fa-minus-circle\"></i></button>";
		html += "</div>";

		html += "</div>";
		html += "</section>";

        // console.log(html);
        $(".instructing-person-append-section").append(html);
        add_Rows++;
        AutoCompleteFunction();	 
    }









$("#company-d-start_date,#company-d-lead_date,#company-d-lead_expected_close_date,#company-d-lead_close_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
$("body").on("change", "#company-country_id", function () {
  $.get("'.Url::to(['suggestion/zone-options','country_id'=>'']).'"+$(this).val(),function(data){
    $("select#company-zone_id").html(data);
  });
});
$("#company-d-lead_score_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->lead_score>0 ? 'initial_value: '.$model->lead_score : '').'
});
$("#company-d-lead_score_star").on("afterChange", function(ev, data){
  $("#company-lead_score").val(data.to);
});
$("#company-d-confidence_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->confidence>0 ? 'initial_value: '.$model->confidence : '').'
});
$("#company-d-confidence_star").on("afterChange", function(ev, data){
  $("#company-confidence").val(data.to);
});
$(".numeral-input").each(function(index){
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("body").on("blur", ".numeral-input", function () {
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("#company-manager_id").select2({
	allowClear: true,
	width: "100%",
});


    
    

  $("body").on("click", ".remove-apnd-card", function() {
    $(this).parents(".append-card").remove();
  });
  
  
  
     function initAutocomplete() {
        // Initialize the map and other components here
        initialize();
    } 
  
  var map, marker, geocoder, infowindow;
    
  function initialize() {
      
      var latdefault = 25.276987;
      var lngdefault = 55.296249;
      
      var client_lat = $("#lat").val();
      var client_lng = $("#lng").val();
      
      if(client_lat != ""){
          latdefault = parseFloat(client_lat);
      }
      if(client_lng != ""){
          lngdefault = parseFloat(client_lng);
      }
      
      var latlng = new google.maps.LatLng(latdefault,lngdefault);
      
      map = new google.maps.Map(document.getElementById("map"), {
          center: latlng,
          zoom: 13
      });
      marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: true,
          anchorPoint: new google.maps.Point(0, -29)
      });
      var input = document.getElementById("searchInput");
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      geocoder = new google.maps.Geocoder();
      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.bindTo("bounds", map);
      infowindow = new google.maps.InfoWindow();
      
      autocomplete.addListener("place_changed", function() {            
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
              window.alert("Autocompletes returned place contains no geometry");
              return;
          }
          
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
          } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17);
          }
          
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);
          const locationUrl = `https://www.google.com/maps/search/?api=1&query=${place.name},${place.geometry.location.lat()},${place.geometry.location.lng()}`;

          bindDataToForm(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng(), locationUrl);
          infowindow.setContent(place.formatted_address);
          infowindow.open(map, marker);
          
      });
      
      // this function will work on marker move event into map
      google.maps.event.addListener(marker, "dragend", function() {
          geocoder.geocode({
              "latLng": marker.getPosition()
          }, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                  if (results[0]) {
                      bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng());
                      infowindow.setContent(results[0].formatted_address);
                      infowindow.open(map, marker);
                  }
              }
          });
      });
  }
  
  function bindDataToForm(address,lat,lng, locationUrl){
      document.getElementById("location").value = address;
      document.getElementById("lat").value = lat;
      document.getElementById("lng").value = lng;
      document.getElementById("searchInput").value = address;
      document.getElementById("company-address").value = address;
      document.getElementById("locationUrl").value = locationUrl;
  }
  
  function updateLocation(address, lat, lng) {
      
      var latlng = new google.maps.LatLng(lat, lng);
      marker.setPosition(latlng);
      map.setCenter(latlng);
      bindDataToForm(address, lat, lng);
      
  }
  
  window.addEventListener("load", initialize);
      

   
      
      
');
?>
