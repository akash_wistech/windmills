<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\ActivityLogWidget;
use app\assets\ClientDetailAsset;
ClientDetailAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = Yii::t('app', 'Clients');
$cardTitle = Yii::t('app','View Client:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

$emiratesListArr=Yii::$app->appHelperFunctions->emiratedListArr;
$feeStructureOptions=Yii::$app->appHelperFunctions->feeStructureOptionListArr;
$feeStructureTypes=Yii::$app->helperFunctions->feeStructureTypeListArr;

$this->registerJs('
$("#company-d-lead_score_star").rate({
  readonly: true,
  '.($model->lead_score>0 ? 'initial_value: '.$model->lead_score : '').'
});
$("#company-d-confidence_star").rate({
  readonly: true,
  '.($model->confidence>0 ? 'initial_value: '.$model->confidence : '').'
});
$(".numeral-span").each(function(index){
  //$(this).html(numeral($(this).html()).format("0,0.00"));
});
');
?>
<div class="company-view">
  <section class="card card-outline card-primary">
    <header class="card-header">
      <h2 class="card-title"><?= $model->title?></h2>
      <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){?>
      <div class="card-tools">

          <a href="<?= Url::to(['valuation/invoice_monthly_new','id'=>$model['id']])?>" target="_blank" class="btn btn-success">
             Last Month Invoice (Instruction)
          </a>
              <?php if($model['id']== 1){ ?>

              <a href="<?= Url::to(['valuation/invoice_monthly_new_qt','id'=>$model['id']])?>" target="_blank" class="btn btn-success">
                  Last Month Invoice Customers (Instruction)
              </a>

          <?php } ?>
        <a href="<?= Url::to(['update','id'=>$model['id']])?>" class="btn btn-tool">
          <i class="fas fa-edit"></i>
        </a>


      </div>
      <?php }?>
    </header>
    <div class="card-body multi-cards">
      <div class="row">
        <div class="col-sm-12">
          <section class="card card-outline card-info mb-3">
            <header class="card-header">
              <h2 class="card-title"><?= Yii::t('app','General Info')?></h2>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </header>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Created:').'</strong> '.Yii::$app->formatter->asDate($model->created_at);?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Relation Started:').'</strong> '.Yii::$app->formatter->asDate($model->start_date);?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Website:').'</strong> '.$model['website'];?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Country:').'</strong> '.$model->countryName;?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','State / Province:').'</strong> '.$model->zoneName;?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','City:').'</strong> '.$model->city;?>
                </div>
              </div>
              <div>
                <?= '<strong>'.Yii::t('app','Address:').'</strong><br />'.nl2br($model->address);?>
              </div>
              <div>
                <?= '<strong>'.Yii::t('app','Description:').'</strong><br />'.nl2br($model->descp);?>
              </div>
            </div>
          </section>
          <section class="card card-outline card-info mb-3">
            <header class="card-header">
              <h2 class="card-title"><?= Yii::t('app','Instructing Person Info')?></h2>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </header>
            <div class="card-body">
              <?php if($model->primaryContact!=null){?>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Name:').'</strong> '.$model->primaryContact->name;?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Job Title:').'</strong> '.($model->primaryContact->profileInfo!=null && $model->primaryContact->profileInfo->jobTitle!=null ? $model->primaryContact->profileInfo->jobTitle->title : '');?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Mobile:').'</strong> '.($model->primaryContact->profileInfo!=null ? $model->primaryContact->profileInfo->mobile : '');?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Email:').'</strong> '.$model->primaryContact->email;?>
                </div>
              </div>
              <?php }?>
            </div>
          </section>
          <?php
          if($feeStructureTypes!=null){
            foreach($feeStructureTypes as $fstKey=>$fstVal){
          ?>
          <section class="card card-outline card-info mb-3">
            <header class="card-header">
              <h2 class="card-title"><?= Yii::t('app','Agreed Fees Structure For {type} Standard Property',['type'=>$fstVal])?></h2>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </header>
            <div class="card-body">
              <table class="table table-bordered table-striped">
                <tr>
                  <th><?= Yii::t('app','Emirate')?></th>
                  <?php foreach($feeStructureOptions as $key=>$val){?>
                  <th width="15%" class="text-center text-danger"><?= $val?></th>
                  <th width="10%" class="text-center text-success"><?= Yii::t('app','TAT')?></th>
                  <?php }?>
                </tr>
                <?php foreach($emiratesListArr as $key=>$val){?>
                <tr>
                  <th><?= $val?></th>
                  <?php
                    foreach($feeStructureOptions as $fskey=>$fsval){
                      $feeStructure=Yii::$app->appHelperFunctions->getCompanyFeeStructureValues($model->id,$fstKey,$key,$fskey);
                      $feeValue=$feeStructure['fee'];
                      $tatValue=$feeStructure['tat'];
                  ?>
                  <td align="center"><?= $feeValue?></td>
                  <td align="center"><?= $tatValue?></td>
                  <?php }?>
                </tr>
                <?php }?>
              </table>
            </div>
          </section>
          <?php
            }
          }
          ?>
          <section class="card card-outline card-info mb-3">
            <header class="card-header">
              <h2 class="card-title"><?= Yii::t('app','Sales & Marketing')?></h2>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </header>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Lead Type:').'</strong> '.($model->lead_type>0 ? Yii::$app->appHelperFunctions->leadTypeListArr[$model->lead_type] : '');?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Lead Source:').'</strong> '.($model->lead_source>0 ? Yii::$app->appHelperFunctions->leadSourceListArr[$model->lead_source] : '');?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Lead Status:').'</strong> '.($model->lead_status>0 ? Yii::$app->appHelperFunctions->leadStatusListArr[$model->lead_status] : '');?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Lead Date:').'</strong> '.($model->lead_date!=null ? Yii::$app->formatter->asDate($model->lead_date) : '');?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Deal Status:').'</strong> '.($model->deal_status>0 ? Yii::$app->appHelperFunctions->dealStatusListArr[$model->deal_status] : '');?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Deal Value:').'</strong> <span class="numeral-span">'.Yii::$app->helperFunctions->withCurrencySign($model->deal_value).'</span>';?>
                </div>
              </div>
              <?= '<strong>'.Yii::t('app','Interest:').'</strong> '.$model->interest;?>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Expected Close Date:').'</strong> '.($model->expected_close_date!=null ? Yii::$app->formatter->asDate($model->expected_close_date) : '');?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Close Date:').'</strong> '.($model->close_date!=null ? Yii::$app->formatter->asDate($model->close_date) : '');?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Lead Score:').'</strong> ';?><div id="company-d-lead_score_star" class="rating"></div>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Confidence:').'</strong> ';?>
                  <div>
                    <div id="company-d-confidence_star" class="rating"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
       <!-- <div class="col-sm-4">
          <?/*= ActivityLogWidget::widget(['type'=>'client','model'=>$model])*/?>
        </div>-->
      </div>
    </div>
  </section>
</div>
