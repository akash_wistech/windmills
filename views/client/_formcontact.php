<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\ClientFormAsset;
ClientFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
$zonesList=[];
if($model->country_id>0){
    $zonesList=Yii::$app->appHelperFunctions->getCountryZoneListArr($model->country_id);
}
$emiratesListArr=Yii::$app->appHelperFunctions->emiratedListArr;
$feeStructureOptions=Yii::$app->appHelperFunctions->feeStructureOptionListArr;
//$feeStructureTypes=Yii::$app->helperFunctions->feeStructureTypeListArr;
$feeStructureTypes='';

// if($model->primaryContact!=null){
//     // echo "<pre>"; print_r($model->primaryContact); echo "</pre>"; die();

//   $model->firstname=$model->primaryContact->firstname;
//   $model->lastname=$model->primaryContact->lastname;
//   $model->job_title=$model->primaryContact->profileInfo!=null && $model->primaryContact->profileInfo->jobTitle!=null ? $model->primaryContact->profileInfo->jobTitle->title : '';
//   $model->job_title_id=$model->primaryContact->profileInfo->job_title_id;
//   $model->mobile=$model->primaryContact->profileInfo->mobile;
//   $model->email=$model->primaryContact->email;
// }

$actionBtns='Edit';

// die('here');



?>
<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }
</style>
<script>

</script>
<section class="company-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['id'=>'company-form']); ?>
    <div class="hidden">
        <?= $form->field($model, 'job_title_id')->textInput()?>
        <?= $form->field($model, 'lead_score')->textInput()?>
        <?= $form->field($model, 'confidence')->textInput()?>
       <!-- --><?/*= $form->field($model, 'company_id')->textInput(['id'=>'selected_company_id'])*/?>
    </div>
    <?=  $form->field($model, 'existing_valuation_contact_c')->hiddenInput(['value'=> $existing_valuation_contact])->label(false); ?>
    <?=  $form->field($model, 'bilal_contact_c')->hiddenInput(['value'=> $bilal_contact])->label(false); ?>
    <?=  $form->field($model, 'valuation_contact_c')->hiddenInput(['value'=> $valuation_contact])->label(false); ?>
    <?=  $form->field($model, 'prospect_contact_c')->hiddenInput(['value'=>$prospect_contact])->label(false); ?>
    <?=  $form->field($model, 'property_owner_contact_c')->hiddenInput(['value'=> $property_owner_contact])->label(false); ?>
    <?=  $form->field($model, 'inquiry_valuations_contact_c')->hiddenInput(['value'=> $inquiry_valuations_contact])->label(false); ?>
    <?=  $form->field($model, 'icai_contact_c')->hiddenInput(['value'=> $icai_contact])->label(false); ?>
    <?=  $form->field($model, 'broker_contact_c')->hiddenInput(['value'=> $broker_contact])->label(false); ?>
    <?=  $form->field($model, 'developer_contact_c')->hiddenInput(['value'=> $developer_contact])->label(false); ?>
    <?php if(isset($cardTitle)){?>
        <header class="card-header">
            <h2 class="card-title"><?= $cardTitle?></h2>
        </header>
    <?php }?>
    <div class="card-body">

        <section class="valuation-form card card-outline card-primary">

            <header class="card-header">
                <h2 class="card-title">Client Name and Address</h2>
            </header>
            <div class="card-body">

                <div class="row">
                  <!--  <div class="col-sm-6">
                        <?/*= $form->field($model, 'title')->textInput(['id'=>'selected_company_title', 'maxlength' => true])->label($model->getAttributeLabel('company_name').$companyAddLink)*/?>
                    </div>-->
                    <div class="col-sm-6">
                        <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Client Official Name')?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'nick_name')->textInput(['maxlength' => true])->label('Client Nick Name')?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'client_type')->dropDownList(yii::$app->quotationHelperFunctions->clienttype)->label('Client Segment')?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'client_industry')->dropDownList(yii::$app->appHelperFunctions->clientIndustry)?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'address')->textInput(['maxlength' => true])->label('Street')?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'p_o_box')->textInput(['maxlength' => true])->label('P. O. Box')?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'zone_id')->dropDownList(Yii::$app->appHelperFunctions->emiratedListArr,['prompt'=>Yii::t('app','Select')])?>
                    </div>
                <!--    <div class="col-sm-6">
                        <?/*= $form->field($model, 'zone_id')->dropDownList($zonesList)*/?>
                    </div>-->
                    <div class="col-sm-6">
                        <?php

                        if($model->country_id > 0){

                        }else{
                            $model->country_id = 221;
                        }

                        if($model->country_id_nationality > 0){

                        }else{
                            $model->country_id_nationality = 221;
                        }

                        ?>
                        <?= $form->field($model, 'country_id')->dropDownList(Yii::$app->appHelperFunctions->countryListArr,['prompt'=>Yii::t('app','Select')])->label('Country of Incorporation')?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'country_id_nationality')->dropDownList(Yii::$app->appHelperFunctions->countryListArr,['prompt'=>Yii::t('app','Select')])->label('Nationality')?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'website')->textInput(['maxlength' => true])?>
                        <?=  $form->field($model, 'data_type')->hiddenInput(['value'=> 0])->label(false); ?>
                    </div>
                 <!--   <div class="col-sm-4">
                        <?/*= $form->field($model, 'land_line_number')->textInput(['maxlength' => true])->label('Land Line Number')*/?>
                    </div>-->
                    <div class="col-sm-6">
                        <?= $form->field($model, 'trn_number')->textInput(['maxlength' => true])->label('TRN')?>
                    </div>

                </div>

                <!-- </div>-->

                    <!--  <?php /*if(Yii::$app->user->identity->id == 33 || Yii::$app->user->identity->id == 1) { */?>
                <div class="col-sm-4">
                    <?/*= $form->field($model, 'quickbooks_registration_id')->textInput(['maxlength' => true,'type' => 'number'])->label('QuickBook ID')*/?>
                </div>
            --><?php /*} */?>
                    <?php if(isset($model->id) && $model->id > 0) {
                        if ($model->client_type == 'bank') {

                            ?>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'relative_discount')->widget(\kartik\select2\Select2::classname(), [
                                    'data' => yii::$app->quotationHelperFunctions->relativediscount,
                                    'options' => ['placeholder' => 'Select a Relative Discount ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]); ?>
                            </div>
                        <?php }
                    }?>

                    <!--<div class="col-sm-4">
                <?php
                    /*                echo $form->field($model, 'fee_master_file_vat')->widget(\kartik\select2\Select2::classname(), [
                                        'data' => array('0' => 'No', '1' => 'Yes'),

                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label('Fee Master file Vat');
                                    */?>
            </div>-->

                    <!--  <div class="col-sm-4">
                <?/*= $form->field($model, 'send_monthly_quotation_invoice')->dropDownList([0=>'No', 1=>'Yes'])*/?>
            </div>-->
                    <?php if(isset($model->id) && $model->id == 1) {
                        ?>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'client_fixed_fee')->textInput(['maxlength' => true,'type' => 'number'])->label('Shaikh Zayed case fixed fee')?>
                        </div>
                    <?php } ?>



            </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app','<b>Client Strategic Details</b>')?></h2>
            </header>

            <div class="card-body ">
                <div class="row">
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_priority')->dropDownList(Yii::$app->appHelperFunctions->clientPriority,['prompt'=>Yii::t('app','Select')])->label('Client Priority')?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'number_of_total_properties')->dropDownList(Yii::$app->appHelperFunctions->numberOfTotalPropertiesClientOwns,['prompt'=>Yii::t('app','Select')])->label('Number of total properties client owns')?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'sensitivity')->dropDownList(Yii::$app->appHelperFunctions->clientSenisitivity,['prompt'=>Yii::t('app','Select')])->label('Sensitivity')?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'good_bad_client')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'Good', '1' => 'Bad'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Good/Bad Client');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'start_date',['template'=>'
        {label}
        <div class="input-group date" id="company-d-start_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#company-d-start_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])?>
                    </div>

                </div>


            </div>
        </section>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app','<b>Social Media Control</b>')?></h2>
            </header>

            <div class="card-body ">
                <div class="row">

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'email_check')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'No', '1' => 'Yes'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Send Emails');
                        ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'sms_check')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'No', '1' => 'Yes'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Send SMS');
                        ?>
                    </div>

                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'whatsapp_check')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'No', '1' => 'Yes'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Send Whatsapp');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'linkedin_check')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'No', '1' => 'Yes'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('LinkedIn Posts');
                        ?>
                    </div>




            </div>
        </section>

        <section class="card card-outline card-info">
    <header class="card-header">
        <h2 class="card-title"><?= Yii::t('app','<b>Celebration Dates</b>')?></h2>
    </header>

    <div class="card-body ">
        <div class="row">

            <div class="col-sm-6">
                <?= $form->field($model, 'dob_date',['template'=>'
        {label}
        <div class="input-group date" id="company-d-dob_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#company-d-dob_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])->label('Date of Birth/Incorporation')?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'wedding_date',['template'=>'
        {label}
        <div class="input-group date" id="company-d-wedding_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#company-d-wedding_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])->label('Wedding Date')?>
            </div>

        </div>


    </div>
</section>

        <!--
    <?/*= $form->field($model, 'manager_id')->dropDownList(Yii::$app->appHelperFunctions->StaffMemberListArrBusiness,['multiple'=>'multiple'])*/?>
     -->
<section class="card card-outline card-info">
    <header class="card-header">
        <h2 class="card-title"><?= Yii::t('app','<b>Client Requirements</b>')?></h2>
    </header>

    <div class="card-body ">
        <div class="row">
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'print_report')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Hard Copy Reports');
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'land_valutaion')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Land Market Value');
                ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'land_age')->textInput(['maxlength' => true,'type' => 'number'])->label('Minimum Age for Land MV')?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'vat')->dropDownList(Yii::$app->appHelperFunctions->getvatArr())->label('Vat for Quotation')?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'report_password')->textInput(['maxlength' => true])->label('Report Password')?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'client_invoice_customer')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'No', '1' => 'Yes'),

                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Client Require Customer Invoice');
                ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'send_monthly_report')->dropDownList([0=>'No', 1=>'Yes'])?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'send_daily_report')->dropDownList([0=>'No', 1=>'Yes'])?>
            </div>

        </div>
</section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app','<b>Instructing Person Info</b>')?></h2>
            </header>

            <div class="card-body instructing-person-append-section">
                <?php
                $i=0;
                if($model->allContacts!=null){
                    foreach ($model->allContacts as $key => $value) {
                       /* echo "<pre>";
                        print_r($model->allContacts);
                        die;*/
                        // echo "<pre>"; print_r($value); echo "</pre>"; echo "<br>"; die();
                        // echo "<pre>"; print_r($value->firstname); echo "</pre>"; echo "<br>"; die();
                        $jobTitle = $value->profileInfo!=null && $value->profileInfo->jobTitle!=null ? $value->profileInfo->jobTitle->title : '';
                        ?>

                        <section class="card card-outline card-info  append-card">
                            <?php
                            if ($value->profileInfo->primary_contact==1) {?>
                                <header class="card-header bg-success">
                                    <h2 class="card-title"><?= Yii::t('app','<b>Primary Contact</b>')?></h2>
                                </header>
                                <?php
                            }
                            ?>
                            <div class="row mx-2 my-2">
                                <input type="hidden" name="Company[InstructingPersonInfo][<?=$i?>][contant_id]" value="<?= $value->id ?>">
                                <div class="col-sm-4">
                                    <div class="form-group field-company-firstname">
                                        <label class="control-label" for="company-firstname">Firstname</label>
                                        <input type="text" id="company-firstname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][firstname]" value="<?= $value->firstname ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-lastname">
                                        <label class="control-label" for="company-lastname">Lastname</label>
                                        <input type="text" id="company-lastname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][lastname]" value="<?= $value->lastname ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-job_title">
                                        <label class="control-label" for="company-job_title">Job Title</label>
                                        <input type="text" id="company-job_title" class="form-control job-title-cls" name="Company[InstructingPersonInfo][<?=$i?>][job_title]" value="<?= $jobTitle ?>" autocomplete="off">
                                        <input type="hidden" class="job-title-id-field" name="Company[InstructingPersonInfo][<?=$i?>][job_title_id]" value="<?= $value->profileInfo->job_title_id ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-mobile">
                                        <label class="control-label" for="company-mobile">Land Line</label>
                                        <input type="text" id="company-mobile" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][mobile]" value="<?= $value->profileInfo->mobile ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-mobile">
                                        <label class="control-label" for="company-mobile">Mobile 1</label>
                                        <input type="text" id="company-mobile" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][mobile]" value="<?= $value->profileInfo->mobile ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-mobile">
                                        <label class="control-label" for="company-mobile">Mobile 2</label>
                                        <input type="text" id="company-mobile" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][mobile]" value="<?= $value->profileInfo->mobile ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-email">
                                        <label class="control-label" for="company-email">Email</label>
                                        <input type="text" id="company-email" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][email]" value="<?= $value->email ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group field-company-email">
                                        <label class="control-label" for="company-email">Linked in Page</label>
                                        <input type="text" id="company-email" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][email]" value="<?= $value->email ?>">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <?php
                                if ($value->profileInfo->primary_contact!==1) {?>
                                    <div class="col-sm-4 my-2 py-4 text-right">
                                        <button type="button" data-toggle="tooltip" title="Remove" class="btn btn-danger remove-apnd-card"><i class="fa fa-minus-circle"></i></button>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                        </section>

                        <?php
                        $i++;
                    }
                }else{?>
                    <section class="card card-outline card-info  append-card">
                        <div class="row mx-2 my-2">
                            <div class="col-sm-4">
                                <div class="form-group field-company-firstname">
                                    <label class="control-label" for="company-firstname">Firstname</label>
                                    <input type="text" id="company-firstname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][firstname]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-lastname">
                                    <label class="control-label" for="company-lastname">Lastname</label>
                                    <input type="text" id="company-lastname" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][lastname]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-job_title">
                                    <label class="control-label" for="company-job_title">Job Title</label>
                                    <input type="text" id="company-job_title" class="form-control job-title-cls" name="Company[InstructingPersonInfo][<?=$i?>][job_title]" autocomplete="off">
                                    <input type="hidden" class="job-title-id-field" name="Company[InstructingPersonInfo][<?=$i?>][job_title_id]" value="<?= $value->profileInfo->job_title_id ?>">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-mobile">
                                    <label class="control-label" for="company-mobile">Land Line</label>
                                    <input type="text" id="company-mobile" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][land_line]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-mobile">
                                    <label class="control-label" for="company-mobile">Mobile 1</label>
                                    <input type="text" id="company-mobile" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][mobile]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-mobile">
                                    <label class="control-label" for="company-mobile">Mobile 2</label>
                                    <input type="text" id="company-mobile" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][mobile_2]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-email">
                                    <label class="control-label" for="company-email">Email</label>
                                    <input type="text" id="company-email" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][email]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group field-company-email">
                                    <label class="control-label" for="company-email">Linked in Page</label>
                                    <input type="text" id="company-email" class="form-control" name="Company[InstructingPersonInfo][<?=$i?>][linked_in_page]">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php
                    $i++;
                }
                ?>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-sm-10"></div>
                    <div class="col-sm-2 text-right">
                        <button type="button"  data-toggle="tooltip" title="Add" class="btn btn-primary create-instructing-person"><i class="fa fa-plus-circle"></i></button>
                    </div>
                </div>
            </div>
        </section>

        <section class="valuation-form card card-outline card-primary">

            <header class="card-header">
                <h2 class="card-title">CC Emails for Daily Status Reports</h2>
            </header>
            <div class="card-body">

                <table id="attachment" class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td class="text-left">Name</td>
                        <td class="text-left">Email</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $row = 0; ?>
                    <?php



                    if(isset($model->customEmails) && ($model->customEmails <> null)) {
                        foreach ($model->customEmails as $attachment) { ?>
                            <tr id="image-row-attachment-<?php echo $row; ?>">

                                <td>
                                    <input type="text" class="form-control"
                                           name="Company[customEmailsStatus][<?= $row ?>][name]"
                                           value="<?= $attachment->name ?>" placeholder="Name" required/>
                                </td>

                                <td>
                                    <input type="email" class="form-control"
                                           name="Company[customEmailsStatus][<?= $row ?>][email]"
                                           value="<?= $attachment->email ?>" placeholder="Email" required/>
                                </td>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsStatus][<?= $row ?>][id]"
                                       value="<?= $attachment->id ?>" placeholder="Name" required/>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsStatus][<?= $row ?>][type]"
                                       value="<?= $attachment->type ?>" placeholder="Name" required/>

                                <td class="text-left">
                                    <button type="button"
                                            onclick="deleteRow('-attachment-<?= $row ?>', '<?= $attachment->id; ?>', 'attachment')"
                                            data-toggle="tooltip" title="You want to delete Attachment"
                                            class="btn btn-danger"><i
                                                class="fa fa-minus-circle"></i></button>
                                </td>
                            </tr>
                            <?php $row++; ?>
                        <?php }
                    }?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2"></td>
                        <td class="text-left">
                            <button type="button" onclick="addAttachment();" data-toggle="tooltip" title="Add"
                                    class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </section>
        <section class="valuation-form card card-outline card-primary">

            <header class="card-header">
                <h2 class="card-title">CC Emails for Auto Emails</h2>
            </header>
            <div class="card-body">

                <table id="attachment-auto" class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td class="text-left">Name</td>
                        <td class="text-left">Email</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $row_auto = 0; ?>
                    <?php



                    if(isset($model->customEmailsVals) && ($model->customEmailsVals <> null)) {
                        foreach ($model->customEmailsVals as $attachment) { ?>
                            <tr id="image-row-attachment-auto-<?php echo $row_auto; ?>">

                                <td>
                                    <input type="text" class="form-control"
                                           name="Company[customEmailsAuto][<?= $row_auto ?>][name]"
                                           value="<?= $attachment->name ?>" placeholder="Name" required/>
                                </td>

                                <td>
                                    <input type="email" class="form-control"
                                           name="Company[customEmailsAuto][<?= $row_auto ?>][email]"
                                           value="<?= $attachment->email ?>" placeholder="Email" required/>
                                </td>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsAuto][<?= $row_auto ?>][id]"
                                       value="<?= $attachment->id ?>" placeholder="Name" required/>
                                <input type="hidden" class="form-control"
                                       name="Company[customEmailsAuto][<?= $row_auto ?>][type]"
                                       value="<?= $attachment->type ?>" placeholder="Name" required/>

                                <td class="text-left">
                                    <button type="button"
                                            onclick="deleteRowauto('-attachment-auto-<?= $row_auto ?>', '<?= $attachment->id; ?>', 'attachment')"
                                            data-toggle="tooltip" title="You want to delete Attachment"
                                            class="btn btn-danger"><i
                                                class="fa fa-minus-circle"></i></button>
                                </td>
                            </tr>
                            <?php $row_auto++; ?>
                        <?php }
                    }?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2"></td>
                        <td class="text-left">
                            <button type="button" onclick="addAttachmentauto();" data-toggle="tooltip" title="Add"
                                    class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </section>




<section class="valuation-form valuation-form card card-outline card-info">

    <header class="card-header">
        <h2 class="card-title">Documents Information</h2>
    </header>
    <div class="card-body">
        <div class="row" style="padding: 10px">




                <table id="requestTypes"
                       class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td>Description</td>
                        <td>Attachment</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>



                        <tr id="image-row1">

                            <td class="text-left">
                                <div class="required">
                                    <label  class="control-label">
                                        Terms of Engagement
                                    </label>
                                </div>

                            </td>

                            <td class="text-left upload-docs">
                                <div class="form-group">
                                    <a href="javascript:;"
                                       id="upload-document1"
                                       onclick="uploadAttachment(1)"
                                       data-toggle="tooltip"
                                       class="img-thumbnail"
                                       title="Upload Document">
                                        <?php
                                        $attachment= $model->signed_toe;
                                        if ($attachment <> null) {

                                            if (strpos($attachment, '.pdf') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>

                                                <?php

                                            } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php

                                            } else {
                                                ?>
                                                <img src="<?php echo $attachment; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                 alt="" title=""
                                                 data-placeholder="no_image.png"/>
                                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                               target="_blank">
                                                <span class="glyphicon glyphicon-eye-open"></span>
                                            </a>
                                            <?php
                                        }

                                        ?>
                                    </a>
                                    <input type="hidden"
                                           name="Company[signed_toe]"
                                           value="<?= $attachment; ?>"
                                           id="input-attachment1"/>


                                </div>
                            </td>

                        </tr>
                        <tr id="image-row2">

                            <td class="text-left">
                                <div class="required">
                                    <label  class="control-label">
                                       SLA
                                    </label>
                                </div>

                            </td>

                            <td class="text-left upload-docs">
                                <div class="form-group">
                                    <a href="javascript:;"
                                       id="upload-document2"
                                       onclick="uploadAttachment(2)"
                                       data-toggle="tooltip"
                                       class="img-thumbnail"
                                       title="Upload Document">
                                        <?php
                                        $attachment_sla= $model->sla;
                                        if ($attachment_sla <> null) {

                                            if (strpos($attachment_sla, '.pdf') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_sla; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>

                                                <?php

                                            } else if (strpos($attachment_sla, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_sla; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php

                                            } else {
                                                ?>
                                                <img src="<?php echo $attachment_sla; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_sla; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                 alt="" title=""
                                                 data-placeholder="no_image.png"/>
                                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                               target="_blank">
                                                <span class="glyphicon glyphicon-eye-open"></span>
                                            </a>
                                            <?php
                                        }

                                        ?>
                                    </a>
                                    <input type="hidden"
                                           name="Company[sla]"
                                           value="<?= $attachment_sla; ?>"
                                           id="input-attachment2"/>


                                </div>
                            </td>

                        </tr>
                        <tr id="image-row3">
                        <tr id="image-row4">

                            <td class="text-left">
                                <div class="required">
                                    <label  class="control-label">
                                        NDA
                                    </label>
                                </div>

                            </td>

                            <td class="text-left upload-docs">
                                <div class="form-group">
                                    <a href="javascript:;"
                                       id="upload-document2"
                                       onclick="uploadAttachment(2)"
                                       data-toggle="tooltip"
                                       class="img-thumbnail"
                                       title="Upload Document">
                                        <?php
                                        $attachment_nda= $model->nda;
                                        if ($attachment_nda <> null) {

                                            if (strpos($attachment_nda, '.pdf') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_nda; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>

                                                <?php

                                            } else if (strpos($attachment_nda, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                ?>
                                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_nda; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php

                                            } else {
                                                ?>
                                                <img src="<?php echo $attachment_nda; ?>"
                                                     alt="" title=""
                                                     data-placeholder="no_image.png"/>
                                                <a href="<?= $attachment_nda; ?>"
                                                   target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                 alt="" title=""
                                                 data-placeholder="no_image.png"/>
                                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                               target="_blank">
                                                <span class="glyphicon glyphicon-eye-open"></span>
                                            </a>
                                            <?php
                                        }

                                        ?>
                                    </a>
                                    <input type="hidden"
                                           name="Company[nda]"
                                           value="<?= $attachment_nda; ?>"
                                           id="input-attachment2"/>


                                </div>
                            </td>

                        </tr>


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>



        </div>
    </div>

</section>

        <section class="card card-outline card-info">
    <header class="card-header">
        <h2 class="card-title"><?= Yii::t('app','Relatonship Manager')?></h2>
    </header>
    <div class="card-body">
        <?= $form->field($model, 'manager_id')->dropDownList(Yii::$app->appHelperFunctions->staffMemberListArr)->label('Relatonship Manager')?>
        <?= $form->field($model, 'manager_id_back_up')->dropDownList(Yii::$app->appHelperFunctions->staffMemberListArr)->label('Back up Relationship Manager')?>
    </div>
</section>

        <?php   if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){ ?>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Verification') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'status')->widget(\kartik\select2\Select2::classname(), [
                                'data' => array( '2' => 'Unverified','1' => 'Verified'),
                            ]);
                            ?>
                        </div>


                    </div>
                </div>
            </div>
        </section>
        <?php } ?>




    </div>

    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>



<script type="text/javascript">
    var row = <?= $row ?>;
    function addAttachment() {

        html = '<tr id="image-row' + row + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="Company[customEmailsStatus][' + row + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="email" class="form-control"  name="Company[customEmailsStatus][' + row + '][email]" value="" placeholder="Email" required />';
        html += '    </div>';
        html += '  </td>';

        html += '<input type="hidden" class="form-control" name="Company[customEmailsStatus][<?= $row ?>][type]" value="1" placeholder="Name" required/>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment tbody').append(html);

        row++;
    }


    var row_auto = <?= $row_auto ?>;
    function addAttachmentauto() {

        html = '<tr id="image-row-auto' + row_auto + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="Company[customEmailsAuto][' + row_auto + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="email" class="form-control"  name="Company[customEmailsAuto][' + row_auto + '][email]" value="" placeholder="Email" required />';
        html += '    </div>';
        html += '  </td>';

        html += '<input type="hidden" class="form-control" name="Company[customEmailsAuto][<?= $row_auto ?>][type]" value="1" placeholder="Name" required/>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row-auto' + row_auto + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment-auto tbody').append(html);

        row_auto++;
    }


    function deleteRow(rowId, docID, type) {



        var url = '<?= \yii\helpers\Url::to('/client/remove-attachment'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if(response.status == 'exist'){
                            swal("Warning!", response.message, "warning");
                        }else{
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
    function deleteRowauto(rowId, docID, type) {



        var url = '<?= \yii\helpers\Url::to('/client/remove-attachment'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if(response.status == 'exist'){
                            swal("Warning!", response.message, "warning");
                        }else{
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
</script>




<?php

$this->registerJs('

    function AutoCompleteFunction(){
        $(".job-title-cls").autocomplete({
            serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
            noCache: true,
            onSelect: function(suggestion) {
                // if($(".company-job_title_id").val()!=suggestion.data){
                    // $(".job-title-id-field").val(suggestion.data);
                    $(this).parents(".append-card").find(".job-title-id-field").val(suggestion.data);
                // }
            },
            onInvalidateSelection: function() {
                $(this).parents(".append-card").find(".job-title-id-field").val("0");
                // $(".job-title-id-field").val("0");
            }
        });
    }
AutoCompleteFunction();


  $("body").on("click", ".create-instructing-person", function() {
        addInstructingPerson()
  });


	var add_Rows = '.$i.';
	function addInstructingPerson(){
		html = "";
		html += "<section class=\"card card-outline card-info append-card\">";
		html += "<div class=\"row mx-2 my-2\">";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-firstname\">";
		html +=         "<label class=\"control-label\" for=\"company-firstname\">Firstname</label>";
		html +=         "<input type=\"text\" id=\"company-firstname\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][firstname]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-lastname\">";
		html +=         "<label class=\"control-label\" for=\"company-lastname\">Lastname</label>";
		html +=         "<input type=\"text\" id=\"company-lastname\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][lastname]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-job_title\">";
		html +=         "<label class=\"control-label\" for=\"company-job_title\">Job Title</label>";
		html +=         "<input type=\"text\" id=\"company-job_title\" class=\"form-control job-title-cls\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][job_title]\" autocomplete=\"off\">";
		html +=         "<input type=\"hidden\" class=\"job-title-id-field\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][job_title_id]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-mobile\">";
		html +=         "<label class=\"control-label\" for=\"company-mobile\">Land Line</label>";
		html +=         "<input type=\"text\" id=\"company-mobile\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][land_line]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";
		
		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-mobile\">";
		html +=         "<label class=\"control-label\" for=\"company-mobile\">Mobile 1</label>";
		html +=         "<input type=\"text\" id=\"company-mobile\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][mobile]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";
		
		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-mobile\">";
		html +=         "<label class=\"control-label\" for=\"company-mobile\">Mobile 2</label>";
		html +=         "<input type=\"text\" id=\"company-mobile\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][mobile_1]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";
		

		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-email\">";
		html +=         "<label class=\"control-label\" for=\"company-email\">Email</label>";
		html +=         "<input type=\"text\" id=\"company-email\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][email]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";
		
		html += "<div class=\"col-sm-4\">";
		html +=     "<div class=\"form-group field-company-mobile\">";
		html +=         "<label class=\"control-label\" for=\"company-mobile\">Linked In Page</label>";
		html +=         "<input type=\"text\" id=\"company-mobile\" class=\"form-control\" name=\"Company[InstructingPersonInfo]["+add_Rows+"][linked_in_page]\">";
		html +=         "<div class=\"help-block\"></div>";
		html +=     "</div>";
		html += "</div>";

		html += "<div class=\"col-sm-4 my-2 py-4 text-right\">";
		html +=     "<button type=\"button\" data-toggle=\"tooltip\" title=\"Remove\" class=\"btn btn-danger remove-apnd-card\"><i class=\"fa fa-minus-circle\"></i></button>";
		html += "</div>";

		html += "</div>";
		html += "</section>";

        // console.log(html);
        $(".instructing-person-append-section").append(html);
        add_Rows++;
        AutoCompleteFunction();	 
    }









$("#company-d-start_date,#company-d-lead_date,#company-d-lead_expected_close_date,#company-d-lead_close_date,#company-d-dob_date,#company-d-wedding_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});


$("#company-d-lead_score_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->lead_score>0 ? 'initial_value: '.$model->lead_score : '').'
});
$("#company-d-lead_score_star").on("afterChange", function(ev, data){
  $("#company-lead_score").val(data.to);
});
$("#company-d-confidence_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->confidence>0 ? 'initial_value: '.$model->confidence : '').'
});
$("#company-d-confidence_star").on("afterChange", function(ev, data){
  $("#company-confidence").val(data.to);
});
$(".numeral-input").each(function(index){
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("body").on("blur", ".numeral-input", function () {
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("#company-manager_id").select2({
	allowClear: true,
	width: "100%",
});


    
    

  $("body").on("click", ".remove-apnd-card", function() {
    $(this).parents(".append-card").remove();
  });

');
$this->registerJs('
$("#user-job_title").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#user-job_title_id").val()!=suggestion.data){
      $("#user-job_title_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#user-job_title_id").val("0");
  }
});
$("#selected_company_title").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/company']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#selected_company_id").val()!=suggestion.data){
      $("#selected_company_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#selected_company_id").val("0");
  }
});




$("body").on("click",".btn-random", function(){


$.ajax({

  url: "'.Url::to(['contact/generate-random-string','id'=>$model->id]).'",
  dataType: "html",
  type: "POST",

  });
});

$("body").on("click" ,".btn-random", function(){
         AppBlockSave();
      });


');
?>

<script>

    var uploadAttachment = function (attachmentId) {


        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function (json) {
                        console.log(json);
                        alert('ddd');
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);


                            console.log(json.file.href);
                            var str1 = json['file'].href;
                            var str2 = ".pdf";
                            var str3 = ".doc";
                            var str4 = ".docx";
                            var str5 = ".xlsx";
                            var str6 = ".xls";
                            if (str1.indexOf(str2) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                            } else if (str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                            } else {
                                $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                            }
                            $('#input-attachment' + attachmentId).val(json['file'].href);


                            /*  $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                             $('#input-attachment' + attachmentId).val(json['file'].href);*/
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }

</script>
<?= $this->render('/client/js/create_company_script')?>

<script>
    // random-string

    function AppBlockSave(){
        App.blockUI({
            iconOnly: true,
            target: ".Main-class-block",
            overlayColor: "none",
            cenrerY: true,
            boxed: true
        });

    };


</script>
