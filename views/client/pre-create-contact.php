<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = Yii::t('app', $form_title);
$cardTitle = Yii::t('app','New Contact');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;

?>
<div class="company-create">

    <?= $this->render('_formcontactpre', [
        'model' => $model,
        'cardTitle' => $cardTitle,
        's_type'=>$s_type
    ]) ?>
</div>
