<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalStandardReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proposal-standard-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'payment_method') ?>

    <?= $form->field($model, 'timings') ?>

    <?= $form->field($model, 'rics_valuation_standards_departures') ?>

    <?= $form->field($model, 'valuer_duties_supervision') ?>

    <?php // echo $form->field($model, 'internal_external_status_of_the_valuer') ?>

    <?php // echo $form->field($model, 'previous_involvement_and_conflict_of_interest') ?>

    <?php // echo $form->field($model, 'decleration_of_independence_and_objectivity') ?>

    <?php // echo $form->field($model, 'report_handover') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'basis of value') ?>

    <?php // echo $form->field($model, 'market_value') ?>

    <?php // echo $form->field($model, 'statutory_definition_of_market_value') ?>

    <?php // echo $form->field($model, 'market_rent') ?>

    <?php // echo $form->field($model, 'investment_value') ?>

    <?php // echo $form->field($model, 'fair_value') ?>

    <?php // echo $form->field($model, 'valuation_date') ?>

    <?php // echo $form->field($model, 'property') ?>

    <?php // echo $form->field($model, 'assumptions_ext_of_investi_limi_scope_of_work') ?>

    <?php // echo $form->field($model, 'physical_inspection') ?>

    <?php // echo $form->field($model, 'desktop_or_driven_by_valuation') ?>

    <?php // echo $form->field($model, 'structural_and_technical_survey') ?>

    <?php // echo $form->field($model, 'conditions_state_repair_maintenance_property') ?>

    <?php // echo $form->field($model, 'contamination_ground_environmental_consideration') ?>

    <?php // echo $form->field($model, 'statutory_and_regulatory_requirements') ?>

    <?php // echo $form->field($model, 'title_tenancies_and_property_document') ?>

    <?php // echo $form->field($model, 'planning_and_highway_enquiries') ?>

    <?php // echo $form->field($model, 'plant_and_equipment') ?>

    <?php // echo $form->field($model, 'development_properties') ?>

    <?php // echo $form->field($model, 'disposal_costs_and_liabilities') ?>

    <?php // echo $form->field($model, 'insurance_reinstalement_cost') ?>

    <?php // echo $form->field($model, 'nature_source_information_documents_relied_upon') ?>

    <?php // echo $form->field($model, 'description_of_report') ?>

    <?php // echo $form->field($model, 'client_acceptance_of_the_valuation_report') ?>

    <?php // echo $form->field($model, 'restrictions_on_publications') ?>

    <?php // echo $form->field($model, 'third_party_liability') ?>

    <?php // echo $form->field($model, 'valuation_uncertaninty') ?>

    <?php // echo $form->field($model, 'complaints') ?>

    <?php // echo $form->field($model, 'rics_monitoring') ?>

    <?php // echo $form->field($model, 'rera_monitoring') ?>

    <?php // echo $form->field($model, 'limitions_on_liabity') ?>

    <?php // echo $form->field($model, 'liability_and_duty_of_care') ?>

    <?php // echo $form->field($model, 'extent_of_fee') ?>

    <?php // echo $form->field($model, 'indemnification') ?>

    <?php // echo $form->field($model, 'professional_indemmnity_insurance') ?>

    <?php // echo $form->field($model, 'clients_obligations') ?>

    <?php // echo $form->field($model, 'proposal_validity') ?>

    <?php // echo $form->field($model, 'jurisdiction') ?>

    <?php // echo $form->field($model, 'acceptance_of_terms_of_engagement') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
