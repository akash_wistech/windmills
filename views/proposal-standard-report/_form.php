<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\TinyMceAsset;
TinyMceAsset::register($this);

$this->registerJs('


tinymce.init({
  selector: ".editor",

  menubar: false,
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table paste code help wordcount code",
  ],
  toolbar: "undo redo | formatselect | " +
  "bold italic backcolor | alignleft aligncenter " +
  "alignright alignjustify | bullist numlist outdent indent | " +
  "removeformat | code",
});

');

?>
<section class="proposal-standard-report-form card card-outline card-primary">

    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title">Standard Report</h2>
    </header>

    <div class="card-body">
        <div class="row">

          <div class="col-sm-12">
            <?= $form->field($model, 'firstpage_content')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'firstpage_footer')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'payment_method')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
             <?= $form->field($model, 'timings')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'rics_valuation_standards_departures')->textarea(['class'=>'editor']) ?>
          </div>

           <div class="col-sm-12">
            <?= $form->field($model, 'valuation_approaches')->textarea(['class'=>'editor']) ?>
          </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'income_approach')->textarea(['class'=>'editor']) ?>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'profit_approach')->textarea(['class'=>'editor']) ?>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'cost_approach')->textarea(['class'=>'editor']) ?>
            </div>

          <div class="col-sm-12">
              <?= $form->field($model, 'valuer_duties_supervision')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'internal_external_status_of_the_valuer')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'previous_involvement_and_conflict_of_interest')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'decleration_of_independence_and_objectivity')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'report_handover')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'currency')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'basis_of_value')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'market_value')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'statutory_definition_of_market_value')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'market_rent')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'investment_value')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'fair_value')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'valuation_date')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'property')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'assumptions_ext_of_investi_limi_scope_of_work')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'physical_inspection')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'desktop_or_driven_by_valuation')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'structural_and_technical_survey')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'conditions_state_repair_maintenance_property')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'contamination_ground_environmental_consideration')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'statutory_and_regulatory_requirements')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'title_tenancies_and_property_document')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'planning_and_highway_enquiries')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'plant_and_equipment')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'development_properties')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'disposal_costs_and_liabilities')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'insurance_reinstalement_cost')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'nature_source_information_documents_relied_upon')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'description_of_report')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'client_acceptance_of_the_valuation_report')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'restrictions_on_publications')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'third_party_liability')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'valuation_uncertaninty')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'complaints')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'rics_monitoring')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'rera_monitoring')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'limitions_on_liabity')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'liability_and_duty_of_care')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'extent_of_fee')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'indemnification')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'professional_indemmnity_insurance')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'clients_obligations')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'proposal_validity')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'jurisdiction')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'acceptance_of_terms_of_engagement')->textarea(['class'=>'editor']) ?>
          </div>

          <div class="col-sm-12">
            <?= $form->field($model, 'quotation_last_paragraph')->textarea(['class'=>'editor']) ?>
          </div>

        </div>
    </div>

    <div class="card-footer bg-light">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info mr-2']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</section>
