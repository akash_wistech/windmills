<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProposalStandardReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proposal Standard Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-standard-report-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' =>"
          <div class=\"card card-primary card-outline\">
            <div class=\"card-header\">
              <h3 class=\"card-title\">
                List
              </h3>
               <div class=\"card-tools\">
                    ".Html::a(Yii::t('app', '+'), ['create'], ['class' => ['btn btn-primary','py-0 px-2 mr-2 ','font-weight-bold']])."
               </div>
            </div>


                <div class=\"card-body tbl-container table-responsive table-responsive-lg  table-striped table-hover mb-0\">
                    {items}
                </div>

                <div class=\"card-footer\">
                      <div class=\"row\">
                      <div class=\"col-sm-3\">{summary}</div>
                        <div class=\"col-sm-9 12 pager-container\">{pager}</div>
                      </div>
                    </div>
        </div>
          ",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'payment_method',
            'timings',
            'rics_valuation_standards_departures',
            'valuer_duties_supervision',
            //'internal_external_status_of_the_valuer',
            //'previous_involvement_and_conflict_of_interest',
            //'decleration_of_independence_and_objectivity',
            //'report_handover',
            //'currency',
            //'basis of value',
            //'market_value',
            //'statutory_definition_of_market_value',
            //'market_rent',
            //'investment_value',
            //'fair_value',
            //'valuation_date',
            //'property',
            //'assumptions_ext_of_investi_limi_scope_of_work',
            //'physical_inspection',
            //'desktop_or_driven_by_valuation',
            //'structural_and_technical_survey',
            //'conditions_state_repair_maintenance_property',
            //'contamination_ground_environmental_consideration',
            //'statutory_and_regulatory_requirements',
            //'title_tenancies_and_property_document',
            //'planning_and_highway_enquiries',
            //'plant_and_equipment',
            //'development_properties',
            //'disposal_costs_and_liabilities',
            //'insurance_reinstalement_cost',
            //'nature_source_information_documents_relied_upon',
            //'description_of_report',
            //'client_acceptance_of_the_valuation_report',
            //'restrictions_on_publications',
            //'third_party_liability',
            //'valuation_uncertaninty',
            //'complaints',
            //'rics_monitoring',
            //'rera_monitoring',
            //'limitions_on_liabity',
            //'liability_and_duty_of_care',
            //'extent_of_fee',
            //'indemnification',
            //'professional_indemmnity_insurance',
            //'clients_obligations',
            //'proposal_validity',
            //'jurisdiction',
            //'acceptance_of_terms_of_engagement',

            [
              'class' => 'yii\grid\ActionColumn',
              'header'=>'',
              'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
              'contentOptions'=>['class'=>'noprint actions'],
              'template' => '
                <div class="btn-group flex-wrap">
                  <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                  </button>
                  <div class="dropdown-menu" role="menu">
                    {update}{delete}{view}
                  </div>
                </div>',
              'buttons' => [
                  'view' => function ($url, $model) {
                    return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                      'title' => Yii::t('app', 'View'),
                      'class'=>'dropdown-item text-1',
                      'data-pjax'=>"0",
                    ]);
                  },
                  'update' => function ($url, $model) {
                    return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                      'title' => Yii::t('app', 'Edit'),
                      'class'=>'dropdown-item text-1',
                      'data-pjax'=>"0",
                    ]);
                  },
                  'delete' => function ($url, $model) {
                    return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                      'title' => Yii::t('app', 'Delete'),
                      'class'=>'dropdown-item text-1',
                      'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                      'data-method'=>"post",
                      'data-pjax'=>"0",
                    ]);
                  },
              ],
            ],
        ],

        'pager'=>[
            'maxButtonCount' => 5,
            'linkContainerOptions' => ['class'=>'paginate_button page-item'],
            'linkOptions' => ['class'=>'page-link'],
            'prevPageCssClass' => 'previous',
            'disabledPageCssClass' => 'disabled',
            'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link'],
          ],
    ]); ?>


</div>
