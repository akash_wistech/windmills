<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalStandardReport */

$this->title = 'Create Proposal Standard Report';
$this->params['breadcrumbs'][] = ['label' => 'Proposal Standard Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-standard-report-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
