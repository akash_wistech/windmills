<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalStandardReport */

$this->title = 'Update Proposal Standard Report: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Proposal Standard Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proposal-standard-report-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
