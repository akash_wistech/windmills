<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalStandardReport */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Proposal Standard Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="proposal-standard-report-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'payment_method',
            'timings',
            'rics_valuation_standards_departures',
            'valuer_duties_supervision',
            'internal_external_status_of_the_valuer',
            'previous_involvement_and_conflict_of_interest',
            'decleration_of_independence_and_objectivity',
            'report_handover',
            'currency',
            'basis of value',
            'market_value',
            'statutory_definition_of_market_value',
            'market_rent',
            'investment_value',
            'fair_value',
            'valuation_date',
            'property',
            'assumptions_ext_of_investi_limi_scope_of_work',
            'physical_inspection',
            'desktop_or_driven_by_valuation',
            'structural_and_technical_survey',
            'conditions_state_repair_maintenance_property',
            'contamination_ground_environmental_consideration',
            'statutory_and_regulatory_requirements',
            'title_tenancies_and_property_document',
            'planning_and_highway_enquiries',
            'plant_and_equipment',
            'development_properties',
            'disposal_costs_and_liabilities',
            'insurance_reinstalement_cost',
            'nature_source_information_documents_relied_upon',
            'description_of_report',
            'client_acceptance_of_the_valuation_report',
            'restrictions_on_publications',
            'third_party_liability',
            'valuation_uncertaninty',
            'complaints',
            'rics_monitoring',
            'rera_monitoring',
            'limitions_on_liabity',
            'liability_and_duty_of_care',
            'extent_of_fee',
            'indemnification',
            'professional_indemmnity_insurance',
            'clients_obligations',
            'proposal_validity',
            'jurisdiction',
            'acceptance_of_terms_of_engagement',
        ],
    ]) ?>

</div>
