<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MergeProject */

$this->title = 'Create Merge Project';
$this->params['breadcrumbs'][] = ['label' => 'Merge Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="merge-project-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
