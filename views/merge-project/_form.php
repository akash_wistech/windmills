<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Buildings */
/* @var $form yii\widgets\ActiveForm */
?>



<section class="buildings-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title">
            <?= $cardTitle ?>
        </h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'building_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select a Building ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Building/Project Name');
                ?>

            </div>


            <div class="col-sm-12">
                <?php

                   $model->combine_project = explode(',', ($model->combine_project <> null) ? $model->combine_project : "");
                   echo $form->field($model, 'combine_project')->widget(Select2::classname(), [
                       'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                           'title' => SORT_ASC,
                       ])->all(), 'id', 'title'),
                       'options' => ['placeholder' => 'Select a Building/Project ...'],
                       'pluginOptions' => [
                           'placeholder' => 'Select a  Building/Project',
                           'multiple' => true,
                           'allowClear' => true
                       ],
                   ])->label('Select to Merge');;
                ?>
            </div>


        </div>



        </div>

        <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('status')) { ?>
            <section class="card card-outline card-info">
                <header class="card-header">
                    <h2 class="card-title">
                        <?= Yii::t('app', 'Verification') ?>
                    </h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="col-sm-4">
                                <?php
                                echo $form->field($model, 'status')->widget(Select2::classname(), [
                                    'data' => array('2' => 'Unverified', '1' => 'Verified'),
                                ]);
                                ?>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
        <?php } ?>

    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if ($model <> null && $model->id <> null) {
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>