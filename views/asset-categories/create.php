<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetCategory */

$this->title = 'Create Asset Categories';
$this->params['breadcrumbs'][] = ['label' => 'Asset Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sales-and-marketing-purpose-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
