<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetCategory */

$this->title = 'Update Asset Categories: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Asset Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sales-and-marketing-purpose-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
