<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use kartik\select2\Select2;
use kartik\rating\StarRating;
use yii\web\JsExpression;

use app\models\User;

use app\assets\DateRangePickerAsset2;

DateRangePickerAsset2::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BranchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Quotation Verified Follow Up');
$this->params['breadcrumbs'][] = $this->title;

$citiesArr = Yii::$app->appHelperFunctions->emiratedList;

$staffData = Yii::$app->appHelperFunctions->staffMemberListArrLastNameservice;

$total_quality_of_inspection = 0;
$total_quality_of_valuation = 0;
$total_quality_of_tat = 0;
$total_quality_of_valuation_service = 0;

// dd($dataProvider);

?>
<style>
    .yfstar {
        color: #f39c12;
    }

    .gnfstar {
        color: #95a5a6;
    }
</style>

<div class="feedback-index">


<div class="valuation-search card-body mb-0 pb-0">

<?php $form = ActiveForm::begin([
    'action' => ['proposals'],
    'method' => 'get',
]); ?>

<div class="row">


    <div class="col-sm-4 text-left">
        <?php
        echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
            'data' => Yii::$app->appHelperFunctions->reportPeriod,
            'options' => ['placeholder' => 'Time Frame ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(false);
        ?>
    </div>
    <div class="col-sm-4" id="date_range_array" <?php if ($custom == 0) {
                                                    echo 'style="display:none;"';
                                                } ?>>
        <div class="" id="end_date_id">

            <?php //echo $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); 
            ?>
            <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class' => 'form-control div1', 'placeholder' => 'Select Custom Date Range'])->label(false); ?>

        </div>
    </div>
    <div class="col-sm-4">
        <div class="text-left">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
</div>



<?php ActiveForm::end(); ?>
</div>


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'showFooter' => true,
        'options' => [
            'id' => 'sortable-table',
        ],
        'columns' => [

            [
                'attribute' => 'quotation_ref',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('quotation_ref') . '">Quotation Ref</a></div>',
                'value' => function ($model) {
                    return $model->quotation->reference_number;
                },
                'contentOptions' => ['style' => 'vertical-align:middle; width: 200px;'],
            ],
            [
                'attribute' => 'client_id',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('client_id') . '">Client Name</a></div>',
                'value' => function ($model) {
                    // return $model->client->nick_name;
                    return $model->client->title;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'client_id',
                    'data' => ArrayHelper::map(\app\models\Company::find()
                                ->where(['status' => 1])
                                ->andWhere(['allow_for_valuation'=>1])
                                ->orderBy(['title' => SORT_ASC,])
                                ->all(), 'id', 'title'),
                    'options' => [
                        'placeholder' => 'Select Clent...',
                        'class' => 'form-control',
                        
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => 'text-align: left; vertical-align:middle; '],
            ],
            [
                'attribute' => 'no_of_reminder',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('no_of_reminder') . '">Reminder Sent</a></div>',
                'value' => function ($model) {
                    // dd($model);
                    return $model->no_of_reminder;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'no_of_reminder',
                    'data' => Yii::$app->appHelperFunctions->getCrmOptionZeroToNumber(20),
                    'options' => [
                        'placeholder' => 'Select...',
                        'class' => 'form-control',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => ['style' => 'vertical-align:middle;  '],
            ],
            [
                'attribute' => 'date',
                'header' => '<div><a href="' . $dataProvider->sort->createUrl('date') . '">Last Reminder Sent Date</a></div>',
                'value' => function ($model) {
                    return date('d-M-Y',strtotime($model->date));
                },
                'filter' => false,
                'contentOptions' => ['style' => 'text-align: left; vertical-align:middle;  '],
            ],
            // [
            //     'attribute' => 'fee_differ_amount',
            //     'header' => '<div><a href="' . $dataProvider->sort->createUrl('fee_differ_amount') . '">Fee Difference <br>Amount</a></div>',
            //     'format' => 'raw',
            //     'value' => function ($model) {
            //         if ($model->recommended_fee > $model->approved_fee) {
            //             $feeDiffAmount = '<span style="color:red" > - </span>' . number_format($model->fee_differ_amount, 2);
            //         } else if ($model->recommended_fee < $model->approved_fee) {
            //             $feeDiffAmount = '<span style="color:green" > + </span>' . number_format($model->fee_differ_amount, 2);
            //         } else {
            //             $feeDiffAmount = number_format($model->fee_differ_amount, 2);
            //         }
            //         return $feeDiffAmount;
            //     },
            //     'contentOptions' => ['style' => 'text-align: right; vertical-align:middle'],
            // ],
            // [
            //     'attribute' => 'fee_differ_percent',
            //     'header' => '<div><a href="' . $dataProvider->sort->createUrl('fee_differ_percent') . '">Fee Difference <br>Percent(%)</a></div>',
            //     'format' => 'raw',
            //     'value' => function ($model) {
            //         if ($model->recommended_fee > $model->approved_fee) {
            //             $feeDiffPercentage = '<span style="color:red" > - </span>' . number_format($model->fee_differ_percent, 2);
            //         } else if ($model->recommended_fee < $model->approved_fee) {
            //             $feeDiffPercentage = '<span style="color:green" > + </span>' . number_format($model->fee_differ_percent, 2);
            //         } else {
            //             $feeDiffPercentage = number_format($model->fee_differ_percent, 2);
            //         }
            //         return $feeDiffPercentage . ' %';
            //     },
            //     'contentOptions' => ['style' => 'text-align: right; vertical-align:middle'],
            // ],

        ],
        'footerRowOptions' => ['style' => 'font-weight:bold'],
        'pager' => [
            'class' => \yii\widgets\LinkPager::class,
            'options' => ['class' => 'pagination justify-content-center'],
            'maxButtonCount' => 5, // Adjust as needed
            'prevPageLabel' => 'Previous',
            'nextPageLabel' => 'Next',
            'hideOnSinglePage' => true,
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>
</div>


<?php
$this->registerJs('
  



     $(".div1").daterangepicker({
        autoUpdateInput: false,
          locale: {
            format: "YYYY-MM-DD"
          }
      });
     
      $(".div1").on("apply.daterangepicker", function(ev, picker) {
         $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
      });
      $(document).ready(function() {
          // Code to execute when the document is ready
          var period = $("#feedbacksearch-time_period").val();
          
          if (period == 9) {
              $(".div1").prop("required", true);
              $("#date_range_array").show();
          } else {
              $(".div1").prop("required", false);
              $("#date_range_array").hide();
          }
          
          // Change event handler for select input
          $("#feedbacksearch-time_period").on("change", function() {
              var period = $(this).val();
              
              if (period == 9) {
                  $(".div1").prop("required", true);
                  $("#date_range_array").show();
              } else {
                  $(".div1").prop("required", false);
                  $(".div1").val("");
                  $("#date_range_array").hide();
              }
          });
      });




');
?>