<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CommercialSolds */

$this->title = 'Create Commercial Solds';
$this->params['breadcrumbs'][] = ['label' => 'Commercial Solds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commercial-solds-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
