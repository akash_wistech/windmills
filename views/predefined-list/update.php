<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PredefinedList */
$set_country_id=Yii::$app->appHelperFunctions->getSetting('country_list_id');
$set_zone_id=Yii::$app->appHelperFunctions->getSetting('zone_list_id');
$countryListArr = Yii::$app->appHelperFunctions->countryListArr;

$this->title = Yii::t('app', 'Lists');
$cardTitle = Yii::t('app','Update List:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Predefined Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$actionBtns='';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='<li>{update}</li>';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
  $actionBtns.='<li>{status}</li>';
}
$this->registerJs('
$("body").on("beforeSubmit", "form#subOptForm", function () {
  _targetContainer="#subOptForm";
  App.blockUI({
    message: "'.Yii::t('app','Please wait...').'",
    target: _targetContainer,
    overlayColor: "none",
    cenrerY: true,
    boxed: true
  });

  var form = $(this);
  if (form.find(".has-error").length) {
    App.unblockUI($(_targetContainer));
    return false;
  }
  var formData = new FormData(form[0]);
  // submit form
  $.ajax({
    url: form.attr("action"),
    type: "post",
    data: formData,
    dataType: "json",
    success: function (response) {
      App.unblockUI($(_targetContainer));
      if(response["success"]){
        document.getElementById("subOptForm").reset();
        toastr.success(response["success"]["msg"], "", {timeOut: 5000});
        $.pjax.reload({container: "#suOpts-container", timeout: 2000});
      }else{
        swal({title: response["error"]["heading"], text: response["error"]["msg"], type: "error", timer: 10000});
      }
    },
    cache: false,
    contentType: false,
    processData: false
  });
  return false;
});
$("body").on("blur", ".subopt-title", function () {
  _this=$(this);
  id=_this.parents("tr").data("key");
  updateSupOptions(id,1,_this.val());
});
$("body").on("blur", ".subopt-other_name", function () {
  _this=$(this);
  id=_this.parents("tr").data("key");
  updateSupOptions(id,2,_this.val());
});
$("body").on("change", ".subopt-status", function () {
  _this=$(this);
  id=_this.parents("tr").data("key");
  updateSupOptions(id,3,_this.val());
});
$("body").on("blur", ".subopt-iso_code_2", function () {
  _this=$(this);
  id=_this.parents("tr").data("key");
  updateSupOptions(id,4,_this.val());
});
$("body").on("blur", ".subopt-iso_code_3", function () {
  _this=$(this);
  id=_this.parents("tr").data("key");
  updateSupOptions(id,5,_this.val());
});
$("body").on("blur", ".subopt-phone_code", function () {
  _this=$(this);
  id=_this.parents("tr").data("key");
  updateSupOptions(id,6,_this.val());
});

$("body").on("blur change", ".subopt-country_id", function () {
  _this=$(this);
  id=_this.parents("tr").data("key");
  updateSupOptions(id,7,_this.val());
});
$("body").on("blur", ".subopt-code", function () {
  _this=$(this);
  id=_this.parents("tr").data("key");
  updateSupOptions(id,8,_this.val());
});
$("body").on("click", ".del-item", function () {
  _this=$(this);
  id=_this.parents("tr").data("key");
  swal({
    title: "'.Yii::t('app','Are you sure?').'",
    text: "'.Yii::t('app','You will not be able to recover this!').'",
    type: "warning",
    showCancelButton: true,
    confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'"
  }).then((result) => {
    if (result.value) {
      _targetContainer=$("#suOpts-container");
      App.blockUI({
        message: "'.Yii::t('app','Please wait...').'",
        target: _targetContainer,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });
      $.ajax({
        url: "'.Url::to(['delete','id'=>'']).'"+id,
        dataType: "html",
        type: "post",
        success: function(data) {
          if(data=="done"){
            App.unblockUI($(_targetContainer));
            $.pjax.reload({container: "#suOpts-container", timeout: 2000});
          }
        },
        error: bbAlert
      });
    }
  });
});
');
?>
<script>
function updateSupOptions(id,c,v){
  $.ajax({
    url: "<?= Url::to(['update-sub-options','id'=>''])?>"+id+"&c="+c+"&v="+v,
    type: "POST",
    dataType: "json",
    success: function(data) {
      if(data["error"]){
        toastr.error(data["error"]);
      }
    },
  });
}
</script>
<div class="predefined-list-update">
  <?= $this->render('_form', [
    'model' => $model,
    'cardTitle' => $cardTitle,
  ]) ?>
</div>
<section class="predefined-list-form card card-outline card-info">
  <header class="card-header">
    <h2 class="card-title"><?= Yii::t('app','Add new Sub Option')?></h2>
  </header>
  <div class="card-body">
    <?php $form = ActiveForm::begin(['id'=>'subOptForm','action'=>['create']]); ?>
    <div class="hidden">
      <?= $form->field($subModel, 'parent')->textInput(['maxlength' => true])?>
    </div>
    <?php if($model->id==$set_zone_id){?>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($subModel, 'country_id')->dropDownList($countryListArr)?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($subModel, 'code')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <?php }?>
    <div class="row">
      <div class="col-xs-12 col-sm-4">
        <?= $form->field($subModel, 'title')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-4">
        <?= $form->field($subModel, 'other_name')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-4">
        <?= $form->field($subModel, 'status')->dropDownList(Yii::$app->helperFunctions->arrYesNo) ?>
      </div>
    </div>
    <?php if($model->id==$set_country_id){?>
    <div class="row">
      <div class="col-xs-12 col-sm-4">
        <?= $form->field($subModel, 'iso_code_2')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-4">
        <?= $form->field($subModel, 'iso_code_3')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-4">
        <?= $form->field($subModel, 'phone_code')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <?php }?>
    <div class="row">
      <div class="col-xs-12 col-sm-1">
        <?= Html::submitButton(Yii::t('app', 'Add New'), ['class' => 'btn btn-success btn-block']) ?>
      </div>
    </div>
    <?php ActiveForm::end(); ?>
  </div>
</section>
<?php
$columns[]=['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']];
if($model->id==$set_zone_id){
$columns[]=['format'=>'raw','attribute'=>'country_id','value'=>function($model) use ($countryListArr){
  $html='';
  $html.=Html::dropDownList('',$model['country_id'],$countryListArr,['class'=>'form-control subopt-country_id']);
  return $html;
},'filter'=>$countryListArr];
$columns[]=['format'=>'raw','attribute'=>'code','value'=>function($model){
  $html='';
  $html.='<input type="text" value="'.$model['code'].'" class="form-control subopt-code" />';
  return $html;
}];
}
$columns[]=['format'=>'raw','attribute'=>'title','value'=>function($model){
  $html='';
  $html.='<input type="text" value="'.$model['title'].'" class="form-control subopt-title" />';
  return $html;
}];
$columns[]=['format'=>'raw','attribute'=>'other_name','value'=>function($model){
  $html='';
  $html.='<input type="text" value="'.$model['other_name'].'" class="form-control subopt-other_name" />';
  return $html;
}];
if($model->id==$set_country_id){
$columns[]=['format'=>'raw','attribute'=>'iso_code_2','value'=>function($model){
  $html='';
  $html.='<input type="text" value="'.$model['iso_code_2'].'" class="form-control subopt-iso_code_2" />';
  return $html;
}];
$columns[]=['format'=>'raw','attribute'=>'iso_code_3','value'=>function($model){
  $html='';
  $html.='<input type="text" value="'.$model['iso_code_3'].'" class="form-control subopt-iso_code_3" />';
  return $html;
}];
$columns[]=['format'=>'raw','attribute'=>'phone_code','value'=>function($model){
  $html='';
  $html.='<input type="text" value="'.$model['phone_code'].'" class="form-control subopt-phone_code" />';
  return $html;
}];
}
$columns[]=['format'=>'raw','attribute'=>'status','value'=>function($model){
  $html='';
  $html.=Html::dropDownList('status',$model['status'],Yii::$app->helperFunctions->arrYesNo,['class'=>'form-control subopt-status']);
  return $html;
},'headerOptions'=>['class'=>'noprint','style'=>'width:75px;'],'filter'=>Yii::$app->helperFunctions->arrYesNo];
$columns[]=['format'=>'raw','value'=>function($model){
  $html='';
  $html.=Html::a('<i class="fa fa-trash"></i>','javascript:;',['class'=>'btn btn-danger btn-xs del-item']);
  return $html;
},'headerOptions'=>['class'=>'noprint','style'=>'width:15px;']];
?>
<?php CustomPjax::begin(['id'=>'suOpts-container']); ?>
<?= CustomGridView::widget([
  'cardTitle' => Yii::t('app','Sub Options List'),
  'dataProvider' => $dataProvider,
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'createBtn' => false,
  'columns' => $columns,
]);?>
<?php CustomPjax::end(); ?>
