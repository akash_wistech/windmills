<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PredefinedList */

$this->title = Yii::t('app', 'Lists');
$cardTitle = Yii::t('app', 'New List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Predefined Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="predefined-list-create">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
