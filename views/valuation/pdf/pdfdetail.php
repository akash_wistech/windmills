
<?php
$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();
$approver_data_2 = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'reviewer'])->one();

$standard_report = \app\models\StandardReport::find()->one();

$ValuerName= \app\models\User::find()->select(['firstname', 'lastname'])->where(['id'=>$approver_data->created_by])->one();
$ValuerName_2= \app\models\User::find()->select(['firstname', 'lastname'])->where(['id'=>$approver_data_2->updated_by])->one();

$valuerOther= \app\models\UserProfileInfo::find()->select(['valuer_qualifications','valuer_status','valuer_experience_expertise','signature_img_name'])->where(['user_id'=>$approver_data->created_by])->one();
$valuerOther_2= \app\models\UserProfileInfo::find()->select(['valuer_qualifications','valuer_status','valuer_experience_expertise','signature_img_name'])->where(['user_id'=>$approver_data_2->updated_by])->one();
$detail= \app\models\ValuationDetailData::find()->where(['valuation_id' => $model->id])->one();
// print_r(Yii::$app->get('s3bucket')->getUrl('images/'.$valuerOther['signature_img_name'])); die;

$special_assumptionreport = \app\models\SpecialAssumptionreport::find()->one();

$conflict= \app\models\ValuationConflict::find()->where(['valuation_id'=>$model->id])->one();


//18.	Nature and Sources of Information and Documents Relied Upon
$ReceivedDocs= Yii::$app->PdfHelper->getReceiveDocs($model);

//	Market Commentary
$model_data = \app\models\MarketCommentary::find()->where(['property_type' => 0 ])->orderBy('id DESC')->one();
$city_lower=strtolower(Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city]);
$replaceSpace=str_replace(" ","_",$city_lower);
$replaceSpace.='_commentary';


//25 Property Description – Internal
$select_calculations = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $model->id, 'type' => 'list'])->one();



if($model->client->id == 183 || $model->id >12566){
    $appendices_number = '2';
    $definition_number = '3';
    $valuer_number = '4';
    $assumptions_number = '5';
    $market_number = '6';
}else {
    $definition_number = '2';
    $valuer_number = '3';
    $assumptions_number = '4';
    $market_number = '5';
    $appendices_number = '6';
}
?>
<br pagebreak="true"/>



<div  style=" color:#E65100;
       text-align: center;
       font-size:17px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
          0<?= $definition_number; ?>. Definitions
</div>
<br>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $definition_number; ?>.01.  Market Value</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->market_value  ?></td></tr>
</table>
<br><br>

<!-- <table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $definition_number; ?>.02.  Statutory definition of Market Value (capital gains tax, inheritance tax and stamp duty land tax).</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->statutory_definition_marketvalue  ?></td></tr>
</table>
<br><br> -->

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $definition_number; ?>.02. Market Rent</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->market_rent ?></td></tr>
</table>
<!-- <br pagebreak="true"/> -->
<br><br>

<!-- <table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $definition_number; ?>.04. Investment Value (Worth)</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->investment_value ?></td></tr>
</table>
<br><br> -->

<table cellspacing="1" cellpadding="4" class="main-class col-12">
  <tr><td class="detailheading" colspan="2"><h4><?= $definition_number; ?>.03. Fair Value</h4></td></tr>
  <tr><td class="detailtext" colspan="2"><?= $standard_report->fair_value ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $definition_number; ?>.04. Valuation Approaches</h4></td></tr>
<tr><td class="detailtext" colspan="2">RICS Red Book 31st January 2022<br/><br/>
Part 2 Glossary (page 8 to 10) and VPS 5 Valuation approaches and methods - Point 2 (page 71)<br/><br/>
<span class="contentHeading">Market Approach</span><br/>
An approach that provides an indication of value by comparing the subject asset with identical or similar assets for which price information is available. The market approach is based on comparing the subject asset with identical or similar assets (or liabilities) for which price information is available, such as a comparison with market transactions in the same, or closely similar, type of asset (or liability) within an appropriate time horizon.
<br />
<br />
<span  class="contentHeading">Income Approach</span><br/>
An approach that provides an indication of value by converting future cash flows to a single current capital value. The income approach is based on capitalization or conversion of present and predicted income (cash flows), which may take a number of different forms, to produce a single current capital value. Among the forms taken, capitalization of a conventional market-based income or discounting of a specific income projection can both be considered appropriate depending on the type of asset and whether such an approach would be adopted by market participants.<br/><br/>
<span  class="contentHeading">Cost Approach</span><br/>
An approach that provides an indication of value using the economic principle that a buyer will pay no more for an asset than the cost to obtain an asset of equal utility, whether by purchase or construction. The cost approach is based on the economic principle that a purchaser will pay no more for an asset than the cost to obtain one of equal utility whether by purchase or construction.
</td>
</tr>
</table>
<br>


<br pagebreak="true"/>
<div  style=" color:#E65100;
       text-align: center;
       font-size:17px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
          0<?= $valuer_number; ?>. Valuer
</div>
<br>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.01. Valuer Name</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $ValuerName['firstname'].' '.$ValuerName['lastname'] ?>.</td></tr>
</table>
<br>
<br>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.02. Valuer Qualifications</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $valuerOther['valuer_qualifications'] ?>.</td></tr>
</table>
<br>
<br>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.03. Valuer Status</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $valuerOther['valuer_status'] ?></td></tr>
</table>
<br>
<br>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.04. Valuer Experience and Expertise</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $valuerOther['valuer_experience_expertise'] ?></td></tr>
</table>
<br>
<br>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.05. Valuer Duties and Supervision</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->value_duties_supervision ?></td></tr>
</table>
<br>
<br>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.06. Internal/External Status of the Valuer</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->internal_external_status_valuer ?></td></tr>
</table>
<!-- <br pagebreak="true"/> -->
<br>
<br/>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.07. Previous involvement and Conflict of Interest</h4></td></tr>
<tr><td class="detailtext" colspan="2">We are not aware of any conflict of interest on the level of seller, buyer, client, third party engaged by the Client and/or the subject property,
either on the part of the Firm or Valuer or any individual member of our team assigned to this valuation,
which prevent us from providing an independent and objective opinion of the value of the Property.</td></tr>
<?php
if($conflict['related_to_buyer']== 'Yes'){
?>
<tr><td class="detailtext" colspan="2"><b style="color:#0277BD">BUYER</b></td></tr>
<tr><td class="detailtext" colspan="2"><?= $conflict['related_to_buyer'] ?></td></tr>
<?php
}
if($conflict['related_to_seller']== 'Yes'){
?>
<tr><td class="detailtext" colspan="2"><b style="color:#0277BD">SELLER</b></td></tr>
<tr><td class="detailtext" colspan="2"><?= $conflict['related_to_seller'] ?></td></tr>
<?php
}

if($conflict['related_to_property']== 'Yes'){
?>
<tr><td class="detailtext" colspan="2"><b style="color:#0277BD">PROPERTY</b></td></tr>
<tr><td class="detailtext" colspan="2"><?= $conflict['related_to_property'] ?></td></tr>
<?php }
$special_assumptions='';
//special Assumptions Occupancy, Tananted, Vacant, Acquisition
//  $special_assumptions.= '<br><b>Occupancy Status</b><br>';
if($model->inspectProperty->occupancy_status == "Owner Occupied"){
$special_assumptions.=$special_assumptionreport->owner_occupied;

}
if($model->inspectProperty->occupancy_status == "Tenanted"){
$special_assumptions.=$special_assumptionreport->tenanted;
}

if($model->inspectProperty->occupancy_status == "Vacant"){
$special_assumptions.=$special_assumptionreport->vacant;
}
if($model->inspectProperty->acquisition_method == 1 || $model->inspectProperty->acquisition_method == 2 ){
$special_assumptions.= '<br>'.$special_assumptionreport->gifted_granted;
}else{
if(trim($special_assumptionreport->purchased) != 'None' && trim($special_assumptionreport->purchased) != '') {
$special_assumptions .= '<br>' .$special_assumptionreport->purchased.'<br>' ;
}


}
// if ($detail->valuation_date > $model->scheduleInspection->inspection_date  ) {
// $valuationdate_assumption= \app\models\GeneralAsumption::find()->where(['id'=>5])->one();
// $special_assumptions .= '<br>' .$valuationdate_assumption->general_asumption.'';

// }
if($model->special_assumption !== 'None' &&  ($model->special_assumption <> null)) {
$special_assumptions.= trim($model->special_assumption);
}

?>
</table>
<br>
<br>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>.08. Declaration of Independence and Objectivity</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->declaration_independence_objectivity ?></td></tr>
</table>
<!-- <br><br>
        <table cellspacing="1" cellpadding="8" class="main-class">
          <tr><td class="detailheading" colspan="2"><h4>1. Subject Property (Interest) to be Valued</h4></td></tr>
          <tr><td class="detailtext" colspan="2"><?= ''; //$standard_report->subject_property ?></td></tr>
        </table>
<br><br>
        <table cellspacing="1" cellpadding="8" class="main-class">
          <tr><td class="detailheading" colspan="2"><h4><?= $valuer_number; ?>. Valuation Instructions</h4></td></tr>
          <tr><td class="detailtext" colspan="2"><?= '';//$standard_report->valuation_instructions ?></td></tr>
        </table> -->

<br pagebreak="true"/>

<div  class="col-12"
      style=" color:#E65100;
       text-align: center;
       font-size:17px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
        0<?= $assumptions_number; ?>. Assumptions and Considerations
</div>
<br>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.01. RICS Valuation Standards (and Departures from those Standards)</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->rics_valuation  ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.02. Basis of Value</h4></td></tr>
<?php
if($model->purpose_of_valuation == 3) {
$standard_report->basis_value = str_replace("{market_fair}", 'Fair', $standard_report->basis_value);
}else{
$standard_report->basis_value = str_replace("{market_fair}", 'Market', $standard_report->basis_value);
}
?>
<tr><td class="detailtext" colspan="2"><?= $standard_report->basis_value  ?></td></tr>
</table>
<br><br>

<?php $number_plus = 3; ?>
<?php if($model->client->id != 110 && $model->client->id != 119087 && $model->client->id != 120086 && $model->client->id != 120186){ ?>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.0<?= ($number_plus++); ?>.  Estimated Price under the restricted marketing period</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>According to the RICS Valuation Standard, VPS 4 &ndash; 10 (10.1 to 10.9) Valuations reflecting an actual or anticipated market constraint, and forced sales.</p>
            <p>Wherever the valuer, or client, identifies that a valuation may need to reflect an actual or anticipated marketing constraint, details of that constraint must be agreed and set out in the terms of engagement. It is however agreed in the SLA or terms of engagement signed with the client.&nbsp;</p>
            <p>We have assumed that the time limit of Estimated Price Under the Restricted Marketing Period is 3 Months and the estimated price is 15% lower than the estimated Market Value.</p>
            <p>The Estimated Price Under the Restricted Marketing Period is not an estimate of market value and valuation date. It is merely an estimated price only during the anticipated market constraint. It might be seen as a reflection of worth to the particular vendor at the particular point in time having regard to the specific context.</p>
        </td>
    </tr>
</table>
<br><br>
<?php
   // $number_plus =3;
} ?>
<br>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.0<?= ($number_plus++); ?>.  Special Assumptions</h4></td></tr>
<tr><td class="detailtext" colspan="2">
<?php
if($special_assumptions <> null ){
  echo trim($special_assumptions);
}else{
  echo 'None';
}
?>
</td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.0<?= ($number_plus++); ?>. Terms of Engagement Agreed</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->terms_engagement_agreed ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.0<?= ($number_plus++); ?>. Assumptions, Extent of Investigations, Limitations on the Scope of Work</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->assum_extent_inesti_limit_scope_work ?><?php if($model->general_assumption !== 'None' &&  ($model->general_assumption <> null)) {echo '<br>'.trim($model->general_assumption);} ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
 <?php
 if ($model->inspection_type == 2) { $physicalInspection=$special_assumptionreport->physical_inspection; }
 else if($model->inspection_type == 1 || $model->inspection_type == 3) { $physicalInspection=$special_assumptionreport->desktop_driveby; }

 if ($detail->valuation_date > $model->scheduleInspection->inspection_date  ) {
  $valuationdate_assumption= \app\models\GeneralAsumption::find()->where(['id'=>5])->one();
  $physicalInspection .= '' .$valuationdate_assumption->general_asumption; }
  ?>
  <tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.0<?= ($number_plus++); ?>. Physical Inspection</h4></td></tr>
  <tr><td class="detailtext" colspan="2"><?= trim($physicalInspection) ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.0<?= ($number_plus++); ?>. Sustainability, and Environmental, Social and Governance (ESG)  </h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>Sustainability, and environmental, social and governance (ESG) factors can be of a significant market influence and the valuation should always have appropriate regard to their relevance to the particular assignment.</p><p>The RICS Red Book, which is referred to the IVS 2020 Agenda Consultation (p14), define the ESG as ‘The criteria that together establish the framework for assessing the impact of the sustainability and ethical practices of a company on its financial performance and operations. ESG comprises three pillars: environmental, social and governance, all of which collectively contribute to effective performance, with positive benefits for the wider markets, society and world as a whole.’</p><p> Potential or actual constraints on the enjoyment and use of property caused by sustainability and ESG factors may result from natural causes (such as flooding, severe storms and wildfires), from non-natural causes (such as contamination) or sometimes from a combination of the two (such as subsidence resulting from the historic extraction of minerals). There may also be sustainability and ESG factors beyond the directly physical, such as carbon emissions.</p><p> We do not have the specialist knowledge and experience required in assessing the ESG factors mentioned above. The Client is suggested to arrange for an appropriate ESG specialist and expert, and take an advice in respect of the above-mentioned matters.</p></td></tr>
</table>
<!-- <br pagebreak="true" /> -->
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.0<?= ($number_plus++); ?>. Structural and Technical Survey</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->structural_tecnical_survey ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Conditions and State of Repair and Maintenance of the Property</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->conditions_state_repair_matenence ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Contamination, and Ground and Environmental Considerations</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->contamination_ground_enviormental ?></td></tr>
</table>
<!-- <br pagebreak="true" /> -->
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Natural Disasters</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->natural_disasters ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. National Scenarios or Force Majeure Situations</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->national_scenaiors_majeure_situation ?></td></tr>
</table>
<br><br>


<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Statutory and Regulatory Requirements</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->statutory_regulatory_requirements ?></td></tr>
</table>
<!-- <br pagebreak="true" /> -->
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Title, Tenancies and Property Documents</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->title_tenancies_property_document ?></td></tr>
</table>
<!-- <br pagebreak="true" /> -->
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Planning and Highway Enquiries</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->planning_highway_enquires ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>.	Plant, Machinery and Equipment</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->plant_equipment ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>.	Development Properties</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->development_property ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Disposal Costs and Liabilities</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->disposal_cost_libility ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Documentation Provided by the Client</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->documentation_provided_clients ?></td></tr>
</table>
<br><br>


<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Documentation Not Provided by the Client</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->documentation_not_provided_clients ?></td></tr>
</table>
<br/><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Nature and Sources of Information and Documents Relied Upon</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->nature_sources_info_doc ?><?= $ReceivedDocs['dld_data_enquiry'] ?><?= $ReceivedDocs['kharetati_application_enquiry'] ?><?= $ReceivedDocs['dld_sold_transaction_enquiry'] ?><?= $ReceivedDocs['dld_valuations_enquir'] ?><?= $ReceivedDocs['discussions_real_estate'] ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Restriction on Use, Distribution and Publication of the Report</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->restrictions_use_destribution_publication ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. General Uncertainties</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->general_uncertainties ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Limitation of Liability</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->limitation_of_liability ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Confidentiality</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->confidentinality ?></td></tr>
</table>
<br><br>

<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $assumptions_number; ?>.<?= ($number_plus++); ?>. Valuation Calculation Model</h4></td></tr>
<tr><td class="detailtext" colspan="2">
<p>As the name indicates, market approach of valuation is applied in valuing the subject properties like apartment, villa, townhouse, lands that have sufficient and satisfactory number of transactions comparables seen in the market place.</p>
<p>Should a sufficient and satisfactory number of comparable transactions not be present in the market place for land as a subject property, the residual value of land approach applies, which is quite a complex form of land valuation.</p>
<p>The market approach of valuation entails comparing the asset transactions happened or listed to happen in the market place which are closest in location and similarity vis-à-vis the subject property being valued in the recent period.</p>
<p>Using market approach of valuation, we make use of and analyze three sales data streams i.e., recent sold transactions data, current market listings, and our recently carried out valuations related to similar and closest properties for valuing the subject property.</p>
<p>Please see the “Comparable Properties” analysed for valuing the subject property in the next section.</p></td></tr>
</table>

<br pagebreak="true"/>

<div  style=" color:#E65100;
       text-align: center;
       font-size:17px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
      0<?= $market_number; ?>. Market Commentary
</div>
<br>
<?php

$selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $model->id, 'type' => 'sold','search_type'=>0])->all(), 'id', 'selected_id');
$selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $model->id, 'type' => 'list','search_type'=>0])->all(), 'id', 'selected_id');
$selected_data_prvious = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $model->id, 'type' => 'previous','search_type'=>0])->all(), 'id', 'selected_id');
$sold = 0;
$list = 0;
$previous = 0;
if($selected_data_sold <> null && count($selected_data_sold) > 0) {
    $sold = 1;
    $curl_handle=curl_init();
    curl_setopt($curl_handle, CURLOPT_URL,\yii\helpers\Url::toRoute(['valuation/step_12_report','id'=>$model->id]));
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

    $val = curl_exec($curl_handle);
    curl_close($curl_handle);

    $response_sold = json_decode($val);
   /*echo "<pre>";
   print_r($response_sold->adjustments);
   die('dd');*/
}


if($selected_data_list <> null && count($selected_data_list) > 0) {
    $list = 1;

    $curl_handle=curl_init();
    curl_setopt($curl_handle, CURLOPT_URL,\yii\helpers\Url::toRoute(['valuation/step_15_report','id'=>$model->id]));
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

    $val = curl_exec($curl_handle);
    curl_close($curl_handle);

    $response_list = json_decode($val);

   /* echo "<pre>";
    print_r($response_list);
    die;*/
}


if($selected_data_prvious <> null && count($selected_data_prvious) > 0) {
$previous = 1;
}
?>
<?php if($sold == 1 || $list == 1 || $previous==1){ ?>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $market_number; ?>.01. Comparable Properties</h4></td></tr>
<!--<tr><td class="detailtext" colspan="2"><?/*= $standard_report->comparable_properties */?></td></tr>-->
<tr><td class="detailtext" colspan="2"><p>We have considered the following data sets into the valuation of the subject property:</p><ul><?php if($sold == 1){ ?><li>Recent and comparable sold transactions for the same and/or similar properties. </li><?php } ?><?php if($previous == 1){ ?><li>Recent and comparable previous valuations done by us for the same and/or similar properties.</li><?php } ?><?php if($list == 1){ ?><li>Recent and comparable market listings for the same and/or similar properties </li><?php } ?></ul>
<?php if($sold == 1){ ?>Recent and Comparable Sold Transaction Prices Analysis: 
<!--       <p style="font-size: 12px; text-align: right;">The average transaction price of the recent sold transactions chosen is :
<span style="font-size: 12px; text-align: right;">
AED <?/*=  (isset($response_sold->avg_listings_price_size) && $response_sold->avg_listings_price_size <> null) ? number_format($response_sold->avg_listings_price_size): 0 */?>
</span>

</p>-->
<br>
<table cellspacing="1" cellpadding="0" style="padding-left: 0px;">
<tr>
<td style="width: 75%;text-align: left">
The average transaction price of the recent sold transactions chosen is :
</td>
<td style="width: 20%; text-align: right">
AED <?=  (isset($response_sold->avg_listings_price_size) && $response_sold->avg_listings_price_size <> null) ? number_format($response_sold->avg_listings_price_size): 0 ?>
</td>
</tr>
</table>
<!-- <p>The average transaction price/sqft of the recent sold transactions chosen is :
<span style="font-size: 12px;text-align: right;">
AED <?/*= (isset($response_sold->avg_psf) && $response_sold->avg_psf <> null) ? number_format($response_sold->avg_psf): 0 */?>
</span>
</p>-->
<table cellspacing="1" cellpadding="0" style="padding-left: 0px;">
<tr>
<td style="width: 75%;text-align: left">
The average transaction price/sqft of the recent sold transactions chosen is :
</td>
<td style="width: 20%; text-align: right">
AED <?= (isset($response_sold->avg_psf) && $response_sold->avg_psf <> null) ? number_format($response_sold->avg_psf): 0 ?>
</td>
</tr>
</table>
<br/>
<br/>
    <?php
    if(isset($response_sold->adjustments_table) && ($response_sold->adjustments_table <> null) && $model->client->id == 4){
        ?>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">
            <tr style="background-color:#ECEFF1;  text-align: center;  ">

                <td colspan="10" style="color:#0277BD;">Attributes</td>

                <td colspan="10" style="color:#0277BD;">Adjustments</td>
            </tr>
            <?php
            $n =1;

            foreach ($response_sold->adjustments_table as $adkey => $adData) {

                if (isset($adData->adjustments) && $adData->adjustments != 0) {
                    $color = '';
                    if ($n % 2 == 0) {
                        $color = 'color';
                    }


                    ?>


                    <tr class="<?= $color ?>" style=" text-align: center;  vertical-align: bottom; font-size:10px">

                        <td colspan="10">
                            <?= ucfirst(str_replace('_', ' ', $adkey)) ?>
                        </td>

                        <td colspan="10">
                            <?= $adData->adjustments ?>
                        </td>

                    </tr>


                    <?php
                    $n++;
                }
            }
            ?>
        </table>
        <?php
    } ?>
    <br><br>

After making adjustments for <span style="font-size: 11px;">
<?= (isset($response_sold->adjustments) && $response_sold->adjustments <> null) ? $response_sold->adjustments : '' ?>
</span>




<!-- <p>The average transaction price for the subject property arrived at

<span style="font-size: 12px; text-align: right;">
AED <?/*= (isset($response_sold->mv_total_price) && $response_sold->mv_total_price <> null) ? number_format($response_sold->mv_total_price): 0 */?>
</span>



</p>-->
<br>
<table cellspacing="1" cellpadding="0" style="padding-left: 0px;">
<tr>
<td style="width: 75%;text-align: left">
The average transaction price for the subject property arrived at :
</td>
<td style="width: 20%; text-align: right">
AED <?= (isset($response_sold->mv_total_price) && $response_sold->mv_total_price <> null) ? number_format($response_sold->mv_total_price): 0 ?>
</td>
</tr>
</table>
<!-- <p>The average transaction price/sqft for the subject property arrived at
<span style="font-size: 12px; text-align: right;">
AED <?/*= (isset($response_sold->mv_avg_psf) && $response_sold->mv_avg_psf <> null) ? number_format($response_sold->mv_avg_psf): 0 */?>
</span>

</p>-->
<table cellspacing="1" cellpadding="0" style="padding-left: 0px;">
<tr>
<td style="width: 75%;text-align: left">
The average transaction price/sqft for the subject property arrived at :
</td>
<td style="width: 20%; text-align: right">
AED <?= (isset($response_sold->mv_avg_psf) && $response_sold->mv_avg_psf <> null) ? number_format($response_sold->mv_avg_psf): 0 ?>
</td>
</tr>
</table>

<?php } ?>
<?php if($list == 1){ ?>
  <br/>
  <br/>
Recent and Comparable Market Listings Analysis: 
<!--  <p style="background: #00b3ee;">The average asking price of the recent market listings chosen is :
<span style="font-size: 12px; text-align: right;">
AED <?/*= (isset($response_list->avg_listings_price_size) && $response_list->avg_listings_price_size <> null) ? number_format($response_list->avg_listings_price_size): 0 */?>
</span>
</p>-->
<br/>

<table cellspacing="1" cellpadding="0" style="padding-left: 0px;">
<tr>
<td style="width: 75%;text-align: left">
The average asking price of the recent market listings chosen is :
</td>
<td style="width: 20%; text-align: right">
AED <?= (isset($response_list->avg_listings_price_size) && $response_list->avg_listings_price_size <> null) ? number_format($response_list->avg_listings_price_size): 0 ?>
</td>
</tr>
</table>



<!--  <p>The average asking price/sqft of the recent market listings chosen is :
<span style="font-size: 12px; text-align: right;">
AED <?/*= (isset($response_list->avg_psf) && $response_list->avg_psf <> null) ? number_format($response_list->avg_psf): 0 */?>
</span>
</p>-->
<table cellspacing="1" cellpadding="0" style="padding-left: 0px;">
<tr>
<td style="width: 75%;text-align: left">
The average asking price/sqft of the recent market listings chosen is :
</td>
<td style="width: 20%; text-align: right">
AED <?= (isset($response_list->avg_psf) && $response_list->avg_psf <> null) ? number_format($response_list->avg_psf): 0 ?>
</td>
</tr>
</table>
<br/>
<br/>
    <?php
    if(isset($response_list->adjustments_table) && ($response_list->adjustments_table <> null) && $model->client->id == 4){
        ?>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">
            <tr style="background-color:#ECEFF1;  text-align: center;  ">

                <td colspan="10" style="color:#0277BD;">Attributes</td>

                <td colspan="10" style="color:#0277BD;">Adjustments</td>
            </tr>
            <?php
            $n =1;

            foreach ($response_list->adjustments_table as $adkey => $adData) {

                if (isset($adData->adjustments) && $adData->adjustments != 0) {
                    $color = '';
                    if ($n % 2 == 0) {
                        $color = 'color';
                    }


                    ?>


                    <tr class="<?= $color ?>" style=" text-align: center;  vertical-align: bottom; font-size:10px">

                        <td colspan="10">
                            <?= ucfirst(str_replace('_', ' ', $adkey)) ?>
                        </td>

                        <td colspan="10">

                            <?= $adData->adjustments ?>
                        </td>

                    </tr>


                    <?php
                    $n++;
                }
            }
            ?>
        </table>
        <?php
    } ?>
    <br><br>
After making adjustments for <span ><?= (isset($response_list->adjustments) && $response_list->adjustments <> null) ? $response_list->adjustments : '' ?>, and after applying sales negotiation discount of <?= $response_list->sales_discount ?>% after discussion with numerous agents.
</span>



<!-- <p>The average transaction price for the subject property arrived at

<span style="font-size: 12px; text-align: right;">
AED <?/*= (isset($response_list->mv_total_price) && $response_list->mv_total_price <> null) ? number_format($response_list->mv_total_price): 0 */?>
</span>

</p>-->
<br>
<table cellspacing="1" cellpadding="0" style="padding-left: 0px;">
<tr>
<td style="width: 75%;text-align: left">
The average transaction price for the subject property arrived at :
</td>
<td style="width: 20%; text-align: right">
AED <?= (isset($response_list->mv_total_price) && $response_list->mv_total_price <> null) ? number_format($response_list->mv_total_price): 0 ?>
</td>
</tr>
</table>
<!-- <p>The average transaction price/sqft for the subject property arrived at
<span style="font-size: 12px; text-align: right;">
AED <?/*= (isset($response_list->mv_avg_psf) && $response_list->mv_avg_psf <> null) ? number_format($response_list->mv_avg_psf): 0 */?>
</span>

</p>-->
<table cellspacing="1" cellpadding="0" style="padding-left: 0px;">
<tr>
<td style="width: 75%;text-align: left">
The average transaction price/sqft for the subject property arrived at :
</td>
<td style="width: 20%; text-align: right">
AED <?= (isset($response_list->mv_avg_psf) && $response_list->mv_avg_psf <> null) ? number_format($response_list->mv_avg_psf): 0 ?>
</td>
</tr>
</table>


<p>Furthermore, as cited above, in order to derive the market value of the subject property, we have considered the resultant numbers from all data sets analyses explained above and after making financial adjustments that transform/justify the comparable properties into the subject property.</p>
<p>Please also see appendices <?= $appendices_number; ?>.03 and, where applicable, <?= $appendices_number; ?>.04 for further details on how we have calculated the market value of the subject property.</p>

<?php } ?>

</td>
</tr>
</table>
<?php }else{ ?>
<table cellspacing="1" cellpadding="4" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4><?= $market_number; ?>.01. Comparable Properties</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= $standard_report->comparable_properties ?></td></tr>
</table>
<?php } ?>
<br ><br >
<table cellspacing="1" cellpadding="4" class="main-class col-12">
  <tr><td class="detailheading" colspan="2"><h4><?= $market_number; ?>.02.	Market Commentary</h4></td></tr>
  <tr><td class="detailtext" colspan="2"><?= $model_data->{$replaceSpace} ?></td></tr>
</table>

<br ><br >

<table cellspacing="1" cellpadding="4" class="main-class col-12">
  <tr><td class="detailheading" colspan="2"><h4><?= $market_number; ?>.03. Summary of Key Inputs in the Valuation and Reasoning</h4></td></tr>
  <tr><td class="detailtext" colspan="2"><?= $standard_report->summary_key_inputs ?></td></tr>
</table>
<br><br>
<!--<br pagebreak="true"/>-->
<table>
  <tr>

    <?php


    $sig_check = 0;
    if($approver_data->created_by == 86 || $approver_data->created_by == 110465){
        $sig_check =1;
    }


    $stampImg = \yii\helpers\Url::to('@web/images/stamp1.png');  ?>
      <?php

      if(isset($valuerOther->signature_img_name) && $valuerOther->signature_img_name <> null){
          $sigImg = 'https://maxima-media.s3.eu-central-1.amazonaws.com/images/'.$valuerOther->signature_img_name;
      }else{
      if (isset($valuerOther_2->signature_img_name) && $valuerOther_2->signature_img_name <> null) {
          $sigImg = 'https://maxima-media.s3.eu-central-1.amazonaws.com/images/' . $valuerOther_2->signature_img_name;
      }else {
          $sigImg = \yii\helpers\Url::to('@web/images/Picture160d2cecf38a09.png');
      }
      }

      if($sig_check == 1) {

          if (isset($valuerOther_2->signature_img_name) && $valuerOther_2->signature_img_name <> null) {
              $sigImg_2 = 'https://maxima-media.s3.eu-central-1.amazonaws.com/images/' . $valuerOther_2->signature_img_name;
          } else {
              $sigImg_2 = \yii\helpers\Url::to('@web/images/Picture160d2cecf38a09.png');
          }
      }

      if($approver_data->created_by == 38){

      $sigImg = \yii\helpers\Url::to('@web/images/feb_sig6537cbcf709e8.png');
      }else if($approver_data->created_by == 21) {
          $sigImg = \yii\helpers\Url::to('@web/images/kumar_sig6537cbbbc1002.png');
      }else if($approver_data->created_by == 110821) {
          $sigImg = \yii\helpers\Url::to('@web/images/IMG_387765b2070e17fe2.jpg');
      }else if($approver_data->created_by == 110465) {
          $sigImg = \yii\helpers\Url::to('@web/images/owais_sig6537cc1b7c498.png');
      }else if($approver_data->created_by == 86) {
          $sigImg = \yii\helpers\Url::to('@web/images/bkh_sig586538b348f2ccd.jpeg');
      }else if($approver_data->created_by == 111558) {
          $sigImg = \yii\helpers\Url::to('@web/images/ahmet_sig.jpeg');
      }else if($approver_data->created_by == 14) {
          $sigImg = \yii\helpers\Url::to('@web/images/sir_bilal_sig.png');
      }else if($approver_data->created_by == 14) {
          $sigImg = \yii\helpers\Url::to('@web/images/sir_bilal_sig.png');
      }

      if($sigImg <> null){

      }else{
          $sigImg = \yii\helpers\Url::to('@web/images/Picture160d2cecf38a09.png');
      }
      if($sig_check == 1) {
          if ($approver_data_2->updated_by == 38) {

              $sigImg_2 = \yii\helpers\Url::to('@web/images/feb_sig6537cbcf709e8.png');
          } else if ($approver_data_2->updated_by == 21) {
              $sigImg_2 = \yii\helpers\Url::to('@web/images/kumar_sig6537cbbbc1002.png');
          } else if ($approver_data_2->updated_by == 110821) {
              $sigImg_2 = \yii\helpers\Url::to('@web/images/IMG_387765b2070e17fe2.jpg');
          } else if ($approver_data_2->updated_by == 110465) {
              $sigImg_2 = \yii\helpers\Url::to('@web/images/owais_sig6537cc1b7c498.png');
          } else if ($approver_data_2->updated_by == 86) {
              $sigImg_2 = \yii\helpers\Url::to('@web/images/bkh_sig586538b348f2ccd.jpeg');
          } else if ($approver_data_2->updated_by == 111558) {
              $sigImg_2 = \yii\helpers\Url::to('@web/images/ahmet_sig.jpeg');
          } else if ($approver_data_2->updated_by == 14) {
              $sigImg_2 = \yii\helpers\Url::to('@web/images/sir_bilal_sig.png');
          }

          if ($sigImg_2 <> null) {
             // $sigImg_2 = \yii\helpers\Url::to('@web/images/feb_sig6537cbcf709e8.png');
          } else {
              $sigImg_2 = \yii\helpers\Url::to('@web/images/feb_sig6537cbcf709e8.png');
          }
      }





      ?>

  <td>

  <img src="<?php echo $sigImg ?>" width="150px"; height="110px" ><img src="<?= $stampImg ?>" width="100px"; height="100px" >
  </td>
    <?php if($sig_check == 1) { ?>
      <td style="text-align: center">

          <img src="<?php echo $sigImg_2 ?>" width="150px"; height="110px" >
        <!--  <img src="<?/*= $stampImg */?>" width="100px"; height="100px" >-->
      </td>
      <?php }else{ ?>
      <td></td>
      <?php } ?>
  </tr>
  </table>

  <table cellpadding="1">
        <tr style="font-size:11px; color:#0D47A1;">

          <td class="tdbold" >Valuer Signature and Company Stamp<br> </td>
            <?php if($sig_check == 1) { ?>
            <td class="tdbold"  >Valuer Signature<br> </td>
            <?php }else{ ?>
            <td></td>
            <?php } ?>
        </tr>
        <tr style="font-size:11px;">
        <td class="bggray"><?= $ValuerName['firstname'].' '.$ValuerName['lastname'] ?></td>
            <?php if($sig_check == 1) { ?>
                <td class="bggray"><?= $ValuerName_2['firstname'].' '.$ValuerName_2['lastname'] ?></td>
            <?php }else{ ?>
                <td></td>
            <?php } ?>

          <!--  <td></td>-->
        </tr>
        <tr style="font-size:11px;" >
            <?php if($sig_check == 1) { ?>
                <td >RERA Registered Valuer</td>
                <td >RICS and RERA Registered Valuer</td>
            <?php }else{ ?>
                <td >RICS and RERA Registered Valuer</td>
                <td></td>
            <?php } ?>


        <!--   <td></td>-->
        </tr>
        <tr style="font-size:11px;" >
            <?php if($sig_check == 1) { ?>
                <td><?= $valuerOther['valuer_qualifications'] ?></td>
                <td><?= $valuerOther_2['valuer_qualifications'] ?></td>
            <?php }else{ ?>
                <td><?= $valuerOther['valuer_qualifications'] ?></td>
                <td></td>
            <?php } ?>

          <!-- <td></td>-->
        </tr>
        <tr style="font-size:11px;" >

            <?php if($sig_check == 1) { ?>
                <td >Windmills Real Estate Valuation Services, L.L.C.</td>
                <td >Windmills Real Estate Valuation Services, L.L.C.</td>
            <?php }else{ ?>
                <td >Windmills Real Estate Valuation Services, L.L.C.</td>
                <td></td>
            <?php } ?>

        <!--   <td></td>-->
        </tr>
  </table>
  <!-- <br pagebreak="true" /> -->



<!-- <table cellspacing="1" cellpadding="8" class="main-class col-12">
  <tr><td class="detailheading" colspan="2"><h4>20.	Valuation Method and Reasoning</h4></td></tr>
  <tr><td class="detailtext" colspan="2">  We have estimated the market value of the Property based on the Market Approach as per (RICS) International Valuation Standards (referred to above), as we a number of acceptable sold transactions and/or comparable listings for sale/rent are available in the market similar to the subject property, and adjustments to the subject property can be applied.</td></tr>
</table>
<br><br>
<br pagebreak="true" /> -->
<!-- <table cellspacing="1" cellpadding="8" class="main-class col-12">
<tr><td class="detailheading" colspan="2"><h4>27. Property and Valuation Overview and Appendices</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= ''; //$standard_report->property_valuation_overview ?></td></tr>
</table>
<br><br>


<table cellspacing="1" cellpadding="8" class="main-class">
<tr><td class="detailheading" colspan="2"><h4>28. Limitation on Attaching Documents</h4></td></tr>
<tr><td class="detailtext" colspan="2"><?= '';//$standard_report->limitation_attaching_document ?></td></tr>
</table>
<br><br>
<br><br> -->
