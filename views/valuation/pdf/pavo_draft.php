
<?php

// This is for 1.05 Valuation Instructions From.
$instructorname=Yii::$app->PdfHelper->getUserInformation($model);


//1.14  Total Building Floor(s)
if($model->property->title == 'Villa') {
    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1 ) {
        $total_building_floors = "Basement + " .Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
    }else if($model->inspectProperty->number_of_basement > 1){
        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }else{
        $total_building_floors = Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
    }

}else {
    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1) {
        $total_building_floors = "Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    } else if($model->inspectProperty->number_of_basement > 1){
        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }else {
        $total_building_floors = "Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }
}
$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();
$detail= \app\models\ValuationDetailData::find()->where(['valuation_id' => $model->id])->one();
$conflict= \app\models\ValuationConflict::find()->where(['valuation_id'=>$model->id])->one();

$owners_in_valuation = \yii\helpers\ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $model->id])->all(), 'id', 'name');
$woners_name = "";
if(!empty($owners_in_valuation)) {
    $woners_name = implode(", ", $owners_in_valuation);
}



//  1.36  View Type
$ViewType = Yii::$app->PdfHelper->getViewType($model);

// echo "<pre>";
// print_r($ViewType);
// echo "<pre>";
// die();


// print_r($model->inspectProperty->other_facilities );
// die();

// 1.39 Building/Community Facilities
$model->inspectProperty->other_facilities = explode(',', ($model->inspectProperty->other_facilities <> null) ? $model->inspectProperty->other_facilities : "");
foreach ($model->inspectProperty->other_facilities as $key => $value) {
    $other_facilitie= \app\models\OtherFacilities::find()->where(['id'=>$value])->one();
    if ($other_facilities!=null) {  $other_facilities.=', '.$other_facilitie->title;  }
    else { $other_facilities.=$other_facilitie->title;   }
}


// 1.43, 1.44 , 1.45, 1.46  Floor Configuration
$floorConfig=Yii::$app->PdfHelper->getFloorConfig($model);
$floorConfigCustom=Yii::$app->PdfHelper->getFloorConfigCustom($model);


$floorConfig['groundFloortitle'] = rtrim($floorConfig['groundFloortitle'], "<br>");

if($floorConfig['groundFloortitle'] == 'None' && $floorConfigCustom['groundFloortitle'] !=''){
    $floorConfig['groundFloortitle'] = "<br>" . rtrim($floorConfigCustom['groundFloortitle'], "<br>");
}else {
    $floorConfig['groundFloortitle'] .= "<br>" . rtrim($floorConfigCustom['groundFloortitle'], "<br>");
}
$floorConfig['firstFloortitle'] = rtrim($floorConfig['firstFloortitle'], "<br>");
if($floorConfig['firstFloortitle'] == 'None' && $floorConfigCustom['firstFloortitle'] !=''){
    $floorConfig['firstFloortitle'] =  $floorConfigCustom['firstFloortitle']. "<br>";
}else {
    if(rtrim($floorConfigCustom['firstFloortitle'] <> null)) {
        $floorConfig['firstFloortitle'] .= "<br>" . $floorConfigCustom['firstFloortitle']. "<br>";
    }
}

// $floorConfig['firstFloortitle'] .= "<br>".rtrim($floorConfigCustom['firstFloortitle'], "<br>");

$floorConfig['secondFloortitle'] = rtrim($floorConfig['secondFloortitle'], "<br>");
if($floorConfig['secondFloortitle'] == 'None' && $floorConfigCustom['secondFloortitle'] !=''){
    $floorConfig['secondFloortitle'] =  rtrim($floorConfigCustom['secondFloortitle'], "<br>");
}else {
    if(rtrim($floorConfigCustom['secondFloortitle'] <> null)) {
        $floorConfig['secondFloortitle'] .= "<br>" . rtrim($floorConfigCustom['secondFloortitle'], "<br>");
    }
}
//   $floorConfig['secondFloortitle'] .= "<br>".rtrim($floorConfigCustom['secondFloortitle'], "<br>");
$floorConfig['basementTitle'] = rtrim($floorConfig['basementTitle'], "<br>");
if($floorConfig['basementTitle'] == 'None' && $floorConfigCustom['basementTitle'] !=''){
    $floorConfig['basementTitle'] =  rtrim($floorConfigCustom['basementTitle'], "<br>");
}else {
    if(rtrim($floorConfigCustom['basementTitle'] <> null)) {
        $floorConfig['basementTitle'] .= "<br>" . rtrim($floorConfigCustom['basementTitle'], "<br>");
    }
}


//$floorConfig['basementTitle'] .= "<br>".rtrim($floorConfigCustom['basementTitle'], "<br>");

// 1.66 Documents Provided by Client , 1.67 Documents not Provided
$DocumentByClient = Yii::$app->PdfHelper->getDocumentByClient($model);


$DocumentByClient['documentAvail'] = rtrim($DocumentByClient['documentAvail'], "<br>");
$DocumentByClient['documentNotavail'] = rtrim($DocumentByClient['documentNotavail'], "<br>");


$makani_number = ($model->inspectProperty->makani_number > 0)? $model->inspectProperty->makani_number: 'Not Applicable';
$plot_number = ($model->plot_number <> null && $model->plot_number != '0')? $model->plot_number: 'Not Applicable';
$source_bua =($model->inspectProperty->bua_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->bua_source] . ')': '';
$source_plot_size =($model->inspectProperty->plot_area_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->plot_area_source]. ')': '';
$source_extension =($model->inspectProperty->extension_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->extension_source]. ')': '';

$plot_size = ($model->land_size > 0)? $model->land_size.' square feet'.$source_plot_size: 'Not Applicable';

$market = 'Market';
if($model->purpose_of_valuation == 3) {
    $market = 'Fair';
}


$otherInstructingPerson='';
$result = \app\models\User::find()->where(['id'=>$model->other_instructing_person])->one();
if ($result<>null) {
    $otherInstructingPerson = $result->firstname.' '.$result->lastname;
}
// print_r($otherInstructingPerson); die();
?>


<!--  <br pagebreak="true" />-->
<?php


if($model->client->id == 183){ ?>
    <style>
        td.topbarleft{
            font-weight: bold;
            font-size:12px;
            color:#000000;
            text-align: left
        }
        td.topbarright{
            font-weight: bold;
            font-size:12px;
            color:#000000;
            text-align: right
        }
    </style>

    <table class="col-12">
        <tr>
            <td class="topbarleft"><p>Valuation Summary </p></td>
            <td class="topbarright"><p>Report Date: <?= ($model->scheduleInspection->valuation_report_date <> null) ? date('l, jS \of F, Y', strtotime($model->scheduleInspection->valuation_report_date)) : '' ?> </p></td>
        </tr>
    </table>

    <br><br>
    <table cellspacing="1" cellpadding="4" style="border: 1px dashed #64B5F6;" class="col-12">
        <tr><td class="tableSecondHeading" colspan="4"><h3>General Information</h3></td></tr>
        <tr  class="">
            <td colspan="1" style="color:#0277BD; font-size:13;">Client’s Name:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?= $model->client->title ?></td>
        </tr>
        <tr>
            <td colspan="1" style="color:#0277BD; font-size:13;">Applicant’s Name:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?= $model->client_name_passport ?></td>
        </tr>
        <tr  class="">
            <td colspan="1" style="color:#0277BD; font-size:13;">Reference No:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?= $model->reference_number ?></td>
        </tr>

        <tr  class="">
            <td colspan="1" style="color:#0277BD; font-size:13;">Purpose of Valuation:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation] ?></td>
        </tr>


        <tr>
            <td colspan="1" style="color:#0277BD; font-size:13;">Inspection Date:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){
                    ?><?='Not Applicable as desktop valuation'; ?>
                <?php }
                else{
                    echo date('l, jS \of F, Y', strtotime($model->scheduleInspection->inspection_date));
                } ?>
            </td>
        </tr>
        <tr class="">
            <td colspan="1" style="color:#0277BD; font-size:13;">Inspected By:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?php if($model->inspection_type == 3){ ?><?='Not Applicable as desktop valuation '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation '; ?>
                <?php }else{ ?><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->scheduleInspection->inspection_officer] ?>
                <?php } ?>
            </td>
        </tr>


        <tr >
            <td colspan="1" style="color:#0277BD; font-size:13;">Method of Valuation:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach] ?></td>
        </tr>
        <tr class="">
            <td colspan="1"  style="color:#0277BD; font-size:13;">Status of Valuer:</td>
            <td colspan="3"  style="color:#212121; font-size:12;">Internal Valuer</td>
        </tr>
        <tr >
            <td colspan="1" style="color:#0277BD; font-size:13;">Previous Involvement:</td>
            <td colspan="3" style="color:#212121; font-size:12;"><?php
                $conflict_text = '';
                if($conflict['related_to_buyer']== 'Yes' || $conflict['related_to_seller']== 'Yes' || $conflict['related_to_property']== 'Yes'){

                    if($conflict['related_to_property']== 'Yes'){
                        $conflict_text .= 'Subject Property, ';
                    }
                    if($conflict['related_to_seller']== 'Yes'){
                        $conflict_text .= 'Seller, ';
                    }

                    if($conflict['related_to_buyer']== 'Yes'){
                        $conflict_text .= 'Buyer';
                    }


                    ?>We have had previous involvement with the <?= $conflict_text; ?> and there might be potential conflicts of interest exist in accepting this instruction.
                <?php }else{ ?>We confirm we have had no previous involvement with the Subject Property and that no conflicts of interest exist in accepting this instruction.
                <?php } ?>

            </td>
        </tr>
    </table>
    <br><br>



    <table cellspacing="1" cellpadding="4" style="border: 1px dashed #64B5F6;" class="col-12">
        <tr>
            <td colspan="4" class="tableSecondHeading"><h3>Property Information</h3></td>
        </tr>
        <tr  class="">
            <td colspan="1"  style="color:#0277BD; font-size:13;">Property / Site Address</td>
            <td colspan="3"  style="color:#212121; font-size:12;">Unit Number <?= $model->unit_number.', '.$model->building->title ?>
                Plot Number <?= $model->plot_number.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
            </td>
        </tr>

        <tr>
            <td  colspan="1" style="color:#0277BD; font-size:13;">Property Address :</td>
            <td  colspan="3" style="color:#212121; font-size:12;">Unit Number <?= $model->unit_number.', '.$model->building->title ?>
                Plot Number <?= $model->plot_number.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
            </td>
        </tr>
        <tr class="">
            <td  colspan="1" style="color:#0277BD; font-size:13;">Owner's Name:</td>
            <td  colspan="3" style="color:#212121; font-size:12;"><?= $woners_name ?></td>
        </tr>
    </table>
    <br><br>
    <table cellspacing="1" cellpadding="4" style="border: 1px dashed #64B5F6;" class="col-12">
        <tr  class="">
            <td  style="color:#0277BD; font-size:13;">Property Type/Description:</td>
            <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->no_of_bedrooms ?> Bedrooms <?= $model->property->title ?></td>
            <td  style="color:#0277BD; font-size:13;">Property Category:</td>
            <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category] ?></td>
        </tr>
        <tr>
            <td  style="color:#0277BD; font-size:13;">Total Built Up Area :</td>
            <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->net_built_up_area ?> square feet </td>
            <td  style="color:#0277BD; font-size:13;">Tenure:</td>
            <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
        </tr>
        <tr class="">
            <td  style="color:#0277BD; font-size:13;">Occupancy Status:</td>
            <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->occupancy_status ?></td>
            <td  style="color:#0277BD; font-size:13;">Developer’s name:</td>
            <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->developer->title ?></td>
        </tr>
        <tr>
            <td  style="color:#0277BD; font-size:13;">Market Value: </td>
            <td  style="color:#212121; font-size:12;"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
            <td  style="color:#0277BD; font-size:13;">Insurance Reinstatement Cost:</td>
            <td  style="color:#212121; font-size:12;">Not Applicable</td>
        </tr>
    </table>



    <?php
}else{
    ?>
    <table cellspacing="1" cellpadding="2" style="border: 1px dashed #64B5F6;" class="col-12">
        <tr>
            <td colspan="7" class="tableSecondHeading" style="color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;"><h5>Executive Valuation Report Summary</h5></td>
        </tr>

        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> A. Valuation Overview</h5></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Client’s Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= $model->client->title ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Client’s Customer Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= $model->client_name_passport ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Intended User(s)</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= ( $model->other_intended_users <> null) ? $model->other_intended_users : 'None' ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Instruction Date</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?php echo trim( date('l, jS \of F, Y', strtotime($model->instruction_date))) ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Valuation Scope</td><td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td><?php if($model->purpose_of_valuation == 3){ ?><td  colspan="3" style="color:#212121; font-size:12; width:293px;"  class="">Estimated Fair Value of the Subject Property</td>
            <?php } else{ ?>
                <td  colspan="2" style="color:#212121; font-size:12; width:293px;"  class=""><?=  Yii::$app->appHelperFunctions->valuationScopeArr[$model->valuation_scope] ?></td>
            <?php } ?>

        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"></td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class=""></td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""></td>
        </tr>

        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> B. Property Description - Location</h5></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Property Valued</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].' '.$model->property->title ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Project/Building Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= $model->building->title ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Unit Number</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= $model->unit_number ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Plot Number</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= $plot_number ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Sub-Community Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= $model->building->subCommunities->title ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Community Name</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= $model->building->communities->title ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> City and Country</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>, United Arab Emirates</td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"></td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class=""></td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""></td>
        </tr>


        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> C. Property Description -External</h5></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Tenure</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Plot Size</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= $plot_size ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Built-Up Area (BUA)</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= $model->inspectProperty->net_built_up_area ?> square feet <?= $source_bua ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Accommodation</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?php if($model->inspection_type == 3){?><?='Not known as desktop valuation. Assumed '; ?>
                <?php }else if($model->inspection_type == 1){ ?><?='Not known as drive-by valuation. Assumed '; ?>
                <?php } ?><?= $model->inspectProperty->no_of_bedrooms ?> Bedrooms
            </td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"></td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class=""></td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""></td>
        </tr>


        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> D. Valuation Details</h5></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Inspection Type</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type] ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Inspection Date</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?php if($model->inspection_type == 3){
                    ?><?='Not Applicable as desktop valuation '; ?>
                <?php }
                else{?><?php echo date('l, jS \of F, Y', strtotime($model->scheduleInspection->inspection_date)) ?>
                    <?php //echo Yii::$app->formatter->asDate($model->scheduleInspection->inspection_date,Yii::$app->params['fulldaydate']) ?>
                <?php } ?>
            </td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Valuation Date</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?=  date('l, jS \of F, Y', strtotime($detail->valuation_date)) ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Valuation Approach</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach] ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"></td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class=""></td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""></td>
        </tr>
        <tr>
            <td colspan="7" class="tableSecondHeading"><h5> E. Market Detail</h5></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Market Value</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:330px;"  class=""><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Market Value Rate</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot': "Not Applicable" ?></td>
        </tr>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:13;"> Market Rent</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?php if($model->client->id == 1){?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent * 0.9) .'- '. number_format($approver_data->estimated_market_rent * 1.1).' per annum' : "Not Applicable" ?>
                <?php }else{ ?><?= ($approver_data->estimated_market_rent <> null) ? "AED " . number_format($approver_data->estimated_market_rent) . ' per annum' : "Not Applicable" ?>
                <?php } ?>
            </td>
        </tr>
    <?php if($model->client->id != 110 && $model->client->id != 119087){ ?>
        <tr class="" style="width:10%;">
            <td colspan="2" style="color:#0277BD; font-size:11px;">Estimated Price Under the Restricted Marketing Period of 1 Month</td>
            <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="">:</td>
            <td colspan="4" style="color:#212121; font-size:12; width:293px;"  class=""><?=($approver_data->estimated_market_value <> null) ? "AED " . number_format($approver_data->estimated_market_value * 0.9)  : "" ?></td>
        </tr>
        <?php } ?>
    </table>


    <?php
}
?>