
<?php

die('dd');

$dataProvider= Yii::$app->PdfHelper->getTransactionlist($model);
$dataProvider2= Yii::$app->PdfHelper->getTransactionlistSold($model);

$selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $model->id, 'type' => 'list'])->all(), 'id', 'selected_id');
$selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $model->id, 'type' => 'sold'])->all(), 'id', 'selected_id');
$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();
$list = 0;
$sold = 0;
echo "<pre>";
print_r($selected_data_list);
die;
if($selected_data_list <> null && count($selected_data_list) > 0) {

    $list = 1;
    $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
    $totalResultsList = (new \yii\db\Query())
        ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
        ->from('listings_transactions')
        ->where(['id' => $selected_data_list])
        ->all();

    $curl_handle=curl_init();
    curl_setopt($curl_handle, CURLOPT_URL,\yii\helpers\Url::toRoute(['valuation/step_15_report','id'=>$model->id]));
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

    $val = curl_exec($curl_handle);
    curl_close($curl_handle);

    $response_list = json_decode($val);

    echo "<pre>";
    print_r($response_list);
    die;

}

if($selected_data_sold <> null && count($selected_data_sold) > 0) {

    $sold = 1;
    $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
    $totalResultsSold = (new \yii\db\Query())
        ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
        ->from('sold_transaction')
        ->where(['id' => $selected_data_sold])
        ->all();

    $curl_handle=curl_init();
    curl_setopt($curl_handle, CURLOPT_URL,\yii\helpers\Url::toRoute(['valuation/step_12_report','id'=>$model->id]));
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

    $val = curl_exec($curl_handle);
    curl_close($curl_handle);

    $response_sold = json_decode($val);

}
?>

<br pagebreak="true" />
<div class="col mx-auto">
<table class="col-12">
  <tr>
    <td class="table_of_content">
      <h3>5.03: Recent comparable market listings</h3>
    </td>
  </tr>
</table>

<br><br>
              <table  cellspacing="2" cellpadding="5" class="main-class col-12" style="font-size:11px">

              <tr><td class="table_of_content" style="font-size:13px;" colspan="19"><h3>Available for Sale Market Listings</h3></td></tr>
              <tr style="background-color:#ECEFF1; text-align:center" >
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">#</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">Listing Date</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">Building</td>
              <td colspan="2" style="color:#0277BD;"style="color:#0277BD;">No. of Rooms</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">Land Size</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">BUA</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">Price</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">Price / sf</td>
              </tr>



      <?php   $n=1; foreach ($dataProvider['Listselected']->models as $value) {
              $color='';   if ($n%2==0) {  $color='color';  }  ?>

              <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
              <td colspan="3"><?= $n ?></td>
              <td colspan="2"><?= $value['listing_date'] ?></td>
              <td colspan="3"><?= $value->building->title ?></td>
              <td colspan="2"><?= number_format($value['no_of_bedrooms']) ?></td>
              <td colspan="3"><?= number_format($value['land_size']) ?></td>
              <td colspan="2"><?= number_format($value['built_up_area']) ?></td>
              <td colspan="2"><?= number_format($value['listings_price']) ?></td>
              <td colspan="2"><?= number_format($value['price_per_sqt']) ?></td>
              </tr>

      <?php  $n++; }  ?>

            <?php if($list == 1){ ?>
                  <tr  style=" text-align:center; font-size:10px">
                      <td colspan="3">Average</td>
                      <td colspan="2"><?= $response_list->avg_date ?></td>
                      <td colspan="3"></td>
                      <td colspan="2"><?= number_format($response_list->avg_room) ?></td>
                      <td colspan="3"><?= number_format($response_list->avg_land_size) ?></td>
                      <td colspan="2"><?= number_format($response_list->avg_bua) ?></td>
                      <td colspan="2"><?= number_format($response_list->avg_listings_price_size) ?></td>
                      <td colspan="2"><?= number_format($response_list->avg_psf) ?></td>
                  </tr>
                  <tr  style=" text-align:center; font-size:10px">
                      <td colspan="3">Subject Property</td>
                      <td colspan="2"><?= date('Y-m-d') ?></td>
                      <td colspan="3"></td>
                      <td colspan="2"><?= number_format($inspection_data->no_of_bedrooms) ?></td>
                      <td colspan="3"><?= number_format($model->land_size) ?></td>
                      <td colspan="2"><?= number_format($inspection_data->built_up_area) ?></td>
                      <td colspan="2"></td>
                      <td colspan="2"></td>
                  </tr>
                  <tr  style=" text-align:center; font-size:10px">
                      <td colspan="3">Adjustments</td>
                      <td colspan="2"></td>
                      <td colspan="3"></td>
                      <td colspan="2"></td>
                      <td colspan="3"></td>
                      <td colspan="2"></td>
                      <td colspan="1">+/-</td>
                      <td colspan="3"><?= number_format($response_list->sold_adjustments) ?></td>
                  </tr>
                <tr  style=" text-align:center; font-size:10px">
                    <td colspan="3">Market Value</td>
                    <td colspan="2"></td>
                    <td colspan="3"></td>
                    <td colspan="2"></td>
                    <td colspan="3"></td>
                    <td colspan="2"></td>
                    <td colspan="1"></td>
                    <td colspan="3"><?= number_format($approver_data->estimated_market_value) ?></td>
                </tr>
                  <?php } ?>


              </table>

      <?php  if ($dataProvider['Listselected']->models!=null) {   ?>

              <br pagebreak="true" />

      <?php } ?>

            <!--  <br><br><br><br>

              <table cellspacing="2" cellpadding="5" class="main-class" style="font-size:11px">

              <tr><td class="table_of_content" style="font-size:13px;" colspan="19"><h3>Available for Sale Market Listings - Outliers</h3></td></tr>
              <tr style="background-color:#ECEFF1; text-align:center" >
              <td colspan="1" style="color:#0277BD;" style="color:#0277BD;">#</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">Listing Date</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">Building</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">No. of Rooms</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">Land Size</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">BUA</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">Price</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">Price / sf</td>
              </tr>



      <?php /*  $n=1; foreach ($dataProvider['Listunselected']->models as $value) {
              $color='';  if ($n%2==0) {  $color='color';  } */?>

              <tr class="<?/*= $color */?>" style=" text-align:center; font-size:10px">
              <td colspan="1"><?/*= $n */?></td>
              <td colspan="3"><?/*= $value['listing_date'] */?></td>
              <td colspan="3"><?/*= $value->building->title */?></td>
              <td colspan="3"><?/*= $value['no_of_bedrooms'] */?></td>
              <td colspan="3"><?/*= $value['land_size'] */?></td>
              <td colspan="2"><?/*= $value['built_up_area'] */?></td>
              <td colspan="2"><?/*= $value['listings_price'] */?></td>
              <td colspan="2"><?/*= $value['price_per_sqt'] */?></td>
              </tr>

      <?php /*$n++; } */?>

            </table>
-->

   <!-- <?php /* if ($dataProvider2['Soldselected']->models!=null) {   */?>
            <br pagebreak="true" />
    --><?php /*   }   */?>
<br><br>
<table class="col-12">
  <tr>
    <td class="table_of_content">
      <h3>5.04. Recent comparable sold transactions</h3>
    </td>
  </tr>
</table>

<br><br>
              <table cellspacing="2" cellpadding="5" class="main-class col-12" style="font-size:11px">

              <tr><td class="table_of_content" style="font-size:13px;" colspan="19"><h3>Sold Transaction Selected</h3></td></tr>
              <tr  style="background-color:#ECEFF1; text-align:center" >
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">#</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">Transaction Date</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">Building</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">No. of Rooms</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">Land Size</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">BUA</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">Price</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">Price / sf</td>
              </tr>


    <?php    $n=1;    foreach ($dataProvider2['Soldselected']->models as $value) {
             $color=''; if ($n%2==0) {  $color='color';  } ?>


              <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
              <td colspan="3"><?= $n ?></td>
              <td colspan="2"><?= $value['transaction_date'] ?></td>
              <td colspan="3"><?= $value->building->title ?></td>
              <td colspan="2"><?= number_format($value['no_of_bedrooms']) ?></td>
              <td colspan="3"><?= number_format($value['land_size']) ?></td>
              <td colspan="2"><?= number_format($value['built_up_area']) ?></td>
              <td colspan="2"><?= number_format($value['listings_price']) ?></td>
              <td colspan="2"><?= number_format($value['price_per_sqt']) ?></td>
              </tr>

    <?php $n++;  } ?>

                  <?php if($sold == 1){ ?>
                      <tr  style=" text-align:center; font-size:10px">
                          <td colspan="3">Average</td>
                          <td colspan="2"><?= $response_sold->avg_date ?></td>
                          <td colspan="3"></td>
                          <td colspan="2"><?= number_format($response_sold->avg_room) ?></td>
                          <td colspan="3"><?= number_format($response_sold->avg_land_size) ?></td>
                          <td colspan="2"><?= number_format($response_sold->avg_bua) ?></td>
                          <td colspan="2"><?= number_format($response_sold->avg_listings_price_size) ?></td>
                          <td colspan="2"><?= number_format($response_sold->avg_psf) ?></td>
                      </tr>
                      <tr  style=" text-align:center; font-size:10px">
                          <td colspan="3">Subject Property</td>
                          <td colspan="2"><?= date('Y-m-d') ?></td>
                          <td colspan="3"></td>
                          <td colspan="2"><?= number_format($inspection_data->no_of_bedrooms) ?></td>
                          <td colspan="3"><?= number_format($model->land_size) ?></td>
                          <td colspan="2"><?= number_format($inspection_data->built_up_area) ?></td>
                          <td colspan="2"></td>
                          <td colspan="2"></td>
                      </tr>
                      <tr  style=" text-align:center; font-size:10px">
                          <td colspan="3">Adjustments</td>
                          <td colspan="2"></td>
                          <td colspan="3"></td>
                          <td colspan="2"></td>
                          <td colspan="3"></td>
                          <td colspan="2"></td>
                          <td colspan="1">+/-</td>
                          <td colspan="3"><?= number_format($response_sold->sold_adjustments) ?></td>
                      </tr>
                      <tr  style=" text-align:center; font-size:10px">
                          <td colspan="3">Market Value</td>
                          <td colspan="2"></td>
                          <td colspan="3"></td>
                          <td colspan="2"></td>
                          <td colspan="3"></td>
                          <td colspan="2"></td>
                          <td colspan="1"></td>
                          <td colspan="3"><?= number_format($approver_data->estimated_market_value) ?></td>
                      </tr>
                  <?php } ?>

            </table>
          </div>



  <!--  <?php /*    if ($dataProvider['Soldunselected']->models!=null) {  */?>
              <br pagebreak="true" />
    <?php /* }  */?>



              <br><br><br><br>
              <table cellspacing="2" cellpadding="5" class="main-class" style="font-size:11px">

              <tr><td class="table_of_content" style="font-size:13px;" colspan="19"><h3>Sold Transaction Non Selected</h3></td></tr>
              <tr  style="background-color:#ECEFF1; text-align:center">
              <td colspan="1" style="color:#0277BD;">#</td>
              <td colspan="3" style="color:#0277BD;">Transaction Date</td>
              <td colspan="3" style="color:#0277BD;">Building</td>
              <td colspan="3" style="color:#0277BD;">No. of Rooms</td>
              <td colspan="3" style="color:#0277BD;">Land Size</td>
              <td colspan="2"style="color:#0277BD;">BUA</td>
              <td colspan="2"style="color:#0277BD;">Price</td>
              <td colspan="2"style="color:#0277BD;">Price/sf</td>
              </tr>



     <?php /* $n=1;  foreach ($dataProvider2['Soldunselected']->models as $value) {

        $color=''; if ($n%2==0) {  $color='color';  } */?>

               <tr class="<?/*= $color */?>" style=" text-align:center; font-size:10px">
               <td colspan="1"><?/*= $n */?></td>
               <td colspan="3"><?/*= $value['transaction_date'] */?></td>
               <td  colspan="3"><?/*= $value->building->title */?></td>
               <td colspan="3"><?/*= $value['no_of_bedrooms'] */?></td>
               <td colspan="3"><?/*= $value['land_size'] */?></td>
               <td colspan="2"><?/*= $value['built_up_area'] */?></td>
               <td colspan="2"><?/*= $value['listings_price'] */?></td>
               <td colspan="2"><?/*= $value['price_per_sqt'] */?></td>
               </tr>

     <?php /* $n++;  }   */?>


         </table>
-->
