<?php
//Google Map
$lon=  $model->inspectProperty->longitude;
$lan = $model->inspectProperty->latitude;
//$map_image_src = "https://maps.googleapis.com/maps/api/staticmap?zoom=11&size=900x300&maptype=hybrid&markers=color:red%7Clabel:B%7C".trim($lan).",".trim($lon)."&key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs";
//$Sateliteimage_src="https://maps.googleapis.com/maps/api/staticmap?zoom=11&size=900x300&maptype=roadmap&markers=color:red%7Clabel:B%7C".trim($lan).",".trim($lon)."&key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs";
$img_map = 'map/'.$model->id.'_m.png';
$img_satelite = 'map/'.$model->id.'_s.png';
if(!file_exists($img_map)){
    $map_image_src_url = "https://maps.googleapis.com/maps/api/staticmap?zoom=11&size=900x370&maptype=hybrid&markers=color:red%7Clabel:B%7C".trim($lan).",".trim($lon)."&key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs";
    file_put_contents($img_map, file_get_contents($map_image_src_url));
    $map_image_src = $img_map;
}else{
    $map_image_src = $img_map;
}
if(!file_exists($img_satelite)){
    $Sateliteimage_src_url="https://maps.googleapis.com/maps/api/staticmap?zoom=11&size=900x370&maptype=roadmap&markers=color:red%7Clabel:B%7C".trim($lan).",".trim($lon)."&key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs";
    file_put_contents($img_satelite, file_get_contents($Sateliteimage_src_url));
    $Sateliteimage_src = $img_satelite;
}else{
    $Sateliteimage_src = $img_satelite;
}

if($model->client->id == 183 || $model->id >12566){
    $appendices_number = '2';
}else {
    $appendices_number = '6';
}
?>
<?php /*?>
<br pagebreak="true"/>
<div style=" color:#0288D1;  font-size:14px; font-weight:bold; border-bottom: 1px solid black;">Inspection Sheet</div>
<br>
<table cellspacing="1" cellpadding="5" class="main-class" style="font-size:12px;">

  <tr><td class="table_of_content" colspan="2"><h4>Inspection Sheet</h4></td></tr>
  <tr><td class="table_of_content" style=" color:#0288D1;" colspan="2"><h5>Part I: Inspection Reference Details</h5></td></tr>


      <tr class="color">
        <td class="tdbold ">Surveyor’s name</td>
        <td ><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->instruction_person] ?></td>
      </tr>
      <tr>
        <td class="tdbold ">Date of the inspection</td>
        <td><?= $model->scheduleInspection->inspection_date ?></td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Client Reference Number</td>
        <td ><?= $model->client_reference ?></td>
      </tr>
      <tr>
        <td class="tdbold ">Windmills File Reference Number</td>
        <td><?= $model->reference_number ?></td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Related Party Disclosure</td>
        <td>None</td>
      </tr>
      <tr>
        <td class="tdbold ">Full address and postcode of the property</td>
        <td><?= $model->unit_number.','.$model->plot_number.','.$model->building->subCommunities->title.','.$model->building->communities->title.','.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?></td>
      </tr>
      <tr class="color">
        <td class="tdbold ">The status of the property when the inspection took
            place</td>
        <td>Ready</td>
      </tr>
      <tr>
        <td class="tdbold ">Type of property</td>
        <td><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].','.$model->property->title ?></td>
      </tr>


      <tr><td class="table_of_content" style=" color:#0288D1; border-top:1px solid #0D47A1" colspan="2"><h5>Part II: About the Subject Property</h5></td></tr>


      <tr class="color">
        <td class="tdbold ">Approximate year the property was built</td>
        <td><?= $model->inspectProperty->estimated_age ?></td>
      </tr>
      <tr>
        <td class="tdbold ">Property Exposure</td>
        <td>Middle and Single Row</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Accommodation</td>
        <td><?= $model->inspectProperty->no_of_bedrooms ?> Bedrooms</td>
      </tr>
      <tr>
        <td class="tdbold ">Basement Floor Configuration</td>
        <td>Not Applicable</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Ground Floor Configuration</td>
        <td><?= $number_of_rooms ?></td>
      </tr>
      <tr>
        <td class="tdbold ">First Floor Configuration</td>
        <td><?= $number_of_rooms ?></td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Second Floor Configuration</td>
        <td><?= $number_of_rooms ?></td>
      </tr>


      <tr><td class="table_of_content" style=" color:#0288D1; border-top:1px solid #0D47A1" colspan="2"><h5>Part III: Quality and Maintainance</h5></td></tr>


      <tr class="color">
        <td class="tdbold ">Construction Quality of the subject property (Rating on Scale of 1 to 5)</td>
        <td><?= $model->valuationConfiguration->over_all_upgrade ?></td>
      </tr>
      <tr>
        <td class="tdbold ">Defects within the subject property</td>
        <td>None.</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Upgrades within subject property (Example Cielings, Walls, Kitchen,
          Bathroom Fittings, Conservatory/Porches)
        </td>
        <td>None.</td>
      </tr>
      <tr>
        <td class="tdbold ">Size of the subject property (as per measurements, in square feet)</td>
        <td>1,889 square feet </td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Developer of subject property</td>
        <td>Emaar</td>
      </tr>
      <tr>
        <td class="tdbold ">Occupancy Status</td>
        <td>Vacant</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Elevators in subject property</td>
        <td>No</td>
      </tr>
      <tr>
        <td class="tdbold ">Fire Alarm System, Security and Safety Equipment</td>
        <td>Yes</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Common Facilities Provided</td>
        <td>Fully Equipped Gym, Swimming Pool, Basket Football, Tennis
          Court and Children Playground (as per REIDIN)</td>
      </tr>
      <tr>
        <td class="tdbold ">Total Floors of Subject Property</td>
        <td>Ground + 1 Floor</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Location rating of subject property</td>
        <td>Good</td>
      </tr>
      <tr>
        <td class="tdbold ">Access to Public Transportation (Bus, Tram, Train, Taxi Stand)</td>
        <td>Private Transportation</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Access to shops and community facilities</td>
        <td>Yes</td>
      </tr>
      <tr>
        <td class="tdbold ">Hazardous materials kept on the property (e.g chemicals, radioactive
              substances)</td>
        <td>None</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Gas, Water, Drainage and Electricity Connections</td>
        <td>Connected</td>
      </tr>

      <tr>
        <td class="tdbold ">Central Heating/Air Conditioning System:</td>
        <td>Yes</td>
      </tr>

      <tr class="color">
        <td class="tdbold ">Parking available:</td>
        <td>Yes</td>
      </tr>


      <tr>
        <td class="tdbold ">Any Natural Hazards (ground instability, mining or mineral extraction, risk of Flooding)
        </td>
        <td>None</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Non-natural hazards such as ground contamination</td>
        <td>None</td>
      </tr>
      <tr>
        <td class="tdbold ">Potential for development or redevelopment</td>
        <td>Not known</td>
      </tr>


<tr><td class="table_of_content" style=" color:#0288D1; border-top:1px solid #0D47A1" colspan="2"><h5>Part IV: Quality and Assurance</h5></td></tr>



      <tr class="color">
        <td class="tdbold ">Construction Quality of the subject property (Rating on Scale of 1 to 5)</td>
        <td>4</td>
      </tr>
      <tr>
        <td class="tdbold ">Defects within the subject property</td>
        <td>None.</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Upgrades within subject property (Example Cielings, Walls, Kitchen,
                            Bathroom Fittings, Conservatory/Porches)</td>
        <td>None.</td>
      </tr>
      <tr>
        <td class="tdbold ">Size of the subject property (as per measurements, in square feet)</td>
        <td>2,193 square feet (as per Sale Purchase Agreement)</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Developer of subject property</td>
        <td>Damac Properties</td>
      </tr>
      <tr>
        <td class="tdbold ">Occupancy Status</td>
        <td>Tenanted</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Elevators in subject property</td>
        <td>No</td>
      </tr>
      <tr>
        <td class="tdbold ">Fire Alarm System, Security and Safety Equipment</td>
        <td>Yes</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Common Facilities Provided</td>
        <td>Fully Equipped Gym, Swimming Pool, Basket Football, Tennis
          Court and Children Playground (as per REIDIN)</td>
      </tr>
      <tr>
        <td class="tdbold ">Total Floors of Subject Property</td>
        <td>Ground + 1 Floor</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Location rating of subject property</td>
        <td>Good</td>
      </tr>
      <tr>
        <td class="tdbold ">Access to Public Transportation (Bus, Tram, Train, Taxi Stand)</td>
        <td>Private Transportation</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Access to shops and community facilities</td>
        <td>Yes</td>
      </tr>
      <tr>
        <td class="tdbold ">Hazardous materials kept on the property (e.g chemicals, radioactive
              substances)</td>
        <td>None</td>
      </tr>
      <tr class="color">
        <td class="tdbold ">Gas, Water, Drainage and Electricity Connections</td>
        <td>Connected</td>
      </tr>
</table>
<?php */?>
<br pagebreak="true"/>

<div  style=" color:#E65100;
       text-align: center;
       font-size:17px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
    0<?=$appendices_number?>. Appendices
</div>
<br>
      <table class="main-class ">
        <tr>
            <td class="table_of_content"><h3><?=$appendices_number?>.01. Property Location Map</h3></td>
        </tr>
        <br>
        <tr>
            <td>
                <img src="<?= $map_image_src ?>" style=" display: block; margin: 0 auto;" >
            </td>
        </tr>
        <br>
        <tr>
            <td>
                <img src="<?= $Sateliteimage_src ?>" style=" display: block; margin: 0 auto;">
            </td>
        </tr>
    </table>
    <br pagebreak="true" />

    <?php if($model->inspection_type != 3){?>
        
        <table class="mx-auto">
            <tr>
                <td class="table_of_content"><h3><?=$appendices_number?>.02. Property Photos</h3></td>
            </tr>
        </table>
        <br/>
        <br/>
        <table class="mx-auto" cellspacing="1" cellpadding="1" class="main-class" style="font-size:12px;">
            <tr>

                <?php


                $configuration = \app\models\ConfigurationFiles::find()->select(['type','attachment','custom_field_id','rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on'])->asArray()->all();

                $n=1; $j=1;$count=0;

                foreach ($configuration as $key => $value) {
                $attachment_type = $value['type'];
                if($value['type'] == 'custom_fields'){
                    $custom_attachment= \app\models\CustomAttachements::find()->where(['id' => $value['custom_field_id']])->one();
                    $value['type'] = $custom_attachment->name;
                }

                if ($value['attachment']!=null && $value['type'] !='') {
                $image =  $value['rep_image'];

                $title = str_replace("_", " ", str_replace("config"," ",$value['type']));
                $title=ucfirst(trim($title));
                if($attachment_type != 'custom_fields') {
                    $title = rtrim($title, "s ");
                }
                ?>

                <td
                        style="color:#0D47A1;
                              font-size:14px;
                              font-weight:bold;
                              text-align:center;">
                   <!-- <h4><?/*= ($title.' No '.($i+1)) */?></h4>-->
                    <h4><?= ($title); ?></h4>
                    <img src="<?= $image ?>"  width="250px" height="250px" style=" display: block; margin: 0 auto;"></td>


                <?php
                $n++;
                $count++;
                if ($n==3) {
                $n=1;
                ?>
            </tr><br/><tr collapse='2'><td></td></tr><tr>

                <!-- </table>  -->

                <?php

                }
                //      $j++;
                // if ($j==3) {
                //     $j=1;
                ?>
                <!-- <br pagebreak="true" /> -->
                <?php  //}   ?>

                <!-- <br><br>
                  <table cellspacing="1" cellpadding="5" class="main-class mx-auto" style="font-size:12px;">
                  <tr> -->

                <?php   //}
                if($count == 8){?>
                    <br pagebreak="true" />
                <?php
                }
                }
                }


                ?>




            </tr>
        </table>
        

    <?php }

    //die('hrewqdq324234rr');
    ?>


