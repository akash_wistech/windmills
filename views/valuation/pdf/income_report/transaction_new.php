<?php
/*echo $model->property->valuation_approach;
die;*/
if ($model->client->id == 183 || $model->id >12566 || $model->client->id == 71) {
    $appendices_number = '2';
} else {
    $appendices_number = '6';
}

$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();
if($model->valuation_approach == 1 || $model->property->valuation_approach == 3) {

    $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
    $stype = 0;
    if ($InspectProperty->no_studios > 0) {
        $stype = 1;
?>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?= $appendices_number ?>.03. transactions
                    </h3>
                </td>
            </tr>
        </table>
        <?php
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
         //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }

        ?>


        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="21">
                <h2>Studio</h2>
            </td>
        </tr>
         <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="21">
                <h3> Ejari Transaction </h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="2" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {

                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="2">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="3" class="tdBorder">+/-</td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                         Market Rents (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_one_bedrooms > 0) {
        $stype = 2;

        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }

        ?>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h2>One Bedrooms</h2>
            </td>
        </tr>
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h3> Ejari Transaction </h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        Market Rents (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_two_bedrooms > 0) {
        $stype = 3;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }

        ?>


        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h2>Two Bedrooms</h2>
            </td>
        </tr>
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h3> Ejari Transaction </h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        Market Rents (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>


        </table>
        <?php
    }


    if ($InspectProperty->no_three_bedrooms > 0) {
        $stype = 4;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }

        ?>


        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h2>Three Bedrooms</h2>
            </td>
        </tr>
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h3> Ejari Transaction </h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        Market Rents (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_four_bedrooms > 0) {
        $stype = 5;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }

        ?>


        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h2>Four Bedrooms</h2>
            </td>
        </tr>
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h3> Ejari Transaction </h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        Market Rents (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_penthouse > 0) {
        $stype = 6;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }

        ?>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h2>Penthouse</h2>
            </td>
        </tr>
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h3> Ejari Transaction </h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        Market Rents (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_of_shops > 0) {
        $stype = 7;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }

        ?>


        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h2>Shops</h2>
            </td>
        </tr>
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h3> Ejari Transaction </h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        Market Rents (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_of_offices > 0) {
        $stype = 8;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }

        ?>


        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h2>Offices</h2>
            </td>
        </tr>
        <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h3> Ejari Transaction </h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        Market Rents (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Unit Number</td>
                <td colspan="4" style="color:#0277BD;">Type</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>

            <?php } ?>


        </table>
        <?php
    }

}
 ?>