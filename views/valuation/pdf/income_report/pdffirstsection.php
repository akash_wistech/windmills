<?php

$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();

$ValuerName= \app\models\User::find()->select(['firstname', 'lastname'])->where(['id'=>$approver_data->created_by])->one();

$unitNumber = ($model->unit_number <> "" || $model->unit_number == 0) ? $model->unit_number : "Not Applicable";
?>
<br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br>

<table style="font-size:11px; line-height: 12px; padding-top: 1px; padding-left:-15px">
        <tr>
        <td colspan="2" style="padding-top: 50px"><h4 style="font-weight:normal"> Property</h4></td>
        <td colspan="5" style="padding-top: 50px"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].' '.$model->property->title ?></td>
      </tr>
      <tr>
      <td colspan="2"><h4  style="font-weight:normal"> Property Address</h4></td>
      <td colspan="5"><?php if($model->unit_number > 0) { echo 'Unit Number '. $unitNumber.', '; }?><?= $model->building->title; ?></td>
      </tr>
      <tr>
        <td colspan="2"></td>
         <td colspan="5">Plot Number <?= $model->plot_number.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?></td>
         </tr>
      <tr>
        <td colspan="2"><h4  style="font-weight:normal"> Client Reference No</h4></td>
        <td colspan="5"><?= ($model->client_reference <> "") ? $model->client_reference : "Not Applicable" ?></td>
      </tr>
      <tr>
      <td colspan="2"><h4  style="font-weight:normal"> Windmills Reference</h4></td>
      <td colspan="5"><?= $model->reference_number ?></td>
      </tr>
      <tr>
        <td colspan="2"><h4  style="font-weight:normal"> Client Name</h4></td>
          <?php if($model->id==7251){ ?>
              <td colspan="5" style="font-size: 8.5px;"><?= $model->client->title ?></td>
          <?php }else{ ?>
              <td colspan="5"><?= $model->client->title ?></td>
          <?php } ?>
      </tr>
    <tr>
        <td colspan="2"><h4  style="font-weight:normal"> Valuer Name</h4></td>
        <td colspan="5"><?= $ValuerName['firstname'].' '.$ValuerName['lastname']?></td>
    </tr>
    <tr>
        <td colspan="2"><h4  style="font-weight:normal"> Service Officer Name</h4></td>
        <td colspan="5"><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->service_officer_name] ?></td>
    </tr>
      <tr>
      <td colspan="2"><h4  style="font-weight:normal"> Valuation Report Date</h4></td>
      <!--<td colspan="3"><?/*= Yii::$app->formatter->asDate($model->scheduleInspection->valuation_date) */?></td>-->
     <?php if($model->submission_approver_date <> null){ ?>
          <td colspan="5"><?= ($model->submission_approver_date  <> null) ? date('l, jS \of F, Y', strtotime($model->submission_approver_date )) : '' ?></td>
     <?php }else{
         $date = date('Y-m-d');
         ?>
      <td colspan="5"><?= ($date <> null) ? date('l, jS \of F, Y', strtotime($date)) : '' ?></td>
      <?php } ?>
      </tr>
</table>


