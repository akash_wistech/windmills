<?php
/*echo $model->property->valuation_approach;
die;*/
if ($model->client->id == 183 || $model->id >12566) {
    $appendices_number = '2';
} else {
    $appendices_number = '6';
}
if($model->property->valuation_approach == 1 || $model->property->valuation_approach == 3){
    /*
    $dataProvider= Yii::$app->PdfHelper->getTransactionlist($model);
    $dataProvider2= Yii::$app->PdfHelper->getTransactionlistSold($model);
    if($model->id <= 8790 && $model->valuation_status == 5) {
        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0])->all(), 'id', 'selected_id');
        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0])->all(), 'id', 'selected_id');
    }else{
        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list','search_type'=>0,'latest'=>1])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');
        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold','search_type'=>0,'latest'=>1])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');
    }   
    $approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();
    $list = 0;
    $sold = 0;
    if($selected_data_list <> null && count($selected_data_list) > 0) {

        $list = 1;
        $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
        $totalResultsList = (new \yii\db\Query())
            ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
            ->from('listings_transactions')
            ->where(['id' => $selected_data_list])
            ->one();

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation/step_15_report', 'id' => $model->id]));
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

        $val = curl_exec($curl_handle);
        curl_close($curl_handle);

        $response_list = json_decode($val);

        //   echo "<pre>";
         //  print_r($response_list);
         //  die;

    }

    if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

        $sold = 1;
        $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
        $totalResultsSold = (new \yii\db\Query())
            ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
            ->from('sold_transaction')
            ->where(['id' => $selected_data_sold])
            ->one();

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation/step_12_report', 'id' => $model->id]));
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

        $val = curl_exec($curl_handle);
        curl_close($curl_handle);

        $response_sold = json_decode($val);

        $market = 'Market';
        if ($model->purpose_of_valuation == 3) {
            $market = 'Fair';
        }
    }
    ?>

    <br pagebreak="true" />

    <table class="col-12">
        <tr>
            <td class="table_of_content">
                <h3>
                    <?= $appendices_number ?>.03: Recent comparable sold transactions <?php  if($model->building->city == 3510) { ?>(Source Reidin)<?php } ?>
                </h3>
            </td>
        </tr>
    </table>

    <br><br>
    <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

        <!-- <tr>
            <td class="table_of_content" style="font-size:13px;" colspan="19">
                <h3>Sold Transaction Selected</h3>
            </td>
        </tr> -->
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Value
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($approver_data->estimated_market_value) ?>
                    </td>
                </tr>
            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Sold transactions are not available</td>
            </tr>
            <?php
        }
        ?>
    </table>

    <?php 
    if($sold == 1 && $model->client->id == 4){ ?>
        <br/>
        <br/>


        <table cellspacing="1" cellpadding="4" style="padding-left: 0px; font-size:11px;">
            <tr>
                <td>Recent and Comparable Sold Transaction Price Analysis:
                </td>
            </tr>
        </table>
        <!-- <p>Recent and Comparable Sold Transaction Prices Analysis: </p> -->

        <table cellspacing="1" cellpadding="4" style="padding-left: 0px;font-size:11px;" >
            <tr>
                <td style="width: 75%;text-align: left">
                    The average transaction price of the recent sold transactions chosen is :
                </td>
                <td style="width: 20%; text-align: right">
                    AED <?=  (isset($response_sold->avg_listings_price_size) && $response_sold->avg_listings_price_size <> null) ? number_format($response_sold->avg_listings_price_size): 0 ?>
                </td>
            </tr>
        </table>
        <!-- <p>The average transaction price/sqft of the recent sold transactions chosen is :
<span style="font-size: 11px;text-align: right;">
AED <?//= (isset($response_sold->avg_psf) && $response_sold->avg_psf <> null) ? number_format($response_sold->avg_psf): 0  ?>
</span>
</p>-->
        <table cellspacing="1" cellpadding="4" style="padding-left: 0px;font-size:11px;">
            <tr>
                <td style="width: 75%;text-align: left">
                    The average transaction price/sqft of the recent sold transactions chosen is :
                </td>
                <td style="width: 20%; text-align: right">
                    AED <?= (isset($response_sold->avg_psf) && $response_sold->avg_psf <> null) ? number_format($response_sold->avg_psf): 0 ?>
                </td>
            </tr>
        </table>


        <br/>
        <br/>
        <table cellspacing="1" cellpadding="4" style="padding-left: 0px; font-size:11px;">
            <tr>
                <td>After making adjustments for <span style="font-size: 11px;">
                <?= (isset($response_sold->adjustments) && $response_sold->adjustments <> null) ? $response_sold->adjustments : '' ?>
                </span>
                </td>
            </tr>
        </table>

        <!-- <p>After making adjustments for <span style="font-size: 12px;">
            <?= (isset($response_sold->adjustments) && $response_sold->adjustments <> null) ? $response_sold->adjustments : '' ?>
            </span>
        </p> -->


        <!-- <p>The average transaction price for the subject property arrived at

<span style="font-size: 11px; text-align: right;">
AED <?//= (isset($response_sold->mv_total_price) && $response_sold->mv_total_price <> null) ? number_format($response_sold->mv_total_price): 0 ?>
</span>



</p>-->
        <table cellspacing="1" cellpadding="4" style="padding-left: 0px; font-size:11px;">
            <tr>
                <td style="width: 75%;text-align: left">
                    The average transaction price for the subject property arrived at :
                </td>
                <td style="width: 20%; text-align: right">
                    AED <?= (isset($response_sold->mv_total_price) && $response_sold->mv_total_price <> null) ? number_format($response_sold->mv_total_price): 0 ?>
                </td>
            </tr>
        </table>
        <!-- <p>The average transaction price/sqft for the subject property arrived at
<span style="font-size: 11px; text-align: right;">
AED <?//= (isset($response_sold->mv_avg_psf) && $response_sold->mv_avg_psf <> null) ? number_format($response_sold->mv_avg_psf): 0 ?>
</span>

</p>-->
        <table cellspacing="1" cellpadding="4" style="padding-left: 0px; font-size:11px;">
            <tr>
                <td style="width: 75%;text-align: left">
                    The average transaction price/sqft for the subject property arrived at :
                </td>
                <td style="width: 20%; text-align: right">
                    AED <?= (isset($response_sold->mv_avg_psf) && $response_sold->mv_avg_psf <> null) ? number_format($response_sold->mv_avg_psf): 0 ?>
                </td>
            </tr>
        </table>


    <?php } ?>


    <?php 
    if ($dataProvider['Listselected']->models != null) { ?>

        <br pagebreak="true" />

    <?php } ?>

        <!--  <br><br><br><br>

              <table cellspacing="2" cellpadding="5" class="main-class" style="font-size:11px">

              <tr><td class="table_of_content" style="font-size:13px;" colspan="19"><h3>Available for Sale Market Listings - Outliers</h3></td></tr>
              <tr style="background-color:#ECEFF1; text-align:center" >
              <td colspan="1" style="color:#0277BD;" style="color:#0277BD;">#</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">Listing Date</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">Building</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">No. of Rooms</td>
              <td colspan="3" style="color:#0277BD;" style="color:#0277BD;">Land Size</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">BUA</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">Price</td>
              <td colspan="2" style="color:#0277BD;" style="color:#0277BD;">Price / sf</td>
              </tr>



      <?php /*  // $n=1; foreach ($dataProvider['Listunselected']->models as $value) {
         $color='';  if ($n%2==0) {  $color='color';  }  ?>

              <tr class="<? //= $color *?>" style=" text-align:center; font-size:10px">
              <td colspan="1"><? //= $n *?></td>
              <td colspan="3"><? //= $value['listing_date'] *?></td>
              <td colspan="3"><? //= $value->building->title *?></td>
              <td colspan="3"><? //= $value['no_of_bedrooms'] *?></td>
              <td colspan="3"><? //= $value['land_size'] *?></td>
              <td colspan="2"><? //= $value['built_up_area'] *?></td>
              <td colspan="2"><? //= $value['listings_price'] *?></td>
              <td colspan="2"><? //= $value['price_per_sqt'] *?></td>
              </tr>

      <?php //$n++; } *?>

            </table>
        -->

        <!-- <?php // if ($dataProvider2['Soldselected']->models!=null) {   *?>
            <br pagebreak="true" />
        -->
        <?php //   }   *?>
        <br><br>


        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?= $appendices_number ?>.04. Recent comparable market listings (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->property->title ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>
            
        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="3" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Value
                    </td>
                    <td colspan="4" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($approver_data->estimated_market_value) ?>
                    </td>
                </tr>
            <?php } ?>


        </table>

    <?php if($list == 1 && $model->client->id == 4){ ?>

        <br/>
<br/>

<table cellspacing="1" cellpadding="4" style="padding-left: 0px; font-size:11px;">
            <tr>
                <td>Recent and Comparable Market Listings Analysis:
                </td>
            </tr>
        </table>
        <!-- <p style="font-size:11px;">Recent and Comparable Market Listings Analysis: </p> -->
        <!--  <p style="background: #00b3ee;">The average asking price of the recent market listings chosen is :
<span style="font-size: 11px; text-align: right;">
AED <?//= (isset($response_list->avg_listings_price_size) && $response_list->avg_listings_price_size <> null) ? number_format($response_list->avg_listings_price_size): 0 *?>
</span>
</p>-->
        <table cellspacing="1" cellpadding="4" style="padding-left: 0px; font-size:11px;">
            <tr>
                <td style="width: 75%;text-align: left">
                    The average asking price of the recent market listings chosen is :
                </td>
                <td style="width: 20%; text-align: right">
                    AED <?= (isset($response_list->avg_listings_price_size) && $response_list->avg_listings_price_size <> null) ? number_format($response_list->avg_listings_price_size): 0 ?>
                </td>
            </tr>
        </table>



        <!--  <p>The average asking price/sqft of the recent market listings chosen is :
<span style="font-size: 11px; text-align: right;">
AED <?//= (isset($response_list->avg_psf) && $response_list->avg_psf <> null) ? number_format($response_list->avg_psf): 0 *?>
</span>
</p>-->
        <table cellspacing="1" cellpadding="4" style="padding-left: 0px; font-size:11px;">
            <tr>
                <td style="width: 75%;text-align: left">
                    The average asking price/sqft of the recent market listings chosen is :
                </td>
                <td style="width: 20%; text-align: right">
                    AED <?= (isset($response_list->avg_psf) && $response_list->avg_psf <> null) ? number_format($response_list->avg_psf): 0 ?>
                </td>
            </tr>
        </table>
        <br/>
        <br/>
        <table cellspacing="1" cellpadding="4" style="padding-left: 0px; font-size:11px;">
            <tr>
                <td>After making adjustments for <span style="font-size: 11px;">
                    <?= (isset($response_list->adjustments) && $response_list->adjustments <> null) ? $response_list->adjustments : '' ?>, and after applying sales negotiation discount of <?= $response_list->sales_discount ?>% after discussion with numerous agents.
                    </span>
                </td>
            </tr>
        </table>

        <!-- <p>After making adjustments for <span style="font-size: 11px; font-size:11px;">
            <?= (isset($response_list->adjustments) && $response_list->adjustments <> null) ? $response_list->adjustments : '' ?>, and after applying sales negotiation discount of <?= $response_list->sales_discount ?>% after discussion with numerous agents.
            </span>
        </p> -->



        <!-- <p>The average transaction price for the subject property arrived at

<span style="font-size: 11px; text-align: right;">
AED <?//= (isset($response_list->mv_total_price) && $response_list->mv_total_price <> null) ? number_format($response_list->mv_total_price): 0 *?>
</span>

</p>-->

        <table cellspacing="1" cellpadding="4" style="padding-left: 0px;font-size:11px;">
            <tr>
                <td style="width: 75%;text-align: left">
                    The average transaction price for the subject property arrived at :
                </td>
                <td style="width: 20%; text-align: right">
                    AED <?= (isset($response_list->mv_total_price) && $response_list->mv_total_price <> null) ? number_format($response_list->mv_total_price): 0 ?>
                </td>
            </tr>
        </table>
        <!-- <p>The average transaction price/sqft for the subject property arrived at
<span style="font-size: 11px; text-align: right;">
AED <?//= (isset($response_list->mv_avg_psf) && $response_list->mv_avg_psf <> null) ? number_format($response_list->mv_avg_psf): 0 *?>
</span>

</p>-->
        <table cellspacing="1" cellpadding="4" style="padding-left: 0px;font-size:11px;">
            <tr>
                <td style="width: 75%;text-align: left">
                    The average transaction price/sqft for the subject property arrived at :
                </td>
                <td style="width: 20%; text-align: right">
                    AED <?= (isset($response_list->mv_avg_psf) && $response_list->mv_avg_psf <> null) ? number_format($response_list->mv_avg_psf): 0 ?>
                </td>
            </tr>
        </table>



    <?php } ?>


<?php */
} else if ($model->property->valuation_approach == 2) {

if($model->id > 13860 && ($model->parent_id == null || $model->parent_id >  13860)) {

    $curl_handle = curl_init();
    curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_20_report', 'id' => $model->id]));
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

    $val = curl_exec($curl_handle);
    curl_close($curl_handle);

    $response_list = json_decode($val);
}else{
    $curl_handle = curl_init();
    curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation/step_20_report', 'id' => $model->id]));
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

    $val = curl_exec($curl_handle);
    curl_close($curl_handle);

    $response_list = json_decode($val);
}

    /*echo "<pre>";
    print_r($response_list);
    die('ddd');*/

    ?>
        <br pagebreak="true" />
        <div class="col mx-auto">
            <table class="col-12">
                <tr>
                    <td class="table_of_content">
                        <h3>
                        <?= $appendices_number ?>.03: Income Approach
                        </h3>
                    </td>
                </tr>
            </table>

            <br><br>
            <table cellspacing="2" cellpadding="5" class="main-class col-12" style="font-size:11px">


                <tr style="background-color:#ECEFF1; text-align:center">
                    <td colspan="9" style="color:#0277BD;" style="color:#0277BD;">Income Subjects</td>
                    <td colspan="10" style="color:#0277BD;" style="color:#0277BD;">Property Financial Source</td>
                </tr>




                <tr class="" style=" text-align:center; font-size:10px">
                    <td colspan="9"> Gross Rental Income</td>
                    <td colspan="10">
                    <?= number_format($response_list->income_approach->gross_rental_income->source) ?>
                    </td>
                </tr>
                <tr class="color" style=" text-align:center; font-size:10px">
                    <td colspan="9"> Operations and Maintenance Costs</td>
                    <td colspan="10">
                    <?= number_format($response_list->income_approach->operations_and_maintainance_costs->source) ?>
                    </td>
                </tr>
                <tr class="" style=" text-align:center; font-size:10px">
                    <td colspan="9"> Vacancy</td>
                    <td colspan="10">
                    <?= number_format($response_list->income_approach->vacancy->source) ?>
                    </td>
                </tr>
                <tr class="color" style=" text-align:center; font-size:10px">
                    <td colspan="9"> Land Lease Costs</td>
                    <td colspan="10">
                    <?= number_format($response_list->income_approach->land_lease_costs->source) ?>
                    </td>
                </tr>
                <tr class="" style=" text-align:center; font-size:10px">
                    <td colspan="9"> Utilities Cost</td>
                    <td colspan="10">
                    <?= number_format($response_list->income_approach->insurance->source) ?>
                    </td>
                </tr>
                <tr class="color" style=" text-align:center; font-size:10px">
                    <td colspan="9"> Net Income</td>
                    <td colspan="10">
                    <?= number_format($response_list->income_approach->net_income->source) ?>
                    </td>
                </tr>
                <tr class="" style=" text-align:center; font-size:10px">
                    <td colspan="9"> Net Yield Applied</td>
                    <td colspan="10">
                    <?= number_format($response_list->income_approach->net_yield_applied->factor, 2) ?> %
                    </td>
                </tr>
                <tr class="color" style=" text-align:center; font-size:10px">
                    <td colspan="9"> Estimated
                    <?= $market; ?> Value <br>
                        as per Income Approach
                    </td>
                    <td colspan="10">
                    <?= number_format($response_list->income_approach->estimated_market_value->source) ?>
                    </td>
                </tr>




            </table>
    <?php
    $listing_appendex = 3;
} else if ($model->property->valuation_approach == 4) {
    /* 
    $curl_handle = curl_init();
    curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation/step_19_report', 'id' => $model->id]));
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

    $val = curl_exec($curl_handle);
    curl_close($curl_handle);

    $response_list = json_decode($val);


    ?>
                <br pagebreak="true" />
                <div class="col  mx-auto">
                    <table class="col-12">
                        <tr>
                            <td class="table_of_content">
                                <h3>
                            <?= $appendices_number ?>.03: Cost Approach
                                </h3>
                            </td>
                        </tr>
                    </table>

                    <br><br>
                    <table cellspacing="2" cellpadding="5" class="main-class col-12" style="font-size:11px">


                        <tr style="background-color:#ECEFF1; text-align:center">
                            <td colspan="9" style="color:#0277BD;" style="color:#0277BD;">Cost Subjects</td>
                            <td colspan="10" style="color:#0277BD;" style="color:#0277BD;">Calculations</td>
                        </tr>




                        <tr class="" style=" text-align:center; font-size:10px">
                            <td colspan="9"> Land
                        <?= $market; ?> Value
                            </td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->land_market_value->calculations) ?>
                            </td>
                        </tr>
                        <tr class="color" style=" text-align:center; font-size:10px">
                            <td colspan="9"> Financing Charges for Land</td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->financing_charges_for_land->calculations) ?>
                            </td>
                        </tr>
                        <tr class="" style=" text-align:center; font-size:10px">
                            <td colspan="9"> Construction Costs</td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->construction_costs->calculations) ?>
                            </td>
                        </tr>
                        <tr class="color" style=" text-align:center; font-size:10px">
                            <td colspan="9">Professional Charges</td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->professional_charges->calculations) ?>
                            </td>
                        </tr>
                        <tr class="" style=" text-align:center; font-size:10px">
                            <td colspan="9"> Contingency Margin</td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->contingency_margin->calculations) ?>
                            </td>
                        </tr>
                        <tr class="color" style=" text-align:center; font-size:10px">
                            <td colspan="9"> Effective interest rate</td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->effective_interest_rate->calculations) ?>
                            </td>
                        </tr>
                        <tr class="" style=" text-align:center; font-size:10px">
                            <td colspan="9"> Depreciation</td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->depreciation->calculations) ?>
                            </td>
                        </tr>
                        <tr class="color" style=" text-align:center; font-size:10px">
                            <td colspan="9"> Obscolence</td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->obscolence->calculations) ?>
                            </td>
                        </tr>

                        <tr class="" style=" text-align:center; font-size:10px">
                            <td colspan="9"> Net Development Cost</td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->net_development_cost->total_value_by_cost) ?>
                            </td>
                        </tr>
                        <tr class="color" style=" text-align:center; font-size:10px">
                            <td colspan="9"> Land Value Plus<br>
                                Net Development Cost</td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->land_value_plus_net_development_cost->total_value_by_cost) ?>
                            </td>
                        </tr>
                        <tr class="" style=" text-align:center; font-size:10px">
                            <td colspan="9"> Developer Profit </td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->developer_profit->total_value_by_cost) ?>
                            </td>
                        </tr>
                        <tr class="color" style=" text-align:center; font-size:10px">
                            <td colspan="9"> Estimated Total Cost</td>
                            <td colspan="10">
                        <?= number_format($response_list->cost_approach->estimated_total_cost->total_value_by_cost) ?>
                            </td>
                        </tr>




                    </table>


            <?php */
} ?>




        <!--  <?php /*    if ($dataProvider['Soldunselected']->models!=null) {  */?>
              <br pagebreak="true" />
    <?php /* }  */?>



              <br><br><br><br>
              <table cellspacing="2" cellpadding="5" class="main-class" style="font-size:11px">

              <tr><td class="table_of_content" style="font-size:13px;" colspan="19"><h3>Sold Transaction Non Selected</h3></td></tr>
              <tr  style="background-color:#ECEFF1; text-align:center">
              <td colspan="1" style="color:#0277BD;">#</td>
              <td colspan="3" style="color:#0277BD;">Transaction Date</td>
              <td colspan="3" style="color:#0277BD;">Building</td>
              <td colspan="3" style="color:#0277BD;">No. of Rooms</td>
              <td colspan="3" style="color:#0277BD;">Land Size</td>
              <td colspan="2"style="color:#0277BD;">BUA</td>
              <td colspan="2"style="color:#0277BD;">Price</td>
              <td colspan="2"style="color:#0277BD;">Price/sf</td>
              </tr>



     <?php /* $n=1;  foreach ($dataProvider2['Soldunselected']->models as $value) {

   $color=''; if ($n%2==0) {  $color='color';  } */?>

               <tr class="<? /*= $color */?>" style=" text-align:center; font-size:10px">
               <td colspan="1"><? /*= $n */?></td>
               <td colspan="3"><? /*= $value['transaction_date'] */?></td>
               <td  colspan="3"><? /*= $value->building->title */?></td>
               <td colspan="3"><? /*= $value['no_of_bedrooms'] */?></td>
               <td colspan="3"><? /*= $value['land_size'] */?></td>
               <td colspan="2"><? /*= $value['built_up_area'] */?></td>
               <td colspan="2"><? /*= $value['listings_price'] */?></td>
               <td colspan="2"><? /*= $value['price_per_sqt'] */?></td>
               </tr>

     <?php /* $n++;  }   */?>


         </table>
-->

     <?php
     if($model->id > 13860 && ($model->parent_id == null || $model->parent_id >  13860)){
     if ($model->client->id == 183 || $model->id >12566 || $model->client->id == 71) {
    $appendices_number = '2';
} else {
    $appendices_number = '6';
         $listing_appendex = 3;
}

$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();
if($model->valuation_approach == 1 || $model->property->valuation_approach == 3) {

    $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
    $stype = 0;
    if ($InspectProperty->no_studios > 0) {
        $stype = 1;
        $listing_appendex++;
?>

        <!--<table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?/*= $appendices_number */?>.03. transactions
                    </h3>
                </td>
            </tr>
        </table>-->
        <?php
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
         //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);



            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }

        ?>


         <br pagebreak="true" />
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

         <tr>
            <td class="table_of_content"  colspan="21">
                <h3><?= $appendices_number ?>.0<?= $listing_appendex ?> Studio Ejari Transaction (Source Reidin)</h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="2" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {

                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="2">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="3" class="tdBorder">+/-</td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>
                 <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_sold->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
    $listing_appendex++;
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?= $appendices_number ?>.0<?= $listing_appendex ?> Studio Market Rents Listings (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_list->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_one_bedrooms > 0) {
        $stype = 2;

        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }
        $listing_appendex++;
        ?>

        <br pagebreak="true" />
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

        <tr>
            <td class="table_of_content" colspan="21">
                <h3><?= $appendices_number ?>.0<?= $listing_appendex ?> One Bedroom Ejari Transaction (Source Reidin)</h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_sold->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
    $listing_appendex++;
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?= $appendices_number ?>.0<?= $listing_appendex ?> One Bedroom Market Rents Listings (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>
                   <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_list->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_two_bedrooms > 0) {
        $stype = 3;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }
        $listing_appendex++;
        ?>


         <br pagebreak="true" />
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

        <tr>
            <td class="table_of_content"  colspan="21">
                <h3> <?= $appendices_number ?>.0<?= $listing_appendex ?> Two Bedrooms Ejari Transaction (Source Reidin) </h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>
                 <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_sold->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
    $listing_appendex++;
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?= $appendices_number ?>.0<?= $listing_appendex ?> Two Bedrooms Market Rents Listings (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_list->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>


        </table>
        <?php
    }


    if ($InspectProperty->no_three_bedrooms > 0) {
        $stype = 4;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }
        $listing_appendex++;
        ?>


         <br pagebreak="true" />
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

        <tr>
            <td class="table_of_content"  colspan="21">
                <h3><?= $appendices_number ?>.0<?= $listing_appendex ?> Three Bedrooms Ejari Transaction (Source Reidin)</h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>
                 <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_sold->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
    $listing_appendex++;
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?= $appendices_number ?>.0<?= $listing_appendex ?> Three Bedrooms Market Rents Listings (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>
                 <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_list->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_four_bedrooms > 0) {
        $stype = 5;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }
        $listing_appendex++;
        ?>


         <br pagebreak="true" />
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

        <tr>
            <td class="table_of_content"  colspan="21">
                <h3><?= $appendices_number ?>.0<?= $listing_appendex ?> Four Bedrooms Ejari Transaction (Source Reidin)</h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_sold->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
    $listing_appendex++;
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?= $appendices_number ?>.0<?= $listing_appendex ?> Four Bedrooms Market Rents Listings(Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>
                 <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_list->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_penthouse > 0) {
        $stype = 6;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }
        $listing_appendex++;
        ?>

         <br pagebreak="true" />
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

        <tr>
            <td class="table_of_content"  colspan="21">
                <h3><?= $appendices_number ?>.0<?= $listing_appendex ?> Penthouse Ejari Transaction (Source Reidin)</h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>
                 <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_sold->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
    $listing_appendex++;
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?= $appendices_number ?>.0<?= $listing_appendex ?> Penthouse Market Rents Listings (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Apartment
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>
                 <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_list->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_of_shops > 0) {
        $stype = 7;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }
        $listing_appendex++;
        ?>


         <br pagebreak="true" />
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

        <tr>
            <td class="table_of_content"  colspan="21">
                <h3><?= $appendices_number ?>.0<?= $listing_appendex ?> Shops Ejari Transaction (Source Reidin)</h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Shop
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>
                  <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_sold->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
    $listing_appendex++;
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?= $appendices_number ?>.0<?= $listing_appendex ?> Shops Market Rents Listings (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Listing Date</td>
                <td colspan="4" style="color:#0277BD;">Building</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Shop
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>
                 <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_list->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_of_offices > 0) {
        $stype = 8;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);


        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();

            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }
        $listing_appendex++;
        ?>


         <br pagebreak="true" />
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

        <tr>
            <td class="table_of_content"  colspan="21">
                <h3><?= $appendices_number ?>.0<?= $listing_appendex ?> Offices Ejari Transaction (Source Reidin)</h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Office
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>
                  <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_sold->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?= $appendices_number ?>.<?= $listing_appendex ?> Market Rents Listings (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Unit Number</td>
                <td colspan="4" style="color:#0277BD;">Type</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                       Office
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>
                  <tr style=" text-align:center; font-size:10px">
                        <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                        </td>
                        <td colspan="3" class="tdBorder"></td>
                        <td colspan="5" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="2" class="tdBorder"></td>
                        <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            <?= number_format($response_list->mv_total_price) ?>
                        </td>
                    </tr>

            <?php } ?>


        </table>
        <?php
    }

    if ($InspectProperty->no_warehouses > 0) {
        $stype = 9;
        $dataProvider = Yii::$app->PdfHelper->getTransactionlistType($model, $stype);
        $dataProvider2 = Yii::$app->PdfHelper->getTransactionlistSoldType($model, $stype);

        $selected_data_list = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'list', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_sold = \yii\helpers\ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $model->id, 'type' => 'sold', 'search_type' => 0, 'latest' => 1, 'income_type' => $stype])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $list = 0;
        $sold = 0;
        if ($selected_data_list <> null && count($selected_data_list) > 0) {

            $list = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsList = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
                ->from('listings_rent')
                ->where(['id' => $selected_data_list])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_15_report?id='.$model->id.'&stype='.$stype]);
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_list = json_decode($val);

        }

        if ($selected_data_sold <> null && count($selected_data_sold) > 0) {

            $sold = 1;
            $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $model->id])->one();
            $totalResultsSold = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) as avg_listings_price_size,AVG(price_per_sqt) as avg_price_per_sqt, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                ->from('sold_transaction_rents')
                ->where(['id' => $selected_data_sold])
                ->one();
            $url =  \yii\helpers\Url::toRoute(['valuation-income/step_12_report?id='.$model->id.'&stype='.$stype]);

            $curl_handle = curl_init();
            //   curl_setopt($curl_handle, CURLOPT_URL, \yii\helpers\Url::toRoute(['valuation-income/step_12_report?=id'.$model->id,'&stype='.$stype]));
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');

            $val = curl_exec($curl_handle);
            curl_close($curl_handle);

            $response_sold = json_decode($val);

            $market = 'Market';
            if ($model->purpose_of_valuation == 3) {
                $market = 'Fair';
            }
        }
        $listing_appendex++;
        ?>


        <br pagebreak="true" />
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

        <tr>
            <td class="table_of_content"  colspan="21">
                <h3><?= $appendices_number ?>.0<?= $listing_appendex ?> Warehouse Ejari Transaction (Source Reidin)</h3>
            </td>
        </tr>
        <tr style="background-color:#ECEFF1; text-align:center;">
            <td colspan="1" style="color:#0277BD;">#</td>
            <td colspan="3" style="color:#0277BD;">Transaction Date</td>
            <td colspan="4" style="color:#0277BD;">Building</td>
            <td colspan="4" style="color:#0277BD;">Property</td>
            <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
            <td colspan="2" style="color:#0277BD;">Land Size</td>
            <td colspan="2" style="color:#0277BD;">BUA</td>
            <td colspan="3" style="color:#0277BD;">Price</td>
            <td colspan="2" style="color:#0277BD;">Price / sf</td>
        </tr>


        <?php if (count($dataProvider2['Soldselected']->models) > 0) { ?>

            <?php $n = 1;
            foreach ($dataProvider2['Soldselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>


                <tr class="<?= $color ?>" style=" text-align:center; vertical-align: bottom; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?=  date('d-M-Y', strtotime($value['transaction_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Warehouse
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php
                $n++;
            }
            ?>
            <tr><td></td></tr>
            </table>
            <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


            <?php if ($sold == 1) { ?>
                <tr style=" text-align:center; font-size:10px;">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y', strtotime($response_sold->avg_date))?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsSold['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_sold->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_sold->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"  style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->sold_adjustments) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_sold->mv_total_price) ?>
                    </td>
                </tr>

            <?php } ?>
            <?php
        } else { ?>
            <tr>
                <td colspan="20" style="text-align:center;">Ejari transactions are not available</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <br><br>
        <table class="col-12">
            <tr>
                <td class="table_of_content">
                    <h3>
                        <?= $appendices_number ?>.<?= $listing_appendex ?> Market Rents Listings (Source Bayut)
                    </h3>
                </td>
            </tr>
        </table>

        <br><br>
        <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <!-- <tr>
                <td class="table_of_content" style="font-size:13px;" colspan="19">
                    <h3>Available for Sale Market Listings</h3>
                </td>
            </tr> -->
            <tr style="background-color:#ECEFF1; text-align:center">
                <td colspan="1" style="color:#0277BD;">#</td>
                <td colspan="3" style="color:#0277BD;">Unit Number</td>
                <td colspan="4" style="color:#0277BD;">Type</td>
                <td colspan="4" style="color:#0277BD;">Property</td>
                <td colspan="2" style="color:#0277BD;">No. of Rooms</td>
                <td colspan="2" style="color:#0277BD;">Land Size</td>
                <td colspan="2" style="color:#0277BD;">BUA</td>
                <td colspan="3" style="color:#0277BD;">Price</td>
                <td colspan="2" style="color:#0277BD;">Price / sf</td>
            </tr>



            <?php $n = 1;
            foreach ($dataProvider['Listselected']->models as $value) {
                $color = '';
                if ($n % 2 == 0) {
                    $color = 'color';
                } ?>

                <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                    <td colspan="1">
                        <?= $n ?>
                    </td>
                    <td colspan="3">
                        <?= date('d-M-Y', strtotime($value['listing_date'])) ?>
                    </td>
                    <td colspan="4">
                        <?= $value->building->title ?>
                    </td>
                    <td colspan="4">
                        Warehouse
                    </td>
                    <td colspan="2">
                        <?= number_format($value['no_of_bedrooms']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['land_size']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['built_up_area']) ?>
                    </td>
                    <td colspan="3">
                        <?= number_format($value['listings_price']) ?>
                    </td>
                    <td colspan="2">
                        <?= number_format($value['price_per_sqt']) ?>
                    </td>
                </tr>

                <?php $n++;
            } ?>
            <tr><td></td></tr>

        </table>
        <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">

            <?php if ($list == 1) { ?>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                    <td colspan="3" class="tdBorder">
                        <?=  date('d-M-Y', strtotime($response_list->avg_date)) ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_room) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($totalResultsList['avg_land_size']) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($response_list->avg_bua) ?>
                    </td>
                    <td colspan="3" class="tdBorder">
                        <?= number_format($response_list->avg_listings_price_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->avg_psf) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Subject Property</td>
                    <td colspan="3" class="tdBorder">
                        <?= date('d-M-Y') ?>
                    </td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->no_of_bedrooms) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($model->land_size) ?>
                    </td>
                    <td colspan="2" class="tdBorder">
                        <?= number_format($inspection_data->built_up_area) ?>
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder" style="border-right:1px solid #666666"></td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Adjustments</td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder">+/-</td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->sold_adjustments) ?>
                    </td>
                </tr>
                <tr style=" text-align:center; font-size:10px">
                    <td colspan="4" class="tdBorder" style="border-left:1px solid #666666; text-align: left;"><?= $market; ?> Rent
                    </td>
                    <td colspan="3" class="tdBorder"></td>
                    <td colspan="5" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="2" class="tdBorder"></td>
                    <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                        <?= number_format($response_list->mv_total_price) ?>
                    </td>
                </tr>

            <?php } ?>


        </table>
        <?php
    }

}
}
 ?>

            <?php
            if($model->id > 13860 && ($model->parent_id == null || $model->parent_id >  13860)) {
                ?>

                <?php
                $previous_records = \app\models\ClientProvidedRents::find()->where(['valuation_id' => $model->id])->orderBy(['income_type' => SORT_ASC])->all();
                if (isset($previous_records) && count($previous_records) > 0) {
                    ?>
                    <br pagebreak="true" />
                    <table cellspacing="1" cellpadding="4" class="main-class col-12">
                        <tr>
                            <td class="detailheading" colspan="2"><h4><?= $appendices_number; ?>.04. Client Provided Rents</h4></td>
                        </tr>

                    </table>
                    <br><br>
                    <table cellspacing="1" cellpadding="5" class="main-class col-12" style="font-size:11px">

                        <!-- <tr>
                            <td class="table_of_content" style="font-size:13px;" colspan="19">
                                <h3>Available for Sale Market Listings</h3>
                            </td>
                        </tr> -->
                        <tr style="background-color:#ECEFF1; text-align:center">
                            <td colspan="2" style="color:#0277BD;">Unit Number</td>
                            <td colspan="2" style="color:#0277BD;">Type</td>
                            <td colspan="4" style="color:#0277BD;">Contract Start Date</td>
                            <td colspan="4" style="color:#0277BD;">Contract End Date</td>
                            <td colspan="2" style="color:#0277BD;">NLA</td>
                            <td colspan="2" style="color:#0277BD;">Rent</td>
                            <td colspan="3" style="color:#0277BD;">Rent/NLA</td>
                            <td colspan="3" style="color:#0277BD;">Occupancy Status</td>
                        </tr>


                        <?php $n = 1;
                        foreach ($previous_records as $value) {
                            $color = '';
                            if ($n % 2 == 0) {
                                $color = 'color';
                            }

                            $nla = $nla + $value->nla;
                            $rent = $rent + $value->rent;
                            $total = $total + $value->rent_sqf;
                            $rec_count = count($previous_records);
                            $nla_avg = round($nla / $rec_count);
                            $rent_avg = round($rent / $rec_count);
                            $total_avg = round($total / $rec_count);


                            ?>

                            <tr class="<?= $color ?>" style=" text-align:center; font-size:10px">
                                <td colspan="2">
                                    <?= $value->unit_number ?>
                                </td>
                                <td colspan="2">
                                    <?= Yii::$app->appHelperFunctions->getIncomeTypesArr()[$value->income_type] ?>
                                </td>
                                <td colspan="4">
                                    <?= date('d-m-Y', strtotime($value->contract_start_date)); ?>
                                </td>
                                <td colspan="4">
                                    <?= date('d-m-Y', strtotime($value->contract_end_date)); ?>
                                </td>
                                <td colspan="2">
                                    <?= Yii::$app->appHelperFunctions->wmFormate($value->nla); ?>
                                </td>
                                <td colspan="2">
                                    <?= Yii::$app->appHelperFunctions->wmFormate($value->rent); ?>
                                </td>
                                <td colspan="3">
                                    <?= Yii::$app->appHelperFunctions->wmFormate($value->rent_sqf); ?>
                                </td>
                                <td colspan="3">
                                    <?= Yii::$app->appHelperFunctions->getCprArr()[$value->status]; ?>
                                </td>
                            </tr>

                            <?php $n++;
                        } ?>
                        <tr>
                            <td></td>
                        </tr>

                    </table>
                    <table cellspacing="0" cellpadding="5" class="main-class col-12" style="font-size:11px">


                        <tr style=" text-align:center; font-size:10px">
                            <td colspan="3" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Total</td>
                            <td colspan="3" class="tdBorder">

                            </td>
                            <td colspan="5" class="tdBorder"></td>
                            <td colspan="2" class="tdBorder">

                            </td>
                            <td colspan="2" class="tdBorder">
                                <?= $nla ?>
                            </td>
                            <td colspan="2" class="tdBorder">
                                <?= $rent ?>
                            </td>
                            <td colspan="3" class="tdBorder">
                                <?= $total ?>
                            </td>
                            <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            </td>
                        </tr>
                        <tr style=" text-align:center; font-size:10px">
                            <td colspan="3" class="tdBorder" style="border-left:1px solid #666666; text-align: left;">Average</td>
                            <td colspan="3" class="tdBorder">

                            </td>
                            <td colspan="5" class="tdBorder"></td>
                            <td colspan="2" class="tdBorder">
                            </td>
                            <td colspan="2" class="tdBorder">
                                <?= $nla_avg ?>
                            </td>
                            <td colspan="2" class="tdBorder">
                                <?= $rent_avg ?>
                            </td>
                            <td colspan="3" class="tdBorder">
                                <?= $total_avg ?>
                            </td>
                            <td colspan="3" class="tdBorder" style="border-right:1px solid #666666">
                            </td>
                        </tr>


                    </table>
                <?php }
            } ?>
