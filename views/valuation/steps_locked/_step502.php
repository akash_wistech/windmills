<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use app\components\widgets\StatusVerified;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Green Green Efficiency');
$cardTitle = Yii::t('app', ' Green Efficiency:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_502/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 502]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <fieldset disabled="disabled">
                            <header class="card-header">
                                <h2 class="card-title">Green Efficiency</h2>
                            </header>
                            <div class="card-body">

                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Green Efficiency Certification</h2>
                                    </header>
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-sm-4" id="green_efficient_certification">
                                                <?php
                                                echo $form->field($model, 'green_efficient_certification')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->greenEfficientCertificationArr,
                                                    // 'options' => ['placeholder' => 'Select a Person ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'disabled' => true
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4" id="certifier_name">
                                                <?php
                                                echo $form->field($model, 'certifier_name')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->certifierNameArr,
                                                    // 'options' => ['placeholder' => 'Select a Person ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'disabled' => true
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4" id="certification_level">
                                                <?php
                                                echo $form->field($model, 'certification_level')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->certificationLevelArr,
                                                    // 'options' => ['placeholder' => 'Select a Person ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'disabled' => true
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4" id="source_of_green_certificate_information">
                                                <?php
                                                echo $form->field($model, 'source_of_green_certificate_information')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->sGCInformationArr,
                                                    // 'options' => ['placeholder' => 'Select a Person ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'disabled' => true
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                        </div>

                                    </div>


                                </section>



                                


                            </div>
                            
                            </fieldset>
                            <?php ActiveForm::end(); ?>
                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

