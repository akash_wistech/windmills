<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ValuationFormAsset;


ValuationFormAsset::register($this);
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */



// $land = array(4, 5, 20, 23, 26, 29, 39, 46, 47, 48, 49, 50, 53);

$villas = Yii::$app->appHelperFunctions->villaIdArr;
$lands = Yii::$app->appHelperFunctions->landIdArr;
$apartments = array(1);
$office = array(28);
$shop = array(17);

$landIds = [];
foreach ($lands as $land) {
    $landIds[] = $land->id;
}

$villaIds = [];
foreach ($villas as $villa) {
    $villaIds[] = $villa->id;
}

$no_show_land_size = array_merge($apartments, $office, $shop);
$no_show_bua_gfa = array_merge($landIds);

// $land_size =  (in_array($model->property_id, $no_show_land_size)) ? "none" : "block";
// $bua_gfa =  (in_array($model->property_id, $no_show_bua_gfa)) ? "none" : "block";




// if (in_array($model->property_id, $landIds)) {
//     $style_land = "none";
// } else {
//     $style_land = "block";
// }


$this->registerJs('


$(document).ready(function () {
    var $building_info = $("#valuationdetail-building_info");
    var $property_id = $("#valuationdetail-property_id");

    function toggleFieldsVisibilityByPropertyType(propertyTypeArray, className) {
        if (propertyTypeArray.includes(parseInt($property_id.val()))) {
            $("." + className).hide();
        } else {
            $("." + className).show();
        }
    }

    var noShowFieldLandSize= ' . json_encode($no_show_land_size) . ';
    var noShowFieldBUAGFA= ' . json_encode($no_show_bua_gfa) . ';
    

    function toggleFunctions(){   

        toggleFieldsVisibilityByPropertyType(noShowFieldLandSize, "land_size");
        toggleFieldsVisibilityByPropertyType(noShowFieldLandSize, "land_size_source");
        toggleFieldsVisibilityByPropertyType(noShowFieldBUAGFA, "built_up_area");
        toggleFieldsVisibilityByPropertyType(noShowFieldBUAGFA, "built_up_area_source");
    }


    $building_info.on("change", function () {
        toggleFunctions();
    });

    $property_id.on("change", function () {
        toggleFunctions();
    });

    if($building_info.val() != ""){ 
        toggleFunctions();
    }

}); 
    

');
?>


<style>
    .datepicker-days .table>thead>tr>th,
    .table>tbody>tr>th,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>tbody>tr>td,
    .table>tfoot>tr>td {
        padding: inherit !important;
    }

    .fade {
        opacity: 1;
    }

    .nav-tabs.flex-column .nav-link.active {
        background-color: #007bff;
        color: white;
        font-weight: bold;
    }

    .help-block2 {
        color: red;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -14px;
        font-family: inherit;
    }
</style>

<section class="valuation-form card card-outline card-primary">

    <?php $form = ActiveForm::begin(); ?>
    <fieldset disabled="disabled">
        <header class="card-header">
            <h2 class="card-title">

            </h2>
        </header>
        <div class="card-body">

            <section class="card card-outline card-info">
                <header class="card-header">
                    <h2 class="card-title">
                        <?= Yii::t('app', 'Address Details') ?>
                    </h2>
                </header>
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'building_info')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                                    'title' => SORT_ASC,
                                ])->all(), 'id', 'title'),
                                'options' => ['placeholder' => 'Select a Building ...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ])->label('Building/Project Name');
                            ?>

                        </div>
                        <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>">
                            <?= $form->field($model, 'building_number')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'plot_number')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
                        </div>


                        <div class="col-sm-4">
                            <?= $form->field($model, 'community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'sub_community')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'country')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                        </div>
                        <div class="col-sm-4 land_off_fields unit_number" style="display: <?= $style_land ?>">
                            <?= $form->field($model, 'unit_number')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-4 land_off_fields floor_number" style="display: <?= $style_land ?>">
                            <?= $form->field($model, 'floor_number')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'location_pin')->textInput(['maxlength' => true])->label('Location Pin <span class="text-danger">*</span>') ?>
                        </div>
                    </div>
                </div>
            </section>
            <div class="clearfix"></div>

            <section class="card card-outline card-info">
                <header class="card-header">
                    <h2 class="card-title">
                        <?= Yii::t('app', 'Property Details') ?>
                    </h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy([
                                    'title' => SORT_ASC,
                                ])->all(), 'id', 'title'),
                                'options' => ['placeholder' => 'Select a Property Type ...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ])->label('Property Type');
                            ?>

                        </div>
                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ]);
                            ?>

                        </div>

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'tenure')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->buildingTenureArr,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ]);
                            ?>

                        </div>

                        <!-- <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'property_purpose')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\PropertyPurposes::find()->where(['status' => 1])->orderBy([
                                'title' => SORT_ASC,
                            ])->all(), 'id', 'title'),
                            'pluginOptions' => [
                                'allowClear' => true,
                                'disabled' => true
                            ],
                        ]);
                        ?>

                    </div> -->


                        <!-- <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>">
                        <?= $form->field($model, 'no_of_towers')->textInput(['type' => 'number', 'maxlength' => true])->label('Number of Buildings/Units on Land') ?>
                    </div>
                    <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>">
                        <?= $form->field($model, 'bedrooms')->textInput(['maxlength' => true]) ?>
                    </div> -->




                        <!-- <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>">
                        <?= $form->field($model, 'parking_floors')->textInput(['maxlength' => true])->label("Parking Space") ?>
                    </div> -->


                        <!-- <div class="col-sm-4 land_off_fields" style="display: <?= $style_land ?>">
                        <?php
                        echo $form->field($model, 'parking_source')->widget(Select2::classname(), [
                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrParkingSource,
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Source of Number of Parkings');
                        ?>

                    </div> -->
                        <!-- <?php if (isset($model->building->city) && ($model->building->city == 3507)) { ?>
                        <div class="col-sm-4">
                            <?= $form->field($model, 'title_deed')->textInput(['maxlength' => true])->label('Title Deed Certificate Number') ?>
                        </div>
                    <?php } ?> -->
                    </div>

                </div>
            </section>

            <section class="card card-outline card-info">
                <header class="card-header">
                    <h2 class="card-title">
                        <?= Yii::t('app', 'Size Details') ?>
                    </h2>
                </header>
                <div class="card-body">
                    <div class="row">



                        <div class="col-sm-4 land_size">
                            <?= $form->field($model, 'land_size')->textInput(['maxlength' => true])->label('Land Size <span class="text-danger">*</span>'); ?>
                            <div class="help-block2"></div>
                        </div>
                        <div class="col-sm-4 land_off_fields land_size_source">
                            <?php
                            echo $form->field($model, 'plot_source')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrPlotSource,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ])->label(' Source Document of Land Size');
                            ?>

                        </div>
                        <div class="col-sm-4 land_off_fields built_up_area">
                            <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true])->label('BUA / GFA <span class="text-danger">*</span>') ?>
                            <div class="help-block2"></div>
                        </div>
                        <div class="col-sm-4 land_off_fields built_up_area_source">
                            <?php
                            echo $form->field($model, 'bua_source')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrBuaSource,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ])->label(' Source Document of BUA/GFA ');
                            ?>

                        </div>

                        <div class="col-sm-4 land_off_fields built_up_area_source">
                            <?php
                            echo $form->field($model, 'nla_source')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrNlaSource,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ])->label(' Source Document of NLA ');
                            ?>

                        </div>






                    </div>

                </div>
            </section>



        </div>

    </fieldset>
    <?php ActiveForm::end(); ?>
</section>