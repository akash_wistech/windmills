<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use  app\components\widgets\StatusVerified;

ListingsFormAsset::register($this);
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */

if ($model->email_status !=1) {
    $AlertText='Data and Valuation Status will be saved and Email will be sent once?';
}
else{
    $AlertText='Only Data will be saved?';
}

$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date,#listingstransactions-client_requirement_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

 $("#listingstransactions-inspection_time").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "HH:mm"
    });


$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "'.Yii::t('app',$AlertText).'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
      console.log("Hello 123")
      $("#w0").unbind("submit").submit();
    }
});
});


var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});

$("body").on("click", ".sav-btn", function (e) {
swal({
title: "'.Yii::t('app','Valuation status will be saved and Email will be sent to Clients ?').'",
html: "'.Yii::t('app','').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({

  url: "'.Url::to(['valuation/receive-valuation-email','id'=>$model->id,'step'=>1]).'",
  dataType: "html",
  type: "POST",
  });
    }
});
});




    $("body").on("change", ".client-cls", function () {
           console.log($(this).val());
           if($(this).val() == 1){
            $("#client_fixed_fee").show();
                   }else{
                    $("#client_fixed_fee").hide();
                   }
        $(".appentWali-row").remove();
        _this = $(this);
        GetOtherInstructingPersons(_this);
    });


    var other_instructing_person_id =  "'.$model->other_instructing_person.'";

    function GetOtherInstructingPersons(_this){
        var keyword = "Valuation";
        var client_id = _this.val();      
        if (client_id!= "") {
            var data = {client_id:client_id, other_instructing_person_id:other_instructing_person_id, keyword:keyword};
            $.ajax({
                url: "'.Url::to(['client/other-instructing-persons']).'",
                data: data,
                method: "post",
                dataType: "html",
                success: function(data) {
//                    console.log(data);
                    $(".parent-row-ff").html(data);
                     var element = $("#other-instructing-person");
                     var option = $(\'option:selected\', element).attr(\'data_phone\');
                     $("#valuation-mobile_instructing_person").val(option);
                    
                     $("#other-instructing-person").on("change",function(){
                      var element = $("#other-instructing-person");
                     var option = $(\'option:selected\',element).attr(\'data_phone\');
                     $("#valuation-mobile_instructing_person").val(option);
                    
                    });
                   console.log(option);
                },
                error: bbAlert
            });
        }
        else{
            $(".appentWali-row").remove();
        }
         if (client_id!= "") {
         var data = {client_id:client_id};
            $.ajax({
                url: "'.Url::to(['client/clientinvoice']).'",
                data: data,
                method: "post",
                dataType: "html",
                success: function(response) {
//                    console.log(response);
                   if(response == "1"){
                   $("#client_invoice_type").show();
                   }else{
                    $("#client_invoice_type").hide();
                   }
                },
                error: bbAlert
            });
        }
        else{
            $(".appentWali-row").remove();
        }

    }
   
    $(".client-cls").trigger("change");



');

?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css"
      integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg=="
      crossorigin="anonymous" referrerpolicy="no-referrer"/>
<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }
    .help-block2 {
        color: #a94442;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -6px;
        font-family: inherit;
    }
</style>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }

    .fade {
        opacity: 1;
    }

    .nav-tabs.flex-column .nav-link.active {
        background-color: #007bff;
        color: white;
        font-weight: bold;
    }
    .number_field_width{
        margin-right: 85px;
    }
</style>

<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }
    .help-block2 {
        color: #a94442;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -6px;
        font-family: inherit;
    }
</style>
<section class="valuation-form card card-outline card-primary ">

    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title">Client Details</h2>
        <div class="card-tools">
            <?php  // if(in_array( Yii::$app->user->identity->id, [1,33])) { ?>
                <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('step1_edit')) { ?>
                    <a href="<?= Url::to(['valuation/invoice_toe','id'=>$model['id']])?>" target="_blank" class="btn btn-success">
                        Tax Invoice
                    </a>
                <?php //} ?>
            <?php } ?>

            <?php  if(isset($model->building->city) && ($model->building->city == 3507)){ ?>
                <a href="https://maxima.windmillsgroup.com/client/export-ajman-download?id=<?= $model->id ?>" style="margin-left: 10px;" class="btn btn-info"> Verify Excel report</a>
            <?php } ?>



        </div>
    </header>
    <fieldset disabled="disabled">
    <div class="card-body">


        <div class="row ">
            <div class="col-sm-4">
                <?= $form->field($model, 'reference_number')->textInput(['maxlength' => true, 'value' => ($model->reference_number <> null) ? $model->reference_number : Yii::$app->appHelperFunctions->uniqueReference]) ?>
            </div>
            <div class="col-sm-4">
                <?php
                if(isset($model->id) && $model->id <> null){

                }else{
                    $tomorrow = date("Y-m-d", time() + 86400);
                    $model->instruction_date = date('Y-m-d');
                    $model->target_date = $tomorrow;
                    $model->client_requirement_date = $tomorrow;
                }

                ?>
                <?= $form->field($model, 'instruction_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-instruction_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-instruction_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4" id="inspection_time_id">
                <?= $form->field($model, 'instruction_time', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-inspection_time" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-inspection_time" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'client_reference')->textInput(['maxlength' => true]) ?>
            </div>
            <?php


            ?>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'client_id')->widget(Select2::classname(), [
                    'data' => $existing_client_array,

                    'options' => ['placeholder' => 'Select a Client ...', 'class'=> 'client-cls'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'disabled' => true
                    ],
                ])->label('Client Name');
                ?>

            </div>


            <div class="col-sm-4 parent-row-ff">

                <div class="form-group field-valuation-mobile_instructing_person">
                    <label class="control-label" for="valuation-mobile_instructing_person">Instructing Person<span class="text-danger">*</span></label>
                    <input type="text" readonly id="valuation-mobile_instructing_person" class="form-control">

                    <div class="help-block"></div>
                </div>

            </div>





            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'inspection_type')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->inspectionTypeArr,
                    'options' => ['placeholder' => 'Select a Inspection Type'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'disabled' => true
                    ],
                ]);
                ?>
            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'urgency')->widget(Select2::classname(), [
                    'data' => array('0'=>'Normal','1'=>'Urgent'),
                    'pluginOptions' => [
                        'allowClear' => true,
                        'disabled' => true
                    ],
                ])->label('Urgency');
                ?>
            </div>


            <div class="col-sm-4">
                <?= $form->field($model, 'client_requirement_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-client_requirement_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-client_requirement_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'target_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-target_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-target_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])->label('Windmills Target Date') ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'email_subject')->textInput(['required'=>true]);
                ?>

            </div>





        </div>
        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Client\'s Customer Details') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'prefix_customer_name')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->smHelper->getTitleArr(),
                            'options' => ['placeholder' => 'Select...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'initialize' => true,
                                'disabled' => true
                            ],
                        ])->label('Client\'s Customer - Prefix');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_name_passport')->textInput(['maxlength' => true])->label('Client\'s Customer - First Name') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_lastname_passport')->textInput(['maxlength' => true])->label('Client\'s Customer  - Last Name') ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Contact Person Details') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4" id="contact_person_prefix_id">
                        <?php
                        echo $form->field($model, 'prefix')->widget(Select2::classname(), [
                            'data' => array('Mr.' => 'Mr.', 'Miss' => 'Miss'),
                            'pluginOptions' => [
                                'allowClear' => true,
                                'disabled' => true
                            ],
                        ])
                        ?>
                    </div>
                    <div class="col-sm-4 only-char">
                        <?= $form->field($model, 'contact_person_name')->textInput(['maxlength' => true])->label('Contact Person First Name') ?>
                    </div>
                    <div class="col-sm-4 only-char" >
                        <?= $form->field($model, 'contact_person_lastname')->textInput(['maxlength' => true])->label('Contact Person Last Name') ?>
                    </div>
                    <div class="col-sm-4" id="contact_email_id">
                        <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>
                    </div>




                    <div class="col-sm-4" >
                        <label for="phone">Phone number </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <select name='Valuation[phone_code]' id="country-code" class="form-control">
                                    <option value="050" <?php if(isset($model->phone_code) && ($model->phone_code == '050')){ echo 'selected';} ?>>050</option>
                                    <option value="052" <?php if(isset($model->phone_code) && ($model->phone_code == '052')){ echo 'selected';} ?>>052</option>
                                    <option value="054" <?php if(isset($model->phone_code) && ($model->phone_code == '054')){ echo 'selected';} ?>>054</option>
                                    <option value="055" <?php if(isset($model->phone_code) && ($model->phone_code == '055')){ echo 'selected';} ?>>055</option>
                                    <option value="056" <?php if(isset($model->phone_code) && ($model->phone_code == '056')){ echo 'selected';} ?>>056</option>
                                    <option value="058" <?php if(isset($model->phone_code) && ($model->phone_code == '058')){ echo 'selected';} ?>>058</option>
                                </select>
                            </div>
                            <?= $form->field($model, 'contact_phone_no')->textInput(['maxlength' => true,'class' => 'form-control number_field_width','placeholder' => 'Enter phone number'])->label(false) ?>
                        </div>
                    </div>

                    <div class="col-sm-4" >
                        <label for="phone">Land Line number </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <select name="Valuation[land_line_code]" id="country-code" class="form-control">
                                    <option value="02" <?php if(isset($model->land_line_code) && ($model->land_line_code == '02')){ echo 'selected';} ?>>02</option>
                                    <option value="04" <?php if(isset($model->land_line_code) && ($model->land_line_code == '04')){ echo 'selected';} ?>>04</option>
                                    <option value="03" <?php if(isset($model->land_line_code) && ($model->land_line_code == '03')){ echo 'selected';} ?>>03</option>
                                    <option value="06" <?php if(isset($model->land_line_code) && ($model->land_line_code == '06')){ echo 'selected';} ?>>06</option>
                                    <option value="07" <?php if(isset($model->land_line_code) && ($model->land_line_code == '07')){ echo 'selected';} ?>>07</option>
                                    <option value="09" <?php if(isset($model->land_line_code) && ($model->land_line_code == '09')){ echo 'selected';} ?>>09</option>
                                </select>
                            </div>
                            <?= $form->field($model, 'land_line_no')->textInput(['maxlength' => true,'class' => 'form-control number_field_width','placeholder' => 'Enter phone number'])->label(false) ?>
                        </div>
                    </div>

                    <?php if(($model->parent_id <> null) && $model->parent_id > 0){ ?>

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'revised_reason')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->revisedReasons,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'disabled' => true
                                ],
                            ])->label('Reason for Revision');
                            ?>

                        </div>
                    <?php } ?>
                    <?php
                    if($model->client->client_invoice_customer == 1) {
                        $style = "block";
                    }else{
                        $style = "none";
                    }

                    if($model->client->id == 1) {
                        $style_fee = "block";
                    }else{
                        $style_fee = "none";
                    }


                    ?>
                    <div class="col-sm-4" id="client_invoice_type" style="display: <?= $style ?>">
                        <?php
                        echo $form->field($model, 'client_invoice_type')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'Client Name', '1' => 'Customer Name'),

                            'pluginOptions' => [
                                'allowClear' => true,
                                'disabled' => true
                            ],
                        ])->label('Client Invoice Name');
                        ?>
                    </div>

                    <div class="col-sm-4" id="client_fixed_fee" style="display: <?= $style_fee ?>">
                        <?php


                        echo $form->field($model, 'client_fixed_fee_check')->widget(Select2::classname(), [
                            'data' => array('1' => 'Normal', '2' => 'Shaikh Zayed case fixed fee'),
                            'options' => ['placeholder' => 'Select a Placement ...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'disabled' => true
                            ],
                        ])->label('Fee Selection');
                        ?>
                    </div>

                </div>
                <?php
                $model->step=1;
                ?>

                <?php echo $form->field($model, 'step')->textInput(['maxlength' => true,

                    'type'=>'hidden'])->label('') ?>
        </section>





    </div>
    </fieldset>

    <?php ActiveForm::end(); ?>
</section>
<?php
$allow_array = array(1,142,92,110465,111647,111729);
if (in_array(Yii::$app->user->identity->id, $allow_array) || Yii::$app->user->identity->id == 1) {
?>

<section class="valuation-form card card-outline card-primary">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card-body">

        <div class="col-sm-4">
            <?= $form->field($model, 'taqyeem_number')->textInput(['maxlength' => true])->label('Taqyimee Number') ?>
        </div>
        <div class="col-sm-4">
            <?php
            echo $form->field($model, 'ajman_approved')->widget(Select2::classname(), [
                'data' => array(0 => 'Pending From Ajman Rera', 1 => 'Approved By Ajman Rera',2=>'Pending From Windmills'),
               // 'disabled' => true
            ]);
            ?>
        </div>
        <div class="col-sm-4 upload-docs">
            <div class="form-group">
                <a href="javascript:;"
                   id="upload-document0"
                   onclick="uploadAttachment(0)"
                   data-toggle="tooltip"
                   class="img-thumbnail"
                   title="Upload Document">
                    <?php
                    $attachment =$model->ajman_approved_image;

                    if ($attachment <> null) {

                        $attachment_link= $attachment;
                        $explode_attach_doc = explode('received-info/',$attachment);

                        if(isset($explode_attach_doc[1])) {
                            $attachment_src = 'https://maxima.windmillsgroup.com/ajmanfiles/'.$explode_attach_doc[1];
                        }else{
                            $attachment_src = Yii::$app->params['uploadDocsIcon'];
                        }
                        $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/',$attachment);
                        if(isset($explode_attach[1])) {
                            $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                        }else {
                            $explode_attach = explode('https://newcliudfrontaclmax.s3.ap-southeast-1.amazonaws.com/', $attachment);
                            if (isset($explode_attach[1])) {


                                $attachment_link = Yii::$app->get('s3bucketwe')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');

                                /*  echo $explode_attach[1];
                                  die;*/

                            } else {
                                $explode_attach = explode('https://maxima-media.s3.eu-central-1.amazonaws.com/', $attachment);
                                $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                            }
                        }



                        if (strpos($attachment, '.pdf') !== false) {
                            ?>
                            <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                 id="deleted-<?= $model->id ?>"
                                 alt="" title=""
                                 data-placeholder="no_image.png"/>
                            <a href="<?= $attachment_link; ?>"
                               target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>


                            <?php

                        } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                            ?>
                            <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                 id="deleted-<?= $model->id ?>"
                                 alt="" title=""
                                 data-placeholder="no_image.png"/>
                            <a href="<?= $attachment_link; ?>"
                               target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>

                            <?php

                        } else {
                            ?>
                            <img src="<?php echo $attachment_src; ?>"
                                 id="deleted-<?= $model->id ?>"
                                 alt="" title=""
                                 data-placeholder="no_image.png"/>
                            <a href="<?= $attachment_link; ?>"
                               target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>

                            <?php
                        }
                    } else {
                        ?>
                        <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                             alt="" title=""
                             data-placeholder="no_image.png"/>
                        <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                           target="_blank">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                        <?php
                    }

                    ?>
                </a>
                <input type="hidden"
                       class="removed-<?= $attachment_Details->id; ?> mandatory-doc"
                       name="Valuation[ajman_approved_image]"
                       value="<?= $attachment; ?>"
                       id="input-attachment0"/>

            </div>

        </div>

        <div class="row">
            <div class="col-sm-4 upload-docs">
                <label class="control-label" for="valuation-taqyeem_certificate">Taqyimee Certificate</label>
                <div class="form-group">
                    <a href="javascript:;"
                       id="upload-document1"
                       onclick="uploadAttachmentFile(1)"
                       data-toggle="tooltip"
                       class="img-thumbnail"
                       title="Upload Document">
                        <?php
                        $attachment = $model->taqyeem_certificate;

                        if ($attachment <> null) {

                            $attachment_link= $attachment;
                            $explode_attach_doc = explode('received-info/',$attachment);

                            if(isset($explode_attach_doc[1])) {
                                $attachment_src = 'https://maxima.windmillsgroup.com/cache/'.$explode_attach_doc[1];
                            }else{
                                $attachment_src = Yii::$app->params['uploadDocsIcon'];
                            }
                            $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/',$attachment);
                            if(isset($explode_attach[1])) {
                                $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                            }else {
                                $explode_attach = explode('https://newcliudfrontaclmax.s3.ap-southeast-1.amazonaws.com/', $attachment);
                                if (isset($explode_attach[1])) {


                                    $attachment_link = Yii::$app->get('s3bucketwe')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');

                                    /*  echo $explode_attach[1];
                                    die;*/

                                } else {
                                    $explode_attach = explode('https://maxima-media.s3.eu-central-1.amazonaws.com/', $attachment);
                                    $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                }
                            }



                            if (strpos($attachment, '.pdf') !== false) {
                                ?>
                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                     id="deleted-<?= $model->id ?>"
                                     alt="" title=""
                                     data-placeholder="no_image.png"/>
                                <a href="<?= $attachment_link; ?>"
                                   target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>


                                <?php

                            } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                ?>
                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                     id="deleted-<?= $model->id ?>"
                                     alt="" title=""
                                     data-placeholder="no_image.png"/>
                                <a href="<?= $attachment_link; ?>"
                                   target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>

                                <?php

                            } else {
                                ?>
                                <img src="<?php echo $attachment_src; ?>"
                                     id="deleted-<?= $model->id ?>"
                                     alt="" title=""
                                     data-placeholder="no_image.png"/>
                                <a href="<?= $attachment_link; ?>"
                                   target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>

                                <?php
                            }
                        } else {
                            ?>
                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                 alt="" title=""
                                 data-placeholder="no_image.png"/>
                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                               target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                            <?php
                        }

                        ?>
                    </a>
                    <input type="hidden"
                           class="removed-<?= $attachment_Details->id; ?> mandatory-doc"
                           name="Valuation[taqyeem_certificate]"
                           value="<?= $attachment; ?>"
                           id="input-attachment1"/>

                </div>

            </div>
            <div class="col-sm-4 upload-docs">
                <label class="control-label" for="Valuation-payment_slip">Payment Slip</label>
                <div class="form-group">
                    <a href="javascript:;"
                       id="upload-document2"
                       onclick="uploadAttachmentFile(2)"
                       data-toggle="tooltip"
                       class="img-thumbnail"
                       title="Upload Document">
                        <?php
                        $attachment =$model->payment_slip;

                        if ($attachment <> null) {

                            $attachment_link= $attachment;
                            $explode_attach_doc = explode('received-info/',$attachment);

                            if(isset($explode_attach_doc[1])) {
                                $attachment_src = 'https://maxima.windmillsgroup.com/cache/'.$explode_attach_doc[1];
                            }else{
                                $attachment_src = Yii::$app->params['uploadDocsIcon'];
                            }
                            $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/',$attachment);
                            if(isset($explode_attach[1])) {
                                $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                            }else {
                                $explode_attach = explode('https://newcliudfrontaclmax.s3.ap-southeast-1.amazonaws.com/', $attachment);
                                if (isset($explode_attach[1])) {


                                    $attachment_link = Yii::$app->get('s3bucketwe')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');

                                    /*  echo $explode_attach[1];
                                    die;*/

                                } else {
                                    $explode_attach = explode('https://maxima-media.s3.eu-central-1.amazonaws.com/', $attachment);
                                    $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                }
                            }



                            if (strpos($attachment, '.pdf') !== false) {
                                ?>
                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                     id="deleted-<?= $model->id ?>"
                                     alt="" title=""
                                     data-placeholder="no_image.png"/>
                                <a href="<?= $attachment_link; ?>"
                                   target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>


                                <?php

                            } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                ?>
                                <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                     id="deleted-<?= $model->id ?>"
                                     alt="" title=""
                                     data-placeholder="no_image.png"/>
                                <a href="<?= $attachment_link; ?>"
                                   target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>

                                <?php

                            } else {
                                ?>
                                <img src="<?php echo $attachment_src; ?>"
                                     id="deleted-<?= $model->id ?>"
                                     alt="" title=""
                                     data-placeholder="no_image.png"/>
                                <a href="<?= $attachment_link; ?>"
                                   target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>

                                <?php
                            }
                        } else {
                            ?>
                            <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                 alt="" title=""
                                 data-placeholder="no_image.png"/>
                            <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                               target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                            <?php
                        }

                        ?>
                    </a>
                    <input type="hidden"
                           class="removed-<?= $attachment_Details->id; ?> mandatory-doc"
                           name="Valuation[payment_slip]"
                           value="<?= $attachment; ?>"
                           id="input-attachment2"/>

                </div>

            </div>

        </div>

    </div>


    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn2']) ?>

    </div>
    <?php ActiveForm::end(); ?>

</section>
<?php } ?>

<script type="text/javascript">
    var unit_row = 0;


    var uploadAttachment = function (attachmentId) {


        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= \yii\helpers\Url::to(['/file-manager/uploadaj', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function (json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);


                            console.log(json.file.href);
                            var str1 = json['file'].href;
                            var str2 = ".pdf";
                            var str3 = ".doc";
                            var str4 = ".docx";
                            var str5 = ".xlsx";
                            var str6 = ".xls";
                            if (str1.indexOf(str2) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                            } else if (str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                            } else {
                                $('#upload-document' + attachmentId + ' img').prop('src', json['file'].cache_path);
                            }
                            $('#input-attachment' + attachmentId).val(json['file'].href);


                            /*  $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                             $('#input-attachment' + attachmentId).val(json['file'].href);*/
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }
    var uploadAttachmentFile = function(attachmentId) {

        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= \yii\helpers\Url::to(['/file-manager/uploadfile', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function() {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function(json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);


                            console.log(json.file.href);
                            var str1 = json['file'].href;
                            var str2 = ".pdf";
                            var str3 = ".doc";
                            var str4 = ".docx";
                            var str5 = ".xlsx";
                            var str6 = ".xls";
                            if (str1.indexOf(str2) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                            } else if (str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                            } else {
                                $('#upload-document' + attachmentId + ' img').prop('src', json['file'].cache_path);
                            }
                            $('#input-attachment' + attachmentId).val(json['file'].href);


                            /*  $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                             $('#input-attachment' + attachmentId).val(json['file'].href);*/
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }

</script>

