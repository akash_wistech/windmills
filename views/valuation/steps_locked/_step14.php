<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'List List-Transactions');
$cardTitle = Yii::t('app', 'List List-Transactions:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_11/'.$valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<style>
    .red{
        background-color: red !important;
    }
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <fieldset disabled="disabled">
    <div class="card-body">
        <div class="row">
            <?php   if ($readonly<>1){  ?>

                <div class="col-5 col-sm-3">
                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                        <?php  echo $this->render('../left-nav', ['model' => $valuation,'step' => 14]); ?>
                    </div>
                </div>
            <?php } ?>

            <?php if ($readonly==1){  $class='"col-12 col-sm-12"'; } ?>

            <?php if ($readonly<>1){  $class='"col-7 col-sm-9"'; } ?>
            <div class=<?= $class ?>>
                <div class="tab-content" id="vert-tabs-tabContent">
                    <section class="valuation-form card card-outline card-primary">


                        <header class="card-header">
                            <h2 class="card-title">Selected Enquiry</h2>
                        </header>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">

                                    <label>Buildings: </label> <?= $selected_records_display->building_info ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Date From: </label> <?= Yii::$app->formatter->asDate($selected_records_display->date_from) ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Date To: </label> <?= Yii::$app->formatter->asDate($selected_records_display->date_to) ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Property Listing: </label> <?= $selected_records_display->property_id ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Property Category: </label> <?= $selected_records_display->property_category ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Bedroom From: </label> <?=$selected_records_display->bedroom_from ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Bedroom To: </label> <?= $selected_records_display->bedroom_to ?>
                                </div>
                            </div>


                        </div>
                        <div class="card-footer">
                        </div>

                    </section>

                    <div class="listings-transactions-index">

                        <?php
                        $visibility_of_last_checkbox=1;

                        if ($readonly==1) {
                            $visibility_of_last_checkbox=0;
                        }


                        $form = ActiveForm::begin(); ?>

                        <?= CustomGridView::widget([
                            'dataProvider' => $dataProvider,
                            'selected_data' => $selected_data,
                           // 'selected_data_auto' => $selected_data_auto,
                            //  'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],

                               /* ['attribute' => 'source',
                                    'label' => Yii::t('app', 'Source'),
                                    'value' => function ($model) {

                                        return ($model->source <> null)? Yii::$app->appHelperFunctions->propertySourceListArr[$model->source]: '';
                                    },
                                ],*/


                                ['attribute' => 'listing_date',
                                    'label' => Yii::t('app', 'Listing Date'),
                                    'value' => function ($model) {
                                        $transDate=$model->listing_date;
                                        if($transDate!='' && $transDate!=null)$transDate=Yii::$app->formatter->asDate($transDate);
                                        return $transDate;
                                    },
                                ],
                                ['attribute' => 'property_exposure',
                                    'label' => Yii::t('app', 'Exposure'),
                                    'value' => function ($model) {

                                        return ($model->property_exposure <> null)? Yii::$app->appHelperFunctions->propertyExposureListArr[$model->property_exposure]: '';
                                    },
                                ],
                                ['attribute' => 'property_placement',
                                    'label' => Yii::t('app', 'Placement,'),
                                    'value' => function ($model) {

                                        return ($model->property_placement <> null)? Yii::$app->appHelperFunctions->propertyPlacementListArr[$model->property_placement]: '';
                                    },
                                ],
                                ['attribute' => 'full_building_floors',
                                    'label' => Yii::t('app', 'Floor Number,'),
                                    'value' => function ($model) {

                                        return ($model->floor_number <> null)? $model->floor_number: 0;
                                    },
                                ],
                                ['attribute' => 'upgrades',
                                    'label' => Yii::t('app', 'Upgrades,'),
                                    'value' => function ($model) {

                                        return ($model->upgrades <> null)? $model->upgrades: '';
                                    },
                                ],
                                /*['attribute' => 'listing_date',
                                    'label' => Yii::t('app', 'Transaction Date'),
                                    'value' => function ($model) {
                                        $transDate=$model->listing_date;
                                        if($transDate!='' && $transDate!=null)$transDate=Yii::$app->formatter->asDate($transDate);
                                        return $transDate;
                                    },
                                ],*/
                                ['attribute' => 'building_info',
                                    'label' => Yii::t('app', 'Building'),
                                    'value' => function ($model) {
                                        return $model->building->title;
                                    }
                                ],
                                ['attribute' => 'no_of_bedrooms', 'label' => Yii::t('app', 'No. of Rooms')],
                                ['attribute' => 'land_size', 'label' => Yii::t('app', 'Land Size')],
                                ['attribute' => 'built_up_area', 'label' => Yii::t('app', 'BUA')],
                                ['attribute' => 'listings_price', 'label' => Yii::t('app', 'Price')],
                                ['attribute' => 'price_per_sqt', 'label' => Yii::t('app', 'Price / sf')],
                                [ 'attribute' => 'id', 'label' => Yii::t('app', 'View'), 'format' => 'raw', 'value' => function ($model) { return Html::a("<i class=\"fa fa-fw fa-edit\"></i>", [ 'listings-transactions/update', 'id' => $model->id ], ['target' => '_blank']); },
                                    'visible' =>  $visibility_of_last_checkbox ? true : false],
                                ['attribute' => 'changed',
                                    'label' => Yii::t('app', 'Changed?'),
                                    'value' => function ($model) {
                                        if($model->changed == 0){
                                            return 'No';
                                        }else{
                                            return 'Yes';
                                        }
                                    }
                                ],
                                ['attribute' => 'status',
                                    'label' => Yii::t('app', 'Verified?'),
                                    'value' => function ($model) {
                                        if($model->status == 1){
                                            return 'Yes';
                                        }else{
                                            return 'No';
                                        }
                                    }
                                ],
                                [
                                    'class' => 'yii\grid\CheckboxColumn',
                                    'checkboxOptions' => function($model) use ($selected_data,$selected_data_auto) {

                                        if($model->id >0) {

                                            /*   echo "<pre>";
                                               print_r($selected_data_auto);
                                               die;*/
                                            $checked = false;
                                            $class = 'simple';


                                            if (in_array($model->id, $selected_data)) {
                                                $checked = true;
                                            }
                                            if(in_array($model->id, $selected_data) && !in_array($model->id, $selected_data_auto)) {
                                                $class = '';
                                            }
                                            if(!in_array($model->id, $selected_data) && in_array($model->id, $selected_data_auto)) {
                                                $class = '';
                                            }

                                            return ['checked' => $checked, 'class' => $class];
                                        }

                                        // return ['checked' => true];
                                        /* return ['value' => $model->id];*/
                                    },
                                    'visible' =>  $visibility_of_last_checkbox ? true : false,

                                    // 'cssClass'=> (in_array($model->id, $selected_data_auto, TRUE)) ? 'gg':'',

                                ],
                                ['attribute' => 'building_info',
                                    'label' => Yii::t('app', 'Status'),
                                    'format' => 'raw',
                                    'value' => function($model) use ($selected_data,$selected_data_r,$selected_data_auto) {

                                        if($model->id >0) {

                                            if(in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10004;, R&#10004;, A&#10004;';
                                            }
                                            if(!in_array($model->id, $selected_data) && !in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return'M&#10060;, R&#10060, A&#10060; ';
                                            }

                                            if(in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10060;, R&#10004;, A&#10004; ';
                                            }

                                            if(in_array($model->id, $selected_data) && !in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10060;, R&#10060;, A&#10004; ';
                                            }

                                            if(in_array($model->id, $selected_data) && !in_array($model->id, $selected_data_r) && in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10004;, R&#10060;, A&#10004; ';
                                            }
                                            if(!in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10060;, R&#10004;, A&#10060; ';
                                            }

                                            if(!in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10004;, R&#10004;, A&#10060; ';
                                            }

                                            if(in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10004;, R&#10004;, A&#10060; ';
                                            }
                                            if(!in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10060;, R&#10004;, A&#10060; ';
                                            }
                                            if(!in_array($model->id, $selected_data_r) && in_array($model->id, $selected_data_auto) & !in_array($model->id, $selected_data)) {
                                                return ' M&#10004;, R&#10060;, A&#10060; ';
                                            }
                                            return $model->id;


                                            // return ['checked' => $checked, 'class' => $class];
                                        }

                                        // return ['checked' => true];
                                        /* return ['value' => $model->id];*/
                                    },
                                ],
                            ],
                        ]); ?>
                        <?php if ($readonly<>1){  ?>

                        <?php } ?>
                        <?php ActiveForm::end(); ?>


                        <?php if(!empty($select_calculations)){ ?>
                            <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title"> Calculations </h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Average Land:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->avg_land_size <> null) ? $select_calculations->avg_land_size: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Average BUA:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->built_up_area_size <> null) ? $select_calculations->built_up_area_size: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Average Market Value:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->avg_listings_price_size <> null) ? $select_calculations->avg_listings_price_size: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Highest Price:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->max_price <> null) ? $select_calculations->max_price: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Lowest Price:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->min_price <> null) ? $select_calculations->min_price: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Highest Price/ Sqt:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->max_price_sqt <> null) ? $select_calculations->max_price_sqt: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Lowest Price/ Sqt:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->min_price_sqt <> null) ? $select_calculations->min_price_sqt: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Average PSF:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->avg_psf <> null) ? $select_calculations->avg_psf: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Average Date:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->avg_listing_date <> null) ? $select_calculations->avg_listing_date: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Average Gross Yield:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->avg_gross_yield <> null) ? $select_calculations->avg_gross_yield: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>
    </fieldset>


    <?php
    $this->registerJs('
    
   var rows = $(".changed").closest("td").addClass("red");
     $(".simple").closest("td").css(\'text-align\', \'center\');
     $(".simple").on("click",function(){
      if($(this).closest("td").hasClass("red")){
      $(this).removeClass("red");
      $(this).closest("td").removeClass("red");
      }else{
     $(this).addClass("red");
     $(this).closest("td").addClass("red");
     }
    
     });
    
    ');
    ?>


