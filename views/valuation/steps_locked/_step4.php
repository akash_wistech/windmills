<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Schedule Inspection');
$cardTitle = Yii::t('app', 'Schedule Inspection:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_4/'.$valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
if ($model->email_status !=1) {
    $AlertText='Data and Valuation Status will be saved and Email will be sent once?';
}
else{
    $AlertText='Only Data will be saved?';
}

$this->registerJs('

$("#listingstransactions-valuation_date,#listingstransactions-inspection_date,#listingstransactions-client_deadline,#listingstransactions-valuation_report_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
$("#listingstransactions-inspection_time").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "hh:mm"
});

var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});

$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "'.Yii::t('app',$AlertText).'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
      console.log("Hello 123")
      $("#w0").unbind("submit").submit();
    }
});
});
$(\'#listingstransactions-valuation_date > .form-control\').prop(\'disabled\', true);
$(\'#listingstransactions-inspection_date > .form-control\').prop(\'disabled\', true);
$(\'#listingstransactions-client_deadline> .form-control\').prop(\'disabled\', true);
$(\'#listingstransactions-valuation_report_date > .form-control\').prop(\'disabled\', true);
$(\'#listingstransactions-inspection_time > .form-control\').prop(\'disabled\', true);

');


?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
</style>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <fieldset disabled="disabled">
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                    <?php  echo $this->render('../left-nav', ['model' => $valuation,'step' => 4]); ?>
                </div>
            </div>

            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <input type="hidden" id="inspection_type_id" value="<?= $valuation->inspection_type ?>">



                                    <div class="col-sm-4" id="valuation_date_id">
                                        <?= $form->field($model, 'valuation_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-valuation_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-valuation_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                    </div>


                                    <div class="col-sm-4" id="inspection_date_id">

                                        <label for="html" id='inpect_date_label'>Inspection Date</label><br>
                                        <?= $form->field($model, 'inspection_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-inspection_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-inspection_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])->label(false) ?>
                                    </div>

                                    <div class="col-sm-4" id="inspection_time_id">
                                        <?= $form->field($model, 'inspection_time', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-inspection_time" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-inspection_time" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                    </div>



                                    <div class="col-sm-4" id="client_deadline_id">
                                        <?= $form->field($model, 'client_deadline', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-client_deadline" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-client_deadline" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                    </div>

                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'valuation_report_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-valuation_report_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-valuation_report_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                    </div>

                                    <div class="col-sm-4" id="inspection_officer_id">
                                        <?php
                                        echo $form->field($model, 'inspection_officer')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->staffMemberListArr,
                                            'options' => ['placeholder' => 'Select a Person ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>


                                    <div class="col-sm-4" id="contact_person_name_id">
                                        <?= $form->field($model, 'contact_person_name')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4" id="contact_email_id">
                                        <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>
                                    </div>

                                    <div class="col-sm-4" id="contact_phone_no_id">
                                        <?= $form->field($model, 'contact_phone_no')->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>
                                <?php

                                if ($model->email_status==1) {
                                    $check=true;
                                }
                                ?>

                                <?php $model->step=1;  ?>
                                <?php echo $form->field($model, 'step')->textInput(['maxlength' => true,
                                    'type'=>'hidden'])->label('') ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </fieldset>


</div>



