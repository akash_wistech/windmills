<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use app\components\widgets\StatusVerified;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Enter Valuation Details');
$cardTitle = Yii::t('app', ' Enter Valuation Details:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_401/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');


$this->registerJs('

    $("#listingstransactions-valuation_report_date,#listingstransactions-valuation_date,#listingstransactions-inspection_date,#listingstransactions-client_deadline").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "YYYY-MM-DD"
    });
    
    $("#listingstransactions-inspection_time").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "HH:mm"
    });


    $(\'#listingstransactions-valuation_date > .form-control\').prop(\'disabled\', true);
   
    


    $(".only-char").keypress(function(event) {
        var inputValue = String.fromCharCode(event.which);
        // Regular expression to allow only letters and exclude the "+" sign
        var regex = /^[a-zA-Z]*$/;
        // Check if the input value matches the regular expression
        if (!regex.test(inputValue)) {
          event.preventDefault(); // Prevent input of non-letter characters
        }
      });
       $("body").on("change", "#valuationdetaildata-other_intended_users_check", function () {
           console.log($(this).val());
           if($(this).val() == "Yes"){
            $("#iuser_fixed_fee").show();
                   }else{
                    $("#valuationdetaildata-other_intended_users").val("");
                    $("#iuser_fixed_fee").hide();
                   }
       
    });
');
if ($valuation->purpose_of_valuation == 14) {
    $valuation_lable = 'Assessment Date';
} else {
    $valuation_lable = 'Valuation Date';
}
?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }

    .field-scheduleinspection-contact_phone_no, .field-scheduleinspection-land_line_no {
        width: 73% !important;
    }
</style>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 401]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <fieldset disabled="disabled">
                            <header class="card-header">
                                <h2 class="card-title">Valuation Details</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'other_intended_users_check')->widget(Select2::classname(), [
                                            'data' => array('No' => 'No', 'Yes' => 'Yes'),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ])->label('Other Intended Users');
                                        ?>
                                    </div>
                                    <?php
                                    if($model->other_intended_users_check == 'Yes') {
                                        $style = "block";
                                    }else{
                                        $style = "none";
                                    }



                                    ?>
                                    <div class="col-sm-4" id="iuser_fixed_fee" style="display: <?= $style ?>">
                                        <?= $form->field($model, 'other_intended_users')->textInput(['maxlength' => true])->label('Other Intended Users Value'); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'purpose_of_valuation')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->purposeOfValuationArr,
                                            'options' => ['placeholder' => 'Select a Purpose ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>

                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'valuation_scope')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->valuationScopeArr,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>
                                    <?php
                                    if (in_array(Yii::$app->user->identity->permission_group_id, [12, 4, 14, 1, 5, 7, 10, 22,37])) {
                                        ?>
                                        <div class="col-sm-4">
                                            <?php
                                            echo $form->field($model, 'service_officer_name')->widget(Select2::classname(), [
                                                'data' => Yii::$app->appHelperFunctions->staffMemberListArr,
                                                'options' => ['placeholder' => 'Select a Officer ...'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'disabled' => true
                                                ],
                                            ]);
                                            ?>

                                        </div>
                                        <?php

                                    }
                                    ?>


                                    <div class="col-sm-4" id="valuation_date_id">
                                        <?= $form->field($model, 'valuation_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-valuation_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-valuation_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])->label($valuation_lable) ?>
                                    </div>


                                </div>
                                <?php
                                if (in_array(Yii::$app->user->identity->permission_group_id, [12, 4, 14, 1, 5, 7, 10, 22,37])) {
                                ?>
                                <section class="valuation-form card card-outline card-primary">


                                    <header class="card-header">
                                        <h2 class="card-title">Change of Service Officer</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">

                                            <div class="col-sm-4" id="">
                                                <?php
                                                echo $form->field($model, 'changed_service_officer_name')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->staffMemberListArr,
                                                    'options' => ['placeholder' => 'Select a Person ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'disabled' => true
                                                    ],
                                                ]);
                                                ?>

                                            </div>


                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'changed_service_officer_reason')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(\app\models\InspectionTransferReasons::find()->where(['status' => 1])->andWhere(['trashed' => 0])->orderBy([
                                                        'title' => SORT_ASC,
                                                    ])->all(), 'id', 'title'),
                                                    'options' => ['placeholder' => 'Select a Change Reason ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'disabled' => true
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                    <?php

                                }
                                ?>
                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Owner Details</h2>
                                    </header>
                                    <div class="card-body">

                                        <table id="attachment"
                                               class="table table-striped table-bordered table-hover images-table">
                                            <thead>
                                            <tr>
                                                <td class="text-left">First Name</td>
                                                <td class="text-left">Last Name</td>
                                                <td class="text-left">Owner Percentage</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $row = 0; ?>
                                            <?php foreach ($model->valuationOwners as $attachment) { ?>
                                                <tr id="image-row-attachment-<?php echo $row; ?>">

                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="ValuationDetailData[owners_data][<?= $row ?>][name]"
                                                               value="<?= $attachment->name ?>" placeholder="First Name"
                                                               required/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="ValuationDetailData[owners_data][<?= $row ?>][lastname]"
                                                               value="<?= $attachment->lastname ?>" placeholder="Last Name"
                                                               required/>
                                                    </td>

                                                    <td>
                                                        <input type="number" class="form-control" step=".01"
                                                               name="ValuationDetailData[owners_data][<?= $row ?>][percentage]"
                                                               value="<?= $attachment->percentage ?>"
                                                               placeholder="Percentage" required/>
                                                    </td>
                                                    <input type="hidden" class="form-control"
                                                           name="ValuationDetailData[customAttachments][<?= $row ?>][id]"
                                                           value="<?= $attachment->id ?>" placeholder="Name" required/>
                                                    <input type="hidden" class="form-control"
                                                           name="ValuationDetailData[customAttachments][<?= $row ?>][index_id]"
                                                           value="<?= $row ?>" placeholder="Name" required/>

                                                    <td class="text-left">
                                                        <button type="button"
                                                                onclick="deleteRow('-attachment-<?= $row ?>', '<?= $attachment->id; ?>', 'attachment')"
                                                                data-toggle="tooltip"
                                                                title="You want to delete Attachment"
                                                                class="btn btn-danger"><i
                                                                    class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $row++; ?>
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="3"></td>
                                                <td class="text-left">
                                                    <button type="button" onclick="addAttachment();"
                                                            data-toggle="tooltip" title="Add"
                                                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>


                                </section>
                                


                            </div>
                            </fieldset>
                            <?php ActiveForm::end(); ?>
                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    var row = <?= $row ?>;
    function addAttachment() {

        html = '<tr id="image-row' + row + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ValuationDetailData[owners_data][' + row + '][name]" value="" placeholder="First Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ValuationDetailData[owners_data][' + row + '][lastname]" value="" placeholder="Last Name" required />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="number" class="form-control"  name="ValuationDetailData[owners_data][' + row + '][percentage]" step=".01" value="" placeholder="Percentage" required />';
        html += '    </div>';
        html += '  </td>';


        html += '       <input type="hidden" class="form-control"  name="ValuationDetailData[owners_data][' + row + '][index_id]" value="' + row + '" placeholder="Quantity" required />';



        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment tbody').append(html);

        row++;
    }

    function deleteRow(rowId, docID, type) {



        var url = '<?= \yii\helpers\Url::to('/valuation/remove-owner'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if(response.status == 'exist'){
                            swal("Warning!", response.message, "warning");
                        }else{
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
</script>
<script>



</script>
