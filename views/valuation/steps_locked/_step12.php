<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Derive MV/ Sold Transactions');
$cardTitle = Yii::t('app', 'Derive MV/ Sold Transactions:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_12/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <fieldset disabled="disabled">
    <div class="card-body">
        <div class="row">
            <?php   if ($readonly<>1){  ?>

                <div class="col-4 col-sm-2">
                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                         aria-orientation="vertical">
                        <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => '12_v']); ?>
                    </div>
                </div>
            <?php } ?>

            <?php if ($readonly==1){  $class='"col-12 col-sm-12"'; } ?>

            <?php if ($readonly<>1){  $class='"col-8 col-sm-9"'; } ?>

            <div class=<?= $class ?>>
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">
                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">
                                <div class="row">

                                    <table id="owner_container"
                                           class="table table-striped table-bordered table-hover text-center images-table ">
                                        <thead>
                                        <tr>
                                            <td style="padding: 10px !important;" class=" width_5">
                                                <strong>Label</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class=" width_5">
                                                <strong>Avg Valuation</strong>
                                            </td>

                                            <td style="padding: 10px !important;" class=" width_25">
                                                <strong>&nbsp;Subject Property</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class=" width_15">
                                                <strong>Difference</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_10">
                                                <strong>Standard Weightage</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_25">
                                                <strong>Change Weightage</strong>
                                            </td>
                                            <td style="padding: 10px !important;" class="width_25">
                                                <strong>Adjustments</strong>
                                            </td>
                                        </tr>
                                        </thead>
                                        <tbody>



                                            <?php
                                            if(!empty($preCalculationArray)) {
                                                foreach ($preCalculationArray as $key => $value) {
                                                    ?>
                                                    <tr>

                                                        <td>
                                                            <p><?=   ucfirst(str_replace("_"," ",$key)); ?></p>
                                                        </td>
                                                        <td>
                                                            <input type="text"
                                                                   name="ValuationDriveMv[<?= 'average_'.$key ?>]"
                                                                   value="<?=  $value['average']; ?>"
                                                                   placeholder="Days"
                                                                   class="form-control"
                                                                   readonly
                                                            />
                                                        </td>
                                                        <td>
                                                            <input type="text"
                                                                   value="<?=  $value['subject_property']; ?>"
                                                                   placeholder="Days"
                                                                   class="form-control"
                                                                   readonly
                                                            />
                                                        </td>
                                                        <td>
                                                            <input type="text"
                                                                   value="<?=  $value['difference']; ?>"
                                                                   placeholder="Days"
                                                                   class="form-control"
                                                                   readonly
                                                            />
                                                        </td>
                                                        <td>
                                                            <input type="text"
                                                                   value="<?=  $value['standard_weightage']; ?>"
                                                                   placeholder="Days"
                                                                   class="form-control"
                                                                   readonly
                                                            />
                                                        </td>
                                                        <td>
                                                            <input type="text"
                                                                   name="ValuationDriveMv[<?= 'weightage_'.$key ?>]"
                                                                   value="<?=  $value['change_weightage']; ?>"
                                                                   placeholder="Days"
                                                                   class="form-control"
                                                                <?php if ($readonly==1) {?>
                                                                    readonly
                                                                <?php  } ?>
                                                            />
                                                        </td>
                                                        <td>
                                                            <input type="text"
                                                                   value="<?=  $value['adjustments']; ?>"
                                                                   placeholder="Days"
                                                                   class="form-control"
                                                                   readonly
                                                            />
                                                        </td>
                                                    </tr>


                                                    <?php
                                                }
                                            }

                                            ?>

                                        </tbody>
                                    </table>

                                </div>
                                <?php if ($readonly<>1){  ?>

                                <?php } ?>
                                <?php ActiveForm::end(); ?>
                        </section>

                            <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title"> Calculations </h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Average Market Value:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($avg_listings_price_size <> null) ? $avg_listings_price_size: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Estimated Market Value as per Sold Transactions:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($model->mv_total_price <> null) ? $model->mv_total_price: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Average PSF:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($avg_psf <> null) ? $avg_psf: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Sold Transactions PSF:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($model->mv_avg_psf <> null) ? $model->mv_avg_psf : 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </fieldset>
    <!-- /.card -->
</div>



