<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

$this->registerJs('


$(".imgInps").change(function(){
    readURL(this);
});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if(input.value.includes(".pdf")== true){
                $("#blah").attr("src","'.Yii::$app->params['uploadPdfIcon'].'");
              }
              else {
                  $("#blah").attr("src", e.target.result);
               }
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(".imgInps").change(function(){
    readURL(this);
});


$("body").on("click", ".remove_file", function () {
_this=$(this);
swal({
title: "'.Yii::t('app','Confirmation').'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({
  url: "'.Url::to(['valuation/signaturedelete','id'=>$model->id]).'",
  dataType: "html",
  type: "POST",
  success: function(html) {
  $("#blah").attr("src", "'.Yii::$app->params['dummy_image_address'].'");
  },
  error: function(xhr,ajaxoptions,thownError){

    alert(xhr.responseText);
  }
  });
    }
});
});


');

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Signature');
$cardTitle = Yii::t('app', 'Signature:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_24/' . $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->reference_number ?>
        </h3>
    </div>
    <fieldset disabled="disabled">
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $model, 'step' => 24]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">


                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                            <header class="card-header">
                                <h2 class="card-title">Add Signature</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-sm-6">

                                        <p style="font-weight: bold; font-size:18px">Upload Only Pdf</p>
                                        <?php

                                        $imgname = Yii::$app->params['dummy_image_address'];
                                        if ($model->signature_img != null) {

                                            // echo 'working';
                                            // print_r($model->signature_img);
                                            // die();
                                            $imgname = Yii::$app->get('s3bucket')->getUrl('signature/images/'.$model->signature_img);
                                            $link=$imgname;
                                        }
                                        ?>
                                        <div style='width:350px; margin-bottom:30px; position:relative;'>

                                            <?php

                                            if (strpos($imgname,'.pdf') == true) {
                                                $imgname=Yii::$app->params['uploadPdfIcon'];
                                            }

                                            ?>
                                            <img id="blah" src="<?= $imgname ?>" alt="No Image is selected." width="350px"/>


                                            <?php
                                            if ($model->signature_img != null) {

                                                // print_r($model->signature_img);
                                                // die();
                                                ?>
                                                <a style="color:#ffffff; font-weight: bold; padding-left:7px; padding-right :7px; font-size:22px; position:absolute;  bottom:10px; left:20px; background-color:#558B2F;";
                                                   href="<?= $link ?>" target="_blank"><i class="fas fa-eye"></i></a>
                                                <!--  <p class="remove_file"
                                                    style="color:#ffffff; font-weight: bold; padding-left:7px; padding-right :7px; font-size:22px; position:absolute; bottom:0; right:15px; background-color:#558B2F;";
                                                 ><i class="fa fa-trash"></i></p><br> -->
                                            <?php } ?>

                                        </div>
                                        <div><?php

                                            $scanOfficer=app\models\User::find()->where(['id'=>Yii::$app->user->identity->id, 'permission_group_id'=>11])->select(['id'])->one();
                                            if ($scanOfficer['id'] !=null) {
                                                echo $form->field($model, 'sig_verification_status')->checkbox(['class'=>'mt-4']);
                                            } ?></div>
                                    </div>
                                </div>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </fieldset>
    <!-- /.card -->
</div>


<script type="text/javascript">




</script>
