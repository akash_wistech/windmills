<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Inspect Property');
$cardTitle = Yii::t('app', 'Inspect Property:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_5/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
if ($model->email_status !=1) {
    $AlertText='Data and Valuation Status will be saved and Email will be sent once?';
}
else{
    $AlertText='Only Data will be saved?';
}
$AlertText='Only Data will be saved?';
$this->registerJs('

$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "'.Yii::t('app',$AlertText).'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
      console.log("Hello 123")
      $("#w0").unbind("submit").submit();
    }
});
});

$("body").on("click", ".sav-btn", function (e) {

swal({
title: "'.Yii::t('app','Email will be sent to Clients!').'",
html: "'.Yii::t('app','').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Email will be sent to Clients!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({

  url: "'.Url::to(['valuation/inspectpropert','id'=>$model->id]).'",
  dataType: "html",
  type: "POST",
  });
    }
});
});

var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});
  $("#inspectproperty-listing_property_type_options").on(\'change\',function(){
        $("#inspectproperty-listing_property_type").val($(this).val());
        });


');

$i=1;
?>
<script>

</script>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 5]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <fieldset disabled="disabled">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body">
<div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group field-inspectproperty-makani_number required">
                                        <label class="control-label" for="inspectproperty-makani_number">Makani Number</label>
                                        <input type="text" id="inspectproperty-makani_number" class="form-control" name="InspectProperty[makani_number]" value="<?= $model->makani_number ?>" maxlength="255" aria-required="true" readonly>

                                        <div class="help-block"></div>
                                    </div>

                                </div>
                                <div class="col-sm-4">

                                        <div class="form-group field-inspectproperty-makani_number required">
                                            <label class="control-label" for="inspectproperty-makani_number">Latitude</label>
                                            <input type="text" id="inspectproperty-makani_number" class="form-control" name="InspectProperty[makani_number]" value="<?= $model->latitude ?>" maxlength="255" aria-required="true" readonly>

                                            <div class="help-block"></div>
                                        </div>


                                </div>
    <div class="col-sm-4">
        <?php
        echo $form->field($model, 'utilities_connected')->widget(Select2::classname(), [
            'data' => array('Yes' => 'Yes', 'No' => 'No'),
            'pluginOptions' => [
                'allowClear' => true,
                'disabled' => true
            ],
        ]);
        ?>
    </div>
</div>
                                <div class="row">

                                    <div class="col-sm-4">

                                        <div class="form-group field-inspectproperty-makani_number required">
                                            <label class="control-label" for="inspectproperty-makani_number">Longitude</label>
                                            <input type="text" id="inspectproperty-makani_number" class="form-control" name="InspectProperty[makani_number]" value="<?= $model->longitude ?>" maxlength="255" aria-required="true" readonly>

                                            <div class="help-block"></div>
                                        </div>


                                    </div>

                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_placement')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertyPlacementListArr,
                                        'options' => ['placeholder' => 'Select a Placement ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>

                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'acquisition_method')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->acquisitionMethodsArr,
                                        'options' => ['placeholder' => 'Select a Placement ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_visibility')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertyVisibilityListArr,
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_exposure')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertyExposureListArr,
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_category')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertiesCategoriesListArr,
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>

                                </div>
                                </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_condition')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertyConditionListArr,
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'property_defect')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->propertyDefectsArr,
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
<!--
                                    <div class="col-sm-4">
                                        <?php
/*
                                        $model->property_defect = explode(',', ($model->property_defect <> null) ? $model->other_facilities : "");
                                        echo $form->field($model, 'other_facilities')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertyDefectsArr,
                                            'options' => ['placeholder' => 'Select a Defects ...'],
                                            'pluginOptions' => [
                                                'placeholder' => 'Select a Facilities',
                                                'multiple' => true,
                                                'allowClear' => true
                                            ],
                                        ]);
                                        */?>
                                    </div>-->


                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'development_type')->widget(Select2::classname(), [
                                        'data' => array('Standard' => 'Standard', 'Non-Standard' => 'Non-Standard'),
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                </div>
                                    <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'finished_status')->widget(Select2::classname(), [
                                        'data' => array('Shell & Core' => 'Shell & Core', 'Fitted' => 'Fitted'),
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'developer_id')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(\app\models\Developers::find()->orderBy([
                                            'title' => SORT_ASC,
                                        ])->all(), 'id', 'title'),
                                        'options' => ['placeholder' => 'Select a Developer ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>

                                </div>
                                        <div class="col-sm-4">

                                            <div class="form-group field-inspectproperty-makani_number required">
                                                <label class="control-label" for="inspectproperty-makani_number">Estimated Age</label>
                                                <input type="text" id="inspectproperty-makani_number" class="form-control" name="InspectProperty[makani_number]" value="<?= $model->estimated_age ?>" maxlength="255" aria-required="true" readonly>

                                                <div class="help-block"></div>
                                            </div>


                                        </div>

                                    </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'estimated_remaining_life')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                                </div>

                                    <div class="col-sm-4">

                                        <div class="form-group field-inspectproperty-makani_number required">
                                            <label class="control-label" for="inspectproperty-makani_number">Balcony Size</label>
                                            <input type="text" id="inspectproperty-makani_number" class="form-control" name="InspectProperty[makani_number]" value="<?= $model->balcony_size ?>" maxlength="255" aria-required="true" readonly>

                                            <div class="help-block"></div>
                                        </div>


                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-group field-inspectproperty-makani_number required">
                                            <label class="control-label" for="inspectproperty-makani_number">Service Area Size</label>
                                            <input type="text" id="inspectproperty-makani_number" class="form-control" name="InspectProperty[makani_number]" value="<?= $model->service_area_size ?>" maxlength="255" aria-required="true" readonly>

                                            <div class="help-block"></div>
                                        </div>


                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-4">

                                        <div class="form-group field-inspectproperty-makani_number required">
                                            <label class="control-label" for="inspectproperty-makani_number">Measured Built Up Area</label>
                                            <input type="text" id="inspectproperty-makani_number" class="form-control" name="InspectProperty[makani_number]" value="<?= $model->built_up_area_m ?>" maxlength="255" aria-required="true" readonly>

                                            <div class="help-block"></div>
                                        </div>


                                    </div>

                                    <div class="col-sm-4">

                                        <div class="form-group field-inspectproperty-makani_number required">
                                            <label class="control-label" for="inspectproperty-makani_number">Net Built Up Area</label>
                                            <input type="text" id="inspectproperty-makani_number" class="form-control" name="InspectProperty[makani_number]" value="<?= $model->net_built_up_area ?>" maxlength="255" aria-required="true" readonly>

                                            <div class="help-block"></div>
                                        </div>


                                    </div>

                                    <div class="col-sm-4">

                                        <div class="form-group field-inspectproperty-makani_number required">
                                            <label class="control-label" for="inspectproperty-makani_number">Number Of Basement</label>
                                            <input type="text" id="inspectproperty-makani_number" class="form-control" name="InspectProperty[makani_number]" value="<?= $model->number_of_basement ?>" maxlength="255" aria-required="true" readonly>

                                            <div class="help-block"></div>
                                        </div>


                                    </div>

                                </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'number_of_levels')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->listingLevelsListArr,
                                        'options' => ['placeholder' => 'Select a Level ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>

                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'pool')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes', 'No' => 'No'),

                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                    <div class="col-sm-4">
                                        <?php
                                      /*  echo "<pre>";
                                        print_r($model->other_facilities);
                                        die;
                                        echo $model->other_facilities;
                                        die;
                                        if($model->other_facilities <> null) {
                                            $model->other_facilities = explode(',', ($model->other_facilities <> null) ? $model->other_facilities : "");
                                        }*/
                                        $model->other_facilities = explode(',', ($model->other_facilities <> null) ? $model->other_facilities : "");
                                        echo $form->field($model, 'other_facilities')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(\app\models\OtherFacilities::find()->orderBy([
                                                'title' => SORT_ASC,
                                            ])->all(), 'id', 'title'),
                                            'options' => ['placeholder' => 'Select a Facilities ...'],
                                            'pluginOptions' => [
                                                'placeholder' => 'Select a Facilities',
                                                'multiple' => true,
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                               <!-- <div class="col-sm-4">
                                    <?php
/*                                    echo $form->field($model, 'gym')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    */?>
                                </div>-->
                                </div>
                                <div class="row">
                               <!-- <div class="col-sm-4">
                                    <?php
/*                                    echo $form->field($model, 'play_area')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    */?>
                                </div>-->




                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'occupancy_status')->widget(Select2::classname(), [
                                            'data' => array('Owner Occupied' => 'Owner Occupied', 'Tenanted' => 'Tenanted', 'Vacant' => 'Vacant'),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-group field-inspectproperty-makani_number required">
                                            <label class="control-label" for="inspectproperty-makani_number">Number Of Basement</label>
                                            <input type="text" id="inspectproperty-makani_number" class="form-control" name="InspectProperty[makani_number]" value="<?= $model->number_of_basement ?>" maxlength="255" aria-required="true" readonly>

                                            <div class="help-block"></div>
                                        </div>


                                    </div>
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'full_building_floors')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($model, 'parking_floors')->textInput(['maxlength' => true])->label("Parking Space") ?>
                                </div>
                                </div>
                                <div class="row">

                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'landscaping')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes', 'No' => 'No', 'Semi-Landscape' => 'Semi-Landscape'),
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'fridge')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'listing_property_type')->widget(Select2::classname(), [
                                           // 'data' => Yii::$app->appHelperFunctions->listingsPropertyTypeListArr,
                                            'data' => ArrayHelper::map(\app\models\ListingSubTypes::find()->orderBy([
                                                'title' => SORT_ASC,
                                            ])->all(), 'title', 'title'),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>
                             <!--       <div class="col-sm-4">
                                        <?/*= $form->field($model, 'listing_property_type')->textInput(['maxlength' => true]) */?>
                                    </div>-->
                                </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'oven')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'cooker')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">





                                    <?php

                                    $model->ac_type = explode(',', ($model->ac_type <> null) ? $model->ac_type : "");
                                    echo $form->field($model, 'ac_type')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->acTypesArr,
                                        'options' => ['placeholder' => 'Select a AC Type ...'],
                                        'pluginOptions' => [
                                            'placeholder' => 'Select a AC Type',
                                            'multiple' => true,
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);



                                 /*   echo $form->field($model, 'ac_type')->widget(Select2::classname(), [
                                        'data' => Yii::$app->appHelperFunctions->acTypesArr,
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);*/
                                    ?>

                                </div>
                                </div>
                                <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'washing_machine')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'furnished')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unfurnished','standard'=> 'Yes – we are valuing the unit as standard furnished', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo $form->field($model, 'white_goods')->widget(Select2::classname(), [
                                        'data' => array('Yes' => 'Yes – we are valuing the unit as unequipped as comparable are unequipped','standard'=> 'Yes – we are valuing the unit as standard equipped', 'No' => 'No'),
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'disabled' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'mode_of_transport')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->modesOfTransportArr,
                                            'options' => ['placeholder' => 'Select a Transport Mode'],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ]);
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'start_kilometres')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'end_kilometres')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'plot_area_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrPlotSource,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Source of Plot Area');
                                        ?>

                                    </div>

                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'plot_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrPlotSource,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ])->label('Plot Source');
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'parking_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrParkingSource,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ])->label('Parking Source');
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'age_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrAgeSource,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ])->label('Age Source');
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'bua_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrBuaSource,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ])->label('Source of BUA');
                                        ?>

                                    </div>

                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'measurement')->textInput(['maxlength' => true])->label("Measurement") ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'extension')->textInput(['maxlength' => true])->label("extension") ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        echo $form->field($model, 'extension_source')->widget(Select2::classname(), [
                                            'data' => Yii::$app->appHelperFunctions->propertiesDocumentsListArrExtensionSource,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'disabled' => true
                                            ],
                                        ])->label("Extension Permission Document");
                                        ?>

                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($model, 'completion_status')->textInput(['maxlength' => true])?>
                                    </div>
                                </div>

                                <section class="card card-outline card-info">
                                    <header class="card-header">
                                        <h2 class="card-title"><?= Yii::t('app', 'Location Attributes') ?></h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <?php
                                                echo $form->field($model, 'location_highway_drive')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                                    'disabled' => true
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                echo $form->field($model, 'location_school_drive')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                                    'disabled' => true
                                                    ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                echo $form->field($model, 'location_mall_drive')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                                    'disabled' => true
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                echo $form->field($model, 'location_sea_drive')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                                    'disabled' => true
                                                    ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                echo $form->field($model, 'location_park_drive')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->locationAttributesValue,
                                                    'disabled' => true
                                                ]);
                                                ?>
                                            </div>


                                        </div>

                                    </div>

                                </section>

                                <section class="card card-outline card-info">
                                    <header class="card-header">
                                        <h2 class="card-title"><?= Yii::t('app', 'View Attributes') ?></h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_community')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewCommunityAttributesValue,
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_pool')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                    'disabled' => true
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_burj')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                    'disabled' => true
                                                ]);
                                                ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_sea')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                    'disabled' => true
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_marina')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                    'disabled' => true
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_mountains')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                    'disabled' => true
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_lake')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                    'disabled' => true
                                                    ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_golf_course')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                    'disabled' => true
                                                ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_park')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                    'disabled' => true
                                                    ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'view_special')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->viewAttributesValue,
                                                    'disabled' => true
                                                    ]);
                                                ?>
                                            </div>
                                        </div>

                                    </div>

                                </section>


                                <div class="clearfix"></div>
                                <!-- Configration Information-->
                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Configration Information</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_bedrooms')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_bathrooms')->textInput(['maxlength' => true]) ?>
                                            </div>


                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_kitchen')->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_living_area')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_dining_area')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_maid_rooms')->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_laundry_area')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_store')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_service_block')->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_garage')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_balcony')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_family_room')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_powder_room')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'no_of_study_room')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <?php $model->step=1;  ?>
                                            <?php echo $form->field($model, 'step')->textInput(['maxlength' => true,
                                                'type'=>'hidden'])->label('') ?>

                                        </div>

                                    </div>
                                </section>

                                <!-- Configration Information Custom-->
                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Custom Configration Information</h2>
                                    </header>
                                    <div class="card-body">

                                        <table id="attachment" class="table table-striped table-bordered table-hover images-table">
                                            <thead>
                                            <tr>
                                                <td class="text-left">Name</td>
                                                <td class="text-left">Quantity</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $row = 0; ?>
                                            <?php foreach ($model->customAttachements as $attachment) { ?>
                                                <tr id="image-row-attachment-<?php echo $row; ?>">

                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="InspectProperty[customAttachments][<?= $row ?>][name]"
                                                               value="<?= $attachment->name ?>" placeholder="Name" required />
                                                    </td>

                                                    <td>
                                                        <input type="number" class="form-control"
                                                               name="InspectProperty[customAttachments][<?= $row ?>][quantity]"
                                                               value="<?= $attachment->quantity ?>" placeholder="Quantity" required />
                                                    </td>
                                                    <input type="hidden" class="form-control"
                                                           name="InspectProperty[customAttachments][<?= $row ?>][id]"
                                                           value="<?= $attachment->id ?>" placeholder="Name" required />

                                                    <td class="text-left">
                                                        <button type="button"
                                                                onclick="deleteRow('-attachment-<?= $row ?>', '<?= $attachment->id; ?>', 'attachment')"
                                                                data-toggle="tooltip" title="You want to delete Attachment"
                                                                class="btn btn-danger"><i
                                                                    class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $row++; ?>
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td class="text-left">
                                                    <button type="button" onclick="addAttachment();" data-toggle="tooltip" title="Add"
                                                            class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>

                            </div>
                        </section>


                            </div>

                           <!-- <div class="card-footer">
                                <?/*= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) */?>
                                <?/*= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) */?>
                            </div>-->
                            <?php ActiveForm::end(); ?>



                    </fieldset>


                    <section class="valuation-form card card-outline card-primary">

                        <?php $form = ActiveForm::begin(); ?>
                        <div class="card-body">

                            <div class="col-sm-4">
                                <?= $form->field($model, 'built_up_area')->textInput(['maxlength' => true])->label('Final Built Up Area') ?>
                            </div>
                            <input type="hidden" id="inspectproperty-checkofc" class="form-control" name="InspectProperty[checkofc]" value="1" aria-invalid="false">

                        </div>


                        <div class="card-footer">
                            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn2']) ?>

                        </div>
                        <?php ActiveForm::end(); ?>

                    </section>



                </div>
            </div>
        </div>


    </div>
    <!-- /.card -->
</div>





<script type="text/javascript">
    var row = <?= $row ?>;
    function addAttachment() {

        html = '<tr id="image-row' + row + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="InspectProperty[customAttachments][' + row + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="number" class="form-control"  name="InspectProperty[customAttachments][' + row + '][quantity]" value="" placeholder="Quantity" required />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#attachment tbody').append(html);

        row++;
    }

    function deleteRow(rowId, docID, type) {



        var url = '<?= \yii\helpers\Url::to('/valuation/remove-attachment'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if(response.status == 'exist'){
                            swal("Warning!", response.message, "warning");
                        }else{
                            swal("Deleted!", response.message, "success");
                            $('#image-row' + rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
</script>