<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ValuationFormAsset;
ValuationFormAsset::register($this);
use kartik\depdrop\DepDrop;
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('

// $("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
//     allowInputToggle: true,
//     viewMode: "months",
//     format: "YYYY-MM-DD"
// });
');


?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }
    .fade{
        opacity: 1;
    }
    .nav-tabs.flex-column .nav-link.active{
        background-color: #007bff;
        color: white;
        font-weight: bold;
    }
</style>





    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-edit"></i>
                Receive Valuation
            </h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-5 col-sm-3">
                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="vert-tabs-home-tab" data-toggle="pill" href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home" aria-selected="false">Receive Valuation</a>
                    </div>
                </div>
                <div class="col-7 col-sm-9">
                    <div class="tab-content" id="vert-tabs-tabContent">
                        <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                             aria-labelledby="vert-tabs-home-tab">
                            <?php echo $this->render('steps_income/_form_step_1', ['model' => $model, 'step' => 1, 'existing_client_array' => $existing_client_array]); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>



