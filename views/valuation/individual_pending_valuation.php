
<?php

use app\models\Company;
use app\models\InspectProperty;
use app\models\User;
use app\models\Valuation;
use app\models\ValuationApproversData;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\assets\SortBootstrapAsset;
SortBootstrapAsset::register($this);
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


$this->title = Yii::t('app', $page_title);
$cardTitle = Yii::t('app', $page_title);
$this->params['breadcrumbs'][] = $this->title;

$propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$propertiesArr = ['' => 'select'] + $propertiesArr;

$communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$communitiesArr = ['' => 'select'] + $communitiesArr;

$citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
$citiesArr = ['' => 'select'] + $citiesArr;

?>

<style>


    .dataTable th {
        color: #0056b3;
        font-size: 15px;
        text-align: left !important;
        /* padding-right: 30px !important; */
    }
    .dataTable td {
        font-size: 16px;
        text-align: left;
        /* padding-right: 50px; */
        padding-top: 3px;
        max-width: 100px;
    }

    .content-header h1 {
        font-size: 16px !important;

    }

    .content-header .row {
        margin-bottom: 0px !important;
    }

    .content-header {
        padding: 0px !important;
    }

<?php 
    if($page_title == "Pending Individual Inspection Scheduled") { ?>
    .custom-footer .dataTables_length{
        clear: both !important;
    }
    th:nth-child(1) {
        min-width:  75px;
        max-width:  75px;
    }
    th:nth-child(2) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(3) {
        min-width:  55px;
        max-width:  55px;
    }

    th:nth-child(4) {
        min-width:  67px;
        max-width:  67px;
    }

    th:nth-child(5) {
        min-width:  60px;
        max-width:  60px;
    }

    th:nth-child(6) {
        min-width:  70px;
        max-width:  70px;
    }
    th:nth-child(7) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(8) {
        min-width:  30px;
        max-width:  30px;
    }
    th:nth-child(9) {
        min-width:  50px;
        max-width:  50px;
    }
    th:nth-child(10) {
        min-width:  55px;
        max-width:  55px;
    }
    th:nth-child(11) {
        min-width:  60px;
        max-width:  60px;
    }
    th:nth-child(12) {
        min-width:  0px;
        max-width:  0px;
    }
    th:nth-child(13) {
        min-width:  45px;
        max-width:  45px;
    }

<?php } ?>


</style>


<?php  if( $page_title == "Pending Individual Inspection Scheduled" ) { ?>
    <div class="insp_scheduled-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
            <span><strong><?= $page_title ?></strong></span><br /><br /><br />
                <table id="insp_scheduled" class="table table-striped dataTable table-responsive">
                    <thead>
                        <tr>
                            <th class="">Instruction Date
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <th class="">Inspection Schedule
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th>

                            <!-- <th class="">Inspection Done
                                <input type="date" class="custom-search-input form-control" placeholder="Date">
                            </th> -->

                            <th class="">Ref. Number
                                <input id="reference_number" type="text" class="form-control" placeholder="Ref. No.">
                            </th>

                            <th class="">Client Ref. No
                                <input type="text" class="custom-search-input form-control" placeholder="Ref#">
                            </th>

                            <!-- <th class="">Client
                                <input type="text" class="custom-search-input form-control" placeholder="Client">
                            </th> -->

                            <th class="">Property
                                <input type="text" class="custom-search-input form-control" placeholder="Property">
                            </th>
                            
                            <th style="padding: 0px 0px 10px 0px;" class="">Community
                                <input type="text" class="custom-search-input form-control" placeholder="Community">
                            </th>

                            <th class="">Project / Building
                                <input type="text" class="custom-search-input form-control" placeholder="Building">
                            </th>

                            <th class="">City
                                <input type="text" class="custom-search-input form-control" placeholder="City">
                            </th>

                            <th class="">Inspector
                                <input type="text" class="custom-search-input form-control" placeholder="I/O">
                            </th>

                            <th class="">Contact Person
                                <input type="text" class="custom-search-input form-control" placeholder="Person">
                            </th>

                            <th class="">Contact Phone
                                <input type="text" class="custom-search-input form-control" placeholder="Phone">
                            </th>

                            <th> </th>

                            <th>Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($dataProvider->getModels())>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($dataProvider->getModels() as $model){
                        ?>
                        <tr class="active">

                            <!-- Instruction -->
                            <td>
                                <?php  
                                    echo date('d-M-Y', strtotime($model->instruction_date));
                                    echo "<br>";
                                    if(isset($model->instruction_time))
                                    {
                                        echo date('h:i A', strtotime($model->instruction_time));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Inspection Schedule -->
                            <td>
                                <?php 
                                    if(isset($model->scheduleInspection->inspection_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->scheduleInspection->inspection_date));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($model->scheduleInspection->inspection_time));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Inspection Done -->
                            <!-- <td>
                                <?php   
                                    $data = InspectProperty::find()->where(['valuation_id' => $model->id])->one();
                                    if(isset($data->inspection_done_date))
                                    {
                                        echo   date('d-M-Y', strtotime($data->inspection_done_date));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td> -->

                            <!-- Reference Number -->
                            <td>
                                <?= $model->reference_number ?>
                            </td>

                            <!-- Client ref -->
                            <td>
                                <?= $model->client_reference ?>
                            </td>
                                
                            <!-- Client -->
                            <!-- <td>
                                <?= $model->client->nick_name ?>
                            </td> -->

                            <!-- Property -->
                            <td>
                                <?= $model->property->title ?>
                            </td>

                            <!-- Community -->
                            <td>
                                <?= $model->building->communities->title ?>
                            </td>

                            <!-- Project / Building -->
                            <td>
                                <?= $model->building->title ?>
                            </td>

                            <!-- City -->
                            <td>
                                <?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
                            </td>

                            <!-- Inspection Officer -->
                            <td>
                                <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                            </td>

                            <!-- Contact Person -->
                            <td>
                                <?php
                                    if(isset($model->contact_person_name))
                                    {
                                        echo $model->contact_person_name;
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>

                            <!-- Contact Phone -->
                            <td>
                                <?php
                                    if ($model->contact_phone_no <> null) $contact_number = $model->phone_code . '' . $model->contact_phone_no;
                                    if ($model->land_line_no <> null) $contact_number = $model->land_line_code . '' . $model->land_line_no;
                                    echo $contact_number;
                                ?>
                            </td>

                            <td style="text-align:center;"> <a href="javascript:;" class="showModalButton" data-valuation-ref="<?= $model->reference_number ?>" data-valuation-id="<?= $model->id ?>"><i class="fas fa-eye text-success"></i></a> </td>

                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item text-1"
                                            href="<?= yii\helpers\Url::to(['valuation/step_5','id'=>$model->id]) ?>"
                                            title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> View 
                                        </a>
                                    </div>
                                </div>
                            </td>

                        </tr>
                        <?php
                    }
                }
            ?>
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <!-- <th></th> -->
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?php
    $this->registerJs('
      
                
        var dataTable = $("#insp_scheduled").DataTable({
         
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            
            columnDefs: [ {
                "targets": 0, 
                "type": "date-eu", 
                "render": function(data, type, row) {
                    if (type === \'sort\' || type === \'type\') {
                        return moment(data, \'DD-MMM-YYYY\').format(\'YYYY-MM-DD\');
                    }
                    return data;
                }
            }
              ]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    dataTable.search(data).draw();
                },
                error: bbAlert
                });
            });
                
          $("#insp_scheduled_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#insp_scheduled_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#insp_scheduled_wrapper").append(customFooter);

          $(".showModalButton").on("click", function (event) {
            // console.log(event.delegateTarget.dataset.valuationRef)
            // console.log(event.delegateTarget)
            // $id = $(".showModalButton").attr("data-valuation-id");
    
            // model_id = event.delegateTarget.dataset.valuationId;
            // model_ref = event.delegateTarget.dataset.valuationRef;
    
            // $(".modal-title").text(model_ref)
    
            // document.getElementById("id1").value=model_id ; 
    
            // $("#pending_modal").modal("show");
    
            $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/get_pending_inspection_reasons']).'/"+$(this).attr("data-valuation-id"),
                method: "post",
                dataType: "html",
                success: function(data) {
                //   console.log(data);
                  $("#modalContainer").append(data);
                  $("#modalContainer").modal("show");
                },
                error: bbAlert
            });
        });
    
        $("#modalContainer").on("hidden.bs.modal", function() {
            $(this).find(".modal-dialog").empty();
        });
          
          
    ');
    ?>
<?php } ?>

<div class="modal fade" id="modalContainer"></div>

