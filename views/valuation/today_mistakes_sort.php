<?php

use app\models\Valuation;
use app\models\ValuationApproversData;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$clientsArr = ArrayHelper::map(\app\models\Company::find()
->select([
  'id','title',
  'cname_with_nick' => 'CONCAT(title," ",nick_name)',
])
  ->where(['status' => 1])
  ->andWhere([
      'or',
      ['data_type' => 0],
      ['data_type' => null],
  ])
  ->orderBy(['title' => SORT_ASC,])
  ->all(), 'id', 'title');
  $clientsArr = ['' => 'select'] + $clientsArr;

  $propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
  $propertiesArr = ['' => 'select'] + $propertiesArr;

  $communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
  $communitiesArr = ['' => 'select'] + $communitiesArr;

  $citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
  $citiesArr = ['' => 'select'] + $citiesArr;
?>

<style>
/* .dataTable th {*/
/*    color: #0056b3;*/
/*    font-size: 14px;*/
/*    text-align: right !important;*/
/*    padding-right: 30px !important;*/
/*}*/
/*.dataTable td {*/
/*    font-size: 16px;*/
/*    text-align: right;*/
/*    padding-right: 50px;*/
/*}*/
/*.content-header h1 {*/
/*       font-size: 16px !important;*/

/*    }*/
/*.content-header .row {*/
/*    margin-bottom: 0px !important;*/
/*}*/
/*.content-header {*/
/*    padding: 4px !important;*/
/*} */



.dataTable th {
      color: #0056b3;
      font-size: 15px;
      text-align: left !important;
      /* padding-right: 30px !important; */
  }
  .dataTable td {
      font-size: 15px;
      text-align: left;
      /* padding-right: 50px; */
      padding-top: 3px;
  }

  .table th, .table td {
        padding: 9px !important;
    }

    th:nth-child(1) {
      min-width: 80px ;
      max-width: 80px ;
    }
    th:nth-child(2) {
      min-width: 80px ;
      max-width: 80px ;
    }
    th:nth-child(3) {
      min-width: 80px ;
      max-width: 80px ;
    }
    th:nth-child(4) {
      min-width: 55px ;
      max-width: 55px ;
    }
    th:nth-child(5) {
      min-width: 50px ;
      max-width: 50px ;
    }
    th:nth-child(6) {
      min-width: 55px ;
      max-width: 55px ;
    }
    th:nth-child(7) {
      min-width: 60px ;
      max-width: 60px ;
    }
    th:nth-child(8) {
      min-width: 63px ;
      max-width: 63px ;
    }
    th:nth-child(9) {
      min-width: 50px ;
      max-width: 50px ;
    }
    th:nth-child(10) {
      min-width: 63px ;
      max-width: 63px ;
    }
    th:nth-child(11) {
      min-width: 63px ;
      max-width: 63px ;
    }
    th:nth-child(12) {
      min-width: 57px ;
      max-width: 57px ;
    }
    th:nth-child(13) {
      min-width: 59px ;
      max-width: 59px ;
    }
    th:nth-child(14) {
      min-width: 53px ;
      max-width: 53px ;
    }
    th:nth-child(15) {
      min-width: 50px ;
      max-width: 50px ;
    }
    th:nth-child(16) {
      min-width: 45px ;
      max-width: 45px ;
    }
    </style>


<div class="bank-revenue-index col-12">
    <div class="card card-outline card-info">
        <div class="card-body">
        <span><strong><?= $page_title; ?></strong></span><br />
            <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                <thead>
                    <tr>
                        <th class="">Instruction Date
                            <input type="text" class="custom-search-input form-control" placeholder="date">
                        </th>

                        <th class="">Challenge Date
                          <input type="date" class="custom-search-input form-control" placeholder="Challenge date">
                        </th>

                        <th class="">Error Date
                          <input type="date" class="custom-search-input form-control" placeholder="Error date">
                        </th>

                        <th class="">WM New Ref Number
                          <input type="text" class="custom-search-input form-control" placeholder="New Ref.">
                        </th>

                        <th class="">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder'=>'client']); ?>
                        </th>
                        
                        <th class="">Client Ref
                          <input type="text" class="custom-search-input form-control" placeholder="client ref">
                        </th>

                        <th class="">Property
                            <?php echo Html::dropDownList('property', null, $propertiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'Property']); ?>
                        </th>
                        
                        <th class="">Inspector<input type="text" class="custom-search-input form-control" placeholder="Inspector"></th>
                        
                        <th class="">Valuer<input type="text" class="custom-search-input form-control" placeholder="Valuer"></th>
                        
                        <th class="">Reviewer<input type="text" class="custom-search-input form-control" placeholder="Reviewer"></th>
                                            
                        <th class="">Approver<input type="text" class="custom-search-input form-control" placeholder="approver"></th>
                      
                        <th class="">Approve Market Value<input type="text" class="custom-search-input form-control" placeholder="Market Value Approved"></th>

                        <th class="">Revise Market Value Recomm<input type="text" class="custom-search-input form-control" placeholder="Revised Market Value Recommended"></th>

                        <th class="">Revise Market Value Review<input type="text" class="custom-search-input form-control" placeholder="Revised Market Value Reviewed"></th>

                        <th class="">Revise Market Value Appr<input type="text" class="custom-search-input form-control" placeholder="Revised Market Value Approved"></th>


                        
                        <?php 
                          if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                        ?>
                        <th>Action</th>
                        <?php
                          }
                        ?>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($dataProvider->getModels() as $model){
                    ?>
                    <tr class="active">

                      <!-- Instruction date -->
                      <td>
                          <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                      </td>

                      <!-- Challenge date -->
                      <td>
                      <?=  date('d-M-Y', strtotime($model->created_at)) ?>
                      </td>

                      <!-- Error date -->
                      <td>
                      <?=  date('d-M-Y', strtotime($model->created_at)) ?>
                      </td>

                      <!-- WM NEW REF -->
                      <td>
                          <?php 
                            $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                            echo $get_valuation->reference_number;
                          ?>
                      </td>

                      <!-- client -->
                      <td>
                          <?= ($model->client->nick_name <> null) ?$model->client->nick_name : $model->client->title ?>
                      </td>
                      
                      <!-- client reference -->
                      <td><?= $model->client_reference ?></td>

                      <!-- property -->
                      <td>
                          <?= $model->property->title ?>
                      </td>
                    
                      <!-- Inspector -->
                      <td>
                        <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                      </td>

                      <!-- Valuer -->
                      <td>
                          <?php
                              echo (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '';
                          ?>
                      </td>

                      <!-- Reviewer -->
                      <td>
                          <?php
                              if($model->valuerData->approver_type == "reviewer")
                              {
                                  $get_name = User::find()->where(['id' => $model->valuerData->updated_by])->one();
                                  echo  $get_name->lastname;
                              }else{
                                  echo "";
                              }
                          ?>
                      </td>

                      <!-- Approver -->
                      <td><?= (isset($model->approverData->user->firstname) && ($model->approverData->user->firstname <> null))? ($model->approverData->user->lastname): '' ?>
                      
                      <!-- Market Value Approved -->
                      <td>
                          <?php 
                              $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                              ->andWhere(['approver_type' => 'approver'])
                              ->orderBy(['id' => SORT_DESC])
                              ->one();
                              
                              echo (isset($get_valuation_data->id) && ($get_valuation_data->id <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                          ?>
                      </td>

                      <!-- Revised Market Value Recommended -->
                      <td>
                          <?php
                          $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();

                          $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                          ->andWhere(['approver_type' => 'valuer'])
                          ->orderBy(['id' => SORT_DESC])
                          ->one();

                          echo (isset($get_valuation_data->estimated_market_value) && ($get_valuation_data->estimated_market_value <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                          ?>
                      </td>

                      <!-- Revised Market Value Reviewed -->
                      <td>
                          <?php 
                              $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                          
                              $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                              ->andWhere(['approver_type' => "reviewer"])
                              ->orderBy(['id' => SORT_DESC])
                              ->one();
                              if(isset($get_valuation_data->estimated_market_value))
                              {
                                  echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                              }else{
                                  echo "";
                              }
                          ?>
                      </td>

                      <!-- Revised Market Value Approved -->
                      <td>
                          <?php 
                              $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();
                          
                              $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                              ->andWhere(['approver_type' => "approver"])
                              ->orderBy(['id' => SORT_DESC])
                              ->one();
                              if(isset($get_valuation_data->estimated_market_value))
                              {
                                  echo Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value);
                              }else{
                                  echo "";
                              }
                          ?>
                      </td>

                      
                      
                      <?php 
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                      ?>
                      <td class="noprint actions">
                          <div class="btn-group flex-wrap">
                              <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                  data-toggle="dropdown">
                                  <span class="caret"></span>
                              </button>
                              <div class="dropdown-menu" role="menu">
                                  <a class="dropdown-item text-1"
                                      href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                      title="Step 1" data-pjax="0">
                                      <i class="fas fa-edit"></i> Step 1
                                  </a>
                              </div>
                          </div>
                      </td>
                      <?php
                        }
                      ?>

                    </tr>
                    <?php
                }
            }
        ?>
                </tbody>
                <tfoot>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>

                <?php 
                  if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                ?>
                <th></th>
                <?php
                  }
                ?>
                  
                </tfoot>
            </table>
        </div>
    </div>
</div>




<?php
    $this->registerJs('
    
    
        // $("#bank-revenue").DataTable({
        //     order: [[0, "asc"]],
        //     pageLength: 50,
        //     searching: false,
        // });
        
          var dataTable = $("#bank-revenue").DataTable({
            order: [[0, "asc"]],
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            columnDefs: [{
              targets: -1,  // The last column index
              orderable: false  // Disable sorting on the last column
            }]
          });
        
          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input-client").on("change", function () {
              $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                  console.log(data);
                  dataTable.search(data).draw();
                },
                error: bbAlert
            });

          });
                
          $("#bank-revenue_filter").css({
            "display":"none",
          });
        
        $(".custom-search-input").on("click", function (event) {
          event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
          event.stopPropagation();
        });
        

        // Move the length menu to the table footer
        // var lengthMenu = $(".dataTables_length");
        // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");

        // Create a custom footer element
        var customFooter = $("<div class=\"custom-footer\"></div>");
      
        // Move the length menu to the custom footer
        var lengthMenu = $(".dataTables_length");
        lengthMenu.detach().appendTo(customFooter);
      
        // Append the custom footer to the DataTable wrapper
        $("#bank-revenue_wrapper").append(customFooter);
        
        

    ');
?>