<?php

use app\models\CrmReceivedProperties;


//same as TOE
$discount = 0;
$discount_p = 0;
$VAT =0;
$discount_quotations = 0;


$toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $quotation_data->id])->sum('toe_fee');
if($type=='first_half'){
    $toe_fee_total_2 = number_format(($toe_fee_total * ('0.'.$quotation_data->first_half_payment)), 2, '.', '');
}
if($type=='final_half'){
    $toe_fee_total_2 = number_format(($toe_fee_total * ('0.'.$quotation_data->second_half_payment)), 2, '.', '');
}

$grossfee = $toe_fee_total;
if ($quotation_data->relative_discount_toe!=null) {
    $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total,$quotation_data->relative_discount_toe);
    // $grossfee= $grossfee - $discount;
}
if ($quotation_data->no_of_property_discount!=null) {
    $discount_p =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($toe_fee_total,$quotation_data->no_of_property_discount);
    $discount= $discount + $discount_p;
}

if($quotation_data->same_building_discount > 0) {
    $same_building_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($quotation_data->same_building_discount, $toe_fee_total);
    $discount = $discount + $same_building_discount_amount;
}
if($quotation_data->first_time_discount > 0) {
    $first_time_fee_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($quotation_data->first_time_discount, $toe_fee_total);
    $discount = $discount + $first_time_fee_discount_amount;
}



$VAT =0;
if($model->client_id != 1) {

    $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
    // $netValuationFee= $netValuationFee - $VAT ;
    $finalFeePayable = $netValuationFee + $VAT;
}else{

    $VAT = yii::$app->quotationHelperFunctions->getVatTotal_add($netValuationFee);
    /*    echo $grossfee.'<br>';
        echo $netValuationFee.'<br>';

        echo $VAT;
        die*/;

    $finalFeePayable = $netValuationFee ;
    $netValuationFee = $netValuationFee - $VAT;
    // $grossfee= $grossfee -   $VAT;
}

//End same as TOE
$boxfee= Yii::$app->appHelperFunctions->wmFormate($finalFeePayable);

$clientAddress = \app\models\Company::find()->select(['address'])->where(['id'=>$model->client_id])->one();
$fee_to_words     = yii::$app->quotationHelperFunctions->numberTowords(number_format($finalFeePayable,0,'.',''));
if($type=='first_half'){
    $fee_to_words_2     = yii::$app->quotationHelperFunctions->numberTowords($finalFeePayable * ('0.'.$quotation_data->first_half_payment));
    $boxfee= Yii::$app->appHelperFunctions->wmFormate($finalFeePayable * ('0.'.$quotation_data->first_half_payment));
}
if($type=='final_half'){
    $fee_to_words_2     = yii::$app->quotationHelperFunctions->numberTowords($finalFeePayable * ('0.'.$quotation_data->second_half_payment));
    $boxfee= Yii::$app->appHelperFunctions->wmFormate($finalFeePayable * ('0.'.$quotation_data->second_half_payment));
}

$postfix_invoice_number = '';
if($type=='first_half'){
    $postfix_invoice_number= '-AP';
}else if($type=='final_half'){
    $postfix_invoice_number= '-FP';
}
?>




<style>
    .box1{
        background-color: #e6e6e6;
        width: 100px;
        padding-top: 20px !important;
        border-right: 5px white;
        border-left: 5px white;
    }

    .border {
        width: 100%;
        border-top-width: 0.05px;
        border-bottom-width: 0.05px;
        border-left-width: 0.05px;
        border-right-width: 0.05px;
        padding: 5px;
    }
    .font_color{
        color: #3763ae;
    }

</style>


<table class="border" width="547">

    <tr>
        <td colspan="5" class="detailtexttext first-section" >
            <br><span class="font_color" ><?= $branch_address->company; ?> </span>
            <br><span><?= $branch_address->address; ?></span>
            <br><span><?= $branch_address->office_phone; ?></span>
            <br><span>finance@windmillsgroup.com, serviceteam@windmillsgroup.com </span>
            <br><span><?= $branch_address->website; ?></span>
            <br><span>TRN: <?= $branch_address->trn_number; ?></span>
        </td>


        <td colspan="3" class="detailtexttext first-section">
            <br><span class="font_color">Bank Details: </span>
            <br><span>Emirates NBD </span>
            <br><span>Al Quoz Branch  </span>
            <br><span>Account No: 1015286631901 </span>
            <br><span>IBAN: <?= $branch_address->iban_number; ?> </span>
            <br><span>Swift code: EBILAEAD</span>
        </td>
    </tr>

</table><br><br>

<table>
    <thead>
    <tr>
        <td colspan="1" class="detailtexttext first-section" style="width: 30%">
            <br><span class="size-8 airal-family font_color">Client</span>

            <br><span><?php

                if(	$model->client_invoice_type == 1){
                ?><?= $model->client_name_passport;?></span>
            <?php }else{ ?><?= $model->client->title ?></span>
            <?php } ?>



            <br><span><?= $clientAddress->address ?></span>
            <?php
            if ($model->client->id !=9167) {?>
                <br><span>UAE.</span>
                <?php
            }
            ?>
            <?php if($model->client->trn_number <> null ){ ?>
                <br>TRN: <span><?= $model->client->trn_number ?></span>
                <?php
            }
            ?>

        </td>

        <td class="box1 text-dec airal-family" style="margin-top: 20px; padding-top: 20px!important;top:50px; width: 18%">
            <span style="color: #000000;"><p><span style="font-weight: bold; font-size: 10px">Invoice Number</span>
                    <br>
                    <span style="padding-top: 10px;font-size: 9px; margin-top: 10px; line-height: 20px"><?= Yii::$app->appHelperFunctions->getInvoiceNumber($model);
                        ?><?= $postfix_invoice_number ?></span></p>
            </span>
        </td>
        <td class="box1 text-dec airal-family" style="margin-top: 20px; padding-top: 20px!important;top:50px;width: 18%">
            <span style="color: #000000;"><p><span style="font-weight: bold; font-size: 10px">Issue Date</span>
                    <br>
                    <span style="padding-top: 10px;font-size: 9px; margin-top: 10px; line-height: 20px"><?= date('d-M-Y', strtotime($model->submission_approver_date)) ?></span></p>
            </span>
        </td>
        <td class="box1 text-dec airal-family" style="margin-top: 20px; padding-top: 20px!important;top:50px;width: 18%">
            <span style="color: #000000;"><p><span style="font-weight: bold; font-size: 10px">Payable Fee</span>
                    <br>
                    <span style="padding-top: 10px;font-size: 9px; margin-top: 10px; line-height: 20px"><?= $boxfee ?></span></p>
            </span>
        </td>
        <td class="box1 text-dec airal-family" style="margin-top: 20px; padding-top: 20px!important;top:50px;width: 18%">
            <span style="color: #000000;"><p><span style="font-weight: bold; font-size: 10px">Due Date</span>
                    <br>
                    <span style="padding-top: 10px;font-size: 9px; margin-top: 10px; line-height: 20px">


                    </span></p>
            </span>
        </td>

    </tr>
    </thead>
</table><br><br>
<br>

<table class="border" cellspacing="1" cellpadding="3" width="466">


    <tr  style="color:#3763ae ; border: 1px solid !important;">
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:18%;">&nbsp;&nbsp;Instruction Date</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold;width:15%">WM Reference</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:17% ">Client Reference</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; ;width:15%">Customer</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:20% ">Property Description</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:10% ">Fee AED</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:10% ">VAT 5%</td>
        <td class="airal-family" style="font-size:9px; font-weight: bold; width:10% ">Total</td>
    </tr>
</table>
<table  cellspacing="1" cellpadding="3" width="466" style=" padding: 5px;">
    <tbody>
    <?php
    $i=1;
    if($valuations <> null && !empty($valuations)) {
        foreach($valuations as $key => $valuation){
            $current_property = CrmReceivedProperties::find()->where(['id' => $valuation->quotation_property])->one();
            if($quotation_data->relative_discount_toe != null ) {


                $discount_per_property = yii::$app->quotationHelperFunctions->getDiscountRupee($current_property->toe_fee, $quotation_data->relative_discount_toe);
             //   $valuation->fee = $valuation->fee + $discount_per_property;
            }

            if($quotation_data->no_of_property_discount != null ) {

                $discount_per_property_num = yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($current_property->toe_fee, $quotation_data->relative_discount_toe);
              //  $valuation->fee = $valuation->fee + $discount_per_property_num;
            }


            if($valuation->client_id == 1){
                $vat_property = yii::$app->quotationHelperFunctions->getVatTotal_add($valuation->fee);
                //  $valuation->fee = $valuation->fee - $vat_property;
            }else{
                $vat_property = yii::$app->quotationHelperFunctions->getVatTotal($valuation->fee);
            }
            if($valuation->parent_id > 0){
                $valuation->fee = 0;
            }

            ?>
            <tr style="background-color: ;">
                <td  style="font-size:8.5px;width:18%">&nbsp;&nbsp;<?= date('d-M-Y',strtotime($valuation->instruction_date))  ?></td>
                <td  style="font-size:8.5px;width:15%"><?= $valuation->reference_number; ?></td>
                <td  style=" font-size:9px; width:17%"><?= $valuation->client_reference; ?></td>
                <td  style=" font-size:9px; width:15%"><?= $valuation->client_name_passport; ?></td>
                <td  style="font-size:9px;width:20% "><?= $valuation->property->title; ?>,<?= $valuation->building->title; ?>,<?= $valuation->building->subCommunities->title ?> , <?= Yii::$app->appHelperFunctions->emiratedListArr[$valuation->building->city]; ?></td>
                <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"><?= number_format($valuation->fee,2); ?></td>
                <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"><?=  number_format($vat_property,2) ?></td>
                <td class="tbl-body" style="font-size:9px; text-align:right;width: 10%"><?= number_format(($valuation->fee + $vat_property)) ?></td>

            </tr>



            <?php

        }
    }
    ?>

    </tbody>
</table>
<br><br>
<table cellspacing="1" cellpadding="3" width="466">
    <thead>
    <tr style="background-color: ;">
        <td class="airal-family" style="font-size:10px; text-align:left;">Total Fee </td>
    </tr>
    </thead>
</table>
<table class="border airal-family" cellspacing="1" cellpadding="5" width="547">
    <tbody>
    <?php
    if($model <> null && !empty($model)) {

        ?>
        <tr style="line-height: 7px">
            <td  style="font-size:9px;text-align:left;font-weight:bold">Gross Fee</td>
            <td colspan="5"  style=" font-size:9px; text-align: center;"></td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px;font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($netValuationFee); ?></td>


        </tr>
       <!-- <tr style="line-height: 7px">
            <td  style="font-size:9px;text-align:left;font-weight:bold">Discount</td>
            <td colspan="5"  style=" font-size:9px; text-align: center;font-weight:bold"></td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; font-weight:bold"><?/*= Yii::$app->appHelperFunctions->wmFormate($discount); */?></td>

        </tr>-->
 <!--       <tr style="line-height: 7px">
            <td  style="font-size:9px;text-align:left;font-weight:bold">Net Fee</td>
            <td colspan="5"  style=" font-size:9px; text-align: center;"></td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px;font-weight:bold"><?/*= Yii::$app->appHelperFunctions->wmFormate($netValuationFee); */?></td>


        </tr>-->
        <tr style="line-height: 7px">
            <td  style="font-size:9px;text-align:left;font-weight:bold">VAT (5%)</td>
            <td colspan="5"  style=" font-size:9px; text-align: center;"></td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($VAT); ?></td>


        </tr>
        <tr style="line-height: 7px">
            <td  style="font-size:9px;text-align:left;font-weight:bold" >Fee + VAT</td>

            <td colspan="5"  style=" font-size:9px; text-align: center;font-weight:bold">&nbsp;<?= $fee_to_words." only." ?></td>
            <td  style="font-size:9px;font-weight:bold">AED</td>
            <td  style=" font-size:9px; font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($finalFeePayable); ?></td>
        </tr>


    <?php }
    ?>

    </tbody>
</table>



<br><br>
<?php if($type=='first_half'){ ?>
    <table cellspacing="1" cellpadding="3" width="466">
        <thead>
        <tr style="background-color: ;">
            <td class="airal-family" style="font-size:10px; text-align:left;">Advance Payment </td>
        </tr>
        </thead>
    </table>
    <table class="border airal-family" cellspacing="1" cellpadding="5" width="547">
        <tbody>
        <?php
        if($model <> null && !empty($model)) {

            ?>
            <tr  style="line-height: 8px">
                <td colspan="2" style="font-size:9px;text-align:left;font-weight:bold">50 % Advance Payment</td>
                <td colspan="4"  style=" font-size:9px; text-align: center;"></td>
                <td  style="font-size:9px;font-weight:bold">AED</td>
                <td  style=" font-size:9px;font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($netValuationFee * ('0.'.$quotation_data->second_half_payment)); ?></td>


            </tr>
            <tr style="line-height: 8px">
                <td colspan="2" style="font-size:9px;text-align:left;font-weight:bold">VAT (5%)</td>
                <td colspan="4"  style=" font-size:9px; text-align: center;"></td>
                <td  style="font-size:9px;font-weight:bold">AED</td>
                <td  style=" font-size:9px; font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($VAT * ('0.'.$quotation_data->second_half_payment)); ?></td>


            </tr>
            <tr style="line-height: 8px">
                <td  style="font-size:9px;text-align:left;font-weight:bold">Payable Fee</td>

                <td colspan="5"  style=" font-size:9px; text-align: center;font-weight:bold"><?= $fee_to_words_2." only." ?></td>
                <td  style="font-size:9px;font-weight:bold">AED</td>
                <td  style=" font-size:9px; font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($finalFeePayable* ('0.'.$quotation_data->second_half_payment)); ?></td>
            </tr>


        <?php }
        ?>

        </tbody>
    </table>
<?php }

if($type=='final_half'){

    ?>
    <table cellspacing="1" cellpadding="3" width="466">
        <thead>
        <tr style="background-color: ;">
            <td class="airal-family" style="font-size:10px; text-align:left;">Final Payment </td>
        </tr>
        </thead>
    </table>
    <table class="border airal-family" cellspacing="1" cellpadding="5" width="547">
        <tbody>
        <?php
        if($model <> null && !empty($model)) {

            ?>
            <tr style="line-height: 8px">
                <td colspan="2"  style="font-size:9px;text-align:left;font-weight:bold">50 % Last Payment</td>
                <td colspan="4"  style=" font-size:9px; text-align: center;"></td>
                <td  style="font-size:9px;font-weight:bold">AED</td>
                <td  style=" font-size:9px;font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($netValuationFee* ('0.'.$quotation_data->second_half_payment)); ?></td>


            </tr>
            <tr style="line-height: 8px">
                <td  style="font-size:9px;text-align:left;font-weight:bold">VAT (5%)</td>
                <td colspan="5"  style=" font-size:9px; text-align: center;"></td>
                <td  style="font-size:9px;font-weight:bold">AED</td>
                <td  style=" font-size:9px; font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($VAT* ('0.'.$quotation_data->second_half_payment)); ?></td>


            </tr>
            <tr style="line-height: 8px">
                <td  style="font-size:9px;text-align:left;font-weight:bold">Payable Fee</td>

                <td colspan="5"  style=" font-size:9px; text-align: center;font-weight:bold"><?= $fee_to_words_2." only." ?></td>
                <td  style="font-size:9px;font-weight:bold">AED</td>
                <td  style=" font-size:9px; font-weight:bold"><?= Yii::$app->appHelperFunctions->wmFormate($finalFeePayable * ('0.'.$quotation_data->second_half_payment)); ?></td>
            </tr>


        <?php }
        ?>

        </tbody>
    </table>

<?php } ?>

<br><br>
<table cellspacing="1" cellpadding="3" width="466">
    <thead>
    <tr style="background-color: ;">
        <td class="airal-family" style="font-size:10px; text-align:left;">We sincerely thank you for giving us this privilege to work for you. </td>

    </tr>

    </thead>
</table>
<br>
<br>
<br>
<br>
<br>
<br>


<style>
    .airal-family{
        font-family: Arial, Helvetica, sans-serif;
    }
</style>


<style>

    .text-dec{
        font-size: 8px;
        text-align: center;
        padding-right: 10px;
    }

    /*td.detailtext{*/
    /*background-color:#BDBDBD; */
    /*font-size:9px;*/
    /*border-top: 0.4px solid grey;*/
    /*}*/

    td.detailtexttext{
        /* background-color:#EEEEEE; */
        font-size:10px;
        font-weight: 100;

    }
    th.detailtext{
        /* background-color:#BDBDBD; */
        font-size:9px;
        border-top: 1px solid black;
    }
    /*.border {
    border: 1px solid black;
    }*/

    th.amount-heading{
        color: #039BE5;
    }
    td.amount-heading{
        color: #039BE5;
    }
    th.total-due{
        font-size: 16px;
    }
    td.total-due{
        font-size: 16px;
    }
    span.size-8{
        font-size: 10px;
        font-weight: bold;
    }
    td.first-section{
        padding-left: 10px;
        /* background-color: black; */
    }
</style>

