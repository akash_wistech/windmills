
<?php

// This is for 1.05 Valuation Instructions From.
$instructorname=Yii::$app->PdfHelper->getUserInformation($model);


//1.14  Total Building Floor(s)
if($model->property->title == 'Villa') {
    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1 ) {
        $total_building_floors = "Basement + " .Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
    }else if($model->inspectProperty->number_of_basement > 1){
        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }else{
        $total_building_floors = Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
    }

}else {
    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1) {
        $total_building_floors = "Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    } else if($model->inspectProperty->number_of_basement > 1){
        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }else {
        $total_building_floors = "Ground + " . $model->inspectProperty->full_building_floors . " Building floors";
    }
}
$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();

$conflict= \app\models\ValuationConflict::find()->where(['valuation_id'=>$model->id])->one();

$owners_in_valuation = \yii\helpers\ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $model->id])->all(), 'id', 'name');
$woners_name = "";
if(!empty($owners_in_valuation)) {
    $woners_name = implode(", ", $owners_in_valuation);
}



//  1.36  View Type
$ViewType = Yii::$app->PdfHelper->getViewType($model);

// echo "<pre>";
// print_r($ViewType);
// echo "<pre>";
// die();


// print_r($model->inspectProperty->other_facilities );
// die();

// 1.39 Building/Community Facilities
$model->inspectProperty->other_facilities = explode(',', ($model->inspectProperty->other_facilities <> null) ? $model->inspectProperty->other_facilities : "");
foreach ($model->inspectProperty->other_facilities as $key => $value) {
    $other_facilitie= \app\models\OtherFacilities::find()->where(['id'=>$value])->one();
    if ($other_facilities!=null) {  $other_facilities.=', '.$other_facilitie->title;  }
    else { $other_facilities.=$other_facilitie->title;   }
}


// 1.43, 1.44 , 1.45, 1.46  Floor Configuration
$floorConfig=Yii::$app->PdfHelper->getFloorConfig($model);
$floorConfigCustom=Yii::$app->PdfHelper->getFloorConfigCustom($model);


$floorConfig['groundFloortitle'] = rtrim($floorConfig['groundFloortitle'], "<br>");

if($floorConfig['groundFloortitle'] == 'None' && $floorConfigCustom['groundFloortitle'] !=''){
    $floorConfig['groundFloortitle'] = "<br>" . rtrim($floorConfigCustom['groundFloortitle'], "<br>");
}else {
    $floorConfig['groundFloortitle'] .= "<br>" . rtrim($floorConfigCustom['groundFloortitle'], "<br>");
}
$floorConfig['firstFloortitle'] = rtrim($floorConfig['firstFloortitle'], "<br>");
if($floorConfig['firstFloortitle'] == 'None' && $floorConfigCustom['firstFloortitle'] !=''){
    $floorConfig['firstFloortitle'] =  $floorConfigCustom['firstFloortitle']. "<br>";
}else {
    if(rtrim($floorConfigCustom['firstFloortitle'] <> null)) {
        $floorConfig['firstFloortitle'] .= "<br>" . $floorConfigCustom['firstFloortitle']. "<br>";
    }
}

// $floorConfig['firstFloortitle'] .= "<br>".rtrim($floorConfigCustom['firstFloortitle'], "<br>");

$floorConfig['secondFloortitle'] = rtrim($floorConfig['secondFloortitle'], "<br>");
if($floorConfig['secondFloortitle'] == 'None' && $floorConfigCustom['secondFloortitle'] !=''){
    $floorConfig['secondFloortitle'] =  rtrim($floorConfigCustom['secondFloortitle'], "<br>");
}else {
    if(rtrim($floorConfigCustom['secondFloortitle'] <> null)) {
        $floorConfig['secondFloortitle'] .= "<br>" . rtrim($floorConfigCustom['secondFloortitle'], "<br>");
    }
}
//   $floorConfig['secondFloortitle'] .= "<br>".rtrim($floorConfigCustom['secondFloortitle'], "<br>");
$floorConfig['basementTitle'] = rtrim($floorConfig['basementTitle'], "<br>");
if($floorConfig['basementTitle'] == 'None' && $floorConfigCustom['basementTitle'] !=''){
    $floorConfig['basementTitle'] =  rtrim($floorConfigCustom['basementTitle'], "<br>");
}else {
    if(rtrim($floorConfigCustom['basementTitle'] <> null)) {
        $floorConfig['basementTitle'] .= "<br>" . rtrim($floorConfigCustom['basementTitle'], "<br>");
    }
}


//$floorConfig['basementTitle'] .= "<br>".rtrim($floorConfigCustom['basementTitle'], "<br>");

// 1.66 Documents Provided by Client , 1.67 Documents not Provided
$DocumentByClient = Yii::$app->PdfHelper->getDocumentByClient($model);


$DocumentByClient['documentAvail'] = rtrim($DocumentByClient['documentAvail'], "<br>");
$DocumentByClient['documentNotavail'] = rtrim($DocumentByClient['documentNotavail'], "<br>");


$makani_number = ($model->inspectProperty->makani_number > 0)? $model->inspectProperty->makani_number: 'Not Applicable';
$plot_number = ($model->plot_number <> null && $model->plot_number != '0')? $model->plot_number: 'Not Applicable';
$source_bua =($model->inspectProperty->bua_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->bua_source] . ')': '';
$source_plot_size =($model->inspectProperty->plot_area_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->plot_area_source]. ')': '';
$source_extension =($model->inspectProperty->extension_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->extension_source]. ')': '';

$plot_size = ($model->land_size > 0)? $model->land_size.' square feet'.$source_plot_size: 'Not Applicable';

$market = 'Market';
if($model->purpose_of_valuation == 3) {
    $market = 'Fair';
}


$otherInstructingPerson='';
$result = \app\models\User::find()->where(['id'=>$model->other_instructing_person])->one();
if ($result<>null) {
    $otherInstructingPerson = $result->firstname.' '.$result->lastname;
}
// print_r($otherInstructingPerson); die();
?>


<!--  <br pagebreak="true" />-->

<table cellspacing="1" cellpadding="2" style="border: 1px dashed #64B5F6;" class="col-12">
    <tr>
        <td colspan="7" class="tableSecondHeading" style="color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;"><h5>Executive Valuation Report Summary</h5></td>
    </tr>

    <tr>
        <td colspan="7" class="tableSecondHeading"><h5> A. Valuation Overview</h5></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Client’s Name</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->client->title ?></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Other Intended User(s)</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= ( $model->other_intended_users <> null) ? $model->other_intended_users : 'N/A' ?></td>
    </tr>
    <tr  class="">
        <td colspan="3" style="color:#0277BD; font-size:13;">Reference No:</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12;"><?= $model->reference_number ?>-Draft</td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Date of Assessment</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php echo trim( date('l, jS \of F, Y', strtotime($model->scheduleInspection->valuation_date))) ?></td>
    </tr>
    <tr class="" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Inspection Date</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php if($model->inspection_type == 3){
                ?>
                <?='Not Applicable as desktop valuation '; ?>
            <?php }
            else{?><?php echo date('l, jS \of F, Y', strtotime($model->scheduleInspection->inspection_date)) ?>
                <?php //echo Yii::$app->formatter->asDate($model->scheduleInspection->inspection_date,Yii::$app->params['fulldaydate']) ?>
            <?php } ?>
        </td>
    </tr>

    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;">Scope of Work</td><td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td><?php if($model->purpose_of_valuation == 3){ ?><td  colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray">Estimated Fair Value of the Subject Property</td>
        <?php } else{ ?>
            <td  colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?=  Yii::$app->appHelperFunctions->valuationScopeArr[$model->valuation_scope] ?></td>
        <?php } ?>

    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;">Purpose of Assessment</td><td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td><td  colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray">Reinstatement cost for Insurance Purpose to comply with local legislation and international best practice. </td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"></td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray"></td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"></td>
    </tr>

    <tr>
        <td colspan="7" class="tableSecondHeading"><h5> B. Property Description - Location</h5></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Property Type</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].' '.$model->property->title ?></td>
    </tr>
    <tr class="" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Project/Building Name</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->building->title ?></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Plot Number</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->plot_number ?></td>
    </tr>
    <tr class="" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Sub-Community Name</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->building->subCommunities->title ?></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Community Name</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->building->communities->title ?></td>
    </tr>
    <tr class="" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> City and Country</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>, United Arab Emirates</td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"></td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray"></td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"></td>
    </tr>


    <tr>
        <td colspan="7" class="tableSecondHeading"><h5> C. Property Description -External</h5></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Tenure</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
    </tr>
    <tr class="" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Plot Size</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $plot_size ?></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Built-Up Area (BUA)</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->inspectProperty->net_built_up_area ?> square feet <?= $source_bua ?></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Gross Floor Area (GFA)</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->inspectProperty->gfa ?> square feet <?= $source_bua ?></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Number of Towers</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->no_of_towers ?></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Number of Units</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?= $model->unit_number ?></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Building Age</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?php if($model->client->id == 1 && $model->inspectProperty->estimated_remaining_life > 0){ ?><?= ($model->inspectProperty->estimated_remaining_life - 1) . ' - '. ($model->inspectProperty->estimated_remaining_life + 1) ?> Years<?php  }else{ ?><?= round($model->inspectProperty->estimated_remaining_life) ?> Years<?php } ?></td>
    </tr>

    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"></td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray"></td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"></td>
    </tr>


    <tr>
        <td colspan="7" class="tableSecondHeading"><h5> D.Reinstatement Cost</h5></td>
    </tr>
    <tr class="bggray" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Reinstatement Cost</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:10; width:293px;"  class="bggray"><?= ($estimate_price_byapprover <> null)? "AED ".$estimate_price_byapprover: "Not Applicable" ?></td>
    </tr>
    <!-- <tr class="" style="width:10%;">
        <td colspan="3" style="color:#0277BD; font-size:13;"> Reinstatement Cost Rate</td>
        <td colspan="1" style="color:#0277BD; font-size:13; width:10px; text-align:center;" class="bggray">:</td>
        <td colspan="3" style="color:#212121; font-size:12; width:293px;"  class="bggray"><?/*= ($approver_data->estimated_market_value_sqf <> null)? "AED ".number_format($approver_data->estimated_market_value_sqf).' per square foot': "Not Applicable" */?></td>
    </tr>-->
</table>
