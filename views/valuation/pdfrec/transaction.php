
<?php
/*echo $model->property->valuation_approach;
die;*/
?>
<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
    Appendix 4 – Construction Cost Index
</div>
<br><br>
<?php
$construction_cost_index_statistics =\app\models\WindmillsDocs::find()->where(['file' => 'construction_cost_index_statistics'])->orderBy(['created_at' => SORT_DESC])->one();
?>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>7.1.	Main Construction Cost Basis</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><?php if($construction_cost_index_statistics<>null){ ?>
                <img src="<?= $construction_cost_index_statistics->url ?>"  >
            <?php } ?>
           <p>Source: Dubai Statistics Centre</p>
        </td></tr>
</table>
<br><br>
<br pagebreak="true" />
<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
    Appendix 5 – Interest Rate and Financing Schedule
</div>
<br><br>
<table cellpadding="5" border="1" style="padding: 10px;">
    <tr style="background-color: #ECEFF1">
        <th><h4>Period.</h4></th>
        <th><h4>Loan</h4></th>
        <th><h4>Monthly Interest</h4></th>
        <th><h4>Payback Period</h4></th>
        <th><h4>Efective Interest</h4></th>
        <th><h4>Loan + Interest</h4></th>
    </tr>

    <?php
    $total_months = 12 * $model->inspectProperty->project_development_period;




    for ($i=0; $i< $total_months; $i++){

        $loan = round($cost_approach['total_professional_fee_values_saved']/$total_months);
        $monthly_interest = round($loan * ( $cost_approach['finance_cost']['fee_rate']/$total_months));
        $payback_period = $total_months - ($i + 1);
        $efective_interest = round($monthly_interest * $payback_period);
        $loan_interest = round($loan + $efective_interest);

        ?>
        <tr>

            <td style="border: none"><?= ($i + 1); ?></td>
            <td  style="border: none"><?= Yii::$app->appHelperFunctions->wmFormate($loan); ?></td>
            <td  style="border: none"><?= Yii::$app->appHelperFunctions->wmFormate($monthly_interest); ?></td>
            <td  style="border: none"><?= Yii::$app->appHelperFunctions->wmFormate($payback_period); ?></td>
            <td  style="border: none"><?= Yii::$app->appHelperFunctions->wmFormate($efective_interest); ?></td>
            <td  style="border: none"><?= Yii::$app->appHelperFunctions->wmFormate($loan_interest); ?></td>
        </tr>
    <?php  } ?>


</table>
<br>
<?php
$uae_central_bank_eibor_rates =\app\models\WindmillsDocs::find()->where(['file' => 'uae_central_bank_eibor_rates'])->orderBy(['created_at' => SORT_DESC])->one();
?>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailtext" colspan="2"><?php if($uae_central_bank_eibor_rates<>null){ ?>
                <img src="<?= $uae_central_bank_eibor_rates->url ?>" height="500" width="500" >
            <?php } ?>
        </td></tr>
</table>
<br><br>
<br pagebreak="true" />
<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
    Appendix 6 - Schedule of Items Typically Included/Excluded Within Reinstatement Cost Assessment
</div>
<br>

<table  cellpadding="8" class="main-class col-12">
    <tr><td class="detailheading" colspan="2"><h4>Included within Owners/Landlords</h4></td></tr>
    <!--<tr><td class="detailheading" colspan="2"><h4></h4></td></tr>
    <tr><td class="detailheading" colspan="2"><h4>1.Shell</h4></td></tr>-->
    <br>
    <tr><td style="background-color: darkgrey" colspan="2"><h4>1.Shell</h4></td></tr>
    <tr><td class="detailtext" colspan="2"><p>1.1.	Excavations as necessary to construct foundations.</p>
    <p>1.2.	Concrete, reinforcement, etc. and all other work in foundations up to and including damp proof membrane and lowest floor slab.</p>
    <p>1.3.	Structural steelwork, concrete, reinforcement, etc. and all other work in beams, columns and the like in construction of the frame.</p>
    <p>1.4.	Concrete, reinforcement, etc. and all other work in the construction of the upper floor slabs.</p>
    <p>1.5.	Concrete, reinforcement, etc. metal decking, boarding or the like in the construction of the roof slabs.</p>
    <p>1.6.	Coverings to roofs including tiles, slates, insulation, asphalt, felt or similar materials.</p>
    <p>1.7.	Rainwater gutters, downpipes, outlets and the like down to lowest floor level.</p>
    <p>1.8.	Concrete, reinforcement, structural steelwork and the like in construction of staircases, landings, etc.</p>
    <p>1.9.	Brickwork, blockwork, concrete work, reinforcement, cladding, insulation, or the like in external walls, party walls, internal partitions or walls, lift shafts, staircase enclosure walls, strong rooms and the like having regard to degree of annexation within the building.</p>
    <p>1.10.	Windows, glass, window calls, and damp-proof course in external walls.</p>
    <p>1.11.	Excavations, concrete beds, surrounds, granular beds, brickwork, rain pipes, fittings, accessories in surface water and soil water drainage, manholes, inspection chambers, petrol interceptors and the like within the curtilage of the demised site and beyond the curtilage up to and including the connection to the public drainage system.</p>
    <p>1.12.	Roads, car parks, paths, boundary walls, external signage and the like within the curtilage of the demised site. </p>
    <p>1.12.	Roads, car parks, paths, boundary walls, external signage and the like within the curtilage of the demised site. </p>
        </td>
    </tr>

    <tr><td style="background-color: darkgrey" colspan="2"><h4>2. Fitting Out Works - Owners/Landlords Fixtures and Fittings </h4></td></tr>
    <tr><td class="detailtext" colspan="2">
            <p>2.1.	Brickwork, blockwork, or studwork in forming partitions, ducts etc.</p>
            <p>2.2.	Doors, beams, architraves, ironmongery, borrowed lights, internal fixed screens, WC cubicles etc.</p>
            <p>2.3.	Plaster and other similar wall finishes, ceramic or similar tiled surfaces or facings to walls, plain sheet linings; internal and external decorations but excluding trade linings/claddings and specialized internal decorations, mirrored column cladding etc.</p>
            <p>2.4.	Suspended ceilings; plaster and other similar finishes to soffits of floors, landings, stair flights and the like. </p>
            <p>2.5.	Screeded and other similar finishes to floors, landings and treads and risers, ceramic or similar tiled surfaces or block finishing, asphalt or similar finishing’s, fixed raised floor duct systems, (excluding vinyl, Lino, carpet or other similar 'loose' coverings).</p>
            <p>2.6.	Timber or other similar rails and skirtings, built in to or fixed onto the structure.</p>
            <p>2.7.	Balustrade, handrails, framework in the staircases, access ladders etc.</p>
            <p>2.8.	Shop fronts complete including finishing to columns, pilasters, fascia paneling, lobbies, stall risers etc. but excluding internal and external illuminated or non-illuminated trade lettering or signage.</p>
            <p>2.9.	Fixed sanitary plumbing installations (i.e. soil, waste ventilation and overflow pipes from and including sanitary fittings) including pipework, fittings, accessories, sanitary fittings, gullies etc.</p>
            <p>2.10. Hot and cold-water plumbing installations including pipework, fittings, lagging and accessories, excluding trade installations likely to be removed on vacation by the tenant.</p>
            <p>2.11. Main engineering plant comprising heating and air conditioning installations including boilers, calorifiers, and all similar heating plant, distributive primary and secondary pipework and lagging, fittings and accessories including pumps, radiators, convectors, over door heaters, fan assisted blowers, heat pumps, air/water cooled condensers, air conditioning plant, fans, and all other similar ventilation plant, ductwork complete with fittings and accessories and all other similar supply and extract air distribution systems including all primary control systems even if computerized but excluding trade mechanical systems, trade refrigeration and kitchen plant and fittings, kitchen equipment, specialist computer equipment, prefabricated cold stores etc. subject to items 3.1, 3.2 and 3.3.</p>
            <p>2.12. Primary electrical installations including all cable, conduit, trunking, trays, plant, equipment and accessories, transformer substation installation, main and sub-main cable installation, control systems, switchgear and distribution boards, general lighting and power installation, lift installation, clocks and bell pushes, fire alarm installation, supplies to mechanical services plant, ventilation plant and air conditioning plant, interconnecting services, supplies to lifts, escalators and roller shutters, standby generator, attenuation, oil supply and wiring, energy control equipment and primary control systems even if computerized, wiring, smoke detector installations, etc. but excluding all trade electrical systems, security systems and alarms, kitchen equipment, sales counters, preparation areas, refrigeration areas, computer areas etc. subject to items 3.1, 3.2 and 3.3.</p>
            <p>2.13. Lifts including motive power plant. </p>
            <p>2.14. Escalators including motive power plant. </p>
            <p>2.15. Sprinkler installations complete including pumps. </p>
            <p>2.16. Hose reel installations complete. </p>
            <p>2.17. Bases, duct casings, pipe casings etc. access doors etc.  </p>
            <p>2.18. Roller shutters, fire shutters, security gates or the like.   </p>
        </td>
    </tr>

    <tr><td style="background-color: darkgrey" colspan="2"><h4>Excluded from Landlords </h4></td></tr>
    <tr><td class="detailtext" colspan="2">
            <p>1.1.	To exclude items of Fixtures and Fittings specific to the tenant's use which would or should be removed upon vacation of the premises. For example: furniture, kiosks, display equipment, panelling and trade cladding to sales floor and the like, restaurant trade decor, merchandise fixtures, security systems, air locked security lobbies etc., computer rooms and associated M&E and A/C installations, refrigeration cabinets and counters complete with cladding and equipment, prefabricated cold stores, refrigerators, cookers etc., lockers, sundry fittings, shelving etc. internal and external illuminated and non-illuminated trade signs, perimeter trade lighting, coverings complete, hand driers, free standing flour silos not forming part of the structure of the building and all other similar elements. </p>
            <p>1.2.	In consideration of the above it is important to note the degree of annexation that the fixture, fitting or elements have within the building.</p>
            <p>1.3.	In consideration of the above it is important to note the use of the building, i.e. whether that use is fundamental to the design and construction of that building and whether installation is likely to be removed or asked to be removed upon vacation by the tenant. </p>
            <p>1.4.	No allowance will be included in our figures for loss of rental income or for temporary payment of rental that you may incur on temporary accommodation during the period of reinstatement following either partial or complete destruction of the property. Separate provision should be made to cover this risk by means of a loss of notional rent if required. We would recommend that the appropriate period to be adopted is that which will facilitate the complete rebuilding of the premises.</p>
            <p>1.5.	We will not make any allowance for loss assessor fees relating to claim negotiation. We understand that most policies exclude recovery of such costs and client should check with their brokers to ascertain whether additional cover is required.</p>
            <p>
                1.6.	VAT is chargeable on professional fees and building works to new and existing premises. If the building owner is able to recover this, the VAT element of the sum can be adjusted accordingly. For the purposes of our calculation, VAT has been omitted.
            </p>
            <p>1.7.	This report will be provided for insurance reinstatement purposes only and does not contain any advice concerning the condition of the property or possible defects therein.</p>
            <p>1.8.	The assessment does not include allowances for cover in respect to other property insurances, such as plant and machinery within the buildings, occupiers' fitting outworks, contents, plate, glass and Third Party and Public Liability matters.</p>
        </td>
    </tr>

</table>
<br><br>

<br pagebreak="true" />
<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
    Appendix 7 – Major Assets / Equipment
</div>
<br><br>
<?php
$recostassets =\app\models\ReCostAssets::find()->where(['valuation_id' => $model->id])->all();
?>

<table cellpadding="5" border="1">
    <tr style="background-color: #ECEFF1">
        <th><h4>Serial Number</h4></th>
        <th><h4>Description</h4></th>
        <th><h4>Total Quantity</h4></th>
    </tr>

        <?php foreach ($recostassets as $key => $recostasset){
            ?>
    <tr>

        <td style="border: none"><?= ($key + 1); ?></td>
        <td  style="border: none"><?= $recostasset->name ?></td>
        <td  style="border: none"><?= $recostasset->quantity ?></td>
    </tr>
        <?php  } ?>


</table>

<br pagebreak="true" />
<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">
    Appendix 8 – Legal Fee Resources
</div>
<br>
<?php
$legal_fee_resources =\app\models\WindmillsDocs::find()->where(['file' => 'legal_fee_resources'])->orderBy(['created_at' => SORT_DESC])->one();
?>
<table cellspacing="1" cellpadding="8" class="main-class col-12">
    <tr><td class="detailtext" colspan="2"><?php if($legal_fee_resources<>null){ ?>
                <img src="<?= $legal_fee_resources->url ?>" height="500" width="500" >
            <?php } ?>
        </td></tr>
</table>

