<h2 style="color: #0a73bb">Contents</h2>
<p  style=" color:#E65100; text-align: center">.......... Executive Report Summary .........</p>
<p>1...................... Subject Property Overview</p>
<p>2...................... Valuer</p>
<p>3...................... Assumptions, Limitation And Considerations</p>
<p>4...................... Methodology</p>
<p>5...................... Construction Cost Outlook</p>
<p>6...................... Subject Property Description</p>
<p>7...................... Reinstatement Cost Assessment</p>
<p>8...................... Conclusion</p>
<p>Appendix 1 ............ Property Location Maps</p>
<p>Appendix 2 ............ Inspection Photos</p>
<p>Appendix 3 ............ Affection Plan and Floor Plan</p>
<p>Appendix 4 ............ Construction Cost Index</p>
<p>Appendix 5 ............ Interest Rate and Financing Schedule</p>
<p>Appendix 6 ............ Schedule of Items Typically Included/Excluded Within Reinstatement Cost Assessment</p>
<p>Appendix 7 ............ Major Assets / Equipment</p>
<p>Appendix 8 ............ Terms of Engagement</p>
<br pagebreak="true" />