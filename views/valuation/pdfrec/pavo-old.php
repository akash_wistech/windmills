
<?php

// This is for 1.05 Valuation Instructions From.
$instructorname=Yii::$app->PdfHelper->getUserInformation($model);

//1.14  Total Building Floor(s)
if($model->property->title == 'Villa') {
    $total_building_floors = Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];

}else{
    $total_building_floors = "Ground + ". $model->inspectProperty->full_building_floors ." Building floors";
}
$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();



$owners_in_valuation = \yii\helpers\ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $model->id])->all(), 'id', 'name');
$woners_name = "";
if(!empty($owners_in_valuation)) {
    $woners_name = implode(", ", $owners_in_valuation);
}

//  1.36  View Type
 $ViewType = Yii::$app->PdfHelper->getViewType($model);


 // 1.39 Building/Community Facilities
 $model->building->other_facilities = explode(',', ($model->building->other_facilities <> null) ? $model->building->other_facilities : "");
  foreach ($model->building->other_facilities as $key => $value) {
     $other_facilitie= \app\models\OtherFacilities::find()->where(['id'=>$value])->one();
       if ($other_facilities!=null) {  $other_facilities.=', '.$other_facilitie->title;  }
       else { $other_facilities.=$other_facilitie->title;   }
         }


         // 1.43, 1.44 , 1.45, 1.46  Floor Configuration
         $floorConfig=Yii::$app->PdfHelper->getFloorConfig($model);


         $floorConfig['groundFloortitle'] = rtrim($floorConfig['groundFloortitle'], "<br>");

         $floorConfig['firstFloortitle'] = rtrim($floorConfig['firstFloortitle'], "<br>");

         $floorConfig['secondFloortitle'] = rtrim($floorConfig['secondFloortitle'], "<br>");

         // 1.66 Documents Provided by Client , 1.67 Documents not Provided
         $DocumentByClient = Yii::$app->PdfHelper->getDocumentByClient($model);


              $DocumentByClient['documentAvail'] = rtrim($DocumentByClient['documentAvail'], "<br>");
              $DocumentByClient['documentNotavail'] = rtrim($DocumentByClient['documentNotavail'], "<br>");


         $makani_number = ($model->inspectProperty->makani_number > 0)? $model->inspectProperty->makani_number: 'Not Applicable';
      $source_bua =($model->inspectProperty->bua_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->bua_source] . ')': '';
      $source_plot_size =($model->inspectProperty->plot_area_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->plot_area_source]. ')': '';
      $source_extension =($model->inspectProperty->extension_source > 0)? ' ('.Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->inspectProperty->extension_source]. ')': '';

       $plot_size = ($model->land_size > 0)? $model->land_size.' square feet'.$source_plot_size: 'Not Applicable';


?>


  <br pagebreak="true" />

<div  style=" color:#E65100;
       text-align: center;
       font-size:18px;
       font-weight:bold;
       border: 1px solid #64B5F6;">

          1.VALUATION OVERVIEW

</div>
<br>
<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;">

  <tr><td class="tableSecondHeading" colspan="2"><h3>General Details</h3></td></tr>


      <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.01 Windmills Reference</td>
        <td style="color:#212121; font-size:12;"><?= $model->reference_number ?></td>
      </tr>
      <tr >
      <td  style="color:#0277BD; font-size:13;">1.02 Client Reference </td>
      <td  style="color:#212121; font-size:12;"><?= $model->client_reference ?></td>
      </tr>
      <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.03 Client’s Full Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->client->title ?></td>
      </tr>
      <tr >
      <td  style="color:#0277BD; font-size:13;">1.04 Intended User(s) Name</td>
      <td  style="color:#212121; font-size:12;">None</td>
      </tr>

      <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.05 Valuation Instructions From</td>
        <td  style="color:#212121; font-size:12;"><?= $instructorname ?></td>
      </tr>
      <tr >
      <td  style="color:#0277BD; font-size:13;">1.06 Valuation Instructions Date</td>
      <td  style="color:#212121; font-size:12;"><?=  date('l, jS \of F, Y', strtotime($model->instruction_date)) ?></td>
      </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.07 Valuation Scope</td>
        <td  style="color:#212121; font-size:12;"><?=  Yii::$app->appHelperFunctions->valuationScopeArr[$model->valuation_scope] ?></td>
    </tr>
      <tr >
      <td  style="color:#0277BD; font-size:13;">1.08 Owner’s Full Name</td>
      <td  style="color:#212121; font-size:12;"><?= $woners_name ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.09 Client’s Customer Full Name</td>
      <td  style="color:#212121; font-size:12;"><?= $model->client_name_passport ?></td>
      </tr >
      <tr >
      <td  style="color:#0277BD; font-size:13;">1.10 Transaction Price</td>
      <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->transaction_price <> null && $model->costDetails->transaction_price != "0.00")? "AED ".number_format($model->costDetails->transaction_price) : "Not known" ?></td>
      </tr>
    <?php if($model->costDetails->transaction_price <> null && $model->costDetails->transaction_price != "0.00") { ?>
        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.11 Transaction Price Rate</td>
            <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->transaction_price <> null && $model->costDetails->transaction_price != "0.00" )? "AED ".number_format((float)($model->costDetails->transaction_price/$model->inspectProperty->net_built_up_area), 2, ',', '') ." per square feet" : "Not known"?></td>
        </tr>

        <tr >
            <td  style="color:#0277BD; font-size:13;">1.12 Transaction Price Date</td>
            <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->transaction_price_date <> null)? date('l, jS \of F, Y', strtotime($model->costDetails->transaction_price_date)): "Not known" ?></td>
        </tr>

        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.13 Source of Transaction Price</td>
            <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->transaction_source <> null) ? Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->costDetails->transaction_source] : "Not known"  ?> </td>
        </tr>
    <?php  }else{ ?>

        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.11 Transaction Price Rate</td>
            <td  style="color:#212121; font-size:12;">Not known</td>
        </tr>
        <tr >
            <td  style="color:#0277BD; font-size:13;">1.12 Transaction Price Date</td>
            <td  style="color:#212121; font-size:12;">Not known</td>
        </tr>


        <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.13 Source of Transaction Price</td>
            <td  style="color:#212121; font-size:12;">Not known </td>
        </tr>
    <?php } ?>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.14 Original Purchase Price</td>
        <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->original_purchase_price <> null && $model->costDetails->original_purchase_price != "0.00")? "AED ".number_format($model->costDetails->original_purchase_price) : "Not known" ?></td>
    </tr>

    <?php if($model->costDetails->original_purchase_price <> null && $model->costDetails->original_purchase_price != "0.00") { ?>

    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.15 Original Purchase Price Rate</td>
        <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->original_purchase_price <> null && $model->costDetails->original_purchase_price != "0.00") ? "AED ".number_format((float)($model->costDetails->original_purchase_price/$model->inspectProperty->net_built_up_area), 2, '.', '')." per square feet" : "Not known" ?>  </td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.16 Source of Original Purchase Price</td>
        <td  style="color:#212121; font-size:12;"><?=  ($model->costDetails->source_of_original_date_price <> null) ? Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$model->costDetails->source_of_original_date_price] : "Not known" ?></td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.17 Date of Original Purchase Price</td>
        <td  style="color:#212121; font-size:12;"><?= ($model->costDetails->date_of_original_date_price <> null) ? date('l, jS \of F, Y', strtotime($model->costDetails->date_of_original_date_price)) : "Not known"?> </td>
    </tr>
    <?php  }else{ ?>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.15 Original Purchase Price Rate</td>
        <td  style="color:#212121; font-size:12;">Not known  </td>
    </tr>
    <tr >
        <td  style="color:#0277BD; font-size:13;">1.16 Source of Original Purchase Price</td>
        <td  style="color:#212121; font-size:12;">Not known</td>
    </tr>
    <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.17 Date of Original Purchase Price</td>
        <td  style="color:#212121; font-size:12;">Not known </td>
    </tr>
    <?php  } ?>
</table>

  <br pagebreak="true" />

<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;">

  <tr><td class="tableSecondHeading" colspan="2"><h3>Property Description - Location</h3></td></tr>

      <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.18 Property (Interest) Valued</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].' '.$model->property->title ?></td>
      </tr>
      <tr>
      <td  style="color:#0277BD; font-size:13;">1.19 Property Use</td>
      <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category] ?></td>
      </tr>
      <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.20 Total Building Floor(s)</td>
        <td  style="color:#212121; font-size:12;"><?= $total_building_floors ?></td>
      </tr>
      <tr>
      <td  style="color:#0277BD; font-size:13;">1.21 Floor Number(s)</td>
      <td  style="color:#212121; font-size:12;"><?= $model->floor_number ?></td>
      </tr>

      <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.22 Property Unit Number</td>
        <td  style="color:#212121; font-size:12;"><?= $model->unit_number ?></td>
      </tr>
      <tr>
      <td  style="color:#0277BD; font-size:13;">1.23 Building Number/Name</td>
      <td  style="color:#212121; font-size:12;"><?= $model->building->title ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.24 Street Number/Name</td>
      <td  style="color:#212121; font-size:12;"><?= $model->street ?></td>
      </tr>
      <tr>
        <td style="color:#0277BD; font-size:13;">1.25 Plot Number</td>
        <td style="color:#212121; font-size:12;"><?= (($model->plot_number == 0)? "Not Applicable":$model->plot_number) ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.26 Municipality’ Makani Number</td>
      <td  style="color:#212121; font-size:12;"><?= $makani_number?></td>
      </tr>
      <tr>
        <td  style="color:#0277BD; font-size:13;">1.27 Project/Sub Community Name</td>
        <td  style="color:#212121; font-size:12;"><?= $model->building->subCommunities->title ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.28 Community Name</td>
      <td  style="color:#212121; font-size:12;"><?= $model->building->communities->title ?></td>
      </tr>

      <tr>
        <td  style="color:#0277BD; font-size:13;">1.29 City and Country Name</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>, UAE</td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.30 Location Characteristics</td>
      <td  style="color:#212121; font-size:12;"><b>Good location</b>
      <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_highway_drive) ?> drive to highway
      <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_school_drive) ?> to school
      <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_mall_drive) ?> to commercial mall
      <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_sea_drive) ?> to special landmark
      <br><?= Yii::$app->PdfHelper->strReplace($model->inspectProperty->location_park_drive) ?> drive to pool/park
      </td>
      </tr>
      <tr>
      <td  style="color:#0277BD; font-size:13;">1.31 Location Coordinates</td>
      <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->latitude.','.$model->inspectProperty->longitude ?>(as per Google Maps)</td>
      </tr>

      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.32 Placement (Middle / Corner) </td>
      <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->propertyPlacementListArr[$model->inspectProperty->property_placement] ?></td>
      </tr>
      <tr>
      <td  style="color:#0277BD; font-size:13;">1.33 Exposure (Single Row / Back)</td>
      <td  style="color:#212121; font-size:12;">
      <?= Yii::$app->appHelperFunctions->propertyExposureListArr[$model->inspectProperty->property_exposure] ?>
      </td>
      </tr>
</table>


<br pagebreak="true" />
<table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;">

  <tr><td class="tableSecondHeading" colspan="2"><h3>Property Description - External</h3></td></tr>

      <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.34  Property Type (1E, 2M etc.)</td>
        <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->listingsPropertyTypeListArr[$model->inspectProperty->listing_property_type] ?></td>
      </tr>
      <tr>
      <td  style="color:#0277BD; font-size:13;">1.35 Development (Standard/Non)</td>
      <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->development_type ?></td>
      </tr>
      <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.36 Tenure (FH/NFH/Leasehold)</td>
        <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure] ?></td>
      </tr>
      <tr>
      <td  style="color:#0277BD; font-size:13;">1.37 Completion Percentage</td>
      <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->completion_status ?></td>
      </tr>

      <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.38 Estimate Age (in years)</td>
        <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->estimated_age.'Years' ?></td>
      </tr>
      <tr>
      <td  style="color:#0277BD; font-size:13;">1.39 Estimated Remaining Life (in years)</td>
      <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->estimated_remaining_life ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.40  Plot Size (in square feet)</td>
      <td  style="color:#212121; font-size:12;"><?= $plot_size ?></td>
      </tr>


      <tr>
        <td style="color:#0277BD; font-size:13;">1.41 Built Up Area (in square feet)</td>
        <td style="color:#212121; font-size:12;"><?= $model->inspectProperty->net_built_up_area ?>square feet <?= $source_bua ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.42 View Type</td>
      <td  style="color:#212121; font-size:12;"><?= $ViewType['viewCommunity'].$ViewType['viewPool'].$ViewType['viewBurj'].$ViewType['viewSea'].$ViewType['viewMarina'].$ViewType['viewLake'].$ViewType['viewGolfCourse'].$ViewType['viewPark'].$ViewType['viewSpecial'] ?></td>
      </tr>
      <tr>
        <td  style="color:#0277BD; font-size:13;">1.43 Landscaping Details</td>
        <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->landscaping ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.44 Parking Spaces</td>
      <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->parking_floors ?></td>
      </tr>

      <tr>
        <td  style="color:#0277BD; font-size:13;">1.45  Building/Community Facilities</td>
        <td  style="color:#212121; font-size:12;"><?= $other_facilities ?></td>
      </tr>

      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.46 Developer’s Full Name</td>
      <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->developer->title ?></td>
      </tr>

</table>


  <br pagebreak="true" />
<table cellspacing="1" cellpadding="6" style="border: 1px dashed #64B5F6;">

  <tr><td class="tableSecondHeading" colspan="2"><h3>Property Description – Internal</h3></td></tr>


      <tr>
        <td style="color:#0277BD; font-size:13;">1.47 Accommodation</td>
        <td style="color:#212121; font-size:12;"><?= $model->inspectProperty->no_of_bedrooms ?> Bedrooms</td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.48 Basement Floor Configuration</td>
      <td  style="color:#212121; font-size:12;">Not Applicable </td>
      </tr>

      <?php
      $property='Ground';

      if($model->property_id==1)
      {
        $property='Unit';
      }

       ?>


      <tr>
        <td  style="color:#0277BD; font-size:13;">1.49 <?= $property ?> Floor Configuration</td>
        <td  style="color:#212121; font-size:12;"><?= $floorConfig['groundFloortitle'] ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.50 First Floor Configuration</td>
      <td  style="color:#212121; font-size:12;"><?= $floorConfig['firstFloortitle'] ?></td>
      </tr>
      <tr>
        <td  style="color:#0277BD; font-size:13;">1.51 Second Floor Configuration</td>
        <td  style="color:#212121; font-size:12;"><?= $floorConfig['secondFloortitle'] ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.52 Upgrade Details</td>
      <td  style="color:#212121; font-size:12;"><?= $floorConfig['upgrade_text'] ?></td>
      </tr>
      <tr>
      <td  style="color:#0277BD; font-size:13;">1.53 Extensions Details</td>
      <td  style="color:#212121; font-size:12;"><?= ($model->inspectProperty->extension <> null)? $model->inspectProperty->extension.$source_extension:"None" ?></td>
      </tr>


      <tr class="bggray">
        <td style="color:#0277BD; font-size:13;">1.54 Property Condition/Quality</td>
        <td style="color:#212121; font-size:12;">Good. No material defects were noticed during inspection.</td>
      </tr>
      <tr >
      <td  style="color:#0277BD; font-size:13;">1.55 Furnished</td>
      <td  style="color:#212121; font-size:12;"><?=  Yii::$app->appHelperFunctions->reportAttributesValuefurnished[$model->inspectProperty->furnished]   ?></td>
      </tr>
      <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.56 Swimming Pool </td>
        <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->pool ?></td>
      </tr>
      <tr >
      <td  style="color:#0277BD; font-size:13;">1.57 Cooker Installed</td>
      <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->cooker] ?></td>
      </tr>

      <tr class="bggray">
        <td  style="color:#0277BD; font-size:13;">1.58 Oven Installed</td>
        <td  style="color:#212121; font-size:12;"><?=  Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->oven] ?></td>
      </tr>
      <tr >
      <td  style="color:#0277BD; font-size:13;">1.59 Fridge Installed</td>
      <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->fridge]  ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.60 Washing Machine Installed</td>
      <td  style="color:#212121; font-size:12;"><?=  Yii::$app->appHelperFunctions->reportAttributesValueOther[$model->inspectProperty->washing_machine] ?></td>
      </tr>
  <?php
  $var=Yii::$app->appHelperFunctions->acTypesArr[$model->inspectProperty->ac_type];
  $arr =['Split Units AC'=>'No','Window AC'=>'No','Central Chiller'=>'No'];
  $arr[$var]='Yes';
  ?>
      <tr >
      <td  style="color:#0277BD; font-size:13;">1.61 Central Air-conditioning</td>
      <td  style="color:#212121; font-size:12;"><?= $arr['Central Chiller'] ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.62 Split Air-conditioning Units</td>
      <td  style="color:#212121; font-size:12;"><?= $arr['Split Units AC'] ?></td>
      </tr>
      <tr>
      <td  style="color:#0277BD; font-size:13;">1.63 Window Air-conditioning Unit</td>
      <td  style="color:#212121; font-size:12;"><?= $arr['Window AC'] ?></td>
      </tr>
      <tr class="bggray">
      <td  style="color:#0277BD; font-size:13;">1.64 Utilities connected</td>
      <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->utilities_connected ?></td>
      </tr>
      <tr>
      <td  style="color:#0277BD; font-size:13;">1.65 Tenancies / Occupancy Status</td>
      <td  style="color:#212121; font-size:12;"><?= $model->inspectProperty->occupancy_status ?></td>
      </tr>
</table>


  <br pagebreak="true" />
    <table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;">
      <tr><td class="tableSecondHeading" colspan="2"><h3>Valuation Details</h3></td></tr>


          <tr class="bggray">
            <td style="color:#0277BD; font-size:13;">1.66 Inspection Type Instructed</td>
            <td style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->inspectionTypeArr[$model->scheduleInspection->inspection_type] ?></td>
          </tr>
          <tr >
          <td  style="color:#0277BD; font-size:13;">1.67 Purpose of the Valuation</td>
          <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation] ?></td>
          </tr>
          <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.68 Inspecting Officer</td>
            <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->scheduleInspection->inspection_officer] ?></td>
          </tr>
          <tr >
          <td  style="color:#0277BD; font-size:13;">1.69 Service Officer</td>
          <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->service_officer_name] ?></td>
          </tr>

          <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.70 Inspection Date</td>
            <td  style="color:#212121; font-size:12;"><?= Yii::$app->formatter->asDate($model->scheduleInspection->inspection_date,Yii::$app->params['fulldaydate']) ?></td>
          </tr>
          <tr >

          <td  style="color:#0277BD; font-size:13;">1.71 Valuation Date</td>
          <td  style="color:#212121; font-size:12;"><?= Yii::$app->formatter->asDate($model->scheduleInspection->valuation_date,Yii::$app->params['fulldaydate']) ?></td>
          </tr>
          <tr class="bggray">
          <td  style="color:#0277BD; font-size:13;">1.72 Documents Provided by Client</td>
          <td  style="color:#212121; font-size:12;"><?= $DocumentByClient['documentAvail'] ?></td>
          </tr>

          <tr >
            <td style="color:#0277BD; font-size:13;">1.73 Documents not Provided </td>
            <td style="color:#212121; font-size:12;"><?= $DocumentByClient['documentNotavail'] ?></td>
          </tr>
          <tr class="bggray">
          <td  style="color:#0277BD; font-size:13;">1.74 Basis of Value</td>
          <td  style="color:#212121; font-size:12;"><?= $model->property->basis_of_value ?></td>
          </tr>
          <tr>
            <td  style="color:#0277BD; font-size:13;">1.75 Valuation Approach</td>
            <td  style="color:#212121; font-size:12;"><?= Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach] ?></td>
          </tr>
          <tr class="bggray">
          <td  style="color:#0277BD; font-size:13;">1.76 Approach Reasoning</td>
          <td  style="color:#212121; font-size:12;"><?= $model->property->approach_reason ?></td>
          </tr>

          <tr>
            <td  style="color:#0277BD; font-size:13;">1.77 Valuation Adjustments</td>
            <td  style="color:#212121; font-size:12;">Size, Floor, Quality, Row, Placement, Listing Date and Sales Negotiation Discount.</td>
          </tr>
    </table>
      <br pagebreak="true" />
      <table cellspacing="1" cellpadding="8" style="border: 1px dashed #64B5F6;">
        <tr><td class="tableSecondHeading" colspan="2"><h3>Market Value</h3></td></tr>
            <tr>
              <td style="color:#0277BD; font-size:13;">1.78 Market Value</td>
              <td style="color:#212121; font-size:12;">AED <?= $estimate_price_byapprover ?></td>
            </tr>
            <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.79 Market Value Rate</td>
            <td  style="color:#212121; font-size:12;">AED <?= number_format($approver_data->estimated_market_value_sqf); ?> per square feet</td>
            </tr>
            <tr>
              <td  style="color:#0277BD; font-size:13;">1.80 Market Rent</td>
              <td  style="color:#212121; font-size:12;">AED <?= number_format($approver_data->estimated_market_rent); ?> per annum</td>
            </tr>
            <?php
            $special_assumptions='';
            //special Assumptions Occupancy, Tananted, Vacant, Acquisition
          //  $special_assumptions.= '<br><b>Occupancy Status</b><br>';
$special_assumptionreport = \app\models\SpecialAssumptionreport::find()->one();


            if($model->inspectProperty->occupancy_status == "Owner Occupied"){
                $special_assumptions.='<br>'.$special_assumptionreport->owner_occupied;

            }
            if($model->inspectProperty->occupancy_status == "Tenanted"){
                $special_assumptions.= '<br>'.$special_assumptionreport->owner_occupied;
            }

            if($model->inspectProperty->occupancy_status == "Vacant"){
                $special_assumptions.= '<br>'.$special_assumptionreport->vacant;
            }
            if($model->inspectProperty->acquisition_method == 1 || $model->inspectProperty->acquisition_method == 2 ){
                $special_assumptions.= '<br>'.$special_assumptionreport->gifted_granted;
            }else{
                $special_assumptions.= '<br>'.$special_assumptionreport->purchased;

            }
            ?>
            <tr class="bggray">
            <td  style="color:#0277BD; font-size:13;">1.81 Special Assumptions & Concerns</td>
            <td  style="color:#212121; font-size:12;"><?= $special_assumptions ?></td>
            </tr>
      </table>
