<?php

$approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();

$ValuerName= \app\models\User::find()->select(['firstname', 'lastname'])->where(['id'=>$approver_data->created_by])->one();

?>
<br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br>
<div><table style="font-size:10px; padding-top: 1px;">
        <tr style="">
        <td colspan="2" class="tdbold " style="padding-top: 50px"><h4> Property</h4></td>
        <td colspan="3" style="padding-top: 50px"><?= Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category].' '.$model->property->title ?></td>
      </tr>
      <tr>
      <td colspan="2"  class="tdbold "><h4> Property Address</h4></td>
      <td colspan="3">Unit Number <?= $model->unit_number.', '.$model->building->title ?></td>
      </tr>
      <tr>
        <td colspan="2"  class="tdbold "></td>
         <td colspan="3">Plot Number <?= $model->plot_number.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?></td>
         </tr>
      <tr>
        <td colspan="2"  class="tdbold "><h4> Client Reference No</h4></td>
        <td colspan="3"><?= $model->client_reference ?></td>
      </tr>
      <tr>
      <td colspan="2"  class="tdbold "><h4> Windmills Reference</h4></td>
      <td colspan="3"><?= $model->reference_number ?></td>
      </tr>
      <tr>
        <td colspan="2"  class="tdbold "><h4> Client Name</h4></td>
        <td colspan="3"><?= $model->client->title ?></td>
      </tr>

    <tr>
        <td colspan="2"  class="tdbold "><h4> Valuer Name</h4></td>
        <td colspan="3"><?= $ValuerName['firstname'].' '.$ValuerName['lastname']?></td>
    </tr>
    <tr>
        <td colspan="2"  class="tdbold "><h4> Service Officer Name</h4></td>
        <td colspan="3"><?= Yii::$app->appHelperFunctions->staffMemberListArr[$model->service_officer_name] ?></td>
    </tr>
      <tr>
      <td colspan="2"  class="tdbold "><h4> Valuation Report Date</h4></td>
      <!--<td colspan="3"><?/*= Yii::$app->formatter->asDate($model->scheduleInspection->valuation_date) */?></td>-->
      <td colspan="3"><?= ($model->scheduleInspection->valuation_report_date <> null) ? date('l, jS \of F, Y', strtotime($model->scheduleInspection->valuation_report_date)) : '' ?></td>
      </tr>
</table>
</div>

