<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\DateRangePickerAsset2;
DateRangePickerAsset2::register($this);


/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', 'Valuations');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) {
    $actionBtns .= '{step_1}';
}
// if (Yii::$app->menuHelperFunction->checkActionAllowed('cancel_valuation')) {
    $cancelBtns = '{cancel_valuation}';
// }


if (Yii::$app->menuHelperFunction->checkActionAllowed('cancel_permission')) {
    $updateToCancel = '{update_to_cancel}';
}
$scanOfficer=User::find()->where(['id'=>Yii::$app->user->identity->id, 'permission_group_id'=>11])->select(['id'])->one();
if ($scanOfficer['id'] !=null) {
    $step_24 = '{step_24}';
}



$this->registerJs('

$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
var url=_this.attr("data-url");
var alerttext=_this.attr("data-text");
e.preventDefault();
swal({
title: alerttext,
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
     if (result) {
  $.ajax({
  url: url,
  dataType: "html",
  type: "POST",
  });
    }
});
});


$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});

$(".change_pageSize").on("change", function() {

  $("form").submit();

});


    ');

// echo "<pre>";
// print_r($dataProvider);
// echo "</pre>";
// die();

?>




<div class="position-relative" style="height:60px;">
<!-- Button trigger modal -->

  <button type="button" class="btn btn-warning position-absolute my-3 text-white font-weight-bold" style="right:10px; " data-toggle="modal" data-target="#exampleModal">
    Search Criteria
  </button>

  <?php $form = ActiveForm::begin([
      'action' => ['report-all'],
      'method' => 'get',
  ]); ?>

  <div class="form-group my-3 position-absolute" style="left:7px; width:100px;">
    <?php //echo Html::label('Page Size', ['No of Records' => 'form-control']) ?>
    <?= Html::activeDropDownList($searchModel, 'pageSize',[1=>'20',2=>'50',3=>'100',4=>'Show All'],['class'=>'form-control change_pageSize']); ?>
</div>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Search Valuation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">

        <div class="row px-3">
          <div class="col-sm-6">
            <?php
            echo $form->field($searchModel, 'reference_number')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->referenceListArr,
                'options' => ['placeholder' => 'Select Reference Number ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
          </div>
            <div class="col-sm-6">
                <?php
                echo $form->field($searchModel, 'property_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select Property'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <?php if ($user==null) { ?>
          <div class="col-sm-6">
              <?php
              echo $form->field($searchModel, 'client_id')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(\app\models\Company::find()->where(['data_type'=>0])->orWhere(['data_type'=>null])->orderBy([
                      'title' => SORT_ASC,
                  ])->all(), 'id', 'title'),
                  'options' => ['placeholder' => 'Select a Officer ...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);
              ?>
          </div>
                <div class="col-sm-6">
                    <?php
                    echo $form->field($searchModel, 'valuer')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->andWhere(['status'=>1])->andWhere(['trashed'=>0])->andWhere(['permission_group_id'=>[3,5,7,8,9,10,12]])->orderBy([
                            'firstname' => SORT_ASC,
                        ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];}),
                        'options' => ['placeholder' => 'Select Valuer ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
            <?php } ?>
        </div>
        <div class="row px-3">
          <div class="col">
            <?php
            echo $form->field($searchModel, 'building_info')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->buildingListArr,
                'options' => ['placeholder' => 'Select Building ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Building/Project');
            ?>
          </div>

          <div class="col">
            <?php
            echo $form->field($searchModel, 'client_revenue')->label('Client Revenue');
            ?>
          </div>
        </div>

        <div class="row px-3">
          <div class="col">
            <?php
            echo $form->field($searchModel, 'community')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->communitiesListArr,
                'options' => ['placeholder' => 'Select Community ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
          </div>

          <div class="col">
            <?php
            echo $form->field($searchModel, 'city')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->emiratedListArr,
                'options' => ['placeholder' => 'Select City ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
          </div>
        </div>

        <div class="row px-3">
        <div class="col">
          <?= $form->field($searchModel, 'instruction_date_btw')->textInput(['class'=>'form-control div1'])->label('Instruction Date'); ?>
        </div>
        <div class="col">
          <?= $form->field($searchModel, 'target_date_btw')->textInput(['class'=>'form-control div1'])->label('Target Date'); ?>
        </div>
        </div>

        <div class="row px-3">

          <div class="col">
            <?= $form->field($searchModel, 'inspection_date_btw')->textInput(['class'=>'form-control div1'])->label('Valuation Report Date'); ?>
          </div>

            <div class="col">
                <?php
                echo $form->field($searchModel, 'valuation_status')->widget(Select2::classname(), [
                    'data' => Yii::$app->helperFunctions->valuationStatusListArr,
                    'options' => ['placeholder' => 'Select Valuation Status ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Valuation Status');
                ?>
            </div>
        </div>

        <div class="row px-3">

      <!--  <div class="col">
          <?php
/*          echo $form->field($searchModel, 'status')->widget(Select2::classname(), [
              'data' => Yii::$app->helperFunctions->arrFilterStatus,
              'options' => ['placeholder' => 'Select Status ...'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
          ])->label('Status');
          */?>
        </div>-->
      </div>
      
      
        <div class="row px-3">
            <div class="col-6">
                <?php
                echo $form->field($searchModel, 'client_invoice_type')->widget(\kartik\select2\Select2::classname(), [
                    'data' => array('0' => 'Client Name', '1' => 'Customer Name'),
                    'options' => ['placeholder' => 'Select'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Client Invoice Name');
                ?>
            </div>
        </div>


      </div>
      <div class="modal-footer">
          <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary ']) ?>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>

<div class="valuation-index col-12">



    <?php CustomPjax::begin(['id' => 'grid-container1']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
       // 'createBtn' => $createBtn,
        'showFooter' => true,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'reference_number', 'label' => Yii::t('app', 'Reference')],
            ['attribute' => 'id',
                'label' => Yii::t('app', 'QuickBooks INV'),
                'value' => function ($model) {
        $inv_number = '';
        if($model->id > 4651 ){
            $inv_number =  Yii::$app->appHelperFunctions->getInvoiceNumber($model);
        }
                    // return date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
                    return $inv_number;
                },
            ],
            ['attribute' => 'client_id',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                    return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'client_reference',
                'label' => Yii::t('app', 'Client Reference'),
                'value' => function ($model) {
                    return $model->client_reference;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'client_name_passport',
                'label' => Yii::t('app', 'Customer Full name'),
                'value' => function ($model) {
                    return $model->client_name_passport;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title;
                },
                'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Building'),
                'value' => function ($model) {
                    return $model->building->title;
                },
                'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Community'),
                'value' => function ($model) {
                    return $model->building->communities->title;
                },
                // 'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                //     'title' => SORT_ASC,
                // ])->all(), 'id', 'community')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'City'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];

                },
                'filter' => Yii::$app->appHelperFunctions->emiratedListArr
            ],

            ['attribute' => 'instruction_date',
                'label' => Yii::t('app', 'Instruction Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->instruction_date));
                },
            ],
            ['attribute' => 'id',
                'label' => Yii::t('app', 'Valuation Report Date'),
                'value' => function ($model) {
                   // return date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
                    return date('d-m-Y', strtotime($model->submission_approver_date));
                },
            ],
            ['attribute' => 'inspection_type',
                'label' => Yii::t('app', 'Inspection Type'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type] ;
                    // return date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
                   // return date('d-m-Y', strtotime($model->instruction_date));
                },
            ],
            ['attribute' => 'id',
                'label' => Yii::t('app', 'Fee'),
                'value' => function ($model) {
                  /*if (Yii::$app->appHelperFunctions->getClientRevenue($model->id)!=null) {
                    $total+=(int)Yii::$app->appHelperFunctions->getClientRevenue($model->id);
                  }*/
                  return $model->total_fee;
                  //  return Yii::$app->appHelperFunctions->getClientRevenue($model->id);
                },
               // 'footer' => '<b>Fee Total</b> '.\app\models\Valuation::getTotal($dataProvider->models, 'fee'),
            ],
            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
                },
                'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
                    'firstname' => SORT_ASC,
                ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];})
            ],
            [
                'attribute' => 'quotation_id',
                //'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return $model->quotation_id;
                }
            ],
            [
                'attribute' => 'tenure',
                //'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure];
                }
            ],

            [
                'attribute' => 'id',
                'label' => Yii::t('app', 'Market Value'),
                'value' => function ($model) {
                    return (isset($model->approverData) && ($model->approverData <> null))? (number_format($model->approverData->estimated_market_value)): '';
                },
            ],
          /*  [
                'attribute' => 'taqyeem_number',
                'label' => Yii::t('app', 'Taqyeem Number'),
                'value' => function ($model) {
                    return (isset($model->taqyeem_number) && ($model->taqyeem_number <> null))? $model->taqyeem_number: '';
                },
            ],*/
            ['format' => 'raw',
              'attribute' => 'valuation_status',
                'label' => Yii::t('app', 'Valuation Status'),
                'value' => function ($model) {
                    return Yii::$app->helperFunctions->valuationStatusListArrLabel[$model->valuation_status];
                },
                'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
        ],

    ]);
?>
    <?php CustomPjax::end(); ?>


</div>

<?php



$this->registerJs('

');
 ?>

 <script>


 </script>
