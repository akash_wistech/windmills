<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

// use app\assets\DateRangePickerAsset2;
// DateRangePickerAsset2::register($this);

// use app\assets\SortAsset;
// SortAsset::register($this);

// use app\assets\GlobalAsset;
// GlobalAsset::register($this);

$asset = in_array(Yii::$app->user->id, $allowedId) ? 'app\assets\GlobalAsset' : 'app\assets\DateRangePickerAsset2';
$asset::register($this);



/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// print_r(Yii::$app->appHelperFunctions->valuerListArr);
// die();

$this->title = Yii::t('app', $page_title);
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) {
    $actionBtns .= '{step_1}';
}
// if (Yii::$app->menuHelperFunction->checkActionAllowed('cancel_valuation')) {
    $cancelBtns = '{cancel_valuation}';
// }


if (Yii::$app->menuHelperFunction->checkActionAllowed('cancel_permission')) {
    $updateToCancel = '{update_to_cancel}';
}
$scanOfficer=User::find()->where(['id'=>Yii::$app->user->identity->id, 'permission_group_id'=>11])->select(['id'])->one();
if ($scanOfficer['id'] !=null) {
    $step_24 = '{step_24}';
}



$this->registerJs('

$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
var url=_this.attr("data-url");
var alerttext=_this.attr("data-text");
e.preventDefault();
swal({
title: alerttext,
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
     if (result) {
  $.ajax({
  url: url,
  dataType: "html",
  type: "POST",
  });
    }
});
});


$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});

$(".change_pageSize").on("change", function() {
  $("form").submit();
});


$(\'#valuationsearch-time_period\').on(\'change\',function(){
    var period = $(this).val();
  
    if(period == 9){
        $(\'#date_range_array\').show();
    }else{
        $(\'#date_range_array\').hide();
    }
});


    
    ');

// echo "<pre>";
// print_r(Yii::$app->user->id);
// echo "</pre>";
// die();



$target = null;
    if(isset($request) && $request['ValuationSearch']['target']<>null){
        $target = $request['ValuationSearch']['target'];
    }
?>
<style>
    .search_filter{
        padding: 5px !important;
        padding-top: 8px !important;
        padding-bottom: 0px !important;

    }
</style>
<?php
// dd(Yii::$app->user->id);
    if(in_array(Yii::$app->user->id, $allowedId)){ 

        $form = ActiveForm::begin([
            'action' => ['dashboard-report-all'],
            'method' => 'get',
        ]);
?>

<div class="card card-outline card-warning mx-2">
    <div class="card-body search_filter">
        <div class="row">
            <!-- <div class="col-3 d-none">
            <?= $form->field($searchModel, 'target')->hiddenInput(['value' => $target])->label(false); ?> 
            </div>

            <div class="col-sm-4">
            <?php
            echo $form->field($searchModel, 'time_period')->widget(Select2::classname(), [
                'data' => Yii:: $app->appHelperFunctions->reportPeriodNew,
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(false);
            ?>
            </div>
            <div class="col-sm-3" id="date_range_array" <?php if($searchModel->time_period != 9){ echo 'style="display:none;"';} ?>>
                <?= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label(false); ?>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div> -->

            <div class="col-3">
                <div class="form-group">
                    <label> General Search : </label>
                    <input type="search" id="custom-search-input-text" placeholder="Search from datatable" class="form-control custom-search-input-text">
                </div>
            </div>
        </div>

       <!-- <div class="row" id="date_range_array" <?php /*if($searchModel->time_period != 9){ echo 'style="display:none;"';} */?>>
            <div class="col-sm-4"></div>
            <div class="col-sm-4" id="date_range_array" <?php /*if($searchModel->time_period != 9){ echo 'style="display:none;"';} */?>>
                <?/*= $form->field($searchModel, 'custom_date_btw')->textInput(['class'=>'form-control div1'])->label('Select Custom Date Range'); */?>
            </div>
            <div class="col-sm-4"></div>
        </div>

        <div class="text-center">
            <div class="form-group">
                <?/*= Html::submitButton('Search', ['class' => 'btn btn-primary']) */?>
            </div>
        </div>-->
    </div>
</div>



        


<?php
        ActiveForm::end();
}else{
 ?>
    <div class="position-relative" style="height:60px;">
        <button type="button" class="btn btn-warning position-absolute my-3 text-white font-weight-bold"
            style="right:10px; " data-toggle="modal" data-target="#exampleModal">
            Search Criteria
        </button>

        <?php $form = ActiveForm::begin([
            'action' => ['dashboard-report-all'],
            'method' => 'get',
        ]); ?>

        <div class="form-group my-3 position-absolute" style="left:7px; width:100px;">
            <?php //echo Html::label('Page Size', ['No of Records' => 'form-control']) ?>
            <?= Html::activeDropDownList($searchModel, 'pageSize',[1=>'20',2=>'50',3=>'100',4=>'Show All'],['class'=>'form-control change_pageSize']); ?>
        </div>

    </div> 

    
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Search Valuation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body ">

                        <div class="row px-3">
                            <div class="col-sm-6">
                                <?php
            echo $form->field($searchModel, 'reference_number')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->referenceListArr,
                'options' => ['placeholder' => 'Select Reference Number ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
                            </div>
                            <div class="col-sm-6">
                                <?php
                echo $form->field($searchModel, 'property_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                        'title' => SORT_ASC,
                    ])->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Select Property'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
                            </div>
                            <?php if ($user==null) { ?>
                            <div class="col-sm-6">
                                <?php
              echo $form->field($searchModel, 'client_id')->widget(Select2::classname(), [
                  'data' => Yii::$app->appHelperFunctions->clientListArr,
                  'options' => ['placeholder' => 'Select a Officer ...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);
              ?>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="row px-3">
                            <div class="col">
                                <?php
            echo $form->field($searchModel, 'building_info')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->buildingListArr,
                'options' => ['placeholder' => 'Select Building ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Building/Project');
            ?>
                            </div>

                            <div class="col">
                                <?php
            echo $form->field($searchModel, 'client_revenue')->label('Client Revenue');
            ?>
                            </div>
                        </div>

                        <div class="row px-3">
                            <div class="col">
                                <?php
            echo $form->field($searchModel, 'community')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->communitiesListArr,
                'options' => ['placeholder' => 'Select Community ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
                            </div>

                            <div class="col">
                                <?php
            echo $form->field($searchModel, 'city')->widget(Select2::classname(), [
                'data' => Yii::$app->appHelperFunctions->emiratedListArr,
                'options' => ['placeholder' => 'Select City ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
                            </div>
                        </div>

                        <div class="row px-3">
                            <div class="col">
                                <?= $form->field($searchModel, 'instruction_date_btw')->textInput(['class'=>'form-control div1'])->label('Instruction Date'); ?>
                            </div>
                            <div class="col">
                                <?= $form->field($searchModel, 'target_date_btw')->textInput(['class'=>'form-control div1'])->label('Target Date'); ?>
                            </div>
                        </div>

                        <div class="row px-3">

                            <div class="col">
                                <?= $form->field($searchModel, 'inspection_date_btw')->textInput(['class'=>'form-control div1'])->label('Valuation Report Date'); ?>
                            </div>
                            <div class="col">
                                <?php
            echo $form->field($searchModel, 'valuer')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->andWhere(['status'=>1])->andWhere(['trashed'=>0])->andWhere(['permission_group_id'=>[3,5,7,8,9,10,12]])->orderBy([
                    'firstname' => SORT_ASC,
                ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];}),
                'options' => ['placeholder' => 'Select Valuer ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
                            </div>
                        </div>

                        <div class="row px-3">
                            <div class="col">
                                <?php
          echo $form->field($searchModel, 'valuation_status')->widget(Select2::classname(), [
              'data' => Yii::$app->helperFunctions->valuationStatusListArr,
              'options' => ['placeholder' => 'Select Valuation Status ...'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
          ])->label('Valuation Status');
          ?>
                            </div>
                            <div class="col">
                                <?php
          echo $form->field($searchModel, 'status')->widget(Select2::classname(), [
              'data' => Yii::$app->helperFunctions->arrFilterStatus,
              'options' => ['placeholder' => 'Select Status ...'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
          ])->label('Status');
          ?>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary ']) ?>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

<?php 
} 
?>



<?php 
if(in_array(Yii::$app->user->id, $allowedId)){ 
    if($page_title=='Today Challenged'){
        $vieww = 'today_challehged_sort.php';
    }
    else if($page_title=='Today Mistakes'){
        $vieww = 'today_mistakes_sort.php';
    }
    else if($page_title=='Today Received' || $page_title=='Today Dynamic Overview'){
        $vieww = 'today_received_sort.php';
    }
    else if($page_title=='Today Inspected'){
        $vieww = 'today_inspected_sort.php';
    }
    else if($page_title=='Today Documents Requested')
    {
        $vieww = 'today_documents_requested.php';
    }else if($page_title=='Pending Documents Requested' || $page_title=='Pending Inspected' || $page_title == "Pending Reminders" || $page_title == "Partial Inspected")
    {
        $vieww = 'pending_valuation_performance.php';
    }else if($page_title=='Total Received' || $page_title == 'Total Inspected'  || $page_title == 'Valuation Performance' || $page_title == 'Total Reminders' || $page_title == 'Total Approved' || $page_title == 'Total Challenged' || $page_title == 'Total Mistakes' || $page_title == 'Total Cancelled' ){
        $vieww = 'total_valuation_performance.php';
    }
    else{
        $vieww = 'Sort_Report_View.php';
    }

    echo $this->render($vieww, [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'user' => $user,
        'request' => Yii::$app->request->queryParams,
        'page_title' => $page_title,
    ]);

}

else{ ?>

<div class="valuation-index col-12">

<?php CustomPjax::begin(['id' => 'grid-container1']); ?>
<?= CustomGridView::widget([
'dataProvider' => $dataProvider,
// 'filterModel' => $searchModel,
'cardTitle' => $cardTitle,
// 'createBtn' => $createBtn,
'showFooter' => true,
'columns' => [
//  ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
['attribute' => 'reference_number', 'label' => Yii::t('app', 'Reference')],
['attribute' => 'client_id',
    'label' => Yii::t('app', 'Client'),
    'value' => function ($model) {
        return $model->client->title;
    },
    'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
        'title' => SORT_ASC,
    ])->all(), 'id', 'title')
],
['attribute' => 'client_reference',
    'label' => Yii::t('app', 'Client Reference'),
    'value' => function ($model) {
        return $model->client_reference;
    },
    'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
        'title' => SORT_ASC,
    ])->all(), 'id', 'title')
],
['attribute' => 'client_name_passport',
    'label' => Yii::t('app', 'Customer Full name'),
    'value' => function ($model) {
        return $model->client_name_passport;
    },
    'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
        'title' => SORT_ASC,
    ])->all(), 'id', 'title')
],
['attribute' => 'property_id',
    'label' => Yii::t('app', 'Property'),
    'value' => function ($model) {
        return $model->property->title;
    },
    'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
        'title' => SORT_ASC,
    ])->all(), 'id', 'title')
],
['attribute' => 'building_info',
    'label' => Yii::t('app', 'Building'),
    'value' => function ($model) {
        return $model->building->title;
    },
    'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
        'title' => SORT_ASC,
    ])->all(), 'id', 'title')
],
['attribute' => 'building_info',
    'label' => Yii::t('app', 'Community'),
    'value' => function ($model) {
        return $model->building->communities->title;
    },
    // 'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
    //     'title' => SORT_ASC,
    // ])->all(), 'id', 'community')
],

['attribute' => 'building_info',
    'label' => Yii::t('app', 'Address'),
    'value' => function ($model) {
        return $model->unit_number.', '.$model->building->title.', '.$model->plot_number.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];
    },
    // 'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
    //     'title' => SORT_ASC,
    // ])->all(), 'id', 'community')
],
// ['attribute' => 'building_info',
//     'label' => Yii::t('app', 'Sub Community'),
//     'value' => function ($model) {
//         return $model->building->subCommunities->title;
//     },
//     // 'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
//     //     'title' => SORT_ASC,
//     // ])->all(), 'id', 'community')
// ],
['attribute' => 'building_info',
    'label' => Yii::t('app', 'City'),
    'value' => function ($model) {
        return Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];

    },
    'filter' => Yii::$app->appHelperFunctions->emiratedListArr
],

['attribute' => 'instruction_date',
    'label' => Yii::t('app', 'Instruction Date'),
    'value' => function ($model) {
        return date('d-m-Y', strtotime($model->instruction_date));
    },
],
/* ['attribute' => 'target_date',
    'label' => Yii::t('app', 'Target Date'),
    'value' => function ($model) {
        return date('d-m-Y', strtotime($model->target_date));
    },
],*/
['attribute' => 'id',
    'label' => Yii::t('app', 'Valuation Report Date'),
    'value' => function ($model) {
       // return date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
        if($model->scheduleInspection->valuation_report_date <> null) {
            return date('d-m-Y', strtotime($model->scheduleInspection->valuation_report_date));
        }else{

        }
    },
],
['attribute' => 'id',
    'label' => Yii::t('app', 'Fee'),
    'value' => function ($model) {
      /*if (Yii::$app->appHelperFunctions->getClientRevenue($model->id)!=null) {
        $total+=(int)Yii::$app->appHelperFunctions->getClientRevenue($model->id);
      }*/
      if(in_array( Yii::$app->user->identity->id, [1,14,33,110465])) {
        return $model->fee;
      }else{
        return "";
      }
      //  return Yii::$app->appHelperFunctions->getClientRevenue($model->id);
    },
    // 'footer' => '<b>Fee Total</b> '.Yii::$app->appHelperFunctions->wmFormate(\app\models\Valuation::getTotal($dataProvider->models, 'fee')),
],
// ['attribute' => 'approval_id',
//     'label' => Yii::t('app', 'Approver'),
//     'value' => function ($model) {
//         return $model->user->firstname.' '.$model->user->lastname;
//     },
//     // 'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
//     //     'title' => SORT_ASC,
//     // ])->all(), 'id', 'community')
// ],
[
    'attribute' => 'service_officer_name',
    'label' => Yii::t('app', 'Valuer'),
    'value' => function ($model) {
        return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
    },
    'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
        'firstname' => SORT_ASC,
    ])->all(), 'id',  function($model) {return $model['firstname'].' '.$model['lastname'];})
],
[
    'attribute' => 'quotation_id',
    //'label' => Yii::t('app', 'Valuer'),
    'value' => function ($model) {
        return $model->quotation_id;
    }
],
[
    'attribute' => 'tenure',
    //'label' => Yii::t('app', 'Valuer'),
    'value' => function ($model) {
        return Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure];
    }
],

[
    'attribute' => 'id',
    'label' => Yii::t('app', 'Market Value'),
    'value' => function ($model) {
        return (isset($model->approverData) && ($model->approverData <> null))? (number_format($model->approverData->estimated_market_value)): '';
    },
],

// ['attribute' => 'purpose_of_valuation',
//     'label' => Yii::t('app', 'Purpose'),
//     'value' => function ($model) {
//         return Yii::$app->appHelperFunctions->purposeOfValuationArr[$model['purpose_of_valuation']];
//     },
//     'filter' => Yii::$app->appHelperFunctions->purposeOfValuationArr
// ],
['format' => 'raw',
  'attribute' => 'valuation_status',
    'label' => Yii::t('app', 'Valuation Status'),
    'value' => function ($model) {
        return Yii::$app->helperFunctions->valuationStatusListArrLabel[$model->valuation_status];
    },
    'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
        'title' => SORT_ASC,
    ])->all(), 'id', 'title')
],






// [
//  'attribute' => 'id',
//
// ],
],

]);
?>
<?php CustomPjax::end(); ?>


</div>

<?php    
}
?>



















