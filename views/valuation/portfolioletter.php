<?php

use app\models\CrmReceivedProperties;


//same as TOE
$discount = 0;
$discount_p = 0;
$VAT =0;
$discount_quotations = 0;

/*echo "<pre>";
print_r($valuations_data->submission_approver_date);
die;*/
$otherInstructingPerson='';
$result = \app\models\User::find()->where(['id'=>$valuations_data->other_instructing_person])->one();
if ($result<>null) {
    $otherInstructingPerson = $result->firstname.' '.$result->lastname;
}

?>




<style>
    .box1{
        background-color: #e6e6e6;
        width: 100px;
        padding-top: 20px !important;
      /*  border-right: 5px white;*/
       /* border-left: 5px white;*/
    }

    .border {
        width: 100%;
      /*  border-top-width: 0.05px;
        border-bottom-width: 0.05px;
        border-left-width: 0.05px;
        border-right-width: 0.05px;*/
        padding: 5px;
    }
    .font_color{
        color: #3763ae;
    }

</style>


<table class="" style="padding-bottom: 2px;" >
    <tr>
        <td>
        <p style="font-size: 11px;"><?= date("F j, Y", strtotime($valuations_data->submission_approver_date)); ?>
        </p>
        </td>
    </tr>

    <tr>
        <td>
        <p style="font-size: 11px;"><?= $otherInstructingPerson ?></p>
         <p style="font-size: 11px;"><?= $valuations_data->client->title ?></p>
            <br>
        </td>
    </tr>
    <tr>
        <td><p style="font-size: 11px;">Dear Sir/Madam,</p>
        </td>
    </tr>
    <tr>
        <td class="firstpage fontsizeten"><p style="font-size: 11px;">Portfolio Valuation – Attached Valuations Summary List - <?= $ref_portfolio ?></p>
        </td>
    </tr>
<br>
    <tr>
        <td class="detailtexttext first-section" ><p>We would like to thank you for giving us this privilege to value your important portfolio of properties for you. We enjoyed working for you.
                <br><br>While you have emailed to you the individual valuation reports for each subject property under valuation, we are pleased to herewith share with you the attached valuations summary list of all subject properties, their details, and their values estimated by our firm.
                <br><br>Kindly note that the attached list is only for central presentation purpose as requested by the Client. However, the individual values reported in the list are an integral part of the individual valuation reports per property. They are extracted from the individual valuation reports. They must be read/used in conjunction with the valuation reports, which include the in depth relevant information, analysis, calculations, justifications and assumptions, and not alone. All assumptions, terms and conditions applied to the individual valuation reports also apply to the attached summary list.
                <br><br>Thank you for your kind understanding and cooperation.
            </p>
        </td>
    </tr>


    <tr>
        <td class="firstpage fontsizeten">
            <p style="font-size: 11px;">Sincerely,</p>
            <img src="<?php echo '/images/lksign.png' ?>" style="width: 111px;height: 45px; ">
            <p style="font-size: 11px;">Laxmi Kumar</p>
            <p style="font-size: 11px;">Chief Valuation Officer</p>
            <p style="font-size: 11px;">Windmills Real Estate Valuation Services LLC</p>
        </td>
    </tr>

</table>





<style>
    .airal-family{
        font-family: Arial, Helvetica, sans-serif;
    }
</style>


<style>

    .text-dec{
        font-size: 8px;
        text-align: center;
        padding-right: 10px;
    }

    /*td.detailtext{*/
    /*background-color:#BDBDBD; */
    /*font-size:9px;*/
    /*border-top: 0.4px solid grey;*/
    /*}*/

    td.detailtexttext{
        /* background-color:#EEEEEE; */
        font-size:10px;
        font-weight: 100;

    }
    th.detailtext{
        /* background-color:#BDBDBD; */
        font-size:9px;
       /*// border-top: 1px solid black;*/
    }
    /*.border {
    border: 1px solid black;
    }*/

    th.amount-heading{
        color: #039BE5;
    }
    td.amount-heading{
        color: #039BE5;
    }
    th.total-due{
        font-size: 16px;
    }
    td.total-due{
        font-size: 16px;
    }
    span.size-8{
        font-size: 10px;
        font-weight: bold;
    }
    td.first-section{
        padding-left: 10px;
        /* background-color: black; */
    }
</style>

