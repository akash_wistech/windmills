<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Assumption/Summary');
$cardTitle = Yii::t('app', 'Assumption/Summary Details:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_9/'.$valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-edit"></i>
                <?= $valuation->reference_number ?>
            </h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-5 col-sm-3">
                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                        <?php  echo $this->render('../left-nav', ['model' => $valuation,'step' => 9]); ?>
                    </div>
                </div>
                <div class="col-7 col-sm-9">
                    <div class="tab-content" id="vert-tabs-tabContent">
                        <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                            <section class="valuation-form card card-outline card-primary">

                                <?php $form = ActiveForm::begin(); ?>
                                <header class="card-header">
                                    <h2 class="card-title"><?= $cardTitle ?></h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <?= $form->field($model, 'special_assumption')->textarea(['maxlength' => true, 'rows' => 6])->label('Special Assumptions') ?>
                                        </div>

                                        <div class="col-sm-12">
                                            <?= $form->field($model, 'general_assumption')->textarea(['maxlength' => true, 'rows' => 6])->label('General Assumptions') ?>
                                        </div>

                                    </div>


                                </div>
                                <div class="card-footer">
                                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </section>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>



