<?php

namespace app\controllers;

use app\models\Buildings;
use app\models\Company;
use app\models\ConfigurationFiles;
use app\models\CostDetails;
use app\models\InspectProperty;
use app\models\ListingsTransactionsSearch;
use app\models\PreviousTransactions;
use app\models\PreviousTransactionsSearch;
use app\models\Properties;
use app\models\ReceivedDocs;
use app\models\ReceivedDocsFiles;
use app\models\ScheduleInspection;
use app\models\SoldTransactionSearch;
use app\models\User;
use app\models\UserProfileInfo;
use app\models\ValuationApproversData;
use app\models\ValuationConfiguration;
use app\models\ValuationConflict;
use app\models\ValuationDeveloperDriveMargin;
use app\models\ValuationDmPayments;
use app\models\ValuationDriveMv;
use app\models\ValuationEnquiry;
use app\models\ValuationListCalculation;
use app\models\ValuationOwners;
use app\models\ValuationSelectedLists;
use Yii;
use app\models\Valuation;
use app\models\ValuationSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use  \app\modules\wisnotify\listners\NotifyEvent;

/**
 * ValuationController implements the CRUD actions for Valuation model.
 */
class ValuationController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Valuation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 2;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionApprovedList()
    {
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('approved_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionCancelledList()
    {
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 3;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('cancelled_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Valuation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Valuation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Valuation();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $model->valuation_status = 1;
                $sequence = array_search($model->valuation_status,Yii::$app->appHelperFunctions->getStepsSequence(),true);

                if ( $sequence ==1 && $model->email_status !== 1) {
                    // echo 'why is it in?';
                    // die();
                    $notifyData = [
                        'client' => $model->client,
                        'attachments' => [],
                        'subject' => $model->email_subject,
                        'uid' => $model->id,
                        'replacements'=>[
                            '{clientName}'=>   $model->client->title,
                        ],
                    ];

                    $allow_properties = [1, 2, 5, 6, 12, 37];
                    if (in_array($model->property_id, $allow_properties)) {
                       // \app\modules\wisnotify\listners\NotifyEvent::fire('Received.Valuation', $notifyData);
                    }
                    //   UPDATE (table name, column values, condition)
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 1], 'id='.$model->id .'')->execute();
                    Yii::$app->db->createCommand()->update('valuation', ['email_status' => 1], 'id='.$model->id .'')->execute();
                }
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['valuation/step_1/' . $model->id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Valuation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->redirect('valuation/step_1/' . $id);
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_1/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionStep_1($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $model = $this->findModel($id);

        $model->city = Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];
        $model->community = $model->building->communities->title;
        $model->sub_community = $model->building->subCommunities->title;

        if ($model->load(Yii::$app->request->post())) {
            //$model->valuation_status = 1;
            if ($model->save()) {

                $sequence = array_search($model->valuation_status,Yii::$app->appHelperFunctions->getStepsSequence(),true);

                if ( $sequence ==1 && $model->email_status !== 1) {

                    // echo 'why is it in?';
                    // die();
                    $notifyData = [
                        'client' => $model->client,
                        'attachments' => [],
                        'subject' => $model->email_subject,
                        'uid' => $model->id,
                        'replacements'=>[
                            '{clientName}'=>   $model->client->title,
                        ],
                    ];


                    $allow_properties = [1, 2, 5, 6, 12, 37];
                    if (in_array($model->property_id, $allow_properties)) {
                       // \app\modules\wisnotify\listners\NotifyEvent::fire('Received.Valuation', $notifyData);
                    }
                    //   UPDATE (table name, column values, condition)
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 1], 'id='.$model->id .'')->execute();
                    Yii::$app->db->createCommand()->update('valuation', ['email_status' => 1], 'id='.$id .'')->execute();
                }



                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_1/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step1', [
            'model' => $model,
        ]);
    }

    public function actionStep_2($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_2')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $model = ReceivedDocs::find()->where(['valuation_id' => $id])->one();


        if ($model !== null) {

        } else {
            $model = new ReceivedDocs();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            if ($model->save()) {

                $sequence = array_search($valuation->valuation_status,Yii::$app->appHelperFunctions->getStepsSequence(),true);

                if ( $sequence ==1 && $model->email_status !== 1) {

                    // echo 'why is it in?';
                    // die();

                    $notifyData = [
                        'client' => $valuation->client,
                        'attachments' => [],
                        'subject' => $valuation->email_subject,
                        'uid' => $valuation->id,
                        'replacements'=>[
                            '{clientName}'=>   $valuation->client->title,
                        ],
                    ];

                    // \app\modules\wisnotify\listners\NotifyEvent::fire('Received.Doc', $notifyData);
                    // UPDATE (table name, column values, condition)
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 7], 'id='.$model->valuation_id.'')->execute();
                    Yii::$app->db->createCommand()->update('received_docs', ['email_status' => 1], 'id='.$model->id .'')->execute();
                }


               // $valuation->valuation_status = 7;
               // $valuation->save();
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_2/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step2', [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_3($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_3')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $model = ValuationConflict::find()->where(['valuation_id' => $id])->one();

        $client =  Company::find()->where(['id' => $valuation->client_id])->one();

        //$related_to_buyer_check =  PreviousTransactions::find()->where(['client_customer_name' => $valuation->client_name_passport])->all();
/*        $owners_in_valuation = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $id])->all(), 'id', 'name');
        $owners_valutions_id = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['name' => $owners_in_valuation])->all(), 'id', 'valuation_id');
        $owners_previous_data = PreviousTransactions::find()->where(['valuation_id' => $owners_valutions_id])->all();*/

        $related_to_buyer_check =  PreviousTransactions::find()->where(['client_name' => $valuation->client_name_passport])->all();

        $owners_in_valuation = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $id])->all(), 'id', 'name');
        $owners_valutions_names = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['name' => $owners_in_valuation])->all(), 'id', 'name');


        $owners_previous_data = PreviousTransactions::find()->where(['client_name' => $owners_valutions_names])->all();

        $related_to_client_check =  PreviousTransactions::find()->where(['client_name' => $client->title])->all();
        $related_to_property_check =  PreviousTransactions::find()->where(['building_info' => $valuation->building_info,'unit_number'=>$valuation->unit_number])->all();



        if ($model !== null) {

        } else {
            $model = new ValuationConflict();
            if($related_to_buyer_check <> null && count($related_to_buyer_check)>0){
                $model->related_to_buyer = 'Yes';
            }
            if($owners_previous_data <> null && count($owners_previous_data)>0){
                $model->related_to_seller = 'Yes';
            }
            if($related_to_client_check <> null && count($related_to_client_check)>0){
                $model->related_to_client = 'Yes';
            }
            if($related_to_property_check <> null && count($related_to_property_check)>0){
                $model->related_to_property = 'Yes';
            }

        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_3/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error)
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {{
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step3', [
            'model' => $model,
            'valuation' => $valuation,
            'buyer_data' => $related_to_buyer_check,
            'seller_data' => $owners_previous_data,
            'client_data' => $related_to_client_check,
            'property_data' => $related_to_property_check,
        ]);
    }

    public function actionStep_4($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_4')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $model = ScheduleInspection::find()->where(['valuation_id' => $id])->one();

        if ($model !== null) {

        } else {
            $model = new ScheduleInspection();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            if ($model->save()) {

                $sequence = array_search($valuation->valuation_status,Yii::$app->appHelperFunctions->getStepsSequence(),true);

                if ( $sequence ==2 && $model->email_status !== 1) {



                    $InspectionTypeLabel='';
                    $InspectionTypeLabelDate='';
                    $InspectionTimeLabel='';
                    $InspectionTime='';
                    $InspectionOfficerLabel='';
                    $InspectionOfficer='';

                    $contactPersonNameLabel='';
                    $contactPersonName='';
                    $contactEmailLabel='';
                    $contactEmail='';
                    $contactPhoneNoLabel='';
                    $contactPhoneNo='';

                    if ($model->inspection_type ==1 || $model->inspection_type ==2) {
                        $InspectionTypeLabel=Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type];
                        $InspectionTypeLabelDate=  date('d-m-Y', strtotime($model->inspection_date));
                        $InspectionTimeLabel=$InspectionTypeLabel.' Time:  ';
                        $InspectionTypeLabel.=':';
                        $InspectionTime=$model->inspection_time;
                        $InspectionOfficerLabel='Inspection Officer:';
                        $InspectionOfficer= Yii::$app->appHelperFunctions->staffMemberListArr[$model->inspection_officer];
                    }
                    if ($model->inspection_type ==2) {
                        $contactPersonNameLabel='Contact Person Name:';
                        $contactPersonName=$model->contact_person_name;
                        $contactEmailLabel='Contact Email:';
                        $contactEmail=$model->contact_email;
                        $contactPhoneNoLabel='Contact PhoneNo:';
                        $contactPhoneNo=$model->contact_phone_no;
                    }



                    $notifyData = [
                        'client' => $valuation->client,
                        'subject' => $valuation->email_subject,
                        'attachments' => [],
                        'uid' => $valuation->id,
                        'replacements'=>[
                            '{clientName}'=>          $valuation->client->title,
                            '{inspectionType}'=>      Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type],
                            '{valuationData}'=>       $model->valuation_date,
                            '{valuationReportDate}'=> $model->valuation_report_date,
                            '{label}'=>               $InspectionTypeLabel,
                            '{labelDate}'=>           $InspectionTypeLabelDate,
                            '{inspectionTimeLabel}'=> $InspectionTimeLabel,
                            '{inspectionTime}'=>      $InspectionTime,
                            '{inspectionOfficerLabel}'=> $InspectionOfficerLabel,
                            '{inspectionOfficer}'=>   $InspectionOfficer,
                            '{contactPersonNameLable}'=>   $contactPersonNameLabel,
                            '{contactPersonName}'=>   $contactPersonName,
                            '{contactEmailLabel}'=>        $contactEmailLabel,
                            '{contactEmail}'=>        $contactEmail,
                            '{contactPhoneNoLabel}'=>      $contactPhoneNoLabel,
                            '{contactPhoneNo}'=>      $contactPhoneNo,
                        ],
                    ];
                    if($model->inspection_type != 3) {
                        $allow_properties = [1, 2, 5, 6, 12, 37];
                        if (in_array($valuation->property_id, $allow_properties)) {
                           // \app\modules\wisnotify\listners\NotifyEvent::fire('schedule.send', $notifyData);
                        }
                    }
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 2], 'id='.$model->valuation_id.'')->execute();
                    Yii::$app->db->createCommand()->update('schedule_inspection', ['email_status' => 1], 'id='.$model->id .'')->execute();
                }


               // $valuation->valuation_status = 2;
                //$valuation->save();
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_4/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step4', [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_5($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_5')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $model = InspectProperty::find()->where(['valuation_id' => $id])->one();

        if ($model !== null) {

        } else {

            $model = new InspectProperty();
            $model->makani_number = $valuation->building->makani_number;
            //$model->location = $valuation->building->location;
            $model->latitude = $valuation->building->latitude;
            $model->longitude = $valuation->building->longitude;
            $model->property_placement = $valuation->building->property_placement;
            $model->property_visibility = $valuation->building->property_visibility;
            $model->property_exposure = $valuation->building->property_exposure;
            $model->property_category = $valuation->building->property_category;
            $model->property_condition = $valuation->building->property_condition;
            $model->development_type = $valuation->building->development_type;
            $model->finished_status = $valuation->building->finished_status;
            $model->developer_id = $valuation->building->developer_id;
            $model->estimated_age = $valuation->building->estimated_age;
            $model->estimated_remaining_life = $valuation->property->age - $valuation->building->estimated_age;
            $model->number_of_basement = $valuation->building->number_of_basement;
            $model->pool = $valuation->building->pool;
           // $model->gym = $valuation->building->gym;
           // $model->play_area = $valuation->building->play_area;
            $model->other_facilities = $valuation->building->other_facilities;
            $model->completion_status = $valuation->building->completion_status;
            $model->landscaping = $valuation->building->landscaping;
            $model->white_goods = $valuation->building->white_goods;
            $model->furnished = $valuation->building->furnished;
            $model->utilities_connected = $valuation->building->utilities_connected;
            $model->location_highway_drive = $valuation->building->location_highway_drive;
            $model->location_school_drive = $valuation->building->location_school_drive;
            $model->location_mall_drive = $valuation->building->location_mall_drive;
            $model->location_sea_drive = $valuation->building->location_sea_drive;
            $model->location_park_drive = $valuation->building->location_park_drive;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;


            if ($model->save()) {
                $sequence = array_search($valuation->valuation_status,Yii::$app->appHelperFunctions->getStepsSequence(),true);

                if ( $sequence ==3) {
                    // UPDATE (table name, column values, condition)
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 3], 'id='.$model->valuation_id.'')->execute();
                }
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_5/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }




            /*if ($model->save()) {

                if ($valuation->valuation_status!==3 && $model->email_status !== 1) {

                    $notifyData = [
                        'client' => $valuation->client,
                        'subject' => $valuation->email_subject,
                        'attachments' => [],
                        'replacements'=>[
                            '{clientName}'=>   $valuation->client->title,
                        ],
                    ];
                   // \app\modules\wisnotify\listners\NotifyEvent::fire('Inspect.Property', $notifyData);
                    // UPDATE (table name, column values, condition)
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 3], 'id='.$model->valuation_id.'')->execute();
                    Yii::$app->db->createCommand()->update('inspect_property', ['email_status' => 1], 'id='.$model->id .'')->execute();
                }
               // $valuation->valuation_status = 3;
               // $valuation->save();

                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_5/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }*/
        }

        return $this->render('steps/_step5', [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_6($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_6')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $configuration = InspectProperty::find()->where(['valuation_id' => $id])->one();
        $model = ValuationConfiguration::find()->where(['valuation_id' => $id])->one();

        if ($model !== null) {

        } else {
            $model = new ValuationConfiguration();
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_6/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step6', [
            'model' => $model,
            'valuation' => $valuation,
            'configuration' => $configuration
        ]);
    }

    public function actionStep_7($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_7')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $valuation = $this->findModel($id);
        $model = CostDetails::find()->where(['valuation_id' => $id])->one();
        if ($model !== null) {

        } else {
            $model = new CostDetails();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_7/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step7', [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_8($id, $action = null)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_8')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $total_days = 0;
        $total_percentage = 0;
        $total_amount = 0;
        $total_present_value = 0;
        $d_m_percentage = 0;
        $valuation = $this->findModel($id);
        $model = ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();


        if ($model !== null) {

            if (isset($_POST['ValuationDeveloperDriveMargin']['frequency_of_payments']) && ($_POST['ValuationDeveloperDriveMargin']['frequency_of_payments'] != $model->frequency_of_payments)) {


                ValuationDmPayments::deleteAll(['valuation_id' => $id]);

            }
        } else {
            $model = new ValuationDeveloperDriveMargin();
        }
        $payments_table = ValuationDmPayments::find()->where(['valuation_id' => $id])->all();
        if (!empty($payments_table) && $payments_table <> null) {
            $show_payment_table = 1;
        } else {
            $show_payment_table = 0;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            // $model->frequency_of_payments = 4;


            if (!empty($payments_table) && $payments_table <> null) {


                $CostDetails = CostDetails::find()->where(['valuation_id' => $id])->one();
                $start_date = $model->payment_info[0]['installment_date'];

                if ($start_date <> null) {
                    foreach ($model->payment_info as $key => $payment) {

                        $date1 = date_create($start_date);
                        $date2 = date_create($payment['installment_date']);
                        $diff = date_diff($date1, $date2);
                        $model->payment_info[$key]['no_of_days'] = $diff->format("%a");


                        $percentage = $payment['percentage'];
                        $original_purchase_price = $CostDetails['original_purchase_price'];
                        $model->payment_info[$key]['amount'] = round((($percentage / 100) * $original_purchase_price), 2);
                        $total_rate = $model->funding_cost + $model->admin_charges;
                        if ($key > 0) {
                            $sqrt = $model->payment_info[$key]['no_of_days'] / 365;
                            $sqrt_interest = pow((1 + ($total_rate / 100)), $sqrt);
                            $model->payment_info[$key]['present_value'] = round(($model->payment_info[$key]['amount'] / $sqrt_interest), 2);
                        } else {
                            $model->payment_info[$key]['present_value'] = $model->payment_info[$key]['amount'];
                        }
                        $model->payment_info[$key]['index_id'] = $key;
                        $model->payment_info[$key]['valuation_id'] = $id;

                        $total_amount = $total_amount + $model->payment_info[$key]['amount'];
                        $total_present_value = $total_present_value + $model->payment_info[$key]['present_value'];


                    }

                }
                if ($total_amount > 0) {
                    $model->developer_margin = $total_amount - $total_present_value;
                    $model->developer_margin = round($model->developer_margin);
                    $model->developer_margin_percentage = round((($model->developer_margin / $total_amount) * 100), 2);
                    $model->cash_equivalent_price = $total_amount - $model->developer_margin;
                    $model->estimated_price_per_valuation = round($model->cash_equivalent_price * (1 - ($model->decline_in_prices / 100)));
                }

            } else {
                $payments_table = array();

                for ($i = 0; $i < $model->frequency_of_payments; $i++) {
                    $sub_payment = array();
                    if ($i == 0) {
                        $sub_payment['installment_date'] = $model->initial_deposit_contract_date;
                    } else if (($i + 1) == $model->frequency_of_payments) {
                        $sub_payment['installment_date'] = $model->last_payment_date;
                    } else {
                        $sub_payment['installment_date'] = '';
                    }
                    $sub_payment['no_of_days'] = 0;
                    $sub_payment['percentage'] = '';
                    $sub_payment['amount'] = 0;
                    $sub_payment['present_value'] = 0;
                    $sub_payment['index_id'] = $i;
                    $sub_payment['valuation_id'] = $id;
                    $payments_table[] = $sub_payment;
                }
                $model->payment_info = $payments_table;
            }

            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_8/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }

        }

        return $this->render('steps/_step8', [
            'model' => $model,
            'valuation' => $valuation,
            'show_payment_table' => $show_payment_table,
            'payments_table' => $payments_table,
        ]);
    }


    public function actionStep_9($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_9')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_9/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step9', [
            'model' => $model,
            'valuation' => $model,
        ]);
    }

    /* Start sold calculations*/
    public function actionStep_10($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_10')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $valuation = $this->findModel($id);
        $model = ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();
        if ($model !== null) {

        } else {
            $model = new ValuationEnquiry();
            $model->date_from = '2020-01-01';
            $model->date_to = date('Y-m-d');
            $model->building_info = $valuation->building_info;
            $model->bedroom_from = $valuation->inspectProperty->no_of_bedrooms;
            $model->bedroom_to = $valuation->inspectProperty->no_of_bedrooms;
        }

        if ($model->load(Yii::$app->request->post())) {
            // ValuationEnquiry::deleteAll(['valuation_id' => $id,'type'=> 'sold']);
            $model->valuation_id = $id;
            $model->type = 'sold';
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_10/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step10', [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_11($id,$readonly=0)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_11')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            if (isset($post['selection']) && $post['selection'] <> null) {
                ValuationSelectedLists::deleteAll(['valuation_id' => $id, 'type' => 'sold']);
                foreach ($post['selection'] as $selected_row) {
                    $selected_data_detail = new ValuationSelectedLists();
                    $selected_data_detail->selected_id = $selected_row;
                    $selected_data_detail->type = 'sold';
                    $selected_data_detail->valuation_id = $id;
                    $selected_data_detail->save();
                }

            }
        }

        $selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'sold'])->all(), 'id', 'selected_id');

        $totalResults = (new \yii\db\Query())
            ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) avg_listings_price_size, MIN(listings_price) as min_price, MIN(price_per_sqt) as min_price_sqt, MAX(listings_price) as max_price, MAX(price_per_sqt) as max_price_sqt , FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
            ->from('sold_transaction')
            ->where(['id' => $selected_data])
            ->all();


        //Average and calculations

        if (!empty($totalResults) && $totalResults <> null) {
            if (isset($totalResults[0]['avg_bedrooms'])) {
                ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'sold']);
                $calculations = new ValuationListCalculation();
                $calculations->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                $calculations->avg_land_size = round($totalResults[0]['avg_land_size']);
                $calculations->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                $calculations->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                $calculations->min_price = round($totalResults[0]['min_price']);
                $calculations->max_price = round($totalResults[0]['max_price']);
                $calculations->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                $calculations->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                $calculations->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                $calculations->avg_psf = round($calculations->avg_listings_price_size / $calculations->built_up_area_size);
                $calculations->avg_gross_yield = 0;
                $calculations->type = 'sold';
                $calculations->valuation_id = $id;
                $calculations->save();
            }

        }

        $select_calculations = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();


        // $sold_selected_records = \app\models\ValuationSelectedLists::find()->where(['selected_id' => $selected_data,'type'=> 'sold'])->all();

        // selected_data
        $sold_selected_records = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();


        $listing_filter = array();
        if (!empty($sold_selected_records) && $sold_selected_records) {

            $listing_filter['SoldTransactionSearch']['date_from'] = $sold_selected_records->date_from;
            $listing_filter['SoldTransactionSearch']['date_to'] = $sold_selected_records->date_to;
            $listing_filter['SoldTransactionSearch']['building_info_data'] = $sold_selected_records->building_info;
            $listing_filter['SoldTransactionSearch']['property_id'] = $sold_selected_records->property_id;
            $listing_filter['SoldTransactionSearch']['property_category'] = $sold_selected_records->property_category;
            $listing_filter['SoldTransactionSearch']['bedroom_from'] = $sold_selected_records->bedroom_from;
            $listing_filter['SoldTransactionSearch']['bedroom_to'] = $sold_selected_records->bedroom_to;
            $listing_filter['SoldTransactionSearch']['landsize_from'] = $sold_selected_records->landsize_from;
            $listing_filter['SoldTransactionSearch']['landsize_to'] = $sold_selected_records->landsize_to;
            $listing_filter['SoldTransactionSearch']['bua_from'] = $sold_selected_records->bua_from;
            $listing_filter['SoldTransactionSearch']['bua_to'] = $sold_selected_records->bua_to;
            $listing_filter['SoldTransactionSearch']['price_from'] = $sold_selected_records->price_from;
            $listing_filter['SoldTransactionSearch']['price_to'] = $sold_selected_records->price_to;
            $listing_filter['SoldTransactionSearch']['price_psf_from'] = $sold_selected_records->price_psf_from;
            $listing_filter['SoldTransactionSearch']['price_psf_to'] = $sold_selected_records->price_psf_to;
        }


        $selected_records_display = $sold_selected_records;

        if ($selected_records_display <> null & !empty($selected_records_display)) {
            $building_info_data = '';

            if ($selected_records_display->building_info <> null) {
                $buildings = explode(",", $sold_selected_records->building_info);
                $all_buildings_titles = Buildings::find()->where(['id' => $buildings])->all();

                if ($all_buildings_titles <> null && !empty($all_buildings_titles)) {
                    foreach ($all_buildings_titles as $title) {
                        $building_info_data .= $title['title'] . ',&nbsp';
                    }

                }

            }
            $selected_records_display->building_info = $building_info_data;
            $property_data = Properties::find()->where(['id' => $sold_selected_records->property_id])->one();
            if ($property_data <> null && !empty($property_data)) {
                $selected_records_display->property_id = $property_data->title;
            }
            $property_data = Properties::find()->where(['id' => $sold_selected_records->property_id])->one();
            if ($selected_records_display->property_category <> null) {
                $selected_records_display->property_category = Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$sold_selected_records_display->property_category];
            }
        }

        $valuation = $this->findModel($id);
        // $searchModel = new ListingsTransactionsSearch();
        $searchModel = new SoldTransactionSearch();
        $dataProvider = $searchModel->searchvaluation($listing_filter);
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;


        if ($readonly==1) {
            $dataProvider->sort = false;
            return $this->renderPartial('steps/_step11', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'valuation' => $valuation,
                'selected_data' => $selected_data,
                'totalResults' => $totalResults,
                'select_calculations' => $select_calculations,
                'selected_records_display' => $selected_records_display,
                'readonly' => $readonly
            ]);
        }



        return $this->render('steps/_step11', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'valuation' => $valuation,
            'selected_data' => $selected_data,
            'totalResults' => $totalResults,
            'select_calculations' => $select_calculations,
            'selected_records_display' => $selected_records_display,
            'readonly' => $readonly

        ]);

    }

    public function actionStep_12($id,$readonly=0)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_12')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();
            if ($drive_margin_saved->load(Yii::$app->request->post())) {
                if ($drive_margin_saved->save()) {
                    Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                    // return $this->redirect(['index']);
                    return $this->redirect(['valuation/step_12/' . $id]);
                } else {
                    if ($drive_margin_saved->hasErrors()) {
                        foreach ($drive_margin_saved->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }

        $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();

        $location_attributes_value = Yii::$app->appHelperFunctions->getLocationAttributes($inspection_data);
        $view_attributes_value = Yii::$app->appHelperFunctions->getViewAttributes($inspection_data);

        $list_calculate_data = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();
        $weightages = \app\models\Weightages::find()->where(['property_type' => $model->property_id])->one();


        $buildongs_info = \app\models\Buildings::find()->where(['id' => $model->building_info])->one();
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $developer_margin = \app\models\ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();

        $weightages_required_month = array();
        $weightages_required_year = array();

        $weightages_required_month[1] = $weightages->less_than_1_month;
        $weightages_required_month[2] = $weightages->less_than_2_month;
        $weightages_required_month[3] = $weightages->less_than_3_month;
        $weightages_required_month[4] = $weightages->less_than_4_month;
        $weightages_required_month[5] = $weightages->less_than_5_month;
        $weightages_required_month[6] = $weightages->less_than_6_month;
        $weightages_required_month[7] = $weightages->less_than_7_month;
        $weightages_required_month[8] = $weightages->less_than_8_month;
        $weightages_required_month[9] = $weightages->less_than_9_month;
        $weightages_required_month[10] = $weightages->less_than_10_month;
        $weightages_required_month[11] = $weightages->less_than_11_month;
        $weightages_required_month[12] = $weightages->less_than_12_month;

        $weightages_required_year[2] = $weightages->less_than_2_year;
        $weightages_required_year[3] = $weightages->less_than_3_year;
        $weightages_required_year[4] = $weightages->less_than_4_year;
        $weightages_required_year[5] = $weightages->less_than_5_year;
        $weightages_required_year[6] = $weightages->less_than_6_year;
        $weightages_required_year[7] = $weightages->less_than_7_year;
        $weightages_required_year[8] = $weightages->less_than_8_year;
        $weightages_required_year[9] = $weightages->less_than_9_year;
        $weightages_required_year[10] = $weightages->less_than_10_year;

        $avg_date = date_create($list_calculate_data->avg_listing_date);
        $current_date = date('Y-m-d');
        $current_date = date_create($current_date);
        $diff = date_diff($current_date, $avg_date);
        $difference_in_days = $diff->format("%a");
        $date_weightages = 0;

        $years_remaining = intval($difference_in_days / 365);
        $days_remaining = $difference_in_days % 365;

        if ($years_remaining > 1) {
            if ($days_remaining > 0) {

                $date_weightages = $weightages_required_year[$years_remaining + 1];
            } else {
                $date_weightages = $weightages_required_year[$years_remaining];
            }
        } else {
            $month_no = intval($days_remaining / 30);
            $days_remaining_month = $days_remaining % 30;
            if ($days_remaining_month > 0) {
                $date_weightages = $weightages_required_month[$month_no + 1];
            } else {
                $date_weightages = $weightages_required_month[$month_no];
            }
        }

        $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();

        $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);
        $difference_land_size = ($landsize * $inspection_data->built_up_area) - $model->land_size;
       if($list_calculate_data->avg_land_size > 0) {
           $difference_land_size_percentage = round(abs(($difference_land_size / $list_calculate_data->avg_land_size) * 100), 2);
       }else{
           $difference_land_size_percentage = 0;
       }
        $difference_bua = $inspection_data->built_up_area - $list_calculate_data->built_up_area_size;
        $difference_bua_percentage = round(abs(($difference_bua / $list_calculate_data->built_up_area_size) * 100),2);

        if (empty($drive_margin_saved) && $drive_margin_saved == null) {

            $selected_data_last_step = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'sold'])->all(), 'id', 'selected_id');

            $totalResults_average = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_no_of_bedrooms')
                ->from('sold_transaction')
                ->where(['id' => $selected_data_last_step])
                ->all();

            $drive_margin_saved_data = new ValuationDriveMv();

            $drive_margin_saved_data->average_location = $weightages->location_avg;
            $drive_margin_saved_data->average_age = $buildongs_info->estimated_age;
            $drive_margin_saved_data->average_tenure = $weightages->tenure_avg;
            $drive_margin_saved_data->average_view = $weightages->view_avg;
            $drive_margin_saved_data->average_finished_status = $weightages->finished_status_avg;
            $drive_margin_saved_data->average_property_condition = $weightages->property_condition_avg;
            $drive_margin_saved_data->average_upgrades = $weightages->upgrades_avg;
            // $drive_margin_saved_data->average_furnished = 3;
            $drive_margin_saved_data->average_property_exposure = $weightages->property_exposure_avg;
            $drive_margin_saved_data->average_property_placement = $weightages->property_exposure_avg;
           // $drive_margin_saved_data->average_full_building_floors = (isset($model->floor_number) ? round($model->floor_number, 2) : 0);;
            $drive_margin_saved_data->average_full_building_floors = (isset($inspection_data->full_building_floors) ? round($inspection_data->full_building_floors/2, 2) : 0);
            $drive_margin_saved_data->average_number_of_levels = $weightages->number_of_levels_avg;
            $drive_margin_saved_data->average_no_of_bedrooms = (isset($totalResults_average[0]['avg_no_of_bedrooms']) ? round($totalResults_average[0]['avg_no_of_bedrooms'], 2) : 0);;
            $drive_margin_saved_data->average_parking_space = $weightages->parking_space_avg;
            $drive_margin_saved_data->average_pool = $weightages->pool_avg;
            $drive_margin_saved_data->average_landscaping = $weightages->landscaping_avg;
            $drive_margin_saved_data->average_white_goods = $weightages->white_goods_avg;
            $drive_margin_saved_data->average_utilities_connected = $weightages->utilities_connected_avg;
            $drive_margin_saved_data->average_developer_margin = $weightages->developer_margin_avg;
           $drive_margin_saved_data->average_land_size = $weightages->land_size_avg;
            $drive_margin_saved_data->average_balcony_size = $weightages->balcony_size_avg;;
            $drive_margin_saved_data->average_built_up_area = $list_calculate_data->built_up_area_size;
            $drive_margin_saved_data->average_date = $list_calculate_data->avg_listing_date;
            $drive_margin_saved_data->average_sale_price = $list_calculate_data->avg_listings_price_size;;
            $drive_margin_saved_data->average_psf = $list_calculate_data->avg_psf;;

            $drive_margin_saved_data->weightage_location = $weightages->location;
            $drive_margin_saved_data->weightage_age = $weightages->age;
            $drive_margin_saved_data->weightage_tenure = $weightages->tenure;
            $drive_margin_saved_data->weightage_view = $weightages->view;
            $drive_margin_saved_data->weightage_finished_status = $weightages->finishing_status;
            $drive_margin_saved_data->weightage_property_condition = $weightages->upgrades;
            $drive_margin_saved_data->weightage_upgrades = $weightages->quality;
            $drive_margin_saved_data->weightage_furnished = $weightages->furnished;
            $drive_margin_saved_data->weightage_property_exposure = $weightages->property_exposure;
            $drive_margin_saved_data->weightage_property_placement = $weightages->property_placement;
            $drive_margin_saved_data->weightage_full_building_floors = $weightages->floor;
            $drive_margin_saved_data->weightage_number_of_levels = $weightages->number_of_levels;
            $drive_margin_saved_data->weightage_no_of_bedrooms = $weightages->bedrooom;
            $drive_margin_saved_data->weightage_parking_space = $weightages->parking;
            $drive_margin_saved_data->weightage_pool = $weightages->pool;
            $drive_margin_saved_data->weightage_landscaping = $weightages->landscape;
            $drive_margin_saved_data->weightage_white_goods = $weightages->white_goods;
            $drive_margin_saved_data->weightage_utilities_connected = $weightages->utilities_connected;
            $drive_margin_saved_data->weightage_developer_margin = $developer_margin->developer_margin_percentage;




            $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
            $drive_margin_saved_data->weightage_balcony_size = $weightages->balcony_size;
            $drive_margin_saved_data->weightage_built_up_area =  Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
            $drive_margin_saved_data->weightage_date = $date_weightages;
            $drive_margin_saved_data->valuation_id = $id;
            $drive_margin_saved_data->type = 'sold';
            if (!$drive_margin_saved_data->save()) {
            }
            $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();
        }

        $preCalculationArray = array();

        $preCalculationArray['location']['average'] = $drive_margin_saved->average_location;
       // $preCalculationArray['location']['subject_property'] = $inspection_data->location;
        $preCalculationArray['location']['subject_property'] = $location_attributes_value;
        $preCalculationArray['location']['difference'] = $preCalculationArray['location']['subject_property'] - $preCalculationArray['location']['average'];
        $preCalculationArray['location'] ['standard_weightage'] = $weightages->location;
        $preCalculationArray['location']['change_weightage'] = $drive_margin_saved->weightage_location;
        $preCalculationArray['location']['adjustments'] = round(($preCalculationArray['location']['difference'] * ($preCalculationArray['location']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['age']['average'] = $drive_margin_saved->average_age;
        $preCalculationArray['age']['subject_property'] = $inspection_data->estimated_age;
        $preCalculationArray['age']['difference'] = $preCalculationArray['age']['average'] - $preCalculationArray['age']['subject_property'];
        $preCalculationArray['age']['standard_weightage'] = $weightages->age;
        $preCalculationArray['age']['change_weightage'] = $drive_margin_saved->weightage_age;
        $preCalculationArray['age']['adjustments'] = round(($preCalculationArray['age']['difference'] * ($preCalculationArray['age']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['tenure']['average'] = $drive_margin_saved->average_tenure;
        $preCalculationArray['tenure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure]];
        $preCalculationArray['tenure']['difference'] = $preCalculationArray['tenure']['subject_property'] - $preCalculationArray['tenure']['average'];
        $preCalculationArray['tenure']['standard_weightage'] = $weightages->tenure;
        $preCalculationArray['tenure']['change_weightage'] = $drive_margin_saved->weightage_tenure;
        $preCalculationArray['tenure']['adjustments'] = round(($preCalculationArray['tenure']['difference'] * ($preCalculationArray['tenure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['view']['average'] = $drive_margin_saved->average_view;
       // $preCalculationArray['view']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->otherPropertyViewListArr[$inspection_data->view]];
        $preCalculationArray['view']['subject_property'] = $view_attributes_value;
        $preCalculationArray['view']['difference'] = $preCalculationArray['view']['subject_property'] - $preCalculationArray['view']['average'];
        $preCalculationArray['view']['standard_weightage'] = $weightages->view;
        $preCalculationArray['view']['change_weightage'] = $drive_margin_saved->weightage_view;
        $preCalculationArray['view']['adjustments'] = round(($preCalculationArray['view']['difference'] * ($preCalculationArray['view']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['finished_status']['average'] = $drive_margin_saved->average_finished_status;
        $preCalculationArray['finished_status']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->finished_status];
        $preCalculationArray['finished_status']['difference'] = $preCalculationArray['finished_status']['subject_property'] - $preCalculationArray['finished_status']['average'];
        $preCalculationArray['finished_status']['standard_weightage'] = $weightages->furnished;
        $preCalculationArray['finished_status']['change_weightage'] = $drive_margin_saved->weightage_finished_status;
        $preCalculationArray['finished_status']['adjustments'] = round(($preCalculationArray['finished_status']['difference'] * ($preCalculationArray['finished_status']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['property_condition']['average'] = $drive_margin_saved->average_property_condition;
        $preCalculationArray['property_condition']['subject_property'] = Yii::$app->appHelperFunctions->propertyConditionRatingtArr[$inspection_data->property_condition];
        $preCalculationArray['property_condition']['difference'] = $preCalculationArray['property_condition']['subject_property'] - $preCalculationArray['property_condition']['average'];
        $preCalculationArray['property_condition']['standard_weightage'] = $weightages->upgrades;
        $preCalculationArray['property_condition']['change_weightage'] = $drive_margin_saved->weightage_property_condition;
        $preCalculationArray['property_condition']['adjustments'] = round(($preCalculationArray['property_condition']['difference'] * ($preCalculationArray['property_condition']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));


        $preCalculationArray['upgrades']['average'] = $drive_margin_saved->average_upgrades;
        $preCalculationArray['upgrades']['subject_property'] = $model->valuationConfiguration->over_all_upgrade;
        $preCalculationArray['upgrades']['difference'] = $preCalculationArray['upgrades']['subject_property'] - $preCalculationArray['upgrades']['average'];
        $preCalculationArray['upgrades']['standard_weightage'] = $weightages->quality;
        $preCalculationArray['upgrades']['change_weightage'] = $drive_margin_saved->weightage_upgrades;
        $preCalculationArray['upgrades']['adjustments'] = round(($preCalculationArray['upgrades']['difference'] * ($preCalculationArray['upgrades']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));


        $preCalculationArray['property_exposure']['average'] = $drive_margin_saved->average_property_exposure;
        $preCalculationArray['property_exposure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyExposureListArr[$inspection_data->property_exposure]];
        $preCalculationArray['property_exposure']['difference'] = $preCalculationArray['property_exposure']['subject_property'] - $preCalculationArray['property_exposure']['average'];
        $preCalculationArray['property_exposure']['standard_weightage'] = $weightages->property_exposure;
        $preCalculationArray['property_exposure']['change_weightage'] = $drive_margin_saved->weightage_property_exposure;
        $preCalculationArray['property_exposure']['adjustments'] = round(($preCalculationArray['property_exposure']['difference'] * ($preCalculationArray['property_exposure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['property_placement']['average'] = $drive_margin_saved->average_property_placement;
        $preCalculationArray['property_placement']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyPlacementListArr[$inspection_data->property_placement]];
        $preCalculationArray['property_placement']['difference'] = $preCalculationArray['property_placement']['subject_property'] - $preCalculationArray['property_placement']['average'];
        $preCalculationArray['property_placement']['standard_weightage'] = $weightages->property_placement;
        $preCalculationArray['property_placement']['change_weightage'] = $drive_margin_saved->weightage_property_placement;
        $preCalculationArray['property_placement']['adjustments'] = round(($preCalculationArray['property_placement']['difference'] * ($preCalculationArray['property_placement']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['floors_adjustment']['average'] = $drive_margin_saved->average_full_building_floors;
        $preCalculationArray['floors_adjustment']['subject_property'] = (isset($model->floor_number) ? $model->floor_number : 0);
        $preCalculationArray['floors_adjustment']['difference'] = $preCalculationArray['floors_adjustment']['subject_property'] - $preCalculationArray['floors_adjustment']['average'];
        $preCalculationArray['floors_adjustment']['standard_weightage'] = $weightages->floor;
        $preCalculationArray['floors_adjustment']['change_weightage'] = $drive_margin_saved->weightage_full_building_floors;
        $preCalculationArray['floors_adjustment']['adjustments'] = round(($preCalculationArray['floors_adjustment']['difference'] * ($preCalculationArray['floors_adjustment']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['number_of_levels']['average'] = $drive_margin_saved->average_number_of_levels;
        $preCalculationArray['number_of_levels']['subject_property'] = $inspection_data->number_of_levels;
        $preCalculationArray['number_of_levels']['difference'] = $preCalculationArray['number_of_levels']['subject_property'] - $preCalculationArray['number_of_levels']['average'];
        $preCalculationArray['number_of_levels']['standard_weightage'] = $weightages->number_of_levels;
        $preCalculationArray['number_of_levels']['change_weightage'] = $drive_margin_saved->weightage_number_of_levels;
        $preCalculationArray['number_of_levels']['adjustments'] = round(($preCalculationArray['number_of_levels']['difference'] * ($preCalculationArray['number_of_levels']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['no_of_bedrooms']['average'] = $drive_margin_saved->average_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['subject_property'] = $inspection_data->no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['difference'] = $preCalculationArray['no_of_bedrooms']['subject_property'] - $preCalculationArray['no_of_bedrooms']['average'];
        $preCalculationArray['no_of_bedrooms']['standard_weightage'] = $weightages->bedrooom;
        $preCalculationArray['no_of_bedrooms']['change_weightage'] = $drive_margin_saved->weightage_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['adjustments'] = round(($preCalculationArray['no_of_bedrooms']['difference'] * ($preCalculationArray['no_of_bedrooms']['standard_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['parking_space']['average'] = $drive_margin_saved->average_parking_space;
        $preCalculationArray['parking_space']['subject_property'] = $inspection_data->parking_floors;
        $preCalculationArray['parking_space']['difference'] = $preCalculationArray['parking_space']['subject_property'] - $preCalculationArray['parking_space']['average'];
        $preCalculationArray['parking_space']['standard_weightage'] = $weightages->parking;
        $preCalculationArray['parking_space']['change_weightage'] = $drive_margin_saved->weightage_parking_space;
        $preCalculationArray['parking_space']['adjustments'] = round(($cost_info->parking_price * $preCalculationArray['parking_space']['difference']) * ($preCalculationArray['parking_space']['change_weightage'] / 100), 2);


        $preCalculationArray['pool']['average'] = $drive_margin_saved->average_pool;
        $preCalculationArray['pool']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->pool];
        $preCalculationArray['pool']['difference'] = $preCalculationArray['pool']['subject_property'] - $preCalculationArray['pool']['average'];
        $preCalculationArray['pool']['standard_weightage'] = $weightages->pool;
        $preCalculationArray['pool']['change_weightage'] = $drive_margin_saved->weightage_pool;
        $preCalculationArray['pool']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['pool']['difference']) * ($preCalculationArray['pool']['change_weightage'] / 100), 2);

        $preCalculationArray['landscaping']['average'] = $drive_margin_saved->average_landscaping;
        $preCalculationArray['landscaping']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->landscaping];
        $preCalculationArray['landscaping']['difference'] = $preCalculationArray['landscaping']['subject_property'] - $preCalculationArray['landscaping']['average'];
        $preCalculationArray['landscaping']['standard_weightage'] = $weightages->landscape;
        $preCalculationArray['landscaping']['change_weightage'] = $drive_margin_saved->weightage_landscaping;;
        $preCalculationArray['landscaping']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['landscaping']['difference']) * ($preCalculationArray['landscaping']['change_weightage'] / 100), 2);

        $preCalculationArray['white_goods']['average'] = $drive_margin_saved->average_white_goods;
        $preCalculationArray['white_goods']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->white_goods];
        $preCalculationArray['white_goods']['difference'] = $preCalculationArray['white_goods']['subject_property'] - $preCalculationArray['white_goods']['average'];
        $preCalculationArray['white_goods']['standard_weightage'] = $weightages->white_goods;
        $preCalculationArray['white_goods']['change_weightage'] = $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['change_weightage']= $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['white_goods']['difference']) * ($preCalculationArray['white_goods']['change_weightage'] / 100), 2);

        $preCalculationArray['utilities_connected']['average'] = $drive_margin_saved->average_utilities_connected;
        $preCalculationArray['utilities_connected']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->utilities_connected];
        $preCalculationArray['utilities_connected']['difference'] = $preCalculationArray['utilities_connected']['subject_property'] - $preCalculationArray['utilities_connected']['average'];
        $preCalculationArray['utilities_connected']['standard_weightage'] = $weightages->utilities_connected;
        $preCalculationArray['utilities_connected']['change_weightage']= $drive_margin_saved->weightage_utilities_connected;
        $preCalculationArray['utilities_connected']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['utilities_connected']['difference']) * ($preCalculationArray['utilities_connected']['change_weightage'] / 100), 2);

        $preCalculationArray['developer_margin']['average'] = 0;
        $preCalculationArray['developer_margin']['subject_property'] = 0;
        $preCalculationArray['developer_margin']['difference'] = 0;
        $preCalculationArray['developer_margin']['standard_weightage'] = 0;
        $preCalculationArray['developer_margin']['change_weightage'] = $drive_margin_saved->weightage_developer_margin;
        $preCalculationArray['developer_margin']['adjustments'] = round((($drive_margin_saved->weightage_developer_margin / 100) * $list_calculate_data->avg_listings_price_size), 2);

        $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);

        $preCalculationArray['land_size']['average'] = $landsize * $inspection_data->built_up_area;
        $preCalculationArray['land_size']['subject_property'] = $model->land_size;
        $preCalculationArray['land_size']['difference'] = $preCalculationArray['land_size']['subject_property'] - $preCalculationArray['land_size']['average'];
        $preCalculationArray['land_size']['standard_weightage'] =  Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
        $preCalculationArray['land_size']['change_weightage'] = $drive_margin_saved->weightage_land_size;
        $preCalculationArray['land_size']['adjustments'] = round(($preCalculationArray['land_size']['difference'] * ($preCalculationArray['land_size']['change_weightage'] / 100) * $cost_info->lands_price));

        $preCalculationArray['balcony_size']['average'] = $drive_margin_saved->average_balcony_size;
        $preCalculationArray['balcony_size']['subject_property'] = $inspection_data->balcony_size;
        $preCalculationArray['balcony_size']['difference'] = $preCalculationArray['balcony_size']['subject_property'] - $preCalculationArray['balcony_size']['average'];
        $preCalculationArray['balcony_size']['standard_weightage'] = $weightages->balcony_size;
        $preCalculationArray['balcony_size']['change_weightage'] = $drive_margin_saved->weightage_balcony_size;
        $preCalculationArray['balcony_size']['adjustments'] = round(($preCalculationArray['balcony_size']['difference'] * ($preCalculationArray['balcony_size']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['built_up_area']['average'] = $drive_margin_saved->average_built_up_area;
        $preCalculationArray['built_up_area']['subject_property'] = $inspection_data->built_up_area;
        $preCalculationArray['built_up_area']['difference'] = $preCalculationArray['built_up_area']['subject_property'] - $preCalculationArray['built_up_area']['average'];
        $preCalculationArray['built_up_area']['standard_weightage'] =  Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
        $preCalculationArray['built_up_area']['change_weightage'] = $drive_margin_saved->weightage_built_up_area;
        $preCalculationArray['built_up_area']['adjustments'] = round(($preCalculationArray['built_up_area']['difference'] * ($preCalculationArray['built_up_area']['change_weightage'] / 100) * $list_calculate_data->avg_psf));

        $preCalculationArray['date']['average'] = $drive_margin_saved->average_date;
        $preCalculationArray['date']['subject_property'] = date('Y-m-d');
        $preCalculationArray['date']['difference'] = $difference_in_days;
        $preCalculationArray['date']['standard_weightage'] = $date_weightages;
        $preCalculationArray['date']['change_weightage'] = $drive_margin_saved->weightage_date;
        $preCalculationArray['date']['adjustments'] = round((($preCalculationArray['date']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $sold_avg_psf = round($list_calculate_data->avg_psf / $inspection_data->built_up_area, 2);


        $sold_adjustments = 0;

        foreach ($preCalculationArray as $calucation) {
            $sold_adjustments = $sold_adjustments + $calucation['adjustments'];
        }
        $total_estimate_price = $list_calculate_data->avg_listings_price_size + $sold_adjustments;
        $sold_avg_psf = round($total_estimate_price / $inspection_data->built_up_area, 2);

        $drive_margin_saved->mv_total_price = $total_estimate_price;
        $drive_margin_saved->mv_avg_psf = $sold_avg_psf;
        $drive_margin_saved->save();

        $drive_margin_saved_updated = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();

        if ($readonly==1) {

            return $this->renderPartial('steps/_step12', [
                'model' => $drive_margin_saved_updated,
                'valuation' => $model,
                'preCalculationArray' => $preCalculationArray,
                'avg_psf' => $list_calculate_data->avg_psf,
                'avg_listings_price_size' => $list_calculate_data->avg_listings_price_size,
                'sold_avg_psf' => $sold_avg_psf,
                'inspection_data' => $inspection_data,
                'readonly' => $readonly,
            ]);
        }

        return $this->render('steps/_step12', [
            'model' => $drive_margin_saved_updated,
            'valuation' => $model,
            'preCalculationArray' => $preCalculationArray,
            'avg_psf' => $list_calculate_data->avg_psf,
            'avg_listings_price_size' => $list_calculate_data->avg_listings_price_size,
            'sold_avg_psf' => $sold_avg_psf,
            'inspection_data' => $inspection_data,
            'readonly' => $readonly,
        ]);

    }
    /* End sold calculations*/

    /* Start list claculations*/

    public function actionStep_13($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_13')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $valuation = $this->findModel($id);
        $model = ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'list'])->one();
        if ($model !== null) {
        } else {
            $model = new ValuationEnquiry();
            $model->date_from = '2020-01-01';
            $model->date_to = date('Y-m-d');
            $model->building_info = $valuation->building_info;
            $model->bedroom_from = $valuation->inspectProperty->no_of_bedrooms;
            $model->bedroom_to = $valuation->inspectProperty->no_of_bedrooms;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            $model->type = 'list';
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_13/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step13', [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_14($id,$readonly=0)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_14')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            if (isset($post['selection']) && $post['selection'] <> null) {
                ValuationSelectedLists::deleteAll(['valuation_id' => $id, 'type' => 'list']);
                foreach ($post['selection'] as $selected_row) {
                    $selected_data_detail = new ValuationSelectedLists();
                    $selected_data_detail->selected_id = $selected_row;
                    $selected_data_detail->type = 'list';
                    $selected_data_detail->valuation_id = $id;
                    $selected_data_detail->save();
                }

            }
        }

        $selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'list'])->all(), 'id', 'selected_id');

        $totalResults = (new \yii\db\Query())
            ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) avg_listings_price_size, MIN(listings_price) as min_price, MIN(price_per_sqt) as min_price_sqt, MAX(listings_price) as max_price, MAX(price_per_sqt) as max_price_sqt, AVG(gross_yield) as avg_gross_yield, FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(listing_date))) as avg_listing_date')
            ->from('listings_transactions')
            ->where(['id' => $selected_data])
            ->all();

        //Average and calculations

        if (!empty($totalResults) && $totalResults <> null) {
            if (isset($totalResults[0]['avg_bedrooms'])) {
                ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'list']);
                $calculations = new ValuationListCalculation();
                $calculations->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                $calculations->avg_land_size = round($totalResults[0]['avg_land_size']);
                $calculations->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                $calculations->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                $calculations->min_price = round($totalResults[0]['min_price']);
                $calculations->max_price = round($totalResults[0]['max_price']);
                $calculations->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                $calculations->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                $calculations->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                $calculations->avg_psf = round($calculations->avg_listings_price_size / $calculations->built_up_area_size);
                $calculations->avg_gross_yield = $totalResults[0]['avg_gross_yield'];
                $calculations->type = 'list';
                $calculations->valuation_id = $id;
                $calculations->save();
            }

        }
        $select_calculations = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'list'])->one();

        // $list_selected_records = \app\models\ValuationSelectedLists::find()->where(['selected_id' => $selected_data,'type'=> 'sold'])->all();

        // selected_data
        $list_selected_records = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'list'])->one();


        $listing_filter = array();
        if (!empty($list_selected_records) && $list_selected_records) {

            $listing_filter['ListingsTransactionsSearch']['date_from'] = $list_selected_records->date_from;
            $listing_filter['ListingsTransactionsSearch']['date_to'] = $list_selected_records->date_to;
            $listing_filter['ListingsTransactionsSearch']['building_info_data'] = $list_selected_records->building_info;
            $listing_filter['ListingsTransactionsSearch']['property_id'] = $list_selected_records->property_id;
            $listing_filter['ListingsTransactionsSearch']['property_category'] = $list_selected_records->property_category;
            $listing_filter['ListingsTransactionsSearch']['bedroom_from'] = $list_selected_records->bedroom_from;
            $listing_filter['ListingsTransactionsSearch']['bedroom_to'] = $list_selected_records->bedroom_to;
            $listing_filter['ListingsTransactionsSearch']['landsize_from'] = $list_selected_records->landsize_from;
            $listing_filter['ListingsTransactionsSearch']['landsize_to'] = $list_selected_records->landsize_to;
            $listing_filter['ListingsTransactionsSearch']['bua_from'] = $list_selected_records->bua_from;
            $listing_filter['ListingsTransactionsSearch']['bua_to'] = $list_selected_records->bua_to;
            $listing_filter['ListingsTransactionsSearch']['price_from'] = $list_selected_records->price_from;
            $listing_filter['ListingsTransactionsSearch']['price_to'] = $list_selected_records->price_to;
            $listing_filter['ListingsTransactionsSearch']['price_psf_from'] = $list_selected_records->price_psf_from;
            $listing_filter['ListingsTransactionsSearch']['price_psf_to'] = $list_selected_records->price_psf_to;
        }

        $selected_records_display = $list_selected_records;

        if ($selected_records_display <> null & !empty($selected_records_display)) {
            $building_info_data = '';

            if ($selected_records_display->building_info <> null) {
                $buildings = explode(",", $list_selected_records->building_info);
                $all_buildings_titles = Buildings::find()->where(['id' => $buildings])->all();

                if ($all_buildings_titles <> null && !empty($all_buildings_titles)) {
                    foreach ($all_buildings_titles as $title) {
                        $building_info_data .= $title['title'] . ',&nbsp';
                    }

                }

            }
            $selected_records_display->building_info = $building_info_data;
            $property_data = Properties::find()->where(['id' => $list_selected_records->property_id])->one();
            if ($property_data <> null && !empty($property_data)) {
                $selected_records_display->property_id = $property_data->title;
            }
            $property_data = Properties::find()->where(['id' => $list_selected_records->property_id])->one();
            if ($selected_records_display->property_category <> null) {
                $selected_records_display->property_category = Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$sold_selected_records_display->property_category];
            }
        }

        $valuation = $this->findModel($id);
        $searchModel = new ListingsTransactionsSearch();
        $dataProvider = $searchModel->searchvaluation($listing_filter);

        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;



        if ($readonly==1) {
            $dataProvider->sort = false;
            return $this->renderPartial('steps/_step14', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'valuation' => $valuation,
                'selected_data' => $selected_data,
                'totalResults' => $totalResults,
                'select_calculations' => $select_calculations,
                'selected_records_display' => $selected_records_display,
                'readonly' => $readonly
            ]);
        }

        return $this->render('steps/_step14', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'valuation' => $valuation,
            'selected_data' => $selected_data,
            'totalResults' => $totalResults,
            'select_calculations' => $select_calculations,
            'selected_records_display' => $selected_records_display,
            'readonly' => $readonly

        ]);

    }

    public function actionStep_15($id,$readonly=0)
    {

        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_15')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $model = $this->findModel($id);


        if (Yii::$app->request->post()) {
            $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'list'])->one();
            if ($drive_margin_saved->load(Yii::$app->request->post())) {

                if ($drive_margin_saved->save()) {
                    Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                    // return $this->redirect(['index']);
                    return $this->redirect(['valuation/step_15/' . $id]);
                } else {
                    if ($drive_margin_saved->hasErrors()) {
                        foreach ($drive_margin_saved->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }


        $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();

        $location_attributes_value = Yii::$app->appHelperFunctions->getLocationAttributes($inspection_data);
        $view_attributes_value = Yii::$app->appHelperFunctions->getViewAttributes($inspection_data);

        $list_calculate_data = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'list'])->one();
        $weightages = \app\models\Weightages::find()->where(['property_type' => $model->property_id])->one();
        $buildongs_info = \app\models\Buildings::find()->where(['id' => $model->building_info])->one();
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $developer_margin = \app\models\ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();
        $sales_discount = \app\models\SaleDiscount::find()->one();


        $weightages_required_month = array();
        $weightages_required_year = array();

        $weightages_required_month[1] = $weightages->less_than_1_month;
        $weightages_required_month[2] = $weightages->less_than_2_month;
        $weightages_required_month[3] = $weightages->less_than_3_month;
        $weightages_required_month[4] = $weightages->less_than_4_month;
        $weightages_required_month[5] = $weightages->less_than_5_month;
        $weightages_required_month[6] = $weightages->less_than_6_month;
        $weightages_required_month[7] = $weightages->less_than_7_month;
        $weightages_required_month[8] = $weightages->less_than_8_month;
        $weightages_required_month[9] = $weightages->less_than_9_month;
        $weightages_required_month[10] = $weightages->less_than_10_month;
        $weightages_required_month[11] = $weightages->less_than_11_month;
        $weightages_required_month[12] = $weightages->less_than_12_month;

        $weightages_required_year[2] = $weightages->less_than_2_year;
        $weightages_required_year[3] = $weightages->less_than_3_year;
        $weightages_required_year[4] = $weightages->less_than_4_year;
        $weightages_required_year[5] = $weightages->less_than_5_year;
        $weightages_required_year[6] = $weightages->less_than_6_year;
        $weightages_required_year[7] = $weightages->less_than_7_year;
        $weightages_required_year[8] = $weightages->less_than_8_year;
        $weightages_required_year[9] = $weightages->less_than_9_year;
        $weightages_required_year[10] = $weightages->less_than_10_year;

        $avg_date = date_create($list_calculate_data->avg_listing_date);
        $current_date = date('Y-m-d');
        $current_date = date_create($current_date);
        $diff = date_diff($current_date, $avg_date);
        $difference_in_days = $diff->format("%a");
        $date_weightages = 0;

        $years_remaining = intval($difference_in_days / 365);
        $days_remaining = $difference_in_days % 365;

        if ($years_remaining > 1) {
            if ($days_remaining > 0) {

                $date_weightages = $weightages_required_year[$years_remaining + 1];
            } else {
                $date_weightages = $weightages_required_year[$years_remaining];
            }
        } else {
            $month_no = intval($days_remaining / 30);
            $days_remaining_month = $days_remaining % 30;
            if ($days_remaining_month > 0) {
                $date_weightages = $weightages_required_month[$month_no + 1];
            } else {
                $date_weightages = $weightages_required_month[$month_no];
            }
        }

        $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'list'])->one();

        $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);
        $difference_land_size = ($landsize * $inspection_data->built_up_area) - $model->land_size;
        if($list_calculate_data->avg_land_size > 0) {
            $difference_land_size_percentage = round(abs(($difference_land_size / $list_calculate_data->avg_land_size) * 100), 2);
        }else{
            $difference_land_size_percentage = 0;
        }
        $difference_bua = $inspection_data->built_up_area - $list_calculate_data->built_up_area_size;
        $difference_bua_percentage = round(abs(($difference_bua / $list_calculate_data->built_up_area_size) * 100),2);

        if (empty($drive_margin_saved) && $drive_margin_saved == null) {


            $selected_data_last_step = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'list'])->all(), 'id', 'selected_id');




            $totalResults_average = (new \yii\db\Query())
                ->select('
            AVG(land_size) as avg_land_size,
            AVG(built_up_area) as built_up_area_size,
            AVG(tenure) as avg_tenure,
            AVG(estimated_age) as avg_estimated_age,
            AVG(upgrades) as avg_upgrades,
            AVG (CASE 
            WHEN finished_status = "Shell & Core" THEN 1 
            WHEN finished_status = "Fitted" THEN 2
            ELSE 0 END) AS avg_finished_status,
            AVG(property_condition) as avg_property_condition,
            AVG(property_exposure) as avg_property_exposure,
            AVG(property_placement) as avg_property_placement,
            AVG(floor_number) as avg_full_building_floors,
            AVG(number_of_levels) as avg_number_of_levels,
            AVG(no_of_bedrooms) as avg_no_of_bedrooms,
            AVG(parking_space) as avg_parking_space,            
            AVG (CASE 
            WHEN pool = "Yes" THEN 2 
            ELSE 1 END) AS avg_pool,            
            AVG (CASE 
            WHEN landscaping = "Yes" THEN 2 
            WHEN landscaping = "No" THEN 1
            WHEN landscaping = "Semi-Landscape" THEN 1.5
            ELSE 0 END) AS avg_landscaping,
            AVG (CASE 
            WHEN white_goods = "Yes" THEN 2 
            ELSE 1 END) AS avg_white_goods,          
            AVG (CASE 
            WHEN utilities_connected = "Yes" THEN 2 
            ELSE 1 END) AS avg_utilities_connected,            
            AVG (CASE 
            WHEN developer_margin = "Yes" THEN 2 
            ELSE 1 END) AS avg_developer_margin,          
            AVG(land_size) as avg_land_size,
            AVG(balcony_size) as avg_balcony_size')
                ->from('listings_transactions')
                ->where(['id' => $selected_data_last_step])
                ->all();


            $location_avg_attributes_value = 0;
            $view_avg_attributes_value = 0;

            if(!empty($selected_data_last_step)){
                foreach ($selected_data_last_step as $item){
                    $list_data = \app\models\ListingsTransactions::find()->where(['id' => $item])->one();
                    $location_avg_attributes_value = $location_avg_attributes_value + Yii::$app->appHelperFunctions->getLocationAttributes($list_data);
                    $view_avg_attributes_value = $view_avg_attributes_value + Yii::$app->appHelperFunctions->getViewAttributes($list_data);

                }

                $location_avg_attributes_value = round(($location_avg_attributes_value/count($selected_data_last_step)), 2);
                $view_avg_attributes_value = round(($view_avg_attributes_value/count($selected_data_last_step)), 2);
            }



            $drive_margin_saved_data = new ValuationDriveMv();

            $drive_margin_saved_data->average_location = (isset($totalResults_average[0]['avg_location']) ? round($totalResults_average[0]['avg_location'], 2) : $location_avg_attributes_value);
            $drive_margin_saved_data->average_age = (isset($totalResults_average[0]['avg_estimated_age']) ? round($totalResults_average[0]['avg_estimated_age'], 2) : 0);
            $drive_margin_saved_data->average_tenure = (isset($totalResults_average[0]['avg_tenure']) ? round($totalResults_average[0]['avg_tenure'], 2) : 0);
            $drive_margin_saved_data->average_view = (isset($totalResults_average[0]['avg_view']) ? round($totalResults_average[0]['avg_view'], 2) : $view_avg_attributes_value);
            $drive_margin_saved_data->average_finished_status = (isset($totalResults_average[0]['avg_finished_status']) ? round($totalResults_average[0]['avg_finished_status'], 2) : 0);
            $drive_margin_saved_data->average_property_condition = (isset($totalResults_average[0]['avg_property_condition']) ? round($totalResults_average[0]['avg_property_condition'], 2) : 0);
            $drive_margin_saved_data->average_upgrades = (isset($totalResults_average[0]['avg_upgrades']) ? round($totalResults_average[0]['avg_upgrades'], 2) : 0);
            // $drive_margin_saved_data->average_furnished = 3;
            $drive_margin_saved_data->average_property_exposure = (isset($totalResults_average[0]['avg_property_exposure']) ? round($totalResults_average[0]['avg_property_exposure'], 2) : 0);
            $drive_margin_saved_data->average_property_placement = (isset($totalResults_average[0]['avg_property_placement']) ? round($totalResults_average[0]['avg_property_placement'], 2) : 0);
            //$drive_margin_saved_data->average_full_building_floors = (isset($model->floor_number) ? round($model->floor_number, 2) : 0);
            //$drive_margin_saved_data->average_full_building_floors = (isset($inspection_data->full_building_floors) ? $inspection_data->full_building_floors : 0);
            $drive_margin_saved_data->average_full_building_floors =(isset($totalResults_average[0]['avg_full_building_floors']) ? round($totalResults_average[0]['avg_full_building_floors'], 2) : 0);;
            $drive_margin_saved_data->average_number_of_levels = (isset($totalResults_average[0]['avg_number_of_levels']) ? round($totalResults_average[0]['avg_number_of_levels'], 2) : 0);
            $drive_margin_saved_data->average_no_of_bedrooms = (isset($totalResults_average[0]['avg_no_of_bedrooms']) ? round($totalResults_average[0]['avg_no_of_bedrooms'], 2) : 0);
            $drive_margin_saved_data->average_parking_space = (isset($totalResults_average[0]['avg_parking_space']) ? round($totalResults_average[0]['avg_parking_space'], 2) : 0);
            $drive_margin_saved_data->average_pool = (isset($totalResults_average[0]['avg_pool']) ? round($totalResults_average[0]['avg_pool'], 2) : 0);
            $drive_margin_saved_data->average_landscaping = (isset($totalResults_average[0]['avg_landscaping']) ? round($totalResults_average[0]['avg_landscaping'], 2) : 0);
            $drive_margin_saved_data->average_white_goods = (isset($totalResults_average[0]['avg_white_goods']) ? round($totalResults_average[0]['avg_white_goods'], 2) : 0);
            $drive_margin_saved_data->average_utilities_connected = (isset($totalResults_average[0]['avg_utilities_connected']) ? round($totalResults_average[0]['avg_utilities_connected'], 2) : 0);
            $drive_margin_saved_data->average_developer_margin = (isset($totalResults_average[0]['avg_developer_margin']) ? round($totalResults_average[0]['avg_developer_margin'], 2) : 0);
            $drive_margin_saved_data->average_land_size = (isset($totalResults_average[0]['avg_land_size']) ? round($totalResults_average[0]['avg_land_size'], 2) : 0);
            $drive_margin_saved_data->average_balcony_size = $inspection_data->balcony_size;;
            $drive_margin_saved_data->average_built_up_area = $list_calculate_data->built_up_area_size;
            $drive_margin_saved_data->average_date = $list_calculate_data->avg_listing_date;
            $drive_margin_saved_data->average_sale_price = $list_calculate_data->avg_listings_price_size;;
            $drive_margin_saved_data->average_psf = $list_calculate_data->avg_psf;;

            $drive_margin_saved_data->weightage_location = $weightages->location;
            $drive_margin_saved_data->weightage_age = $weightages->age;
            $drive_margin_saved_data->weightage_tenure = $weightages->tenure;
            $drive_margin_saved_data->weightage_view = $weightages->view;
            $drive_margin_saved_data->weightage_finished_status = $weightages->finishing_status;
            $drive_margin_saved_data->weightage_property_condition = $weightages->upgrades;
            $drive_margin_saved_data->weightage_upgrades = $weightages->quality;
            $drive_margin_saved_data->weightage_furnished = $weightages->furnished;
            $drive_margin_saved_data->weightage_property_exposure = $weightages->property_exposure;
            $drive_margin_saved_data->weightage_property_placement = $weightages->property_placement;
            $drive_margin_saved_data->weightage_full_building_floors = $weightages->floor;
            $drive_margin_saved_data->weightage_number_of_levels = $weightages->number_of_levels;
            $drive_margin_saved_data->weightage_no_of_bedrooms = $weightages->bedrooom;
            $drive_margin_saved_data->weightage_parking_space = $weightages->parking;
            $drive_margin_saved_data->weightage_pool = $weightages->pool;
            $drive_margin_saved_data->weightage_landscaping = $weightages->landscape;
            $drive_margin_saved_data->weightage_white_goods = $weightages->white_goods;
            $drive_margin_saved_data->weightage_utilities_connected = $weightages->utilities_connected;
            $drive_margin_saved_data->weightage_developer_margin = $developer_margin->developer_margin_percentage;


            $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
            $drive_margin_saved_data->weightage_balcony_size = $weightages->balcony_size;
            $drive_margin_saved_data->weightage_built_up_area =  Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
            $drive_margin_saved_data->weightage_date = $date_weightages;
            $drive_margin_saved_data->valuation_id = $id;
            $drive_margin_saved_data->type = 'list';
            if (!$drive_margin_saved_data->save()) {
            }
            $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'list'])->one();
        }


        $preCalculationArray = array();

        $preCalculationArray['location']['average'] = $drive_margin_saved->average_location;
        //$preCalculationArray['location']['subject_property'] = $inspection_data->location;
        $preCalculationArray['location']['subject_property'] = $location_attributes_value;
        $preCalculationArray['location']['difference'] = $preCalculationArray['location']['subject_property'] - $preCalculationArray['location']['average'];
        $preCalculationArray['location'] ['standard_weightage'] = $weightages->location;
        $preCalculationArray['location']['change_weightage'] = $drive_margin_saved->weightage_location;
        $preCalculationArray['location']['adjustments'] = round(($preCalculationArray['location']['difference'] * ($preCalculationArray['location']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['age']['average'] = $drive_margin_saved->average_age;
        $preCalculationArray['age']['subject_property'] = $inspection_data->estimated_age;
        $preCalculationArray['age']['difference'] = $preCalculationArray['age']['average'] - $preCalculationArray['age']['subject_property'];
        $preCalculationArray['age']['standard_weightage'] = $weightages->age;
        $preCalculationArray['age']['change_weightage'] = $drive_margin_saved->weightage_age;
        $preCalculationArray['age']['adjustments'] = round(($preCalculationArray['age']['difference'] * ($preCalculationArray['age']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['tenure']['average'] = $drive_margin_saved->average_tenure;
        // $preCalculationArray['tenure']['subject_property']=  $model->tenure;
        $preCalculationArray['tenure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure]];
        $preCalculationArray['tenure']['difference'] = $preCalculationArray['tenure']['subject_property'] - $preCalculationArray['tenure']['average'];
        $preCalculationArray['tenure']['standard_weightage'] =  $weightages->tenure;
        $preCalculationArray['tenure']['change_weightage'] = $drive_margin_saved->weightage_tenure;
        $preCalculationArray['tenure']['adjustments'] = round(($preCalculationArray['tenure']['difference'] * ($preCalculationArray['tenure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['view']['average'] = $drive_margin_saved->average_view;
        //$preCalculationArray['view']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->otherPropertyViewListArr[$inspection_data->view]];
        $preCalculationArray['view']['subject_property'] = $view_attributes_value;
        $preCalculationArray['view']['difference'] = $preCalculationArray['view']['subject_property'] - $preCalculationArray['view']['average'];
        $preCalculationArray['view']['standard_weightage'] = $weightages->view;
        $preCalculationArray['view']['change_weightage'] = $drive_margin_saved->weightage_view;
        $preCalculationArray['view']['adjustments'] = round(($preCalculationArray['view']['difference'] * ($preCalculationArray['view']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['finished_status']['average'] = $drive_margin_saved->average_finished_status;
        $preCalculationArray['finished_status']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->finished_status];
        $preCalculationArray['finished_status']['difference'] = $preCalculationArray['finished_status']['subject_property'] - $preCalculationArray['finished_status']['average'];
        $preCalculationArray['finished_status']['standard_weightage'] = $weightages->furnished;
        $preCalculationArray['finished_status']['change_weightage'] = $drive_margin_saved->weightage_finished_status;
        $preCalculationArray['finished_status']['adjustments'] = round(($preCalculationArray['finished_status']['difference'] * ($preCalculationArray['finished_status']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['property_condition']['average'] = $drive_margin_saved->average_property_condition;
        $preCalculationArray['property_condition']['subject_property'] = Yii::$app->appHelperFunctions->propertyConditionRatingtArr[$inspection_data->property_condition];
        $preCalculationArray['property_condition']['difference'] = $preCalculationArray['property_condition']['subject_property'] - $preCalculationArray['property_condition']['average'];
        $preCalculationArray['property_condition']['standard_weightage'] = $weightages->upgrades;
        $preCalculationArray['property_condition']['change_weightage'] = $drive_margin_saved->weightage_property_condition;
        $preCalculationArray['property_condition']['adjustments'] = round(($preCalculationArray['property_condition']['difference'] * ($preCalculationArray['property_condition']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['upgrades']['average'] = $drive_margin_saved->average_upgrades;
        $preCalculationArray['upgrades']['subject_property'] = $model->valuationConfiguration->over_all_upgrade;
        $preCalculationArray['upgrades']['difference'] = $preCalculationArray['upgrades']['subject_property'] - $preCalculationArray['upgrades']['average'];
        $preCalculationArray['upgrades']['standard_weightage'] = $weightages->quality;
        $preCalculationArray['upgrades']['change_weightage'] = $drive_margin_saved->weightage_upgrades;
        $preCalculationArray['upgrades']['adjustments'] = round(($preCalculationArray['upgrades']['difference'] * ($preCalculationArray['upgrades']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));


        $preCalculationArray['property_exposure']['average'] = $drive_margin_saved->average_property_exposure;
        $preCalculationArray['property_exposure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyExposureListArr[$inspection_data->property_exposure]];
        $preCalculationArray['property_exposure']['difference'] = $preCalculationArray['property_exposure']['subject_property'] - $preCalculationArray['property_exposure']['average'];
        $preCalculationArray['property_exposure']['standard_weightage'] = $weightages->property_exposure;
        $preCalculationArray['property_exposure']['change_weightage'] = $drive_margin_saved->weightage_property_exposure;
        $preCalculationArray['property_exposure']['adjustments'] = round(($preCalculationArray['property_exposure']['difference'] * ($preCalculationArray['property_exposure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['property_placement']['average'] = $drive_margin_saved->average_property_placement;
        $preCalculationArray['property_placement']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyPlacementListArr[$inspection_data->property_placement]];
        $preCalculationArray['property_placement']['difference'] = $preCalculationArray['property_placement']['subject_property'] - $preCalculationArray['property_placement']['average'];
        $preCalculationArray['property_placement']['standard_weightage'] = $weightages->property_placement;
        $preCalculationArray['property_placement']['change_weightage'] = $drive_margin_saved->weightage_property_placement;
        $preCalculationArray['property_placement']['adjustments'] = round(($preCalculationArray['property_placement']['difference'] * ($preCalculationArray['property_placement']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['floors_adjustment']['average'] = $drive_margin_saved->average_full_building_floors;
        $preCalculationArray['floors_adjustment']['subject_property'] = (isset($model->floor_number) ? $model->floor_number : 0);
        $preCalculationArray['floors_adjustment']['difference'] = $preCalculationArray['floors_adjustment']['subject_property'] - $preCalculationArray['floors_adjustment']['average'];
        $preCalculationArray['floors_adjustment']['standard_weightage'] = $weightages->floor;
        $preCalculationArray['floors_adjustment']['change_weightage'] = $drive_margin_saved->weightage_full_building_floors;
        $preCalculationArray['floors_adjustment']['adjustments'] = round(($preCalculationArray['floors_adjustment']['difference'] * ($preCalculationArray['floors_adjustment']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['number_of_levels']['average'] = $drive_margin_saved->average_number_of_levels;
        $preCalculationArray['number_of_levels']['subject_property'] = $inspection_data->number_of_levels;
        $preCalculationArray['number_of_levels']['difference'] = $preCalculationArray['number_of_levels']['subject_property'] - $preCalculationArray['number_of_levels']['average'];
        $preCalculationArray['number_of_levels']['standard_weightage'] = $weightages->number_of_levels;
        $preCalculationArray['number_of_levels']['change_weightage'] = $drive_margin_saved->weightage_number_of_levels;
        $preCalculationArray['number_of_levels']['adjustments'] = round(($preCalculationArray['number_of_levels']['difference'] * ($preCalculationArray['number_of_levels']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['no_of_bedrooms']['average'] = $drive_margin_saved->average_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['subject_property'] = $inspection_data->no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['difference'] = $preCalculationArray['no_of_bedrooms']['subject_property'] - $preCalculationArray['no_of_bedrooms']['average'];
        $preCalculationArray['no_of_bedrooms']['standard_weightage'] = $weightages->bedrooom;
        $preCalculationArray['no_of_bedrooms']['change_weightage'] = $drive_margin_saved->weightage_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['adjustments'] = round(($preCalculationArray['no_of_bedrooms']['difference'] * ($preCalculationArray['no_of_bedrooms']['standard_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['parking_space']['average'] = $drive_margin_saved->average_parking_space;
        $preCalculationArray['parking_space']['subject_property'] = $inspection_data->parking_floors;
        $preCalculationArray['parking_space']['difference'] = $preCalculationArray['parking_space']['subject_property'] - $preCalculationArray['parking_space']['average'];
        $preCalculationArray['parking_space']['standard_weightage'] = $weightages->parking;
        $preCalculationArray['parking_space']['change_weightage'] = $drive_margin_saved->weightage_parking_space;
        $preCalculationArray['parking_space']['adjustments'] = round(($cost_info->parking_price * $preCalculationArray['parking_space']['difference']) * ($preCalculationArray['parking_space']['change_weightage'] / 100), 2);


        $preCalculationArray['pool']['average'] = $drive_margin_saved->average_pool;
        $preCalculationArray['pool']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->pool];
        $preCalculationArray['pool']['difference'] = $preCalculationArray['pool']['subject_property'] - $preCalculationArray['pool']['average'];
        $preCalculationArray['pool']['standard_weightage'] = $weightages->pool;
        $preCalculationArray['pool']['change_weightage'] = $drive_margin_saved->weightage_pool;
        $preCalculationArray['pool']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['pool']['difference']) * ($preCalculationArray['pool']['change_weightage'] / 100), 2);

        $preCalculationArray['landscaping']['average'] = $drive_margin_saved->average_landscaping;
        $preCalculationArray['landscaping']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->landscaping];
        $preCalculationArray['landscaping']['difference'] = $preCalculationArray['landscaping']['subject_property'] - $preCalculationArray['landscaping']['average'];
        $preCalculationArray['landscaping']['standard_weightage'] = $weightages->landscape;
        $preCalculationArray['landscaping']['change_weightage'] = $drive_margin_saved->weightage_landscaping;;
        $preCalculationArray['landscaping']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['landscaping']['difference']) * ($preCalculationArray['landscaping']['change_weightage'] / 100), 2);

        $preCalculationArray['white_goods']['average'] = $drive_margin_saved->average_white_goods;
        $preCalculationArray['white_goods']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->white_goods];
        $preCalculationArray['white_goods']['difference'] = $preCalculationArray['white_goods']['subject_property'] - $preCalculationArray['white_goods']['average'];
        $preCalculationArray['white_goods']['standard_weightage'] = $weightages->white_goods;
        $preCalculationArray['white_goods']['change_weightage'] = $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['change_weightage']= $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['white_goods']['difference']) * ($preCalculationArray['white_goods']['change_weightage'] / 100), 2);

        $preCalculationArray['utilities_connected']['average'] = $drive_margin_saved->average_utilities_connected;
        $preCalculationArray['utilities_connected']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->utilities_connected];
        $preCalculationArray['utilities_connected']['difference'] = $preCalculationArray['utilities_connected']['subject_property'] - $preCalculationArray['utilities_connected']['average'];
        $preCalculationArray['utilities_connected']['standard_weightage'] = $weightages->utilities_connected;
        $preCalculationArray['utilities_connected']['change_weightage']= $drive_margin_saved->weightage_utilities_connected;
        $preCalculationArray['utilities_connected']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['utilities_connected']['difference']) * ($preCalculationArray['utilities_connected']['change_weightage'] / 100), 2);

        $preCalculationArray['developer_margin']['average'] = 0;
        $preCalculationArray['developer_margin']['subject_property'] = 0;
        $preCalculationArray['developer_margin']['difference'] = 0;
        $preCalculationArray['developer_margin']['standard_weightage'] = 0;
        $preCalculationArray['developer_margin']['change_weightage'] = $drive_margin_saved->weightage_developer_margin;
        $preCalculationArray['developer_margin']['adjustments'] = round((($drive_margin_saved->weightage_developer_margin / 100) * $list_calculate_data->avg_listings_price_size), 2);

        $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);

        $preCalculationArray['land_size']['average'] = $landsize * $inspection_data->built_up_area;
        $preCalculationArray['land_size']['subject_property'] = $model->land_size;
        $preCalculationArray['land_size']['difference'] = $preCalculationArray['land_size']['subject_property'] - $preCalculationArray['land_size']['average'];
        $preCalculationArray['land_size']['standard_weightage'] =Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
        $preCalculationArray['land_size']['change_weightage'] = $drive_margin_saved->weightage_land_size;
        $preCalculationArray['land_size']['adjustments'] = round(($preCalculationArray['land_size']['difference'] * ($preCalculationArray['land_size']['change_weightage'] / 100) * $cost_info->lands_price));

        $preCalculationArray['balcony_size']['average'] = $drive_margin_saved->average_balcony_size;
        $preCalculationArray['balcony_size']['subject_property'] = $inspection_data->balcony_size;
        $preCalculationArray['balcony_size']['difference'] = $preCalculationArray['balcony_size']['subject_property'] - $preCalculationArray['balcony_size']['average'];
        $preCalculationArray['balcony_size']['standard_weightage'] = $weightages->balcony_size;
        $preCalculationArray['balcony_size']['change_weightage'] = $drive_margin_saved->weightage_balcony_size;
        $preCalculationArray['balcony_size']['adjustments'] = round(($preCalculationArray['balcony_size']['difference'] * ($preCalculationArray['balcony_size']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['built_up_area']['average'] = $drive_margin_saved->average_built_up_area;
        $preCalculationArray['built_up_area']['subject_property'] = $inspection_data->built_up_area;
        $preCalculationArray['built_up_area']['difference'] = $preCalculationArray['built_up_area']['subject_property'] - $preCalculationArray['built_up_area']['average'];
        $preCalculationArray['built_up_area']['standard_weightage'] = Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
        $preCalculationArray['built_up_area']['change_weightage'] = $drive_margin_saved->weightage_built_up_area;
        $preCalculationArray['built_up_area']['adjustments'] = round(($preCalculationArray['built_up_area']['difference'] * ($preCalculationArray['built_up_area']['change_weightage'] / 100) * $list_calculate_data->avg_psf));

        $preCalculationArray['date']['average'] = $drive_margin_saved->average_date;
        $preCalculationArray['date']['subject_property'] = date('Y-m-d');
        $preCalculationArray['date']['difference'] = $difference_in_days;
        $preCalculationArray['date']['standard_weightage'] = $date_weightages;
        $preCalculationArray['date']['change_weightage'] = $drive_margin_saved->weightage_date;
        $preCalculationArray['date']['adjustments'] = round((($preCalculationArray['date']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));




        $sold_avg_psf = round($list_calculate_data->avg_psf / $inspection_data->built_up_area, 2);

        $sold_adjustments = 0;
        $sold_adjustments_before_deiscount = 0;
        $discount = 0;
        $discount_percentage = 0;


        foreach ($preCalculationArray as $calucation) {
            $sold_adjustments = $sold_adjustments + $calucation['adjustments'];
        }

        $sold_adjustments_before_discount = $list_calculate_data->avg_listings_price_size + $sold_adjustments;


        if ($sold_adjustments_before_discount < 1000000 && $drive_margin_saved->sales_discount == '') {
            $discount_percentage = $sales_discount->upto_1_million;
            $discount = ($sales_discount->upto_1_million / 100) * $sold_adjustments_before_discount;
        } else if ($sold_adjustments_before_discount < 2000000 && $drive_margin_saved->sales_discount == '') {
            $discount_percentage = $sales_discount->upto_2_million;
            $discount = ($sales_discount->upto_2_million / 100) * $sold_adjustments_before_discount;
        } else if ($sold_adjustments_before_discount < 3000000 && $drive_margin_saved->sales_discount == '') {
            $discount_percentage = $sales_discount->upto_3_million;
            $discount = ($sales_discount->upto_3_million / 100) * $sold_adjustments_before_discount;
        } else if ($sold_adjustments_before_discount < 4000000 && $drive_margin_saved->sales_discount == '') {
            $discount_percentage = $sales_discount->upto_4_million;
            $discount = ($sales_discount->upto_4_million / 100) * $sold_adjustments_before_discount;
        } else if ($sold_adjustments_before_discount < 5000000 && $drive_margin_saved->sales_discount == '') {
            $discount_percentage = $sales_discount->upto_5_million;
            $discount = ($sales_discount->upto_5_million / 100) * $sold_adjustments_before_discount;
        } else if ($sold_adjustments_before_discount < 20000000 && $drive_margin_saved->sales_discount == '') {
            $discount_percentage = $sales_discount->upto_20_million;
            $discount = ($sales_discount->upto_20_million / 100) * $sold_adjustments_before_discount;
        } else if ($sold_adjustments_before_discount < 50000000 && $drive_margin_saved->sales_discount == '') {
            $discount_percentage = $sales_discount->upto_50_million;
            $discount = ($sales_discount->upto_50_million / 100) * $sold_adjustments_before_discount;
        } else if($drive_margin_saved->sales_discount == ''){
            $discount_percentage = $sales_discount->upto_100_million;
            $discount = ($sales_discount->upto_100_million / 100) * $sold_adjustments_before_discount;
        }else{
            $discount_percentage = $drive_margin_saved->sales_discount;
            $discount = ($drive_margin_saved->sales_discount / 100) * $sold_adjustments_before_discount;
        }
        $drive_margin_saved->sales_discount = $discount_percentage;

        //$sales_discount


        $total_estimate_price = $list_calculate_data->avg_listings_price_size + $sold_adjustments - $discount;

        $sold_avg_psf = round($total_estimate_price / $inspection_data->built_up_area, 2);

        $drive_margin_saved->mv_total_price = $total_estimate_price;

        $drive_margin_saved->mv_avg_psf = $sold_avg_psf;

        $drive_margin_saved->save();


        $drive_margin_saved_updated = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'list'])->one();

        if ($readonly==1) {

            return $this->renderPartial('steps/_step15', [
                'model' => $drive_margin_saved_updated,
                'valuation' => $model,
                'preCalculationArray' => $preCalculationArray,
                'avg_psf' => $list_calculate_data->avg_psf,
                'avg_listings_price_size' => $list_calculate_data->avg_listings_price_size,
                'sold_avg_psf' => $sold_avg_psf,
                'inspection_data' => $inspection_data,
                'sold_adjustments_before_deiscount' => $sold_adjustments_before_discount,
                'discount' => $discount,
                'discount_percentage' => $discount_percentage,
                'sold_adjustments' => $sold_adjustments,
                'readonly' => $readonly
            ]);
        }


        return $this->render('steps/_step15', [
            'model' => $drive_margin_saved_updated,
            'valuation' => $model,
            'preCalculationArray' => $preCalculationArray,
            'avg_psf' => $list_calculate_data->avg_psf,
            'avg_listings_price_size' => $list_calculate_data->avg_listings_price_size,
            'sold_avg_psf' => $sold_avg_psf,
            'inspection_data' => $inspection_data,
            'sold_adjustments_before_deiscount' => $sold_adjustments_before_discount,
            'discount' => $discount,
            'discount_percentage' => $discount_percentage,
            'sold_adjustments' => $sold_adjustments,
            'readonly' => $readonly
        ]);

    }
    /* End list calculations*/


    /* Start previous claculations*/

    public function actionStep_16($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_16')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $valuation = $this->findModel($id);
        $model = ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'previous'])->one();
        if ($model !== null) {
        } else {
            $model = new ValuationEnquiry();
            $model->date_from = '2020-01-01';
            $model->date_to = date('Y-m-d');
            $model->building_info = $valuation->building_info;
            $model->bedroom_from = $valuation->inspectProperty->no_of_bedrooms;
            $model->bedroom_to = $valuation->inspectProperty->no_of_bedrooms;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            $model->type = 'previous';
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_16/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step16', [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_17($id,$readonly=0)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_17')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            if (isset($post['selection']) && $post['selection'] <> null) {
                ValuationSelectedLists::deleteAll(['valuation_id' => $id, 'type' => 'previous']);
                foreach ($post['selection'] as $selected_row) {
                    $selected_data_detail = new ValuationSelectedLists();
                    $selected_data_detail->selected_id = $selected_row;
                    $selected_data_detail->type = 'previous';
                    $selected_data_detail->valuation_id = $id;
                    $selected_data_detail->save();
                }

            }
        }

        $selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'previous'])->all(), 'id', 'selected_id');

        $totalResults = (new \yii\db\Query())
            ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(market_value) avg_listings_price_size, MIN(market_value) as min_price, MIN(psf) as min_price_sqt, MAX(market_value) as max_price, MAX(psf) as max_price_sqt , FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(date_submitted))) as avg_listing_date')
            ->from('previous_transactions')
            ->where(['id' => $selected_data])
            ->all();


        //Average and calculations

        if (!empty($totalResults) && $totalResults <> null) {
            if (isset($totalResults[0]['avg_bedrooms'])) {
                ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'previous']);
                $calculations = new ValuationListCalculation();
                $calculations->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                $calculations->avg_land_size = round($totalResults[0]['avg_land_size']);
                $calculations->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                $calculations->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                $calculations->min_price = round($totalResults[0]['min_price']);
                $calculations->max_price = round($totalResults[0]['max_price']);
                $calculations->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                $calculations->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                $calculations->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                $calculations->avg_psf = round($calculations->avg_listings_price_size / $calculations->built_up_area_size);
                $calculations->avg_gross_yield = 0;
                $calculations->type = 'previous';
                $calculations->valuation_id = $id;
                $calculations->save();
            }

        }


        $select_calculations = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'previous'])->one();


        // $list_selected_records = \app\models\ValuationSelectedLists::find()->where(['selected_id' => $selected_data,'type'=> 'sold'])->all();

        // selected_data
        $list_selected_records = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'previous'])->one();

        $listing_filter = array();
        if (!empty($list_selected_records) && $list_selected_records) {

            $listing_filter['PreviousTransactionsSearch']['date_from'] = $list_selected_records->date_from;
            $listing_filter['PreviousTransactionsSearch']['date_to'] = $list_selected_records->date_to;
            $listing_filter['PreviousTransactionsSearch']['building_info_data'] = $list_selected_records->building_info;
            $listing_filter['PreviousTransactionsSearch']['property_id'] = $list_selected_records->property_id;
            $listing_filter['PreviousTransactionsSearch']['property_category'] = $list_selected_records->property_category;
            $listing_filter['PreviousTransactionsSearch']['bedroom_from'] = $list_selected_records->bedroom_from;
            $listing_filter['PreviousTransactionsSearch']['bedroom_to'] = $list_selected_records->bedroom_to;
            $listing_filter['PreviousTransactionsSearch']['landsize_from'] = $list_selected_records->landsize_from;
            $listing_filter['PreviousTransactionsSearch']['landsize_to'] = $list_selected_records->landsize_to;
            $listing_filter['PreviousTransactionsSearch']['bua_from'] = $list_selected_records->bua_from;
            $listing_filter['PreviousTransactionsSearch']['bua_to'] = $list_selected_records->bua_to;
            $listing_filter['PreviousTransactionsSearch']['price_from'] = $list_selected_records->price_from;
            $listing_filter['PreviousTransactionsSearch']['price_to'] = $list_selected_records->price_to;
            $listing_filter['PreviousTransactionsSearch']['price_psf_from'] = $list_selected_records->price_psf_from;
            $listing_filter['PreviousTransactionsSearch']['price_psf_to'] = $list_selected_records->price_psf_to;
        }

        $selected_records_display = $list_selected_records;

        if ($selected_records_display <> null & !empty($selected_records_display)) {
            $building_info_data = '';

            if ($selected_records_display->building_info <> null) {
                $buildings = explode(",", $list_selected_records->building_info);
                $all_buildings_titles = Buildings::find()->where(['id' => $buildings])->all();

                if ($all_buildings_titles <> null && !empty($all_buildings_titles)) {
                    foreach ($all_buildings_titles as $title) {
                        $building_info_data .= $title['title'] . ',&nbsp';
                    }

                }

            }
            $selected_records_display->building_info = $building_info_data;
            $property_data = Properties::find()->where(['id' => $list_selected_records->property_id])->one();
            if ($property_data <> null && !empty($property_data)) {
                $selected_records_display->property_id = $property_data->title;
            }
            $property_data = Properties::find()->where(['id' => $list_selected_records->property_id])->one();
            if ($selected_records_display->property_category <> null) {
                $selected_records_display->property_category = Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$sold_selected_records_display->property_category];
            }
        }

        $valuation = $this->findModel($id);
        $searchModel = new PreviousTransactionsSearch();
        $dataProvider = $searchModel->searchvaluation($listing_filter);

        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;


        if ($readonly==1) {
            $dataProvider->sort = false;
            return $this->renderPartial('steps/_step17', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'valuation' => $valuation,
                'selected_data' => $selected_data,
                'totalResults' => $totalResults,
                'select_calculations' => $select_calculations,
                'selected_records_display' => $selected_records_display,
                'readonly' => $readonly
            ]);
        }

        return $this->render('steps/_step17', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'valuation' => $valuation,
            'selected_data' => $selected_data,
            'totalResults' => $totalResults,
            'select_calculations' => $select_calculations,
            'selected_records_display' => $selected_records_display,
            'readonly' => $readonly
        ]);

    }

    public function actionStep_18($id,$readonly=0)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_18')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'previous'])->one();
            if ($drive_margin_saved->load(Yii::$app->request->post())) {
                if ($drive_margin_saved->save()) {
                    Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                    // return $this->redirect(['index']);
                    return $this->redirect(['valuation/step_18/' . $id]);
                } else {
                    if ($drive_margin_saved->hasErrors()) {
                        foreach ($drive_margin_saved->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }


        $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();

        $location_attributes_value = Yii::$app->appHelperFunctions->getLocationAttributes($inspection_data);
        $view_attributes_value = Yii::$app->appHelperFunctions->getViewAttributes($inspection_data);
        $list_calculate_data = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'previous'])->one();
        $weightages = \app\models\Weightages::find()->where(['property_type' => $model->property_id])->one();
        $buildongs_info = \app\models\Buildings::find()->where(['id' => $model->building_info])->one();
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $developer_margin = \app\models\ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();


        $weightages_required_month = array();
        $weightages_required_year = array();

        $weightages_required_month[1] = $weightages->less_than_1_month;
        $weightages_required_month[2] = $weightages->less_than_2_month;
        $weightages_required_month[3] = $weightages->less_than_3_month;
        $weightages_required_month[4] = $weightages->less_than_4_month;
        $weightages_required_month[5] = $weightages->less_than_5_month;
        $weightages_required_month[6] = $weightages->less_than_6_month;
        $weightages_required_month[7] = $weightages->less_than_7_month;
        $weightages_required_month[8] = $weightages->less_than_8_month;
        $weightages_required_month[9] = $weightages->less_than_9_month;
        $weightages_required_month[10] = $weightages->less_than_10_month;
        $weightages_required_month[11] = $weightages->less_than_11_month;
        $weightages_required_month[12] = $weightages->less_than_12_month;

        $weightages_required_year[2] = $weightages->less_than_2_year;
        $weightages_required_year[3] = $weightages->less_than_3_year;
        $weightages_required_year[4] = $weightages->less_than_4_year;
        $weightages_required_year[5] = $weightages->less_than_5_year;
        $weightages_required_year[6] = $weightages->less_than_6_year;
        $weightages_required_year[7] = $weightages->less_than_7_year;
        $weightages_required_year[8] = $weightages->less_than_8_year;
        $weightages_required_year[9] = $weightages->less_than_9_year;
        $weightages_required_year[10] = $weightages->less_than_10_year;

        $avg_date = date_create($list_calculate_data->avg_listing_date);
        $current_date = date('Y-m-d');
        $current_date = date_create($current_date);
        $diff = date_diff($current_date, $avg_date);
        $difference_in_days = $diff->format("%a");
        $date_weightages = 0;

        $years_remaining = intval($difference_in_days / 365);
        $days_remaining = $difference_in_days % 365;

        if ($years_remaining > 1) {
            if ($days_remaining > 0) {

                $date_weightages = $weightages_required_year[$years_remaining + 1];
            } else {
                $date_weightages = $weightages_required_year[$years_remaining];
            }
        } else {
            $month_no = intval($days_remaining / 30);
            $days_remaining_month = $days_remaining % 30;
            if ($days_remaining_month > 0) {
                $date_weightages = $weightages_required_month[$month_no + 1];
            } else {
                $date_weightages = $weightages_required_month[$month_no];
            }
        }


        $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'previous'])->one();

        $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);
        $difference_land_size = ($landsize * $inspection_data->built_up_area) - $model->land_size;
        if($list_calculate_data->avg_land_size > 0) {
            $difference_land_size_percentage = round(abs(($difference_land_size / $list_calculate_data->avg_land_size) * 100), 2);
        }else{
            $difference_land_size_percentage = 0;
        }
        $difference_bua = $inspection_data->built_up_area - $list_calculate_data->built_up_area_size;
        $difference_bua_percentage = round(abs(($difference_bua / $list_calculate_data->built_up_area_size) * 100),2);

        if (empty($drive_margin_saved) && $drive_margin_saved == null) {


            $selected_data_last_step = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'previous'])->all(), 'id', 'selected_id');

            $totalResults_average = (new \yii\db\Query())
                ->select('
            AVG(built_up_area) as built_up_area_size,
            AVG(tenure) as avg_tenure,
            AVG(estimated_age) as avg_estimated_age,
            AVG (CASE 
            WHEN finished_status = "Shell & Core" THEN 1 
            WHEN finished_status = "Fitted" THEN 2
            ELSE 0 END) AS avg_finished_status,
            AVG(property_condition) as avg_property_condition,
            AVG(property_exposure) as avg_property_exposure,
            AVG(property_placement) as avg_property_placement,
            AVG(floor_number) as avg_full_building_floors,
            AVG(number_of_levels) as avg_number_of_levels,
            AVG(no_of_bedrooms) as avg_no_of_bedrooms,
            AVG(parking_space) as avg_parking_space,            
            AVG (CASE 
            WHEN pool = "Yes" THEN 2 
            ELSE 1 END) AS avg_pool,            
            AVG (CASE 
            WHEN landscaping = "Yes" THEN 2 
            WHEN landscaping = "No" THEN 1
            WHEN landscaping = "Semi-Landscape" THEN 1.5
            ELSE 0 END) AS avg_landscaping,
            AVG (CASE 
            WHEN white_goods = "Yes" THEN 2 
            ELSE 1 END) AS avg_white_goods,          
            AVG (CASE 
            WHEN utilities_connected = "Yes" THEN 2 
            ELSE 1 END) AS avg_utilities_connected,            
            AVG (CASE 
            WHEN developer_margin = "Yes" THEN 2 
            ELSE 1 END) AS avg_developer_margin,          
            AVG(land_size) as avg_land_size,
            AVG(balcony_size) as avg_balcony_size')
                ->from('previous_transactions')
                ->where(['id' => $selected_data_last_step])
                ->all();

            $location_avg_attributes_value_prev = 0;
            $view_avg_attributes_value_prev = 0;

            if(!empty($selected_data_last_step)){
                foreach ($selected_data_last_step as $item){
                    $list_data = \app\models\PreviousTransactions::find()->where(['id' => $item])->one();
                    $location_avg_attributes_value_prev = $location_avg_attributes_value_prev + Yii::$app->appHelperFunctions->getLocationAttributes($list_data);
                    $view_avg_attributes_value_prev = $view_avg_attributes_value_prev + Yii::$app->appHelperFunctions->getViewAttributes($list_data);

                }

                $location_avg_attributes_value_prev = round(($location_avg_attributes_value_prev/count($selected_data_last_step)), 2);
                $view_avg_attributes_value_prev = round(($view_avg_attributes_value_prev/count($selected_data_last_step)), 2);
            }


            $drive_margin_saved_data = new ValuationDriveMv();

            $drive_margin_saved_data->average_location = (isset($totalResults_average[0]['avg_location']) ? round($totalResults_average[0]['avg_location'], 2) : $location_avg_attributes_value_prev);
            $drive_margin_saved_data->average_age = (isset($totalResults_average[0]['avg_estimated_age']) ? round($totalResults_average[0]['avg_estimated_age'], 2) : 0);
            $drive_margin_saved_data->average_tenure = (isset($totalResults_average[0]['avg_tenure']) ? round($totalResults_average[0]['avg_tenure'], 2) : 0);
            $drive_margin_saved_data->average_view = (isset($totalResults_average[0]['avg_view']) ? round($totalResults_average[0]['avg_view'], 2) : $view_avg_attributes_value_prev);
            $drive_margin_saved_data->average_finished_status = (isset($totalResults_average[0]['avg_finished_status']) ? round($totalResults_average[0]['avg_finished_status'], 2) : 0);
            $drive_margin_saved_data->average_property_condition = (isset($totalResults_average[0]['avg_property_condition']) ? round($totalResults_average[0]['avg_property_condition'], 2) : 0);
            $drive_margin_saved_data->average_upgrades = (isset($totalResults_average[0]['avg_property_condition']) ? round($totalResults_average[0]['avg_property_condition'], 2) : 0);
            // $drive_margin_saved_data->average_furnished = 3;
            $drive_margin_saved_data->average_property_exposure = (isset($totalResults_average[0]['avg_property_exposure']) ? round($totalResults_average[0]['avg_property_exposure'], 2) : 0);
            $drive_margin_saved_data->average_property_placement = (isset($totalResults_average[0]['avg_property_placement']) ? round($totalResults_average[0]['avg_property_placement'], 2) : 0);
            //$drive_margin_saved_data->average_full_building_floors = (isset($model->floor_number) ? round($model->floor_number, 2) : 0);
           // $drive_margin_saved_data->average_full_building_floors = (isset($inspection_data->full_building_floors) ? $inspection_data->full_building_floors : 0);
            $drive_margin_saved_data->average_full_building_floors = (isset($totalResults_average[0]['avg_full_building_floors']) ? round($totalResults_average[0]['avg_full_building_floors'], 2) : 0);
            $drive_margin_saved_data->average_number_of_levels = (isset($totalResults_average[0]['avg_number_of_levels']) ? round($totalResults_average[0]['avg_number_of_levels'], 2) : 0);
            $drive_margin_saved_data->average_no_of_bedrooms = (isset($totalResults_average[0]['avg_no_of_bedrooms']) ? round($totalResults_average[0]['avg_no_of_bedrooms'], 2) : 0);
            $drive_margin_saved_data->average_parking_space = (isset($totalResults_average[0]['avg_parking_space']) ? round($totalResults_average[0]['avg_parking_space'], 2) : 0);
            $drive_margin_saved_data->average_pool = (isset($totalResults_average[0]['avg_pool']) ? round($totalResults_average[0]['avg_pool'], 2) : 0);
            $drive_margin_saved_data->average_landscaping = (isset($totalResults_average[0]['avg_landscaping']) ? round($totalResults_average[0]['avg_landscaping'], 2) : 0);
            $drive_margin_saved_data->average_white_goods = (isset($totalResults_average[0]['avg_white_goods']) ? round($totalResults_average[0]['avg_white_goods'], 2) : 0);
            $drive_margin_saved_data->average_utilities_connected = (isset($totalResults_average[0]['avg_utilities_connected']) ? round($totalResults_average[0]['avg_utilities_connected'], 2) : 0);
            $drive_margin_saved_data->average_developer_margin = (isset($totalResults_average[0]['avg_developer_margin']) ? round($totalResults_average[0]['avg_developer_margin'], 2) : 0);
            $drive_margin_saved_data->average_land_size = (isset($totalResults_average[0]['avg_land_size']) ? round($totalResults_average[0]['avg_land_size'], 2) : 0);
            $drive_margin_saved_data->average_balcony_size = $inspection_data->balcony_size;;
            $drive_margin_saved_data->average_built_up_area = $list_calculate_data->built_up_area_size;
            $drive_margin_saved_data->average_date = $list_calculate_data->avg_listing_date;
            $drive_margin_saved_data->average_sale_price = $list_calculate_data->avg_listings_price_size;;
            $drive_margin_saved_data->average_psf = $list_calculate_data->avg_psf;;

            $drive_margin_saved_data->weightage_location = $weightages->location;
            $drive_margin_saved_data->weightage_age = $weightages->age;
            $drive_margin_saved_data->weightage_tenure = $weightages->tenure;
            $drive_margin_saved_data->weightage_view = $weightages->view;
            $drive_margin_saved_data->weightage_finished_status = $weightages->finishing_status;
            $drive_margin_saved_data->weightage_property_condition = $weightages->upgrades;
            $drive_margin_saved_data->weightage_upgrades = $weightages->upgrades;
            $drive_margin_saved_data->weightage_furnished = $weightages->furnished;
            $drive_margin_saved_data->weightage_property_exposure = $weightages->property_exposure;
            $drive_margin_saved_data->weightage_property_placement = $weightages->property_placement;
            $drive_margin_saved_data->weightage_full_building_floors = $weightages->floor;
            $drive_margin_saved_data->weightage_number_of_levels = $weightages->number_of_levels;
            $drive_margin_saved_data->weightage_no_of_bedrooms = $weightages->bedrooom;
            $drive_margin_saved_data->weightage_parking_space = $weightages->parking;
            $drive_margin_saved_data->weightage_pool = $weightages->pool;
            $drive_margin_saved_data->weightage_landscaping = $weightages->landscape;
            $drive_margin_saved_data->weightage_white_goods = $weightages->white_goods;
            $drive_margin_saved_data->weightage_utilities_connected = $weightages->utilities_connected;
            $drive_margin_saved_data->weightage_developer_margin = $developer_margin->developer_margin_percentage;
            $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
            $drive_margin_saved_data->weightage_balcony_size = $weightages->balcony_size;
            $drive_margin_saved_data->weightage_built_up_area =  Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
            $drive_margin_saved_data->weightage_date = $date_weightages;
            $drive_margin_saved_data->valuation_id = $id;
            $drive_margin_saved_data->type = 'previous';
            if (!$drive_margin_saved_data->save()) {
            }
            $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'previous'])->one();
        }


        $preCalculationArray = array();

        $preCalculationArray['location']['average'] = $drive_margin_saved->average_location;
        //$preCalculationArray['location']['subject_property'] = $inspection_data->location;
        $preCalculationArray['location']['subject_property'] = $location_attributes_value;
        $preCalculationArray['location']['difference'] = $preCalculationArray['location']['subject_property'] - $preCalculationArray['location']['average'];
        $preCalculationArray['location'] ['standard_weightage'] = $weightages->location;
        $preCalculationArray['location']['change_weightage'] = $drive_margin_saved->weightage_location;
        $preCalculationArray['location']['adjustments'] = round(($preCalculationArray['location']['difference'] * ($preCalculationArray['location']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['age']['average'] = $drive_margin_saved->average_age;
        $preCalculationArray['age']['subject_property'] = $inspection_data->estimated_age;
        $preCalculationArray['age']['difference'] = $preCalculationArray['age']['average'] - $preCalculationArray['age']['subject_property'];
        $preCalculationArray['age']['standard_weightage'] = $weightages->age;
        $preCalculationArray['age']['change_weightage'] = $drive_margin_saved->weightage_age;
        $preCalculationArray['age']['adjustments'] = round(($preCalculationArray['age']['difference'] * ($preCalculationArray['age']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['tenure']['average'] = $drive_margin_saved->average_tenure;
        $preCalculationArray['tenure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure]];
        $preCalculationArray['tenure']['difference'] = $preCalculationArray['tenure']['subject_property'] - $preCalculationArray['tenure']['average'];
        $preCalculationArray['tenure']['standard_weightage'] = $weightages->tenure;
        $preCalculationArray['tenure']['change_weightage'] = $drive_margin_saved->weightage_tenure;
        $preCalculationArray['tenure']['adjustments'] = round(($preCalculationArray['tenure']['difference'] * ($preCalculationArray['tenure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['view']['average'] = $drive_margin_saved->average_view;
        //$preCalculationArray['view']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->otherPropertyViewListArr[$inspection_data->view]];
        $preCalculationArray['view']['subject_property'] = $view_attributes_value;
        $preCalculationArray['view']['difference'] = $preCalculationArray['view']['subject_property'] - $preCalculationArray['view']['average'];
        $preCalculationArray['view']['standard_weightage'] = $weightages->view;
        $preCalculationArray['view']['change_weightage'] = $drive_margin_saved->weightage_view;
        $preCalculationArray['view']['adjustments'] = round(($preCalculationArray['view']['difference'] * ($preCalculationArray['view']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['finished_status']['average'] = $drive_margin_saved->average_finished_status;
        $preCalculationArray['finished_status']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->finished_status];
        $preCalculationArray['finished_status']['difference'] = $preCalculationArray['finished_status']['subject_property'] - $preCalculationArray['finished_status']['average'];
        $preCalculationArray['finished_status']['standard_weightage'] = $weightages->furnished;
        $preCalculationArray['finished_status']['change_weightage'] = $drive_margin_saved->weightage_finished_status;
        $preCalculationArray['finished_status']['adjustments'] = round(($preCalculationArray['finished_status']['difference'] * ($preCalculationArray['finished_status']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

       /* $preCalculationArray['property_condition']['average'] = $drive_margin_saved->average_property_condition;
        $preCalculationArray['property_condition']['subject_property'] = $inspection_data->property_condition;
        $preCalculationArray['property_condition']['difference'] = $preCalculationArray['property_condition']['subject_property'] - $preCalculationArray['property_condition']['average'];
        $preCalculationArray['property_condition']['standard_weightage'] = $weightages->upgrades;
        $preCalculationArray['property_condition']['change_weightage'] = $drive_margin_saved->weightage_property_condition;
        $preCalculationArray['property_condition']['adjustments'] = round(($preCalculationArray['property_condition']['difference'] * ($preCalculationArray['property_condition']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));*/

        $preCalculationArray['upgrades']['average'] = $drive_margin_saved->average_upgrades;
        $preCalculationArray['upgrades']['subject_property'] = $model->valuationConfiguration->over_all_upgrade;
        $preCalculationArray['upgrades']['difference'] = $preCalculationArray['upgrades']['subject_property'] - $preCalculationArray['upgrades']['average'];
        $preCalculationArray['upgrades']['standard_weightage'] = $weightages->quality;
        $preCalculationArray['upgrades']['change_weightage'] = $drive_margin_saved->weightage_upgrades;
        $preCalculationArray['upgrades']['adjustments'] = round(($preCalculationArray['upgrades']['difference'] * ($preCalculationArray['upgrades']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));



        $preCalculationArray['property_exposure']['average'] = $drive_margin_saved->average_property_exposure;
        $preCalculationArray['property_exposure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyExposureListArr[$inspection_data->property_exposure]];
        $preCalculationArray['property_exposure']['difference'] = $preCalculationArray['property_exposure']['subject_property'] - $preCalculationArray['property_exposure']['average'];
        $preCalculationArray['property_exposure']['standard_weightage'] = $weightages->property_exposure;
        $preCalculationArray['property_exposure']['change_weightage'] = $drive_margin_saved->weightage_property_exposure;
        $preCalculationArray['property_exposure']['adjustments'] = round(($preCalculationArray['property_exposure']['difference'] * ($preCalculationArray['property_exposure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['property_placement']['average'] = $drive_margin_saved->average_property_placement;
        $preCalculationArray['property_placement']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyPlacementListArr[$inspection_data->property_placement]];
        $preCalculationArray['property_placement']['difference'] = $preCalculationArray['property_placement']['subject_property'] - $preCalculationArray['property_placement']['average'];
        $preCalculationArray['property_placement']['standard_weightage'] = $weightages->property_placement;
        $preCalculationArray['property_placement']['change_weightage'] = $drive_margin_saved->weightage_property_placement;
        $preCalculationArray['property_placement']['adjustments'] = round(($preCalculationArray['property_placement']['difference'] * ($preCalculationArray['property_placement']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['floors_adjustment']['average'] = $drive_margin_saved->average_full_building_floors;
        $preCalculationArray['floors_adjustment']['subject_property'] = (isset($model->floor_number) ? $model->floor_number : 0);
        $preCalculationArray['floors_adjustment']['difference'] = $preCalculationArray['floors_adjustment']['subject_property'] - $preCalculationArray['floors_adjustment']['average'];
        $preCalculationArray['floors_adjustment']['standard_weightage'] = $weightages->floor;
        $preCalculationArray['floors_adjustment']['change_weightage'] = $drive_margin_saved->weightage_full_building_floors;
        $preCalculationArray['floors_adjustment']['adjustments'] = round(($preCalculationArray['floors_adjustment']['difference'] * ($preCalculationArray['floors_adjustment']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['number_of_levels']['average'] = $drive_margin_saved->average_number_of_levels;
        $preCalculationArray['number_of_levels']['subject_property'] = $inspection_data->number_of_levels;
        $preCalculationArray['number_of_levels']['difference'] = $preCalculationArray['number_of_levels']['subject_property'] - $preCalculationArray['number_of_levels']['average'];
        $preCalculationArray['number_of_levels']['standard_weightage'] = $weightages->number_of_levels;
        $preCalculationArray['number_of_levels']['change_weightage'] = $drive_margin_saved->weightage_number_of_levels;
        $preCalculationArray['number_of_levels']['adjustments'] = round(($preCalculationArray['number_of_levels']['difference'] * ($preCalculationArray['number_of_levels']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['no_of_bedrooms']['average'] = $drive_margin_saved->average_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['subject_property'] = $inspection_data->no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['difference'] = $preCalculationArray['no_of_bedrooms']['subject_property'] - $preCalculationArray['no_of_bedrooms']['average'];
        $preCalculationArray['no_of_bedrooms']['standard_weightage'] = $weightages->bedrooom;
        $preCalculationArray['no_of_bedrooms']['change_weightage'] = $drive_margin_saved->weightage_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['adjustments'] = round(($preCalculationArray['no_of_bedrooms']['difference'] * ($preCalculationArray['no_of_bedrooms']['standard_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['parking_space']['average'] = $drive_margin_saved->average_parking_space;
        $preCalculationArray['parking_space']['subject_property'] = $inspection_data->parking_floors;
        $preCalculationArray['parking_space']['difference'] = $preCalculationArray['parking_space']['subject_property'] - $preCalculationArray['parking_space']['average'];
        $preCalculationArray['parking_space']['standard_weightage'] = $weightages->parking;
        $preCalculationArray['parking_space']['change_weightage'] = $drive_margin_saved->weightage_parking_space;
        $preCalculationArray['parking_space']['adjustments'] = round(($cost_info->parking_price * $preCalculationArray['parking_space']['difference']) * ($preCalculationArray['parking_space']['change_weightage'] / 100), 2);


        $preCalculationArray['pool']['average'] = $drive_margin_saved->average_pool;
        $preCalculationArray['pool']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->pool];
        $preCalculationArray['pool']['difference'] = $preCalculationArray['pool']['subject_property'] - $preCalculationArray['pool']['average'];
        $preCalculationArray['pool']['standard_weightage'] = $weightages->pool;
        $preCalculationArray['pool']['change_weightage'] = $drive_margin_saved->weightage_pool;
        $preCalculationArray['pool']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['pool']['difference']) * ($preCalculationArray['pool']['change_weightage'] / 100), 2);

        $preCalculationArray['landscaping']['average'] = $drive_margin_saved->average_landscaping;
        $preCalculationArray['landscaping']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->landscaping];
        $preCalculationArray['landscaping']['difference'] = $preCalculationArray['landscaping']['subject_property'] - $preCalculationArray['landscaping']['average'];
        $preCalculationArray['landscaping']['standard_weightage'] = $weightages->landscape;
        $preCalculationArray['landscaping']['change_weightage'] = $drive_margin_saved->weightage_landscaping;;
        $preCalculationArray['landscaping']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['landscaping']['difference']) * ($preCalculationArray['landscaping']['change_weightage'] / 100), 2);

        $preCalculationArray['white_goods']['average'] = $drive_margin_saved->average_white_goods;
        $preCalculationArray['white_goods']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->white_goods];
        $preCalculationArray['white_goods']['difference'] = $preCalculationArray['white_goods']['subject_property'] - $preCalculationArray['white_goods']['average'];
        $preCalculationArray['white_goods']['standard_weightage'] = $weightages->white_goods;
        $preCalculationArray['white_goods']['change_weightage'] = $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['change_weightage']= $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['white_goods']['difference']) * ($preCalculationArray['white_goods']['change_weightage'] / 100), 2);

        $preCalculationArray['utilities_connected']['average'] = $drive_margin_saved->average_utilities_connected;
        $preCalculationArray['utilities_connected']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->utilities_connected];
        $preCalculationArray['utilities_connected']['difference'] = $preCalculationArray['utilities_connected']['subject_property'] - $preCalculationArray['utilities_connected']['average'];
        $preCalculationArray['utilities_connected']['standard_weightage'] = $weightages->utilities_connected;
        $preCalculationArray['utilities_connected']['change_weightage']= $drive_margin_saved->weightage_utilities_connected;
        $preCalculationArray['utilities_connected']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['utilities_connected']['difference']) * ($preCalculationArray['utilities_connected']['change_weightage'] / 100), 2);

        $preCalculationArray['developer_margin']['average'] = 0;
        $preCalculationArray['developer_margin']['subject_property'] = 0;
        $preCalculationArray['developer_margin']['difference'] = 0;
        $preCalculationArray['developer_margin']['standard_weightage'] = 0;
        $preCalculationArray['developer_margin']['change_weightage'] = $drive_margin_saved->weightage_developer_margin;
        $preCalculationArray['developer_margin']['adjustments'] = round((($drive_margin_saved->weightage_developer_margin / 100) * $list_calculate_data->avg_listings_price_size), 2);

        $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);

        $preCalculationArray['land_size']['average'] = $landsize * $inspection_data->built_up_area;
        $preCalculationArray['land_size']['subject_property'] = $model->land_size;
        $preCalculationArray['land_size']['difference'] = $preCalculationArray['land_size']['subject_property'] - $preCalculationArray['land_size']['average'];
        $preCalculationArray['land_size']['standard_weightage'] =  Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
        $preCalculationArray['land_size']['change_weightage'] = $drive_margin_saved->weightage_land_size;
        $preCalculationArray['land_size']['adjustments'] = round(($preCalculationArray['land_size']['difference'] * ($preCalculationArray['land_size']['change_weightage'] / 100) * $cost_info->lands_price));

        $preCalculationArray['balcony_size']['average'] = $drive_margin_saved->average_balcony_size;
        $preCalculationArray['balcony_size']['subject_property'] = $inspection_data->balcony_size;
        $preCalculationArray['balcony_size']['difference'] = $preCalculationArray['balcony_size']['subject_property'] - $preCalculationArray['balcony_size']['average'];
        $preCalculationArray['balcony_size']['standard_weightage'] =$weightages->balcony_size;
        $preCalculationArray['balcony_size']['change_weightage'] = $drive_margin_saved->weightage_balcony_size;
        $preCalculationArray['balcony_size']['adjustments'] = round(($preCalculationArray['balcony_size']['difference'] * ($preCalculationArray['balcony_size']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['built_up_area']['average'] = $drive_margin_saved->average_built_up_area;
        $preCalculationArray['built_up_area']['subject_property'] = $inspection_data->built_up_area;
        $preCalculationArray['built_up_area']['difference'] = $preCalculationArray['built_up_area']['subject_property'] - $preCalculationArray['built_up_area']['average'];
        $preCalculationArray['built_up_area']['standard_weightage'] =   Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
        $preCalculationArray['built_up_area']['change_weightage'] = $drive_margin_saved->weightage_built_up_area;
        $preCalculationArray['built_up_area']['adjustments'] = round(($preCalculationArray['built_up_area']['difference'] * ($preCalculationArray['built_up_area']['change_weightage'] / 100) * $list_calculate_data->avg_psf));

        $preCalculationArray['date']['average'] = $drive_margin_saved->average_date;
        $preCalculationArray['date']['subject_property'] = date('Y-m-d');
        $preCalculationArray['date']['difference'] = $difference_in_days;
        $preCalculationArray['date']['standard_weightage'] = $date_weightages;
        $preCalculationArray['date']['change_weightage'] = $drive_margin_saved->weightage_date;
        $preCalculationArray['date']['adjustments'] = round((($preCalculationArray['date']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $sold_avg_psf = round($list_calculate_data->avg_psf / $inspection_data->built_up_area, 2);

        $sold_adjustments = 0;

        foreach ($preCalculationArray as $calucation) {
            $sold_adjustments = $sold_adjustments + $calucation['adjustments'];
        }


        $total_estimate_price = $list_calculate_data->avg_listings_price_size + $sold_adjustments;

        $sold_avg_psf = round($total_estimate_price / $inspection_data->built_up_area, 2);

        $drive_margin_saved->mv_total_price = $total_estimate_price;

        $drive_margin_saved->mv_avg_psf = $sold_avg_psf;

        $drive_margin_saved->save();


        $drive_margin_saved_updated = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'previous'])->one();


        if ($readonly==1) {
            return $this->renderPartial('steps/_step18', [
                'model' => $drive_margin_saved_updated,
                'valuation' => $model,
                'preCalculationArray' => $preCalculationArray,
                'avg_psf' => $list_calculate_data->avg_psf,
                'avg_listings_price_size' => $list_calculate_data->avg_listings_price_size,
                'sold_avg_psf' => $sold_avg_psf,
                'inspection_data' => $inspection_data,
                'readonly' => $readonly,

            ]);
        }

        return $this->render('steps/_step18', [
            'model' => $drive_margin_saved_updated,
            'valuation' => $model,
            'preCalculationArray' => $preCalculationArray,
            'avg_psf' => $list_calculate_data->avg_psf,
            'avg_listings_price_size' => $list_calculate_data->avg_listings_price_size,
            'sold_avg_psf' => $sold_avg_psf,
            'inspection_data' => $inspection_data,
            'readonly' => $readonly,

        ]);

    }

    /* End previous calculations*/


    public function actionStep_19($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_19')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $costapproch_data = $this->actionCostEstimateValue($id);


        return $this->render('steps/_step19', [
            'model' => $valuation,
            'valuation' => $valuation,
            'cost_approach' => $costapproch_data['cost_approach'],
        ]);
    }

    public function actionStep_20($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_20')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $valuation = $this->findModel($id);
        $incomeapproch_data = $this->actionIncomeEstimateValue($id);


        return $this->render('steps/_step20', [
            'model' => $valuation,
            'valuation' => $valuation,
            'income_approach' => $incomeapproch_data['income_approach'],
        ]);
    }


    public function actionStep_21_old($id)
    {

        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_21')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $model = ValuationApproversData::find()->where(['valuation_id' => $id,'approver_type' => 'valuer'])->one();

        if ($model !== null) {

        } else {
            $model = new ValuationApproversData();
        }
       /* echo "<pre>";
        print_r($_POST);
        die;*/
        if ($model->load(Yii::$app->request->post())) {

            $model->valuation_id = $id;
            $model->approver_type = 'valuer';

            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['valuation/step_21/' . $valuation->id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        return $this->render('steps/_step21', [
            'model' => $model,
            'valuation' => $valuation,
            'summary' => $this->actionSummary($id),
        ]);
    }
    public function actionStep_21($id,$readonly=0)
    {

        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_21')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $valuation = $this->findModel($id);
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();
        $summary = $this->actionSummary($id);


        $model = ValuationApproversData::find()->where(['valuation_id' => $id,'approver_type' => 'valuer'])->one();

        if ($model !== null) {

        } else {
            $model = new ValuationApproversData();

            $model->estimated_market_value =  $summary['approval_market_value'];
            $model->estimated_market_value_sqf = round($model->estimated_market_value / $InspectProperty->built_up_area, 2);
            $model->estimated_market_rent = $summary['estimated_market_rent'];
            $model->estimated_market_rent_sqf = round($model->estimated_market_rent / $InspectProperty->built_up_area, 2);
            $model->parking_market_value = $cost_info->parking_price;
            $model->parking_market_value_sqf = round($model->parking_market_value / $InspectProperty->built_up_area, 2);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            $model->estimated_market_value_sqf = round($model->estimated_market_value / $InspectProperty->built_up_area, 2);
            $model->estimated_market_rent_sqf = round($model->estimated_market_rent / $InspectProperty->built_up_area, 2);
            $model->parking_market_value_sqf = round($model->parking_market_value / $InspectProperty->built_up_area, 2);
            $model->approver_type = 'valuer';
            if ($model->save()) {
               // $valuation->valuation_status = 8;
               // $valuation->save();

               /* $sequence = array_search($valuation->valuation_status,Yii::$app->appHelperFunctions->getStepsSequence(),true);
                if ($model->status=='Approve') {
                    if ( $sequence ==4) {
                        // UPDATE (table name, column values, condition)
                        Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 8], 'id='.$id.'')->execute();
                    }
                }
                else if($model->status=='Reject') {
                    // $valuation->valuation_status = 6;
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 10], 'id='.$id.'')->execute();
                    //  $valuation->save();
                }
                */




                if ($model->email_status != 1) {
                   // Yii::$app->helperFunctions->getModelStepSubmit($model,$valuation);
                    Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .'
                      	AND approver_type="valuer"')->execute();
                }

                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['valuation/step_21/' . $valuation->id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        if ($readonly==1) {
            return $this->renderPartial('steps/_step21', [
                'model' => $model,
                'valuation' => $valuation,
                'summary' => $this->actionSummary($id),
                'bua' => $InspectProperty->built_up_area,
                'readonly' => $readonly,
            ]);
        }

        return $this->render('steps/_step21', [
            'model' => $model,
            'valuation' => $valuation,
            'summary' => $this->actionSummary($id),
            'bua' => $InspectProperty->built_up_area,
            'readonly' => $readonly,
        ]);
    }


    public function actionStep_22($id)
    {

        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_22')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $valuation = $this->findModel($id);
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();
        $summary = $this->actionSummary($id);

        $model = ValuationApproversData::find()->where(['valuation_id' => $id,'approver_type' => 'reviewer'])->one();
        if ($model !== null) {



        } else {
            $model = new ValuationApproversData();

            $model->estimated_market_value =  $summary['approval_market_value'];
            $model->estimated_market_value_sqf = round($model->estimated_market_value / $InspectProperty->built_up_area, 2);
            $model->estimated_market_rent = $summary['estimated_market_rent'];
            $model->estimated_market_rent_sqf = round($model->estimated_market_rent / $InspectProperty->built_up_area, 2);
            $model->parking_market_value = $cost_info->parking_price;
            $model->parking_market_value_sqf = round($model->parking_market_value / $InspectProperty->built_up_area, 2);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            $model->estimated_market_value_sqf = round($model->estimated_market_value / $InspectProperty->built_up_area, 2);
            $model->estimated_market_rent_sqf = round($model->estimated_market_rent / $InspectProperty->built_up_area, 2);
            $model->parking_market_value_sqf = round($model->parking_market_value / $InspectProperty->built_up_area, 2);
            $model->approver_type = 'reviewer';
            if ($model->save()) {
               // $valuation->valuation_status = 4;
              //  $valuation->save();
               /* $sequence = array_search($valuation->valuation_status,Yii::$app->appHelperFunctions->getStepsSequence(),true);
                if ( $sequence ==5) {
                    // UPDATE (table name, column values, condition)
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 4], 'id='.$id.'')->execute();
                }*/

               /* $sequence = array_search($valuation->valuation_status,Yii::$app->appHelperFunctions->getStepsSequence(),true);
                if ($model->status=='Approve') {
                    if ( $sequence ==5) {
                        // UPDATE (table name, column values, condition)
                        Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 4], 'id='.$id.'')->execute();
                    }
                }
                else if($model->status=='Reject') {
                    // $valuation->valuation_status = 6;
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 12], 'id='.$id.'')->execute();
                    //  $valuation->save();
                }*/



                if ($model->email_status != 1) {
                   // Yii::$app->helperFunctions->getModelStepSubmit($model,$valuation);
                    Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$model->id .' AND approver_type="reviewer"')->execute();
                }

                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_22/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step22', [
            'model' => $model,
            'valuation' => $valuation,
            'bua' => $InspectProperty->built_up_area,
        ]);
    }

    public function actionStep_23($id)
    {

       /* if($id == 704){
            $valuation = $this->findModel($id);
            $model = ValuationApproversData::find()->where(['valuation_id' => $id,'approver_type' => 'approver'])->one();
            Yii::$app->helperFunctions->getModelStepSubmit($model,$valuation);
        }*/
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_23')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();
        $summary = $this->actionSummary($id);

        $model = ValuationApproversData::find()->where(['valuation_id' => $id,'approver_type' => 'approver'])->one();
        if ($model !== null) {



        } else {
            $model = new ValuationApproversData();

            $estimated_market_value = str_replace(",", "", $model->estimated_market_value);
            $model->estimated_market_value =  $summary['approval_market_value'];
            $model->estimated_market_value_sqf = round(floatval($estimated_market_value)/ $InspectProperty->built_up_area, 2);
            $model->estimated_market_rent = $summary['estimated_market_rent'];
            $model->estimated_market_rent_sqf = round(floatval($model->estimated_market_rent) / $InspectProperty->built_up_area, 2);
            $model->parking_market_value = $cost_info->parking_price;
            $model->parking_market_value_sqf = round(floatval($model->parking_market_value) / $InspectProperty->built_up_area, 2);
        }

        if ($model->load(Yii::$app->request->post())) {
            $estimated_market_value = str_replace(",", "", $model->estimated_market_value);
            $model->valuation_id = $id;
            $model->estimated_market_value_sqf = round(floatval($estimated_market_value) / $InspectProperty->built_up_area, 2);

            $estimated_market_rent = str_replace(",", "", $model->estimated_market_rent);

            $model->estimated_market_rent_sqf = round(floatval($estimated_market_rent) / $InspectProperty->built_up_area, 2);

            $parking_market_value = str_replace(",", "", $model->parking_market_value);

            $model->parking_market_value_sqf = round(floatval($parking_market_value) / $InspectProperty->built_up_area, 2);
            $model->approver_type = 'approver';

            if ($model->save()) {

                if ($model->status=='Approve') {
                    //$valuation->valuation_status = 5;
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 5], 'id='.$id.'')->execute();


                }
                else if($model->status=='Reject') {
                   // $valuation->valuation_status = 6;
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 6], 'id='.$id.'')->execute();
                  //  $valuation->save();
                }
                if ($model->email_status != 1) {
                    Yii::$app->helperFunctions->getModelStepSubmit($model,$valuation);
                }

                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_23/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        $model->estimated_market_value =  $model->estimated_market_value_text;
        $model->estimated_market_rent =  $model->estimated_market_rent_text;
        $model->parking_market_value =  $model->parking_market_value_text;
        return $this->render('steps/_step23', [
            'model' => $model,
            'valuation' => $valuation,
            'bua' => $InspectProperty->built_up_area,
        ]);
    }

    public function actionBuacalculation($mv,$mr,$pmv,$bua)
    {
        $resp = array();
        $mv = str_replace(",", "", $mv);
        $mr = str_replace(",", "", $mr);
        $pmv = str_replace(",", "", $pmv);

        $resp['mv']= ($bua <> null && $bua > 0)? round($mv / $bua, 2): 0;
        $resp['mr']= ($bua <> null && $bua > 0)? round($mr / $bua, 2): 0;
        $resp['pmv'] = ($bua <> null && $bua > 0)? round($pmv / $bua, 2): 0;

        return json_encode($resp);
    }


    public function actionCostEstimateValue($id)
    {
        $valuation = $this->findModel($id);
        $cost_approach = array();
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();
        $valuationConfiguration = \app\models\ValuationConfiguration::find()->where(['valuation_id' => $id])->one();
        $developments_info = \app\models\Developments::find()->where(['emirates' => $valuation->building->city, 'property_type' => $valuation->property_id])->asArray()->one();

        /*Start Land Market Value Calculations*/

        $land_market_value['factor'] = $cost_info->lands_price;
        $land_market_value['size'] = $valuation->land_size;
        $land_market_value['status'] = '';
        $land_market_value['calculations'] = round($cost_info->lands_price * $valuation->land_size, 2);
        $land_market_value['total_value_by_cost'] = 0;
        $land_market_value['price_per_sf_bua'] = round($land_market_value['calculations'] / $InspectProperty->built_up_area, 2);
        $cost_approach['land_market_value'] = $land_market_value;
        /*End Land Market Value Calculations*/


        /*Start Financing Charges for Land Calculations*/

        $financing_charges_for_land['factor'] = 5;
        $financing_charges_for_land['size'] = $valuation->land_size;
        $financing_charges_for_land['status'] = '';
        $financing_charges_for_land['calculations'] = (5 / 100) * $land_market_value['calculations'];
        $financing_charges_for_land['total_value_by_cost'] = 0;
        $financing_charges_for_land['price_per_sf_bua'] = '';
        $cost_approach['financing_charges_for_land'] = $financing_charges_for_land;
        /*End Financing Charges for Land Calculations*/

        /*Start Financing Charges for Land Calculations*/

        $land_price_total['factor'] = '';
        $land_price_total['size'] = '';
        $land_price_total['status'] = '';
        $land_price_total['calculations'] = '';
        $land_price_total['total_value_by_cost'] = $land_market_value['calculations'] + $financing_charges_for_land['calculations'];
        $land_price_total['price_per_sf_bua'] = '';
        $cost_approach['land_price_total'] = $land_price_total;
        /*End Financing Charges for Land Calculations*/

        /*Start Construction Costs Calculations*/
        $construction_costs['factor'] = $developments_info['star' . round($valuationConfiguration->over_all_upgrade)];
        $construction_costs['size'] = $InspectProperty->built_up_area;
        $construction_costs['status'] = $InspectProperty->completion_status;
        $construction_costs['calculations'] = $construction_costs['factor'] * $construction_costs['size'];
        $construction_costs['total_value_by_cost'] = '';
        $construction_costs['price_per_sf_bua'] = '';
        $cost_approach['construction_costs'] = $construction_costs;

        $professional_charges['factor'] = $developments_info['professional_charges'];
        $professional_charges['size'] = '';
        $professional_charges['status'] = '';
        $professional_charges['calculations'] = round(($professional_charges['factor'] / 100) * $construction_costs['calculations'], 2);
        $professional_charges['total_value_by_cost'] = '';
        $professional_charges['price_per_sf_bua'] = '';
        $cost_approach['professional_charges'] = $professional_charges;


        $contingency_margin['factor'] = $developments_info['contingency_margin'];
        $contingency_margin['size'] = '';
        $contingency_margin['status'] = '';
        $contingency_margin['calculations'] = round(($contingency_margin['factor'] / 100) * $construction_costs['calculations'], 2);
        $contingency_margin['total_value_by_cost'] = '';
        $contingency_margin['price_per_sf_bua'] = '';
        $cost_approach['contingency_margin'] = $professional_charges;

        $final_construction_cost['factor'] = '';
        $final_construction_cost['size'] = '';
        $final_construction_cost['status'] = '';
        $final_construction_cost['calculations'] = $construction_costs['calculations'] + $professional_charges['calculations'] + $contingency_margin['calculations'];
        $final_construction_cost['total_value_by_cost'] = '';
        $final_construction_cost['price_per_sf_bua'] = '';
        $cost_approach['final_construction_cost'] = $final_construction_cost;

        /*End Construction Costs Calculations*/


        /*Start Financing Charges Value Calculations*/


        $number_of_years['factor'] = $cost_info->number_of_years;
        $number_of_years['size'] = '';
        $number_of_years['status'] = '';
        $number_of_years['calculations'] = '';
        $number_of_years['total_value_by_cost'] = '';
        $number_of_years['price_per_sf_bua'] = '';
        $cost_approach['number_of_years'] = $number_of_years;

        $interest_rate['factor'] = $developments_info['interest_rate'];
        $interest_rate['size'] = '';
        $interest_rate['status'] = '';
        $interest_rate['calculations'] = '';
        $interest_rate['total_value_by_cost'] = '';
        $interest_rate['price_per_sf_bua'] = '';
        $cost_approach['interest_rate'] = $interest_rate;


        $effective_interest_rate['factor'] = $number_of_years['factor'] * $interest_rate['factor'];
        $effective_interest_rate['size'] = '';
        $effective_interest_rate['status'] = '';
        $effective_interest_rate['calculations'] = round(($final_construction_cost['calculations'] / 100) * $effective_interest_rate['factor']);
        $effective_interest_rate['total_value_by_cost'] = '';
        $effective_interest_rate['price_per_sf_bua'] = '';
        $cost_approach['effective_interest_rate'] = $effective_interest_rate;

        $total_financing_charges['factor'] = '';
        $total_financing_charges['size'] = '';
        $total_financing_charges['status'] = '';
        $total_financing_charges['calculations'] = $final_construction_cost['calculations'] + $effective_interest_rate['calculations'];
        $total_financing_charges['total_value_by_cost'] = '';
        $total_financing_charges['price_per_sf_bua'] = '';
        $cost_approach['total_financing_charges'] = $total_financing_charges;
        /*End Financing Charges Value Calculations*/

        /*Start Costs Deductions Value Calculations*/
        $depreciation['factor'] = round((($InspectProperty->estimated_age / ($InspectProperty->estimated_age + $InspectProperty->estimated_remaining_life)) * 100), 4);
        $depreciation['size'] = '';
        $depreciation['status'] = '';
        $depreciation['calculations'] = round(($depreciation['factor'] / 100) * $total_financing_charges['calculations'], 2);
        $depreciation['total_value_by_cost'] = '';
        $depreciation['price_per_sf_bua'] = '';
        $cost_approach['depreciation'] = $depreciation;

        $obscolence['factor'] = $developments_info['obsolescence'];
        $obscolence['size'] = '';
        $obscolence['status'] = '';
        $obscolence['calculations'] = ($obscolence['factor'] / 100) * $total_financing_charges['calculations'];
        $obscolence['total_value_by_cost'] = '';
        $obscolence['price_per_sf_bua'] = '';
        $cost_approach['obscolence'] = $obscolence;

        /*End Costs Deductions Value Calculations*/


        /*Start overall Value Calculations*/
        $net_development_cost['factor'] = '';
        $net_development_cost['size'] = '';
        $net_development_cost['status'] = '';
        $net_development_cost['calculations'] = '';
        $net_development_cost['total_value_by_cost'] = $total_financing_charges['calculations'] - $depreciation['calculations'] - $obscolence['calculations'];
        $net_development_cost['price_per_sf_bua'] = '';
        $cost_approach['net_development_cost'] = $net_development_cost;

        $land_value_plus_net_development_cost['factor'] = '';
        $land_value_plus_net_development_cost['size'] = '';
        $land_value_plus_net_development_cost['status'] = '';
        $land_value_plus_net_development_cost['calculations'] = '';
        $land_value_plus_net_development_cost['total_value_by_cost'] = $land_price_total['total_value_by_cost'] + $net_development_cost['total_value_by_cost'];
        $land_value_plus_net_development_cost['price_per_sf_bua'] = round($land_value_plus_net_development_cost['total_value_by_cost'] / $InspectProperty->built_up_area, 2);
        $cost_approach['land_value_plus_net_development_cost'] = $land_value_plus_net_development_cost;

        $developer_profit['factor'] = $developments_info['developer_profit'];
        $developer_profit['size'] = '';
        $developer_profit['status'] = '';
        $developer_profit['calculations'] = '';
        $developer_profit['total_value_by_cost'] = round(($developer_profit['factor'] / 100) * $land_value_plus_net_development_cost['total_value_by_cost']);
        $developer_profit['price_per_sf_bua'] = round($developer_profit['total_value_by_cost'] / $InspectProperty->built_up_area, 2);
        $cost_approach['developer_profit'] = $developer_profit;

        $estimated_total_cost['factor'] = '';
        $estimated_total_cost['size'] = '';
        $estimated_total_cost['status'] = '';
        $estimated_total_cost['calculations'] = '';
        $estimated_total_cost['total_value_by_cost'] = round($land_value_plus_net_development_cost['total_value_by_cost'] + $developer_profit['total_value_by_cost']);
        $estimated_total_cost['price_per_sf_bua'] = round($estimated_total_cost['total_value_by_cost'] / $InspectProperty->built_up_area, 2);
        $cost_approach['estimated_total_cost'] = $estimated_total_cost;


        /*End overall Value Calculations*/


        return [
            'model' => $valuation,
            'valuation' => $valuation,
            'estimated_total_cost_value' => $estimated_total_cost['total_value_by_cost'],
            'cost_approach' => $cost_approach,
        ];
    }

    public function actionIncomeEstimateValue($id)
    {

        $valuation = $this->findModel($id);
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $building_info = \app\models\Buildings::find()->where(['id' => $valuation->building_info])->one();
        $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();
        $valuationConfiguration = \app\models\ValuationConfiguration::find()->where(['valuation_id' => $id])->one();
        $developments_info = \app\models\Developments::find()->where(['emirates' => $valuation->building->city, 'property_type' => $valuation->property_id])->asArray()->one();


        $income_approach = array();

        /*Start Gross Rental Income Value Calculations*/


        $selected_data_last_step = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'list'])->all(), 'id', 'selected_id');

        $totalResults_average = (new \yii\db\Query())
            ->select('AVG(listings_rent) as avg_listings_rent,AVG(gross_yield) as avg_gross_yield')
            ->from('listings_transactions')
            ->where(['id' => $selected_data_last_step])
            ->all();


        $gross_rental_income['factor'] = '';
        $gross_rental_income['source'] = round($totalResults_average[0]['avg_listings_rent']);
        $gross_rental_income['psf'] = round(($gross_rental_income['source'] / $InspectProperty->built_up_area), 2);
        $gross_rental_income['yield'] = round($totalResults_average[0]['avg_gross_yield'], 2);
        $income_approach['gross_rental_income'] = $gross_rental_income;

        /*End Gross Rental Income Value Calculations*/

        /*Start operations_and_maintainance_costs Income Value Calculations*/

        $op_psf = $building_info->service_charges_psf;
        $op_source = $building_info->service_charges_psf * $InspectProperty->built_up_area;
        if ($op_source > 0 && $gross_rental_income['source'] > 0) {
            $op_factor = round(($op_source / $gross_rental_income['source']) * 100, 2);
        } else {
            $op_factor = 0;
        }

        $operations_and_maintainance_costs['factor'] = $op_factor;
        $operations_and_maintainance_costs['source'] = $op_source;
        $operations_and_maintainance_costs['psf'] = $op_psf;
        $operations_and_maintainance_costs['yield'] = '';
        $income_approach['operations_and_maintainance_costs'] = $operations_and_maintainance_costs;

        /*End operations_and_maintainance_costs Income Value Calculations*/

        /*Start vacancy Income Value Calculations*/




        $vacancy['factor'] = $building_info->vacancy;
        $vacancy['source'] = round(($vacancy['factor'] / 100) * $gross_rental_income['source'], 2);;
        //  $vacancy['source']= 4500;

        if ($vacancy['source'] > 0) {
            $vacancy_factor = round(($vacancy['source'] / $InspectProperty->built_up_area), 2);
        } else {
            $vacancy_factor = 0;
        }
        $vacancy['psf'] = $vacancy_factor;
        $vacancy['yield'] = '';
        $income_approach['vacancy'] = $vacancy;



        /*End vacancy Income Value Calculations*/

        /*Start land_lease_costs Income Value Calculations*/

        $land_lease_costs['factor'] = $cost_info->land_lease_costs;
        $land_lease_costs['source'] = round(($land_lease_costs['factor'] / 100) * $gross_rental_income['source'], 2);
        $land_lease_costs['psf'] = round($land_lease_costs['source'] / $InspectProperty->built_up_area);
        $land_lease_costs['yield'] = '';
        $income_approach['land_lease_costs'] = $land_lease_costs;

        /*End land_lease_costs Income Value Calculations*/

        /*Start insurance Income Value Calculations*/

        $insurance['factor'] = $developments_info['utilities_cost'];
        $insurance['source'] = round(($insurance['factor'] / 100) * $gross_rental_income['source'], 2);;
        $insurance['psf'] = round($insurance['source'] / $InspectProperty->built_up_area);;
        $insurance['yield'] = '';
        $income_approach['insurance'] = $insurance;

        /*End insurance Income Value Calculations*/

        /*Start net_income Income Value Calculations*/

        $net_income['factor'] = '';
        $net_income['source'] = $gross_rental_income['source'] - ($operations_and_maintainance_costs['source'] + $vacancy['source'] + $insurance['source'] + $land_lease_costs['source']);
        $net_income['psf'] = round($net_income['source'] / $InspectProperty->built_up_area);
        $net_income['yield'] = '';
        $income_approach['net_income'] = $net_income;

        /*End net_income Income Value Calculations*/

        /*Start net_yield_applied Income Value Calculations*/

        $net_yield_applied['factor'] = round($gross_rental_income['yield'] * (1 - (($operations_and_maintainance_costs['factor'] / 100) + ($vacancy['factor'] / 100) + ($land_lease_costs['factor'] / 100) + ($insurance['factor'] / 100))), 2);
       if($net_yield_applied['factor'] > 0){
       $net_yield_applied['source'] = 1 / ($net_yield_applied['factor'] / 100);
    }else{
           $net_yield_applied['source'] = 0;
    }


        $net_yield_applied['psf'] = ($insurance['source'] > 0)? round($net_yield_applied['source'] / $insurance['source']): 0;
        $net_yield_applied['yield'] = '';
        $income_approach['net_yield_applied'] = $net_yield_applied;

        /*End net_yield_applied Income Value Calculations*/

        /*Start estimated_market_value Income Value Calculations*/

        $estimated_market_value['factor'] = '';
        $estimated_market_value['source'] = $net_income['source'] * $net_yield_applied['source'];
        $estimated_market_value['psf'] = round($estimated_market_value['source'] / $InspectProperty->built_up_area);;
        $estimated_market_value['yield'] = '';
        $income_approach['estimated_market_value'] = $estimated_market_value;

        /*End estimated_market_value Income Value Calculations*/


        return [
            'model' => $valuation,
            'valuation' => $valuation,
            'income_approach' => $income_approach,
            'estimated_market_value' => $estimated_market_value['source'],
            'estimated_market_rent' => $gross_rental_income['source'],
        ];
    }

    public function actionSummary($id)
    {
        $valuation = $this->findModel($id);
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $building_info = \app\models\Buildings::find()->where(['id' => $valuation->building_info])->one();
        $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();
        $developer_margin = \app\models\ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();
        // $developments_info = \app\models\Developments::find()->where(['emirates' => $valuation->building->city,'property_type'=> $valuation->property_id ])->asArray()->one();
        $drive_margin_sold = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();
        $drive_margin_list = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'list'])->one();
        $drive_margin_previous = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'previous'])->one();
        $costapproch_data = $this->actionCostEstimateValue($id);
        $incomeapproch_data = $this->actionIncomeEstimateValue($id);
        $list_calculate_data = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'list'])->one();


        $summary = array();

        $summary['valuation_approach'] = Yii::$app->appHelperFunctions->valuationApproachListArr[$building_info->property->valuation_approach];
        $summary['basis_of_value'] = $building_info->property->basis_of_value;
        $summary['approach_reasoning'] = $building_info->property->approach_reason;


        $original_purchase_price['mv'] = $cost_info->original_purchase_price;
        $original_purchase_price['mv_per_bua'] = round($cost_info->original_purchase_price / $InspectProperty->built_up_area, 2);
        $original_purchase_price['mv_per_land_size'] = ($valuation->land_size > 0)? round($cost_info->original_purchase_price / $valuation->land_size, 2) : 0;
        $summary['original_purchase_price'] = $original_purchase_price;

        $transaction_price['mv'] = $cost_info->transaction_price;
        $transaction_price['mv_per_bua'] = round($cost_info->transaction_price / $InspectProperty->built_up_area, 2);
        $transaction_price['mv_per_land_size'] = ($valuation->land_size > 0) ? round($cost_info->transaction_price / $valuation->land_size, 2): 0;
        $summary['transaction_price'] = $original_purchase_price;

        $elevation_analysis_price['mv'] = $developer_margin->estimated_price_per_valuation;
        $elevation_analysis_price['mv_per_bua'] = round($developer_margin->estimated_price_per_valuation / $InspectProperty->built_up_area, 2);
        $elevation_analysis_price['mv_per_land_size'] = ($valuation->land_size > 0)? round($developer_margin->estimated_price_per_valuation / $valuation->land_size, 2): 0;
        $summary['elevation_analysis_price'] = $elevation_analysis_price;


        $mv_sold['mv'] = $drive_margin_sold->mv_total_price;
        $mv_sold['mv_per_bua'] = round($drive_margin_sold->mv_total_price / $InspectProperty->built_up_area, 2);
        $mv_sold['mv_per_land_size'] = ($valuation->land_size > 0 ) ? round($drive_margin_sold->mv_total_price / $valuation->land_size, 2) : 0;
        $summary['mv_sold'] = $mv_sold;

        $mv_list['mv'] = $drive_margin_list->mv_total_price;
        $mv_list['mv_per_bua'] = round($drive_margin_list->mv_total_price / $InspectProperty->built_up_area, 2);
        $mv_list['mv_per_land_size'] = ($valuation->land_size > 0) ? round($drive_margin_list->mv_total_price / $valuation->land_size, 2) : 0;
        $summary['mv_list'] = $mv_list;

        $mv_prvious['mv'] = $drive_margin_previous->mv_total_price;
        $mv_prvious['mv_per_bua'] = round($drive_margin_previous->mv_total_price / $InspectProperty->built_up_area, 2);
        $mv_prvious['mv_per_land_size'] = ($valuation->land_size > 0) ? round($drive_margin_previous->mv_total_price / $valuation->land_size, 2) : 0;
        $summary['mv_prvious'] = $mv_prvious;
if($valuation->property->title != 'Apartment') {
    $mv_cost['mv'] = $costapproch_data['estimated_total_cost_value'];
    $mv_cost['mv_per_bua'] = round($costapproch_data['estimated_total_cost_value'] / $InspectProperty->built_up_area, 2);
    $mv_cost['mv_per_land_size'] = ($valuation->land_size > 0) ? round($costapproch_data['estimated_total_cost_value'] / $valuation->land_size, 2) : 0;
    $summary['mv_cost'] = $mv_cost;
}

        $mv_income['mv'] = $incomeapproch_data['estimated_market_value'];
        $mv_income['mv_per_bua'] = round($incomeapproch_data['estimated_market_value'] / $InspectProperty->built_up_area, 2);
        $mv_income['mv_per_land_size'] = ($valuation->land_size > 0) ? round($incomeapproch_data['estimated_market_value'] / $valuation->land_size, 2) : 0;
        $summary['mv_income'] = $mv_income;
        if($valuation->property->title != 'Apartment') {
            $avaerage['mv'] = round((($mv_sold['mv'] + $mv_list['mv'] + $mv_prvious['mv'] + $mv_cost['mv'] + $mv_income['mv']) / 5), 2);
            $avaerage['mv_per_bua'] = round((($mv_sold['mv_per_bua'] + $mv_list['mv_per_bua'] + $mv_prvious['mv_per_bua'] + $mv_cost['mv_per_bua'] + $mv_income['mv_per_bua']) / 5), 2);
            $avaerage['mv_per_land_size'] = round((($mv_sold['mv_per_land_size'] + $mv_list['mv_per_land_size'] + $mv_prvious['mv_per_land_size'] + $mv_cost['mv_per_land_size'] + $mv_income['mv_per_land_size']) / 5), 2);
            $summary['avaerage'] = $avaerage;
        }else{
            $avaerage['mv'] = round((($mv_sold['mv'] + $mv_list['mv'] + $mv_prvious['mv']  + $mv_income['mv']) / 4), 2);
            $avaerage['mv_per_bua'] = round((($mv_sold['mv_per_bua'] + $mv_list['mv_per_bua'] + $mv_prvious['mv_per_bua']  + $mv_income['mv_per_bua']) / 4), 2);
            $avaerage['mv_per_land_size'] = round((($mv_sold['mv_per_land_size'] + $mv_list['mv_per_land_size'] + $mv_prvious['mv_per_land_size']  + $mv_income['mv_per_land_size']) / 4), 2);
            $summary['avaerage'] = $avaerage;
        }
        $summary['market_value_of_land'] = $cost_info->lands_price;
        $summary['estimated_market_rent'] = $incomeapproch_data['estimated_market_rent'];
        $summary['gross_yield'] = $list_calculate_data->avg_gross_yield;
        if($valuation->property->title != 'Apartment') {
            $summary['approval_market_value'] = round((($mv_sold['mv'] + $mv_list['mv'] + $mv_prvious['mv'] + $mv_cost['mv'] + $mv_income['mv']) / 5), 3);
        }else{
            $summary['approval_market_value'] = round((($mv_sold['mv'] + $mv_list['mv'] + $mv_prvious['mv']  + $mv_income['mv']) / 5), 3);
        }
        $summary['approval_date'] = Yii::$app->formatter->asDate(date('Y-m-d'));
        return $summary;
    }

    public function actionStep_24($id)
    {
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_24')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            $model->signature_img = UploadedFile::getInstance($model, 'signature_img');
            if ($model->signature_img<>null) {
                if ($model->upload()) {

                    // if (Yii::$app->request->post()['Valuation']['sig_verification_status'] !=null) {
                    //     $model->sig_verification_status= Yii::$app->request->post()['Valuation']['sig_verification_status'];
                    // }
                    $model->signature_img =$model->signature_img->baseName . '.' . $model->signature_img->extension;


                    if(!$model->save()){
                    }
                    return $this->redirect(['valuation/step_24/' . $id]);
                }else {

                }
            }
            else{

                return $this->redirect(['valuation/step_24/' . $id]);
            }
        }
        return $this->render('steps/_step24', [
            'model' => $model,
        ]);
    }



    /**
     * Deletes an existing Valuation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

public function numberTowords1($amount)
    {
        $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
        // Check if there is any number after decimal
        $amt_hundred = null;
        $count_length = strlen($num);
        $x = 0;
        $string = array();
        $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
            3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
            7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
            10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
            13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
            16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
            19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
            40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
            70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
        $here_digits = array('', 'Hundred','Thousand','Thousand Hundred', 'Crore');
        while( $x < $count_length ) {
            $get_divider = ($x == 2) ? 10 : 100;
            $amount = floor($num % $get_divider);
            $num = floor($num / $get_divider);
            $x += $get_divider == 10 ? 1 : 2;
            if ($amount) {
                $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
                $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
                $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.' 
         '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. ' 
         '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
            }else $string[] = null;
        }
        $implode_to_Rupees = implode('', array_reverse($string));
        $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . " 
   " . $change_words[$amount_after_decimal % 10]) . ' Fils' : '';
        return ($implode_to_Rupees ? $implode_to_Rupees . 'AED ' : '') . $get_paise;
    }

    function convertNumberToWord($num = false)
    {
        $num = str_replace(array(',', ' '), '' , trim($num));
        if(! $num) {
            return false;
        }
        $num = (int) $num;
        $words = array();
        $list1 = array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven',
            'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'
        );
        $list2 = array('', 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety', 'Hundred');
        $list3 = array('', 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion', 'Quintillion', 'Sextillion', 'Septillion',
            'Octillion', 'Nonillion', 'Decillion', 'Undecillion', 'Duodecillion', 'Tredecillion', 'Quattuordecillion',
            'Quindecillion', 'Sexdecillion', 'Septendecillion', 'Octodecillion', 'Novemdecillion', 'Vigintillion'
        );
        $num_length = strlen($num);
        $levels = (int) (($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);
        for ($i = 0; $i < count($num_levels); $i++) {
            $levels--;
            $hundreds = (int) ($num_levels[$i] / 100);
            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
            $tens = (int) ($num_levels[$i] % 100);
            $singles = '';
            if ( $tens < 20 ) {
                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
            } else {
                $tens = (int)($tens / 10);
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int) ($num_levels[$i] % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
        } //end for loop
        $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }
        return implode(' ', $words);
    }


    function numberTowords($num)
    {

        $ones = array(
            0 =>"Zero",
            1 => "One",
            2 => "Two",
            3 => "Three",
            4 => "Four",
            5 => "Five",
            6 => "Six",
            7 => "Seven",
            8 => "Eight",
            9 => "Nine",
            10 => "Ten",
            11 => "Eleven",
            12 => "Twelve",
            13 => "Thirteen",
            14 => "Fourteen",
            15 => "Fifteen",
            16 => "Sixteen",
            17 => "Seventeen",
            18 => "Eighteen",
            19 => "Nineteen",
            "014" => "Fourteen"
        );
        $tens = array(
            0 => "Zero",
            1 => "Ten",
            2 => "Twenty",
            3 => "Thirty",
            4 => "Forty",
            5 => "Fifty",
            6 => "Sixty",
            7 => "Seventy",
            8 => "Eighty",
            9 => "Ninety"
        );
        $hundreds = array(
            "Hundred",
            "Thousand",
            "Million",
            "Billion",
            "Trillion",
            "Quardrillion"
        ); /*limit t quadrillion */
        $num = number_format($num,2,".",",");
        $num_arr = explode(".",$num);
        $wholenum = $num_arr[0];
        $decnum = $num_arr[1];
        $whole_arr = array_reverse(explode(",",$wholenum));
        krsort($whole_arr,1);
        $rettxt = "";
        foreach($whole_arr as $key => $i){

            while(substr($i,0,1)=="0")
                $i=substr($i,1,5);
            if($i < 20){
                /* echo "getting:".$i; */
                $rettxt .= $ones[$i];
            }elseif($i < 100){
                if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)];
                if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)];
            }else{
                if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0];
                if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)];
                if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)];
            }
            if($key > 0){
                $rettxt .= " ".$hundreds[$key]." ";
            }
        }
        if($decnum > 0){
            $rettxt .= " and ";
            if($decnum < 20){
                $rettxt .= $ones[$decnum];
            }elseif($decnum < 100){
                $rettxt .= $tens[substr($decnum,0,1)];
                $rettxt .= " ".$ones[substr($decnum,1,1)];
            }
        }
        return ucwords($rettxt);
    }

    public function actionRevise($id)
    {
        //step 1
        $model_old = $this->findModel($id);
        $valuation_history = Valuation::find()->where(['parent_id' => $id])->orderBy(['id'=> 'DESC'])->all();
        $valuation_root = Valuation::find()->where(['id' => $model_old->root_id])->one();
        $valuation_history_root = Valuation::find()->where(['root_id' => $model_old->root_id])->orderBy(['id'=> 'DESC'])->all();


        unset($model_old->id);
        unset($model_old->approval_id);
        unset($model_old->approval_status);
        unset($model_old->revise_reason);
        unset($model_old->created_by);
        unset($model_old->created_at);
        unset($model_old->updated_at);
        unset($model_old->updated_by);
        unset($model_old->trashed);
        unset($model_old->trashed_at);
        unset($model_old->trashed_by);


        $model_old->parent_id = $id;
        if(($model_old->root_id <> null && $model_old->root_id > 0)) {
            $model_old->reference_number = $valuation_root->reference_number.'-V-'.(count($valuation_history_root) + 1);
        }else if($valuation_history <> null && count($valuation_history) > 0){
            $model_old->reference_number = $model_old->reference_number.'-V-'.(count($valuation_history) + 1);
        }else{
            $model_old->root_id = $id;
            $model_old->reference_number = $model_old->reference_number.'-V-1';
        }
        $model = new Valuation();
        $model->setAttributes($model_old->attributes);
        if($model->email_subject == null){
            $model->email_subject = $model_old->reference_number;
        }
        $model->parent_id = $id;
        $model->valuation_status = 1;
        if ($model->save()) {


            //step 2
            $step_2_data = ReceivedDocs::find()->where(['valuation_id' => $id])->one();
            unset($step_2_data->id);
            unset($step_2_data->email_status);

            $owners = \app\models\ValuationOwners::find()->where(['valuation_id' => $id])->asArray()->all();
            $received_docs = \app\models\ReceivedDocsFiles::find()->where(['valuation_id' => $id])->asArray()->all();
            $step_2 = new ReceivedDocs();
            $step_2->setAttributes($step_2_data->attributes);
            $step_2->valuation_id = $model->id;
            $step_2->save();
            // Save all owners terms
            foreach ($owners as $owner_data) {
                $owner_data_detail = new ValuationOwners();
                $owner_data_detail->name = $owner_data['name'];
                $owner_data_detail->percentage = $owner_data['percentage'];
                $owner_data_detail->index_id = $owner_data['index_id'];
                $owner_data_detail->valuation_id = $model->id;
                $owner_data_detail->save();
            }

            // Save all Documents terms
            foreach ($received_docs as $received_doc) {
                $received_data_detail = new ReceivedDocsFiles();
                $received_data_detail->document_id = $received_doc['document_id'];
                $received_data_detail->attachment = $received_doc['attachment'];
                $received_data_detail->valuation_id = $model->id;
                $received_data_detail->save();
            }


            //step 3
            $step_3_data = ValuationConflict::find()->where(['valuation_id' => $id])->one();
            unset($step_3_data->id);
            $step_3 = new ValuationConflict();
            $step_3->setAttributes($step_3_data->attributes);
            $step_3->valuation_id = $model->id;
            $step_3->save();

            //step 4

            $step_4_data = ScheduleInspection::find()->where(['valuation_id' => $id])->one();
            unset($step_4_data->id);
            unset($step_4_data->email_status);
            $step_4 = new ScheduleInspection();
            $step_4->setAttributes($step_4_data->attributes);
            $step_4->valuation_id = $model->id;
            $step_4->save();

            //step 5
            $step_5_data = InspectProperty::find()->where(['valuation_id' => $id])->one();

            unset($step_5_data->id);
            unset($step_5_data->email_status);
            $step_5 = new InspectProperty();
            $step_5->setAttributes($step_5_data->attributes);

            $step_5->valuation_id = $model->id;

            if( !$step_5->save()){
            }
            /*echo "<pre>";
            print_r($step_5);
            die;*/

            //step 6
            $step_6_data = ValuationConfiguration::find()->where(['valuation_id' => $id])->one();
            $configurationFiles = \app\models\ConfigurationFiles::find()->where(['valuation_id' => $id])->asArray()->all();
            unset($step_6_data->id);
            $step_6 = new ValuationConfiguration();
            $step_6->setAttributes($step_6_data->attributes);
            $step_6->valuation_id = $model->id;
            if(!$step_6->save()){
                /*  echo "<pre>";
                  print_r($step_6->errors);
                  die;*/
            }

            if(!empty($configurationFiles)) {
                foreach ($configurationFiles as  $config_data) {
                    $config_detail = new ConfigurationFiles();
                    $config_detail->type = $config_data['type'];
                    $config_detail->floor = $config_data['floor'];
                    $config_detail->flooring = $config_data['flooring'];
                    $config_detail->ceilings = $config_data['ceilings'];
                    $config_detail->speciality = $config_data['speciality'];
                    $config_detail->upgrade = $config_data['upgrade'];
                    $config_detail->attachment = $config_data['attachment'];
                    $config_detail->index_id = $config_data['index_id'];
                    $config_detail->valuation_id = $model->id;
                    $config_detail->checked_image = $config_data['checked_image'];
                    $config_detail->save();
                }
            }


            $step_6_latest = ValuationConfiguration::find()->where(['valuation_id' =>  $model->id])->one();
            if($step_6_data->over_all_upgrade <> null){
                \Yii::$app->db->createCommand("UPDATE valuation_configuration SET over_all_upgrade=".$step_6_data->over_all_upgrade." WHERE id=".$step_6_latest->id)->execute();
            }

            //step 7
            $step_7_data = CostDetails::find()->where(['valuation_id' => $id])->one();
            unset($step_7_data->id);
            $step_7 = new CostDetails();
            $step_7->setAttributes($step_7_data->attributes);
            $step_7->valuation_id = $model->id;
            $step_7->save();


            //step 8
            $step_8_data = ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();
            $payment_info = \app\models\ValuationDmPayments::find()->where(['valuation_id' => $id])->asArray()->all();
            unset($step_8_data->id);
            $step_8 = new ValuationDeveloperDriveMargin();
            $step_8->setAttributes($step_8_data->attributes);
            $step_8->valuation_id = $model->id;
            $step_8->save();

            // Save all payments terms
            if(!empty($payment_info)) {
                foreach ($payment_info as $key => $payment_data) {
                    $payment_data_detail = new ValuationDmPayments();
                    $payment_data_detail->installment_date = $payment_data['installment_date'];
                    $payment_data_detail->no_of_days = $payment_data['no_of_days'];
                    $payment_data_detail->percentage = $payment_data['percentage'];
                    $payment_data_detail->amount = $payment_data['amount'];
                    $payment_data_detail->present_value = $payment_data['present_value'];
                    $payment_data_detail->index_id = $payment_data['index_id'];
                    $payment_data_detail->valuation_id = $model->id;
                    $payment_data_detail->save();
                }
            }

            //step 9
            //done in first step


            //step 10 - 13 - 16 (enquiry)
            $step_10_13_16_data = ValuationEnquiry::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_10_13_16_data)) {
                foreach ($step_10_13_16_data as $key => $enquiry_data) {
                    $step_10_13_16 = new ValuationEnquiry();
                    $step_10_13_16->date_from = $enquiry_data->date_from;
                    $step_10_13_16->date_to = $enquiry_data->date_to;
                    $step_10_13_16->building_info = $enquiry_data->building_info;
                    $step_10_13_16->property_id = $enquiry_data->property_id;
                    $step_10_13_16->property_category = $enquiry_data->property_category;
                    $step_10_13_16->bedroom_from = $enquiry_data->bedroom_from;
                    $step_10_13_16->bedroom_to = $enquiry_data->bedroom_to;
                    $step_10_13_16->landsize_from = $enquiry_data->landsize_from;
                    $step_10_13_16->landsize_to = $enquiry_data->landsize_to;
                    $step_10_13_16->bua_from = $enquiry_data->bua_from;
                    $step_10_13_16->bua_to = $enquiry_data->bua_to;
                    $step_10_13_16->price_from = $enquiry_data->price_from;
                    $step_10_13_16->price_to = $enquiry_data->price_to;
                    $step_10_13_16->price_psf_from = $enquiry_data->price_psf_from;
                    $step_10_13_16->price_psf_to = $enquiry_data->price_psf_to;
                    $step_10_13_16->type = $enquiry_data->type;
                    $step_10_13_16->valuation_id =  $model->id;
                    $step_10_13_16->save();
                }
            }


            //step 12 - 14 - 17 (List Selection)
            $step_12_14_17_list = ValuationSelectedLists::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_12_14_17_list)) {
                foreach ($step_12_14_17_list as $selected_row) {
                    $selected_data_detail = new ValuationSelectedLists();
                    $selected_data_detail->selected_id = $selected_row->selected_id;
                    $selected_data_detail->type =  $selected_row->type;
                    $selected_data_detail->valuation_id = $model->id;
                    $selected_data_detail->save();
                }
            }

            $step_12_14_17_list_calculations = ValuationListCalculation::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_12_14_17_list_calculations)) {
                foreach ($step_12_14_17_list_calculations as $list_row) {
                    $calculations = new ValuationListCalculation();
                    $calculations->avg_bedrooms = $list_row->avg_bedrooms;
                    $calculations->avg_land_size = $list_row->avg_land_size;
                    $calculations->built_up_area_size = $list_row->built_up_area_size;
                    $calculations->avg_listings_price_size = $list_row->avg_listings_price_size;
                    $calculations->min_price = $list_row->min_price;
                    $calculations->max_price = $list_row->max_price;
                    $calculations->min_price_sqt = $list_row->min_price_sqt;
                    $calculations->max_price_sqt = $list_row->max_price_sqt;
                    $calculations->avg_listing_date = $list_row->avg_listing_date;
                    $calculations->avg_psf = $list_row->avg_psf;
                    $calculations->avg_gross_yield = $list_row->avg_gross_yield;
                    $calculations->type = $list_row->type;
                    $calculations->valuation_id = $model->id;
                    $calculations->save();
                }
            }





            //step 13 - 15 - 18 (Derive MV)

            $step_13_15_18_derive_mv = ValuationDriveMv::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_13_15_18_derive_mv)) {
                foreach ($step_13_15_18_derive_mv as $mv_row) {
                    $drive_margin_saved_data = new ValuationDriveMv();
                    $drive_margin_saved_data->average_location = $mv_row->average_location;
                    $drive_margin_saved_data->average_age = $mv_row->average_age;
                    $drive_margin_saved_data->average_tenure = $mv_row->average_tenure;
                    $drive_margin_saved_data->average_view = $mv_row->average_view;
                    $drive_margin_saved_data->average_finished_status = $mv_row->average_finished_status;
                    $drive_margin_saved_data->average_property_condition = $mv_row->average_property_condition;
                    $drive_margin_saved_data->average_upgrades = $mv_row->average_upgrades;
                    $drive_margin_saved_data->average_property_exposure =$mv_row->average_property_exposure;
                    $drive_margin_saved_data->average_property_placement = $mv_row->average_property_placement;
                    $drive_margin_saved_data->average_full_building_floors = $mv_row->average_full_building_floors;
                    $drive_margin_saved_data->average_number_of_levels = $mv_row->average_number_of_levels;
                    $drive_margin_saved_data->average_no_of_bedrooms = $mv_row->average_no_of_bedrooms;
                    $drive_margin_saved_data->average_parking_space = $mv_row->average_parking_space;
                    $drive_margin_saved_data->average_pool = $mv_row->average_pool;
                    $drive_margin_saved_data->average_landscaping = $mv_row->average_landscaping;
                    $drive_margin_saved_data->average_white_goods = $mv_row->average_white_goods;
                    $drive_margin_saved_data->average_utilities_connected = $mv_row->average_utilities_connected;
                    $drive_margin_saved_data->average_developer_margin =$mv_row->average_developer_margin;
                    $drive_margin_saved_data->average_land_size = $mv_row->average_land_size;
                    $drive_margin_saved_data->average_balcony_size =$mv_row->average_balcony_size;
                    $drive_margin_saved_data->average_built_up_area = $mv_row->average_built_up_area;
                    $drive_margin_saved_data->average_date = $mv_row->average_date;
                    $drive_margin_saved_data->average_sale_price = $mv_row->average_sale_price;
                    $drive_margin_saved_data->average_psf = $mv_row->average_psf;
                    $drive_margin_saved_data->weightage_location = $mv_row->weightage_location;
                    $drive_margin_saved_data->weightage_age = $mv_row->weightage_age;
                    $drive_margin_saved_data->weightage_tenure = $mv_row->weightage_tenure;
                    $drive_margin_saved_data->weightage_view = $mv_row->weightage_view;
                    $drive_margin_saved_data->weightage_finished_status = $mv_row->weightage_finished_status;
                    $drive_margin_saved_data->weightage_property_condition = $mv_row->weightage_property_condition;
                    $drive_margin_saved_data->weightage_furnished = $mv_row->weightage_furnished;
                    $drive_margin_saved_data->weightage_property_exposure = $mv_row->weightage_property_exposure;
                    $drive_margin_saved_data->weightage_property_placement = $mv_row->weightage_property_placement;
                    $drive_margin_saved_data->weightage_full_building_floors = $mv_row->weightage_full_building_floors;
                    $drive_margin_saved_data->weightage_number_of_levels = $mv_row->weightage_number_of_levels;
                    $drive_margin_saved_data->weightage_no_of_bedrooms = $mv_row->weightage_no_of_bedrooms;
                    $drive_margin_saved_data->weightage_parking_space = $mv_row->weightage_parking_space;
                    $drive_margin_saved_data->weightage_pool = $mv_row->weightage_pool;
                    $drive_margin_saved_data->weightage_landscaping = $mv_row->weightage_landscaping;
                    $drive_margin_saved_data->weightage_white_goods = $mv_row->weightage_white_goods;
                    $drive_margin_saved_data->weightage_utilities_connected = $mv_row->weightage_utilities_connected;
                    $drive_margin_saved_data->weightage_developer_margin = $mv_row->weightage_developer_margin;
                    $drive_margin_saved_data->weightage_land_size = $mv_row->weightage_land_size;
                    $drive_margin_saved_data->weightage_balcony_size = $mv_row->weightage_balcony_size;
                    $drive_margin_saved_data->weightage_built_up_area =  $mv_row->weightage_built_up_area;
                    $drive_margin_saved_data->weightage_date = $mv_row->weightage_date;
                    $drive_margin_saved_data->valuation_id = $model->id;
                    $drive_margin_saved_data->type = $mv_row->type;
                    $drive_margin_saved_data->save();
                }
            }


            //step 21, 22,23
            $step_23_data = ValuationApproversData::find()->where(['valuation_id' => $id])->all();

            //   unset($step_23_data->id);
            //  unset($step_23_data->status);

            if(!empty($step_23_data)) {
                foreach ($step_23_data as $step_23_row) {
                    unset($step_23_data->email_status);
                    $calculations = new ValuationApproversData();
                    $estimated_market_value = str_replace(",", "", $step_23_row->estimated_market_value);

                    $calculations->estimated_market_value = floatval($estimated_market_value);
                    $calculations->estimated_market_value_sqf = $step_23_row->estimated_market_value_sqf;
                    $calculations->estimated_market_rent = $step_23_row->estimated_market_rent;
                    $calculations->estimated_market_rent_sqf = $step_23_row->estimated_market_rent_sqf;
                    $calculations->parking_market_value = $step_23_row->parking_market_value;
                    $calculations->parking_market_value_sqf = $step_23_row->parking_market_value_sqf;
                    $calculations->approver_type = $step_23_row->approver_type;
                    $calculations->reason = $step_23_row->reason;
                    $calculations->status = $step_23_row->status;
                    $calculations->valuation_id = $model->id;
                    if(!$calculations->save()){

                    }
                }
            }

            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->redirect(['valuation/step_1/' . $model->id]);
        } else {
           /* echo "<pre>";
            print_r($model->errors);
            die;*/
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        die('Process is in progress.');



    }
    public function actionEdit($id)
    {
        //step 1
        $model_old = $this->findModel($id);
        $valuation_history = Valuation::find()->where(['parent_id' => $id])->orderBy(['id'=> 'DESC'])->all();
        $valuation_root = Valuation::find()->where(['id' => $model_old->root_id])->one();
        $valuation_history_root = Valuation::find()->where(['root_id' => $model_old->root_id])->orderBy(['id'=> 'DESC'])->all();


        unset($model_old->id);
        unset($model_old->approval_id);
        unset($model_old->approval_status);
        unset($model_old->revise_reason);
        unset($model_old->created_by);
        unset($model_old->created_at);
        unset($model_old->updated_at);
        unset($model_old->updated_by);
        unset($model_old->trashed);
        unset($model_old->trashed_at);
        unset($model_old->trashed_by);


        $model_old->parent_id = $id;
        if(($model_old->root_id <> null && $model_old->root_id > 0)) {
            $model_old->reference_number = $valuation_root->reference_number.'-V-'.(count($valuation_history_root) + 1);
        }else if($valuation_history <> null && count($valuation_history) > 0){
            $model_old->reference_number = $model_old->reference_number.'-V-'.(count($valuation_history) + 1);
        }else{
            $model_old->root_id = $id;
            $model_old->reference_number = $model_old->reference_number.'-V-1';
        }
        $model = new Valuation();
        $model->setAttributes($model_old->attributes);
        if($model->email_subject == null){
            $model->email_subject = $model_old->reference_number;
        }
        $model->parent_id = $id;
        if ($model->save()) {


            //step 2
            $step_2_data = ReceivedDocs::find()->where(['valuation_id' => $id])->one();
            unset($step_2_data->id);
            // unset($step_2_data->email_status);

            $owners = \app\models\ValuationOwners::find()->where(['valuation_id' => $id])->asArray()->all();
            $received_docs = \app\models\ReceivedDocsFiles::find()->where(['valuation_id' => $id])->asArray()->all();
            $step_2 = new ReceivedDocs();
            $step_2->setAttributes($step_2_data->attributes);
            $step_2->valuation_id = $model->id;
            $step_2->save();
            // Save all owners terms
            foreach ($owners as $owner_data) {
                $owner_data_detail = new ValuationOwners();
                $owner_data_detail->name = $owner_data['name'];
                $owner_data_detail->percentage = $owner_data['percentage'];
                $owner_data_detail->index_id = $owner_data['index_id'];
                $owner_data_detail->valuation_id = $model->id;
                $owner_data_detail->save();
            }

            // Save all Documents terms
            foreach ($received_docs as $received_doc) {
                $received_data_detail = new ReceivedDocsFiles();
                $received_data_detail->document_id = $received_doc['document_id'];
                $received_data_detail->attachment = $received_doc['attachment'];
                $received_data_detail->valuation_id = $model->id;
                $received_data_detail->save();
            }


            //step 3
            $step_3_data = ValuationConflict::find()->where(['valuation_id' => $id])->one();
            unset($step_3_data->id);
            $step_3 = new ValuationConflict();
            $step_3->setAttributes($step_3_data->attributes);
            $step_3->valuation_id = $model->id;
            $step_3->save();

            //step 4

            $step_4_data = ScheduleInspection::find()->where(['valuation_id' => $id])->one();
            unset($step_4_data->id);
            // unset($step_4_data->email_status);
            $step_4 = new ScheduleInspection();
            $step_4->setAttributes($step_4_data->attributes);
            $step_4->valuation_id = $model->id;
            $step_4->save();

            //step 5
            $step_5_data = InspectProperty::find()->where(['valuation_id' => $id])->one();

            unset($step_5_data->id);
            // unset($step_5_data->email_status);
            $step_5 = new InspectProperty();
            $step_5->setAttributes($step_5_data->attributes);

            $step_5->valuation_id = $model->id;

            if( !$step_5->save()){
            }
            /*echo "<pre>";
            print_r($step_5);
            die;*/

            //step 6
            $step_6_data = ValuationConfiguration::find()->where(['valuation_id' => $id])->one();
            $configurationFiles = \app\models\ConfigurationFiles::find()->where(['valuation_id' => $id])->asArray()->all();
            unset($step_6_data->id);
            $step_6 = new ValuationConfiguration();
            $step_6->setAttributes($step_6_data->attributes);
            $step_6->valuation_id = $model->id;
            if(!$step_6->save()){
                /*  echo "<pre>";
                  print_r($step_6->errors);
                  die;*/
            }

            if(!empty($configurationFiles)) {
                foreach ($configurationFiles as  $config_data) {
                    $config_detail = new ConfigurationFiles();
                    $config_detail->type = $config_data['type'];
                    $config_detail->floor = $config_data['floor'];
                    $config_detail->flooring = $config_data['flooring'];
                    $config_detail->ceilings = $config_data['ceilings'];
                    $config_detail->speciality = $config_data['speciality'];
                    $config_detail->upgrade = $config_data['upgrade'];
                    $config_detail->attachment = $config_data['attachment'];
                    $config_detail->index_id = $config_data['index_id'];
                    $config_detail->valuation_id = $model->id;
                    $config_detail->checked_image = $config_data['checked_image'];
                    $config_detail->save();
                }
            }


            $step_6_latest = ValuationConfiguration::find()->where(['valuation_id' =>  $model->id])->one();
            if($step_6_data->over_all_upgrade <> null){
                \Yii::$app->db->createCommand("UPDATE valuation_configuration SET over_all_upgrade=".$step_6_data->over_all_upgrade." WHERE id=".$step_6_latest->id)->execute();
            }

            //step 7
            $step_7_data = CostDetails::find()->where(['valuation_id' => $id])->one();
            unset($step_7_data->id);
            $step_7 = new CostDetails();
            $step_7->setAttributes($step_7_data->attributes);
            $step_7->valuation_id = $model->id;
            $step_7->save();


            //step 8
            $step_8_data = ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();
            $payment_info = \app\models\ValuationDmPayments::find()->where(['valuation_id' => $id])->asArray()->all();
            unset($step_8_data->id);
            $step_8 = new ValuationDeveloperDriveMargin();
            $step_8->setAttributes($step_8_data->attributes);
            $step_8->valuation_id = $model->id;
            $step_8->save();

            // Save all payments terms
            if(!empty($payment_info)) {
                foreach ($payment_info as $key => $payment_data) {
                    $payment_data_detail = new ValuationDmPayments();
                    $payment_data_detail->installment_date = $payment_data['installment_date'];
                    $payment_data_detail->no_of_days = $payment_data['no_of_days'];
                    $payment_data_detail->percentage = $payment_data['percentage'];
                    $payment_data_detail->amount = $payment_data['amount'];
                    $payment_data_detail->present_value = $payment_data['present_value'];
                    $payment_data_detail->index_id = $payment_data['index_id'];
                    $payment_data_detail->valuation_id = $model->id;
                    $payment_data_detail->save();
                }
            }

            //step 9
            //done in first step


            //step 10 - 13 - 16 (enquiry)
            $step_10_13_16_data = ValuationEnquiry::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_10_13_16_data)) {
                foreach ($step_10_13_16_data as $key => $enquiry_data) {
                    $step_10_13_16 = new ValuationEnquiry();
                    $step_10_13_16->date_from = $enquiry_data->date_from;
                    $step_10_13_16->date_to = $enquiry_data->date_to;
                    $step_10_13_16->building_info = $enquiry_data->building_info;
                    $step_10_13_16->property_id = $enquiry_data->property_id;
                    $step_10_13_16->property_category = $enquiry_data->property_category;
                    $step_10_13_16->bedroom_from = $enquiry_data->bedroom_from;
                    $step_10_13_16->bedroom_to = $enquiry_data->bedroom_to;
                    $step_10_13_16->landsize_from = $enquiry_data->landsize_from;
                    $step_10_13_16->landsize_to = $enquiry_data->landsize_to;
                    $step_10_13_16->bua_from = $enquiry_data->bua_from;
                    $step_10_13_16->bua_to = $enquiry_data->bua_to;
                    $step_10_13_16->price_from = $enquiry_data->price_from;
                    $step_10_13_16->price_to = $enquiry_data->price_to;
                    $step_10_13_16->price_psf_from = $enquiry_data->price_psf_from;
                    $step_10_13_16->price_psf_to = $enquiry_data->price_psf_to;
                    $step_10_13_16->type = $enquiry_data->type;
                    $step_10_13_16->valuation_id =  $model->id;
                    $step_10_13_16->save();
                }
            }


            //step 12 - 14 - 17 (List Selection)
            $step_12_14_17_list = ValuationSelectedLists::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_12_14_17_list)) {
                foreach ($step_12_14_17_list as $selected_row) {
                    $selected_data_detail = new ValuationSelectedLists();
                    $selected_data_detail->selected_id = $selected_row->selected_id;
                    $selected_data_detail->type =  $selected_row->type;
                    $selected_data_detail->valuation_id = $model->id;
                    $selected_data_detail->save();
                }
            }

            $step_12_14_17_list_calculations = ValuationListCalculation::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_12_14_17_list_calculations)) {
                foreach ($step_12_14_17_list_calculations as $list_row) {
                    $calculations = new ValuationListCalculation();
                    $calculations->avg_bedrooms = $list_row->avg_bedrooms;
                    $calculations->avg_land_size = $list_row->avg_land_size;
                    $calculations->built_up_area_size = $list_row->built_up_area_size;
                    $calculations->avg_listings_price_size = $list_row->avg_listings_price_size;
                    $calculations->min_price = $list_row->min_price;
                    $calculations->max_price = $list_row->max_price;
                    $calculations->min_price_sqt = $list_row->min_price_sqt;
                    $calculations->max_price_sqt = $list_row->max_price_sqt;
                    $calculations->avg_listing_date = $list_row->avg_listing_date;
                    $calculations->avg_psf = $list_row->avg_psf;
                    $calculations->avg_gross_yield = $list_row->avg_gross_yield;
                    $calculations->type = $list_row->type;
                    $calculations->valuation_id = $model->id;
                    $calculations->save();
                }
            }





            //step 13 - 15 - 18 (Derive MV)

            $step_13_15_18_derive_mv = ValuationDriveMv::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_13_15_18_derive_mv)) {
                foreach ($step_13_15_18_derive_mv as $mv_row) {
                    $drive_margin_saved_data = new ValuationDriveMv();
                    $drive_margin_saved_data->average_location = $mv_row->average_location;
                    $drive_margin_saved_data->average_age = $mv_row->average_age;
                    $drive_margin_saved_data->average_tenure = $mv_row->average_tenure;
                    $drive_margin_saved_data->average_view = $mv_row->average_view;
                    $drive_margin_saved_data->average_finished_status = $mv_row->average_finished_status;
                    $drive_margin_saved_data->average_property_condition = $mv_row->average_property_condition;
                    $drive_margin_saved_data->average_upgrades = $mv_row->average_upgrades;
                    $drive_margin_saved_data->average_property_exposure =$mv_row->average_property_exposure;
                    $drive_margin_saved_data->average_property_placement = $mv_row->average_property_placement;
                    $drive_margin_saved_data->average_full_building_floors = $mv_row->average_full_building_floors;
                    $drive_margin_saved_data->average_number_of_levels = $mv_row->average_number_of_levels;
                    $drive_margin_saved_data->average_no_of_bedrooms = $mv_row->average_no_of_bedrooms;
                    $drive_margin_saved_data->average_parking_space = $mv_row->average_parking_space;
                    $drive_margin_saved_data->average_pool = $mv_row->average_pool;
                    $drive_margin_saved_data->average_landscaping = $mv_row->average_landscaping;
                    $drive_margin_saved_data->average_white_goods = $mv_row->average_white_goods;
                    $drive_margin_saved_data->average_utilities_connected = $mv_row->average_utilities_connected;
                    $drive_margin_saved_data->average_developer_margin =$mv_row->average_developer_margin;
                    $drive_margin_saved_data->average_land_size = $mv_row->average_land_size;
                    $drive_margin_saved_data->average_balcony_size =$mv_row->average_balcony_size;
                    $drive_margin_saved_data->average_built_up_area = $mv_row->average_built_up_area;
                    $drive_margin_saved_data->average_date = $mv_row->average_date;
                    $drive_margin_saved_data->average_sale_price = $mv_row->average_sale_price;
                    $drive_margin_saved_data->average_psf = $mv_row->average_psf;
                    $drive_margin_saved_data->weightage_location = $mv_row->weightage_location;
                    $drive_margin_saved_data->weightage_age = $mv_row->weightage_age;
                    $drive_margin_saved_data->weightage_tenure = $mv_row->weightage_tenure;
                    $drive_margin_saved_data->weightage_view = $mv_row->weightage_view;
                    $drive_margin_saved_data->weightage_finished_status = $mv_row->weightage_finished_status;
                    $drive_margin_saved_data->weightage_property_condition = $mv_row->weightage_property_condition;
                    $drive_margin_saved_data->weightage_furnished = $mv_row->weightage_furnished;
                    $drive_margin_saved_data->weightage_property_exposure = $mv_row->weightage_property_exposure;
                    $drive_margin_saved_data->weightage_property_placement = $mv_row->weightage_property_placement;
                    $drive_margin_saved_data->weightage_full_building_floors = $mv_row->weightage_full_building_floors;
                    $drive_margin_saved_data->weightage_number_of_levels = $mv_row->weightage_number_of_levels;
                    $drive_margin_saved_data->weightage_no_of_bedrooms = $mv_row->weightage_no_of_bedrooms;
                    $drive_margin_saved_data->weightage_parking_space = $mv_row->weightage_parking_space;
                    $drive_margin_saved_data->weightage_pool = $mv_row->weightage_pool;
                    $drive_margin_saved_data->weightage_landscaping = $mv_row->weightage_landscaping;
                    $drive_margin_saved_data->weightage_white_goods = $mv_row->weightage_white_goods;
                    $drive_margin_saved_data->weightage_utilities_connected = $mv_row->weightage_utilities_connected;
                    $drive_margin_saved_data->weightage_developer_margin = $mv_row->weightage_developer_margin;
                    $drive_margin_saved_data->weightage_land_size = $mv_row->weightage_land_size;
                    $drive_margin_saved_data->weightage_balcony_size = $mv_row->weightage_balcony_size;
                    $drive_margin_saved_data->weightage_built_up_area =  $mv_row->weightage_built_up_area;
                    $drive_margin_saved_data->weightage_date = $mv_row->weightage_date;
                    $drive_margin_saved_data->valuation_id = $model->id;
                    $drive_margin_saved_data->type = $mv_row->type;
                    $drive_margin_saved_data->save();
                }
            }


            //step 21, 22,23
            $step_23_data = ValuationApproversData::find()->where(['valuation_id' => $id])->all();

            //   unset($step_23_data->id);
            //  unset($step_23_data->status);

            if(!empty($step_23_data)) {
                foreach ($step_23_data as $step_23_row) {
                    // unset($step_23_data->email_status);
                    $calculations = new ValuationApproversData();
                    $estimated_market_value = str_replace(",", "", $step_23_row->estimated_market_value);

                    $calculations->estimated_market_value = floatval($estimated_market_value);
                    $calculations->estimated_market_value_sqf = $step_23_row->estimated_market_value_sqf;
                    $calculations->estimated_market_rent = $step_23_row->estimated_market_rent;
                    $calculations->estimated_market_rent_sqf = $step_23_row->estimated_market_rent_sqf;
                    $calculations->parking_market_value = $step_23_row->parking_market_value;
                    $calculations->parking_market_value_sqf = $step_23_row->parking_market_value_sqf;
                    $calculations->approver_type = $step_23_row->approver_type;
                    $calculations->reason = $step_23_row->reason;
                    $calculations->status = $step_23_row->status;
                    $calculations->valuation_id = $model->id;
                    if(!$calculations->save()){

                    }
                }
            }

            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->redirect(['valuation/step_1/' . $model->id]);
        } else {
            /* echo "<pre>";
             print_r($model->errors);
             die;*/
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        die('Process is in progress.');



    }
    public function actionRevise_1($id)
    {
        //step 1
        $model_old = $this->findModel($id);
        $valuation_history = Valuation::find()->where(['parent_id' => $id])->orderBy(['id'=> 'DESC'])->all();
        $valuation_root = Valuation::find()->where(['id' => $model_old->root_id])->one();
        $valuation_history_root = Valuation::find()->where(['root_id' => $model_old->root_id])->orderBy(['id'=> 'DESC'])->all();


        unset($model_old->id);
        unset($model_old->approval_id);
        unset($model_old->approval_status);
        unset($model_old->revise_reason);
        unset($model_old->created_by);
        unset($model_old->created_at);
        unset($model_old->updated_at);
        unset($model_old->updated_by);
        unset($model_old->trashed);
        unset($model_old->trashed_at);
        unset($model_old->trashed_by);


        $model_old->parent_id = $id;
        if(($model_old->root_id <> null && $model_old->root_id > 0)) {
            $model_old->reference_number = $valuation_root->reference_number.'-V-'.(count($valuation_history_root) + 1);
        }else if($valuation_history <> null && count($valuation_history) > 0){
            $model_old->reference_number = $model_old->reference_number.'-V-'.(count($valuation_history) + 1);
        }else{
            $model_old->root_id = $id;
            $model_old->reference_number = $model_old->reference_number.'-V-1';
        }
        $model = new Valuation();
        $model->setAttributes($model_old->attributes);
        if($model->email_subject == null){
            $model->email_subject = $model_old->reference_number;
        }
        $model->parent_id = $id;
        $model->valuation_status = 1;
        if ($model->save()) {


            //step 2
            $step_2_data = ReceivedDocs::find()->where(['valuation_id' => $id])->one();
            unset($step_2_data->id);


            $owners = \app\models\ValuationOwners::find()->where(['valuation_id' => $id])->asArray()->all();
            $received_docs = \app\models\ReceivedDocsFiles::find()->where(['valuation_id' => $id])->asArray()->all();
            $step_2 = new ReceivedDocs();
            $step_2->setAttributes($step_2_data->attributes);
            $step_2->valuation_id = $model->id;
            $step_2->save();
            // Save all owners terms
            foreach ($owners as $owner_data) {
                $owner_data_detail = new ValuationOwners();
                $owner_data_detail->name = $owner_data['name'];
                $owner_data_detail->percentage = $owner_data['percentage'];
                $owner_data_detail->index_id = $owner_data['index_id'];
                $owner_data_detail->valuation_id = $model->id;
                $owner_data_detail->save();
            }

            // Save all Documents terms
            foreach ($received_docs as $received_doc) {
                $received_data_detail = new ReceivedDocsFiles();
                $received_data_detail->document_id = $received_doc['document_id'];
                $received_data_detail->attachment = $received_doc['attachment'];
                $received_data_detail->valuation_id = $model->id;
                $received_data_detail->save();
            }


            //step 3
            $step_3_data = ValuationConflict::find()->where(['valuation_id' => $id])->one();
            unset($step_3_data->id);
            $step_3 = new ValuationConflict();
            $step_3->setAttributes($step_3_data->attributes);
            $step_3->valuation_id = $model->id;
            $step_3->save();

            //step 4

            $step_4_data = ScheduleInspection::find()->where(['valuation_id' => $id])->one();
            unset($step_4_data->id);
            $step_4 = new ScheduleInspection();
            $step_4->setAttributes($step_4_data->attributes);
            $step_4->valuation_id = $model->id;
            $step_4->save();

            //step 5
            $step_5_data = InspectProperty::find()->where(['valuation_id' => $id])->one();

            unset($step_5_data->id);
            $step_5 = new InspectProperty();
            $step_5->setAttributes($step_5_data->attributes);

            $step_5->valuation_id = $model->id;

            if( !$step_5->save()){
            }
            /*echo "<pre>";
            print_r($step_5);
            die;*/

            //step 6
            $step_6_data = ValuationConfiguration::find()->where(['valuation_id' => $id])->one();
            $configurationFiles = \app\models\ConfigurationFiles::find()->where(['valuation_id' => $id])->asArray()->all();
            unset($step_6_data->id);
            $step_6 = new ValuationConfiguration();
            $step_6->setAttributes($step_6_data->attributes);
            $step_6->valuation_id = $model->id;
            if(!$step_6->save()){
                /*  echo "<pre>";
                  print_r($step_6->errors);
                  die;*/
            }

            if(!empty($configurationFiles)) {
                foreach ($configurationFiles as  $config_data) {
                    $config_detail = new ConfigurationFiles();
                    $config_detail->type = $config_data['type'];
                    $config_detail->floor = $config_data['floor'];
                    $config_detail->flooring = $config_data['flooring'];
                    $config_detail->ceilings = $config_data['ceilings'];
                    $config_detail->speciality = $config_data['speciality'];
                    $config_detail->upgrade = $config_data['upgrade'];
                    $config_detail->attachment = $config_data['attachment'];
                    $config_detail->index_id = $config_data['index_id'];
                    $config_detail->valuation_id = $model->id;
                    $config_detail->checked_image = $config_data['checked_image'];
                    $config_detail->save();
                }
            }


            $step_6_latest = ValuationConfiguration::find()->where(['valuation_id' =>  $model->id])->one();
            if($step_6_data->over_all_upgrade <> null){
                \Yii::$app->db->createCommand("UPDATE valuation_configuration SET over_all_upgrade=".$step_6_data->over_all_upgrade." WHERE id=".$step_6_latest->id)->execute();
            }

            //step 7
            $step_7_data = CostDetails::find()->where(['valuation_id' => $id])->one();
            unset($step_7_data->id);
            $step_7 = new CostDetails();
            $step_7->setAttributes($step_7_data->attributes);
            $step_7->valuation_id = $model->id;
            $step_7->save();


            //step 8
            $step_8_data = ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();
            $payment_info = \app\models\ValuationDmPayments::find()->where(['valuation_id' => $id])->asArray()->all();
            unset($step_8_data->id);
            $step_8 = new ValuationDeveloperDriveMargin();
            $step_8->setAttributes($step_8_data->attributes);
            $step_8->valuation_id = $model->id;
            $step_8->save();

            // Save all payments terms
            if(!empty($payment_info)) {
                foreach ($payment_info as $key => $payment_data) {
                    $payment_data_detail = new ValuationDmPayments();
                    $payment_data_detail->installment_date = $payment_data['installment_date'];
                    $payment_data_detail->no_of_days = $payment_data['no_of_days'];
                    $payment_data_detail->percentage = $payment_data['percentage'];
                    $payment_data_detail->amount = $payment_data['amount'];
                    $payment_data_detail->present_value = $payment_data['present_value'];
                    $payment_data_detail->index_id = $payment_data['index_id'];
                    $payment_data_detail->valuation_id = $model->id;
                    $payment_data_detail->save();
                }
            }

            //step 9
            //done in first step


            //step 10 - 13 - 16 (enquiry)
            $step_10_13_16_data = ValuationEnquiry::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_10_13_16_data)) {
                foreach ($step_10_13_16_data as $key => $enquiry_data) {
                    $step_10_13_16 = new ValuationEnquiry();
                    $step_10_13_16->date_from = $enquiry_data->date_from;
                    $step_10_13_16->date_to = $enquiry_data->date_to;
                    $step_10_13_16->building_info = $enquiry_data->building_info;
                    $step_10_13_16->property_id = $enquiry_data->property_id;
                    $step_10_13_16->property_category = $enquiry_data->property_category;
                    $step_10_13_16->bedroom_from = $enquiry_data->bedroom_from;
                    $step_10_13_16->bedroom_to = $enquiry_data->bedroom_to;
                    $step_10_13_16->landsize_from = $enquiry_data->landsize_from;
                    $step_10_13_16->landsize_to = $enquiry_data->landsize_to;
                    $step_10_13_16->bua_from = $enquiry_data->bua_from;
                    $step_10_13_16->bua_to = $enquiry_data->bua_to;
                    $step_10_13_16->price_from = $enquiry_data->price_from;
                    $step_10_13_16->price_to = $enquiry_data->price_to;
                    $step_10_13_16->price_psf_from = $enquiry_data->price_psf_from;
                    $step_10_13_16->price_psf_to = $enquiry_data->price_psf_to;
                    $step_10_13_16->type = $enquiry_data->type;
                    $step_10_13_16->valuation_id =  $model->id;
                    $step_10_13_16->save();
                }
            }


            //step 12 - 14 - 17 (List Selection)
            $step_12_14_17_list = ValuationSelectedLists::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_12_14_17_list)) {
                foreach ($step_12_14_17_list as $selected_row) {
                    $selected_data_detail = new ValuationSelectedLists();
                    $selected_data_detail->selected_id = $selected_row->selected_id;
                    $selected_data_detail->type =  $selected_row->type;
                    $selected_data_detail->valuation_id = $model->id;
                    $selected_data_detail->save();
                }
            }

            $step_12_14_17_list_calculations = ValuationListCalculation::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_12_14_17_list_calculations)) {
                foreach ($step_12_14_17_list_calculations as $list_row) {
                    $calculations = new ValuationListCalculation();
                    $calculations->avg_bedrooms = $list_row->avg_bedrooms;
                    $calculations->avg_land_size = $list_row->avg_land_size;
                    $calculations->built_up_area_size = $list_row->built_up_area_size;
                    $calculations->avg_listings_price_size = $list_row->avg_listings_price_size;
                    $calculations->min_price = $list_row->min_price;
                    $calculations->max_price = $list_row->max_price;
                    $calculations->min_price_sqt = $list_row->min_price_sqt;
                    $calculations->max_price_sqt = $list_row->max_price_sqt;
                    $calculations->avg_listing_date = $list_row->avg_listing_date;
                    $calculations->avg_psf = $list_row->avg_psf;
                    $calculations->avg_gross_yield = $list_row->avg_gross_yield;
                    $calculations->type = $list_row->type;
                    $calculations->valuation_id = $model->id;
                    $calculations->save();
                }
            }





            //step 13 - 15 - 18 (Derive MV)

            $step_13_15_18_derive_mv = ValuationDriveMv::find()->where(['valuation_id' => $id])->all();
            if(!empty($step_13_15_18_derive_mv)) {
                foreach ($step_13_15_18_derive_mv as $mv_row) {
                    $drive_margin_saved_data = new ValuationDriveMv();
                    $drive_margin_saved_data->average_location = $mv_row->average_location;
                    $drive_margin_saved_data->average_age = $mv_row->average_age;
                    $drive_margin_saved_data->average_tenure = $mv_row->average_tenure;
                    $drive_margin_saved_data->average_view = $mv_row->average_view;
                    $drive_margin_saved_data->average_finished_status = $mv_row->average_finished_status;
                    $drive_margin_saved_data->average_property_condition = $mv_row->average_property_condition;
                    $drive_margin_saved_data->average_upgrades = $mv_row->average_upgrades;
                    $drive_margin_saved_data->average_property_exposure =$mv_row->average_property_exposure;
                    $drive_margin_saved_data->average_property_placement = $mv_row->average_property_placement;
                    $drive_margin_saved_data->average_full_building_floors = $mv_row->average_full_building_floors;
                    $drive_margin_saved_data->average_number_of_levels = $mv_row->average_number_of_levels;
                    $drive_margin_saved_data->average_no_of_bedrooms = $mv_row->average_no_of_bedrooms;
                    $drive_margin_saved_data->average_parking_space = $mv_row->average_parking_space;
                    $drive_margin_saved_data->average_pool = $mv_row->average_pool;
                    $drive_margin_saved_data->average_landscaping = $mv_row->average_landscaping;
                    $drive_margin_saved_data->average_white_goods = $mv_row->average_white_goods;
                    $drive_margin_saved_data->average_utilities_connected = $mv_row->average_utilities_connected;
                    $drive_margin_saved_data->average_developer_margin =$mv_row->average_developer_margin;
                    $drive_margin_saved_data->average_land_size = $mv_row->average_land_size;
                    $drive_margin_saved_data->average_balcony_size =$mv_row->average_balcony_size;
                    $drive_margin_saved_data->average_built_up_area = $mv_row->average_built_up_area;
                    $drive_margin_saved_data->average_date = $mv_row->average_date;
                    $drive_margin_saved_data->average_sale_price = $mv_row->average_sale_price;
                    $drive_margin_saved_data->average_psf = $mv_row->average_psf;
                    $drive_margin_saved_data->weightage_location = $mv_row->weightage_location;
                    $drive_margin_saved_data->weightage_age = $mv_row->weightage_age;
                    $drive_margin_saved_data->weightage_tenure = $mv_row->weightage_tenure;
                    $drive_margin_saved_data->weightage_view = $mv_row->weightage_view;
                    $drive_margin_saved_data->weightage_finished_status = $mv_row->weightage_finished_status;
                    $drive_margin_saved_data->weightage_property_condition = $mv_row->weightage_property_condition;
                    $drive_margin_saved_data->weightage_furnished = $mv_row->weightage_furnished;
                    $drive_margin_saved_data->weightage_property_exposure = $mv_row->weightage_property_exposure;
                    $drive_margin_saved_data->weightage_property_placement = $mv_row->weightage_property_placement;
                    $drive_margin_saved_data->weightage_full_building_floors = $mv_row->weightage_full_building_floors;
                    $drive_margin_saved_data->weightage_number_of_levels = $mv_row->weightage_number_of_levels;
                    $drive_margin_saved_data->weightage_no_of_bedrooms = $mv_row->weightage_no_of_bedrooms;
                    $drive_margin_saved_data->weightage_parking_space = $mv_row->weightage_parking_space;
                    $drive_margin_saved_data->weightage_pool = $mv_row->weightage_pool;
                    $drive_margin_saved_data->weightage_landscaping = $mv_row->weightage_landscaping;
                    $drive_margin_saved_data->weightage_white_goods = $mv_row->weightage_white_goods;
                    $drive_margin_saved_data->weightage_utilities_connected = $mv_row->weightage_utilities_connected;
                    $drive_margin_saved_data->weightage_developer_margin = $mv_row->weightage_developer_margin;
                    $drive_margin_saved_data->weightage_land_size = $mv_row->weightage_land_size;
                    $drive_margin_saved_data->weightage_balcony_size = $mv_row->weightage_balcony_size;
                    $drive_margin_saved_data->weightage_built_up_area =  $mv_row->weightage_built_up_area;
                    $drive_margin_saved_data->weightage_date = $mv_row->weightage_date;
                    $drive_margin_saved_data->valuation_id = $model->id;
                    $drive_margin_saved_data->type = $mv_row->type;
                    $drive_margin_saved_data->save();
                }
            }


            //step 21, 22,23
            $step_23_data = ValuationApproversData::find()->where(['valuation_id' => $id])->all();

            //   unset($step_23_data->id);
            //  unset($step_23_data->status);

            if(!empty($step_23_data)) {
                foreach ($step_23_data as $step_23_row) {
                    $calculations = new ValuationApproversData();
                    $estimated_market_value = str_replace(",", "", $step_23_row->estimated_market_value);

                    $calculations->estimated_market_value = floatval($estimated_market_value);
                    $calculations->estimated_market_value_sqf = $step_23_row->estimated_market_value_sqf;
                    $calculations->estimated_market_rent = $step_23_row->estimated_market_rent;
                    $calculations->estimated_market_rent_sqf = $step_23_row->estimated_market_rent_sqf;
                    $calculations->parking_market_value = $step_23_row->parking_market_value;
                    $calculations->parking_market_value_sqf = $step_23_row->parking_market_value_sqf;
                    $calculations->approver_type = $step_23_row->approver_type;
                    $calculations->reason = $step_23_row->reason;
                    $calculations->status = $step_23_row->status;
                    $calculations->valuation_id = $model->id;
                    if(!$calculations->save()){

                    }
                }
            }

            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->redirect(['valuation/step_1/' . $model->id]);
        } else {
            /* echo "<pre>";
             print_r($model->errors);
             die;*/
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        die('Process is in progress.');



    }

    public function actionPdf($id)
    {

        if(!Yii::$app->menuHelperFunction->checkActionAllowed('pdf')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

  //      ini_set('max_execution_time', '150'); //300 seconds = 5 minutes
ini_set('max_execution_time', '0'); // for infinite time of execution
        if(!Yii::$app->menuHelperFunction->checkActionAllowed('pdf')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $model=$this->findModel($id);

        // 1.72 Market Value
        $approver_data = ValuationApproversData::find()->where(['valuation_id' => $id,'approver_type' => 'approver'])->one();

        $estimate_price_byapprover= ($approver_data <> null)? (number_format($approver_data->estimated_market_value) .' ('.$this->convertNumberToWord($approver_data->estimated_market_value). ' Dirhams Only)') : '';

        //define ('K_PATH_IMAGES', dirname(__FILE__).'/../web/images/');

        // Include the main TCPDF library (search for installation path).
        require_once( __DIR__ .'/../components/tcpdf/MyPDF.php');

        // create new PDF document
        $pdf = new \MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->model = $model;

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($model->reference_number);
        $pdf->SetSubject('Valuation Report');
        $pdf->SetKeywords($model->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(15, 0, 8);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('arialn', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');


        $pdf->Image('images/frontpage.png', 0, 0, 470, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        $pdf->writeHTML( $html, true, false, false, false, '');
        $pdf->SetMargins(myPDF_MARGIN_LEFT, myPDF_MARGIN_TOP, myPDF_MARGIN_RIGHT,0);


        $incomeapproch_data = $this->actionIncomeEstimateValue($model->id);
        $costapproch_data = $this->actionCostEstimateValue($model->id);
        //1.73 Market Value Rate , 1.74 Market Rent
        $summary = Yii::$app->PdfHelper->getMarketRent($incomeapproch_data,$model,$costapproch_data);





        //Table of Content
        $toc=$this->renderPartial('pdf/toc',[ 'model'=>$model ]);




        // 1. Property and Valuation Overview
        $pavo=$this->renderPartial('pdf/pavo',[
            'model'=>$model,
            'estimate_price_byapprover'=>$estimate_price_byapprover,
            'summary'=>$summary,
        ]);
        $pdfdetail= $this->renderPartial('pdf/pdfdetail',[ 'model'=>$model ]);
        $transactionList = $this->renderPartial('pdf/transaction',[  'model'=>$model ]);
        $inspectionSheet=  $this->renderPartial('pdf/inspectionSheet_Email',[ 'model'=>$model ]);

        $pdffirstsection=$this->renderPartial('pdf/pdffirstsection',[ 'model'=>$model ]);

        $html1 = ''.$pdffirstsection.Yii::$app->PdfHelper->tabeleCss.$toc.$pavo.$pdfdetail;

        $html2= Yii::$app->PdfHelper->tabeleCss.$inspectionSheet.$transactionList;

        // $html1 = $transactionList.$inspectionSheet.'';

        $pdf->writeHTML( $html1, true, false, false, false, '');

        //
        $html3='';

        $pdf->SetMargins(5, 20, 8,25);
        $pdf->writeHTML( $html2, true, false, false, false, '');
        $pdf->AddPage('P','A4');
        $pdf->SetMargins(0, 0, 0,0);
        $pdf->writeHTML( $html3, true, false, false, false, '');

        if(isset($model->building->city) && ($model->building->city == 3507)){
            $pdf->Image('images/lastpage_ajman.png', 0, 0, 500, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        }else {
            $pdf->Image('images/lastpage.png', 0, 0, 500, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        }
        // ---------------------------------------------------------
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output($model->reference_number.'.pdf', 'I');
    }



    /**
     * Finds the Valuation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Valuation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Valuation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionSignaturedelete($id)
    {

        $model = $this->findModel($id);

        return 'working';

// if ($id !=null) {

        // delete single image when user select delete
        Yii::$app->get('s3bucket')->delete('signature/images/'.$model->signature_img);
        $model->signature_img = 'unnamed.png';
        $model->save();
        // }


    }
    public function actionAttachmentdelete($id)
    {

        $model = ReceivedDocsFiles::findOne($id);
// if ($id !=null) {
        // delete single image when user select delete
        // Yii::$app->get('s3bucket')->delete($model->attachment);
        $model->trashed = 1;
        $model->save();
        // }
    }


    public function actionConfigattachmentdelete($id)
    {

        $model = ConfigurationFiles::findOne($id);
// if ($id !=null) {
        // delete single image when user select delete
        // Yii::$app->get('s3bucket')->delete($model->attachment);

        $model->img_avail = $model->attachment;
        $model->attachment='';
        $model->save();
        // }
        return true;
    }

    public function actionSendScheduleemail($id){
        $model = ScheduleInspection::find()->where(['id'=>$id])->one();
        $valuation = Valuation::find()->where(['id'=>$model->valuation_id])->one();
        // echo "<pre>";
        // print_r($model->contact_person_name);
        // echo "<pre>";
        // die();


        $InspectionTypeLabel='';
        $InspectionTypeLabelDate='';
        $InspectionTimeLabel='';
        $InspectionTime='';
        $InspectionOfficerLabel='';
        $InspectionOfficer='';

        $contactPersonNameLabel='';
        $contactPersonName='';
        $contactEmailLabel='';
        $contactEmail='';
        $contactPhoneNoLabel='';
        $contactPhoneNo='';

        if ($model->inspection_type ==1 || $model->inspection_type ==2) {
            $InspectionTypeLabel=Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type];
            $InspectionTypeLabelDate= $model->inspection_date;
            $InspectionTimeLabel=$InspectionTypeLabel.' Time:  ';
            $InspectionTypeLabel.=':';
            $InspectionTime=$model->inspection_time;
            $InspectionOfficerLabel='Inspection Officer:';
            $InspectionOfficer= Yii::$app->appHelperFunctions->staffMemberListArr[$model->inspection_officer];
        }
        if ($model->inspection_type ==2) {
            $contactPersonNameLabel='Contact Person Name:';
            $contactPersonName=$model->contact_person_name;
            $contactEmailLabel='Contact Email:';
            $contactEmail=$model->contact_email;
            $contactPhoneNoLabel='Contact PhoneNo:';
            $contactPhoneNo=$model->contact_phone_no;
        }



        $notifyData = [
            'client' => $valuation->client,
            'attachments' => [],
            'uid' => $valuation->id,
            'replacements'=>[
                '{clientName}'=>          $valuation->client->title,
                '{inspectionType}'=>      Yii::$app->appHelperFunctions->inspectionTypeArr[$model->inspection_type],
                '{valuationData}'=>       $model->valuation_date,
                '{valuationReportDate}'=> $model->valuation_report_date,
                '{label}'=>               $InspectionTypeLabel,
                '{labelDate}'=>           $InspectionTypeLabelDate,
                '{inspectionTimeLabel}'=> $InspectionTimeLabel,
                '{inspectionTime}'=>      $InspectionTime,
                '{inspectionOfficerLabel}'=> $InspectionOfficerLabel,
                '{inspectionOfficer}'=>   $InspectionOfficer,
                '{contactPersonNameLable}'=>   $contactPersonNameLabel,
                '{contactPersonName}'=>   $contactPersonName,
                '{contactEmailLabel}'=>        $contactEmailLabel,
                '{contactEmail}'=>        $contactEmail,
                '{contactPhoneNoLabel}'=>      $contactPhoneNoLabel,
                '{contactPhoneNo}'=>      $contactPhoneNo,
            ],
        ];

        //\app\modules\wisnotify\listners\NotifyEvent::fire('schedule.send', $notifyData);

       // Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 2], 'id='.$model->valuation_id.'')->execute();
        Yii::$app->db->createCommand()->update('schedule_inspection', ['email_status' => 1], 'id='.$id .'')->execute();

        //$model->email_status = 1;
        if ($model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->redirect(['valuation/step_4/'. $valuation->id]);
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

    }



    public function actionSendEmail_old($id,$step){

        // echo $id;
        // die();
        $model = ValuationApproversData::find()->where(['id'=>$id ])->one();
        $valuation = Valuation::find()->where(['id'=>$model->valuation_id])->one();

        if ($model->status=='Approve') {
            $Status='Recommended For Approval';
        }else {
            $Status='Reject';
            $reason=$model->reason;
            $reasonlabel='Reason for Rejection: ';
        }
        $notifyData = [
            'client' => $valuation->client,
            'attachments' => [],
            'replacements'=>[
                '{clientName}'=>   $valuation->client->title,
                '{estimatedMarkerValue}'=> $model->estimated_market_value,
                '{estimatedMarkerValueSqf}'=>   $model->estimated_market_value_sqf,
                '{estimatedMarkertRent}'=>   $model->estimated_market_rent,
                '{estimatedMarkertRentSqf}'=>   $model->estimated_market_rent_sqf,
                '{parkingMarketValue}'=>   $model->parking_market_value,
                '{parkingMarketValueSqf}'=>   $model->parking_market_value_sqf,
                '{status}'=>   $Status,
                '{reason}'=>   $reason,
                '{reasonlabel}'=> $reasonlabel,
                '{value}'=>   (isset($model->created_by) && ($model->created_by <> null))? ($model->user->firstname): Yii::$app->user->identity->firstname,
                '{date}'=>   (date('Y-m-d',strtotime($model->created_at))),
            ],
        ];

        $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval'];
        // echo $valuation_steps[$step];
        // die();
        //\app\modules\wisnotify\listners\NotifyEvent::fire('Valuation.'.$valuation_steps[$step].'.send', $notifyData);
        // $model->email_status = 1;

        if ($step==21) {
            Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 8], 'id='.$model->valuation_id.'')->execute();
        }
        elseif ($step==22) {
            Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 4], 'id='.$model->valuation_id.'')->execute();
        }
        elseif ($step==23) {
            Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 5], 'id='.$model->valuation_id.'')->execute();
        }

        // UPDATE (table name, column values, condition)
        Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$id .'')->execute();
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
        return $this->redirect(['valuation/step_'.$step.'/' . $valuation->id]);

    }

    public function actionSendEmail($id,$step){

        // echo $id;
        // die();
        $model = ValuationApproversData::find()->where(['id'=>$id ])->one();
        $valuation = Valuation::find()->where(['id'=>$model->valuation_id])->one();

        if ($model->status=='Approve') {
            $Status='Recommended For Approval';
        }else {
            $Status='Reject';
            $reason=$model->reason;
            $reasonlabel='Reason for Rejection: ';
        }
        $notifyData = [
            'client' => $valuation->client,
            'attachments' => [],
            'uid' => $valuation->id,
            'subject' => $model->email_subject,
            'replacements'=>[
                '{clientName}'=>   $valuation->client->title,
                '{estimatedMarkerValue}'=> $model->estimated_market_value,
                '{estimatedMarkerValueSqf}'=>   $model->estimated_market_value_sqf,
                '{estimatedMarkertRent}'=>   $model->estimated_market_rent,
                '{estimatedMarkertRentSqf}'=>   $model->estimated_market_rent_sqf,
                '{parkingMarketValue}'=>   $model->parking_market_value,
                '{parkingMarketValueSqf}'=>   $model->parking_market_value_sqf,
                '{status}'=>   $Status,
                '{reason}'=>   $reason,
                '{reasonlabel}'=> $reasonlabel,
                '{value}'=>   (isset($model->created_by) && ($model->created_by <> null))? ($model->user->firstname): Yii::$app->user->identity->firstname,
                '{date}'=>   (date('Y-m-d',strtotime($model->created_at))),
            ],
        ];

        $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval'];
        // echo $valuation_steps[$step];
        // die();
       // \app\modules\wisnotify\listners\NotifyEvent::fire('Valuation.'.$valuation_steps[$step].'.send', $notifyData);
        // $model->email_status = 1;

        if ($step==21) {
            Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 8], 'id='.$model->valuation_id.'')->execute();
        }
        elseif ($step==22) {
            if ($model->status=='Approve') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 4], 'id='.$model->valuation_id.'')->execute();
            }elseif ($model->status=='Reject') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 6], 'id='.$model->valuation_id.'')->execute();
            }
        }
        elseif ($step==23) {
            if ($model->status=='Approve') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 5], 'id='.$model->valuation_id.'')->execute();
            }elseif ($model->status=='Reject') {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 9], 'id='.$model->valuation_id.'')->execute();
            }
        }

        // UPDATE (table name, column values, condition)
        Yii::$app->db->createCommand()->update('valuation_approvers_data', ['email_status' => 1], 'id='.$id .'')->execute();


        if ($model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->redirect(['valuation/step_'.$step.'/' . $valuation->id]);
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }
    }

    public function actionReceiveValuationEmail($id,$step){
        //  $model = ValuationApproversData::find()->where(['id'=>$id ])->one();
        $model = Valuation::find()->where(['id'=>$id])->one();
        $notifyData = [
            'client' => $model->client,
            'attachments' => [],
            'replacements'=>[
                '{clientName}'=>   $model->client->title,
            ],
        ];
        // $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval',1=>'receive'];
        //\app\modules\wisnotify\listners\NotifyEvent::fire('Received.Valuation', $notifyData);
        // UPDATE (table name, column values, condition)
      //  Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 1], 'id='.$model->id .'')->execute();
        Yii::$app->db->createCommand()->update('valuation', ['email_status' => 1], 'id='.$id .'')->execute();

        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
        return $this->redirect(['valuation/step_'.$step.'/' . $model->id]);

    }
    public function actionReceiveDocEmail($id,$step){
        $model = ReceivedDocs::find()->where(['id'=>$id ])->one();
        $valuation = Valuation::find()->where(['id'=>$model->valuation_id])->one();
        $notifyData = [
            'client' => $valuation->client,
            'attachments' => [],
            'replacements'=>[
                '{clientName}'=>   $valuation->client->title,
            ],
        ];
        // $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval',1=>'receive'];
        //\app\modules\wisnotify\listners\NotifyEvent::fire('Received.Doc', $notifyData);
        // UPDATE (table name, column values, condition)
       // Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 7], 'id='.$model->valuation_id.'')->execute();
        Yii::$app->db->createCommand()->update('received_docs', ['email_status' => 1], 'id='.$model->id .'')->execute();

        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
        return $this->redirect(['valuation/step_'.$step.'/' . $valuation->id]);

    }
    public function actionInspectpropert_old($id,$step){
        $model = InspectProperty::find()->where(['id'=>$id ])->one();
        $valuation = Valuation::find()->where(['id'=>$model->valuation_id])->one();
        $notifyData = [
            'client' => $valuation->client,
            'attachments' => [],
            'subject' => $valuation->email_subject,
            'replacements'=>[
                '{clientName}'=>   $valuation->client->title,
            ],
        ];
        // $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval',1=>'receive'];
       // \app\modules\wisnotify\listners\NotifyEvent::fire('Inspect.Property', $notifyData);
        // UPDATE (table name, column values, condition)
        Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 3], 'id='.$model->valuation_id.'')->execute();
        Yii::$app->db->createCommand()->update('inspect_property', ['email_status' => 1], 'id='.$model->id .'')->execute();

        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
        return $this->redirect(['valuation/step_'.$step.'/' . $valuation->id]);
    }


    public function actionInspectpropert($id){

        $model = InspectProperty::find()->where(['id'=>$id ])->one();
        $valuation = Valuation::find()->where(['id'=>$model->valuation_id])->one();
        if ($model->email_status !== 1) {
            $notifyData = [
                'client' => $valuation->client,
                'attachments' => [],
                'uid' => $valuation->id,
                'subject'=>$valuation->email_subject,
                'replacements'=>[
                    '{clientName}'=>   $valuation->client->title,
                ],
            ];
            if($valuation->scheduleInspection->inspection_type != 3) {
                $allow_properties = [1, 2, 5, 6, 12, 37];
                if (in_array($valuation->property_id, $allow_properties)) {
                   // \app\modules\wisnotify\listners\NotifyEvent::fire('inspectProperty.send', $notifyData);
                }
            }
            // UPDATE (table name, column values, condition)
          //  Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 3], 'id='.$model->valuation_id.'')->execute();
            Yii::$app->db->createCommand()->update('inspect_property', ['email_status' => 1], 'id='.$model->id .'')->execute();
        }
        if ($model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->redirect(['valuation/step_5/' . $valuation->id]);
        }
        else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

    }

    public function actionPdfHtml($id){

        $model=$this->findModel($id);

        // 1.72 Market Value
        $approver_data = ValuationApproversData::find()->where(['valuation_id' => $id,'approver_type' => 'approver'])->one();

        $estimate_price_byapprover= ($approver_data <> null)? (number_format($approver_data->estimated_market_value) .' ('.$this->convertNumberToWord($approver_data->estimated_market_value). ' Dirhams Only)') : '';


        $incomeapproch_data = $this->actionIncomeEstimateValue($model->id);
        $costapproch_data = $this->actionCostEstimateValue($model->id);
        //1.73 Market Value Rate , 1.74 Market Rent
        $summary = Yii::$app->PdfHelper->getMarketRent($incomeapproch_data,$model,$costapproch_data);
        //Table of Content
        $toc=$this->renderPartial('pdf/toc',[ 'model'=>$model ]);
        // 1. Property and Valuation Overview
        $pavo=$this->renderPartial('pdf/pavo',[
            'model'=>$model,
            'estimate_price_byapprover'=>$estimate_price_byapprover,
            'summary'=>$summary,
        ]);
        $pdfdetail= $this->renderPartial('pdf/pdfdetail',[ 'model'=>$model ]);
        $transactionList = $this->renderPartial('pdf/transaction',[  'model'=>$model ]);
        $inspectionSheet=  $this->renderPartial('pdf/inspectionSheet_Email',[ 'model'=>$model ]);
        $pdffirstsection=$this->renderPartial('pdfhtml/pdffirstsection',[ 'model'=>$model ]);
        $html1 = ''.$pdffirstsection.Yii::$app->PdfHelper->tabeleCss.$toc.$pavo.$pdfdetail;
        $html2= Yii::$app->PdfHelper->tabeleCss.$inspectionSheet.$transactionList;
        $html3='<div class="col-8 mx-auto bg-success">'.$html1.$html2.' </div>';
        return $html1.$html2;

    }
    public function actionCancel_valuation($id){

        $valuation= Valuation::find()->where(['id'=>$id])->one();
        if ($valuation->valuation_status!=9) {
            $valuationApproversData=ValuationApproversData::find()->where(['valuation_id'
            =>$id,'approver_type'=>'reviewer'])->select(['created_by','valuation_id'])->one();

            if ($valuationApproversData->user !=null) {

                $notifyData = [
                    'reviewer' => $valuationApproversData->user,
                    // 'reviewer' => $valuation->userbycreated,
                    'attachments' => [],
                    'replacements'=>[
                        '{clientName}'=>   $valuationApproversData->user->firstname,
                    ],
                ];
              //  \app\modules\wisnotify\listners\NotifyEvent::fire('Cancel.Valuation', $notifyData);
                Yii::$app->db->createCommand()->update('valuation', ['cancel_by' => Yii::$app->user->identity->id], 'id='.$id.'')->execute();
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Cancelled Email has sent Reviewer.'));
                return $this->redirect(['valuation/index/']);

            }
            else{
                Yii::$app->getSession()->addFlash('error', 'Reviewer Information is not Available3.');
                return $this->redirect(['valuation/index/']);
            }
        }
        else{

            Yii::$app->getSession()->addFlash('error', 'Reviewer Information is not Availableee.');
            return $this->redirect(['valuation/index/']);
        }

    }




    public function actionUpdate_to_cancel($id){
        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Valuation is Canceled'));
        //   UPDATE (table name, column values, condition)
        Yii::$app->db->createCommand()->update('valuation', ['cancelled_approved_by' => Yii::$app->user->identity->id], 'id='.$id.'')->execute();
        Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 9], 'id='.$id .'')->execute();
        return $this->redirect(['valuation/index/']);
    }

    public function actionSendpdfreport($id){

        //      ini_set('max_execution_time', '150'); //300 seconds = 5 minutes
        ini_set('max_execution_time', '0'); // for infinite time of execution

        $model=Valuation::findOne($id);

        // 1.72 Market Value
        $approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $id,'approver_type' => 'approver'])->one();

        $estimate_price_byapprover= ($approver_data <> null)? (number_format($approver_data->estimated_market_value) .' ('.$this->convertNumberToWord($approver_data->estimated_market_value). ' Dirhams Only)') : '';

        //define ('K_PATH_IMAGES', dirname(__FILE__).'/../web/images/');

        // Include the main TCPDF library (search for installation path).
        require_once( __DIR__ .'/../components/tcpdf/MyPDF.php');

        // create new PDF document
        $pdf = new \MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->model = $model;

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($model->reference_number);
        $pdf->SetSubject('Valuation Report');
        $pdf->SetKeywords($model->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(15, 0, 8);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('arialn', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');


        $pdf->Image('images/frontpage.png', 0, 0, 470, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        $pdf->writeHTML( $html, true, false, false, false, '');
        $pdf->SetMargins(myPDF_MARGIN_LEFT, myPDF_MARGIN_TOP, myPDF_MARGIN_RIGHT,0);


        $incomeapproch_data = $this->actionIncomeEstimateValue($model->id);
        $costapproch_data = $this->actionCostEstimateValue($model->id);
        //1.73 Market Value Rate , 1.74 Market Rent
        $summary = Yii::$app->PdfHelper->getMarketRent($incomeapproch_data,$model,$costapproch_data);





        //Table of Content
        $toc=$this->renderPartial('pdf/toc',[ 'model'=>$model ]);


        // 1. Property and Valuation Overview
        $pavo=$this->renderPartial('pdf/pavo',[
            'model'=>$model,
            'estimate_price_byapprover'=>$estimate_price_byapprover,
            'summary'=>$summary,
        ]);
        $pdfdetail= $this->renderPartial('pdf/pdfdetail',[ 'model'=>$model ]);
        $transactionList = $this->renderPartial('pdf/transaction',[  'model'=>$model ]);
        $inspectionSheet=  $this->renderPartial('pdf/inspectionSheet_Email',[ 'model'=>$model ]);

        $pdffirstsection=$this->renderPartial('pdf/pdffirstsection',[ 'model'=>$model ]);

        $html1 = ''.$pdffirstsection.Yii::$app->PdfHelper->tabeleCss.$toc.$pavo.$pdfdetail;

        $html2= Yii::$app->PdfHelper->tabeleCss.$inspectionSheet.$transactionList;
        // $html1 = $transactionList.$inspectionSheet.'';
        $pdfFile = $model->reference_number.'.pdf';
        $fullPath = realpath(dirname(__FILE__).'/../uploads/client_pdf').'/'.$pdfFile;
       // $file_path = 'https://max.windmillsgroup.com/uploads/client_pdf/'.$pdfFile;

        $pdf->writeHTML( $html1, true, false, false, false, '');

        //
        $html3='';

        $pdf->SetMargins(5, 20, 8,25);
        $pdf->writeHTML( $html2, true, false, false, false, '');
        $pdf->AddPage('P','A4');
        $pdf->SetMargins(0, 0, 0,0);
        $pdf->writeHTML( $html3, true, false, false, false, '');
        $pdf->Image('images/lastpage.png', 0, 0, 500, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        // ---------------------------------------------------------
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        //$pdf->Output($model->reference_number.'.pdf', 'I');

        //$pdf->writeHTML($html1.$html2.$html3, true, false, false, false, '');
        $pdf->Output($fullPath, 'F');
        return $fullPath;


    }

    public function actionReportAll()
    {
        $user=null;
        $searchModel = new ValuationSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->client_id=Yii::$app->user->identity->company_id;
            $user=Yii::$app->user->identity;
        }

        $dataProvider = $searchModel->search_all(Yii::$app->request->queryParams);
        return $this->render('index_advance_search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
        ]);
    }


}
