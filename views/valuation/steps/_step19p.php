<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Derive Profit Approach');
$cardTitle = Yii::t('app', 'Derive Profit Approach:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_19p/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<style>

    .left-panel {
        -ms-flex: 0 0 3%;
        flex: 0 0 3%;
        max-width: 3%;
        max-height: 100vh;
        transition: max-width 0.3s ease;
    }
    .left-panel #vert-tabs-tab {
        -ms-flex: 0 0 0%;
        flex: 0 0 0%;
        max-width: 0%;
        overflow: hidden;
        transition: max-width 0.3s ease;
    }
    .right-panel {
        -ms-flex: 0 0 97%;
        flex: 0 0 97%;
        max-width: 97%;
        transition: max-width 0.3s ease;
    }
    .hide-left-panel.left-panel {
        -ms-flex: 0 0 0%;
        flex: 0 0 0%;
        max-width: 0%;
        max-height: 100%;
    }
    .right-panel.full-right-panel {
        -ms-flex: 0 0 80%;
        flex: 0 0 80%;
        max-width: 80%;
    }
    .hide-left-panel #vert-tabs-tab {
        -ms-flex: 0 0 100%;
        flex: 0 0 100%;
        max-width: 100%;
    }
    .toggleButton { padding:2px 10px; color: #007bff; cursor: pointer;}
    input.form-control { padding: 0.375rem 0.10rem 0.375rem 0.55rem;}
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-4 col-sm-2 left-panel">
     <div class="toggleButton" id="togglePanel" title="Left Panel" ><i class="fas fa-bars"></i></div>

                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 19]); ?>


            </div>
            <div class="col-11 col-sm-11 right-panel">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">

                        <section class="valuation-form card card-outline card-primary">
                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title">Cost Approach - Final</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <table class="table table-bordered proposal-table">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th colspan="2">Year 1</th>
                                            <th colspan="2">Year 2</th>
                                            <th colspan="2">Year 3</th>
                                            <th colspan="2">Year 4</th>
                                            <th colspan="2">Year 5</th>
                                            <th colspan="2">Year 6</th>
                                            <th colspan="2">Year 7</th>
                                            <th colspan="2">Year 8</th>
                                            <th colspan="2">Year 9</th>
                                            <th colspan="2">Year 10</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $number_of_room = \app\models\ProfitMethodYearly::find()->select('number_of_rooms')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td colspan="21"><h4>Base Hotel Business Data</h4></td>

                                            </tr>
                                            <tr>
                                                <td><strong>Number of Rooms</strong></td>
                                                <td><input type="number"
                                                    name="ProfitMethodBase[calculations][1][number_of_rooms]"
                                                    value="<?= $number_of_room[0] ?>" placeholder=""
                                                    class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][number_of_rooms]"
                                                           value="<?= $number_of_room[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][number_of_rooms]"
                                                           value="<?= $number_of_room[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][number_of_rooms]"
                                                           value="<?= $number_of_room[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][number_of_rooms]"
                                                           value="<?= $number_of_room[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][number_of_rooms]"
                                                           value="<?= $number_of_room[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][number_of_rooms]"
                                                           value="<?= $number_of_room[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][number_of_rooms]"
                                                           value="<?= $number_of_room[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][number_of_rooms]"
                                                           value="<?= $number_of_room[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][number_of_rooms]"
                                                           value="<?= $number_of_room[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $days_per_year = \app\models\ProfitMethodYearly::find()->select('days_per_year')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Days per year</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][days_per_year]"
                                                           value="<?= $days_per_year[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][days_per_year]"
                                                           value="<?= $days_per_year[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][days_per_year]"
                                                           value="<?= $days_per_year[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][days_per_year]"
                                                           value="<?= $days_per_year[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][days_per_year]"
                                                           value="<?= $days_per_year[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][days_per_year]"
                                                           value="<?= $days_per_year[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][days_per_year]"
                                                           value="<?= $days_per_year[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][days_per_year]"
                                                           value="<?= $days_per_year[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][days_per_year]"
                                                           value="<?= $days_per_year[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][days_per_year]"
                                                           value="<?= $days_per_year[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $nights_available = \app\models\ProfitMethodYearly::find()->select('nights_available')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Nights Available</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][nights_available]"
                                                           value="<?= $nights_available[0] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][nights_available]"
                                                           value="<?= $nights_available[1] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][nights_available]"
                                                           value="<?= $nights_available[2] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][nights_available]"
                                                           value="<?= $nights_available[3] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][nights_available]"
                                                           value="<?= $nights_available[4] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][nights_available]"
                                                           value="<?= $nights_available[5] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][nights_available]"
                                                           value="<?= $nights_available[6] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][nights_available]"
                                                           value="<?= $nights_available[7] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][nights_available]"
                                                           value="<?= $nights_available[8] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][nights_available]"
                                                           value="<?= $nights_available[9] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $occupancy = \app\models\ProfitMethodYearly::find()->select('occupancy')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Occupancy</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][occupancy]"
                                                           value="<?= $occupancy[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][occupancy]"
                                                           value="<?= $occupancy[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][occupancy]"
                                                           value="<?= $occupancy[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][occupancy]"
                                                           value="<?= $occupancy[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][occupancy]"
                                                           value="<?= $occupancy[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][occupancy]"
                                                           value="<?= $occupancy[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][occupancy]"
                                                           value="<?= $occupancy[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][occupancy]"
                                                           value="<?= $occupancy[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][occupancy]"
                                                           value="<?= $occupancy[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][occupancy]"
                                                           value="<?= $occupancy[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $occupied_nights_per_year = \app\models\ProfitMethodYearly::find()->select('occupied_nights_per_year')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Occupied Nights per year</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][occupied_nights_per_year]"
                                                           value="<?= $occupied_nights_per_year[0] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][occupied_nights_per_year]"
                                                           value="<?= $occupied_nights_per_year[1] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][occupied_nights_per_year]"
                                                           value="<?= $occupied_nights_per_year[2] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][occupied_nights_per_year]"
                                                           value="<?= $occupied_nights_per_year[3] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][occupied_nights_per_year]"
                                                           value="<?= $occupied_nights_per_year[4] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][occupied_nights_per_year]"
                                                           value="<?= $occupied_nights_per_year[5] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][occupied_nights_per_year]"
                                                           value="<?= $occupied_nights_per_year[6] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][occupied_nights_per_year]"
                                                           value="<?= $occupied_nights_per_year[7] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][occupied_nights_per_year]"
                                                           value="<?= $occupied_nights_per_year[8] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][occupied_nights_per_year]"
                                                           value="<?= $occupied_nights_per_year[9] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $adr = \app\models\ProfitMethodYearly::find()->select('adr')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>ADR</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][adr]"
                                                           value="<?= $adr[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][adr]"
                                                           value="<?= $adr[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][adr]"
                                                           value="<?= $adr[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][adr]"
                                                           value="<?= $adr[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][adr]"
                                                           value="<?= $adr[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][adr]"
                                                           value="<?= $adr[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][adr]"
                                                           value="<?= $adr[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][adr]"
                                                           value="<?= $adr[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][adr]"
                                                           value="<?= $adr[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][adr]"
                                                           value="<?= $adr[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $rev_par = \app\models\ProfitMethodYearly::find()->select('rev_par')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>RevPAR</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][rev_par]"
                                                           value="<?= $rev_par[0] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][rev_par]"
                                                           value="<?= $rev_par[1] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][rev_par]"
                                                           value="<?= $rev_par[2] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][rev_par]"
                                                           value="<?= $rev_par[3] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][rev_par]"
                                                           value="<?= $rev_par[4] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][rev_par]"
                                                           value="<?= $rev_par[5] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][rev_par]"
                                                           value="<?= $rev_par[6] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][rev_par]"
                                                           value="<?= $rev_par[7] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][rev_par]"
                                                           value="<?= $rev_par[8] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][rev_par]"
                                                           value="<?= $rev_par[9] ?>" placeholder=""
                                                           class="form-control" readonly></td>
                                                <td></td>
                                            </tr>


                                            <tr>
                                                <td colspan="21"><h4>Revenues</h4></td>

                                            </tr>
                                            <tr>
                                                <?php
                                                $rooms_revenue = \app\models\ProfitMethodYearly::find()->select('rooms_revenue')->where(['valuation_id' => $valuation->id])->column();
                                                $rooms_revenue_percentage = \app\models\ProfitMethodYearly::find()->select('rooms_revenue_percentage')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Rooms Revenue</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][rooms_revenue]"
                                                           value="<?= $rooms_revenue[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rooms_revenue_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][rooms_revenue]"
                                                           value="<?= $rooms_revenue[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rooms_revenue_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][rooms_revenue]"
                                                           value="<?= $rooms_revenue[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rooms_revenue_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][rooms_revenue]"
                                                           value="<?= $rooms_revenue[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rooms_revenue_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][rooms_revenue]"
                                                           value="<?= $rooms_revenue[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rooms_revenue_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][rooms_revenue]"
                                                           value="<?= $rooms_revenue[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rooms_revenue_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][rooms_revenue]"
                                                           value="<?= $rooms_revenue[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rooms_revenue_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][rooms_revenue]"
                                                           value="<?= $rooms_revenue[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rooms_revenue_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][rooms_revenue]"
                                                           value="<?= $rooms_revenue[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rooms_revenue_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][rooms_revenue]"
                                                           value="<?= $rooms_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rooms_revenue_percentage[9] ?>%</td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $f_and_b_revenue = \app\models\ProfitMethodYearly::find()->select('f_and_b_revenue')->where(['valuation_id' => $valuation->id])->column();
                                                $f_and_b_revenue_percentage = \app\models\ProfitMethodYearly::find()->select('f_and_b_revenue_percentage')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>F&B Revenu</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][f_and_b_revenue]"
                                                           value="<?= $f_and_b_revenue[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_revenue_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][f_and_b_revenue]"
                                                           value="<?= $f_and_b_revenue[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_revenue_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][f_and_b_revenue]"
                                                           value="<?= $f_and_b_revenue[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_revenue_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][f_and_b_revenue]"
                                                           value="<?= $f_and_b_revenue[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_revenue_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][f_and_b_revenue]"
                                                           value="<?= $f_and_b_revenue[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_revenue_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][f_and_b_revenue]"
                                                           value="<?= $f_and_b_revenue[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_revenue_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][f_and_b_revenue]"
                                                           value="<?= $f_and_b_revenue[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_revenue_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][f_and_b_revenue]"
                                                           value="<?= $f_and_b_revenue[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_revenue_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][f_and_b_revenue]"
                                                           value="<?= $f_and_b_revenue[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px">"<?= $f_and_b_revenue_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][f_and_b_revenue]"
                                                           value="<?= $f_and_b_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_revenue_percentage[9] ?>%</td>
                                            </tr>

                                            <tr>
                                                <?php
                                                $net_rental_income = \app\models\ProfitMethodYearly::find()->select('net_rental_income')->where(['valuation_id' => $valuation->id])->column();
                                                $net_rental_income_percentage = \app\models\ProfitMethodYearly::find()->select('net_rental_income_percentage')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Net Rental Income</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][net_rental_income]"
                                                           value="<?= $net_rental_income[0]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_percentage[0]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][net_rental_income]"
                                                           value="<?= $net_rental_income[1]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_percentage[1]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][net_rental_income]"
                                                           value="<?= $net_rental_income[2]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_percentage[2]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][net_rental_income]"
                                                           value="<?= $net_rental_income[3]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_percentage[3]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][net_rental_income]"
                                                           value="<?= $net_rental_income[4]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_percentage[4]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][net_rental_income]"
                                                           value="<?= $net_rental_income[5]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_percentage[5]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][net_rental_income]"
                                                           value="<?= $net_rental_income[6]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_percentage[6]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][net_rental_income]"
                                                           value="<?= $net_rental_income[7]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_percentage[7]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][net_rental_income]"
                                                           value="<?= $net_rental_income[8]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_percentage[8]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][net_rental_income]"
                                                           value="<?= $net_rental_income[9]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_percentage[9]; ?>%</td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $other_operating_departments = \app\models\ProfitMethodYearly::find()->select('other_operating_departments')->where(['valuation_id' => $valuation->id])->column();
                                                $other_operating_departments_percentage = \app\models\ProfitMethodYearly::find()->select('other_operating_departments_percentage')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Other Operating Departments</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][other_operating_departments]"
                                                           value="<?= $other_operating_departments[0]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_percentage[0]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][other_operating_departments]"
                                                           value="<?= $other_operating_departments[1]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_percentage[1]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][other_operating_departments]"
                                                           value="<?= $other_operating_departments[2]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_percentage[2]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][other_operating_departments]"
                                                           value="<?= $other_operating_departments[3]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_percentage[3]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][other_operating_departments]"
                                                           value="<?= $other_operating_departments[4]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_percentage[4]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][other_operating_departments]"
                                                           value="<?= $other_operating_departments[5]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_percentage[5]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][other_operating_departments]"
                                                           value="<?= $other_operating_departments[6]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_percentage[6]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][other_operating_departments]"
                                                           value="<?= $other_operating_departments[7]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_percentage[7]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][other_operating_departments]"
                                                           value="<?= $other_operating_departments[8]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_percentage[8]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][other_operating_departments]"
                                                           value="<?= $other_operating_departments[9]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_percentage[9]; ?>%</td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $total_revenue = \app\models\ProfitMethodYearly::find()->select('total_revenue')->where(['valuation_id' => $valuation->id])->column();
                                                $total_revenue_percentage = \app\models\ProfitMethodYearly::find()->select('total_revenue_percentage')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Total Revenue</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][total_revenue]"
                                                           value="<?= $total_revenue[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][total_revenue]"
                                                           value="<?= $total_revenue[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][total_revenue]"
                                                           value="<?= $total_revenue[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][total_revenue]"
                                                           value="<?= $total_revenue[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][total_revenue]"
                                                           value="<?= $total_revenue[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][total_revenue]"
                                                           value="<?= $total_revenue[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][total_revenue]"
                                                           value="<?= $total_revenue[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][total_revenue]"
                                                           value="<?= $total_revenue[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][total_revenue]"
                                                           value="<?= $total_revenue[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][total_revenue]"
                                                           value="<?= $total_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[9] ?>%</td>
                                            </tr>


                                            <tr>
                                                <td colspan="21"><h4>Expenses</h4></td>

                                            </tr>
                                            <tr>
                                                <td colspan="21"><h4>Department Costs & Revenues</h4></td>

                                            </tr>
                                            <tr>
                                                <?php
                                                $total_revenue = \app\models\ProfitMethodYearly::find()->select('rooms')->where(['valuation_id' => $valuation->id])->column();
                                                $total_revenue_percentage = \app\models\ProfitMethodYearly::find()->select('rooms_percentage')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Rooms</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][rooms]"
                                                           value="<?= $total_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][rooms]"
                                                           value="<?= $total_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][rooms]"
                                                           value="<?= $total_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][rooms]"
                                                           value="<?= $total_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][rooms]"
                                                           value="<?= $total_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][rooms]"
                                                           value="<?= $total_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][rooms]"
                                                           value="<?= $total_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][rooms]"
                                                           value="<?= $total_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][rooms]"
                                                           value="<?= $total_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][rooms]"
                                                           value="<?= $total_revenue[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_revenue_percentage[9] ?>%</td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $f_and_b = \app\models\ProfitMethodYearly::find()->select('f_and_b')->where(['valuation_id' => $valuation->id])->column();
                                                $f_and_b_percentage = \app\models\ProfitMethodYearly::find()->select('f_and_b_percentage')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>F&B</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][f_and_b]"
                                                           value="<?= $f_and_b[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][f_and_b]"
                                                           value="<?= $f_and_b[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][f_and_b]"
                                                           value="<?= $f_and_b[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][f_and_b]"
                                                           value="<?= $f_and_b[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][f_and_b]"
                                                           value="<?= $f_and_b[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][f_and_b]"
                                                           value="<?= $f_and_b[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][f_and_b]"
                                                           value="<?= $f_and_b[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][f_and_b]"
                                                           value="<?= $f_and_b[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][f_and_b]"
                                                           value="<?= $f_and_b[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][f_and_b]"
                                                           value="<?= $f_and_b[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $f_and_b_percentage[9] ?>%</td>
                                            </tr>
                                        <?php
                                        $net_rental_income_exp = \app\models\ProfitMethodYearly::find()->select('net_rental_income_exp')->where(['valuation_id' => $valuation->id])->column();
                                        $net_rental_income_exp_percentage = \app\models\ProfitMethodYearly::find()->select('net_rental_income_exp_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Net Rental Income</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][net_rental_income_exp]"
                                                           value="<?= $net_rental_income_exp[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_exp_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][net_rental_income_exp]"
                                                           value="<?= $net_rental_income_exp[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_exp_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][net_rental_income_exp]"
                                                           value="<?= $net_rental_income_exp[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_exp_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][net_rental_income_exp]"
                                                           value="<?= $net_rental_income_exp[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_exp_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][net_rental_income_exp]"
                                                           value="<?= $net_rental_income_exp[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_exp_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][net_rental_income_exp]"
                                                           value="<?= $net_rental_income_exp[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_exp_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][net_rental_income_exp]"
                                                           value="<?= $net_rental_income_exp[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_exp_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][net_rental_income_exp]"
                                                           value="<?= $net_rental_income_exp[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_exp_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][net_rental_income_exp]"
                                                           value="<?= $net_rental_income_exp[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_exp_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][net_rental_income_exp]"
                                                           value="<?= $net_rental_income_exp[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $net_rental_income_exp_percentage[9] ?>%</td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $other_operating_departments_exp = \app\models\ProfitMethodYearly::find()->select('other_operating_departments_exp')->where(['valuation_id' => $valuation->id])->column();
                                                $other_operating_departments_exp_percentage = \app\models\ProfitMethodYearly::find()->select('other_operating_departments_exp_percentage')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Other Operating Departments</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][other_operating_departments_exp]"
                                                           value="<?= $other_operating_departments_exp_percentage[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_exp_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][other_operating_departments_exp]"
                                                           value="<?= $other_operating_departments_exp_percentage[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_exp_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][other_operating_departments_exp]"
                                                           value="<?= $other_operating_departments_exp_percentage[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_exp_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][other_operating_departments_exp]"
                                                           value="<?= $other_operating_departments_exp_percentage[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_exp_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][other_operating_departments_exp]"
                                                           value="<?= $other_operating_departments_exp_percentage[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_exp_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][other_operating_departments_exp]"
                                                           value="<?= $other_operating_departments_exp_percentage[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_exp_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][other_operating_departments_exp]"
                                                           value="<?= $other_operating_departments_exp_percentage[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_exp_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][other_operating_departments_exp]"
                                                           value="<?= $other_operating_departments_exp_percentage[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_exp_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][other_operating_departments_exp]"
                                                           value="<?= $other_operating_departments_exp_percentage[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_exp_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][other_operating_departments_exp]"
                                                           value="<?= $other_operating_departments_exp_percentage[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_operating_departments_exp_percentage[9] ?>%</td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $total_costs_and_expenses = \app\models\ProfitMethodYearly::find()->select('total_costs_and_expenses')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Total Costs & Expenses</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][total_costs_and_expenses]"
                                                           value="<?= $total_costs_and_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][total_costs_and_expenses]"
                                                           value="<?= $total_costs_and_expenses[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][total_costs_and_expenses]"
                                                           value="<?= $total_costs_and_expenses[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][total_costs_and_expenses]"
                                                           value="<?= $total_costs_and_expenses[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px">0%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][total_costs_and_expenses]"
                                                           value="<?= $total_costs_and_expenses[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][total_costs_and_expenses]"
                                                           value="<?= $total_costs_and_expenses[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][total_costs_and_expenses]"
                                                           value="<?= $total_costs_and_expenses[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][total_costs_and_expenses]"
                                                           value="<?= $total_costs_and_expenses[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][total_costs_and_expenses]"
                                                           value="<?= $total_costs_and_expenses[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][total_costs_and_expenses]"
                                                           value="<?= $total_costs_and_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"></td>
                                            </tr>

                                            <tr>
                                                <?php
                                                $total_operating_dept_income = \app\models\ProfitMethodYearly::find()->select('total_operating_dept_income')->where(['valuation_id' => $valuation->id])->column();
                                                $total_operating_dept_income_percentage = \app\models\ProfitMethodYearly::find()->select('total_operating_dept_income_percentage')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Total Operating Dept Income</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][total_operating_dept_income]"
                                                           value="<?= $total_costs_and_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_operating_dept_income_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][total_operating_dept_income]"
                                                           value="<?= $total_costs_and_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_operating_dept_income_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][total_operating_dept_income]"
                                                           value="<?= $total_costs_and_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_operating_dept_income_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][total_operating_dept_income]"
                                                           value="<?= $total_costs_and_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_operating_dept_income_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][total_operating_dept_income]"
                                                           value="<?= $total_costs_and_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_operating_dept_income_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][total_operating_dept_income]"
                                                           value="<?= $total_costs_and_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_operating_dept_income_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][total_operating_dept_income]"
                                                           value="<?= $total_costs_and_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_operating_dept_income_percentage[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][total_operating_dept_income]"
                                                           value="<?= $total_costs_and_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_costs_and_expenses[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][total_operating_dept_income]"
                                                           value="<?= $total_costs_and_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_costs_and_expenses[9] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][total_operating_dept_income]"
                                                           value="<?= $total_costs_and_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_costs_and_expenses[9] ?>%</td>
                                            </tr>

                                        <?php
                                        $admin_and_general = \app\models\ProfitMethodYearly::find()->select('admin_and_general')->where(['valuation_id' => $valuation->id])->column();
                                        $admin_and_general_percentage = \app\models\ProfitMethodYearly::find()->select('admin_and_general_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>

                                            <tr>
                                                <td colspan="21"><h4>Undistributed Operating Expenses</h4></td>

                                            </tr>
                                            <tr>
                                                <td><strong>Admin & General</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][admin_and_general]"
                                                           value="<?= $admin_and_general[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $admin_and_general_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][admin_and_general]"
                                                           value="<?= $admin_and_general[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $admin_and_general_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][admin_and_general]"
                                                           value="<?= $admin_and_general[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $admin_and_general_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][admin_and_general]"
                                                           value="<?= $admin_and_general[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $admin_and_general_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][admin_and_general]"
                                                           value="<?= $admin_and_general[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $admin_and_general_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][admin_and_general]"
                                                           value="<?= $admin_and_general[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $admin_and_general_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][admin_and_general]"
                                                           value="<?= $admin_and_general[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $admin_and_general_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][admin_and_general]"
                                                           value="<?= $admin_and_general[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $admin_and_general_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][admin_and_general]"
                                                           value="<?= $admin_and_general[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $admin_and_general_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][admin_and_general]"
                                                           value="<?= $admin_and_genral[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $admin_and_general_percentage[9] ?>%</td>
                                            </tr>
                                        <?php
                                        $information_and_communication = \app\models\ProfitMethodYearly::find()->select('information_and_communication')->where(['valuation_id' => $valuation->id])->column();
                                        $information_and_communication_percentage = \app\models\ProfitMethodYearly::find()->select('information_and_communication_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Information and Communication</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][information_and_communication]"
                                                           value="<?= $information_and_communication[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $information_and_communication_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][information_and_communication]"
                                                           value="<?= $information_and_communication[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $information_and_communication_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][information_and_communication]"
                                                           value="<?= $information_and_communication[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $information_and_communication_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][information_and_communication]"
                                                           value="<?= $information_and_communication[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $information_and_communication_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][information_and_communication]"
                                                           value="<?= $information_and_communication[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $information_and_communication_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][information_and_communication]"
                                                           value="<?= $information_and_communication[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $information_and_communication_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][information_and_communication]"
                                                           value="<?= $information_and_communication[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $information_and_communication_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][information_and_communication]"
                                                           value="<?= $information_and_communication[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $information_and_communication_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][information_and_communication]"
                                                           value="<?= $information_and_communication[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $information_and_communication_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][information_and_communication]"
                                                           value="<?= $information_and_communication[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $information_and_communication_percentage[9] ?>%</td>
                                            </tr>
                                        <?php
                                        $marketing = \app\models\ProfitMethodYearly::find()->select('marketing')->where(['valuation_id' => $valuation->id])->column();
                                        $marketing_percentage = \app\models\ProfitMethodYearly::find()->select('marketing_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Marketing</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][marketing]"
                                                           value="<?= $marketing[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $marketing_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][marketing]"
                                                           value="<?= $marketing[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $marketing_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][marketing]"
                                                           value="<?= $marketing[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $marketing_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][marketing]"
                                                           value="<?= $marketing[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $marketing_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][marketing]"
                                                           value="<?= $marketing[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $marketing_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][marketing]"
                                                           value="<?= $marketing[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $marketing_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][marketing]"
                                                           value="<?= $marketing[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $marketing_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][marketing]"
                                                           value="<?= $marketing[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $marketing_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][marketing]"
                                                           value="<?= $marketing[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $marketing_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][marketing]"
                                                           value="<?= $marketing[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $marketing_percentage[9] ?>%</td>
                                            </tr>
                                        <?php
                                        $pom = \app\models\ProfitMethodYearly::find()->select('pom')->where(['valuation_id' => $valuation->id])->column();
                                        $pom_percentage = \app\models\ProfitMethodYearly::find()->select('pom_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>POM</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][pom]"
                                                           value="<?= $pom[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pom_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][pom]"
                                                           value="<?= $pom[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pom_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][pom]"
                                                           value="<?= $pom[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pom_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][pom]"
                                                           value="<?= $pom[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pom_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][pom]"
                                                           value="<?= $pom[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pom_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][pom]"
                                                           value="<?= $pom[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pom_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][pom]"
                                                           value="<?= $pom[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pom_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][pom]"
                                                           value="<?= $pom[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pom_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][pom]"
                                                           value="<?= $pom[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pom_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][pom]"
                                                           value="<?= $pom[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pom_percentage[9] ?>%</td>
                                            </tr>
                                        <?php
                                        $energy = \app\models\ProfitMethodYearly::find()->select('energy')->where(['valuation_id' => $valuation->id])->column();
                                        $energy_percentage = \app\models\ProfitMethodYearly::find()->select('energy_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Energy</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][energy]"
                                                           value="<?= $energy[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $energy_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][energy]"
                                                           value="<?= $energy[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $energy_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][energy]"
                                                           value="<?= $energy[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $energy_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][energy]"
                                                           value="<?= $energy[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $energy_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][energy]"
                                                           value="<?= $energy[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $energy_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][energy]"
                                                           value="<?= $energy[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $energy_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][energy]"
                                                           value="<?= $energy[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $energy_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][energy]"
                                                           value="<?= $energy[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $energy_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][energy]"
                                                           value="<?= $energy[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $energy_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][energy]"
                                                           value="<?= $energy[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $energy_percentage[9] ?>%</td>
                                            </tr>
                                        <?php
                                        $total_undistributed_expenses = \app\models\ProfitMethodYearly::find()->select('energy')->where(['valuation_id' => $valuation->id])->column();
                                        $total_undistributed_expenses_percentage = \app\models\ProfitMethodYearly::find()->select('energy_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Total Undistributed Expenses</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][total_undistributed_expenses]"
                                                           value="<?= $total_undistributed_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_undistributed_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][total_undistributed_expenses]"
                                                           value="<?= $total_undistributed_expenses[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_undistributed_expenses_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][total_undistributed_expenses]"
                                                           value="<?= $total_undistributed_expenses[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_undistributed_expenses_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][total_undistributed_expenses]"
                                                           value="<?= $total_undistributed_expenses[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_undistributed_expenses_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][total_undistributed_expenses]"
                                                           value="<?= $total_undistributed_expenses[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_undistributed_expenses_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][total_undistributed_expenses]"
                                                           value="<?= $total_undistributed_expenses[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_undistributed_expenses_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][total_undistributed_expenses]"
                                                           value="<?= $total_undistributed_expenses[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_undistributed_expenses_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][total_undistributed_expenses]"
                                                           value="<?= $total_undistributed_expenses[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_undistributed_expenses_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][total_undistributed_expenses]"
                                                           value="<?= $total_undistributed_expenses[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_undistributed_expenses_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][total_undistributed_expenses]"
                                                           value="<?= $total_undistributed_expenses[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_undistributed_expenses_percentage[9] ?>%</td>
                                            </tr>


                                            <tr>
                                                <td colspan="21"></td>

                                            </tr>
                                        <?php
                                        $gross_operating_profit = \app\models\ProfitMethodYearly::find()->select('gross_operating_profit')->where(['valuation_id' => $valuation->id])->column();
                                        $gross_operating_profit_percentage = \app\models\ProfitMethodYearly::find()->select('gross_operating_profit_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Gross Operating Profit</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][gross_operating_profit]"
                                                           value="<?= $gross_operating_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $gross_operating_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][gross_operating_profit]"
                                                           value="<?= $gross_operating_profit[1] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $gross_operating_profit_percentage[1] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][gross_operating_profit]"
                                                           value="<?= $gross_operating_profit[2] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $gross_operating_profit_percentage[2] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][gross_operating_profit]"
                                                           value="<?= $gross_operating_profit[3] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $gross_operating_profit_percentage[3] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][gross_operating_profit]"
                                                           value="<?= $gross_operating_profit[4] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $gross_operating_profit_percentage[4] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][gross_operating_profit]"
                                                           value="<?= $gross_operating_profit[5] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $gross_operating_profit_percentage[5] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][gross_operating_profit]"
                                                           value="<?= $gross_operating_profit[6] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $gross_operating_profit_percentage[6] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][gross_operating_profit]"
                                                           value="<?= $gross_operating_profit[7] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $gross_operating_profit_percentage[7] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][gross_operating_profit]"
                                                           value="<?= $gross_operating_profit[8] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $gross_operating_profit_percentage[8] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][gross_operating_profit]"
                                                           value="<?= $gross_operating_profit[9] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $gross_operating_profit_percentage[9] ?>%</td>
                                            </tr>
                                        <?php
                                        $management_fee_base = \app\models\ProfitMethodYearly::find()->select('management_fee_base')->where(['valuation_id' => $valuation->id])->column();
                                        $management_fee_base_percentage = \app\models\ProfitMethodYearly::find()->select('management_fee_base_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Management Fee (base)</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][management_fee_base]"
                                                           value="<?= $management_fee_base[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $management_fee_base_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][management_fee_base]"
                                                           value="<?= $management_fee_base[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $management_fee_base_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][management_fee_base]"
                                                           value="<?= $management_fee_base[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $management_fee_base_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][management_fee_base]"
                                                           value="<?= $management_fee_base[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $management_fee_base_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][management_fee_base]"
                                                           value="<?= $management_fee_base[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $management_fee_base_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][management_fee_base]"
                                                           value="<?= $management_fee_base[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $management_fee_base_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][management_fee_base]"
                                                           value="<?= $management_fee_base[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $management_fee_base_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][management_fee_base]"
                                                           value="<?= $management_fee_base[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $management_fee_base_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][management_fee_base]"
                                                           value="<?= $management_fee_base[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $management_fee_base_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][management_fee_base]"
                                                           value="<?= $management_fee_base[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $management_fee_base_percentage[0] ?>%</td>
                                            </tr>

                                        <?php
                                        $adjusted_gross_profit = \app\models\ProfitMethodYearly::find()->select('adjusted_gross_profit')->where(['valuation_id' => $valuation->id])->column();
                                        $adjusted_gross_profit_percentage = \app\models\ProfitMethodYearly::find()->select('adjusted_gross_profit_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Adjusted Gross Profit (AGOP)</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                            </tr>
                                        <?php
                                        $adjusted_gross_profit = \app\models\ProfitMethodYearly::find()->select('adjusted_gross_profit')->where(['valuation_id' => $valuation->id])->column();
                                        $adjusted_gross_profit_percentage = \app\models\ProfitMethodYearly::find()->select('adjusted_gross_profit_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Adjusted Gross Profit (AGOP)</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px">0%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px">0%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][adjusted_gross_profit]"
                                                           value="<?= $adjusted_gross_profit[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $adjusted_gross_profit_percentage[0] ?>%</td>
                                            </tr>

                                            <tr>
                                                <td colspan="21"></td>

                                            </tr>
                                            <tr>
                                                <td colspan="21">Fixed Expenses</td>

                                            </tr>
                                        <?php
                                        $rental_expenses = \app\models\ProfitMethodYearly::find()->select('rental_expenses')->where(['valuation_id' => $valuation->id])->column();
                                        $rental_expenses_percentage = \app\models\ProfitMethodYearly::find()->select('rental_expenses_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Rental Expenses</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][rental_expenses]"
                                                           value="<?= $rental_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rental_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][rental_expenses]"
                                                           value="<?= $rental_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rental_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][rental_expenses]"
                                                           value="<?= $rental_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rental_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][rental_expenses]"
                                                           value="<?= $rental_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rental_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][rental_expenses]"
                                                           value="<?= $rental_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rental_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][rental_expenses]"
                                                           value="<?= $rental_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rental_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][rental_expenses]"
                                                           value="<?= $rental_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rental_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][rental_expenses]"
                                                           value="<?= $rental_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rental_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][rental_expenses]"
                                                           value="<?= $rental_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rental_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][rental_expenses]"
                                                           value="<?= $rental_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rental_expenses_percentage[0] ?>%</td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $trade_license_fee = \app\models\ProfitMethodYearly::find()->select('trade_license_fee')->where(['valuation_id' => $valuation->id])->column();
                                                $trade_license_fee_percentage = \app\models\ProfitMethodYearly::find()->select('trade_license_fee')->where(['valuation_id' => $valuation->id])->column();

                                                ?>
                                                <td><strong>Trade License Fee</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][trade_license_fee]"
                                                           value="<?= $trade_license_fee[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $trade_license_fee_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][trade_license_fee]"
                                                           value="<?= $trade_license_fee[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $trade_license_fee_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][trade_license_fee]"
                                                           value="<?= $trade_license_fee[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $trade_license_fee_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][trade_license_fee]"
                                                           value="<?= $trade_license_fee[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $trade_license_fee_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][trade_license_fee]"
                                                           value="<?= $trade_license_fee[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $trade_license_fee_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][trade_license_fee]"
                                                           value="<?= $trade_license_fee[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $trade_license_fee_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][trade_license_fee]"
                                                           value="<?= $trade_license_fee[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $trade_license_fee_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][trade_license_fee]"
                                                           value="<?= $trade_license_fee[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $trade_license_fee_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][trade_license_fee]"
                                                           value="<?= $trade_license_fee[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $trade_license_fee_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][trade_license_fee]"
                                                           value="<?= $trade_license_fee[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $trade_license_fee_percentage[0] ?>%</td>
                                            </tr>
                                        <?php
                                        $insurance = \app\models\ProfitMethodYearly::find()->select('insurance')->where(['valuation_id' => $valuation->id])->column();
                                        $insurance_percentage = \app\models\ProfitMethodYearly::find()->select('insurance_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Insurance</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][insurance]"
                                                           value="<?= $insurance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $insurance_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][insurance]"
                                                           value="<?= $insurance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $insurance_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][insurance]"
                                                           value="<?= $insurance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $insurance_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][insurance]"
                                                           value="<?= $insurance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $insurance_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][insurance]"
                                                           value="<?= $insurance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $insurance_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][insurance]"
                                                           value="<?= $insurance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $insurance_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][insurance]"
                                                           value="<?= $insurance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $insurance_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][insurance]"
                                                           value="<?= $insurance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $insurance_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][insurance]"
                                                           value="<?= $insurance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $insurance_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][insurance]"
                                                           value="<?= $insurance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $insurance_percentage[0] ?>%</td>
                                            </tr>
                                        <?php
                                        $capacity_charges = \app\models\ProfitMethodYearly::find()->select('capacity_charges')->where(['valuation_id' => $valuation->id])->column();
                                        $capacity_charges_percentage = \app\models\ProfitMethodYearly::find()->select('capacity_charges_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Capacity Charges</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][capacity_charges]"
                                                           value="><?= $capacity_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $capacity_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][capacity_charges]"
                                                           value="<?= $capacity_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $capacity_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][capacity_charges]"
                                                           value="<?= $capacity_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $capacity_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][capacity_charges]"
                                                           value="<?= $capacity_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $capacity_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][capacity_charges]"
                                                           value="<?= $capacity_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $capacity_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][capacity_charges]"
                                                           value="<?= $capacity_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $capacity_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][capacity_charges]"
                                                           value="<?= $capacity_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $capacity_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][capacity_charges]"
                                                           value="<?= $capacity_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $capacity_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][capacity_charges]"
                                                           value="<?= $capacity_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $capacity_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][capacity_charges]"
                                                           value="<?= $capacity_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $capacity_charges_percentage[0] ?>%</td>
                                            </tr>
                                        <?php
                                        $master_community_charges = \app\models\ProfitMethodYearly::find()->select('master_community_charges')->where(['valuation_id' => $valuation->id])->column();
                                        $master_community_charges_percentage = \app\models\ProfitMethodYearly::find()->select('master_community_charges_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Master Community Charges</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][master_community_charges]"
                                                           value="<?= $master_community_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $master_community_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][master_community_charges]"
                                                           value="<?= $master_community_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $master_community_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][master_community_charges]"
                                                           value="<?= $master_community_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $master_community_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][master_community_charges]"
                                                           value="<?= $master_community_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $master_community_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][master_community_charges]"
                                                           value="<?= $master_community_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $master_community_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][master_community_charges]"
                                                           value="<?= $master_community_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $master_community_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][master_community_charges]"
                                                           value="<?= $master_community_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $master_community_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][master_community_charges]"
                                                           value="<?= $master_community_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $master_community_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][master_community_charges]"
                                                           value="<?= $master_community_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $master_community_charges_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][master_community_charges]"
                                                           value="<?= $master_community_charges[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $master_community_charges_percentage[0] ?>%</td>
                                            </tr>
                                        <?php
                                        $incentive_fee_as_based_on_performance = \app\models\ProfitMethodYearly::find()->select('incentive_fee_as_based_on_performance')->where(['valuation_id' => $valuation->id])->column();
                                        $incentive_fee_as_based_on_performance_percentage = \app\models\ProfitMethodYearly::find()->select('incentive_fee_as_based_on_performance_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Incentive Fee (as based on performance)</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][incentive_fee_as_based_on_performance]"
                                                           value="<?= $incentive_fee_as_based_on_performance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px">0%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][incentive_fee_as_based_on_performance]"
                                                           value="<?= $incentive_fee_as_based_on_performance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $incentive_fee_as_based_on_performance[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][incentive_fee_as_based_on_performance]"
                                                           value="<?= $incentive_fee_as_based_on_performance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $incentive_fee_as_based_on_performance[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][incentive_fee_as_based_on_performance]"
                                                           value="<?= $incentive_fee_as_based_on_performance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $incentive_fee_as_based_on_performance[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][incentive_fee_as_based_on_performance]"
                                                           value="<?= $incentive_fee_as_based_on_performance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $incentive_fee_as_based_on_performance[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][incentive_fee_as_based_on_performance]"
                                                           value="<?= $incentive_fee_as_based_on_performance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $incentive_fee_as_based_on_performance[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][incentive_fee_as_based_on_performance]"
                                                           value="<?= $incentive_fee_as_based_on_performance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $incentive_fee_as_based_on_performance[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][incentive_fee_as_based_on_performance]"
                                                           value="<?= $incentive_fee_as_based_on_performance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $incentive_fee_as_based_on_performance[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][incentive_fee_as_based_on_performance]"
                                                           value="<?= $incentive_fee_as_based_on_performance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $incentive_fee_as_based_on_performance[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][incentive_fee_as_based_on_performance]"
                                                           value="<?= $incentive_fee_as_based_on_performance[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $incentive_fee_as_based_on_performance[0] ?>%</td>
                                            </tr>
                                        <?php
                                        $other_expenses = \app\models\ProfitMethodYearly::find()->select('other_expenses')->where(['valuation_id' => $valuation->id])->column();
                                        $other_expenses_percentage = \app\models\ProfitMethodYearly::find()->select('other_expenses_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Other Expenses</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][other_expenses]"
                                                           value="<?= $other_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][other_expenses]"
                                                           value="<?= $other_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][other_expenses]"
                                                           value="<?= $other_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][other_expenses]"
                                                           value="<?= $other_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][other_expenses]"
                                                           value="<?= $other_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][other_expenses]"
                                                           value="<?= $other_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][other_expenses]"
                                                           value="<?= $other_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][other_expenses]"
                                                           value="<?= $other_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][other_expenses]"
                                                           value="<?= $other_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][other_expenses]"
                                                           value="<?= $other_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                            </tr>
                                        <?php
                                        $total_fixed_expenses = \app\models\ProfitMethodYearly::find()->select('total_fixed_expenses')->where(['valuation_id' => $valuation->id])->column();
                                        $total_fixed_expenses_percentage = \app\models\ProfitMethodYearly::find()->select('total_fixed_expenses_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Total Fixed Expenses</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][total_fixed_expenses]"
                                                           value="<?= $total_fixed_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][total_fixed_expenses]"
                                                           value="<?= $total_fixed_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][total_fixed_expenses]"
                                                           value="<?= $total_fixed_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][total_fixed_expenses]"
                                                           value="<?= $total_fixed_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][total_fixed_expenses]"
                                                           value="<?= $total_fixed_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][total_fixed_expenses]"
                                                           value="<?= $total_fixed_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][total_fixed_expenses]"
                                                           value="<?= $total_fixed_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][total_fixed_expenses]"
                                                           value="<?= $total_fixed_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][total_fixed_expenses]"
                                                           value="<?= $total_fixed_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][total_fixed_expenses]"
                                                           value="<?= $total_fixed_expenses[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $other_expenses_percentage[0] ?>%</td>
                                            </tr>
                                            <tr>
                                                <td colspan="21"></td>

                                            </tr>
                                        <?php
                                        $leasehold_rent = \app\models\ProfitMethodYearly::find()->select('leasehold_rent')->where(['valuation_id' => $valuation->id])->column();
                                        $leasehold_rent_percentage = \app\models\ProfitMethodYearly::find()->select('leasehold_rent_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Leasehold Rent</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][leasehold_rent]"
                                                           value="<?= $leasehold_rent[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $leasehold_rent_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][leasehold_rent]"
                                                           value="<?= $leasehold_rent[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $leasehold_rent_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][leasehold_rent]"
                                                           value="<?= $leasehold_rent[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $leasehold_rent_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][leasehold_rent]"
                                                           value="<?= $leasehold_rent[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $leasehold_rent_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][leasehold_rent]"
                                                           value="<?= $leasehold_rent[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $leasehold_rent_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][leasehold_rent]"
                                                           value="<?= $leasehold_rent[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $leasehold_rent_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][leasehold_rent]"
                                                           value="<?= $leasehold_rent[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $leasehold_rent_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][leasehold_rent]"
                                                           value="<?= $leasehold_rent[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $leasehold_rent_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][leasehold_rent]"
                                                           value="<?= $leasehold_rent[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $leasehold_rent_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][leasehold_rent]"
                                                           value="<?= $leasehold_rent[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $leasehold_rent_percentage[0] ?>%</td>
                                            </tr>
                                        <?php
                                        $reserve_for_replacement = \app\models\ProfitMethodYearly::find()->select('reserve_for_replacement')->where(['valuation_id' => $valuation->id])->column();
                                        $reserve_for_replacement_percentage = \app\models\ProfitMethodYearly::find()->select('reserve_for_replacement_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Reserve for Replacement</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][reserve_for_replacement]"
                                                           value="<?= $reserve_for_replacement[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $reserve_for_replacement_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][reserve_for_replacement]"
                                                           value="<?= $reserve_for_replacement[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $reserve_for_replacement_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][reserve_for_replacement]"
                                                           value="<?= $reserve_for_replacement[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $reserve_for_replacement_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][reserve_for_replacement]"
                                                           value="<?= $reserve_for_replacement[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $reserve_for_replacement_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][reserve_for_replacement]"
                                                           value="<?= $reserve_for_replacement[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $reserve_for_replacement_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][reserve_for_replacement]"
                                                           value="<?= $reserve_for_replacement[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $reserve_for_replacement_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][reserve_for_replacement]"
                                                           value="<?= $reserve_for_replacement[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $reserve_for_replacement_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][reserve_for_replacement]"
                                                           value="<?= $reserve_for_replacement[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $reserve_for_replacement_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][reserve_for_replacement]"
                                                           value="<?= $reserve_for_replacement[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $reserve_for_replacement_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][reserve_for_replacement]"
                                                           value="<?= $reserve_for_replacement[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $reserve_for_replacement_percentage[0] ?>%</td>
                                            </tr>
                                        <?php
                                        $total_a_b = \app\models\ProfitMethodYearly::find()->select('total_a_b')->where(['valuation_id' => $valuation->id])->column();
                                        $total_a_b_percentage = \app\models\ProfitMethodYearly::find()->select('total_a_b_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Total (A+B)</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][total_a_b]"
                                                           value="<?= $total_a_b[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_a_b_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][total_a_b]"
                                                           value="<?= $total_a_b[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_a_b_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][total_a_b]"
                                                           value="<?= $total_a_b[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_a_b_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][total_a_b]"
                                                           value="<?= $total_a_b[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_a_b_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][total_a_b]"
                                                           value="<?= $total_a_b[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_a_b_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][total_a_b]"
                                                           value="<?= $total_a_b[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_a_b_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][total_a_b]"
                                                           value="<?= $total_a_b[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_a_b_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][total_a_b]"
                                                           value="<?= $total_a_b[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_a_b_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][total_a_b]"
                                                           value="<?= $total_a_b[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_a_b_percentage[0] ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][total_a_b]"
                                                           value="<?= $total_a_b[0] ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $total_a_b_percentage[0] ?>%</td>
                                            </tr>

                                            <tr>
                                                <td colspan="21">Net Operating Income (Hotel and FHP)</td>

                                            </tr>
                                        <?php
                                        $rent_expeneses_fhp = \app\models\ProfitMethodYearly::find()->select('rent_expeneses_fhp')->where(['valuation_id' => $valuation->id])->column();
                                        $rent_expeneses_fhp_percentage = \app\models\ProfitMethodYearly::find()->select('rent_expeneses_fhp_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Rent Expeneses FHP</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][rent_expeneses_fhp]"
                                                           value="<?= $rent_expeneses_fhp[0]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rent_expeneses_fhp_percentage[0]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][rent_expeneses_fhp]"
                                                           value="<?= $rent_expeneses_fhp[1]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rent_expeneses_fhp_percentage[1]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][rent_expeneses_fhp]"
                                                           value="<?= $rent_expeneses_fhp[2]; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rent_expeneses_fhp_percentage[2]; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][rent_expeneses_fhp]"
                                                           value="<?= $rent_expeneses_fhp; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rent_expeneses_fhp_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][rent_expeneses_fhp]"
                                                           value="<?= $rent_expeneses_fhp; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rent_expeneses_fhp_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][rent_expeneses_fhp]"
                                                           value="<?= $rent_expeneses_fhp; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rent_expeneses_fhp_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][rent_expeneses_fhp]"
                                                           value="<?= $rent_expeneses_fhp; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rent_expeneses_fhp_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][rent_expeneses_fhp]"
                                                           value="<?= $rent_expeneses_fhp; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rent_expeneses_fhp_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][rent_expeneses_fhp]"
                                                           value="<?= $rent_expeneses_fhp; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rent_expeneses_fhp_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][rent_expeneses_fhp]"
                                                           value="<?= $rent_expeneses_fhp; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $rent_expeneses_fhp_percentage; ?>%</td>
                                            </tr>
                                        <?php
                                        $asset_management_fee = \app\models\ProfitMethodYearly::find()->select('asset_management_fee')->where(['valuation_id' => $valuation->id])->column();
                                        $asset_management_fee_percentage = \app\models\ProfitMethodYearly::find()->select('asset_management_fee_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Asset Management Fee</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][asset_management_fee]"
                                                           value="<?= $asset_management_fee; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $asset_management_fee_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][asset_management_fee]"
                                                           value="<?= $asset_management_fee; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $asset_management_fee_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][asset_management_fee]"
                                                           value="<?= $asset_management_fee; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $asset_management_fee_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][asset_management_fee]"
                                                           value="<?= $asset_management_fee; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $asset_management_fee_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][asset_management_fee]"
                                                           value="<?= $asset_management_fee; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $asset_management_fee_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][asset_management_fee]"
                                                           value="<?= $asset_management_fee; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $asset_management_fee_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][asset_management_fee]"
                                                           value="<?= $asset_management_fee; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $asset_management_fee_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][asset_management_fee]"
                                                           value="<?= $asset_management_fee; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $asset_management_fee_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][asset_management_fee]"
                                                           value="<?= $asset_management_fee; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $asset_management_fee_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][rent_expeneses_fhp]"
                                                           value="<?= $asset_management_fee; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $asset_management_fee_percentage; ?>%</td>
                                            </tr>
                                        <?php
                                        $ebida_noi = \app\models\ProfitMethodYearly::find()->select('ebida_noi')->where(['valuation_id' => $valuation->id])->column();
                                        $ebida_noi_percentage = \app\models\ProfitMethodYearly::find()->select('ebida_noi_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>EBIDA/NOI</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][ebida_noi]"
                                                           value="<?= $ebida_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $ebida_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][ebida_noi]"
                                                           value="<?= $ebida_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $ebida_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][ebida_noi]"
                                                           value="<?= $ebida_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $ebida_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][ebida_noi]"
                                                           value="<?= $ebida_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $ebida_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][ebida_noi]"
                                                           value="<?= $ebida_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $ebida_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][ebida_noi]"
                                                           value="<?= $ebida_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $ebida_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][ebida_noi]"
                                                           value="<?= $ebida_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $ebida_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][ebida_noi]"
                                                           value="<?= $ebida_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $ebida_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][ebida_noi]"
                                                           value="<?= $ebida_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $ebida_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][ebida_noi]"
                                                           value="<?= $ebida_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $ebida_noi_percentage; ?>%</td>
                                            </tr>
                                            <tr>
                                                <td colspan="21"></td>

                                            </tr>
                                        <?php
                                        $pv_of_noi = \app\models\ProfitMethodYearly::find()->select('pv_of_noi')->where(['valuation_id' => $valuation->id])->column();
                                        $pv_of_noi_percentage = \app\models\ProfitMethodYearly::find()->select('pv_of_noi_percentage')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>PV of NOI</strong></td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][1][pv_of_noi]"
                                                           value="<?= $pv_of_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pv_of_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][2][pv_of_noi]"
                                                           value="<?= $pv_of_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pv_of_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][3][pv_of_noi]"
                                                           value="<?= $pv_of_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pv_of_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][4][pv_of_noi]"
                                                           value="<?= $pv_of_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pv_of_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][5][pv_of_noi]"
                                                           value="<?= $pv_of_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pv_of_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][6][pv_of_noi]"
                                                           value="<?= $pv_of_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pv_of_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][7][pv_of_noi]"
                                                           value="<?= $pv_of_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pv_of_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][8][pv_of_noi]"
                                                           value="<?= $pv_of_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pv_of_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][9][pv_of_noi]"
                                                           value="<?= $pv_of_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pv_of_noi_percentage; ?>%</td>
                                                <td><input type="number"
                                                           name="ProfitMethodBase[calculations][10][pv_of_noi]"
                                                           value="<?= $pv_of_noi; ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td style="padding-top: 20px"><?= $pv_of_noi_percentage; ?>%</td>
                                            </tr>
                                            <tr>
                                                <td colspan="21"></td>

                                            </tr>
                                        <?php
                                        $discount_rate = \app\models\ProfitMethodBase::find()->select('discount_rate')->where(['valuation_id' => $valuation->id])->column();
                                        $growth_rate = \app\models\ProfitMethodBase::find()->select('growth_rate')->where(['valuation_id' => $valuation->id])->column();
                                        $reversion_year_yield_rate = \app\models\ProfitMethodBase::find()->select('reversion_year_yield_rate')->where(['valuation_id' => $valuation->id])->column();
                                        $final_estimated_pv_of_subject_property = \app\models\ProfitMethodBase::find()->select('final_estimated_pv_of_subject_property')->where(['valuation_id' => $valuation->id])->column();
                                        $final_estimated_bua = \app\models\ProfitMethodBase::find()->select('final_estimated_bua')->where(['valuation_id' => $valuation->id])->column();
                                        $final_leasable_area = \app\models\ProfitMethodBase::find()->select('final_leasable_area')->where(['valuation_id' => $valuation->id])->column();
                                        $final_leasable_rooms = \app\models\ProfitMethodBase::find()->select('final_leasable_rooms')->where(['valuation_id' => $valuation->id])->column();

                                        ?>
                                            <tr>
                                                <td><strong>Discount Rate</strong></td>
                                                <td colspan="2"><input type="number"
                                                           name="ProfitMethodBase[discount_rate]"
                                                           value="<?= $discount_rate ?>" placeholder=""
                                                           class="form-control"></td>
                                                <td colspan="18"></td>

                                            </tr>
                                            <tr>
                                                <td><strong>Growth Rate (g)</strong></td>
                                                <td colspan="2"><input type="number"
                                                                       name="ProfitMethodBase[growth_rate]"
                                                                       value="<?= $growth_rate ?>" placeholder=""
                                                                       class="form-control"></td>
                                                <td colspan="18"></td>

                                            </tr>
                                            <tr>
                                                <td><strong>Reversion Year (Terminal Value) Yield Rate</strong></td>
                                                <td colspan="2"><input type="number"
                                                                       name="ProfitMethodBase[reversion_year_yield_rate]"
                                                                       value="<?= $reversion_year_yield_rate ?>" placeholder=""
                                                                       class="form-control"></td>
                                                <td colspan="18"></td>

                                            </tr>
                                            <tr>
                                                <td><strong>Final Estimated PV of Subject Property</strong></td>
                                                <td colspan="2"><input type="number"
                                                                       name="ProfitMethodBase[final_estimated_pv_of_subject_property]"
                                                                       value="<?= $final_estimated_pv_of_subject_property ?>" placeholder=""
                                                                       class="form-control"></td>
                                                <td colspan="18"></td>

                                            </tr>

                                            <tr>
                                                <td><strong>Final Estimated PV of Subject Property / BUA</strong></td>
                                                <td colspan="2"><input type="number"
                                                                       name="ProfitMethodBase[final_estimated_bua]"
                                                                       value="<?= $final_estimated_bua ?>" placeholder=""
                                                                       class="form-control"></td>
                                                <td colspan="18"></td>

                                            </tr>
                                            <tr>
                                                <td><strong>Final Estimated PV of Subject Property / Leasable Area</strong></td>
                                                <td colspan="2"><input type="number"
                                                                       name="ProfitMethodBase[final_leasable_area]"
                                                                       value="<?= $final_estimated_bua ?>" placeholder=""
                                                                       class="form-control"></td>
                                                <td colspan="18"></td>

                                            </tr>
                                            <tr>
                                                <td><strong>Final Estimated PV of Subject Property / Per Room</strong></td>
                                                <td colspan="2"><input type="number"
                                                                       name="ProfitMethodBase[final_leasable_rooms]"
                                                                       value="<?= $final_leasable_rooms ?>" placeholder=""
                                                                       class="form-control"></td>
                                                <td colspan="18"></td>

                                            </tr>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>
</div>

                    </div>
                </div>
        </div>
    </div>
    <!-- /.card -->
</div>


<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
    $(document).ready(function () {
        $('#togglePanel').click(function () {
            $('.left-panel').toggleClass('hide-left-panel');
            $('.right-panel').toggleClass('full-right-panel');
            $(this).find('i').toggleClass('fa-arrow-left fa-bars');
        });
    });
</script>
