<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Inspect Property');
$cardTitle = Yii::t('app', 'Inspect Property:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_5/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');


$i = 1;
?>
<script>

</script>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => '5_0']); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title">Major Assets/Equipment</h2>
                            </header>
                            <div class="card-body">
                                <!-- Configration Information Custom-->
                                <section class="card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Electrical</h2>
                                    </header>
                                    <div class="card-body">

                                        <table id="attachment"
                                               class="table table-striped table-bordered table-hover images-table">
                                            <thead>
                                            <tr>
                                                <td class="text-left">Name</td>
                                                <td class="text-left">Quantity</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $row = 0; ?>
                                            <?php foreach ($electrical_data_all as $electrical_data) { ?>
                                                <tr id="image-row-attachment-1-<?php echo $row; ?>">

                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="ReCostAssets[electrical][<?= $row ?>][name]"
                                                               value="<?= $electrical_data->name ?>" placeholder="Name"
                                                               required/>
                                                    </td>

                                                    <td>
                                                        <input type="number" class="form-control"
                                                               name="ReCostAssets[electrical][<?= $row ?>][quantity]"
                                                               value="<?= $electrical_data->quantity ?>"
                                                               placeholder="Quantity" required/>
                                                    </td>
                                                    <input type="hidden" class="form-control"
                                                           name="ReCostAssets[electrical][<?= $row ?>][id]"
                                                           value="<?= $electrical_data->id ?>" placeholder="Name" required/>

                                                    <td class="text-left">
                                                        <button type="button"
                                                                onclick="deleteRow('-attachment-1-<?= $row ?>', '<?= $electrical_data->id; ?>', '1')"
                                                                data-toggle="tooltip"
                                                                title="You want to delete Attachment"
                                                                class="btn btn-danger"><i
                                                                    class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $row++; ?>
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td class="text-left">
                                                    <button type="button" onclick="addAttachment();"
                                                            data-toggle="tooltip" title="Add"
                                                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>


                                </section>


                                <section class="card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Fire System</h2>
                                    </header>
                                    <div class="card-body">

                                        <table id="attachment_2"
                                               class="table table-striped table-bordered table-hover images-table">
                                            <thead>
                                            <tr>
                                                <td class="text-left">Name</td>
                                                <td class="text-left">Quantity</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $row_2 = 0; ?>
                                            <?php foreach ($fire_system_data_all as $fire_system_data) { ?>
                                                <tr id="image-row-attachment-2-<?php echo $row_2; ?>">

                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="ReCostAssets[fire_system][<?= $row_2 ?>][name]"
                                                               value="<?= $fire_system_data->name ?>" placeholder="Name"
                                                               required/>
                                                    </td>

                                                    <td>
                                                        <input type="number" class="form-control"
                                                               name="ReCostAssets[fire_system][<?= $row_2 ?>][quantity]"
                                                               value="<?= $fire_system_data->quantity ?>"
                                                               placeholder="Quantity" required/>
                                                    </td>
                                                    <input type="hidden" class="form-control"
                                                           name="ReCostAssets[fire_system][<?= $row_2 ?>][id]"
                                                           value="<?= $fire_system_data->id ?>" placeholder="Name" required/>

                                                    <td class="text-left">
                                                        <button type="button"
                                                                onclick="deleteRow('-attachment-2-<?= $row_2 ?>', '<?= $fire_system_data->id; ?>', '2')"
                                                                data-toggle="tooltip"
                                                                title="You want to delete Attachment"
                                                                class="btn btn-danger"><i
                                                                    class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $row_2++; ?>
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td class="text-left">
                                                    <button type="button" onclick="addAttachment_2();"
                                                            data-toggle="tooltip" title="Add"
                                                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>


                                </section>

                                <section class="card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Heat Ventilation & Air Conditioning</h2>
                                    </header>
                                    <div class="card-body">

                                        <table id="attachment_3"
                                               class="table table-striped table-bordered table-hover images-table">
                                            <thead>
                                            <tr>
                                                <td class="text-left">Name</td>
                                                <td class="text-left">Quantity</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $row_3 = 0; ?>
                                            <?php foreach ($heat_ventilation_data_all as $heat_ventilation_data) { ?>
                                                <tr id="image-row-attachment-3-<?php echo $row_3; ?>">

                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="ReCostAssets[heat_ventilation][<?= $row_3 ?>][name]"
                                                               value="<?= $heat_ventilation_data->name ?>" placeholder="Name"
                                                               required/>
                                                    </td>

                                                    <td>
                                                        <input type="number" class="form-control"
                                                               name="ReCostAssets[heat_ventilation][<?= $row_3 ?>][quantity]"
                                                               value="<?= $heat_ventilation_data->quantity ?>"
                                                               placeholder="Quantity" required/>
                                                    </td>
                                                    <input type="hidden" class="form-control"
                                                           name="ReCostAssets[heat_ventilation][<?= $row_3 ?>][id]"
                                                           value="<?= $heat_ventilation_data->id ?>" placeholder="Name" required/>

                                                    <td class="text-left">
                                                        <button type="button"
                                                                onclick="deleteRow('-attachment-3-<?= $row_3 ?>', '<?= $heat_ventilation_data->id; ?>', '3')"
                                                                data-toggle="tooltip"
                                                                title="You want to delete Attachment"
                                                                class="btn btn-danger"><i
                                                                    class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $row_3++; ?>
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td class="text-left">
                                                    <button type="button" onclick="addAttachment_3();"
                                                            data-toggle="tooltip" title="Add"
                                                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>


                                </section>

                                <section class="card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Mechanical</h2>
                                    </header>
                                    <div class="card-body">

                                        <table id="attachment_4"
                                               class="table table-striped table-bordered table-hover images-table">
                                            <thead>
                                            <tr>
                                                <td class="text-left">Name</td>
                                                <td class="text-left">Quantity</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $row_4 = 0; ?>
                                            <?php foreach ($mechanical_data_all as $mechanical_data) { ?>
                                                <tr id="image-row-attachment-4-<?php echo $row_4; ?>">

                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="ReCostAssets[mechanical][<?= $row_4 ?>][name]"
                                                               value="<?= $mechanical_data->name ?>" placeholder="Name"
                                                               required/>
                                                    </td>

                                                    <td>
                                                        <input type="number" class="form-control"
                                                               name="ReCostAssets[mechanical][<?= $row_4 ?>][quantity]"
                                                               value="<?= $mechanical_data->quantity ?>"
                                                               placeholder="Quantity" required/>
                                                    </td>
                                                    <input type="hidden" class="form-control"
                                                           name="ReCostAssets[mechanical][<?= $row_4 ?>][id]"
                                                           value="<?= $mechanical_data->id ?>" placeholder="Name" required/>

                                                    <td class="text-left">
                                                        <button type="button"
                                                                onclick="deleteRow('-attachment-4-<?= $row ?>', '<?= $mechanical_data->id; ?>', '4')"
                                                                data-toggle="tooltip"
                                                                title="You want to delete Attachment"
                                                                class="btn btn-danger"><i
                                                                    class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $row_4++; ?>
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td class="text-left">
                                                    <button type="button" onclick="addAttachment_4();"
                                                            data-toggle="tooltip" title="Add"
                                                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>


                                </section>

                                <section class="card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Security System</h2>
                                    </header>
                                    <div class="card-body">

                                        <table id="attachment_5"
                                               class="table table-striped table-bordered table-hover images-table">
                                            <thead>
                                            <tr>
                                                <td class="text-left">Name</td>
                                                <td class="text-left">Quantity</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $row_5 = 0; ?>
                                            <?php foreach ($security_system_data_all as $security_system_data) { ?>
                                                <tr id="image-row-attachment-5-<?php echo $row_5; ?>">

                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="ReCostAssets[security_system][<?= $row_5 ?>][name]"
                                                               value="<?= $security_system_data->name ?>" placeholder="Name"
                                                               required/>
                                                    </td>

                                                    <td>
                                                        <input type="number" class="form-control"
                                                               name="ReCostAssets[security_system][<?= $row_5 ?>][quantity]"
                                                               value="<?= $security_system_data->quantity ?>"
                                                               placeholder="Quantity" required/>
                                                    </td>
                                                    <input type="hidden" class="form-control"
                                                           name="ReCostAssets[security_system][<?= $row_5 ?>][id]"
                                                           value="<?= $security_system_data->id ?>" placeholder="Name" required/>

                                                    <td class="text-left">
                                                        <button type="button"
                                                                onclick="deleteRow('-attachment-5-<?= $row_5 ?>', '<?= $security_system_data->id; ?>', '5')"
                                                                data-toggle="tooltip"
                                                                title="You want to delete Attachment"
                                                                class="btn btn-danger"><i
                                                                    class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $row_5++; ?>
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td class="text-left">
                                                    <button type="button" onclick="addAttachment_5();"
                                                            data-toggle="tooltip" title="Add"
                                                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>


                                </section>

                                <section class="card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Water System</h2>
                                    </header>
                                    <div class="card-body">

                                        <table id="attachment_6"
                                               class="table table-striped table-bordered table-hover images-table">
                                            <thead>
                                            <tr>
                                                <td class="text-left">Name</td>
                                                <td class="text-left">Quantity</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $row_6 = 0; ?>
                                            <?php foreach ($water_system_data_all as $water_system_data) { ?>
                                                <tr id="image-row-attachment-6-<?php echo $row_6; ?>">

                                                    <td>
                                                        <input type="text" class="form-control"
                                                               name="ReCostAssets[water_system][<?= $row_6 ?>][name]"
                                                               value="<?= $water_system_data->name ?>" placeholder="Name"
                                                               required/>
                                                    </td>

                                                    <td>
                                                        <input type="number" class="form-control"
                                                               name="ReCostAssets[water_system][<?= $row_6 ?>][quantity]"
                                                               value="<?= $water_system_data->quantity ?>"
                                                               placeholder="Quantity" required/>
                                                    </td>
                                                    <input type="hidden" class="form-control"
                                                           name="ReCostAssets[water_system][<?= $row_6 ?>][id]"
                                                           value="<?= $water_system_data->id ?>" placeholder="Name" required/>

                                                    <td class="text-left">
                                                        <button type="button"
                                                                onclick="deleteRow('-attachment-6-<?= $row_6 ?>', '<?= $water_system_data->id; ?>', '6')"
                                                                data-toggle="tooltip"
                                                                title="You want to delete Attachment"
                                                                class="btn btn-danger"><i
                                                                    class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $row_6++; ?>
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td class="text-left">
                                                    <button type="button" onclick="addAttachment_6();"
                                                            data-toggle="tooltip" title="Add"
                                                            class="btn btn-primary"><i class="fa fa-plus-circle"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>


                                </section>

                                <div class="card-footer">
                                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>


<script type="text/javascript">
    var row = <?= $row ?>;
    function addAttachment() {

        html = '<tr id="image-row-attachment-1-' + row + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ReCostAssets[electrical][' + row + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="number" class="form-control"  name="ReCostAssets[electrical][' + row + '][quantity]" value="" placeholder="Quantity" required />';
        html += '    </div>';
        html += '  </td>';

        html += '</tr>';

        $('#attachment tbody').append(html);

        row++;
    }

    var row_2 = <?= $row_2 ?>;
    function addAttachment_2() {

        html = '<tr id="image-row-attachment-2-' + row_2 + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ReCostAssets[fire_system][' + row_2 + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="number" class="form-control"  name="ReCostAssets[fire_system][' + row_2 + '][quantity]" value="" placeholder="Quantity" required />';
        html += '    </div>';
        html += '  </td>';

        html += '</tr>';

        $('#attachment_2 tbody').append(html);

        row_2++;
    }


    var row_3 = <?= $row_3 ?>;
    function addAttachment_3() {

        html = '<tr id="image-row-attachment-3-' + row_3 + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ReCostAssets[heat_ventilation][' + row_3 + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="number" class="form-control"  name="ReCostAssets[heat_ventilation][' + row_3 + '][quantity]" value="" placeholder="Quantity" required />';
        html += '    </div>';
        html += '  </td>';

        html += '</tr>';

        $('#attachment_3 tbody').append(html);

        row_3++;
    }

    var row_4 = <?= $row_4 ?>;
    function addAttachment_4() {

        html = '<tr id="image-row-attachment-4-' + row_4 + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ReCostAssets[mechanical][' + row_4 + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="number" class="form-control"  name="ReCostAssets[mechanical][' + row_4 + '][quantity]" value="" placeholder="Quantity" required />';
        html += '    </div>';
        html += '  </td>';

        html += '</tr>';

        $('#attachment_4 tbody').append(html);

        row_4++;
    }


    var row_5 = <?= $row_5 ?>;
    function addAttachment_5() {

        html = '<tr id="image-row-attachment-5-' + row_5 + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ReCostAssets[security_system][' + row_5 + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="number" class="form-control"  name="ReCostAssets[security_system][' + row_5 + '][quantity]" value="" placeholder="Quantity" required />';
        html += '    </div>';
        html += '  </td>';

        html += '</tr>';

        $('#attachment_5 tbody').append(html);

        row_5++;
    }


    var row_6 = <?= $row_6 ?>;
    function addAttachment_6() {

        html = '<tr id="image-row-attachment-6-' + row_6 + '">';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="text" class="form-control"  name="ReCostAssets[water_system][' + row_6 + '][name]" value="" placeholder="Name" required/>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '       <input type="number" class="form-control"  name="ReCostAssets[water_system][' + row_6 + '][quantity]" value="" placeholder="Quantity" required />';
        html += '    </div>';
        html += '  </td>';

        html += '</tr>';

        $('#attachment_6 tbody').append(html);

        row_6++;
    }
    function deleteRow(rowId, docID, type) {
            console.log('#image-row'+ rowId);


        var url = '<?= \yii\helpers\Url::to('/valuation/remove-attachment-asset'); ?>?id=' + docID;


        swal({
            title: "Are you sure?",
            text: 'You want to delete it',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (response) {
                        if (response.status == 'exist') {
                            swal("Warning!", response.message, "warning");
                        } else {
                            swal("Deleted!", response.message, "success");
                            $('#image-row'+ rowId + ', .tooltip').remove();
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting image.", "error");
            }
        });
    }
</script>