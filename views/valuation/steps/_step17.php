<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'List Previous-Transactions');
$cardTitle = Yii::t('app', 'List Previous-Transactions:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_17/'.$valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
    <style>
        .red{
            background-color: red !important;
        }
    </style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <?php   if ($readonly<>1){  ?>
                <div class="col-5 col-sm-3">
                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                        <?php  echo $this->render('../left-nav', ['model' => $valuation,'step' => 17]); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if ($readonly==1){  $class='"col-12 col-sm-12"'; } ?>

            <?php if ($readonly<>1){  $class='"col-7 col-sm-9"'; } ?>

            <div class=<?= $class ?>>
                <div class="tab-content" id="vert-tabs-tabContent">

                    <section class="valuation-form card card-outline card-primary">


                        <header class="card-header">
                            <h2 class="card-title">Selected Enquiry</h2>
                        </header>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">

                                    <label>Buildings: </label> <?= $selected_records_display->building_info ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Date From: </label> <?= Yii::$app->formatter->asDate($selected_records_display->date_from) ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Date To: </label> <?= Yii::$app->formatter->asDate($selected_records_display->date_to) ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Property Listing: </label> <?= $selected_records_display->property_id ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Property Category: </label> <?= $selected_records_display->property_category ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Bedroom From: </label> <?=$selected_records_display->bedroom_from ?>
                                </div>
                                <div class="col-sm-6">

                                    <label>Bedroom To: </label> <?= $selected_records_display->bedroom_to ?>
                                </div>
                            </div>


                        </div>
                        <div class="card-footer">
                        </div>

                    </section>
                    <div class="listings-transactions-index">

                        <?php


                        $visibility_of_last_checkbox=1;

                        if ($readonly==1) {
                            $visibility_of_last_checkbox=0;
                        }


                        $form = ActiveForm::begin(); ?>

                        <?= CustomGridView::widget([
                            'dataProvider' => $dataProvider,
                            'selected_data' => $selected_data,
                            //  'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
                                ['attribute' => 'date_submitted',
                                    'label' => Yii::t('app', 'Date Submitted'),
                                    'value' => function ($model) {
                                        $transDate=$model->date_submitted;
                                        if($transDate!='' && $transDate!=null)$transDate=Yii::$app->formatter->asDate($transDate);
                                        return $transDate;
                                    },
                                ],
                                /*['attribute' => 'listing_date',
                                    'label' => Yii::t('app', 'Transaction Date'),
                                    'value' => function ($model) {
                                        $transDate=$model->listing_date;
                                        if($transDate!='' && $transDate!=null)$transDate=Yii::$app->formatter->asDate($transDate);
                                        return $transDate;
                                    },
                                ],*/
                                ['attribute' => 'building_info',
                                    'label' => Yii::t('app', 'Building'),
                                    'value' => function ($model) {
                                        return $model->building->title;
                                    }
                                ],
                                ['attribute' => 'no_of_bedrooms', 'label' => Yii::t('app', 'No. of Rooms')],
                                ['attribute' => 'land_size', 'label' => Yii::t('app', 'Land Size')],
                                ['attribute' => 'built_up_area', 'label' => Yii::t('app', 'BUA')],
                                ['attribute' => 'market_value', 'label' => Yii::t('app', 'Price')],
                                ['attribute' => 'psf', 'label' => Yii::t('app', 'Price / sf')],
                                [ 'attribute' => 'id', 'label' => Yii::t('app', 'View'), 'format' => 'raw', 'value' => function ($model) { return Html::a("<i class=\"fa fa-fw fa-edit\"></i>", [ 'previous-transactions/update', 'id' => $model->id ], ['target' => '_blank']); },'visible' =>  $visibility_of_last_checkbox ? true : false ],
                                [
                                    'class' => 'yii\grid\CheckboxColumn',
                                    'checkboxOptions' => function($model) use ($selected_data,$selected_data_auto) {

                                        if($model->id >0) {
                                            $checked = false;
                                            $class = 'simple';
                                            if (in_array($model->id, $selected_data)) {
                                                $checked = true;
                                            }
                                            if(in_array($model->id, $selected_data) && !in_array($model->id, $selected_data_auto)) {
                                                $class = 'changed simple';
                                            }
                                            if(!in_array($model->id, $selected_data) && in_array($model->id, $selected_data_auto)) {
                                                $class = 'changed simple';
                                            }

                                            return ['checked' => $checked, 'class' => $class];
                                        }

                                        // return ['checked' => true];
                                        /* return ['value' => $model->id];*/
                                    },
                                    'visible' =>  $visibility_of_last_checkbox ? true : false,

                                    // 'cssClass'=> (in_array($model->id, $selected_data_auto, TRUE)) ? 'gg':'',

                                ],
                                ['attribute' => 'building_info',
                                    'label' => Yii::t('app', 'Status'),
                                    'format' => 'raw',
                                    'value' => function($model) use ($selected_data,$selected_data_r,$selected_data_auto) {

                                        if($model->id >0) {

                                            if(in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10004;, R&#10004;, A&#10004; 1';
                                            }
                                            if(!in_array($model->id, $selected_data) && !in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return'M&#10060;, R&#10060, A&#10060; 2';
                                            }

                                            if(in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10060;, R&#10004;, A&#10004; 3';
                                            }

                                            if(in_array($model->id, $selected_data) && !in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10060;, R&#10060;, A&#10004; 4';
                                            }

                                            if(in_array($model->id, $selected_data) && !in_array($model->id, $selected_data_r) && in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10004;, R&#10060;, A&#10004; 5';
                                            }
                                            if(!in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10060;, R&#10004;, A&#10060; 6';
                                            }

                                            if(!in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10004;, R&#10004;, A&#10060; 7';
                                            }

                                            if(in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10004;, R&#10004;, A&#10060; 8';
                                            }
                                            if(!in_array($model->id, $selected_data) && in_array($model->id, $selected_data_r) && !in_array($model->id, $selected_data_auto)) {
                                                return ' M&#10060;, R&#10004;, A&#10060; 9';
                                            }
                                            if(!in_array($model->id, $selected_data_r) && in_array($model->id, $selected_data_auto) & !in_array($model->id, $selected_data)) {
                                                return ' M&#10004;, R&#10060;, A&#10060; 10';
                                            }
                                            return $model->id;


                                            // return ['checked' => $checked, 'class' => $class];
                                        }

                                        // return ['checked' => true];
                                        /* return ['value' => $model->id];*/
                                    },
                                ],
                            ],
                        ]); ?>
                        <?php if ($readonly<>1){  ?>
                        <div class="card-footer">
                            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                            <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                        </div>
                        <?php } ?>
                        <?php ActiveForm::end(); ?>


                        <?php if(!empty($select_calculations)){ ?>
                            <section class="valuation-form card card-outline card-primary">

                                <header class="card-header">
                                    <h2 class="card-title"> Calculations </h2>
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Average Land:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->avg_land_size <> null) ? $select_calculations->avg_land_size: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Average BUA:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->built_up_area_size <> null) ? $select_calculations->built_up_area_size: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Average Market Value:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->avg_listings_price_size <> null) ? $select_calculations->avg_listings_price_size: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Highest Price:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->max_price <> null) ? $select_calculations->max_price: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Lowest Price:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->min_price <> null) ? $select_calculations->min_price: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Highest Price/ Sqt:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->max_price_sqt <> null) ? $select_calculations->max_price_sqt: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Lowest Price/ Sqt:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->min_price_sqt <> null) ? $select_calculations->min_price_sqt: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Average PSF:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->avg_psf <> null) ? $select_calculations->avg_psf: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Average Date:</label>
                                                <input type="text" class="form-control"
                                                       value="<?= ($select_calculations->avg_listing_date <> null) ? $select_calculations->avg_listing_date: 0 ?>"
                                                       readonly>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>



<?php
$this->registerJs('
    
     var rows = $(".changed").closest("td").addClass("red");
     $(".simple").closest("td").css(\'text-align\', \'center\');
     $(".simple").on("click",function(){
      if($(this).closest("td").hasClass("red")){
      $(this).removeClass("red");
      $(this).closest("td").removeClass("red");
      }else{
     $(this).addClass("red");
     $(this).closest("td").addClass("red");
     }
    
     });
    
    ');
?>