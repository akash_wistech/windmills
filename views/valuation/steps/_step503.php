<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use app\components\widgets\StatusVerified;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Inspection Travel Details');
$cardTitle = Yii::t('app', ' Inspection Travel Details:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_503/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->registerJs('

  
    
    $("#listingstransactions-start_time,#listingstransactions-end_time").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "HH:mm"
    }); ');
?>


<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 503]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title">Inspection Travel Details</h2>
                            </header>
                            <div class="card-body">

                                <section class="valuation-form card card-outline card-primary">

                                    <header class="card-header">
                                        <h2 class="card-title">Inspection Travel Details</h2>
                                    </header>
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?php
                                                echo $form->field($model, 'mode_of_transport')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->appHelperFunctions->modesOfTransportArr,
                                                    'options' => ['placeholder' => 'Select a Transport Mode'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                                ?>

                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'start_kilometres')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4" id="start_time_id">
                                                <?= $form->field($model, 'start_time', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-start_time" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-start_time" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'end_kilometres')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-4" id="end_time_id">
                                                <?= $form->field($model, 'end_time', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-end_time" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-end_time" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
                                            </div>

                                            <div class="col-sm-4">
                                                <?= $form->field($model, 'car_details')->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>

                                    </div>


                                </section>



                                <?php


                                $allow_array = array(1, 21, 38);
                                if (isset($model->created_by) && ($model->created_by <> null)) {
                                    $check_current_id = $model->created_by;
                                } else {
                                    $check_current_id = Yii::$app->user->identity->id;
                                }
                                if (($key = array_search($check_current_id, $allow_array)) !== false) {
                                    unset($allow_array[$key]);
                                }
                                if (in_array(Yii::$app->user->identity->id, $allow_array) || Yii::$app->user->identity->id == 1) {
                                    echo StatusVerified::widget(['model' => $model, 'form' => $form]);
                                }
                                ?>


                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                                <?php
                                if ($model <> null && $model->id <> null) {
                                    echo Yii::$app->appHelperFunctions->getLastActionHitory([
                                        'model_id' => $model->id,
                                        'model_name' => 'app\models\TransportData',
                                    ]);
                                }
                                ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

