<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
use app\components\widgets\StatusVerified;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Receive Info/Documents');
$cardTitle = Yii::t('app', 'Receive Info/Documents:  {nameAttribute}', [
    'nameAttribute' => $valuation->reference_number,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_2/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
if ($model->email_status != 1) {
    $AlertText = 'Data and Valuation Status will be saved and Email will be sent once?';
} else {
    $AlertText = 'Only Data will be saved?';
}


$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

$("body").on("click", ".sav-btn1", function (e) {
    _this=$(this);
    e.preventDefault();
    swal({
        title: "' . Yii::t('app', $AlertText) . '",
        html: "' . Yii::t('app', 'Are you sure you want to delete this? You will not be able to recover this!') . '",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#47a447",
        confirmButtonText: "' . Yii::t('app', 'Yes, Do you want to save it!') . '",
        cancelButtonText: "' . Yii::t('app', 'Cancel') . '",
    },function(result) {
        if (result) {
            console.log("Hello 123")
            $("#w0").unbind("submit").submit();
        }
    });
});


var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});




$("body").on("click", ".delete-file", function (e) {

    id = $(this).attr("id")
    data = {id:id}

    swal({
    title: "' . Yii::t('app', 'Are you sure you want to delete file from bucket ?') . '",
    html: "' . Yii::t('app', '') . '",
    type: "warning",
    showCancelButton: true,
        confirmButtonColor: "#47a447",
    confirmButtonText: "' . Yii::t('app', 'Yes, Delete it!') . '",
    cancelButtonText: "' . Yii::t('app', 'Cancel') . '",
    },function(result) {
        if (result) {
            $.ajax({
            data : data,
            url: "' . Url::to(['valuation/delete-file']) . '",
            dataType: "html",
            type: "POST",
            success: function(data) {
                data = JSON.parse(data)
                if(data.msg == "success"){

                    deleted = "#deleted-"+id;
                    $(deleted). attr("src", "' . Yii::$app->params['uploadIcon'] . '");

                    removed = ".removed-"+id;
                    $(removed).remove()

                    removedDelBtn = "#del-btn-"+id;
                    $(removedDelBtn).remove()

                    swal({
                        title: "' . Yii::t('app', 'Successfully Deleted') . '",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#47a447",
                    });
                }
            },
            error: bbAlert
        });
        }
    });
    });











');

// dd($valuationDetail);

$image_row = 0;
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css"
      integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg=="
      crossorigin="anonymous" referrerpolicy="no-referrer"/>
<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }
    .help-block2 {
        color: #a94442;
        font-size: .85rem;
        font-weight: 400;
        line-height: 1.5;
        margin-top: -6px;
        font-family: inherit;
    }
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 2]); ?>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title">Enter Documents Details</h2>
                            </header>
                            <div class="card-body">

                                <!-- Media Information-->


                                <?php
                                // dd($valuation->client->client_type);

                                if (isset($valuation->property->required_documents) && ($valuation->property->required_documents <> null)) {
                                    $required_documents = explode(',', $valuation->property->required_documents);
                                }
                                ?>
                                <?php

                                if (isset($valuation->property->optional_documents) && ($valuation->property->optional_documents <> null)) {
                                    $optional_documents = explode(',', $valuation->property->optional_documents);
                                }
                                ?>

                                <?php $unit_row = 0; ?>

                                <?php if ($required_documents <> null && !empty($required_documents)) { ?>
                                    <table id="requestTypes"
                                           class="table table-striped table-bordered table-hover images-table">
                                        <thead>
                                        <tr>
                                            <td>Description</td>
                                            <td>Attachment</td>
                                            <td>Date</td>
                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                        foreach ($required_documents as $required_document) {


                                            if($required_document == 60){
                                                if ($valuation->client->client_type !== "corporate" && $valuation->client->client_type !== "individual") {
                                                    continue;
                                                }
                                            }
                                            if($required_document == 73){
                                                if ($valuation->client->client_type !== "bank" ) {
                                                    continue;
                                                }
                                            }
                                            if($required_document == 58 || $required_document == 59 ){
                                                if ($valuationDetail->extended == "No") {
                                                    continue;
                                                }
                                            }
                                            if($required_document == 61 ){
                                                if ($valuationDetail->completion_status == 1) {
                                                    continue;
                                                }
                                            }
                                            if($required_document == 50 ){
                                                if ($valuationDetail->building->cityName->title != "Dubai") {
                                                    continue;
                                                }
                                            }
                                            if($required_document == 11 ){
                                                if (in_array($valuationDetail->property_id, [20,46]) && $valuationDetail->completion_status == 1) {
                                                    continue;
                                                }
                                            }


                                            $attachment_Details = \app\models\ReceivedDocsFiles::find()->where(["valuation_id" => $valuation->id, "document_id" => $required_document])->one();
                                            // echo "<pre>"; print_r($attachment_Details); echo "</pre>"; die;
                                            $doc_insert_date = '';
                                            if ($attachment_Details->attachment <> null) {
                                                if ($attachment_Details->doc_insert_date <> null) {
                                                    $doc_insert_date = date('d F Y', strtotime($attachment_Details->doc_insert_date));
                                                } else {
                                                    $data = \app\models\ScheduleInspection::find()->where(['valuation_id' => $attachment_Details->valuation_id])->one();
                                                    $doc_insert_date = ($valuation->instruction_date <> null) ? date('d F Y', strtotime($valuation->instruction_date)) : '';
                                                }
                                            }
                                            // dd($attachment_Details->id);
                                            $attachment = $attachment_Details->attachment;
                                            ?>

                                            <tr id="image-row<?php echo $unit_row; ?>">

                                                <td class="text-left">
                                                    <div class="required">


                                                        <label class="control-label">
                                                            <?= Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] ?>
                                                        </label>
                                                        <div class="help-block2" style="display:none"></div>
                                                    </div>
                                                </td>

                                                <td class="text-left upload-docs">
                                                    <div class="form-group">
                                                        <a href="javascript:;"
                                                           id="upload-document<?= $unit_row; ?>"
                                                           onclick="uploadAttachment(<?= $unit_row; ?>)"
                                                           data-toggle="tooltip"
                                                           class="img-thumbnail"
                                                           title="Upload Document">
                                                            <?php

                                                            if ($attachment <> null) {
                                                                $attachment_link= $attachment;
                                                                $explode_attach_doc = explode('received-info/',$attachment);

                                                                if(isset($explode_attach_doc[1])) {
                                                                    $attachment_src = 'https://maxima.windmillsgroup.com/cache/'.$explode_attach_doc[1];
                                                                }else{
                                                                    $attachment_src = Yii::$app->params['uploadDocsIcon'];
                                                                }
                                                                $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/',$attachment);
                                                                if(isset($explode_attach[1])) {
                                                                    $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                }else {
                                                                    $explode_attach = explode('https://newcliudfrontaclmax.s3.ap-southeast-1.amazonaws.com/', $attachment);
                                                                    if (isset($explode_attach[1])) {


                                                                        $attachment_link = Yii::$app->get('s3bucketwe')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');

                                                                      /*  echo $explode_attach[1];
                                                                        die;*/

                                                                    } else {
                                                                        $explode_attach = explode('https://maxima-media.s3.eu-central-1.amazonaws.com/', $attachment);
                                                                        if (isset($explode_attach[1])) {
                                                                            $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                        } else {

                                                                            $explode_attach = explode('https://winsmills-wiz-prod.s3.eu-central-1.amazonaws.com/', $attachment);
                                                                            if (isset($explode_attach[1])) {
                                                                                $attachment_link = Yii::$app->get('olds3bucketwz')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                            }else{
                                                                                $explode_attach = explode('https://maxclientmodule.s3.ap-southeast-1.amazonaws.com/', $attachment);
                                                                                $attachment_link = Yii::$app->get('olds3bucketcm')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                if (strpos($attachment, '.pdf') !== false) {
                                                                    ?>
                                                                    <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                                         id="deleted-<?= $attachment_Details->id ?>"
                                                                         alt="" title=""
                                                                         data-placeholder="no_image.png"/>
                                                                    <a href="<?= $attachment_link; ?>"
                                                                       target="_blank">
                                                                        <span class="glyphicon glyphicon-eye-open"></span>
                                                                    </a>
                                                                    <a href="javascript:;"
                                                                       id="del-btn-<?= $attachment_Details->id ?>">
                                                                        <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                              id="<?= $attachment_Details->id; ?>"></span>
                                                                    </a>

                                                                    <?php

                                                                } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                                    ?>
                                                                    <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                                         id="deleted-<?= $attachment_Details->id ?>"
                                                                         alt="" title=""
                                                                         data-placeholder="no_image.png"/>
                                                                    <a href="<?= $attachment_link; ?>"
                                                                       target="_blank">
                                                                        <span class="glyphicon glyphicon-eye-open"></span>
                                                                    </a>
                                                                    <a href="javascript:;"
                                                                       id="del-btn-<?= $attachment_Details->id ?>">
                                                                        <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                              id="<?= $attachment_Details->id; ?>"></span>
                                                                    </a>
                                                                    <?php

                                                                } else {
                                                                    ?>
                                                                    <img src="<?php echo $attachment_src; ?>"
                                                                         id="deleted-<?= $attachment_Details->id ?>"
                                                                         alt="" title=""
                                                                         data-placeholder="no_image.png"/>
                                                                    <a href="<?= $attachment_link; ?>"
                                                                       target="_blank">
                                                                        <span class="glyphicon glyphicon-eye-open"></span>
                                                                    </a>
                                                                    <a href="javascript:;"
                                                                       id="del-btn-<?= $attachment_Details->id ?>">
                                                                        <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                              id="<?= $attachment_Details->id; ?>"></span>
                                                                    </a>
                                                                    <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                                     alt="" title=""
                                                                     data-placeholder="no_image.png"/>
                                                                <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                                                   target="_blank">
                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                </a>
                                                                <?php
                                                            }

                                                            ?>
                                                        </a>
                                                        <input type="hidden"
                                                               class="removed-<?= $attachment_Details->id; ?> mandatory-doc"
                                                               name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][attachment]"
                                                               value="<?= $attachment; ?>"
                                                               id="input-attachment<?php echo $unit_row; ?>"/>
                                                        <input type="hidden"
                                                               class="removed-<?= $attachment_Details->id; ?>"
                                                               name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][document_id]"
                                                               value="<?= $required_document; ?>"
                                                               id="input-attachment<?php echo $unit_row; ?>"/>

                                                        <input type="hidden"
                                                               class="removed-<?= $attachment_Details->id; ?>"
                                                               name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][valuation_id]"
                                                               value="<?= $valuation->id; ?>"
                                                               id="input-history_id<?php echo $unit_row; ?>"/>
                                                    </div>
                                                </td>

                                                <td>
                                                    <span class="badge grid-badge badge-info bg-info"><?= ($doc_insert_date <> null) ? $doc_insert_date : ''; ?></span>
                                                </td>

                                            </tr>
                                            <?php $unit_row++; ?>

                                        <?php }
                                        foreach ($optional_documents as $optinal_document) {

                                            // if($optinal_document == 68 ){
                                            //     if ($numberOfUnits < 2) {
                                            //         continue;                
                                            //     }
                                            // }
                                            if($optinal_document == 9 ){
                                                if (in_array($valuationDetail->property_id, [4,5,39,48,49,50,53,20,46]) && $valuationDetail->completion_status == 1) {
                                                    continue;
                                                }
                                            }
                                            if($optinal_document == 73){
                                                if ($valuation->client->client_type !== "bank" ) {
                                                    continue;
                                                }
                                            }

                                            if($optinal_document == 1){
                                                if ($valuation->client->client_type == "bank" ) {
                                                    continue;
                                                }
                                            }

                                            $attachment_Details = \app\models\ReceivedDocsFiles::find()->where(["valuation_id" => $valuation->id, "document_id" => $optinal_document])->one();
                                            // echo "<pre>"; print_r($attachment_Details); echo "</pre>"; die;
                                            $doc_insert_date = '';
                                            if ($attachment_Details->attachment <> null) {
                                                if ($attachment_Details->doc_insert_date <> null) {
                                                    $doc_insert_date = date('d F Y', strtotime($attachment_Details->doc_insert_date));
                                                } else {
                                                    $data = \app\models\ScheduleInspection::find()->where(['valuation_id' => $attachment_Details->valuation_id])->one();
                                                    $doc_insert_date = ($data->inspection_date <> null) ? date('d F Y', strtotime($data->inspection_date)) : '';
                                                }
                                            }
                                            // dd($attachment_Details->id);
                                            $attachment = $attachment_Details->attachment;





                                            ?>

                                            <tr id="image-row<?php echo $unit_row; ?>">

                                                <td class="text-left ">
                                                    <div class="required">


                                                        <label>
                                                            <?= Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$optinal_document] ?>
                                                        </label>
                                                    </div>
                                                </td>

                                                <td class="text-left upload-docs">
                                                    <div class="form-group">
                                                        <a href="javascript:;"
                                                           id="upload-document<?= $unit_row; ?>"
                                                           onclick="uploadAttachment(<?= $unit_row; ?>)"
                                                           data-toggle="tooltip"
                                                           class="img-thumbnail"
                                                           title="Upload Document">
                                                            <?php

                                                            if ($attachment <> null) {



                                                                    $attachment_link = $attachment;
                                                                    $explode_attach_doc = explode('received-info/', $attachment);

                                                                    if (isset($explode_attach_doc[1])) {
                                                                        $attachment_src = 'https://maxima.windmillsgroup.com/cache/' . $explode_attach_doc[1];
                                                                    } else {
                                                                        $attachment_src = Yii::$app->params['uploadDocsIcon'];
                                                                    }
                                                                    $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/', $attachment);
                                                                    if (isset($explode_attach[1])) {
                                                                        $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                    } else {
                                                                        $explode_attach = explode('https://newcliudfrontaclmax.s3.ap-southeast-1.amazonaws.com/', $attachment);
                                                                        if (isset($explode_attach[1])) {


                                                                            $attachment_link = Yii::$app->get('s3bucketwe')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');

                                                                            /*  echo $explode_attach[1];
                                                                              die;*/

                                                                        } else {
                                                                            $explode_attach = explode('https://maxima-media.s3.eu-central-1.amazonaws.com/', $attachment);
                                                                            if (isset($explode_attach[1])) {
                                                                                $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                            } else {

                                                                                $explode_attach = explode('https://winsmills-wiz-prod.s3.eu-central-1.amazonaws.com/', $attachment);
                                                                                if (isset($explode_attach[1])) {
                                                                                    $attachment_link = Yii::$app->get('olds3bucketwz')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                                }else{
                                                                                    $explode_attach = explode('https://maxclientmodule.s3.ap-southeast-1.amazonaws.com/', $attachment);
                                                                                    $attachment_link = Yii::$app->get('olds3bucketcm')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                                                }
                                                                            }
                                                                        }
                                                                    }


                                                                if (strpos($attachment, '.pdf') !== false) {
                                                                    ?>
                                                                    <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>"
                                                                         id="deleted-<?= $attachment_Details->id ?>"
                                                                         alt="" title=""
                                                                         data-placeholder="no_image.png"/>
                                                                    <a href="<?= $attachment_link; ?>"
                                                                       target="_blank">
                                                                        <span class="glyphicon glyphicon-eye-open"></span>
                                                                    </a>
                                                                    <a href="javascript:;"
                                                                       id="del-btn-<?= $attachment_Details->id ?>">
                                                                        <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                              id="<?= $attachment_Details->id; ?>"></span>
                                                                    </a>

                                                                    <?php

                                                                } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {
                                                                    ?>
                                                                    <img src="<?= Yii::$app->params['uploadDocsIcon']; ?>"
                                                                         id="deleted-<?= $attachment_Details->id ?>"
                                                                         alt="" title=""
                                                                         data-placeholder="no_image.png"/>
                                                                    <a href="<?= $attachment_link; ?>"
                                                                       target="_blank">
                                                                        <span class="glyphicon glyphicon-eye-open"></span>
                                                                    </a>
                                                                    <a href="javascript:;"
                                                                       id="del-btn-<?= $attachment_Details->id ?>">
                                                                        <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                              id="<?= $attachment_Details->id; ?>"></span>
                                                                    </a>
                                                                    <?php

                                                                } else {
                                                                    ?>
                                                                    <img src="<?php echo $attachment_src; ?>"
                                                                         id="deleted-<?= $attachment_Details->id ?>"
                                                                         alt="" title=""
                                                                         data-placeholder="no_image.png"/>
                                                                    <a href="<?= $attachment_link; ?>"
                                                                       target="_blank">
                                                                        <span class="glyphicon glyphicon-eye-open"></span>
                                                                    </a>
                                                                    <a href="javascript:;"
                                                                       id="del-btn-<?= $attachment_Details->id ?>">
                                                                        <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                              id="<?= $attachment_Details->id; ?>"></span>
                                                                    </a>
                                                                    <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <img src="<?php echo Yii::$app->params['uploadIcon'];; ?>"
                                                                     alt="" title=""
                                                                     data-placeholder="no_image.png"/>
                                                                <a href="<?= Yii::$app->params['uploadIcon']; ?>"
                                                                   target="_blank">
                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                </a>
                                                                <?php
                                                            }

                                                            ?>
                                                        </a>
                                                        <input type="hidden"
                                                               class="removed-<?= $attachment_Details->id; ?>"
                                                               name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][attachment]"
                                                               value="<?= $attachment; ?>"
                                                               id="input-attachment<?php echo $unit_row; ?>"/>
                                                        <input type="hidden"
                                                               class="removed-<?= $attachment_Details->id; ?>"
                                                               name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][document_id]"
                                                               value="<?= $optinal_document; ?>"
                                                               id="input-attachment<?php echo $unit_row; ?>"/>

                                                        <input type="hidden"
                                                               class="removed-<?= $attachment_Details->id; ?>"
                                                               name="ReceivedDocs[received_docs][<?php echo $unit_row; ?>][valuation_id]"
                                                               value="<?= $valuation->id; ?>"
                                                               id="input-history_id<?php echo $unit_row; ?>"/>
                                                    </div>
                                                </td>

                                                <td>
                                                    <span class="badge grid-badge badge-info bg-info"><?= ($doc_insert_date <> null) ? $doc_insert_date : ''; ?></span>
                                                </td>


                                            </tr>
                                            <?php $unit_row++; ?>

                                        <?php }

                                        ?>
                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                <?php } ?>




                                <?php $model->step = 1; ?>
                                <?php echo $form->field($model, 'step')->textInput(['maxlength' => true,
                                    'type' => 'hidden'])->label('') ?>

                                <?php $atch_count = 0; ?>
                                <section class="card mx-4" style="border-top:2px solid #FFA500;">
                                    <header class="card-header">
                                        <h2 class="card-title"><strong>Additional Documents Provided by Client</strong></h2>

                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool add-km-image btn-warning text-dark"
                                                    title="Additional Document Image">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                    </header>
                                    <div class="card-body">
                                        <div class="row" id="km-table">
                                            <?php
                                            if($valuation->kmImages<>null){
                                              $atch_count = $unit_row;
                                                foreach($valuation->kmImages as $key => $image){

                                                    $attachment_link= $image->attachment;
                                                    $explode_attach_doc = explode('received-info/',$image->attachment);

                                                    if(isset($explode_attach_doc[1])) {
                                                        $attachment_src = 'https://maxima.windmillsgroup.com/cache/'.$explode_attach_doc[1];
                                                    }else{
                                                        $attachment_src = Yii::$app->params['uploadDocsIcon'];
                                                    }
                                                    $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/',$image->attachment);
                                                    if(isset($explode_attach[1])) {
                                                        $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                    }else {
                                                        $explode_attach = explode('https://newcliudfrontaclmax.s3.ap-southeast-1.amazonaws.com/', $image->attachment);
                                                        if (isset($explode_attach[1])) {


                                                            $attachment_link = Yii::$app->get('s3bucketwe')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');

                                                            /*  echo $explode_attach[1];
                                                              die;*/

                                                        } else {
                                                            $explode_attach = explode('https://maxima-media.s3.eu-central-1.amazonaws.com/', $image->attachment);
                                                            $attachment_link = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                                                        }
                                                    }



                                                    ?>
                                                    <div class="col-2 my-2 upload-docs" id="image-row<?=$atch_count?>">
                                                        <div class="form-group">
                                                            <a href="javascript:;" id="upload-document<?= $atch_count ?>"
                                                               data-uploadid=<?= $atch_count ?>  data-toggle="tooltip"
                                                               class="img-thumbnail open-img-window" title="Upload Document">

                                                                <img src="<?= $attachment_src ?>" alt=""
                                                                     title="" data-placeholder="no_image.png" />
                                                            </a>
                                                            <a href="<?= $attachment_link ?>" class="mx-2" target="_blank">
                                                                <i class="fa fa-eye text-primary"></i>
                                                            </a>
                                                            <input type="hidden"
                                                                   name="ReceivedDocs[km_images][<?= $atch_count ?>][attachment]"
                                                                   id="input-attachment<?=$atch_count?>"
                                                                   value="<?= $image->attachment ?>" />
                                                            <input type="hidden"
                                                                   name="ReceivedDocs[km_images][<?= $atch_count ?>][db_id]"
                                                                   value="<?= $image->id ?>" />
                                                        </div>
                                                    </div>
                                                    <?php $unit_row++; ?>
                                                    <?php
                                                    $atch_count++;
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </section>



                                <?php
                                $allow_array = array(1,142,92);
                                if(isset($model->created_by) && ($model->created_by <> null)){
                                    $check_current_id = $model->created_by;
                                }else{
                                    $check_current_id = Yii::$app->user->identity->id;
                                }
                                if (($key = array_search($check_current_id, $allow_array)) !== false) {
                                    unset($allow_array[$key]);
                                }
                                if (in_array(Yii::$app->user->identity->id, $allow_array) || Yii::$app->user->identity->id == 1) {
                                    echo StatusVerified::widget([ 'model' => $model, 'form' => $form ]);
                                }
                                ?>
                            </div>
                            <div class="card-footer">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                                <?php
                                if($model<>null && $model->id<>null){
                                    echo Yii::$app->appHelperFunctions->getLastActionHitory([
                                        'model_id' => $model->id,
                                        'model_name' =>'app\models\ReceivedDocs',
                                    ]);
                                }
                                ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>



<script type="text/javascript">
    var unit_row = <?= $unit_row ?>;


    var uploadAttachment = function (attachmentId) {


        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function (json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);


                            console.log(json.file.href);
                            var str1 = json['file'].href;
                            var str2 = ".pdf";
                            var str3 = ".doc";
                            var str4 = ".docx";
                            var str5 = ".xlsx";
                            var str6 = ".xls";
                            if (str1.indexOf(str2) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                            } else if (str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1) {
                                $('#upload-document' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                            } else {
                                $('#upload-document' + attachmentId + ' img').prop('src', json['file'].cache_path);
                            }
                            $('#input-attachment' + attachmentId).val(json['file'].href);


                            /*  $('#upload-document' + attachmentId + ' img').prop('src', json['file'].href);
                             $('#input-attachment' + attachmentId).val(json['file'].href);*/
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }

</script>

<?php

$this->registerJs('






    
    


    var atch_count = "'.$unit_row.'";

    $("body").on("click", ".add-km-image", function (e) {
        
        call_url = "'.\yii\helpers\Url::to(['suggestion/getvalimagehtml']).'";
        $.ajax({
            url: call_url,
            data: {atch_count:atch_count},
            dataType: "html",
            success: function(data) {
                data = JSON.parse(data);
                console.log(data)
                $("#km-table").append(data.col);
                atch_count++;                    
            },
            error: bbAlert
        });
    });


    
    $("body").on("click", ".open-img-window", function (e) {
        target_id = $(this).attr("data-uploadid");
        uploadAttachment(target_id)
    });



































        
    function initAutocomplete(){
        initialize();
    }
        
        
    var map, marker, geocoder, infowindow;
    
    function initialize() {
        
        var latdefault = 25.276987;
        var lngdefault = 55.296249;
        
        var client_lat = $("#lat").val();
        var client_lng = $("#lng").val();
        
        if(client_lat != ""){
            latdefault = parseFloat(client_lat);
        }
        if(client_lng != ""){
            lngdefault = parseFloat(client_lng);
        }
        
        var latlng = new google.maps.LatLng(latdefault,lngdefault);
        
        map = new google.maps.Map(document.getElementById("map"), {
            center: latlng,
            zoom: 13
        });
        marker = new google.maps.Marker({
            map: map,
            position: latlng,
            draggable: false,
            anchorPoint: new google.maps.Point(0, -29)
        });
        var input = document.getElementById("searchInput");
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        geocoder = new google.maps.Geocoder();
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo("bounds", map);
        infowindow = new google.maps.InfoWindow();
        
        autocomplete.addListener("place_changed", function() {            
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocompletes returned place contains no geometry");
                return;
            }
            
            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            const locationUrl = `https://www.google.com/maps/search/?api=1&query=${place.name},${place.geometry.location.lat()},${place.geometry.location.lng()}`;
            // console.log(locationUrl)
            bindDataToForm(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng(), locationUrl);
            infowindow.setContent(place.formatted_address);
            infowindow.open(map, marker);
            
        });
        
        // this function will work on marker move event into map
        google.maps.event.addListener(marker, "dragend", function() {

            geocoder.geocode({
                "latLng": marker.getPosition()
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        // var locationUrl = `https://www.google.com/maps/search/?api=1&query=${marker.getPosition().lat()},${marker.getPosition().lng()}`;
                        // console.log(locationUrl)
                        bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });
    }
    
    function bindDataToForm(address, lat, lng, locationUrl){
        document.getElementById("location").value = address;
        document.getElementById("lat").value = lat;
        document.getElementById("lng").value = lng;
        document.getElementById("meeting_place").value = address;
        document.getElementById("searchInput").value = address;
        document.getElementById("locationUrl").value = locationUrl;
    }
    
    function updateLocation(address, lat, lng, locationUrl) {
        
        var latlng = new google.maps.LatLng(lat, lng);
        marker.setPosition(latlng);
        map.setCenter(latlng);
        bindDataToForm(address, lat, lng, locationUrl);
        
    }
    
    window.addEventListener("load", initialize);
        
');
?>




