<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;
ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Client Provided Rents');
$cardTitle = Yii::t('app', 'Client Provided Rents:  {nameAttribute}', [
    'nameAttribute' => $model->valuation_id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/client-provided-rents?id='.$valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$options = '';
foreach (Yii::$app->appHelperFunctions->getIncomeTypesArr() as $key => $value) {
    $options .= "<option value='".$key."'> ".$value." </option>";
}

$statusOptions = '';
foreach (Yii::$app->appHelperFunctions->getCprArr() as $key => $value) {
    $statusOptions .= "<option value='".$key."'> ".$value." </option>";
}



?>
<style>
    .datepicker-days .table>thead>tr>th,
    .table>tbody>tr>th,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>tbody>tr>td,
    .table>tfoot>tr>td {
        padding: inherit !important;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .width_40 {
        width: 40% !important;

    }

    .width_20 {
        width: 20% !important;

    }

    .padding_5 {
        padding: 5px;
    }

    .kv-file-content, .upload-docs img {
        width: 80px !important;
        height: 80px !important;
    }
</style>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $valuation->reference_number ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php  echo $this->render('../left-nav', ['model' => $valuation,'step' => 'cpr']); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title"><?= $cardTitle ?></h2>
                            </header>
                            <div class="card-body append-area">


                                <?php
                                $i=0;
                                // $n=0;
                                if (is_array($previous_records) AND count($previous_records)>0) {
                                    foreach ($previous_records as $key => $model) {
                                        $this->registerJs('
                                        $("#cpr-contract_start_date'.$i.', #cpr-contract_end_date'.$i.'").datetimepicker({
                                          icons: { time: "far fa-clock" },
                                          allowInputToggle: true,
                                          viewMode: "months",
                                          format: "YYYY-MM-DD",
                                        });
                                      ');
                                        ?>
                                        <div class="card append-card">
                                            <div class="card-header">
                                                <div class="card-tools">
                                                    <button type="button" data-toggle="tooltip" title="Remove"
                                                            class="btn btn-danger btn-sm remove-append-card"><i
                                                                class="fa fa-minus-circle"></i></button>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <?= $form->field($model, '[cpr]['.$i.']income_type')->dropDownList(Yii::$app->appHelperFunctions->getIncomeTypesArr(),['prompt'=>'Select...'])->label('Unit')?>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?= $form->field($model, '[cpr]['.$i.']unit_number')->textInput(['maxlength' => true]) ?>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?= $form->field($model, '[cpr]['.$i.']nla')->textInput(['maxlength' => true]) ?>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <?= $form->field($model, '[cpr]['.$i.']contract_start_date', ['template' => '
                                                    {label}
                                                    <div class="input-group date" style="display: flex" id="cpr-contract_start_date'.$i.'" data-target-input="nearest">
                                                    {input}
                                                    <div class="input-group-append" data-target="#cpr-contract_start_date'.$i.'" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    </div>
                                                    {hint}{error}
                                                    '])->textInput(['maxlength' => true]) ?>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?= $form->field($model, '[cpr]['.$i.']contract_end_date', ['template' => '
                                                    {label}
                                                    <div class="input-group date" style="display: flex" id="cpr-contract_end_date'.$i.'" data-target-input="nearest">
                                                    {input}
                                                    <div class="input-group-append" data-target="#cpr-contract_end_date'.$i.'" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    </div>
                                                    {hint}{error}
                                                    '])->textInput(['maxlength' => true]) ?>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?= $form->field($model, '[cpr]['.$i.']rent')->textInput(['maxlength' => true]) ?>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?= $form->field($model, '[cpr]['.$i.']rent_sqf')->textInput(['maxlength' => true]) ?>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <?= $form->field($model, '[cpr]['.$i.']service_charges_unit')->textInput(['maxlength' => true]) ?>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?= $form->field($model, '[cpr]['.$i.']status')->dropDownList(Yii::$app->appHelperFunctions->getCprArr(),['prompt'=>'Select...'])?>
                                                    </div>

                                                    <div class="col-sm-4 upload-docs">
                                                        <?php $image_row = $i; ?>
                                                        <a href="javascript:;" id="thumb-image<?php echo $image_row; ?>" data-toggle="tooltip"
                                                           onclick="uploadAttachment(<?= $image_row; ?>)"
                                                           class="img-thumbnail">

                                                            <?php

                                                            if ($model->attachment <> null) {

                                                                if (strpos($model->attachment, '.pdf') !== false) {
                                                                    ?>
                                                                    <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>" alt="" title=""
                                                                         data-placeholder="no_image.png"/>
                                                                    <?php

                                                                }else  if (strpos($model->attachment, '.doc') !== false || strpos($model->attachment, '.docx') !== false || strpos($model->attachment, '.xlsx') !== false || strpos($model->attachment, '.xls') !== false) {
                                                                    ?>
                                                                    <img src="<?= Yii::$app->params['uploadIcon']; ?>" alt="" title=""
                                                                         data-placeholder="no_image.png"/>
                                                                    <?php

                                                                }else{
                                                                    ?>
                                                                    <img src="<?php echo $model->attachment; ?>" alt="" title=""
                                                                         data-placeholder="no_image.png"/>
                                                                    <?php
                                                                }
                                                            }else{
                                                                ?>
                                                                <img src="<?= Yii::$app->params['uploadIcon']; ?>" alt="" title=""
                                                                     data-placeholder="no_image.png"/>
                                                                <?php
                                                            }

                                                            ?>
                                                        </a>
                                                        <input type="hidden" name="ClientProvidedRents[cpr][<?php echo $image_row; ?>][attachment]"
                                                               value="<?php echo $model->attachment; ?>"
                                                               id="input-image<?php echo $image_row; ?>"/>

                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label>Report Check</label>
                                                        <input class="mt-4 pt-2 check_image_checked  " <?= ($model->image_checked == 1) ? 'checked' : '' ?> type="checkbox"  name="ClientProvidedRents[cpr][<?= $i; ?>][image_checked]" <?= ($CheckedEmail=='on') ? 'checked' : '' ?>>
                                                    </div>




                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        // echo $this->render('cpr-repeat',['i'=>$n,'form'=>$form,'model'=>$model]);
                                        // $n++;
                                        $i++;
                                    }
                                    // $i++;
                                }
                                else{
                                    ?>

                                    <div class="card append-card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, '[cpr]['.$i.']income_type')->dropDownList(Yii::$app->appHelperFunctions->getIncomeTypesArr(),['prompt'=>'Select...'])?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, '[cpr]['.$i.']unit_number')->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, '[cpr]['.$i.']nla')->textInput(['maxlength' => true]) ?>
                                                </div>

                                                <div class="col-sm-4">
                                                    <?= $form->field($model, '[cpr]['.$i.']contract_start_date', ['template' => '
                                                      {label}
                                                      <div class="input-group date" style="display: flex" id="cpr-contract_start_date" data-target-input="nearest">
                                                      {input}
                                                      <div class="input-group-append" data-target="#cpr-contract_start_date" data-toggle="datetimepicker">
                                                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                      </div>
                                                      </div>
                                                      {hint}{error}
                                                      '])->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, '[cpr]['.$i.']contract_end_date', ['template' => '
                                                      {label}
                                                      <div class="input-group date" style="display: flex" id="cpr-contract_end_date" data-target-input="nearest">
                                                      {input}
                                                      <div class="input-group-append" data-target="#cpr-contract_end_date" data-toggle="datetimepicker">
                                                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                      </div>
                                                      </div>
                                                      {hint}{error}
                                                      '])->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, '[cpr]['.$i.']rent')->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, '[cpr]['.$i.']rent_sqf')->textInput(['maxlength' => true]) ?>
                                                </div>

                                                <div class="col-sm-4">
                                                    <?= $form->field($model, '[cpr]['.$i.']service_charges_unit')->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?= $form->field($model, '[cpr]['.$i.']status')->dropDownList(Yii::$app->appHelperFunctions->getCprArr(),['prompt'=>'Select...'])?>
                                                </div>

                                                <div class="col-sm-4 upload-docs">
                                                    <?php $image_row = $i; ?>
                                                    <a href="javascript:;" id="thumb-image<?php echo $image_row; ?>" data-toggle="tooltip"
                                                       onclick="uploadAttachment(<?= $image_row; ?>)"
                                                       class="img-thumbnail">

                                                        <?php

                                                        if ($model->attachment <> null) {

                                                            if (strpos($model->attachment, '.pdf') !== false) {
                                                                ?>
                                                                <img src="<?= Yii::$app->params['uploadPdfIcon']; ?>" alt="" title=""
                                                                     data-placeholder="no_image.png"/>
                                                                <?php

                                                            }else  if (strpos($model->attachment, '.doc') !== false || strpos($model->attachment, '.docx') !== false || strpos($model->attachment, '.xlsx') !== false || strpos($model->attachment, '.xls') !== false) {
                                                                ?>
                                                                <img src="<?= Yii::$app->params['uploadIcon']; ?>" alt="" title=""
                                                                     data-placeholder="no_image.png"/>
                                                                <?php

                                                            }else{
                                                                ?>
                                                                <img src="<?php echo $model->attachment; ?>" alt="" title=""
                                                                     data-placeholder="no_image.png"/>
                                                                <?php
                                                            }
                                                        }else{
                                                            ?>
                                                            <img src="<?= Yii::$app->params['uploadIcon']; ?>" alt="" title=""
                                                                 data-placeholder="no_image.png"/>
                                                            <?php
                                                        }

                                                        ?>
                                                    </a>
                                                    <input type="hidden" name="ClientProvidedRents[cpr][<?php echo $image_row; ?>][attachment]"
                                                           value="<?php echo $model->attachment; ?>"
                                                           id="input-image<?php echo $image_row; ?>"/>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                $i++;
                                ?>

                            </div>
                            <div class="card-footer bg-white">
                                <div class="row float-right">
                                    <div class="col">
                                        <button type="button" data-toggle="tooltip" title="Add" class="btn btn-sm btn-primary add-cpr"><i class="fa fa-plus-circle"></i></button>
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div class="card-footer">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    </section>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs('

  var i = '.$i.';
  var image_row = '.$i.';

  $("#cpr-contract_start_date,#cpr-contract_end_date").datetimepicker({
      icons: { time: "far fa-clock" },
      allowInputToggle: true,
      viewMode: "months",
      format: "YYYY-MM-DD",
  });

  $("body").on("click", ".remove-append-card", function() {
    $(this).parents(".append-card").remove();
  });

  $("body").on("click", ".add-cpr", function() {
      addCPR()
  });

  function addCPR(){
    html = "";
    html += "<div class=\"card append-card\">";
    html +=   "<div class=\"card-header\">";
    html +=     "<div class=\"card-tools\">";
    html +=       "<button type=\"button\" data-toggle=\"tooltip\" title=\"Remove\" class=\"btn btn-danger btn-sm remove-append-card\"><i class=\"fa fa-minus-circle\"></i></button>";
    html +=     "</div>";
    html +=   "</div>";
    html += "<div class=\"card-body\">";
    html += "<div class=\"row\">";

    html += "<div class=\"col-sm-4\">";
    html +=   "<div class=\"form-group field-clientprovidedrents-income_type\">";
    html +=     "<label class=\"control-label\" for=\"clientprovidedrents-income_type\">Unit</label>";
    html +=       "<select id=\"clientprovidedrents-income_type\" class=\"form-control\" name=\"ClientProvidedRents[cpr]["+i+"][income_type]\">";
    html +=         "<option value>Select...</option>";
    html +=         "'.$options.'";
    html +=       "</select>";
    html +=     "<div class=\"help-block\"></div>";
    html += "</div>";
    html += "</div>";

    html += "<div class=\"col-sm-4\">";
    html += "<div class=\"form-group field-clientprovidedrents-unit_number has-success\">";
    html += "<label class=\"control-label\" for=\"clientprovidedrents-unit_number\">Unit Number</label>";
    html += "<input type=\"text\" id=\"clientprovidedrents-unit_number\" class=\"form-control\" name=\"ClientProvidedRents[cpr]["+i+"][unit_number]\" aria-invalid=\"false\">";
    html += "<div class=\"help-block\"></div>";
    html += "</div>";
    html += "</div>";

    html += "<div class=\"col-sm-4\">";
    html += "<div class=\"form-group field-clientprovidedrents-nla\">";
    html += "<label class=\"control-label\" for=\"clientprovidedrents-nla\">Nla</label>";
    html += "<input type=\"text\" id=\"clientprovidedrents-nla\" class=\"form-control\" name=\"ClientProvidedRents[cpr]["+i+"][nla]\" maxlength=\"255\">";
    html += "<div class=\"help-block\"></div>";
    html += "</div>";
    html += "</div>";


    html += "<div class=\"col-sm-4\">";
    html += "<div class=\"form-group field-clientprovidedrents-contract_start_date\">";
    html += "<label class=\"control-label\" for=\"clientprovidedrents-contract_start_date\">Contract Date</label>";
    html += "<div class=\"input-group date\" style=\"display: flex\" id=\"cpr-contract_start_date-"+i+"\" data-target-input=\"nearest\">";
    html += "<input type=\"text\" id=\"clientprovidedrents-contract_start_date1\" class=\"form-control\" name=\"ClientProvidedRents[cpr]["+i+"][contract_start_date]\">";
    html += "<div class=\"input-group-append\" data-target=\"#cpr-contract_start_date-"+i+"\" data-toggle=\"datetimepicker\">";
    html += "<div class=\"input-group-text\"><i class=\"fa fa-calendar\"></i></div>";
    html += "</div>";
    html += "</div>";
    html += "<div class=\"help-block\"></div>";
    html += "</div>";
    html += "</div>";


    html += "<div class=\"col-sm-4\">";
    html += "<div class=\"form-group field-clientprovidedrents-contract_end_date\">";
    html += "<label class=\"control-label\" for=\"clientprovidedrents-contract_end_date\">Contract End Date</label>";
    html += "<div class=\"input-group date\" style=\"display: flex\" id=\"cpr-contract_end_date-"+i+"\" data-target-input=\"nearest\">";
    html += "<input type=\"text\" id=\"clientprovidedrents-contract_end_date\" class=\"form-control\" name=\"ClientProvidedRents[cpr]["+i+"][contract_end_date]\">";
    html += "<div class=\"input-group-append\" data-target=\"#cpr-contract_end_date-"+i+"\" data-toggle=\"datetimepicker\">";
    html += "<div class=\"input-group-text\"><i class=\"fa fa-calendar\"></i></div>";
    html += "</div>";
    html += "</div>";
    html += "<div class=\"help-block\"></div>";
    html += "</div>";
    html += "</div>";



    html += "<div class=\"col-sm-4\">";
    html += "<div class=\"form-group field-clientprovidedrents-rent\">";
    html += "<label class=\"control-label\" for=\"clientprovidedrents-rent\">Rent</label>";
    html += "<input type=\"text\" id=\"clientprovidedrents-rent\" class=\"form-control\" name=\"ClientProvidedRents[cpr]["+i+"][rent]\" maxlength=\"255\">";
    html += "<div class=\"help-block\"></div>";
    html += "</div>";
    html += "</div>";


    html += "<div class=\"col-sm-4\">";
    html += "<div class=\"form-group field-clientprovidedrents-rent_sqf\">";
    html += "<label class=\"control-label\" for=\"clientprovidedrents-rent_sqf\">Rent/Sqf</label>";
    html += "<input type=\"text\" id=\"clientprovidedrents-rent_sqf\" class=\"form-control\" name=\"ClientProvidedRents[cpr]["+i+"][rent_sqf]\" maxlength=\"255\">";
    html += "<div class=\"help-block\"></div>";
    html += "</div>";
    html += "</div>";


    html += "<div class=\"col-sm-4\">";
    html += "<div class=\"form-group field-clientprovidedrents-service_charges_unit\">";
    html += "<label class=\"control-label\" for=\"clientprovidedrents-service_charges_unit\">Service Chg/Unit</label>";
    html += "<input type=\"text\" id=\"clientprovidedrents-service_charges_unit\" class=\"form-control\" name=\"ClientProvidedRents[cpr]["+i+"][service_charges_unit]\" maxlength=\"255\">";
    html += "<div class=\"help-block\"></div>";
    html += "</div>";
    html += "</div>";



    html += "<div class=\"col-sm-4\">";
    html += "<div class=\"form-group field-clientprovidedrents-status\">";
    html += "<label class=\"control-label\" for=\"clientprovidedrents-status\">Status</label>";
    html += "<select id=\"clientprovidedrents-status\" class=\"form-control\" name=\"ClientProvidedRents[cpr]["+i+"][status]\">";
    html += "<option value>Select...</option>";
    html += "'.$statusOptions.'";
    html += "</select>";
    html += "<div class=\"help-block\"></div>";
    html += "</div>";
    html += "</div>";
    
      html += "<div class=\"col-sm-4 upload-docs\" >";
    html += "<div class=\"form-group field-clientprovidedrents-status\">";
        html += \'   <a href="javascript:;" id="thumb-image\' + image_row + \'" data-toggle="image" onclick="uploadAttachment(\'+image_row+\')" class="img-thumbnail">\';
        html += \'       <img src="'. Yii::$app->params['uploadIcon'].'" alt="" title="" data-placeholder="" />\';
        html += \'   </a>\';
        html += \'   <input type="hidden" name="ClientProvidedRents[cpr][\' + image_row + \'][attachment]" value="" id="input-image\' + image_row + \'" />\';
       html += "<div class=\"help-block\"></div>";
    html += "</div>";
    html += "</div>";
    
    
    html += "<div class=\"col-sm-4\">";
                                                      html += "<label>Report Check</label>";
                                                       html += "<input type=\"checkbox\" id=\"clientprovidedrents-service_charges_unit\" class=\"mt-4 pt-2 check_image_checked\" name=\"ClientProvidedRents[cpr]["+i+"][image_checked]\" >";
           
                                                  html += "</div>";
                                                  html += "<div class=\"help-block\"></div>";


    html += "</div>";
    html += "</div>";
    html += "</div>";

    $(".append-area").append(html);
    datePicker()
    i++;
  }



  function datePicker()
  {
    $(".append-card").find("#cpr-contract_start_date-"+i+",#cpr-contract_end_date-"+i+"").datetimepicker({
        icons: { time: "far fa-clock" },
        allowInputToggle: true,
        viewMode: "months",
        format: "YYYY-MM-DD",
    });
  }







');
?>

<script type="text/javascript">

    var uploadAttachment = function (attachmentId) {

        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= \yii\helpers\Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('#thumb-image' + attachmentId + ' img').hide();
                        $('#thumb-image' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#thumb-image' + attachmentId).prop('disabled', true);
                    },
                    complete: function() {
                        $('#thumb-image' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#thumb-image' + attachmentId).prop('disabled', false);
                        $('#thumb-image' + attachmentId + ' img').show();
                    },
                    success: function(json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json.file.href);
                            var str1 = json['file'].href;
                            var str2 = ".pdf";
                            var str3 = ".doc";
                            var str4 = ".docx";
                            var str5 = ".xlsx";
                            var str6 = ".xls";
                            if(str1.indexOf(str2) != -1){
                                $('#thumb-image' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadPdfIcon']; ?>");
                            }else if(str1.indexOf(str3) != -1 || str1.indexOf(str4) != -1 || str1.indexOf(str5) != -1 || str1.indexOf(str6) != -1){
                                $('#thumb-image' + attachmentId + ' img').prop('src', "<?= Yii::$app->params['uploadDocsIcon']; ?>");
                            }else {
                                $('#thumb-image' + attachmentId + ' img').prop('src', json['file'].href);
                            }
                            $('#input-image' + attachmentId).val(json['file'].href);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }

</script>
