<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Valuations');
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$step_24 = '';
$updateToCancel = '';
$cancelBtns = '';
$actionBtns = '';
$createBtn = false;
if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) {
    $actionBtns .= '{step_1}';
}
$cancelBtns = '{cancel_valuation}';


$reviewerId=User::find()->where(['id'=>Yii::$app->user->identity->id, 'permission_group_id'=>5])->select(['id'])->one();
if ($reviewerId['id'] !=null) {
    $updateToCancel = '{update_to_cancel}';
}
$scanOfficer=User::find()->where(['id'=>Yii::$app->user->identity->id, 'permission_group_id'=>2])->select(['id'])->one();
if ($scanOfficer['id'] !=null) {
    $step_24 = '{step_24}';
}

$this->registerJs('

$("body").on("click", ".sav-btn1", function (e) {

_this=$(this);
var url=_this.attr("data-url");
var alerttext=_this.attr("data-text");
e.preventDefault();
swal({
title: alerttext,
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
     if (result) {
  $.ajax({
  url: url,
  dataType: "html",
  type: "POST",
  });
    }
});
});


    ');
?>

<div class="valuation-index">


    <?php CustomPjax::begin(['id' => 'grid-container']); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'cardTitle' => $cardTitle,
        'createBtn' => $createBtn,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:15px;']],
            ['attribute' => 'reference_number', 'label' => Yii::t('app', 'Reference')],
            ['attribute' => 'client_id',
                'label' => Yii::t('app', 'Client'),
                'value' => function ($model) {
                    return $model->client->title;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'building_info',
                'label' => Yii::t('app', 'Building'),
                'value' => function ($model) {
                    return $model->building->title;
                },
                'filter' => ArrayHelper::map(\app\models\Buildings::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
            ['attribute' => 'instruction_date',
                'label' => Yii::t('app', 'Instruction Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->instruction_date));
                },
            ],
            ['attribute' => 'property_id',
                'label' => Yii::t('app', 'Property'),
                'value' => function ($model) {
                    return $model->property->title;
                },
                'filter' => ArrayHelper::map(\app\models\Properties::find()->orderBy([
                    'title' => SORT_ASC,
                ])->all(), 'id', 'title')
            ],
           /* ['attribute' => 'target_date',
                'label' => Yii::t('app', 'Target Date'),
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->target_date));
                },
            ],*/
            [
                'attribute' => 'service_officer_name',
                'label' => Yii::t('app', 'Valuer'),
                'value' => function ($model) {
                    return (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->firstname.' '.$model->approver->lastname): '';
                },
                'filter' => ArrayHelper::map(\app\models\User::find()->where(['user_type'=>10])->orderBy([
                    'firstname' => SORT_ASC,
                ])->all(), 'id', 'firstname')
            ],

            ['format' => 'raw','attribute' => 'valuation_status',
                'label' => Yii::t('app', 'Valuation Status'),
                'value' => function ($model) {
                    $value=Yii::$app->helperFunctions->valuationStatusListArrLabel[$model['valuation_status']];

// target="_blank"
                    return Html::a($value, Url::to(['valuation/step_1','id'=>$model->id]), [
                        'title' => $value,
                        // 'class' => 'dropdown-item text-1',
                        // 'data-pjax' => "0",
                    ]);
                },
                'contentOptions' => ['class' => ' pt-3 pl-2'],
                'filter' => Yii::$app->helperFunctions->valuationStatusListArr
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:50px;'],
                'contentOptions' => ['class' => 'noprint actions'],
                'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              ' . $actionBtns . '
              '.$cancelBtns.'
              '.$updateToCancel.'
              '.$step_24.'
  					</div>
  				</div>',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fas fa-table"></i> ' . Yii::t('app', 'View'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },

                    'step_1' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Step 1'), $url, [
                            'title' => Yii::t('app', 'Step 1'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },
                    'status' => function ($url, $model) {
                        if ($model['status'] == 1) {
                            return Html::a('<i class="fas fa-eye-slash"></i> ' . Yii::t('app', 'Disable'), $url, [
                                'title' => Yii::t('app', 'Disable'),
                                'class' => 'dropdown-item text-1',
                                'data-confirm' => Yii::t('app', 'Are you sure you want to disable this item?'),
                                'data-method' => "post",
                            ]);
                        } else {
                            return Html::a('<i class="fas fa-eye"></i> ' . Yii::t('app', 'Enable'), $url, [
                                'title' => Yii::t('app', 'Enable'),
                                'class' => 'dropdown-item text-1',
                                'data-confirm' => Yii::t('app', 'Are you sure you want to enable this item?'),
                                'data-method' => "post",
                            ]);
                        }
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fas fa-trash"></i> ' . Yii::t('app', 'Delete'), $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'dropdown-item text-1',
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'data-method' => "post",
                            'data-pjax' => "0",
                        ]);
                    },
                    'cancel_valuation' => function ($url, $model) {
                        return Html::a('<i class="far fa-window-close"></i> ' . Yii::t('app', 'Cancel'),$url, [
                            'title' => Yii::t('app', 'Cancel'),
                            'class' => 'dropdown-item text-1 sav-btn1',
                            'data-url' => $url,
                            'data-text' => 'Email will be sent to Reviewer to Cancel the Valuation.',


                        ]);
                    },
                    'update_to_cancel' => function ($url, $model) {
                        return Html::a('<i class="far fa-window-close"></i> ' . Yii::t('app', 'Update to Cancel'), $url, [
                            'title' => Yii::t('app', 'Update To cancel'),
                            'class' => 'dropdown-item text-1 sav-btn1',
                            'data-text' => 'Valuation will be Canceled.',

                        ]);
                    },
                    'step_24' => function ($url, $model) {
                        return Html::a('<i class="fas fa-edit"></i> ' . Yii::t('app', 'Step 24'), $url, [
                            'title' => Yii::t('app', 'Step 24'),
                            'class' => 'dropdown-item text-1',
                            'data-pjax' => "0",
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>
    <?php CustomPjax::end(); ?>


</div>
