<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Valuations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="valuation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'reference_number',
            'client_id',
            'client_reference',
            'client_passport',
            'no_of_owners',
            'service_officer_name',
            'instruction_date',
            'target_date',
            'building_id',
            'property_id',
            'property_category',
            'tenure',
            'unit_number',
            'city',
            'payment_plan',
            'building_number',
            'plot_number',
            'street',
            'floor_number',
            'instruction_person',
            'land_size',
            'purpose_of_valuation',
            'created_by',
            'created_at',
            'updated_at',
            'updated_by',
            'trashed',
            'trashed_at',
            'trashed_by',
        ],
    ]) ?>

</div>
