<?php
// dd("im here");

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Valuation;
use app\models\ValuationApproversData;
use app\assets\SortBootstrapAsset;
SortBootstrapAsset::register($this);

use app\assets\DateRangePickerAsset2;
use app\models\User;

DateRangePickerAsset2::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\ValuationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', $page_title);
$cardTitle = Yii::t('app', 'Today Approved List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns = '';
$createBtn = false;
if (Yii::$app->menuHelperFunction->checkActionAllowed('create')) {
    $createBtn = true;
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('update')) {
    $actionBtns .= '{update}';
}
if (Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) {
    $actionBtns .= '{step_1}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('revise')) {

    $actionBtns .= '{revise}';
}



$clientsArr = ArrayHelper::map(\app\models\Company::find()
->select([
  'id','title',
  'cname_with_nick' => 'CONCAT(title," ",nick_name)',
])
  ->where(['status' => 1])
  ->andWhere([
      'or',
      ['data_type' => 0],
      ['data_type' => null],
  ])
  ->orderBy(['title' => SORT_ASC,])
  ->all(), 'id', 'title');
  $clientsArr = ['' => 'select'] + $clientsArr;

  $propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
  $propertiesArr = ['' => 'select'] + $propertiesArr;

  $communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
  $communitiesArr = ['' => 'select'] + $communitiesArr;

  $citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
  $citiesArr = ['' => 'select'] + $citiesArr;
  
  
?>


<style>
    th {
        color: #0056b3;
        font-size: 16px;
        text-align: left !important;
        /* padding-right: 30px !important; */
    }
    td {
        font-size: 16px;
        text-align: left;
        /* padding-right: 50px; */
        max-width: 107px;
    }
    
<?php if($page_title == 'Today Recommended') { ?>
    .table th, .table td {
        padding: 16px !important;
    }
    th:nth-child(1) {
      min-width: 85px;
      max-width: 85px;
    }  
    th:nth-child(2) {
      min-width: 85px;
      max-width: 85px;
    }  
    th:nth-child(3) {
      min-width: 80px;
      max-width: 80px;
    }  
    th:nth-child(4) {
      min-width: 70px;
      max-width: 70px;
    }  
    
    th:nth-child(5) {
      min-width: 70px;
      max-width: 70px;
    }

    th:nth-child(6) {
      min-width: 70px;
      max-width: 70px;
    }

    th:nth-child(7) {
      min-width: 70px;
      max-width: 70px;
    }

    th:nth-child(8) {
      min-width: 60px;
      max-width: 60px;
    }

    th:nth-child(9) {
      min-width: 65px;
      max-width: 65px;
    }

    th:nth-child(10) {
      min-width: 60px;
      max-width: 60px;
    }

    th:nth-child(11) {
      min-width: 60px;
      max-width: 60px;
    }

<?php } else if($page_title == 'Today Approved') { ?>
    .table th, .table td {
        padding: 8px !important;
    }
    th:nth-child(1) {
      min-width: 85px;
      max-width: 85px;
    }  
    th:nth-child(2) {
      min-width: 85px;
      max-width: 85px;
    }  
    th:nth-child(3) {
      min-width: 70px;
      max-width: 70px;
    }  
    th:nth-child(4) {
      min-width: 50px;
      max-width: 50px;
    }  
    
    th:nth-child(5) {
        min-width: 50px;
      max-width: 50px;
    }

    th:nth-child(6) {
      min-width: 70px;
      max-width: 70px;
    }

    th:nth-child(7) {
      min-width: 60px;
      max-width: 60px;
    }

    th:nth-child(8) {
      min-width: 70px;
      max-width: 70px;
    }

    th:nth-child(9) {
      min-width: 60px;
      max-width: 60px;
    }

    th:nth-child(10) {
      min-width: 60px;
      max-width: 60px;
    }

    th:nth-child(11) {
      min-width: 60px;
      max-width: 60px;
    }
    th:nth-child(12) {
      min-width: 70px;
      max-width: 70px;
    }
    th:nth-child(13) {
      min-width: 50px;
      max-width: 50px;
    }

<?php } ?>



</style>


<?php
$this->registerJs('

$(".div1").daterangepicker({
   autoUpdateInput: false,
     locale: {
     format: "YYYY-MM-DD"
   }


 });

 $(".div1").on("apply.daterangepicker", function(ev, picker) {
    $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
});

$(\'#valuationsearch-time_period\').on(\'change\',function(){
    var period = $(this).val();
  
    if(period == 9){
        $(\'#date_range_array\').show();
    }else{
        $(\'#date_range_array\').hide();
    }
});

');
?>

<?php 
$form = ActiveForm::begin([
    'action' => [$callback_url],
    'method' => 'get',
]);
?>

<div class="card card-outline card-warning">
    <div class="card-body">
        <div class="row">
        <div class="col-3">
                <div class="form-group">
                    <label> General Search : </label>
                    <input type="search" id="custom-search-input-text" placeholder="Search from datatable" class="form-control custom-search-input-text">
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php if($page_title == "Today Recommended") { ?> 

    <div class="bank-revenue-index">
    <div class="card card-outline card-primary">
        <div class="card-body">
        <span><strong><?= $page_title; ?></strong></span><br /><br />
            <table id="bank-revenue" class="table table-responsive table-striped dataTable">
                <thead>
                    <tr>
                        <th class="">Instruction Date
                            <input id="intruction_date" type="date" class="form-control" placeholder="date">
                        </th>

                        <th class="">Inspection Done Date
                            <input id="inspection_date" type="date" class="form-control" placeholder="date">
                        </th>

                        <th class="">WM Reference<input type="text" class="custom-search-input form-control" placeholder="ref#"></th>

                        <th class="">Client Ref.<input type="text" class="custom-search-input form-control" placeholder="Client ref#"></th>

                        <th class="">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder'=>'client']); ?>
                        </th>

                        <th class="">Property
                          <?php echo Html::dropDownList('property', null, $propertiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'Property']); ?>
                        </th>

                        <th class="">Inspector<input type="text" class="custom-search-input form-control" placeholder="inspector"></th>

                        <th class="">Valuer<input type="text" class="custom-search-input form-control" placeholder="valuer"></th>

                        <th class="">Recomm Market Value<input type="text" class="custom-search-input form-control" placeholder="market value"></th>
                        
                        <th class="">Revised Market Value<input type="text" class="custom-search-input form-control" placeholder="revised market value"></th>

                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                    <?php 
                    if(count($dataProvider->getModels())>0){
                        foreach($dataProvider->getModels() as $model){
                    ?>
                        <tr>
                            <!-- instruction_date -->
                            <td><?=  date('d-M-Y', strtotime($model->instruction_date))  ?>
                            <br>
                            <?= Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time) ?>
                            </td>
                            
                            <!-- Inspection Done Date -->
                            <td>
                                <?php
                                    if(isset($model->inspectProperty->inspection_done_date))
                                    {
                                        echo date('d-M-Y', strtotime($model->inspectProperty->inspection_done_date));
                                        echo "<br>";
                                        echo date('h:i A', strtotime($model->inspectProperty->inspection_done_date));

                                    }else{
                                        echo "";
                                    }
 
                                ?>   
                            </td>

                            <!-- WM Reference -->
                            <td><?php echo $model->reference_number ?></td>

                            <!-- Client Ref -->
                            <td><?= $model->client_reference ?></td>

                            <!-- Client -->
                            <td><?= ($model->client->nick_name <> null) ?$model->client->nick_name : $model->client->title ?></td>
                            
                            <!-- property -->
                            <td><?= $model->property->title ?></td>

                            <!-- inspector -->
                            <td><?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null))? (Yii::$app->appHelperFunctions->staffMemberListArrLastName[$model->scheduleInspection->inspection_officer]): '' ?>
                            
                            <!-- Valuer -->
                            <td><?= (isset($model->service_officer_name) && ($model->service_officer_name <> null))? ($model->approver->lastname): '' ?>

                            <!-- Market Value -->
                            <td>
                                <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => 'valuer'])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    
                                    echo (isset($get_valuation_data->id) && ($get_valuation_data->id <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                                ?>
                                 </td>


                            <!-- Revised Market Value -->

                            <td><?php 
                            
                                $get_valuation = Valuation::find()->where(['parent_id' => $model->id])->one();

                                $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $get_valuation->id])
                                ->orderBy(['id' => SORT_DESC])
                                ->one();
                                
                                echo (isset($get_valuation_data->id) && ($get_valuation_data->id <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                            ?></td>

                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                                        <a class="dropdown-item text-1" href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>" title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> Step 1
                                        </a>
                                    </div>
                                </div>
                            </td>

                        </tr>
                    <?php
                        }
                    }else{
                        
                    }
                    ?>
                </tbody>

                <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<?php } else if ($page_title == "Today Approved") { ?> 

    <div class="bank-revenue-index">
    <div class="card card-outline card-primary">
        <div class="card-body">
        <span><strong><?= $page_title; ?></strong></span><br /><br />
            <table id="bank-revenue" class="table table-responsive table-striped dataTable">
                <thead>
                    <tr>
                    <th class="">Instruction Date
                            <input id="intruction_date" type="date" class="form-control" placeholder="date">
                        </th>

                        <th class="">Review Date
                            <input id="inspection_date" type="date" class="form-control" placeholder="date">
                        </th>

                        <th class="">WM Ref. No.<input type="text" class="custom-search-input form-control" placeholder="ref#"></th>

                        <th class="">Client Ref.<input type="text" class="custom-search-input form-control" placeholder="Client ref#"></th>

                        <th class="">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder'=>'client']); ?>
                        </th>

                        <th class="">Property
                          <?php echo Html::dropDownList('property', null, $propertiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'Property']); ?>
                        </th>

                        <th class="">Reviwer<input type="text" class="custom-search-input form-control" placeholder="Reviwer"></th>

                        <th class="">Approver<input type="text" class="custom-search-input form-control" placeholder="approver"></th>

                        <th class="">Recom Market Value<input type="text" class="custom-search-input form-control" placeholder="market value"></th>

                        <th class="">Review Market Value<input type="text" class="custom-search-input form-control" placeholder="Market Value Reviewed"></th>

                        <th class="">Appr Market Value<input type="text" class="custom-search-input form-control" placeholder="Market Value Approved"></th>

                        <th class="">Total TAT<input type="text" class="custom-search-input form-control" placeholder="Total TAT Taken"></th>

                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                    <?php 
                    if(count($dataProvider->getModels())>0){
                        foreach($dataProvider->getModels() as $model){
                    ?>
                        <tr>
                            <!-- instruction_date -->
                            <td><?=  date('d-M-Y', strtotime($model->instruction_date))  ?>
                            <br>
                            <?= Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time) ?>
                            </td>
                            
                            <!-- Review date -->
                            <td>
                                <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();

                                    if(isset($get_valuation_data->created_at))
                                    {
                                        echo date('d-M-Y', strtotime($get_valuation_data->created_at)) ;
                                        echo "<br>";
                                        echo date('h:i A', strtotime($get_valuation_data->created_at));
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>


                            <!-- WM Reference -->
                            <td><?php echo $model->reference_number ?></td>

                            <!-- Client Ref -->
                            <td><?= $model->client_reference ?></td>

                            <!-- Client -->
                            <td><?= ($model->client->nick_name <> null) ?$model->client->nick_name : $model->client->title ?></td>
                            
                            <!-- property -->
                            <td><?= $model->property->title ?></td>

                            <!-- Reviewer -->
                            <td>
                                <?php  
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => "reviewer"])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();
                                    echo  $get_name->lastname;
                                ?> 
                            </td>

                            <!-- Approver -->
                            <td>
                            <?php  
                                $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                ->andWhere(['approver_type' => "approver"])
                                ->orderBy(['id' => SORT_DESC])
                                ->one();
                                $get_name = User::find()->where(['id' => $get_valuation_data->created_by])->one();

                                echo  $get_name->lastname;
                                ?> 
                            </td>

                            
                            <!-- Recom. Market Value -->
                            <td>
                                <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => 'valuer'])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    
                                    echo (isset($get_valuation_data->estimated_market_value) && ($get_valuation_data->estimated_market_value <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                                 ?>
                            </td>


                            <!-- Reviewed  Market Value -->
                            <td>
                                <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => 'reviewer'])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    
                                    echo (isset($get_valuation_data->estimated_market_value) && ($get_valuation_data->estimated_market_value <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                                 ?>
                            </td>

                            <!-- Approved  Market Value -->
                            <td>
                                <?php
                                    $get_valuation_data = ValuationApproversData::find()->where(['valuation_id' => $model->id])
                                    ->andWhere(['approver_type' => 'approver'])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->one();
                                    
                                    echo (isset($get_valuation_data->estimated_market_value) && ($get_valuation_data->estimated_market_value <> null))? (Yii::$app->appHelperFunctions->wmFormate($get_valuation_data->estimated_market_value)): '' ;
                                 ?>
                            </td>

                            <!-- Total TAT -->
                            <td>
                            <?php
                                if($model->scheduleInspection->inspection_type != 3) {
                                    $inspection_time = $model->scheduleInspection->inspection_time . ":00";
                                    if ($inspection_time == null) {
                                        $inspection_time = '00:00:00';
                                    }

                                    // dd($model->submission_approver_date);

                                    if(!empty($model->scheduleInspection->inspection_date) && !empty($model->submission_approver_date))
                                    {
                                        $startDate = $model->scheduleInspection->inspection_date . ' ' .$inspection_time;
                                        $endDate = $model->submission_approver_date;

                                        $startDateTime = new DateTime($startDate); // Start date and time
                                        $endDateTime = new DateTime($endDate);

                                        $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                                        echo $test = abs(number_format(($workingHours/8.5),1)).'<br>'.'<strong>'.$workingHours.' Hours'.'</strong>';
                                    }
                                }else{
                                    echo "";
                                }
                            ?>
                        </td>

                            <td class="noprint actions">
                                <div class="btn-group flex-wrap">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                                        <a class="dropdown-item text-1" href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>" title="Step 1" data-pjax="0">
                                            <i class="fas fa-edit"></i> Step 1
                                        </a>
                                    </div>
                                </div>
                            </td>

                        </tr>
                    <?php
                        }
                    }else{
                        
                    }
                    ?>
                </tbody>

                <tfoot>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tfoot>
            </table>
        </div>
    </div>
</div>

<?php } ?>


<?php 
    $this->registerJs('
        // $("#bank-revenue").DataTable({
        //     order: [[0, "asc"]],
        //     pageLength: 50,
        // });
        
        
                
        var dataTable = $("#bank-revenue").DataTable({
            order: [[0, "asc"]],
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            columnDefs: [{
                targets: -1,  // The last column index
                orderable: false  // Disable sorting on the last column
              }]
          });
        
          $(".custom-search-input").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          
          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });
          $(".custom-search-input").on("change", function () {
            dataTable.search(this.value).draw();
          });
          
          $(".custom-search-input-client").on("change", function () {
                $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    dataTable.search(data).draw();
                },
                error: bbAlert
                });
            });
                
          $("#bank-revenue_filter").css({
            "display":"none",
          });
  
          $(".custom-search-input").on("click", function (event) {
            event.stopPropagation();
          });
          $(".custom-search-input-client").on("click", function (event) {
            event.stopPropagation();
          });

          //for date search
          $("#intruction_date").on("change", function () {
            $(".custom-search-input").val("");
            var format1 = moment($("#intruction_date").val()).format("DD-MMM-YYYY"); 
              dataTable.search(format1).draw();
            });
    
            //for inspection date search
            $("#inspection_date").on("change", function () {
              $(".custom-search-input").val("");
              var format1 = moment($("#inspection_date").val()).format("DD-MMM-YYYY"); 
                dataTable.search(format1).draw();
              });
  
          
          // Move the length menu to the table footer
          // var lengthMenu = $(".dataTables_length");
          // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");
  
          // Create a custom footer element
          var customFooter = $("<div class=\"custom-footer\"></div>");
        
          // Move the length menu to the custom footer
          var lengthMenu = $(".dataTables_length");
          lengthMenu.detach().appendTo(customFooter);
        
          // Append the custom footer to the DataTable wrapper
          $("#bank-revenue_wrapper").append(customFooter);
          
          
    ');
?>




