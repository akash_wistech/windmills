<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ValuationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="valuation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'reference_number') ?>

    <?= $form->field($model, 'client_id') ?>

    <?= $form->field($model, 'client_reference') ?>

    <?= $form->field($model, 'client_passport') ?>

    <?php // echo $form->field($model, 'no_of_owners') ?>

    <?php // echo $form->field($model, 'service_officer_name') ?>

    <?php // echo $form->field($model, 'instruction_date') ?>

    <?php // echo $form->field($model, 'target_date') ?>

    <?php // echo $form->field($model, 'building_id') ?>

    <?php // echo $form->field($model, 'property_id') ?>

    <?php // echo $form->field($model, 'property_category') ?>

    <?php // echo $form->field($model, 'community') ?>

    <?php // echo $form->field($model, 'sub_community') ?>

    <?php // echo $form->field($model, 'tenure') ?>

    <?php // echo $form->field($model, 'unit_number') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'payment_plan') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'building_number') ?>

    <?php // echo $form->field($model, 'plot_number') ?>

    <?php // echo $form->field($model, 'street') ?>

    <?php // echo $form->field($model, 'floor_number') ?>

    <?php // echo $form->field($model, 'instruction_person') ?>

    <?php // echo $form->field($model, 'land_size') ?>

    <?php // echo $form->field($model, 'purpose_of_valuation') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'trashed') ?>

    <?php // echo $form->field($model, 'trashed_at') ?>

    <?php // echo $form->field($model, 'trashed_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
