<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ValuationFormAsset;
use  app\components\widgets\StatusVerified;

ValuationFormAsset::register($this);
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */

if ($model->email_status !=1) {
    $AlertText='Data and Valuation Status will be saved and Email will be sent once?';
}
else{
    $AlertText='Only Data will be saved?';
}

$this->registerJs('

$("#listingstransactions-instruction_date,#listingstransactions-target_date,#listingstransactions-client_requirement_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});

 $("#listingstransactions-inspection_time").datetimepicker({
        allowInputToggle: true,
        viewMode: "months",
        format: "HH:mm"
    });


$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "'.Yii::t('app',$AlertText).'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
      console.log("Hello 123")
      $("#w0").unbind("submit").submit();
    }
});
});


var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});

$("body").on("click", ".sav-btn", function (e) {
swal({
title: "'.Yii::t('app','Valuation status will be saved and Email will be sent to Clients ?').'",
html: "'.Yii::t('app','').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({

  url: "'.Url::to(['valuation/receive-valuation-email','id'=>$model->id,'step'=>1]).'",
  dataType: "html",
  type: "POST",
  });
    }
});
});




    $("body").on("change", ".client-cls", function () {
           console.log($(this).val());
           if($(this).val() == 1){
            $("#client_fixed_fee").show();
                   }else{
                    $("#client_fixed_fee").hide();
                   }
        $(".appentWali-row").remove();
        _this = $(this);
        GetOtherInstructingPersons(_this);
    });


    var other_instructing_person_id =  "'.$model->other_instructing_person.'";

    function GetOtherInstructingPersons(_this){
        var keyword = "Valuation";
        var client_id = _this.val();      
        if (client_id!= "") {
            var data = {client_id:client_id, other_instructing_person_id:other_instructing_person_id, keyword:keyword};
            $.ajax({
                url: "'.Url::to(['client/other-instructing-persons']).'",
                data: data,
                method: "post",
                dataType: "html",
                success: function(data) {
//                    console.log(data);
                    $(".parent-row-ff").html(data);
                     var element = $("#other-instructing-person");
                     var option = $(\'option:selected\', element).attr(\'data_phone\');
                     $("#valuation-mobile_instructing_person").val(option);
                    
                     $("#other-instructing-person").on("change",function(){
                      var element = $("#other-instructing-person");
                     var option = $(\'option:selected\',element).attr(\'data_phone\');
                     $("#valuation-mobile_instructing_person").val(option);
                    
                    });
                   console.log(option);
                },
                error: bbAlert
            });
        }
        else{
            $(".appentWali-row").remove();
        }
         if (client_id!= "") {
         var data = {client_id:client_id};
            $.ajax({
                url: "'.Url::to(['client/clientinvoice']).'",
                data: data,
                method: "post",
                dataType: "html",
                success: function(response) {
//                    console.log(response);
                   if(response == "1"){
                   $("#client_invoice_type").show();
                   }else{
                    $("#client_invoice_type").hide();
                   }
                },
                error: bbAlert
            });
        }
        else{
            $(".appentWali-row").remove();
        }

    }
   
    $(".client-cls").trigger("change");



');

?>
<style>
    .datepicker-days .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: inherit !important;
    }

    .fade {
        opacity: 1;
    }

    .nav-tabs.flex-column .nav-link.active {
        background-color: #007bff;
        color: white;
        font-weight: bold;
    }
    .number_field_width{
        margin-right: 85px;
    }
</style>

<section class="valuation-form card card-outline card-primary">

    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title">Client Details</h2>
    </header>
    <div class="card-body">


        <div class="row ">
            <div class="col-sm-4">
                <?= $form->field($model, 'reference_number')->textInput(['maxlength' => true, 'value' => ($model->reference_number <> null) ? $model->reference_number : Yii::$app->appHelperFunctions->uniqueReference]) ?>
            </div>
            <div class="col-sm-4">
                <?php
                if(isset($model->id) && $model->id <> null){

                }else{
                    $tomorrow = date("Y-m-d", time() + 86400);
                    $model->instruction_date = date('Y-m-d');
                    $model->target_date = $tomorrow;
                    $model->client_requirement_date = $tomorrow;
                }

                ?>
                <?= $form->field($model, 'instruction_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-instruction_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-instruction_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4" id="inspection_time_id">
                <?= $form->field($model, 'instruction_time', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-inspection_time" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-inspection_time" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>

<?php


?>






            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'inspection_type')->widget(Select2::classname(), [
                    'data' => Yii::$app->appHelperFunctions->inspectionTypeArr,
                    'options' => ['placeholder' => 'Select a Inspection Type'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>

            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'urgency')->widget(Select2::classname(), [
                    'data' => array('0'=>'Normal','1'=>'Urgent'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Urgency');
                ?>
            </div>


            <div class="col-sm-4">
                <?= $form->field($model, 'client_requirement_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-client_requirement_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-client_requirement_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'target_date', ['template' => '
        {label}
        <div class="input-group date" style="display: flex" id="listingstransactions-target_date" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#listingstransactions-target_date" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true])->label('Windmills Target Date') ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'email_subject')->textInput(['required'=>true]);
                ?>

            </div>
            <?php

            if($model->valuation_status == 13){

                ?>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'proceed')->widget(Select2::classname(), [
                        'data' => array('0'=>'Proceed','1'=>'Hold'),

                    ])->label('Hold Status on Partial Inspection');
                    ?>
                </div>
            <?php } ?>





        </div>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Client\'s Details') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'client_id')->widget(Select2::classname(), [
                            'data' => $existing_client_array,

                            'options' => ['placeholder' => 'Select a Client ...', 'class'=> 'client-cls'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Client Name');
                        ?>

                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_reference')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4 parent-row-ff">

                        <div class="form-group field-valuation-mobile_instructing_person">
                            <label class="control-label" for="valuation-mobile_instructing_person">Instructing Person<span class="text-danger">*</span></label>
                            <input type="text" readonly id="valuation-mobile_instructing_person" class="form-control">

                            <div class="help-block"></div>
                        </div>

                    </div>
                </div>
            </div>
        </section>



        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Client\'s Customer Details') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'prefix_customer_name')->widget(\kartik\select2\Select2::classname(), [
                            'data' => Yii::$app->smHelper->getTitleArr(),
                            'options' => ['placeholder' => 'Select...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'initialize' => true,
                            ],
                        ])->label('Client\'s Customer - Prefix');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_name_passport')->textInput(['maxlength' => true])->label('Client\'s Customer - First Name') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'client_lastname_passport')->textInput(['maxlength' => true])->label('Client\'s Customer  - Last Name') ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="card card-outline card-info">
            <header class="card-header">
                <h2 class="card-title"><?= Yii::t('app', 'Contact Person Details') ?></h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4" id="contact_person_prefix_id">
                        <?php
                        echo $form->field($model, 'prefix')->widget(Select2::classname(), [
                            'data' => array('Mr.' => 'Mr.', 'Miss' => 'Miss'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])
                        ?>
                    </div>
                    <div class="col-sm-4 only-char">
                        <?= $form->field($model, 'contact_person_name')->textInput(['maxlength' => true])->label('Contact Person First Name') ?>
                    </div>
                    <div class="col-sm-4 only-char" >
                        <?= $form->field($model, 'contact_person_lastname')->textInput(['maxlength' => true])->label('Contact Person Last Name') ?>
                    </div>
                    <div class="col-sm-4" id="contact_email_id">
                        <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>
                    </div>




                    <div class="col-sm-4" >
                        <label for="phone">Phone number </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <select name='Valuation[phone_code]' id="country-code" class="form-control">
                                    <option value="050" <?php if(isset($model->phone_code) && ($model->phone_code == '050')){ echo 'selected';} ?>>050</option>
                                    <option value="052" <?php if(isset($model->phone_code) && ($model->phone_code == '052')){ echo 'selected';} ?>>052</option>
                                    <option value="054" <?php if(isset($model->phone_code) && ($model->phone_code == '054')){ echo 'selected';} ?>>054</option>
                                    <option value="055" <?php if(isset($model->phone_code) && ($model->phone_code == '055')){ echo 'selected';} ?>>055</option>
                                    <option value="056" <?php if(isset($model->phone_code) && ($model->phone_code == '056')){ echo 'selected';} ?>>056</option>
                                    <option value="058" <?php if(isset($model->phone_code) && ($model->phone_code == '058')){ echo 'selected';} ?>>058</option>
                                </select>
                            </div>
                            <?= $form->field($model, 'contact_phone_no')->textInput(['maxlength' => true,'class' => 'form-control number_field_width','placeholder' => 'Enter phone number'])->label(false) ?>
                        </div>
                    </div>

                    <div class="col-sm-4" >
                        <label for="phone">Land Line number </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <select name="Valuation[land_line_code]" id="country-code" class="form-control">
                                    <option value="02" <?php if(isset($model->land_line_code) && ($model->land_line_code == '02')){ echo 'selected';} ?>>02</option>
                                    <option value="04" <?php if(isset($model->land_line_code) && ($model->land_line_code == '04')){ echo 'selected';} ?>>04</option>
                                    <option value="03" <?php if(isset($model->land_line_code) && ($model->land_line_code == '03')){ echo 'selected';} ?>>03</option>
                                    <option value="06" <?php if(isset($model->land_line_code) && ($model->land_line_code == '06')){ echo 'selected';} ?>>06</option>
                                    <option value="07" <?php if(isset($model->land_line_code) && ($model->land_line_code == '07')){ echo 'selected';} ?>>07</option>
                                    <option value="09" <?php if(isset($model->land_line_code) && ($model->land_line_code == '09')){ echo 'selected';} ?>>09</option>
                                </select>
                            </div>
                            <?= $form->field($model, 'land_line_no')->textInput(['maxlength' => true,'class' => 'form-control number_field_width','placeholder' => 'Enter phone number'])->label(false) ?>
                        </div>
                    </div>

                    <?php if(($model->parent_id <> null) && $model->parent_id > 0){ ?>

                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'revised_reason')->widget(Select2::classname(), [
                                'data' => Yii::$app->appHelperFunctions->revisedReasons,
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label('Reason for Revision');
                            ?>

                        </div>
                    <?php } ?>
                    <?php
                    if($model->client->client_invoice_customer == 1) {
                        $style = "block";
                    }else{
                        $style = "none";
                    }

                    if($model->client->id == 1) {
                        $style_fee = "block";
                    }else{
                        $style_fee = "none";
                    }


                    ?>
                    <div class="col-sm-4" id="client_invoice_type" style="display: <?= $style ?>">
                        <?php
                        echo $form->field($model, 'client_invoice_type')->widget(\kartik\select2\Select2::classname(), [
                            'data' => array('0' => 'Client Name', '1' => 'Customer Name'),

                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Client Invoice Name');
                        ?>
                    </div>

                    <div class="col-sm-4" id="client_fixed_fee" style="display: <?= $style_fee ?>">
                        <?php


                        echo $form->field($model, 'client_fixed_fee_check')->widget(Select2::classname(), [
                            'data' => array('1' => 'Normal', '2' => 'Shaikh Zayed case fixed fee'),
                            'options' => ['placeholder' => 'Select a Placement ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Fee Selection');
                        ?>
                    </div>

                </div>
                <?php
                $model->step=1;
                ?>

                <?php echo $form->field($model, 'step')->textInput(['maxlength' => true,

                    'type'=>'hidden'])->label('') ?>
        </section>

        <?php
        $allow_array = array(1,142,92);
        if(isset($model->created_by) && ($model->created_by <> null)){
            $check_current_id = $model->created_by;
        }else{
            $check_current_id = Yii::$app->user->identity->id;
        }
        if (($key = array_search($check_current_id, $allow_array)) !== false) {
            unset($allow_array[$key]);
        }
        if (in_array(Yii::$app->user->identity->id, $allow_array) || Yii::$app->user->identity->id == 1) {
            echo StatusVerified::widget([ 'model' => $model, 'form' => $form ]);
        }
        ?>



    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php
        if($model<>null && $model->id<>null){
            echo Yii::$app->appHelperFunctions->getLastActionHitory([
                'model_id' => $model->id,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
            ]);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>



