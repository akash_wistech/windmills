<?php  

// dd($valuation);

$instruction_date = date('dS-M-Y', strtotime($valuation->instruction_date));
$building_project = $valuation->building->title;
if($valuation->client_invoice_type == 1){
    $client_name      = $valuation->client_name_passport;
}else {
    $client_name = $valuation->client->title;

}

$inv_format = 'INV-'.date('Y').'-'.$valuation->id;
if($valuation->client_id=1){
    $inv_format = 'INV-'.date('Y').'-DIB'.$valuation->id;
}

$client_reference = $valuation->client_reference;
$property         = $valuation->property->title;
$community        = $valuation->building->communities->title;
$city             = Yii::$app->appHelperFunctions->emiratedListArr[$valuation->building->city];
$fee              = Yii::$app->appHelperFunctions->getClientRevenue($valuation->id);
$fee              = number_format((float)$fee, 2, '.', '');
$fee_to_words     = yii::$app->quotationHelperFunctions->numberTowords($fee);

$vat = 0;
if($valuation->client->fee_master_file_vat==0){
    $vat =  number_format((float)(5 / 100) * $fee, 2, '.', '');

    $fee              = $fee + $vat;
    $fee_to_words     = yii::$app->quotationHelperFunctions->numberTowords($fee);
}




?>

<br><br>
<table>
    <tr>
        <td colspan="2" class="upper-table">To: <?=$client_name ?></td>
        <td class="upper-table">Invoice # <?= $inv_format ?></td>
    </tr>
    <tr>
        <td colspan="2" class="upper-table">Unit Number <?= $valuation->unit_number.', '.$valuation->building->title ?></td>
        <td class="upper-table">Invoice Date: <?= date('d-m-Y') ?></td>
    </tr>
    <tr>
        <td colspan="2" class="upper-table">Plot Number <?= $valuation->plot_number.', '.$valuation->building->subCommunities->title.', '.$valuation->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$valuation->building->city] ?></td>
        <td class="upper-table">TRN: <?= $valuation->client->trn_number; ?></td>
    </tr>
</table>

<br><br>


<table class="outline-border-top">
    <tr style="background-color:#E3F2FD;">
        <td class="lower-table-heading border-left" style="width:36px; text-align:center;"><b>SI No.</b></td>
        <td class="lower-table-heading" style="width:76px; text-align:center;"><b>Instruction Date</b></td>
        <td class="lower-table-heading" style="width:48px; text-align:center;"><b>Fee(AED)</b></td>
        <td class="lower-table-heading" style="width:48px; text-align:center;"><b>Client Ref</b></td>
        <td class="lower-table-heading" style="width:72px; text-align:center;"><b>Customer Name</b></td>
        <td class="lower-table-heading" style="width:52px; text-align:center;"><b>Property</b></td>
        <td class="lower-table-heading" style="width:100px; text-align:center;"><b>Building/Project Name</b></td>
        <td class="lower-table-heading" style="width:76; text-align:center;"><b>Community/Area</b></td>
        <td class="lower-table-heading border-right-outline" style="width:40; text-align:center;"><b>City</b></td>
    </tr>
    <tr>
        <td class="border-left border-right" style="text-align:center;">1</td>
        <td class="border-top border-right" style="text-align:center;"><?= $instruction_date ?></td>
        <td class="border-top border-right" style="text-align:center;"><?= $fee ?></td>
        <td class="border-top border-right" style="text-align:center;"><?= $client_reference ?></td>
        <td class="border-top border-right" style="text-align:center;"><?= $client_name ?></td>
        <td class="border-top border-right" style="text-align:center;"><?= $property ?></td>
        <td class="border-top border-right" style="text-align:center;"><?= $building_project ?></td>
        <td class="border-top border-right" style="text-align:center;"><?= $community ?></td>
        <td class="border-top border-right-outline" style="text-align:center;"><?= $city ?></td>
    </tr>
    <tr>
        <td class="border-top border-right border-left"></td>
        <td class="border-top border-right"></td>
        <td class="border-top border-right"></td>
        <td class="border-top border-right"></td>
        <td class="border-top border-right"></td>
        <td class="border-top border-right"></td>
        <td class="border-top border-right"></td>
        <td class="border-top border-right"></td>
        <td class="border-top border-right-outline"></td>
    </tr>
    <tr>
        <td class="border-top border-right border-left"></td>
        <td class="border-top border-right" style="font-size: 9px;"><b>Total Fee Incl. of VAT</b></td>
        <td class="border-top border-right" style="font-size: 9px;"><b><?= $fee ?></b></td>
        <td class="border-top border-right" style="font-size: 9px;" colspan="3"><b>(<?= $fee_to_words ?>)</b>
        </td>
        <td class="border-top border-right"></td>
        <td class="border-top border-right"></td>
        <td class="border-top border-right-outline"></td>
    </tr>
    <tr>
        <td class="border-top border-right border-left border-bottom"></td>
        <td class="border-top border-right border-bottom" style="font-size: 9px;"><b>VAT @ 5 %</b></td>
        <td class="border-top border-right border-bottom" style="font-size: 9px;"><b><?= $vat ?></b></td>
        <td class="border-top border-right border-bottom"></td>
        <td class="border-top border-right border-bottom"></td>
        <td class="border-top border-right border-bottom"></td>
        <td class="border-top border-right border-bottom"></td>
        <td class="border-top border-right border-bottom"></td>
        <td class="border-top border-right-outline border-bottom"></td>
    </tr>
</table>

<br><br><br><br><br><br>


<table>
    <tr>
        <td class="font-11" colspan="2">Please transfer payment in our following bank account</td>
    </tr>
    <br>
    <tr>
        <td class="font-11" style="width:150px">IBAN</td>
        <td class="font-11">: AE66 0260 0010 1528 6631 901</td>
    </tr>
    <tr>
        <td class="font-11">Account Title</td>
        <td class="font-11">: Windmills Real Estate Valuation Services L.L.C.</td>
    </tr>
    <tr>
        <td class="font-11">Bank</td>
        <td class="font-11">: Emirates NBD Swift Code: EBILAEAD</td>
    </tr>
</table>

<br><br><br><br>


<table>
    <tr>
        <td style="width:150px; text-align:left"><img src="images/proposalsignature.jpg" alt=""></td>
        <td style="width:100px; text-align:right;"><img src="images/proposallogo.jpg" alt=""></td>
    </tr>
</table>

<table>

    <br>
    <tr>
        <td class="font-11" style="width:150px">Bilal Moti MRICS</td>

    </tr>
    <tr>
        <td class="font-11">Managing Director</td>

    </tr>

</table>
























<style>
td.font-11 {
    font-size: 11px;
}

td.upper-table {
    font-size: 10px;
}

td.lower-table-heading {
    font-size: 9px;
    height: 25px;
    line-height: 25px;
}

.outline-border-top {
    border-top: 2px solid black;
}

td.border-top {
    font-size: 10px;
    border-top: 1px solid #eee;
}

td.border-left {
    font-size: 10px;
    border-left: 1px solid black
}

td.border-right {
    font-size: 10px;
    border-right: 1px solid #eee;
}

td.border-right-outline {
    font-size: 10px;
    border-right: 1px solid black
}

td.border-bottom {
    border-bottom: 1px solid black
}
</style>