<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$clientsArr = ArrayHelper::map(\app\models\Company::find()
    ->select([
        'id','title',
        'cname_with_nick' => 'CONCAT(title," ",nick_name)',
    ])
    ->where(['status' => 1])
    ->andWhere([
        'or',
        ['data_type' => 0],
        ['data_type' => null],
    ])
    ->orderBy(['title' => SORT_ASC,])
    ->all(), 'id', 'title');
$clientsArr = ['' => 'select'] + $clientsArr;

$propertiesArr = ArrayHelper::map(\app\models\Properties::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$propertiesArr = ['' => 'select'] + $propertiesArr;

$communitiesArr = ArrayHelper::map(\app\models\Communities::find()->select(['title',])->orderBy(['title' => SORT_ASC,])->all(), 'title', 'title');
$communitiesArr = ['' => 'select'] + $communitiesArr;

$citiesArr = Yii::$app->appHelperFunctions->emiratedListSearchArr;
$citiesArr = ['' => 'select'] + $citiesArr;


$instruction_date_width = '75px';
$client_width = '70px';
$wm_ref_width = '70px';
$property_width = '70px';
$community_width = '70px';
$city_width = '70px';
$valuer_width = '50px';
$fee_width = '80px';
$inspector_width = '70px';
$val_status_width = '70px';
$client_ref_width = '70px';

?>

    <style>
        /* .dataTable th {*/
        /*    color: #0056b3;*/
        /*    font-size: 14px;*/
        /*    text-align: right !important;*/
        /*    padding-right: 30px !important;*/
        /*}*/
        /*.dataTable td {*/
        /*    font-size: 16px;*/
        /*    text-align: right;*/
        /*    padding-right: 50px;*/
        /*}*/
        /*.content-header h1 {*/
        /*       font-size: 16px !important;*/

        /*    }*/
        /*.content-header .row {*/
        /*    margin-bottom: 0px !important;*/
        /*}*/
        /*.content-header {*/
        /*    padding: 4px !important;*/
        /*} */



        .dataTable th {
            color: #0056b3;
            font-size: 15px;
            text-align: left !important;
            /* padding-right: 30px !important; */
        }
        .dataTable td {
            font-size: 16px;
            text-align: left;
            /* padding-right: 50px; */
            padding-top: 3px;
            max-width: 107px;
        }

        .content-header h1 {
            font-size: 16px !important;
        }

        .content-header .row {
            margin-bottom: 0px !important;
        }

        .content-header {
            padding: 0px !important;
        }


        /* <style> */

        th:nth-child(1) {
            min-width: 75px;
            max-width: 75px;
        }
        th:nth-child(2) {
            min-width: <?= $client_width ?>;
            max-width: <?= $client_width ?>;
        }
        th:nth-child(3) {
            min-width: <?= $wm_ref_width ?>;
            max-width: <?= $wm_ref_width ?>;
        }
        th:nth-child(4) {
            min-width: <?= $wm_ref_width ?>;
            max-width: <?= $wm_ref_width ?>;
        }
        th:nth-child(5) {
            min-width: <?= $property_width ?>;
            max-width: <?= $property_width ?>;
        }
        th:nth-child(6) {
            min-width: 50px;
            max-width: 50px;
        }
        th:nth-child(7) {
            min-width: 55px;
            max-width: 55px;
        }
        th:nth-child(8) {
            min-width: 60px;
            max-width: 60px;
        }
        th:nth-child(9) {
            min-width: <?= $fee_width ?>;
            max-width: <?= $fee_width ?>;
        }
        th:nth-child(10) {
            min-width: 40px;
            max-width: 40px;
        }
        th:nth-child(11) {
            min-width: 65px;
            max-width: 65px;
        }
        th:nth-child(12) {
            min-width: 45px;
            max-width: 45px;
        }

    </style>

<?php

// echo "<pre>";
// print_r($_REQUEST);
// echo "<pre>";die;
?>


    <div class="bank-revenue-index col-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <span><strong><?= $page_title; ?></strong></span><br />
                <table id="bank-revenue" class="table table-striped dataTable table-responsive">
                    <thead>
                    <tr>

                        <th class="">Instruction Date
                            <input id="intruction_date" type="date" class="form-control" placeholder="date">
                        </th>
                        <th class="">Inspection Schedule Date
                            <input id="schedule_date" type="date" class="form-control" placeholder="date">
                        </th>

                        <th class="">Inspector Arrival Date
                            <input id="inspection_schedule" type="date" class="form-control" placeholder="date">
                        </th>

                        <th class="">Inspection Done Date
                            <input id="done_date" type="date" class="form-control" placeholder="date">
                        </th>

                        <th class="">WM Reference
                            <input type="text" class="custom-search-input form-control" placeholder="ref#">
                        </th>

                        <th class="">Client Ref
                            <input type="text" class="custom-search-input form-control" placeholder="client ref">
                        </th>

                        <th class="">Client
                            <?php echo Html::dropDownList('client', null, $clientsArr, ['class' => 'custom-search-input-client form-control', 'placeholder'=>'client']); ?>
                        </th>

                        <th class="">Property
                            <?php echo Html::dropDownList('property', null, $propertiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'Property']); ?>
                        </th>


                        <th class="">Community
                            <?php echo Html::dropDownList('community', null, $communitiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'commmunity']); ?>
                        </th>
                        <th class="">City
                            <?php echo Html::dropDownList('city', null, $citiesArr, ['class' => 'custom-search-input form-control', 'placeholder'=>'city']); ?>
                        </th>

                        <th class="">Inspector
                            <input type="text" class="custom-search-input form-control" placeholder="inspector">
                        </th>


                        <!-- <th class="">Fee
                            <input type="text" class="custom-search-input form-control" placeholder="fee">
                        </th>

                        <th class="">Valuation Status
                            <input type="text" class="custom-search-input form-control" placeholder="status">
                        </th> -->

                        <?php
                        if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                            ?>
                            <th>Action</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($dataProvider->getModels())>0){
                        // dd(count($dataProvider->getModels()));
                        foreach($dataProvider->getModels() as $model){
                            ?>
                            <tr class="active">

                                <!-- Instruction date -->
                                <td>
                                    <?=  date('d-M-Y', strtotime($model->instruction_date)) ?>
                                    <br>
                                    <?= Yii::$app->appHelperFunctions->formatTimeAmPm($model->instruction_time) ?>
                                </td>

                                <!-- Inspection Schedule Date -->
                                <td>
                                    <?=  date('d-M-Y', strtotime($model->scheduleInspection->inspection_date)) ?>
                                    <br>
                                    <?= Yii::$app->appHelperFunctions->formatTimeAmPm($model->scheduleInspection->inspection_time) ?>
                                </td>



                                <!-- Inspector Arrival Time -->
                                <td>
                                    <?php
                                    $get_valuation_detail_data = \app\models\ValuationDetail::find()->where(['valuation_id' => $model->id])->one();
                                    echo date('d-M-Y', strtotime($get_valuation_detail_data->arrived_date_time));
                                    echo "<br>";
                                    echo Yii::$app->appHelperFunctions->formatTimeAmPm($get_valuation_detail_data->arrived_date_time);
                                    ?>
                                </td>

                                <!-- Inspection Done Date -->
                                <td>
                                    <?= date('d-M-Y',strtotime($model->inspectProperty->inspection_done_date )) ?>
                                    <br>
                                    <?= date('h:i A',strtotime($model->inspectProperty->inspection_done_date )) ?>
                                </td>

                                <!-- WM Ref No -->
                                <td> <?= $model->reference_number ?> </td>

                                <!-- Client ref -->
                                <td> <?= $model->client_reference ?> </td>

                                <!-- Client -->
                                <td>
                                    <?= ($model->client->nick_name <> null) ?$model->client->nick_name : $model->client->title ?>
                                </td>

                                <!-- Property -->
                                <td>
                                    <?= $model->property->title ?>
                                </td>

                                <!-- Community -->
                                <td>
                                    <?= $model->building->communities->title ?>
                                </td>

                                <!-- City -->
                                <td>
                                    <?= Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city] ?>
                                </td>

                                <!-- Inspector -->
                                <td>
                                    <?php  echo (isset($model->scheduleInspection->inspection_officer) && ($model->scheduleInspection->inspection_officer <> null)) ? $model->scheduleInspection->inspectorData->lastname: '' ?>
                                </td>

                                <!-- No use Code -->
                                <!-- <td style="text-align:right!important;">
                              <?= Yii::$app->appHelperFunctions->wmFormate($model->fee) ?>
                          </td>

                          <td>
                              <?= Yii::$app->helperFunctions->valuationStatusListArrWords[$model->valuation_status] ?>
                          </td> -->




                                <?php
                                if(Yii::$app->user->id==1 || Yii::$app->user->id==14){
                                    ?>
                                    <td class="noprint actions">
                                        <div class="btn-group flex-wrap">
                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <a class="dropdown-item text-1"
                                                   href="<?= yii\helpers\Url::to(['valuation/step_1','id'=>$model->id]) ?>"
                                                   title="Step 1" data-pjax="0">
                                                    <i class="fas fa-edit"></i> Step 1
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>

                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <!-- <th></th>
                    <th></th>
                    <th><strong>Total Fee:</strong></th> -->
                    <th></th>
                    <!-- <th  style="padding-right: 10px!important; text-align:right!important;"> <?= Yii::$app->appHelperFunctions->wmFormate(\app\models\Valuation::getTotal($dataProvider->models, 'fee')) ?></th> -->
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>

                    <?php if(Yii::$app->user->id==1 || Yii::$app->user->id==14){ ?>
                        <th></th>
                    <?php } ?>

                    </tfoot>
                </table>
            </div>
        </div>
    </div>




<?php
$this->registerJs('
        // $("#bank-revenue").DataTable({
        //     order: [[0, "asc"]],
        //     pageLength: 50,
        //     searching: false,
        // });
        
          var dataTable = $("#bank-revenue").DataTable({
            order: [[0, "asc"]],
            pageLength: 50,
            searching: true,
            dom: "lrtip", // Specify the desired layout
            columnDefs: [{
              targets: -1,  // The last column index
              orderable: false  // Disable sorting on the last column
            }]
          });
        
          $(".custom-search-input").on("change", function () {
            $("#intruction_date").val("");
            $("#schedule_date").val("");
            $("#done_date").val("");
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input").on("keyup", function () {
            $("#intruction_date").val("");
            $("#schedule_date").val("");
            $("#done_date").val("");
            dataTable.search(this.value).draw();
          });

          //for date search
          $("#intruction_date").on("change", function () {
            $(".custom-search-input").val("");
            var format1 = moment($("#intruction_date").val()).format("DD-MMM-YYYY"); 
              dataTable.search(format1).draw();
            });

            //for inspecpection schedule date search
            $("#schedule_date").on("change", function () {
              $(".custom-search-input").val("");
              var format1 = moment($("#schedule_date").val()).format("DD-MMM-YYYY"); 
                dataTable.search(format1).draw();
            });

              //for inspection done date search
              $("#done_date").on("change", function () {
                $(".custom-search-input").val("");
                var format1 = moment($("#done_date").val()).format("DD-MMM-YYYY"); 
                  dataTable.search(format1).draw();
              });

          $(".custom-search-input-text").on("keyup", function () {
            dataTable.search(this.value).draw();
          });

          $(".custom-search-input-client").on("change", function () {
              $.ajax({
                url: "'.yii\helpers\Url::to(['suggestion/getclient']).'/"+this.value,
                method: "post",
                dataType: "html",
                success: function(data) {
                  console.log(data);
                  dataTable.search(data).draw();
                },
                error: bbAlert
            });

          });
                
          $("#bank-revenue_filter").css({
            "display":"none",
          });
        
        $(".custom-search-input").on("click", function (event) {
          event.stopPropagation();
        });
        $(".custom-search-input-client").on("click", function (event) {
          event.stopPropagation();
        });
        

        // Move the length menu to the table footer
        // var lengthMenu = $(".dataTables_length");
        // lengthMenu.detach().appendTo("#bank-revenue_wrapper .dataTables_footer");

        // Create a custom footer element
        var customFooter = $("<div class=\"custom-footer\"></div>");
      
        // Move the length menu to the custom footer
        var lengthMenu = $(".dataTables_length");
        lengthMenu.detach().appendTo(customFooter);
      
        // Append the custom footer to the DataTable wrapper
        $("#bank-revenue_wrapper").append(customFooter);

        if($("#comm-id").val() == "Today Performance Overview")
        {
          $("#community-id").css({
            "display":"none",
          });
        }

    ');
?>