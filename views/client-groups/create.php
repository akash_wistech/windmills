<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientGroups */

$this->title = 'Create Client Groups';
$cardTitle = Yii::t('app','Create Client Groups');
$this->params['breadcrumbs'][] = ['label' => 'Client Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-Groups-create">


    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>

</div>
