<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientGroups */

$this->title = 'Update Client Groups: ' . $model->title;
$cardTitle = Yii::t('app','Update Client Groups:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => 'Client Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="client-Groups-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
