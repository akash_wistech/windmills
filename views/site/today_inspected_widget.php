
<div class="col-lg-4 col-md-6 col-sm-6 py-3">
    <div id="w0" class="card-stats">
        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                <div class="card-icon" style="font-size:15px; text-align:center">
                <?= date('d'); ?> <br> <?= date('M'); ?>
                </div>
            </div>
            <div class=" col-10 position-absolute" style="right:0px;" >
                <h4  class="card-category font-increase text-right" style="color:#757575"> <span class="today-text-info"> Inspection Scheduled </span>  <br><?= $today_total_schedule_insp ?></h4 >
                <h4 class=" float-right font-increase" style="color:#757575" >
                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                        <?= number_format($today_total_schedule_insp_fee)?>
                    <?php } ?>
                </h4>
            </div>
        </div>
        <div class="card-footer bg-white">
            <div class="stats position-relative" style="box-sizing: border-box;">
            <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/valuations-to-inspect?time=today'])?>"> <span style="font-size:14px !important;">Go to Today for Inspected</span> </a>
            </div>
        </div>
    </div>
</div>