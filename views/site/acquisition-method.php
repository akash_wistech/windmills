<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\TinyMceAsset;
TinyMceAsset::register($this);
use  app\components\widgets\StatusVerified;
use yii\helpers\Url;

$this->registerJs('


tinymce.init({
  selector: ".editor",

  menubar: false,
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table paste code help wordcount"
  ],
  toolbar: "undo redo | formatselect | " +
  "bold italic backcolor | alignleft aligncenter " +
  "alignright alignjustify | bullist numlist outdent indent | " +
  "removeformat | help",
});





');



/* @var $this yii\web\View */
/* @var $model app\models\SpecialAssumptionreport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="special-assumptionreport-form card card-outline card-primary">

    <?php $form = ActiveForm::begin(); ?>

    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
        <div class="card-tools">
            <a href="<?= Url::toRoute(['site/get-history?id='.$model->id.'&m_name=SpecialAssumptionreport&file_type=acquisitionmethod']) ?>"
                target="_blank" class="btn btn-info btn-sm">History</a>
        </div>
    </header>

    <div class="card-body">
        <div class="row">
        
  <div class="col-sm-12">
    <?= $form->field($model, 'gifted_granted')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'purchased')->textarea(['class'=>'editor']) ?>
</div>
            <div class="col-sm-12">
                <?= $form->field($model, 'inherited')->textarea(['class'=>'editor']) ?>
            </div>
        </div>
          <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?php 
            if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'file_type' => 'acquisitionmethod',
                    'rec_type' => 'masterfile',
                ]);
            }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
