<?php 
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Reinstatement Cost Documents');
?>

<section class="active-zone-form card">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="card-header">
        <h3 class="card-title text-bold">
            <i class="fas fa-edit"></i>
            <?= $this->title ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">Market Construction Cost </label> <?php echo ($old_market_construction_cost->url<>null) ? '<a href="'.$old_market_construction_cost->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="market_construction_cost">
                            <?php 
                                if($old_market_construction_cost->url<>null){?>
                                    <label class="custom-file-label text-dark text-bold"><?= substr($old_market_construction_cost->base_name,0,50) ?></label>
                                <?php } else{ ?>
                                    <label class="custom-file-label text-dark text-bold">Choose file</label>
                                <?php }
                            ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">Construction Cost Index UAE</label>  <?php echo ($old_construction_cost_index->url<>null) ? '<a href="'.$old_construction_cost_index->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="construction_cost_index">
                            <?php 
                                if($old_construction_cost_index->url<>null){?>
                                    <label class="custom-file-label text-dark text-bold"><?= substr($old_construction_cost_index->base_name,0,50) ?></label>
                                <?php } else{ ?>
                                    <label class="custom-file-label text-dark text-bold">Choose file</label>
                                <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">Construction Cost Index Dubai</label>  <?php echo ($old_construction_cost_index_dubai->url<>null) ? '<a href="'.$old_construction_cost_index_dubai->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="construction_cost_index_dubai">
                            <?php
                            if($old_construction_cost_index_dubai->url<>null){?>
                                <label class="custom-file-label text-dark text-bold"><?= substr($old_construction_cost_index_dubai->base_name,0,50) ?></label>
                            <?php } else{ ?>
                                <label class="custom-file-label text-dark text-bold">Choose file</label>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">Main Construction Cost Basis</label>  <?php echo ($old_main_construction_cost_basis->url<>null) ? '<a href="'.$old_main_construction_cost_basis->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="main_construction_cost_basis">
                            <?php
                            if($old_main_construction_cost_basis->url<>null){?>
                                <label class="custom-file-label text-dark text-bold"><?= substr($old_main_construction_cost_basis->base_name,0,50) ?></label>
                            <?php } else{ ?>
                                <label class="custom-file-label text-dark text-bold">Choose file</label>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">Main Construction Cost Analysis</label>  <?php echo ($old_main_construction_cost_analysis->url<>null) ? '<a href="'.$old_main_construction_cost_analysis->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="main_construction_cost_analysis">
                            <?php
                            if($old_main_construction_cost_analysis->url<>null){?>
                                <label class="custom-file-label text-dark text-bold"><?= substr($old_main_construction_cost_analysis->base_name,0,50) ?></label>
                            <?php } else{ ?>
                                <label class="custom-file-label text-dark text-bold">Choose file</label>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">Construction Cost Index Statistics</label>  <?php echo ($old_construction_cost_index_statistics->url<>null) ? '<a href="'.$old_construction_cost_index_statistics->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="construction_cost_index_statistics">
                            <?php
                            if($old_construction_cost_index_statistics->url<>null){?>
                                <label class="custom-file-label text-dark text-bold"><?= substr($old_construction_cost_index_statistics->base_name,0,50) ?></label>
                            <?php } else{ ?>
                                <label class="custom-file-label text-dark text-bold">Choose file</label>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">Uae Central Bank Eibor Rates</label>  <?php echo ($old_uae_central_bank_eibor_rates->url<>null) ? '<a href="'.$old_uae_central_bank_eibor_rates->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="uae_central_bank_eibor_rates">
                            <?php
                            if($old_uae_central_bank_eibor_rates->url<>null){?>
                                <label class="custom-file-label text-dark text-bold"><?= substr($old_uae_central_bank_eibor_rates->base_name,0,50) ?></label>
                            <?php } else{ ?>
                                <label class="custom-file-label text-dark text-bold">Choose file</label>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">Legal Fee Resources</label>  <?php echo ($old_legal_fee_resources->url<>null) ? '<a href="'.$old_legal_fee_resources->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="legal_fee_resources">
                            <?php
                            if($old_legal_fee_resources->url<>null){?>
                                <label class="custom-file-label text-dark text-bold"><?= substr($old_legal_fee_resources->base_name,0,50) ?></label>
                            <?php } else{ ?>
                                <label class="custom-file-label text-dark text-bold">Choose file</label>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">Soft Cost</label>  <?php echo ($old_interest_rate_and_financing_schedule->url<>null) ? '<a href="'.$old_interest_rate_and_financing_schedule->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="interest_rate_and_financing_schedule">
                            <?php
                            if($old_interest_rate_and_financing_schedule->url<>null){?>
                                <label class="custom-file-label text-dark text-bold"><?= substr($old_interest_rate_and_financing_schedule->base_name,0,50) ?></label>
                            <?php } else{ ?>
                                <label class="custom-file-label text-dark text-bold">Choose file</label>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">Signature Image</label>  <?php echo ($old_signature_image->url<>null) ? '<a href="'.$old_signature_image->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="signature_image">
                            <?php
                            if($old_signature_image->url<>null){?>
                                <label class="custom-file-label text-dark text-bold"><?= substr($old_signature_image->base_name,0,50) ?></label>
                            <?php } else{ ?>
                                <label class="custom-file-label text-dark text-bold">Choose file</label>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-footer bg-white text-center">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</section>