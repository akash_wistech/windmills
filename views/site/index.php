<?php

use yii\helpers\Url;
use app\models\Valuation;
use app\models\Buildings;
use app\models\Lead;
use  app\components\widgets\QuotationAllTimeWidgets;
use  app\components\widgets\QuotationTodayWidgets;
use  app\components\widgets\QuotationAllWidgets;
use  app\components\widgets\QuotationPendingWidgets;
use  app\components\widgets\QuotationTodayDynamicWidgets;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Dashboard');
?>
<?php
$quotation_status = [];

$startDate = date('Y-m-d') . ' 00:00:00';
$EndDate = date('Y-m-d') . ' 23:59:59';


$quotation_status['inquiry_recieved'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 0])
    ->andFilterWhere(['between', 'created_at', $startDate, $EndDate])
    ->asArray()
    ->all();

/*$valuation_over_all_status['overall'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 0])
    ->andFilterWhere(['between', 'created_at', $startDate, $EndDate])
    ->asArray()
    ->all();*/

$quotation_status['quotation_sent'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_fee_approved) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 1])
    ->andFilterWhere(['between', 'quotation_sent_date', $startDate, $EndDate])
    ->asArray()
    ->all();

$quotation_status['toe_sent'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 3])
    ->andFilterWhere(['between', 'toe_sent_date', $startDate, $EndDate])
    ->asArray()
    ->all();

$quotation_status['toe_Sign_and_Rec'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['toe_signed_and_received' => 1])
    ->andFilterWhere(['between', 'toe_signed_and_received_date', $startDate, $EndDate])
    ->asArray()
    ->all();

$quotation_status['payment_recieved'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 6])
    ->andFilterWhere(['between', 'payment_received_date', $startDate, $EndDate])
    ->asArray()
    ->all();

$quotation_status['on_hold'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 10])
    ->andFilterWhere(['between', 'on_hold_date', $startDate, $EndDate])
    ->asArray()
    ->all();


$quotation_status['quotation_rejected'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->orWhere(['and',
        ['quotation_status' => 7],
        ['between', 'quotation_rejected_date', $startDate, $EndDate]
    ])
    ->orWhere(['and',
        ['quotation_status' => 13],
        ['between', 'status_change_date', $startDate, $EndDate]
    ])
    ->asArray()
    ->all();


$quotation_status['toe_rejected'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->orWhere(['and',
        ['quotation_status' => 8],
        ['between', 'toe_rejected_date', $startDate, $EndDate]
    ])
    ->orWhere(['and',
        ['quotation_status' => 13],
        ['between', 'status_change_date', $startDate, $EndDate]
    ])
    ->asArray()
    ->all();


// echo "<pre>"; print_r($quotation_status['toe_rejected']); echo "</pre>"; die;

$quotation_status['cancelled'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 11])
    ->andFilterWhere(['between', 'cancelled_date', $startDate, $EndDate])
    ->asArray()
    ->all();

$quotation_status['regretted'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 12])
    ->andFilterWhere(['between', 'regretted_date', $startDate, $EndDate])
    ->asArray()
    ->all();


$quotation_status['closed'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 13])
    ->andFilterWhere(['between', 'status_change_date', $startDate, $EndDate])
    ->asArray()
    ->all();


$quotation_status['quotation_approved'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->orWhere(['and',
        ['quotation_status' => 6],
        ['between', 'payment_received_date', $startDate, $EndDate]
    ])
    ->orWhere(['and',
        ['toe_signed_and_received' => 1],
        ['between', 'toe_signed_and_received_date', $startDate, $EndDate]
    ])
    ->asArray()
    ->all();


$quotation_status['active_quotation'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->andFilterWhere(['not in', 'quotation_status', [6, 11, 12, 13]])
    ->andFilterWhere(['not in', 'toe_signed_and_received', 1])
    ->andFilterWhere(['between', 'status_change_date', $startDate, $EndDate])
    ->asArray()
    ->all();


// echo "<pre>"; print_r( $quotation_status['closed'] ); echo "</pre> <br><br>";die();

if(in_array(Yii::$app->user->identity->permission_group_id, [1,22]) ){



    $total_pending_signature=0;
    $date_from_last_month = date('Y-m-d', strtotime('first day of last month'));
    $date_to_last_month = date('Y-m-d', strtotime('last day of last month'));
    $totalResults_signatures = (new \yii\db\Query())
        ->select(' COUNT(valuation.id) as count')
        ->from('valuation')
        ->innerJoin('company', "company.id=valuation.client_id")
        ->where(['valuation.valuation_status' => [5]])
        ->andWhere(['company.print_report' => 1])
        ->andFilterWhere(['between', 'submission_approver_date', $date_from_last_month, $date_to_last_month])
        ->one();
    if(isset($totalResults_signatures['count']) && $totalResults_signatures['count'] > 0){
        $total_pending_signature = $totalResults_signatures['count'];
    }


    $first_five_days = Yii::$app->appHelperFunctions->getFirstFiveWorkingDays();
    $sixth_working_day = Yii::$app->appHelperFunctions->getNthWorkingDay(6);
    $notification=0;
    $last_date = date("Y-m-t");


    $date_now = date("Y-m-d"); // this format is string comparable
    if(in_array($date_now, $first_five_days) && $total_pending_signature > 0){
        $notification = 1;
    }

    if ($date_now >= $sixth_working_day && $date_now <= $last_date && $total_pending_signature > 0) {
        $notification = 2;
        if (Yii::$app->user->identity->signature_check == 0) {
          //  Yii::$app->db->createCommand()->update('user', ['status' => 2,'signature_check'=>1], 'id=' . 142 . '')->execute();
        }
    }

        if($notification == 1){
        echo '<div class="alert alert-warning"><strong>Warning!</strong> Please Upload all signatures Reports.</div>';
        }
    if($notification == 2){
       // echo '<div class="alert alert-danger"><strong>Your account has bee suspended due to pending signatures reports. </strong></div>';
       // die;
    }

}


?>

    <div class="row px-4">
        <?php
        $BarChartData = '';
        //number count for client, buildings, valuation steps.
        $totalValuation = (int)Valuation::find()->count('id');
        $totalValuation_fee = (int)Valuation::find()->sum('fee');
        $clients = (int)\app\models\Company::find()->count('id');
        $buildings = Buildings::find()->count('id');

        $totalValuation = (int)Valuation::find()->where(['valuation_status' => 5])->count('id');
        $totalValuation_fee = (int)Valuation::find()->where(['valuation_status' => 5])->sum('total_fee');


        //Valuation steps such as received, reviewed. approved etc.
        foreach (Yii::$app->helperFunctions->valuationStatusListArr as $key => $value) {
            $valuationReport[$key] = (int)Valuation::find()->where(['valuation_status' => $key])->count('id');
        }

        foreach (Yii::$app->helperFunctions->valuationStatusListArr as $key => $value) {
            $valuationReport_fee[$key] = (int)Valuation::find()->where(['valuation_status' => $key])->sum('fee');
        }
        $valuationReport[9] = (int)Valuation::find()->where(['valuation_status' => 9])->count('id');

        $valuationReport_today = (int)Valuation::find()->where(['valuation_status' => '1'])->andFilterWhere(['instruction_date' => date('Y-m-d')])->count('id');

        $total_sub_communities = (int)\app\models\Communities::find()->count('id');
        $total_communities = (int)\app\models\Communities::find()->where(['trashed' => '0'])->count('id');
        $total_developers = (int)\app\models\Developers::find()->where(['trashed' => '0'])->count('id');
        $total_Buildings = (int)\app\models\Buildings::find()->where(['trashed' => '0'])->count('id');

        //pending valuation overview
        $valuationReport_active = (int)Valuation::find()->andFilterWhere(['IN', 'valuation_status', [1, 2, 3, 4, 7, 8]])->andWhere(['valuation.parent_id' => null])->count('id');
        $valuationReport_active_fee = (float)Valuation::find()->andFilterWhere(['IN', 'valuation_status', [1, 2, 3, 4, 7, 8]])->andWhere(['valuation.parent_id' => null])->sum('fee');

        // pending valuation received
        $pending_val_count = (int)Valuation::find()->andFilterWhere(['IN', 'valuation_status', [1]])->andWhere(['valuation.parent_id' => null])->count('id');
        $pending_val_count_fee = (float)Valuation::find()->andFilterWhere(['IN', 'valuation_status', [1]])->andWhere(['valuation.parent_id' => null])->sum('fee');


        //  $valuationReport_today_receieved_overall = (int)Valuation::find()->where(['instruction_date' => date('Y-m-d')])->count('id');
        $valuationReport_today_receieved_overall = (int)Valuation::find()
            ->where(['parent_id' => null])
            ->andWhere(['not', ['client_id' => 9166]])
            ->andFilterWhere(['between', 'created_at', $startDate, $EndDate])->count('id');

        $valuationReport_today_receieved_fee_overall = (float)Valuation::find()
            ->where(['parent_id' => null])
            ->andWhere(['not', ['client_id' => 9166]])
            ->andFilterWhere(['between', 'created_at', $startDate, $EndDate])->sum('total_fee');

        $valuationReport_today_amendment_overall = (int)Valuation::find()
            ->where(['not', ['parent_id' => null]])
            ->orWhere(['IN' , 'client_id' ,'9166'])
            ->andFilterWhere(['between', 'created_at', $startDate, $EndDate])->count('id');

        $valuationReport_today_amendment_fee_overall = (float)Valuation::find()
            ->where(['not', ['parent_id' => null]])
            ->orWhere(['IN' , 'client_id' ,'9166'])
            ->andFilterWhere(['between', 'created_at', $startDate, $EndDate])->sum('total_fee');

        //  $valuationReport_today_receieved = (int)Valuation::find()->where(['instruction_date' => date('Y-m-d'), 'valuation_status' => 1])->count('id');
        $valuationReport_today_receieved = (int)Valuation::find()->where(['valuation_status' => 1])->andFilterWhere(['between', 'created_at', $startDate, $EndDate])->count('id');
        // $valuationReport_today_receieved_fee = (float)Valuation::find()->where(['instruction_date' => date('Y-m-d'), 'valuation_status' => 1])->sum('fee');
        $valuationReport_today_receieved_fee = (float)Valuation::find()->where(['valuation_status' => 1])->andFilterWhere(['between', 'created_at', $startDate, $EndDate])->sum('total_fee');
        $valuationReport_cancel_today = (int)Valuation::find()->where(['valuation_status' => '9'])->andFilterWhere(['instruction_date' => date('Y-m-d')])->count('id');
        $valuationReport_cancel_today_fee = (float)Valuation::find()->where(['valuation_status' => '9'])->andFilterWhere(['instruction_date' => date('Y-m-d')])->sum('fee');
        $valuationReport_challneged_today = (int)Valuation::find()->where(['revised_reason' => [1, 2]])->andFilterWhere(['instruction_date' => date('Y-m-d')])->count('id');
        $valuationReport_challneged_today = (float)Valuation::find()->where(['revised_reason' => [1, 2]])->andFilterWhere(['instruction_date' => date('Y-m-d')])->sum('fee');
        $valuationReport_challneged = (int)Valuation::find()->where(['revised_reason' => [1, 2]])->count('id');
        $valuationReport_challneged_fee = (float)Valuation::find()->where(['revised_reason' => [1, 2]])->sum('fee');

        $valuationReport_mistakes = (int)Valuation::find()->where(['revised_reason' => [3]])->count('id');
        $valuationReport_mistakes_fee = (float)Valuation::find()->where(['revised_reason' => [3]])->sum('fee');

        $valuationReport_active_challneged = (int)Valuation::find()->where(['revised_reason' => [1, 2]])->andFilterWhere(['IN', 'valuation_status', [1, 2, 3, 4, 7, 8]])->count('id');
        $valuationReport_active_challneged_fee = (float)Valuation::find()->where(['revised_reason' => [1, 2]])->andFilterWhere(['IN', 'valuation_status', [1, 2, 3, 4, 7, 8]])->sum('fee');

        $valuationReport_active_errors = (int)Valuation::find()->where(['revised_reason' => [3]])->andFilterWhere(['IN', 'valuation_status', [1, 2, 3, 4, 7, 8]])->count('id');
        $valuationReport_active_errors_fee = (float)Valuation::find()->where(['revised_reason' => [3]])->andFilterWhere(['IN', 'valuation_status', [1, 2, 3, 4, 7, 8]])->sum('fee');
        $valuationReport_today_inspected = \Yii::$app->db->createCommand('SELECT COUNT(valuation.id) as inspected
FROM valuation
INNER JOIN schedule_inspection ON valuation.id=schedule_inspection.valuation_id
WHERE schedule_inspection.inspection_date ="2021-11-23" AND
valuation.valuation_status NOT IN(6,9)')->execute();
        $totalResults = (new \yii\db\Query())
            ->select(' COUNT(valuation.id) as count,SUM(valuation.total_fee) as fee_sum')
            ->from('valuation')
            ->innerJoin('schedule_inspection', "schedule_inspection.valuation_id=valuation.id")
//->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
            ->where(['schedule_inspection.inspection_date' => date('Y-m-d')])
            ->andWhere(['valuation.valuation_status' => [1, 2, 3, 4, 5, 7, 8]])
            ->all();
        $valuationReport_today_inspected = $totalResults[0]['count'];
        $valuationReport_today_inspected_fee = $totalResults[0]['fee_sum'];


        $today_start_date = date('Y-m-d') . ' 00:00:00';
        $today_end_date = date('Y-m-d') . ' 23:59:59';

        $totalResults_inspect = (new \yii\db\Query())
            ->select('SUM(valuation.total_fee) as fee_sum')
            ->from('valuation')
            ->innerJoin('inspect_property', "inspect_property.valuation_id=valuation.id")
//->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
            ->andFilterWhere(['between', 'inspection_done_date', $today_start_date, $today_end_date])
            ->andWhere(['valuation.valuation_status' => [1, 2, 3, 4, 5, 7, 8]])
            ->all();

        $totalResults_approved = (new \yii\db\Query())
            ->select('SUM(valuation.total_fee) as fee_sum')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
//->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
            ->andFilterWhere(['between', 'valuation_approvers_data.created_at', $today_start_date, $today_end_date])
            ->andWhere(['valuation.valuation_status' => [1, 2, 3, 4, 5, 7, 8]])
            ->andWhere(['valuation_approvers_data.approver_type' => 'approver'])
            ->all();

        $totalResults_valuer = (new \yii\db\Query())
            ->select('COUNT(valuation.id) as count,SUM(valuation.total_fee) as fee_sum')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
//->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
            ->where(['valuation.parent_id' => null])
            ->andFilterWhere(['between', 'valuation_approvers_data.created_at', $today_start_date, $today_end_date])

            ->andWhere(['valuation_approvers_data.approver_type' => 'valuer'])
            ->all();

        $totalResults_valuer_active = (new \yii\db\Query())
            ->select('SUM(valuation.total_fee) as fee_sum')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            ->andWhere(['valuation.valuation_status' => [3]])
            ->andWhere(['valuation_approvers_data.approver_type' => 'valuer'])
            ->all();

        $totalResults_valuer_active_count = (new \yii\db\Query())
            ->select('COUNT(valuation.id) as pending_approval')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            ->andWhere(['valuation.valuation_status' => [3]])
            ->andWhere(['valuation_approvers_data.approver_type' => 'valuer'])
            ->all();


        //today recommended data
        $valuationReport_today_recommended = (new \yii\db\Query())
            ->select('COUNT(valuation.id) as count,SUM(valuation.total_fee) as fee_sum')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            ->where(['valuation.parent_id' => null])
            // ->andWhere(['valuation.valuation_status' => [3]])
            ->andFilterWhere(['between', 'valuation_approvers_data.created_at', $today_start_date, $today_end_date])
            ->andWhere(['valuation_approvers_data.approver_type' => 'valuer'])
            ->all();
        $valuationReport_today_recommended_fee = $valuationReport_today_recommended[0]['fee_sum'];
        $valuationReport_today_recommended = $valuationReport_today_recommended[0]['count'];

        //pending recommended data
        $valuationReport_pending_recommended = (new \yii\db\Query())
            ->select('COUNT(valuation.id) as count, SUM(valuation.total_fee) as fee_sum')
            ->from('valuation')
            ->leftJoin('valuation_approvers_data vad_reviewer', 'vad_reviewer.valuation_id = valuation.id AND vad_reviewer.approver_type = "reviewer"')
            ->innerJoin('valuation_approvers_data vad_valuer', 'vad_valuer.valuation_id = valuation.id AND vad_valuer.approver_type = "valuer"')
            ->where(['valuation.parent_id' => null])
            ->andWhere(['valuation.valuation_status' => 3])
            ->andWhere(['vad_reviewer.valuation_id' => null])
            ->all();
        // dd($valuationReport_pending_recommended);
        $valuationReport_pending_recommended_fee = $valuationReport_pending_recommended[0]['fee_sum'];
        $valuationReport_pending_recommended_count = $valuationReport_pending_recommended[0]['count'];

        //pending reviewed
        $valuationReport_today_reviewed = (new \yii\db\Query())
            ->select('COUNT(valuation.id) as count,SUM(valuation.total_fee) as fee_sum')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            ->where(['valuation.parent_id' => null])
            ->andWhere(['valuation.valuation_status' => [3]])
            ->andWhere(['valuation_approvers_data.approver_type' => 'reviewer'])
            ->all();
        $valuationReport_today_reviewed_fee = $valuationReport_today_reviewed['0']['fee_sum'];
        $valuationReport_today_reviewed = $valuationReport_today_reviewed['0']['count'];


        $valuationReport_today_inspected_done = (int)\app\models\InspectProperty::find()->andFilterWhere(['between', 'inspection_done_date', $today_start_date, $today_end_date])->count('id');
        $valuationReport_today_inspected_done_fee = $totalResults_inspect[0]['fee_sum'];


        //total pending inspected
        $valuationReport_inspection_pending_data = app\models\Valuation::find()
            ->select(['COUNT(valuation.id) AS record_count' , 'SUM(valuation.fee) AS total_fee'])
            ->leftJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            ->where(['valuation.parent_id' => null])
            ->andWhere(['valuation.valuation_status' => 3])
            ->andWhere(['valuation_approvers_data.valuation_id' => null])
            ->asArray()
            ->one();

        $valuationReport_inspection_pending = $valuationReport_inspection_pending_data['record_count'];
        $valuationReport_inspection_pending_fee = $valuationReport_inspection_pending_data['total_fee'];

        //total pending inspected
        $valuationReport_inspection_pending_partial_data = app\models\Valuation::find()
            ->select(['COUNT(valuation.id) AS record_count' , 'SUM(valuation.fee) AS total_fee'])
            ->leftJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            ->where(['valuation.parent_id' => null])
            ->andWhere(['valuation.valuation_status' => [13]])
            ->andWhere(['valuation_approvers_data.valuation_id' => null])
            ->asArray()
            ->one();

        $valuationReport_inspection_pending_partial = $valuationReport_inspection_pending_partial_data['record_count'];
        $valuationReport_inspection_pending_partial_fee = $valuationReport_inspection_pending_partial_data['total_fee'];

        //total pending inspected
        $valuationReport_inspection_pending_partial_data = app\models\Valuation::find()
            ->select(['COUNT(valuation.id) AS record_count' , 'SUM(valuation.fee) AS total_fee'])
            ->leftJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            ->where(['valuation.parent_id' => null])
            ->andWhere(['valuation.valuation_status' => [13]])
            ->andWhere(['valuation_approvers_data.valuation_id' => null])
            ->asArray()
            ->one();

        $valuationReport_inspection_pending_partial = $valuationReport_inspection_pending_partial_data['record_count'];
        $valuationReport_inspection_pending_partial_fee = $valuationReport_inspection_pending_partial_data['total_fee'];

        // echo "<pre>";
        // print_r($valuationReport_inspection_pending_data);
        // echo "</pre>";die;

        $valuationReport_today_approved = (int)\app\models\ValuationApproversData::find()->where(['approver_type' => 'approver'])->andFilterWhere(['between', 'created_at', $today_start_date, $today_end_date])->count('id');
        $valuationReport_today_approved_fee = $totalResults_approved[0]['fee_sum'];
        //$valuationReport_today_recommended = (int)\app\models\ValuationApproversData::find()->where(['approver_type' => 'valuer'])->andFilterWhere(['between', 'created_at', $today_start_date, $today_end_date])->count('id');


        $valuationReport_active_recommended = $totalResults_valuer_active_count[0]['pending_approval'];
        $valuationReport_active_recommended_fee = $totalResults_valuer_active[0]['fee_sum'];

        $valuationReport_challneged_today = (int)Valuation::find()->where(['revised_reason' => [1, 2]])->andFilterWhere(['between', 'created_at', $today_start_date, $today_end_date])->count('id');

        $valuationReport_errors_today = (int)Valuation::find()->where(['revised_reason' => [3]])->andFilterWhere(['between', 'created_at', $today_start_date, $today_end_date])->count('id');
        $valuationReport_errors_today_fee = (int)Valuation::find()->where(['revised_reason' => [3]])->andFilterWhere(['between', 'created_at', $today_start_date, $today_end_date])->sum('total_fee');
        $valuationReport_challneged_today_fee = (int)Valuation::find()->where(['revised_reason' => [1, 2]])->andFilterWhere(['between', 'created_at', $today_start_date, $today_end_date])->sum('total_fee');

        if (Yii::$app->user->identity->permission_group_id == 15) {
            foreach (Yii::$app->helperFunctions->valuationStatusListArr as $key => $value) {
                $valuationReport[$key] = (int)Valuation::find()->where(['valuation_status' => $key])->andWhere(['client_id' => Yii::$app->user->identity->company_id])->count('id');
                $totalValuation = (int)Valuation::find()->where(['client_id' => Yii::$app->user->identity->company_id])->count('id');
            }
        }


        // Build the query using ActiveRecord
        $today_inspections = app\models\ScheduleInspection::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->innerJoin('valuation', 'schedule_inspection.valuation_id = valuation.id')
            // ->where(['DATE(inspection_date)' => date('Y-m-d')])
            ->where(['valuation.parent_id' => null])
            ->andFilterWhere(['between', 'schedule_inspection.created_at', $today_start_date, $today_end_date]);
        // ->andWhere(['valuation.valuation_status' => 2]);
        if (Yii::$app->user->identity->permission_group_id == 9) {
            //  $today_inspections->andWhere(['inspection_officer' => Yii::$app->user->id ]);
        }

        //total inspections
        $total_inspections_scheduled = app\models\ScheduleInspection::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->innerJoin('valuation', 'schedule_inspection.valuation_id = valuation.id')
            ->where(['valuation.parent_id' => null])
            ->andWhere(['valuation.valuation_status' => 2])
            ->asArray()
            ->one();

        $total_schedule_insp = $total_inspections_scheduled['record_count'];
        $total_schedule_insp_fee = $total_inspections_scheduled['total_fee'];

        $today_inspections->asArray();
        $today_inspections = $today_inspections->one();
        $today_total_schedule_insp = $today_inspections['record_count'];
        $today_total_schedule_insp_fee = $today_inspections['total_fee'];

        // Build the query using ActiveRecord
        $pending_inspections = app\models\ScheduleInspection::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->innerJoin('valuation', 'schedule_inspection.valuation_id = valuation.id')
            ->where(['valuation.parent_id' => null])
            ->andWhere(['valuation.valuation_status' => 2]);
        if (Yii::$app->user->identity->permission_group_id == 9) {
            //  $pending_inspections->andWhere(['inspection_officer' => Yii::$app->user->id ]);
        }
        $pending_inspections->asArray();
        $pending_inspections = $pending_inspections->one();
        $pending_total_schedule_insp = $pending_inspections['record_count'];
        $pending_total_schedule_insp_fee = $pending_inspections['total_fee'];

        //pending inspections scheduled
        $pending_insp_count = (int)Valuation::find()->andFilterWhere(['valuation_status' => 2])->andWhere(['valuation.parent_id' => null])->count('id');
        $pending_insp_count_fee = (float)Valuation::find()->andFilterWhere(['valuation_status' => 2])->andWhere(['valuation.parent_id' => null])->sum('fee');


        $total_today_reminders = Valuation::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->innerJoin(\app\models\VoneValuations::tableName(), 'valuation.id = vone_valuations.valuation_id')
            ->where(['vone_valuations.reason' => 8])
            ->andFilterWhere(['between', 'vone_valuations.created_at', $today_start_date, $today_end_date])
            ->asArray()->one();

        $total_pending_reminders = Valuation::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->innerJoin(\app\models\VoneValuations::tableName(), 'valuation.id = vone_valuations.valuation_id')
            ->where(['vone_valuations.reason' => 8, 'vone_valuations.error_status' => 'pending'])
            ->asArray()->one();

        $total_all_reminders = Valuation::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->innerJoin(\app\models\VoneValuations::tableName(), 'valuation.id = vone_valuations.valuation_id')
            ->where(['vone_valuations.reason' => 8])
            ->asArray()->one();


        /*   $total_today_doc_requested = Valuation::find()
               ->select(['COUNT(*) AS record_count', 'SUM(valuation.total_fee) AS total_fee'])
               ->where(['valuation.valuation_status' => 7])
               ->andFilterWhere(['like', 'valuation.updated_at', date("Y-m-d")])
               ->asArray()->one();*/

        $total_today_doc_requested = \app\models\ReceivedDocs::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->innerJoin('valuation', 'valuation.id = received_docs.valuation_id')
            ->andWhere(['valuation.parent_id' => null])
            ->where(['valuation.valuation_status' => 7])
            ->andFilterWhere(['between', 'received_docs.created_at', $today_start_date, $today_end_date])
            ->asArray()->one();

        $total_doc_requested = \app\models\ReceivedDocs::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->innerJoin('valuation', 'valuation.id = received_docs.valuation_id')
            ->andWhere(['valuation.parent_id' => null])
            ->where(['valuation.valuation_status' => 7])
            ->asArray()->one();


        $total_cancelled = Valuation::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->where(['valuation.valuation_status' => 9])
            ->asArray()->one();

        $total_approved = \app\models\ValuationApproversData::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->innerJoin('valuation', 'valuation.id = valuation_approvers_data.valuation_id')
            ->where(['valuation_approvers_data.approver_type' => 'approver'])
            ->andWhere(['valuation.parent_id' => null])
            ->andFilterWhere(['between', 'valuation_approvers_data.created_at', $today_start_date, $today_end_date])
            ->asArray()->one();

        $today_approved_data = \app\models\Valuation::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            ->where(['valuation_approvers_data.approver_type' => 'approver'])
            ->andWhere(['valuation.parent_id' => null])
            ->andWhere(['valuation.valuation_status' => 5])
            ->andFilterWhere(['between', 'valuation.submission_approver_date', $today_start_date, $today_end_date])
            ->asArray()->one();

        $overall_tat = \app\models\ValuationApproversData::find()
            ->select(['COUNT(*) AS record_count', 'SUM(valuation.fee) AS total_fee'])
            ->innerJoin('valuation', 'valuation.id = valuation_approvers_data.valuation_id')
            ->where(['valuation_approvers_data.approver_type' => 'approver'])
            ->andWhere(['valuation.parent_id' => null])
            ->asArray()->one();

        // echo "<pre>";
        // print_r($total_inspections_scheduled);
        // echo "<pre>";
        // die();

        // Quotation queries
        $quotation_status = [];

        $startDate = date('Y-m-d') . ' 00:00:00';
        $EndDate = date('Y-m-d') . ' 23:59:59';
        $quotation_status['inquiry_recieved'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->andFilterWhere(['between', 'created_at', $startDate, $EndDate])
            ->andFilterWhere(['in', 'quotation_status', [0, 1, 3, 6, 10, 7, 13, 8, 11, 12]])
            ->asArray()
            ->all();


        $quotation_status['quotation_sent'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->andFilterWhere(['between', 'quotation_sent_date', $startDate, $EndDate])
            ->andFilterWhere(['in', 'quotation_status', [0, 1, 3, 6, 10, 7, 13, 8, 11, 12]])
            ->asArray()
            ->all();

        // dd($quotation_status);


        $quotation_status['toe_sent'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->andFilterWhere(['between', 'toe_sent_date', $startDate, $EndDate])
            ->andFilterWhere(['in', 'quotation_status', [0, 1, 3, 6, 10, 7, 13, 8, 11, 12]])
            ->asArray()
            ->all();

        $quotation_status['toe_Sign_and_Rec'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->where(['toe_signed_and_received' => 1])
            ->andFilterWhere(['between', 'toe_signed_and_received_date', $startDate, $EndDate])
            ->asArray()
            ->all();

        $quotation_status['payment_recieved'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->andFilterWhere(['between', 'payment_received_date', $startDate, $EndDate])
            ->andFilterWhere(['in', 'quotation_status', [0, 1, 3, 6, 10, 7, 13, 8, 11, 12]])
            ->asArray()
            ->all();

        $quotation_status['on_hold'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->andFilterWhere(['between', 'on_hold_date', $startDate, $EndDate])
            ->andFilterWhere(['in', 'quotation_status', [0, 1, 3, 6, 10, 7, 13, 8, 11, 12]])
            ->asArray()
            ->all();


        $quotation_status['quotation_rejected'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->orWhere(['and',
                ['quotation_status' => [7]],
                ['between', 'quotation_rejected_date', $startDate, $EndDate]
            ])
            ->orWhere(['and',
                ['quotation_status' => [13]],
                ['between', 'status_change_date', $startDate, $EndDate]
            ])
            ->asArray()
            ->all();


        $quotation_status['toe_rejected'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->orWhere(['and',
                ['quotation_status' => [8]],
                ['between', 'toe_rejected_date', $startDate, $EndDate]
            ])
            ->orWhere(['and',
                ['quotation_status' => [13]],
                ['between', 'status_change_date', $startDate, $EndDate]
            ])
            ->asArray()
            ->all();


        // echo "<pre>"; print_r($quotation_status['toe_rejected']); echo "</pre>"; die;

        $quotation_status['cancelled'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->andFilterWhere(['between', 'cancelled_date', $startDate, $EndDate])
            ->andFilterWhere(['in', 'quotation_status', [11]])
            ->asArray()
            ->all();

        $quotation_status['regretted'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->andFilterWhere(['between', 'regretted_date', $startDate, $EndDate])
            ->andFilterWhere(['in', 'quotation_status', [12]])
            ->asArray()
            ->all();


        $quotation_status['closed'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->andFilterWhere(['between', 'status_change_date', $startDate, $EndDate])
            ->andFilterWhere(['in', 'quotation_status', [13]])
            ->asArray()
            ->all();


        $quotation_status['quotation_approved'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->orWhere(['and',
                ['quotation_status' => [0, 1, 3, 6, 10, 7, 13, 8, 11, 12]],
                ['between', 'payment_received_date', $startDate, $EndDate]
            ])
            ->orWhere(['and',
                ['toe_signed_and_received' => 1],
                ['between', 'toe_signed_and_received_date', $startDate, $EndDate]
            ])
            ->asArray()
            ->all();


        $quotation_status['active_quotation'] = \app\models\CrmQuotations::find()
            ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
            ->andFilterWhere(['not in', 'quotation_status', [6, 11, 12, 13]])
            ->andFilterWhere(['not in', 'toe_signed_and_received', 1])
            ->andFilterWhere(['between', 'status_change_date', $startDate, $EndDate])
            ->asArray()
            ->all();

        ?>
    </div>

    <style>

        .card-header {
            padding: 10px !important;
            padding-bottom: 20px !important;
        }

        .card-footer {
            padding: 0px !important;
        }

        .card-footer span {
            font-size: 13.5px !important;
        }

        .font-increase {
            font-size: 15px;
        }

        .footer-increase {
            font-weight: bold;
        }

        /* .button2:hover {
        box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
        } */

        .button {
            background-color: white;
            color: #757575;
            padding: 44px 0px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 17px;
            font-weight: bold;
            margin: 4px 2px;
            cursor: pointer;
            -webkit-transition-duration: 0.4s; /* Safari */
            transition-duration: 0.4s;
            border: none;
            outline: 0 !important;
            width: inherit;
        }

        .t-color {
            color: rgb(1,123,254);
        }

        .t-color-2 {
            color: #04A96D;
        }

        .t-color-3 {
            color: #757575;;
        }

        .t-color-4 {
            color: #FF7518;
        }

        h4{
            font-size: 16px !important;
            font-weight: bold;
            font-family: sans-serif !important;
            width: fit-content;
        }

        .col-10{
            width: auto;
        }

        .content, .content-wrapper{
            background-color: #E5E4E2;
        }

        .today-text-info {
            color: #017BFD !important;
        }

        .pending-text-info {
            color: #08A86D !important;
        }

    </style>







<?php

if( Yii::$app->user->identity->location_status == 0){ ?>

    <?php
    if (Yii::$app->user->identity->permission_group_id == 9 || Yii::$app->user->id == 15 || Yii::$app->user->id == 142) {
        ?>

        <div class="row">
            <div class="col-lg-4">
                <div class="container">
                    <div class="row mt-4 mb-4">
                        <div class="text-center">
                            <button type="button" class="button info" data-toggle="modal" data-target=".bd-example-modal-xl-0">
                                <center>
                                    <h4 class="card-category">
                                        <span style="padding-left: 40px; padding-right: 40px;" class="t-color"> Today Valuation Performances </span>
                                    </h4>
                        </div>
                        </center>
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="container">
                    <div class="row mt-4 mb-4">
                        <div class="text-center">
                            <button type="button" class="button info" data-toggle="modal" data-target=".bd-example-modal-xl-02">

                                <center>
                                    <h4 class="card-category">
                                        <span style="padding-left: 40px; padding-right: 40px;" class="t-color"> Pending Valuation Performance </span>
                                    </h4>
                                </center>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-xl-0" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> Today Valuation Performance </h5>
                        <button type="button btn info" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="card-body" style="background: #E5E4E2">
                        <div class="row px-4">
                            <?= $this->render('today_inspected_widget', [
                                'today_total_schedule_insp' => $today_total_schedule_insp,
                                'today_total_schedule_insp_fee' => $today_total_schedule_insp_fee,
                            ]) ?>

                            <?php
                            if(in_array(Yii::$app->user->identity->permission_group_id, [40,3,9])) {
                                $totalResults_today_individual_all = (new \yii\db\Query())
                                    ->select(' COUNT(valuation.id) as count,SUM(valuation.total_fee) as fee_sum')
                                    ->from('schedule_inspection')
                                    ->innerJoin('valuation', "schedule_inspection.valuation_id=valuation.id")
                                    ->where(['schedule_inspection.inspection_date' => date('Y-m-d')])
                                    ->andWhere(['valuation.valuation_status' => [2]])
                                    ->andWhere(['schedule_inspection.inspection_officer' => Yii::$app->user->identity->id])
                                    ->andWhere(['valuation.parent_id' => null])
                                    ->all();
                                ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;" >
                                                <h4  class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Over All My Pending Inspections</span><br><?= $totalResults_today_individual_all[0]['count'] ?></h4 >
                                                <h4 class=" float-right font-increase" style="color:#757575" >

                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;color:#C0CA33;" href="<?= yii\helpers\Url::to(['valuation/inspections', 'ValuationSearch[target]' => 'from_dashboard', 'ValuationSearch[page_title]' => 'Pending Individual Inspection Scheduled'])?>"> <span style="font-size:15px;">Go to Pending Inspections Scheduled</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        </div>

        <div class="modal fade bd-example-modal-xl-02" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> Pending Valuation Performance </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="card-body" style="background: #E5E4E2">
                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                            <div id="w0" class="card-stats">
                                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                    </div>
                                    <div class=" col-10 position-absolute" style="right:0px;" >
                                        <h4  class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Pending Inspection Scheduled </span><br><?= $pending_total_schedule_insp ?></h4 >
                                        <h4 class=" float-right font-increase" style="color:#757575" >
                                            <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                                <?= number_format($pending_total_schedule_insp_fee) ?>
                                            <?php } ?>
                                        </h4>
                                    </div>
                                </div>
                                <div class="card-footer bg-white">
                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;color:#C0CA33;" href="<?= yii\helpers\Url::to(['valuation/index', 'ValuationSearch[target]' => 'from_dashboard', 'ValuationSearch[page_title]' => 'Pending Inspection Scheduled'])?>"> <span style="font-size:15px;">Go to Pending Inspection Scheduled</span> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
    ?>

    <?php if(in_array(Yii::$app->user->identity->permission_group_id, [40,3,9])) { ?>

        <div class="row">
            <div class="col-lg-4">
                <div class="container">
                    <div class="row mt-4 mb-4">
                        <div class="text-center">
                            <button type="button" class="button info" data-toggle="modal" data-target=".bd-example-modal-xl-01">
                                <center>
                                    <h4 class="card-category">
                                        <span style="padding-left: 40px; padding-right: 40px;" class="t-color"> Today Valuation Performance </span>
                                    </h4>
                        </div>
                        </center>
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="container">
                    <div class="row mt-4 mb-4">
                        <div class="text-center">
                            <button type="button" class="button info" data-toggle="modal" data-target=".bd-example-modal-xl-02">

                                <center>
                                    <h4 class="card-category">
                                        <span style="padding-left: 40px; padding-right: 40px;" class="t-color"> Pending Valuation Performance </span>
                                    </h4>
                                </center>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-xl-01" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> Today Valuation Performance </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="card-body" style="background: #E5E4E2">
                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                            <div id="w0" class="card-stats">
                                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                        </div>
                                    </div>
                                    <div class=" col-10 position-absolute" style="right:0px;">
                                        <h4 class="card-category font-increase text-right" style="color:#757575">Today TAT Performance
                                            <br><?= $total_approved['record_count'] ?></h4>
                                        <h4 class=" float-right font-increase" style="color:#757575">
                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                <?= number_format( $total_approved['total_fee']) ?>
                                            <?php } ?>
                                        </h4>
                                    </div>
                                </div>
                                <div class="card-footer bg-white footer-increase">
                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                        <i class="material-icons today-today-text-info pt-1 pl-3" style="font-size:30px">description</i>
                                        <a class="position-absolute today-today-text-info" style="left:53px;top:5px;"
                                           href="<?= yii\helpers\Url::to(['reports/tat-reports-today']) ?>"> <span
                                                    style="font-size:15px;">Go to Today TAT</span> </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                        if(in_array(Yii::$app->user->identity->permission_group_id, [40,3,9])) {
                            $totalResults_today_individual_all = (new \yii\db\Query())
                                ->select(' COUNT(valuation.id) as count,SUM(valuation.total_fee) as fee_sum')
                                ->from('schedule_inspection')
                                ->innerJoin('valuation', "schedule_inspection.valuation_id=valuation.id")
                                ->where(['schedule_inspection.inspection_date' => date('Y-m-d')])
                                ->andWhere(['valuation.valuation_status' => [2]])
                                ->andWhere(['schedule_inspection.inspection_officer' => Yii::$app->user->identity->id])
                                ->andWhere(['valuation.parent_id' => null])
                                ->all();
                            ?>
                            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                <div id="w0" class="card-stats">
                                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                        <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                            <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                        </div>
                                        <div class=" col-10 position-absolute" style="right:0px;" >
                                            <h4  class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Over All My Pending Inspections</span><br><?= $totalResults_today_individual_all[0]['count'] ?></h4 >
                                            <h4 class=" float-right font-increase" style="color:#757575" >

                                            </h4>
                                        </div>
                                    </div>
                                    <div class="card-footer bg-white">
                                        <div class="stats position-relative" style="box-sizing: border-box;">
                                            <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                            <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;color:#C0CA33;" href="<?= yii\helpers\Url::to(['valuation/inspections', 'ValuationSearch[target]' => 'from_dashboard', 'ValuationSearch[page_title]' => 'Pending Individual Inspection Scheduled'])?>"> <span style="font-size:15px;">Go to Pending Inspections Scheduled</span> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-xl-02" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> Pending Valuation Performance </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="card-body" style="background: #E5E4E2">
                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                            <div id="w0" class="card-stats">
                                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                    </div>
                                    <div class=" col-10 position-absolute" style="right:0px;" >
                                        <h4  class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Pending Inspection Scheduled </span><br><?= $pending_total_schedule_insp ?></h4 >
                                        <h4 class=" float-right font-increase" style="color:#757575" >
                                            <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                                <?= number_format($pending_total_schedule_insp_fee) ?>
                                            <?php } ?>
                                        </h4>
                                    </div>
                                </div>
                                <div class="card-footer bg-white">
                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;color:#C0CA33;" href="<?= yii\helpers\Url::to(['valuation/index', 'ValuationSearch[target]' => 'from_dashboard', 'ValuationSearch[page_title]' => 'Pending Inspection Scheduled'])?>"> <span style="font-size:15px;">Go to Pending Inspection Scheduled</span> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php } ?>

    <div class="container">
        <div class="row">
            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_dashboard')) { ?>

                <div class="col-lg-4 text-center mt-4">
                    <button type="button" class="button info" data-toggle="modal" data-target=".bd-example-modal-xl">

                        <center>
                            <div class=" col-12">
                                <h4 class="card-category">
                                    <span class="t-color"> Today Valuation Performance </span>
                                    <br>
                                    <span style="float:left"><?= $valuationReport_today_receieved_overall ?> </span>
                                    <span style="float:right"><?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                            <?= number_format($valuationReport_today_receieved_fee_overall) ?>
                                        <?php } ?> </span>
                                </h4>
                            </div>
                        </center>
                    </button>
                </div>

                <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="modal-title"> Today Valuation Performance </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="card-body" style="background: #E5E4E2">
                                <div class="row px-4">

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_performance_overview')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Today Performance Overview </span>
                                                            <br><?= $valuationReport_today_receieved_overall ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($valuationReport_today_receieved_fee_overall) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/dashboard-report-all', 'ValuationSearch[created_at]' => date('Y-m-d'), 'ValuationSearch[parent_target]' => 'today_total_performance', 'ValuationSearch[target]' => 'instruction_date', 'ValuationSearch[page_title]' => 'Today Performance Overview']) ?>">
                                                            <span style="font-size: 14px !important;">Go to Today Performance Overview</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_amendment_overview')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Today Amendment Overview </span>
                                                            <br><?= $valuationReport_today_amendment_overall ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($valuationReport_today_amendment_fee_overall) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/dashboard-report-all', 'ValuationSearch[created_at]' => date('Y-m-d'), 'ValuationSearch[parent_target]' => 'today_amendment_overview', 'ValuationSearch[target]' => 'instruction_date', 'ValuationSearch[page_title]' => 'Today Performance Overview']) ?>">
                                                            <span style="font-size: 14px !important;">Go to Today Amendment Overview</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_inspection_scheduled')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;" >
                                                        <h4  class="card-category font-increase text-right" style="color:#757575"> <span class="today-text-info"> Inspection Scheduled </span>  <br><?= $today_total_schedule_insp ?></h4 >
                                                        <h4 class=" float-right font-increase" style="color:#757575" >
                                                            <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                                                                <?= number_format($today_total_schedule_insp_fee)?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/valuations-to-inspect?time=today'])?>"> <span style="font-size:14px !important;">Go to Today for Inspected</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_inspected')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Today Inspected </span>
                                                            <br><?= $valuationReport_today_inspected_done ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($valuationReport_today_inspected_done_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/dashboard-report-all', 'ValuationSearch[inspection_done_date]' => date('Y-m-d'), 'ValuationSearch[target]' => 'inspection_done_date', 'ValuationSearch[page_title]' => 'Today Inspected']) ?>">
                                                            <span style="font-size:14px !important;">Go to Inspected</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_recommended')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Today Recommended </span>
                                                            <br><?= $valuationReport_today_recommended ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($valuationReport_today_recommended_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/recommended-today-list']) ?>"> <span
                                                                    style="font-size:15px;">Go to Recommended</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_approved')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Today Approved </span>
                                                            <br><?= $today_approved_data['record_count'] ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format( $today_approved_data['total_fee']) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/approved-today-list']) ?>"> <span
                                                                    style="font-size:15px;">Go to Approved</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_tat_performance')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Today TAT Performance </span>
                                                            <br><?= $today_approved_data['record_count'] ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format( $today_approved_data['total_fee']) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/tat-reports-today']) ?>"> <span
                                                                    style="font-size:15px;">Go to Today TAT</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_challenged')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:5px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">&nbsp;
                                                            <span class="today-text-info"> Today Challenged </span> <br><?= $valuationReport_challneged_today ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>

                                                                <?= number_format($valuationReport_challneged_today_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/dashboard-report-all', 'ValuationSearch[revised_reason]' => [1, 2], 'ValuationSearch[challenge]' => date('Y-m-d'), 'ValuationSearch[target]' => 'today_challenged', 'ValuationSearch[page_title]' => 'Today Challenged']) ?>">
                                                            <span style="font-size:15px;">Go to Challenged</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_mistakes')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:5px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">&nbsp;
                                                            <span class="today-text-info"> Today Mistakes </span>
                                                            <br><?= $valuationReport_errors_today ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>

                                                                <?= number_format($valuationReport_errors_today_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/dashboard-report-all', 'ValuationSearch[revised_reason]' => [3], 'ValuationSearch[challenge]' => date('Y-m-d'), 'ValuationSearch[target]' => 'today_mistakes', 'ValuationSearch[page_title]' => 'Today Mistakes']) ?>">
                                                            <span style="font-size:15px;">Go to Mistakes</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_reminders')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:5px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">&nbsp;
                                                            <span class="today-text-info"> Today Reminders </span>
                                                            <br> <?= $total_today_reminders['record_count'] ?> </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33])) { ?>
                                                                <?= number_format($total_today_reminders['total_fee']) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/dashboard-report-all', 'ValuationSearch[reminder_id]' => [8], 'ValuationSearch[target]' => 'today_reminders', 'ValuationSearch[page_title]' => 'Today Reminders']) ?>">
                                                            <span style="font-size:15px;">Go to Today Reminders</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_ahead')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Ahead of Time </span>
                                                            <br><?= $query_ahead_of_time_count ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px; color:#6A1B9A;"
                                                           href="<?= yii\helpers\Url::toRoute(['reports/tat-ahead-reports']); ?>"> <span
                                                                    style="font-size:15px;">Go to Ahead of Time</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_ontime')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> On Time </span>
                                                            <br><?= $query_on_time_count ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px; color:#6A1B9A;"
                                                           href="<?= yii\helpers\Url::toRoute(['reports/tat-on-time-reports']); ?>"> <span
                                                                    style="font-size:15px;">Go to On Time</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_delay')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <div class="card-icon" style="font-size:15px; text-align:center">
                                                            <?= date('d'); ?> <br> <?= date('M'); ?>
                                                        </div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Delay </span>
                                                            <br><?= $query_delay_count ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info text-success" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/tat-delay-reports']) ?>"> <span
                                                                    style="font-size:15px;">Go to Delay </span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            <?php } ?>

            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('active_dashboard')) { ?>

                <div class="col-lg-4 text-center mt-4">
                    <button type="button" class="button info" data-toggle="modal" data-target=".bd-example-modal-xl-1">
                        <center>
                            <div class=" col-12">
                                <h4 class="card-category">
                                    <span class="t-color"> Pending Valuation Performance </span>
                                    <br />
                                    <span style="float:left"><?= $valuationReport_active ?> </span>
                                    <span style="float:right">
                                    <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                        <?= number_format($valuationReport_active_fee) ?>
                                    <?php } ?>
                            </span>
                                </h4>
                            </div>
                        </center>
                    </button>
                </div>

                <div class="modal fade bd-example-modal-xl-1" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"> Pending Valuation Performance </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="card-body" style="background: #E5E4E2">
                                <div class="row px-4">

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_received_overview')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">

                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Pending Received Overview </span>
                                                            <br><?= $valuationReport_active ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($valuationReport_active_fee) ?>
                                                            <?php } ?>
                                                        </h4>

                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/index', 'ValuationSearch[time_period]' => '0', 'ValuationSearch[target]' => 'from_dashboard', 'ValuationSearch[page_title]' => 'Pending Valuation Performance'] ) ?>">
                                                            <span style="font-size:15px;">Go to Pending Received Overview</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_valuation_received')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">

                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info">Pending Valuation Received </span>
                                                            <br><?= $pending_val_count ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($pending_val_count_fee) ?>
                                                            <?php } ?>
                                                        </h4>

                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/index', 'ValuationSearch[target]' => 'from_dashboard', 'ValuationSearch[page_title]' => 'Pending Valuation Received'] ) ?>">
                                                            <span style="font-size:15px;">Go to Pending Valuation Received</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_inspection_scheduled')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;" >
                                                        <h4  class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Pending Inspection Scheduled </span><br><?= $pending_insp_count ?></h4 >
                                                        <h4 class=" float-right font-increase" style="color:#757575" >
                                                            <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                                                <?= number_format($pending_insp_count_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;color:#C0CA33;" href="<?= yii\helpers\Url::to(['valuation/index', 'ValuationSearch[target]' => 'from_dashboard', 'ValuationSearch[page_title]' => 'Pending Inspection Scheduled'])?>"> <span style="font-size:15px;">Go to Pending Inspection Scheduled</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_documents_requested')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Pending Documents Requested </span>
                                                            <br><?= $valuationReport[7] ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($valuationReport_fee[7]) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info text-success" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/dashboard-report-all', 'ValuationSearch[valuation_status]' => 7, 'ValuationSearch[today_doc]' => 'pending_doc', 'ValuationSearch[target]' => 'pedning_doc_requested', 'ValuationSearch[page_title]' => 'Pending Documents Requested']); ?>">
                                                            <span style="font-size:15px;">Go to Pending Documents Requested</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_inspected')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Pending Inspected </span>
                                                            <br><?= $valuationReport_inspection_pending ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($valuationReport_inspection_pending_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/dashboard-report-all', 'ValuationSearch[page_title]' => 'Pending Inspected']) ?>">
                                                            <span style="font-size:15px;">Go to Pending Inspected</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Partial Inspected </span>
                                                            <br><?= $valuationReport_inspection_pending_partial ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($valuationReport_inspection_pending_partial_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/dashboard-report-all', 'ValuationSearch[page_title]' => 'Partial Inspected']) ?>">
                                                            <span style="font-size:15px;">Go to Partial Inspected</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_recommended')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Pending Recommended </span>
                                                            <br><?= $valuationReport_pending_recommended_count ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($valuationReport_pending_recommended_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/recommended-pending-list']) ?>"> <span
                                                                    style="font-size:15px;">Go to Pending Recommended</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_reviewed')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Pending Reviewed </span>
                                                            <br><?= $valuationReport_today_reviewed ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($valuationReport_today_reviewed_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/reviewed-pending-list']) ?>"> <span
                                                                    style="font-size:15px;">Go to Pending Reviewed</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_tat_performance')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Pending TAT Performance </span>
                                                            <br><?= $valuationReport_active ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($valuationReport_active_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/tat-reports-pending?time=pending']) ?>"> <span
                                                                    style="font-size:15px;">Go to Pending TAT Performance</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_delayed')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:5px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">&nbsp;
                                                            <span class="today-text-info"> Pending Delayed </span> <br> <?= $total_pending_reminders['record_count'] ?> </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33])) { ?>
                                                                <?= number_format($total_pending_reminders['total_fee']) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-today-text-info footer-increase" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/pending-delay-reports?time=pending']) ?>">
                                                            <span style="font-size:15px;">Go to Pending Delayed</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_reminders')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:5px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">&nbsp;
                                                            <span class="today-text-info"> Pending Reminders </span>
                                                            <br> <?= $total_pending_reminders['record_count'] ?> </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33])) { ?>
                                                                <?= number_format($total_pending_reminders['total_fee']) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-today-text-info footer-increase" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/dashboard-report-all', 'ValuationSearch[reminder_id]' => [8], 'ValuationSearch[target]' => 'pending_reminders', 'ValuationSearch[page_title]' => 'Pending Reminders']) ?>">
                                                            <span style="font-size:15px;">Go to Pending Reminders</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="col-lg-4 col-md-6 col-sm-6 py-3 d-none">
                                        <div id="w0" class="card-stats">
                                            <div
                                                    class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                                </div>
                                                <div class=" col-10 position-absolute" style="right:0px;">
                                                    <h4 class="card-category font-increase text-right" style="color:#757575">
                                                        <span class="today-text-info"> Pending Approvals </span>
                                                        <br><?= $valuationReport_active_recommended ?></h4>
                                                    <h4 class=" float-right font-increase" style="color:#757575">
                                                        <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                            <?= number_format($valuationReport_active_recommended_fee) ?>
                                                        <?php } ?>
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="card-footer bg-white footer-increase">
                                                <div class="stats position-relative" style="box-sizing: border-box;">
                                                    <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                    <a class="position-absolute" style="left:53px;top:5px;color:#FFA000;"
                                                       href="<?= yii\helpers\Url::to(['valuation/recommended-active-list']) ?>"> <span
                                                                style="font-size:15px;">Go to Pending Approvals</span> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php } ?>

            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_dashboard')) { ?>

                <div class="col-lg-4 text-center mt-4">
                    <button type="button" class="button info" data-toggle="modal" data-target=".bd-example-modal-xl-3">
                        <center>
                            <div class="col-12">
                                <h4 class="card-category">
                                    <span class="t-color"> Total Valuation Performance </span>
                                    <br>
                                    <span style="float:left"><?= number_format(Yii::$app->appHelperFunctions->getTotalReceivedCountFee()['record_count']) ?> </span>
                                    <span style="float:right">
                                    <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                        <?= number_format(Yii::$app->appHelperFunctions->getTotalReceivedCountFee()['total_fee']) ?>
                                    <?php } ?>
                            </span>
                                </h4>
                            </div>
                        </center>
                    </button>
                </div>

                <div class="modal fade bd-example-modal-xl-3" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"> Total Valuation Performance </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="card-body" style="background: #E5E4E2">
                                <div class="row px-4">

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_received')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:5px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Total
                                                Received</span><br><?= number_format(Yii::$app->appHelperFunctions->getTotalReceivedCountFee()['record_count']) ?>
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33])) { ?>
                                                                <?= number_format(Yii::$app->appHelperFunctions->getTotalReceivedCountFee()['total_fee']) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px; color:#6A1B9A;"
                                                           href="<?= yii\helpers\Url::toRoute(['valuation/dashboard-report-all', 'ValuationSearch[page_title]' => 'Total Received', 'ValuationSearch[time_period]' => '4']); ?>">
                                                            <span style="font-size:15px;">Go to Total Received</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_inspected')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:5px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Total
                                                Inspected</span><br><?= number_format(Yii::$app->appHelperFunctions->getTotalInspectedCountFee()['record_count']) ?>
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33])) { ?>
                                                                <?= number_format(Yii::$app->appHelperFunctions->getTotalInspectedCountFee()['total_fee']) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px; color:#6A1B9A;"
                                                           href="<?= yii\helpers\Url::toRoute(['valuation/dashboard-report-all', 'ValuationSearch[target]' => 'total_inspected', 'ValuationSearch[page_title]' => 'Total Inspected', 'ValuationSearch[time_period]' => '4']); ?>">
                                                            <span style="font-size:15px;">Go to Total Inspected</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_approved')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:5px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Total
                                                Approved</span><br><?= number_format($totalValuation) ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format($totalValuation_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px; color:#6A1B9A;"
                                                           href="<?= yii\helpers\Url::toRoute(['valuation/dashboard-report-all', 'ValuationSearch[page_title]' => 'Total Approved' , 'ValuationSearch[time_period]' => '4' ]); ?>">
                                                            <span style="font-size:15px;">Go to Total Approved</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_challenged')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Total
                                                Challenged </span>
                                                            <br><?= number_format($valuationReport_challneged) ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>

                                                                <?= number_format($valuationReport_challneged_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px; color:#6A1B9A;"
                                                           href="<?= yii\helpers\Url::toRoute(['valuation/dashboard-report-all', 'ValuationSearch[page_title]' => 'Total Challenged', 'ValuationSearch[time_period]' => '4' , 'ValuationSearch[revised_reason]' => [1, 2]]); ?>">
                                                            <span style="font-size:15px;">Go to Challenged</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_mistakes')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Total Mistakes</span>
                                                            <br><?= number_format($valuationReport_mistakes) ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>

                                                                <?= number_format($valuationReport_mistakes_fee) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px; color:#6A1B9A;"
                                                           href="<?= yii\helpers\Url::toRoute(['valuation/dashboard-report-all', 'ValuationSearch[page_title]' => 'Total Mistakes', 'ValuationSearch[time_period]' => '4' , 'ValuationSearch[revised_reason]' => [3]]); ?>">
                                                            <span style="font-size:15px;">Go to Mistakes</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_tat_performance')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Overall TAT Performance</span>
                                                            <br><?= $overall_tat['record_count'] ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>
                                                                <?= number_format( $overall_tat['total_fee']) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/tat-reports']) ?>"> <span
                                                                    style="font-size:15px;">Go to Overall TAT</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="col-lg-4 col-md-6 col-sm-6 py-3 d-none">
                                        <div id="w0" class="card-stats">
                                            <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                                                <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                    <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                </div>
                                                <div class=" col-10 position-absolute" style="right:0px;" >
                                                    <h4  class="card-category font-increase text-right" style="color:#757575"> Total Inspection Scheduled <br><?= $total_schedule_insp ?></h4 >
                                                    <h4 class=" float-right font-increase" style="color:#757575" >
                                                        <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                                                            <?= number_format($total_schedule_insp_fee)?>
                                                        <?php } ?>
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="card-footer bg-white">
                                                <div class="stats position-relative" style="box-sizing: border-box;">
                                                    <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                    <a class="position-absolute text-info footer-increase" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/valuations-to-inspect?time=overall'])?>"> <span style="font-size:15px;">Go to Overall Inspection Scheduled</span> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_reminders')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:5px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">&nbsp; <span class="today-text-info"> Total
                                                Reminders </span> <br> <?= $total_all_reminders['record_count'] ?> </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33])) { ?>
                                                                <?= number_format($total_all_reminders['total_fee']) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['valuation/dashboard-report-all', 'ValuationSearch[reminder_id]' => [8], 'ValuationSearch[target]' => 'total_reminders', 'ValuationSearch[time_period]' => '4' ,'ValuationSearch[page_title]' => 'Total Reminders']) ?>">
                                                            <span style="font-size:15px;">Go to Total Reminders</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_ahead')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575"> <span class="today-text-info"> Total Ahead of
                                                Time </span>
                                                            <br><?= $query_totalAhead_of_time_count ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px; color:#6A1B9A;"
                                                           href="<?= yii\helpers\Url::toRoute(['reports/tat-ahead-reports', 'ValuationReportsSearch[target]' => 'all_aheadoftime', 'ValuationReportsSearch[time_period]' => '4' ,'ValuationReportsSearch[page_title]' => 'Total Ahead of Time Reports']); ?>">
                                                            <span style="font-size:15px;">Go to Total Ahead of Time</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_ontime')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Total On Time </span>
                                                            <br><?= $query_totalOn_time_count ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px; color:#6A1B9A;"
                                                           href="<?= yii\helpers\Url::toRoute(['reports/tat-on-time-reports', 'ValuationReportsSearch[target]' => 'all_ontime','ValuationReportsSearch[time_period]' => '4' ,'ValuationReportsSearch[page_title]' => 'Total On Time']); ?>">
                                                            <span style="font-size:15px;">Go to Total On Time</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_delay')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Total Delay </span>
                                                            <br><?= $query_totalDelay_count ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info text-success footer-increase" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/tat-delay-reports', 'ValuationReportsSearch[target]' => 'all_delay','ValuationReportsSearch[time_period]' => '4' ,'ValuationReportsSearch[page_title]' => 'Total Delay Reports']) ?>">
                                                            <span style="font-size:15px;">Go to Total Delay </span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_cancelled')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                                                        <i class="fas fa-history pt-1 pl-3" style="font-size: 38px;"></i>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">
                                                            <span class="today-text-info"> Total Cancelled </span>
                                                            <br><?= number_format($total_cancelled['record_count']) ?></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                            <?php if (in_array(Yii::$app->user->identity->id, [1, 14, 110465, 33, 6319])) { ?>

                                                                <?= number_format($total_cancelled['total_fee']) ?>
                                                            <?php } ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                        <a class="position-absolute today-text-info" style="left:53px;top:5px; color:#6A1B9A;"
                                                           href="<?= yii\helpers\Url::toRoute(['valuation/dashboard-report-all','ValuationSearch[time_period]' => '4' ,'ValuationSearch[page_title]' => 'Total Cancelled', 'ValuationSearch[valuation_status]' => [9]]); ?>">
                                                            <span style="font-size:15px;">Go to Total Cancelled</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- </div>
            </div> -->
            <?php } ?>


            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('proposal_dashboard')) { ?>
                <?php echo QuotationTodayDynamicWidgets::widget() ?>
            <?php } ?>

            <?php if( Yii::$app->menuHelperFunction->checkActionAllowed('pending_proposal_dashboard') ) { ?>
                <?php echo QuotationTodayWidgets::widget() ?>
            <?php } ?>

            <?php if( Yii::$app->menuHelperFunction->checkActionAllowed('total_proposal_dashboard') ) { ?>
                <?php echo QuotationPendingWidgets::widget() ?>
            <?php } ?>

            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('business_performance')) { ?>
                <!-- <div class="container"> -->
                <!-- <div class="row mt-4 mb-4"> -->
                <div class="col-lg-4 text-center mt-4">
                    <button type="button" class="button" data-toggle="modal" data-target=".bd-example-modal-xl-7">

                        <center>
                            <div class=" col-12">
                                <h4 class="card-category">
                                    <span class="t-color-3"> Business Performance </span>
                                    <br>
                                    <span style="float:left"> </span>
                                    <span style="float:right"> </span>

                                </h4>
                            </div>
                        </center>
                    </button>
                </div>

                <div class="modal fade bd-example-modal-xl-7" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"> Business Performance </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="card-body" style="background: #E5E4E2">
                                <div class="row px-4">

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('revenue_by_corporate')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Revenue By
                                                            Corporate</h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/client-revenue-corporate']) ?>"> <span
                                                                    style="font-size:15px;">Go to Revenue By Corporate</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('revenue_by_individual')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Revenue By
                                                            Individual</h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/client-revenue-individual']) ?>"> <span
                                                                    style="font-size:15px;">Go to Revenue By Individual</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('revenue_by_property')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Revenue By
                                                            Property
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/client-revenue-by-property-type']) ?>"> <span
                                                                    style="font-size:15px;">Go to Revenue By Property</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('revenue_by_cities')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Revenue By
                                                            Cities
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/client-revenue-by-cities']) ?>"> <span
                                                                    style="font-size:15px;">Go to Revenue By Cities</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('revenue_by_banks')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Revenue By
                                                            Banks
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/client-revenue-banks']) ?>"> <span
                                                                    style="font-size:15px;">Go to Revenue By Banks</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('periodical_valuations_by_client')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Periodical Valuations By
                                                            Clients
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/client-revenue-banks-statistics']) ?>"> <span
                                                                    style="font-size:15px;">Go to Periodically Clients Valuations </span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('periodical_fee_by_clients')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Periodical Fee By
                                                            Clients
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/client-revenue-banks-fee']) ?>"> <span
                                                                    style="font-size:15px;">Go to Periodically Fee By Clients </span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            <?php } ?>


            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('data_performance')) { ?>
                <div class="col-lg-4 text-center mt-4">
                    <button type="button" class="button" data-toggle="modal" data-target=".bd-example-modal-xl-8">

                        <center>
                            <div class=" col-12">
                                <h4 class="card-category">
                                    <span class="t-color-3"> Data Performance </span>
                                    <br>
                                    <span style="float:left"> </span>
                                    <span style="float:right"> </span>

                                </h4>
                            </div>
                        </center>
                    </button>
                </div>

                <div class="modal fade bd-example-modal-xl-8" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"> Data Performance </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="card-body" style="background: #E5E4E2">
                                <div class="row px-4">

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('sold_duplicates')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Sold Duplicates
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/sold-duplicates']) ?>"> <span
                                                                    style="font-size:15px;">Go to Sold Duplicates</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('list_duplicates')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">List Duplicates
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/list-duplicates']) ?>"> <span
                                                                    style="font-size:15px;">Go to List Duplicates</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('list_duplicates_reference')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">List Duplicates
                                                            Reference</h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/list-duplicates-reference']) ?>"> <span
                                                                    style="font-size:15px;">Go to List Duplicates Ref</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('sold_not_uploaded')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Sold Not
                                                            Uploaded
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/sold-not-uploaded']) ?>"> <span
                                                                    style="font-size:15px;">Go to Sold Not Uploaded</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('windmills_performance')) { ?>
                <div class="col-lg-4 text-center mt-4">
                    <button type="button" class="button" data-toggle="modal" data-target=".bd-example-modal-xl-9">

                        <center>
                            <div class=" col-12">
                                <h4 class="card-category">
                                    <span class="t-color-3"> Windmills Performance </span>
                                    <br>
                                    <span style="float:left"> </span>
                                    <span style="float:right"> </span>

                                </h4>
                            </div>
                        </center>
                    </button>
                </div>

                <div class="modal fade bd-example-modal-xl-9" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"> Windmills Performance </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="card-body" style="background: #E5E4E2">
                                <div class="row px-4">

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('total_revenue')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Total
                                                            Revenue</h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/client-revenue-all']) ?>"> <span
                                                                    style="font-size:15px;">Go to Total Revenue</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('valuers_performance')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Valuers Performance
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/valuers-performance-data', 'ValuationReportsSearch[time_period]' => '2']) ?>"> <span
                                                                    style="font-size:15px;">Go to Valuers Performance</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('revenue_by_inspector')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Revenue By
                                                            Inspector
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/client-revenue-by-inspect']) ?>"> <span
                                                                    style="font-size:15px;">Go to Revenue By Inspector</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('approver_records')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Approver Records
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/client-revenue-by-approvers']) ?>"> <span
                                                                    style="font-size:15px;">Go to Approver Records</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('periodical_revenue_comparison')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Periodical Revenue
                                                            comparison
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/client-revenue-allnew']) ?>"> <span
                                                                    style="font-size:15px;">Go to Comparison</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('tat_reports')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Tat
                                                            Reports</h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/tat-reports']) ?>"> <span
                                                                    style="font-size:15px;">Go to Tat Reports</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('gross_by_property_type')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Gross Yield By
                                                            Property Type</h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/gross-yield-by-property-type']) ?>"> <span
                                                                    style="font-size:15px;">Go to G-Y By Property Type</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('low_high_valuations')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Low and High
                                                            Valuations</h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/low-high-valuations']) ?>"> <span
                                                                    style="font-size:15px;">Go to Low/High Valuations</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('valuation_statistics')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">description</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Valuation
                                                            Statistics </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575">
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                                                        <a class="position-absolute text-info" style="left:53px;top:5px;"
                                                           href="<?= yii\helpers\Url::to(['reports/valuation-status-all']) ?>"> <span
                                                                    style="font-size:15px;">Go to Valuation Statistics</span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- </div>
            </div> -->
            <?php } ?>

            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('business_master_files')) { ?>
                <!-- <div class="container"> -->
                <!-- <div class="row mb-4"> -->

                <div class="col-lg-4 text-center mt-4">
                    <button type="button" class="button" data-toggle="modal" data-target=".bd-example-modal-xl-10">

                        <center>
                            <div class=" col-12">
                                <h4 class="card-category">
                                    <span class="t-color-4"> Business Master Files </span>
                                    <br>
                                    <span style="float:left"> </span>
                                    <span style="float:right"> </span>

                                </h4>
                            </div>
                        </center>
                    </button>
                </div>


                <div class="modal fade bd-example-modal-xl-10" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"> Business Master Files </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="card-body" style="background: #E5E4E2">
                                <div class="row px-4">

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('business_master_files')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class=" py-2 shadow rounded ml-2"
                                                         style="width:60px; height:60px; background-color:#795548">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">money</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Total Clients
                                                            <br>
                                                        </h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#795548;">money</i>
                                                        <a class="position-absolute" style="left:53px;top:5px; "
                                                           href="<?= yii\helpers\Url::to(['client/index']) ?>"> <span
                                                                    style="font-size:20px; color:#795548;">Go to Clients </span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('business_master_files')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class=" py-2 shadow rounded ml-2"
                                                         style="width:60px; height:60px; background-color:#795548">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">money</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Total Prospects
                                                            <br></h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#795548;">money</i>
                                                        <a class="position-absolute" style="left:53px;top:5px; "
                                                           href="<?= yii\helpers\Url::to(['client/prospects']) ?>"> <span
                                                                    style="font-size:20px; color:#795548;">Go to Prospects </span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('business_master_files')) { ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                            <div id="w0" class="card-stats">
                                                <div
                                                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                    <div class=" py-2 shadow rounded ml-2"
                                                         style="width:60px; height:60px; background-color:#795548">
                                                        <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                            style="font-size:40px">money</i></div>
                                                    </div>
                                                    <div class=" col-10 position-absolute" style="right:0px;">
                                                        <h4 class="card-category font-increase text-right" style="color:#757575">Total
                                                            Contacts</h4>
                                                        <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-white footer-increase">
                                                    <div class="stats position-relative" style="box-sizing: border-box;">
                                                        <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#795548;">money</i>
                                                        <a class="position-absolute" style="left:53px;top:5px; "
                                                           href="<?= yii\helpers\Url::to(['contact/index']) ?>"> <span
                                                                    style="font-size:20px; color:#795548;">Go to Contacts </span> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('valuation_master_files')) { ?>
                <div class="col-lg-4 text-center mt-4">
                    <button type="button" class="button" data-toggle="modal" data-target=".bd-example-modal-xl-11">

                        <center>
                            <div class=" col-12">
                                <h4 class="card-category">
                                    <span class="t-color-4"> Valuation Master Files </span>
                                    <br>
                                    <span style="float:left"> </span>
                                    <span style="float:right"> </span>

                                </h4>
                            </div>
                        </center>
                    </button>
                </div>

                <div class="modal fade bd-example-modal-xl-11" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"> Valuation Master Files </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="card-body" style="background: #E5E4E2">
                                <div class="row px-4">
                                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                        <div id="w0" class="card-stats">
                                            <div
                                                    class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                <div class=" py-2 shadow rounded ml-2 "
                                                     style="width:60px; height:60px; background-color:#FFA000;">
                                                    <div class="card-icon pt-2 pl-2">
                                                        <i class="material-icons text-white pl-1" style="font-size:40px">business</i>
                                                    </div>
                                                </div>
                                                <div class=" col-10 position-absolute" style="right:0px;">
                                                    <h4 class="card-category font-increase text-right" style="color:#757575">Total Building
                                                        &
                                                        Projects <br><?= number_format($total_Buildings) ?></h4>
                                                    <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                </div>
                                            </div>
                                            <div class="card-footer bg-white footer-increase">
                                                <div class="stats position-relative" style="box-sizing: border-box;">
                                                    <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#FFA000;">business</i>
                                                    <a class="position-absolute" style="left:53px;top:5px;color:#FFA000;"
                                                       href="<?= yii\helpers\Url::to(['buildings/index']) ?>"> <span
                                                                style="font-size:15px;">Go
                                                    to Projects</span> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                        <div id="w0" class="card-stats">
                                            <div
                                                    class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                <div class=" py-2 shadow rounded ml-2 bg-success" style="width:60px; height:60px;">
                                                    <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                        style="font-size:40px">check_circle</i></div>
                                                </div>
                                                <div class=" col-10 position-absolute" style="right:5px;">
                                                    <h4 class="card-category font-increase text-right" style="color:#757575">Total
                                                        Communities
                                                        <br><?= number_format($total_communities) ?> </h4>
                                                    <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                </div>
                                            </div>
                                            <div class="card-footer bg-white footer-increase">
                                                <div class="stats position-relative" style="box-sizing: border-box;">
                                                    <i class="material-icons text-success pt-1 pl-3" style="font-size:30px">check_circle</i>
                                                    <a class="position-absolute text-success" style="left:53px;top:5px;"
                                                       href="<?= yii\helpers\Url::to(['communities/index']) ?>"> <span
                                                                style="font-size:15px;">Go to Communities </span> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                        <div id="w0" class="card-stats">
                                            <div
                                                    class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                                <div class=" py-2 shadow rounded ml-2 bg-success" style="width:60px; height:60px;">
                                                    <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                                                                        style="font-size:40px">check_circle</i></div>
                                                </div>
                                                <div class=" col-10 position-absolute" style="right:5px;">
                                                    <h4 class="card-category font-increase text-right" style="color:#757575">Total
                                                        Developers
                                                        <br><?= number_format($total_developers) ?> </h4>
                                                    <h4 class=" float-right font-increase" style="color:#757575"></h4>
                                                </div>
                                            </div>
                                            <div class="card-footer bg-white footer-increase">
                                                <div class="stats position-relative" style="box-sizing: border-box;">
                                                    <i class="material-icons text-success pt-1 pl-3" style="font-size:30px">check_circle</i>
                                                    <a class="position-absolute text-success" style="left:53px;top:5px;"
                                                       href="<?= yii\helpers\Url::to(['developers/index']) ?>"> <span
                                                                style="font-size:15px;">Go
                                                    to Developers </span> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('operation_master_files')) { ?>
                <div class="col-lg-4 text-center mt-4">
                    <button type="button" class="button" data-toggle="modal" data-target=".bd-example-modal-xl-12">
                        <center>
                            <div class=" col-12">
                                <h4 class="card-category">
                                    <span class="t-color-4"> Operation Master Files </span>
                                    <br>
                                    <span style="float:left"> </span>
                                    <span style="float:right"> </span>

                                </h4>
                            </div>
                        </center>
                    </button>
                </div>

                <div class="modal fade bd-example-modal-xl-12" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"> Operation Master Files </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <h3> No data to display .. !! </h3>
                        </div>
                    </div>
                </div>
                <!-- </div>
            </div> -->
            <?php } ?>
        </div>
    </div>
<?php }else{

    /*   $totalResults_all = (new \yii\db\Query())
           ->select(' COUNT(valuation.id) as count,SUM(valuation.total_fee) as fee_sum')
           ->from('schedule_inspection')
           ->innerJoin('valuation', "schedule_inspection.valuation_id=valuation.id")
           ->andWhere(['valuation.valuation_status' => [2]])
           ->andWhere(['valuation.parent_id' => null])
           ->all();*/

    $totalResults_today_all = (new \yii\db\Query())
        ->select(' COUNT(valuation.id) as count,SUM(valuation.total_fee) as fee_sum')
        ->from('schedule_inspection')
        ->innerJoin('valuation', "schedule_inspection.valuation_id=valuation.id")
        ->where(['schedule_inspection.inspection_date' => date('Y-m-d')])
        ->andWhere(['valuation.valuation_status' => [2]])
        ->andWhere(['schedule_inspection.inspection_officer' => Yii::$app->user->identity->id])
        ->andWhere(['valuation.parent_id' => null])
        ->all();

    /*    $totalResults_individual_all = (new \yii\db\Query())
            ->select(' COUNT(valuation.id) as count,SUM(valuation.total_fee) as fee_sum')
            ->from('schedule_inspection')
            ->innerJoin('valuation', "schedule_inspection.valuation_id=valuation.id")
            ->where(['valuation.valuation_status' => [2]])
            ->andWhere(['schedule_inspection.inspection_officer' => Yii::$app->user->identity->id])
            ->andWhere(['valuation.parent_id' => null])
            ->all();*/

    /*    $totalResults_today_individual_all = (new \yii\db\Query())
            ->select(' COUNT(valuation.id) as count,SUM(valuation.total_fee) as fee_sum')
            ->from('schedule_inspection')
            ->innerJoin('valuation', "schedule_inspection.valuation_id=valuation.id")
            ->where(['schedule_inspection.inspection_date' => date('Y-m-d')])
            ->andWhere(['valuation.valuation_status' => [2]])
            ->andWhere(['schedule_inspection.inspection_officer' => Yii::$app->user->identity->id])
            ->andWhere(['valuation.parent_id' => null])
            ->all();*/

    /*    echo "<pre>";
        print_r($totalResults_all);
        print_r($totalResults_today_all);
        print_r($totalResults_individual_all);
        print_r($totalResults_today_individual_all);
        die;*/

    ?>


    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
            <div id="w0" class="card-stats">
                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                    </div>
                    <div class=" col-10 position-absolute" style="right:0px;" >
                        <h4  class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Over All My Pending Inspections</span><br><?= $totalResults_today_all[0]['count'] ?></h4 >
                        <h4 class=" float-right font-increase" style="color:#757575" >

                        </h4>
                    </div>
                </div>
                <div class="card-footer bg-white">
                    <div class="stats position-relative" style="box-sizing: border-box;">
                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;color:#C0CA33;" href="<?= yii\helpers\Url::to(['valuation/inspections', 'ValuationSearch[target]' => 'from_dashboard', 'ValuationSearch[page_title]' => 'Pending Individual Inspection Scheduled'])?>"> <span style="font-size:15px;">Go to Pending Inspections Scheduled</span> </a>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-4 col-md-6 col-sm-6 py-3">
            <div id="w0" class="card-stats">
                <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                    <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                        <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                    </div>
                    <div class=" col-10 position-absolute" style="right:0px;" >
                        <h4  class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Pending Inspection Scheduled </span><br><?= $pending_total_schedule_insp ?></h4 >
                        <h4 class=" float-right font-increase" style="color:#757575" >
                            <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= number_format($pending_insp_count_fee) ?>
                            <?php } ?>
                        </h4>
                    </div>
                </div>
                <div class="card-footer bg-white">
                    <div class="stats position-relative" style="box-sizing: border-box;">
                        <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                        <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;color:#C0CA33;" href="<?= yii\helpers\Url::to(['valuation/index', 'ValuationSearch[target]' => 'from_dashboard', 'ValuationSearch[page_title]' => 'Pending Inspection Scheduled'])?>"> <span style="font-size:15px;">Go to Pending Inspection Scheduled</span> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--   <div class="col-lg-4 col-md-6 col-sm-6 py-3">
        <div id="w0" class="card-stats">
            <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                </div>
                <div class=" col-10 position-absolute" style="right:0px;" >
                    <h4  class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Today My Pending Inspections</span><br><?/*= $totalResults_today_individual_all[0]['count'] */?></h4 >
                    <h4 class=" float-right font-increase" style="color:#757575" >

                    </h4>
                </div>
            </div>
            <div class="card-footer bg-white">
                <div class="stats position-relative" style="box-sizing: border-box;">
                    <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                    <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;color:#C0CA33;" href="<?/*= yii\helpers\Url::to(['valuation/inspections', 'ValuationSearch[target]' => 'from_dashboard', 'ValuationSearch[page_title]' => 'All Pending Inspection Scheduled'])*/?>"> <span style="font-size:15px;">Go to Pending Inspections Scheduled</span> </a>
                </div>
            </div>
        </div>
    </div>-->

<?php }





$totalResults_individual_all = (new \yii\db\Query())
    ->select(' COUNT(valuation.id) as count,SUM(valuation.total_fee) as fee_sum')
    ->from('schedule_inspection')
    ->innerJoin('valuation', "schedule_inspection.valuation_id=valuation.id")
    ->where(['schedule_inspection.inspection_date' => date('Y-m-d')])
    ->andWhere(['valuation.valuation_status' => [2]])
    ->andWhere(['schedule_inspection.inspection_officer' => Yii::$app->user->identity->id])
    ->andWhere(['valuation.parent_id' => null])
    ->all();
?>



    <!-- <div class="col-lg-4 col-md-6 col-sm-6 py-3">
        <div id="w0" class="card-stats">
            <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #017BFD !important;">
                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                </div>
                <div class=" col-10 position-absolute" style="right:0px;" >
                    <h4  class="card-category font-increase text-right" style="color:#757575"><span class="today-text-info">Over All My Pending Inspections</span><br><?/*= $totalResults_individual_all[0]['count'] */?></h4 >
                    <h4 class=" float-right font-increase" style="color:#757575" >

                    </h4>
                </div>
            </div>
            <div class="card-footer bg-white">
                <div class="stats position-relative" style="box-sizing: border-box;">
                    <i class="fas fa-arrow-right today-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                    <a class="position-absolute today-text-info footer-increase" style="left:53px;top:5px;color:#C0CA33;" href="<?/*= yii\helpers\Url::to(['valuation/inspections', 'ValuationSearch[target]' => 'from_dashboard', 'ValuationSearch[page_title]' => 'Pending Inspections Scheduled'])*/?>"> <span style="font-size:15px;">Go to Pending Inspections Scheduled</span> </a>
                </div>
            </div>
        </div>
    </div>-->


<?php
$this->registerJs('
    setTimeout(function() {
        location.reload();
    }, 5 * 60 * 1000); // Refresh after 5 minutes (5 minutes * 60 seconds * 1000 milliseconds)
');
?>