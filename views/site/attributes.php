<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;


$this->title = Yii::t('app', 'Attributes Data');
$cardTitle = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['site/attributes-data']];
$this->params['breadcrumbs'][] = $cardTitle;
?>

<?php $form = ActiveForm::begin(); ?>
    <section class="communities-form card card-outline card-primary">
        <div class="col-6 my-3 mx-2">
            <label for=""><h5><b>Transaction Limit</h5></b></label>
            <input  type="number"  name="AttributesData[transaction_limit]"
                    value="<?= \Yii::$app->helperFunctions->getTransactionData(); ?>" placeholder="1" class="form-control"/>
        </div>
        <header class="card-header">
            <h2 class="card-title">Location Attributes</h2>
            <div class="card-tools">
                <a href="<?= Url::toRoute(['site/get-master-file-history?file_type=attributes-data']) ?>" target="_blank" class="btn btn-info btn-sm mx-2"><i class="fas fa-history mx-1"></i>History</a>
            </div>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <strong>2 minutes</strong>
                            </td>
                            <td>
                                <strong>5 minutes</strong>
                            </td>
                            <td>
                                <strong>10 minutes</strong>
                            </td>
                            <td>
                                <strong>15 minutes</strong>
                            </td>
                            <td>
                                <strong>20 minutes</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                           $drive_to_highway_data =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'location', 'type' => 'drive_to_highway'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Drive to Highway/Main <br> Road and metro</strong>
                            </td>
                            <td>

                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_highway][minutes_2]"
                                           value="<?= (isset($drive_to_highway_data['minutes_2'])) ? $drive_to_highway_data['minutes_2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_highway][minutes_5]"
                                           value="<?= (isset($drive_to_highway_data['minutes_5'])) ? $drive_to_highway_data['minutes_5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_highway][minutes_10]"
                                           value="<?= (isset($drive_to_highway_data['minutes_10'])) ? $drive_to_highway_data['minutes_10'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_highway][minutes_15]"
                                           value="<?= (isset($drive_to_highway_data['minutes_15'])) ? $drive_to_highway_data['minutes_15'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_highway][minutes_20]"
                                           value="<?= (isset($drive_to_highway_data['minutes_20'])) ? $drive_to_highway_data['minutes_20'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <?php
                            $drive_to_school =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'location', 'type' => 'drive_to_school'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Drive to school</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_school][minutes_2]"
                                           value="<?= (isset($drive_to_school['minutes_2'])) ? $drive_to_school['minutes_2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_school][minutes_5]"
                                            value="<?= (isset($drive_to_school['minutes_5'])) ? $drive_to_school['minutes_5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_school][minutes_10]"
                                            value="<?= (isset($drive_to_school['minutes_10'])) ? $drive_to_school['minutes_10'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_school][minutes_15]"
                                            value="<?= (isset($drive_to_school['minutes_15'])) ? $drive_to_school['minutes_15'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_school][minutes_20]"
                                            value="<?= (isset($drive_to_school['minutes_20'])) ? $drive_to_school['minutes_20'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <?php
                            $drive_to_commercial_area =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'location', 'type' => 'drive_to_commercial_area'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Drive to commercial area/Mall</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_commercial_area][minutes_2]"
                                            value="<?= (isset($drive_to_commercial_area['minutes_2'])) ? $drive_to_commercial_area['minutes_2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_commercial_area][minutes_5]"
                                            value="<?= (isset($drive_to_commercial_area['minutes_5'])) ? $drive_to_commercial_area['minutes_5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_commercial_area][minutes_10]"
                                            value="<?= (isset($drive_to_commercial_area['minutes_10'])) ? $drive_to_commercial_area['minutes_10'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_commercial_area][minutes_15]"
                                            value="<?= (isset($drive_to_commercial_area['minutes_15'])) ? $drive_to_commercial_area['minutes_15'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_commercial_area][minutes_20]"
                                            value="<?= (isset($drive_to_commercial_area['minutes_20'])) ? $drive_to_commercial_area['minutes_20'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <?php
                            $drive_to_special_landmark =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'location', 'type' => 'drive_to_special_landmark'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Drive to special landmark,<br> sea, marina, </strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_special_landmark][minutes_2]"
                                            value="<?= (isset($drive_to_special_landmark['minutes_2'])) ? $drive_to_special_landmark['minutes_2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_special_landmark][minutes_5]"
                                            value="<?= (isset($drive_to_special_landmark['minutes_5'])) ? $drive_to_special_landmark['minutes_5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_special_landmark][minutes_10]"
                                            value="<?= (isset($drive_to_special_landmark['minutes_10'])) ? $drive_to_special_landmark['minutes_10'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_special_landmark][minutes_15]"
                                            value="<?= (isset($drive_to_special_landmark['minutes_15'])) ? $drive_to_special_landmark['minutes_15'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_special_landmark][minutes_20]"
                                            value="<?= (isset($drive_to_special_landmark['minutes_20'])) ? $drive_to_special_landmark['minutes_20'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <?php
                            $drive_to_pool =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'location', 'type' => 'drive_to_pool'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Drive to pool and park</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_pool][minutes_2]"
                                            value="<?= (isset($drive_to_pool['minutes_2'])) ? $drive_to_pool['minutes_2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_pool][minutes_5]"
                                            value="<?= (isset($drive_to_pool['minutes_5'])) ? $drive_to_pool['minutes_5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_pool][minutes_10]"
                                            value="<?= (isset($drive_to_pool['minutes_10'])) ? $drive_to_pool['minutes_10'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_pool][minutes_15]"
                                            value="<?= (isset($drive_to_pool['minutes_15'])) ? $drive_to_pool['minutes_15'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[location][drive_to_pool][minutes_20]"
                                            value="<?= (isset($drive_to_pool['minutes_20'])) ? $drive_to_pool['minutes_20'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </section>

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">View Attributes</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <strong>Open area or road </strong>
                            </td>
                            <td>
                                <strong>Normal</strong>
                            </td>
                            <td>
                                <strong>Close buildings</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $community =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'view', 'type' => 'community'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Community &nbsp;&nbsp;</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[view][community][open_area]"
                                            value="<?= (isset($community['open_area'])) ? $community['open_area'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[view][community][normal]"
                                            value="<?= (isset($community['normal'])) ? $community['normal'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="AttributesData[view][community][close_buildings]"
                                            value="<?= (isset($community['close_buildings'])) ? $community['close_buildings'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                        </tr>
                        <tr>
                           <td colspan="4">

                           </td>
                        </tr>


                        </tbody>
                    </table>
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <strong>Full</strong>
                            </td>
                            <td>
                                <strong>Partial</strong>
                            </td>
                            <td>
                                <strong>None</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $pool_fountain =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'view', 'type' => 'pool_fountain'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Pool / Fountain</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][pool_fountain][full]"
                                            value="<?= (isset($pool_fountain['full'])) ? $pool_fountain['full'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][pool_fountain][partial]"
                                            value="<?= (isset($pool_fountain['partial'])) ? $pool_fountain['partial'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][pool_fountain][none]"
                                            value="<?= (isset($pool_fountain['none'])) ? $pool_fountain['none'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $burj =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'view', 'type' => 'burj'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Burj</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][burj][full]"
                                            value="<?= (isset($burj['full'])) ? $burj['full'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][burj][partial]"
                                            value="<?= (isset($burj['partial'])) ? $burj['partial'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][burj][none]"
                                            value="<?= (isset($burj['none'])) ? $burj['none'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $sea =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'view', 'type' => 'sea'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Sea</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][sea][full]"
                                            value="<?= (isset($sea['full'])) ? $sea['full'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][sea][partial]"
                                            value="<?= (isset($sea['partial'])) ? $sea['partial'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][sea][none]"
                                            value="<?= (isset($sea['none'])) ? $sea['none'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $marina =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'view', 'type' => 'marina'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Marina</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][marina][full]"
                                            value="<?= (isset($marina['full'])) ? $marina['full'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][marina][partial]"
                                            value="<?= (isset($marina['partial'])) ? $marina['partial'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][marina][none]"
                                            value="<?= (isset($marina['none'])) ? $marina['none'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $mountains =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'view', 'type' => 'mountains'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Mountains</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][mountains][full]"
                                            value="<?= (isset($mountains['full'])) ? $mountains['full'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][mountains][partial]"
                                            value="<?= (isset($mountains['partial'])) ? $mountains['partial'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][mountains][none]"
                                            value="<?= (isset($mountains['none'])) ? $mountains['none'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $lake =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'view', 'type' => 'lake'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Lake</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][lake][full]"
                                            value="<?= (isset($lake['full'])) ? $lake['full'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][lake][partial]"
                                            value="<?= (isset($lake['partial'])) ? $lake['partial'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][lake][none]"
                                            value="<?= (isset($lake['none'])) ? $lake['none'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $golf_course=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'view', 'type' => 'golf_course'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Golf Course</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][golf_course][full]"
                                            value="<?= (isset($golf_course['full'])) ? $golf_course['full'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][golf_course][partial]"
                                            value="<?= (isset($golf_course['partial'])) ? $golf_course['partial'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][golf_course][none]"
                                            value="<?= (isset($golf_course['none'])) ? $golf_course['none'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $park=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'view', 'type' => 'park'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Park</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][park][full]"
                                            value="<?= (isset($park['full'])) ? $park['full'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][park][partial]"
                                            value="<?= (isset($park['partial'])) ? $park['partial'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][park][none]"
                                            value="<?= (isset($park['none'])) ? $park['none'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $special_view =  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'view', 'type' => 'special_view'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Other Special view</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][special_view][full]"
                                            value="<?= (isset($special_view['full'])) ? $special_view['full'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][special_view][partial]"
                                            value="<?= (isset($special_view['partial'])) ? $special_view['partial'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[view][special_view][none]"
                                            value="<?= (isset($special_view['none'])) ? $special_view['none'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </section>

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">Upgrades Attributes</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <strong>Value </strong>
                            </td>
                            <td>
                                <strong>&nbsp;</strong>
                            </td>
                            <td>
                                <strong>&nbsp;</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $standard =  \app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'standard'])->one();
                            ?>
                            <td>
                                <strong>Standard &nbsp;&nbsp;</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][standard][standard]"
                                            value="<?= (isset($standard['value'])) ? $standard['value'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>


                            </td>
                            <td>

                            </td>

                        </tr>
                        <tr>
                            <td colspan="4">

                            </td>
                        </tr>


                        </tbody>
                    </table>
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <strong>Flooring</strong>
                            </td>
                            <td>
                                <strong>Ceilings</strong>
                            </td>
                            <td>
                                <strong>Speciality</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $bedrooms=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'bedrooms'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Bedrooms</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][bedrooms][flooring]"
                                            value="<?= (isset($bedrooms['flooring'])) ? $bedrooms['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][bedrooms][ceilings]"
                                            value="<?= (isset($bedrooms['ceilings'])) ? $bedrooms['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][bedrooms][speciality]"
                                            value="<?= (isset($bedrooms['speciality'])) ? $bedrooms['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $bathrooms=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'bathrooms'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Bathrooms</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][bathrooms][flooring]"
                                            value="<?= (isset($bathrooms['flooring'])) ? $bathrooms['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][bathrooms][ceilings]"
                                            value="<?= (isset($bathrooms['ceilings'])) ? $bathrooms['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][bathrooms][speciality]"
                                            value="<?= (isset($bathrooms['speciality'])) ? $bathrooms['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $kitchen=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'kitchen'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Kitchen</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][kitchen][flooring]"
                                            value="<?= (isset($kitchen['flooring'])) ? $kitchen['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][kitchen][ceilings]"
                                            value="<?= (isset($kitchen['ceilings'])) ? $kitchen['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][kitchen][speciality]"
                                            value="<?= (isset($kitchen['speciality'])) ? $kitchen['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $living_area=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'living_area'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Living Area</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][living_area][flooring]"
                                            value="<?= (isset($living_area['flooring'])) ? $living_area['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][living_area][ceilings]"
                                            value="<?= (isset($living_area['ceilings'])) ? $living_area['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][living_area][speciality]"
                                            value="<?= (isset($living_area['speciality'])) ? $living_area['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $dining_area=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'dining_area'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Dining Area</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][dining_area][flooring]"
                                            value="<?= (isset($living_area['flooring'])) ? $living_area['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][dining_area][ceilings]"
                                            value="<?= (isset($living_area['ceilings'])) ? $living_area['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][dining_area][speciality]"
                                            value="<?= (isset($living_area['speciality'])) ? $living_area['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $maid_rooms=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'maid_rooms'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Maid Rooms</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][maid_rooms][flooring]"
                                            value="<?= (isset($maid_rooms['flooring'])) ? $maid_rooms['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][maid_rooms][ceilings]"
                                            value="<?= (isset($maid_rooms['ceilings'])) ? $maid_rooms['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][maid_rooms][speciality]"
                                            value="<?= (isset($maid_rooms['speciality'])) ? $maid_rooms['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $laundry_area=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'laundry_area'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Laundry Area</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][laundry_area][flooring]"
                                            value="<?= (isset($laundry_area['flooring'])) ? $laundry_area['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>Bedrooms
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][laundry_area][ceilings]"
                                            value="<?= (isset($laundry_area['ceilings'])) ? $laundry_area['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][laundry_area][speciality]"
                                            value="<?= (isset($laundry_area['speciality'])) ? $laundry_area['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $store=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'store'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Store</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][store][flooring]"
                                            value="<?= (isset($store['flooring'])) ? $store['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][store][ceilings]"
                                            value="<?= (isset($store['ceilings'])) ? $store['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][store][speciality]"
                                            value="<?= (isset($store['speciality'])) ? $store['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $service_block=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'service_block'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Service Block</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][service_block][flooring]"
                                            value="<?= (isset($service_block['flooring'])) ? $service_block['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][service_block][ceilings]"
                                            value="<?= (isset($service_block['ceilings'])) ? $service_block['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][service_block][speciality]"
                                            value="<?= (isset($service_block['speciality'])) ? $service_block['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $garage=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'garage'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Garage</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][garage][flooring]"
                                            value="<?= (isset($garage['flooring'])) ? $garage['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][garage][ceilings]"
                                            value="<?= (isset($garage['ceilings'])) ? $garage['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][garage][speciality]"
                                            value="<?= (isset($garage['speciality'])) ? $garage['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $balcony=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'balcony'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Balcony</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][balcony][flooring]"
                                            value="<?= (isset($balcony['flooring'])) ? $balcony['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][balcony][ceilings]"
                                            value="<?= (isset($balcony['ceilings'])) ? $balcony['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][balcony][speciality]"
                                            value="<?= (isset($balcony['speciality'])) ? $balcony['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <?php
                            $powder_room=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'powder_room'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Powder Room</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][powder_room][flooring]"
                                            value="<?= (isset($powder_room['flooring'])) ? $powder_room['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][powder_room][ceilings]"
                                            value="<?= (isset($powder_room['ceilings'])) ? $powder_room['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][powder_room][speciality]"
                                            value="<?= (isset($powder_room['speciality'])) ? $powder_room['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $family_room=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'family_room'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Family Room</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][family_room][flooring]"
                                            value="<?= (isset($family_room['flooring'])) ? $family_room['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][family_room][ceilings]"
                                            value="<?= (isset($family_room['ceilings'])) ? $family_room['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][family_room][speciality]"
                                            value="<?= (isset($family_room['speciality'])) ? $family_room['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <?php
                            $study_room=  ArrayHelper::map(\app\models\AttributesData::find()->where(['attribute_name' => 'upgrades', 'type' => 'study_room'])->all(), 'key_name', 'value');
                            ?>
                            <td>
                                <strong>Study Room</strong>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][study_room][flooring]"
                                            value="<?= (isset($study_room['flooring'])) ? $study_room['flooring'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][study_room][ceilings]"
                                            value="<?= (isset($study_room['ceilings'])) ? $study_room['ceilings'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="AttributesData[upgrades][study_room][speciality]"
                                            value="<?= (isset($study_room['speciality'])) ? $study_room['speciality'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                        </tr>


                        </tbody>
                    </table>
        <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
                </div>
            </div>

        </div>

    </section>


    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php 
            // if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'file_type' => 'attributes-data',
                    'rec_type' => 'masterfile',
                ]);
            // }
        ?>
    </div>
<?php ActiveForm::end(); ?>