<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\TinyMceAsset;
TinyMceAsset::register($this);
use  app\components\widgets\StatusVerified;

$this->registerJs('


tinymce.init({
  selector: ".editor",

  menubar: false,
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table paste code help wordcount"
  ],
  toolbar: "undo redo | formatselect | " +
  "bold italic backcolor | alignleft aligncenter " +
  "alignright alignjustify | bullist numlist outdent indent | " +
  "removeformat | help",
});





');



/* @var $this yii\web\View */
/* @var $model app\models\StandardReport */
/* @var $form yii\widgets\ActiveForm */
?>



<section class="properties-form card card-outline card-primary">




    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title">Valuation Report - Market</h2>
    </header>

    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">

    <?= $form->field($model, 'client_intended_users')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'subject_property')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'valuation_instructions')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'purpose_valuation')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'terms_engagement_agreed')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'value_duties_supervision')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'internal_external_status_valuer')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'declaration_independence_objectivity')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'assum_extent_inesti_limit_scope_work')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'structural_tecnical_survey')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'conditions_state_repair_matenence')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'contamination_ground_enviormental')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'natural_disasters')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'national_scenaiors_majeure_situation')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'statutory_regulatory_requirements')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'title_tenancies_property_document')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'planning_highway_enquires')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'plant_equipment')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'development_property')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'disposal_cost_libility')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'documentation_provided_clients')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'documentation_not_provided_clients')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'nature_sources_info_doc')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'rics_valuation')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'basis_value')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'market_value')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'statutory_definition_marketvalue')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'market_rent')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'investment_value')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'fair_value')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'valuation_date')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'comparable_analyses')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'comparable_properties')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'market_commentary')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'summary_key_inputs')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'valuation')->textarea(['rows' =>6]) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'general_uncertainties')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'cronavirus_related_contingencies')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'confidentinality')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'restrictions_use_destribution_publication')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'property_valuation_overview')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'limitation_attaching_document')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'limitation_of_liability')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'other_terms_conditions')->textarea(['class'=>'editor']) ?>
  </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'restricted_marketing_period')->textarea(['class'=>'editor']) ?>
            </div>
  <div class="col-sm-12">
  <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
  </div>

    <div class="card-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?php 
                if($model<>null && $model->id<>null){
                    echo Yii::$app->appHelperFunctions->getLastActionHitory([
                        'model_id' => $model->id,
                        'model_name' => get_class($model),
                    ]);
                }
            ?>
    </div>

    <?php ActiveForm::end(); ?>

</section>
