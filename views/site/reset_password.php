<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\ResetPasswordForm */

$this->title = Yii::t('app','Reset Password');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(['id' => 'reset-password-form', 'options' => ['class'=>'forget-form1']]); ?>
    <h3><?= $this->title?></h3>
    <p>
         <?= Yii::t('app','Enter your new password.')?>
    </p>
    <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control form-control-solid placeholder-no-fix','autocomplete'=>'off','placeholder'=>$model->getAttributeLabel('password')])->label(false)?>
    <div class="form-group form-actions">
        <?= Html::submitButton(Yii::t('app','Change Password'), ['class' => 'btn btn-success uppercase btn-block', 'name' => 'fp-submit-button']) ?>
    </div>
    <br class="clearfix" />
<?php ActiveForm::end(); ?>
