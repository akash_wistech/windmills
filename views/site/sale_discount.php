<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;

$this->title = Yii::t('app', 'Sales Discount');
$cardTitle = Yii::t('app','Settings');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['site/sales-discounts']];
$this->params['breadcrumbs'][] = $cardTitle;

Yii::$app->session->set('model_name', 'ap\models\SaleDiscount');


?>
<section class="communities-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(); ?>
    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
        <div class="card-tools">
            <a href="<?= Url::toRoute(['site/get-history?id='.$model->id.'&m_name=SaleDiscount']) ?>" target="_blank" class="btn btn-info btn-sm">History</a>
        </div>
    </header>
    <div class="card-body">
<div class="row justify-content-md-center">
        <div class="col-sm-8">
            <table class="table table-striped ">
                <tbody>
                <tr>
                    <td>
                        <div class="required">
                            <label class="control-label" for="salediscount-upto_1_million">Upto 1 Million (%)</label>
                        </div>
                    </td>
                    <td>
                        <?= $form->field($model, 'upto_1_million')->textInput(['maxlength' => true])->label(false)?>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="required">
                            <label class="control-label" for="salediscount-upto_2_million">Upto 2 Million (%)</label>
                        </div>
                    </td>
                    <td>
                        <?= $form->field($model, 'upto_2_million')->textInput(['maxlength' => true])->label(false)?>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="required">
                            <label class="control-label" for="salediscount-upto_3_million">Upto 3 Million (%)</label>
                        </div>
                    </td>
                    <td>
                        <?= $form->field($model, 'upto_3_million')->textInput(['maxlength' => true])->label(false)?>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="required">
                            <label class="control-label" for="salediscount-upto_4_million">Upto 4 Million (%)</label>
                        </div>
                    </td>
                    <td>
                        <?= $form->field($model, 'upto_4_million')->textInput(['maxlength' => true])->label(false)?>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="required">
                            <label class="control-label" for="salediscount-upto_5_million">Upto 5 Million (%)</label>
                        </div>
                    </td>
                    <td>
                        <?= $form->field($model, 'upto_5_million')->textInput(['maxlength' => true])->label(false)?>

                    </td>
                </tr><tr>
                    <td>
                        <div class="required">
                            <label class="control-label" for="salediscount-upto_20_million">Upto 20 Million (%)</label>
                        </div>
                    </td>
                    <td>
                        <?= $form->field($model, 'upto_20_million')->textInput(['maxlength' => true])->label(false)?>

                    </td>
                </tr><tr>
                    <td>
                        <div class="required">
                            <label class="control-label" for="salediscount-upto_50_million">Upto 50 Million (%)</label>
                        </div>
                    </td>
                    <td>
                        <?= $form->field($model, 'upto_50_million')->textInput(['maxlength' => true])->label(false)?>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="required">
                            <label class="control-label" for="salediscount-upto_100_million">Upto 100 Million (%)</label>
                        </div>
                    </td>
                    <td>
                        <?= $form->field($model, 'upto_100_million')->textInput(['maxlength' => true])->label(false)?>

                    </td>
                </tr>


                </tbody>
            </table>
        <?= StatusVerified::widget([ 'model' => $model, 'form' => $form ]) ?>
        </div>
</div>

    </div>
    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php 
            if($model<>null && $model->id>0){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => 'app\models\SaleDiscount',
                ]);
            }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
