<?php 
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Windmills Documents');
?>

<section class="active-zone-form card">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="card-header">
        <h3 class="card-title text-bold">
            <i class="fas fa-edit"></i>
            <?= $this->title ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">About Windmills</label> <?php echo ($old_about_windmills->url<>null) ? '<a href="'.$old_about_windmills->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="about_windmills">
                            <?php 
                                if($old_about_windmills->url<>null){?>
                                    <label class="custom-file-label text-dark text-bold"><?= substr($old_about_windmills->base_name,0,50) ?></label>
                                <?php } else{ ?>
                                    <label class="custom-file-label text-dark text-bold">Choose file</label>
                                <?php }
                            ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="text-success">Corporate Profile</label>  <?php echo ($old_corporate_profile->url<>null) ? '<a href="'.$old_corporate_profile->url.'" target="_blank"  class="download-btnn mx-2" title="Download"> <i class="text-primary fas fa-download"></i> </a>' : '' ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="corporate_profile">
                            <?php 
                                if($old_corporate_profile->url<>null){?>
                                    <label class="custom-file-label text-dark text-bold"><?= substr($old_corporate_profile->base_name,0,50) ?></label>
                                <?php } else{ ?>
                                    <label class="custom-file-label text-dark text-bold">Choose file</label>
                                <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-footer bg-white text-center">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</section>