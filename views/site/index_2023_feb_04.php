<?php

use yii\helpers\Url;
use app\models\Valuation;
use app\models\Buildings;
use app\models\Lead;
use  app\components\widgets\QuotationAllTimeWidgets;
/* @var $this yii\web\View */
$this->title = Yii::t('app','Dashboard');
?>
<?php
$quotation_status=[];

$startDate = date('Y-m-d').' 00:00:00';
$EndDate = date('Y-m-d').' 23:59:59';


$quotation_status['inquiry_recieved'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status'=>0])
    ->andFilterWhere(['between', 'created_at', $startDate, $EndDate])
    ->asArray()
    ->all();




$quotation_status['quotation_sent'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_fee_approved) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status'=>1])
    ->andFilterWhere(['between', 'quotation_sent_date', $startDate, $EndDate])
    ->asArray()
    ->all();

$quotation_status['toe_sent'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status'=>3])
    ->andFilterWhere(['between', 'toe_sent_date', $startDate, $EndDate])
    ->asArray()
    ->all();

$quotation_status['toe_Sign_and_Rec']   = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['toe_signed_and_received'=>1])
    ->andFilterWhere(['between', 'toe_signed_and_received_date', $startDate, $EndDate])
    ->asArray()
    ->all();

$quotation_status['payment_recieved']   = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status'=>6])
    ->andFilterWhere(['between', 'payment_received_date', $startDate, $EndDate])
    ->asArray()
    ->all();

$quotation_status['on_hold'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status'=>10])
    ->andFilterWhere(['between', 'on_hold_date', $startDate, $EndDate])
    ->asArray()
    ->all();









$quotation_status['quotation_rejected']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->orWhere(['and',
        ['quotation_status' => 7],
        ['between', 'quotation_rejected_date', $startDate, $EndDate]
    ])
    ->orWhere(['and',
        ['quotation_status' => 13],
        ['between', 'status_change_date', $startDate, $EndDate]
    ])
    ->asArray()
    ->all();



$quotation_status['toe_rejected']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])

    ->orWhere(['and',
        ['quotation_status' => 8],
        ['between', 'toe_rejected_date', $startDate, $EndDate]
    ])
    ->orWhere(['and',
        ['quotation_status' => 13],
        ['between', 'status_change_date', $startDate, $EndDate]
    ])
    ->asArray()
    ->all();



// echo "<pre>"; print_r($quotation_status['toe_rejected']); echo "</pre>"; die;

$quotation_status['cancelled']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status'=>11])
    ->andFilterWhere(['between', 'cancelled_date', $startDate, $EndDate])
    ->asArray()
    ->all();

$quotation_status['regretted']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status'=>12])
    ->andFilterWhere(['between', 'regretted_date', $startDate, $EndDate])
    ->asArray()
    ->all();


$quotation_status['closed']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status'=>13])
    ->andFilterWhere(['between', 'status_change_date', $startDate, $EndDate])
    ->asArray()
    ->all();




$quotation_status['quotation_approved'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->orWhere(['and',
        ['quotation_status' => 6],
        ['between', 'payment_received_date', $startDate, $EndDate]
    ])
    ->orWhere(['and',
        ['toe_signed_and_received' => 1],
        ['between', 'toe_signed_and_received_date', $startDate, $EndDate]
    ])
    ->asArray()
    ->all();





$quotation_status['active_quotation']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->andFilterWhere([ 'not in', 'quotation_status',  [6,11,12,13] ])
    ->andFilterWhere([ 'not in', 'toe_signed_and_received' , 1 ])
    ->andFilterWhere(['between', 'status_change_date', $startDate, $EndDate])
    ->asArray()
    ->all();





// echo "<pre>"; print_r( $quotation_status['closed'] ); echo "</pre> <br><br>";die();



?>

    <div class="row px-4">
        <?php
        $BarChartData='';
        //number count for client, buildings, valuation steps.
        $totalValuation =(int)Valuation::find()->count('id');
        $totalValuation_fee =(int)Valuation::find()->sum('fee');
        $clients =(int)\app\models\Company::find()->count('id');
        $buildings =Buildings::find()->count('id');


        //Valuation steps such as received, reviewed. approved etc.
        foreach (Yii::$app->helperFunctions->valuationStatusListArr as $key => $value) {
            $valuationReport[$key] =(int)Valuation::find()->where(['valuation_status'=>$key])->count('id');
        }

        foreach (Yii::$app->helperFunctions->valuationStatusListArr as $key => $value) {
            $valuationReport_fee[$key] =(int)Valuation::find()->where(['valuation_status'=>$key])->sum('fee');
        }
        $valuationReport[9] =(int)Valuation::find()->where(['valuation_status'=>9])->count('id');

        $valuationReport_today =(int)Valuation::find()->where(['valuation_status'=>'1'])->andFilterWhere(['instruction_date'=> date('Y-m-d')])->count('id');

        $total_sub_communities =(int)\app\models\SubCommunities::find()->count('id');
        $total_Buildings =(int)\app\models\Buildings::find()->where(['trashed'=>'0'])->count('id');

        $valuationReport_active =(int)Valuation::find()->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]])->count('id');
        $valuationReport_active_fee =(float)Valuation::find()->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]])->sum('fee');

        $valuationReport_today_receieved =(int)Valuation::find()->where(['instruction_date'=> date('Y-m-d')])->count('id');
        $valuationReport_today_receieved_fee =(float)Valuation::find()->where(['instruction_date'=> date('Y-m-d')])->sum('fee');

        $valuationReport_cancel_today =(int)Valuation::find()->where(['valuation_status'=>'9'])->andFilterWhere(['instruction_date'=> date('Y-m-d')])->count('id');
        $valuationReport_cancel_today_fee =(float)Valuation::find()->where(['valuation_status'=>'9'])->andFilterWhere(['instruction_date'=> date('Y-m-d')])->sum('fee');
        $valuationReport_challneged_today =(int)Valuation::find()->where(['revised_reason'=>[1,2]])->andFilterWhere(['instruction_date'=> date('Y-m-d')])->count('id');
        $valuationReport_challneged_today =(float)Valuation::find()->where(['revised_reason'=>[1,2]])->andFilterWhere(['instruction_date'=> date('Y-m-d')])->sum('fee');
        $valuationReport_challneged =(int)Valuation::find()->where(['revised_reason'=>[1,2]])->count('id');
        $valuationReport_challneged_fee =(float)Valuation::find()->where(['revised_reason'=>[1,2]])->sum('fee');
        $valuationReport_active_challneged =(int)Valuation::find()->where(['revised_reason'=>[1,2]])->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]])->count('id');
        $valuationReport_active_challneged_fee =(float)Valuation::find()->where(['revised_reason'=>[1,2]])->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]])->sum('fee');
        $valuationReport_today_inspected=  \Yii::$app->db->createCommand('SELECT COUNT(valuation.id) as inspected
FROM valuation
INNER JOIN schedule_inspection ON valuation.id=schedule_inspection.valuation_id
WHERE schedule_inspection.inspection_date ="2021-11-23" AND
valuation.valuation_status NOT IN(6,9)')->execute();
        $totalResults = (new \yii\db\Query())
            ->select(' COUNT(valuation.id) as count,SUM(valuation.fee) as fee_sum')
            ->from('valuation')
            ->innerJoin('schedule_inspection',"schedule_inspection.valuation_id=valuation.id")

            //->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
            ->where(['schedule_inspection.inspection_date' => date('Y-m-d')])
            ->andWhere(['valuation.valuation_status' => [1,2,3,4,5,7,8]])
            ->all();
        $valuationReport_today_inspected = $totalResults[0]['count'];
        $valuationReport_today_inspected_fee = $totalResults[0]['fee_sum'];


        $today_start_date = date('Y-m-d').' 00:00:00';
        $today_end_date = date('Y-m-d').' 23:59:59';

        $totalResults_inspect = (new \yii\db\Query())
            ->select('SUM(valuation.fee) as fee_sum')
            ->from('valuation')
            ->innerJoin('inspect_property',"inspect_property.valuation_id=valuation.id")

            //->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
            ->andFilterWhere(['between','inspection_done_date',$today_start_date, $today_end_date])
            ->andWhere(['valuation.valuation_status' => [1,2,3,4,5,7,8]])
            ->all();

        $totalResults_approved = (new \yii\db\Query())
            ->select('SUM(valuation.fee) as fee_sum')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data',"valuation_approvers_data.valuation_id=valuation.id")

            //->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
            ->andFilterWhere(['between','valuation_approvers_data.created_at',$today_start_date, $today_end_date])
            ->andWhere(['valuation.valuation_status' => [1,2,3,4,5,7,8]])
            ->andWhere(['valuation_approvers_data.approver_type' =>'approver'])
            ->all();




        $valuationReport_today_inspected_done =(int)\app\models\InspectProperty::find()->andFilterWhere(['between','inspection_done_date',$today_start_date, $today_end_date])->count('id');
        $valuationReport_today_inspected_done_fee =$totalResults_inspect[0]['fee_sum'];
        $valuationReport_today_approved =(int)\app\models\ValuationApproversData::find()->where(['approver_type'=>'approver'])->andFilterWhere(['between','created_at',$today_start_date, $today_end_date])->count('id');
        $valuationReport_today_approved_fee =$totalResults_approved[0]['fee_sum'];
        $valuationReport_challneged_today =(int)Valuation::find()->where(['revised_reason'=>[1,2]])->andFilterWhere(['between','created_at',$today_start_date, $today_end_date])->count('id');
        $valuationReport_challneged_today_fee =(int)Valuation::find()->where(['revised_reason'=>[1,2]])->andFilterWhere(['between','created_at',$today_start_date, $today_end_date])->sum('fee');

        if(Yii::$app->user->identity->permission_group_id == 15){
            foreach (Yii::$app->helperFunctions->valuationStatusListArr as $key => $value) {
                $valuationReport[$key] =(int)Valuation::find()->where(['valuation_status'=>$key])->andWhere(['client_id'=>Yii::$app->user->identity->company_id ])->count('id');
                $totalValuation =(int)Valuation::find()->where(['client_id'=>Yii::$app->user->identity->company_id])->count('id');
            }
        }


        // echo "<pre>";
        // print_r($valuationByyearjson);
        // echo "<pre>";
        // die();
        ?>
        <style>
            h4{
                font-size: large !important;
            }
        </style>
    </div>


    <?php  if(Yii::$app->menuHelperFunction->checkActionAllowed('today_dashboard')){ ?>
    <section class="valuation-form card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title">Today Valuation Performance</h2>
        </header>
        <div class="card-body" style="background: #E5E4E2">
            <div class="row px-4">

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Today Received <br><?= $valuationReport_today_receieved ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                                <?= number_format($valuationReport_today_receieved_fee)?>
                                <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[instruction_date]'=> date('Y-m-d')])?>"> <span style="font-size:18px;">Go to Received</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow rounded ml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:5px;" >
                                <h4  class="card-category text-right" style="color:#757575">&nbsp; Today Challenged <br><?= $valuationReport_challneged_today ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>

                              <?= number_format($valuationReport_challneged_today)?>
                                <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[revised_reason]'=> [1,2],'ValuationSearch[challenge]'=> date('Y-m-d')])?>"> <span style="font-size:18px;">Go to Challenged</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:5px;" >
                                <h4  class="card-category text-right" style="color:#757575">Inspections Planned <br><?= $valuationReport_today_inspected ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>

                                <?= number_format($valuationReport_today_inspected_fee)?>
                                <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[inspection_date]'=> date('Y-m-d')])?>"> <span style="font-size:18px;">Go to Inspected</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Today Inspected <br><?= $valuationReport_today_inspected_done ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                                <?= number_format($valuationReport_today_inspected_done_fee)?>
                                <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[inspection_done_date]'=> date('Y-m-d')])?>"> <span style="font-size:18px;">Go to Inspected</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Today Approved <br><?= $valuationReport_today_approved ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                                    <?= number_format($valuationReport_today_approved_fee)?>
                                <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/approved-today-list'])?>"> <span style="font-size:18px;">Go to Approved</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2" style="width:90px; height:80px; background-color:#C62828">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">money</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Today Cancelled <br><?= $valuationReport_cancel_today  ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                               <?= number_format($valuationReport_cancel_today)?>
                                <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#C62828;" >money</i>
                                <a class="position-absolute" style="left:53px;top:5px; " href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 9,'ValuationSearch[instruction_date]'=> date('Y-m-d')])?>"> <span style="font-size:20px; color:#C62828;">Go to Cancelled</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
        <?php } ?>
    <?php  if(Yii::$app->menuHelperFunction->checkActionAllowed('active_dashboard')){ ?>
    <section class="valuation-form card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title">Active Valuation Performance</h2>
        </header>
        <div class="card-body" style="background: #E5E4E2">
            <div class="row px-4">
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >

                                <h4  class="card-category text-right" style="color:#757575">Active Valuations <br><?= $valuationReport_active ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                                <?= number_format($valuationReport_active_fee) ?>
                                <?php } ?>
                                </h4>

                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/index'])?>"> <span style="font-size:18px;">Go to Active</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Active Valuation Received <br><?= $valuationReport[1] ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                               <?= number_format($valuationReport_fee[1]) ?>
                                <?php } ?>
                                </h4>
                            </div>

                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 1])?>"> <span style="font-size:18px;">Go to Received</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                            <div class="col-4 py-2 shadow roundedml-2 " style="width:90px; height:80px; background-color:#C0CA33;">
                                <div class="card-icon pt-2 pl-2">
                                    <i class="material-icons text-white pl-1" style="font-size:60px">business</i>
                                </div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Active Inspection Requested <br><?= $valuationReport[2] ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>

                               <?= number_format($valuationReport_fee[2]) ?>
                                <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#C0CA33;" >business</i>
                                <a class="position-absolute" style="left:53px;top:5px;color:#C0CA33;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 2])?>"> <span style="font-size:18px;">Go to Inspection Requested</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                            <div class="col-4 py-2 shadow roundedml-2 " style="width:90px; height:80px; background-color:#C0CA33;">
                                <div class="card-icon pt-2 pl-2">
                                    <i class="material-icons text-white pl-1" style="font-size:60px">business</i>
                                </div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Active Valuation Challenged <br><?= $valuationReport_active_challneged ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                               <?= number_format($valuationReport_active_challneged_fee) ?>
                                <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#C0CA33;" >business</i>
                                <a class="position-absolute" style="left:53px;top:5px;color:#C0CA33;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> [1,2,3,4,7,8],'ValuationSearch[revised_reason]'=> [1,2],'ValuationSearch[instruction_date]'=> date('Y-m-d')])?>"> <span style="font-size:18px;">Go to Challenged</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                            <div class="col-4 py-2 shadow roundedml-2 bg-success" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">assignment</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Active Documents Requested <br><?=  $valuationReport[7] ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                               <?= number_format($valuationReport_fee[7]) ?>
                                <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-success pt-1 pl-3" style="font-size:30px" >assignment</i>
                                <a class="position-absolute text-success" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 7]); ?>"> <span style="font-size:18px;">Go to Documents Requested</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                            <div class="col-4 py-2 shadow roundedml-2 " style="width:90px; height:80px; background-color:#FFA000;">
                                <div class="card-icon pt-2 pl-2">
                                    <i class="material-icons text-white pl-1" style="font-size:60px">business</i>
                                </div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Active Inspected <br><?= $valuationReport[3] ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                                <?= number_format($valuationReport_fee[3]) ?>
                                <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#FFA000;" >business</i>
                                <a class="position-absolute" style="left:53px;top:5px;color:#FFA000;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 3])?>"> <span style="font-size:18px;">Go to Inspected</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    <?php  if(Yii::$app->menuHelperFunction->checkActionAllowed('total_dashboard')){ ?>
    <section class="valuation-form card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title">Total Valuation Performance</h2>
        </header>
        <div class="card-body" style="background: #E5E4E2">
            <div class="row px-4">
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                            <div class="col-4 py-2 shadow roundedml-2"  style="width:90px; height:80px; background-color:#6A1B9A;">
                                <div class="card-icon pt-2 pl-2">
                                    <i class="material-icons text-white pl-1" style="font-size:60px">pending_actions</i>
                                </div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:5px;" >
                                <h4  class="card-category text-right" style="color:#757575">Total Valuations <br><?= number_format($totalValuation) ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                                <?= number_format($totalValuation_fee) ?>
                                <?php } ?>
                                </h4>
                        </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#6A1B9A;" >pending_actions</i>
                                <a class="position-absolute" style="left:53px;top:5px; color:#6A1B9A;" href="<?= yii\helpers\Url::toRoute(['valuation/report-all']); ?>"> <span style="font-size:18px;">Go to Valuations</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                            <div class="col-4 py-2 shadow roundedml-2"  style="width:90px; height:80px; background-color:#6A1B9A;">
                                <div class="card-icon pt-2 pl-2">
                                    <i class="material-icons text-white pl-1" style="font-size:60px">pending_actions</i>
                                </div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Total Valuations Challenged <br><?= number_format($valuationReport_challneged) ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>

                                <?= number_format($valuationReport_challneged_fee) ?>
                            <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#6A1B9A;" >pending_actions</i>
                                <a class="position-absolute" style="left:53px;top:5px; color:#6A1B9A;" href="<?= yii\helpers\Url::toRoute(['valuation/report-all','ValuationSearch[revised_reason]'=> [1,2]]); ?>"> <span style="font-size:18px;">Go to Challenged</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                            <div class="col-4 py-2 shadow roundedml-2 bg-success" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">check_circle</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:5px;" >
                                <h4  class="card-category text-right" style="color:#757575">Total Sub Communities <br><?= number_format($total_sub_communities) ?> </h4 >
                                <h4 class=" float-right"></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-success pt-1 pl-3" style="font-size:30px" >check_circle</i>
                                <a class="position-absolute text-success" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['sub-communities/index'])?>"> <span style="font-size:18px;">Go to Sub-Communities </span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                            <div class="col-4 py-2 shadow roundedml-2 " style="width:90px; height:80px; background-color:#FFA000;">
                                <div class="card-icon pt-2 pl-2">
                                    <i class="material-icons text-white pl-1" style="font-size:60px">business</i>
                                </div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:0px;" >
                                <h4  class="card-category text-right" style="color:#757575">Total Building & Projects <br><?=number_format($total_Buildings) ?></h4 >
                                <h4 class=" float-right"></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#FFA000;" >business</i>
                                <a class="position-absolute" style="left:53px;top:5px;color:#FFA000;" href="<?= yii\helpers\Url::to(['buildings/index'])?>"> <span style="font-size:18px;">Go to Projects</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2" style="width:90px; height:80px; background-color:#795548">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">money</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Total Clients <br><?= $clients ?></h4 >
                                <h4 class=" float-right"></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#795548;" >money</i>
                                <a class="position-absolute" style="left:53px;top:5px; " href="<?= yii\helpers\Url::to(['client/index'])?>"> <span style="font-size:20px; color:#795548;">Go to Clients </span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2" style="width:90px; height:80px; background-color:#C62828">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">money</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Total Cancelled <br><?= $valuationReport[9]  ?></h4 >
                                <h4 class=" float-right">
                                    <?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
                               <?= number_format($valuationReport[9])  ?>
                            <?php } ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#C62828;" >money</i>
                                <a class="position-absolute" style="left:53px;top:5px; " href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 9])?>"> <span style="font-size:20px; color:#C62828;">Go to Cancelled</span> </a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <?php } ?>
    <?php  if(Yii::$app->menuHelperFunction->checkActionAllowed('proposal_dashboard')){ ?>




<?php
$quotation_status=[];

$startDate = date('Y-m-d').' 00:00:00';
$EndDate = date('Y-m-d').' 23:59:59';
$quotation_status['inquiry_recieved'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->andFilterWhere(['between', 'created_at', $startDate, $EndDate])
    ->andFilterWhere(['in', 'quotation_status', [0,1,3,6,10,7,13,8,11,12]])
    ->asArray()
    ->all();





$quotation_status['quotation_sent'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->andFilterWhere(['between', 'quotation_sent_date', $startDate, $EndDate])
    ->andFilterWhere(['in', 'quotation_status', [0,1,3,6,10,7,13,8,11,12]])
    ->asArray()
    ->all();

// dd($quotation_status);



$quotation_status['toe_sent'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->andFilterWhere(['between', 'toe_sent_date', $startDate, $EndDate])
    ->andFilterWhere(['in', 'quotation_status', [0,1,3,6,10,7,13,8,11,12]])
    ->asArray()
    ->all();

$quotation_status['toe_Sign_and_Rec']   = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->where(['toe_signed_and_received'=>1])
    ->andFilterWhere(['between', 'toe_signed_and_received_date', $startDate, $EndDate])
    ->asArray()
    ->all();

$quotation_status['payment_recieved']   = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->andFilterWhere(['between', 'payment_received_date', $startDate, $EndDate])
    ->andFilterWhere(['in', 'quotation_status', [0,1,3,6,10,7,13,8,11,12]])
    ->asArray()
    ->all();

$quotation_status['on_hold'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->andFilterWhere(['between', 'on_hold_date', $startDate, $EndDate])
    ->andFilterWhere(['in', 'quotation_status', [0,1,3,6,10,7,13,8,11,12]])
    ->asArray()
    ->all();


$quotation_status['quotation_rejected']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->orWhere(['and',
        ['quotation_status' => [7]],
        ['between', 'quotation_rejected_date', $startDate, $EndDate]
    ])
    ->orWhere(['and',
        ['quotation_status' => [13]],
        ['between', 'status_change_date', $startDate, $EndDate]
    ])
    ->asArray()
    ->all();



$quotation_status['toe_rejected']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])

    ->orWhere(['and',
        ['quotation_status' => [8]],
        ['between', 'toe_rejected_date', $startDate, $EndDate]
    ])
    ->orWhere(['and',
        ['quotation_status' => [13]],
        ['between', 'status_change_date', $startDate, $EndDate]
    ])
    ->asArray()
    ->all();



// echo "<pre>"; print_r($quotation_status['toe_rejected']); echo "</pre>"; die;

$quotation_status['cancelled']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->andFilterWhere(['between', 'cancelled_date', $startDate, $EndDate])
    ->andFilterWhere(['in', 'quotation_status', [11]])
    ->asArray()
    ->all();

$quotation_status['regretted']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->andFilterWhere(['between', 'regretted_date', $startDate, $EndDate])
    ->andFilterWhere(['in', 'quotation_status', [12]])
    ->asArray()
    ->all();


$quotation_status['closed']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->andFilterWhere(['between', 'status_change_date', $startDate, $EndDate])
    ->andFilterWhere(['in', 'quotation_status', [13]])
    ->asArray()
    ->all();




$quotation_status['quotation_approved'] = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->orWhere(['and',
        ['quotation_status' => [0,1,3,6,10,7,13,8,11,12]],
        ['between', 'payment_received_date', $startDate, $EndDate]
    ])
    ->orWhere(['and',
        ['toe_signed_and_received' => 1],
        ['between', 'toe_signed_and_received_date', $startDate, $EndDate]
    ])
    ->asArray()
    ->all();





$quotation_status['active_quotation']  = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(toe_final_fee) as totalFee , count(id) as totalRecords')])
    ->andFilterWhere([ 'not in', 'quotation_status',  [6,11,12,13] ])
    ->andFilterWhere([ 'not in', 'toe_signed_and_received' , 1 ])
    ->andFilterWhere(['between', 'status_change_date', $startDate, $EndDate])
    ->asArray()
    ->all();

?>




    <?= QuotationAllTimeWidgets::widget() ?>






<?php } ?>


<?php  if(Yii::$app->menuHelperFunction->checkActionAllowed('ahead_of_time_dashboard')){ ?>
    <section class="valuation-form card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title">Ahead of Time Performance</h2>
        </header>
        <div class="card-body" style="background: #E5E4E2">
            <div class="row px-4">
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                            <div class="col-4 py-2 shadow roundedml-2"  style="width:90px; height:80px; background-color:#6A1B9A;">
                                <div class="card-icon pt-2 pl-2">
                                    <i class="material-icons text-white pl-1" style="font-size:60px">pending_actions</i>
                                </div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Ahead of Time <br><?= $query_ahead_of_time_count ?></h4 >
                                <h4 class=" float-right"></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#6A1B9A;" >pending_actions</i>
                                <a class="position-absolute" style="left:53px;top:5px; color:#6A1B9A;" href="<?= yii\helpers\Url::toRoute(['reports/tat-ahead-reports']); ?>"> <span style="font-size:18px;">Go to Ahead of Time</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                            <div class="col-4 py-2 shadow roundedml-2"  style="width:90px; height:80px; background-color:#6A1B9A;">
                                <div class="card-icon pt-2 pl-2">
                                    <i class="material-icons text-white pl-1" style="font-size:60px">pending_actions</i>
                                </div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">On Time <br><?= $query_on_time_count ?></h4 >
                                <h4 class=" float-right"></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#6A1B9A;" >pending_actions</i>
                                <a class="position-absolute" style="left:53px;top:5px; color:#6A1B9A;" href="<?= yii\helpers\Url::toRoute(['reports/tat-on-time-reports']); ?>"> <span style="font-size:18px;">Go to On Time</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                            <div class="col-4 py-2 shadow roundedml-2 bg-success" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">check_circle</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Delay <br><?= $query_delay_count ?></h4 >
                                <h4 class=" float-right"></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-success pt-1 pl-3" style="font-size:30px" >check_circle</i>
                                <a class="position-absolute text-success" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/tat-delay-reports'])?>"> <span style="font-size:18px;">Go to Delay </span> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<?php if(in_array( Yii::$app->user->identity->id,[1,14])) { ?>
    <section class="valuation-form card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title">Inquiries And Reports</h2>
        </header>
        <div class="card-body" style="background: #E5E4E2">
            <div class="row px-4">
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Total Revenue</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/client-revenue-all'])?>"> <span style="font-size:18px;">Go to Total Revenue</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Revenue By Valuers</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/client-revenue-by-valuers'])?>"> <span style="font-size:18px;">Go to Revenue By Valuers</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Revenue By Individual</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/client-revenue-individual">'])?>"> <span style="font-size:18px;">Go to Revenue By Individual</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Revenue By Property</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/client-revenue-by-property-type'])?>"> <span style="font-size:18px;">Go to Revenue By Property</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Revenue By Corporate</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/client-revenue-corporate'])?>"> <span style="font-size:18px;">Go to Revenue By Corporate</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Revenue By Cities</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/client-revenue-by-cities'])?>"> <span style="font-size:18px;">Go to Revenue By Cities</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Revenue By Banks</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/client-revenue-banks'])?>"> <span style="font-size:18px;">Go to Revenue By Banks</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Revenue comparison by client segment</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/client-revenue-allnew'])?>"> <span style="font-size:18px;">Go to Revenue By Comparison</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Sold Duplicates</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/sold-duplicates'])?>"> <span style="font-size:18px;">Go to Sold Duplicates</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">List Duplicates</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/list-duplicates'])?>"> <span style="font-size:18px;">Go to List Duplicates</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">List Duplicates Reference</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/list-duplicates-reference'])?>"> <span style="font-size:18px;">Go to List Duplicates Reference</span> </a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Sold Not Uploaded</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/sold-not-uploaded'])?>"> <span style="font-size:18px;">Go to Sold Not Uploaded</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Tat Reports</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/tat-reports'])?>"> <span style="font-size:18px;">Go to Tat Reports</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Gross Yield By Property Type</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/gross-yield-by-property-type'])?>"> <span style="font-size:18px;">Go to G-Y By Property Type</span> </a>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Low and High Valuations</h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/low-high-valuations'])?>"> <span style="font-size:18px;">Go to Low/High Valuations</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="col-4 py-2 shadow roundedml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-8 position-absolute" style="right:20px;" >
                                <h4  class="card-category text-right" style="color:#757575">Valuation Statistics </h4 >
                                <h4 class=" float-right">
                                </h4>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['reports/valuation-status-all'])?>"> <span style="font-size:18px;">Go to Valuations By comparison</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
<?php } ?>