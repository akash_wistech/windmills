<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use  app\components\widgets\StatusVerified;


$this->title = Yii::t('app', 'Rating HR Module');
$cardTitle = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['site/rating-hr-data']];
$this->params['breadcrumbs'][] = $cardTitle;
?>

<?php $form = ActiveForm::begin(); ?>

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">Education - Academic Degrees</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                <strong>Secondary Schooling</strong>
                            </td>
                            <td>
                                <strong>High Schooling</strong>
                            </td>
                            <td>
                                <strong>Graduation</strong>
                            </td>
                            <td>
                                <strong>Post Graduation </strong>
                            </td>
                            <td>
                                <strong>Super Post Graduation</strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                           $academic_degrees_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'academic_degrees'])->all(), 'type', 'value');
                            ?>
                            <td>

                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="RatingHrData[academic_degrees][1]"
                                           value="<?= (isset($academic_degrees_data['1'])) ? $academic_degrees_data['1'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="RatingHrData[academic_degrees][2]"
                                           value="<?= (isset($academic_degrees_data['2'])) ? $academic_degrees_data['2'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="RatingHrData[academic_degrees][3]"
                                           value="<?= (isset($academic_degrees_data['3'])) ? $academic_degrees_data['3'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="RatingHrData[academic_degrees][4]"
                                           value="<?= (isset($academic_degrees_data['4'])) ? $academic_degrees_data['4'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                           name="RatingHrData[academic_degrees][5]"
                                           value="<?= (isset($academic_degrees_data['5'])) ? $academic_degrees_data['5'] : 0 ?>"
                                           placeholder="1" class="form-control"
                                           required
                                    />
                                </div>
                            </td>

                          <td>
                              <div class='form-group'>
                                  <select class='form-control' name='RatingHrData[academic_degrees][weightage]' required>

                                      <?php
                                      for($i=0; $i<=100; $i++) {
                                          if ((isset($academic_degrees_data['weightage'])) && $i == $academic_degrees_data['weightage']) {
                                              ?>
                                              <option selected value="<?= $i ?>"><?= $i ?></option>
                                          <?php } else {
                                              ?>
                                              <option value="<?= $i ?>"><?= $i ?></option>

                                          <?php }
                                      } ?>
                                  </select>
                              </div>
                          </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">Education - Professional Degrees</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                <strong>Professional Certification 1</strong>
                            </td>
                            <td>
                                <strong>Professional Certification 2</strong>
                            </td>
                            <td>
                                <strong>Professional Certification 3</strong>
                            </td>
                            <td>
                                <strong>Professional Certification 4 </strong>
                            </td>
                            <td>
                                <strong>Professional Degree</strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $professional_degrees_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'academic_degrees'])->all(), 'type', 'value');
                          /*  echo "<pre>";
                            print_r($professional_degrees_data);
                            die;*/

                            ?>
                            <td>

                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[professional_degrees][1]"
                                            value="<?= (isset($professional_degrees_data['1'])) ? $professional_degrees_data['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[professional_degrees][2]"
                                            value="<?= (isset($professional_degrees_data['2'])) ? $professional_degrees_data['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[professional_degrees][3]"
                                            value="<?= (isset($professional_degrees_data['3'])) ? $professional_degrees_data['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[professional_degrees][4]"
                                            value="<?= (isset($professional_degrees_data['4'])) ? $professional_degrees_data['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[professional_degrees][5]"
                                            value="<?= (isset($professional_degrees_data['5'])) ? $professional_degrees_data['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class='form-group'>
                                    <select class='form-control' name='RatingHrData[professional_degrees][weightage]' required>

                                        <?php
                                        for($i=0; $i<=100; $i++) {
                                            if ((isset($professional_degrees_data['weightage'])) && $i == $professional_degrees_data['weightage']) {
                                                ?>
                                                <option selected value="<?= $i ?>"><?= $i ?></option>
                                            <?php } else {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>

                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">Experience - Total</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                <strong><3</strong>
                            </td>
                            <td>
                                <strong><5</strong>
                            </td>
                            <td>
                                <strong><10</strong>
                            </td>
                            <td>
                                <strong><15</strong>
                            </td>
                            <td>
                                <strong>>15</strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $experience_total_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'experience_total'])->all(), 'type', 'value');
                            ?>
                            <td>

                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_total][1]"
                                            value="<?= (isset($experience_total_data['1'])) ? $experience_total_data['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_total][2]"
                                            value="<?= (isset($experience_total_data['2'])) ? $experience_total_data['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_total][3]"
                                            value="<?= (isset($experience_total_data['3'])) ? $experience_total_data['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_total][4]"
                                            value="<?= (isset($experience_total_data['4'])) ? $experience_total_data['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_total][5]"
                                            value="<?= (isset($experience_total_data['5'])) ? $experience_total_data['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class='form-group'>
                                    <select class='form-control' name='RatingHrData[experience_total][weightage]' required>

                                        <?php
                                        for($i=0; $i<=100; $i++) {
                                            if ((isset($experience_total_data['weightage'])) && $i == $experience_total_data['weightage']) {
                                                ?>
                                                <option selected value="<?= $i ?>"><?= $i ?></option>
                                            <?php } else {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>

                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">Experience - Relevant</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                <strong><1</strong>
                            </td>
                            <td>
                                <strong><3</strong>
                            </td>
                            <td>
                                <strong><5</strong>
                            </td>
                            <td>
                                <strong><10</strong>
                            </td>
                            <td>
                                <strong>>10</strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $experience_relevent_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'experience_relevent'])->all(), 'type', 'value');
                            ?>
                            <td>

                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_relevent][1]"
                                            value="<?= (isset($experience_relevent_data['1'])) ? $experience_relevent_data['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_relevent][2]"
                                            value="<?= (isset($experience_relevent_data['2'])) ? $experience_relevent_data['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_relevent][3]"
                                            value="<?= (isset($experience_relevent_data['3'])) ? $experience_relevent_data['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_relevent][4]"
                                            value="<?= (isset($experience_relevent_data['4'])) ? $experience_relevent_data['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_relevent][5]"
                                            value="<?= (isset($experience_relevent_data['5'])) ? $experience_relevent_data['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class='form-group'>
                                    <select class='form-control' name='RatingHrData[experience_relevent][weightage]' required>

                                        <?php
                                        for($i=0; $i<=100; $i++) {
                                            if ((isset($experience_relevent_data['weightage'])) && $i == $experience_relevent_data['weightage']) {
                                                ?>
                                                <option selected value="<?= $i ?>"><?= $i ?></option>
                                            <?php } else {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>

                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">Experience - Last 10 years (Organizations)</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                <strong>Left 5 organization</strong>
                            </td>
                            <td>
                                <strong>Left 4 organization</strong>
                            </td>
                            <td>
                                <strong>Left 3 organization</strong>
                            </td>
                            <td>
                                <strong>Left 2 organization</strong>
                            </td>
                            <td>
                                <strong>Left 1 organization</strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $experience_organization_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'experience_organization'])->all(), 'type', 'value');
                            ?>
                            <td>

                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_organization][1]"
                                            value="<?= (isset($experience_organization_data['1'])) ? $experience_organization_data['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_organization][2]"
                                            value="<?= (isset($experience_organization_data['2'])) ? $experience_organization_data['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_organization][3]"
                                            value="<?= (isset($experience_organization_data['3'])) ? $experience_organization_data['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_organization][4]"
                                            value="<?= (isset($experience_organization_data['4'])) ? $experience_organization_data['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[experience_organization][5]"
                                            value="<?= (isset($experience_organization_data['5'])) ? $experience_organization_data['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class='form-group'>
                                    <select class='form-control' name='RatingHrData[experience_organization][weightage]' required>

                                        <?php
                                        for($i=0; $i<=100; $i++) {
                                            if ((isset($experience_organization_data['weightage'])) && $i == $experience_organization_data['weightage']) {
                                                ?>
                                                <option selected value="<?= $i ?>"><?= $i ?></option>
                                            <?php } else {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>

                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">Performance - KPI/Performance Tasks</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                <strong>Task 1</strong>
                            </td>
                            <td>
                                <strong>Task 2</strong>
                            </td>
                            <td>
                                <strong>Task 3</strong>
                            </td>
                            <td>
                                <strong>Task 4</strong>
                            </td>
                            <td>
                                <strong>Task 5</strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $performance_kpi_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'performance_kpi'])->all(), 'type', 'value');
                            ?>
                            <td>

                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[performance_kpi][1]"
                                            value="<?= (isset($performance_kpi_data['1'])) ? $performance_kpi_data['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[performance_kpi][2]"
                                            value="<?= (isset($performance_kpi_data['2'])) ? $performance_kpi_data['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[performance_kpi][3]"
                                            value="<?= (isset($performance_kpi_data['3'])) ? $performance_kpi_data['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[performance_kpi][4]"
                                            value="<?= (isset($performance_kpi_data['4'])) ? $performance_kpi_data['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[performance_kpi][5]"
                                            value="<?= (isset($performance_kpi_data['5'])) ? $performance_kpi_data['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class='form-group'>
                                    <select class='form-control' name='RatingHrData[performance_kpi][weightage]' required>

                                        <?php
                                        for($i=0; $i<=100; $i++) {
                                            if ((isset($performance_kpi_data['weightage'])) && $i == $performance_kpi_data['weightage']) {
                                                ?>
                                                <option selected value="<?= $i ?>"><?= $i ?></option>
                                            <?php } else {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>

                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">Performance - Value Addition In last organization</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                <strong>Value Addition 1</strong>
                            </td>
                            <td>
                                <strong>Value Addition 2</strong>
                            </td>
                            <td>
                                <strong>Value Addition 3</strong>
                            </td>
                            <td>
                                <strong>Value Addition 4</strong>
                            </td>
                            <td>
                                <strong>Value Addition 5</strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $performance_value_addition_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'performance_value_addition'])->all(), 'type', 'value');
                            ?>
                            <td>

                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[performance_value_addition][1]"
                                            value="<?= (isset($performance_value_addition_data['1'])) ? $performance_value_addition_data['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[performance_value_addition][2]"
                                            value="<?= (isset($performance_value_addition_data['2'])) ? $performance_value_addition_data['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[performance_value_addition][3]"
                                            value="<?= (isset($performance_value_addition_data['3'])) ? $performance_value_addition_data['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[performance_value_addition][4]"
                                            value="<?= (isset($performance_value_addition_data['4'])) ? $performance_value_addition_data['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[performance_value_addition][5]"
                                            value="<?= (isset($performance_value_addition_data['5'])) ? $performance_value_addition_data['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class='form-group'>
                                    <select class='form-control' name='RatingHrData[performance_value_addition][weightage]' required>

                                        <?php
                                        for($i=0; $i<=100; $i++) {
                                            if ((isset($performance_value_addition_data['weightage'])) && $i == $performance_value_addition_data['weightage']) {
                                                ?>
                                                <option selected value="<?= $i ?>"><?= $i ?></option>
                                            <?php } else {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>

                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>




    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">Skills - Languages</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                <strong>1</strong>
                            </td>
                            <td>
                                <strong>2</strong>
                            </td>
                            <td>
                                <strong>3</strong>
                            </td>
                            <td>
                                <strong>4</strong>
                            </td>
                            <td>
                                <strong>5></strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $communication_skills_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'communication_skills'])->all(), 'type', 'value');
                            ?>
                            <td>

                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[communication_skills][1]"
                                            value="<?= (isset($communication_skills_data['1'])) ? $communication_skills_data['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[communication_skills][2]"
                                            value="<?= (isset($communication_skills_data['2'])) ? $communication_skills_data['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[communication_skills][3]"
                                            value="<?= (isset($communication_skills_data['3'])) ? $communication_skills_data['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[communication_skills][4]"
                                            value="<?= (isset($communication_skills_data['4'])) ? $communication_skills_data['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[communication_skills][5]"
                                            value="<?= (isset($communication_skills_data['5'])) ? $communication_skills_data['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class='form-group'>
                                    <select class='form-control' name='RatingHrData[communication_skills][weightage]' required>

                                        <?php
                                        for($i=0; $i<=100; $i++) {
                                            if ((isset($communication_skills_data['weightage'])) && $i == $communication_skills_data['weightage']) {
                                                ?>
                                                <option selected value="<?= $i ?>"><?= $i ?></option>
                                            <?php } else {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>

                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">Skills - Technological</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                <strong>1</strong>
                            </td>
                            <td>
                                <strong>2</strong>
                            </td>
                            <td>
                                <strong>3</strong>
                            </td>
                            <td>
                                <strong>4</strong>
                            </td>
                            <td>
                                <strong>5></strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $technological_skills_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'technological_skills'])->all(), 'type', 'value');
                            ?>
                            <td>

                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[technological_skills][1]"
                                            value="<?= (isset($technological_skills_data['1'])) ? $technological_skills_data['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[technological_skills][2]"
                                            value="<?= (isset($technological_skills_data['2'])) ? $technological_skills_data['2'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[technological_skills][3]"
                                            value="<?= (isset($technological_skills_data['3'])) ? $technological_skills_data['3'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[technological_skills][4]"
                                            value="<?= (isset($technological_skills_data['4'])) ? $technological_skills_data['4'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[technological_skills][5]"
                                            value="<?= (isset($technological_skills_data['5'])) ? $technological_skills_data['5'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class='form-group'>
                                    <select class='form-control' name='RatingHrData[technological_skills][weightage]' required>

                                        <?php
                                        for($i=0; $i<=100; $i++) {
                                            if ((isset($technological_skills_data['weightage'])) && $i == $technological_skills_data['weightage']) {
                                                ?>
                                                <option selected value="<?= $i ?>"><?= $i ?></option>
                                            <?php } else {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>

                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

   <!-- <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title">Skills - Ethics and Attitude</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <td>
                                <strong>Poor</strong>
                            </td>
                            <td>
                                <strong>Acceptable</strong>
                            </td>
                            <td>
                                <strong>Medium</strong>
                            </td>
                            <td>
                                <strong>Good</strong>
                            </td>
                            <td>
                                <strong>Excellent</strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
/*                            $ethics_and_attitude_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'ethics_and_attitude'])->all(), 'type', 'value');
                            */?>
                            <td>

                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[ethics_and_attitude][1]"
                                            value="<?/*= (isset($ethics_and_attitude_data['1'])) ? $ethics_and_attitude_data['1'] : 0 */?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[ethics_and_attitude][2]"
                                            value="<?/*= (isset($ethics_and_attitude_data['2'])) ? $ethics_and_attitude_data['2'] : 0 */?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[ethics_and_attitude][3]"
                                            value="<?/*= (isset($ethics_and_attitude_data['3'])) ? $ethics_and_attitude_data['3'] : 0 */?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[ethics_and_attitude][4]"
                                            value="<?/*= (isset($ethics_and_attitude_data['4'])) ? $ethics_and_attitude_data['4'] : 0 */?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[ethics_and_attitude][5]"
                                            value="<?/*= (isset($ethics_and_attitude_data['5'])) ? $ethics_and_attitude_data['5'] : 0 */?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>
                            </td>

                            <td>
                                <div class='form-group'>
                                    <select class='form-control' name='RatingHrData[ethics_and_attitude][weightage]' required>

                                        <?php
/*                                        for($i=0; $i<=100; $i++) {
                                            if ((isset($ethics_and_attitude_data['weightage'])) && $i == $ethics_and_attitude_data['weightage']) {
                                                */?>
                                                <option selected value="<?/*= $i */?>"><?/*= $i */?></option>
                                            <?php /*} else {
                                                */?>
                                                <option value="<?/*= $i */?>"><?/*= $i */?></option>

                                            <?php /*}
                                        } */?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>-->

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title"> Driving License</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>

                            <td>
                                <strong> Rating</strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $driving_license_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'driving_license'])->all(), 'type', 'value');
                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[driving_license][1]"
                                            value="<?= (isset($driving_license_data['1'])) ? $driving_license_data['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>


                            <td>
                                <div class='form-group'>
                                    <select class='form-control' name='RatingHrData[driving_license][weightage]' required>

                                        <?php
                                        for($i=0; $i<=100; $i++) {
                                            if ((isset($driving_license_data['weightage'])) && $i == $driving_license_data['weightage']) {
                                                ?>
                                                <option selected value="<?= $i ?>"><?= $i ?></option>
                                            <?php } else {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>

                                            <?php }
                                        } ?>
                                    </select>
                                </div>

                            </td>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="communities-form card card-outline card-primary">

        <header class="card-header">
            <h2 class="card-title"> Cover Letter</h2>
        </header>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-sm-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>

                            <td>
                                <strong>If Yes (if "No" then rating is 0)</strong>
                            </td>
                            <td>
                                <strong>Weightage (%)</strong>
                            </td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <?php
                            $cover_lettere_data =  ArrayHelper::map(\app\models\RatingHrData::find()->where(['attribute_name' => 'cover_letter'])->all(), 'type', 'value');
                            ?>
                            <td>
                                <div class="form-group">
                                    <input  type="number" step=".01"
                                            name="RatingHrData[cover_letter][1]"
                                            value="<?= (isset($cover_lettere_data['1'])) ? $cover_lettere_data['1'] : 0 ?>"
                                            placeholder="1" class="form-control"
                                            required
                                    />
                                </div>

                            </td>


                            <td>
                                <div class='form-group'>
                                    <select class='form-control' name='RatingHrData[cover_letter][weightage]' required>

                                        <?php
                                        for($i=0; $i<=100; $i++) {
                                            if ((isset($cover_lettere_data['weightage'])) && $i == $cover_lettere_data['weightage']) {
                                                ?>
                                                <option selected value="<?= $i ?>"><?= $i ?></option>
                                            <?php } else {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>

                                            <?php }
                                        } ?>
                                    </select>
                                </div>

                            </td>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="card-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php 
            // if($model<>null && $model->id<>null){
                echo Yii::$app->appHelperFunctions->getLastActionHitory([
                    'model_id' => $model->id,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'file_type' => 'attributes-data',
                    'rec_type' => 'masterfile',
                ]);
            // }
        ?>
    </div>
<?php ActiveForm::end(); ?>