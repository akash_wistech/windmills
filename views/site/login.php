<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="card">
  <div class="card-body login-card-body">
    <p class="login-box-msg"><?= Yii::t('app','Please fill out the following fields to login')?></p>
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    <?= $form->field($model, 'username',[
      'template'=>'
      <div class="mb-3">
        <div class="input-group">
          {input}
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        {error}
      </div>
      '
    ])
    ->textInput(['placeholder'=>$model->getAttributeLabel('username'),'autofocus' => true])->label(false) ?>
      <?= $form->field($model, 'password',[
        'template'=>'
        <div class="mb-3">
          <div class="input-group">
            {input}
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          {error}
        </div>
        '
      ])
      ->passwordInput(['placeholder'=>$model->getAttributeLabel('password')])->label(false) ?>
      <div class="row">
        <div class="col-8">
          <?= $form->field($model, 'rememberMe')->checkbox([
              'template' => "<div class=\"icheck-primary\">{input} {label}</div>\n<div>{error}</div>",
          ]) ?>
        </div>
        <div class="col-4">
          <?= Html::submitButton('Sign In', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
        </div>
      </div>
    <?php ActiveForm::end(); ?>
    <p class="mb-1">
      <a href="<?= Url::to(['site/forget-password'])?>"><?= Yii::t('app','I forgot my password')?></a>
    </p>
  </div>
</div>
